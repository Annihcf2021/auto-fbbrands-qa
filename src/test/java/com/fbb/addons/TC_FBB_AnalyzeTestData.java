package com.fbb.addons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.headers.Headers;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.SheetsQuickstart;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_AnalyzeTestData extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	final String spreadsheetId = configProperty.get("dataSourceID");
	
	@Test(groups = { "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void FBB_AnalyzeTestData(String browser) throws Exception {
		// Get the web driver instance
		Log.testCaseInfo("TestData Analysis: " + Utils.getCurrentBrand().getConfiguration());
		final WebDriver driver = WebDriverFactory.get(browser);
		List<String> checkedData = new ArrayList<String>();
		try {
			checkedData = checkData(TestData.getAllProductData(), driver);
		} // try
		catch (Exception e) {
			e.printStackTrace();
		} // catch
		finally {
			String columnKey = SheetsQuickstart.getColumnKey() + "2";
			SheetsQuickstart.updateData(spreadsheetId, "missingData", columnKey, checkedData);
			if (checkedData.size() == 0) {
				Log.pass("All Data are up-to-date for " + Utils.getCurrentBrand().getConfiguration());
			} else {
				Log.failsoft("Invalid datapoint count:: " + checkedData.size());
				Log.message("Invalid data keys:: " + checkedData.toString());
				Log.fail("All data are not up-to-date for " + Utils.getCurrentBrand().getConfiguration() + ".");
			}
			Log.endTestCase(driver);
		} // finally
	}// FBB_AnalyzeTestData

	public static void check_monetate(WebDriver driver) {
		try {
			WebElement mdl_monetate_lightbox = driver.findElement(By.cssSelector(".monetate_form_banner"));
			if(mdl_monetate_lightbox.isDisplayed()) {
				Log.message("Monetate Light Box displayed", driver);
				mdl_monetate_lightbox.sendKeys(Keys.ESCAPE);
			}
		}catch(NoSuchElementException e) {
			Log.event("No Monetate");
		}catch(Exception e1) {
			Log.failsoft(e1.getStackTrace().toString(), driver);
		}
	}
	
	public List<String> checkData(HashMap<String, String> testData, WebDriver driver) throws Exception {
		List<String> returnData = new ArrayList<String>();
		List<String> keys = new ArrayList<String>(testData.keySet());
		HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
		Headers header = homePage.headers;
		check_monetate(driver);
		HashMap<String, String> prdID = new HashMap<String, String>();
		for (int i = 0; i < testData.size(); i++) {
			List<String> missingDataRow = new ArrayList<String>();
			Log.message(i + ". " + keys.get(i) + ":: " + testData.get(keys.get(i)));
			String toSearch = testData.get(keys.get(i));
			if (keys.get(i).startsWith("Stock_V") || keys.get(i).contains("set"))
				continue;
			try {
				if (keys.get(i).contains("qc_")) {
					if(prdID.containsKey(keys.get(i))) {
						if(prdID.get(testData.get(keys.get(i))).equals("unavail")) {
							missingDataRow.add(keys.get(i)); missingDataRow.add(testData.get(keys.get(i))); missingDataRow.add(driver.getCurrentUrl());
							Log.printTableRow(missingDataRow, driver);
							returnData.add(keys.get(i));
						}
						continue;
					}
					if(header.verifyNavigatedToQuickOrderBySearchingKeyword()) {
						QuickOrderPage cqoPage = new QuickOrderPage(driver).get();
						cqoPage.searchCQOProduct(toSearch.split("\\|")[0]);
						check_monetate(driver);
						String[] verifyProdId = {toSearch.split("\\|")[0]};
						if(cqoPage.verifySearchedCatalogItemsDisplayed(verifyProdId)) {
							prdID.put(testData.get(keys.get(i)), "avail");
							cqoPage.removeAllProducts();
						}else {
							prdID.put(testData.get(keys.get(i)), "unavail");
							missingDataRow.add(keys.get(i)); missingDataRow.add(testData.get(keys.get(i))); missingDataRow.add(driver.getCurrentUrl());
							Log.printTableRow(missingDataRow, driver);
							returnData.add(keys.get(i));
						}
					} else {
						prdID.put(testData.get(keys.get(i)), "unavail");
						missingDataRow.add(keys.get(i)); missingDataRow.add(testData.get(keys.get(i))); missingDataRow.add(driver.getCurrentUrl());
						Log.printTableRow(missingDataRow, driver);
						returnData.add(keys.get(i));
					}
				} else if (keys.get(i).toLowerCase().contains("url")) { 
					if (header.verifyNavigateToPDPUsingUrl(testData.get(keys.get(i)))) {
						prdID.put(testData.get(keys.get(i)), "avail");
					} else {
						prdID.put(testData.get(keys.get(i)), "unavail");
						missingDataRow.add(keys.get(i)); missingDataRow.add(testData.get(keys.get(i))); missingDataRow.add(driver.getCurrentUrl());
						Log.printTableRow(missingDataRow, driver);
						returnData.add(keys.get(i));
					}
					
				} else {
					BrowserActions.scrollToTopOfPage(driver);
					if(header.verifyNavigatedToPdp(toSearch)) {
						prdID.put(toSearch, "avail");
					} else {
						prdID.put(toSearch, "unavail");
						missingDataRow.add(keys.get(i)); missingDataRow.add(testData.get(keys.get(i))); missingDataRow.add(driver.getCurrentUrl());
						Log.printTableRow(missingDataRow, driver);
						returnData.add(keys.get(i));
					}
					check_monetate(driver);
				}
			}catch(Exception e) {
				Log.message("Exception during :: "+ e.getMessage() + " :: " + keys.get(i) + " -> " + driver.getCurrentUrl(), driver);
				prdID.put(testData.get(keys.get(i)), "unavail");
				missingDataRow.add(keys.get(i)); missingDataRow.add(testData.get(keys.get(i))); missingDataRow.add(driver.getCurrentUrl());
				Log.printTableRow(missingDataRow, driver);
				returnData.add(keys.get(i));
			}
		}
		return returnData;
	} //checkData
	
}// TC_FBB_AnalyzeTestData
