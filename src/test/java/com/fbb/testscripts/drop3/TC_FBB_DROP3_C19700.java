package com.fbb.testscripts.drop3;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.account.WishListPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19700 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19700(String browser) throws Exception {

		//Initialize Test data & other required variables
		String videoPrd = TestData.get("prd_video");
		String prdKey = TestData.get("prd_quickshop_scroll").split("\\|")[0];
		String Lvl1 = TestData.get("prd_quickshop_scroll").split("\\|")[1];

		//Initialize web driver
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo();
		int i = 1; //Step Counter
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);                   

			PlpPage plpPage = homePage.headers.navigateTo(Lvl1);
			Log.message(i++ + ". Navigated to PLP Page!", driver);

			QuickShop quickShop = plpPage.clickQuickShop(prdKey); 
			Log.message(i++ + ". Quick Shop overlay opened!", driver);

			//2
			Log.softAssertThat(quickShop.verifyQuickShopModalHeight(),
					"Quickshop modal should have a minimum fixed height of 650px and it should be scale up to a maximum of 700px",
					"Quickshop modal have a minimum fixed height of 650px and it should be scale up to a maximum of 700px",
					"Quickshop modal don't have a minimum fixed height of 650px and it should be scale up to a maximum of 700px", driver);			

			//Defect : PXSFCC-1170 (The QuickShop container height exceeds maximum limit (700px))

			//Verify the display of scroll bar in Quick Shop modal
			Log.softAssertThat(quickShop.verifyproductVariationContentHeight(),
					"When height of the product attribute exceeds the fixed height, a scroll bar should be displayed",
					"When height of the product attribute exceeds the fixed height, a scroll bar is displayed",
					"When height of the product attribute exceeds the fixed height, a scroll bar not displayed", driver);			

			//Verify the functionality of scroll bar in Quick Shop modal
			Log.softAssertThat(quickShop.verifyQuickViewScroll(), 
					"Interact with the scroll bar itself & use the mouse to scroll to the access attributes",
					"Interact with the scroll bar itself & use the mouse to scroll to the access attributes",
					"Doesn't Interact with the scroll bar & doesn't use the mouse to scroll to the access attributes", driver);

			/*quickShop.zoomProductImage();
			Log.message(i++ + ". Zoom the product image!", driver);

			Log.softAssertThat(quickShop.verifyZoomLensAndZoomWindowAreDisplayed(),
					"The primary product image should be zoomed!",
					"The primary product image is zoomed",
					"The primary product image is not zoomed", driver);

			Log.softAssertThat(quickShop.verifyZoomedImageDisplayedRightOfPrimaryProductImage(),
					"The zoomed image should be displayed to the right of primary product image!",
					"The zoomed image is displayed to the right of primary product image!",
					"The zoomed image is not displayed to the right of primary product image!", driver);

			Log.softAssertThat(quickShop.verifyTopOfZoomedImageAllignsWithTopOfProductImage(),
					"The top of zoomed image should be alligned with the top of primary product image!",
					"The top of zoomed image is alligned with the top of primary product image!",
					"The top of zoomed image is not alligned with the top of primary product image!", driver);*/
			//4
			
			quickShop.clickAlternateImages(1);
			Log.message(i++ + ". Clicked on 1st alternate image!", driver);
			
			Log.softAssertThat(quickShop.verifyIfSelectedThumbnailImageDisplayedAsPrimaryProductImage(),
					"The selected thumbnail image should be displayed as primary product image!",
					"The selected thumbnail image is displayed as primary product image!",
					"The selected thumbnail image is not displayed as primary product image!", driver);

			quickShop.clickAlternateImages(2);
			Log.message(i++ + ". Clicked on 2nd alternate image!", driver);

			Log.softAssertThat(quickShop.verifyIfSelectedThumbnailImageDisplayedAsPrimaryProductImage(),
					"The selected thumbnail image should be displayed as primary product image!",
					"The selected thumbnail image is displayed as primary product image!",
					"The selected thumbnail image is not displayed as primary product image!", driver);

			homePage.headers.redirectToPDP(videoPrd, driver);
			Log.message(i++ + ". Navigated to PDP Page!", driver);

			plpPage = homePage.headers.navigateToLastSubCategory();
			Log.message(i++ + ". Navigated to PLP Page!", driver);

			quickShop = plpPage.clickQuickShop(videoPrd); 
			Log.message(i++ + ". Quick Shop overlay opened!", driver);
			
			Log.softAssertThat(quickShop.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtProductTitleDesktop", "imgProductPrimaryImage", quickShop),
					"The product name should be displayed right side to the primary product image!",
					"The product name is displayed right side to the primary product image!",
					"The product name is not displayed right side to the primary product image!", driver);
			
			if(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("lnkVideo"), quickShop)) {
				Log.softAssertThat(quickShop.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgProductPrimaryImage", "lnkVideo",  quickShop),
						"The video link should be displayed below the primary product image!",
						"The video link is displayed below the primary product image!",
						"The video link is not displayed below the primary product image!", driver);
				
				quickShop.clickMainVideoLink();
				Log.message(i++ + ". Clicked Video link!", driver);

				Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPlayVideo"), quickShop),
						"To check the video player is overtaken the product main image.",
						"The video player is overtaken the product main image",
						"The video player is not overtaken the product main image", driver);

				quickShop.clickAlternateImages(0);
				Log.message(i++ + ". Clicked on First alternate image", driver);

				Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("imgProductPrimaryImage"), quickShop),
						"When user tabs on Alternate Images during video plays: The Liveclicker player should close and display the selected image.",
						"Live clicker closed & selected image displayed",
						"Player not replaced with image.", driver);
			}

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_DROP3_C19700_1
	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP3_C19700(String browser) throws Exception {

		String prdKey = TestData.get("prd_quickshop_scroll").split("\\|")[0];
		String saleColor = TestData.get("SalePriceColorRGB");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo();
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdp = homePage.headers.navigateToPDPByProdName(prdKey);
			Log.message(i++ + ". Navigated to PDP", driver);
			
			PlpPage plpPage = pdp.clickLastBreadCrumbLink();
			Log.message(i++ + ". Navigated to 1 level up PLP for the product.", driver);
			
			String price = plpPage.getPriceOf(prdKey);
			Log.message(i++ + ". Product price :: " + prdKey, driver);
			
			QuickShop quickShop = plpPage.clickQuickShop(prdKey); 
			Log.message(i++ + ". Quick Shop overlay opened!", driver);

			Log.softAssertThat(quickShop.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtProductTitleDesktop", "imgProdMainImg", quickShop), 
					"Product name should be displayed right side of the product image.",
					"Product name displayed right side of the product image.",
					"Product name not displayed right side of the product image.", driver);

			Log.softAssertThat(quickShop.verifyNoSizeSwatchSelectedByDefault(),
					"By default no size swatch should be selected!",
					"By default no size swatch is selected!",
					"By default size swatch is selected!", driver);

			/*Log.softAssertThat(quickShop.verifyColorSwatchSelectedByDefault(),
					"By default color swatch should be selected!",
					"By default color swatch is selected!",
					"By default color swatch is not selected!", driver);*/

			Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayedBelow("txtProductTitleDesktop", "txtProductPrice", quickShop),
					"The product price should be displayed below the product title!",
					"The product price is displayed below the product title!",
					"The product price is not displayed below the product title!", driver);

			Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("txtProductPrice"), quickShop),
					"The product price should be displayed properly!",
					"The product price is displayed properly!",
					"The product price is not displayed properly!", driver);

			Log.softAssertThat(quickShop.elementLayer.verifyElementColor("txtProductSalesPrice", saleColor, quickShop),
					"The sales price should display in red color!",
					"The sales price is displaying in red color!",
					"The sales price is not displaying in red color!", driver);
			
			Log.softAssertThat(quickShop.getSelectedQuantityValue().trim().equals("1"),
					"The default selected value should be 1!",
					"The default selected value is 1!",
					"The default selected value is not 1!", driver);

			Log.softAssertThat(quickShop.getQuantityDropdownValues().equals(Arrays.asList("1","2","3","4","5","6","7","8","9","10")),
					"Values from 1 to 10 must be displayed in the dropdown!",
					"Values from 1 to 10 is displayed in the dropdown!",
					"Values from 1 to 10 is not displayed in the dropdown!", driver);

			Log.softAssertThat(quickShop.elementLayer.verifyElementTextContains("txtProductSalesPrice", price, quickShop),
					"The sales price should display same price as in Product list page!",
					"The sales price is displaying same price as in Product list page!",
					"The sales price is not displaying same price as in Product list page!", driver);

			String selectedSize = quickShop.selectSize();
			Log.message(i++ + ". Size swatch selected :: " + selectedSize, driver);

			String colorSelected = quickShop.selectColor();
			Log.message(i++ + ". Selected color in color swatch :: "+ colorSelected, driver);

			Log.softAssertThat(quickShop.elementLayer.verifyVerticalAllignmentOfElements(driver, "divProductVariationContent", "btnAddToBag", quickShop),
					"The Add To Bag should be displayed below the product variation attribute!",
					"The Add To Bag is displayed below the product variation attribute!",
					"The Add To Bag is not displayed below the product variation attribute!", driver);

			Log.softAssertThat(quickShop.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnAddToBag", "lnkAddToWishList", quickShop),
					"The Add To Bag should be displayed above the add to wishlist link!",
					"The Add To Bag is displayed above the add to wishlist link!",
					"The Add To Bag is not displayed above the add to wishlist link!", driver);

			Log.softAssertThat(quickShop.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductPrice", "txtSavingsMessage", quickShop),
					"The savings message should be displayed below the product price!",
					"The savings message is displayed below the product price!",
					"The savings message is not displayed below the product price!", driver);
			//8

			Log.softAssertThat(quickShop.getSelectedSizeValue().trim().equalsIgnoreCase(selectedSize.trim()),
					"The selected size should get displayed!",
					"The selected size is displayed!",
					"The selected size is not displayed!", driver);
			//7
			Log.softAssertThat(quickShop.getSelectedColorValue().trim().equalsIgnoreCase(colorSelected),
					"The selected color should be displayed properly!",
					"The selected color is displayed properly!",
					"The selected color is not displayed properly!", driver);
			
			quickShop.selectProductAlternateImage(2);
			Log.message(i++ + ". Clicked on alternate image.", driver);

			Log.softAssertThat(quickShop.verifyIfSelectedColorDisplayedAsPrimaryProductImage(),
					"The selected color swatch should be displayed as primary product image!",
					"The selected color swatch is displayed as primary product image!",
					"The selected color swatch is not displayed as primary product image!", driver);

			quickShop.addToBag();
			Log.message(i++ + ". Clicked on Add To Bag Button.", driver);

			Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("flytCart"), quickShop),
					"The Cart Overlay should be displayed!",
					"The Cart Overlay is displayed!",
					"The Cart Overlay is not displayed!", driver);

			quickShop.closeCartOverlay();
			Log.message(i++ + ". Clicked on Close Button in ATB Overlay.", driver);

			Log.softAssertThat(quickShop.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("flytCart"), quickShop),
					"The Cart Overlay should be closed!",
					"The Cart Overlay is closed!",
					"The Cart Overlay is not closed!", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_DROP3_C19700_2
	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP3_C19700(String browser) throws Exception {

		String productID = TestData.get("prd_size-chart");

		final WebDriver driver = WebDriverFactory.get(browser);
		Log.testCaseInfo();
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);                   

			homePage.headers.redirectToPDP(productID, driver);
			Log.message(i++ + ". Navigated to PDP Page!", driver);

			PlpPage plpPage = homePage.headers.navigateToLastSubCategory();
			Log.message(i++ + ". Navigated to PLP Page!", driver);

			QuickShop quickShop = plpPage.clickQuickShop(productID); 
			Log.message(i++ + ". Quick Shop overlay opened!", driver);             

			Log.softAssertThat(quickShop.verifySizeChartLabelDisplayedRightOfSizeChartIcon(),
					"The Size Chart label should be displayed right of Size Chart icon!",
					"The Size Chart label is displayed right of Size Chart icon!",
					"The Size Chart label is not displayed right of Size Chart icon!", driver);

			Log.softAssertThat(quickShop.verifySizeChartLabel(),
					"The Size Chart label should be displayed properly!",
					"The Size Chart label is displayed properly!",
					"The Size Chart label is not displayed properly!", driver);

			quickShop.clickOnSizeChartLink();
			Log.message(i++ + ". Clicked on size chart link!", driver);

			Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("divSizechartContainer"), quickShop),
					"The Size Chart Overlay should be displayed!",
					"The Size Chart is displayed!",
					"The Size Chart is not displayed!", driver);

			quickShop = quickShop.closeSizeChartModalPopup(); //Defect ID - PXSFCC-1388
			Log.message(i++ + ". Clicked on close button on size chart modal!", driver);

			Log.softAssertThat(quickShop.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("divSizechartContainer"), quickShop),
					"The Size Chart Overlay should be closed!",
					"The Size Chart is closed!",
					"The Size Chart is not closed!", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_DROP3_C19700_3
	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP3_C19700(String browser) throws Exception {

		String productID = TestData.get("prd_size-chart");

		final WebDriver driver = WebDriverFactory.get(browser);
		String userName = AccountUtils.generateEmail(driver);

		Log.testCaseInfo();
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);                   

			homePage.headers.navigateToMyAccount(userName, accountData.get("password_global"), true);
			Log.message(i++ + ". Logged in as registered user With Email :: " + userName);

			PdpPage pdpPage = homePage.headers.redirectToPDP(productID, driver);
			Log.message(i++ + ". Navigated to PDP Page!", driver);

			PlpPage plpPage = homePage.headers.navigateToLastSubCategory();
			Log.message(i++ + ". Navigated to PLP Page!", driver);

			QuickShop quickShop = plpPage.clickQuickShop(productID); 
			Log.message(i++ + ". Quick Shop overlay opened!", driver);                   

			//Step-1: Verify the functionality X button in "Quick Shop" overlay
			Log.softAssertThat(!(quickShop.elementLayer.verifyElementDisplayedleft("divQuickShop", "btnClose", quickShop)),
					"X button should be displayed right side corner of the Quick shop overlay",
					"X button displayed right side corner of the Quick shop overlay",
					"X button not displayed right side corner of the Quick shop overlay", driver);

			quickShop.closeQuickShopOverlay();
			Log.message(i++ + ". Clicked on Close button in Quickshop overlay.", driver);

			Log.softAssertThat(quickShop.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("divQuickShop"), quickShop),
					"On click, the modal should close",
					"Modal closed as expected.",
					"Modal not closed", driver);

			quickShop = plpPage.clickQuickShop(productID); 
			Log.message(i++ + ". Quick Shop overlay opened!", driver);

			quickShop.closeQuickShopOverlayByClickingOutSide();
			Log.message(i++ + ". Clicked on Out side of the modal.", driver);

			Log.softAssertThat(quickShop.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("divQuickShop"), quickShop),
					"On click, the modal should close",
					"Modal closed as expected.",
					"Modal not closed", driver);

			quickShop = plpPage.clickQuickShop(productID); 
			Log.message(i++ + ". Quick Shop overlay opened!", driver);

			quickShop.selectColor();
			Log.message(i++ + ". Color swatch selected!", driver);

			quickShop.selectSize();
			Log.message(i++ + ". Size swatch selected!", driver);

			WishListPage wishListPage = (WishListPage) quickShop.addToWishlist();
			Log.message(i++ + ". Clicked on Add To Wishlist!", driver);

			Log.softAssertThat(wishListPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("readyElement"), wishListPage),
					"The Wishlist page should be displayed!",
					"The Wishlist page is displayed!",
					"The Wishlist page is not displayed!", driver);

			pdpPage = homePage.headers.redirectToPDP(productID, driver);
			Log.message(i++ + ". Navigated to PDP Page!", driver);

			plpPage = homePage.headers.navigateToLastSubCategory();
			Log.message(i++ + ". Navigated to PLP Page!", driver);

			quickShop = plpPage.clickQuickShop(productID); 
			Log.message(i++ + ". Quick Shop overlay opened!", driver);

			String selectedColorInQuickShop = quickShop.selectColor();
			Log.message(i++ + ". Color swatch selected!", driver);

			String selectedSizeInQuickShop = quickShop.selectSize();
			Log.message(i++ + ". Size swatch selected!", driver);

			/*String selectedSizeInQuickShop = quickShop.getSelectedSize();
			String selectedColorInQuickShop = quickShop.getSelectedColor();*/

			pdpPage = quickShop.viewFullDetail();
			Log.message(i++ + ". Navigated to Pdp!", driver);
			
			System.out.println("selectedSizeInQuickShop -- -- -- -- "+selectedSizeInQuickShop);
			Log.softAssertThat(pdpPage.getSelectedSize().equals(selectedSizeInQuickShop),
					"The size selected in the overlay should be retained in pdp!",
					"The size selected in the overlay is retained in pdp!",
					"The size selected in the overlay is not retained in pdp!", driver);

			Log.softAssertThat(pdpPage.getSelectedColor().contains(selectedColorInQuickShop),
					"The color selected in the overlay should be retained in pdp!",
					"The color selected in the overlay is retained in pdp!",
					"The color selected in the overlay is not retained in pdp!", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TTC_FBB_DROP3_C19700_4

	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M5_FBB_DROP3_C19700(String browser) throws Exception {

		//Initialize Test data & other required variables
		String altImgPrd = TestData.get("prd_more-alt-image");

		//Initialize web driver
		final WebDriver driver = WebDriverFactory.get(browser);

		Log.testCaseInfo();
		int i = 1; //Step Counter
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);                   

			homePage.headers.navigateToPDP(altImgPrd);
			Log.message(i++ + ". Navigated to PDP Page!", driver);

			PlpPage plpPage = homePage.headers.navigateToLastSubCategory();
			Log.message(i++ + ". Navigated to PLP Page!", driver);

			QuickShop quickShop = plpPage.clickQuickShop(altImgPrd); 
			Log.message(i++ + ". Quick Shop overlay opened!", driver);

			Log.softAssertThat(quickShop.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("slickArrows"), quickShop),
					"Products with more than 4 alternate images should have a carousel to view the images",
					"Products with more than 4 alternate images have a carousel to view the images",
					"Products with more than 4 alternate images don't have carousel to view the images", driver);

			Log.softAssertThat(quickShop.verifyAlternateImagesNotActive(),
					"The alternate images should not be active!",
					"The alternate images is not active!",
					"The alternate images is active!", driver);

			Log.softAssertThat(!quickShop.verifyPreviousLinkEnabled(),
					"The Previous link should not be enabled!",
					"The Previous link is not enabled!",
					"The Previous link is enabled!", driver);

			Log.softAssertThat(quickShop.verifyNextLinkEnabled(),
					"The Next link should be enabled!",
					"The Next link is enabled!",
					"The Next link is not enabled!", driver);

			Log.softAssertThat(quickShop.verifyNextLinkGreyedOut(),
					"The Next link should be greyed out!",
					"The Next link is greyed out!",
					"The Next link is not greyed out!", driver);

			Log.softAssertThat(quickShop.verifyPreviousLinkGreyedOut(),
					"The Previous link should be greyed out!",
					"The Previous link is greyed out!",
					"The Previous link is not greyed out!", driver);

			quickShop.closeQuickShopOverlay();
			Log.message(i++ + ". Clicked on close button in Quick Shop overlay!", driver);

			Log.softAssertThat(quickShop.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("divQuickShop"), quickShop),
					"The Quick Shop overlay should be closed!",
					"The Quick Shop overlay is closed!",
					"The Quick Shop overlay is not closed!", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}

}// search
