package com.fbb.testscripts.drop3;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.EnlargeViewPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19697 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19697(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String badgePrd = TestData.get("prd_badge").split("\\|")[0];
		String prdBadgeType = TestData.get("prd_badge").split("\\|")[1];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser); 
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(badgePrd);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyInsideElementAlligned("prodBadge", "imgPrimaryImage", "left", pdpPage) &&
					pdpPage.elementLayer.verifyInsideElementAlligned("prodBadge", "imgPrimaryImage", "top", pdpPage),
					"Badge should be located in top left corner and should be consistent across all brands",
					"Badge is located in top left corner and should be consistent across all brands",
					"Badge is not located in top left corner and should be consistent across all brands", driver);
			
			//Step1
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("mainProdImage","prodBadge","btnEnlarge"), pdpPage),
					"To check the components(Product Image, Badge, Enlarge & Video buttons) displaying in the product imagery.",
					"The 'Main Product Image', 'Product Batch', 'Enlarge button', 'Video link' are displaying",
					"Some components are missing / not displaying properly", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("alternateImagesList"), pdpPage),
					"To check the alternative images are displaying.",
					"The 'Alternative Image List' is displaying",
					"The 'Alternative Image List' is not displaying", driver);
	
			//Step2 - Product Badge is displaying / not
	
			PlpPage plpPage = homePage.headers.navigateToLastSubCategory();
			Log.message(i++ + ". Navigated to PLP Page.", driver);
	
			Log.softAssertThat(plpPage.verifyProdBadgeImgInParticularProd(badgePrd, prdBadgeType),
					"To check the product badge is displaying in the product list page.",
					"The product has 'product badge' in the product list page.",
					"The product didnt have 'product badge' in the product list page.", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// TC_FBB_DROP2_C19697

	@Test(groups = { "high", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP3_C19697(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String seachTerm = TestData.get("product_search_terms").split("\\|")[1];
		String prdEnlarge = TestData.get("prd_enlarge");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser); 
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			Headers header = homePage.headers;
			SearchResultPage slPage = header.searchProductKeyword(seachTerm);
			Log.message(i++ + ". SLP", driver);
			PdpPage pdpPage = slPage.navigateToPDPAllSwatch();
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			EnlargeViewPage enlargeModal = pdpPage.clickOnEnlargeButton();
			Log.message(i++ + ". Clicked on Enlarge button.", driver);
			
			Log.softAssertThat(enlargeModal.elementLayer.verifyElementDisplayed(Arrays.asList("mdlEnlargeWindow","txtProductName","colorSwatches","alternateImages"), enlargeModal),					 
					"'Product Name','Color Swatch', 'Alternate Images' should be displayed on enlarge view page", 
					"'Product Name','Color Swatch', 'Alternate Images' is displayed", 
					"'Product Name','Color Swatch', 'Alternate Images' is not displayed", driver);
			
			//Setp-6: Click on Enlarge button
			if(Utils.isDesktop()){
				Log.softAssertThat(enlargeModal.verifyInitalImageMapping(), 
						"Alternate images associated with the color attribute group should be displayed with the first sequenced alternate image by default", 
						"Alternate images associated with the color attribute group should be displayed with the first sequenced alternate image by default", 
						"Alternate images associated with the color attribute group should be displayed with the first sequenced alternate image by default", driver);
				
				enlargeModal.selectColor(0);
				enlargeModal.clickOnAlternateImage(1);
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlEnlargeWindow"), pdpPage), 
						"Clicking on Enlarge button, a pop-up window will display.", 
						"Clicking on Enlarge button, a pop-up window will display.", 
						"Clicking on Enlarge button, a pop-up window will display.", driver);
	
				enlargeModal.mouseHoverOnPrimaryImage();
				Log.message(i++ + ". Mouse hovered on Primary Image.", driver);
				
				//Log.softAssertThat(enlargeModal.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("zoomedImage"), enlargeModal),
				Log.softAssertThat(enlargeModal.verifyZoomedImage(),
						"Product image should display as zoomed", 
						"Product image should display as zoomed", 
						"Product image should display as zoomed", driver);
				
				//Functionality cannot be verified because alternative image selected is not displaying in DOM  
				//Log.failsoft("Arrow functionality verification commented due to :: SM-5738");
				/*int getSlickSlideIndexBefore = enlargeModal.getAltImgSlickIndex();
				
				enlargeModal.clickOnNextImageArrow();
				Log.message(i++ + ". Clicked on Right Arrow.", driver);
				
				int getSlickSlideIndexAfer = enlargeModal.getAltImgSlickIndex();
				
				Log.softAssertThat(getSlickSlideIndexAfer == (getSlickSlideIndexBefore + 1), 
						"Click on right arrow, should sequece the next image", 
						"Click on right arrow, should sequece the next image", 
						"Click on right arrow, should sequece the next image", driver);
	
				enlargeModal.clickOnPrevImageArrow();
				Log.message(i++ + ". Clicked on left Arrow.", driver);
				
				getSlickSlideIndexAfer = enlargeModal.getAltImgSlickIndex();
				
				Log.softAssertThat(getSlickSlideIndexAfer == getSlickSlideIndexBefore, 
						"Click on left arrow, should sequece the next image", 
						"Click on left arrow, should sequece the next image", 
						"Click on left arrow, should sequece the next image", driver);
	
				String mainImgCode = enlargeModal.getPrimaryImgColorCode();
				String thumImgCode = enlargeModal.getSelectedColorCode();
				Log.event("Primary Image Color Code :: " + mainImgCode);
				Log.event("Selected Image Color Code :: " + thumImgCode);
				Log.softAssertThat(mainImgCode.contains(thumImgCode), 
						"Selected alternate image should populate in the main image space", 
						"Selected alternate image should populate in the main image space", 
						"Selected alternate image should populate in the main image space", driver);*/
	
				enlargeModal.closeEnlargeViewModal();
				Log.message(i++ + ". Closed View Modal", driver);
				
				pdpPage = header.navigateToPDP(prdEnlarge);
				Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
				
				enlargeModal = pdpPage.clickOnEnlargeButton();
				Log.message(i++ + ". Clicked on Enlarge button.", driver);
				
				enlargeModal.mouseHoverOnAltImage(2);
				Log.message(i++ + ". Mouse hovered on 2nd alternate image.", driver);
				
				Log.softAssertThat(enlargeModal.elementLayer.verifyMaxElementsInRow("lstcolorSwatchesLi", 8, enlargeModal), 
						"Maximum 8 swatches should display per row in the desktop", 
						"Maximum 8 swatches should display per row in the desktop", 
						"Maximum 8 swatches should display per row in the desktop", driver);
	
				Log.softAssertThat(enlargeModal.elementLayer.verifyMaxElementsInRow("lstAlternateImages", 5, enlargeModal), 
						"Maximum 5 thumbnails should be displayed per row", 
						"Maximum 5 thumbnails should be displayed per row", 
						"Maximum 5 thumbnails should be displayed per row", driver);
	
			}else if(Utils.isTablet()){
				Log.softAssertThat(enlargeModal.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgMainProduct_Tablet", "attributeSection", enlargeModal), 
						"Once user tab on Enlarge button the enlarged view displays at 100% of the viewport with the product name, color swatches and alternate images displayed beneath", 
						"Once user tab on Enlarge button the enlarged view displays at 100% of the viewport with the product name, color swatches and alternate images displayed beneath", 
						"Once user tab on Enlarge button the enlarged view displays at 100% of the viewport with the product name, color swatches and alternate images displayed beneath", driver);
	
				Log.softAssertThat(enlargeModal.elementLayer.verifyMaxElementsInRow("lstAlternateImages", 4, enlargeModal), 
						"4 alternate images are displayed in per row", 
						"4 alternate images are displayed in per row", 
						"4 alternate images are displayed in per row", driver);
	
			}
	
			enlargeModal.closeEnlargeViewModal();
			pdpPage = new PdpPage(driver).get();
			Log.message(i++ + ". Clicked on Close button in enlarge modal.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("mdlEnlargeWindow"), pdpPage), 
					"Clicking on X button, the takeover should be closed and the user should be returned to the PDP Page", 
					"Clicking on X button, the takeover should be closed and the user should be returned to the PDP Page", 
					"Clicking on X button, the takeover should be closed and the user should be returned to the PDP Page", driver);
	
			//Step1
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// TC_FBB_DROP2_C19697

	@Test(groups = { "high", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP3_C19697(String browser) throws Exception {
		Log.testCaseInfo();
	
		String altImagesPrd = TestData.get("prd_more-alt-image");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser); 
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(altImagesPrd);
			//Step 3 cannot be done as it is demand ware configuration
	
			Log.reference("Step 6b is covered in the 19709 test script");
	
			//Step 8
			int NoOfThumbProdImg = pdpPage.getNoOfThumbnailProdImages();
			pdpPage.clickOnSpecifiedAlternateImage(1);
	
			//Step 9
			if (Utils.isDesktop()){
	
				Log.softAssertThat(pdpPage.verifyNewProdImageLoaded(2),
						"To check the selected image is populated in place of main product image.",
						"The main product image is replaced with selected image!",
						"The main product image is not replaced with selected image", driver);
			}
			//Step 10 is DM configuration, hence cannot be done
	
			//Step 11
			if (NoOfThumbProdImg > 4) {
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnPrevImageDisable","btnNextImageEnable"), pdpPage),
						"To check the arrows displaying for product image thumbnails.",
						"The 'Prev arrow', 'Next arrow' are displaying when the product have 5 or more thumbnail images",
						"The arrow is not displaying even the thumbnails are having 5 or more product images", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnPrevImageDisable"), pdpPage),
						"To check the previous arrow is disabled in the begining of image sequence.",
						"The 'Prev arrow' is displaying and disabled in the begining of image sequence.",
						"The 'Prev arrow' is not disabled", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnNextImageDisable"), pdpPage),
						"To check the next arrow is enabled in the begining of image sequence.",
						"The 'Next arrow' is displaying and enabled in the begining of image sequence.",
						"The 'Next arrow' is not enabled", driver);
	
				if (Utils.isDesktop()) {
					pdpPage.clickOnSpecifiedAlternateImage(5);
	
					Log.softAssertThat(pdpPage.verifyAlternateImageDisplayStatus(5, "false"),
							"To check the half hidden alternate image is displayed fully when the image is clicked.",
							"The alternate image is displaying fully.",
							"The alternate image is not displaying fully", driver);
				}
	
				pdpPage.scrollAlternateImageInSpecifiedDirection("Next");
	
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnNextImageDisable"), pdpPage),
						"To check the next arrow is disabled when the alternate image sequence reached the end.",
						"The 'Next arrow' is displaying and disabled in the end of image sequence.",
						"The 'Next arrow' is not disabled", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnPrevImageDisable"), pdpPage),
						"To check the Prev arrow is enabled at the end of image sequence.",
						"The 'Prev arrow' is displaying and enabled at the end of image sequence.",
						"The 'Prev arrow' is not enabled", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnPrevImageEnable","btnNextImageEnable"), pdpPage),
						"To check the arrows displaying for product image thumbnails.",
						"The product only have 4 or less thumbnail images, thus the arrows are not displayed",
						"The arrow is displaying even the product only have 4 or less thumbnail images", driver);
			}
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// TC_FBB_DROP2_C19697
	
	@Test(enabled = false, groups = { "high", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP3_C19697(String browser) throws Exception {
		Log.testCaseInfo();
	
		String videoPrd = TestData.get("prd_video");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser); 
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(videoPrd);
			Log.message(i++ + ". Navigated to PDP for product :: " + pdpPage.getProductName());
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkVideoMain"), pdpPage),
					"To check the Video buttons displaying in the product imagery.",
					"The 'Video link' is displaying",
					"The 'Video link' is not displaying", driver);
			
			pdpPage.clickMainVideoLink();
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("prdVideoContainer"), pdpPage),					 
					"'Video Player' should be displayed", 
					"'Video Player' is displayed and it is taken over product main image", 
					"'Video Player' is not displayed in the place of product image", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("videoPlayer"), pdpPage),
					"To check the product image is overtaken the product main image.",
					"The product image is overtaken the product main image",
					"The product image is not overtaken the product main image", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// TC_FBB_DROP2_C19697
	
}// search
