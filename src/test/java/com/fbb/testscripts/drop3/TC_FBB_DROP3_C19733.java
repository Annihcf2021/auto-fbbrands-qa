package com.fbb.testscripts.drop3;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19733 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "critical", "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19733(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String productCatalog = TestData.get("qc_valid");
		String catalogExpired = TestData.get("qc_expired");
		String arrayInvalidCQO[] = {"111-55555-111","111-55555-111","111-55555-111","111-55555-111"};
		String arrayValidInvalidCQO[] = {"111-55555-111", productCatalog};
		String arrayValidCQO[] = {productCatalog};
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
	
		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!",driver);
	
			homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			QuickOrderPage quickOrderPage = homePage.footers.navigateToQuickOrder();
			Log.message(i++ + ". Navigated to Quick order Page!", driver);
	
			quickOrderPage.removeAllProducts();
			//1 Verify the functionality of breadcrumb
			if(!Utils.isMobile()){
				quickOrderPage.clickOnBreadCrumbHome();
				homePage = new HomePage(driver).get();
				Log.message(i++ + ". Clicked on Breadcrumb Home.", driver);
	
				Log.softAssertThat(homePage.getPageLoadStatus(),
						"Breadcrumb Home icon should be actionable!",
						"Breadcrumb Home icon is actionable!",
						"Breadcrumb Home icon is not be actionable!!", driver);
	
				quickOrderPage = homePage.footers.navigateToQuickOrder();
				Log.message(i++ + ". Navigated to Quick order Page!", driver);
	
				Log.softAssertThat(quickOrderPage.elementLayer.verifyElementDisplayedOverlay("divGridHeader", "divBreadCrumb", quickOrderPage),
						"The Breadcrumb should be overlays the Heading Asset!",
						"The Breadcrumb overlays the Heading Asset!",
						"The Breadcrumb not be overlays the Heading Asset!", driver);
			}
	
			// 3 need to be automated once the content slot is configured.
			
			//4
			Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "quickOrderHeading", "frmOrderForm", quickOrderPage), 
					"Check Item search text box is displaying below the quick order heading", 
					"Item search text box is displaying below the quick order heading", 
					"Item search text box is not displaying below the quick order heading", driver);
			
			//5
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtItemSearch3", "txtBlueBoxNumberField", quickOrderPage), 
						"Check Search button is displaying below the Blue box number field", 
						"Search button is displaying below the Blue box number field", 
						"Search button is not displaying below the Blue box number field", driver);
			} else {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtItemSearch4", "txtBlueBoxNumberField", quickOrderPage), 
						"Check Search button is displaying below the Blue box number field", 
						"Search button is displaying below the Blue box number field", 
						"Search button is not displaying below the Blue box number field", driver);
			}
			
			//6
			if (Utils.isMobile()) {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtBlueBoxNumberField", "btnSearch", quickOrderPage), 
						"Check Item search button is displaying above to the Blue box number field", 
						"Item search button is displaying above to the Blue box number field", 
						"Item search button is not displaying above to the Blue box number field", driver);
			} else {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnSearch", "txtBlueBoxNumberField", quickOrderPage), 
						"Check Item search button is displaying left to the Blue box number field", 
						"Item search button is displaying left to the Blue box number field", 
						"Item search button is not displaying left to the Blue box number field", driver);
			}
			
			//7 Verify the functionality of search button
			
			quickOrderPage.clickOnSearchButton();
			
			Log.softAssertThat(quickOrderPage.elementLayer.verifyPageElements(Arrays.asList("genericErrorRequired"), quickOrderPage), 
					"Do not enter any CQO number and click on search - The system should display an error message", 
					"Do not enter any CQO number and click on search - The system is display an error message", 
					"Do not enter any CQO number and click on search - The system is not display an error message", driver);
			
			quickOrderPage.searchMultipleProductInQuickOrder(arrayInvalidCQO);
			
			Log.softAssertThat(quickOrderPage.elementLayer.verifyPageElements(Arrays.asList("genericError","item1ErrorMsg","item2ErrorMsg","item3ErrorMsg","item4ErrorMsg"), quickOrderPage), 
					"Enter all the invalid CQO number and click on search- The system should display the error message on all the item numbers fields", 
					"Enter all the invalid CQO number and click on search- The system is display the error message on all the item numbers fields", 
					"Enter all the invalid CQO number and click on search- The system is not display the error message on all the item numbers fields", driver);
			
			quickOrderPage.searchMultipleProductInQuickOrder(arrayValidInvalidCQO);
			
			Log.softAssertThat(quickOrderPage.elementLayer.verifyPageElements(Arrays.asList("genericError","invalidErrorMsg"), quickOrderPage), 
					"1st filed enter invalid number & 2nd filed enter valid number - The system should display an error message on the 1st field and for 2nd field system retrieve the item ", 
					"1st filed enter invalid number & 2nd filed enter valid number - The system is display an error message on the 1st field and for 2nd field system retrieve the item ", 
					"1st filed enter invalid number & 2nd filed enter valid number - The system is not display an error message on the 1st field and for 2nd field system retrieve the item ", driver);
			
			quickOrderPage.removeAllProducts();
			Log.message(i++ + ". Removed all items.", driver);
			
			quickOrderPage.searchMultipleProductInQuickOrder(arrayValidCQO);
			Log.message(i++ + ". Searched for multiple cqo products.", driver);
			
			Log.softAssertThat(quickOrderPage.getOrderCatlogNumber().equalsIgnoreCase(productCatalog), 
					"System should check the entered item number is order catalog number.",
					"System is check the entered item number is order catalog number.", 
					"System is not check the entered item number is order catalog number.", driver);
			
			quickOrderPage.searchItemInQuickOrder(catalogExpired);
			Log.message(i++ + ". Searched for expired catalog product", driver);
			
			String colorSelected = quickOrderPage.selectColor(1);
			Log.message(i++ +". Selected first product random color:: " + colorSelected, driver);
			
			String sizeSelected = quickOrderPage.selectSize(1);
			Log.message(i++ +". Selected first product random size:: " + sizeSelected, driver);
			
			Log.softAssertThat(quickOrderPage.elementLayer.verifyPageElements(Arrays.asList("txtPrice","catalogPriceExpired"), quickOrderPage), 
					"When the item have active pricing , system should display the product with quick order pricing.", 
					"When the item have active pricing , system is display the product with quick order pricing.", 
					"When the item have active pricing , system is not display the product with quick order pricing.", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M1_FBB_DROP2_C19733


}// search
