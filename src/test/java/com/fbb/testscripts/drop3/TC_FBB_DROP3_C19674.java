package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.GiftCardPage;
import com.fbb.pages.HomePage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19674 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19674(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String cardnumber = TestData.giftcard("valid_gift_card").split("\\|")[0];
		String invalidcardnumber = TestData.giftcard("invalid_gift_card").split("\\|")[0];
		
		String pin = TestData.giftcard("valid_gift_card").split("\\|")[1];
		String invalidpin = TestData.giftcard("invalid_gift_card").split("\\|")[1];
		//String expiredcardnumber = cardnumbers[2].split("\\|")[0];
		//String expiredpin = cardnumbers[2].split("\\|")[1];
		// Get the web driver instance
		
		//Get WebDriver Instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			GiftCardPage giftCardPage = homePage.footers.navigateToGiftCardPage();
			Log.message(i++ + ". Navigated to Gift Card Page!", driver);
			
			giftCardPage.clickOnCheckYourBalance();
			Log.message(i++ + ". Gift Card Balance Lookup overlay opened!", driver);
	
			//Step-1
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("divChkBalanceModelPopup"), giftCardPage),
					"SVS check balance model pop up should be opened.",
					"SVS check balance model pop up opened.",
					"SVS check balance model pop up did not open.", driver);
	
			//Step-2
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("txtPleaseEnterCardnoandPin"), giftCardPage),
					"\"Please enter Card Number and Pin\" should be present",
					"\"Please enter Card Number and Pin\" is present",
					"\"Please enter Card Number and Pin\" is not present", driver);
	
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("txtfieldCardnumber"), giftCardPage),
					"\"Card number field\" should be present",
					"\"Card number field\" is present",
					"\"Card number field\" is not present", driver);
	
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("txtFieldCardPin"), giftCardPage),
					"\"Pin number field\" should be present",
					"\"Pin number field\" is present",
					"\"Pin number field\" is not present", driver);
	
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("applyButton"), giftCardPage),
					"\"Apply Button\" should be present",
					"\"Apply Button\" is present",
					"\"Apply Button\" is not present", driver);
	
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("btnclose"), giftCardPage),
					"\"Close Button\" should be present",
					"\"Close Button\" is present",
					"\"Close Button\" is not present", driver);
	
			//Step-3		    
			Log.softAssertThat(giftCardPage.elementLayer.verifyElementDisplayedBelow("txtPleaseEnterCardnoandPin", "txtfieldCardnumber", giftCardPage),
					"\"Please enter Card number and Pin\" should be shown above \"Card Number\" field.",
					"\"Please enter Card number and Pin\" is shown above \"Card Number\" field.",
					"\"Please enter Card number and Pin\" is not shown above \"Card Number\" field.", driver);
	
			//step-4
			Log.softAssertThat(giftCardPage.elementLayer.verifyTextContains("placeholderCardNumber", "Card Number", giftCardPage),
					"Card number field placeholder should display as \"Card Number\".",
					"Card number field placeholder is displayed as \"Card Number\".",
					"Card number field placeholder is not displayed as \"Card Number\".", driver);
	
			Log.softAssertThat(giftCardPage.elementLayer.verifyTextContains("placeholderPin", "Pin", giftCardPage),
					"Pin number field placeholder should display as \"Pin\".",
					"Pin number field placeholder is displayed as \"Pin\".",
					"Pin number field placeholder is not displayed as \"Pin\".", driver);
	
			Log.softAssertThat(giftCardPage.elementLayer.verifyPlaceHolderMovesAbove("txtfieldCardnumber", "placeholderCardNumber", cardnumber, giftCardPage),
					"While entering the data in the Card number field, placeholder \"Card Number\" should move to the top portion of the form field.",
					"Placeholder \"Card Number\" moved to the top portion of the form field.",
					"Placeholder \"Card Number\" did not move to the top portion of the form field.", driver);
	
			Log.softAssertThat(giftCardPage.elementLayer.verifyPlaceHolderMovesAbove("txtFieldCardPin", "placeholderPin", pin, giftCardPage),
					"While entering the data in the Pin number field, placeholder \"Pin\" should move to the top portion of the form field.",
					"Placeholder \"Pin\" moved to the top portion of the form field.",
					"Placeholder \"Pin\" did not move to the top portion of the form field.", driver);
	
			//Step-5: PXSFCC-2522
			giftCardPage.checkGiftCardBalance("","");		    
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("errfieldCardnumber", "errFieldCardPin"), giftCardPage),
					"Error message should be displayed for Cardnumber & Pin Text fields.",
					"Error message displayed.",
					"Error Message not displayed.", driver);
	
			//Step-6: 
			//only adding valid card number in card details field.
			giftCardPage.checkGiftCardBalance(cardnumber,"");	
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("errFieldCardPin"), giftCardPage),
					"Error message should be displayed for Pin Text field.",
					"Error message displayed.",
					"Error Message not displayed.", driver);
	
			//Step-7:
			//valid pin number without adding card number 
			giftCardPage.checkGiftCardBalance("",pin);	
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("errfieldCardnumber"), giftCardPage),
					"Error message should be displayed for Cardnumber Text field.",
					"Error message displayed.",
					"Error Message not displayed.", driver);
			//Step-8:
			//adding invalid card and pin number
			giftCardPage.checkGiftCardBalance(invalidcardnumber,invalidpin);
	
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("invalidcardMsg"), giftCardPage),
					"Validation failure message \"Please enter valid Gift Card Number\" should be displayed.",
					"Validation failure message \"Please enter valid Gift Card Number\" is displayed.",
					"Validation failure message \"Please enter valid Gift Card Number\" is not displayed.", driver);
	
			//Step-9:
			//adding expired card number and pin number 
			/*giftCardPage.checkGiftCardBalance(expiredcardnumber,expiredpin);
			List<String> elementsToBeVerified = null;
			elementsToBeVerified = Arrays.asList("totalBalanceMsg");
			Log.softAssertThat(!giftCardPage.elementLayer.VerifyElementDisplayed(elementsToBeVerified, giftCardPage),
					"Validation failure message \"Card is expired\" should be displayed",
					"Validation failure message is displayed",
					"Validation failure message is not displayed", driver);
	
			//Step-10:
			giftCardPage.checkGiftCardBalance(cardnumber,pin);
			Log.softAssertThat(giftCardPage.elementLayer.VerifyElementDisplayed(elementsToBeVerified, giftCardPage),
					"The Total balance should be displayed below the Pin number.",
					"The Total balance is displayed below the Pin number.",
					"The Total balance is not displayed below the Pin number.", driver);
	
			//Step-11:
			giftCardPage.clickClose();
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("divChkBalanceModelPopup"), giftCardPage),
					"Check balance model Popup should be closed",
					"Popup closed",
					"Popup did not close.", driver);*/
			Log.reference("Expired Gift Card verification hold for test data.");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}


}// search
