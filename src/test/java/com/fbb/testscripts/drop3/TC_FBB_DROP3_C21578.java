package com.fbb.testscripts.drop3;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C21578 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C21578(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String btnErrMessage = "PLEASE SELECT COLOR & SIZE";
		String[] prdVariation = TestData.get("sku_stock<5_variation").split("\\|");
		String prdID = prdVariation[0];
		String prdColor = prdVariation[1];
		String prdSize = prdVariation[2];
		String prdFamily = null;
		if(prdVariation.length > 3) prdFamily = prdVariation[3];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdID);
			Log.message(i++ + ". Navigated to PdpPage for Product :: " + pdpPage.getProductName(), driver);
	
			//Step-1: Verify when the user does not select the required fields & tab on the Add to Bag
			pdpPage.mouseHoverAddToBag();
			if(Utils.isDesktop())
				Log.message(i++ + ". Mouse Hovered on Add to Bag Button!", driver);
			else
				Log.message(i++ + ". Tabbed on Add to Bag Button!", driver);
	
			Log.softAssertThat(pdpPage.verifyToCartButtonDisabled(), 
					"Add To Bag button should be disabled.",
					"Add to Bag button disabled!",
					"Add to bag Button not disabled.", driver);
	
			Log.softAssertThat(pdpPage.getAdd2BagBtnText().trim().toLowerCase().equals(btnErrMessage.toLowerCase()), 
					"Error Message should be displayed on the Button",
					"Error Message displayed on the Button!",
					"Error Message not displayed on the Button", driver);
	
			homePage.headers.redirectToPDP(prdID, driver);
			Log.message(i++ + ". Navigated to PDP for product :: " + pdpPage.getProductName(), driver);
			
			try {
				pdpPage.selectSizeFamily(prdFamily);
			} catch (ArrayIndexOutOfBoundsException e) {
				Log.event("No Size family for this product!");
			}
			pdpPage.selectColor(prdColor);
			pdpPage.selectSize(prdSize);
			Log.message(i++ + ". Selected variations.", driver);
	
			//Step3
			Log.softAssertThat(pdpPage.getQuantityListSize() == pdpPage.getStockValuefrmInventoryMsg(), 
					"Quantity drop down should displayed based the inventory items available.", 
					"Quantity drop down is displayed based the inventory items available.", 
					"Quantity drop down is not displayed based the inventory items available.", driver);
	
			pdpPage.selectMaxQty();
			Log.message(i++ + ". Maximum Quantity Selected.", driver);
	
			pdpPage.addToBagKeepOverlay();
			pdpPage.addToBagKeepOverlay();
			Log.message(i++ + ". Product added to bag.", driver);
	
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Cart Page.", driver);
	
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("qtyError"), cartPage),
					"The system will display an error message",
					"The system will display an error message",
					"The system will display an error message", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP2_C21578

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP3_C21578(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String variation2 = TestData.get("prd_variation");
	
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Step-2:Verify the functionality when the user reaches the maximum 40 items in the bag.
			PdpPage	pdpPage = homePage.headers.navigateToPDP(variation2);
			Log.message(i++ + ". Navigated to PdpPage for Product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.selectColor();
			pdpPage.selectSize();
			
			int mcQty = 0;
			int mcQtyBefore = mcQty;
			int selectedQty = 1;
			do{
				selectedQty = Integer.parseInt(pdpPage.selectMaxQty());
				
				if((mcQty + selectedQty) > 40) {
					pdpPage.selectQty(Integer.toString(40 - mcQty));
				}
				
				pdpPage.clickAddProductToBag();
				pdpPage.closeAddToBagOverlay();
				mcQty = Integer.parseInt(homePage.headers.getMiniCartCount());
				if(mcQtyBefore == mcQty) {
					Log.fail("Cannot add more products.", driver);
				}else
					mcQtyBefore = mcQty;
			}while(mcQty < 40);
			
			Log.message(i++ + ". Added 40 Items to Shopping Cart.", driver);
			
	
			pdpPage.selectQty("3");
			pdpPage.clickAddProductToBag();
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblMaxCartError"), pdpPage) && 
					pdpPage.elementLayer.verifyCssPropertyForElement("lblMaxCartError", "display", "block", pdpPage), 
					"If the user attempts to add more quantity than is ATS, Error message should be displayed",
					"Error message is displayed",
					"Error message not displayed", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP2_C21578
	
}// search
