package com.fbb.testscripts.drop3;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.reusablecomponents.Enumerations.Toggle;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19735 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	
	//private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "mobile" , "tablet"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19735(String browser) throws Exception {
		Log.testCaseInfo();
	
		String productID[] = {TestData.get("qc_valid_4"), TestData.get("qc_valid_1")};
		String InvalidProduct[] = {TestData.get("prd_variation"),"111-12222-111","111-12222-222","111-12222-333"};
		String InvalidAndValid[] = {"111-12222-111",TestData.get("qc_valid_1")};
		
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!",driver);
			
			String quickOrderUrl = Utils.getWebSite() + redirectData.get("quick-order");
			
			driver.get(quickOrderUrl);
			QuickOrderPage quickOrderPage= new QuickOrderPage(driver).get();
			Log.message(i++ + ". Navigated to Quick order Page!", driver);
			
			//Step-2: Verify the functionality of Breadcrumb
			if(Utils.isMobile()) {
			Log.softAssertThat(quickOrderPage.elementLayer.verifyTextContains("bcCurrentMobile", "Home", quickOrderPage), 
					"Breadcrumb should be displayed as 'HOME'", 
					"Breadcrumb should be displayed as 'HOME'", 
					"Breadcrumb should be displayed as 'HOME'", driver);
			
			homePage = quickOrderPage.clickOnBreadCrumbHome();
			Log.message(i++ + ". Clicked on Home link in breadcrumb.", driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Tap on HOME, User should navigate to the brand homepage.", 
					"Tap on HOME, User should navigate to the brand homepage.", 
					"Tap on HOME, User should navigate to the brand homepage.", driver);
			
			driver.get(quickOrderUrl);
			quickOrderPage= new QuickOrderPage(driver).get();
			Log.message(i++ + ". Navigated back to Quick order catalog page.", driver);
			
			}else {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "headerTopBanner", "qcBreadCrumb", quickOrderPage) &&
						quickOrderPage.elementLayer.verifyInsideElementAlligned("qcBreadCrumb", "divMain", "left", quickOrderPage), 
						"Breadcrumb should be displayed below the header and left side of the screen", 
						"Breadcrumb is displaying below the header and left side of the screen", 
						"Breadcrumb should be displayed below the header and left side of the screen", driver);
			}
	
			//Step-1: Verify the functionality of Quick order landing with items page
			quickOrderPage.searchMultipleProductInQuickOrder(productID);
			Log.message(i++ + ". Product added to catalog.", driver);
			
			/*boolean status = false;
			if (Utils.isMobile()) {
				status = quickOrderPage.elementLayer.verifyListElementSize("txtCQONumberOfItems_Mobile", 2, quickOrderPage)
						&& quickOrderPage.verifyWithoutOrderAllTheSearchedItemsAreGettingDisplayed(productID);
			} else {
				status = quickOrderPage.elementLayer.verifyListElementSize("txtCQONumberOfItems_Des_Tab", 2, quickOrderPage)
						&& quickOrderPage.verifyWithoutOrderAllTheSearchedItemsAreGettingDisplayed(productID);
			}*/
			
			Log.softAssertThat(quickOrderPage.verifySearchedCatalogItemsDisplayed(productID),
					"When entering in 2 valid CQO item #s in 2 of the fields, leaving 2 blank, the search results should return those 2 items.", 
					"When entering in 2 valid CQO item #s in 2 of the fields, leaving 2 blank, the search results is returning those 2 items.", 
					"When entering in 2 valid CQO item #s in 2 of the fields, leaving 2 blank, the search results is not returning those 2 items.", driver);
			
			boolean statusContentImg = false;
			String ContentAssetElem = null;
			
			if(BrandUtils.isBrand(Brand.ks)) {
				statusContentImg = quickOrderPage.elementLayer.verifyElementDisplayed(Arrays.asList("imgHeadContentAsset_KS"), quickOrderPage);
				ContentAssetElem = "imgHeadContentAsset_KS";
			} else {
				if(Utils.isDesktop()) {
					statusContentImg = quickOrderPage.elementLayer.verifyElementDisplayed(Arrays.asList("divHeadContentAssetImg_Desktop"), quickOrderPage);
					ContentAssetElem = "divHeadContentAssetImg_Desktop";
				} else if(Utils.isTablet()) {
					statusContentImg = quickOrderPage.elementLayer.verifyElementDisplayed(Arrays.asList("divHeadContentAssetImg_Tablet"), quickOrderPage);
					ContentAssetElem = "divHeadContentAssetImg_Tablet";
				} else {
					statusContentImg = quickOrderPage.elementLayer.verifyElementDisplayed(Arrays.asList("divHeadContentAssetImg_Mobile"), quickOrderPage);
					ContentAssetElem = "divHeadContentAssetImg_Mobile";
				}
			}
			
			//Step-3: Verify the functionality of Head Asset
			if (!statusContentImg) {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "qcBreadCrumb", "divHeadContentAsset", quickOrderPage), 
						"Heading Asset should be displayed below the Breadcrumb", 
						"Heading Asset is displayed below the Breadcrumb", 
						"Heading Asset is not displayed below the Breadcrumb", driver);
			} else {
				Log.reference("Content asset is configured with images hence 'bread crumb displayed above the content asset' validation is skipped!.");
			}
			
			Log.reference("Brand specific Content Asset verification is skipped due to BM verification");
			//Step-4: Verify the functionality of Content Slot
			
			if(Utils.isDesktop()) {
				if (statusContentImg) {
					Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, ContentAssetElem, "divCatalogQuickOrderleft", quickOrderPage) &&
							quickOrderPage.elementLayer.verifyInsideElementAlligned("divCatalogQuickOrderleft", "divMain", "left", quickOrderPage), 
							"Content Slot | Slot ID catalog-quick-order should be displayed below the Heading Asset and left side of the screen", 
							"Content Slot | Slot ID catalog-quick-order should be displayed below the Heading Asset and left side of the screen", 
							"Content Slot | Slot ID catalog-quick-order should be displayed below the Heading Asset and left side of the screen", driver);
				} else {
					Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divHeadContentAsset", "divCatalogQuickOrderleft", quickOrderPage) &&
							quickOrderPage.elementLayer.verifyInsideElementAlligned("divCatalogQuickOrderleft", "divMain", "left", quickOrderPage), 
							"Content Slot | Slot ID catalog-quick-order should be displayed below the Heading Asset and left side of the screen", 
							"Content Slot | Slot ID catalog-quick-order should be displayed below the Heading Asset and left side of the screen", 
							"Content Slot | Slot ID catalog-quick-order should be displayed below the Heading Asset and left side of the screen", driver);
				}
			} else {
				Log.softAssertThat(quickOrderPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("divCatalogQuickOrderleft"), quickOrderPage), 
						"Content slot should not be available.", 
						"Content slot is not available.", 
						"Content slot is available.", driver);
			}
			
			//Step-5: Verify the functionality of Order Form
			
			Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "quickOrderHeading", "frmOrderForm", quickOrderPage), 
					"All the Search boxes should be displayed below the Quick order heading ", 
					"All the Search boxes are displayed below the Quick order heading", 
					"All the Search boxes are not displayed below the Quick order heading", driver);
			
			quickOrderPage.removeAllProducts();
			Log.message(i++ + ". Removed All items from Quick order");
			
			quickOrderPage.searchMultipleProductInQuickOrder(InvalidProduct);
			Log.message(i++ + ". Searching the product.", driver);
			
			Log.softAssertThat(quickOrderPage.elementLayer.verifyElementDisplayed(Arrays.asList("item1ErrorMsg", "item2ErrorMsg", 
					"item3ErrorMsg", "item4ErrorMsg", "genericError"), quickOrderPage), 
					"System will display an error message if the user enter invalid data in any field and Only catalog item number is valid number.", 
					"System will display an error message if the user enter invalid data in any field and Only catalog item number is valid number.", 
					"System will display an error message if the user enter invalid data in any field and Only catalog item number is valid number.", driver);
			
			quickOrderPage.searchMultipleProductInQuickOrder(productID);
			Log.message(i++ + ". Product added to catalog.", driver);
			
			Log.softAssertThat(quickOrderPage.getNoOfProductPrice() >= productID.length && 
					quickOrderPage.verifySearchedCatalogItemsDisplayed(productID), 
					"All the products should displayed with prices.", 
					"All the products are displayed with prices.", 
					"All the products are not displayed with prices.", driver);
			
			
			quickOrderPage.removeAllProducts();
			Log.message(i++ + ". Removed All items from Quick order");
			
			quickOrderPage.searchMultipleProductInQuickOrder(InvalidAndValid);
			Log.message(i++ + ". Seached with valid and invalid CQO number.", driver);
			
			Log.softAssertThat(quickOrderPage.elementLayer.verifyElementDisplayed(Arrays.asList("item1InvalidErrorMsg", "genericError"), quickOrderPage)
							&& quickOrderPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("item2ErrorMsg", "item3ErrorMsg", "item4ErrorMsg"), quickOrderPage), 
					"An error should display in the Text Entry Field that had the error.", 
					"An error is displaying in the Text Entry Field that had the error.", 
					"An error is not display in the Text Entry Field that had the error.", driver);
			
			//An error should display an Inline Error Message in the Text Entry Field that had the error and 
			
			Log.softAssertThat(quickOrderPage.elementLayer.verifyElementTextContains("txtItemSearch1", InvalidAndValid[0], quickOrderPage) &&
					!quickOrderPage.elementLayer.verifyElementTextContains("txtItemSearch2", InvalidAndValid[1], quickOrderPage), 
					"Text should remain in error field and all correct IDs should clear.", 
					"Text is remaining in error field and all correct IDs are cleared.", 
					"Text is not remaining in error field and all correct IDs are not cleared.", driver);
			
			//Step-6: Verify the functionality of Product Information
			Log.reference("Functionality covered in the test case id: C19740 Step 2 to Step 10.");
			
			quickOrderPage.removeAllProducts();
			Log.message(i++ + ". Removed All items from Quick order");
			
			quickOrderPage.searchMultipleProductInQuickOrder(productID);
			Log.message(i++ + ". Product added to catalog.", driver);
			
			//Step-7: Verify the On click functionality of Add all to bag button
			//Add all to bag button removed from functionality - SM-5375
			/*Log.softAssertThat(quickOrderPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnAddAllToBag", "footerTopSection", quickOrderPage), 
					"Add all to bag Button should be displayed above the footer.", 
					"Add all to bag Button should be displayed above the footer.", 
					"Add all to bag Button should be displayed above the footer.", driver);
			
			if(Utils.isDesktop()) {
				quickOrderPage.mouseHoverOnAddAllToBag();
				Log.message(i++ + ". Without selecting size/color,  mouse hovered on Add All To Bag button.", driver);
				
				Log.softAssertThat(quickOrderPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnAddAllToBagSizeError"), quickOrderPage), 
						"System will display an error message if the user forgot to select all the items variation.", 
						"System will display an error message if the user forgot to select all the items variation.", 
						"System will display an error message if the user forgot to select all the items variation.", driver);
			}
			
			int noOfPrd = quickOrderPage.getNoOfProductInQuickOrderPage();
			Log.event("No of Products :: " + noOfPrd);
			quickOrderPage.openOrCloseAllShopNowSectionInPrdSet(true);
			quickOrderPage.selectSizeForAllPrd();
			quickOrderPage.selectColorForAllPrd();
			quickOrderPage.clickAddAllProductToBag();
			Log.message(i++ + ". Tried to click on add all to bag button.", driver);
			
			int noOfPrdInATB = quickOrderPage.getnoOfProductInATBMdl();
			Log.event("No of Products :: " + noOfPrdInATB);
			Log.softAssertThat(noOfPrd == noOfPrdInATB &&
					quickOrderPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlMiniCartOverLay"), quickOrderPage), 
					"Click on Add all to bag button, it adds all of the items quick order list to the bag and invokes the add to cart overlay in the multiple items added state", 
					"Click on Add all to bag button, it adds all of the items quick order list to the bag and invokes the add to cart overlay in the multiple items added state", 
					"Click on Add all to bag button, it adds all of the items quick order list to the bag and invokes the add to cart overlay in the multiple items added state", driver);
			
			quickOrderPage.closeAddToBagOverlay();
			Log.message(i++ + ". Add all to bag overlay is closed.", driver);*/
			
			if(Utils.isMobile()) {
			quickOrderPage.toggleProductDetails(0, Toggle.Open);
			Log.message(i++ + ". Opened Shop now drawer.", driver);	
				
			Log.softAssertThat(quickOrderPage.elementLayer.verifyElementDisplayed(Arrays.asList("colorAttribute", "sizeAttribute"), quickOrderPage), 
					"User should be allow to select Product color,size,size shop variation", 
					"User should be allow to select Product color,size,size shop variation", 
					"User should be allow to select Product color,size,size shop variation", driver);
			
			Log.softAssertThat(quickOrderPage.elementLayer.verifyElementDisplayed(Arrays.asList("drpQty"), quickOrderPage), 
					"User should allow to select product quantity.", 
					"User should allow to select product quantity.", 
					"User should allow to select product quantity.", driver);
			
			quickOrderPage.selectColor(1);
			Log.message(i++ +" Selected first product color", driver);
			
			quickOrderPage.selectSize(1);
			Log.message(i++ +" Selected first product Size", driver);
			
			quickOrderPage.clickAddProductToBag(0);
			Log.message(i++ + ". Clicked on Add To Bag button for first product.", driver);
			
			Log.softAssertThat(quickOrderPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlMiniCartOverLay"), quickOrderPage), 
					"User should be allow to add the item to cart page by clicking on \"Add to bag\" button ", 
					"User should be allow to add the item to cart page by clicking on \"Add to bag\" button ", 
					"User should be allow to add the item to cart page by clicking on \"Add to bag\" button ", driver);
			
			}
			//Step-9: Verify the functionality "Shop Now Drawer Close" in Mobile.
			//quickOrderPage.closeAddToBagOverlay();
			
			quickOrderPage.toggleProductDetails(0, Toggle.Close);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(!(quickOrderPage.elementLayer.verifyElementDisplayed(Arrays.asList("shopNowWindow_ProductSet_Open"), quickOrderPage)), 
						"Tap on SHOP NOW or on the (^) symbol, closes SHOP NOW drawer.", 
						"Tap on SHOP NOW or on the (^) symbol, closes SHOP NOW drawer.", 
						"Tap on SHOP NOW or on the (^) symbol, closes SHOP NOW drawer.", driver);
			}
			
			Log.testCaseResult();

	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP2_C19735


}// search
