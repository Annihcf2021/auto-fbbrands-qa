package com.fbb.testscripts.drop3;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19721 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(enabled = false, groups = { "low", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19721(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("ps_product-set");
	
		List<String> pdp_ElementsToBeVerified=null;
		List<String> elementsToBeVerified = null;
		List<String> socialIcons_ElementsToBeVerified=null;
		if(Utils.isTablet()){
			elementsToBeVerified = Arrays.asList("divBreadcrumb","divPrimaryimges","divProductSetAlternativeimagesTablet","divRatings","txtProductSetNameTablet",
					"divProductdescriptionTablet","divLongdescriptionTablet","txtshopthelook","btnAddalltobag","divproductsetlist");
		}else{
			elementsToBeVerified = Arrays.asList("divBreadcrumb","divPrimaryimges","divProductSetAlternativeimages","divRating","txtProductSetNameDesktop",
					"divProductdescription","divLongdescription","txtshopthelook","btnAddalltobag","divproductsetlist");
		}
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to Pdp Page!", driver);
	
			//1 Verify the display of Components in Product set detail Page
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(elementsToBeVerified, pdpPage),
					"Breadcrumb, Primary Image, Alt.Imgs, Review, PS Name, Description, Long Description, Add all to Bag, Product Set List should be displayed.",
					"All the above mentioned items displayed!",
					"Items not displayed as Expected!", driver);
	
			//2 Verify the Bread crumb in Product set Page
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedleft("cntPdpContent", "bcCurrentCategoryNameDesktop", pdpPage),
					"Breadcrumb should be displayed left side in pdp page",
					"Breadcrumb is be displayed left side in pdp page", 
					"Breadcrumb is not be displayed left side in pdp page", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("divHeaderBanner", "prodBreadCrumb", pdpPage),
					"Breadcrumb should be displayed below the L1 navigation in pdp page",
					"Breadcrumb is be displayed below the L1 navigation in pdp page", 
					"Breadcrumb is not be displayed below the L1 navigation in pdp page", driver);
	
			Log.softAssertThat(pdpPage.verifyBreadcrumbSymbol(),
					"Categories in the brand category root should be delimited with the symbol ' / '",
					"Categories in the brand category root is delimited with the symbol as expected ' / '", 
					"Categories in the brand category root is not delimited with the symbol ' / '", driver);
	
			//3 Verify the Product set image in Product set Page
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("prodBreadCrumb", "mainProdImage", pdpPage),
					"Product image should be displayed to the left hand side below the breadcrumb",
					"Product image should is displayed to the left hand side below the breadcrumb", 
					"Product image should is not displayed to the left hand side below the breadcrumb", driver);
	
			if(Utils.isDesktop()){
				pdp_ElementsToBeVerified = Arrays.asList("divProductBadge","divImageOverlay","divProductSetAlternativeimages", "btnControls");
			}else{
				pdp_ElementsToBeVerified = Arrays.asList("divProductBadge","divImageOverlay","divProductSetAlternativeimagesTablet");
			}
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(pdp_ElementsToBeVerified, pdpPage),
					"Above contents should be displayed in product imagery",
					"Above contents is displayed in product imagery as expected", 
					"Above contents is not displayed in product imagery", driver);
	
			//4 Verify the display of Alternate images in Product Set Page
			if(Utils.isDesktop()){
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("mainProdImage", "divProductSetAlternativeimages", pdpPage),
						"Alternate images should be displayed below the Product set main image.",
						"Alternate images is displayed below the Product set main image as expected", 
						"Alternate images is not displayed below the Product set main image.", driver);				
			}
			else if(Utils.isTablet()){
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("divProductdescription", "divProductSetAlternativeimages", pdpPage),
						"Alternate images should be displayed below the pricing in Pdp Page",
						"Alternate images is displayed below the pricing in Pdp Page as expected", 
						"Alternate images is not displayed below the pricing in Pdp Page.", driver);	
			}
	
			//5 Verify the Product Rating in Product set Page
	
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divRating", "mainProdImage", pdpPage),
					"The Ratings should display right side to the product main image",
					"The Ratings is getting displayed to the right side of product main image", 
					"The Ratings is not getting displayed to the right side of product main image", driver);
	
			if (Utils.isDesktop()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divRating", "txtProductSetNameDesktop", pdpPage),
						"The Ratings should display above the product set name",
						"The Ratings is displaying above the product set name", 
						"The Ratings is not displaying above the product set name", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divRating", "txtProductSetNameTablet", pdpPage),
						"The Ratings should display above the product set name",
						"The Ratings is displaying above the product set name", 
						"The Ratings is not displaying above the product set name", driver);
			}
	
			String[] star = pdpPage.getProductReviewStarInPrdSet();
	
			if(star[1].contains("5") || !(star[0].contains("0"))) {
				Log.softAssertThat(star[1].contains("5") || !(star[0].contains("0")),
						"The product is already reviewed and star should displayed",
						"The product is already reviewed and star is "+star[0]+"."+star[1],
						"The product is not already reviewed", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lblReviewCount"), pdpPage),
						"Check the review count is displaying in the page",
						"The review count is displaying in the page",
						"The review count is not displaying in the page", driver);
	
				pdpPage.clickOnProductReviewCount();
	
				Log.softAssertThat(pdpPage.elementLayer.verifyAttributeForElement("lblReviewCount", "class", "current", pdpPage),
						"Check the review is displaying if we click on the review count",
						"The review is displaying if we click on the review count",
						"The review is not displaying if we click on the review count", driver);
	
			} else {
				Log.softAssertThat(pdpPage.verifyProductReview("BE THE FIRST TO WRITE A REVIEW"), 
						"Product is not reviewed and Be the first to write a review should displayed", 
						"Product is not reviewed and Be the first to write a review is displayed", 
						"Product is not reviewed and Be the first to write a review is not displayed", driver);
			}
	
			//6 Verify the display of Product set Name in Product set Page
			if(Utils.isDesktop()){
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("divRating", "txtProductSetNameDesktop", pdpPage),
						"Product name should be displayed below the product ratings in Pdp Page",
						"Product name is displayed below the product ratings in Pdp Page as expected", 
						"Product name is not displayed below the product ratings in Pdp Page.", driver);			
			}
			else if(Utils.isTablet()){
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("divRatings", "txtProductSetNameTablet", pdpPage),
						"Product name should be displayed below the product ratings in Pdp Page",
						"Product name is displayed below the product ratings in Pdp Page as expected", 
						"Product name is not displayed below the product ratings in Pdp Page.", driver);	
			}
	
			// 7  Product Short Description
			if(Utils.isDesktop()){
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("divRating","divProductdescription", pdpPage),
						"Short description should be displayed below the Pricing on the right-hand side.",
						"Short description should be displayed below the Pricing on the right-hand side as expected", 
						"Short description should be displayed below the Pricing on the right-hand side.", driver);			
			}
			else if(Utils.isTablet()){
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("divRatings","divProductdescriptionTablet", pdpPage),
						"Short description should be displayed below the Pricing on the right-hand side.",
						"Short description should be displayed below the Pricing on the right-hand side as expected", 
						"Short description should be displayed below the Pricing on the right-hand side.", driver);	
			}
	
			//9 Long Description
			if(Utils.isDesktop()){
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("divRating", "divLongdescription", pdpPage),
						"Long description should be displayed below the Product pricing in Pdp Page",
						"Long description should be displayed below the Product pricing. Page as expected", 
						"Long description should be displayed below the Product pricing..", driver);	
	
			}
			else if(Utils.isTablet()){
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("divProductSetAlternativeimagesTablet", "divLongdescriptionTablet", pdpPage),
						"Long description should be displayed below the Alternate Images in Pdp Page",
						"Long description is displayed below the Alternate Images Page as expected", 
						"Long description is not displayed below the Alternate Images.", driver);	
	
			}
			//10 Social Icon
			if(pdpPage.elementLayer.verifyPageElements(Arrays.asList("shareIcons"), pdpPage))
				Log.softAssertThat(pdpPage.verifyShareIconPosition(),
						"The text label <<Share>> should be display in the front of Social icons in Pdp Page",
						"The text label <<Share>> is display in the front of Social icons as expected", 
						"The text label <<Share>> is not display in the front of Social icons..", driver);	
			else
				Log.failsoft(i++ + ". Social Sharing Icons in Product description section not available.");
	
			if(Utils.isDesktop()){
				socialIcons_ElementsToBeVerified = Arrays.asList("socialIconPinterest","socialIconFacebook","socialIconTwitter","socialIconEmail");
			}else{
				socialIcons_ElementsToBeVerified = Arrays.asList("socialIconPinterestTablet","socialIconFacebookTablet","socialIconTwitterTablet","socialIconEmailTablet");
			}
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(socialIcons_ElementsToBeVerified, pdpPage),
					"Above contents should be displayed in Social Icon List in the Pdp Page",
					"Above contents is displayed in Social Icon List in the Pdp Page as expected", 
					"Above contents is not displayed in Social Icon List in the Pdp Page", driver);
	
			//11 shop look
			if(Utils.isDesktop()){
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("divSocialIcons", "txtshopthelook", pdpPage),
						"Shop the look section should be displayed below the social icons",
						"Shop the look section is displayed below the social icons as expected", 
						"Shop the look section is not displayed below the social icons", driver);	
			}
			else if(Utils.isTablet()){
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("mainProdImage", "txtshopthelook", pdpPage),
						"Shop the look section should be displayed below the Product set main image. in Pdp Page",
						"Shop the look section should be displayed below the Product set main image. as expected", 
						"Shop the look section should be displayed below the Product set main image.", driver);	
	
			}   
			//12 Add to all bag
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("divSocialIcons", "btnAddalltobag", pdpPage),
					"ADD ALL TO BAG should be displayed in immediate first component product",
					"ADD ALL TO BAG is displayed in immediate first component product", 
					"ADD ALL TO BAG is not displayed in immediate first component product", driver);	
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("mainProdImage", "btnAddalltobag", pdpPage),
					"ADD ALL TO BAG should be displayed in immediate last component product",
					"ADD ALL TO BAG is displayed in immediate last component product", 
					"ADD ALL TO BAG is not displayed in immediate last component product", driver);	
	
			//12 Product set details
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtshopthelook", "prodContentSplProductSet", pdpPage),
					"The Shop look section should display above the product set content",
					"The Shop look section is displaying above the product set content", 
					"The Shop look section is not displaying above the product set content", driver);
	
			//Review page
			Log.message("Review pages functionalities hold due to PXSFCC-5498");
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_19721


}// search
