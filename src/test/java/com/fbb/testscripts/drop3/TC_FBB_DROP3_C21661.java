package com.fbb.testscripts.drop3;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C21661 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "low", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C21661(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prdWithQCPrice = TestData.get("qc_catprod_equivalent_origprod").split("\\|")[0];
		String nonQCPrd = TestData.get("qc_invalid");
		String nonActiveQCPricePrd = TestData.get("qc_expired");
		String regPrd = TestData.get("qc_catprod_equivalent_origprod").split("\\|")[1];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String credential = AccountUtils.generateEmail(driver) + "|" + accountData.get("password_global");
		//Pre-requisite - Account Should have more than one payment information
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, credential);
		}
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Due to Quick Order in Footer not working, My Account Flow used here
			homePage.headers.navigateToMyAccount(AccountUtils.generateEmail(driver), accountData.get("password_global"), true);
			Log.message(i++ + ". Navigated to Account Page!", driver);
	
			QuickOrderPage quickOrder = homePage.footers.navigateToQuickOrder();
			Log.message(i++ + ". Navigated to Quick order Landing Page", driver);
	
			//quickOrder.removeAllProducts();
			//Log.message(i++ + ". Removed All products", driver);
			//Step-1: Verify the item number is matched with order catalog number
			//a,b cannot be automated due to demandware verifications
			//c - If item have active pricing, system should display the product with quick order pricing.
			quickOrder.searchItemInQuickOrder(prdWithQCPrice, true);
			quickOrder.selectSize(0);
			Log.message(i++ + ". Product added to Quick Order Catalog : " + quickOrder.getLastAddedProduct());
	
			//Log.softAssertThat(quickOrder.getProductPrice(prdWithQCPrice).trim().equals(qcPrice),
			Log.softAssertThat(quickOrder.elementLayer.verifyPageElements(Arrays.asList("txtPrice"), quickOrder),
					"System should display the product with quick order pricing.",
					"System display's the product with quick order pricing!",
					"System not displayed the product with quick order pricing.", driver);
	
			quickOrder.removeAllProducts();
			//Step-2: Verify the item number for brand storefront catalog
			//a,b cannot be automated due to demandware verifications
			//c - When item number not available, system should display No Result message.
			quickOrder.searchItemInQuickOrder(nonQCPrd, false);
			Log.message(i++ + ". Tried to Add Invalid Quick order product id.", driver);
	
			Log.softAssertThat(quickOrder.elementLayer.verifyPageElements(Arrays.asList("txtInvalidErrorMessage"), quickOrder),
					"System should display 'Please select a valid number' in Item number box.'",
					"System displays the error message item number box.",
					"System not displayed any error message in item number box.", driver);
	
			//Step-3: Verify the items active pricing in quick order catalog pricebook.
			//Already automated in step-1
			quickOrder.searchItemInQuickOrder(nonActiveQCPricePrd, true);
			quickOrder.selectSize(0);
			Log.message(i++ + ". Product which have non-active QO price book added to Quick Order Catalog : " + quickOrder.getLastAddedProduct());
	
			Log.softAssertThat(quickOrder.elementLayer.verifyPageElements(Arrays.asList("catalogPriceExpired"), quickOrder),
					"System should display Catalog Price Expired message.",
					"System displays Catalog Price Expired message",
					"System not displayed Catalog Price Expired Message", driver);
	
			quickOrder.removeAllProducts();
			//Step-4: Verify when the entered item number is not a quick order catalog number
			//Cannot automated due to demandware verification
	
			//Step-5: Verify when the item number is a product ID in Brand storefront catalog
			//a,b - cannot automated due to demandware verification
			//c - When Item number is a Product ID, System should allocate Web pricing
	
			quickOrder.searchItemInQuickOrder(prdWithQCPrice, true);
			String size = quickOrder.selectSize(0);
			Log.message(i++ + ". Product which have active QO price book added to Quick Order Catalog : " + quickOrder.getLastAddedProduct());
	
			String priceInQC = quickOrder.getProductPrice(prdWithQCPrice);
			Log.message(i++ + ". Product Price from Quick Order Page :: " +priceInQC);
	
			PdpPage pdpPage = homePage.headers.redirectToPDP(regPrd, driver);
			pdpPage.selectSize(size);
			String priceInPDP = pdpPage.getProductPrice();
	
			Log.message(i++ + ". Navigated to PDP and Product Price :: " + priceInPDP);
	
			Log.softAssertThat(!(priceInQC.trim().equals(priceInPDP.trim())),
					"When Item number is a Product ID, System should allocate Web pricing",
					"When Item number is a Product ID, System allocate Web pricing",
					"When Item number is a Product ID, System not allocate Web pricing", driver);
	
			quickOrder.removeAllProducts();
			//d - already automated in Step-2
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP2_C21661	


}// search
