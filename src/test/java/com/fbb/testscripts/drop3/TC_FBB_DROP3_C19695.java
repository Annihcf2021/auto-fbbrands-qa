package com.fbb.testscripts.drop3;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19695 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19695(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String regularPriceProd = TestData.get("prd_single-price");
		String regularPriceRangeProd = TestData.get("prd_price-range");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Load Regular Price Product
			PdpPage pdpPage = homePage.headers.navigateToPDP(regularPriceProd);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			Log.message("<br><b><u>Regular Price Product</u></b>");
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblRegularPrice"), pdpPage)
					&& pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lblSalePrice","lblOriginalPriceInSalePrice"), pdpPage), 
					"For Regular Price Product, It should show only Original Price.", 
					"Original Price displayed!", 
					"Original Price Not displayed", driver);
	
			//Load Regular Price Range Product - Hold Due to TestData requirement
			pdpPage = homePage.headers.navigateToPDP(regularPriceRangeProd);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			Log.message("<br><b><u>Regular Price Range Product</u></b>");
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblRegularPriceRange"), pdpPage)
					&& pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lblSalePrice","lblOriginalPriceInSalePrice"), pdpPage), 
					"For Regular Price Range Product, It should show only Original Price.", 
					"Original Price displayed!", 
					"Original Price Not displayed", driver);
	
			Log.softAssertThat(pdpPage.getRegularPriceRange().contains("-") &&
					pdpPage.getRegularPriceRange().split("\\-")[0].contains("$") &&
					pdpPage.getRegularPriceRange().split("\\-")[1].contains("$"),
					"Orignial Price range should be displayed as $XX.XX - $XX.XX",
					"Orignial Price range displayed as $XX.XX - $XX.XX!",
					"Orignial Price range Not displayed as Expected.", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP2_C19695

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP3_C19695(String browser) throws Exception {
		Log.testCaseInfo();
	
		String salePriceProd = TestData.get("prd_markdown-single-price");
		String salePriceColor=TestData.get("SalesPriceColor");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Load Sale Price Product
			PdpPage pdpPage = homePage.headers.navigateToPDP(salePriceProd);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			Log.message("<br><b><u>Sale Price Product</u></b>");
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblSalePrice","lblOriginalPriceInSalePrice"), pdpPage), 
					"For Sale Price Product, It should show Original Price & Sales Price.", 
					"Original Price & Sales Price displayed!", 
					"Original Price or Sales price Not displayed", driver);
			{
				try{
					float originalPrice = Float.parseFloat(pdpPage.getOriginalPrice().replace("$", ""));
					float salePrice = Float.parseFloat(pdpPage.getSalePrice().replace("$", ""));
	
					Log.softAssertThat(originalPrice > salePrice, 
							"Sale price will only display when the sale price is different from the original price.", 
							"Orginal Price and Sale Price are different!", 
							"Orignal Price and Slae Price are not different.", driver);
				}catch(NumberFormatException e){
					Log.failsoft("Skipping this validation due to Given product doesn't have single markdown price.", driver);
				}
			}
	
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("lblOriginalPriceInSalePrice", "text-decoration", "line-through", pdpPage), 
					"Original Price should displayed with strikethrough", 
					"Original Price displayed with strikethrough!", 
					"Original Price not displayed with strikethrough.", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lblSalePrice", "lblOriginalPriceInSalePrice", pdpPage),
					"Sale Price should be displayed to the right side of Original Price",
					"Sale Price displayed in the right side of Original Price!",
					"Sale Price not displayed in the right side of Original Price", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementColor("lblSalePrice",salePriceColor, pdpPage),
					"Sale Price should be displayed in Red Color",
					"Sale Price displayed in Red Color!",
					"Sale Price Not displayed in Red Color.", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP2_C19695
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP3_C19695(String browser) throws Exception {
		Log.testCaseInfo();
	
		String salePriceRangeProd = TestData.get("prd_markdown-price-range");
		String regularPriceRangeProd = TestData.get("prd_price-range");
		String salePriceColor=TestData.get("SalesPriceColor");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Load Sale Price Range Product
			PdpPage pdpPage = homePage.headers.navigateToPDP(salePriceRangeProd);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			Log.message("<br><b><u>Sale Price Range Product</u></b>");
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblSalePrice","lblOriginalPriceInSalePrice"), pdpPage), 
					"For Sale Price Range Product, It should show Original Price Range and Sales Price Range.", 
					"Original Price Range and Sales Price Range displayed!", 
					"Original Price or Sales Price Range Not displayed", driver);
			{
				String originalPrice = pdpPage.getOriginalPrice();
				String salePrice = pdpPage.getSalePrice();
				Log.softAssertThat(!originalPrice.equals(salePrice), 
						"Sale price will only display when the sale price range is different from the original price range.", 
						"Orginal Price range and Sale Price range are different!", 
						"Orignal Price range and Sale Price range are not different.", driver);
			}
	
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("lblOriginalPriceInSalePrice", "text-decoration", "line-through", pdpPage), 
					"Original Price should displayed with strikethrough", 
					"Original Price displayed with strikethrough!", 
					"Original Price not displayed with strikethrough.", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lblSalePrice", "lblOriginalPriceInSalePrice", pdpPage),
					"Sale Price should be displayed to the right side of Original Price",
					"Sale Price displayed in the right side of Original Price!",
					"Sale Price not displayed in the right side of Original Price", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementColor("lblSalePrice", salePriceColor, pdpPage),
					"Sale Price should be displayed in Red Color",
					"Sale Price displayed in Red Color!",
					"Sale Price Not displayed in Red Color.", driver);
			
			pdpPage = homePage.headers.navigateToPDP(regularPriceRangeProd);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			Log.softAssertThat(pdpPage.getRegularPriceRange().contains("-") &&
					pdpPage.getRegularPriceRange().split("\\-")[0].contains("$") &&
					pdpPage.getRegularPriceRange().split("\\-")[1].contains("$"),
					"Orignial Price range should be displayed as $XX.XX - $XX.XX",
					"Orignial Price range displayed as $XX.XX - $XX.XX!",
					"Orignial Price range Not displayed as Expected.", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP2_C19695

}// search
