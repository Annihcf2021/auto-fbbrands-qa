package com.fbb.testscripts.drop3;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.GiftCardPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19699 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;

	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19699(String browser) throws Exception {
		Log.testCaseInfo();
	
		String giftCardSize;		
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.footers.navigateToEGiftCard();
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			//Step-2: Verify the product image in the Gift card page
			if(Utils.isMobile()){
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductNameDesktop", "mainProdImage_Mobile", pdpPage),
						"Product Image should be displayed below the product name.",
						"Product Image displayed below the product name.",
						"Product Image not displayed below the product name.", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "prodBreadCrumb", "mainProdImage", pdpPage) && 
						pdpPage.elementLayer.verifyElementDisplayedleft("cntPdpContent","mainProdImage", pdpPage),
						"Product Image should be displayed to the left hand side of the Gift card page below the Bread crumb",
						"Product Image displayed to the left hand side of the Gift card page below the Bread crumb",
						"Product Image not displayed to the left hand side of the Gift card page below the Bread crumb", driver);
			}
	
			Log.softAssertThat(pdpPage.verifyGcImages(),
					"For gift card ,there Should only be one image.", 
					"For gift card ,only be one image is displayed!", 
					"For gift card ,more than one image is displayed", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("splProductMessaging"), pdpPage), 
					"Special Product messaging should be displayed", 
					"Special Product messaging is displayed", 
					"Special Product messaging is not displayed", driver);
	
			//Step-3: Verify the Special Product Messaging in the E-Gift card page
			//Need to get time configuration from BM on runtime which requires to load BM page load
	
			//Step-4: Click/tap on Terms and conditions in the E-Gift card page
			if(pdpPage.isSpecialProductMessageDisplayed())
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblProductSpecialMessage", "lnkTermsAndConditions", pdpPage),
						"The Terms and Conditions should be displayed as a link below the Special product messaging.",
						"The Terms and Conditions displayed as a link below the Special product messaging.",
						"The Terms and Conditions not displayed as a link below the Special product messaging.", driver);
			else
				Log.reference("Special Product Message Not displayed. Since skipping this validation: <b>The Terms and Conditions should be displayed as a link below the Special product messaging.</b>");
	
			pdpPage.openCloseToolTipOverlay("open");
			Log.message(i++ + ". Clicked on Terms & Conditions!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("toolTipOverLay"), pdpPage), 
					"Terms & Conditions Tool tip should be opened.", 
					"Terms & Conditions Tool tip opened!",
					"Terms & Conditions Tool tip not opened.", driver);
	
			pdpPage.openCloseToolTipOverlay("close");
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("toolTipOverLay","display","none", pdpPage), 
					"Tooltip should be closed when clicking on X mark.", 
					"Tooltip closed when clicking on X mark!", 
					"Tooltip not closed when clicking on X mark", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("toolTipOverLay"), pdpPage), 
					"Tooltip should be closed when clicking on X mark.", 
					"Tooltip closed when clicking on X mark!", 
					"Tooltip not closed when clicking on X mark", driver);
	
			Log.reference("Tooltip dimension validations verified with C21621.");
	
			//Step-5: Verify the card Design for the E-Gift card page - Not Automatable
			if(pdpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divColorSwatches"), pdpPage)) {
				Log.softAssertThat(pdpPage.getNumberOfColorSwatches() >= 1, 
						"User Should have the ability to choose gift card image.", 
						"User can choose the gift card image", 
						"User don't have the ability to choose the gift card image", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkTermsAndConditions", "divColorSwatches", pdpPage),
						"Card Design should display below the Terms and Conditions link.",
						"Card Design displayed below the Terms and Conditions link.",
						"Card Design not displayed below the Terms and Conditions link.", driver);
		
				int noOfColorSwatches = pdpPage.getNumberOfColorSwatches();
				Log.softAssertThat(noOfColorSwatches > 0,
						"There should be several images available in the card design section.User should able to select the design of the image",
						"User can able to select design of the product.",
						"Available color designs(" + noOfColorSwatches + "). User cannot able to select another designs.", driver);
			}else {
				Log.reference("Gift Card color swatch selection disabled. Verification skipped.");
			}
	
			//Step-6: Click/Tap on Select Amount name or (v) button
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("lnkTermsAndConditions", "drpGiftCardSizeSelect", pdpPage),
					"Select Amount should be displayed below the Terms and Conditions section.",
					"Select Amount displayed below the Terms and Conditions section.",
					"Select Amount not displayed below the Terms and Conditions section.", driver);
	
			Log.softAssertThat(pdpPage.getSelectedGCAmount().equalsIgnoreCase("Select Amount"), 
					"'Select Amount' text should be displayed by default", 
					"'Select Amount' text displayed by default" , 
					"'Select Amount' text not displayed by default", driver);
	
			String selectedSize = pdpPage.selectGCSize();
			Log.message(i++ + ". Size(" + selectedSize + ") selected for Giftcard ", driver);
	
			Log.softAssertThat(!(selectedSize.isEmpty()), 
					"Select Amount drop down should be opened", 
					"Select Amount drop down opened!", 
					"Select Amount drop down not opened", driver);
	
			giftCardSize = pdpPage.selectGiftCardSize();
			Log.message(i++ + ". Clicked on Select Amount Dropdown.", driver);
	
			Log.softAssertThat(pdpPage.getSelectedGCAmount().equals(giftCardSize), 
					"Selected value should be display in the select amount field", 
					"Selected value displayed in the select amount field", 
					"Selected value not displayed in the select amount field", driver);
	
			//Step-7: Verify the Recipient Email address and Email verification field
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("drpGiftCardSizeSelect", "sectionEGiftEmail", pdpPage),
					"Recipient Email address and Email verification field should display below the Select amount drop down box.",
					"Recipient Email address and Email verification field displayed below the Select amount drop down box.",
					"Recipient Email address and Email verification field not displayed below the Select amount drop down box.", driver);
	
			pdpPage.checkGCPersonalizedMessage();
			pdpPage.enterRecepientEmailAdd("abc");
			Log.message(i++ + ". Entered invalid recepient Email id ", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtRecepientEmailError"), pdpPage),
					"Error should be displayed when entering invalid recepient email address",
					"Error is displayed when entering invalid recepient email address",
					"Error is not displayed when entering invalid recepient email address", driver);
			
			pdpPage.clearRecepientEmailAddress();
			//Step-8: 
			pdpPage.enterRecepientEmailAdd("abc@gmail.com");
			Log.message(i++ + ". Entered valid recepient Email id", driver);
	
			pdpPage.enterRecepientEmailConfirmAdd("abc");
			Log.message(i++ + ". Entered invalid recepient confirm Email id ", driver);
	
			pdpPage.clickAddProductToBag();
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("errRecepientConfirmMail"), pdpPage),
					"Error should be displayed when entering invalid recepient email address in confirm address",
					"Error is displayed when entering invalid recepient email address in confirm address",
					"Error is not displayed when entering invalid recepient email address in confirm address", driver);
	
			//Step 9: 
			pdpPage.clearRecepientEmailAddress();
	
			selectedSize = pdpPage.selectGCSize();
			Log.message(i++ + ". Size(" + selectedSize + ") selected for Giftcard ", driver);
	
			/*pdpPage.selectColor();
			Log.message(i++ + ". Color Selected");*/
	
			pdpPage.enterRecepientEmailAdd("abc@gmail.com");
			Log.message(i++ + ". Entered valid recepient Email id ", driver);
	
			pdpPage.enterRecepientEmailConfirmAdd("abc@gmail.com");
			Log.message(i++ + ". Entered valid recepient confirm Email id ", driver);
	
			pdpPage.clickGiftCardCheckbox(true);
			Log.message(i++ + ". Checked personalisation checkbox", driver);
	
			pdpPage.enterRecepientName();
			Log.message(i++ + ". Entered Recepient Name", driver);
	
			pdpPage.enterPersonalMessage("Personal message");
			Log.message(i++ + ". Entered text in the message textbox ", driver);
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add To Bag Button after selecting the amount.", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlMiniCartOverLay"), pdpPage), 
					"Gift card should be added to the cart", 
					"Gift card is added to the cart.", 
					"Gift card is not added to the cart.", driver);
	
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Mini cart flyout is closed.", driver);
	
			//Step-10  
			pdpPage.clearRecepientEmailAddress();
	
			/*pdpPage.selectColor();
			Log.message(i++ + ". Color Selected", driver);*/
	
			pdpPage.enterRecepientEmailAdd("abc@gmail.com");
			Log.message(i++ + ". Entered valid recepient Email id ", driver);
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add To Bag Button without Confirm Email Address.", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("errPleaseSelectConfirmMail","errConfirmMailEmpty"), pdpPage),
					"Error message should be displayed for Confirm Email Address box",
					"Error message is displayed",
					"Error message is not displayed", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("drpGiftCardSizeSelect", "chkGCPersonalizedMessage", pdpPage), 
					"Personalization checkbox should display below the select amount", 
					"Personalization checkbox is display below the select amount", 
					"Personalization checkbox is not display below the select amount", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("personalizedMethodUchecked"), pdpPage), 
					"Personalization checkbox should display below the select amount and unchecked",
					"Personalization checkbox is display below the select amount and unchecked", 
					"Personalization checkbox is not display below the select amount and unchecked", driver);
			//Step-11
			pdpPage.clickGiftCardCheckbox(true);
	
			pdpPage.clearGCPersonalMessage();
			Log.message(i++ + ". Checked personalisation checkbox", driver);
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add To Bag Button after selecting the amount.", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyAttributeForElement("txtAreaPersonalMsgError", "class", "error", pdpPage),
					"Error message should be displayed in Personal Message Box.",
					"Error message is displayed",
					"Error message is not displayed", driver);
	
			//Step -12
			pdpPage.enterPersonalMessage("Personal message");
			Log.message(i++ + ". Entered text in the message textbox ", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtPersonalMsgCounter"), pdpPage),
					"For text count verification in Personal Message Text Area.",
					"Text 'You have reached the limit' is displayed!",
					"Text 'You have reached the limit' is not displayed!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("txtAreaPersonalMsg"), 330, pdpPage),
					"For text count verification in Personal Message Text Area.",
					"After maximum character the TextArea didnt allow to enter text!",
					"After maximum character the TextArea allowing to enter text!", driver);
	
			//Step -13              
	
			Log.reference("Refer test case # 19683 for Step 13");
	
			//Step-14: Click/ Tap on Add to bag button when the personalization checkbox is unselected & amount, email & Email confirmation is provided.
	
			pdpPage.enterRecepientEmailAdd("abc@gmail.com");
			Log.message(i++ + ". Entered valid recepient Email id ", driver);
	
			pdpPage.enterRecepientEmailConfirmAdd("abc@gmail.com");
			Log.message(i++ + ". Entered valid recepient confirm Email id ", driver);
	
			pdpPage.clickGiftCardCheckbox(true);
			Log.message(i++ + ". Unchecked personalisation checkbox", driver);
	
			pdpPage.enterRecepientName();
			Log.message(i++ + ". Entered Recepient Name", driver);
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add To Bag Button after selecting the amount.", driver);
	
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlMiniCartOverLay"), pdpPage), 
					"Gift card should be added to the cart", 
					"Gift card is added to the cart.", 
					"Gift card is not added to the cart.", driver);
	
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Mini cart flyout is closed.", driver);
	
			//Step-15
			pdpPage.enterRecepientEmailAdd("abc@gmail.com");
			Log.message(i++ + ". Entered valid recepient Email id ", driver);
	
			pdpPage.enterRecepientEmailConfirmAdd("abc@gmail.com");
			Log.message(i++ + ". Entered valid recepient confirm Email id ", driver);
	
			pdpPage.clickGiftCardCheckbox(true);
			Log.message(i++ + ". Checked personalisation checkbox", driver);
	
			pdpPage.enterRecepientName();
			Log.message(i++ + ". Entered Recepient Name", driver);
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add To Bag Button after selecting the amount.", driver);
	
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlMiniCartOverLay"), pdpPage), 
					"Gift card should be added to the cart", 
					"Gift card is added to the cart.", 
					"Gift card is not added to the cart.", driver);
	
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Mini cart flyout is closed.", driver);
	
			//Step-16: Click/ Tap on Add to bag button without selecting the Amount
			Log.reference("Refer test case # 19683 for Step 16");
	
			//Step -18 Check by adding 40 items in the bag and try to add E-gift card
			Log.reference("Refer test case # 19728 for Step 18");
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M1_FBB_DROP2_C19699

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP3_C19699(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String category = "e-Gift Card";
		String categoryAlt = "Home";
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Step-1
			GiftCardPage gcLanding = homePage.footers.navigateToGiftCardLandingPage();
			Log.message(i++ + ". Navigated to Gift Card Landing Page", driver);
			
			PdpPage pdpPage = gcLanding.navigateToGiftCertificate();
			Log.message(i++ + ". Navigated to PDP Pag for Product: " + pdpPage.getProductName(), driver);
			
			//Step-1: Click /Tap on the categories from the breadcrumb in the Gift card page
			if(Utils.isMobile()){
				String breadcrumbCategoryText = pdpPage.elementLayer.getElementText("lblCurrentBreadCrumbMobile", pdpPage).toUpperCase();
				Log.softAssertThat(breadcrumbCategoryText.contains(category.toUpperCase())
								|| breadcrumbCategoryText.contains(category.replace("-", "").toUpperCase())
								|| breadcrumbCategoryText.contains(categoryAlt.toUpperCase()), 
						"When user navigates via browse: breadcrumb should display the category path.", 
						"When user navigates via browse: breadcrumb display the category path.", 
						"When user navigates via browse: breadcrumb not display the category path.", driver);
			}else{
				String breadcrumbCategoryText = pdpPage.elementLayer.getElementText("lblPrimaryBreadCrumbDesktop", pdpPage).toUpperCase();
				Log.softAssertThat(breadcrumbCategoryText.contains(category.toUpperCase())
								|| breadcrumbCategoryText.contains(category.replace("-", "").toUpperCase())
								|| breadcrumbCategoryText.contains(categoryAlt.toUpperCase()), 
						"When user navigates via browse: breadcrumb should display the category path.", 
						"When user navigates via browse: breadcrumb display the category path.", 
						"When user navigates via browse: breadcrumb not display the category path.", driver);
			}
			
			Log.testCaseResult();

	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M2_FBB_DROP2_C19699
	
}// TC_FBB_DROP2_C19699
