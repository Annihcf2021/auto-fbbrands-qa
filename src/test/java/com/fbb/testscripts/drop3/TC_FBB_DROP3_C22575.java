package com.fbb.testscripts.drop3;
import com.fbb.reusablecomponents.TestData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.EnlargeViewPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.WishlistLoginPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C22575 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
		String product = TestData.get("prd_brandprimaryCat");
		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(product);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			//Step-1: Verify the functionality of Breadcrumb in the product detail page
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedleft("cntPdpContent", "divBreadcrumb", pdpPage),
					"Check the breadcrumb is displayed in left side of the product details page",
					"The breadcrumb is displayed in left side of the product details page",
					"The breadcrumb is not displayed in left side of the product details page", driver);
			
			pdpPage.clickOnHomeBC();
			Log.message(i++ + ". Navigated to Home page");
			
			Log.softAssertThat(new HomePage(driver).get().getPageLoadStatus(),
					"If user tabs/click on home in breadcrumb, system should navigate user to the homepage",
					"If user tabs/click on home in breadcrumb, system navigate user to the homepage",
					"If user tabs/click on home in breadcrumb, system not navigate user to the homepage", driver);
			
			pdpPage = homePage.headers.navigateToPDP(product);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			String category = pdpPage.getBreadCrumbLevelText(pdpPage.getBreadcrumbElementNumber()-2);
			Log.message(i++ +". Category detected:: " + category);
			
			PlpPage plpPage = homePage.headers.navigateToLastSubCategory();
			Log.message(i++ + ". Navigated to:: " + category, driver);
			
			if (Utils.isMobile()) {
				Log.softAssertThat(plpPage.elementLayer.verifyElementTextContains("lblCategoryNameMobile", category, plpPage),
						"if user tabs/click on any other category link, system should navigate user to the selected category page.",
						"if user tabs/click on any other category link, system navigate user to the selected category page.",
						"if user tabs/click on any other category link, system not navigate user to the selected category page.", driver);
			} else {
				Log.softAssertThat(plpPage.elementLayer.verifyElementTextContains("divCurrentBreadCrumbDesktopTablet", category, plpPage),
						"if user tabs/click on any other category link, system should navigate user to the selected category page.",
						"if user tabs/click on any other category link, system navigate user to the selected category page.",
						"if user tabs/click on any other category link, system not navigate user to the selected category page.", driver);
			}
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}//M1_FBB_DROP3_C22575
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
		String product = TestData.get("prd_variation2");
		String productBadge = TestData.get("prd_badge").split("\\|")[0];
		String productBadgeType = TestData.get("prd_badge").split("\\|")[1];
		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(product);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtProductName", "mainProdImage", pdpPage),
					"Product name should be displayed to the right hand side of the product imagery.",
					"Product name is displayed to the right hand side of the product imagery.",
					"Product name is not displayed to the right hand side of the product imagery.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedleft("cntPdpContent", "mainProdImage", pdpPage),
					"Product Images should be displayed to the left hand side of the Product detail page below the breadcrumb.",
					"Product Images is displayed to the left hand side of the Product detail page below the breadcrumb.",
					"Product Images is not displayed to the left hand side of the Product detail page below the breadcrumb.", driver);
			
			pdpPage = homePage.headers.navigateToPDP(productBadge);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedleft("cntPdpContent", "prodBadge", pdpPage),
					"Badge should be located in left corner.",
					"Badge is located in left corner.",
					"Badge is not located in left corner.", driver);
			
			int Count = pdpPage.getTotalNoOfBreadCrumb();
			if(Count < 2) {
				Log.failsoft("Breadcrumb not having category link, Hence cannot verify 'Same badge should be "
						+ "displayed in category page' verification");
			} else {
				PlpPage plpPage = pdpPage.clickOnBreadCrumbByIndex(Count-1);
				
				Log.softAssertThat(plpPage.verifyProdBadgeImgInParticularProd(productBadge, productBadgeType),
						"Same badge should be displayed in category page",
						"Same badge is displayed in category page.",
						"Same badge is not displayed in category page.", driver);
			}
			Log.testCaseResult();

			
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}//M2_FBB_DROP3_C22575
	
	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
		String product = TestData.get("prd_variation2");
		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.navigateToPDP(product);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);

			pdpPage.zoomProductImage();
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("zoomedProdImg"), pdpPage),
					"To check the transperant overlay of image is displaying or not.",
					"The image overlay is displaying.",
					"The image overlay is not displaying.", driver);

			Log.softAssertThat(pdpPage.elementLayer.verifyElementsAreInSameRow("mainProdImage", "zoomedProdImg", pdpPage),
					"To check the original product image and zoomed image are aligned in same row.",
					"The original product image and zoomed product image are in same row!",
					"The original product image and zoomed product image are not in same row", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}//M2_FBB_DROP3_C22575
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
	
		String videoPrd = TestData.get("prd_video");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser); 
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(videoPrd);
			Log.message(i++ + ". Navigated to PDP for product :: " + pdpPage.getProductName());
			
			if(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkVideoMain"), pdpPage)) {
				pdpPage.clickMainVideoLink();
				Log.message(i++ + ". Clicked on Video link.", driver);
		
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkVideoMain"), pdpPage),
						"To check the components(Video buttons) displaying in the product imagery.",
						"The 'Main Product Image', 'Product Batch', 'Enlarge button', 'Video link' are displaying",
						"Some components are missing / not displaying properly", driver);
		
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("prdVideoContainer"), pdpPage),					 
						"'Video Player' should be displayed", 
						"'Video Player' is displayed and it is taken over product main image", 
						"'Video Player' is not displayed in the place of product image", driver);
		
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("videoPlayer"), pdpPage),
						"To check the product image is overtaken the product main image.",
						"The product image is overtaken the product main image",
						"The product image is not overtaken the product main image", driver);
			}else {
				Log.reference("Video link not available for product.");
			}
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// M4_FBB_DROP3_C22575
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M5_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String badgePrd = TestData.get("prd_enlarge").split("\\|")[0];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser); 
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(badgePrd);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			EnlargeViewPage enlargeModal = pdpPage.clickOnEnlargeButton();
			Log.message(i++ + ". Clicked on Enlarge button.", driver);
			
			Utils.waitForPageLoad(driver);
			
			Log.softAssertThat(enlargeModal.elementLayer.verifyElementDisplayed(Arrays.asList("mdlEnlargeWindow","txtProductName","colorSwatches","alternateImages"), enlargeModal),					 
					"'Product Name','Color Swatch', 'Alternate Images' should be displayed on enlarge view page", 
					"'Product Name','Color Swatch', 'Alternate Images' is displayed", 
					"'Product Name','Color Swatch', 'Alternate Images' is not displayed", driver);
			
			//Setp-6: Click on Enlarge button
			if(Utils.isDesktop()){
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlEnlargeWindow"), pdpPage), 
						"Clicking on Enlarge button, a pop-up window will display.", 
						"Clicking on Enlarge button, a pop-up window will display.", 
						"Clicking on Enlarge button, a pop-up window will display.", driver);
	
				enlargeModal.selectColor(0);
				enlargeModal.clickOnAlternateImage(1);
				
				enlargeModal.mouseHoverOnPrimaryImage();
				Log.message(i++ + ". Mouse hovered on Primary Image.", driver);
				
				Log.softAssertThat(enlargeModal.verifyZoomedImage(), 
						"Product image should display as zoomed", 
						"Product image should display as zoomed", 
						"Product image should display as zoomed", driver);
				
				Log.softAssertThat(enlargeModal.verifyInitalImageMapping(), 
						"Alternate images associated with the color attribute group should be displayed with the first sequenced alternate image by default", 
						"Alternate images associated with the color attribute group should be displayed with the first sequenced alternate image by default", 
						"Alternate images associated with the color attribute group should be displayed with the first sequenced alternate image by default", driver);
	
				
				if (enlargeModal.elementLayer.verifyElementDisplayed(Arrays.asList("nextImageArrowEnlarge" ,"prevImageArrowEnlarge"), enlargeModal)) {
					int getSlickSlideIndexBefore = enlargeModal.getAltImgSlickIndex();
					
					enlargeModal.clickOnNextImageArrow();
					Log.message(i++ + ". Clicked on Right Arrow.", driver);
					
					int getSlickSlideIndexAfer = enlargeModal.getAltImgSlickIndex();
					
					Log.softAssertThat(getSlickSlideIndexAfer == (getSlickSlideIndexBefore + 1), 
							"Click on right arrow, should sequece the next image", 
							"Click on right arrow, should sequece the next image", 
							"Click on right arrow, should sequece the next image", driver);
		
					enlargeModal.clickOnPrevImageArrow();
					Log.message(i++ + ". Clicked on left Arrow.", driver);
					
					getSlickSlideIndexAfer = enlargeModal.getAltImgSlickIndex();
					
					Log.softAssertThat(getSlickSlideIndexAfer == getSlickSlideIndexBefore, 
							"Click on left arrow, should sequece the next image", 
							"Click on left arrow, should sequece the next image", 
							"Click on left arrow, should sequece the next image", driver);
				} else {
					Log.reference("Previous and Next arrows in enalrge preview are disabled in SM-6015. Arrow will remain disabled until PDP redesign, SM-5738.");
				}
				
				String thumImgCode = enlargeModal.selectAltImgAndGetColorCode(1);
				
				String mainImgCode = enlargeModal.getPrimaryImgColorCode();
				
				Log.event("Primary Image Color Code :: " + mainImgCode);
				Log.event("Selected Image Color Code :: " + thumImgCode);
				Log.softAssertThat(mainImgCode.contains(thumImgCode), 
						"Selected alternate image should populate in the main image space", 
						"Selected alternate image should populate in the main image space", 
						"Selected alternate image should populate in the main image space", driver);
	
				enlargeModal.mouseHoverOnAltImage(2);
				Log.message(i++ + ". Mouse hovered on 2nd alternate image.", driver);
				
				Log.softAssertThat(enlargeModal.elementLayer.verifyMaxElementsInRow("lstcolorSwatchesLi", 8, enlargeModal), 
						"Maximum 8 swatches should display per row in the desktop", 
						"Maximum 8 swatches should display per row in the desktop", 
						"Maximum 8 swatches should display per row in the desktop", driver);
	
				Log.softAssertThat(enlargeModal.elementLayer.verifyMaxElementsInRow("lstAlternateImages", 5, enlargeModal), 
						"Maximum 5 thumbnails should be displayed per row", 
						"Maximum 5 thumbnails should be displayed per row", 
						"Maximum 5 thumbnails should be displayed per row", driver);
	
			}else if(Utils.isTablet()){
				Log.softAssertThat(enlargeModal.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgMainProduct_Tablet", "attributeSection", enlargeModal), 
						"Once user tab on Enlarge button the enlarged view displays at 100% of the viewport with the product name, color swatches and alternate images displayed beneath", 
						"Once user tab on Enlarge button the enlarged view displays at 100% of the viewport with the product name, color swatches and alternate images displayed beneath", 
						"Once user tab on Enlarge button the enlarged view displays at 100% of the viewport with the product name, color swatches and alternate images displayed beneath", driver);
	
				Log.softAssertThat(enlargeModal.elementLayer.verifyMaxElementsInRow("lstAlternateImages", 4, enlargeModal), 
						"4 alternate images are displayed in per row", 
						"4 alternate images are displayed in per row", 
						"4 alternate images are displayed in per row", driver);
	
			}
	
			enlargeModal.closeEnlargeViewModal();
			pdpPage = new PdpPage(driver).get();
			Log.message(i++ + ". Clicked on Close button in enlarge modal.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("mdlEnlargeWindow"), pdpPage), 
					"Clicking on X button, the takeover should be closed and the user should be returned to the PDP Page", 
					"Clicking on X button, the takeover should be closed and the user should be returned to the PDP Page", 
					"Clicking on X button, the takeover should be closed and the user should be returned to the PDP Page", driver);
	
			//Step1
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// M5_FBB_DROP3_C22575
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M6_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String Prd = TestData.get("prd_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser); 
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(Prd);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.selectAllSwatchesForVariationProduct();
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductNameDesktop","divReviewAndRating", pdpPage), 
					"Product name should be displayed above the reviews in the PDP.", 
					"Product name displayed above the reviews in the PDP.", 
					"Product name not displayed above the reviews in the PDP.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductNameDesktop", "lblOriginalPrice", pdpPage), 
					"Selling price should be displayed below the Product name in the PDP", 
					"Selling price displayed below the Product name in the PDP", 
					"Selling price not displayed below the Product name in the PDP", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblOriginalPrice", "divColorGrid", pdpPage), 
					"Color attributes should be displayed below the product name and selling price/promotional messages", 
					"Color attributes displayed below the product name and selling price/promotional messages", 
					"Color attributes not displayed below the product name and selling price/promotional messages", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProdAvailStatus", "divQtySection", pdpPage), 
					"Quantity dropdown should be displayed below the inventory state", 
					"Quantity dropdown displayed below the inventory state", 
					"Quantity dropdown not displayed below the inventory state", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProdAvailStatus", "btnAddToCart", pdpPage), 
					"Add to bag button should be displayed below the inventory state.", 
					"Add to bag button displayed below the inventory state.", 
					"Add to bag button not displayed below the inventory state.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnAddToCart", "txtShippingAndReturnInfo", pdpPage), 
					"Shipping and Returns should be displayed below add to bag button", 
					"Shipping and Returns displayed below add to bag button", 
					"Shipping and Returns not displayed below add to bag button", driver);
			
			if(pdpPage.isShoeSizeAvailable()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionColorAttribute", "divShoeSize", pdpPage), 
						"Color Attribute should display above size attribute", 
						"Color Attribute is displaying above size attribute", 
						"Color Attribute is not displaying above size attribute", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionColorAttribute", "sectionSizeAttribute", pdpPage), 
						"Color Attribute should display above size attribute", 
						"Color Attribute is displaying above size attribute", 
						"Color Attribute is not displaying above size attribute", driver);
			}
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnAddToCart", "wishlist", pdpPage) &&
					pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "wishlist", "txtShippingAndReturnInfo", pdpPage), 
					"Add to Wishlist link should be in between Add To Bag and Shipping & Returns drop down", 
					"Add to Wishlist link is in between Add To Bag and Shipping & Returns drop down", 
					"Add to Wishlist link is not in between Add To Bag and Shipping & Returns drop down", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtShippingAndReturnInfo", "divSocialIcons", pdpPage), 
					"Social links should be displayed below Shipping and Returns", 
					"Social links is displayed below Shipping and Returns", 
					"Social links is not displayed below Shipping and Returns", driver);
			
			if(pdpPage.isSizeChartApplicable()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionColorAttribute", "lnkSizechart", pdpPage), 
						"The Size Chart link should be displayed below color attribute!",
						"The Size Chart link is displayed below color attribute",
						"The Size Chart link is not displayed below color attribute", driver);
				
				pdpPage.clickSizeChartLink();
				Log.message(i++ + ". Clicked on Size Chart link!", driver);

				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("modalSizechart"), pdpPage),
						"The Size Chart modal should be displayed!",
						"The Size Chart modal is displayed!",
						"The Size Chart modal is not displayed!", driver);
			} else {
				Log.failsoft("Test Data not applicable for Size Chart validations.");
			}
			
			int NoOfThumbProdImg = pdpPage.getNoOfThumbnailProdImages();
			
			Log.softAssertThat(NoOfThumbProdImg > 0,
					"Alternate images should be displayed!",
					"Alternate images is displayed!",
					"Alternate images is not displayed!", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgProductPrimaryImage", "productThumbnaillink", pdpPage), 
					"Alternate images should be displayed below the main product image!",
					"Alternate images is displayed below the main product image!",
					"Alternate images is not displayed below the main product image!", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// M6_FBB_DROP3_C22575
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M7_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String productID = TestData.get("prd_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser); 
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(productID);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			if(Utils.isDesktop()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodImageRecommendationDesktop","prodNameRecommendationDesktop","prodPriceRecommendationDesktop"), pdpPage), 
						"Attributes displayed for the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
						"Attributes displayed in the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
						"Attributes not displayed recommendations product tile", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodSalePriceRecommendationDesktop"), pdpPage) ||
						pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodStdPriceRecommendationDesktop"), pdpPage), 
						"Price Attributes displayed for the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price\"",
						"Price Attributes displayed in the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price.\"",
						"Price Attributes not displayed recommendations product tile", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodSalePriceRecommendationMobileTablet"), pdpPage) ||
						pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodStdPriceRecommendationMobileTablet"), pdpPage), 
						"Price Attributes displayed for the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price\"",
						"Price Attributes displayed in the recommendations product tile: \"Recommendation Standard Price\", \"Recommendation Sales Price.\"",
						"Price Attributes not displayed recommendations product tile", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodImageRecommendationMobileTablet","prodNameRecommendationMobileTablet","prodPriceRecommendationMobileTablet"), pdpPage), 
						"Attributes displayed for the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
						"Attributes displayed in the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
						"Attributes not displayed recommendations product tile", driver);
			}
			
			Log.softAssertThat(pdpPage.verifyRecommendationHaveCurrentProduct(productID), 
					"The recommended products should not include the product being viewed",
					"The recommended products is not included the product being viewed",
					"The recommended products is included the product being viewed", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// M7_FBB_DROP3_C22575
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M8_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String productID = TestData.get("prd_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser); 
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(productID);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("sectionReviewTab","sectionDetailsTab"), pdpPage), 
					"Check the review and detail sections are displayed",
					"The review and detail sections are displayed",
					"The review and detail sections are not displayed", driver);
			
			String[] star = pdpPage.getProductReviewStarHalfStar();
			
			if(star[1].contains("5") || !(star[0].contains("0"))) {
				Log.softAssertThat(star[1].contains("5") || !(star[0].contains("0")),
						"The product is already reviewed and star should displayed",
						"The product is already reviewed and star is "+star[0]+"."+star[1],
						"The product is not already reviewed", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("txtReviewCount"), pdpPage),
						"Check the review count is displaying in the page",
						"The review count is displaying in the page",
						"The review count is not displaying in the page", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionColorAttribute", "sectionTurnToFit", pdpPage), 
						"Product messaging should be displayed below the variation!",
						"Product messaging is displayed below the variation!",
						"Product messaging is not displayed below the variation!", driver);
				
			} else {
				Log.softAssertThat(pdpPage.verifyProductReview("BE THE FIRST TO WRITE A REVIEW"), 
						"Product is not reviewed and Be the first to write a review should displayed", 
						"Product is not reviewed and Be the first to write a review is displayed", 
						"Product is not reviewed and Be the first to write a review is not displayed", driver);
			}
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// M8_FBB_DROP3_C22575	
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M9_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String lvl1 = TestData.get("level-1");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser); 
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!",driver);
			
			PlpPage plpPage = homePage.headers.navigateTo(lvl1);
			Log.message(i++ + ". Navigated to PLP Page with Search Keyword", driver);

			int prdCount = plpPage.getProductTileCount();
			prdCount = prdCount>20 ? 20 : prdCount;
			
			String[] prodId = plpPage.getListOfProductId(prdCount);
	
			String[] url = plpPage.getProductUrl(prdCount);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prodId[0]);
			List<String> prdNames = new ArrayList<String>();
			prdNames.add(pdpPage.getProductName());
			Log.message(i++ + ". Navigated to PDP for Product :: " + prdNames.get(0));
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("recentlyViewedSection"), pdpPage),
					"Check the recently viewed section is not displaying when user searches first product",
					"The recently viewed section is not displaying when user searches first product",
					"The recently viewed section is displaying when user searches first product", driver);
	
			pdpPage = homePage.headers.navigateToPDP(prodId[1]);
			prdNames.add(pdpPage.getProductName());
			int size = pdpPage.getNoOfProdDisplayInRecentView();
	
			Log.softAssertThat(size == 1,
					"To check the recently viewed product didnot display the same product with different variant!",
					"The recently viewed product didnot display the same product with different variant",
					"The recently viewed product is displaying the same product with different variant!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "detailsReviewSection", "recentlyViewedSection", pdpPage),
					"To check the recently viewed section is displayed below the reviews and details section!",
					"The recently viewed section is displayed below the reviews and details section!",
					"The recently viewed section is not displayed below the reviews and details section!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("recentlyViewedProdPricing", "recentlyViewedProdImage", "recentlyViewedProdName"), pdpPage),
					"Check 'Recently Viewed' section have 'Product Name', 'Product price' and 'Product image'",
					"'Recently Viewed' section have 'Product Name', 'Product price' and 'Product image'",
					"'Recently Viewed' section is not having 'Product Name', 'Product price' and 'Product image'", driver);
	
			Log.softAssertThat(size > 0,
					"To check the recently viewed section is having visited product!",
					"The recently viewed section is having visited product!",
					"The recently viewed section is not having visited product!", driver);
	
			for (int w=2; w< url.length - 3; w++) {
				Utils.openNewTab(driver);
	
				List<String> handle = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(handle.get(handle.size() - 1));
				driver.get(url[w]);
	
				Utils.waitForPageLoad(driver);
			}
	
			pdpPage = homePage.headers.navigateToPDP(prodId[url.length - 1]);
	
			size = pdpPage.getNoOfProdDisplayInRecentView();
	
			Log.softAssertThat(size == 12,
					"To check the recently viewed section displaying the 12 new viewed product!",
					"The recently viewed section is displaying the 12 new viewed product",
					"The recently viewed section is not displaying the 12 new viewed product!", driver);
	
			if(Utils.isDesktop()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyListElementSize("recentlyViewedCurrentProd", 4, pdpPage),
						"To check the recently viewed section visible 4 product!",
						"The recently viewed section is visible 4 product",
						"The recently viewed section is not visible 4 product!", driver);
			}
			if(!Utils.isMobile()) {	
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("recentlyViewSectionNextArrow","recentlyViewSectionPrevArrow"), pdpPage),
						"Check the recently viewed section is having 'Next' and 'Prev' arrow",
						"The recently viewed section is having 'Next' and 'Prev' arrow",
						"The recently viewed section is not having 'Next' and 'Prev' arrow", driver);
			}
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// M9_FBB_DROP3_C22575	
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M10_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String username;
		String password = accountData.get("password_global");
		String Prd = TestData.get("prd_variation");
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser); 
		
		username = AccountUtils.generateEmail(driver);
		
		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
	
		int i = 1;
		try {
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(Prd);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.selectAllSwatchesForVariationProduct();
			
			pdpPage.addToWishlist();
			Log.message(i++ + ". Clicked on Add to Wishlist.", driver);
	
			WishlistLoginPage  wlLogin = new WishlistLoginPage(driver).get();
			Log.softAssertThat(wlLogin.elementLayer.verifyElementDisplayed(Arrays.asList("divSearchWishlist"), wlLogin), 
					"For a guest User, clicking the link will direct the User to the login page", 
					"For a guest User, clicking the link will direct the User to the login page", 
					"For a guest User, clicking the link will direct the User to the login page", driver);
	
			homePage = homePage.headers.navigateToHome();
			Log.message(i++ + ". Navigated to Home Page.", driver);
	
			homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
			
			pdpPage = homePage.headers.navigateToPDP(Prd);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.selectAllSwatchesForVariationProduct();
			
			pdpPage.addToWishlist();
			Log.message(i++ + ". Clicked on Add to Wishlist.", driver);
			
			WishListPage wlPage = new WishListPage(driver).get();
			
			Log.softAssertThat(!wlPage.elementLayer.verifyTextContains("wishListProductDetails", "automation@yopmail.com", wlPage), 
					"For a registered User, the Gift Card will directly be added to the User's wishlist without the fields 'From', 'To' and the Message Body", 
					"For a registered User, the Gift Card will directly be added to the User's wishlist without the fields 'From', 'To' and the Message Body", 
					"For a registered User, the Gift Card will directly be added to the User's wishlist without the fields 'From', 'To' and the Message Body", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveWishListProducts(driver);
			Log.endTestCase(driver);
		} // finally
	}// M10_FBB_DROP3_C22575	
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M11_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_saving-story");
	
		//Instantiate and Load Test Data
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PPD page!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtSavingStory"), pdpPage),					 
					"Savings story should be displayed", 
					"Savings story is displayed", 
					"Savings story is not displayed", driver);			
	
			Log.softAssertThat((pdpPage.getSavingsStoryDetails()),					 
					"Saving Story should contain 'upto', '%', 'off' and should not contain decimal ", 
					"Saving Story contains 'upto', '%', 'off' and does not contain decimal", 
					"Saving Story does not contain 'upto', '%', 'off' and contains decimal", driver);
	
			Log.softAssertThat((pdpPage.getSavingsStoryDetails()),					 
					"Message should be displayed when the saving percentage is greater than 10% ", 
					"Message is displayed when the saving percentage is greater than 10%", 
					"Message is not displayed when the saving percentage is greater than 10%", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M11_FBB_DROP3_C22575
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M12_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_promo-prd-level");
	
		//Instantiate and Load Web Driver
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			//For Promotion Call Out message
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP page!", driver);
	
			if(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtPromotionCalloutMsg"), pdpPage)) {
				//Step-1 & 3
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtPromotionCalloutMsg"), pdpPage),					 
						"Promotion callout message should be displayed", 
						"Promotion callout message is displayed", 
						"Promotion callout message is not displayed", driver);			
		
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblRegularPriceRange", "txtPromotionCalloutMsg"), pdpPage), 
						"Promotion Callout message should be displayed below the price!", 
						"Promotion Callout message is displayed below the price!", 
						"Promotion Callout message is not displayed below price!", driver);
		
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtSeeDetails"), pdpPage),					 
						"See Details link should be displayed", 
						"See Details link is displayed", 
						"See Details link is not displayed", driver);
		
				Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSeeDetails", "txtPromotionCalloutMsg", pdpPage),
						"See details link should be displayed to the right side of Promotional callout message",
						"See details link is displayed in the right side of Promotional callout message!",
						"See details link is not displayed in the right side of Promotional callout message", driver);
			}else{
				Log.failsoft("Product does not have Promotional Callout Message. Please verify manually.", driver);
			}
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M12_FBB_DROP3_C22575
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M13_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_clearance");
	
		//Instantiate and Load Web Driver
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PPD page!", driver);
	
			//Step-1 &4
			Log.message(i++ + ". Navigated to Pdp page!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtSavingStory"), pdpPage),					 
					"Clearance message should be displayed", 
					"Clearance message is displayed", 
					"Clearance message is not displayed", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblRegularPriceRange", "txtSavingStory"), pdpPage), 
					"Clearance message should be displayed below the price!", 
					"Clearance message is displayed below the price!", 
					"Clearance message is not displayed below price!", driver);
	
			Log.softAssertThat((pdpPage.getClearanceMessage()),					 
					"Text 'Clearance' should be displayed before the clearance message", 
					"Text 'Clearance' is displayed before the clearance message", 
					"Text 'Clearance' is not displayed before the clearance message", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M13_FBB_DROP3_C22575
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M14_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prodId = TestData.get("prd_review");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prodId);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("sectionReviewTab","sectionDetailsTab"), pdpPage), 
					"Check the review and detail sections are displayed",
					"The review and detail sections are displayed",
					"The review and detail sections are not displayed", driver);
	
			pdpPage.clickOnDetailsTab();
			Log.message("The detail message are changing for product to product, hence verifying only whether the details is displaying or not");
	
			if (pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtCustomerGalleryHeading"), pdpPage)) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divSocialIcons", "txtCustomerGalleryHeading", pdpPage), 
						"Check the Content Gallery displayed below the Share Icons",
						"The Content Gallery displayed below the Share Icons",
						"The Content Gallery is not displayed below the Share Icons", driver);
			} else {
				Log.failsoft("Customer Gallery is not displayed in the Product Detail page");
			}
			
			pdpPage.clickOnReviewTab();
			Log.message(i++ + ". Clicked on review tab", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("sectionReviewVisible"), pdpPage), 
					"Check the review section is opened",
					"The review section is opened",
					"The review section is not opened", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(
							Arrays.asList("lblReviewCount","txtAvgRatingsReview","txtAvgRatingBox","lnkReviewMyPost","lnkReviewMorePurchase"), pdpPage),
					"Check the review section has average rating, the number of reviews, Comfort and Service meter, overall sizes, recommend the product, best uses for the product, and links for Review More Purchases and My Posts.",
					"The review section has average rating, the number of reviews, Comfort and Service meter, overall sizes recommend the product, best uses for the product, and links for Review More Purchases and My Posts.",
					"Some components are missing in the review section", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("cmbSortReview","txtSearchReview"), pdpPage), 
					"Check the review section has Sort review combo box and Search review text box",
					"The review section has Sort review combo box and Search review text box",
					"The review section is not having Sort review combo box or Search review text box", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionReviewChart", "txtSearchReview", pdpPage),
					"Check the search review displayed below the review section",
					"The search review is displayed below the review section",
					"The search review is not displayed below the review section", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionReviewChart", "cmbSortReview", pdpPage),
					"Check the sort review displayed below the review section",
					"The sort review is displayed below the review section",
					"The sort review is not displayed below the review section", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtReviewAuthor"), pdpPage), 
					"Check all the review has review name",
					"The reviews has reviewer name",
					"The reviews is not having reviewer name", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtReviewTitle", "txtReviewCustomerStar"), pdpPage), 
					"Check the review section has Sort review combo box and Search review text box",
					"The review section has Sort review combo box and Search review text box",
					"The review section is not having Sort review combo box or Search review text box", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// M14_FBB_DROP3_C22575
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M15_FBB_DROP3_C22575(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String regularPriceProd = TestData.get("prd_single-price");
		String regularPriceRangeProd = TestData.get("prd_price-range");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Load Regular Price Product
			PdpPage pdpPage = homePage.headers.navigateToPDP(regularPriceProd);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			Log.message("<br><b><u>Regular Price Product</u></b>");
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblRegularPrice"), pdpPage)
					&& pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lblSalePrice","lblOriginalPriceInSalePrice"), pdpPage), 
					"For Regular Price Product, It should show only Original Price.", 
					"Original Price displayed!", 
					"Original Price Not displayed", driver);
	
			//Load Regular Price Range Product - Hold Due to TestData requirement
			pdpPage = homePage.headers.navigateToPDP(regularPriceRangeProd);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			Log.message("<br><b><u>Regular Price Range Product</u></b>");
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblRegularPriceRange"), pdpPage)
					&& pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lblSalePrice","lblOriginalPriceInSalePrice"), pdpPage), 
					"For Regular Price Range Product, It should show only Original Price.", 
					"Original Price displayed!", 
					"Original Price Not displayed", driver);
	
			Log.softAssertThat(pdpPage.getRegularPriceRange().contains("-") &&
					pdpPage.getRegularPriceRange().split("\\-")[0].contains("$") &&
					pdpPage.getRegularPriceRange().split("\\-")[1].contains("$"),
					"Orignial Price range should be displayed as $XX.XX - $XX.XX",
					"Orignial Price range displayed as $XX.XX - $XX.XX!",
					"Orignial Price range Not displayed as Expected.", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M15_FBB_DROP3_C22575
	
}// search