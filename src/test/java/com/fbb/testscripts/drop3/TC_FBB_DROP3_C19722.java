package com.fbb.testscripts.drop3;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19722 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19722(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String savingStoryPrd = TestData.get("prd_saving-story");
		String promotionPrd = TestData.get("prd_promo-prd-level");
		String clearancePrd = TestData.get("prd_clearance");
	
		//Instantiate and Load Web Driver
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(savingStoryPrd);
			Log.message(i++ + ". Navigated to PPD page!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtSavingStory"), pdpPage),
					"Savings story should be displayed", 
					"Savings story is displayed", 
					"Savings story is not displayed", driver);
	
			Log.softAssertThat((pdpPage.getSavingsStoryDetails()),
					"Saving Story should contain 'upto', '%', 'off' and should not contain decimal ", 
					"Saving Story contains 'upto', '%', 'off' and does not contain decimal", 
					"Saving Story does not contain 'upto', '%', 'off' and contains decimal", driver);
	
			Log.softAssertThat((pdpPage.getSavingsStoryDetails()),					 
					"Message should be displayed when the saving percentage is greater than 10% ", 
					"Message is displayed when the saving percentage is greater than 10%", 
					"Message is not displayed when the saving percentage is greater than 10%", driver);
	
			//Step-1 & 3
			pdpPage = homePage.headers.navigateToPDP(promotionPrd);
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtPromotionCalloutMsg"), pdpPage),					 
					"Promotion callout message should be displayed", 
					"Promotion callout message is displayed", 
					"Promotion callout message is not displayed", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblRegularPriceRange", "txtPromotionCalloutMsg"), pdpPage), 
					"Promotion Callout message should be displayed below the price!", 
					"Promotion Callout message is displayed below the price!", 
					"Promotion Callout message is not displayed below price!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtSeeDetails"), pdpPage),
					"See Details link should be displayed", 
					"See Details link is displayed", 
					"See Details link is not displayed", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSeeDetails", "txtPromotionCalloutMsg", pdpPage),
					"See details link should be displayed to the right side of Promotional callout message",
					"See details link is displayed in the right side of Promotional callout message!",
					"See details link is not displayed in the right side of Promotional callout message", driver);
	
			//Step-1 &4
			pdpPage = homePage.headers.navigateToPDP(clearancePrd);
			Log.message(i++ + ". Navigated to Pdp page!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtSavingStory"), pdpPage),					 
					"Clearance message should be displayed", 
					"Clearance message is displayed", 
					"Clearance message is not displayed", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblRegularPriceRange", "txtSavingStory"), pdpPage), 
					"Clearance message should be displayed below the price!", 
					"Clearance message is displayed below the price!", 
					"Clearance message is not displayed below price!", driver);
	
			Log.softAssertThat((pdpPage.getClearanceMessage()),					 
					"Text 'Clearance' should be displayed before the clearance message", 
					"Text 'Clearance' is displayed before the clearance message", 
					"Text 'Clearance' is not displayed before the clearance message", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_C19722
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP3_C19722(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_saving-story");
	
		//Instantiate and Load Test Data
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PPD page!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtSavingStory"), pdpPage),					 
					"Savings story should be displayed", 
					"Savings story is displayed", 
					"Savings story is not displayed", driver);			
	
			Log.softAssertThat((pdpPage.getSavingsStoryDetails()),					 
					"Saving Story should contain 'upto', '%', 'off' and should not contain decimal ", 
					"Saving Story contains 'upto', '%', 'off' and does not contain decimal", 
					"Saving Story does not contain 'upto', '%', 'off' and contains decimal", driver);
	
			Log.softAssertThat((pdpPage.getSavingsStoryDetails()),					 
					"Message should be displayed when the saving percentage is greater than 10% ", 
					"Message is displayed when the saving percentage is greater than 10%", 
					"Message is not displayed when the saving percentage is greater than 10%", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_C19722_1
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP3_C19722(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_promo-prd-level");
	
		//Instantiate and Load Web Driver
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			//For Promotion Call Out message
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PPD page!", driver);
	
			//Step-1 & 3
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtPromotionCalloutMsg"), pdpPage),					 
					"Promotion callout message should be displayed", 
					"Promotion callout message is displayed", 
					"Promotion callout message is not displayed", driver);			
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblRegularPriceRange", "txtPromotionCalloutMsg"), pdpPage), 
					"Promotion Callout message should be displayed below the price!", 
					"Promotion Callout message is displayed below the price!", 
					"Promotion Callout message is not displayed below price!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtSeeDetails"), pdpPage),					 
					"See Details link should be displayed", 
					"See Details link is displayed", 
					"See Details link is not displayed", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSeeDetails", "txtPromotionCalloutMsg", pdpPage),
					"See details link should be displayed to the right side of Promotional callout message",
					"See details link is displayed in the right side of Promotional callout message!",
					"See details link is not displayed in the right side of Promotional callout message", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_C19722_2
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP3_C19722(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_clearance");
	
		//Instantiate and Load Web Driver
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PPD page!", driver);
	
			//Step-1 &4
			Log.message(i++ + ". Navigated to Pdp page!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtSavingStory"), pdpPage),					 
					"Clearance message should be displayed", 
					"Clearance message is displayed", 
					"Clearance message is not displayed", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblRegularPriceRange", "txtSavingStory"), pdpPage), 
					"Clearance message should be displayed below the price!", 
					"Clearance message is displayed below the price!", 
					"Clearance message is not displayed below price!", driver);
	
			Log.softAssertThat((pdpPage.getClearanceMessage()),					 
					"Text 'Clearance' should be displayed before the clearance message", 
					"Text 'Clearance' is displayed before the clearance message", 
					"Text 'Clearance' is not displayed before the clearance message", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_C19722


}// search
