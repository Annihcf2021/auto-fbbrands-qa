package com.fbb.testscripts.drop3;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.EnlargeViewPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19709 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop", "tablet"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19709(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_enlarge");
	
		//Instantiate and Load web driver
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Load Regular Price Product
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page", driver);
	
			EnlargeViewPage enlargeViewPage  = pdpPage.clickOnEnlargeButton();
			Log.message(i++ + ". Clicked on Enlarge button on PDP");
	
			Log.softAssertThat(enlargeViewPage.elementLayer.verifyPageElements(Arrays.asList("mdlEnlargeWindow"), enlargeViewPage),					 
					"Enlarge Modal window should be displayed when clicking on enlarge button on PDP", 
					"Enlarge Modal window is displayed", 
					"Enlarge Modal window is not displayed", driver);
	
			//SM-6177 and SM-6015 -- Next and Prev carousels is removed as per this defect comments
			/*Log.softAssertThat(enlargeViewPage.elementLayer.verifyPageElements(Arrays.asList("prevImageArrowEnlarge","nextImageArrowEnlarge","btnEnlargeViewClose"), enlargeViewPage), 
					"Directional Arrow and close button should be displayed in the enlarge button on PDP", 
					"Directional Arrow and close button displayed in the enlarge button on PDP", 
					"Directional Arrow and close button not displayed in the enlarge button on PDP", driver);
	
			Log.softAssertThat(enlargeViewPage.checkThumbnailImageSelectedBasedOnNextButton(), 
					"Check the image is changing when clicking on the Next button in the enlarge view page", 
					"The image is changing when clicking on the Next button in the enlarge view page", 
					"The image is not changing when clicking on the Next button in the enlarge view page", driver);
	
			Log.softAssertThat(enlargeViewPage.checkThumbnailImageSelectedBasedOnPrevButton(), 
					"Check the image is changing when clicking on the Previous button in the enlarge view page", 
					"The image is changing when clicking on the Previous button in the enlarge view page", 
					"The image is not changing when clicking on the Previous button in the enlarge view page", driver);*/
	
			Log.softAssertThat(enlargeViewPage.elementLayer.verifyPageElements(Arrays.asList("txtProductName","colorSwatches","alternateImages"), enlargeViewPage),					 
					"'Product Name','Color Swatch', 'Alternate Images' should be displayed on enlarge view page", 
					"'Product Name','Color Swatch', 'Alternate Images' is displayed", 
					"'Product Name','Color Swatch', 'Alternate Images' is not displayed", driver);
	
			//4: Unable to automate the enlarged view of the image
			enlargeViewPage.mouseHoverOnPrimaryImage();
			Log.message(i++ + ". Mouse hover on Primary image in enlarge view modal", driver);
			if(Utils.isDesktop()){
				Log.softAssertThat(enlargeViewPage.elementLayer.verifyPageElements(Arrays.asList("zoomedImage"), enlargeViewPage),					 
						"Product Image should be zoomed", 
						"Product Image is zoomed", 
						"Product Image is not zoomed", driver);
			}
			else{
				Log.softAssertThat(enlargeViewPage.elementLayer.verifyPageElements(Arrays.asList("zoomedImageTab"), enlargeViewPage),					 
						"Product Image should be zoomed", 
						"Product Image is zoomed", 
						"Product Image is not zoomed", driver);
			}
			Log.softAssertThat(enlargeViewPage.elementLayer.verifyPageElements(Arrays.asList("txtProductName"), enlargeViewPage),					 
					"'Product Name' should be displayed on enlarge view page", 
					"'Product Name' is displayed", 
					"'Product Name' is not displayed", driver);
	
			//To get color swatch count
			int colorSwatchCount = enlargeViewPage.getcolorSwatchesCount();
	
			if(Utils.isDesktop())
			{		
				if(colorSwatchCount>8){
					int yLocationOfFirstColor = enlargeViewPage.getCurrentLocationOfPdtByIndex(0);
					int yLocationOfSixthColor = enlargeViewPage.getCurrentLocationOfPdtByIndex(8);
	
					Log.softAssertThat(yLocationOfFirstColor != yLocationOfSixthColor,					 
							"8 color swatches should be displayed in a row", 
							"8 color swatches is displayed in a row", 
							"8 color swatches is not displayed in a row", driver);
				}
			}else{
				if(colorSwatchCount>5){
					int yLocationOfFirstColor = enlargeViewPage.getCurrentLocationOfPdtByIndex(0);
					int yLocationOfSixthColor = enlargeViewPage.getCurrentLocationOfPdtByIndex(5);
	
					Log.softAssertThat(yLocationOfFirstColor != yLocationOfSixthColor,					 
							"5 color swatches should be displayed in a row", 
							"5 color swatches is displayed in a row", 
							"5 color swatches is not displayed in a row", driver);
				}				
			}
	
			enlargeViewPage.clickOnAlternateImage(2);
			Log.message(i++ + ". Clicked on second alternate image");
	
			Log.softAssertThat(enlargeViewPage.verifyMainAndAlternateImage(),					 
					"Alternate Image and Main Product image should be same", 
					"Alternate Image and Main Product image is same", 
					"Alternate Image and Main Product image is not same", driver);
	
			enlargeViewPage.clickOnAlternateImage(1);
			int altImageCount = enlargeViewPage.getAlternateImagesCount();
	
			if(Utils.isDesktop()){			
				if(altImageCount>5){
					int yLocationOfFirstImg = enlargeViewPage.getCurrentLocationOfAltImgByIndex(1);
					int yLocationOfSixthImg = enlargeViewPage.getCurrentLocationOfAltImgByIndex(5);
	
					Log.softAssertThat(yLocationOfFirstImg != yLocationOfSixthImg,					 
							altImageCount + " alternate images should be displayed in a row", 
							altImageCount + " alternate images is displayed in a row", 
							altImageCount + " alternate images is not displayed in a row", driver);
				}else {
					int yLocationOfFirstImg = enlargeViewPage.getCurrentLocationOfAltImgByIndex(0);
					int yLocationOfSixthImg = enlargeViewPage.getCurrentLocationOfAltImgByIndex(altImageCount-1);
	
					Log.softAssertThat(yLocationOfFirstImg == yLocationOfSixthImg,					 
							altImageCount + " alternate images should be displayed in a row", 
							altImageCount + " alternate images is displayed in a row", 
							altImageCount + " alternate images is not displayed in a row", driver);
				}
			}else{
				if(altImageCount>=4){
					int yLocationOfFirstImg = enlargeViewPage.getCurrentLocationOfAltImgByIndex(1);
					int yLocationOfSixthImg = enlargeViewPage.getCurrentLocationOfAltImgByIndex(4);
	
					Log.softAssertThat(yLocationOfFirstImg != yLocationOfSixthImg,					 
							altImageCount + " alternate images should be displayed in a row", 
							altImageCount + " alternate images is displayed in a row", 
							altImageCount + " alternate images is not displayed in a row", driver);
				}else {
					int yLocationOfFirstImg = enlargeViewPage.getCurrentLocationOfAltImgByIndex(0);
					int yLocationOfSixthImg = enlargeViewPage.getCurrentLocationOfAltImgByIndex(altImageCount-1);
	
					Log.softAssertThat(yLocationOfFirstImg == yLocationOfSixthImg,					 
							altImageCount + " alternate images should be displayed in a row", 
							altImageCount + " alternate images is displayed in a row", 
							altImageCount + " alternate images is not displayed in a row", driver);
				}
			}				
			
			searchKey = TestData.get("product_search_terms").split("\\|")[0];
			
			enlargeViewPage.closeTheEnlargeView();
			Log.message(i++ + ". Clicked on 'Close' on Enalrge View modal");
			
			SearchResultPage searchResultPg = homePage.headers.searchProductKeyword(searchKey);
			Log.message(i++ + ". Typed in the Search Field!", driver);
			pdpPage = searchResultPg.navigateToPDPAllSwatch();
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
	
			enlargeViewPage  = pdpPage.clickOnEnlargeButton();
			Log.message(i++ + ". Clicked on Enlarge button on PDP", driver);
	
			enlargeViewPage.selectColor(0);
			Log.message(i++ + ". First color swatch selected.", driver);
			
			Log.softAssertThat(enlargeViewPage.verifySwatchAndMainImage(),					 
					"Swatch Color selected and main image color should be same", 
					"Swatch Color selected and main image color is same", 
					"Swatch Color selected and main image color is not same", driver);
			
			//SM-6177 and SM-6015 -- Next and Prev carousels is removed as per this defect comments
			/*Log.softAssertThat(enlargeViewPage.getSelectedAlternateImageColor().contains(enlargeViewPage.getSelectedSwatchColor()),					 
					"Swatch Color selected and alternate image color should be same", 
					"Swatch Color selected and alternate image color is same", 
					"Swatch Color selected and alternate image color is not same", driver);*/
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_C19709	


}// search
