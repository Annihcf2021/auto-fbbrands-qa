package com.fbb.testscripts.drop3;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C21583 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;

	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C21583(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String videoProd = TestData.get("prd_video");
		String videoProdColor = TestData.get("prd_video_color");
		String badgeProd = TestData.get("prd_badge").split("\\|")[0];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(videoProd);
			Log.message(i++ + ". Navigated to PDP Page Page for Product :: " + pdpPage.getProductName() , driver);
			
			PlpPage plpPage = homePage.headers.navigateToLastSubCategory();
			Log.message(i++ + ". Navigated to PLP Page of last subcategpry.", driver);
	
			String imgTitle = plpPage.selectProductColorInPLP(videoProd, videoProdColor);
			Log.message(i++ + ". Selected '" + imgTitle + "' for the product.", driver);
	
			pdpPage = plpPage.navigateToPdp(videoProd);
			Log.message(i++ + ". Navigated to PDP Page Page for Product :: " + pdpPage.getProductName() , driver);
	
			//Step-1: Verify the functionality of breadcrumb
			String breadcrumbValue  = pdpPage.getBreadcrumbValue();
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("backArrowIcon"), pdpPage), 
					"Breadcrumb should contain '<' symbol", 
					"Breadcrumb contains '<' symbol", 
					"Breadcrumb does not contain '<' symbol", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyAttributeForElement("imgProductImageMobile", "src", imgTitle, pdpPage), 
					"Check the product image is displayed based on the color selected in the product detail page", 
					"The product image is displayed based on the color selected in the product detail page", 
					"The product image is not displayed based on the color selected in the product detail page", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnSlickDotActive", "videoIcon", pdpPage), 
					"Check the alternative image slick dot displaying right side of the video icon", 
					"The alternative image slick dot is displaying to the right side of the video icon", 
					"The alternative image slick dot is not displaying to the right side of the video icon", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblRegularPrice"), pdpPage), 
					"Check the product price is displaying in the pdp page", 
					"The product price is displaying in the pdp page", 
					"The product price is not displaying in the pdp page", driver);
	
			if(pdpPage.elementLayer.verifyPageElements(Arrays.asList("videoIcon"), pdpPage)) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "videoIcon", "lblRegularPrice", pdpPage), 
						"Check the video icon is displaying above the product price", 
						"The video icon is displaying above the product price",
						"The video icon is not displaying above the product price", driver);
			}
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionColorAttribute", "sectionSizeAttribute", pdpPage)
							|| pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionColorAttribute", "shoeSizeSwatches", pdpPage), 
					"Check the color attribute is displaying above the size attribute", 
					"The color attribute is displaying above the size attribute",
					"The color attribute is not displaying above the size attribute", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionSizeAttribute", "sectionTurnToFit", pdpPage)
							|| pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "shoeSizeSwatches", "turnToFitSection", pdpPage), 
					"Check the size attribute is displaying above the TurnToFit message", 
					"The size attribute is displaying above the TurnToFit message",
					"The size attribute is not displaying above the TurnToFit message", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divSocialIcons", "sectionRecommendationMobile", pdpPage), 
					"Check the social icons is displaying above the Recommendation section", 
					"The social icons is displaying above the Recommendation section",
					"The social icons is not displaying above the Recommendation section", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionRecommendationMobile", "sectionDetailsTab", pdpPage), 
					"Check the Recommendation section is displaying above the details section", 
					"The Recommendation section is displaying above the details section",
					"The Recommendation section is not displaying above the details section", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "sectionReviewTab", "sectionDetailsTab", pdpPage), 
					"Check Review tab should be displayed right side of the product details section", 
					"The Review tab is displaying right side of the product details section", 
					"The Review tab is not displaying right side of the product details section", driver);
	
			breadcrumbValue = breadcrumbValue.replace("BACK", "").trim();
	
			pdpPage.clickOnBreadCrumb(1);
			Log.message(i++ + ". Clicked on breadcrumb in PDP", driver);
	
			Log.softAssertThat(plpPage.getCategoryName().toLowerCase().trim().contains(breadcrumbValue.toLowerCase().trim()), 
					"Page should be redirected to the respective PLP", 
					"Page is redirected to the respective PLP", 
					"Page is not redirected to the respective PLP", driver);
	
			//Step-9
			pdpPage = homePage.headers.navigateToPDP(videoProd);
			Log.message(i++ + ". Navigated to PDP Page", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnAddToCart", "drpQty", pdpPage),
					"Qty drodown should be displayed to the left side of Add to bag button",
					"Qty drodown displayed in the left side of Add to bag button!",
					"Qty drodown not displayed in the left side of Add to bag button", driver);
	
			Log.message("Quantity ==="+pdpPage.getQuantity());
	
			Log.softAssertThat(pdpPage.getQuantity() == 1, 
					"Default value should be 1 in the quantity drop down field.", 
					"Default value is 1 in the quantity drop down field!", 
					"Default value is not 1 in the quantity drop down field.", driver);
	
			Log.softAssertThat(pdpPage.verifyQtyDrp(), 
					"Dropdown should display 1-10", 
					"Dropdown displayed 1-10", 
					"Dropdown not display 1-10", driver);
	
			Log.reference("Step 9 functionality is covered in the test case ID : C19683");
	
			//Step-2: Verify the functionality of Star Rating/Review - Hold
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("divReviewAndRating"), pdpPage), 
					"Check the product review is displaying in the PDP page", 
					"The product review is displaying in the PDP page", 
					"The product review is not displaying in the PDP page", driver);
	
			Log.reference("Step 2 functionality is covered in the test case id: C19675 Step 1 to 4");
	
			//Step-3: Verify the functionality of product image.
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductNameMobile", "imgProductImageMobile", pdpPage), 
					"Check the product name is displaying above the product image", 
					"The product name is displaying above the product image",
					"The product name is not displaying above the product image", driver);
	
			if(pdpPage.elementLayer.verifyPageElements(Arrays.asList("videoIcon"), pdpPage)) {
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("videoIcon"), pdpPage),
						"Video link button should be displayed",
						"Video link button is displayed", 
						"Video link button is not displayed", driver);
		
				pdpPage.clickVideoIcon();
				Log.message(i++ + ". Clicked on Video icon on PDP", driver);
		
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("videoOverlay"), pdpPage), 
						"Video Overlay should be displayed", 
						"Video Overlay is displayed", 
						"Video Overlay is not displayed", driver);
			}
	
			//Step - 5:  			  
			Log.reference("Step 6 Swipe alternate image functionality is not covered due to swipe limitation in selenium.");
	
			Log.message(i++ + ". Step 7 functionality is covered in the test case ID : C19695 -Step 1, 2");
	
			Log.message(i++ + ". Step 8 functionality is covered in the test case ID : C19728 -Step 1 to 24");
	
			Log.message(i++ + ". Step 9 functionality is covered in the test case ID : C19728 -Step 14");
	
	
			//Step-11 - Verify the functionality of Details Tab
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("divDetailsTabContent"), pdpPage), 
					"Product Details content should be displayed", 
					"Product Details content is displayed", 
					"Product Details content is not displayed", driver);
	
			Log.message(i++ + "Step 11 is Covered functionality in test case ID : C19689 -Step 1 to 4");
			Log.message(i++ + "Step 12 is Covered functionality in test case ID : C19671 -Step 8");
			Log.message(i++ + "Step 13 is Covered functionality in test case ID : C19675 -Step 1 to 4");
	
			pdpPage = homePage.headers.navigateToPDP(badgeProd);
			Log.message(i++ + ". Navigated to PDP Page Page for Product :: " + pdpPage.getProductName() , driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodBadgeMobile"), pdpPage), 
					"Check the Product badge is getting displayed in the product", 
					"The Product badge is displayed in the product", 
					"The Product badge is not displayed in the product", driver);
	
			pdpPage.clickOnAltImageSlickDot(2);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("divProductBadge", "display", "none", pdpPage), 
					"Product badge should not be displayed on alternate images.", 
					"The Product badge is not displayed in the alternative images", 
					"The Product badge is displayed in the alternative images", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_21583


}// search
