package com.fbb.testscripts.drop3;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19726 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "low", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19726(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("ps_special-product-set").split("\\|")[0];
		TestData.get("ps_special-product-set").split("\\|");
	
		// Get the web driver instance	
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to Pdp page!", driver);
	
			//19729 - Step 6
			Log.softAssertThat(pdpPage.getNumberOfProductsInProductSet() == pdpPage.getNumberOfProductsColorSwatchesSelected(),
					"Color should be pre-selected by default!",
					"Color is pre-selected by default!",
					"Color is not  pre-selected by default!", driver);
	
			Log.softAssertThat(pdpPage.getNumberOfProductsInProductSet() == pdpPage.getNumberOfProductsSizeSwatchesNotSelected(),
					"Size should not be pre-selected by default!",
					"Size is not pre-selected by default!",
					"Size is pre-selected by default!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtSpecialProductSetPrice"), pdpPage),
					"The Special product price should be displayed properly!",
					"The Special product price is displayed properly!",
					"The Special product price is not displayed properly!", driver);
			//12
			pdpPage.addToBagSpecialProductSet();
			Log.message(i++ + ". Clicked Add To Bag!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("mdlMiniCartOverLay"), pdpPage),
					"The minicart overlay should not be displayed!",
					"The minicart is not displayed!",
					"The minicart is displayed!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtSelectSizeError"), pdpPage),
					"The Please select size error message should be displayed!",
					"The Please select size error message is displayed!",
					"The Please select size error message is not displayed!", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_C19726
	
	@Test(groups = { "low", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP3_C19726(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("ps_special-product-set").split("\\|")[0];
		TestData.get("ps_special-product-set").split("\\|");
	
		// Get the web driver instance	
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to Pdp page!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("prodBreadCrumb"), pdpPage),
					"The breadcrumb should be displayed",
					"The breadcrumb is displayed",
					"The breadcrumb is not displayed", driver);
	
			homePage.headers.navigateToLastSubCategory();
			Log.message(i++ + ". Clicked on category in bread crumb!", driver);
	
			pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to Pdp page!", driver);
	
			Log.softAssertThat(pdpPage.verifyLastBreadcrumbTextNotClickable(),
					"The last breadcrumb text should not be clickable!",
					"The last breadcrumb text is not clickable!",
					"The last breadcrumb text is clickable!", driver);
	
	
			String color = null;
			if(BrandUtils.isBrand(Brand.rm)){
				if(Utils.getRunBrowser(driver).toLowerCase().contains("edge"))
					color = "rgb(0, 0, 0)";
				else
					color = "rgba(0, 0, 0, 1)";
			}else{
				if(Utils.getRunBrowser(driver).toLowerCase().contains("edge"))
					color = "rgb(170, 170, 170)";
				else
					color = "rgba(170, 170, 170, 1)";
			}
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementColor("bcLastCategoryName", color, pdpPage),
					"The last breadcrumb text should be displayed in different color!",
					"The last breadcrumb text is displayed in different color!",
					"The last breadcrumb text is not displayed in different color!", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_C19726

	@Test(groups = { "low", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP3_C19726(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("ps_special-product-set").split("\\|")[0];
		String badgeType = TestData.get("ps_special-product-set").split("\\|")[1];
	
		// Get the web driver instance	
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to Pdp page!", driver);
	
			if(Utils.isDesktop())
			{
				pdpPage.zoomProductImage();
				Log.message(i++ + ". Zoomed the Primary Product Image!", driver);
	
				//Log.failsoft("HTML element for zoom lens and zoom window container is duplicating in Quick Shop and PDP : PXSFCC-2951");
	
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divZoomLens", "divZoomWindowContainer"), pdpPage),
						"The primary image should be zoomed and displayed!",
						"The primary image is zoomed and displayed!",
						"The primary image is not zoomed and displayed!", driver);
	
				Log.softAssertThat(pdpPage.verifyZoomedImageDisplayedRightOfPrimaryProductImage(),
						"The zoomed image should be displayed right of primary image!",
						"The zoomed image is displayed right of primary image!",
						"The zoomed image is not displayed right of primary image!", driver);
	
				Log.softAssertThat(pdpPage.verifyTopOfZoomedImageAllignsWithTopOfProductImage(),
						"The top of zoomed image should allign with the top of primary product image!",
						"The top of zoomed image alligns with the top of primary product image!",
						"The top of zoomed image does not allign with the top of primary product image!", driver);
			}
	
			Log.softAssertThat(pdpPage.verifyBadgeType(badgeType),
					"The badge type should be displayed correctly",
					"The badge type is displayed correctly",
					"The badge type is not displayed correctly", driver);
	
			Log.message("Please refer Test case C19697 Step -2,3,4,5");
	
			//3
			Log.softAssertThat(pdpPage.verifyNewProdImageLoaded(1),
					"The selected thumbnail image should be displayed as primary product image!",
					"The selected thumbnail image is displayed as primary product image!",
					"The selected thumbnail image is not displayed as primary product image!", driver);
	
			//No longer a valid verification
			/*Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("productThumbnailSelected", "border-bottom", "rgb(51, 30, 83)", pdpPage), 
					"Alternate image pane selected image should be underlined", 
					"Alternate image pane selected image is underlined",
					"Alternate image pane selected image is not underlined",driver);
	
			pdpPage.clickOnSpecifiedAlternateImage(1);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("productThumbnailSelected", "border-bottom", "rgb(51, 30, 83)", pdpPage), 
					" While clicking the alternate image, underlines should be retained.", 
					" While clicking the alternate image, underlines is retained.",
					" While clicking the alternate image, underlines is not retained.",driver);*/
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_C19726
	
	@Test(groups = { "low", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP3_C19726(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("ps_special-product-set").split("\\|")[0];
		TestData.get("ps_special-product-set").split("\\|");
	
		// Get the web driver instance	
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to Pdp page!", driver);
	
			int NoOfThumbProdImg = pdpPage.getNoOfThumbnailProdImages();
			if(NoOfThumbProdImg > 4)
			{
	
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnPrevImageDisable","btnNextImageEnable"), pdpPage),
						"To check the arrows displaying for product image thumbnails.",
						"The 'Prev arrow', 'Next arrow' are displaying when the product have 5 or more thumbnail images",
						"The arrow is not displaying even the thumbnails are having 5 or more product images", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnPrevImageDisable"), pdpPage),
						"To check the previous arrow is disabled in the begining of image sequence.",
						"The 'Prev arrow' is displaying and disabled in the begining of image sequence.",
						"The 'Prev arrow' is not disabled", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnNextImageDisable"), pdpPage),
						"To check the next arrow is enabled in the begining of image sequence.",
						"The 'Next arrow' is displaying and enabled in the begining of image sequence.",
						"The 'Next arrow' is not enabled", driver);
	
	
				Log.softAssertThat(pdpPage.verifyNextLinkGreyedOut(),
						"The Next link should be greyed out!",
						"The Next link is greyed out!",
						"The Next link is not greyed out!", driver);
	
	
				Log.softAssertThat(pdpPage.verifyPreviousLinkGreyedOut(),
						"The Next link should be greyed out!",
						"The Next link is greyed out!",
						"The Next link is not greyed out!", driver);
			}
			else
				Log.failsoft("The product does not have more than 4 alternate images");
	
			pdpPage.scrollAlternateImageInSpecifiedDirection("Next");
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnNextImageDisable"), pdpPage),
					"To check the next arrow is disabled when the alternate image sequence reached the end.",
					"The 'Next arrow' is displaying and disabled in the end of image sequence.",
					"The 'Next arrow' is not disabled", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnPrevImageDisable"), pdpPage),
					"To check the Prev arrow is enabled at the end of image sequence.",
					"The 'Prev arrow' is displaying and enabled at the end of image sequence.",
					"The 'Prev arrow' is not enabled", driver);		
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_C19726
	
	@Test(groups = { "low", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M5_FBB_DROP3_C19726(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("ps_special-product-set").split("\\|")[0];
		TestData.get("ps_special-product-set").split("\\|");
	
		// Get the web driver instance	
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to Pdp page!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("divReviewAndRating", "txtSpecialProductSetNameDesktop", pdpPage),
					"The rating must be displayed above the product name!",
					"The rating is displayed above the product name!",
					"The rating is not displayed above the product name!", driver);
	
			if(Utils.isMobile()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divReviewAndRating", "imgProductPrimaryImageMobile", pdpPage),
						"The rating must be displayed to the above the product image!",
						"The rating is displayed to the above the product image!",
						"The rating is not displayed to the above the product image!", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divReviewAndRating", "imgProductPrimaryImage", pdpPage),
						"The rating must be displayed to the right of product image!",
						"The rating is displayed to the right of product image!",
						"The rating is not displayed to the right of product image!", driver);
			}
			
	
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkReviews", "imgRatingStars", pdpPage),
					"The rating must be displayed to the left of number of reviews text!",
					"The rating is displayed to the left of number of reviews text!",
					"The rating is not displayed to the left of number of reviews text - SM-1483!", driver);
	
			String star = pdpPage.getProductReviewStar();
			String halfstar = pdpPage.getProductReviewStar();
			System.out.println("star value "+ star);
	
			if(!halfstar.contains("5"))	{
				if(!star.contains("0")) {
					Log.softAssertThat(!star.contains("0"),
							"The product is already reviewed and star should displayed",
							"The product is already reviewed and star is "+star,
							"The product is not already reviewed"+ star, driver);
				} else {
					Log.softAssertThat(pdpPage.verifyProductReview("BE THE FIRST TO WRITE A REVIEW"), 
							"Product is not review and Be the first to write a review should displayed", 
							"Product is not review and Be the first to write a review is displayed", 
							"Product is not review and Be the first to write a review is not displayed", driver);
				}
			} else {
				Log.softAssertThat(halfstar.contains("5"),
						"The product is already reviewed and star should displayed",
						"The product is already reviewed and star is "+star+"."+halfstar,
						"The product is not already reviewed "+star+"."+halfstar, driver);
			}
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkDetailsTab"), pdpPage),
					"The Details tab should be open by default!",
					"The Details tab is open by default!",
					"The Details tab is not open by default!", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkReviewsTab"), pdpPage),
					"The Reviews tab should be displayed!",
					"The Reviews tab is displayed!",
					"The Reviews tab is not displayed!", driver);
	
			pdpPage.clickReviewsTab();
			Log.message(i++ + ". Clicked Review tab!", driver);
	
	
			Log.softAssertThat(pdpPage.verifyReviewsTabOpened(),
					"The Reviews tab should be opened!",
					"The Reviews tab is opened!",
					"The Reviews tab is not opened!", driver);
	
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("lnkReviewsTab", "divDetailsContent", pdpPage),
					"The Reviews tab should be displayed above the content!",
					"The Reviews tab is displayed above the content!",
					"The Reviews tab is not displayed above the content!", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_C19726
	
	@Test(groups = { "low", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M6_FBB_DROP3_C19726(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("ps_special-product-set").split("\\|")[0];
		TestData.get("ps_special-product-set").split("\\|");
	
		// Get the web driver instance	
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to Pdp page!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblSPSPriceSection", "productPromotion", pdpPage), 
					"Promotions should be displayed below pricing in special product set detail page.", 
					"Promotions is displayed below pricing in special product set detail page.", 
					"Promotions is not displayed below pricing in special product set detail page.", driver);
	
			Log.message("Promotions are covered in another test case, C19722 Step -1, Step -2, Step -3 more details.");
			//Log.failsoft("Product Rating and review module is temporarily down for maintenance message is displayed : PXSFCC-2086");
	
			//5 - Product Recommendation not yet integrated
			//Log.failsoft("Step 5 - Product Recommendation not yet integrated : PXSFCC-356");
	
			//Log.softAssertThat(pdpPage.verifyProductPriceDisplayed(),
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblSPSPriceSection"), pdpPage),
					"The price for the special product set should be displayed",
					"The price for the special product set is displayed",
					"The price for the special product set is not displayed", driver);
			//6
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productReviewStars", "productSetName", pdpPage), 
					"Product set name should be displayed below the Product rating", 
					"Product set name is be displayed below the Product rating", 
					"Product set name is not be displayed below the Product rating", driver);
	
			//7 - Promotions
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtSavingStory"), pdpPage),
					"The savings story should be displayed!",
					"The savings story is displayed!",
					"The savings story is not displayed!", driver);
	
			//9
			if(pdpPage.isSizeChartApplicable()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkSizechart"), pdpPage),
						"The Size Chart link should be displayed!",
						"The Size Chart link is displayed!",
						"The Size Chart link is not displayed!", driver);
	
				Log.softAssertThat(pdpPage.verifySizeChartLinkRightOfSizeAttribute(1),
						"The Size Chart link should be displayed right side of size attribute!",
						"The Size Chart link is displayed right side of size attribute",
						"The Size Chart link is not displayed right side of size attribute", driver);
	
				pdpPage.clickSizeChartLink();
				Log.message(i++ + ". Clicked on Size Chart link!", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("modalSizechart"), pdpPage),
						"The Size Chart modal should be displayed!",
						"The Size Chart modal is displayed!",
						"The Size Chart modal is not displayed!", driver);
	
				pdpPage.closeSizeChartModalPopup();
				Log.message(i++ + ". Clicked on close button in Size Chart modal!", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("modalSizechart"), pdpPage),
						"The Size Chart modal should be closed!",
						"The Size Chart modal is closed!",
						"The Size Chart modal is not closed!", driver);
			}else {
				Log.failsoft("Test Data not applicable for Size Chart validations.");
			}
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_C19726
	
	@Test(groups = { "low", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M7_FBB_DROP3_C19726(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("ps_special-product-set").split("\\|")[0];
		TestData.get("ps_special-product-set").split("\\|");
	
		// Get the web driver instance	
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to Pdp page!", driver);
	
			pdpPage.selectColorSwatchSpecialProductSet(1, 0);
			Log.message(i++ + ". Selected a color swatch!", driver);
	
			pdpPage.selectSizeSwatchSpecialProductSet(1, 0);
			Log.message(i++ + ". Selected a size swatch!", driver);
	
			pdpPage.selectSingleProductComponentColorBasedOnIndex(2, 2);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("swatchSizeOutOfStock"), pdpPage),
					"The size should be out of stock!",
					"The size is out of stock!",
					"The size is not out of stock!", driver);
	
			Log.softAssertThat(pdpPage.addToCartButtonDisabledState().equalsIgnoreCase("true"),
					"The select size error message should be displayed!",
					"The select size error message is displayed!",
					"The select size error message is not displayed!", driver);
	
			pdpPage.selectColorSwatchSpecialProductSet(2, 0);
			Log.message(i++ + ". Selected a color swatch!", driver);
	
			pdpPage.selectSizeSwatchSpecialProductSet(2, 0);
			Log.message(i++ + ". Selected a size swatch!", driver);
	
			pdpPage.selectColorSwatchSpecialProductSet(3, 0);
			Log.message(i++ + ". Selected a color swatch!", driver);
	
			pdpPage.selectSizeSwatchSpecialProductSet(3, 0);
			Log.message(i++ + ". Selected a size swatch!", driver);
	
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtSpecialProductSetInventory"), pdpPage),
					"The inventory message should be displayed!",
					"The inventory message is displayed!",
					"The inventory message is not displayed!", driver);
	
	
			Log.softAssertThat(pdpPage.verifySelectSizeErrorForSpecialProductSet() == false,
					"The select size error message should not be displayed!",
					"The select size error message is not displayed!",
					"The select size error message is displayed!", driver);
	
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlMiniCartOverLay"), pdpPage),
					"The minicart overlay should be displayed!",
					"The minicart is be displayed!",
					"The minicart is not displayed!", driver);
	
	
			Log.softAssertThat(pdpPage.verifyVerticalAllignmentOfChildProductsInCart(),
					"The child products should be displayed one below the other in the cart",
					"The child products are displayed one below the other in the cart",
					"The child products are not displayed one below the other in the cart", driver);
	
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". closed minicart overlay!", driver);
	
			//12
	
			Log.softAssertThat(pdpPage.getSelectedQuantityValue().equals("1"),
					"The default value of Quantity dropdown should be 1!",
					"The default value of Quantity dropdown is 1!",
					"The default value of Quantity dropdown is not 1!", driver);
	
	
			Log.softAssertThat(pdpPage.verifyAddToBagButtonIsRightOfQuantityDropdown(),
					"The Quantity dropdown should be displayed to the left of Add To Bag!",
					"The Quantity dropdown is displayed to the left of Add To Bag!",
					"The Quantity dropdown is not displayed to the left of Add To Bag!", driver);
	
	
			Log.softAssertThat(pdpPage.verifyQtyDrp(),
					"The Quantity dropdown should display values from 1 to 10!",
					"The Quantity dropdown displays values from 1 to 10!",
					"The Quantity dropdown does not display values from 1 to 10!", driver);
	
			String value = pdpPage.selectQuantity(5);
			Log.message(i++ + ". Selected Quantity!", driver);
	
	
			Log.softAssertThat(pdpPage.getSelectedQuantityValue().equals(value),
					"The selected value of Quantity dropdown should be displayed!",
					"The selected value of Quantity dropdown is displayed!",
					"The selected value of Quantity dropdown is not displayed!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtSpecialProductSetInventory", "btnAddToCartSplProdset", pdpPage), 
					"Add to Bag button should be displayed below the inventory state", 
					"Add to Bag button is displayed below the inventory state", 
					"Add to Bag button is not displayed below the inventory state", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_C19726
	
	@Test(groups = { "low", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M8_FBB_DROP3_C19726(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("ps_special-product-set").split("\\|")[0];
	
		// Get the web driver instance	
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to Pdp page!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblSPSPriceSection", "txtSavingStory", pdpPage), 
					"Savings story should be displayed below the pricing.", 
					"Savings story is displayed below the pricing.", 
					"Savings story is not displayed below the pricing.", driver);
			//16
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtShippingAndReturnInfo"), pdpPage),
					"The Shipping & Returns section should be open by default!",
					"The Shipping & Returns section is open by default!",
					"The Shipping & Returns section is not open by default!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkMoreInfo", "lnkClickHere"), pdpPage),
					"The More Info and click Here link should be displayed!",
					"The More Info and click Here link is displayed!",
					"The More Info and click Here link is not displayed!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkClickHere", "lnkMoreInfo", pdpPage),
					"The More Info should be displayed to the right of click Here link!",
					"The More Info is displayed to the right of click Here link!",
					"The More Info is not displayed to the right of click Here link!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("txtShippingInfo"), pdpPage),
					"The full shipping info should not be displayed!",
					"The full shipping info is not displayed!",
					"The full shipping info is displayed!", driver);
	
			pdpPage.selectClickHereInShippingSection();
			Log.message(i++ + ". Clicked for more info!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtShippingInfo"), pdpPage),
					"The full shipping info should be displayed!",
					"The full shipping info is displayed!",
					"The full shipping info is not displayed!", driver);
	
			pdpPage.clickCloseInShippingSection();
			Log.message(i++ + ". Collapsed more info!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("divSocialIcons"), pdpPage),
					"The Social Icons should be displayed!",
					"The Social Icons are displayed!",
					"The Social Icons are not displayed!", driver);
	
			if(Utils.getRunBrowser(driver).toLowerCase().contains("edge")==false){
				Log.softAssertThat(pdpPage.verifyFacebookShare(),
						"Facebook page should be opened!",
						"Facebook page is opened!",
						"Facebook page is not opened!", driver);
	
	
				Log.softAssertThat(pdpPage.verifyPinterestShare(),
						"Pinterest page should be opened!",
						"Pinterest page is opened!",
						"Pinterest page is not opened!", driver);
	
	
				Log.softAssertThat(pdpPage.verifyTwitterShare(),
						"Twitter page should be opened!",
						"Twitter page is opened!",
						"Twitter page is not opened!", driver);
			}
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_C19726

}// search
