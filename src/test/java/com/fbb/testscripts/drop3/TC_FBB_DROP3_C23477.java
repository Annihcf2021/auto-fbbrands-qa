package com.fbb.testscripts.drop3;
import com.fbb.reusablecomponents.TestData;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C23477 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C23477(String browser) throws Exception {
		Log.testCaseInfo();
		
		//Load Test Data
		String prodId = TestData.get("prd_reviewed");
		// Get the web driver instance
	
		//Get WebDriver Instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prodId);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			pdpPage.clickOnNoOfReviews();
			Log.message(i++ + ". Clicked on review count", driver);
			
			Log.softAssertThat(pdpPage.checkReviewTabIsSelected(), 
					"Check the review tab is selected when clicking the number of review.", 
					"The review tab is selected when clicking the number of review.", 
					"The review tab is not selected when clicking the number of review.", driver);
			
			Log.reference("Other steps are covered in the test case C23363.");
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}


}// search
