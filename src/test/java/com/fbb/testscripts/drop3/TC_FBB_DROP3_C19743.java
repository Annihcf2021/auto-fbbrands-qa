package com.fbb.testscripts.drop3;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19743 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;


	@Test( groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19743(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_monogram");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.redirectToPDP(searchKey, driver);
			Log.message(i++ + ". Navigated to '" + pdpPage.getProductName() + "' PDP page.", driver);
	
			//1
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtMonogrammingTitle", "lnkMonogrammingTips"), pdpPage),
					"The Monogramming options should be displayed!",
					"The Monogramming options are displayed!",
					"The Monogramming options are not displayed!", driver);
	
			Log.softAssertThat(pdpPage.verifyMonogrammingCheckboxDisplayed(),
					"The Monogramming checkbox should be displayed!",
					"The Monogramming checkbox are displayed!",
					"The Monogramming checkbox are not displayed!", driver);
			//2
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("tblSizeSwatch", "divMonogrammingOptions", pdpPage),
					"The Monogramming options should be displayed below the Size Swatch attribute!",
					"The Monogramming options is displayed below the Size Swatch attribute!",
					"The Monogramming options is not displayed below the Size Swatch attribute!", driver);
	
			//3
			pdpPage.selectSize();
			Log.message(i++ + ". Selected Size!", driver);
			
			pdpPage.selectColor();
			Log.message(i++ + ". Selected Color!", driver);
	
			Log.softAssertThat(pdpPage.verifyMonogrammingTitle(),
					"The Monogramming title should be displayed correctly!",
					"The Monogramming title is displayed correctly!",
					"The Monogramming title is not displayed correctly!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("txtMonogrammingTitle", "chkEnableMonogramming", pdpPage),
					"The Monogramming checkbox should be displayed below the Monogramming title!",
					"The Monogramming checkbox is displayed below the Monogramming title!",
					"The Monogramming checkbox is not displayed below the Monogramming title!", driver);
	
			Log.softAssertThat(pdpPage.verifyMonogrammingCheckboxSelected() == false,
					"The Monogramming checkbox should be unchecked by default!",
					"The Monogramming checkbox is unchecked by default!",
					"The Monogramming checkbox is not unchecked by default!", driver);
	
			pdpPage.clickOnMonogrammingCheckbox("enable");
			Log.message(i++ + ". The Monogramming checkbox is checked!", driver);
	
			Log.softAssertThat(pdpPage.verifyMonogrammingOptionsDrawerDisplayed(),
					"The Monogramming options should be displayed!",
					"The Monogramming options are displayed!",
					"The Monogramming options are not displayed!", driver);
	
			Log.softAssertThat(pdpPage.verifyMonogrammingOptionsDrawerDisplayed() == true,
					"The Monogramming options drawer should be displayed!",
					"The Monogramming options drawer is displayed!",
					"The Monogramming options drawer is not displayed!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver,"chkEnableMonogramming", "drpFontValue", pdpPage),
					"The Font select dropdown should be displayed below the Monogramming options checkbox!",
					"The Font select dropdown is displayed below the Monogramming options checkbox!",
					"The Font select dropdown is not displayed below the Monogramming options checkbox!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver,"chkEnableMonogramming", "drpLocationValue", pdpPage),
					"The Location select dropdown should be displayed below the Monogramming options checkbox!",
					"The Location select dropdown is displayed below the Monogramming options checkbox!",
					"The Location select dropdown is not displayed below the Monogramming options checkbox!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver,"chkEnableMonogramming", "drpColorValue", pdpPage),
					"The Color select dropdown should be displayed below the Monogramming options checkbox!",
					"The Color select dropdown is displayed below the Monogramming options checkbox!",
					"The Color select dropdown is not displayed below the Monogramming options checkbox!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver,"chkEnableMonogramming", "textField", pdpPage),
					"The Charcters Left textbox should be displayed below the Monogramming options checkbox!",
					"The Charcters Left textbox is displayed below the Monogramming options checkbox!",
					"The Charcters Left textbox is not displayed below the Monogramming options checkbox!", driver);
	
			/*Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("chkEnableMonogramming", "txtMonogramPreview", pdpPage),
					"The Monogramming Preview should be displayed below the Monogramming options checkbox!",
					"The Monogramming Preview is displayed below the Monogramming options checkbox!",
					"The Monogramming Preview is not displayed below the Monogramming options checkbox!", driver);*/
			Log.reference("Text Preview is not availble.");
			//4
			Log.softAssertThat(pdpPage.verifyMessagingDisplayedRightOfMonogrammingCheckbox(),
					"The Monogramming messaging should be displayed to the right of Monogramming options checkbox!",
					"The Monogramming messaging is displayed to the right of Monogramming options checkbox!",
					"The Monogramming messaging is not displayed to the right of Monogramming options checkbox!", driver);
			//5
			Log.softAssertThat(pdpPage.verifyTipsDisplayedRightOfMonogrammingTitle(),
					"The Tips link should be displayed to the right of Monogramming Title!",
					"The Tips link is displayed to the right of Monogramming Title!",
					"The Tips link is not displayed to the right of Monogramming Title!", driver);
	
			pdpPage.clickOnTipsInMonogrammingSection();
			Log.message(i++ + ". Clicked on the Tips link in the Monogramming section!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("flytTips"), pdpPage),
					"The Tips flyout should be displayed!",
					"The Tips flyout is displayed!",
					"The Tips flyout is not displayed!", driver);
	
			pdpPage.clickOnTipsFlyoutCloseButton();
			Log.message(i++ + ". Clicked on the Close button on the Tips flyout!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("flytTips"), pdpPage),
					"The Tips flyout should be closed!",
					"The Tips flyout is closed!",
					"The Tips flyout is not closed!", driver);
	
			pdpPage.clickOnTipsInMonogrammingSection();
			Log.message(i++ + ". Clicked on the Tips link in the Monogramming section!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("flytTips"), pdpPage),
					"The Tips flyout should be displayed!",
					"The Tips flyout is displayed!",
					"The Tips flyout is not displayed!", driver);
	
			pdpPage.closeFlyTipsByClickingOutSide();
			Log.message(i++ + ". Clicked outside the Tips flyout!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("flytTips"), pdpPage),
					"The Tips flyout should be closed!",
					"The Tips flyout is closed!",
					"The Tips flyout is not closed!", driver);
	
			if(!pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("flytTips"), pdpPage))
				pdpPage.clickOnTipsFlyoutCloseButton();
			//6
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on the Add To Bag button!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("errMonogTextField"), pdpPage),
					"The Error Message should be displayed!",
					"The Error Message is displayed",
					"The Error Message is not displayed", driver);
	
			pdpPage.clickOnMonogrammingCheckbox("disable");
			Log.message(i++ + ". The Monogramming checkbox is unchecked!", driver);
	
			Log.softAssertThat(pdpPage.verifyMonogrammingOptionsDrawerDisplayed() == false,
					"The Monogramming options drawer should not be displayed!",
					"The Monogramming options drawer is not displayed!",
					"The Monogramming options drawer is displayed!", driver);
	
	
			//7 - (6b)
			pdpPage.clickOnMonogrammingCheckbox("enable");
			Log.message(i++ + ". The Monogramming checkbox is checked!", driver);
	
			//pdpPage.selectMonogrammingFontValue(1);
			Log.message(i++ + ". The Monogramming Font value is selected!", driver);
	
			//pdpPage.selectMonogrammingLocationValue(1);
			Log.message(i++ + ". The Monogramming Location value is selected!", driver);
	
			//8
			int maxCount = Integer.parseInt(pdpPage.getMaximumCharacterCount());
	
			Log.softAssertThat(pdpPage.getActualCharacterCount().equals(pdpPage.getMaximumCharacterCount()),
					"The initial character count should be equal to max character count!",
					"The initial character count is equal to max character count!",
					"The initial character count is not equal to max character count!", driver);
	
			pdpPage.typeInTextFieldFormonogramming(maxCount+10);
			Log.message(i++ + ". Typed the value in to the Text field!", driver);
	
			Log.softAssertThat(pdpPage.getActualCharacterCount().equals("0"),
					"The number of characters typed must not exceed the maximum character count!",
					"The number of characters typed does not exceed the maximum character count!",
					"The number of characters typed exceeds the maximum character count!", driver);
	
			Log.softAssertThat(pdpPage.getNumberOfCharactersInTextField() == maxCount,
					"The number of characters must not exceed the maximum character count!",
					"The number of characters does not exceed the maximum character count!",
					"The number of characters exceeds the maximum character count!", driver);
			//9
			/*Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("textField", "txtMonogramPreview", pdpPage),
					"The Monogramming Preview should be displayed below the text box!",
					"The Monogramming Preview is displayed below the text box!",
					"The Monogramming Preview is not displayed below the text box!", driver);
	
			Log.softAssertThat(pdpPage.getPreviewText().equalsIgnoreCase(pdpPage.getMonogrammingTextFieldValue()),
					"The text displayed in preview field should be same as entered in text field!",
					"The text displayed in preview field is same as entered in text field!",
					"The text displayed in preview field is not same as entered in text field!", driver);*/
	
			//10
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtMonogramDisclaimer"), pdpPage),
					"Check the Monogramming disclaimer is displaying in the page!",
					"The Monogramming disclaimer is displaying in the page!",
					"The Monogramming disclaimer is not displaying in the page!", driver);
	
			/*Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("txtMonogramPreview", "txtMonogramDisclaimer", pdpPage),
					"The Monogramming Preview should be displayed above the disclaimer!",
					"The Monogramming Preview is displayed above the disclaimer!",
					"The Monogramming Preview is not displayed above the disclaimer!", driver);
	
	
			pdpPage.clickOnMonogrammingCheckbox("enable");
			Log.message(i++ + ". The Monogramming checkbox is checked!", driver);
	
			pdpPage.clearMonogramTextField();
	
			pdpPage.enterTextInMonogramFirstTextBox("Hello");
			Log.message(i++ + ". Typed the value in the first Text field!", driver);
	
			//There is no second monogram textbox
			pdpPage.enterTextInMonogramSecondTextBox("Hi");
			Log.message(i++ + ". Typed the value in the second Text field!", driver);
	
			Log.softAssertThat(pdpPage.checkLocationChangingInMonogramPreview(),
					"Check the location change is applying in the monogram preview!",
					"The location change is applying in the monogram preview!",
					"The location change is not applying in the monogram preview!", driver);
	
			Log.softAssertThat(pdpPage.checkFontChangingInMonogramPreview(),
					"Check the font change is applying in the monogram preview!",
					"The font change is applying in the monogram preview!",
					"The font change is not applying in the monogram preview!", driver);
	
			Log.softAssertThat(pdpPage.checkColorChangingInMonogramPreview(),
					"Check the color change is applying in the monogram preview!",
					"The color change is applying in the monogram preview!",
					"The color change is not applying in the monogram preview!", driver);*/
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP2_C19743


}// search
