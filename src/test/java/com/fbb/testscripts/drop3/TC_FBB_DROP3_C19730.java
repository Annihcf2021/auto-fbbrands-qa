package com.fbb.testscripts.drop3;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.MiniCartPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.CollectionUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19730 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19730(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String productID1 = TestData.get("prd_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(productID1);
			Log.message(i++ + ". Navigated to PdpPage for Product :: " + pdpPage.getProductName(), driver);
	
			Log.message(i++ + ". Product Type :: " + pdpPage.getProductType());

			pdpPage.selectColor();
			pdpPage.selectSize();
	
			HashMap<String, String> productInfo = pdpPage.getProductInfo();
			Log.message(i++ + ". Product Information :: " + productInfo.toString());
			//Step-1: Verify Add to Bag overlay is opened.
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on 'Add To Bag' Button", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("mdlMiniCartOverLay"), pdpPage), 
					"Add to Bag overlay should be displayed.",
					"Add to Bag overlay displayed!",
					"Add to Bag overlay not displayed.", driver);
	
			//Step-11: ER-4: If the customer does not have any items in their cart, then items in shopping bag= number of items added
			Log.softAssertThat(pdpPage.getTotalItemCountInMCOverlay().equals(pdpPage.getTotalProductQty()), 
					"If the customer does not have any items in their cart, then items in shopping bag= number of items added", 
					"Items in shopping bag equal to number of items added", 
					"Items in shopping bag not equal to number of items added", driver);
	
			//Step-2: Verify the Item added message in add to bag overlay page
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("lblMiniCartOverlayTitle", "text-align", "center", pdpPage) && 
					pdpPage.elementLayer.verifyInsideElementAlligned("lblMiniCartOverlayTitle", "mdlMiniCartOverLay", "top", pdpPage),
					"Item Added Message should be displayed to the top of the Add to Bag overlay centrally aligned",
					"Item Added Message displayed to the top of the Add to Bag overlay centrally aligned",
					"Item Added Message not displayed to the top of the Add to Bag overlay or centrally aligned", driver);
	
			//Property File Content Verification
	
			//Step-3: Verify the Close Modal Options​ in the add to bag overlay page
			Log.message(i++ + ". By clicking in three ways, Minicart Overlay should be closed.");
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Clicked on Close button in Minicart Overlay.");
	
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("mdlMiniCartOverLay", "display", "none", pdpPage),
					"Mini cart Overlay should be closed.",
					"Mini cart Overlay closed successfully!",
					"Mini cart Overlay not closed.", driver);
	
			//PXSFCC-1224
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on 'Add To Bag' Button", driver);	
	
			pdpPage.clickOnEmptySpaceInABTOverlay();
			Log.message(i++ + ". Clicked on Empty space on the screen!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("mdlMiniCartOverLay", "display", "none", pdpPage),
					"Mini cart Overlay should be closed.",
					"Mini cart Overlay closed successfully!",
					"Mini cart Overlay not closed.", driver);
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on 'Add To Bag' Button", driver);	
	
			pdpPage.clickOnContinueShoppingInMCOverlay();
			Log.message(i++ + ". Clicked on Continue Shopping in Mini cart Overlay!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("mdlMiniCartOverLay", "display", "none", pdpPage),
					"Mini cart Overlay should be closed.",
					"Mini cart Overlay closed successfully!",
					"Mini cart Overlay not closed.", driver);
	
			ShoppingBagPage shoppingBag = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ +". Navigated to Shopping bag Page!", driver);
	
			shoppingBag.removeAllItemsFromCart();
			Log.message(i++ + ". Removed All Items from Cart", driver);
	
			pdpPage = homePage.headers.navigateToPDP(productID1);
			Log.message(i++ + ". Navigated to PDP page for product :: " + pdpPage.getProductName());
	
			pdpPage.selectSize();
			pdpPage.selectColor();
	
			productInfo = pdpPage.getProductInfo();
			//Step-4: Verify the product image in add to bag overlay page
			pdpPage.clickAddProductToBag();	
			Log.message(i++ + ". Clicked on 'Add To Bag' Button", driver);	
	
			String mainImgSrc = pdpPage.getPrimaryImgSource();
			mainImgSrc = mainImgSrc.split("\\/")[mainImgSrc.split("\\/").length-1].split("\\?")[0];
			String miniCartOverLayImgSrc = pdpPage.getMiniCartOverLayImgSource();
			miniCartOverLayImgSrc = miniCartOverLayImgSrc.split("\\/")[miniCartOverLayImgSrc.split("\\/").length-1];
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "titleShoppingBagPopUp", "imgPrimaryImageInMCOverLay", pdpPage),
					"Main image of the selected color variation should be displayed below the item added message.",
					"Main image of the selected color variation displayed below the item added message!",
					"Main image of the selected color variation not displayed below the item added message.", driver);
	
			//Step-5: Verify the product name in add to bag overlay page
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkProductNameInMCOverLay", "imgPrimaryImageInMCOverLay", pdpPage), 
					"Product name should be displayed right side of the product image.",
					"Product name displayed right side of the product image.",
					"Product name not displayed right side of the product image.", driver);
	
			//Step-6: Verify the attributes in add to bag overlay page
			HashMap<String, String> prdDataInMCOverlay = pdpPage.getProductDetailsFromMCOverLay();
			List<String> dataToBeVerified =  Arrays.asList("lblProductColorInMCOverLay","lblProductSizeInMCOverLay","lblProductQtyInMCOverLay","lblProductAvailInMCOverLay","lblProductPriceInMCOverLay","lblProductTotalInMCOverLay");
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfListOfElements(dataToBeVerified, pdpPage), 
					"Available variation attributes for selected product, to be displayed in the following order : COLOR,SIZE,QTY,AVAILABILITY,PRICE,TOTAL", 
					"Variation Attributes for selected product displayed as expected!", 
					"Variation Attributes for selected product not displayed as expected", driver);
	
			dataToBeVerified =  Arrays.asList("Color","Size","Name");
			Log.message("Product Information in PDP:: " + productInfo.toString());
			Log.message("Product Information in MCO:: " + prdDataInMCOverlay.toString());
			Log.softAssertThat(CollectionUtils.compareTwoHashMap(productInfo, prdDataInMCOverlay, dataToBeVerified),
					"Product attributes should be exactly as selected by the User before clicking on the 'Add to Bag' button",
					"Product attributes are exactly as selected by the User before clicking on the 'Add to Bag' button",
					"Product attributes aren't exactly as selected by the User before clicking on the 'Add to Bag' button", driver);
	
			//Step-7: verify the Quantity in add to bag overlay page
			//verified along with step-6
	
			//Step-8: Verify the inventory state in add to bag overlay page
			Log.reference(i++ + ". Inventory State in Add To Bag Overlay verified with C19731.");
	
			//Step-9: Verify the price in add to bag overlay page
			Log.softAssertThat(prdDataInMCOverlay.get("Price").contains(productInfo.get("Price")), 
					"Price should be the single line item price of the item added.",
					"The single line item price of the item added is Displayed!",
					"Price not the single line item price of the item added.", driver);
	
			//Step-11: Verify the Items in your cart in the add to bag overlay page
			if(Utils.isMobile()){
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblItemInUrBagMCOverlayMobile"), pdpPage),
						"Total no of line item's and total value of selected line items which are available in the cart should be displayed in 'Items In Your Bag'",
						"Number of Line items and total amount displayed as Expected!",
						"Number of Line items and total amount not displayed as Expected", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblProductPriceInMCOverLay", "lblItemInUrBagMCOverlayMobile", pdpPage), 
						"Items in your bag should displayed below the Price section in Add to bag overlay.",
						"Items in your bag displayed below the Price section in Add to bag overlay.",
						"Items in your bag not displayed below the Price section in Add to bag overlay.", driver);
			}else{
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblItemInUrBagMCOverlay"), pdpPage),
						"Total no of line item's and total value of selected line items which are available in the cart should be displayed in 'Items In Your Bag'",
						"Number of Line items and total amount displayed as Expected!",
						"Number of Line items and total amount not displayed as Expected", driver);
	
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblProductPriceInMCOverLay", "lblItemInUrBagMCOverlay", pdpPage), 
						"Items in your bag should displayed below the Price section in Add to bag overlay.",
						"Items in your bag displayed below the Price section in Add to bag overlay.",
						"Items in your bag not displayed below the Price section in Add to bag overlay.", driver);
			}
			
			
			MiniCartPage minicart = homePage.headers.mouseOverMiniCart();
			String costInMCFlyt = minicart.getTotalCost();
			String costInMCOverlay = pdpPage.getTotalCostInMCOverlay();
			String itemNoInMCOverlay = pdpPage.getTotalItemCountInMCOverlay();
			String totalQtyBySumOfAllPrd = pdpPage.getSumOfAllProductQtyInMCOverlay();
			
			//ER-4 - covered in Step-1
	
			Log.softAssertThat(itemNoInMCOverlay.equals(totalQtyBySumOfAllPrd), 
					"If the customer has items in their cart, then the system sums the number of items existing & added",
					"The system sums the number of items existing & added",
					"the system did not sums the number of items existing & added", driver);
	
			Log.softAssertThat(pdpPage.getItemInYouBagMCOverlay().toLowerCase().contains(("ITEM IN YOUR BAG").toLowerCase()) ||
					pdpPage.getItemInYouBagMCOverlay().toLowerCase().contains(("ITEMS IN YOUR BAG").toLowerCase()) , 
					"Items should displayed in the following format : [# of cart items] Item(s) In Your Bag: $[cart total]", 
					"Items displayed in expected format!", 
					"Items not displayed in expected format.", driver);
	
			//Step-13: Verify the recommendation product in add to bag overlay page
			if(Utils.isMobile())
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("recommenationMCOverlay"), pdpPage), 
						"Recommendation section should not be displayed for mobile view", 
						"Recommendation section should not be displayed for mobile view", 
						"Recommendation section should not be displayed for mobile view", driver);
			else
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnCheckoutInATBMdl", "recommenationMCOverlay", pdpPage), 
						"Recommendation should be displayed below the \"Checkout\" button.", 
						"Recommendation should be displayed below the \"Checkout\" button.", 
						"Recommendation should be displayed below the \"Checkout\" button.", driver);
			
			String noOfItemsInCart = homePage.headers.getMiniCartCount();
			
			Log.event("No.of Items in MCart :: " + noOfItemsInCart);
			Log.event("No.of Items in ABT Overlay :: " + itemNoInMCOverlay);
			Log.event("Cost In Minicart :: " + costInMCFlyt);
			Log.event("Cose In ABT Overlay :: " + costInMCOverlay);
			
			Log.softAssertThat(itemNoInMCOverlay.equals(noOfItemsInCart) && costInMCFlyt.equals(costInMCOverlay), 
					"Items in your bag should display the total number of line items in the cart and the total cart value.", 
					"Items in your bag display the total number of line items in the cart and the total cart value.", 
					"Items in your bag not displaying the total number of line items in the cart and the total cart value.", driver);
			
			//Step-12: Verify the checkout in add to bag overlay page
			ShoppingBagPage cartPage = pdpPage.clickOnCheckoutInMCOverlay();
			Log.softAssertThat(cartPage.getPageLoadStatus(), 
					"While clicking on 'Checkout' button,page should be redirected to Cart page.",
					"Page redirected to Cart Page!",
					"Page not redirected to Cart Page.", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP2_C19730


}// search
