package com.fbb.testscripts.drop3;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.GiftCardPage;
import com.fbb.pages.HomePage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;
import org.openqa.selenium.Dimension;
import com.fbb.pages.PdpPage;
import com.fbb.reusablecomponents.TestData;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C19683 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C19683(String browser) throws Exception {
		Log.testCaseInfo();
		String category = "Gift Card";
		String categoryAlt = "Home";
	
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Step-1: Click /Tap on the categories from the breadcrumb in the Gift card page
			GiftCardPage gcLanding = homePage.footers.navigateToGiftCardLandingPage();
			Log.message(i++ + ". Navigated to Gift Card Landing Page", driver);
			
			PdpPage pdpPage = gcLanding.navigateToGiftCards();
			String prdName = pdpPage.getProductName();
			Log.message(i++ + ". Navigated to PDP Pag for Product: " + prdName, driver);
	
			if(Utils.isMobile()){
				String breadcrumbCategoryText = pdpPage.elementLayer.getElementText("lblCurrentBreadCrumbMobile", pdpPage).toUpperCase();
				Log.softAssertThat(breadcrumbCategoryText.contains(category.toUpperCase()) || breadcrumbCategoryText.contains(categoryAlt.toUpperCase()), 
						"When user navigates via browse: breadcrumb should display the category path.", 
						"When user navigates via browse: breadcrumb display the category path.", 
						"When user navigates via browse: breadcrumb not display the category path.", driver);
			} else {
				String breadcrumbCategoryText = pdpPage.elementLayer.getElementText("lblPrimaryBreadCrumbDesktop", pdpPage).toUpperCase();
				Log.softAssertThat(breadcrumbCategoryText.contains(category.toUpperCase()) || breadcrumbCategoryText.contains(categoryAlt.toUpperCase()), 
						"When user navigates via browse: breadcrumb should display the category path.", 
						"When user navigates via browse: breadcrumb display the category path.", 
						"When user navigates via browse: breadcrumb not display the category path.", driver);
			}
	
			//Step-11: Select the Amount,Quantity and Click/Tap on Add to bag button when the personalization Checkbox is unchecked.
			pdpPage.selectGCSize();
			Log.message(i++ + ". Selected Gift Card Size.", driver);
			
			Log.softAssertThat(pdpPage.verifyGcImages(), 
					"For gift card ,there Should only be one image.", 
					"For gift card ,only one image is displayed!", 
					"For gift card ,more than one image displayed", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add to bag button when the personalization Checkbox is unchecked.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlMiniCartOverLay"), pdpPage) || 
					pdpPage.getAddedQtyInCart() == 1,
					"The Gift card should be added to shopping bag",
					"The Gift card added to shopping bag",
					"The Gift card not added to shopping bag", driver);
			
			pdpPage.closeAddToBagOverlay();
			
			//Step-12: Select the Personalization Checkbox and enter Amount,Quantity , "To address, From address , message "Click/Tap on Add to bag button
			pdpPage.checkGCPersonalizedMessage("check");
			Log.message(i++ + ". Checked on Gift card personalization checkbox.", driver);
			
			pdpPage.enterGCFromAddress("automation@yopmail.com");
			pdpPage.enterGCToAddress("automation@yopmail.com");
			pdpPage.enterGCPersonalMessage("My Gift Card Personal Message.");
			Log.message(i++ + ". From, To, Personal Message box filled with data.", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add to bag button when the personalization Checkbox is unchecked");
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlMiniCartOverLay"), pdpPage) || 
					pdpPage.getAddedQtyInCart() == 2,
					"The Gift card should be added to shopping bag",
					"The Gift card added to shopping bag",
					"The Gift card not added to shopping bag", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
		
	}// M1_FBB_DROP3_C19683
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP3_C19683(String browser) throws Exception {
		Log.testCaseInfo();
		
		String amountToSelect = TestData.get("giftcard_Amount");
	
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.footers.navigateToPhysicalGiftCard();
			Log.message(i++ + ". Navigated to Gift Card PDP Page for :: " + pdpPage.getProductName());
			
			//Step-2: Verify the product image in the Gift card page
			if (Utils.isMobile()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "prodBreadCrumb", "mainProdImageActive_Mobile", pdpPage),
						"Product Image should be displayed to the left hand side of the Gift card page below the Bread crumb",
						"Product Image displayed to the left hand side of the Gift card page below the Bread crumb",
						"Product Image not displayed to the left hand side of the Gift card page below the Bread crumb", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "prodBreadCrumb", "mainProdImage", pdpPage),
						"Product Image should be displayed to the left hand side of the Gift card page below the Bread crumb",
						"Product Image displayed to the left hand side of the Gift card page below the Bread crumb",
						"Product Image not displayed to the left hand side of the Gift card page below the Bread crumb", driver);
			}
	
			if(Utils.isMobile()){
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductNameDesktop", "mainProdImageActive_Mobile", pdpPage),
						"Product Image should be displayed below the product name.",
						"Product Image displayed below the product name.",
						"Product Image not displayed below the product name.", driver);
			}
			
			Log.softAssertThat(pdpPage.getNumberOfColorSwatches() <= 1, 
					"User Should have the ability to choose gift card image.", 
					"User can choose the gift card image", 
					"User don't have the ability to choose the gift card image", driver);
	
			//Step-3: Verify the Special Product Messaging in the Gift card page
			if(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblProductSpecialMessage"), pdpPage)) {
				if(Utils.isMobile()) {
					Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgPrimaryImage", "lblProductSpecialMessage", pdpPage), 
							"The Special product messaging should be below the product image.", 
							"The Special product messaging is below the product image.", 
							"The Special product messaging is not below the product image.", driver);
				}else{
					Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lblProductSpecialMessage", "imgPrimaryImage", pdpPage), 
							"The Special product messaging should be displayed right side of the product image.", 
							"The Special product messaging is displayed right side of the product image.", 
							"The Special product messaging is not displayed right side of the product image.", driver);
				}
				
			}
			
			//Step-4: Click/tap on Terms and conditions in the Gift card page - Ref:21621
			if(pdpPage.isSpecialProductMessageDisplayed()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblProductSpecialMessage", "lnkTermsAndConditions", pdpPage),
						"The Terms and Conditions should be displayed as a link below the Special product messaging.",
						"The Terms and Conditions displayed as a link below the Special product messaging.",
						"The Terms and Conditions not displayed as a link below the Special product messaging.", driver);
			} else {
				Log.reference("Special Product Message Not displayed. Since skipping this validation: <b>The Terms and Conditions should be displayed as a link below the Special product messaging.</b>");
			}
			
			pdpPage.openCloseToolTipOverlay("open");
			Log.message(i++ + ". Clicked on Terms & Conditions!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("toolTipOverLay"), pdpPage), 
					"Terms & Conditions Tool tip should be opened.", 
					"Terms & Conditions Tool tip opened!",
					"Terms & Conditions Tool tip not opened.", driver);
	
			if(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("toolTipOverLay"), pdpPage)){
				if(Utils.isMobile()) {
					Dimension hwSize = driver.manage().window().getSize();
					Log.softAssertThat(pdpPage.getToolTipDiementsion("height") <= hwSize.height , 
							"Tool tip height should be in range of 260px - 610px", 
							"Tool tip height displayed as expected!", 
							"Tool tip height not fit in expected range.", driver);
				} else {
				Log.softAssertThat(pdpPage.getToolTipDiementsion("height") >= 260 && pdpPage.getToolTipDiementsion("height") <= 610 , 
						"Tool tip height should be in range of 260px - 610px", 
						"Tool tip height displayed as expected!", 
						"Tool tip height not fit in expected range.", driver);
				}
				
				Log.softAssertThat(pdpPage.getToolTipDiementsion("width") >= 330 && pdpPage.getToolTipDiementsion("width") <= 428 , 
						"Tool tip width should be in range of 330px - 428px", 
						"Tool tip width displayed as expected!", 
						"Tool tip width not fit in expected range.", driver);
	
				pdpPage.openCloseToolTipOverlay("close");
				Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("toolTipOverLay","display","none", pdpPage), 
						"Tooltip should be closed when clicking on X mark.", 
						"Tooltip closed when clicking on X mark!", 
						"Tooltip not closed when clicking on X mark", driver);
			}else{
				Log.failsoft("Terms & Conditions Tool tip not opened. Cannot do ool tip height & Width verification", driver);
			}
			//Scroll Bar verification Pending
	
			//Step-6: Click/Tap on Select Amount name or (v) button
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedBelow("lnkTermsAndConditions", "drpGiftCardSizeSelect", pdpPage),
					"Select Amount should be displayed below the Terms and Conditions section.",
					"Select Amount displayed below the Terms and Conditions section.",
					"Select Amount not displayed below the Terms and Conditions section.", driver);
			
			Log.softAssertThat(pdpPage.getSelectedGCAmount().equalsIgnoreCase("Select Amount"), 
					"'Select Amount' text should be displayed by default", 
					"'Select Amount' text displayed by default" , 
					"'Select Amount' text not displayed by default", driver);
	
			pdpPage.clickOnGiftCardSize();
			Log.message(i++ + ". Clicked on Select Amount Dropdown.", driver);
	
			pdpPage.selectGCSize(amountToSelect);
			Log.message(i++ + ". Selected Amount from Dropdown.", driver);
			Log.event("Selected amount :: " + amountToSelect);
			
			Log.softAssertThat(pdpPage.getSelectedGCAmount().trim().equals(amountToSelect), 
					"Selected value should be display in the select amount field", 
					"Selected value displayed in the select amount field", 
					"Selected value not displayed in the select amount field", driver);
			
			Log.softAssertThat(!pdpPage.getSelectedGCAmount().isEmpty(), 
					"Select Amount drop down should be opened", 
					"Select Amount drop down opened!", 
					"Select Amount drop down not opened", driver);
	
			//Step-7: Click/Tap on Quantity drop down and select any value - for E-Gift card
			Log.softAssertThat(pdpPage.getQuantity() == 1, 
					"Default value should be 1 in the quantity drop down field.", 
					"Default value is 1 in the quantity drop down field!", 
					"Default value is not 1 in the quantity drop down field.", driver);
	
			Log.softAssertThat(pdpPage.verifyQtyDrp(), 
					"Drop down should display 1-10", 
					"Drop down displayed 1-10", 
					"Drop down not displayed as expected", driver);
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add to bag button", driver);
	
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". ATS Overlay closed!", driver);
	
			Log.message(i++ + ". " + pdpPage.getProductName() + " Product added to Shopping Cart!");
			
			Log.softAssertThat(pdpPage.getBagCount() == 1, 
					"The Gift card should be added to shopping bag",
					"The Gift card added to shopping bag",
					"The Gift card not added to shopping bag", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
		
	}// M2_FBB_DROP3_C19683
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP3_C19683(String browser) throws Exception {
		Log.testCaseInfo();
		
		int deltaOverMax = 10; 
		int personalMsgLength = 100;
		int toBoxMax = 50; 
		int fromBoxMax = 50; 
		int personalMsgMax = 330;  
	
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);                   
	
			PdpPage pdpPage = homePage.footers.navigateToPhysicalGiftCard();
			Log.message(i++ + ". Navigated to Gift Card PDP Page for :: " + pdpPage.getProductName(), driver);
	
			//Step-6: Click/ Tap on Add to bag button without selecting the Amount
			pdpPage.checkGCPersonalizedMessage();
			Log.message(i++ + ". Gift card Personalized Message checkbox checked!", driver);
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add To Bag Button without selecting the amount.");
	
			Log.softAssertThat(pdpPage.elementLayer.verifyAttributeForElement("txtAreaPersonalMsgError", "class", "error", pdpPage),
					"Message field should be highlighted with red label.", 
					"Message field highlighted with red label.", 
					"Message field not highlighted with red label.", driver);
	
			Log.softAssertThat(pdpPage.verifyAmountDropdownError(pdpPage), 
					"Select Amount field should be highlighted with red label.", 
					"Select Amount field highlighted with red label.", 
					"Select Amount field not highlighted with red label.", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("errMsgGCAmount"), pdpPage), 
					"Please Select Amount error message should be displayed above Add to bag button", 
					"Please Select Amount error message displayed above Add to bag buttonSelect Amount field highlighted with red label.", 
					"Please Select Amount error message not displayed above Add to bag buttonSelect Amount field not highlighted with red label.", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyAttributeForElement("txtGCPersonalMsgToError", "class", "error", pdpPage) && 
					pdpPage.elementLayer.verifyAttributeForElement("txtGCPersonalMsgFromError", "class", "error", pdpPage), 
					"From & To field should be highlighted with red label.", 
					"Message field highlighted with red label.", 
					"Message field not highlighted with red label.", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("txtGCPersonalMsgTo","txtGCPersonalMsgFrom"),50, pdpPage), 
					"User should able to write up to 50 characters in the From & To Text fields", 
					"User can able to write up to 50 characters in the From & To Text fields!", 
					"User may not be able to write up to 50 characters in the From & To Text fields", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyTextFieldAllowSpecialCharacters(Arrays.asList("txtGCPersonalMsgTo","txtGCPersonalMsgFrom"),Arrays.asList("$",".","-"), pdpPage), 
					"User should able to write up to 50 characters in the From & To Text fields", 
					"User can able to write up to 50 characters in the From & To Text fields!", 
					"User may not be able to write up to 50 characters in the From & To Text fields", driver);
	
			pdpPage.enterGCToAddress(toBoxMax + deltaOverMax);
			Log.message(i++ + ". More than 50 Characters Entered in To Textbox.");
	
			pdpPage.enterGCFromAddress(fromBoxMax + deltaOverMax);
			Log.message(i++ + ". More than 50 Characters Entered in From Textbox.");
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyElementTextCountEqualTo("txtGCPersonalMsgTo", toBoxMax, pdpPage) &&
					pdpPage.elementLayer.VerifyElementTextCountEqualTo("txtGCPersonalMsgFrom", fromBoxMax, pdpPage),
					"When the user has entered the max of 50 characters, it should not permit to enter any additional characters in the field",
					"When the user has entered the max of 50 characters, it does not permit to enter any additional characters in the field", 
					"When the user has entered the max of 50 characters, it permits to enter any additional characters in the field", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("txtGCPersonalMsgTo","txtGCPersonalMsgFrom"), 50, pdpPage),
					"When the user has entered the max of 50 characters, it should not permit to enter any additional characters in the field",
					"When the user has entered the max of 50 characters, it does not permit to enter any additional characters in the field", 
					"When the user has entered the max of 50 characters, it permits to enter any additional characters in the field", driver);
	
			pdpPage.enterGCPersonalMessage(personalMsgMax);
			Log.message(i++ + ". 330 Characters Entered in Personal Message Textbox.");
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyElementTextCountEqualTo("txtAreaPersonalMsg", personalMsgMax, pdpPage),
					"User should able to Write up to 330 characters in the your message body.",
					"User can able to Write up to 330 characters in the your message body.",
					"User not able to Write up to 330 characters in the your message body.", driver);
	
			Log.softAssertThat(pdpPage.getPersonalCounterText().toLowerCase().contains("you have reached the limit"), 
					"Once user enters 330 characters, \"You have reached the limit\" message should be displayed in the right corner of message box",
					"'You have reached the limit' message displayed as expected",
					"'You have reached the limit' message not displayed as expected", driver);
	
			pdpPage.enterGCPersonalMessage(personalMsgLength);
			Log.message(i++ + ". 100 Characters Entered in Personal Message Textbox.");
	
			Log.softAssertThat(pdpPage.getCharacterCount() == (personalMsgMax - personalMsgLength),
					"The Count remaining should be displayed in the bottom corner of the message field, Based on the user entry the count should be updated",
					"Message counter works as expected!",
					"Message counter not works as Expected.", driver);
	
			//Step-10: Verify the functionality of Character count
			Log.softAssertThat(pdpPage.verifyTextCounterIncreaseOrDecrease(deltaOverMax, false) && pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtPersonalMsgCounter"), pdpPage),
					"For text count verification in Personal Message Text Area.",
					"Text Count is correctly reducing with or without spaces!",
					"Text Count is not reducing", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("txtAreaPersonalMsg"), personalMsgMax, pdpPage),
					"For text count verification in Personal Message Text Area.",
					"After maximum character the TextArea didnt allow to enter text!",
					"After maximum character the TextArea allowing to enter text!", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M3_FBB_DROP3_C19683

}// TC_FBB_DROP3_C19683
