package com.fbb.testscripts.drop3;
import com.fbb.reusablecomponents.TestData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP3_C21581 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	
	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP3_C21581(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String lvl1 = TestData.get("level-2").split("\\|")[0];
		String lvl2 = TestData.get("level-2").split("\\|")[1];
				
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!",driver);
			
			PlpPage plpPage = null;
			
			if(Utils.isDesktop()) {
				plpPage = homePage.headers.navigateTo(lvl1);
			} else {
				plpPage = homePage.headers.navigateTo(lvl1, lvl2);
			}
			Log.message(i++ + ". Navigated to SLP Page with Search Keyword");
			
			String[] prodId = plpPage.getListOfProductId(20);
	
			String[] url = plpPage.getProductUrl(20);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prodId[0]);
			List<String> prdNames = new ArrayList<String>();
			prdNames.add(pdpPage.getProductName());
			Log.message(i++ + ". Navigated to PDP for Product :: " + prdNames.get(0));
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("recentlyViewedSection"), pdpPage),
					"Check the recently viewed section is not displaying when user searches first product",
					"The recently viewed section is not displaying when user searches first product",
					"The recently viewed section is displaying when user searches first product", driver);
	
			pdpPage = homePage.headers.navigateToPDP(prodId[1]);
			prdNames.add(pdpPage.getProductName());
			int size = pdpPage.getNoOfProdDisplayInRecentView();
	
			Log.softAssertThat(size == 1,
					"To check the recently viewed product didnot display the same product with different variant!",
					"The recently viewed product didnot display the same product with different variant",
					"The recently viewed product is displaying the same product with different variant!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "detailsReviewSection", "recentlyViewedSection", pdpPage),
					"To check the recently viewed section is displayed below the reviews and details section!",
					"The recently viewed section is displayed below the reviews and details section!",
					"The recently viewed section is not displayed below the reviews and details section!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("recentlyViewedProdPricing", "recentlyViewedProdImage", "recentlyViewedProdName"), pdpPage),
					"Check 'Recently Viewed' section have 'Product Name', 'Product price' and 'Product image'",
					"'Recently Viewed' section have 'Product Name', 'Product price' and 'Product image'",
					"'Recently Viewed' section is not having 'Product Name', 'Product price' and 'Product image'", driver);
	
			Log.softAssertThat(size > 0,
					"To check the recently viewed section is having visited product!",
					"The recently viewed section is having visited product!",
					"The recently viewed section is not having visited product!", driver);
	
			for (int w=2; w< url.length - 3; w++) {
				Utils.openNewTab(driver);
	
				List<String> handle = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(handle.get(handle.size() - 1));
				driver.get(url[w]);
	
				Utils.waitForPageLoad(driver);
			}
	
			pdpPage = homePage.headers.navigateToPDP(prodId[url.length - 1]);
	
			size = pdpPage.getNoOfProdDisplayInRecentView();
	
			Log.softAssertThat(size == 12,
					"To check the recently viewed section displaying the 12 new viewed product!",
					"The recently viewed section is displaying the 12 new viewed product",
					"The recently viewed section is not displaying the 12 new viewed product!", driver);
	
			if(Utils.isDesktop()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyListElementSize("recentlyViewedCurrentProd", 4, pdpPage),
						"To check the recently viewed section visible 4 product!",
						"The recently viewed section is visible 4 product",
						"The recently viewed section is not visible 4 product!", driver);
			}
			if(!Utils.isMobile()) {	
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("recentlyViewSectionNextArrow","recentlyViewSectionPrevArrow"), pdpPage),
						"Check the recently viewed section is having 'Next' and 'Prev' arrow",
						"The recently viewed section is having 'Next' and 'Prev' arrow",
						"The recently viewed section is not having 'Next' and 'Prev' arrow", driver);
			}
			Log.testCaseResult();

	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP3_C21581


}// search
