package com.fbb.testscripts.api;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.externalAPI.RestAssuredAPI;
import com.fbb.support.BaseTest;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.google.gson.Gson;
import com.jayway.restassured.response.Response;

@Listeners(EmailReport.class)
public class TC_FBB_ACCOUNT_API extends BaseTest{
	
	public static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("envAPI");
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	@Test(description = "Verify the account update email API with valid API details.")
	public void TC_API_ACCOUNTUPDATEEMAIL_001() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountupdateUrl = accountData.get("accountupdateemail");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = " {\n" +
				"	\"accountKey\": \"8004963\",\n" +
				"	\"email\": \"abdul240589@gmail.com\",\n"+
				"	\"requestedFrom\": \"KingSizeDirect\",\n"+
				"	\"firstName\": \"Abdul\", \n"+
				"	\"lastName\": \"Sardhar\" ,\n"+
				"	\"billingAddress\": {\n " +
		"	\"firstName\": \"Abdul\", \n"+
		"	\"lastName\": \"Sardhar\" ,\n"+
		"	\"address1\": \"600 Reed Road\", \n"+
		"	\"address2\": \"Suite 302\",\n"+
		"	\"city\": \"Charlotte\",\n"+
		"	\"state\": \"NC\", \n"+
		"	\"zipCode\": \"28205\"\n" +
		"	}, \n "+
		" \"dayPhone\": \"2223334444\", \n"+
		"\"nightPhone\": \"2223334444\" \n"+ 
		"}";
		
		
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountupdateUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==200, "API should displayed the valid status",
				"API is displayed 200 as status code when giving valid request", 
				"API displayed the invalid status code");
		
		Log.testCaseResult();
		
		 
	}
	
	@Test(description = "Verify the account update email API with invalid API details.")
	public void TC_API_ACCOUNTUPDATEEMAIL_002() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountupdateUrl = accountData.get("accountupdateemail");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = "{\n"+
				 "\"lineItems\": [ \n"+
					"{\n"+
				      "\"productId\": \"300757689\",\n"+
				      "\"quantity\": 1, \n"+
				      "\"reason\": \"Too Big\" \n"+
				    "}\n"+
				  "]\n"+
				"}"; 
		
		
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountupdateUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==400, "API should displayed the valid status",
				"API is displayed 400 as status code when giving invalid request", 
				"API displayed the invalid status code");
		
		Log.testCaseResult();
		
		 
	}
	
	@Test(description = "Verify the account API with valid API details.")
	public void TC_API_ACCOUNT_001() throws Exception
	{		
		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountcreationUrl = accountData.get("account");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		//input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = " {\n" +
				  "\"accountKey\": \"962852\",\n " +
				  "\"firstName\": \"Abdul\",\n" +
				  "\"lastName\": \"sardhar\",\n" +
				  "\"email\": \"abdul240589@gmail.com\",\n" +
				  "\"password\": \"aaspire@12\", \n" +
				  "\"previousEmail\": \"abdul240589@gmail.com\",\n" +
				  "\"dayPhone\": \"3334447777\",\n" +
				  "\"eveningPhone\": \"3334447777\",\n" +
				  "\"mobilePhone\": \"3334447777\",\n" +
				  "\"currentlyActiveOn\": \"Roamans\", \n" +
				  " \"birthMonth\": 11,\n" +
				  "\"catalogCustomerId\": \"xxxx\",\n" +
				  "\"isEmailInactive\": true,\n" +
				  "\"affiliateId\": \"NC\",\n" +
				  "\"affiliateLocation\": \"US\" \n "+ 
				"}"; 

		
		
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountcreationUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==200, "API should displayed the valid status",
				"API is displayed 200 as status code when giving valid request", 
				"API displayed the invalid status code");
		
		Log.testCaseResult();
		
		 
	}
	
	@Test(description = "Verify the account API with invalid API details.")
	public void TC_API_ACCOUNT_002() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountcreationUrl = accountData.get("account");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		//input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = "{\n"+
				 "\"lineItems\": [ \n"+
					"{\n"+
				      "\"productId\": \"300757689\",\n"+
				      "\"quantity\": 1, \n"+
				      "\"reason\": \"Too Big\" \n"+
				    "}\n"+
				  "]\n"+
				"}"; 

		
		
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountcreationUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==400, "API should displayed the valid status",
				"API is displayed 400 as status code when giving invalid request", 
				"API displayed the invalid status code");
		
		Log.testCaseResult();
		
		 
	}
	
	
	@SuppressWarnings("unchecked")
	@Test(description = "Verify the GEt Email Subscription API with valid email id.")
	public void TC_API_GETEMAILSUBSCRIPTION_001() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String emailID = accountData.get("emailSubEmailID");
		String accountupdateUrl = accountData.get("emailSubscription").replace("!email!", emailID);

		Response response = RestAssuredAPI.GETBASEURL(baseURL,accountupdateUrl);
		
		JSONParser parser = new JSONParser();
		JSONArray data = (JSONArray) parser.parse(response.asString());
		Log.message(data.toJSONString());
		JSONObject jSONObject = (JSONObject) data.get(0);
		Gson gson = new Gson();
		Map<String, String> myMap = gson.fromJson(jSONObject.toString(),Map.class);
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==200, "API should displayed the valid status",
				"API is displayed 200 as status code when giving valid request", 
				"API displayed the invalid status code");
		
		Log.softAssertThat(myMap.get("email").equalsIgnoreCase("SWAGGER@GMAIL.COM"), "Email Subscription API should displayed the valid email details",
				"Email Subscription API is displayed the valid Email details!", 
				"Email Subscription API is not displayed the valid Email details!");
		
		
		Log.testCaseResult();
		
		 
	}
	
	@Test(description = "Verify the GEt Email Subscription API with valid email id.")
	public void TC_API_GETEMAILSUBSCRIPTION_002() throws Exception
	{		
		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String emailID = accountData.get("invalidEmailSubEmailID");
		String accountupdateUrl = accountData.get("emailSubscription").replace("!email!", emailID);

		Response response = RestAssuredAPI.GETBASEURL(baseURL,accountupdateUrl);
	
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==400, "API should displayed the valid status",
				"API is displayed 200 as status code when giving valid request", 
				"API displayed the invalid status code");
		
		
		
		Log.testCaseResult();
		
		 
	}
	
	@Test(description = "Verify the account update email API with invalid API details.")
	public void TC_API_PASSWORDRESETEMAIL_001() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountKey = accountData.get("accountKey");
		String accountupdateUrl = accountData.get("passwordResetEmail").replace("!accountKey!", accountKey);
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = "{\n"+
				"\"accountKey\":\"8001049\", \n"+
				"\"email\":\"janarthprabhu@gmail.com\", \n"+
				"\"firstName\":\"Janarth\", \n"+
				"\"requestedFrom\":\"Roamans\", \n"+
				"\"token\":\"https://sfcc.qa.rm.plussizetech.com/setpassword?Token=94ee1ea69224ffe0bbc3922ea4360c0fed7fb93cea45ab\" \n"+
				"}" ; 
		
		
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,body,accountupdateUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==200, "API should displayed the valid status",
				"API is displayed 200 as status code when giving invalid request", 
				"API displayed the invalid status code");
		
		Log.testCaseResult();
		
		 
	}
	
	@Test(description = "Verify the account update email API with invalid API details.")
	public void TC_API_PASSWORDRESETEMAIL_002() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountKey = accountData.get("accountKey");
		String accountupdateUrl = accountData.get("passwordResetEmail").replace("!accountKey!", accountKey);
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = "{\n"+
				 "\"lineItems\": [ \n"+
					"{\n"+
				      "\"productId\": \"300757689\",\n"+
				      "\"quantity\": 1, \n"+
				      "\"reason\": \"Too Big\" \n"+
				    "}\n"+
				  "]\n"+
				"}"; 
		
		
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,body,accountupdateUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==400, "API should displayed the valid status",
				"API is displayed 400 as status code when giving invalid request", 
				"API displayed the invalid status code");
		
		Log.testCaseResult();
		
		 
	}

	@Test(description = "Verify the account API with valid API details.")
	public void TC_API_ADDRESS_001() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountcreationUrl = accountData.get("address").replace("cust_id", "962852");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		//input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = " {\n" + 
				"\"accountKey\": \"962852\",\n" + 
				"\"userDefinedAddressKey\": \"Nickupyrb\",\n" + 
				"\"addressType\": \"Preferred\",\n" + 
				"\"currentlyActiveOn\": \"JessicaLondon\",\n" + 
				"\"firstName\": \"John\",\n" + 
				"\"lastName\": \"William\",\n" + 
				"\"addressLine1\": \"3850 E Independence Blvd\",\n" + 
				"\"addressLine2\": \"\",\n" + 
				"\"city\": \"Charlotte\",\n" + 
				"\"stateCode\": \"NC\",\n" + 
				"\"postCode\": \"28205\",\n" + 
				"\"countryCode\": \"US\",\n" + 
				"\"dayPhone\": \"3334445555\",\n" + 
				"\"nightPhone\": \"3334445555\"}"; 
	
		
		Log.message("URL :: " + baseURL + accountcreationUrl);
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountcreationUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==202, "API should displayed the valid status",
				"API is displayed 200 as status code when giving valid request", 
				"API displayed the invalid status code");
		
		Log.testCaseResult();
		
		 
	}
	
	@Test(description = "Verify the account API with valid API details.")
	public void TC_API_ADDRESS_002() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountcreationUrl = accountData.get("address").replace("cust_id", "962852");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		//input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = "{\n"+
				 "\"lineItems\": [ \n"+
					"{\n"+
				      "\"productId\": \"300757689\",\n"+
				      "\"quantity\": 1, \n"+
				      "\"reason\": \"Too Big\" \n"+
				    "}\n"+
				  "]\n"+
				"}"; 
	
		
		Log.message("URL :: " + baseURL + accountcreationUrl);
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountcreationUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==400, "API should displayed the invalid status",
				"API is displayed 400 as status code when giving invalid request", 
				"API displayed the valid status code");
		
		Log.testCaseResult();
	}
	
	@Test(description = "Verify the account API with valid API details.")
	public void TC_API_CATALOGUNSUBSCRIBEEMAIL_001() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountcreationUrl = accountData.get("catalogunsubscribeemail") + "8004963";
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		//input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = "{\n" + 
				"  \"accountKey\": \"8004963\",\n" + 
				"  \"email\": \"janarthprabhu@gmail.com\",\n" + 
				"  \"firstName\": \"janarth\",\n" + 
				"  \"requestedFrom\": \"roamans\"\n" + 
				"}"; 
	
		
		Log.message("URL :: " + baseURL + accountcreationUrl);
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountcreationUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==202, "API should displayed the valid status",
				"API is displayed 200 as status code when giving valid request", 
				"API displayed the invalid status code");
		
		Log.testCaseResult();
	}
	
	@Test(description = "Verify the account API with valid API details.")
	public void TC_API_CATALOGUNSUBSCRIBEEMAIL_002() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountcreationUrl = accountData.get("catalogunsubscribeemail") + "8004963";
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		//input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = "{\n"+
				 "\"lineItems\": [ \n"+
					"{\n"+
				      "\"productId\": \"300757689\",\n"+
				      "\"quantity\": 1, \n"+
				      "\"reason\": \"Too Big\" \n"+
				    "}\n"+
				  "]\n"+
				"}";  
	
		
		Log.message("URL :: " + baseURL + accountcreationUrl);
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountcreationUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==400, "API should displayed the invalid status",
				"API is displayed 400 as status code when giving invalid request", 
				"API displayed the valid status code");
		
		Log.testCaseResult();
	}
	
	@Test(description = "Verify the account API with valid API details.")
	public void TC_API_WELCOMEGUESTUSEREMAIL_001() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountcreationUrl = accountData.get("welcomeguestemail");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		//input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = "{\n" + 
				"  \"email\": \"janarthprabhu@gmail.com\",\n" + 
				"  \"requestedFrom\": \"roamans\"\n" + 
				"}"; 
	
		
		Log.message("URL :: " + baseURL + accountcreationUrl);
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountcreationUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==202, "API should displayed the valid status",
				"API is displayed 200 as status code when giving valid request", 
				"API displayed the invalid status code");
		
		Log.testCaseResult();
	}
	
	@Test(description = "Verify the account API with valid API details.")
	public void TC_API_WELCOMEGUESTUSEREMAIL_002() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountcreationUrl = accountData.get("welcomeguestemail");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		//input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = "{\n"+
				 "\"lineItems\": [ \n"+
					"{\n"+
				      "\"productId\": \"300757689\",\n"+
				      "\"quantity\": 1, \n"+
				      "\"reason\": \"Too Big\" \n"+
				    "}\n"+
				  "]\n"+
				"}";  
	
		
		Log.message("URL :: " + baseURL + accountcreationUrl);
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountcreationUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==400, "API should displayed the invalid status",
				"API is displayed 400 as status code when giving invalid request", 
				"API displayed the valid status code");
		
		Log.testCaseResult();
	}
	
	@Test(description = "Verify the account API with valid API details.")
	public void TC_API_WELCOMERESGISTEREDUSEREMAIL_001() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountcreationUrl = accountData.get("welcomeregisteredemail") + "962852";
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		//input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = "{\n" + 
				"  \"accountKey\": \"962852\",\n" + 
				"  \"email\": \"janarthprabhu@gmail.com\",\n" + 
				"  \"requestedFrom\": \"Roamans\",\n" + 
				"  \"firstName\": \"Janarth\",\n" + 
				"  \"lastName\": \"Prabhu\",\n" + 
				"  \"billingAddress\": {\n" + 
				"    \"firstName\": \"Janarth\",\n" + 
				"    \"lastName\": \"Prabhu\",\n" + 
				"    \"address1\": \"8001 s orange blossom trl\",\n" + 
				"    \"address2\": \"\",\n" + 
				"    \"city\": \"Orlando\",\n" + 
				"    \"state\": \"FL\",\n" + 
				"    \"zipCode\": \"44585\"\n" + 
				"  },\n" + 
				"  \"dayPhone\": \"6789564543\",\n" + 
				"  \"nightPhone\": \"6789564543\",\n" + 
				"  \"isSubscriptionOptIn\": true\n" + 
				"} "; 
	
		
		Log.message("URL :: " + baseURL + accountcreationUrl);
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountcreationUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==202, "API should displayed the valid status",
				"API is displayed 200 as status code when giving valid request", 
				"API displayed the invalid status code");
		
		Log.testCaseResult();
	}
	
	@Test(description = "Verify the account API with valid API details.")
	public void TC_API_WELCOMERESGISTEREDUSEREMAIL_002() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountcreationUrl = accountData.get("welcomeregisteredemail") + "962852";
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		//input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = "{\n"+
				 "\"lineItems\": [ \n"+
					"{\n"+
				      "\"productId\": \"300757689\",\n"+
				      "\"quantity\": 1, \n"+
				      "\"reason\": \"Too Big\" \n"+
				    "}\n"+
				  "]\n"+
				"}";  
	
		
		Log.message("URL :: " + baseURL + accountcreationUrl);
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountcreationUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==400, "API should displayed the invalid status",
				"API is displayed 400 as status code when giving invalid request", 
				"API displayed the valid status code");
		
		Log.testCaseResult();
	}
	
	@Test(description = "Verify the account API with valid API details.")
	public void TC_API_WISHLIST_001() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountcreationUrl = accountData.get("wishlist");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		//input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = "{\n" + 
				"  \"accountKey\": \"962852\",\n" + 
				"  \"senderEmail\": \"janarthprabhu@gmail.com\",\n" + 
				"  \"recipientEmails\": [\n" + 
				"    \"venkatesh.rasappan@aspiresys.com\"\n" + 
				"  ],\n" + 
				"  \"requestedFrom\": \"Roamans\",\n" + 
				"  \"emailSubject\": \"Test\",\n" + 
				"  \"emailPersonalMessage\": \"Please find the wishlist email\",\n" + 
				"  \"senderName\": \"Janarth\",\n" + 
				"  \"wishlistUrl\": \"https://sfcc.qa.rm.plussizetech.com/showotherwishlist?WishListID=133e04942d5d44762852d9582d\"\n" + 
				"} "; 
	
		
		Log.message("URL :: " + baseURL + accountcreationUrl);
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountcreationUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==202, "API should displayed the valid status",
				"API is displayed 200 as status code when giving valid request", 
				"API displayed the invalid status code");
		
		Log.testCaseResult();
	}
	
	@Test(description = "Verify the account API with valid API details.")
	public void TC_API_WISHLIST_002() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountcreationUrl = accountData.get("welcomeregisteredemail") + "962852";
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		//input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = "{\n"+
				 "\"lineItems\": [ \n"+
					"{\n"+
				      "\"productId\": \"300757689\",\n"+
				      "\"quantity\": 1, \n"+
				      "\"reason\": \"Too Big\" \n"+
				    "}\n"+
				  "]\n"+
				"}";  
	
		
		Log.message("URL :: " + baseURL + accountcreationUrl);
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountcreationUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==400, "API should displayed the invalid status",
				"API is displayed 400 as status code when giving invalid request", 
				"API displayed the valid status code");
		
		Log.testCaseResult();
	}
	
	
}
