package com.fbb.testscripts.api;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.externalAPI.RestAssuredAPI;
import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;
import com.google.gson.Gson;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.mongodb.util.JSON;
import com.utils.testrail.TestRailAPICalls;


@Listeners(EmailReport.class)
public class TC_FBB_Order_API extends BaseTest{
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader envAPIData = EnvironmentPropertiesReader.getInstance("envAPI");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test (description= "Testing the get order API with valid ordernumber")
	public void TC_API_GET_001() throws Exception {
		int i = 1;
		
		String ordernumber = envAPIData.get("ordernumber1");
		String url = envAPIData.get("APIUrl");

		//Response response = RestAssured.given().when().get("http://test.order.mw.plussizetech.com/api/orders/"+ordernumber);
		Response response = RestAssuredAPI.GET(url, ordernumber);
		System.out.println(JSON.parse(response.asString()));
		
		Gson gson = new Gson();
		//	Type type = new TypeToken<Map<String, String>>(){}.getType();
		Map<String, String> hmap = gson.fromJson(JSON.parse(response.asString()).toString(), Map.class);
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));

		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>content type--------------------> </b>" + response.getContentType());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<B>Response: </B></n>" + hmap.toString());
		
		Log.message(i++ + ". Verifying the Order Number is matching with reponse.");
		Log.softAssertThat(hmap.get("orderNumber").equals(ordernumber), "Account number is matching", "Account number is not matching");
		
		System.out.println("OrderDate"+ hmap.get("orderDate"));
		
		Log.testCaseResult();
	}
	
	@Test(description= "Testing the get order API with invalid ordernumber")
	public void TC_API_GET_002() throws Exception {
		int i = 1;
		
		String ordernumber = envAPIData.get("ordernumber2");
		Response response =RestAssured.given().when().get("http://test.order.mw.plussizetech.com/api/orders/"+ordernumber);
		
		System.out.println(JSON.parse(response.asString()));
		HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>content type--------------------> </b>" + response.getContentType());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<B>Response: </B></n>" + hmap.toString());
		
		Log.message(i++ + ". Verifying the Order Number is matching with reponse.");
		Log.softAssertThat(hmap.get("message").equals("The order with id "+ ordernumber +" could not be found"), "Account number is Nota valid and valid error message is displayed.", "Valid error message is not displayed");
		
		//Object obj = JSON.parse(response.asString());
		Log.testCaseResult();
	}
	
	@Test(enabled= false,groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_API_GET_003(String browser) throws Exception {
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		String credential = username + "|" + password;
		
		//Load Test Data
		String variation_product = TestData.get("prd_variation1");
		String prd_monogram = TestData.get("prd_monogram");
		String couponCode1 = TestData.get("cpn_freeshipping");
		String couponCode2 = TestData.get("cpn_off_10_percentage");
		
		//Pre-requisite - Account Should have more than one payment information
		{
			GlobalNavigation.registerNewUser(driver, 1, 0, credential);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home page.", driver);
	
			homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
	
			Headers headers = homePage.headers;
			PdpPage pdpPage = headers.navigateToPDP(variation_product);
			Log.message(i++ + ". Navigated to PDP for :: " + pdpPage.getProductName());
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
			
			//Adding Monogram product
			pdpPage = homePage.headers.navigateToPDP(prd_monogram);
			Log.message(i++ + ". Navigated to PDP for :: " + pdpPage.getProductName(), driver);
			
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.clickOnMonogrammingCheckbox("enable");
			/*pdpPage.selectMonogrammingFontValue(1);
			pdpPage.selectMonogrammingColorValue(1);
			pdpPage.selectMonogrammingLocationValue(1);*/
			pdpPage.enterTextInMonogramFirstTextBox("Test");
			pdpPage.selectQty("1");
			pdpPage.addToBagKeepOverlay();
			Log.message(i++ + ". Product added to Bag.", driver);
	
			headers = new Headers(driver).get();
			ShoppingBagPage cartPage = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			CheckoutPage checkoutPage = (CheckoutPage) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
			
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", ShippingMethod.Standard, "valid_address7", "Gift Message Goes Here.");
			Log.message(i++ + ". Shipping details filled successfully.", driver);
			
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Billing/Payment Section.", driver);
				
			checkoutPage.applyPromoCouponCode(couponCode1);
			Log.message(i++ + ". Coupon-1 Applied.", driver);
	
			checkoutPage.applyPromoCouponCode(couponCode2);
			Log.message(i++ + ". Coupon-2 Applied.", driver);
			
			checkoutPage.fillingCardDetails1("card_Visa", false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
	
			OrderConfirmationPage ordConfPage = checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
		
			String ordernumber = ordConfPage.getOrderNumber() != null ? ordConfPage.getOrderNumber() : envAPIData.get("ordernumber1");
			String url = envAPIData.get("APIUrl");
			//Response response =RestAssured.given().when().get("http://test.order.mw.plussizetech.com/api/orders/"+ordernumber);
			
			Response response = RestAssuredAPI.GET(url, ordernumber);

			System.out.println(JSON.parse(response.asString()));
			HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
			Log.message("<b>========================================================================================</b>");
			Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
			Log.message("<b>========================================================================================</b>");
			Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
			Log.message("<b>content type--------------------> </b>" + response.getContentType());
			Log.message("<b>Response Time--------------------> </b>" + response.getTime());
			Log.message("<b>========================================================================================</b>");
			Log.message("<b>***************************	  API RESPONSE	************************************</b>");
			Log.message("<b>========================================================================================</b>");

			Log.message("<B>Response: </B></n>" + hmap.toString());

			Log.message(i++ + ". Verifying the Order Number is matching with reponse.");
			Log.softAssertThat(hmap.get("orderNumber").equals(ordernumber),
					"Account number is matching",
					"Account number is not matching");

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} //finally
	}
	
	@Test(description= "Testing the account update email with valid detail")
	public void TC_API_ACCOUNTUPDATEEMAIL_001() throws Exception {
		
		//Initalizing the URL
		String baseURL = envAPIData.get("baseurls");
		String accountupdateUrl = envAPIData.get("accountupdateemail");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData1.get("body");
		
		String body = " {\n" +
				"	\"accountKey\": \"8004963\",\n" +
				"	\"email\": \"abdul240589@gmail.com\",\n"+
				"	\"requestedFrom\": \"KingSizeDirect\",\n"+
				"	\"firstName\": \"Abdul\", \n"+
				"	\"lastName\": \"Sardhar\" ,\n"+
				"	\"billingAddress\": {\n " +
		"	\"firstName\": \"Abdul\", \n"+
		"	\"lastName\": \"Sardhar\" ,\n"+
		"	\"address1\": \"600 Reed Road\", \n"+
		"	\"address2\": \"Suite 302\",\n"+
		"	\"city\": \"Charlotte\",\n"+
		"	\"state\": \"NC\", \n"+
		"	\"zipCode\": \"28205\"\n" +
		"	}, \n "+
		" \"dayPhone\": \"2223334444\", \n"+
		"\"nightPhone\": \"2223334444\" \n"+ 
		"}";
		
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers, input, body, accountupdateUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.testCaseResult();
	}
	
	@Test(description= "Testing the accountI with valid account detail")
	public void TC_API_ACCOUNT_001() throws Exception {
		
		//Initalizing the URL
		String baseURL = envAPIData.get("baseurls");
		String accountcreationUrl = envAPIData.get("account");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		//input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData1.get("body");
		
		String body = " {\n" +
				  "\"accountKey\": \"962852\",\n " +
				  "\"firstName\": \"Abdul\",\n" +
				  "\"lastName\": \"sardhar\",\n" +
				  "\"email\": \"abdul240589@gmail.com\",\n" +
				  "\"password\": \"aaspire@12\", \n" +
				  "\"previousEmail\": \"abdul240589@gmail.com\",\n" +
				  "\"dayPhone\": \"3334447777\",\n" +
				  "\"eveningPhone\": \"3334447777\",\n" +
				  "\"mobilePhone\": \"3334447777\",\n" +
				  "\"currentlyActiveOn\": \"Roamans\", \n" +
				  " \"birthMonth\": 11,\n" +
				  "\"catalogCustomerId\": \"xxxx\",\n" +
				  "\"isEmailInactive\": true,\n" +
				  "\"affiliateId\": \"NC\",\n" +
				  "\"affiliateLocation\": \"US\" \n "+ 
				"}"; 
		
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers, input, body, accountcreationUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.testCaseResult();
	}
	
	@Test(description= "To verify the Return order API with valid details")
	public void TC_API_ORDER_004() throws Exception {
		
		//Initializing the URL
		String baseURL = envAPIData.get("BaseOrderURL");
		String productid = envAPIData.get("returnOrder");
		String accountcreationUrl = envAPIData.get("returnRequestUrl").replace("!product!", productid);
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		headers.put("Accept","text/plain");
		
		String body = "{\n"+
				 "\"lineItems\": [ \n"+
					"{\n"+
				      "\"productId\": \"300757689\",\n"+
				      "\"quantity\": 1, \n"+
				      "\"reason\": \"Too Big\" \n"+
				    "}\n"+
				  "]\n"+
				"}"; 
		
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers, input, body, accountcreationUrl);
		
		System.out.println(JSON.parse(response.asString()));
		HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode() == 200,
				"Verifying the status code in the API reponse", 
				"API getting the successful response : 200", 
				"API getting the successful response");
		
		Log.testCaseResult();
	}
	
	@Test(description= "To verify the Return order API with invalid product id number")
	public void TC_API_ORDER_005() throws Exception {		

		//Initializing the URL
		String baseURL = envAPIData.get("BaseOrderURL");
		String productid = envAPIData.get("returnOrder");
		String accountcreationUrl = envAPIData.get("returnRequestUrl").replace("!product!", productid);
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		headers.put("Accept","text/plain");
		
		String body = "{\n"+
				 "\"lineItems\": [ \n"+
					"{\n"+
				      "\"productId\": \"300757\",\n"+
				      "\"quantity\": 1, \n"+
				      "\"reason\": \"Too Big\" \n"+
				    "}\n"+
				  "]\n"+
				"}"; 
		
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers, input, body, accountcreationUrl);
	
		System.out.println(JSON.parse(response.asString()));
		HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode() == 400,
				"Verifying the status code in the API reponse", 
				"API getting the successful response : 400", 
				"API getting the successful response");

		Log.softAssertThat(hmap.get("message").equals("One or more items could not be found"),
				"Valid error message should be displayed when trying with invlaid product id.", 
				"Valid error message is displayed!!!", 
				"Invalid error message id displayed!!!");
		
		Log.testCaseResult();
	}
	
	@Test(description= "To verify the Return order API with invalid order id number")
	public void TC_API_ORDER_006() throws Exception {		

		//Initializing the URL
		String baseURL = envAPIData.get("BaseOrderURL");
		String productid = envAPIData.get("invalidReturnOrder");
		String accountcreationUrl = envAPIData.get("returnRequestUrl").replace("!product!", productid);
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		headers.put("Accept","text/plain");
		
		String body = "{\n"+
				 "\"lineItems\": [ \n"+
					"{\n"+
				      "\"productId\": \"300757689\",\n"+
				      "\"quantity\": 1, \n"+
				      "\"reason\": \"Too Big\" \n"+
				    "}\n"+
				  "]\n"+
				"}"; 
		
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers, input, body, accountcreationUrl);
	
		System.out.println(JSON.parse(response.asString()));
		HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode() == 404,
				"Verifying the status code in the API reponse", 
				"API getting the successful response : 404", 
				"API getting the successful response");
		

		Log.softAssertThat(hmap.get("message").equals("The order with id "+ productid +" could not be found"),
				"Valid error message should be displayed when trying with invlaid product id.", 
				"Valid error message is displayed!!!", 
				"Invalid error message id displayed!!!");
		
		Log.testCaseResult();
	}
	
	@SuppressWarnings("unchecked")
	@Test(description= "To verify the place order API with valid order detail")
	public void TC_API_ORDER_007() throws Exception {		
		
		//Initializing the URL
		String baseURL = envAPIData.get("BaseOrderURL");
		//String productid = accountData1.get("invalidReturnOrder");
		String orderurl = envAPIData.get("orderurl");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","text/xml");
		Map<String, String> input = new HashMap<>();
		headers.put("Accept","text/plain");
		
		String body = RestAssuredAPI.generateStringFromResource("./src/main/resources/APITestdata/Order.xml");
		
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL,headers,body,orderurl);
		System.out.println(JSON.parse(response.asString()));
		
		JSONParser parser = new JSONParser();
		JSONArray data = (JSONArray) parser.parse(response.asString());
		Log.message(data.toJSONString());
		
		JSONObject jSONObject = (JSONObject) data.get(0);
		Gson gson = new Gson();
		Map<String, String> myMap = gson.fromJson(jSONObject.toString(),Map.class);
		Log.message(myMap.keySet().toString());
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode() == 200,
				"Verifying the status code in the API reponse", 
				"API getting the successful response : 200", 
				"API getting the successful response");
		
		Log.softAssertThat(myMap.get("status").equals("Accepted"),
				"Verifying the status message in the API reponse", 
				"API getting the successful response and the status is dispayed as : Accepted", 
				"Invalid status message is displayed.");
		
		Log.message("Generated Order Number" + myMap.get("orderNumber"));
		
		Log.testCaseResult();
	}
		
	@Test(description= "To verify the place order API with invalid order detail")
	public void TC_API_ORDER_008() throws Exception {
		
		//Initializing the URL
		String baseURL = envAPIData.get("BaseOrderURL");
		//String productid = accountData1.get("invalidReturnOrder");
		String orderurl = envAPIData.get("orderurl");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","text/xml");
		Map<String, String> input = new HashMap<>();
		headers.put("Accept","text/plain");
		
		//String body = RestAssuredAPI.generateStringFromResource("./src/main/resources/APITestdata/Order.xml");
		String body = "{\n"+
				 "\"lineItems\": [ \n"+
					"{\n"+
				      "\"productId\": \"300757689\",\n"+
				      "\"quantity\": 1, \n"+
				      "\"reason\": \"Too Big\" \n"+
				    "}\n"+
				  "]\n"+
				"}"; 
		
		Log.message("<B>Request Body:<B>\n");
		Log.message(body);
		Response response = RestAssuredAPI.POST(baseURL,headers,body,orderurl);
		Gson gson = new Gson();
		Map<String, String> myMap = gson.fromJson(JSON.parse(response.asString()).toString(),Map.class);
		Log.message("<B>Response Body:<B>\n");
		Log.message(JSON.parse(response.asString()).toString());
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode() == 400,
				"Verifying the status code in the API reponse", 
				"API getting the successful response : 400", 
				"API getting the successful response");
		
		Log.softAssertThat(myMap.get("message").equals("No orders have been specified"),
				"Verifying the error message", 
				"Valid error message is displayed - No orders have been specified", 
				"Invalid error message is displayed.");
		
		Log.softAssertThat(myMap.get("code").equals("BadRequest"),
				"Verifying the error message", 
				"Valid error code is displayed - BadRequest", 
				"Invalid error message is displayed.");
		
		Log.testCaseResult();
	}
	
	@Test(description= "To verify the place single order API with valid order detail")
	public void TC_API_ORDER_009() throws Exception {		

		//Initializing the URL
		String baseURL = envAPIData.get("BaseOrderURL");
		//String productid = accountData1.get("invalidReturnOrder");
		String orderurl = envAPIData.get("singleOrderurl");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","text/xml");
		Map<String, String> input = new HashMap<>();
		headers.put("Accept","text/plain");
		
		String body = RestAssuredAPI.generateStringFromResource("./src/main/resources/APITestdata/SingleOrder.xml");
		
		Log.message("<B>Request Body:<B>\n");
		Log.message(body);
		Response response = RestAssuredAPI.POST(baseURL,headers,body,orderurl);
		System.out.println(JSON.parse(response.asString()));
		
		Gson gson = new Gson();
		Map<String, String> myMap = gson.fromJson(JSON.parse(response.asString()).toString(),Map.class);
		
		Log.message("<B>Response Body:<B>\n");
		Log.message(JSON.parse(response.asString()).toString());
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode() == 200,
				"Verifying the status code in the API reponse", 
				"API getting the successful response : 200", 
				"API getting the successful response");
		
		Log.softAssertThat(myMap.get("status").equals("Accepted"),
				"Verifying the status message in the API reponse", 
				"API getting the successful response and the status is dispayed as : Accepted", 
				"Invalid status message is displayed.");
		
		Log.message("Generated Order Number" + myMap.get("orderNumber"));
		
		Log.testCaseResult();
	}
		
	@Test(description= "To verify the place single order API with invalid order detail")
	public void TC_API_ORDER_010() throws Exception {		

		//Initializing the URL
		String baseURL = envAPIData.get("BaseOrderURL");
		//String productid = accountData1.get("invalidReturnOrder");
		String orderurl = envAPIData.get("singleOrderurl");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","text/xml");
		Map<String, String> input = new HashMap<>();
		headers.put("Accept","text/plain");
		
		//String body = RestAssuredAPI.generateStringFromResource("./src/main/resources/APITestdata/Order.xml");
		String body = "{\n"+
				 "\"lineItems\": [ \n"+
					"{\n"+
				      "\"productId\": \"300757689\",\n"+
				      "\"quantity\": 1, \n"+
				      "\"reason\": \"Too Big\" \n"+
				    "}\n"+
				  "]\n"+
				"}"; 
		
		Log.message("<B>Request Body:<B>\n");
		Log.message(body);
		Response response = RestAssuredAPI.POST(baseURL,headers,body,orderurl);
		Gson gson = new Gson();
		Map<String, String> myMap = gson.fromJson(JSON.parse(response.asString()).toString(),Map.class);
		Log.message("<B>Response Body:<B>\n");
		Log.message(JSON.parse(response.asString()).toString());
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode() == 400,
				"Verifying the status code in the API reponse", 
				"API getting the successful response : 400", 
				"API getting the successful response");
		
		Log.softAssertThat(myMap.get("message").equals("No orders have been specified"),
				"Verifying the error message", 
				"Valid error message is displayed - No orders have been specified", 
				"Invalid error message is displayed.");
		
		Log.softAssertThat(myMap.get("code").equals("BadRequest"),
				"Verifying the error message", 
				"Valid error code is displayed - BadRequest", 
				"Invalid error message is displayed.");
		
		Log.testCaseResult();
	}
}
