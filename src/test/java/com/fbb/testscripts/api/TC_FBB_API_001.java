package com.fbb.testscripts.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.externalAPI.RestAssuredAPI;
import com.fbb.support.BaseTest;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.jayway.restassured.response.Response;

@Listeners(EmailReport.class)
public class TC_FBB_API_001 extends BaseTest{
	
	public static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("envAPI");
	EnvironmentPropertiesReader environmentPropertiesReader;
	String runPltfrm = Utils.getRunPlatForm();
	
	@Test(description = "Verify the account update email API with valid API details.")
	public void TC_API_POST_001() throws Exception
	{		
		//Initializing the URL
		String baseURL = accountData.get("baseurls");
		String accountupdateUrl = accountData.get("accountupdateemail");
		
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		Map<String, String> input = new HashMap<>();
		input.put("accountKey", "8004963");
		headers.put("Accept","application/json");
		//String body = accountData.get("body");
		
		
		String body = "";
		
		
		Log.message("Request Body\n" + body);
		Log.message("Request Body\n" + body);
		Response response = RestAssuredAPI.POST(baseURL, headers,input,body,accountupdateUrl);
	
		//System.out.println(JSON.parse(response.asString()));
		//HashMap<String, String> hmap = TestRailAPICalls.convertJSON(JSON.parse(response.asString()));
		
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************API VERIFICATION DETAILS*************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.message("<b>Status Code--------------------> </b>" + response.getStatusCode());
		Log.message("<b>Response Time--------------------> </b>" + response.getTime());
		Log.message("<b>========================================================================================</b>");
		Log.message("<b>***************************	  API RESPONSE	************************************</b>");
		Log.message("<b>========================================================================================</b>");
		
		Log.softAssertThat(response.getStatusCode()==200, "API should displayed the valid status",
				"API is displayed 200 as status code when giving valid request", 
				"API displayed the invalid status code");
		
		Log.testCaseResult();
		
		 
	}
	
	
	
	
}
