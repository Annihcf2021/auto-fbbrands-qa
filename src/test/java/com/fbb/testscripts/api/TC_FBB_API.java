package com.fbb.testscripts.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.externalAPI.RestAssuredAPI;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.Log;
import com.jayway.restassured.response.Response;

@Listeners(EmailReport.class)
public class TC_FBB_API extends BaseTest{


	@Test (dataProviderClass = DataProviderUtils.class, dataProvider = "api_data_provider")
	public void TC_FBB_API_TEST(HashMap<String, String> data) throws Exception
	{
		Log.testCaseInfo(data.get("api_description"));
		try {
		String baseURL = data.get("api_base");
		String api = data.get("api_url");
		Response response = null;
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		headers.put("Accept","application/json");

		HashMap<String, String> api_path_param = stringToMap(data.get("api_path_parameters"));
		HashMap<String, String> api_param = stringToMap(data.get("api_parameters"));

		if(!api_path_param.isEmpty()) {
			for(String key : api_path_param.keySet()) {
				api = api.replace("{"+key+"}", api_path_param.get(key));
			}
		}

		if(!api_param.isEmpty()) {
			for(String key : stringToMap(data.get("api_input_data")).keySet()) {
				api_param.put("{"+key+"}", api_param.get(key));
			}
		}

		switch(data.get("api_type")) {
		case "get":
			response = RestAssuredAPI.GETBASEURL(baseURL, api);
			break;

		case "post":
			String body = data.get("api_input_data");
			response = RestAssuredAPI.POST(baseURL, headers, api_param,body,api);
			break;

		case "delete":
			response = RestAssuredAPI.DELETE(baseURL, headers, api_param, api);
			break;

		case "put":
			response = RestAssuredAPI.PUT(baseURL, headers, api_param, data.get("api_input_data"), api);
			break;
		}
		int status_code = response.getStatusCode();
		Log.message("API :: " + api);
		Log.message("API Response Code:: " + status_code);
		Log.message("API Response Line:: " + response.getStatusLine());
		Log.event(response.asString());
		
		if(status_code == 503) {
			Log.fail("Could not contact API Service. Kindly contact your API Developer for more details.");
		}else {
			Log.softAssertThat(checkStatus(response, data.get("expected_status_code")), 
					"Expected Status Code :: " + data.get("expected_status_code"),
					"Received expected status code.", 
					"Response was unsuccessfull. Status Code :: " + response.statusCode());
		}
		Log.testCaseResult();
		}catch(Exception e) {
			Log.exception(e);
		}finally {
			Log.endTestCase();
		}
	}


	public HashMap<String, String> stringToMap(String input)throws Exception{
		HashMap<String, String> returnData = new HashMap<String, String>();
		input = input.replaceAll("\n", "");
		String datas[] = input.split("\\,");
		for(String data : datas) {
			if(!StringUtils.isBlank(data))
				returnData.put(data.split(":")[0], data.split(":")[1]);
		}

		return returnData;
	}

	public boolean checkStatus(Response response, String expected_codes)throws Exception{
		boolean status = false;
		String[] expected = expected_codes.split("\\|");
		for(String expected_code : expected) {
			if(response.statusCode() == Integer.parseInt(expected_code))
				status = true;
		}
		return status;

	}
}
