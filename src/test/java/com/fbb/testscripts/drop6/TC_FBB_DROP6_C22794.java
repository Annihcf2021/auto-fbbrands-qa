package com.fbb.testscripts.drop6;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.customerservice.ContactUsPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.customerservice.ShoppingPage;
import com.fbb.support.BaseTest;
import com.fbb.support.CollectionUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP6_C22794 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	

	@Test( groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP6_C22794(String browser) throws Exception{

		Log.testCaseInfo();
		
		final WebDriver driver = WebDriverFactory.get(browser);
		String leftNavigation = TestData.get("left_navigation_article");

		int i=1;
		try
		{
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			CustomerService customerservice = homePage.footers.navigateToCustomerService();
			Log.message(i++ +". Navigated to Customer Service page", driver);
			
			ContactUsPage contactUS = customerservice.clickOnMailWithUs();
			Log.message(i++ + ". Navigated to Contact Us page.", driver);
			
			//Step-1: Verify the functionality of Left Navigation
			if(Utils.isDesktop()) {
				Log.softAssertThat(contactUS.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divContactUsBody", "navLeftNavigation", contactUS), 
			            "Left navigation should be displayed left of the Content Body", 
			            "Left navigation is displayed left of the Content Body", 
			            "Left navigation is not displayed left of the Content Body", driver);
		        
		        List<String> LeftNavigationArticle = Arrays.asList(leftNavigation.split("\\|"));
		        Log.message("LeftNavigationArticle:: " + LeftNavigationArticle.toString());
		        Log.message("contactUS.csNav.LeftNavigationArticleList:: " + contactUS.csNav.LeftNavigationArticleList().toString());
		        
				Log.softAssertThat(CollectionUtils.compareTwoList(LeftNavigationArticle, contactUS.csNav.LeftNavigationArticleList()), 
						"System should display the left navigation based on the sort order.", 
						"System displays the left navigation based on the sort order.", 
						"System does not display the left navigation based on the sort order.", driver);
				
				Log.softAssertThat(contactUS.elementLayer.verifyVerticalAllignmentOfElements(driver, "csLabelDesktop", "leftNavigationSection", contactUS), 
						"The root customer service folder should appears as a heading above the sorted assets.", 
						"The root customer service folder appears as a heading above the sorted assets.", 
						"The root customer service folder does not appears as a heading above the sorted assets.", driver);
			}
			//Tablet & Mobile
			else {
				Log.softAssertThat(contactUS.elementLayer.verifyElementTextContains("MenuMobile", "Contact Us", contactUS), 
						"Left Navigation Drop Down Menu should display the currently viewed article's title", 
			            "Left Navigation Drop Down Menu displayes the currently viewed article's title", 
			            "Left Navigation Drop Down Menu did not display the currently viewed article's title.", driver);
				
				List<String> LeftNavigationArticle = Arrays.asList(leftNavigation.split("\\|"));
				Log.softAssertThat(CollectionUtils.compareTwoList(LeftNavigationArticle, contactUS.csNav.DropDownArticleList()), 
						"On clicking the Left Navigation Drop Down Menu, system should display the list of available articles", 
						"System displays the list of available articles", 
						"System did not display the list of available articles.", driver);
			}
			
			if(Utils.isMobile()) {
				Log.softAssertThat(contactUS.elementLayer.verifyVerticalAllignmentOfElements(driver, "breadcumbMobile", "navLeftNavigationMobile", contactUS), 
			            "Dropdown should be displayed below the Breadcrumb instead of Left Navigation.", 
			            "Dropdown is displayed below the Breadcrumb.", 
			            "Dropdown is not displayed below the Breadcrumb.", driver);
			}
			
			if(Utils.isTablet()) {
				Log.softAssertThat(contactUS.elementLayer.verifyVerticalAllignmentOfElements(driver, "breadcumbTablet", "navLeftNavigationMobile", contactUS), 
			            "Dropdown should be displayed below the Breadcrumb instead of Left Navigation.", 
			            "Dropdown is displayed below the Breadcrumb.", 
			            "Dropdown is not displayed below the Breadcrumb.", driver);
			}
			
			//Step-2: Verify the functionality of Article Heading
			if(!Utils.isDesktop()) {
				Log.softAssertThat(contactUS.elementLayer.verifyVerticalAllignmentOfElements(driver, "navLeftNavigationMobile", "csCurrentHeading", contactUS), 
						"Article Heading should be displayed below the Left Navigation drop down menu", 
						"Article Heading is displayed below the Left Navigation drop down menu", 
						"Article Heading is not displayed below the Left Navigation drop down menu", driver);
	        }
			else {
		        Log.softAssertThat(contactUS.elementLayer.verifyVerticalAllignmentOfElements(driver, "csLabelDesktop", "csCurrentHeading", contactUS), 
		        		"Article Heading should be displayed below the Customer Service Label.", 
		        		"Article Heading is displayed below the Customer Service Label.", 
		        		"Article Heading is not displayed below the Customer Service Label.", driver);
			}
	      
	      	Log.softAssertThat(contactUS.elementLayer.verifyPageElements(Arrays.asList("csCurrentHeading"),contactUS), 
	      			"System should display the content asset's Name attribute.", 
	      			"System displays the content asset's Name attribute.", 
	      			"System does not display the content asset's Name attribute.", driver);	
		      
			//Step-3: Verify the functionality of Content Body
			Log.softAssertThat(contactUS.elementLayer.verifyVerticalAllignmentOfElements(driver, "csCurrentHeading", "formSubText", contactUS)
							|| contactUS.elementLayer.verifyVerticalAllignmentOfElements(driver, "csCurrentHeading", "formSubTextRM", contactUS), 
					"Content Body should be displayed below the Article Heading", 
					"Content Body is displayed below the Article Heading", 
					"Content Body is not displayed below the Article Heading", driver);
			  
			//Step 1 - Continuation
			ShoppingPage shoppingPage = contactUS.navigateToShoppingPage();
			Log.message(i++ + ". Navigated to the Shopping page");
			  
			Log.softAssertThat(shoppingPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"),shoppingPage), 
					"System should navigated to the shopping page.", 
					"System navigated to the shopping page.", 
					"System not navigated to the shopping page.", driver);
			  
			if(!Utils.isDesktop()) {
				shoppingPage.expandOrCollapseLeftNavigationMenu(true);
				Log.message(i++ + ". Clicked on the left navigation in the mobile/tablet");
			     
				Log.softAssertThat(shoppingPage.elementLayer.verifyPageElements(Arrays.asList("leftNavigationContentMobile"),shoppingPage), 
						"System should display the left navigation menus.", 
						"System is displaying the left navigation menus.", 
						"System is not displaying the left navigation menus.", driver);
			}
			  
			//Step-4: Verify the functionality of Contact Module
			Log.softAssertThat(shoppingPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "articleBody", "contactUsSection", shoppingPage), 
					"Contact Module should be displayed below the Content Body",
					"Contact Module is displayed below the Content Body", 
					"Contact Module is not displayed below the Content Body", driver);
			
			Log.reference("Covered further functionality in the test case id: C22773 Step 5");
			
			Log.reference("Covered brand specific content asset functionality in test case id: C22774 Step 4");
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e)
		{
			Log.exception(e, driver);
		} // Ending catch block
		finally{
			Log.endTestCase(driver);
		}// Ending finally
	}

}//TC_FBB_DROP6_C22783
