package com.fbb.testscripts.drop6;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.customerservice.ContactPriority;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP6_C22785 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP6_C22785(String browser) throws Exception {

		Log.testCaseInfo();
		final WebDriver driver = WebDriverFactory.get(browser);

		int i = 1;
		String priorityURL = "/contactuspriority?email=wsu@fbbrands.com&trackingid=31409AA";
		try {
			new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			driver.get(Utils.getWebSite()+ priorityURL);
			ContactPriority contactPriority = new ContactPriority(driver).get();
			Log.message(i++ + ". Navigated to 'Contact Priority' Page!", driver);

			if(Utils.isDesktop()) {
				Log.softAssertThat(contactPriority.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblCustomerService_Desktop", "lblContactPriority", contactPriority),
						"Check the contact priority heading is displayed below the customer service label", 
						"The contact priority heading is displayed below the customer service label", 
						"The contact priority heading is not displayed below the customer service label", driver);
			}

			if(BrandUtils.isBrand(Brand.rm)) {
				//Roaman's uses a Roaman's only div element
				Log.softAssertThat(contactPriority.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblContactPriority", "lblSubHeadingRM", contactPriority),
						"Check the contact priority heading is displayed above the sub heading", 
						"The contact priority heading is displayed above the sub heading", 
						"The contact priority heading is not displayed above the sub heading", driver);
	
				Log.softAssertThat(contactPriority.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblSubHeadingRM", "sectionContactPriorityForm", contactPriority),
						"Check the contact priority form is displayed below the sub heading", 
						"The contact priority form is displayed below the sub heading", 
						"The contact priority form is not displayed below the sub heading", driver);
			}
			else {
				Log.softAssertThat(contactPriority.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblContactPriority", "lblSubHeading", contactPriority),
						"Check the contact priority heading is displayed above the sub heading", 
						"The contact priority heading is displayed above the sub heading", 
						"The contact priority heading is not displayed above the sub heading", driver);
	
				Log.softAssertThat(contactPriority.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblSubHeading", "sectionContactPriorityForm", contactPriority),
						"Check the contact priority form is displayed below the sub heading", 
						"The contact priority form is displayed below the sub heading", 
						"The contact priority form is not displayed below the sub heading", driver);
			}
			
			Log.softAssertThat(contactPriority.elementLayer.verifyEmailAddress("txtEmailId", contactPriority),
					"Check the contact priority email address pre-populated and for the email has correct format", 
					"The contact priority email address pre-populated and for the email has correct format", 
					"The contact priority email address is not pre-populated", driver);
			
			Log.softAssertThat(contactPriority.elementLayer.verifyPlaceHolderMovesAbove("txtEmailId", "lblEmailIdPlaceHolder", "test@fbbrands.com", contactPriority),
					"Check the Email ID placeholder moving upwards when entering text in the mail Id field", 
					"The Email ID placeholder moving upwards when entering text in the mail Id field", 
					"The Email ID placeholder not moving upwards when entering text in the mail Id field", driver);
			
			Log.softAssertThat(contactPriority.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtEmailId", "txtMessage", contactPriority),
					"Check the email Id field is displaying above the message text area field", 
					"The email Id field is displaying above the message text area field", 
					"The email Id field is not displaying above the message text area field", driver);
			
			Log.softAssertThat(contactPriority.elementLayer.verifyPlaceHolderMovesAbove("txtMessage", "lblMessagePlaceHolder", "Message from the customer", contactPriority),
					"Check the Message placeholder moving upwards when entering text in the Message field", 
					"The Message placeholder moving upwards when entering text in the Message field", 
					"The Message placeholder not moving upwards when entering text in the Message field", driver);
			
			contactPriority.enterMessage("Message from the customer");
			Log.message(i++ + ". Entered text in the Message field");
			
			Log.softAssertThat(contactPriority.elementLayer.verifyElementTextContains("txtMessage", "Message from the customer", contactPriority),
					"Check the Message field is editable", 
					"The Message field is editable", 
					"The Message field is not editable", driver);
			
			contactPriority.enterMessage("");
			Log.message(i++ + ". Cleared message field");
			
			contactPriority.clickOnSubmitButton();
			Log.message(i++ + ". Clicked on Submit button");
			
			Log.softAssertThat(contactPriority.elementLayer.verifyPageElements(Arrays.asList("txtMessageError"), contactPriority),
					"Check the message field throws error message when the message field is empty", 
					"The message field throws error message when the message field is empty", 
					"The message field is not throwing error message when the message field is empty", driver);
			
			Log.softAssertThat(contactPriority.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnContactForm", "btnContactPageModule", contactPriority),
					"Check the Contact module should be displayed below the Contact Form", 
					"The Contact module is displayed below the Contact Form", 
					"The Contact module is not displayed below the Contact Form", driver);
			
			Log.softAssertThat(contactPriority.elementLayer.verifyCssPropertyForElement("sectionContactPrioritySubmitBtn", "float", "right", contactPriority),
					"Check the submit button is aligned right side in the form", 
					"The submit button is aligned right side in the form", 
					"The submit button is not aligned right side in the form", driver);
			
			Log.softAssertThat(contactPriority.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnContactForm", "txtMessage", contactPriority),
					"Check the submit button is displaying below the message text box", 
					"The submit button is displaying below the message text box", 
					"The submit button is not displaying below the message text box", driver);
			
			contactPriority.enterEmailAddress("");
			Log.message(i++ + ". Cleared Email Id field");
			
			contactPriority.clickOnSubmitButton();
			Log.message(i++ + ". Clicked on Submit button");
			
			Log.softAssertThat(contactPriority.elementLayer.verifyPageElements(Arrays.asList("txtEmailIdError"), contactPriority),
					"Check the Email ID field is editable and mandatory", 
					"The Email ID field is editable and mandatory", 
					"The Email ID field is not editable", driver);
			
			contactPriority.enterEmailAddress("wsu@fbbrands");
			Log.message(i++ + ". Entering invalid Email Id");
			
			contactPriority.clickOnSubmitButton();
			Log.message(i++ + ". Clicked on Submit button");
			
			Log.softAssertThat(contactPriority.elementLayer.verifyPageElements(Arrays.asList("txtEmailIdError"), contactPriority),
					"Check the Email ID field is validating the Email Id format", 
					"The Email ID field validating the Email Id format", 
					"The Email ID field not validating the Email Id format", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_HEADER_C22785

}//TC_FBB_DROP6_C22783
