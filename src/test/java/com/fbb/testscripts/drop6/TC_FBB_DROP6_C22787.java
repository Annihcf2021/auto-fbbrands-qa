package com.fbb.testscripts.drop6;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.customerservice.ContactUsPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.customerservice.FaqPage;
import com.fbb.pages.customerservice.GiftCardsPage;
import com.fbb.pages.customerservice.PlatinumCardsPage;
import com.fbb.pages.customerservice.ReturnAndExchangePage;
import com.fbb.pages.customerservice.ShippingInformationPage;
import com.fbb.pages.customerservice.ShoppingPage;
import com.fbb.pages.customerservice.SizeChartPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP6_C22787 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP6_C22787(String browser) throws Exception {

		// Get the web driver instance
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		String hightlightColor = TestData.get("highlight_color");

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			CustomerService customerservice = homePage.footers.navigateToCustomerService();
			Log.message(i++ + ". Navigated to Customer Service page", driver);
			
			//Step-1: Verify the functionality of Left Navigation
			ContactUsPage contactUsPage = customerservice.clickOnMailWithUs();
			Log.message(i++ +". Navigated to Contact Us page.");
			Log.softAssertThat(contactUsPage.elementLayer.verifyPageElements(Arrays.asList("csCurrentHeading"),contactUsPage),
					"ContactUsPage should be displayed.",
					"ContactUsPage displayed!",
					"ContactUsPage not displayed", driver); 
			
			ShippingInformationPage shippingInformationpage = contactUsPage.csNav.navigateToShippingInformationPage();
			Log.message(i++ + ". Navigated to CS Shipping Info page", driver);
			Log.softAssertThat(shippingInformationpage.elementLayer.verifyPageElements(Arrays.asList("csCurrentHeading"),shippingInformationpage),
					"Shipping page should be displayed.",
					"Shipping  page  displayed!",
					"Shipping page not displayed", driver);

			ReturnAndExchangePage reAndExchangePage = contactUsPage.csNav.navigateToReturnAndExchangePage();
			Log.message(i++ + ". Navigated to CS Return & Exchange page", driver);
			Log.softAssertThat(reAndExchangePage.elementLayer.verifyPageElements(Arrays.asList("csCurrentHeading"),reAndExchangePage),
					"Return and Exchange page should be displayed.",
					"Return and Exchange page  displayed!",
					"Return and Exchange page not displayed", driver);

			PlatinumCardsPage platinumCardPage = contactUsPage.csNav.navigateToPlatinumCardsPage();
			Log.message(i++ + ". Navigated to CS Platinum Card page", driver);
			Log.softAssertThat(platinumCardPage.elementLayer.verifyPageElements(Arrays.asList("csCurrentHeading"),platinumCardPage),
					"Platinum Card Page should be displayed.",
					"Platinum Card Page displayed!",
					"Platinum Card Page not displayed", driver);

			ShoppingPage shoppingPage=contactUsPage.csNav.navigateToShoppingPage();
			Log.message(i++ + ". Navigated to CS Shopping page", driver);
			Log.softAssertThat(shoppingPage.elementLayer.verifyPageElements(Arrays.asList("csCurrentHeading"),shoppingPage),
					"Shopping Page should be displayed.",
					"Shopping Page is displayed!",
					"Shopping Page is not displayed", driver);

			GiftCardsPage giftCardPage=contactUsPage.csNav.navigateToGiftCardsPage();
			Log.message(i++ + ". Navigated to CS Gift Card page", driver);
			Log.softAssertThat(giftCardPage.elementLayer.verifyPageElements(Arrays.asList("csCurrentHeading"),giftCardPage),
					"GiftCardPage should be displayed.",
					"GiftCardPage displayed!",
					"GiftCardPage not displayed", driver);

			SizeChartPage sizeChartPage=contactUsPage.csNav.navigateToSizeChartPage();
			Log.message(i++ + ". Navigated to CS Size & Fit page", driver);
			Log.softAssertThat(sizeChartPage.elementLayer.verifyPageElements(Arrays.asList("csCurrentHeading"),sizeChartPage),
					"SizeChartPage should be displayed.",
					"SizeChartPage displayed!",
					"SizeChartPage not displayed", driver);  

			FaqPage faqPage=contactUsPage.csNav.navigateToFaqPage();
			Log.message(i++ + ". Navigated to CS FAQ page", driver);
			Log.softAssertThat(faqPage.elementLayer.verifyPageElements(Arrays.asList("csCurrentHeading"),faqPage),
					"FaqPage should be displayed.",
					"FaqPage displayed!",
					"FaqPage not displayed", driver); 
			
			if(Utils.isDesktop()) {
				Log.softAssertThat(faqPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "faqContent", "navLeftNavigation", faqPage), 
						"Left navigation should be displayed left of the Content Body", 
						"Left navigation is displayed left of the Content Body", 
						"Left navigation is not displayed left of the Content Body", driver);
				
				Log.softAssertThat(faqPage.elementLayer.verifyComputedCssPropertyForElement("lnkCurrentPage", "color", hightlightColor, faqPage), 
						"Currently active article page should be highlighted in the Left Navigation", 
						"Currently active article page is highlighted in the Left Navigation", 
						"Currently active article page is not highlighted in the Left Navigation", driver);
			}
			if(Utils.isTablet())  {
				Log.softAssertThat(faqPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "breadcumbTablet", "navLeftNavigationMobile", faqPage), 
						"Dropdown should be displayed below the Breadcrumb instead of Left Navigation.", 
						"Dropdown is displayed below the Breadcrumb.", 
						"Dropdown is not displayed below the Breadcrumb.", driver);
			}
			if(Utils.isMobile())  {
				Log.softAssertThat(faqPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "breadcumbMobile", "navLeftNavigationMobile", faqPage), 
						"Dropdown should be displayed below the Breadcrumb instead of Left Navigation.", 
						"Dropdown is displayed below the Breadcrumb.", 
						"Dropdown is not displayed below the Breadcrumb.", driver);
			}
			if(!Utils.isDesktop()) {
				Log.softAssertThat(faqPage.elementLayer.verifyAttributeForElement("navMobileCurrentSelection", "innerHTML", "FAQ", faqPage), 
						"Left Navigation Drop Down Menu should display the currently viewed article's title", 
						"Left Navigation Drop Down Menu displayes the currently viewed article's title", 
						"Left Navigation Drop Down Menu did not display the currently viewed article's title", driver);
			}
			
			//Step-2: Verify the functionality of Article Heading
			if(Utils.isMobile()) {
				Log.softAssertThat(faqPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "navLeftNavigationMobile", "csCurrentHeading", faqPage), 
						"Article Heading should be displayed below the Left Navigation drop down menu", 
						"Article Heading is displayed below the Left Navigation drop down menu", 
						"Article Heading is not displayed below the Left Navigation drop down menu", driver);
			}
			if(Utils.isTablet()) {
				Log.softAssertThat(faqPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "csLabelTablet", "csCurrentHeading", faqPage), 
						"Article Heading should be displayed below the Customer Service Label.", 
						"Article Heading is displayed below the Customer Service Label.", 
						"Article Heading is not displayed below the Customer Service Label.", driver);
			}
			if(Utils.isDesktop()) {
				Log.softAssertThat(faqPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "csLabelDesktop", "csCurrentHeading", faqPage), 
						"Article Heading should be displayed below the Customer Service Label.", 
						"Article Heading is displayed below the Customer Service Label.", 
						"Article Heading is not displayed below the Customer Service Label.", driver);
			}
			
			Log.softAssertThat(faqPage.elementLayer.verifyPageElements(Arrays.asList("csCurrentHeading"),faqPage), 
					"System should display the content asset's Name attribute.", 
					"System displays the content asset's Name attribute.", 
					"System does not display the content asset's Name attribute.", driver);

			//Step-3: Verify the functionality of Content Body
			Log.softAssertThat(faqPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "csCurrentHeading", "faqContent", faqPage), 
					"Content Body should be displayed below the Article Heading", 
					"Content Body is displayed below the Article Heading", 
					"Content Body is not displayed below the Article Heading", driver);
			
			Log.softAssertThat(faqPage.verifyAllQAContentState(),
					"Section Elements should be in collapsed mode",
					"Section Elements is in collapsed mode",
					"Section Elemenst not in collapsed mode", driver); 
			
			Log.softAssertThat(faqPage.verifyArrowSymbol(),
					"caret symbol should be Available in the corner of each FAQ page",
					"caret symbol Available in the corner of each FAQ page",
					"caret symbol not Available in the corner of each FAQ page", driver);
			
			Log.softAssertThat(faqPage.verifyExpandedState(),
					"FAQ should be displayed in the expanded state",
					"FAQ should  displayed in the expanded state",
					"FAQ not displayed in the expanded state", driver);

			//Step-4: Verify the functionality of Contact Module
			Log.softAssertThat(faqPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "faqContent", "tileContactModule", faqPage), 
					"Contact Module should be displayed below the Content Body", 
					"Contact Module is displayed below the Content Body", 
					"Contact Module is not displayed below the Content Body", driver);
			
			Log.softAssertThat(faqPage.elementLayer.verifyPageElements(Arrays.asList("tileContactModule"),faqPage),
					"Brand specific content asset should be displayed.",
					"Brand specific content asset displayed!",
					"Brand specific content asset not displayed", driver);
			
			if(BrandUtils.isBrand(Brand.ww)) {
				if(faqPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lblChatAvailable"), faqPage)) {
					Log.softAssertThat(faqPage.elementLayer.verifyElementColor("lblChatAvailable", "236, 0, 140", faqPage), 
							"Avaialble Now chat content should display with correct color.", 
							"Avaialble Now chat content is displayed with correct color.", 
							"Avaialble Now chat content is not displayed with correct color.", driver);
				}
				else if(faqPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lblChatUnavailable"), faqPage)) {
					Log.softAssertThat(faqPage.elementLayer.verifyElementColor("lblChatUnavailable", "189, 3, 12", faqPage), 
							"Not Available chat content should display with correct color.", 
							"Not Available chat content is displayed with correct color.", 
							"Not Available chat content is not displayed with correct color.", driver);
				}else {
					Log.failsoft("Chat Avaialblity is missing from page.");
				}
			}
			if(BrandUtils.isBrand(Brand.ks)) {
				Log.softAssertThat(faqPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lblChatText"), faqPage),
						"Chat content should be available.",
						"Chat content is available",
						"Chat content is not available", driver);
			}
			
			if(Utils.isMobile()) {
				Log.softAssertThat(!faqPage.elementLayer.verifyCssPropertyForElement("lblContactNumber", "cursor", "text", faqPage)
								|| faqPage.elementLayer.verifyCssPropertyForElement("lblContactNumber", "cursor", "auto", faqPage),
						"The call us link should be clickable ",
						"The call us link is clickable ",
						"The call us link is not clickable ", driver);
			}
			else {
				Log.softAssertThat(faqPage.elementLayer.verifyCssPropertyForElement("lblContactNumber", "cursor", "text", faqPage)
								|| faqPage.elementLayer.verifyCssPropertyForElement("lblContactNumber", "cursor", "auto", faqPage),
						"The call us link should not be clickable ",
						"The call us link is not clickable ",
						"The call us link is not clickable ", driver);
			}
			
			faqPage.navigateToContactUsPagethroughContactModule();
			Log.message(i++ + ". Navigated to CS Contact Us page", driver);
			Log.softAssertThat(contactUsPage.elementLayer.verifyAttributeForElement("csCurrentHeading", "innerHTML", "Contact Us", contactUsPage),
					"It should Navigated to contactUsPage through contact module",
					"It Navigated to contactUsPage through contact module!",
					"It Navigated to contactUsPage through contact module is Failed", driver); 
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_HEADER_C22787

}//TC_FBB_DROP6_C22783
