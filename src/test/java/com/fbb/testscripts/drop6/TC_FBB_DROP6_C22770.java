package com.fbb.testscripts.drop6;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.Explore;
import com.fbb.pages.HomePage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP6_C22770 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	
	@Test(groups = { "low", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP6_C22770(String browser) throws Exception
	{
		Log.testCaseInfo();
		final WebDriver driver = WebDriverFactory.get(browser);
		
		
		int i=1;
		try
		{
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			Explore explore =homePage.headers.navigateToExplore();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Eplore-Lookup Page!", driver);

			Log.softAssertThat(explore.elementLayer.verifyPageElements(Arrays.asList("readyElement"), explore), 
					"Lookbooks screen should load successfully.", 
					"Lookbooks screen is loaded successfully. ", 
					"Lookbooks screen is loaded successfully. ", driver);

			//Step-1: Verify the display of available components in Lookbooks screen
			if(Utils.isDesktop() || Utils.isTablet()) {
				Log.softAssertThat(explore.elementLayer.verifyPageElements(Arrays.asList("breadcrumHomeHyperLink","breadcrumExplore"), explore), 
						"Breadcrumb should be displayed.", 
						"Breadcrumb is display.", 
						"Breadcrumb not displayed.", driver);
			}
			
			if(Utils.isMobile()) {
				Log.softAssertThat(explore.elementLayer.verifyPageElements(Arrays.asList("breadcrumExploreMobile"), explore), 
						"Breadcrumb should be displayed", 
						"Breadcrumb is displayed", 
						"Breadcrumb is not displayed", driver);
			}
			
			//Step-2: Verify the display and functionality of the Breadcrumb
			//Content slot
			Log.softAssertThat(explore.elementLayer.verifyPageElements(Arrays.asList("contentSlot"), explore), 
					"Content slot should be displayed.", 
					"Content slot is displayed.", 
					"Content slot is not displayed.", driver);

			//Promotional Content
			Log.softAssertThat(explore.elementLayer.verifyPageElements(Arrays.asList("promotionalContent"), explore), 
					" Breadcrumb should be displayed below the Promotional Content", 
					" Breadcrumb displayed below the Promotional Content", 
					" Breadcrumb not displayed below the Promotional Content", driver);

			//Implemented verification for multiple style breadcrumb according to SFCCQA-221
			Log.softAssertThat(explore.getBreadCrumbText().equalsIgnoreCase("homelookbooks")
					||(explore.getBreadCrumbText().startsWith("homeexplore") && explore.getBreadCrumbText().contains("lookbook")),
					"Breadcrumb should be displayed according to folder structure.'",
					"Breadcrumb displayed according to folder structure.",
					"Breadcrumb not displayed according to folder structure.", driver);

			//Step-3: Verify the display of Content Slots
			Log.softAssertThat(explore.elementLayer.verifyElementDisplayedBelow("breadcrumHomeHyperLink", "contentSlotHeading", explore), 
					"Content slot heading should be displayed below the Breadcrumb", 
					"Content slot heading is displayed below the Breadcrumb", 
					"Content slot heading is not displayed below the Breadcrumb", driver);

			Log.reference("BM Configuration cannot be automated, so step #3.b is not automated");

			explore.navigateToHotspotPage();

			//Step-4: Verify the functionality of the Content Slot when user clicks on the hotspots
			Log.reference("Quickview Overlay Modal covered in  C19698, C19700, C22777, C22778, C19701, C19703, C19705, C21652, C21655, C19707");

			Log.testCaseResult();

		} // Ending try block
		catch(Exception e)
		{
			Log.exception(e, driver);
		} // Ending catch block
		finally{
			Log.endTestCase(driver);
		}// Ending finally

	}

}//TC_FBB_DROP6_C22783
