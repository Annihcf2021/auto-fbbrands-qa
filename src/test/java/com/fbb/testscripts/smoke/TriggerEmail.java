package com.fbb.testscripts.smoke;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TriggerEmail extends BaseTest{
	String utagParamsStr = "affiliate_id|affiliate_location_id|utm_medium|utm_source|utm_campaign";
	List<String> utagParamsList = Arrays.asList(utagParamsStr.split("\\|"));
	
	JSONParser jsonParser = new JSONParser();
	
	@SuppressWarnings("unchecked")
	@Test(groups = { "email" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_Email_Batch01(String browser) throws Exception {
		Log.testCaseInfo("Email Batch 1");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		String selectiveCID = "";
		List<String> listCID = Arrays.asList(selectiveCID.split("\\|"));
		
		int i = 1;
		try {
			FileReader readerTrackingTestData = new FileReader("./src/main/resources/TriggerEmail/EpsilonTrackingTest.json");
			Object obj = jsonParser.parse(readerTrackingTestData);
			JSONObject allCampaigns = (JSONObject) jsonParser.parse(obj.toString());
		    
		    FileReader emailLinkData = new FileReader("./src/main/resources/TriggerEmail/EmailBatch1.json");
		    obj = jsonParser.parse(emailLinkData);
		    JSONObject jsonEmail = (JSONObject) jsonParser.parse(obj.toString());
		    
		    if (selectiveCID.length() == 0) {
		    	listCID = new ArrayList<String>();
		    	listCID.addAll(jsonEmail.keySet());
		    }
			Log.reference("Email template(s) being validated:: " + listCID.toString());
		    
		    HashMap<String, String> emailLinksMap = new HashMap<String, String>();
			for (String cid : listCID) {
				JSONObject campaignLinks = (JSONObject) jsonEmail.get(cid);
				emailLinksMap.put(cid, campaignLinks.get(Utils.getCurrentBrandShort().toUpperCase()).toString());
			}
			
			for(String cid: emailLinksMap.keySet()) {
				if (selectiveCID.length() == 0 || selectiveCID.contains(cid)) {
					driver.get(emailLinksMap.get(cid));
					Log.reference("<br>" + i++ + ". Loaded " + cid + ":: " + emailLinksMap.get(cid), driver);
				} else
					continue;
				
				List<WebElement> linksCTAElements = driver.findElements(By.cssSelector("span>a[href*='e.']"));
				Log.reference("Number of CTA links:: " + linksCTAElements.size());
				List<String> linksCTA = new ArrayList<String>();
				for(WebElement elementCTA : linksCTAElements) {
					String hrefCTA = elementCTA.getAttribute("href");
					if(!hrefCTA.equalsIgnoreCase(emailLinksMap.get(cid))) {
						linksCTA.add(elementCTA.getAttribute("href"));
					}
				}
				
				for (String urlCTA : linksCTA) {
					driver.navigate().to(urlCTA);
					JavascriptExecutor jsExec = (JavascriptExecutor) driver;
					Log.message("<br>Navigated to CTA link:: " + driver.getCurrentUrl(), driver);
					
					try {
						Log.message("UTAG data:: " + jsExec.executeScript("return utag.data['qp.affiliate_id']+' | '+utag.data['qp.affiliate_location_id']+' | '+utag.data['qp.utm_medium']+' | '+utag.data['qp.utm_source']+' | '+utag.data['qp.utm_campaign']"));
						JSONObject currentCampaignData = (JSONObject) allCampaigns.get(cid);
						for (String utagParam : utagParamsList) {
							String utagExpected = currentCampaignData.get(utagParam).toString();
							String utagActual = null;
							String jsCommand = "return utag.data['qp." + utagParam + "']";
							try {
								Object utagActualObj = jsExec.executeScript(jsCommand);
								utagActual = utagActualObj.toString().trim();
							} catch(Exception e) {
								Log.event("Exception type:: " + e.getClass());
							} finally {
								Log.softAssertUTAG(utagParam, utagExpected, utagActual);
							}
						}
					} catch(JavascriptException jse) {
						Log.reference("Utag is not defined in current page, probably not a FullBeautyBrands page.");
					}
				}
			}
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase(driver);
		}
	}// TC_FBB_Email_Batch01
	
	@SuppressWarnings("unchecked")
	@Test(groups = { "email" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_Email_Batch02(String browser) throws Exception {
		Log.testCaseInfo("Email Batch 2");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String selectiveCID = "";
		List<String> listCID = Arrays.asList(selectiveCID.split("\\|"));
		
		int i = 1;
		try {
			FileReader readerTrackingTestData = new FileReader("./src/main/resources/TriggerEmail/EpsilonTrackingTest.json");
			Object obj = jsonParser.parse(readerTrackingTestData);
			JSONObject allCampaigns = (JSONObject) jsonParser.parse(obj.toString());
		    
		    FileReader emailLinkData = new FileReader("./src/main/resources/TriggerEmail/EmailBatch2.json");
		    obj = jsonParser.parse(emailLinkData);
		    JSONObject jsonEmail = (JSONObject) jsonParser.parse(obj.toString());
		    
		    if (selectiveCID.length() == 0) {
		    	listCID = new ArrayList<String>();
		    	listCID.addAll(jsonEmail.keySet());
		    }
			Log.reference("Email template(s) being validated:: " + listCID.toString());
		    
		    HashMap<String, String> emailLinksMap = new HashMap<String, String>();
			for (String cid : listCID) {
				JSONObject campaignLinks = (JSONObject) jsonEmail.get(cid);
				emailLinksMap.put(cid, campaignLinks.get(Utils.getCurrentBrandShort().toUpperCase()).toString());
			}
			
			for(String cid: emailLinksMap.keySet()) {
				if (selectiveCID.length() == 0 || selectiveCID.contains(cid)) {
					driver.get(emailLinksMap.get(cid));
					Log.reference("<br>" + i++ + ". Loaded " + cid + ":: " + emailLinksMap.get(cid), driver);
				} else
					continue;
				
				List<WebElement> linksCTAElements = driver.findElements(By.cssSelector("span>a[href*='e.']"));
				Log.reference("Number of CTA links:: " + linksCTAElements.size());
				List<String> linksCTA = new ArrayList<String>();
				for(WebElement elementCTA : linksCTAElements) {
					String hrefCTA = elementCTA.getAttribute("href");
					if(!hrefCTA.equalsIgnoreCase(emailLinksMap.get(cid))) {
						linksCTA.add(elementCTA.getAttribute("href"));
					}
				}
				
				for (String urlCTA : linksCTA) {
					driver.navigate().to(urlCTA);
					JavascriptExecutor jsExec = (JavascriptExecutor) driver;
					Log.message("<br>Navigated to CTA link:: " + driver.getCurrentUrl(), driver);
					
					try {
						Log.message("UTAG data:: " + jsExec.executeScript("return utag.data['qp.affiliate_id']+' | '+utag.data['qp.affiliate_location_id']+' | '+utag.data['qp.utm_medium']+' | '+utag.data['qp.utm_source']+' | '+utag.data['qp.utm_campaign']"));
						JSONObject currentCampaignData = (JSONObject) allCampaigns.get(cid);
						for (String utagParam : utagParamsList) {
							String utagExpected = currentCampaignData.get(utagParam).toString();
							String utagActual = null;
							String jsCommand = "return utag.data['qp." + utagParam + "']";
							try {
								Object utagActualObj = jsExec.executeScript(jsCommand);
								utagActual = utagActualObj.toString().trim();
							} catch(Exception e) {
								Log.event("Exception type:: " + e.getClass());
							} finally {
								Log.softAssertUTAG(utagParam, utagExpected, utagActual);
							}
						}
					} catch(JavascriptException jse) {
						Log.reference("Utag is not defined in current page, probably not a FullBeautyBrands page.");
					}
				}
			}
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase(driver);
		}
	}// TC_FBB_Email_Batch02
	
	@SuppressWarnings("unchecked")
	@Test(groups = { "email" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_Email_Batch03(String browser) throws Exception {
		Log.testCaseInfo("Email Batch 3");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		String selectiveCID = "";
		List<String> listCID = Arrays.asList(selectiveCID.split("\\|"));
		
		int i = 1;
		try {
			FileReader readerTrackingTestData = new FileReader("./src/main/resources/TriggerEmail/EpsilonTrackingTest.json");
			Object obj = jsonParser.parse(readerTrackingTestData);
			JSONObject allCampaigns = (JSONObject) jsonParser.parse(obj.toString());
		    
		    FileReader emailLinkData = new FileReader("./src/main/resources/TriggerEmail/EmailBatch3.json");
		    obj = jsonParser.parse(emailLinkData);
		    JSONObject jsonEmail = (JSONObject) jsonParser.parse(obj.toString());
		    
		    if (selectiveCID.length() == 0) {
		    	listCID = new ArrayList<String>();
		    	listCID.addAll(jsonEmail.keySet());
		    }
			Log.reference("Email template(s) being validated:: " + listCID.toString());
		    
		    HashMap<String, String> emailLinksMap = new HashMap<String, String>();
			for (String cid : listCID) {
				JSONObject campaignLinks = (JSONObject) jsonEmail.get(cid);
				emailLinksMap.put(cid, campaignLinks.get(Utils.getCurrentBrandShort().toUpperCase()).toString());
			}
			
			for(String cid: emailLinksMap.keySet()) {
				if (selectiveCID.length() == 0 || selectiveCID.contains(cid)) {
					driver.get(emailLinksMap.get(cid));
					Log.reference("<br>" + i++ + ". Loaded " + cid + ":: " + emailLinksMap.get(cid), driver);
				} else
					continue;
				
				List<WebElement> linksCTAElements = driver.findElements(By.cssSelector("span>a[href*='e.']"));
				Log.reference("Number of CTA links:: " + linksCTAElements.size());
				List<String> linksCTA = new ArrayList<String>();
				for(WebElement elementCTA : linksCTAElements) {
					String hrefCTA = elementCTA.getAttribute("href");
					if(!hrefCTA.equalsIgnoreCase(emailLinksMap.get(cid))) {
						linksCTA.add(elementCTA.getAttribute("href"));
					}
				}
				
				for (String urlCTA : linksCTA) {
					driver.navigate().to(urlCTA);
					JavascriptExecutor jsExec = (JavascriptExecutor) driver;
					Log.message("<br>Navigated to CTA link:: " + driver.getCurrentUrl(), driver);
					
					try {
						Log.message("UTAG data:: " + jsExec.executeScript("return utag.data['qp.affiliate_id']+' | '+utag.data['qp.affiliate_location_id']+' | '+utag.data['qp.utm_medium']+' | '+utag.data['qp.utm_source']+' | '+utag.data['qp.utm_campaign']"));
						JSONObject currentCampaignData = (JSONObject) allCampaigns.get(cid);
						for (String utagParam : utagParamsList) {
							String utagExpected = currentCampaignData.get(utagParam).toString();
							String utagActual = null;
							String jsCommand = "return utag.data['qp." + utagParam + "']";
							try {
								Object utagActualObj = jsExec.executeScript(jsCommand);
								utagActual = utagActualObj.toString().trim();
							} catch(Exception e) {
								Log.event("Exception type:: " + e.getClass());
							} finally {
								Log.softAssertUTAG(utagParam, utagExpected, utagActual);
							}
						}
					} catch(JavascriptException jse) {
						Log.reference("Utag is not defined in current page, probably not a FullBeautyBrands page.");
					}
				}
			}
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase(driver);
		}
	}// TC_FBB_Email_Batch03
	
	@SuppressWarnings("unchecked")
	@Test(groups = { "email" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_Email_Batch04(String browser) throws Exception {
		Log.testCaseInfo("Email Batch 4");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		String selectiveCID = "";
		List<String> listCID = Arrays.asList(selectiveCID.split("\\|"));
		
		int i = 1;
		try {
			FileReader readerTrackingTestData = new FileReader("./src/main/resources/TriggerEmail/EpsilonTrackingTest.json");
			Object obj = jsonParser.parse(readerTrackingTestData);
			JSONObject allCampaigns = (JSONObject) jsonParser.parse(obj.toString());
		    
		    FileReader emailLinkData = new FileReader("./src/main/resources/TriggerEmail/EmailBatch4.json");
		    obj = jsonParser.parse(emailLinkData);
		    JSONObject jsonEmail = (JSONObject) jsonParser.parse(obj.toString());
		    
		    if (selectiveCID.length() == 0) {
		    	listCID = new ArrayList<String>();
		    	listCID.addAll(jsonEmail.keySet());
		    }
			Log.reference("Email template(s) being validated:: " + listCID.toString());
		    
		    HashMap<String, String> emailLinksMap = new HashMap<String, String>();
			for (String cid : listCID) {
				JSONObject campaignLinks = (JSONObject) jsonEmail.get(cid);
				emailLinksMap.put(cid, campaignLinks.get(Utils.getCurrentBrandShort().toUpperCase()).toString());
			}
			
			for(String cid: emailLinksMap.keySet()) {
				if (selectiveCID.length() == 0 || selectiveCID.contains(cid)) {
					driver.get(emailLinksMap.get(cid));
					Log.reference("<br>" + i++ + ". Loaded " + cid + ":: " + emailLinksMap.get(cid), driver);
				} else
					continue;
				
				List<WebElement> linksCTAElements = driver.findElements(By.cssSelector("span>a[href*='e.']"));
				Log.reference("Number of CTA links:: " + linksCTAElements.size());
				List<String> linksCTA = new ArrayList<String>();
				for(WebElement elementCTA : linksCTAElements) {
					String hrefCTA = elementCTA.getAttribute("href");
					if(!hrefCTA.equalsIgnoreCase(emailLinksMap.get(cid))) {
						linksCTA.add(elementCTA.getAttribute("href"));
					}
				}
				
				for (String urlCTA : linksCTA) {
					driver.navigate().to(urlCTA);
					JavascriptExecutor jsExec = (JavascriptExecutor) driver;
					Log.message("<br>Navigated to CTA link:: " + driver.getCurrentUrl(), driver);
					
					try {
						Log.message("UTAG data:: " + jsExec.executeScript("return utag.data['qp.affiliate_id']+' | '+utag.data['qp.affiliate_location_id']+' | '+utag.data['qp.utm_medium']+' | '+utag.data['qp.utm_source']+' | '+utag.data['qp.utm_campaign']"));
						JSONObject currentCampaignData = (JSONObject) allCampaigns.get(cid);
						for (String utagParam : utagParamsList) {
							String utagExpected = currentCampaignData.get(utagParam).toString();
							String utagActual = null;
							String jsCommand = "return utag.data['qp." + utagParam + "']";
							try {
								Object utagActualObj = jsExec.executeScript(jsCommand);
								utagActual = utagActualObj.toString().trim();
							} catch(Exception e) {
								Log.event("Exception type:: " + e.getClass());
							} finally {
								Log.softAssertUTAG(utagParam, utagExpected, utagActual);
							}
						}
					} catch(JavascriptException jse) {
						Log.reference("Utag is not defined in current page, probably not a FullBeautyBrands page.");
					}
				}
			}
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase(driver);
		}
	}// TC_FBB_Email_Batch04
	
	@SuppressWarnings("unchecked")
	@Test(groups = { "email" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_Email_Batch05(String browser) throws Exception {
		Log.testCaseInfo("Email Batch 5");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		String selectiveCID = "";
		List<String> listCID = Arrays.asList(selectiveCID.split("\\|"));
		
		int i = 1;
		try {
			FileReader readerTrackingTestData = new FileReader("./src/main/resources/TriggerEmail/EpsilonTrackingTest.json");
			Object obj = jsonParser.parse(readerTrackingTestData);
			JSONObject allCampaigns = (JSONObject) jsonParser.parse(obj.toString());
		    
		    FileReader emailLinkData = new FileReader("./src/main/resources/TriggerEmail/EmailBatch5.json");
		    obj = jsonParser.parse(emailLinkData);
		    JSONObject jsonEmail = (JSONObject) jsonParser.parse(obj.toString());
		    
		    if (selectiveCID.length() == 0) {
		    	listCID = new ArrayList<String>();
		    	listCID.addAll(jsonEmail.keySet());
		    }
			Log.reference("Email template(s) being validated:: " + listCID.toString());
		    
		    HashMap<String, String> emailLinksMap = new HashMap<String, String>();
			for (String cid : listCID) {
				JSONObject campaignLinks = (JSONObject) jsonEmail.get(cid);
				emailLinksMap.put(cid, campaignLinks.get(Utils.getCurrentBrandShort().toUpperCase()).toString());
			}
			
			for(String cid: emailLinksMap.keySet()) {
				if (selectiveCID.length() == 0 || selectiveCID.contains(cid)) {
					driver.get(emailLinksMap.get(cid));
					Log.reference("<br>" + i++ + ". Loaded " + cid + ":: " + emailLinksMap.get(cid), driver);
				} else
					continue;
				
				List<WebElement> linksCTAElements = driver.findElements(By.cssSelector("span>a[href*='e.']"));
				Log.reference("Number of CTA links:: " + linksCTAElements.size());
				List<String> linksCTA = new ArrayList<String>();
				for(WebElement elementCTA : linksCTAElements) {
					String hrefCTA = elementCTA.getAttribute("href");
					if(!hrefCTA.equalsIgnoreCase(emailLinksMap.get(cid))) {
						linksCTA.add(elementCTA.getAttribute("href"));
					}
				}
				
				for (String urlCTA : linksCTA) {
					driver.navigate().to(urlCTA);
					JavascriptExecutor jsExec = (JavascriptExecutor) driver;
					Log.message("<br>Navigated to CTA link:: " + driver.getCurrentUrl(), driver);
					
					try {
						Log.message("UTAG data:: " + jsExec.executeScript("return utag.data['qp.affiliate_id']+' | '+utag.data['qp.affiliate_location_id']+' | '+utag.data['qp.utm_medium']+' | '+utag.data['qp.utm_source']+' | '+utag.data['qp.utm_campaign']"));
						JSONObject currentCampaignData = (JSONObject) allCampaigns.get(cid);
						for (String utagParam : utagParamsList) {
							String utagExpected = currentCampaignData.get(utagParam).toString();
							String utagActual = null;
							String jsCommand = "return utag.data['qp." + utagParam + "']";
							try {
								Object utagActualObj = jsExec.executeScript(jsCommand);
								utagActual = utagActualObj.toString().trim();
							} catch(Exception e) {
								Log.event("Exception type:: " + e.getClass());
							} finally {
								Log.softAssertUTAG(utagParam, utagExpected, utagActual);
							}
						}
					} catch(JavascriptException jse) {
						Log.reference("Utag is not defined in current page, probably not a FullBeautyBrands page.");
					}
				}
			}
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase(driver);
		}
	}// TC_FBB_Email_Batch05
	
	@SuppressWarnings("unchecked")
	@Test(groups = { "email" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_Email_Batch06(String browser) throws Exception {
		Log.testCaseInfo("Email Batch 6");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		String selectiveCID = "";
		List<String> listCID = Arrays.asList(selectiveCID.split("\\|"));
		
		int i = 1;
		try {
			FileReader readerTrackingTestData = new FileReader("./src/main/resources/TriggerEmail/EpsilonTrackingTest.json");
			Object obj = jsonParser.parse(readerTrackingTestData);
			JSONObject allCampaigns = (JSONObject) jsonParser.parse(obj.toString());
		    
		    FileReader emailLinkData = new FileReader("./src/main/resources/TriggerEmail/EmailBatch6.json");
		    obj = jsonParser.parse(emailLinkData);
		    JSONObject jsonEmail = (JSONObject) jsonParser.parse(obj.toString());
		    
		    if (selectiveCID.length() == 0) {
		    	listCID = new ArrayList<String>();
		    	listCID.addAll(jsonEmail.keySet());
		    }
			Log.reference("Email template(s) being validated:: " + listCID.toString());
		    
		    HashMap<String, String> emailLinksMap = new HashMap<String, String>();
			for (String cid : listCID) {
				JSONObject campaignLinks = (JSONObject) jsonEmail.get(cid);
				emailLinksMap.put(cid, campaignLinks.get(Utils.getCurrentBrandShort().toUpperCase()).toString());
			}
			
			for(String cid: emailLinksMap.keySet()) {
				if (selectiveCID.length() == 0 || selectiveCID.contains(cid)) {
					driver.get(emailLinksMap.get(cid));
					Log.reference("<br>" + i++ + ". Loaded " + cid + ":: " + emailLinksMap.get(cid), driver);
				} else
					continue;
				
				List<WebElement> linksCTAElements = driver.findElements(By.cssSelector("span>a[href*='e.']"));
				Log.reference("Number of CTA links:: " + linksCTAElements.size());
				List<String> linksCTA = new ArrayList<String>();
				for(WebElement elementCTA : linksCTAElements) {
					String hrefCTA = elementCTA.getAttribute("href");
					if(!hrefCTA.equalsIgnoreCase(emailLinksMap.get(cid))) {
						linksCTA.add(elementCTA.getAttribute("href"));
					}
				}
				
				for (String urlCTA : linksCTA) {
					driver.navigate().to(urlCTA);
					JavascriptExecutor jsExec = (JavascriptExecutor) driver;
					Log.message("<br>Navigated to CTA link:: " + driver.getCurrentUrl(), driver);
					
					try {
						Log.message("UTAG data:: " + jsExec.executeScript("return utag.data['qp.affiliate_id']+' | '+utag.data['qp.affiliate_location_id']+' | '+utag.data['qp.utm_medium']+' | '+utag.data['qp.utm_source']+' | '+utag.data['qp.utm_campaign']"));
						JSONObject currentCampaignData = (JSONObject) allCampaigns.get(cid);
						for (String utagParam : utagParamsList) {
							String utagExpected = currentCampaignData.get(utagParam).toString();
							String utagActual = null;
							String jsCommand = "return utag.data['qp." + utagParam + "']";
							try {
								Object utagActualObj = jsExec.executeScript(jsCommand);
								utagActual = utagActualObj.toString().trim();
							} catch(Exception e) {
								Log.event("Exception type:: " + e.getClass());
							} finally {
								Log.softAssertUTAG(utagParam, utagExpected, utagActual);
							}
						}
					} catch(JavascriptException jse) {
						Log.reference("Utag is not defined in current page, probably not a FullBeautyBrands page.");
					}
				}
			}
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase(driver);
		}
	}// TC_FBB_Email_Batch06
	
	@SuppressWarnings("unchecked")
	@Test(groups = { "email" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_Email_Batch07(String browser) throws Exception {
		Log.testCaseInfo("Email Batch 7");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		String selectiveCID = "";
		List<String> listCID = Arrays.asList(selectiveCID.split("\\|"));
		
		int i = 1;
		try {
			FileReader readerTrackingTestData = new FileReader("./src/main/resources/TriggerEmail/EpsilonTrackingTest.json");
			Object obj = jsonParser.parse(readerTrackingTestData);
			JSONObject allCampaigns = (JSONObject) jsonParser.parse(obj.toString());
		    
		    FileReader emailLinkData = new FileReader("./src/main/resources/TriggerEmail/EmailBatch7.json");
		    obj = jsonParser.parse(emailLinkData);
		    JSONObject jsonEmail = (JSONObject) jsonParser.parse(obj.toString());
		    
		    if (selectiveCID.length() == 0) {
		    	listCID = new ArrayList<String>();
		    	listCID.addAll(jsonEmail.keySet());
		    }
			Log.reference("Email template(s) being validated:: " + listCID.toString());
		    
		    HashMap<String, String> emailLinksMap = new HashMap<String, String>();
			for (String cid : listCID) {
				JSONObject campaignLinks = (JSONObject) jsonEmail.get(cid);
				emailLinksMap.put(cid, campaignLinks.get(Utils.getCurrentBrandShort().toUpperCase()).toString());
			}
			
			for(String cid: emailLinksMap.keySet()) {
				if (selectiveCID.length() == 0 || selectiveCID.contains(cid)) {
					driver.get(emailLinksMap.get(cid));
					Log.reference("<br>" + i++ + ". Loaded " + cid + ":: " + emailLinksMap.get(cid), driver);
				} else
					continue;
				
				List<WebElement> linksCTAElements = driver.findElements(By.cssSelector("span>a[href*='e.']"));
				Log.reference("Number of CTA links:: " + linksCTAElements.size());
				List<String> linksCTA = new ArrayList<String>();
				for(WebElement elementCTA : linksCTAElements) {
					String hrefCTA = elementCTA.getAttribute("href");
					if(!hrefCTA.equalsIgnoreCase(emailLinksMap.get(cid))) {
						linksCTA.add(elementCTA.getAttribute("href"));
					}
				}
				
				for (String urlCTA : linksCTA) {
					driver.navigate().to(urlCTA);
					JavascriptExecutor jsExec = (JavascriptExecutor) driver;
					Log.message("<br>Navigated to CTA link:: " + driver.getCurrentUrl(), driver);
					
					try {
						Log.message("UTAG data:: " + jsExec.executeScript("return utag.data['qp.affiliate_id']+' | '+utag.data['qp.affiliate_location_id']+' | '+utag.data['qp.utm_medium']+' | '+utag.data['qp.utm_source']+' | '+utag.data['qp.utm_campaign']"));
						JSONObject currentCampaignData = (JSONObject) allCampaigns.get(cid);
						for (String utagParam : utagParamsList) {
							String utagExpected = currentCampaignData.get(utagParam).toString();
							String utagActual = null;
							String jsCommand = "return utag.data['qp." + utagParam + "']";
							try {
								Object utagActualObj = jsExec.executeScript(jsCommand);
								utagActual = utagActualObj.toString().trim();
							} catch(Exception e) {
								Log.event("Exception type:: " + e.getClass());
							} finally {
								Log.softAssertUTAG(utagParam, utagExpected, utagActual);
							}
						}
					} catch(JavascriptException jse) {
						Log.reference("Utag is not defined in current page, probably not a FullBeautyBrands page.");
					}
				}
			}
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase(driver);
		}
	}// TC_FBB_Email_Batch07
}
