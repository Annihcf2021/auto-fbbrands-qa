package com.fbb.testscripts.smoke;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.EnlargeViewPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OfferPage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PaypalConfirmationPage;
import com.fbb.pages.PaypalPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlatinumCardApplication;
import com.fbb.pages.PlpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MailDropPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.pages.account.PasswordResetPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.customerservice.TrackOrderPage;
import com.fbb.pages.headers.HamburgerMenu;
import com.fbb.pages.headers.Headers;
import com.fbb.pages.ordering.GuestOrderStatusLandingPage;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.CollectionUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class Smoke extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");

	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23450(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		
		int i = 1;
		try {
			
			//Step 1 - Verify header section components
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			List<String> headerElements = new ArrayList<>();
			headerElements.addAll(Arrays.asList("divBrandSelector", "lnkOffer", "btnHeaderSearchIcon", "quickOrederIcon", "lnkMiniCart"));
			if (!Utils.isMobile()) {
				headerElements.addAll(Arrays.asList("lnkAccountButton"));
			}
			if (!BrandUtils.isBrand(Brand.el)) {
				headerElements.addAll(Arrays.asList("lnkCardPLCC"));
			}
			
			Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayed(headerElements, homePage.headers),
					"Brand navigation section, Deals button, Search button, Catalog button, PLCC button(not applicable for Ellos), Account button, My Bag button should be displayed.",
					"Brand navigation section, Deals button, Search button, Catalog button, PLCC button(not applicable for Ellos), Account button, My Bag button are displayed.",
					"Brand navigation section, Deals button, Search button, Catalog button, PLCC button(not applicable for Ellos), Account button, My Bag button are not displayed.", driver);

			if (!Utils.isDesktop()) {
				Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayed(Arrays.asList("lnkHamburger", "moreBrandIcon"), homePage.headers),
						"Hamburger button, 'More' button should be displayed",
						"Hamburger button, 'More' button are displayed",
						"Hamburger button, 'More' button are not displayed", driver);
			}
			
			//Step 2: Verify account flyout menu
			if (!Utils.isMobile()) {
				homePage.headers.openAccountMenu();
				Log.message(i++ + ". Opened account menu", driver);

				List<String> flyoutElements = Arrays.asList("fldUserNameDesktop", "fldPasswordDesktop", "btnFlyoutSignIn", "chkRememberMeDesktop", "lnkForgetPasswordDesktop", "lblOtherBrandLoginBrandsDesktop", "btnFlyoutCreateAccount");
				Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayedWithoutScrolling(flyoutElements, homePage.headers), 
						"Email field, Password field, Sign In button, Remember Me checkbox, Forgot Password link, other brand content, Create Account button should be displayed",
						"Email field, Password field, Sign In button, Remember Me checkbox, Forgot Password link, other brand content, Create Account button are displayed",
						"Email field, Password field, Sign In button, Remember Me checkbox, Forgot Password link, other brand content, Create Account button are not displayed", driver);
			}
			
			//Step 3: Login with valid credentials
			homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to 'My Account page ", driver);

			Log.softAssertThat(homePage.headers.verifyUserIsLoggedIn(), 
					"Page should be navigated to My account page when login with valid credentials", 
					"Page is navigated to My account page when login with valid credentials",
					"Page is not navigated to My account page when login with valid credentials", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_SMOKE_C23450
	
	@Test(groups = { "desktop", "mobile", "tablet", "release" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23451(String browser) throws Exception {
		Log.testCaseInfo();
		
		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to My Account page!", driver);
			
			Log.softAssertThat(myAcc.verifyMyAccountCategoryTabs(),
					"User should working fine and able to navigate to my account all category tabs",
					"User is working fine and able to navigate to my account all category tabs",
					"User is not working fine and able to navigate to my account all category tabs", driver);

			ProfilePage profilePage = myAcc.navigateToUpdateProfile();
			Log.message(i++ + ". Navigated to Profile Page!", driver);

			HashMap<String, String> profileInfoBefore = profilePage.getProfileInformation();

			profilePage.fillProfileInformation();
			profilePage.typeOnPassword(password);
			Log.message(i++ + ". Entered the fields with new information.", driver);

			profilePage.clickUpdateInfoBtn();
			Log.message(i++ + ". Updated profile with new information.", driver);

			profilePage = myAcc.navigateToUpdateProfile();
			Log.message(i++ + ". Navigated to Profile Page!", driver);

			HashMap<String, String> profileInfoAfter = profilePage.getProfileInformation();

			Log.message(i++ + ". Profile Before :: " + profileInfoBefore);
			Log.message(i++ + ". Profile After :: " + profileInfoAfter);

			Log.softAssertThat(CollectionUtils.compareTwoHashMapNotEqual(profileInfoBefore, profileInfoAfter),
					"Profile page should be updated.",
					"Profile is successfully updated with new information", 
					"Profile not updated.", driver);

			myAcc = homePage.headers.navigateToMyAccount();
			Log.message(i++ + ". Navigated To My Account Page!", driver);

			AddressesPage addPage = myAcc.navigateToAddressPage();
			Log.message(i++ + ". Navigated to Addressbook Page!", driver);
			
			addPage.clickSaveAddress();
			Log.message(i++ + ". Clicked on Save Address without entering any information.", driver);

			Log.assertThat(addPage.elementLayer.verifyPageElements(Arrays.asList("errorFirstName", "errorLastName", "errorAddress1", "errorMsgPhoneNo", "errorPostalCodeInvalid", "errorCity", "stateError", "errorMsgAddressValidation"), addPage), 
					"System Should display error message for all the required fields.", 
					"System is displayed error message for all the required fields.", 
					"System is not displayed error message for all the required fields.",  driver);

			addPage.fillingAddressDetails("address_1", false);
			Log.message(i++ + ". Filled Address Details.", driver);

			addPage.clickSaveAddress();
			Log.message(i++ + ". Clicked on Save Address.", driver);

			Log.softAssertThat(addPage.elementLayer.verifyPageElements(Arrays.asList("defaultAddress"), addPage), 
					"New Address Should be added to Account as default.", 
					"New Address is added to Account as default.", 
					"New Address is not added to Account as default.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.removeAllSavedAddress(driver);
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_SMOKE_C23451
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
    public void TC_FBB_SMOKE_C23452(String browser) throws Exception {
        Log.testCaseInfo();
        
        String prdAltImage = TestData.get("prd_more-color");
        final WebDriver driver = WebDriverFactory.get(browser);
        int i = 1;
        try {
            HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
            Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

            //Load Regular Price Product
            PdpPage pdpPage = homePage.headers.navigateToPDP(prdAltImage);
            String productName = pdpPage.getProductName();
            Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
            
            Log.softAssertThat(pdpPage.elementLayer.verifyPageLoadStatus(pdpPage),                                  
	                "Page should be navigated to PDP", 
	                "Page is navigated to PDP", 
	                "Page is not navigated to PDP", driver);
            
            pdpPage.selectAllSwatches();
            Log.message(i++ + ". Selected all color and size attributes", driver);
            
            if(!BrandUtils.isBrand(Brand.bh)) {
            	Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("productName", "txtProdPrice", "lnkSizechart", "txtSpecialProductSetInventory"), pdpPage), 
                        "Product Attributes on PDP(Product Name, Price, Size Chart, Inventory State) should be dispplayed", 
                        "Product Attributes on PDP(Product Name, Price, Size Chart, Inventory State) is dispplayed",
                        "Product Attributes on PDP(Product Name, Price, Size Chart, Inventory State) not dispplayed",  driver);
            } else {
            	Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("productName", "txtProdPrice", "txtSpecialProductSetInventory"), pdpPage), 
                        "Product Attributes on PDP(Product Name, Price, Size Chart, Inventory State) should be dispplayed", 
                        "Product Attributes on PDP(Product Name, Price, Size Chart, Inventory State) is dispplayed",
                        "Product Attributes on PDP(Product Name, Price, Size Chart, Inventory State) not dispplayed",  driver);
            }
            
            //Step 6: Click on different color swatch
            String selectedColor = pdpPage.getSelectedColor();
            String selectedColorNew = pdpPage.selectDifferentColor();
            Log.message(i++ + ". Clicked on the different color swatch:: " + selectedColorNew, driver);
            
            Log.softAssertThat((selectedColorNew != null && pdpPage.verifyColorSwatchBorder(selectedColorNew))
            				|| (selectedColorNew == null && pdpPage.verifyColorSwatchBorder(selectedColor)),
                    "The selected color swatch should be displayed with a border",
                    "The selected color swatch is displayed with a border",
                    "The selected color swatch is not displayed with a border", driver);
            
            Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "selectedColorVariant", "colorlabel", pdpPage),
                    "The color name should be displayed next to color label",
                    "The color name is displayed next to color label",
                    "The color name is not displayed next to color label", driver);
            
            Log.softAssertThat(pdpPage.verifyDisplayImageMatchesSwatchColor(),
                    "Based on the selected color main image should display",
                    "Based on the selected color main image is displayed",
                    "Based on the selected color main image is not displayed", driver);
            
            //Step 7: Click on any one of the alternate images 
            int altImageIndex = 1;
            boolean altImageAvailable = pdpPage.clickOnAlternateImages(altImageIndex);
            Log.message(i++ + ". Clicked on the 1st alternate image", driver);
            
            if(altImageAvailable) {
            	Log.softAssertThat(pdpPage.verifySelectedAlternateImageDisplayed(altImageIndex),
                        "Based on the selected alternate image main image should display",
                        "Based on the selected alternate image main image is  displayed",
                        "Based on the selected alternate image main image is not displayed", driver);
            }
            
            //Step 8: Click on Enlarge button
            EnlargeViewPage enlargeViewPage  = pdpPage.clickOnEnlargeButton();
            Log.message(i++ + ". Clicked on Enlarge button on PDP");

            Log.softAssertThat(enlargeViewPage.elementLayer.verifyPageElements(Arrays.asList("mdlEnlargeWindow"), enlargeViewPage),                                       
                    "Enlarge Modal window should be displayed when clicking on enlarge button on PDP", 
                    "Enlarge Modal window is displayed", 
                    "Enlarge Modal window is not displayed", driver);
            
            enlargeViewPage.closeEnlargeViewModal();
            Log.message(i++ + ". Enlarge Window Closed.", driver);
            
            if(Utils.isDesktop()) {
				pdpPage.mouseHoverPrimaryImage();
				Log.message(i++ + ". Mouse hovered on the product primary image", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("divZoomImage"), pdpPage),
				        "Transparent overlay will be displayed on the left hand side.",
				        "Transparent overlay is displayed on the left hand side.",
				        "Transparent overlay is not displayed on the left hand side.", driver);
				
				Log.softAssertThat(pdpPage.verifyTopOfZoomedImageAllignsWithTopOfProductImage()
								&& pdpPage.verifyZoomedImageDisplayedRightOfPrimaryProductImage()
								&& pdpPage.verifyZoomImageFitWithProductDetailSection(),
				        "Zoom image will be displayed on the right hand side.",
				        "Zoom image is displayed on the right hand side.",
				        "Zoom image is not displayed on the right hand side.", driver);
            }
            
            //Step 9
            BrowserActions.refreshPage(driver);
            if(!BrandUtils.isBrand(Brand.bh)) {
            	if(Utils.isDesktop()) {
            		Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("sectionRecommendationDesktop", "prodImageRecommendationDesktop"), pdpPage),
            				"Recommendation section should be displayed on PDP page",
            				"Recommendation section is displaying on PDP page",
            				"Recommendation section is not displaying on PDP page",  driver);
            	} else {
            		Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("sectionRecommendationMobileTablet", "prodImageRecommendationMobileTablet"), pdpPage),
            				"Recommendation section should be displayed on PDP page",
            				"Recommendation section is displaying on PDP page",
            				"Recommendation section is not displaying on PDP page",  driver);
            	}
            } else {
                Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("sectionRecommendation_BH", "prodImageRecommendation_BH"), pdpPage),
                        "Recommendation section should be displayed on PDP page", 
                        "Recommendation section is displaying on PDP page", 
                        "Recommendation section is not displaying on PDP page",  driver);
            }
            
            pdpPage.addToBagCloseOverlay();
            Log.message(i++ + ". Product Added to Cart.", driver);
            
            //Click on Breadcrumb
            PlpPage plpPage = pdpPage.clickLastBreadCrumbLink();
            Log.message(i++ + ". Clicked on the breadcrumb and Navigated to PLP Page :: " + plpPage.getCategoryName(), driver);
            
            Log.softAssertThat(plpPage.elementLayer.verifyPageLoadStatus(pdpPage),                                  
                        "Page should be navigated to PLP", 
                        "Page is navigated to PLP", 
                        "Page is not navigated to PLP", driver);
            
            //Confirm item added to the cart
            ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
            Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
            
            Log.softAssertThat(cartPage.getProductName().contains(productName),
                        "Added product should be displayed in the shopping bag", 
                        "Added product is displayed in the shopping bag", 
                        "Added product is not displayed in the shopping bag",  driver);
            
            Log.testCaseResult();
        } // try
        catch (Exception e) {
                Log.exception(e, driver);
        } // catch
        finally {
                Log.endTestCase(driver);
        } // finally

    }// TC_FBB_SMOKE_C23452
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23453(String browser) throws Exception {
		Log.testCaseInfo();
		
		String level1 = TestData.get("level-1");
		String refinement1 = "Color";
		String ref1Value = "BLACK";
		String refinement2 = "Price";

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			PlpPage plpPage = homePage.headers.navigateTo(level1);
			Log.message(i++ + ". Navigated to PLP page for category :: " + level1, driver);

			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), plpPage),					 
					"Page should be navigated to category landing page", 
					"Page is navigated to category landing page", 
					"Page is not navigated to category landing page", driver);	
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProductImages", "txtProdPrice1","txtProductName"), plpPage),
					"Product images, name and price should available in PLP", 
					"Product images, name and price are available in PLP",
					"Product images, name and price are not available in PLP", driver);
			
			boolean primaryImgIsEqual = (boolean) plpPage.checkProdImgIsEqualToColorSwatchImage()[0];
			Log.softAssertThat(primaryImgIsEqual,
					"The system should display the selected colorized image on the main image area.", 
					"The system is displayed the selected colorized image on the main image area.",
					"The system is not displayed the selected colorized image on the main image area.", driver);
			
			Log.softAssertThat(plpPage.verifySortByOptions(),
					"Sort by options should be properly displayed",
					"Sort by options are properly displayed in 'Sort By' drop-down.",
					"Sort by options are not properly displayed in 'Sort By' drop-down.", driver);
			
			String sortOption = plpPage.refinements.selectSortBy("Highest Price");
			Log.message(i++ + ". Sort Option(" + sortOption + ") selected from Sorting menu.", driver);
			
			Log.softAssertThat(plpPage.refinements.verifySortApplied(sortOption), 
					"Products should be sorted and displayed in product list page based on the sort by option", 
					"Products is sorted and displayed in product list page based on the sort by option", 
					"Products is not sorted and displayed in product list page based on the sort by option", driver);
			
			//Step 6 - Verify filter options
			BrowserActions.refreshPage(driver);
			int countBeforeRefinement = plpPage.refinements.getProductCount();
			Log.message(i++ + ". Initial product count: " + countBeforeRefinement, driver);
			
			String[] refinementSelection1 = plpPage.refinements.selectSubRefinementInFilterBy(refinement1, ref1Value);
			Log.message(i++ + ". Filtered by "+ refinementSelection1[0], driver);
			
			if(!Utils.isDesktop()) {
				plpPage.refinements.clickOnBackButtonOnMobileAndTab();
				Log.message(i++ + ". Clicked back from the filter option flyout", driver);
			}
			
			int countAfter1Refinemnt = plpPage.refinements.getProductCount();
			Log.message(i++ + ". First Refinement in filter(" + refinementSelection1[0] + ") selected. Count: " + countAfter1Refinemnt);
			
			Log.softAssertThat(countBeforeRefinement != countAfter1Refinemnt,
					"Available product count should be updated as per selected option.",
					"Available Product count updated as per selected option.",
					"Available product count not updated as per selected option.", driver);
			
			String[] refinementSelection2 = plpPage.refinements.selectSubRefinementInFilterBy(refinement2, 1);
			Log.message(i++ + ". Filtered by "+ refinementSelection2[0], driver);
			
			int countAfter2Refinemnt = plpPage.refinements.getProductCount();
			Log.message(i++ + ". Second Refinement in filter(" + refinementSelection2[0] + ") selected. Count: " + countAfter2Refinemnt);
			
			Log.softAssertThat(countAfter2Refinemnt != countBeforeRefinement && countAfter2Refinemnt != countAfter1Refinemnt,
					"Available product count should be updated as per selected option.",
					"Available Product count updated as per selected option.",
					"Available product count not updated as per selected option.", driver);
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(plpPage.refinements.verifyRefinementSelection(refinementSelection1[1]) && plpPage.refinements.verifyRefinementSelection(refinementSelection2[1].split(" ")[0]),
						"Values of the selected Refinement should be displayed in the Refinement box",
						"Values of the selected Refinement displayed in the Refinement box",
						"Values of the selected Refinement not displayed in the Refinement box", driver);
				
				plpPage.refinements.openCloseFilterBy("collapsed");
				plpPage.refinements.removeRefinement(refinementSelection2[1].split(" ")[0]);
			} else {
				plpPage.refinements.clickOnClearInSubRefinement();
				
				plpPage.refinements.clickOnBackButtonOnMobileAndTab();
				Log.message(i++ + ". Clicked back from the filter option flyout", driver);
			}
			
			int countAfterRemove = plpPage.refinements.getProductCount();
			Log.message(i++ + ". Cleared " + refinementSelection2[1] + ". Count: " + countAfterRemove, driver);
			
			Log.softAssertThat(countAfterRemove != countAfter2Refinemnt,
					"Available product count should be updated.",
					"Product count updated as Expected",
					"Product count not updated as Expected", driver);
			
			plpPage.refinements.clickOnClearAllInRefinement();
			int prdCntAfterRemoveAll = plpPage.refinements.getProductCount();
			Log.message(i++ + ". Cleared all refinements. Count: " + prdCntAfterRemoveAll, driver);
						
			Log.softAssertThat((prdCntAfterRemoveAll == countBeforeRefinement) || (prdCntAfterRemoveAll > countAfter1Refinemnt),
					"Available product count should be updated.",
					"Product count updated as Expected",
					"Product count not updated as Expected", driver);
			
			PdpPage pdpPage = plpPage.navigateToPdp(1);
			Log.message(i++ +". Naviagted to first product PDP:: " + pdpPage.getProductName(), driver);
			
			plpPage = pdpPage.clickLastBreadCrumbLink();
			Log.message(i++ + ". Clicked on the breadcrumb and Navigated to PLP Page :: " + plpPage.getCategoryName(), driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyPageLoadStatus(pdpPage),					 
					"Page should be navigated to PLP", 
					"Page is navigated to PLP", 
					"Page is not navigated to PLP", driver);
			
			plpPage.scrollTogetStickyAndBacktoTopButton();
			
			plpPage.clickBackToTopButton();
			Log.message(i++ + ". Clicked on the Back To Top Button", driver);
			
			Log.softAssertThat(plpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("btnBackToTop"), plpPage),
					"Back To Top button should not be visible.",
					"Back To Top button is not visible.", 
					"Back To Top button is visible.", driver);
			
			if (plpPage.getSearchResultCount()>60) {
				if(Utils.isDesktop()) {
					int lstpg = plpPage.getLastPageNumber();	
					String lstPage = Integer.toString(lstpg);
					
					Log.softAssertThat(plpPage.isPageNumberDisplayed("1") &&
							plpPage.isPageNumberDisplayed(lstPage),
							"Always 1st-page number and last page number should be displayed",
							"Always 1st-page number and last page number is displayed",
							"Always 1st-page number and last page number is not displayed", driver);						
						
					plpPage.clickNextButtonInPagination();
					Log.message(i++ +". Clicked on the 'next' link", driver);
					
					Log.softAssertThat(plpPage.getCurrentPageNumber() == 2, 
							"The system should navigates the user to the 2nd page", 
							"The system is navigates the user to the 2nd page",
							"The system is not navigates the user to the 2nd page", driver);
				} else {
					plpPage.clickOnViewMore();
					Log.message(i++ +". Clicked view more link from pagination", driver);
					
					Log.softAssertThat(plpPage.getProductTileCount()>60,
							"New products should be loaded",
							"New products are loaded",
							"New products are not loaded", driver);
				}
			}			
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_SMOKE_C23453
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23454(String browser) throws Exception {
		Log.testCaseInfo();
		
		String searchKey = TestData.get("product_search_terms").split("\\|")[0];
		
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			Headers headers = homePage.headers;
			
			SearchResultPage slp = homePage.headers.searchProductKeyword(searchKey);
			Log.message(i++ + ". Searched with keyword :: " + searchKey, driver);
			
			Log.softAssertThat(slp.getSearchedProductCountInPage()> 0,
					"Search results should be displayed when searching with valid keyword", 
					"Search results is displayed when searching with valid keyword", 
					"Search results is not displayed when searching with valid keyword", driver);
			
			PdpPage pdpPage = slp.clickOnProductBasedOnIndex(1);
			Log.message(i++ +". Navigated to first product PDP", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("cntPdpContent"), pdpPage),
					"PdpPage should get displayed", 
					"PdpPage is getting displayed", 
					"PdpPage is not getting displayed", driver);
			
			String[] prodId = {pdpPage.getProductID()};
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". Product added to bag", driver);
			
			ShoppingBagPage cartPage = headers.navigateToShoppingBagPage();
			Log.message(i++ +". Navigated to Shopping Bag Page", driver);
			
			Log.softAssertThat(cartPage.verifyProductsAddedToCart(prodId),
					"Check product added to cart", 
					"Product is added to cart", 
					"Product is not added to cart", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_SMOKE_C23454
	
	
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23529(String browser) throws Exception {
		Log.testCaseInfo();
		if(envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
			throw new SkipException("Test not supported in automation for " + Utils.getCurrentEnv());
		}
		
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;


		String username = AccountUtils.generateEmail(driver);
        String password = accountData.get("password_global");
        String credential = username + "|" + password;
        String taxLessAddress = "taxless_address";
        String prdVariation = TestData.get("prd_variation");
        String cardDetails = "card_Visa";
        String firstName = "Hello", lastName = "Welcome";
        
        {
        	GlobalNavigation.registerNewUserWithUserDetail(driver, 0, 0, firstName, lastName, credential);
			GlobalNavigation.addNewAddressToAccount(driver, taxLessAddress, true, credential);
        }
        
        try {
	        
	        HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
	        Log.message(i++ + ". Navigated to Home Page.", driver);
	        
	        SignIn signIn = homePage.headers.navigateToSignInPage();
	        Log.message(i++ + ". Navigated to Sign in Page!", driver);
	
	        MyAccountPage  myAcc = signIn.navigateToMyAccount(username, password);
	        Log.message(i++ + ". Navigated to My Account page as : " + username, driver);
	        
	        PaymentMethodsPage paymentMethods = myAcc.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to 'Payment methods' Page!", driver);
			
			paymentMethods.fillCardDetailsUsingProperty(cardDetails, false, true);
			Log.message(i++ + ". Added a new valid credit card as default card!", driver);
			
			paymentMethods.savePaymentMethod();
			Log.message(i++ + ". Clicked on card save button", driver);
	        
	        PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation);
	        Log.message(i++ + ". Navigated to PDP.", driver);
	        
	        pdpPage.addToBagCloseOverlay();
	        Log.message(i++ + ". Product added to cart!", driver);
	        
	        ShoppingBagPage shoppingBagPg = pdpPage.navigateToShoppingBag();
	        Log.message(i++ + ". Navigated to Shopping bag page.", driver);
	        
	        CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);

			Log.assertThat(checkoutPg.verifyPlaceOrderBtnisAboveItemsInBag(),
					"System should takes the user to check out step 3", 
					"System takes the user to check out step 3", 
					"System is not takes the user to check out step 3",  driver);
			
			checkoutPg.enterCVV("123");
			Log.message(i++ + ". Entered cvv number in the field", driver);
			
			checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on 'Place Order' button!", driver);

			Log.assertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPg.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order should be placed successfully as a Registered user and Order confirmation page should be displayed!", 
					"Order is placed successfully as a Registered user and Order confirmation page is displayed!",
					"Order is not placed successfully as a Registered user and Order confirmation page is not displayed!", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_SMOKE_C23529
	
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23532(String browser) throws Exception {
		Log.testCaseInfo();
		if(envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
			throw new SkipException("Test not supported in automation for " + Utils.getCurrentEnv());
		}

		String searchKey = TestData.get("prd_variation");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		String guestEmail = AccountUtils.generateEmail(driver);
		
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.checkout(driver, searchKey, i, guestEmail);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];

			checkoutPage.fillingShippingDetailsAsGuest("NO","valid_address7", ShippingMethod.Standard);
			Log.message(i++ + ". Entered shipping address", driver);

			checkoutPage.continueToPayment();
			Log.message(i++ + ". Clicked on 'Continue' button after filling shipping address", driver);

			checkoutPage.clickEditShippingLink();
			Log.message(i++ + ". Navigated to shipping address", driver);

			LinkedHashMap<String, String> shippingAddress= checkoutPage.getShippingInformation();
			List<String> shippingAddressValue = new ArrayList<String>(shippingAddress.values());

			checkoutPage.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Select the checkbox 'Use this as Billing Address'.", driver);
			
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Navigated to Payment details section", driver);
			
			checkoutPage.clickBillingAddressEdit();
			LinkedHashMap<String, String> billingAddress = checkoutPage.getBillingInformation();
			List<String> billingAddressValue = new ArrayList<String>(billingAddress.values());
			
			Log.softAssertThat(shippingAddressValue.equals(billingAddressValue),
					"Shipping address and billing address should be same",
					"Shipping address and billing address is same",
					"Shipping address and billing address is not same", driver);
			
			checkoutPage.continueToPaymentFromBilling();
			
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message(i++ + ". Entered Payment details", driver);

			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Clicked on 'Continue' button after entering payment details", driver);

			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on 'Place Order' button!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPage.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order should be placed successfully as a Guest user and Order confirmation page should be displayed!", 
					"Order is placed successfully as a Guest user and Order confirmation page is displayed!",
					"Order is not placed successfully as a Guest user and Order confirmation page is not displayed!", driver);
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_SMOKE_C23532

	@Test(groups = { "desktop", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23531(String browser) throws Exception {
		Log.testCaseInfo();
		
		String level1 = TestData.get("level-1");

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			PlpPage plpPage = homePage.headers.navigateTo(level1);
			Log.message(i++ + ". Navigated to PLP page for category :: " + level1, driver);

			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), plpPage),					 
					"Page should be navigated to category landing page", 
					"Page is navigated to category landing page", 
					"Page is not navigated to category landing page", driver);

			QuickShop quickShop = plpPage.clickOnQuickShop();
			Log.message(i++ + ". Opened QuickShop for Product :: " + quickShop.getProductName(), driver);

			String color = quickShop.selectColor();
			Log.message(i++ + ". Selected Color :: " + color, driver);

			String size = quickShop.selectSize();
			Log.message(i++ + ". Selected Size :: " + size, driver);

			quickShop.addToBag();
			Log.message(i++ + ". Size & Color variation Selected and clicked on Add To Bag.", driver);

			Log.assertThat(quickShop.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("flytMiniCart"), quickShop), 
					"Product Should be added to Cart & ATB Overlay should appear.", 
					"Product added to Cart & ATB Overlay displayed.", 
					"Somthing went wrong.", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_SMOKE_C23531

	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23533(String browser) throws Exception {
		Log.testCaseInfo();

		String searchKey = TestData.get("prd_variation");
		String Coupon = TestData.get("cpn_freeshipping");
		String errorMessageColor = TestData.get("couponErrorColorRGB");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			Log.reference("C23532 :: Guest Checkout been covered in same test script.");
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);

			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Shopping Cart.", driver);

			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);

			String prdName = cartPage.getProductName(0);
			String colorBefore = cartPage.getProductColorVariation(0);
			String sizeBefore = cartPage.getProductSizeVariation(0);
			Log.event("Product Name/Color/Size Values Before:: " + prdName + "/" + colorBefore + "/" + sizeBefore);
			
			cartPage.clickOnEditLink(0);
			Log.message(i++ + ". Clicked on Edit link for Product :: " + prdName, driver);

			String size = new String();
			String color = new String();
			if(Utils.isDesktop()) {
				Log.assertThat(new QuickShop(driver).get().getPageLoadStatus(), 
						"Update Overlay should be displayed.", 
						"Update Overlay displayed.", 
						"Update Overlay not displayed.", driver);
				
				QuickShop quickShop = new QuickShop(driver).get();
				do {
					color = quickShop.selectDifferentColor(colorBefore);
				}while(colorBefore.equalsIgnoreCase(color));
				Log.event("Updated Size/Color :: " + size + "/" + color);
				
				quickShop.addToBag();
				Log.message(i++ + ". Clicked on Update botton.", driver);
				
			} else {
				Log.assertThat(new PdpPage(driver).get().getPageLoadStatus(), 
						"Page should be navigated to PDP Page.",
						"Page navigated to PDP Page.",
						"Page not navigated to PDP Page.", driver);
				
				pdpPage = new PdpPage(driver).get();
				do {
					color = pdpPage.selectDifferentColor(colorBefore);
				}while(colorBefore.equalsIgnoreCase(color));
				Log.event("Updated Size/Color :: " + size + "/" + color);
				
				pdpPage.clickOnUpdate();
				Log.message(i++ +". Clicked on Update Button.", driver);
			}

			Log.assertThat(new ShoppingBagPage(driver).get().getPageLoadStatus(), 
					"Page should be redirected to Shopping Cart Page.",
					"Page redirected to Shopping Cart Page.",
					"Page not redirected to Shopping Cart Page.", driver);
			
			String colorAfter = cartPage.getProductColorVariation(0);
			Log.event("Product Name/Color/Size Values After:: " + prdName + "/" + colorAfter);
			
			Log.softAssertThat(!(colorBefore.equalsIgnoreCase(colorAfter)),
					"Color values should be updated.",
					"Color values are updated.",
					"Color values are not updated.", driver);
			
			String qtyBefore = cartPage.getProductQuantity(0);
			Log.message(i++ + ". Quantity before update :: " + qtyBefore, driver);
			
			cartPage.clickOnQuantityArrow("up");
			Log.message(i++ + ". Clicked on Up arrow in Quantity.", driver);
			
			String qtyAfterUp = cartPage.getProductQuantity(0);
			Log.event("Quantity Before/After :: " + qtyBefore + "/" + qtyAfterUp);
			Log.softAssertThat(!qtyBefore.equals(qtyAfterUp),
					"Quantity should be updated.",
					"Quantity values are updated.",
					"Quantity values are not updated.", driver);
			
			cartPage.clickOnQuantityArrow("down");
			Log.message(i++ + ". Clicked on Down arrow in Quantity.", driver);
			
			String qtyAfterDown = cartPage.getProductQuantity(0);
			Log.event("Quantity Before/After :: " + qtyBefore + "/" + qtyAfterUp);
			Log.softAssertThat(!qtyAfterDown.equals(qtyAfterUp),
					"Quantity should be updated.",
					"Quantity values are updated.",
					"Quantity values are not updated.", driver);
			
			cartPage.removeCartProduct(0);
			Log.message(i++ + ". Clicked on Remove button for product :: " + prdName);
			
			Log.assertThat(cartPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("divSingleCartRow"), cartPage),
					"Product should be removed from Cart.",
					"Product removed from cart.",
					"Product not removed from cart.", driver);
			
			if(envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
			
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to cart Page.", driver);
			
			cartPage.applyPromoCouponCode("Invalid");
			Log.message(i++ + ". Applied invalid promo code", driver);
			
			if(Utils.isMobile()){
				Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("txtcouponErrorMobile"), cartPage)
							&& cartPage.elementLayer.verifyElementColor("txtcouponErrorMobile", errorMessageColor, cartPage),
						"Coupon Error should be displayed in red font color for more than limit.", 
						"Coupon Error is displayed in red font color for more than limit.", 
						"Coupon Error is not displayed for more than limit.", driver);
			}
			else{
				Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("txtcouponError"), cartPage)
							&& cartPage.elementLayer.verifyElementColor("txtcouponError", errorMessageColor, cartPage),
						"Coupon Error should be displayed in red font color for more than limit.", 
						"Coupon Error is displayed in red font color for more than limit.", 
						"Coupon Error is not displayed in red font color for more than limit.", driver);
			}
			
			cartPage.applyPromoCouponCode(Coupon);
			Log.message(i++ + ". Applied valid promo code", driver);
			
			cartPage.clickOnCouponSeeDetails();
			Log.message(i++ + ". Clicked on See details Tool tip", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("txtPromotionDescription"), cartPage),
					"Promotion Description should be displayed", 
					"Promotion Description is displayed", 
					"Promotion Description is not displayed", driver);
			
			cartPage.removeAllAppliedCoupons();
			Log.message(i++ + ". Clicked on Remove button", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("appliedCouponRow"), cartPage),
					"Applied Coupon should be removed when clicking on remove button", 
					"Applied Coupon is removed when clicking on remove button", 
					"Applied Coupon is not removed when clicking on remove button", driver);
			
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
			
			String guestMailID = AccountUtils.generateEmail(driver);
			checkoutPage.continueToShipping(guestMailID);
			Log.message(i++ + ". Continued as Guest with Email ID :: " + guestMailID);
			
			checkoutPage.fillingShippingDetailsAsGuest("YES", "valid_address7", ShippingMethod.Standard);
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Shipping address filled and Continued to Payment.", driver);
			
			checkoutPage.fillingCardDetails1("card_Visa", false);
			Log.message(i++ + ". Master Card Details Filled :: " + EnvironmentPropertiesReader.getInstance("checkout").getProperty("card_Visa"), driver);
			
			checkoutPage.continueToReivewOrder();
			Log.message(i++ + ". Clicked on 'Continue' button after entering payment details", driver);
			
			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on Place Order Button.", driver);

			Log.assertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtOrderConfirmationMsg"), checkoutPage), 
					"Order should place successfully.",
					"Order successfully placed. Order Number :: " + checkoutPage.getOrderNumber(),
					"Order was not successfully placed.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_SMOKE_C23533

	@Test(enabled = false, groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23456(String browser) throws Exception {
		Log.testCaseInfo();
		String searchKey = TestData.get("prd_variation");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		String guestEmail = AccountUtils.generateEmail(driver);
		
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.checkout(driver, searchKey, i, guestEmail);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];

			checkoutPage.fillingShippingDetailsAsGuest("YES","valid_address1", ShippingMethod.Standard);
			Log.message(i++ + ". Entered shipping address", driver);

			checkoutPage.continueToPayment();
			Log.message(i++ + ". Clicked on 'Continue' button after filling shipping address", driver);

			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message(i++ + ". Entered Payment details", driver);

			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Clicked on 'Continue' button after entering payment details", driver);

			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on 'Place Order' button!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPage.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order should be placed successfully as a Guest user and Order confirmation page should be displayed!", 
					"Order is placed successfully as a Guest user and Order confirmation page is displayed!",
					"Order is not placed successfully as a Guest user and Order confirmation page is not displayed!", driver);

			checkoutPage.fillPasswordInOrderReceipt();
			Log.message(i++ + ". Passwords(test123@) Filled successfully in Registration Form.", driver);
			
			checkoutPage.clickOnCreateAccountOrderReceipt();
			Log.message(i++ + ". Clicked on Create Account Button.", driver);
			
			MyAccountPage myAcc = new MyAccountPage(driver).get();

			Log.assertThat(myAcc.getPageLoadStatus(),
					"Page should be redirected to My Account Page.",
					"Page redirected to My Account Page.",
					"Something went wrong.", driver);
			
			AddressesPage addressPage = myAcc.navigateToAddressPage();
			Log.message(i++ + ". Navigated to Addressbook Page.", driver);
			
			Log.assertThat(addressPage.elementLayer.verifyElementDisplayed(Arrays.asList("addressDefaultIndicator"), addressPage),
					"Default Address should be displayed.",
					"Default Address displayed.",
					"Something went wrong.", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_SMOKE_C23456
	
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23534(String browser) throws Exception {
		Log.testCaseInfo();
		if(envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
			throw new SkipException("Test not supported in automation for " + Utils.getCurrentEnv());
		}

		final WebDriver driver = WebDriverFactory.get(browser);
		String searchKey = TestData.get("prd_variation");
		String Coupon = TestData.get("cpn_freeshipping");
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		String credential = username + "|" + password;
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, credential);
			GlobalNavigation.emptyCartForUser(username, password, driver);
		}
		int i = 1;
		try {
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName());
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added Shopping Cart.", driver);

			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Cart Page.", driver);
			
			LinkedList<LinkedHashMap<String, String>> productInformationInCartPage = cartPage.getProductDetails();
			
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to Checkout-Login Page.", driver);

			checkoutPage.continueToShipping(credential);
			Log.message(i++ + ". Continued as Registered user for Email :: " + username);
			
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", ShippingMethod.Standard, "valid_address1");
			Log.message(i++ + ". Entered shipping address", driver);

			checkoutPage.continueToPayment();
			Log.message(i++ + ". Clicked on 'Continue' button after filling shipping address");

			checkoutPage.applyPromoCouponCode("invalid");
			Log.message(i++ + ". Applied Invalid coupon code");
			
			if(Utils.isDesktop()||Utils.isTablet()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyTextContains("couponErrorMessage", "invalid", checkoutPage), 
						"Coupon error message should display promo coupon code.", 
						"Coupon error message is display promo coupon code.", 
						"Coupon error message is not display promo coupon code.", driver);
				}else{
					Log.softAssertThat(checkoutPage.elementLayer.verifyTextContains("couponErrorMessage_Mobile", "invalid", checkoutPage), 
						"Coupon error message should display promo coupon code.", 
						"Coupon error message is display promo coupon code.", 
						"Coupon error message is not display promo coupon code.", driver);
				}
			
			checkoutPage.applyPromoCouponCode(Coupon);
			Log.message(i++ + ". Applied a valid coupon code");
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnSeeDetails"), checkoutPage), 
					"SeeMore Link should be displayed when the applied the active coupons.", 
					"SeeMore Link is displayed when the applied the active coupons.", 
					"SeeMore Link is not displayed when the applied the active coupons.", driver);
			
			checkoutPage.clickOnSeeMoreLink();
			Log.message(i++ + ". Clicked on See more link");
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("discountDetails"), checkoutPage), 
					"After clicking SeeMore Link should be displayed coupons details.", 
					"After clicking SeeMore Link is displayed coupons details.", 
					"After clicking SeeMore Link is not displayed coupons details.", driver);
			
			checkoutPage.removeAppliedCoupon();
			Log.message(i++ + ". Clicked on Remove Link", driver);
			
			checkoutPage.expandOrColapsePromoCodeSection(true);
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("appliedCoupon"), checkoutPage),
					"To check the promo code is removed/not.",
					"The promo code is removed!",
					"The promo code is not removed", driver);
			
			checkoutPage.applyGiftCardByValue(TestGiftCardValue.$1000);
			Log.message(i++ + ". Gift Card is applied", driver);
			
			if(!(checkoutPage.getRemainingOrderAmount() == 00.00)) {
				Log.fail("Order Summary Total is not 0, hence cannot continue!",driver);
			}
			
			Log.softAssertThat((checkoutPage.getRemainingOrderAmount()==00.00), 
					"Order summary total should get 0",
					"Order summary total is 0",
					"Order summary total is not 0", driver);
			
			checkoutPage.continueToReivewOrder();
			Log.message(i++ + ". Clicked on 'Continue' button after entering payment details", driver);
			
			LinkedList<LinkedHashMap<String, String>> productInformationInCheckoutPage = checkoutPage.getProductDetails();
			Log.softAssertThat(CollectionUtils.compareAndPrintTableLinkedListHashMap("Product Details Validation", "Cart", "Checkout", productInformationInCartPage, productInformationInCheckoutPage),
					"Product details such as Product Name, Color, Size, Quanity and Price should be same in Cart & Checkout Page.",
					"Cart Page & Checkout Page has the same values.",
					"Cart Page & Checkout Page doesn't have same values.", driver);
			
			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on 'Place Order' button!", driver);

			Log.assertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPage.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order should be placed successfully as a Registered user and Order confirmation page should be displayed!", 
					"Order is placed successfully as a Registered user and Order confirmation page is displayed!",
					"Order is not placed successfully as a Registered user and Order confirmation page is not displayed!", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_SMOKE_C23534
	
	@Test(groups = { "desktop", "mobile", "tablet", "release" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23567(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			String email = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountData.get("password_global");
			String firstName = "Hello", lastName = "Welcome";
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			SignIn signin = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Navigated to Sign-In Page via header account flyout.", driver);
			
			signin.clickOnCreateAccount();
			Log.message(i++ + ". Clicked on create account.", driver);
			
			//Step-1: Click on "Create Account" button
			Log.softAssertThat(signin.elementLayer.verifyElementDisplayed(
						Arrays.asList("txtFirstName", "txtLastName", "txtEmail", "txtConfirmEmailId", "txtpasswordField", "txtConfirmPasswordField", "checkNewsletters", "btnRegisterCreateAccount"), signin),
					"Create accoutn section should have first name, last name, email, confirm email, password, confirm passwords fields, newsletter checkbox, and create account button.",
					"Create accoutn section has all required fields.",
					"Create accoutn section dord not have all required fields.", driver);
			
			Log.softAssertThat(signin.elementLayer.verifyPageElementsChecked(Arrays.asList("checkNewsletters"), signin),
					"Newsletter Sign up checkbox should be checked by default.",
					"Newsletter Sign up checkbox is checked by default.",
					"Newsletter Sign up checkbox is not checked by default.", driver);
			
			//Step-2: Fill account details, and click "Create Account"
			signin.enterTextOnField("txtFirstName", firstName, "First Name", signin);
			signin.enterTextOnField("txtLastName", lastName, "secondName Name", signin);
			signin.enterTextOnField("txtEmail", email, "Email Id", signin);
			signin.enterTextOnField("txtConfirmEmailId", email, "Confirm Email", signin);
			signin.enterTextOnField("txtpasswordField", password, "Password", signin);
			signin.enterTextOnField("txtConfirmPasswordField", password, "Confirm Password", signin);
			Log.message(i++ + ". Filled in account create information.", driver);

			MyAccountPage myAccPage = (MyAccountPage) signin.clickOnConfirmRegisterBtn();
			Log.message(i++ + ". Account created.", driver);
			
			Log.softAssertThat(homePage.headers.verifyUserIsLoggedIn(), 
					"System should navigate to the My Account Overview page.",
					"System has navigated to the My Account Overview page.",
					"System has not navigate to the My Account Overview page.", driver);
			
			ProfilePage profilePg = myAccPage.navigateToUpdateProfile();
			Log.message(i++ + ". Navigated to profile page.", driver);
			
			Log.softAssertThat(profilePg.verifyEmailAddressIsCorrect(email)
							&& profilePg.verifyFirstNameIsCorrect(firstName)
							&& profilePg.verifyLastNameIsCorrect(lastName),
					"Verify that data entered on the create account page are now displaying correctly on My Account profile page", 
					"Data is verified", 
					"Data is not verified.", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
		
	}//C23567
	
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23459(String browser) throws Exception {
		Log.testCaseInfo();
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String[] orderData = TestData.get("order_history").split("\\|");
		String email = orderData[1];
		String password = orderData[3];

		int i = 1;
		try {
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			MyAccountPage myAccPage = homePage.headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged into Account.", driver);
			
			Log.softAssertThat(myAccPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), myAccPage), 
					"System should navigate to the My Account Overview page.",
					"System has navigated to the My Account Overview page.",
					"System has not navigate to the My Account Overview page.", driver);
			
			OrderHistoryPage orderPage = myAccPage.clickOnOrderHistoryLink();
			Log.message(i++ + ". Navigated to Order Page.", driver);
			
			Log.softAssertThat(orderPage.elementLayer.verifyElementDisplayed(Arrays.asList("divOrderHistory"), orderPage), 
					"User should be able to see all of the orders placed in the past 12 months.",
					"User can see all of the orders placed in the past 12 months.",
					"User does not see orders placed.", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
		
	}//C23459
	
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23460(String browser) throws Exception {
		Log.testCaseInfo();
		
		String[] orderData = TestData.get("guest_order_return").split("\\|");
		String orderNumber = orderData[0];
		String orderEmail = orderData[1];
		String orderZip = orderData[2];

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			TrackOrderPage trackOrderPage;
			if(!Utils.isMobile()) {
				homePage.headers.mouseOverCustomerService();
				Log.message(i++ + ". Mouse hovered on customer service.", driver);
			                
				if(BrandUtils.isBrand(Brand.rm)) {
					Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("CustomerSerivceFlyOut","callUsCustomerFlyoutSection","lnkTrackOrderDesktop", "lnkReturnItemsDesktopRM", "lnkUpdateMyInfoDesktop"), homePage.headers), 
							"Customer Service flyover should contains contact information and Quick Links.",
							"Customer Service flyover contains contact information and Quick Links.",
							"Customer Service flyover not contains contact information and Quick Links.", driver);
				} else {
					Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("CustomerSerivceFlyOut","callUsCustomerFlyoutSection","lnkTrackOrderDesktop", "lnkReturnItemsDesktop", "lnkUpdateMyInfoDesktop"), homePage.headers), 
							"Customer Service flyover should contains contact information and Quick Links.",
							"Customer Service flyover contains contact information and Quick Links.",
							"Customer Service flyover not contains contact information and Quick Links.", driver);
				}
				trackOrderPage = homePage.footers.navigateToTrackMyOrder();
				Log.message(i++ + ". Clicked on Track my order link.", driver);
			                
			} else {
				HamburgerMenu hMenu = (HamburgerMenu) homePage.headers.openCloseHamburgerMenu("open");
				trackOrderPage = hMenu.clickOnTrackOrder();
				Log.message(i++ + ". Clicked on Track my order link.", driver);
			}
			
			trackOrderPage.typeOnOrderTrackFormFields(orderNumber, orderEmail, orderZip);
			Log.message(i++ + ". Entered order information.", driver);
			
			GuestOrderStatusLandingPage guestOrderLanding = trackOrderPage.clickOnSubmitButton();
			Log.message(i++ + ". Clicked on Check order button.", driver);
			
			Log.softAssertThat(guestOrderLanding.elementLayer.verifyElementDisplayed(Arrays.asList("lnkGuestorderStatusPage"), guestOrderLanding), 
					"Order status page should be displayed.",
					"Order status page is displayed.",
					"Order status page is not displayed.", driver);
			
			Log.softAssertThat(guestOrderLanding.elementLayer.getElementText("spanOrderNumber", guestOrderLanding).contains(orderNumber), 
					"Details of specifc order should be displayed.",
					"Specifc order details are displayed.",
					"Specifc order details are not displayed.", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23640
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C25906(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			Log.softAssertThat(homePage.verifyAllImagesLoaded(), 
					"All page images (banner and promo) should be loaded.",
					"All page images are loaded.",
					"All page images are not loaded.", driver);
			
			Log.softAssertThat(homePage.verifyCertonaLoaded(), 
					"(Certona) product recommendations should show.",
					"(Certona) product recommendations is displayed.",
					"(Certona) product recommendations is not displayed.", driver);

			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C25906
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C25907(String browser) throws Exception {
		Log.testCaseInfo();
		
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			OfferPage offerPg = homePage.headers.navigateToOfferPage();
			Log.message(i++ + ". Navigated To Offer Page!", driver);
			
			Log.softAssertThat(offerPg.verifyOffersDisplayed(), 
					"All available offers should be displayed properly.",
					"All available offers are displayed properly.",
					"All available offers are not displayed properly.", driver);
			
			PlpPage plp = homePage.headers.navigateToRandomCategory();
			Log.message(i++ + ". Navigated to PLP: " + plp.getCategoryName(), driver);
			
			Log.softAssertThat(plp.verifyProductTileImages(), 
					"Products should be showing with images attached.",
					"Products are showing with images attached.",
					"Products are not showing with images attached.", driver);
			
			if(Utils.isDesktop()) {
				Log.softAssertThat(plp.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divProductTileContainer", "categoryRefinement", plp), 
						"Categories should be displayed on the left side.",
						"Categories are displayed on the left side.",
						"Categories are not displayed on the left side.", driver);
			}
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C25907
	
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23535(String browser) throws Exception {
		Log.testCaseInfo();	
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String productId = TestData.get("prd_variation");
			String checkoutAdd = "valid_address1";
			String paymentMethod = "card_Visa";
			{
				GlobalNavigation.registerNewUserAddress(driver, checkoutAdd, credential);
				GlobalNavigation.addNewPaymentToAccount(driver, paymentMethod, true, credential);
			}
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			Headers headers = homePage.headers;
			PdpPage pdpPage = homePage.headers.navigateToPDP(productId);
			String prodNamePdp = pdpPage.getProductName();
			Log.message(i++ + ". Navigated to PDP Page for product :: " + prodNamePdp, driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Shopping Cart.", driver);

			ShoppingBagPage cartPage = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
			
			SignIn signin = (SignIn) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			CheckoutPage checkoutPage = (CheckoutPage)signin.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);
			
			checkoutPage.continueToShipping(credential);
			Log.message(i++ + ". Navigated to Checkout page", driver);	
			
			String prodNameCheckout = checkoutPage.getProductName(0);
			
			Log.softAssertThat(prodNamePdp.equalsIgnoreCase(prodNameCheckout), 
					"Product item should be displayed properly", 
					"Product item is displayed properly",
					"Product item is not displayed properly", driver);
			
			Log.softAssertThat(checkoutPage.verifyDefaultDeliveryOptionChecked(), 
					"Default shipping method should be selected", 
					"Default shipping method is selected",
					"Default shipping method is not selected", driver);
			
			String OrderSummaryTotal = checkoutPage.getOrderSummaryTotal();
			
			checkoutPage.enterCVV("333");
			Log.message(i++ +". Entered CVV number ", driver);
			
			OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPage.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order confirmation page should be displayed!", 
					"Order confirmation page is displayed!",
					"Order confirmation page is not displayed!", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
		
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23535
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23547(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			ShoppingBagPage cart = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to cart page.", driver);
			
			Log.softAssertThat(cart.verifyEmptyCart(), 
					"Empty Cart page should be displayed", 
					"Empty Cart page is displayed",
					"Empty Cart page is not displayed", driver);
			
			Log.softAssertThat(cart.elementLayer.verifyElementDisplayed(Arrays.asList("emptyCartTile", "trendNowSlickContainer"), cart), 
					"Empty Cart should be available with the following components: 1. Title  2. Trending Now", 
					"Empty Cart is available with the following components: 1. Title  2. Trending Now",
					"Empty Cart is not available with the following components: 1. Title  2. Trending Now", driver);
			
			Log.softAssertThat(cart.elementLayer.verifyElementDisplayed(Arrays.asList("emptyShoppingHeader", "emptyCartMsg", "btnEmptyWhatsNew"), cart), 
					"My Shopping Bag header, Empty cart message and Shop New button should be displayed in Empty cart tile", 
					"My Shopping Bag header, Empty cart message and Shop New button is displayed in Empty cart tile",
					"My Shopping Bag header, Empty cart message and Shop New button is not displayed in Empty cart tile", driver);
			
			PlpPage plpPage = cart.clickShopNewArrivalButton();
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage) ||
					homePage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), homePage), 
						"System should be navigated to Shop What's New PLP", 
						"System is navigated to Shop What's New PLP",
						"System is not navigated to Shop What's New PLP", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23547
	
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23566(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String address = "valid_address1";
			String paymentCard = "card_Visa";
			
			{
				GlobalNavigation.registerNewUser(driver, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			Headers headers = homePage.headers;
			PdpPage pdpPage = homePage.footers.navigateToPhysicalGiftCardPDP();
			Log.message(i++ + ". Navigated to GC PDP Page", driver);
			
			pdpPage.checkGCPersonalizedMessage();
			Log.message(i++ + ". Opened the Personalized drawer", driver);
			
			String prodImgGC = "prodImage_Desk_Tab";
			String drpGiftCardPrice = "drpGiftCardSizeSelect";
			if(Utils.isMobile()) {
				prodImgGC = "prodImage_Mobile";
			} else if(Utils.isDesktop()) {
				drpGiftCardPrice = "drpGiftCardSize";
			}
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList(prodImgGC, drpGiftCardPrice, "chkGCPersonalizedMessage", "txtGCPersonalMsgFrom", "txtGCPersonalMsgTo", "txtAreaPersonalMsg"), pdpPage), 
					"Gift card image, Select Amount, From & To fields, personalization checkbox should be displayed", 
					"Gift card image, Select Amount, From & To fields, personalization checkbox are displayed",
					"Gift card image, Select Amount, From & To fields, personalization checkbox are not displayed", driver);
			
			pdpPage.checkGCPersonalizedMessage();
			pdpPage.selectGiftCardSize("50.00");
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Gift Card added to Shopping Cart.", driver);

			ShoppingBagPage cartPage = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
					
			CheckoutPage checkoutPage = (CheckoutPage) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);	
			
			checkoutPage.fillingShippingDetailsAsSignedInUser("no", "no", "yes", ShippingMethod.Standard, address);
			Log.message(i++ + ". Filled the Shipping address", driver);
			
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment section", driver);
			
			checkoutPage.clickOnShippingCostTool();
			Log.message(i++ + ". Clicked on the Shipping cost tooltip", driver);		
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyTextContains("giftCardPriceInShippingCostOverlay", "$1.95", checkoutPage), 
					"Gift card shipping and handling fee should be $1.95", 
					"Gift card shipping and handling fee is $1.95",
					"Gift card shipping and handling fee is not $1.95", driver);
			
			checkoutPage.clickOnShippingCostToolClose();
			Log.message(i++ + ". Closed on the Shipping cost tooltip", driver);
			
			checkoutPage.fillingCardDetails1(paymentCard, false);
			Log.message(i++ + ". Filled the Payment card", driver);
			
			checkoutPage.enterCVV("333");
			Log.message(i++ +". Entered CVV number ", driver);
			
			checkoutPage.continueToReivewOrder();
			Log.message(i++ + ". Continued to Review & Place order section", driver);
			
			String OrderSummaryTotal = checkoutPage.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPage.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order confirmation page should be displayed!", 
					"Order confirmation page is displayed!",
					"Order confirmation page is not displayed!", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23566
	
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23568(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			String productId = TestData.get("prd_variation");
			String paypalemail = accountData.get("credential_paypal").split("\\|")[0];
			String paypalpassword = accountData.get("credential_paypal").split("\\|")[1];
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			Headers headers = homePage.headers;
			PdpPage pdpPage = homePage.headers.navigateToPDP(productId);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Shopping Cart.", driver);

			ShoppingBagPage cartPage = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("btnTopBannerPaypal", "btnPaypal"), cartPage), 
					"PayPal checkout button should be displayed in both top banner and footer checkout section.", 
					"PayPal checkout button is displayed in both top banner and footer checkout section.",
					"PayPal checkout button is not displayed in both top banner and footer checkout section.", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
					
			PaypalPage paypalPage = cartPage.clickOnPaypalButton();
			Log.message(i++ + ". Clicked on Paypal button in shopping bag page", driver);
	
			PaypalConfirmationPage pcp = paypalPage.enterPayapalCredentials(paypalemail,paypalpassword);
			Log.message(i++ + ". Continued with Paypal Credentials.", driver);
			
			CheckoutPage checkoutPg = pcp.clickContinueConfirmation();
			Log.message(i++ + ". Clicked on Continue button.", driver);
	
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg), 
					"Checkout page should be displayed!", 
					"Checkout page is displayed",
					"Checkout page is not displayed", driver);
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPg.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order confirmation page should be displayed!", 
					"Order confirmation page is displayed!",
					"Order confirmation page is not displayed!", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23568
	
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23458(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			String productId = TestData.get("prd_variation1");
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String paypalemail = accountData.get("credential_paypal").split("\\|")[0];
			String paypalpassword = accountData.get("credential_paypal").split("\\|")[1];
			String address = "valid_address1";
			
			{
				GlobalNavigation.registerNewUser(driver, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			Headers headers = homePage.headers;
			PdpPage pdpPage = homePage.headers.navigateToPDP(productId);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Shopping Cart.", driver);

			ShoppingBagPage cartPage = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
			
			CheckoutPage checkoutPage = (CheckoutPage) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);	
			
			checkoutPage.fillingShippingDetailsAsSignedInUser("no", "no", "yes", ShippingMethod.Standard, address);
			Log.message(i++ + ". Filled the Shipping address", driver);
			
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment section", driver);
			
			checkoutPage.selectPaypalPayment();
			Log.message(i++ + ". Selected 'Paypal' as payment method", driver);
					
			PaypalPage paypalPage = checkoutPage.clickOnPaypalButton();
			Log.message(i++ + ". Clicked on Paypal button in shopping bag page", driver);
	
			PaypalConfirmationPage pcp = paypalPage.enterPayapalCredentials(paypalemail,paypalpassword);
			Log.message(i++ + ". Continued with Paypal Credentials.", driver);
			
			CheckoutPage checkoutPg = pcp.clickContinueConfirmation();
			Log.message(i++ + ". Clicked on Continue button.", driver);
	
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg), 
					"Checkout page should be displayed!", 
					"Checkout page is displayed",
					"Checkout page is not displayed", driver);
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPg.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order confirmation page should be displayed!", 
					"Order confirmation page is displayed!",
					"Order confirmation page is not displayed!", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();
			
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23458
	
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23462(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {

			String userEMailId = AccountUtils.generateEmail(driver);
			String addressDetails = checkoutData.get("plcc_address");
			String ssnDetails = checkoutData.get("SSNdetails");
			HashMap<String, String> userAddressM1 = GlobalNavigation.formatPLCCAddressToMapWithSSN(addressDetails, ssnDetails, driver, userEMailId);
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			Log.softAssertThat(homePage.headers.checkPromoBannerMessage(), 
					"PLCC banner text should be displayed in header", 
					"PLCC banner text is displayed in header", 
					"PLCC banner text is not displayed in header", driver);
			
			PlatinumCardApplication plccApplication = homePage.navigateToPLCCApplication();
			Log.message(i++ + ". Navigated to Platinum Card Application page", driver);
			
			plccApplication.fillPLCCApplication(userAddressM1);
			Log.message(i++ + ". Filled PLCC application", driver);
			
			plccApplication.clickCheckUncheckConsent(true);
			Log.message(i++ + ". Checked concent.", driver);
			
			plccApplication.clickOnRegisterBtn();
			Log.message(i++ + ". Clicked on SUBMIT button.", driver);
			
			Log.softAssertThat(plccApplication.elementLayer.verifyElementDisplayed(Arrays.asList("divUnderReviewModal"), plccApplication), 
					"PlatinumCard Application Under Review modal should get display", 
					"PlatinumCard Application Under Review modal is getting display", 
					"PlatinumCard Application Under Review modal is not getting display", driver);
			
			plccApplication.clickReviewModalContinueShopping();
			Log.message(i++ + ". Clicked on Continue Shopping button.", driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			Log.testCaseResult();
			
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23462
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23465(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			String emailIdFooter = accountData.get("emailIdDropMail");
			String errorColor = TestData.get("signupfield_errorcolor");
			String validEmailError = demandwareData.get("ValidEmailError");
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("emailSignupMessage","emailSignupMessageValues"), homePage.footers), 
					"Email Signup message should followed with Stay up to date with all of our great fitting styles and new season arrivals. Plus enjoy XX% off.", 
					"Email Signup message is followed with Stay up to date with all of our great fitting styles and new season arrivals. Plus enjoy XX% off.", 
					"Email Signup message is not with Stay up to date with all of our great fitting styles and new season arrivals. Plus enjoy XX% off.", driver);
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyVerticalAllignmentOfElements(driver, "emailSignupMessage", "btnFooterEmailSignUp", homePage.footers), 
					"The Email Sign Up Button / Field displays below the Email signup messaging.", 
					"The Email Sign Up Button / Field displayed below the Email signup messaging.", 
					"The Email Sign Up Button / Field is not displayed below the Email signup messaging.", driver);
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnFooterEmailSignUp", "fldEmailSignUpFooter", homePage.footers), 
					"The Email Signup button should displayed beside (right side) of the 'Email Address' field.", 
					"The Email Signup button is displayed beside (right side) of the 'Email Address' field.", 
					"The Email Signup button is not displayed beside (right side) of the 'Email Address' field.", driver);
			
			Log.softAssertThat(homePage.footers.elementLayer.getElementText("btnFooterEmailSignUp", homePage.footers).equalsIgnoreCase("SIGN UP"), 
					"The text should displayed on Email Signup button is 'SIGN UP'.", 
					"The text is displayed on Email Signup button is 'SIGN UP'.", 
					"The text is not displayed on Email Signup button is 'SIGN UP'.", driver);
			
			homePage.footers.clickOnEmailSignUp();
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyElementColor("txtEmailSignUpError", errorColor, homePage.footers), 
					"Click/Tap on sign up button without giving any Email address-The words Email Address in the textbox will turn red.", 
					"Click/Tap on sign up button without giving any Email address-The words Email Address in the textbox is turn red.", 
					"Click/Tap on sign up button without giving any Email address-The words Email Address in the textbox is not turn red.", driver);
			
			homePage.footers.typeInEmailSignUp("aasdsdf");
			homePage.footers.clickOnEmailSignUp();
			
			Log.softAssertThat(homePage.footers.elementLayer.getElementText("txtEmailSignUpError", homePage.footers).toLowerCase().equals(validEmailError.toLowerCase()), 
					"Enter invalid IP format with the email and click/Tap Signup-Error message should be display(Please enter a valid email).", 
					"Enter invalid IP format with the email and click/Tap Signup-Error message is display(Please enter a valid email).", 
					"Enter invalid IP format with the email and click/Tap Signup-Error message is not display(Please enter a valid email).", driver);
			
			homePage.footers.typeInEmailSignUp(emailIdFooter);
			homePage.footers.clickOnEmailSignUp();
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("lblEmailSignUpSuccessMsg"), homePage.footers), 
					"Thank you message should be display and the message should be brand-specific.", 
					"Thank you message should be display and the message is brand-specific.", 
					"Thank you message should be display and the message is not brand-specific.", driver);
			
			Log.testCaseResult();

		}
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23465
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23471(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			String cqPrd = TestData.get("qc_valid_1").split("\\|")[0];
			String cqValidPrds[] = {TestData.get("qc_valid_1").split("\\|")[0], TestData.get("qc_valid_3")};
			String cqInvalidPrds[] = {TestData.get("qc_invalid"), TestData.get("qc_invalid")};
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			QuickOrderPage qcPage = homePage.headers.navigateToQuickOrder();
			Log.message(i++ + ". Searched with Catalog Quick Order", driver);
			
			Log.softAssertThat(qcPage.elementLayer.verifyPageElements(Arrays.asList("divCatalogQuickOrder"), qcPage), 
					"User should be taken to the Order From Catalog page", 
					"User was taken to the Order From Catalog page", 
					"User was not taken to the Order From Catalog page", driver);
				
			qcPage.searchItemInQuickOrder(cqPrd);
			Log.message(i++ + ". Searched with a single product.", driver);
			
			Log.softAssertThat(qcPage.verifyValidCatalogSearchResults(cqValidPrds, cqInvalidPrds), 
					"For valid CQO number, CQO product should be displayed", driver);
			
			qcPage.selectVariationAllCatalogItem();
			Log.message(i++ + ". Selected variation for all product.", driver);
			
			List<String> qcProductNames = qcPage.getProductNames();
			
			qcPage.addAllPrdToBag();
			Log.message(i++ + ". Clicked on add to bag button.", driver);
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart Page.", driver);
			
			Log.softAssertThat(cartPage.getCartItemNameList().containsAll(qcProductNames), 
					"Make sure added products are properly displaying in the cart page.", driver);
			
			Log.testCaseResult();
		}
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23471
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23467(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			String searchProductId = TestData.get("prd_all_brands");
		
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			Headers headers = homePage.headers;
			
			headers.typeTextInSearchField(searchProductId);
			Log.message(i++ + ". Typed a product Id '" + searchProductId.substring(0, 3) + "' in the Search text field", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divProductSuggestions"), headers),
					"Search suggestion flyout should be displayed after entering at least three valid letters.",
					"Search suggestion flyout is displayed after entering at least three valid letters.",
					"Search suggestion flyout is not displayed after entering at least three valid letters.", driver);
			
			PdpPage pdpPage = headers.selectProductFromSearchSuggestion();
			Log.message(i++ + ". Clicked on a product name from Search suggestion", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage) 
							&& pdpPage.getProductID().contains(searchProductId.substring(0, 3)),
					"User should be taken to the PDP of that specific item",
					"User is taken to the PDP of that specific item",
					"User is not taken to the PDP of that specific item", driver);
			
			Log.testCaseResult();
		}
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23467
	
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23468(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			String username= AccountUtils.generateEmail(driver);
			String billingAddress = "valid_address1";
			String paymentCard = "card_Visa";
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			PdpPage pdpPage = homePage.footers.navigateToEGiftCardPDP();
			Log.message(i++ + ". Navigated to E-GC PDP Page", driver);
			
			pdpPage.checkGCPersonalizedMessage();
			Log.message(i++ + ". Opened the Personalized drawer", driver);
			
			String prodImgGC = "prodImage_Desk_Tab";
			String drpGiftCardPrice = "drpGiftCardSizeSelect";
			if(Utils.isMobile()) {
				prodImgGC = "prodImage_Mobile";
			} else if(Utils.isDesktop()) {
				drpGiftCardPrice = "drpGiftCardSize";
			}
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList(prodImgGC, drpGiftCardPrice, "txtRecepientEmail", "txtRecepientConfirmEmail", "chkGCPersonalizedMessage", "txtRecepientName", "txtAreaPersonalMsg"), pdpPage), 
					"Gift card image, Select Amount, From & To fields, personalization checkbox should be displayed", 
					"Gift card image, Select Amount, From & To fields, personalization checkbox are displayed",
					"Gift card image, Select Amount, From & To fields, personalization checkbox are not displayed", driver);
			
			pdpPage.checkGCPersonalizedMessage();
			pdpPage.selectGiftCardSize("50.00");
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Gift Card added to Shopping Cart.", driver);

			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
					
			SignIn signIn = (SignIn) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout login page", driver);	
			
			signIn.typeGuestEmail(username);
			Log.message(i++ + ". Typed guest email in Checkout login page", driver);
			
			CheckoutPage checkoutPage = signIn.clickOnContinueInGuestLogin();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			checkoutPage.fillingBillingDetailsAsGuest(billingAddress);
			Log.message(i++ + ". Filled the Billing address", driver);
			
			checkoutPage.continueToPaymentFromBilling();
			Log.message(i++ + ". Continued to payment section", driver);
			
			checkoutPage.fillingCardDetails1(paymentCard, false);
			Log.message(i++ + ". Filled the Payment card", driver);
			
			checkoutPage.enterCVV("333");
			Log.message(i++ +". Entered CVV number ", driver);
			
			checkoutPage.continueToReivewOrder();
			Log.message(i++ + ". Continued to Review & Place order section", driver);
			
			String OrderSummaryTotal = checkoutPage.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPage.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order confirmation page should be displayed!", 
					"Order confirmation page is displayed!",
					"Order confirmation page is not displayed!", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();
		}
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23468
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23469(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			
			{
				GlobalNavigation.registerNewUser(driver, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
			
			String userName = homePage.headers.getUserName();
			Log.message(i++ + ". Hello ("+userName+") is displayed", driver);
			
			if(Utils.isMobile()) {
				homePage.headers.openCloseHamburgerMenu("open");
				Log.message(i++ + ". Opened Hamburger menu.", driver);
			} else {
				homePage.headers.mouseOverAccountMenu();
				Log.message(i++ + ". Mouse hovered on My account flyout.", driver);
			}
			SignIn signIn = homePage.headers.signOut();
			Log.message(i++ + ". Clicked on Sign Out link", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), signIn), 
					"User should successfully signs out.", 
					"User successfully signs out.", 
					"User not successfully signs out.", driver);
			
			Log.testCaseResult();
		}
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23469
	
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23557(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			String validPassword = accountData.get("password_global");
			String registeredEmail = TestData.get("registered_mailId");
			String passwordResetMessage = demandwareData.get("passwordResetMessage");
			String newpasswordResetMessage = demandwareData.get("newPasswordResetMessage");
			String passwordResetHeading = demandwareData.get("passwordResetHeading");
			String newpasswordResetHeading = demandwareData.get("newpasswordResetHeading");
			int inboxCount = 0;
			
			{
				try {
					inboxCount = GlobalNavigation.getInboxCountFromMailDrop(registeredEmail, driver);
					Log.message("Inbox count for " + registeredEmail + " is " + inboxCount);
				} catch(Exception e) {
					Log.message("Inbox count for " + registeredEmail + " is " + inboxCount);
				}
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Clicked on Sign in link and navigated to sign in page!", driver);
	
			signIn.clickForgotPwdLink();
			Log.message(i++ + ". Clicked on Forgot Password link!", driver);
			
			signIn.enterEmailInForgotPasswordMdl(registeredEmail);
			Log.message(i++ + ". Typed in Email:: " + registeredEmail, driver);
			
			signIn.clickSendRequestPassword();
			Log.message(i++ + ". Clicked send.", driver);
			
			if(signIn.elementLayer.verifyTextContains("headingRequestPassword", passwordResetHeading, signIn)) {
				Log.softAssertThat(signIn.elementLayer.verifyTextContains("subHeadPasswordResetConfirmation", passwordResetMessage, signIn),
					"Success message should be displayed",
					"Success message is displayed",
					"Success message is not displayed", driver);
									
			} else {
				Log.softAssertThat(signIn.elementLayer.verifyTextContains("headingRequestPassword", newpasswordResetHeading, signIn) 
						&& signIn.elementLayer.verifyTextContains("newSubHeadPasswordResetConfirmation", newpasswordResetMessage, signIn),
						"Email Sent sucess message should be displayed", 
						"Email Sent sucess message is displayed",
						"Email Sent sucess message is not displayed", driver);
			}
						
			Utils.openNewTab(driver);
			List<String> handle = new ArrayList<String> (driver.getWindowHandles());
			driver.switchTo().window(handle.get(handle.size() - 1));
			
			MailDropPage mail = new MailDropPage(driver).get();
			Log.message(i++ + ". MailDrop page loaded successfully in new tab", driver);
			
			mail.navigateToDropMailBox(registeredEmail);
			Log.message(i++ + ". Navigated to usermail inbox " + registeredEmail, driver);
			
			mail.openReceivedMail("password", inboxCount, "Passowrd Reset email");
			Log.message(i++ + ". Navigated to Forgot password request mail ", driver);
			
			PasswordResetPage passwordReset = mail.clickResetMyPasswordInEmail();
			Log.message(i++ + ". Clicked Reset my password button in Email", driver);
			
			if (passwordReset != null) {
				passwordReset.enterPasswordInResetPage("resetPasswordPageNewPassword", validPassword, passwordReset);
				Log.message(i++ + ". Typed valid password in New Password field", driver);
				
				passwordReset.enterPasswordInResetPage("resetPasswordPageConfirmPassword", validPassword, passwordReset);
				Log.message(i++ + ". Typed valid password in Confirm Password field", driver);
				
				passwordReset.clickResetInResetPage();
				Log.message(i++ + ". Clicked on Reset button in Reset password page", driver);
				
				Log.softAssertThat(passwordReset.elementLayer.isElementAvailable(Arrays.asList("msgPasswordUpdated", "myAccount"), passwordReset), 
						"System should navigate user to the Account overview page with the confirmation message on that page.", 
						"System is navigateed user to the Account overview page with the confirmation message on that page.", 
						"System is not navigated user to the Account overview page with the confirmation message on that page.", driver);
				
				signIn = homePage.headers.signOut();
				Log.message(i++ + ". Sign out from the site", driver);
				
				MyAccountPage myacc = signIn.navigateToMyAccount(registeredEmail, validPassword);
				Log.message(i++ + ". Sign in again with the updated password", driver);
				
				Log.softAssertThat(myacc.elementLayer.isElementAvailable(Arrays.asList("readyElement"), myacc), 
						"Confirm new password works fine.", 
						"New password works fine.", 
						"New password not works fine.", driver);
			} else {
				Log.ticket("SC-2013");
			}
			
			Log.testCaseResult();
		}
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23557
	
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23461(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			CustomerService customerServicePage = homePage.footers.navigateToCustomerService();
			Log.message(i++ + ". Navigated to Customer Service page!", driver);

			if(customerServicePage.getLiveChatStatus()) { 
				Log.failsoft(i++ + ". Live chat is not displayed in the footer/header", driver);
			} else {
				customerServicePage.clickOnLiveChatLink();
				Log.message(i++ + ". Clicked on live chat link in CS page", driver);
				
				Log.softAssertThat(customerServicePage.elementLayer.verifyPageElements(Arrays.asList("mdlLiveChat"), customerServicePage), 
                        "Live Chat Modal Should be Shown in the customer service page", 
                        "Live Chat Modal is displaying in the customer service page", 
                        "Live Chat Modal not Shown in the customer service page", driver);
				
				customerServicePage.registerInLiveChat();
				Log.message(i++ + ". registeration completed", driver);
				
				customerServicePage.closeLiveChat();
				Log.message(i++ + ". Closed the Live chat window", driver);
			}
				Log.testCaseResult();
		}
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23461
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23466(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			String prodId = TestData.get("prd_reviewed");
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prodId);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			pdpPage.clickOnReviewStarLink();
			Log.message(i++ + ". Clicked on Reviews star below product name", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("sectionReviewVisible"), pdpPage), 
					"The system should navigate the user to review tab.",
					"The system is navigated the user to review tab.",
					"The system is not navigated the user to review tab.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtReviewCount","txtAvgRatingsReview","txtAvgRatingBox"
					,"lnkReviewMyPost","lnkReviewMorePurchase"), pdpPage),
					"Check the review section has average rating, the number of reviews, Comfort and Service meter, overall sizes,"
							+ " recommend the product, best uses for the product, and links for Review More Purchases and My Posts.",
							"The review section has average rating, the number of reviews, Comfort and Service meter, overall sizes" + 
									" recommend the product, best uses for the product, and links for Review More Purchases and My Posts.",
									"Some components are missing in the review section", driver);
			
			pdpPage.clickOnReviewTab();
			Log.message(i++ + ". Clicked on Reviews tab", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("sectionReviewVisible"), pdpPage), 
					"Check the review section is opened",
					"The review section is opened",
					"The review section is not opened", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtReviewCount","txtAvgRatingsReview","txtAvgRatingBox"
					,"lnkReviewMyPost","lnkReviewMorePurchase"), pdpPage),
					"Check the review section has average rating, the number of reviews, Comfort and Service meter, overall sizes,"
							+ " recommend the product, best uses for the product, and links for Review More Purchases and My Posts.",
							"The review section has average rating, the number of reviews, Comfort and Service meter, overall sizes" + 
									" recommend the product, best uses for the product, and links for Review More Purchases and My Posts.",
									"Some components are missing in the review section", driver);
			
			if(pdpPage.getNoOfReviews() > 5) {
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkShowMoreReview"), pdpPage), 
						"Show more link should get displayed",
						"Show more link is displayed",
						"Show more link is not displayed", driver);
				
				int beforeShowMore = pdpPage.getNoOfDisplayingReviews();
				
				pdpPage.clickShowMoreReviews();
				Log.message(i++ + ".  Clicked on Show more review link", driver);
				
				int afterShowMore = pdpPage.getNoOfDisplayingReviews();
				
				Log.softAssertThat(beforeShowMore != afterShowMore, 
						"The Show more link load should load more reviews when clicked",
						"The Show more link load is loading more reviews when clicked",
						"The Show more link load is not loading more reviews", driver);
			} else {
				Log.reference("Product is not having enough review");
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lnkShowMoreReview"), pdpPage), 
						"Show more link should not displayed",
						"Show more link is not displayed",
						"Show more link is displayed", driver);
			}
			
			Log.testCaseResult();
		}
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}//C23466
	
	
	@Test(groups = { "desktop", "tablet", "mobile"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23571(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		
		
		int i = 1;
		try{
			
			String guestEmail = AccountUtils.generateEmail(driver);
			String prdId = TestData.get("prd_variation");
			
			//Always Approve Data
			String alwaysApproveAddress = "plcc_always_approve_address";
						
			// Step-1 Add some regular items to the Cart
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			CheckoutPage checkoutPage = null;
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdId);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". Product added to cart", driver);
				
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ +". Navigated to shopping bag page", driver);
				
				//Step-2 Continue to checkout
				
			SignIn signIn = (SignIn) cartPage.navigateToCheckout();
			
			signIn.typeGuestEmail(guestEmail);
			Log.message(i++ +". Entered guest email address", driver);
				
			//Step-3 Enter Shipping address (Shipping address should be same as billing
				
			checkoutPage = signIn.clickOnContinueInGuestLogin();
			Log.message(i++ +". Navigated to checkout page as a guest user", driver);
				
									
			checkoutPage.fillingShippingDetailsAsGuest(alwaysApproveAddress,ShippingMethod.Standard);
			Log.message(i++ +". Entered \"Always Approve\" address in shipping address section", driver);
				
			checkoutPage.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ +". Selected checkbox of use this as a billing address", driver);
			
			checkoutPage.continueToPaymentByChoosingOriginalAddress(true);
			Log.message(i++ +". Navigated to billing address", driver);
				
				//Step-4 Continue to Payment Method selection
	
			if(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkoutPage)) {
					
				Log.reference("pre approved modal is displayed");
					
				Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkoutPage),
						"The preapproval modal should display. ",
						"The preapproval modal is display. ",
						"The preapproval modal is not displayed. ", driver);
					
				//Step-7 Click on reject
		
				checkoutPage.clickNoThanksInPLCC();
				Log.message(i++ +". Clicked \"No Thanks\" button", driver);
					
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("sectionPaymentCard"), checkoutPage) &&
						checkoutPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("mdlPLCCApproval"), checkoutPage), 
						"Pre approval overlay should closes and user should taken back to Payment Method selection page", 
						"Pre approval overlay is closed and user is taken back to Payment Method selection page",
						"Pre approval overlay is not closes and user is not taken back to Payment Method selection page", driver);
					
				//Step-8 Verify acquisition rebuttal slot displays
		
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "mdlPLCCRebuttal", "paymentMethodSection", checkoutPage),
						"User should see the acquisition rebuttal slot on top of the payment methods",
						"User see the acquisition rebuttal slot on top of the payment methods", 
						"User not see the acquisition rebuttal slot on top of the payment methods", driver);
					
				//Step-9 Click on 'Learn More' link
					
				checkoutPage.openClosePLCCRebuttal("open");
				Log.message(i++ +". Clicked learn more link", driver);
					
				Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCAcquisitionRebuttal"), checkoutPage),
						"The Rebuttal Offer should display. ",
						"The Rebuttal Offer is display. ",
						"The Rebuttal Offer is not displayed. ", driver);
					
				//Step-10 Click on Accept
		
				checkoutPage.continueToPLCCStep2InPLCCACQ();
				Log.message(i++ +". Clicked Accept button from step 1 overlay", driver);
					
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlPLCCApprovalStep2"), checkoutPage),
						"Step 2 Modal should Display",
						"Step 2 Modal is Displayed",
						"Step 2 Modal is not Displayed", driver);
					
				checkoutPage.typeTextInSSN("1234");
				Log.message(i++ +". Entered SSN number", driver);
					
				checkoutPage.selectDateMonthYearInPLCC2("10","09", "1956");
				Log.message(i++ +". selected date of birth", driver);
					
				checkoutPage.typeTextInMobileInPLCC("8015841844");
				Log.message(i++ +". Updated phone number", driver);
					
				checkoutPage.checkConsentInPLCC("yes");
					
				checkoutPage.clickOnAcceptInPLCC();
				Log.message(i++ +". Clicked Yes, I Accept button", driver);
					
				//Step-12 
				if(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"), checkoutPage)) {
					Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"), checkoutPage),
							"The Congratulations modal will pop up.",
							"The Congratulations modal is pop up.",
							"The Congratulations modal is not pop up.", driver);
				}
				
				//Step-13 
				checkoutPage.dismissCongratulationModal();
				Log.message(i++ +". Clicking the Continue to Checkout button", driver);
					
				Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPage), 
						"user will be taken to the payment method section of checkout.",
						"user will be taken to the payment method section of checkout.",
						"user will be taken to the payment method section of checkout.", driver);
						
		
				//Step-14 Proceed to Review & Place Order section
				checkoutPage.continueToReivewOrder();
				Log.message(i++ +". Navigated to review and place order screen", driver);
				
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
				
				if(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("plccCardNoError"), checkoutPage)) {
					Log.reference("By adding Always Approve data Incorrect card number is added, hence ");
				} else {
				//Step-15 Place Order
		
					checkoutPage.clickOnPlaceOrder();
					Log.message(i++ +". Clicked placed order button", driver);
					
					OrderConfirmationPage receipt = null;
					
					if(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPage)) {
						Log.reference("By using Always Approve currently user can't place order", driver);
																
					} else{
						receipt = new OrderConfirmationPage(driver).get();
						Log.softAssertThat(receipt.elementLayer.verifyPageElements(Arrays.asList("readyElement"), receipt),
								"user should taken to the order confirmation page.",
								"user is taken to the order confirmation page.",
								"user is not taken to the order confirmation page.", driver);					
					}
				}
				
			} else {										
				Log.fail("PLCC Step-1 overlay is not displayed hence can't proceed further");
			}
			Log.testCaseResult();
		}
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}//C24571
	
	
	@Test(groups = { "desktop", "tablet", "mobile"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23570(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
						
		int i = 1;
		try {
			
			String userEMailIdADS = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountData.get("password_global");
			String credential = userEMailIdADS + "|" + password;
			String firstName = checkoutData.get("plcc_always_approve_address").split("\\|")[7];
			String lastName = checkoutData.get("plcc_always_approve_address").split("\\|")[8];
			String prdVariation = TestData.get("prd_variation");
			String alwaysApproveAddress = "plcc_always_approve_address";
			String cardType = "cards_2";
					
			{
				GlobalNavigation.registerNewUserWithGivenUserDetail(driver, firstName, lastName, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			MyAccountPage myAcc = homePage.headers.navigateToMyAccount();
			Log.message(i++ + ". Navigated to My Account page as : " + userEMailIdADS.split("\\|")[0], driver);
			
			AddressesPage addressPage = myAcc.navigateToAddressPage();
			Log.message(i++ +".Navigated to address page", driver);
			
			addressPage.fillingAddressDetails(alwaysApproveAddress, false);
			addressPage.clickSaveAddress();
			Log.message(i++ +". Saved default address", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation);
			Log.message(i++ +". Navigated to PDP", driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". Product added to cart", driver);
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ +". Navigated to cart page", driver);

			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ +". Navigated to checkout page", driver);
			
			checkoutPage.continueToPaymentByChoosingOriginalAddress(true);
			Log.message(i++ +". Clicked continue button from shipping address section", driver);
									
			if(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkoutPage)) {
								
				Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkoutPage),
						"The preapproval modal should display. ",
						"The preapproval modal is display. ",
						"The preapproval modal is not displayed. ", driver);
			
				checkoutPage.clickGetItTodayInPLCC();
				Log.message(i++ +". clicked Get it today button from overlay", driver);
				
				checkoutPage.typeTextInSSN("0000");
				Log.message(i++ +". Entered wrong SSN number");
				
				checkoutPage.selectDateMonthYearInPLCC2("24","12", "1999");
				Log.message(i++ +". Selected date, month and year", driver);
				
				checkoutPage.typeTextInMobileInPLCC("8015841844");
				Log.message(i++ +". Updated phone number", driver);
				
				checkoutPage.checkConsentInPLCC("yes");
				Log.message(i++ +". Selected consent checkbox", driver);
				
				checkoutPage.clickOnAcceptInPLCC();
				Log.message(i++ +". Clicked yes, i accept button ", driver);
				
				if(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("underReviewModal"), checkoutPage) ) {
					Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("underReviewModal"), checkoutPage),
							"Application Under Review modal should be displayed",
							"Application Under Review modal is displayed", 
							"Application Under Review modal is not displayed", driver);
										
				} else {
					Log.reference("Under review modal is not displayed with ALWAYS APPROVE data");
				}
				
				checkoutPage.dismissCongratulationModal();
				Log.message(i++ +". Dismissed the PLCC modal", driver);
				
				checkoutPage.continueToPayment();
				Log.message(i++ +". clicked continue to checkout button from modal", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("underReviewModal"), checkoutPage) &&
						checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("paymentmethodSection"), checkoutPage),
						"The Application Under Review modal should disappears and user should brought back to Payment Information.",
						"The Application Under Review modal is disappears and user is brought back to Payment Information.",
						"The Application Under Review modal is not disappears and user is not brought back to Payment Information.", driver);
				
				if(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnAddNewCreditCard"), checkoutPage)) {
					checkoutPage.clickAddNewCredit();
					Log.message(i++ +". Clicked add new credit card link", driver);
				}
								
				checkoutPage.fillingCardDetails1(cardType, false);
				Log.message(i++ +". Entered credit card details", driver);
				
				checkoutPage.continueToReivewOrder();
				Log.message(i++ +". Clicked continue button from payment section", driver);
				
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
				
				OrderConfirmationPage receipt = checkoutPage.clickOnPlaceOrderButton();
				Log.message(i++ +". Clicked placed order button", driver);
				
				Log.softAssertThat(receipt.elementLayer.verifyPageElements(Arrays.asList("readyElement"), receipt),
							"Order confirmation page should be displayed",
							"Order confirmation page is displayed",
							"Order confirmation page is not displayed", driver);
										
			} else {
				Log.fail("Plcc modal is not displayed hence can't proceed further steps");
			}		
			
			Log.testCaseResult();
		}
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}//C24570

	
	@Test(groups = { "desktop", "tablet", "mobile"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_SMOKE_C23569(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
				
		int i = 1;
		try {
			
			String userEMailIdADS = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountData.get("password_global");
			String credential = userEMailIdADS + "|" + password;
			String alwaysApproveAddress = "plcc_always_approve_address";
			String firstName = checkoutData.get("plcc_always_approve_address").split("\\|")[7];
			String lastName = checkoutData.get("plcc_always_approve_address").split("\\|")[8];
			String prdId = TestData.get("prd_variation");
			boolean preApprovedStatusWithADS = false;			
			
			{
				GlobalNavigation.registerNewUserWithUserDetail(driver,0,0, firstName, lastName, credential);
				GlobalNavigation.addNewAddressToAccount(driver, alwaysApproveAddress, false, credential);
			}
			
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			homePage.headers.navigateToMyAccount(userEMailIdADS, password, false);
			Log.message(i++ +". navigated to my account page", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdId);
			Log.message(i++ +". navigated to PDP", driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". Product added to cart", driver);
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ +". Navigated to cart page", driver);
			
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ +". navigated to checkout page", driver);			
			
			checkoutPage.continueToPaymentByChoosingOriginalAddress(true);
			Log.message(i++ +". Clicked continue button from shipping section", driver);
			
			if(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlPLCCApproval"), checkoutPage)) {
				Log.message(i++ +". Pre Approval modal is displayed with valid data", driver);
				preApprovedStatusWithADS = true;
			}	
			
			cartPage = checkoutPage.clickEditItemSummary();
			homePage.headers.signOut();
			Log.message(i++ + ". Sign out from the site", driver);
			
			homePage = BrowserActions.clearCookies(driver);
			Log.message(i++ + ". Cleared cookies and navigated to Homepage", driver);
			
			
			if(preApprovedStatusWithADS) {
				
				homePage = homePage.headers.navigateToHome();
				Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
				
				pdpPage = homePage.headers.navigateToPDP(prdId);
				Log.message(i++ +". navigated to PDP", driver);
				
				pdpPage.addToBagCloseOverlay();
				Log.message(i++ +". Product added to cart", driver);				
				
				homePage.headers.navigateToMyAccount(userEMailIdADS, password, false);
				Log.message(i++ +". navigated to my account page", driver);				
				
				cartPage = homePage.headers.navigateToShoppingBagPage();
				Log.message(i++ +". Navigated to cart page", driver);
				
				checkoutPage = cartPage.clickOnCheckoutNowBtn();
				Log.message(i++ +". navigated to checkout page", driver);
				
				if(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlPLCCApproval"), checkoutPage)) {
					
					Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlPLCCApproval"), checkoutPage),
							"The pre-approval modal should be displayed",
							"The pre-approval modal is displayed",
							"The pre-approval modal is not displayed", driver);
					
					checkoutPage.clickGetItTodayInPLCC();
					Log.message(i++ +". Clicked Get it today button", driver);
					
					checkoutPage.typeTextInSSN("1254");
					Log.message(i++ +". Entered SSN number");	
					
					checkoutPage.selectDateMonthYearInPLCC2("10", "09", "1956");
					Log.message(i++ +". Selected date, month , year from the dropdowns", driver);
					
					checkoutPage.checkConsentInPLCC("yes");
					Log.message(i++ +". Selected consent checkbox ", driver);
					
					checkoutPage.typeTextInMobileInPLCC("8015841844");
					Log.message(i++ +". Updated phone number", driver);
					
					checkoutPage.clickOnAcceptInPLCC();
					Log.message(i++ +". Clicked yes, I Accept button");
					
					if(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"), checkoutPage)) {
						
						Log.message(i++ +". PLCC Approval modal is displayed", driver);
						
						checkoutPage.dismissCongratulationModal();
						Log.message(i++ +". Dismiss Congratulations modal", driver);
						
						Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("formShippingDetails"), checkoutPage),
								"Shipping address(Step 1) section should be displayed", 
								"Shipping address(Step 1) section is displayed",
								"Shipping address(Step 1) section is not displayed", driver);
						
						checkoutPage.checkUncheckUseThisAsBillingAddress(true);
						Log.message(i++ +". Check the 'Use this as billing address' check box", driver);
						
						checkoutPage.continueToPayment();
						Log.message(i++ + ". Continued to Payment Page", driver);
						
						Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("paymentmethodSection"), checkoutPage),
								"Payment method section should be displayed", 
								"Payment method section is displayed",
								"Payment method section is not displayed", driver);
						
						Log.softAssertThat(checkoutPage.checkPLCCCardSelectedBasedOnBrand(Utils.getCurrentBrandShort().substring(0, 2))
										|| checkoutPage.checkPLCCCardSelectedBasedOnBrand(BrandUtils.getBrandFullName()),
								"PLCC card should be selected by default",
								"PLCC card is selected by default",
								"PLCC card is not selected by default", driver);
						
						checkoutPage.continueToReivewOrder();
						Log.message(i++ +". Navigated to review & place order screen");
						
						
						if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
							Log.reference("Further verfication steps are not supported in current environment.");
							Log.testCaseResult();
							return;
						}
						
						if(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("plccCardNoError"), checkoutPage)) {
							Log.reference("By adding Always Approve data Incorrect card number is added, hence can't proceed");
						
						} else {
							
							checkoutPage.clickOnPlaceOrder();
							Log.message(i++ +". Clicked placed order button", driver);
							
							OrderConfirmationPage receipt = null;
							
							if(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPage)) {
								Log.reference("By using Always Approve currently user can't place order");
															
							} else{
								receipt = new OrderConfirmationPage(driver).get();
								Log.softAssertThat(receipt.elementLayer.verifyPageElements(Arrays.asList("readyElement"), receipt),
										"Order confirmation page should be displayed",
										"Order confirmation page is displayed",
										"Order confirmation page is not displayed", driver);
							}
						}
						
					} else {
						Log.reference("Approval modal is not displayed hence can't proceed further", driver);
					}
				}else {
					Log.fail("Checkout Step -1 overlay is not displayed hence can't proceed further", driver);
				}
			
			} else {
				Log.fail("User is not pre-Apprved hence can't proceed further");
			}	
			Log.testCaseResult();
		}
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}//C24569	
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
    public void TC_FBB_SMOKE_C25967(String browser) throws Exception {
        Log.testCaseInfo();
        final WebDriver driver = WebDriverFactory.get(browser);
        int i = 1;
        try {
            String guestEmailId = AccountUtils.generateEmail(driver);
            String checkoutAdd = "valid_address1";
            int cartUpdatedQty = 2;

            HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
            Log.message(i++ + ". Navigated to Home Page.", driver);
            
            Headers headers = homePage.headers;
            PlpPage plpPage = headers.navigateToRandomCategory();
            Log.message(i++ + ". Navigated to PLP: " + plpPage.getCategoryName(), driver);
            
			PdpPage pdpPage = plpPage.navigateToPdp();
            Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
            
            pdpPage.selectMinimumQuantityVariation(cartUpdatedQty);
            Log.message(i++ + ". Selected product variation.", driver);
            
            boolean isDiscountedProduct = pdpPage.elementLayer.isElementsDisplayed(Arrays.asList("lblDiscountedSalePrice"), pdpPage);
            
            pdpPage.clickAddProductToBag();
            Log.message(i++ + ". Product Added to Shopping Cart.", driver);

            pdpPage.closeAddToBagOverlay();
            Log.message(i++ + ". Closed the ATB overlay.", driver);
            
            ShoppingBagPage cartPage = headers.navigateToShoppingBagPage();
            Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
            
            Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("divOrderSubTotal", "orderSummaryShippingDetail", "orderSummarySalesTax", "lblOrderTotal"), cartPage, true),
                    "The system should display Subtotal, Standard Delivery Shipping & Handling, Estimated Sales Tax & Order Total",
                    "The system is displayed Subtotal, Standard Delivery Shipping & Handling, Estimated Sales Tax & Order Total",
                    "The system is not displayed Subtotal, Standard Delivery Shipping & Handling, Estimated Sales Tax & Order Total", driver);
            
            Double valueSubtotal = cartPage.getOrderSubTotal();
            Double valueShippingHandling = cartPage.getShippingPrice();
            Double valueOrderDiscount = cartPage.getOrderDiscount();
            Double valueShippingDiscount = cartPage.getOrderShippingDiscount();
            Double valueExtimateTax = cartPage.getSalesTax();
            Double valueOrderTotal = cartPage.getOrderTotal();
            DecimalFormat df = new DecimalFormat("#.##");
            
            String additionTotal = df.format(valueSubtotal - valueOrderDiscount + valueShippingHandling - valueShippingDiscount + valueExtimateTax);
            Log.softAssertThat(valueOrderTotal.toString().equals(additionTotal),
                    "The system should display Order Total = Subtotal +Standard Delivery",
                    "The system is displayed Order Total = Subtotal +Standard Delivery",
                    "The system is not displayed Order Total = Subtotal +Standard Delivery", driver);
            if (isDiscountedProduct) {
                Log.softAssertThat(cartPage.elementLayer.isElementsDisplayed(Arrays.asList("orderSummaryTotalSavings"), cartPage),
                        "If the product on discount the system will be display the total saving.",
                        "If the product on discount the system is displayed the total saving.",
                        "If the product on discount the system is not displayed the total saving.", driver);
            } else {
                Log.message(" Product has no discount, hence total saving is not displayed");
            }
            
            cartPage.updateQuantityByPrdIndex(0, ""+cartUpdatedQty);
            Log.message(i++ + ". Updated product quantity", driver);
            
            Log.softAssertThat(((float)cartPage.getProductSubtotal(0) == cartPage.getProductSubtotalEstimate(cartPage.getUnitPrice(0), cartUpdatedQty)),
                    "Subtotal should be increased based on the QTY",
                    "Subtotal is increased based on the QTY",
                    "Subtotal is not increased based on the QTY", driver);
            
            valueSubtotal = cartPage.getOrderSubTotal();
            valueShippingHandling = cartPage.getShippingPrice();
            valueOrderDiscount = cartPage.getOrderDiscount();
            valueShippingDiscount = cartPage.getOrderShippingDiscount();
            valueOrderTotal = cartPage.getOrderTotal();
            valueExtimateTax = cartPage.getSalesTax();
			additionTotal = df.format(valueSubtotal - valueOrderDiscount + valueShippingHandling - valueShippingDiscount + valueExtimateTax);
            Log.softAssertThat(valueOrderTotal.toString().equals(additionTotal),
                    "The system should display Order Total = Subtotal +Standard Delivery",
                    "The system is displayed Order Total = Subtotal +Standard Delivery",
                    "The system is not displayed Order Total = Subtotal +Standard Delivery", driver);
            
            CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
            Log.message(i++ + ". Navigated to Checkout Page.", driver);
            
            checkoutPage.continueToShipping(guestEmailId);
            Log.message(i++ + ". Continued as Guest with Email ID :: " + guestEmailId);
            
            checkoutPage.fillingShippingDetailsAsGuest("YES", checkoutAdd, ShippingMethod.Standard);
            
            Log.softAssertThat(valueSubtotal == cartPage.getOrderSubTotal(),
                    "Subtotal should be the same as step3",
                    "Subtotal is the same as step3",
                    "Subtotal is not same as step3", driver);
            
            Log.softAssertThat(valueShippingHandling == cartPage.getShippingPrice(),
                    "Standard Delivery should be the same as step3",
                    "Standard Delivery is the same as step3",
                    "Standard Delivery is not same as step3", driver);
            
            Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("orderSummarySalesTax"), cartPage)
                    		&& valueExtimateTax <= cartPage.getSalesTax(),
                    "Sales tax should display based on the shipping address",
                    "Sales tax is displayed based on the shipping address",
                    "Sales tax is not displayed based on the shipping address", driver);
            
            valueExtimateTax = cartPage.getSalesTax();
			additionTotal = df.format(valueSubtotal - valueOrderDiscount + valueShippingHandling - valueShippingDiscount + valueExtimateTax);
            Log.softAssertThat(valueOrderTotal.toString().equals(additionTotal),
                    "The system should display Order Total = Subtotal +Standard Delivery +Sales Tax",
                    "The system is displayed Order Total = Subtotal +Standard Delivery +Sales Tax",
                    "The system is not displayed Order Total = Subtotal +Standard Delivery +Sales Tax", driver);
            
            if (isDiscountedProduct) {
                Log.softAssertThat(cartPage.elementLayer.isElementsDisplayed(Arrays.asList("orderSummaryTotalSavings"), cartPage),
                        "If the product on discount the system will be display the total saving.",
                        "If the product on discount the system is displayed the total saving.",
                        "If the product on discount the system is not displayed the total saving.", driver);
            } else {
                Log.message(" Product has no discount, hence total saving is not displayed");
            }
        
            Log.testCaseResult();
        } // try
        catch (Exception e) {
            Log.exception(e, driver);
        } // catch
        finally {
            Log.endTestCase(driver);
        } // finally

    }//C25967
	
	
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
    public void TC_FBB_SMOKE_C25968(String browser) throws Exception {
        Log.testCaseInfo();

        final WebDriver driver = WebDriverFactory.get(browser);
        
        String username = AccountUtils.generateEmail(driver);
        String password = accountData.get("password_global");
        String credential = username + "|" + password;
        String taxLessAddress = "taxless_address";
        String prdVariation = TestData.get("prd_variation");
        String totalSavingsColor = TestData.get("total_savings_color");
        String giftCardAmount = TestData.get("giftcard_Amount");
        String giftCardFee = demandwareData.get("gift_Card_Fee");
        
        {
            GlobalNavigation.registerNewUser(driver, 0, 0, credential);
            GlobalNavigation.addNewAddressToAccount(driver, taxLessAddress, true, credential);
        }
              
        int i = 1;
        try {
        
            HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
            Log.message(i++ + ". Navigated to Home Page.", driver);
            
            SignIn signIn = homePage.headers.navigateToSignInPage();
            Log.message(i++ + ". Navigated to Sign in Page!", driver);

            signIn.navigateToMyAccount(username, password);
            Log.message(i++ + ". Navigated to My Account page as : " + username, driver);
            
            PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation);
            Log.message(i++ + ". Navigated to PDP.", driver);
            
            pdpPage.addToBagCloseOverlay();
            Log.message(i++ + ". Product added to cart!", driver);
            
            ShoppingBagPage shoppingBagPg = pdpPage.navigateToShoppingBag();
            Log.message(i++ + ". Navigated to Shopping bag page.", driver);
            
            Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("divOrderSubTotal", "orderSummaryShippingDetail", "orderSummarySalesTax"), shoppingBagPg),
                    "Subtotal, Shipping & Handling fee, and Estimated tax should be displayed on the Order Summary section.",
                    "Subtotal, Shipping & Handling fee, and Estimated tax is displayed on the Order Summary section.",
                    "Subtotal, Shipping & Handling fee, and Estimated tax not displayed on the Order Summary section.", driver);
            
            Log.softAssertThat(shoppingBagPg.verifySalesTax(0),
                    "Estimated tax should be ($0.00 / -) displayed, because user did not enter their shipping address.",
                    "Estimated tax is displayed as ($0.00 / -), because user did not enter their shipping address.",
                    "Estimated tax not displayed as ($0.00 / -) , because user did not enter their shipping address.", driver);
            
            if(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("productItemTotalMarkDown"), shoppingBagPg)) {
                Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryTotalSavings"), shoppingBagPg),
                        "If the product on discount the Total Savings should be displayed on the Order Summary section.",
                        "If the product on discount the Total Savings is displayed on the Order Summary section.",
                        "If the product on discount the Total Savings not displayed on the Order Summary section.", driver);
            }
            
            Double subTotalBefore = shoppingBagPg.getOrderSubTotal();
            Double shippingPriceBefore = shoppingBagPg.getShippingPrice();
            
            pdpPage = homePage.footers.navigateToPhysicalGiftCardPDP();
            Log.message(i++ + ". Navigated to gift card PDP page", driver);
            
            pdpPage.selectGCSize(giftCardAmount);
            Log.message(i++ + ". Gift card amount selected", driver);
            
            pdpPage.clickAddProductToBag();
            Log.message(i++ + ". Click add to bag button!", driver);
            
            shoppingBagPg = pdpPage.navigateToShoppingBag();
            Log.message(i++ + ". Navigated to Shopping bag page.", driver);
            
            Double subTotalAfter = shoppingBagPg.getOrderSubTotal();
            Double shippingPriceAfter = shoppingBagPg.getShippingPrice();
            
            Double Price = shippingPriceAfter - shippingPriceBefore;
            DecimalFormat df = new DecimalFormat("#.##");
            String shippingPrice = df.format(Price);
            
            Log.softAssertThat((subTotalAfter > subTotalBefore),
                    "Subtotal should get increased after adding gift card product to the cart.",
                    "Subtotal gets increased after adding gift card product to the cart.",
                    "Subtotal not gets increased after adding gift card product to the cart.", driver);
            
            Log.softAssertThat(shippingPrice.equalsIgnoreCase(giftCardFee),
                    "Shipping & Handling fee should increase by $1.95 after adding gift card product to the cart.",
                    "Shipping & Handling fee is increased by $1.95 after adding gift card product to the cart.",
                    "Shipping & Handling fee is not increased by $1.95 after adding gift card product to the cart.", driver);
            
            shoppingBagPg.clickOnShoppingCartToolTip();
            Log.message(i++ + ". Clicked on Shipping & Handling fee icon", driver);
            
            Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("deliveryMethodInTooltip", "giftCardFeeInTooltip", "totalFeeInTooltip"), shoppingBagPg),
                    "Shipping cost, Gift card fee, and Shipping & Handling total fee should be displayed on the Order Summary Tooltip Overlay.",
                    "Shipping cost, Gift card fee, and Shipping & Handling total fee is displayed on the Order Summary Tooltip Overlay.",
                    "Shipping cost, Gift card fee, and Shipping & Handling total fee is not displayed on the Order Summary Tooltip Overlay.", driver);
            
            shoppingBagPg.closeShippingOverlayToolTip();
            Log.message(i++ + ". Clicked close icon on Shipping & Handling tool-tip overlay", driver);
            
            Log.softAssertThat(shoppingBagPg.verifySalesTax(0),
                    "Estimated tax should be ($0.00 / -) displayed, because user did not reach the checkout page.",
                    "Estimated tax is displayed as ($0.00 / -) , because user did not reach the checkout page.",
                    "Estimated tax not displayed as ($0.00 / -) , because user did not reach the checkout page.", driver);
            
            Double cartTotal= shoppingBagPg.getOrderTotal();
            Double cartTotalAdded = Double.parseDouble(df.format(subTotalAfter + shippingPriceAfter));
            
            Log.softAssertThat(cartTotalAdded.compareTo(cartTotal) == 0,
                    "Order Total should be calculated as sum of (Subtotal +Standard Delivery)",
                    "Order Total is calculated as sum of (Subtotal +Standard Delivery)",
                    "Order Total is not calculated as sum of (Subtotal +Standard Delivery)", driver);
            
            if(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("productItemTotalMarkDown"), shoppingBagPg)) {
                Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryTotalSavings"), shoppingBagPg),
                        "If the product on discount the Total Savings should be displayed on the Order Summary section.",
                        "If the product on discount the Total Savings is displayed on the Order Summary section.",
                        "If the product on discount the Total Savings not displayed on the Order Summary section.", driver);
                
                Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementColor("orderSummaryTotalSavings", totalSavingsColor, shoppingBagPg),
                        "Total Savings should be displayed as red color",
                        "Total Savings is displaying as red color",
                        "Total Savings is not displayed as red color", driver);
            }
            
            CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
            Log.message(i++ + ". Navigated to Checkout page", driver);
            
            String checkoutSubTotal = checkoutPg.getSubtotalInSummary();
            Double checkoutShippingPrice = checkoutPg.getShippingPrice();
            
            Log.softAssertThat(String.valueOf(subTotalAfter).equalsIgnoreCase(checkoutSubTotal.replace(",", "")),
                    "Order Sub-total should be same as from cart page.",
                    "Order Sub-total is same as from cart page.",
                    "Order Sub-total is not same as from cart page.", driver);
            
            Log.softAssertThat(checkoutShippingPrice.compareTo(shippingPriceAfter) == 0,
                    "Order ShippingPrice should be same as from cart page.",
                    "Order ShippingPrice is same as from cart page.",
                    "Order ShippingPrice is not same as from cart page.", driver);
            
            Log.softAssertThat(checkoutPg.verifyOrderTotalCalculation(),
                    "Order Total should be calculated as sum of (Subtotal +Standard Delivery + Sales Tax)",
                    "Order Total is calculated as sum of (Subtotal +Standard Delivery + Sales Tax)",
                    "Order Total is not calculated as sum of (Subtotal +Standard Delivery + Sales Tax)", driver);
            
            if(!Utils.isMobile()) {
                if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("productItemTotalMarkDownDesktop"), checkoutPg)) {
                    Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryTotalSavings"), checkoutPg),
                            "If the product on discount the Total Savings should be displayed on the Order Summary section.",
                            "If the product on discount the Total Savings is displayed on the Order Summary section.",
                            "If the product on discount the Total Savings not displayed on the Order Summary section.", driver);
                }
            } else {
                if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("productItemTotalMarkDownMobile"), checkoutPg)) {
                    Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryTotalSavings"), checkoutPg),
                            "If the product on discount the Total Savings should be displayed on the Order Summary section.",
                            "If the product on discount the Total Savings is displayed on the Order Summary section.",
                            "If the product on discount the Total Savings not displayed on the Order Summary section.", driver);
                }
            }
            
            Log.testCaseResult();
        } // try
        catch (Exception e) {
            Log.exception(e, driver);
        } // catch
        finally {
        	GlobalNavigation.RemoveAllProducts(driver);
            Log.endTestCase(driver);
        } // finally
    
    }//C25968
	
	
}// search
