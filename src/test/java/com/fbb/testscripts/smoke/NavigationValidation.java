package com.fbb.testscripts.smoke;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.GiftCardPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlatinumCardApplication;
import com.fbb.pages.PlpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class NavigationValidation extends BaseTest {

	@SuppressWarnings("unused")
	@Test(groups = { "desktop", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_TEMPLATE_SCRIPT(String browser) throws Exception {
		Log.testCaseInfo();
		final WebDriver driver = WebDriverFactory.get(browser);
		
		EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

		// Flags
		boolean logIn = false;
		boolean signUp = false;
		boolean navigateProfile = false;
		boolean navigateAddress = false;
		boolean navigatePayment = false;

		boolean addToCart = true;
		boolean navigateSLP = true;
		boolean navigatePLP = true;
		boolean navigateQuickshop = true;
		boolean navigaetPDP = true;
		boolean navigateCQO = true;
		boolean navigateGiftCard = true;

		boolean checkPLCC = true;

		boolean checkoutGuest = true;

		// Data
		String userEmail = AccountUtils.generateEmail(driver);
		String userPassword = accountData.get("password_global");
		String userCredential = userEmail + "|" + userPassword;
		if (logIn) {
			GlobalNavigation.registerNewUser(driver, navigateAddress ? 1 : 0, navigatePayment ? 1 : 0, userCredential);
		}

		String searchTerm = TestData.get("product_search_terms").split("\\|")[1];
		Log.message("Search Term:: " + searchTerm);
		String plpCategory = TestData.get("level-1");
		Log.message("PLP navigation category:: " + plpCategory);
		String prdVariation = TestData.get("prd_variation");
		Log.message("Product ID:: " + prdVariation);
		String cqoSearch = TestData.get("qc_valid").split("\\|")[0];
		Log.message("CQO number:: " + cqoSearch);

		// Script
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to FBB homepage.", driver);
			Headers header = homePage.headers;
			Footers footer = homePage.footers;

			if (logIn) {
				MyAccountPage myAcc = header.navigateToMyAccount(userEmail, userPassword, logIn);
				Log.message(i++ + ". User logged in.", driver);

				if (navigateProfile) {
					ProfilePage profile = myAcc.navigateToUpdateProfile();
					Log.message(i++ + ". Navigated to profile update page.", driver);
				}

				if (navigateAddress) {
					AddressesPage address = myAcc.navigateToAddressPage();
					Log.message(i++ + ". Navigated to address page.", driver);
				}

				if (navigatePayment) {
					PaymentMethodsPage payment = myAcc.navigateToPaymentMethods();
					Log.message(i++ + ". Navigated to payment methods page.", driver);
				}
			} else if (signUp) {
				SignIn signIn = header.navigateToSignInPage();
				Log.message(i++ + ". Navigated to sign in/account creation page page", driver);

				signIn.clickOnCreateAccount();
				Log.message(i++ + ". Opened account creation section.", driver);
			}

			if (navigateSLP) {
				SearchResultPage slp = header.searchProductKeyword(searchTerm);
				Log.message(i++ + ". Searched for " + searchTerm, driver);

				if (navigateQuickshop && !navigatePLP) {
					QuickShop quickshop = slp.clickOnQuickShopByIndex(1);
					Log.message(i++ + ". Opened QuickShop panel for " + quickshop.getProductName(), driver);
				}
			}

			if (navigatePLP) {
				PlpPage plp = header.navigateTo(plpCategory);
				Log.message(i++ + ". Navigated to " + plpCategory, driver);

				if (navigateQuickshop) {
					QuickShop quickshop = plp.clickOnQuickShop();
					Log.message(i++ + ". Opened QuickShop panel for " + quickshop.getProductName(), driver);
					
					quickshop.closeQuickShopOverlay();
					Log.message(i++ + ". Closed QuickShop.", driver);
				}
			}

			if (navigaetPDP) {
				PdpPage pdp = header.navigateToPDP(prdVariation);
				Log.message(i++ + ". Navigated to " + pdp.getProductName(), driver);
				
				if(addToCart) {
					pdp.addToBagKeepOverlay();
					Log.message(i++ + ". Added product to bag.", driver);
					
					pdp.closeAddToBagOverlay();
					Log.message(i++ + ". Closed ATB overlay.", driver);
				}
			}

			if (navigateCQO) {
				QuickOrderPage cqo = header.navigateToQuickOrder();
				Log.message(i++ + ". Navigated to CQO page.", driver);

				cqo.searchItemInQuickOrder(cqoSearch);
				Log.message(i++ + ". Searched for catalog item.", driver);
			}

			if (navigateGiftCard) {
				GiftCardPage giftcard = footer.navigateToGiftCardLandingPage();
				Log.message(i++ + ". Navigated to gift card landing page.", driver);
				
				giftcard.clickOnCheckYourBalance();
				Log.message(i++ + ". Clicked on \"check your balance\" button", driver);
				
				giftcard.clickClose();
				Log.message(i++ + ". Closed balance check modal.", driver);
				
				giftcard.clickGiftCardButton();
				Log.message(i++ + ". Navigated to Gift Card", driver);
				
				giftcard = footer.navigateToGiftCardLandingPage();
				Log.message(i++ + ". Navigated to gift card landing page.", driver);
				
				giftcard.clickEGiftCardButton();
				Log.message(i++ + ". Navigated to e-Gift Card", driver);
			}

			if (checkPLCC) {
				PlatinumCardApplication plccApply = homePage.navigateToPLCCApplication();
				Log.message(i++ + ". Navigated to PLCC application page.", driver);
			}

			ShoppingBagPage cartpage = header.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to shopping bag page.", driver);
			
			boolean cartEmpty = header.getMiniCartCount().contentEquals("0"); 
			if (!cartEmpty) {
				CheckoutPage checkout = cartpage.clickOnCheckoutNowBtn();
				Log.message(i++ + ". Clicked on checkout button.", driver);

				if (!logIn && !signUp) {
					if (checkoutGuest) {
						checkout.continueToShipping(userEmail);
						Log.message(i++ + ". Continued checkout as guest with email:: " + userEmail, driver);
					} else {
						checkout.continueToShipping(userCredential);
						Log.message(i++ + ". Continued checkout as authenticated user for email:: " + userEmail, driver);
					}
				}
			}

			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}
	}
}
