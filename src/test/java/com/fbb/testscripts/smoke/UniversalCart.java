package com.fbb.testscripts.smoke;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.WishlistLoginPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.CollectionUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class UniversalCart extends BaseTest{
	
	EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	EnvironmentPropertiesReader demandWareProperty = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_UniversalCart_01(String browser) throws Exception {
		Log.testCaseInfo("UniversalCart Navigation");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to FBB homepage.", driver);
			Headers header = homePage.headers;
			
			List<Brand> brandsEnv = BrandUtils.getEnvPropertyBrands("enabledSharedCart");
			for(Brand targetBrand : brandsEnv) {
				Log.message(i++ + ". Navigating to " + targetBrand.getConfiguration(), driver);
				header.chooseBrandFromHeader(targetBrand);
				Log.message(i++ + ". Navigated to " + header.getBrandNameFromLogo(), driver);
				
				Log.softAssertThat(BrandUtils.compareBrandName(targetBrand.getConfiguration(), header.getBrandNameFromLogo()), 
						"Clicking on Header SC link should navigate to desired brand.", 
						"Navigated to desired brand", 
						"Did not navigate to desired brand.", driver);
			}
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase(driver);
		}
	}// TC_FBB_UniversalCart_01
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_UniversalCart_02(String browser) throws Exception {
		Log.testCaseInfo("UniversalCart Authentication");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String userEmail = AccountUtils.generateEmail(driver);
		String userPassword = accountData.get("password_global");
		String userCredential = userEmail + "|" + userPassword;
		
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, userCredential);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to FBB homepage.", driver);
			Headers header = homePage.headers;
			
			header.navigateToMyAccount(userEmail, userPassword);
			Log.message(i++ + ". User logged in.", driver);
			
			if (header.verifyUserIsLoggedIn()) {
				List<Brand> brandsEnv = BrandUtils.getEnvPropertyBrands("enabledSharedCart");
				for(Brand targetBrand : brandsEnv) {
					header.chooseBrandFromHeader(targetBrand);
					Log.message(i++ + ". Navigated to " + header.getBrandNameFromLogo(), driver);
					
					Log.softAssertThat(header.verifyUserIsLoggedIn(), 
							"User should stay logged in on " + targetBrand.getConfiguration(), 
							"User is logged in.", 
							"User is not logged in.", driver);
				}
			} else {
				Log.failsoft("User could not be logged in on initial SC site:: " + BrandUtils.getBrandFullName());
			}
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase(driver);
		}
	}// TC_FBB_UniversalCart_02
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_UniversalCart_03(String browser) throws Exception {
		Log.testCaseInfo("Active-Inactive Shared Cart Navigation Tile");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String tileColorActive = demandWareProperty.get("active_tile_bgColor");
		String tileColorInactive = demandWareProperty.get("inactive_tile_bgColor");
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to FBB homepage.", driver);
			Headers header = homePage.headers;
			
			List<Brand> brandsEnv = BrandUtils.getEnvPropertyBrands("enabledSharedCart");
			for(Brand targetBrand : brandsEnv) {
				header.chooseBrandFromHeader(targetBrand);
				Log.message(i++ + ". Navigated to " + header.getBrandNameFromLogo(), driver);
				
				Log.softAssertThat(header.elementLayer.verifyCssPropertyForElement("divActiveBrand", "background-color", tileColorActive, header)
								&& header.elementLayer.verifyCssPropertyForListElement("lstInactiveBrandSelectors", "background-color", tileColorInactive, header), 
						"Active and inactive brand tiles should be properly distinguished.", 
						"Brand tiles are properly distinguished.", 
						"Brand tiles are not properly distinguished.", driver);
				
				for(Brand brand : brandsEnv) {
					String hoverMessage = header.getHoverMessageFromBrandScTile(brand);
					
					Log.softAssertThat(BrandUtils.containBrandName(hoverMessage, brand.getConfiguration()), 
							"Hover message displayed should be brand specific", 
							"Message is brand specific", 
							"Message is not brand specific", driver);
				}
			}
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase(driver);
		}
	}// TC_FBB_UniversalCart_03
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_UniversalCart_04(String browser) throws Exception {
		Log.testCaseInfo("UniversalCart Guest Cart Item");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String userEmail = AccountUtils.generateEmail(driver);
		String checkoutAddress = "taxless_address";
		String paymentMethod = "card_Visa";
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to FBB homepage.", driver);
			Headers header = homePage.headers;
			
			List<Brand> brandsEnv = BrandUtils.getEnvPropertyBrands("enabledSharedCart");
			Map<String, Brand> sharedCartProducts = new HashMap<String, Brand>();
			Map<String, Brand> sharedCartVariationProducts = new HashMap<String, Brand>();
			
			ShoppingBagPage cartPage = null;
			
			for(Brand targetBrand : brandsEnv) {
				header.chooseBrandFromHeader(targetBrand);
				Log.message(i++ + ". Navigated to " + header.getBrandNameFromLogo(), driver);
				
				PdpPage pdp = header.navigateToPDP(TestData.get("prd_variation", targetBrand));
				Log.message(i++ + ". Navigated to PDP", driver);
				sharedCartProducts.put(pdp.getProductIdFromUrl(), pdp.getProductBrand());
				
				pdp.addToBagKeepOverlay();
				Log.message(i++ + ". Added product to cart.", driver);
				sharedCartVariationProducts.put(pdp.getVariationFromCartOverlay(), pdp.getProductBrand());
				
				cartPage = pdp.navigateToShoppingBag();
				Log.message(i++ + ". Navigated to cart.", driver);
				
				Log.softAssertThat(CollectionUtils.verifyMapsEqual(sharedCartProducts, cartPage.getProductIdAndBrandName()),
						"Cart products should be mapped to originating brands for redirect.",
						"Cart Products are correctly mapped.",
						"Cart products are not mapped correctly.", driver);
			}
			
			SignIn signIn = (SignIn) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			CheckoutPage checkoutPg = signIn.checkoutAsGuest(userEmail);
			Log.message(i++ + ". Continued as guest user", driver);
			
			Log.softAssertThat(CollectionUtils.verifyMapsEqual(sharedCartVariationProducts, checkoutPg.getProductVariationAndBrandName()),
					"Cart products in checkoutshould be mapped to originating brands for redirect.",
					"Cart Products are correctly mapped.",
					"Cart products are not mapped correctly.", driver);
			
			if (Utils.getCurrentEnv().equals("prod")) {
				Log.testCaseResult();
				Log.reference("Further verfication steps are not supported in current environment.");
				return;
			}
			
			checkoutPg.fillingShippingDetailsAsGuest("YES", checkoutAddress, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			/*HashMap<String, String> cardDetails = */checkoutPg.fillingCardDetails1(paymentMethod, false, false);
			Log.message(i++ + ". Filled in Card details.", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			Log.softAssertThat(CollectionUtils.verifyMapsEqual(sharedCartVariationProducts, checkoutPg.getProductVariationAndBrandName()),
					"Cart products in checkout should be mapped to originating brands for redirect.",
					"Cart Products are correctly mapped.",
					"Cart products are not mapped correctly.", driver);
			
			OrderConfirmationPage receipt = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			Log.softAssertThat(CollectionUtils.verifyMapsEqual(sharedCartVariationProducts, receipt.getProductVariationAndBrandName()),
					"Cart products in receipt should be mapped to originating brands for redirect.",
					"Cart Products are correctly mapped.",
					"Cart products are not mapped correctly.", driver);
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase(driver);
		}
	}// TC_FBB_UniversalCart_04
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_UniversalCart_05(String browser) throws Exception {
		Log.testCaseInfo("UniversalCart Authenticated Cart Item");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String userEmail = AccountUtils.generateEmail(driver);
		String userPassword = accountData.get("password_global");
		String userCredential = userEmail + "|" + userPassword;
		String paymentMethod = "card_Visa";
		
		{
			GlobalNavigation.registerNewUser(driver, 1, 0, userCredential);
			GlobalNavigation.emptyCartForUser(userEmail, userPassword, driver);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to FBB homepage.", driver);
			Headers header = homePage.headers;
			
			header.navigateToMyAccount(userEmail, userPassword);
			Log.message(i++ + ". User logged in.", driver);
			
			List<Brand> brandsEnv = BrandUtils.getEnvPropertyBrands("enabledSharedCart");
			Map<String, Brand> sharedCartProducts = new HashMap<String, Brand>();
			Map<String, Brand> sharedCartVariationProducts = new HashMap<String, Brand>();
			
			ShoppingBagPage cartPage = null;
			
			for(Brand targetBrand : brandsEnv) {
				header.chooseBrandFromHeader(targetBrand);
				Log.message(i++ + ". Navigated to " + header.getBrandNameFromLogo(), driver);
				
				PdpPage pdp = header.navigateToPDP(TestData.get("prd_variation", targetBrand));
				Log.message(i++ + ". Navigated to PDP", driver);
				sharedCartProducts.put(pdp.getProductIdFromUrl(), pdp.getProductBrand());
				
				pdp.addToBagKeepOverlay();
				Log.message(i++ + ". Added product to cart.", driver);
				sharedCartVariationProducts.put(pdp.getVariationFromCartOverlay(), pdp.getProductBrand());
				
				cartPage = pdp.navigateToShoppingBag();
				Log.message(i++ + ". Navigated to cart.", driver);
				
				Log.softAssertThat(CollectionUtils.verifyMapsEqual(sharedCartProducts, cartPage.getProductIdAndBrandName()),
						"Cart products should be mapped to originating brands for redirect.",
						"Cart Products are correctly mapped.",
						"Cart products are not mapped correctly.", driver);
			}
			
			CheckoutPage checkoutPg = (CheckoutPage) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			Log.softAssertThat(CollectionUtils.verifyMapsEqual(sharedCartVariationProducts, checkoutPg.getProductVariationAndBrandName()),
					"Cart products should be mapped to originating brands for redirect.",
					"Cart Products are correctly mapped.",
					"Cart products are not mapped correctly.", driver);
			
			if (Utils.getCurrentEnv().equals("prod")) {
				Log.testCaseResult();
				Log.reference("Further verfication steps are not supported in current environment.");
				return;
			}
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continud to payment", driver);
			
			/*HashMap<String, String> cardDetails = */checkoutPg.fillingCardDetails1(paymentMethod, false, false);
			Log.message(i++ + ". Filled in Card details.", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			Log.softAssertThat(CollectionUtils.verifyMapsEqual(sharedCartVariationProducts, checkoutPg.getProductVariationAndBrandName()),
					"Cart products should be mapped to originating brands for redirect.",
					"Cart Products are correctly mapped.",
					"Cart products are not mapped correctly.", driver);
			
			OrderConfirmationPage receipt = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			Log.softAssertThat(CollectionUtils.verifyMapsEqual(sharedCartVariationProducts, receipt.getProductVariationAndBrandName()),
					"Cart products should be mapped to originating brands for redirect.",
					"Cart Products are correctly mapped.",
					"Cart products are not mapped correctly.", driver);
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}
	}// TC_FBB_UniversalCart_05
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_UniversalCart_06(String browser) throws Exception {
		Log.testCaseInfo("UniversalCart Wishlist Login");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String userEmail = AccountUtils.generateEmail(driver);
		String userPassword = accountData.get("password_global");
		String userCredential = userEmail + "|" + userPassword;
		
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, userCredential);
			GlobalNavigation.RemoveWishListProductsWithLogin(driver, userCredential);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to FBB homepage.", driver);
			Headers header = homePage.headers;
			
			List<Brand> brandsEnv = BrandUtils.getEnvPropertyBrands("enabledSharedCart");
			Map<String, Brand> sharedCartProducts = new HashMap<String, Brand>();
			
			PdpPage pdp = header.navigateToPDP(TestData.get("prd_variation"));
			Log.message(i++ + ". Navigated to PDP", driver);
			sharedCartProducts.put(pdp.getProductIdFromUrl(), pdp.getProductBrand());
			
			WishlistLoginPage wishLogin = (WishlistLoginPage) pdp.addToWishlist();
			Log.message(i++ + ". Product added to wishlist.", driver);
			
			WishListPage wishlist = wishLogin.loginUser(userEmail, userPassword);
			Log.message(i++ + ". User logged in.", driver);
			
			Log.softAssertThat(CollectionUtils.verifyMapsEqual(sharedCartProducts, wishlist.getProductIdAndBrandName()),
					"Wishlist products should be mapped to originating brands for redirect.",
					"Wishlist Products are correctly mapped.",
					"Wishlist products are not mapped correctly.", driver);
			
			for(Brand targetBrand : brandsEnv) {
				header.chooseBrandFromHeader(targetBrand);
				Log.message(i++ + ". Navigated to " + header.getBrandNameFromLogo(), driver);
				
				wishlist = header.navigateToWishListPage();
				Log.message(i++ + ". Navigated to Wishlist page.", driver);
				
				Log.softAssertThat(CollectionUtils.verifyMapsEqual(sharedCartProducts, wishlist.getProductIdAndBrandName()),
						"Wishlist products should be mapped to originating brands for redirect.",
						"Wishlist Products are correctly mapped.",
						"Wishlist products are not mapped correctly.", driver);
			}
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			GlobalNavigation.RemoveWishListProducts(driver);
			Log.endTestCase(driver);
		}
	}// TC_FBB_UniversalCart_06
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_UniversalCart_07(String browser) throws Exception {
		Log.testCaseInfo("UniversalCart Wishlist Persistence");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String userEmail = AccountUtils.generateEmail(driver);
		String userPassword = accountData.get("password_global");
		String userCredential = userEmail + "|" + userPassword;
		
		{
			GlobalNavigation.registerNewUser(driver, 1, 1, userCredential);
			GlobalNavigation.RemoveWishListProductsWithLogin(driver, userCredential);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to FBB homepage.", driver);
			Headers header = homePage.headers;
			
			header.navigateToMyAccount(userEmail, userPassword);
			Log.message(i++ + ". User logged in.", driver);
			
			List<Brand> brandsEnv = BrandUtils.getEnvPropertyBrands("enabledSharedCart");
			Map<String, Brand> addedProducts = new HashMap<String, Brand>();
			Map<String, Brand> addedProductVariations = new HashMap<String, Brand>();
			
			WishListPage wishlist = null;
			
			for(Brand targetBrand : brandsEnv) {
				header.chooseBrandFromHeader(targetBrand);
				Log.message(i++ + ". Navigated to " + header.getBrandNameFromLogo(), driver);
				
				PdpPage pdp = header.navigateToPDP(TestData.get("prd_variation", targetBrand));
				Log.message(i++ + ". Navigated to PDP", driver);
				addedProducts.put(pdp.getProductIdFromUrl(), pdp.getProductBrand());
				
				pdp.addToWishlist();
				Log.message(i++ + ". Product added to wishlist.", driver);
				addedProductVariations.put(pdp.getVariationFromCartOverlay(), pdp.getProductBrand());
				
				wishlist = header.navigateToWishListPage();
				Log.message(i++ + ". Navigated to Wishlist page.", driver);
				
				Log.softAssertThat(CollectionUtils.verifyMapsEqual(addedProducts, wishlist.getProductIdAndBrandName()),
						"Wishlist products should be mapped to originating brands for redirect.",
						"Wishlist Products are correctly mapped.",
						"Wishlist products are not mapped correctly.", driver);
				
				Log.softAssertThat(CollectionUtils.verifyMapsEqual(addedProductVariations, wishlist.getProductVariationAndBrandName()),
						"Wishlist variation products should be mapped to originating brands for redirect.",
						"Wishlist variation products are correctly mapped.",
						"Wishlist variation products are not mapped correctly.", driver);
			}
			
			Map<String, Brand> wishlistVariationProducts = wishlist.getProductVariationAndBrandName();
			wishlist.addAllToCart();
			Log.message(i++ + ". All wishlist items added to cart.", driver);
			
			ShoppingBagPage cartPage = header.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to cart.", driver);
			
			Map<String, Brand> cartVariationProducts = cartPage.getProductVariationAndBrandName();
			Log.softAssertThat(CollectionUtils.verifyMapsEqual(wishlistVariationProducts, cartVariationProducts),
					"Cart items should preserve brand mapping from wishlist.",
					"Cart items preserved mapping from wishlist.",
					"Cart items did not preserve mapping from wishlist.", driver);
			
			if (Utils.getCurrentEnv().equals("prod")) {
				Log.testCaseResult();
				Log.reference("Further verfication steps are not supported in current environment.");
				return;
			}
			
			CheckoutPage checkoutPg = (CheckoutPage) cartPage.navigateToCheckout();
			Log.message(i++ + ". Clicked on checkout now button.", driver);
			
			checkoutPg.enterCVV("123");
			Log.message(i++ + ". Entered cvv number in the field", driver);
			
			OrderConfirmationPage orderConfirmationPg = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on 'Place Order' button!", driver);
			
			Log.softAssertThat(CollectionUtils.verifyMapsEqual(cartVariationProducts, orderConfirmationPg.getProductVariationAndBrandName()),
					"Order confirmation page should preserve brand mapping from cart.",
					"Order confirmation page preserved mapping from cart.",
					"Order confirmation page did not preserve mapping from cart.", driver);
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			GlobalNavigation.RemoveWishListProducts(driver);
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}
	}// TC_FBB_UniversalCart_07
	
	@Test(groups = { "desktop", "mobile", "tablet", "release", "daily" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void TC_FBB_UniversalCart_08(String browser) throws Exception {
		Log.testCaseInfo("UniversalCart Direct PDP");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String pdpURL = Utils.getWebSite() + UrlUtils.getPath(TestData.get("prd_URL"));
		String authenticatedURL = UrlUtils.getProtectedWebSiteWithYotta(pdpURL);
		
		int i = 1;
		try {
			driver.get(authenticatedURL);
			PdpPage pdp = new PdpPage(driver).get();
			Log.message(i++ + ". Navigated to FBB PDP:: " + pdp.getProductName(), driver);
			
			Log.testCaseResult();
		} catch (Exception e) {
			Log.exception(e, driver);
		} finally {
			Log.endTestCase(driver);
		}
	}// TC_FBB_UniversalCart_08
}
