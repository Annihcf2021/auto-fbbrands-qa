package com.fbb.testscripts.uc;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.MiniCartPage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_UC_C26129 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_UC_C26129(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			//step 1 - Navigate to the website
			String userEMailId = AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = userEMailId + "|" + password;
			String prd_surchargeRMX = TestData.get("prd_surcharge", Brand.rmx);
			String lvl1ELX = TestData.get("level-1", Brand.elx);
			String payment = "card_Visa";
			String prd_BackorderELX = TestData.get("prd_back-order", Brand.elx).split("\\|")[0];
			String color_BackorderELX = TestData.get("prd_back-order", Brand.elx).split("\\|")[1];
			String size_BackorderELX = TestData.get("prd_back-order", Brand.elx).split("\\|")[2];
			String checkoutAdd = "valid_address1";
			String billingAdd = "valid_address2";
			
			{
				GlobalNavigation.registerNewUser(driver, 0, 0, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			Headers headers = homePage.headers;
			
			HashSet<String> productsAdded = new HashSet<String>();
			productsAdded.add(prd_BackorderELX);
			productsAdded.add(prd_surchargeRMX);
			
			headers.chooseBrandFromHeader(Brand.elx);
			Log.message(i++ +". Choosed ELX brand from header", driver);
			
			PlpPage plpPage = headers.navigateTo(lvl1ELX);
			Log.message(i++ + ". Navigated to '"+lvl1ELX+" page'!", driver);
			
			Log.softAssertThat(plpPage.verifyCategoryHeader(lvl1ELX.trim()),
					"User should have navigated to correct category page.", 
					"User navigated to correct category page.", 
					"User has not navigated to correct category page.", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("divRefinement"), plpPage),
					"Horizontal & vertical refinement bar should be available in the Product listing page", 
					"Horizontal & vertical refinement bar is available in the Product listing page", 
					"Horizontal & vertical refinement bar is not available in the Product listing page", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divRefinement", "ProductTiles", plpPage) && 
					plpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divRefinement", "ProductSwatches", plpPage),
					"Below product title product name and color swatches should display", 
					"Below product title product name and color swatches is display", 
					"Below product title product name and color swatches is not display", driver);
			
			//String productId = plpPage.getProductWithColorSwatch(1);
			
			//String color = plpPage.selectProductColorInPLP(productId, 1);
			
			Log.softAssertThat(plpPage.verifyProductImageUpdate(1),
					"Product image should be updated based on the color selected", 
					"Product image is updated based on the color selected", 
					"Product image is not updated based on the color selected", driver);
			
			PdpPage pdpPage = headers.navigateToPDP(prd_BackorderELX);
			Log.message(i++ + ". Navigated to 'PdpPage page'!", driver);
			
			String prd_BackOrder_Name = pdpPage.getProductName();
			
			pdpPage.selectColor(color_BackorderELX);
			Log.message(i++ + ". Color Selected!", driver);
			
			pdpPage.selectSize(size_BackorderELX);
			Log.message(i++ + ". Size Selected!", driver);
			
			ShoppingBagPage cartPage = pdpPage.addToBagAndNavigate();
			Log.message(i++ + ". Added product to bag!", driver);
			
			double subTotalOld = cartPage.getOrderSubTotal();
			
			Log.softAssertThat(cartPage.verifyProductAddedInCart(prd_BackorderELX),
					"Product should be added in a cart", 
					"Product is added in a cart", 
					"Product is not added in a cart", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementTextContains("lblProductAvailability", "Ship", cartPage),
					"\"Ship soon\" special message should be displayed on the back-order product", 
					"\"Ship soon\" special message is displayed on the back-order product", 
					"\"Ship soon\" special message is not displayed on the back-order product", driver);
			
			headers.chooseBrandFromHeader(Brand.rmx);
			Log.message(i++ +". Choosed RMX brand from header", driver);
			
			headers.typeTextInSearchField(prd_surchargeRMX);
			Log.message(i++ + ". Typed in the Search Field!", driver);
			
			Log.softAssertThat(headers.getEnteredTextFromSearchTextBox().trim().equals(prd_surchargeRMX.trim()), 
					"Text should be entered in the Search text box", 
					"Text is entered in the Search text box", 
					"Text is not entered in the Search text box", driver);
			
			pdpPage = headers.navigateToPDP(prd_surchargeRMX);
			Log.message(i++ + ". Navigated to 'PdpPage page'!", driver);
			
			String prd_SurCharge_Name = pdpPage.getProductName();
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productName", "divReviewAndRating", pdpPage), 
					"Product name should display above the review stars", 
					"Product name is displaying above the review stars", 
					"Product name is not displaying above the review stars", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("sectionReviewTab","sectionDetailsTab"), pdpPage), 
					"Check the review and detail sections are displayed",
					"The review and detail sections are displayed",
					"The review and detail sections are not displayed", driver);
			
			pdpPage.clickOnDetailsTab();
			Log.message(i++ + ". Clicked on Details tab", driver);
	
			if (pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtCustomerGalleryHeading"), pdpPage)) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divSocialIcons", "txtCustomerGalleryHeading", pdpPage), 
						"Check the Content Gallery displayed below the Share Icons",
						"The Content Gallery displayed below the Share Icons",
						"The Content Gallery is not displayed below the Share Icons", driver);
			} else {
				Log.failsoft("Customer Gallery is not displayed in the Product Detail page");
			}
			
			pdpPage.clickOnReviewTab();
			Log.message(i++ + ". Clicked on review tab", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("sectionReviewVisible"), pdpPage), 
					"Check the review section is opened",
					"The review section is opened",
					"The review section is not opened", driver);
			
			pdpPage.selectAllSwatches();
			Log.message(i++ + ". Selected all the swatches!", driver);
			
			cartPage = pdpPage.addToBagAndNavigate();
			Log.message(i++ + ". Added product to bag!", driver);
			
			Log.softAssertThat(cartPage.verifyProductAddedInCart(prd_BackorderELX), 
					"Confirm the selected back order product added from the Ellos displaying on the cart page",
					"The selected back order product added from the Ellos displaying on the cart page",
					"The selected back order product added from the Ellos is not displaying on the cart page", driver);
			
			Log.softAssertThat(cartPage.verifyProductAddedInCart(prd_surchargeRMX), 
					"Confirm the selected surcharge product added from the Roamans brand is displaying on the cart page",
					"The selected surcharge product added from the Roamans brand is displaying on the cart page",
					"The selected surcharge product added from the Roamans brand is not displaying on the cart page", driver);
			
			Log.softAssertThat(cartPage.getOrderSubTotal() > subTotalOld, 
					"Confirm the subtotal is updated in order Summary",
					"The subtotal is updated in order Summary",
					"The subtotal is not updated in order Summary", driver);
			
			headers.chooseBrandFromHeader(Brand.jlx);
			Log.message(i++ +". Choosed JLX brand from header", driver);
			
			HashSet<String> PrdNames = new HashSet<String>();
			PrdNames.add(prd_BackOrder_Name);
			PrdNames.add(prd_SurCharge_Name);
			
			MiniCartPage miniCart;
            if(!Utils.isMobile()) {
                miniCart = headers.mouseOverMiniCart();
                Log.message(i++ +". Mouse hover on my bag icon.", driver);
                
				Log.softAssertThat(miniCart.verifyProductDisplayedInOverlay(PrdNames), 
						"Confirm the selected surcharge product added from the Roamans brand is displaying on the cart page",
						"The selected surcharge product added from the Roamans brand is displaying on the cart page",
						"The selected surcharge product added from the Roamans brand is not displaying on the cart page", driver);
			}
			
			Log.softAssertThat(headers.getMiniCartCount().trim().equals("2"), 
					"Added product counts should be displayed in my bag icon",
					"Added product counts is displayed in my bag icon",
					"Added product counts is not displayed in my bag icon", driver);
			
			cartPage = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart Page", driver);
			
			HashSet<String> productIdList = new HashSet<String>();
			productIdList = cartPage.getProductIdList();
			
			HashSet<String> productNameListCart = new HashSet<String>();
			productNameListCart = cartPage.getCartItemNameList();
			
			Log.softAssertThat(cartPage.elementLayer.compareTwoHashSet(productsAdded, productIdList), 
					"Selected product from PDP should be displayed on the cart page.", 
					"Selected product from PDP is displayed on the cart page.", 
					"Selected product from PDP is not displayed on the cart page.", driver);
			
			CheckoutPage checkoutPg = (CheckoutPage) cartPage.navigateToCheckout();
			
			checkoutPg.continueToShipping(credential);
			Log.message(i++ + ". Navigated to Shipping section", driver);
			
			checkoutPg.fillingShippingDetailsAsSignedInUser("NO", "NO", "NO", ShippingMethod.Express, checkoutAdd);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPg.fillingBillingDetailsAsSignedUser("NO", "NO", billingAdd);
			Log.message(i++ +". Filled different Billing address", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			//Step 8 - Navigate to Checkout page - step 2
									
			checkoutPg.fillingCardDetails1(payment, true, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			HashSet<String> productNameList = checkoutPg.getCheckoutSummeryNameList();
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			Log.softAssertThat(checkoutPg.elementLayer.compareTwoHashSet(productNameList, productNameListCart), 
					"The product added to cart should be updated in the checkout page", 
					"The product added to cart is updated in the checkout page", 
					"The product added to cart is not updated in the checkout page", driver);
			
			Log.softAssertThat(checkoutPg.comparePaymentBeforeOrderWithEnteredPaymentMethod(payment), 
					"The system should keep the payment details", 
					"Payment method is saved and displayed", 
					"Payment method is not saved correctly", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
								
			//Step 9 - Navigate to Checkout page - step 3
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			Log.softAssertThat(receipt.checkEnteredShippingAddressReflectedInOrderReceipt(checkoutAdd), 
					"Same shipping address should display in the receipt which is entered in checkout shipping detail", 
					"Same shipping address is displaying", 
					"Different shipping address is displaying", driver);
			
			Log.softAssertThat(receipt.checkEnteredBillingAddressReflectedInOrderReceipt(checkoutAdd), 
					"Same billing address should display in the receipt which is entered in checkout billing detail", 
					"Same billing address is displaying", 
					"Different billing address is displaying", driver);
			
			Log.softAssertThat(receipt.comparePaymentAfterOrderWithEnteredPaymentMethod(payment), 
					"Same payment method should display in the receipt which is entered in checkout payment section", 
					"Same payment method is displaying", 
					"Different payment method is displaying", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblProfileEmail", userEMailId, receipt), 
					"The mail ID entered should reflected in receipt", 
					"The mail ID entered is reflected in receipt", 
					"The mail ID entered is not reflected in receipt", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblOrderReceipt", userEMailId, receipt), 
					"The order receipt should send to the entered mail ID", 
					"The order receipt is send to the entered mail ID", 
					"The order receipt is not send to the entered mail ID", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt), 
					"The user can able to take print out of the order receipt", 
					"The user can able to print the order receipt", 
					"The user cannot able to print the order receipt", driver);
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_UC_C26129