package com.fbb.testscripts.uc;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlatinumCardApplication;
import com.fbb.pages.PlatinumCardLandingPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_UC_C26135 extends BaseTest {
	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "uc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_UC_C26135(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		String userEMailId = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
		String validPassword = accountData.get("password_global");
		String inValidEmail = accountData.get("Invalid_format_email");
		String level1Cat = TestData.get("level-1");
		String errorColor = TestData.get("error_color_2");
		String checkoutAdd = "valid_address7";

		// Always Approve Data
		String addressDetails1 = checkoutData.get("plcc_always_approve_address");
		String ssnDetails = checkoutData.get("SSNdetails");
		HashMap<String, String> plccData = GlobalNavigation.formatPLCCAddressToMapWithSSN(addressDetails1, ssnDetails, driver, userEMailId);
		String firstName = addressDetails1.split("\\|")[7].trim();
		HashSet<String> productsAdded = new HashSet<String>();

		int i = 1;
		try {
			// step 1 - Navigate to the universal cart website as the Roamans brand.
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.softAssertThat(homePage.checkPrimaryLogoBrandName(Brand.rmx) 
					&& homePage.headers.getActiveSharedCartBrandName().equals(Brand.rmx),
					"Home page should be displayed for RM brand site",
					"Home page is displayed for RM brand site",
					"Home page is not displayed for RM brand site", driver);

			Log.softAssertThat(homePage.headers.verifyActiveScBrandLocation(Brand.rmx),
					"RM logo should move & display as 1st brand logo in the utility bar.",
					"RM logo is moved & displayed as 1st brand logo in the utility bar.",
					"RM logo is not moved & displayed as 1st brand logo in the utility bar.", driver);

			Headers header = homePage.headers;

			// Step 2 - Click on learn more link from the PLCC banner.

			PlatinumCardLandingPage plccLandingPage = header.clickOnLearnmorelinkPLCC();
			Log.softAssertThat(plccLandingPage.elementLayer.verifyPageElements(Arrays.asList("divplccLandingPage"), plccLandingPage),
					"The 'LEARN MORE' link should take the User to a PLCC content ",
					"The 'LEARN MORE' link took the User to a PLCC content ",
					"The 'LEARN MORE' link should did not take the User to a PLCC content ", driver);

			Log.softAssertThat(plccLandingPage.verifyTopBannerSlotContents(),
					"The Offer contents on the PLCC landing page should be displayed properly",
					"The Offer contents on the PLCC landing page is displayed properly",
					"The Offer contents on the PLCC landing page is not displayed properly", driver);

			Log.softAssertThat(plccLandingPage.elementLayer.verifyPageElements(Arrays.asList("headingBenefits", "headingRewards", "headingFAQS", "headingPlatinumPerk"), plccLandingPage)
					&& plccLandingPage.elementLayer.verifyPageElements(Arrays.asList("exclusiveBenefitsCopy", "sectionRewards", "sectionFAQS", "platinumPerksLogos"), plccLandingPage),
					"Verify the options. (BENEFITS, REWARDS, FAQS, PLATINUM PERK) and respective sections should be displayed",
					"Verify the options. (BENEFITS, REWARDS, FAQS, PLATINUM PERK) and respective sections is displayed",
					"Verify the options. (BENEFITS, REWARDS, FAQS, PLATINUM PERK) and respective sections not displayed", driver);

			PlatinumCardApplication plccApplication = plccLandingPage.clickOnApplyButton();
			Log.message(i++ + ". Navigated to Web Instant Create Page", driver);

			// Step 3 - Navigate to Web Instant Create Page

			plccApplication.clickOnRegisterBtn();
			Log.message(i++ + ". Clicked on the submit button, without entering any fields.", driver);

			Log.softAssertThat(plccApplication.elementLayer.verifyListElementSize("errorAllMandatoryFields", 12, plccApplication)
					&& plccApplication.elementLayer.verifyPageListElements(Arrays.asList("errorAllMandatoryFields"), plccApplication),
					"An error message should be displayed for all the mandatory fields.",
					"An error message is displayed for all the mandatory fields.",
					"An error message is not displayed for all the mandatory fields.", driver);

			plccApplication.fillPLCCApplication(plccData);
			Log.message(i++ + ". Entered Always, approve address", driver);

			plccApplication.clickCheckUncheckConsent(true);
			Log.message(i++ + ". Clicked on consent checkbox", driver);

			plccApplication.clickOnRegisterBtn();
			Log.message(i++ + ". Clicked on SUBMIT button.", driver);

			if (plccApplication.elementLayer.verifyElementDisplayed(Arrays.asList("divApprovedModal"), plccApplication)) {
				Log.softAssertThat(plccApplication.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divApprovedModal"), plccApplication),
						"PlatinumCard approved modal should get display",
						"PlatinumCard approved modal is getting display",
						"PlatinumCard approved modal is not getting display", driver);

				String creditLimit = plccApplication.elementLayer.getElementText("divUnderReviewSubhead", plccApplication);
				double creditAmount = StringUtils.getPriceFromString(creditLimit);
				
				Log.softAssertThat((plccApplication.elementLayer.verifyElementTextEqualTo("divUnderReviewHeader", "Welcome, " + firstName + "!", plccApplication)
						&& plccApplication.elementLayer.verifyElementDisplayed(Arrays.asList("btnApprovedModalGuestSignIn", "btnApprovedModalContinueShopping"), plccApplication)
						&& creditLimit.toLowerCase().contains("credit limit") && (creditAmount >= 500)),
						"PLCC modal should display the first name, Credit limit, Sign in to save the card and Continue shopping buttons.",
						"PLCC modal is displaying the first name, Credit limit, Sign in to save the card and Continue shopping buttons.",
						"PLCC modal is not displaying the first name, Credit limit, Sign in to save the card and Continue shopping buttons.", driver);

				plccApplication.clickApprovedModalContinueShopping();
				Log.message(i++ + ". Clicked on Continue Shopping button.", driver);

				Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage),
						"Home page should be displayed",
						"Home page is getting displayed",
						"Home page is not getting displayed", driver);

				// Step 4 - Click on the Ellos Favicon from the Header section on the universal
				// cart website.

				header.chooseBrandFromHeader(Brand.elx);
				Log.message(i++ + ". Navigated to '" + BrandUtils.getBrandFullName(Brand.elx) + "' Home Page!", driver);

				Log.softAssertThat(homePage.checkPrimaryLogoBrandName(Brand.elx)
						&& homePage.headers.getActiveSharedCartBrandName().equals(Brand.elx),
						"Home page should be displayed for EL brand site",
						"Home page is displayed for EL brand site",
						"Home page is not displayed for EL brand site", driver);

				// Step 5 - Click on any category. Ex: "Dresses".

				PlpPage plpPage = homePage.headers.navigateTo(level1Cat);
				Log.message(i++ + ". Navigated to PLP", driver);

				BrowserActions.scrollToBottomOfPage(driver);
				Log.message(i++ + ". Scrolled to bottom of the page", driver);

				Log.softAssertThat(homePage.headers.checkGlobalHeaderIsStickyWithoutScrollDown()
						&& plpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("stickyGlobalNavigation", "divRefinement", "btnBackToTop"), plpPage),
						"'sticky header', 'refinements' and 'back to top' icon should be displayed.",
						"'sticky header', 'refinements' and 'back to top' icon are displayed.",
						"'sticky header', 'refinements' and 'back to top' icon are not displayed.", driver);

				plpPage.clickBackToTopButton();
				Log.message(i++ + ". Clicked on Back to Top button.", driver);

				Log.softAssertThat(plpPage.elementLayer.getElementCSSValue("btnBackToTop", "display", plpPage).equalsIgnoreCase("none")
						|| !plpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnBackToTop"), plpPage),
						"The user should take back to the top of the page and the button should be removed.",
						"The user should take back to the top of the page and the button is removed.",
						"The user should take back to the top of the page and the button is not removed.", driver);

				Log.softAssertThat(plpPage.VerifyBackViewDisplayedOrNot(),
						"On mouse hover, The system should display the back view image",
						"On mouse hover, The system is displayed the back view image",
						"On mouse hover, The system is not displayed the back view image", driver);

				// Step 6 - Navigate to the product details page.

				PdpPage pdpPage = plpPage.navigateToPdp(1);
				Log.message(i++ + ". Naviagted to first product PDP: " + pdpPage.getProductName(), driver);

				productsAdded.add(pdpPage.getProductIdFromUrl());
				pdpPage.addToBagCloseOverlay();
				Log.message(i++ + ". Product added to cart!", driver);

				// Step 7 - Click on the Jessica London Favicon from the Header section on the
				// universal cart website.

				header.chooseBrandFromHeader(Brand.jlx);
				Log.message(i++ + ". Navigated to '" + BrandUtils.getBrandFullName(Brand.jlx) + "' Home Page!", driver);

				Log.softAssertThat(homePage.checkPrimaryLogoBrandName(Brand.jlx)
						&& homePage.headers.getActiveSharedCartBrandName().equals(Brand.jlx),
						"Home page should be displayed for JL brand site",
						"Home page is displayed for JL brand site",
						"Home page is not displayed for JL brand site", driver);

				// Step 8 - Search with invalid text and hit the enter button.

				SearchResultPage slp = header.navigateToSLP("TTSS");
				Log.message(i++ + ". The no search result page should be displayed.", driver);

				Log.softAssertThat(slp.elementLayer.verifyElementDisplayed(Arrays.asList("lblNotAbleToFindResults"), slp),
						"No search result page should be displayed.",
						"No search result page is displayed.",
						"No search result page is not displayed.", driver);

				Log.softAssertThat(slp.verifyNoResultsFoundMessage(),
						"In Secondary search box, place holder text should be displayed like We were not able to find results for \"TTSS\"",
						"In Secondary search box, place holder text is displayed like We were not able to find results for \"TTSS\"",
						"In Secondary search box, place holder text is not displayed like We were not able to find results for \"TTSS\"", driver);

				Log.softAssertThat(slp.elementLayer.verifyElementDisplayed(Arrays.asList("lblNoResults", "lblTrendingNow"), slp),
						"No Results heading and trending now section should be displayed.",
						"No Results heading and trending now section is displayed.",
						"No Results heading and trending now section is not displayed.", driver);

				slp = slp.clickDidYouMeanSuggestionLink();
				Log.message(i++ + ". The search result page should be displayed.", driver);

				// Step 9 - Navigate to the search result page.

				if (slp.getSearchResultCount() > 60) {
					if (Utils.isDesktop()) {
						slp.clickNextButtonInPagination();
						Log.message(i++ + ". Clicked on next link in pagination", driver);

						Log.softAssertThat(slp.getProductTileCount() <= 60,
								"Next 60 more products should load.",
								"Next 60 more products are loaded.",
								"Next 60 more products are not loaded.", driver);
					} else {
						int prdCountBefore = slp.getProductTileCount();
						slp.clickOnViewMore();
						Log.message(i++ + ". Clicked on view more link in pagination", driver);

						Log.softAssertThat(slp.getProductTileCount() > prdCountBefore,
								"Next 60 more products should load.",
								"Next 60 more products are loaded.",
								"Next 60 more products are not loaded.", driver);
					}
				}

				// Step 10 - Navigate to the Product Details Page

				pdpPage = plpPage.navigateToPdp(1);
				Log.message(i++ + ". Navigated to PDP: " + pdpPage.getProductName(), driver);

				productsAdded.add(pdpPage.getProductIdFromUrl());
				pdpPage.addToBagKeepOverlay();
				Log.message(i++ + ". Product added to cart!", driver);

				// Step 11 - Navigate to Cart page

				ShoppingBagPage cartPage = pdpPage.clickOnCheckoutInMCOverlay();
				Log.message(i++ + ". Navigated to Shopping bag page", driver);

				HashSet<String> productNameListCart = new HashSet<String>();
				productNameListCart = cartPage.getCartItemNameList();

				Double valueSubtotal = cartPage.getOrderSubTotal();
				Double valueShippingHandling = cartPage.getShippingPrice();
				Double valueOrderTotal = cartPage.getOrderTotal();
				DecimalFormat df = new DecimalFormat("#.##");

				String additionTotal = df.format(valueSubtotal + valueShippingHandling);
				Log.softAssertThat(valueOrderTotal.toString().equals(additionTotal),
						"The system should display Order Total = Subtotal + Standard Delivery",
						"The system is displayed Order Total = Subtotal + Standard Delivery",
						"The system is not displayed Order Total = Subtotal + Standard Delivery", driver);

				// Step 12 - Navigate to checkout login page

				CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
				Log.message(i++ + ". Navigated to Checkout page", driver);

				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("signInLoginBox", "lblCheckoutAsGuest"), checkoutPage),
						"The checkout login page should be displayed with the sign, Guest checkout sections.",
						"The checkout login page is displayed with the sign, Guest checkout sections.",
						"The checkout login page is not displayed with the sign, Guest checkout sections.", driver);

				HashSet<String> productNameList = checkoutPage.getCheckoutSummeryNameList();

				Log.softAssertThat(checkoutPage.elementLayer.compareTwoHashSet(productNameList, productNameListCart),
						"The product added to cart should be updated in the checkout page",
						"The product added to cart is updated in the checkout page",
						"The product added to cart is not updated in the checkout page", driver);

				checkoutPage.enterInvalidGuestUserEmail(inValidEmail);
				Log.message(i++ + ". Entered invalid email address", driver);

				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblGuestEmailError"), checkoutPage)
						&& checkoutPage.elementLayer.verifyElementColor("lblGuestEmailError", errorColor, checkoutPage),
						"Email field placeholder text will become red color and the message should display as 'Please enter a valid email.'",
						"Email field placeholder text is red color and the message is displayed as 'Please enter a valid email.'",
						"Email field placeholder text is not red color and the message not displayed as 'Please enter a valid email.'", driver);

				checkoutPage.continueToShipping(userEMailId);
				Log.message(i++ + ". Navigated to Shipping section", driver);

				// Step 13 - Navigate to the shipping address section.
				// SC-2919(Remove Tax Validation and Allow Orders to Flow to Mainframe) - Skipping tax error validation steps

				checkoutPage.fillingShippingDetailsAsGuest("YES", checkoutAdd, ShippingMethod.Standard);
				Log.message(i++ + ". Shipping Address entered with correct zipcode successfully", driver);

				// Step 14 - Navigate to Checkout page - step 2

				checkoutPage.continueToPayment();
				Log.message(i++ + ". Continued to Payment Page", driver);
				
				if (!Utils.isMobile()) {
					Log.softAssertThat(checkoutPage.elementLayer.verifyElementTextContains("drpCardType", "Credit Card", checkoutPage),
							"PLCC card should be selected when PLCC card approved",
							"PLCC card is selected when PLCC card approved",
							"PLCC card is not selected when PLCC card approved", driver);
				} else {
					Log.softAssertThat(checkoutPage.elementLayer.verifyElementTextContains("lbl_First_PLCC_Card_Name_Mobile", "Credit Card", checkoutPage),
							"PLCC card should be selected automatically when PLCC card approved",
							"PLCC card is selected automatically when PLCC card approved",
							"PLCC card is not selected automatically when PLCC card approved", driver);
				}

				Log.softAssertThat(checkoutPage.checkPLCCCardSelectedBasedOnBrand(Utils.getCurrentBrandShort())
						|| checkoutPage.checkPLCCCardSelectedBasedOnBrand(BrandUtils.getBrandFullName()),
						" The PLCC type that the user just signed up for will be selected in the Selected Credit Card Type field.",
						" The PLCC type that the user just signed up for selected in the Selected Credit Card Type field.",
						" The PLCC type that the user just signed up for will not be selected in the Selected Credit Card Type field.", driver);

				Log.softAssertThat(checkoutPage.elementLayer.verifyNumberMaskedWithSpecifiedDigits("fldTxtCardNo", 4, checkoutPage)
						&& ((checkoutPage.elementLayer.getElementText("fldTxtCardNo", checkoutPage).length()) == 9),
						"The masked number and last 4 digits of the PLCC card should be displayed in the \"Card Number\" field.",
						"The masked number and last 4 digits of the PLCC card is displayed in the \"Card Number\" field.",
						"The masked number and last 4 digits of the PLCC card is not displayed in the \"Card Number\" field.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("orderSummaryDiscount"), checkoutPage),
						"The $10 discount should not displayed in the Order Summary section.",
						"The $10 discount is not displayed in the Order Summary section.",
						"The $10 discount is displayed in the Order Summary section.", driver);

				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
				
				// Step 15 - Navigate to the Checkout page - step 3

				checkoutPage.continueToReivewOrder();
				Log.message(i++ + ". Navigated to review and place order screeen", driver);

				if (checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("plccCardNoError"), checkoutPage)) {
					Log.reference("PLCC card number is not valid, hence can't proceed further", driver);

				} else {
					HashSet<String> ProductId = checkoutPage.getOrderedPrdListNumber();
					String OrderSummaryTotal = checkoutPage.getOrderSummaryTotal();

					OrderConfirmationPage receipt = checkoutPage.clickOnPlaceOrderButton();
					Log.message(i++ + ". Order Placed successfully", driver);

					// Step 16 - Order confirmation page

					Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()),
							"Order number is generated automatically when order placed",
							"Order is placed and order number generated", "Order number is not generated", driver);

					Log.softAssertThat(receipt.checkEnteredShippingAddressReflectedInOrderReceipt(checkoutAdd),
							"Same shipping address should display in the receipt which is entered in checkout shipping detail",
							"Same shipping address is displaying", "Different shipping address is displaying", driver);

					Log.softAssertThat(receipt.checkEnteredBillingAddressReflectedInOrderReceipt(checkoutAdd),
							"Same billing address should display in the receipt which is entered in checkout billing detail",
							"Same billing address is displaying", "Different billing address is displaying", driver);

					Log.softAssertThat(receipt.elementLayer.verifyNumberMaskedWithSpecifiedDigits("lblEnteredCardNo", 4, receipt),
							"The last 4 digits of the PLCC card should be be displayed.",
							"The last 4 digits of the PLCC card is be displayed.",
							"The last 4 digits of the PLCC card is not displayed.", driver);

					Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblProfileEmail", userEMailId, receipt),
							"The mail ID entered should reflected in receipt",
							"The mail ID entered is reflected in receipt",
							"The mail ID entered is not reflected in receipt", driver);

					Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblOrderReceipt", userEMailId, receipt),
							"The order receipt should send to the entered mail ID",
							"The order receipt is send to the entered mail ID",
							"The order receipt is not send to the entered mail ID", driver);

					Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt),
							"The user can able to take print out of the order receipt",
							"The user can able to print the order receipt",
							"The user cannot able to print the order receipt", driver);

					HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();

					Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId),
							"The product added should be present in the receipt",
							"The product added is present in the receipt",
							"The product added is not present in the receipt", driver);

					String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();

					Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal),
							"The order total should display correctly",
							"The order total is displaying correctly",
							"The order total is not displaying correctly", driver);

					receipt.enterValueInPassword(validPassword);
					receipt.enterValueInConfirmPassword(validPassword);
					Log.message(i++ + ". Entered details in password and confirma password field", driver);

					receipt.createUserInOrderSummary(validPassword);
					Log.message(i++ + ". Clicked create account button", driver);

					// Step 17 - Navigate to My Account- Payment page. - skipped

					Log.reference("Order placed and account created with \"Always Approve\" address data, " + "So validation not applicable in My account and My address pages");
				}
			} else {
				Log.reference("PLCC Approval modal is not displayed hence can't proceed further", driver);
			}

			Log.testCaseResult();

		} // Ending try block
		catch (Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		} // Ending finally

	}// M1_FBB_UC_C26135

}// TC_FBB_UC_C26135
