package com.fbb.testscripts.uc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_UC_C26136 extends BaseTest {
    
    EnvironmentPropertiesReader environmentPropertiesReader;
    private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
    private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
    private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandWare");
    
    @Test(groups = { "uc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
    public void M1_FBB_UC_C26136(String browser) throws Exception {
        Log.testCaseInfo();
        final WebDriver driver = WebDriverFactory.get(browser);
        
        int i = 1;
        try {
            String userEmail = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
            String userPassword = accountData.get("password_global");
            String credential = userEmail+"|"+userPassword;
            
            String addressAlwaysApprove = "plcc_always_approve_address";
            String firstNameAlwaysApprove = checkoutData.get("plcc_always_approve_address").split("\\|")[7];
            String lastNameAlwaysApprove = checkoutData.get("plcc_always_approve_address").split("\\|")[8];

            String prdIdBadgeJLX = TestData.get("prd_badge", Brand.jlx).split("\\|")[0];
            String deferredCode = checkoutData.get("deferredPaymentCode");
            HashSet<String> productsIdToVerify = new HashSet<String>();
            HashSet<String> productsNameToVerify = new HashSet<String>();
            String giftCardFee = demandwareData.get("gift_Card_Fee");
            
            {
                GlobalNavigation.registerNewUserWithUserDetail(driver, 0, 0, firstNameAlwaysApprove, lastNameAlwaysApprove, credential);
            }
            
            HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
            Log.message(i++ +". Navigated to RMX Home Page!", driver);
            
            homePage.headers.chooseBrandFromHeader(Brand.jlx);
            Log.message(i++ +". Choosed JLX brand from header.", driver);
            
            Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage)
                    && UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.jlx),
                    "System should navigate to JLX brand home page.",
                    "System is navigated to JLX brand home page.",
                    "System is not navigated to JLX brand home page.", driver);
            
            SignIn signInPage = homePage.headers.navigateToSignInPage();
            Log.message(i++ +". Navigated to signin page.", driver);
            
            MyAccountPage myAccountPage = signInPage.navigateToMyAccount(userEmail, userPassword);
            Log.message(i++ +". Clicked on sign in button.", driver);
            
            Log.softAssertThat(myAccountPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), myAccountPage),
                    "System should be navigated to the My Account-Overview page.",
                    "System is navigated to the My Account-Overview page.",
                    "System is not navigated to the My Account-Overview page.", driver);
            
            homePage.headers.typeTextInSearchField(prdIdBadgeJLX);
            Log.message(i++ +". Typed "+ prdIdBadgeJLX +" in search field.", driver);
            
            Log.softAssertThat(homePage.headers.verifySearchSuggestions(),
                    "Search suggestion flyout should be displayed.",
                    "Search suggestion flyout is displayed.",
                    "Search suggestion flyout is not displayed.", driver);
            
            PdpPage pdpPage = homePage.headers.navigateToPDP(prdIdBadgeJLX);
            Log.message(i++ +". Navigated to PDP.", driver);
            
            productsIdToVerify.add(prdIdBadgeJLX);
            productsNameToVerify.add(pdpPage.getProductName().toLowerCase());
            
            Log.softAssertThat(pdpPage.verifyBadgeDisplayedTopLeftOfPrimaryImage(),
                    "The badge should be displayed on the main image.",
                    "The badge is displayed on the main image.",
                    "The badge is not displayed on the main image.", driver); 
            
            pdpPage.clickOnAlternateImages(1);
            Log.message(i++ +". Clicked on alternate image.", driver);
            
            pdpPage.selectColor();
            Log.message(i++ +". Selected color swatch.", driver);
            
            Log.softAssertThat(pdpPage.verifyDisplayImageMatchesSwatchColor(),
                    "The badge should be displayed on the main image.",
                    "The badge is displayed on the main image.",
                    "The badge is not displayed on the main image.", driver);
            
            Log.softAssertThat(pdpPage.verifyBadgeDisplayedTopLeftOfPrimaryImage(),
                    "The badge should be displayed on the colorised main image.",
                    "The badge is displayed on the colorised main image.",
                    "The badge is not displayed on the colorised main image.", driver); 
            
            pdpPage.selectSize();
            Log.message(i++ +". Selected size swatch.", driver);
            
            pdpPage.clickAddProductToBag();
            Log.message(i++ +". Clicked on Add to bag.", driver);
            
            ShoppingBagPage cartPage = pdpPage.clickOnCheckoutInMCOverlay();
            Log.message(i++ +". Clicked on Review my bag button from ATB overlay.", driver);
            
            //Verify Badge should not display on image.
            
            int qtyBeforeUpdate = cartPage.getQtyInCart();
            double subTotalBeforeUpdate = cartPage.getOrderSubTotal();
            double orderTotalBeforeUpdate = cartPage.getOrderTotal();
            
            cartPage.clickOnQuantityArrow("up");
            Log.message(i++ +". Increased the qty by clicking the upward arrow in quantity dropdown.", driver);
            
            int qtyAfterUpdate = cartPage.getQtyInCart();
            double subTotalAfterUpdate = cartPage.getOrderSubTotal();
            double orderTotalAfterUpdate = cartPage.getOrderTotal();
            
            Log.softAssertThat((qtyAfterUpdate == (qtyBeforeUpdate+1) && subTotalAfterUpdate > subTotalBeforeUpdate) 
                    && ( orderTotalAfterUpdate > orderTotalBeforeUpdate), 
                    "Cart quantity, subtotal and order total should update properly.",
                    "Cart quantity, subtotal and order total are updated properly.",
                    "Cart quantity, subtotal and order total are not updated properly.", driver);
            
            CheckoutPage checkoutPage= cartPage.clickOnCheckoutNowBtn();
            Log.message(i++ +". Clicked on checkout button.", driver);
            
            Log.softAssertThat(checkoutPage.verifyNavigatedToShippingSection(),
                    "The system should be navigated to shipping section.",
                    "The system is navigated to shipping section.",
                    "The system is not navigated to shipping section.", driver);
            
            checkoutPage.fillingShippingDetailsAsSignedInUser("no", "yes", ShippingMethod.Standard, addressAlwaysApprove);
            Log.message(i++ +".  Entered PLCC test data in the shipping address section.", driver);
            
            Log.softAssertThat(checkoutPage.verifyUseThisAsBillingAddressIsChecked(),
                    "The Checkbox of use this as a billing address should be in a selected state.",
                    "The Checkbox of use this as a billing address is in a selected state.",
                    "The Checkbox of use this as a billing address is not in a selected state.", driver);
            
            checkoutPage.continueToBilling(true);
            Log.message(i++ +". Clicked on continue button.", driver);
            
            if(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkoutPage)){
                
                Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlPLCCApproval"), checkoutPage), 
                        "PLCC checkout overlay should be displayed", 
                        "PLCC checkout overlay is getting displayed", 
                        "PLCC checkout overlay is not getting displayed", driver);

                Log.softAssertThat(checkoutPage.elementLayer.verifyTextContains("txtCongratulationsInPLCCModal", "congratulations", checkoutPage)
                        && checkoutPage.elementLayer.verifyTextContains("firstNameInPLCCModal", firstNameAlwaysApprove, checkoutPage), 
                        "Congratulations text and first name should be displayed.", 
                        "Congratulations text and first name are displayed.", 
                        "Congratulations text and first name are not displayed.", driver); 
                
                Log.softAssertThat(checkoutPage.verifyPLCCStep1ContentDisplayed(),
                        "The offer content should be displayed in the PLCC banner.", 
                        "The offer content should be displayed in the PLCC banner.", 
                        "The offer content should be displayed in the PLCC banner.", driver);
                        
                Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnGetItTodayInPLCC", "btnNoThanksInPLCC1"), checkoutPage),
                        "No thanks and Get it today buttons should be displayed.", 
                        "No thanks and Get it today buttons are displayed.", 
                        "No thanks and Get it today buttons are not displayed.", driver);
                
                checkoutPage.continueToPLCCStep2();
                Log.message(i++ +". Clicked on Get It Today button", driver);
                
                Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApprovalStep2"), checkoutPage),
                        "When user selects Get It Today, user will be taken to the Step 2 overlay.",
                        "When user selects Get It Today, user is taken to the Step 2 overlay.",
                        "When user selects Get It Today, user is not taken to the Step 2 overlay.", driver);
                
                Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtSSNinPLCCStep2", "divPLCCBirthMonthDrp", "divPLCCBirthYearDrp", "txtMobileNoInPLCCStep2"), checkoutPage),
                        "SSN, Date of birth fields, Phone number of fields should be displayed.",
                        "SSN, Date of birth fields, Phone number of fields are displayed.",
                        "SSN, Date of birth fields, Phone number of fields are not displayed.", driver);
                
                //The first five-digit of SSN number should be masked and the user not allowed to modify.
                
                checkoutPage.typeTextInSSN("1234");
                Log.message(i++ +". Entered SSN number");
                
                checkoutPage.selectDateMonthYearInPLCC2("01","05","1947");
                Log.message(i++ +". Selected date of birth");
                
                checkoutPage.typeTextInMobileInPLCC("8015841844");
                Log.message(i++ +". Entered phone number", driver);
                
                checkoutPage.clickonEditAddressInPLCC();
                Log.message(i++ +". Clicked on edit link in personal details", driver);
                
                checkoutPage.checkConsentInPLCC("YES");
                Log.message(i++ +". Selected consonent checbox", driver);
                
                checkoutPage.clickOnAcceptInPLCC();
                Log.message(i++ +". Clicked Yes, I Accept button", driver);
                
                if(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("approvedModal"), checkoutPage)) {
                                        
                    Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"), checkoutPage),
                            "The Congratulations modal will pop up.",
                            "The Congratulations modal is pop up.",
                            "The Congratulations modal is not pop up.", driver);
                
                    Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("lbl_First_PLCC_Card_holder_Name", "lbl_First_PLCC_Available_Credit", "btnContinueToCheckout"), checkoutPage)
                            && checkoutPage.elementLayer.verifyTextContains("lbl_First_PLCC_Card_holder_Name", firstNameAlwaysApprove, checkoutPage),
                            "Congratulations modal should consists of First name, Credit limit and Continue to shopping.",
                            "Congratulations modal consists of First name, Credit limit and Continue to shopping.",
                            "Congratulations modal not consists of First name, Credit limit and Continue to shopping.", driver);
                    
                    checkoutPage.dismissCongratulationModal();
                    Log.message(i++ +". Clicking the Continue to Checkout button", driver);                
                
                    Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPage)
                                && !checkoutPage.elementLayer.verifyPageElements(Arrays.asList("approvedModal"), checkoutPage), 
                            "The Approval modal should be closed and user will be taken to the payment method section of checkout.",
                            "The Approval modal is closed and user will be taken to the payment method section of checkout.",
                            "The Approval modal is not closed and user will be taken to the payment method section of checkout.", driver);
                    
                    Log.softAssertThat(checkoutPage.checkPLCCCardSelectedBasedOnBrand(Brand.fromConfiguration("jlx").toString()), 
                        "Jessica London PLCC card should be selected in the card field.", 
                        "Jessica London PLCC card should be selected in the card field.",
                        "Jessica London PLCC card should be selected in the card field.", driver);
                
                } else {
                    Log.reference("PLCC Approval modal is not displayed.", driver);
                }
            
            } else {
                Log.fail("PLCC Step-1 overlay is not displayed with ALWAYS APPROVE address data.");
            }
            
            cartPage = checkoutPage.clickEditItemSummary();
            Log.message(i++ +". Clicked on Edit link in items summary section .", driver); 
            
            Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("miniCartContent"), cartPage), 
                    "The page should be redirected to the Cart page.", 
                    "The page is redirected to the Cart page.",
                    "The page is not redirected to the Cart page.", driver);
            
            homePage.headers.chooseBrandFromHeader(Brand.rmx);
            Log.message(i++ +". Choosed RMX brand from header.", driver);
            
            Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage)
                    && UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.rmx),
                    "System should navigate to RMX brand home page.",
                    "System is navigated to RMX brand home page.",
                    "System is not navigated to RMX brand home page.", driver);
            
            pdpPage = homePage.footers.navigateToPhysicalGiftCard();
            Log.message(i++ +". Navigated to gift card PDP.", driver);
            
            pdpPage.selectGiftCardSize("25.00");
            Log.message(i++ +". Selected gift card amount.", driver);
            
            productsIdToVerify.add(pdpPage.getProductID());
            productsNameToVerify.add(pdpPage.getProductName().toLowerCase());
            
            String giftAmount = pdpPage.getSelectedGCAmount();
            pdpPage.clickAddProductToBag();
            Log.message(i++ +". Clicked on Add to bag.", driver);
            
            pdpPage.clickProductNameInMCOverlay();
            Log.message(i++ +". Clicked product name from ATB overlay.", driver);
            
            Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage),
                    "The page should navigate to respective product PDP.",
                    "The page is navigated to respective product PDP.",
                    "The page is not navigated to respective product PDP.", driver);
            
            Log.softAssertThat(giftAmount.equalsIgnoreCase(pdpPage.getSelectedGCAmount()),
                    "The selected amount should be pre-selected.",
                    "The selected amount is pre-selected.",
                    "The selected amount is not pre-selected.", driver);
            
            homePage.headers.chooseBrandFromHeader(Brand.elx);
            Log.message(i++ +". Choosed ELX brand from header.", driver);
            
            Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage)
                    && UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.elx),
                    "System should navigate to ELX brand home page.",
                    "System is navigated to ELX brand home page.",
                    "System is not navigated to ELX brand home page.", driver);
            
            cartPage = homePage.headers.navigateToShoppingBagPage();
            Log.message(i++ +". Navigated to cart page.", driver);
            
            HashSet<String> cartProductId = cartPage.getProductIdList();
            Log.softAssertThat(cartPage.elementLayer.compareTwoHashSet(productsIdToVerify, cartProductId),
                    "Previously added products should be displayed in cart page", 
                    "Previously added products are displayed in cart page", 
                    "Previously added products are not displayed in cart page", driver);
            
            cartPage.clickOnShippingCostTool();
            Log.message(i++ +". Clicked on shipping cost tooltip.", driver);
            
            Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("shippingCostOverlay"), cartPage), 
                    "The shipping & handling tooltip overlay should be displayed.", 
                    "The shipping & handling tooltip overlay is displayed.", 
                    "The shipping & handling tooltip overlay is not displayed.", driver);
            
            Log.softAssertThat(cartPage.elementLayer.verifyTextContains("giftCardFeeInTooltip", giftCardFee, cartPage), 
                    "Gift card shipping and handling fee should be $1.95", 
                    "Gift card shipping and handling fee is $1.95",
                    "Gift card shipping and handling fee is not $1.95", driver);
            
            checkoutPage = cartPage.clickOnCheckoutNowBtn();
            Log.message(i++ +". Clicked on checkout now.", driver);
            
            Log.softAssertThat(checkoutPage.verifyShippingAddressPrePopulate(), 
                    "The shipping address should be pre-populates in form fields.", 
                    "The shipping address is pre-populates in form fields.",
                    "The shipping address is not pre-populates in form fields.", driver);
            
            LinkedHashMap<String, String> shippingAddress= checkoutPage.getShippingInformation();
            List<String> shippingAddressValue = new ArrayList<String>(shippingAddress.values());
            
            checkoutPage.selectShippingMethodOnly(ShippingMethod.Express);
            Log.message(i++ +". Selected Express delivery option.", driver);
            
            Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("shippingmethodExceptionOverlay"), checkoutPage), 
                    "The shipping exception overlay should be displayed.", 
                    "The shipping exception overlay is displayed.", 
                    "The shipping exception overlay is not displayed.", driver);
            
            checkoutPage.clickShippingMethoCancelBtn();
            Log.message(i++ +". Clicked on cancel button from the Shipping exception overlay.", driver);
            
            Log.softAssertThat(checkoutPage.verifyDefaultDeliveryOptionChecked(), 
                    "The standard delivery should be selected by default.", 
                    "The standard delivery is selected by default.",
                    "The standard delivery is not selected by default.", driver);
            
            checkoutPage.continueToBilling();
            Log.message(i++ +". Clicked on continue button.", driver);
            
            checkoutPage.clickBillingAddressEdit();
            Log.message(i++ +". Clicked on Edit link of billing address.", driver);
            
            LinkedHashMap<String, String> billingAddress = checkoutPage.getBillingInformation();
            List<String> billingAddressValue = new ArrayList<String>(billingAddress.values());
            
            Log.softAssertThat(shippingAddressValue.containsAll(billingAddressValue),
                    "The system should keep the billing address is same as the shipping address.",
                    "The billing address is same as the shipping address.",
                    "The billing address is not same as the shipping address.", driver);
            
            checkoutPage.clickselectPaymentMethod();
            Log.message(i++ +". Clicked on select payment method.", driver);
            
            String plccCardName = checkoutPage.getDefaultPaymentMethod();
            Log.softAssertThat(plccCardName.contains("Jessica London"),
                    "Jessica London PLCC card should be selected by default in the select credit card field.",
                    "Jessica London PLCC card is selected by default in the select credit card field.",
                    "Jessica London PLCC card is not selected by default in the select credit card field.", driver);
            
            //9 digit card number should be displayed in the card number field.
                        
            checkoutPage.applyDeferredPaymentCode(deferredCode);
            Log.message(i++ +". Applied deferred payment code.", driver);
            
            checkoutPage.continueToReivewOrder();
            Log.message(i++ +". Clicked on continue button.", driver);
            
            Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("orderSummaryDiscount"), checkoutPage),
                    "The $10 discount should be displayed in the Order Summary section.", 
                    "The $10 discount is displayed in the Order Summary section.",
                    "The $10 discount is not displayed in the Order Summary section.", driver);
                        
            checkoutPage.clickOnShippingCostTool();
            Log.message(i++ +". Clicked on tooltip.", driver);
            
            Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shippingCostOverlay"), checkoutPage),
                    "The shipping cost tool-tip overlay should be displayed.", 
                    "The shipping cost tool-tip overlay should be displayed.",
                    "The shipping cost tool-tip overlay should be displayed.", driver);
            
            checkoutPage.clickOnShippingCostToolClose();
            Log.message(i++ +". Closed tooltip modal.", driver);
            
            cartProductId = checkoutPage.getCheckoutSummeryNameList();
            Log.softAssertThat(cartPage.elementLayer.compareTwoHashSet(productsNameToVerify, cartProductId),
                    "Previously added products should be displayed in checkout page", 
                    "Previously added products are displayed in checkout page", 
                    "Previously added products are not displayed in checkout page", driver);
            
            HashSet<String> checkoutProductItemNo = checkoutPage.getOrderedPrdListNumber();
            String OrderSummaryTotal = checkoutPage.getOrderSummaryTotal();
            
            if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
                Log.reference("Further verfication steps are not supported in current environment.");
                Log.testCaseResult();
                return;
            }
            
            OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
            Log.message(i++ + ". Order Placed successfully", driver);
            
            HashSet<String> ProductItemNoReceipt = receipt.getOrderedPrdListNumber();
            Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductItemNoReceipt, checkoutProductItemNo), 
                    "The product added should be present in the receipt", 
                    "The product added is present in the receipt", 
                    "The product added is not present in the receipt", driver);
            
            String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
            Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
                    "The order total should display correctly", 
                    "The order total is displaying correctly", 
                    "The order total is not displaying correctly", driver);
            
            myAccountPage = homePage.headers.navigateToMyAccount();
            Log.message(i++ + ". Navigated to my account page", driver);
            
            PaymentMethodsPage paymentPage = myAccountPage.navigateToPaymentMethods();
            Log.message(i++ + ". Navigated to payment method page", driver);
            
            String defaultCardType = paymentPage.getDefaultCardType();
            Log.softAssertThat(defaultCardType.toLowerCase().contains("jessica london") || defaultCardType.contains("jl_plcc"), 
                    "Jessica London PLCC card should be displayed as a default card.", 
                    "Jessica London PLCC card is displayed as a default card.", 
                    "Jessica London PLCC card is not displayed as a default card.", driver);
            
            Log.testCaseResult();
        } // Ending try block
        catch(Exception e) {
            Log.exception(e, driver);
        } // Ending catch block
        finally {
            Log.endTestCase(driver);
        } // Ending finally
    } // Ending C26136
}