package com.fbb.testscripts.uc;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.MiniCartPage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_UC_C26128 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	

	@Test(groups = { "uc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_UC_C26128(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		String userEMailId = AccountUtils.generateEmail(driver);
		String level2Cat = TestData.get("level_with_productID_color");
		String prdId = TestData.get("level_with_productID_color", Brand.rmx).split("\\|")[2];
		String prdIdClearance = TestData.get("prd_clearance", Brand.jlx);
		String prdIdPoBox= TestData.get("prd_po-box-restricted", Brand.elx);
		String checkoutAdd = "taxless_address";
		String payment = "card_Visa";
		
		HashSet<String> productsAdded = new HashSet<String>();
		
		int i=1;
		try
		{
			//step 1 - Navigate to the universal cart website as a Roamans brand
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage),
					"Home page should be displayed",
					"Home page is getting displayed",
					"Home page is not getting displayed", driver);
			
			Headers header = homePage.headers;
			
			Log.softAssertThat(homePage.headers.verifyHoverMessageAllBrands(), 
					"System should display the 'Hover Message' for all the brands",
					"System is displayed the 'Hover Message' for all the brands",
					"System is not displayed the 'Hover Message' for all the brands", driver);

			// step 2 - Confirm the system displaying the utility bar

			Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayed(Arrays.asList("brandLogoRM", "brandLogoJL", "brandLogoEL"), homePage.headers),
					"System should display the 'Brand logo' on the utility bar",
					"System is displayed the 'Brand logo' on the utility bar",
					"System is not displayed the 'Brand logo' on the utility bar", driver);

			if (!Utils.isMobile()) {
				Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayed(Arrays.asList("iconMyBagWoQty", "lnkAccountButton"), homePage.headers),
						"System should display the 'Mini cart','Sign in link' on the utility bar",
						"System is displayed the 'Mini cart','Sign in link' on the utility bar",
						"System is not displayed the 'Mini cart','Sign in link' on the utility bar", driver);
			}
			 
			if(Utils.isDesktop()) {
				
				Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayed(Arrays.asList("custService"), homePage.headers),
						"System should display the 'Customer service link' on the utility bar",
						"System is displayed the 'Customer service link' on the utility bar",
						"System is not displayed the 'Customer service link' on the utility bar",driver);
				
				Log.softAssertThat(homePage.headers.verifyHoverMessageAllBrands(), 
						"System should display the 'Hover Message' for all the brands",
						"System is displayed the 'Hover Message' for all the brands",
						"System is not displayed the 'Hover Message' for all the brands", driver);
			}
			
			Log.softAssertThat(homePage.headers.verifyCurrentBrandNotClickable(), 
					"Current brand favicon logo should not be clickable", 
					"Current brand favicon logo is not be clickable", 
					"Current brand favicon logo is clickable", driver);
			
			//Step 3 - Navigate to Product listing page
			
			PlpPage plpPage = homePage.headers.navigateTo(level2Cat.split("\\|")[0], level2Cat.split("\\|")[1]);
			Log.message(i++ + ". Navigated to PLP", driver);
			
			Log.softAssertThat(plpPage.verifyCategoryHeader(level2Cat.split("\\|")[1]), 
					"User should be navigated to correct category page.", 
					"Navigated to correct category.", 
					"User not navigated to correct category.", driver);
			
			ShoppingBagPage shoppingBagPg = null;
			
			if(Utils.isDesktop()) {
				QuickShop quickShop = plpPage.clickOnQuickShop(prdId);
				Log.message(i++ + ". Quick shop window opened", driver);
				
				quickShop.selectColor();
				Log.message(i++ + ". Selected color", driver);
				
				quickShop.selectAllSwatches();
				Log.message(i++ + ". Selected size", driver);
				
				quickShop.addToBag();
				Log.message(i++ + ". Product added to bag", driver);
				
				productsAdded.add(prdId);
				
				shoppingBagPg = quickShop.clickOnCheckoutInMCOverlay();
				Log.message(i++ + ". Navigated to Shopping bag page", driver);
			} else {
				PdpPage pdpPage = plpPage.navigateToPdp(prdId);
				Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
				
				pdpPage.selectColor();
				Log.message(i++ + ". Selected color", driver);
				
				pdpPage.selectAllSwatches();
				Log.message(i++ + ". Selected size", driver);
				
				pdpPage.addToBagKeepOverlay();
				Log.message(i++ + ". Product added to bag", driver);
				
				productsAdded.add(prdId);
				
				shoppingBagPg = pdpPage.clickOnCheckoutInMCOverlay();
				Log.message(i++ + ". Navigated to Shopping bag page", driver);
			}
			
			//Step 4 - Navigate to cart page
			HashSet<String> productIdList = new HashSet<String>();
			productIdList = shoppingBagPg.getProductIdList();
			
			Log.softAssertThat(shoppingBagPg.elementLayer.compareTwoHashSet(productsAdded, productIdList), 
					"Selected product from PDP should be displayed on the cart page.", 
					"Selected product from PDP is displayed on the cart page.", 
					"Selected product from PDP is not displayed on the cart page.", driver);
			
			Log.softAssertThat(shoppingBagPg.verifyProductQuantity(1, shoppingBagPg.getProductIndexByPrdID(prdId)),
					"Selected product quantity from PDP should be displayed on the cart page.", 
					"Selected product quantity from PDP is displayed on the cart page.", 
					"Selected product quantity from PDP is not displayed on the cart page.", driver);
			
			String editLink;
			if(Utils.isDesktop()) {
				editLink = "lnkEdit";
			} else {
				editLink = "lnkEdit_Tablet_Mobile"; 
			}
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyHorizontalAllignmentOfElements(driver, "cartProductOptions", "cartProductAttribute", shoppingBagPg) &&
						shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList(editLink, "lnkAddToWishlist", "removeProduct"), shoppingBagPg),
						"Edit, Add to Wishlist & Remove link should be displayed on the right side of the added standard product.", 
						"Edit, Add to Wishlist & Remove link is displayed on the right side of the added standard product.", 
						"Edit, Add to Wishlist & Remove link is not displayed on the right side of the added standard product.",  driver);
			} else {
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "cartProductAttribute", "cartProductOptions", shoppingBagPg) &&
						shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList(editLink, "lnkAddToWishlist", "removeProduct"), shoppingBagPg),
						"Edit, Add to Wishlist & Remove link should be displayed on the below of the added standard product.", 
						"Edit, Add to Wishlist & Remove link is displayed on the below of the added standard product.", 
						"Edit, Add to Wishlist & Remove link is not displayed on the below of the added standard product.",  driver);
			}
			
			Double subTotalBefore = shoppingBagPg.getOrderSubTotal();
			
			//Step 5 - Click on Jessica-London favicon from the Header section on the universal cart website.
			
			header.chooseBrandFromHeader(Brand.jlx);
			
			Log.softAssertThat(homePage.checkPrimaryLogoBrandName(Brand.jlx) &&
					homePage.headers.getActiveSharedCartBrandName().equals(Brand.jlx),
					"Home page should be displayed for JL brand site",
					"Home page is displayed for JL brand site",
					"Home page is not displayed for JL brand site", driver);
			
			Log.softAssertThat(homePage.headers.verifyActiveScBrandLocation(Brand.jlx),
					"JL logo should move & display as 1st brand logo in the utility bar.",
					"JL logo is moved & displayed as 1st brand logo in the utility bar.",
					"JL logo is not moved & displayed as 1st brand logo in the utility bar.", driver);
			
			//Step 6 - Place the cursor on the search field below the header
			
			Log.softAssertThat(homePage.headers.verifyPrimarySearchCursor(),
					"The cursor should be displayed in the search box.",
					"The cursor is displayed in the search box.",
					"The cursor is not displayed in the search box.", driver);
			
			//Step 7 - Navigate to the Product detail page
			//Step 8 - Navigate to the cart page.
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdIdClearance);
			Log.message(i++ + ". Navigated to PDP page", driver);
			
			pdpPage.addToBagKeepOverlay();
			Log.message(i++ + ". Product added to bag", driver);
			
			productsAdded.add(prdIdClearance);
			
			shoppingBagPg = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Clicked \"Review my bag\" from added to bag overlay", driver);

			productIdList = shoppingBagPg.getProductIdList();
			
			HashSet<String> productNameListCart = new HashSet<String>();
			productNameListCart = shoppingBagPg.getCartItemNameList();
			
			Log.softAssertThat(shoppingBagPg.elementLayer.compareTwoHashSet(productsAdded, productIdList), 
					"Selected product from PDP should be displayed on the cart page.", 
					"Selected product from PDP is displayed on the cart page.", 
					"Selected product from PDP is not displayed on the cart page.", driver);
			
			Double subTotalAfter = shoppingBagPg.getOrderSubTotal();
			
			String productCount = Integer.toString(shoppingBagPg.getProductCountInCart());
			
			if(!Utils.isMobile()) {
				MiniCartPage miniCart = homePage.headers.mouseOverMiniCart();
				String productCountMini = homePage.headers.getMiniCartCount();
				
				Log.softAssertThat(productCountMini.equalsIgnoreCase(productCount), 
						"The product count should be updated on My bag icon", 
						"The product count is updated on My bag icon", 
						"The product count is not updated on My bag icon",  driver);
				
				HashSet<String> productNameListMini = new HashSet<String>();
				productNameListMini = miniCart.getCartItemNameList();
				
				Log.softAssertThat(shoppingBagPg.elementLayer.compareTwoHashSet(productNameListMini, productNameListCart), 
						"Selected product from Brands should be displayed on the Mini-cart.", 
						"Selected product from Brands London is displayed on the Mini-cart.", 
						"Selected product from Brands London is not displayed on the Mini-cart.", driver);
			}
			
			Log.softAssertThat(subTotalAfter > subTotalBefore, 
					"The subtotal should be updated in order Summary.", 
					"The subtotal is updated in order Summary.",
					"The subtotal is not updated in order Summary.", driver);
			
			//Step 9 - Click on Ello's favicon from the Header section on the universal cart website.
			
			header.chooseBrandFromHeader(Brand.elx);
			
			Log.softAssertThat(homePage.checkPrimaryLogoBrandName(Brand.elx) &&
					homePage.headers.getActiveSharedCartBrandName().compareTo(Brand.elx) == 0,
					"Home page should be displayed for EL brand site",
					"Home page is displayed for EL brand site",
					"Home page is not displayed for EL brand site", driver);
			
			Log.softAssertThat(homePage.headers.verifyActiveScBrandLocation(Brand.elx),
					"EL logo should move & display as 1st brand logo in the utility bar.",
					"EL logo is moved & displayed as 1st brand logo in the utility bar.",
					"EL logo is not moved & displayed as 1st brand logo in the utility bar.", driver);
			
			//Step 10 - Navigate to Product listing page
			
			plpPage = homePage.headers.navigateToPLP(prdIdPoBox);
			Log.message(i++ + ". Navigated to PLP page", driver);
			
			pdpPage = plpPage.navigateToPdp(prdIdPoBox);
			Log.message(i++ + ". Product to PDP page", driver);
			
			pdpPage.addToBagKeepOverlay();
			Log.message(i++ + ". Product added to bag", driver);
			
			productsAdded.add(prdIdPoBox);
			
			//Step 11 - Navigate to the cart page.
			
			shoppingBagPg = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Navigated to cart page", driver);
			
			productIdList = shoppingBagPg.getProductIdList();
			productNameListCart = shoppingBagPg.getCartItemNameList();
			
			Log.softAssertThat(shoppingBagPg.elementLayer.compareTwoHashSet(productIdList, productsAdded), 
					"Selected product from PDP should be displayed on the cart page.", 
					"Selected product from PDP is displayed on the cart page.", 
					"Selected product from PDP is not displayed on the cart page.", driver);
			
			Double subTotalAfter1 = shoppingBagPg.getOrderSubTotal();
			
			Log.softAssertThat(subTotalAfter1 > subTotalAfter, 
					"The subtotal should be updated in order Summary.", 
					"The subtotal is updated in order Summary.",
					"The subtotal is not updated in order Summary.", driver);
			
			//Step 12 - Click on the Roamans favicon from the header section on the universal cart website.
			
			header.chooseBrandFromHeader(Brand.rmx);
			
			Log.softAssertThat(homePage.checkPrimaryLogoBrandName(Brand.rmx) &&
					homePage.headers.getActiveSharedCartBrandName().compareTo(Brand.rmx) == 0,
					"Home page should be displayed for RM brand site",
					"Home page is displayed for RM brand site",
					"Home page is not displayed for RM brand site", driver);
			
			Log.softAssertThat(homePage.headers.verifyActiveScBrandLocation(Brand.rmx),
					"RM logo should move & display as 1st brand logo in the utility bar.",
					"RM logo is moveed & displayed as 1st brand logo in the utility bar.",
					"RM logo is not moved & displayed as 1st brand logo in the utility bar.", driver);
			
			//Step 13 - For Desktop & tablet: Hover on My bag icon
			//Step 14 - Navigate to the cart page

			if(!Utils.isMobile()) {
				MiniCartPage miniCart = homePage.headers.mouseOverMiniCart();
				Log.message(i++ + ". Mini-cart flyout overlay opened", driver);
				
				HashSet<String> productNameListMini = new HashSet<String>();
				productNameListMini = miniCart.getCartItemNameList();
				
				Log.softAssertThat(shoppingBagPg.elementLayer.compareTwoHashSet(productNameListCart, productNameListMini), 
						"The product count should be updated on My bag icon", 
						"The product count is updated on My bag icon", 
						"The product count is not updated on My bag icon",  driver);
				
				Double subTotalCurrent = miniCart.getSubTotal();
				
				Log.softAssertThat(subTotalCurrent.equals(subTotalAfter1), 
						"The subtotal should be updated in order Summary.", 
						"The subtotal is updated in order Summary.",
						"The subtotal is not updated in order Summary.", driver);
				
				shoppingBagPg = miniCart.clickOnCheckOut();	
				Log.message(i++ + ". Clicked \"review my bag\" from mini-cart flyout", driver);
			}
			
			Log.softAssertThat(shoppingBagPg.elementLayer.compareTwoHashSet(productIdList, productsAdded), 
					"Selected product from PDP should be displayed on the cart page.", 
					"Selected product from PDP is displayed on the cart page.", 
					"Selected product from PDP is not displayed on the cart page.", driver);
			
			Double subTotalCurrent = shoppingBagPg.getOrderSubTotal();
			
			Log.softAssertThat(subTotalCurrent.equals(subTotalAfter1), 
					"The subtotal should be updated in order Summary.", 
					"The subtotal is updated in order Summary.",
					"The subtotal is not updated in order Summary.", driver);
			
			//Step 15 - Navigate to the checkout sign-in page
			
			SignIn signin = (SignIn) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			//Step 6 - Navigate to the checkout sign in page
			CheckoutPage checkoutPg = (CheckoutPage) signin.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("errUsername", "errPassword"), checkoutPg), 
					"Username and Password fields are mandatory, so it cannot be left blank", 
					"Error thrown when username/password fields are left blank", 
					"Error is not thrown when username/password fields are left blank", driver);
			
			checkoutPg.continueToShipping(userEMailId);
			Log.message(i++ + ". Navigated to Shipping section", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("lblGuestUserEmail"), checkoutPg), 
					"The system should accept the valid email address", 
					"The system accepts the valid email address", 
					"The system not accepts the valid email address", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("billingEditFirstname"), checkoutPg), 
					"The system should navigate the user to checkout page - Step 1", 
					"The system is navigate the user to checkout page - Step 1", 
					"The system is not navigate the user to checkout page - Step 1", driver);
			
			//Step 16 - Navigate to the Checkout page - step 1
			//Step 17 - Navigate to the Checkout page - step 2

			checkoutPg.fillingShippingDetailsAsGuest("NO", checkoutAdd, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			Log.softAssertThat(checkoutPg.compareBillingAddressWithEnteredShippingAddress(checkoutAdd), 
					"The system should keep the billing address same as shipping address", 
					"Billing address same as shipping address", 
					"Billing address not same as shipping address", driver);
			
			checkoutPg.fillingCardDetails1(payment, false, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			//Step 18 - Navigate to the Checkout page - step 3
			HashSet<String> productNameList = checkoutPg.getCheckoutSummeryNameList();
			
			Log.softAssertThat(checkoutPg.elementLayer.compareTwoHashSet(productNameList, productNameListCart), 
					"The product added to cart should be updated in the checkout page", 
					"The product added to cart is updated in the checkout page", 
					"The product added to cart is not updated in the checkout page", driver);
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			//step 19 - Navigate to Checkout page - step 3
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			OrderConfirmationPage receipt = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 20 - Order confirmation page
			Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			Log.softAssertThat(receipt.checkEnteredShippingAddressReflectedInOrderReceipt(checkoutAdd), 
					"Same shipping address should display in the receipt which is entered in checkout shipping detail", 
					"Same shipping address is displaying", 
					"Different shipping address is displaying", driver);
			
			Log.softAssertThat(receipt.checkEnteredBillingAddressReflectedInOrderReceipt(checkoutAdd), 
					"Same billing address should display in the receipt which is entered in checkout billing detail", 
					"Same billing address is displaying", 
					"Different billing address is displaying", driver);
			
			Log.softAssertThat(receipt.comparePaymentAfterOrderWithEnteredPaymentMethod(payment), 
					"Same payment method should display in the receipt which is entered in checkout payment section", 
					"Same payment method is displaying", 
					"Different payment method is displaying", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblProfileEmail", userEMailId, receipt), 
					"The mail ID entered should reflected in receipt", 
					"The mail ID entered is reflected in receipt", 
					"The mail ID entered is not reflected in receipt", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblOrderReceipt", userEMailId, receipt), 
					"The order receipt should send to the entered mail ID", 
					"The order receipt is send to the entered mail ID", 
					"The order receipt is not send to the entered mail ID", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt), 
					"The user can able to take print out of the order receipt", 
					"The user can able to print the order receipt", 
					"The user cannot able to print the order receipt", driver);
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}//M1_FBB_UC_C26128

}//TC_FBB_UC_C26128
