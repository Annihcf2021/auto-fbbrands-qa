package com.fbb.testscripts.uc;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.MiniCartPage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.footers.Curalate;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_UC_C26132 extends BaseTest {
    
    EnvironmentPropertiesReader environmentPropertiesReader;
    private static EnvironmentPropertiesReader demandData = EnvironmentPropertiesReader.getInstance("demandware");
    
    @Test(groups = { "uc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
    public void M1_FBB_UC_C26132(String browser) throws Exception {
        Log.testCaseInfo();
        final WebDriver driver = WebDriverFactory.get(browser);
        
        String userEmail = AccountUtils.generateEmail(driver);
        String recepientAddress = "test@yopmail.com";
        String checkoutShippingAdd = "valid_address1";
        String checkoutBillingAdd = "taxless_address";
        String mandatoryFieldsError = demandData.get("missingMandatoryFieldsError");
        
        String regularPrdRMX = TestData.get("prd_variation1", Brand.rmx);
        String plpLimitedStockJLX = TestData.get("sku_stock<5_variation_plp", Brand.jlx);
        String prdIdLimitedStockJLX = TestData.get("sku_stock<5_variation", Brand.jlx).split("\\|")[0];
        String swatchColorLimitedStockJLX = TestData.get("sku_stock<5_variation", Brand.jlx).split("\\|")[1];
        String swatchSizeLimitedStockJLX = TestData.get("sku_stock<5_variation", Brand.jlx).split("\\|")[2];
        HashSet<String> productsIdToVerify = new HashSet<String>();
        HashSet<String> productsNameToVerify = new HashSet<String>();
        String rewardCert = TestData.get(Utils.getRewardCertificate(), Brand.jlx);
        
        int i = 1;
        try {
            
            HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
            Log.message(i++ +". Navigated to RMX Home Page!", driver);
            
            Curalate curalate = homePage.scrollToCuralate();
            Log.message(i++ +". Scrolled down to curalate section.", driver);
            
            curalate.clickOnPictureByIndex(1);
            Log.message(i++ +". Clicked on product title in curalate section.", driver);
            
            Log.softAssertThat(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("divSocialIcon"), curalate),
                    "Social icons should be displayed at the bottom of the pop-up window.",
                    "Social icons are displayed at the bottom of the pop-up window.",
                    "Social icons are not displayed at the bottom of the pop-up window.", driver);
            
            //Skipped clickOnPrdImageBelowGetThisLookByIndex() - When clicking product image from curalte section, it redirected to PROD site and landed on respective PDP.
            /*
            * PdpPage pdpPage = curalte.clickOnPrdImageBelowGetThisLookByIndex(1);
            * Log.message(i++ +". Clicked on product image below Get this look heading in curalate section.", driver);
            */
            
            curalate.closeCustomerEnlargeModal();
            Log.message(i++ +". Closed curalate enlarge modal.", driver);
            
            PdpPage pdpPage = homePage.headers.navigateToPDP(regularPrdRMX);
            Log.message(i++ +". Navigated to regular product PDP.", driver);
            
            productsIdToVerify.add(pdpPage.getProductID());
            productsNameToVerify.add(pdpPage.getProductName().toLowerCase());
            
            pdpPage.clickOnReviewTab();
            Log.message(i++ +". Clicked on Review tab.", driver);
            
            Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("sectionReviewSummary"), pdpPage),
                    "Added reviews should be displayed in Reviews section.",
                    "Added reviews are displayed in Reviews section.",
                    "Added reviews are not displayed in Reviews section.", driver);
            
            pdpPage.selectAllSwatches();
            Log.message(i++ +". Selected the color & size variations.", driver);
            
            double curalateProductPrice = StringUtils.getPriceFromString(pdpPage.getProductPrice());
            pdpPage.clickAddProductToBag();
            Log.message(i++ +". Clicked on Add to bag button.", driver);
            
            ShoppingBagPage cartPage = pdpPage.clickOnCheckoutInMCOverlay();
            Log.message(i++ +". Clicked on Review My Bag button from ATB overlay.", driver);
            
            HashSet<String> cartProductId = cartPage.getProductIdList();
            Log.softAssertThat(cartPage.elementLayer.compareTwoHashSet(productsIdToVerify, cartProductId),
                    "Previously added products should be displayed in cart page.",
                    "Previously added products are displayed in cart page.",
                    "Previously added products are not displayed in cart page.", driver);
            
            Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("recentlyViewedSection", "curalateModule"), cartPage),
                    "The recently viewed section and curlate section should be displayed.",
                    "The recently viewed section and curlate section is displayed.",
                    "The recently viewed section and curlate section is not displayed.", driver);
            
            MiniCartPage miniCart;
            if(!Utils.isMobile()) {
                miniCart = homePage.headers.mouseOverMiniCart();
                Log.message(i++ +". Mouse hover on my bag icon.", driver);
                
                Log.softAssertThat(miniCart.verifyProductDisplayedInOverlay(productsNameToVerify),
                        "Previously added products should be displayed in mini cart flyout.",
                        "Previously added products are displayed in mini cart flyout.",
                        "Previously added products are not displayed in mini cart flyout.", driver);
                
                Log.softAssertThat(miniCart.elementLayer.verifyElementDisplayed(Arrays.asList("qtyMiniCartBagIndicator"), miniCart),
                        "Product count should be displayed over mini cart.",
                        "Product count is displayed over mini cart.",
                        "Product count is not displayed over mini cart.", driver);
            }
            
            DecimalFormat deciFormat = new DecimalFormat("#.##");
            String cartSubTotal = Double.toString(cartPage.getOrderSubTotal());
            Log.softAssertThat(cartSubTotal.equals(deciFormat.format(curalateProductPrice)),
                    "Confirm the subtotal is updated in order Summary.",
                    "The subtotal is updated in order Summary.",
                    "The subtotal is not updated in order Summary.", driver);
            
            homePage.headers.chooseBrandFromHeader(Brand.elx);
            Log.message(i++ +". Choosed ELX brand from header.", driver);
            
            Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage)
                    && UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.elx),
                    "System should navigate to ELX brand home page.",
                    "System is navigated to ELX brand home page.",
                    "System is not navigated to ELX brand home page.", driver);
            
            pdpPage = homePage.footers.navigateToEGiftCardPDP();
            Log.message(i++ +". Navigated to E-Gift card PDP.", driver);
            
            Log.softAssertThat(UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.elx),
                    "The system should redirect to the Ellos site.",
                    "The system is redirected to the Ellos site.",
                    "The system is not redirected to the Ellos site.", driver);
            
            String eGiftName = pdpPage.getProductName();
            productsIdToVerify.add(pdpPage.getProductID());
            productsNameToVerify.add(eGiftName.toLowerCase());
            
            pdpPage.selectGCSize("25.00");
            Log.message(i++ +". Selected amount from the dropbox.", driver);
            
            double giftAmount = StringUtils.getPriceFromString(pdpPage.getSelectedGCAmount());
            pdpPage.enterRecepientEmailAdd(recepientAddress);
            Log.message(i++ +". Entered recipient email address.", driver);
            
            pdpPage.enterRecepientEmailConfirmAdd(recepientAddress);
            Log.message(i++ +". Entered confirm recipient email address.", driver);
            
            pdpPage.clickAddProductToBag();
            Log.message(i++ +". Clicked on Add to bag button.", driver);
            
            cartPage = pdpPage.clickOnCheckoutInMCOverlay();
            Log.message(i++ +". Navigated to checkout page.", driver);
            
            cartProductId = cartPage.getProductIdList();
            Log.softAssertThat(cartPage.elementLayer.compareTwoHashSet(productsIdToVerify, cartProductId),
                    "Previously added products should be displayed in cart page.",
                    "Previously added products are displayed in cart page.",
                    "Previously added products are not displayed in cart page.", driver);
            
            Log.softAssertThat(cartPage.verifyAddToWishlistNotDisplayedForEGift(eGiftName),
                    "Add to Wishlist link should not be displayed on the added E-gift card.",
                    "Add to Wishlist link is not displayed on the added E-gift card.",
                    "Add to Wishlist link is displayed on the added E-gift card.", driver);
            
            if(!Utils.isMobile()) {
                miniCart = homePage.headers.mouseOverMiniCart();
                Log.message(i++ +". Mouse hover on my bag icon.", driver);
                
                Log.softAssertThat(miniCart.verifyProductDisplayedInOverlay(productsNameToVerify),
                        "Previously added products should be displayed in mini cart flyout.",
                        "Previously added products are displayed in mini cart flyout.",
                        "Previously added products are not displayed in mini cart flyout.", driver);
                
                Log.softAssertThat(miniCart.elementLayer.verifyElementDisplayed(Arrays.asList("qtyMiniCartBagIndicator"), miniCart),
                        "Product count should be displayed over mini cart.",
                        "Product count is displayed over mini cart.",
                        "Product count is not displayed over mini cart.", driver);
            }
            
            cartSubTotal = Double.toString(cartPage.getOrderSubTotal());
            Log.softAssertThat(cartSubTotal.equals(deciFormat.format(curalateProductPrice + giftAmount)),
                    "Confirm the subtotal is updated in order Summary.",
                    "The subtotal is updated in order Summary.",
                    "The subtotal is not updated in order Summary.", driver);
            
            homePage.headers.chooseBrandFromHeader(Brand.jlx);
            Log.message(i++ +". Choosed JLX brand from header.", driver);
            
            Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage)
                    && UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.jlx),
                    "System should navigate to JLX brand home page.",
                    "System is navigated to JLX brand home page.",
                    "System is not navigated to JLX brand home page.", driver);
            
            PlpPage plpPage = homePage.headers.navigateTo(plpLimitedStockJLX);
            Log.message(i++ +". Navigated to category PLP having a limited inventory item.", driver);
            
            plpPage.selectProductColorInPLP(prdIdLimitedStockJLX, swatchColorLimitedStockJLX);
            Log.message(i++ +". Clicked on color swatch below the product image.", driver);
            
            pdpPage = plpPage.navigateToPdp(prdIdLimitedStockJLX);
            Log.message(i++ +". Navigated to limited inventory product PDP.", driver);
            
            productsIdToVerify.add(prdIdLimitedStockJLX);
            productsNameToVerify.add(pdpPage.getProductName().toLowerCase());
            
            pdpPage.selectSize(swatchSizeLimitedStockJLX);
            Log.message(i++ +". Selected size swatch.", driver);
            
            Log.softAssertThat(pdpPage.getQuantityListSize() == pdpPage.getStockValuefrmInventoryMsg(),
                    "Limited qty should be displayed in the dropbox ex: (1 to 5)",
                    "Limited qty is displayed in the dropbox.",
                    "Limited qty is not displayed in the dropbox.", driver);
            
            double limitedInventoryProductPrice = StringUtils.getPriceFromString(pdpPage.getProductPrice());
            cartPage = pdpPage.addToBagAndNavigate();
            Log.message(i++ +". Clicked on Add to bag button.", driver);
            
            cartProductId = cartPage.getProductIdList();
            Log.softAssertThat(cartPage.elementLayer.compareTwoHashSet(productsIdToVerify, cartProductId),
                    "Previously added products should be displayed in cart page",
                    "Previously added products are displayed in cart page",
                    "Previously added products are not displayed in cart page", driver);
            
            if(!Utils.isMobile()) {
                miniCart = homePage.headers.mouseOverMiniCart();
                Log.message(i++ +". Mouse hover on my bag icon.", driver);
                
                Log.softAssertThat(miniCart.verifyProductDisplayedInOverlay(productsNameToVerify),
                        "Previously added products should be displayed in mini cart flyout.",
                        "Previously added products are displayed in mini cart flyout.",
                        "Previously added products are not displayed in mini cart flyout.", driver);
                
                Log.softAssertThat(miniCart.elementLayer.verifyElementDisplayed(Arrays.asList("qtyMiniCartBagIndicator"), miniCart),
                        "Product count should be displayed over mini cart ",
                        "Product count is displayed over mini cart ",
                        "Product count is not displayed over mini cart ", driver);
            }
            
            cartSubTotal = Double.toString(cartPage.getOrderSubTotal());
            Log.softAssertThat(cartSubTotal.equals(deciFormat.format(curalateProductPrice + giftAmount + limitedInventoryProductPrice)),
                    "Confirm the subtotal is updated in order Summary.",
                    "The subtotal is updated in order Summary.",
                    "The subtotal is not updated in order Summary.", driver);
            
            CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
            Log.message(i++ +". Navigated to checkout page.", driver);
            
            checkoutPage.enterGuestUserEmail(userEmail);
            Log.message(i++ +". Entered guest email Id "+ userEmail, driver);
            
            checkoutPage.clickOnContinueInGuestLogin();
            Log.message(i++ +". Clicked on continue in guest checkout login.", driver);
            
            Log.softAssertThat(checkoutPage.verifyNavigatedToShippingSection(),
                    "The page should navigate to checkout page -Step 1.",
                    "The page is navigated to checkout page -Step 1.",
                    "The page is not navigated to checkout page -Step 1.", driver);
            
            checkoutPage.fillingShippingDetailsAsGuest(checkoutShippingAdd, ShippingMethod.Express);
            Log.message(i++ +". Filled taxable shiiping address with Express shipping method.", driver);
            
            checkoutPage.checkUncheckUseThisAsBillingAddress(false);
            Log.message(i++ +". Unchecked Use this as billing address checkbox.", driver);
            
            checkoutPage.clickShippingContinue();
            Log.message(i++ +". Clicked on continue button.", driver);
            
            checkoutPage.clickAvsContinueButton();
            Log.message(i++ +". Clicked on continue button from USPS address validation popup.", driver);
            
            checkoutPage.fillingBillingDetailsAsGuest(checkoutBillingAdd);
            Log.message(i++ +". Filled billing address.", driver);
            
            checkoutPage.continueToPaymentFromBilling();
            Log.message(i++ +". Clicked on payment method button.", driver);
            
            checkoutPage.continueToReivewOrder();
            Log.message(i++ +". Clicked on continue button without entering mandatory fields.", driver);
            
            String missingMandatoryFieldsError = checkoutPage.elementLayer.getElementText("errMissingFieldAboveSelectPayment", checkoutPage);
            Log.softAssertThat(mandatoryFieldsError.equalsIgnoreCase(missingMandatoryFieldsError),
                    "'Please complete all fields to continue' message should be shown.",
                    "'Please complete all fields to continue' message is shown.",
                    "'Please complete all fields to continue' message is not shown.", driver);
            
            checkoutPage.applyRewardCertificate(rewardCert);
            Log.message(i++ +". Applied a reward certificate.", driver);
            
            checkoutPage.applyGiftCardByValue(TestGiftCardValue.$20);
            Log.message(i++ +". Applied a first Gift card.", driver);
            
            checkoutPage.applyGiftCardByValue(TestGiftCardValue.$500);
            Log.message(i++ +". Applied a second reward certificate.", driver);
            
            checkoutPage.continueToReivewOrder();
            Log.message(i++ +". Clicked on continue button.", driver);
            
            cartProductId = checkoutPage.getCheckoutSummeryNameList();
            Log.softAssertThat(cartPage.elementLayer.compareTwoHashSet(productsNameToVerify, cartProductId),
                    "Previously added products should be displayed in checkout page.",
                    "Previously added products are displayed in checkout page.",
                    "Previously added products are not displayed in checkout page.", driver);
            
            HashSet<String> checkoutProductItemNo = checkoutPage.getOrderedPrdListNumber();
            String OrderSummaryTotal = checkoutPage.getOrderSummaryTotal();
            if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
                Log.reference("Further verfication steps are not supported in current environment.");
                Log.testCaseResult();
                return;
            }
            
            OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
            Log.message(i++ + ". Order Placed successfully.", driver);
            
            HashSet<String> ProductItemNoReceipt = receipt.getOrderedPrdListNumber();
            Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductItemNoReceipt, checkoutProductItemNo),
                    "The product added should be present in the receipt.",
                    "The product added is present in the receipt.",
                    "The product added is not present in the receipt.", driver);
            
            String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
            Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal),
                    "The order total should display correctly.",
                    "The order total is displaying correctly.",
                    "The order total is not displaying correctly.", driver);
            
            Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt),
                    "The print link should be displayed.",
                    "The print link is displayed.",
                    "The print link is not displayed.", driver);
            
            receipt.clickPrintLink();
            Log.message(i++ + ". Clicked on print link.", driver);
            
            Log.testCaseResult();
        } // Ending try block
        catch(Exception e) {
            Log.exception(e, driver);
        } // Ending catch block
        finally {
            Log.endTestCase(driver);
        } // Ending finally
    } // Ending C26132
}