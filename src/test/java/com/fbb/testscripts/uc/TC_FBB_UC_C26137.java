package com.fbb.testscripts.uc;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OfferPage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.headers.Headers;
import com.fbb.pages.ordering.GuestOrderStatusLandingPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.TestData;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_UC_C26137 extends BaseTest {
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader demandware =EnvironmentPropertiesReader.getInstance("demandware");
	private static EnvironmentPropertiesReader checkout =EnvironmentPropertiesReader.getInstance("checkout");
    
	@Test(groups = { "uc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
    public void M1_FBB_UC_C26137(String browser) throws Exception {
        Log.testCaseInfo();

        final WebDriver driver = WebDriverFactory.get(browser);
        
        String guestEmailAddress =  AccountUtils.generateEmail(driver);
        String lvlProdEligibleRMX = TestData.get("level_with_prodPromo", Brand.rmx);
        String prdPromoLevelRMX = TestData.get("prd_promo-prd-level", Brand.rmx);
        String prdShoeIDJLX = TestData.get("prd_shoeType", Brand.jlx);
        String couponOrderLevelJLX = TestData.get("orderLevelPromo", Brand.jlx);
        String NewletterPopUpMessage = demandware.get("NewletterPopUpMessage");
        String shippingAddress = "taxless_address";
        String billingAddress = "address_withtax";
        String billingZipCode = checkout.get("address_withtax").split("\\|")[4];
        String paymentCard = "card_Amex";
        
        int i = 1;
        try {

            // step 1 - Navigate to the universal cart website as the Roamans brand.
            HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();

            homePage.headers.chooseBrandFromHeader(Brand.elx);
            Log.message(i++ +". Navigated to Ellos brand home page", driver);

            Headers headers = homePage.headers;

            OfferPage offerPage = headers.navigateToOfferPage();
            Log.message(i++ +". Clicked on offers link", driver);
            
            Log.softAssertThat(offerPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), offerPage), 
                    "Offers page should be displayed",
                    "Offers page is displayed",
                    "Offers page is not displayed", driver);
            
            String prdName = offerPage.getProductNameByIndex(1);
            
            PdpPage pdpPage = offerPage.navigateToPdpByIndex(1);
            Log.message(i++ +". Navigated to first product PDP", driver);
            
            Log.softAssertThat(pdpPage.getProductName().equalsIgnoreCase(prdName),
                    "Respective product PDP should be displayed.", 
                    "Respective product PDP is displayed.",
                    "Respective product PDP is not displayed.", driver);
            
            pdpPage.selectAllSwatches();
            Log.message(i++ +". Selected color and size swatches", driver);
            
            pdpPage.clickAddProductToBag();
            Log.message(i++ +". Clicked Add to bag button", driver);
			
            pdpPage.clickOnEmptySpaceInABTOverlay(); 
            Log.message(i++ +". Clicked out side of the ATB overlay", driver);

            Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("flytMiniCart"), pdpPage),
            		"ATB overlay should close and user still in PDP.",
            		"ATB overlay is closed and user still in PDP.",
            		"ATB overlay is not closed and user is not in PDP.", driver);

            headers.chooseBrandFromHeader(Brand.rmx); 
            Log.message(i++	+". Navigated to roamans brand home Page", driver);

            PlpPage plpPage = homePage.headers.navigateTo(lvlProdEligibleRMX);
            Log.message(i++ +". Navigated to Product level promotion eligible PLP",	driver);

            Log.softAssertThat(plpPage.getCategoryName().equalsIgnoreCase(lvlProdEligibleRMX.split("\\|")[1]), 
            		"Page should navigate to respective category.",
            		"Page is navigated to respective category.",
            		"Page is not navigated to respective category.", driver);

            Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProdPromoMessage"), plpPage),
            		"Promotion callout message should be displayed.",
            		"Promotion callout message should be displayed.",
            		"Promotion callout message should be displayed.", driver);

            plpPage.navigateToPdp(prdPromoLevelRMX); 
            Log.message(i++ +". Navigated to PDP",driver);

            Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtPromotionCalloutMsg"), pdpPage),
            		"Promotion callout message should be displayed.",
            		"Promotion callout message should be displayed.",
            		"Promotion callout message should be displayed.", driver);

            ShoppingBagPage cartPage = pdpPage.addToBagAndNavigate(); 
            Log.message(i++ +". Product added to cart", driver);

            headers.chooseBrandFromHeader(Brand.jlx); 
            Log.message(i++	+". Selected and navigated to JL brand home Page", driver);

            homePage.headers.navigateToPDP(prdShoeIDJLX);
            Log.message(i++	+". Navigated to shoe type product", driver);

            Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtPromotionCalloutMsg"), pdpPage),
            		"Promotion callout message should be displayed in PDP",
            		"Promotion callout message is displayed in PDP",
            		"Promotion callout message is not displayed in PDP", driver);

            pdpPage.selectAllSwatches();
            Log.message(i++ +". Selected color and size", driver);

            pdpPage.selectQuantity(3);
            pdpPage.clickAddProductToBag(); 
            Log.message(i++	+". Product added to cart", driver);

            pdpPage.clickOnCheckoutInMCOverlay();
            Log.message(i++ +"Clicked review my bag link from MC overlay", driver);

            double orderTotalBefore = cartPage.getOrderTotal();

            cartPage.applyPromoCouponCode(couponOrderLevelJLX);
            Log.message(i++ +". Applied order level promo code", driver);

            Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryDiscount"), cartPage),
            		"Discount should be displayed on Order summary section.",
            		"Discount is displayed on Order summary section.",
            		"Discount is not displayed on Order summary section.", driver);

            Log.softAssertThat( (cartPage.getOrderTotal() < orderTotalBefore),
            		"Order total should be updated properly",
            		"Order total is updated properly",
            		"Order total is not updated properly", driver);

            SignIn signIn = (SignIn) cartPage.navigateToCheckout();
            Log.message(i++ +". Navigated to checkout login page", driver);

            signIn.typeGuestEmail(guestEmailAddress);
            Log.message(i++ +". Entered guest email Address", driver);

            signIn.selectNewsLetterSignUpCheckbox(false);
            Log.message(i++ +". Deselect the checkbox of \"Keep me informed with the latest Deals and Trends\"", driver);
            
            Log.softAssertThat(signIn.elementLayer.verifyElementDisplayed(Arrays.asList("txtNewsletterPopup"), signIn) 
            		&& signIn.elementLayer.verifyTextContains("txtNewsletterPopup", NewletterPopUpMessage, signIn),
            		"pop up should be displayed underneath the checkbox and pop should consist of \"Don't Miss Out on the Latest EVENTS & TRENDS! the box\"", 
            		"pop up is displayed underneath the checkbox and pop is consist of \"Don't Miss Out on the Latest EVENTS & TRENDS! the box\"",
            		"pop up not displayed underneath the checkbox and pop is not consist of \"Don't Miss Out on the Latest EVENTS & TRENDS! the box\"", driver);
            
            CheckoutPage checkoutPage = signIn.clickOnContinueInGuestLogin();
            Log.message(i++ +". navigated to checkout page", driver);
            
            checkoutPage.fillingShippingDetailsAsGuest(shippingAddress,ShippingMethod.Standard);
            Log.message(i++ +". Entered shipping address", driver);
            
            checkoutPage.checkUncheckUseThisAsBillingAddress(false);
            Log.message(i++ +". Deselected the checkbox of use this as a billing address", driver);
            
            double orderTotalBrfore = Double.parseDouble(checkoutPage.getOrderTotalInSummary());
            
            checkoutPage.selectShippingMethod(ShippingMethod.Express);
            Log.message(i++ +". Selected express delivery method", driver);
            
            double orderTotalAfter = Double.parseDouble(checkoutPage.getOrderTotalInSummary());
            
            Log.softAssertThat(orderTotalAfter > orderTotalBrfore, 
            		"order total should be updated properly.",
            		"order total is updated properly.",
            		"order total is not updated properly.", driver);
            
            checkoutPage.continueToBilling();
            Log.message(i++ +". navigated to billing address section", driver);
            
            checkoutPage.fillingBillingDetailsAsGuest(billingAddress);
            Log.message(i++ +". Entered billing address", driver);
            
            checkoutPage.continueToPayment();
            Log.message(i++ +". Navigated to payment section", driver);
            
            Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("appiledCoupon"), checkoutPage),
            		"Applied promo code should be displayed on promo code section.",
            		"Applied promo code is displayed on promo code section.",
            		"Applied promo code is not displayed on promo code section.", driver);
            
            checkoutPage.fillingCardDetails1(paymentCard, false);
            Log.message(i++ +". Entered Amex card details", driver);
            
            checkoutPage.continueToReivewOrder();
            Log.message(i++ +". Clicked continue button", driver);
            
            OrderConfirmationPage orderReciept = checkoutPage.clickOnPlaceOrderButton();
            Log.message(i++ +". Clicked place order button and navigated to order confirmation page", driver);
            
            String orderNumber = orderReciept.getOrderNumber();
            LinkedList<LinkedHashMap<String, String>> productDetails =  orderReciept.getProductDetails();
            String orderTotal = orderReciept.getOrderTotal();
            String orderBillingAddress = orderReciept.getBillingAddressDetails();
            String orderShippingAddress = orderReciept.getShippingAddressDetails();
            String orderShippingMethod = orderReciept.getShippingMethod();
            String promoCode = orderReciept.getAppliedPromoCode();
                        
            headers.chooseBrandFromHeader(Brand.rmx);
            Log.message(i++ +". redirect to the Roamans site on the universal cart", driver);
            
            homePage.footers.navigateToOrderStatusUnderAccountSection();
            Log.message(i++ +". Clicked on order status from footer section", driver);
            
            signIn.clickOnLookUpToggle();
            Log.message(i++ +". Clicked look up button", driver);
            
            GuestOrderStatusLandingPage orderDetailsPage = signIn.navigateToGuestOrderStatusPage(orderNumber, guestEmailAddress, billingZipCode);
            Log.message(i++ +". Entered Order number, Email address, billing zip code and navigated to order details page", driver);
            
            Log.softAssertThat((orderDetailsPage.getOrderNumber().equals(orderNumber) && orderDetailsPage.getPromoCode().equals(promoCode)) 
            		&& (orderDetailsPage.getPaymetMethodName().contains(paymentCard.split("\\|")[0]) && orderDetailsPage.getorderTotal().equalsIgnoreCase(orderTotal)), 
            		"order number, promo code, order total and payment method should be same in order confirmation page and order detaila page.",
            		"order number, promo code, order total and payment method should be same in order confirmation page and order detaila page.",
            		"order number, promo code, order total and payment method should be same in order confirmation page and order detaila page.", driver);
            
            Log.softAssertThat((orderDetailsPage.getShippingAddressDetails().equalsIgnoreCase(orderShippingAddress) && orderDetailsPage.getBillingAddressDetails().equalsIgnoreCase(orderBillingAddress)) 
            		&& (orderDetailsPage.getShippingMethod().equalsIgnoreCase(orderShippingMethod) && orderDetailsPage.getProductDetails().containsAll(productDetails)),
            		"Shipping address, Billing Address, Shipping method and Products details should be same in order confirmation page and order details page",
            		"Shipping address, Billing Address, Shipping method and Products details should be same in order confirmation page and order details page",
            		"Shipping address, Billing Address, Shipping method and Products details should be same in order confirmation page and order details page", driver);
            
            Log.testCaseResult();
        } // Ending try block
        catch (Exception e) {
            Log.exception(e, driver);
        } // Ending catch block
        finally {
            Log.endTestCase(driver);
        } // Ending finally

    }// M1_FBB_UC_C26137

}// TC_FBB_UC_C26137