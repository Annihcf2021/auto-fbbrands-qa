package com.fbb.testscripts.uc;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.GiftCardPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.MiniCartPage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.WishlistLoginPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_UC_C26133 extends BaseTest {
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "uc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_UC_C26133(String browser) throws Exception {
		
		Log.testCaseInfo();
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			String userEmail = AccountUtils.generateEmail(driver);
			String userPassword = accountData.get("password_global");
			String credential = userEmail+"|"+userPassword;
			String standardProdID = TestData.get("prd_standard_slp_search", Brand.jlx).split("\\|")[1]; 
			String giftCardNumberAndPin = TestData.get("gift_card_$20_1", Brand.elx); 
			String freeShippingProdID = TestData.get("prd_variation", Brand.rmx);
			HashSet<String> productsIdToVerify = new HashSet<String>();
			HashSet<String> productsNameToVerify = new HashSet<String>();
			String freeShipCouponCode = TestData.get("cpn_freeshipping");
			String dicountCouponCode = TestData.get("cpn_off_10_percentage");
			String cpnLimitError = demandData.get("couponLimitError");
			String checkoutAdd = "valid_address1";
			String checkoutAdd2 = "taxless_address";
			String paymentMethod = "card_Visa";
			String paymentDetails = checkoutData.get(paymentMethod);
			
			{
				GlobalNavigation.registerNewUserAddress(driver, checkoutAdd +"|"+ checkoutAdd2, credential);
				GlobalNavigation.addNewPaymentToAccount(driver, paymentMethod, true, credential);
			}

			//Step:1 - Navigate to the universal cart website as a Jessica London brand.
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Navigated to RMX Home Page!", driver);
			
			homePage.headers.chooseBrandFromHeader(Brand.jlx);
			Log.message(i++ +". Choosed JLX brand from header", driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage)
					&& UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.jlx), 
					"System should navigate to JLX brand home page", 
					"System is navigated to JLX brand home page", 
					"System is not navigated to JLX brand home page", driver);
			
			//Step:2 - Search & navigate to standard product PDP.
			
			Headers headers = homePage.headers;
			PdpPage pdpPage = headers.navigateToPDP(standardProdID);
			Log.message(i++ +". Navigated to standard product PDP", driver);
			
			productsIdToVerify.add(standardProdID);
			String standardPrdName = pdpPage.getProductName();
			productsNameToVerify.add(standardPrdName.toLowerCase());
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("selectedSize"), pdpPage), 
					"Size should be pre-selected", 
					"Size is pre-selected", 
					"Size is not pre-selected", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkAddToWishList"), pdpPage), 
					"Wishlist link should be displayed", 
					"Wishlist link is displayed", 
					"Wishlist link is not displayed", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ +". Clicked on Add to bag button", driver);
			
			pdpPage.clickOnContinueShoppingInMCOverlay();
			Log.message(i++ +". Clicked on Continue shopping from ATB overlay", driver);
			
			//Step:3 - Verify mini cart flyout.
			
			MiniCartPage miniCart;
			if(!Utils.isMobile()) {
				miniCart = headers.mouseOverMiniCart();
				Log.message(i++ +". Mouse hover on my bag icon", driver);
				
				Log.softAssertThat(miniCart.verifyProductDisplayedInOverlay(productsNameToVerify), 
						"Previosuly added products should be displayed in mini cart flyout.", 
						"Previosuly added products are displayed in mini cart flyout.", 
						"Previosuly added products are not displayed in mini cart flyout.", driver);
				
				Log.softAssertThat(miniCart.elementLayer.verifyElementDisplayed(Arrays.asList("qtyMiniCartBagIndicator"), miniCart), 
						"Product count should be displayed over mini cart ", 
						"Product count is displayed over mini cart ", 
						"Product count is not displayed over mini cart ", driver);
				
				Log.softAssertThat(miniCart.elementLayer.verifyElementDisplayed(Arrays.asList("miniCartImage", "miniCartName", "miniCartSizeVariationByCSS", "miniCartColorVariationByCSS", "miniCartProductQuantityByCSS", "miniCartProductPriceByCSS", "miniCartProductTotalPriceByCSS"), miniCart), 
						"Product variations(size, color, qty & price..etc.) should be displayed properly", 
						"Product variations(size, color, qty & price..etc.) are displayed properly", 
						"Product variations(size, color, qty & price..etc.) are not displayed properly", driver);
			}
			
			//Step:4 - Click on Roamon's favicon from the Header
			
			headers.chooseBrandFromHeader(Brand.rmx);
			Log.message(i++ +". Choosed RMX brand from header", driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage)
					&& UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.rmx), 
					"System should navigate to RMX brand home page", 
					"System is navigated to RMX brand home page", 
					"System is not navigated to RMX brand home page", driver);
			
			//Step:5 - Navigate to free shipping eligible product PDP.
			
			pdpPage = headers.navigateToPDP(freeShippingProdID);
			Log.message(i++ +". Navigated to free shipping eligible product PDP", driver);
			
			productsIdToVerify.add(freeShippingProdID);
			String freeShippingProductName = pdpPage.getProductName();
			productsNameToVerify.add(freeShippingProductName.toLowerCase());
			
			//Step:6 - Navigate to wishlist log in page.
			
			WishlistLoginPage wishlistLoginPage = (WishlistLoginPage)pdpPage.addToWishlist();
			Log.message(i++ +". Clicked on wishlist link.", driver);
			
			Log.softAssertThat(wishlistLoginPage.elementLayer.verifyElementDisplayed(Arrays.asList("divSearchWishlist"), wishlistLoginPage), 
					"System should navigate to wishlist login page", 
					"System is navigated to wishlist login page", 
					"System is not navigated to wishlist login page", driver);
			
			WishListPage wishlistPage = wishlistLoginPage.loginUser(userEmail, userPassword);
			Log.message(i++ +". Sign in to wishlist page.", driver);
			
			Log.softAssertThat(wishlistPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), wishlistPage)
					&& UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.rmx), 
					"System should navigate to RMX brand wishlist page", 
					"System is navigated to RMX brand wishlist page", 
					"System is not navigated to RMX brand wishlist page", driver);
			
			Log.softAssertThat(wishlistPage.verifyProductAddedToWishlist(Arrays.asList(freeShippingProductName)),
					"Added free shipping product should be displayed in wishlist", 
					"Added free shipping product is displayed in wishlist", 
					"Added free shipping product is not displayed in wishlist", driver);
			
			wishlistPage.clickAddtoBagByIndex(0);
			Log.message(i++ +". Clicked on add to bag button.", driver);
			
			//Step:7 - Navigate to cart page.
			
			ShoppingBagPage cartPage = wishlistPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ +". Navigated to cart page.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("miniCartContent"), cartPage)
					&& UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.rmx), 
					"System should navigate to RMX brand cart page", 
					"System is navigated to RMX brand cart page", 
					"System is not navigated to RMX brand cart page", driver);
			
			HashSet<String> cartProductId = cartPage.getProductIdList();
			Log.softAssertThat(cartPage.elementLayer.compareTwoHashSet(productsIdToVerify, cartProductId),
					"Previously added products should be displayed in cart page", 
					"Previously added products are displayed in cart page", 
					"Previously added products are not displayed in cart page", driver);
			
			double totalBeforeCouponApplied = cartPage.getOrderTotal();
			cartPage.applyPromoCouponCode(freeShipCouponCode);
			Log.message(i++ +". Applied a free shipping promo code.", driver);
			
			double totalAfterCouponApplied = cartPage.getOrderTotal();
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryShippingDiscountDetail"), cartPage) 
					&& (totalBeforeCouponApplied > totalAfterCouponApplied),
					"Order summary should be recalculated after applied promo code", 
					"Order summary is recalculated after applied promo code", 
					"Order summary is not recalculated after applied promo code", driver);
			
			//Step:8 - Update the product quantity.
			
			Object obj = cartPage.clickOnEditLink(standardProdID);
			if(Utils.isDesktop()) {
				QuickShop quickShop = (QuickShop) obj;
				Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("divQuickShop"), quickShop) 
						&& UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.rmx),
						"Confirm Edit overlay is displaying in Roaman's brand.", 
						"Edit overlay is displaying in Roaman's brand.", 
						"Edit overlay is not displaying in Roaman's brand.", driver);
				
				quickShop.selectQty("2");
				quickShop.clickOnUpdateCartSplProdEditOverlay();
				Log.message(i++ +". Updated the qty as 2", driver);
				
				//Step:9 - Click on Ellos's favicon from the Header
				
				headers.chooseBrandFromHeader(Brand.elx);
				Log.message(i++ +". Choosed ELX brand from header", driver);
			} else {
				pdpPage = (PdpPage) obj;
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage) 
						&& UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.jlx)
						&& pdpPage.getProductName().equals(standardPrdName),
						"Confirm the system redirected to Jessica London and landed on PDP of the standard product.", 
						"The system is redirected to Jessica London and landed on PDP of the standard product.", 
						"The system is not redirected to Jessica London and landed on PDP of the standard product.", driver);
				
				pdpPage.selectQty("2");
				Log.message(i++ +". Selected the qty as 2", driver);
				
				pdpPage.clickOnUpdate();
				Log.message(i++ +". Updated the qty as 2", driver);
				
				//Step:9 - Click on Ellos's favicon from the Header
				
				headers.chooseBrandFromHeader(Brand.elx);
				Log.message(i++ +". Choosed ELX brand from header", driver);
			} 
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage)
					&& UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.elx), 
					"System should navigate to ELX brand home page", 
					"System is navigated to ELX brand home page", 
					"System is not navigated to ELX brand home page", driver);
			
			//Step:10 - From the footer section click on the "Gift card Balance" link.
			
			GiftCardPage giftLandindPage = homePage.footers.navigateToGiftCardLandingPage();
			Log.message(i++ +". Navigated to Gift card landing page", driver);
			
			Log.softAssertThat(giftLandindPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnGiftCard", "btnGiftCert", "btnCheckYourBalance"), giftLandindPage), 
					"Gift card & E-gift card links and 'Check your balance' button should be displayed", 
					"Gift card & E-gift card links and 'Check your balance' button are displayed", 
					"Gift card & E-gift card links and 'Check your balance' button are not displayed", driver);
			
			//Step:11 - Click on the Check balance button.
			
			giftLandindPage.clickOnCheckYourBalance();
			Log.message(i++ +". Clicked on Balance check up button", driver);
			
			Log.softAssertThat(giftLandindPage.elementLayer.verifyElementDisplayed(Arrays.asList("divChkBalanceModelPopup"), giftLandindPage), 
					"Check balance modal should be displayed", 
					"Balance modal is displayed", 
					"Bbalance modal is not displayed", driver);
			
			giftLandindPage.checkGiftCardBalance(giftCardNumberAndPin.split("\\|")[0], giftCardNumberAndPin.split("\\|")[1]);
			Log.message(i++ +". Applied gift card number & pin in check balance modal", driver);
			
			Log.softAssertThat(giftLandindPage.elementLayer.verifyElementDisplayed(Arrays.asList("totalBalance"), giftLandindPage), 
					"The balance amount should be displayed.", 
					"The balance amount is displayed.", 
					"The balance amount is not displayed.", driver);
			
			giftLandindPage.clickClose();
			Log.message(i++ +". Closed the check balance modal", driver);
			
			//Step:12 - Add Gift card to cart.
			
			pdpPage = giftLandindPage.navigateToGiftCards();
			Log.message(i++ +". Navigated to gift card PDP", driver);
			
			productsIdToVerify.add(pdpPage.getProductID());
			productsNameToVerify.add(pdpPage.getProductName().toLowerCase());
			
			pdpPage.selectGCSize(TestData.get("giftcard_Amount"));
			Log.message(i++ +". Selected gift card amount", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ +". Clicked on Add to bag", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkProductNameInMCOverLay", "imgPrimaryImageInMCOverLay", "lblGiftCardPriceInMCOverLay", "lblProductTotalInMCOverLay"), pdpPage), 
					"The product details should be displayed in MCOverlay.", 
					"The product details are displayed in MCOverlay.", 
					"The product details are not displayed in MCOverlay.", driver);
			
			//Step:13 - Navigate to cart page.
			
			cartPage = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ +". Navigated to cart page", driver);
			
			cartProductId = cartPage.getProductIdList();
			Log.softAssertThat(cartPage.elementLayer.compareTwoHashSet(productsIdToVerify, cartProductId),
					"Previously added products should be displayed in cart page", 
					"Previously added products are displayed in cart page", 
					"Previously added products are not displayed in cart page", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("divAppliedCoupon"), cartPage),
					"The applied free shipping promo code in Roamans should be displayed on the cart page.", 
					"The applied free shipping promo code in Roamans is displayed on the cart page.", 
					"The applied free shipping promo code in Roamans is not displayed on the cart page.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryShippingDiscountDetail"), cartPage),
					"The free shipping label & discounted price should be displayed in the order summary section", 
					"The free shipping label & discounted price is displayed in the order summary section", 
					"The free shipping label & discounted price is not displayed in the order summary section", driver);
			
			cartPage.applyPromoCouponCode(dicountCouponCode);
			Log.message(i++ +". Applied another coupon code.", driver);
			
			String cpnErrorMessage = cartPage.getCouponErrorMessage();
			Log.softAssertThat(cpnErrorMessage.contains(cpnLimitError),
					"Error message should be displayed like 'Only one coupon allowed per order'.", 
					"Error message is displayed like 'Only one coupon allowed per order'.", 
					"Error message is not displayed like 'Only one coupon allowed per order'.", driver); 
			
			//Step:14 - Navigate to the Checkout page - step 3
			
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ +". Navigated to checkout page.", driver);
			
			String selectedShippingAddress = checkoutPage.getselectedShippingAddress();
			String selectedBillingAddress = checkoutPage.getselectedBillingAddress();
			Log.softAssertThat(selectedShippingAddress.contains("Default") && selectedBillingAddress.contains("Default"),
					"Confirm the default address is displayed as shipping & billing address.", 
					"The default address is displayed as shipping & billing address.", 
					"The default address is not displayed as shipping & billing address.", driver);
			
			Log.softAssertThat(checkoutPage.verifyDefaultDeliveryOptionChecked(),
					"Confirm the standard delivery is selected as default.", 
					"The standard delivery is selected as default.", 
					"The standard delivery is not selected as default.", driver);
			
			Log.softAssertThat(paymentDetails.contains(checkoutPage.getDefaultPaymentMethod()),
					"Confirm the default card is selected in the payment section.", 
					"The default card is selected in the payment section.", 
					"The default card is not selected in the payment section.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("fldTxtCvvNo"), checkoutPage),
					"The CVV field of the default card should be displayed.", 
					"The CVV field of the default card is displayed.", 
					"The CVV field of the default card is not displayed.", driver);
			
			cartProductId = checkoutPage.getCheckoutSummeryNameList();
			Log.softAssertThat(cartPage.elementLayer.compareTwoHashSet(productsNameToVerify, cartProductId),
					"Previously added products should be displayed in checkout page", 
					"Previously added products are displayed in checkout page", 
					"Previously added products are not displayed in checkout page", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("appiledCoupon"), checkoutPage),
					"The applied free shipping promo code in Roamans should be displayed on the cart page.", 
					"The applied free shipping promo code in Roamans is displayed on the cart page.", 
					"The applied free shipping promo code in Roamans is not displayed on the cart page.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryShippingDiscountDetail"), checkoutPage),
					"The free shipping label & discounted price should be displayed in the order summary section", 
					"The free shipping label & discounted price is displayed in the order summary section", 
					"The free shipping label & discounted price is not displayed in the order summary section", driver);
			
			checkoutPage.applyGiftCardByValue(TestGiftCardValue.$20);
			Log.message(i++ + ". Gift Card is applied", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryGiftCardDiscount"), checkoutPage),
					"The applied gift card amount should be displayed in the Order summary.", 
					"The applied gift card amount is displayed in the Order summary.", 
					"The applied gift card amount is not displayed in the Order summary.", driver);
			
			checkoutPage.enterCVV("333");
			Log.message(i++ +". Entered CVV number ", driver);
			
			HashSet<String> checkoutProductItemNo = checkoutPage.getOrderedPrdListNumber();
			String OrderSummaryTotal = checkoutPage.getOrderSummaryTotal();
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			//Step:15 - Order confirmation page.
			
			OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			HashSet<String> ProductItemNoReceipt = receipt.getOrderedPrdListNumber();
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductItemNoReceipt, checkoutProductItemNo), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();
		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			GlobalNavigation.RemoveWishListProducts(driver);
			Log.endTestCase(driver);
		} // Ending finally
		
	} // Ending C26133
} 