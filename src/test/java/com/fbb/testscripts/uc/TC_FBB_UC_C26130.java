package com.fbb.testscripts.uc;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.MiniCartPage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_UC_C26130 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");

	@Test(groups = { "uc", "desktop", "tablet", }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_UC_C26130(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		String validUsername = AccountUtils.generateEmail(driver);
		String validPassword = accountData.get("password_global");
		String prdIdDropShipJL = TestData.get("prd_dropship", Brand.jlx);
		String dropShipMsg = demandwareData.get("DropShipMessage");
		String giftcardAmount = TestData.get("giftcard_Amount");
		String personalMessage = "Happy birthday to you!!!";
		String checkoutAdd = "taxless_address";
		String checkoutAddress = checkoutProperty.getProperty(checkoutAdd);
		String payment = "card_MasterCard";
		
		HashSet<String> productsAdded = new HashSet<String>();
		
		int i=1;
		try
		{
			
			{
				GlobalNavigation.registerNewUser(driver, 0, 0, validUsername + "|" + validPassword);
			}
			
			//step 1 - Navigate to the universal cart website as a Jessica London brand
			
			HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
			Log.reference(i++ +". Navigated to RMX for temporary flow", driver);
			
			Headers header = homePage.headers;
						
			header.chooseBrandFromHeader(Brand.jlx);
			Log.message(i++ + ". Navigated to '" + BrandUtils.getBrandFullName(Brand.jlx) + "' Home Page!", driver);
			
			homePage.headers.mouseOverAccountMenu();
			Log.message(i++ + ". Sign in flyout opened", driver);
			
			homePage.headers.typeUserNameInFlyout(validUsername);
			Log.message(i++ + ". Typed username("+validUsername+") Sign in flyout", driver);
			
			homePage.headers.typePasswordInFlyout(validPassword);
			Log.message(i++ + ". Typed username("+validPassword+") Sign in flyout", driver);
			
			homePage.headers.clickOnSignInDesktop();
			Log.message(i++ + ". Clicked Sign in button in flyout", driver);


			// step 2 - Click on the Ellos favicon from the utility bar
			
			header.chooseBrandFromHeader(Brand.elx);
			Log.message(i++ + ". Navigated to '" + BrandUtils.getBrandFullName(Brand.elx) + "' Home Page!", driver);
			
			Log.softAssertThat(homePage.checkPrimaryLogoBrandName(Brand.elx) &&
					homePage.headers.getActiveSharedCartBrandName().equals(Brand.elx),
					"Home page should be displayed for EL brand site",
					"Home page is displayed for EL brand site",
					"Home page is not displayed for EL brand site", driver);
			
			Log.softAssertThat(homePage.headers.verifyUserIsLoggedIn(),
					"The user should be still log in to the EL site.",
					"The user is still logged in to the EL site.",
					"The user is not logged in to the EL site.", driver);
			
			header.chooseBrandFromHeader(Brand.rmx);
			Log.message(i++ + ". Navigated to '" + BrandUtils.getBrandFullName(Brand.rmx) + "' Home Page!", driver);
			
			Log.softAssertThat(homePage.checkPrimaryLogoBrandName(Brand.rmx) &&
					homePage.headers.getActiveSharedCartBrandName().equals(Brand.rmx),
					"Home page should be displayed for RM brand site",
					"Home page is displayed for RM brand site",
					"Home page is not displayed for RM brand site", driver);
			
			Log.softAssertThat(homePage.headers.verifyUserIsLoggedIn(),
					"The user should be still log in to the RM site.",
					"The user is still logged in to the RM site.",
					"The user is not logged in to the RM site.", driver);
			
			header.chooseBrandFromHeader(Brand.jlx);
			Log.message(i++ + ". Navigated to '" + BrandUtils.getBrandFullName(Brand.jlx) + "' Home Page!", driver);
			
			Log.softAssertThat(homePage.checkPrimaryLogoBrandName(Brand.jlx) &&
					homePage.headers.getActiveSharedCartBrandName().equals(Brand.jlx),
					"Home page should be displayed for JL brand site",
					"Home page is displayed for JL brand site",
					"Home page is not displayed for JL brand site", driver);
			
			Log.softAssertThat(homePage.headers.verifyUserIsLoggedIn(),
					"The user should be still log in to the jl site.",
					"The user is still logged in to the jl site.",
					"The user is not logged in to the jl site.", driver);
			
			//Step 3 - Navigate to the Product details page and scroll down to the recently viewed section.
			
			// Navigated to PDP directly, hence flow mismatch (Need to change flow after clarification - check test case steps)
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdIdDropShipJL);
			Log.message(i++ + ". Navigated to Drop ship PDP", driver);
			
			
			//Step 4 - Navigate to the Product detail page
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "productName", "lblOriginalPrice", pdpPage), 
					"The product price should be displayed below the product name.", 
					"The product price is displayed below the product name.",
					"The product price is not displayed below the product name.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyTextContains("txtProdInfos",dropShipMsg, pdpPage),
					"'This items ship directly from a third party brand' should be shown", 
					"'This items ship directly from a third party brand' is shown", 
					"'This items ship directly from a third party brand' is not shown", driver);
			
			productsAdded.add(pdpPage.getProductIdFromUrl());
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". Product added to cart", driver);
			
			//Step 5 - Navigate to the cart page.
			
			ShoppingBagPage shoppingBagPg = pdpPage.navigateToShoppingBag();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			HashSet<String> productIdList = new HashSet<String>();
			productIdList = shoppingBagPg.getProductIdList();
			
			HashSet<String> productNameListCart = new HashSet<String>();
			productNameListCart = shoppingBagPg.getCartItemNameList();
			
			String productCount = Integer.toString(shoppingBagPg.getProductCountInCart());
			
			Log.softAssertThat(shoppingBagPg.elementLayer.compareTwoHashSet(productsAdded, productIdList), 
					"Selected product from Jessica London should be displayed on the cart page.", 
					"Selected product from Jessica London is displayed on the cart page.", 
					"Selected product from Jessica London is not displayed on the cart page.", driver);
			
			MiniCartPage miniCart = homePage.headers.mouseOverMiniCart();
			Log.message(i++ + ". Mini cart flyout opened", driver);
			
			HashSet<String> productNameListMini = new HashSet<String>();
			productNameListMini = miniCart.getCartItemNameList();
			
			Log.softAssertThat(shoppingBagPg.elementLayer.compareTwoHashSet(productNameListMini, productNameListCart), 
					"Selected product from Jessica London should be displayed on the Mini-cart.", 
					"Selected product from Jessica London is displayed on the Mini-cart.", 
					"Selected product from Jessica London is not displayed on the Mini-cart.", driver);
			
			String productCountMini = homePage.headers.getMiniCartCount();
			
			Log.softAssertThat(productCountMini.equalsIgnoreCase(productCount), 
					"The product count should be updated on My bag icon", 
					"The product count is updated on My bag icon", 
					"The product count is not updated on My bag icon",  driver);
			
			//Step 6 - Click on the Ellos favicon from the header section on the universal cart website.
			
			header.chooseBrandFromHeader(Brand.elx);
			Log.message(i++ + ". Navigated to '" + BrandUtils.getBrandFullName(Brand.elx) + "' Home Page!", driver);
			
			Log.softAssertThat(homePage.checkPrimaryLogoBrandName(Brand.elx) &&
					homePage.headers.getActiveSharedCartBrandName().equals(Brand.elx),
					"Home page should be displayed for EL brand site",
					"Home page is displayed for EL brand site",
					"Home page is not displayed for EL brand site", driver);
			
			//Step 7 - Scroll down to the recommendation section.
			
			Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("certonaRecommendations", "certonaRecommendationsProductTile", "certonaRecommendationsProductName", "certonaRecommendationsProductPrice"), homePage),
                    "Recommendation section should be displayed on home page", 
                    "Recommendation section is displaying on home page", 
                    "Recommendation section is not displaying home page",   driver);
			
			pdpPage = homePage.navigateToTrendingNowPDPByIndex(0);
			if (pdpPage == null) {
				Log.reference("Trending now is empty. Navigating to random PDP.");
				PlpPage plpPage = homePage.headers.navigateToRandomCategory();
	            Log.message(i++ + ". Navigated to PLP: " + plpPage.getCategoryName(), driver);
				pdpPage = plpPage.navigateToPdp();
			}
			Log.message(i++ + ". Clicked on a product from the Trending Now section in home page.", driver);
			
			//Step 8 - Navigate to the Product detail page
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("pdpMain"), pdpPage), 
					"Should be Navigate to PDP page.", 
					"Navigated to PDP page.", 
					"Not navigated to PDP page.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productName", "lblRegularPrice", pdpPage), 
					"The product price should be displayed below the product name.", 
					"The product price is displayed below the product name.",
					"The product price is not displayed below the product name.", driver);
			
			if(pdpPage.verifyProperties("FreeExchange")) {
				Log.message(i++ +". Selected product is a free exchange eligible product",  driver);
			} else {
				Log.reference(i++ +". Selected product is not a free exchange eligible product",  driver);
			}
			
			String prdIdEL = pdpPage.getProductIdFromUrl();
			productsAdded.add(prdIdEL);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". Product added to cart", driver);
			
			//Step 9 - Navigate to the cart page.
			
			shoppingBagPg = pdpPage.navigateToShoppingBag();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			productIdList = new HashSet<String>();
			productIdList = shoppingBagPg.getProductIdList();
			
			productNameListCart = new HashSet<String>();
			productNameListCart = shoppingBagPg.getCartItemNameList();
			
			productCount = Integer.toString(shoppingBagPg.getProductCountInCart());
			
			Log.softAssertThat(shoppingBagPg.elementLayer.compareTwoHashSet(productsAdded, productIdList), 
					"Selected product from Jessica London should be displayed on the cart page.", 
					"Selected product from Jessica London is displayed on the cart page.", 
					"Selected product from Jessica London is not displayed on the cart page.", driver);
			
			miniCart = homePage.headers.mouseOverMiniCart();
			Log.message(i++ + ". Mini cart flyout opened", driver);
			
			productNameListMini = new HashSet<String>();
			productNameListMini = miniCart.getCartItemNameList();
			
			Log.softAssertThat(shoppingBagPg.elementLayer.compareTwoHashSet(productNameListMini, productNameListCart), 
					"Selected product from Jessica London should be displayed on the Mini-cart.", 
					"Selected product from Jessica London is displayed on the Mini-cart.", 
					"Selected product from Jessica London is not displayed on the Mini-cart.", driver);
			
			productCountMini = homePage.headers.getMiniCartCount();
			
			Log.softAssertThat(productCountMini.equalsIgnoreCase(productCount), 
					"The product count should be updated on My bag icon", 
					"The product count is updated on My bag icon", 
					"The product count is not updated on My bag icon",  driver);
			
			Double subTotalBefore = shoppingBagPg.getOrderSubTotal();
			String colorBefore = shoppingBagPg.getProductColorVariation(0);
			String sizeBefore = shoppingBagPg.getProductSizeVariation(0);
			Log.event("Product Name/Color/Size Values Before:: " + colorBefore + "/" + sizeBefore);
			
			shoppingBagPg.clickOnEditLink(prdIdDropShipJL);
			Log.message(i++ + ". Clicked on Edit link for Product", driver);

			String size = new String();
			String color = new String();
			if(Utils.isDesktop()) {
				Log.assertThat(new QuickShop(driver).get().getPageLoadStatus(), 
						"Update Overlay should be displayed.", 
						"Update Overlay displayed.", 
						"Update Overlay not displayed.", driver);
				
				QuickShop quickShop = new QuickShop(driver).get();
				do {
					color = quickShop.selectDifferentColor(colorBefore);
				}while(colorBefore.equalsIgnoreCase(color));
				Log.event("Updated Size/Color :: " + size + "/" + color);
				
				quickShop.selectQty("2");
				Log.event("Updated qty :: 2");
				
				quickShop.addToBag();
				Log.message(i++ + ". Clicked on Update button.", driver);
				
			} else {
				Log.assertThat(new PdpPage(driver).get().getPageLoadStatus(), 
						"Page should be navigated to PDP Page.",
						"Page navigated to PDP Page.",
						"Page not navigated to PDP Page.", driver);
				
				pdpPage = new PdpPage(driver).get();
				do {
					color = pdpPage.selectDifferentColor(colorBefore);
				}while(colorBefore.equalsIgnoreCase(color));
				Log.event("Updated Size/Color :: " + size + "/" + color);
				
				pdpPage.selectQty("2");
				Log.event("Updated qty :: 2");
				
				pdpPage.clickOnUpdate();
				Log.message(i++ +". Clicked on Update Button.", driver);
			}
			
			Double subTotalAfter = shoppingBagPg.getOrderSubTotal();
			
			Log.softAssertThat(subTotalAfter > subTotalBefore, 
					"The subtotal should be updated in order Summary.", 
					"The subtotal is updated in order Summary.",
					"The subtotal is not updated in order Summary.", driver);
			
			//Step 10 - Click on the Roamans favicon from the header section on the universal cart website.
			
			header.chooseBrandFromHeader(Brand.rmx);
			Log.message(i++ + ". Navigated to '" + BrandUtils.getBrandFullName(Brand.rmx) + "' Home Page!", driver);
			
			Log.softAssertThat(homePage.checkPrimaryLogoBrandName(Brand.rmx) &&
					homePage.headers.getActiveSharedCartBrandName().equals(Brand.rmx),
					"Home page should be displayed for RM brand site",
					"Home page is displayed for RM brand site",
					"Home page is not displayed for RM brand site", driver);
			
			//Step 11 - Scroll down to the footer section
			
			pdpPage = homePage.footers.navigateToPhysicalGiftCardPDP();
			Log.message(i++ + ". Navigated to GC PDP Page", driver);
			
			//Step 12 - Navigate to the Gift card Product detail page
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("pdpMain"), pdpPage), 
					"Should be Navigate to PDP page.", 
					"Navigated to PDP page.", 
					"Not navigated to PDP page.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "productName", "prdVariation", pdpPage), 
					"The product price should be displayed below the product name.", 
					"The product price is displayed below the product name.",
					"The product price is not displayed below the product name.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("chkGCPersonalizedMessage", "labelGCPersonalizedMessage"), pdpPage), 
					"Add a personalized message label with a checkbox should be shown.", 
					"Add a personalized message label with a checkbox is displayed.",
					"Add a personalized message label with a checkbox is not displayed.", driver);
			
			pdpPage.selectGCSize(giftcardAmount);
			Log.message(i++ + ". Gift card amount selected", driver);
			
			pdpPage.checkGCPersonalizedMessage("check");
			Log.message(i++ + ". Gift card Personalized Message selected", driver);
			
			pdpPage.enterGCFromAddress(validUsername);
			Log.message(i++ + ". Gift card From Address entered", driver);
			
			pdpPage.enterGCToAddress(validUsername);
			Log.message(i++ + ". Gift card From To entered", driver);
			
			pdpPage.enterPersonalMessage(personalMessage);
			Log.message(i++ + ". Added Personal message!", driver);
			
			productsAdded.add(pdpPage.getProductIdFromUrl());
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Click add to bag button!", driver);
			
			//Step 13 - Navigate to the cart page.
			
			shoppingBagPg = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Click on checkout in add to bag overlay!", driver);
			
			productIdList = new HashSet<String>();
			productIdList = shoppingBagPg.getProductIdList();
						
			Log.softAssertThat(productIdList.containsAll(productsAdded), 
					"Selected product from all brands should be displayed on the cart page.", 
					"Selected product from all brands is displayed on the cart page.", 
					"Selected product from all brands is not displayed on the cart page.", driver);
			
			int prdELIndex = shoppingBagPg.getProductIndexByPrdID(prdIdEL);
			colorBefore = shoppingBagPg.getProductColorVariation(prdELIndex);
			sizeBefore = shoppingBagPg.getProductSizeVariation(prdELIndex);
			subTotalBefore = shoppingBagPg.getOrderSubTotal();
			
			pdpPage = null;
			Object obj = shoppingBagPg.clickOnEditLink(prdIdEL);
			
			if(Utils.isDesktop()) {
				QuickShop qsModal = (QuickShop) obj;
				
				Log.softAssertThat(qsModal.elementLayer.verifyElementDisplayed(Arrays.asList("divQuickShop"), qsModal), 
						"Edit Pop up window should be opened on RM brand - current brand", 
						"Edit Pop up window is opened on RM brand - current brand", 
						"Edit Pop up window is not opened on RM brand - current brand", driver);
				
				pdpPage = qsModal.clickOnViewDetails();
				Log.message(i++ + ". 'Clicked on view details from quick shop", driver);
			} else {
				pdpPage = (PdpPage) obj;
			}
						
			do {
				color = pdpPage.selectDifferentColor(colorBefore);
			}while(colorBefore.equalsIgnoreCase(color));
			Log.event("Updated Color :: " + color);
			
			do {
				color = pdpPage.selectDifferentSize(sizeBefore);
			}while(colorBefore.equalsIgnoreCase(size));
			Log.event("Updated Size :: " + size);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Click add to bag button!", driver);
			
			//Step 14 - Navigate to the cart page
			
			shoppingBagPg = pdpPage.navigateToShoppingBag();
			Log.message(i++ + ". Navigated to cart page!", driver);
			
			Log.softAssertThat( ( homePage.headers.getActiveSharedCartBrandName().equals(Brand.elx) )
					&& ( shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("miniCartContent"), shoppingBagPg) ),
					"System should navigated to cart page for EL brand site",
					"System is navigated to cart page for EL brand site",
					"System is not navigated to cart page for EL brand site", driver);
			
			productNameListCart = new HashSet<String>();
			productNameListCart = shoppingBagPg.getCartItemNameList();
			
			Double subTotalEL = shoppingBagPg.getOrderSubTotal();
			
			Log.softAssertThat(subTotalEL != subTotalBefore, 
					"The subtotal should be updated in order Summary.", 
					"The subtotal is updated in order Summary.",
					"The subtotal is not updated in order Summary.", driver);
			
			//Step 15 - Navigate to the Checkout page - step 1
			
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("selectedOptionShipping"), checkoutPage),
					"The system should not display any saved shipping address",
					"The system is not displaying any saved shipping address",
					"The system is displaying any saved shipping address", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("divEmptyFormShipping"), checkoutPage),
					"The system should display add new shipping address in the shipping address section",
					"The system is displayed add new shipping address in the shipping address section",
					"The system is not displayed add new shipping address in the shipping address section", driver);
			
			Log.softAssertThat(checkoutPage.verifyUseThisAsBillingAddressCheckBox(),
					"The checkbox 'Use This As Billing Address' should be selected in the shipping address section",
					"The checkbox 'Use This As Billing Address' is selected in the shipping address section",
					"The checkbox 'Use This As Billing Address' is not selected in the shipping address section", driver);
			
			checkoutPage.fillingShippingDetailsAsGuest(checkoutAdd, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			Double toltalBefore = StringUtils.getPriceFromString(checkoutPage.getOrderSummaryTotal());
			
			checkoutPage.selectShippingMethodOnly(ShippingMethod.Express);
			Log.message(i++ +". Selected Express delivery method", driver);
			
			Double toltalAfterExp = StringUtils.getPriceFromString(checkoutPage.getOrderSummaryTotal());
			
			Log.softAssertThat(toltalAfterExp > toltalBefore, 
					"The Order total should be updated in the Summary section.", 
					"The Order total is updated in the Summary section.",
					"The Order total is not updated in the Summary section.", driver);
			
			checkoutPage.selectShippingMethodOnly(ShippingMethod.NextDay);
			Log.message(i++ +". Selected Next day delivery method", driver);
			
			Double toltalAfterNext = StringUtils.getPriceFromString(checkoutPage.getOrderSummaryTotal());
			
			Log.softAssertThat(toltalAfterNext > toltalAfterExp, 
					"The Order total should be updated in the Summary section.", 
					"The Order total is updated in the Summary section.",
					"The Order total is not updated in the Summary section.", driver);
			
			checkoutPage.clickContinueInPOBoxOverlay();
			Log.message(i++ + ". Clicked Continue in PO box overlay", driver);
			
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			String addressFromBillingPage = checkoutPage.getSavedAddressFromBillingDetails();
			Log.softAssertThat(checkoutPage.compareTwoAddress(checkoutAddress, addressFromBillingPage),
					"Billing address should be same as the incorrect shipping address.",
					"Billing address is same as the incorrect shipping address.",
					"Billing address is not same as the incorrect shipping address.", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
						
			checkoutPage.fillingCardDetails1(payment, false, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			//Step 17 - Navigate to the Checkout page - step 3
						
			HashSet<String> productNameList = checkoutPage.getCheckoutSummeryNameList();
			
			Log.softAssertThat(checkoutPage.elementLayer.compareTwoHashSet(productNameList, productNameListCart), 
					"The product added to cart should be updated in the checkout page", 
					"The product added to cart is updated in the checkout page", 
					"The product added to cart is not updated in the checkout page", driver);
			
			HashSet<String> ProductId = checkoutPage.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPage.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 18 - Order confirmation page
			
			Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			Log.softAssertThat(receipt.checkEnteredShippingAddressReflectedInOrderReceipt(checkoutAdd), 
					"Same shipping address should display in the receipt which is entered in checkout shipping detail", 
					"Same shipping address is displaying", 
					"Different shipping address is displaying", driver);
						
			Log.softAssertThat(receipt.comparePaymentAfterOrderWithEnteredPaymentMethod(payment), 
					"Same payment method should display in the receipt which is entered in checkout payment section", 
					"Same payment method is displaying", 
					"Different payment method is displaying", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblOrderReceipt", validUsername, receipt), 
					"The order receipt should send to the entered mail ID", 
					"The order receipt is send to the entered mail ID", 
					"The order receipt is not send to the entered mail ID", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt), 
					"The user can able to take print out of the order receipt", 
					"The user can able to print the order receipt", 
					"The user cannot able to print the order receipt", driver);
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}//M1_FBB_UC_C26130

}//TC_FBB_UC_C26130
