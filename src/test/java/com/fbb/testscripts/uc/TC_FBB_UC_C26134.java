package com.fbb.testscripts.uc;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.CreateAccountPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.Direction;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_UC_C26134 extends BaseTest {
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "uc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_UC_C26134(String browser) throws Exception {
		Log.testCaseInfo();
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String userEmail = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
		String registeredEmail = "aspiretest@gmail.com";
		String userPassword = accountData.get("password_global");
		String emailExistErrorMsg = demandData.get("emailExistError");
		String shippingExceptionHeadingMsg = demandData.get("ShippingExceptionHeadingForNextDay");
		String checkoutAdd = "valid_address1";
		
		String prdIdBackOrder = TestData.get("prd_back-order", Brand.rmx).split("\\|")[0];
		String swatchColorBackOrder = TestData.get("prd_back-order", Brand.rmx).split("\\|")[1];
		String swatchSizeBackOrder = TestData.get("prd_back-order", Brand.rmx).split("\\|")[2];
		HashSet<String> productsIdToVerify = new HashSet<String>();
		HashSet<String> productsNameToVerify = new HashSet<String>();
		String payment = "card_MasterCard";
		String rewardCert = TestData.get(Utils.getRewardCertificate(), Brand.jlx);
		
		int i = 1;
		try {

			//Step:1 - Navigate to the universal cart website as an Ellos brand.
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Navigated to RMX Home Page!", driver);
			
			homePage.headers.chooseBrandFromHeader(Brand.elx);
			Log.message(i++ +". Choosed ELX brand from header", driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage)
					&& UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.elx), 
					"System should navigate to ELX brand home page", 
					"System is navigated to ELX brand home page", 
					"System is not navigated to ELX brand home page", driver);
			
			//Step:2 - Click on the sign-in link & Click on create account button
			
			Headers headers = homePage.headers;
			CreateAccountPage createAccountPage = headers.navigateToCreateAccount();
			Log.message(i++ +". Navigated to create account page", driver);
			
			createAccountPage.fillFirstName("Aspire");
			Log.message(i++ +". Entered first name", driver);
			
			createAccountPage.fillLastName("QA");
			Log.message(i++ +". Entered last name", driver);
			
			createAccountPage.fillEmailAddress(registeredEmail);
			Log.message(i++ +". Entered email address", driver);
			
			createAccountPage.fillConfirmEmailAddress(registeredEmail);
			Log.message(i++ +". Entered confirm email address", driver);
			
			createAccountPage.fillPassword(userPassword);
			Log.message(i++ +". Entered password", driver);
			
			createAccountPage.fillConfirmPassword(userPassword);
			Log.message(i++ +". Entered confirm password", driver);
			
			createAccountPage.clickOnCreateAccout();
			Log.message(i++ +". Clicked on create account button", driver);
			
			String emailErrorMessage = createAccountPage.elementLayer.getElementText("emailExistError", createAccountPage);
			Log.softAssertThat(emailErrorMessage.equalsIgnoreCase(emailExistErrorMsg), 
					"The error messages should be displayed like 'Email has already taken'.", 
					"The error messages is displayed like 'Email has already taken'.", 
					"The error messages is not displayed like 'Email has already taken'.", driver);
			
			createAccountPage.fillEmailAddress(userEmail);
			Log.message(i++ +". Enetered email address", driver);
			
			createAccountPage.fillConfirmEmailAddress(userEmail);
			Log.message(i++ +". Entered confirm email address", driver);
			
			ProfilePage profilePage = (ProfilePage) createAccountPage.clickOnCreateAccout();
			Log.message(i++ +". Clicked on create account button", driver);
			
			Log.softAssertThat(profilePage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), profilePage), 
					"The user should be created an account and landing on my account Profile page.", 
					"The user successfully created an account and landing on my account Profile page.", 
					"The user not successfully created an account and landing on my account Profile page.", driver);
			
			//Step:3 - Click on the Jessica London Favicon from the Header
			
			headers.chooseBrandFromHeader(Brand.jlx);
			Log.message(i++ +". Choosed JLX brand from header", driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage)
					&& UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.jlx), 
					"System should navigate to JLX brand home page", 
					"System is navigated to JLX brand home page", 
					"System is not navigated to JLX brand home page", driver);
			
			Log.softAssertThat(headers.verifyUserIsLoggedIn(), 
					"User should be logged in", 
					"User is logged in", 
					"User is not logged in", driver);
			
			MyAccountPage myAccountPage = homePage.navigateToMyAccount();
			Log.message(i++ +". Navigated to My Account page", driver);
			
			//Step:4 - Click on Addresses link and navigate to Addresses page.
			
			AddressesPage addressPage = myAccountPage.navigateToAddressPage();
			Log.softAssertThat(addressPage.elementLayer.verifyElementDisplayed(Arrays.asList("divAddressForm"), addressPage), 
					"Addresses form fields should be displayed.", 
					"Addresses form fields is displayed.", 
					"Addresses form fields is not displayed.", driver);
			
			addressPage.fillingAddressDetails(checkoutAdd, false);
			Log.message(i++ +". Provided a taxable address details", driver);
			
			addressPage.clickSaveAddress();
			Log.message(i++ +". Added a taxable address", driver);
			
			String nickNameDefaultAddress = addressPage.getNickNameOfAnAddress(1);
			Log.softAssertThat(addressPage.verifyAddressIsDefault(nickNameDefaultAddress), 
					"The address should be saved as default address.", 
					"The address is saved as default address.", 
					"The address is not saved as default address.", driver);	
			
			//Step:5 - Click on My bag icon and navigate to the Empty cart page.
			
			ShoppingBagPage cartPage = headers.navigateToShoppingBagPage();
			Log.message(i++ +". Navigated to cart page", driver);
			
			Log.softAssertThat(cartPage.verifyEmptyCart(), 
					"System should navigate to empty cart page", 
					"System is navigated to empty cart page", 
					"System is not navigated to empty cart page", driver);
			
			String emptyCartHead = cartPage.elementLayer.getElementText("emptyShoppingHeader", cartPage);
			Log.softAssertThat(emptyCartHead.equalsIgnoreCase("My shopping bag"), 
					"Heading should be displayed as 'My Shopping Bag'", 
					"Heading is displayed as 'My Shopping Bag'", 
					"Heading is not displayed as 'My Shopping Bag'", driver);
			
			String emptyCartSubHead = cartPage.elementLayer.getElementText("emptyCartMsg", cartPage);
			Log.softAssertThat(emptyCartSubHead.equalsIgnoreCase("Your shopping bag is empty"), 
					"Subheading should be displayed as 'Your shopping bag is empty'.", 
					"Subheading is displayed as 'Your shopping bag is empty'.", 
					"Subheading is not displayed as 'Your shopping bag is empty'.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnEmptyWhatsNew"), cartPage), 
					"Shop whats new button should be displayed.", 
					"Shop whats new button is displayed.", 
					"Shop whats new button is displayed.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnEmptyWhatsNew", "trendNowSlickContainer", cartPage),
					"Trending now section should be displayed below of the 'Shop whats new' button.", 
					"Trending now section is displayed below of the 'Shop whats new' button.", 
					"Trending now section is not displayed below of the 'Shop whats new' button.", driver);
			
			PdpPage pdpPage = cartPage.clickTrendNowImage();
			Log.message(i++ +". Clicked on trending now product", driver);
			
			productsIdToVerify.add(pdpPage.getProductID());
			productsNameToVerify.add(pdpPage.getProductName().toLowerCase());
			
			pdpPage.selectSize();
			Log.message(i++ +". Selected size swatch", driver);
			
			pdpPage.selectColor();
			Log.message(i++ +". Selected color swatch", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ +". Clicked on Add to bag button", driver);
			
			pdpPage.clickOnContinueShoppingInMCOverlay();
			Log.message(i++ +". Clicked Continue shopping in ATB Overlay", driver);
			
			//Step:6 - Click on the Roamans Favicon from the Header
			
			headers.chooseBrandFromHeader(Brand.rmx);
			Log.message(i++ +". Choosed RMX brand from header", driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage)
					&& UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.rmx), 
					"System should navigate to RMX brand home page", 
					"System is navigated to RMX brand home page", 
					"System is not navigated to RMX brand home page", driver);
			
			Log.softAssertThat(homePage.headers.verifyUserIsLoggedIn(), 
					"Make sure that the user should be still logged in.", 
					"The user is still logged in.", 
					"The user is not logged in.", driver);
			
			myAccountPage = headers.navigateToMyAccount();
			Log.message(i++ +". Navigated to My account page", driver);
			
			//Step:7 - Click on Addresses link from MyAccount page.
			
			addressPage = myAccountPage.navigateToAddressPage();
			Log.message(i++ +". Navigated to Address page", driver);
			
			Log.softAssertThat(addressPage.verifyAddressIsDefault(nickNameDefaultAddress), 
					"The default address added from Jessica London should be displayed.", 
					"The default address added from Jessica London is displayed.", 
					"The default address added from Jessica London is not displayed.", driver);
			
			Log.softAssertThat(addressPage.verifyDefaultAddressIndicatorForNickName(nickNameDefaultAddress)
						&& addressPage.verifyEditDeleteSavedCards(), 
					"Green indicator, Edit, and Delete should be displayed.", 
					"Green indicator, Edit, and Delete is displayed.", 
					"Green indicator, Edit, and Delete is not displayed.", driver);
			
			addressPage.clickOnAddNewAddress();
			Log.message(i++ +". Clicked on Add new address", driver);
			
			addressPage.fillingAddressDetails("taxless_address", false);
			Log.message(i++ +". Provided the nontaxable address details", driver);
			
			addressPage.clickSaveAddress();
			Log.message(i++ +". Added a nontaxable address", driver);
			
			String nickNameTaxlessAddress = addressPage.getNickNameOfAnAddress(2);
			Log.softAssertThat(!addressPage.verifyAddressIsDefault(nickNameTaxlessAddress), 
					"Address should be saved as non-default.", 
					"Address is saved as non-default.", 
					"Address is not saved as non-default.", driver);
			
			Log.softAssertThat(addressPage.verifyEditDeleteMakeDefaultAllSavedCards(), 
					"Make default, Edit and Delete buttons should be displayed.", 
					"Make default, Edit and Delete buttons are displayed.", 
					"Make default, Edit and Delete buttons are not displayed.", driver);
			
			//Step:8 - From search box search for the backorder product
			
			pdpPage = headers.navigateToPDP(prdIdBackOrder);
			Log.message(i++ +". Navigated to back order product PDP", driver);
			
			productsIdToVerify.add(prdIdBackOrder);
			productsNameToVerify.add(pdpPage.getProductName().toLowerCase());
			
			Log.softAssertThat(pdpPage.verifyProductPriceDisplayed(),
					"The price of the product should be displayed in PDP.", 
					"The price of the product is displayed in PDP.", 
					"The price of the product is not displayed in PDP.", driver); 
			
			pdpPage.selectColor(swatchColorBackOrder);
			Log.message(i++ +". Selected color swatch", driver);
			
			pdpPage.selectSize(swatchSizeBackOrder);
			Log.message(i++ +". Selected size swatch", driver);
			
			String productPrice = pdpPage.getProductPrice();
			String stockMsg = pdpPage.elementLayer.getElementText("backOrderInventoryState", pdpPage);
			Log.softAssertThat(stockMsg.contains("Expected to ship by"),
					"Verify inventory message should be displayed as 'Expedited to ship by Dec 10, 2019'.", 
					"Verify inventory message is displayed as 'Expedited to ship by Dec 10, 2019'.", 
					"Verify inventory message is not displayed as 'Expedited to ship by Dec 10, 2019'.", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ +". Clicked on Add to bag button", driver);
			
			cartPage = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ +". Clicked on Review My Bag button from ATB overlay", driver);
			
			//Step:9 - Navigate to the Cart page.
			
			String cartProductPrice = cartPage.getProductPrice(cartPage.getProductCountInCart()-1);
			Log.softAssertThat(productPrice.equalsIgnoreCase(cartProductPrice),
					"Verify Product prices are should be the same as PDP.", 
					"Product prices are same as PDP.", 
					"Product prices are not the same as PDP.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("divOrderSubTotal", "orderSummaryShippingDetail", "orderSummarySalesTax", "divRemainingTotal", "divTotalSavings"), cartPage),
					"Subtotal, Shipping & handling charges, order total and Total savings should be displayed in order summary section.", 
					"Subtotal, Shipping & handling charges, order total and Total savings are displayed in order summary section.", 
					"Subtotal, Shipping & handling charges, order total and Total savings are not displayed in order summary section.", driver);
			
			HashSet<String> cartProductId = cartPage.getProductIdList();
			Log.softAssertThat(cartPage.elementLayer.compareTwoHashSet(productsIdToVerify, cartProductId),
					"Previously added products should be displayed in cart page", 
					"Previously added products are displayed in cart page", 
					"Previously added products are not displayed in cart page", driver);
			
			//Step:10 - Click on the Ellos Favicon from the Header
			
			headers.chooseBrandFromHeader(Brand.jlx);
			Log.message(i++ +". Choosed JLX brand from header", driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage)
					&& UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).equals(Brand.jlx), 
					"System should navigate to JLX brand home page", 
					"System is navigated to JLX brand home page", 
					"System is not navigated to JLX brand home page", driver);
			
			//Step:11 - Click on my bag icon and navigate to the cart page.
			
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ +". Navigated to shopping bag page", driver);
			
			Log.softAssertThat(cartPage.getNoOfProductsInCart() == 2,
					"Each product should be displayed as a separate row.", 
					"Each product is displayed as a separate row.", 
					"Each product is not displayed as a separate row.", driver);
			
			cartProductId = cartPage.getProductIdList();
			Log.softAssertThat(cartPage.elementLayer.compareTwoHashSet(productsIdToVerify, cartProductId),
					"Previously added products should be displayed in cart page", 
					"Previously added products are displayed in cart page", 
					"Previously added products are not displayed in cart page", driver); 
			
			Log.softAssertThat(cartPage.verifyQuanityEditArrowposition(),
					"Quantity drop-down should consist of upward and downward arrows.", 
					"Quantity drop-down is consist of upward and downward arrows.", 
					"Quantity drop-down is not consist of upward and downward arrows.", driver);
			
			Log.softAssertThat(cartPage.verifyDisabledQtyUpdateArrow(prdIdBackOrder, Direction.Down),
					"Downward arrows should be in disable state and users not allowed to click", 
					"Downward arrows should be in disable state and users not allowed to click", 
					"Downward arrows should be in disable state and users not allowed to click", driver);
			
			//Step:12 - Navigate to the checkout page.
			
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ +". Clicked on Checkout Now button", driver);
			
			Log.softAssertThat(checkoutPage.verifyNavigatedToShippingSection(),
					"The page should navigate to Shipping address section.", 
					"The page is navigated to Shipping address section.", 
					"The page is not navigated to Shipping address section.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("selectedShippingAddressDropDown", "btnEditShippingAddressLogIn", "btnAddNewShippingAddress"), checkoutPage),
					"Saved address drop-down with Edit, Add new links should be displayed.", 
					"Saved address drop-down with Edit, Add new links are displayed.", 
					"Saved address drop-down with Edit, Add new links are not displayed.", driver);
			
			String selectedShippingAddress = checkoutPage.getselectedShippingAddress();
			Log.softAssertThat(selectedShippingAddress.contains("Default"),
					"The default address should be in the selected sate in the dropdown.", 
					"The default address is in the selected sate in the dropdown.", 
					"The default address is not in the selected sate in the dropdown.", driver);
			
			checkoutPage.checkUncheckUseThisAsBillingAddress(false);
			Log.message(i++ +". Deselected the checkbox of 'use this as a billing address'", driver);
			
			Log.softAssertThat(checkoutPage.verifyDefaultDeliveryOptionChecked(),
					"By default standard delivery should be selected", 
					"By default standard delivery is selected", 
					"By default standard delivery is not selected", driver);
			
			checkoutPage.selectShippingMethodOnly(ShippingMethod.NextDay);
			Log.message(i++ +". Selected 'Next business day' delivery checkbox.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("shippingmethodExceptionOverlay"), checkoutPage),
					"The shipping exception overlay should be displayed.", 
					"The shipping exception overlay is displayed.", 
					"The shipping exception overlay is not displayed.", driver);
			
			String shippingExceptionHeading = checkoutPage.elementLayer.getElementText("shippingmethodExceptionOverlayHeading", checkoutPage);
			Log.softAssertThat(shippingExceptionHeading.equalsIgnoreCase(shippingExceptionHeadingMsg),
					"Shipping exception overlay heading message should be displayed.", 
					"Shipping exception overlay heading message is displayed.", 
					"Shipping exception overlay heading message is not displayed.", driver);
			
			String shippingExceptionText = checkoutPage.elementLayer.getElementText("shippingMethodExceptionItemIntroduction", checkoutPage).trim();
			Log.softAssertThat(shippingExceptionText.equalsIgnoreCase("UPDATED SHIPPING METHOD: Standard Delivery"),
					"Text message should be displayed like 'Updated shipping method: Standard delivery'", 
					"Text message is displayed like 'Updated shipping method: Standard delivery'", 
					"Text message is not displayed like 'Updated shipping method: Standard delivery'", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("poBoxBlockOverlayProduct", "btnCancelExpeditedOverlay", "OverlayContinueButton"), checkoutPage),
					"Product details, Cancel and continue button should be displayed.", 
					"Product details, Cancel and continue button are displayed.", 
					"Product details, Cancel and continue button are not displayed.", driver);
			
			checkoutPage.clickOnContinueInShippingMethodExceptionOverlay();
			Log.message(i++ +". Clicked on the Continue button from the shipping exception overlay.", driver);
			
			//Step:13 - Navigate to the billing address section.
			
			checkoutPage.continueToBilling();
			Log.message(i++ +". Clicked on the continue button to billing section.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("selectedBillingAddress", "lnkEditBilling", "lnkAddNewBilling"), checkoutPage),
					"Billing address dropdown should be displayed with Edit, Add new links.", 
					"Billing address dropdown is displayed with Edit, Add new links.", 
					"Billing address dropdown is not displayed with Edit, Add new links.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "savedAddressCountWithText", "selectedBillingAddress", checkoutPage),
					"Saved address count should be displayed above the address dropdown.", 
					"Saved address count is displayed above the address dropdown.", 
					"Saved address count is not displayed above the address dropdown.", driver);
			
			checkoutPage.selectValueFromSavedAddressesDropdownByIndexInBillingDetails(2);
			Log.message(i++ +". Selected the non-default address", driver);
			
			//Step:14 - Navigate to the payment method section.
			
			checkoutPage.applyRewardCertificate(rewardCert);
			Log.message(i++ +". Applied a reward certificate.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryRewardDiscount"), checkoutPage),
					"Applied reward amount should be displayed in the order summary section.", 
					"Applied reward amount is displayed in the order summary section.", 
					"Applied reward amount is not displayed in the order summary section.", driver);
			
			checkoutPage.selectCardType("MasterCard");
			Log.message(i++ +". Selected Master card type.", driver);
			
			checkoutPage.fillingCardDetails1(payment, true, false);
			Log.message(i++ +". Entered Master card details and selected checkbox of 'keep this card on file for easier checkout next time'.", driver);
			
			checkoutPage.continueToReivewOrder();
			Log.message(i++ +". Clicked on the continue button to Review order.", driver);
			
			//Step:15 - Navigate to Review and place order screen.
			
			HashSet<String> checkoutProductItemNo = checkoutPage.getOrderedPrdListNumber();
			String OrderSummaryTotal = checkoutPage.getOrderSummaryTotal();
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step:16 - Navigate to an Order confirmation page.
			
			HashSet<String> ProductItemNoReceipt = receipt.getOrderedPrdListNumber();
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductItemNoReceipt, checkoutProductItemNo), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			myAccountPage = homePage.headers.navigateToMyAccount();
			Log.message(i++ + ". Navigated to my account page", driver);
			
			PaymentMethodsPage paymentPage = myAccountPage.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to Payment methods page", driver);
			
			String defaultCardType = paymentPage.getDefaultCardType();
			Log.softAssertThat(defaultCardType.toLowerCase().contains("mastercard"), 
					"CC- Master card information should be saved as default card under this account.", 
					"CC- Master card information is saved as default card under this account.", 
					"CC- Master card information is not saved as default card under this account.", driver);
			
			Log.testCaseResult();
		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		} // Ending finally
	} // Ending C26134
}
