package com.fbb.testscripts.uc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.headers.Headers;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_UC_C26131 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@SuppressWarnings("unchecked")
	@Test(groups = { "uc", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_UC_C26131(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		String validUsername = AccountUtils.generateEmail(driver);
		String validPassword = accountData.get("password_global");
		String credentialWishlistAccount = validUsername + "|" + validPassword;
		String prdIdBonusFixed = TestData.get("prd_fixed-bonus", Brand.rmx);
		String prdIdCQO = TestData.get("qc_valid", Brand.jlx).split("\\|")[0];
		HashMap<String, String> prdIdCQOVariations = new HashMap<String, String>();
		String checkoutAddShipping = "valid_address1";
		String checkoutAddBilling = "valid_address7";
		
		HashSet<String> productsAdded = new HashSet<String>();
		
		int i=1;
		try
		{
			
			{
				GlobalNavigation.createAccountAddProductsWishList(driver, prdIdBonusFixed, true, credentialWishlistAccount);
				prdIdCQOVariations = Utils.getCatalogVariationFromTestData(TestData.get("qc_valid"));
			}
			
			//Step 1 - Navigate to the universal cart website as a Roamans brand.
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to '" + BrandUtils.getBrandFullName(Brand.rmx) + "' Home Page!", driver);
			
			Headers header = homePage.headers;
			
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(validUsername, validPassword);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
			
			
			//Step 2 - Navigate to the My-Wishlist page
			
			WishListPage wishlistPg = myAcc.navigateToWishListPage();
			Log.message(i++ + ". Navigated To Wishlist page", driver);
			
			List<String> productIdLst = new ArrayList<String>();
			productIdLst = wishlistPg.getProductIdList();
			
			Log.softAssertThat(productIdLst.contains(prdIdBonusFixed),
					"The bonus product should be added as the wishlist item on the wishlist page.",
					"The bonus product is added as the wishlist item on the wishlist page.",
					"The bonus product is not added as the wishlist item on the wishlist page.", driver);
			
			wishlistPg.clickAddtoBagByPrdID(prdIdBonusFixed);
			Log.message(i++ + ". Clicked on Add to bag button from wish-pist page for bonus eligible product", driver);
			
			productsAdded.add(prdIdBonusFixed);
			
			int productCountMini = Integer.parseInt(homePage.headers.getMiniCartCount());
			
			Log.softAssertThat( (productCountMini == 2) ,
					"My bag added product count should be shown on the wishlist page.",
					"My bag added product count is displayed on the wishlist page.",
					"My bag added product count is not displayed on the wishlist page.", driver);
			
			Log.softAssertThat(wishlistPg.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), wishlistPg),
					"The page should remain on my wishlist page.",
					"The page remains on my wishlist page.",
					"The page is not remain on my wishlist page.", driver);

			// step 3 - Click on My Bag icon.
			
			ShoppingBagPage shoppingBagPg = wishlistPg.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			HashSet<String> productIdList = new HashSet<String>();
			productIdList = shoppingBagPg.getProductIdList();
			
			HashSet<String> productNameListCart = new HashSet<String>();
			productNameListCart = shoppingBagPg.getCartItemNameList();
			
			String productCount = Integer.toString(shoppingBagPg.getProductCountInCart());
			
			Log.softAssertThat(productIdList.containsAll(productsAdded), 
					"Selected product from Jessica London should be displayed on the cart page.", 
					"Selected product from Jessica London is displayed on the cart page.", 
					"Selected product from Jessica London is not displayed on the cart page.", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyTextContains("txtWishlistItem", "This is a wishlist item.", shoppingBagPg)
					&& shoppingBagPg.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "productUnitPrice", "txtWishlistItem", shoppingBagPg),
					"\"This is a Wishlist item\" label should be displayed on below the Price deatils.",
					"\"This is a Wishlist item\" label is displayed on below the Price deatils.",
					"\"This is a Wishlist item\" label is not displayed on below the Price deatils.", driver);
			
			Double subTotalBefore = shoppingBagPg.getOrderSubTotal();
			String colorBefore = shoppingBagPg.getProductColorVariation(0);
			String sizeBefore = shoppingBagPg.getProductSizeVariation(0);
			Log.event("Product Name/Color/Size Values Before:: " + colorBefore + "/" + sizeBefore);
			
			PdpPage pdpPage = (PdpPage) shoppingBagPg.clickOnEditLink(prdIdBonusFixed);
			Log.message(i++ + ". Clicked on Edit link for Product ",  driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage),
					"Edit link should take the User to the PDP.",
					"Edit link is taking the User to the PDP",
					"Edit link is not taking the User to the PDP", driver);
			
			String sizeAfter = pdpPage.getSelectedSize();
			String colorAfter =pdpPage.getSelectedColor();
			
			Log.softAssertThat( (colorAfter.equalsIgnoreCase(colorBefore) ) && (sizeAfter.equalsIgnoreCase(sizeBefore) ) , 
					"Selected color and size swatches should be auto-selected by default in the PDP page.", 
					"Selected color and size swatches is auto-selected by default in the PDP page.", 
					"Selected color and size swatches is not auto-selected by default in the PDP page.",  driver);
			
			//Step 4 - Click on Jessica London's favicon from the Header section on the universal cart website.
			
			header.chooseBrandFromHeader(Brand.jlx);
			Log.message(i++ + ". Navigated to '" + BrandUtils.getBrandFullName(Brand.jlx) + "' Home Page!", driver);
			
			Log.softAssertThat(homePage.checkPrimaryLogoBrandName(Brand.jlx) &&
					homePage.headers.getActiveSharedCartBrandName().equals(Brand.jlx),
					"Home page should be displayed for JL brand site",
					"Home page is displayed for JL brand site",
					"Home page is not displayed for JL brand site", driver);
			
			Log.softAssertThat(homePage.headers.verifyUserIsLoggedIn(),
					"The user should be still log in to the jl site.",
					"The user is still logged in to the jl site.",
					"The user is not logged in to the jl site.", driver);
			
			//Step 5 - Click on the Hamburger menu and My Account from the header section.
			
			myAcc = homePage.headers.navigateToMyAccount();
			Log.message(i++ + ". Navigated to My account Page", driver);
			
			//Step 6 - Navigate to the Catalog Quick order page
			
			QuickOrderPage quickOrder = myAcc.navigateToQuickOrder();
			Log.message(i++ + ". Navigated to Quick order catalog page.");
			
			quickOrder.searchItemInQuickOrder(prdIdCQO);
			Log.message(i++ + ". Searched with valid product id.", driver);
			
			Log.softAssertThat(quickOrder.verifySearchedCatalogItemsDisplayed(new String [] {prdIdCQO}),
					"Searched catalog products should be added to the Catalog Quick Order page.",
					"Searched catalog products is added to the Catalog Quick Order page.",
					"Searched catalog products is not added to the Catalog Quick Order page.", driver);
			
			String prdId = quickOrder.getProductIDByCatalogNumber(prdIdCQO);
			productsAdded.add(prdId);
			
			quickOrder.selectVariation(0, prdIdCQOVariations);
			Log.message(i++ +". Selected variations for CQO item.", driver);
			
			quickOrder.clickAddProductToBag(0);
			Log.message(i++ + ". Added product to cart", driver);
			
			quickOrder.closeAddToBagOverlay();
			Log.message(i++ + ". Closed the ATB overlay", driver);
			
			//Step 7 - Navigate to the cart page
			
			shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			productIdList = new HashSet<String>();
			productIdList = shoppingBagPg.getProductIdList();
			
			Log.softAssertThat(productIdList.containsAll(productsAdded), 
					"Selected products from Roamans, Jessica London should be displayed on the cart page.", 
					"Selected products from Roamans, Jessica London is displayed on the cart page.", 
					"Selected products from Roamans, Jessica London is not displayed on the cart page.", driver);
			
			Log.softAssertThat(shoppingBagPg.getNoOfProductsInCart() > productsAdded.size(), 
					"Bonus product should be added to bag", 
					"Bonus product added to bag", 
					"Bonus product not added to bag", driver);
			
			productNameListCart = new HashSet<String>();
			productNameListCart = shoppingBagPg.getCartItemNameList();
			
			productCount = Integer.toString(shoppingBagPg.getProductCountInCart());
			
			String productsCountMini = homePage.headers.getMiniCartCount();
			
			Log.softAssertThat(productsCountMini.equalsIgnoreCase(productCount), 
					"The product count should be updated on My bag icon", 
					"The product count is updated on My bag icon", 
					"The product count is not updated on My bag icon",  driver);
			
			Double subTotalAfter = shoppingBagPg.getOrderSubTotal();
			
			Log.softAssertThat(subTotalAfter > subTotalBefore, 
					"The subtotal should be updated in order Summary.", 
					"The subtotal is updated in order Summary.",
					"The subtotal is not updated in order Summary.", driver);
			
			//Step 8 - Click on Ello's favicon from the Header section on the universal cart website.
			
			header.chooseBrandFromHeader(Brand.elx);
			Log.message(i++ + ". Navigated to '" + BrandUtils.getBrandFullName(Brand.elx) + "' Home Page!", driver);
			
			Log.softAssertThat(homePage.checkPrimaryLogoBrandName(Brand.elx) &&
					homePage.headers.getActiveSharedCartBrandName().equals(Brand.elx),
					"Home page should be displayed for EL brand site",
					"Home page is displayed for EL brand site",
					"Home page is not displayed for EL brand site", driver);
			
			Log.softAssertThat(homePage.headers.verifyUserIsLoggedIn(),
					"The user should be still log in to the El site.",
					"The user is still logged in to the El site.",
					"The user is not logged in to the El site.", driver);
			
			homePage.headers.openCloseHamburgerMenu("close");
			Log.message(i++ +"Closed the hamburger menu");
			
			//Step 9 - Navigate to the cart page
			
			shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			productIdList = new HashSet<String>();
			productIdList = shoppingBagPg.getProductIdList();
			
			Log.softAssertThat(productIdList.containsAll(productsAdded), 
					"Selected products from Roamans, Jessica London should be displayed on the cart page.", 
					"Selected products from Roamans, Jessica London is displayed on the cart page.", 
					"Selected products from Roamans, Jessica London is not displayed on the cart page.", driver);
			
			Log.softAssertThat(shoppingBagPg.getNoOfProductsInCart() > productsAdded.size(), 
					"Bonus product should be added to bag", 
					"Bonus product added to bag", 
					"Bonus product not added to bag", driver);
			
			productCount = Integer.toString(shoppingBagPg.getProductCountInCart());
			
			productsCountMini = homePage.headers.getMiniCartCount();
			
			Log.softAssertThat(productsCountMini.equalsIgnoreCase(productCount), 
					"The product count should be updated on My bag icon", 
					"The product count is updated on My bag icon", 
					"The product count is not updated on My bag icon",  driver);
			
			Double subTotalEL = shoppingBagPg.getOrderSubTotal();
			
			Log.softAssertThat(subTotalEL.compareTo(subTotalAfter) == 0, 
					"The subtotal should be updated in order Summary.", 
					"The subtotal is updated in order Summary.",
					"The subtotal is not updated in order Summary.", driver);
			
			//Step 10 - Navigate to the Checkout page - step 1
			
			CheckoutPage checkoutPage = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("divEmptyFormShipping"), checkoutPage),
					"The system should display add new shipping address in the shipping address section",
					"The system is displayed add new shipping address in the shipping address section",
					"The system is not displayed add new shipping address in the shipping address section", driver);
			
			checkoutPage.fillingShippingDetailsAsGuest("No", checkoutAddShipping, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPage.checkUncheckUseThisAsBillingAddress(false);
			Log.message(i++ + ". Use this as a Billing address check box disabled", driver);
			
			Double toltalBefore = StringUtils.getPriceFromString(checkoutPage.getOrderSummaryTotal());
			
			checkoutPage.selectShippingMethodOnly(ShippingMethod.SuperFast);
			Log.message(i++ +". Selected Express delivery method", driver);
			
			Double toltalAfterExp = StringUtils.getPriceFromString(checkoutPage.getOrderSummaryTotal());
			
			Log.softAssertThat(toltalAfterExp > toltalBefore, 
					"The Order total should be updated in the Summary section.", 
					"The Order total is updated in the Summary section.",
					"The Order total is not updated in the Summary section.", driver);
			
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			//Step 11 - Navigate to the Checkout page - step 2
			
			checkoutPage.fillingBillingDetailsAsGuest(checkoutAddBilling);
			Log.message(i++ + ". Billing Address entered successfully", driver);
			
			checkoutPage.continueToPaymentFromBilling();
			Log.message(i++ + ". Continued to Payment Page", driver);			
			

			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			checkoutPage.applyGiftCardByValue(TestGiftCardValue.$1000);
			Log.message(i++ +". Applied valid gift card", driver);
			
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			//Step 12 - Navigate to the Checkout page - step 3
						
			HashSet<String> productNameList = checkoutPage.getCheckoutSummeryNameList();
			
			Log.softAssertThat(checkoutPage.elementLayer.compareTwoHashSet(productNameList, productNameListCart), 
					"The product added to cart should be updated in the checkout page", 
					"The product added to cart is updated in the checkout page", 
					"The product added to cart is not updated in the checkout page", driver);
			
			HashSet<String> ProductId = checkoutPage.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPage.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 13 - Order confirmation page
			
			receipt.clickOnViewDetailsMobile();
			Log.message(i++ + ". Clicked view details for mobile view", driver);
			
			Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			Log.softAssertThat(receipt.checkEnteredShippingAddressReflectedInOrderReceipt(checkoutAddBilling), 
					"Same shipping address should display in the receipt which is entered in checkout shipping detail", 
					"Same shipping address is displaying", 
					"Different shipping address is displaying", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblOrderReceipt", validUsername, receipt), 
					"The order receipt should send to the entered mail ID", 
					"The order receipt is send to the entered mail ID", 
					"The order receipt is not send to the entered mail ID", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt), 
					"The user can able to take print out of the order receipt", 
					"The user can able to print the order receipt", 
					"The user cannot able to print the order receipt", driver);
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}//M1_FBB_UC_C26131

}//TC_FBB_UC_C26131
