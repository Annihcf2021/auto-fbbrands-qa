package com.fbb.testscripts.drop4;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22510 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(enabled = false, groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22510(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String username;
		String password = accountData.get("password_global");
	
		List<String> elementToBeVerified = Arrays.asList("lblPaymentDetailsHeading", "lblBillingAddressHeading", "lblSelectedAddress", "btneditBillingAddress");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);
	
			homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart.", driver);
	
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Cart Page.", driver);
	
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
	
			//Enter all shipping details
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtNickName"), checkoutPage),
					"The Nick Name field should not be displayed for Guest user",
					"The Nick Name field is not displayed for Guest user",
					"The Nick Name field is displayed for Guest user", driver);
	
			checkoutPage.clickOnEditEmailLnk();
			checkoutPage.typeOnEmail(username);
			checkoutPage.typeOnPassword(password);
			checkoutPage.clickSignInButton();
	
			Log.message(i++ + ". Logged in as :: " + username, driver);
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", ShippingMethod.Standard, "valid_address1");
			Log.message(i++ + ". Shipping details filled successfully.", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continuted to Payment Section.", driver);
	
			//1 - Verify the available components in Checkout page when Billing Address Same As Shipping Address
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(elementToBeVerified, checkoutPage),
					"All the components should be displayed in the Payment details section",
					"All the components are displayed in the Payment details section",
					"All the components are not displayed in the Payment details section", driver);
	
			//2 - Verify the display of Module Heading
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayedBelow("lblPaymentDetailsHeading", "divPaymentDetailsSection", checkoutPage),
					"The Payment Details title should be displayed above the payment section",
					"The Payment Details title is displayed above the payment section",
					"The Payment Details title is not displayed above the payment section", driver);
	
			Log.softAssertThat(checkoutPage.verifyPaymentDetailsTitle(),
					"The Payment Details title should be displayed as it is configured",
					"The Payment Details title is displayed",
					"The Payment Details title is not displayed", driver);
	
			//3 - Verify the display of Billing Address Heading
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayedBelow("lblPaymentDetailsHeading", "lblBillingAddressHeading", checkoutPage),
					"The Payment Details title should be displayed above the Billing Address title",
					"The Payment Details title is displayed above the Billing Address title",
					"The Payment Details title is not displayed above the Billing Address title", driver);
	
			Log.softAssertThat(checkoutPage.verifyBillingAddressTitle(),
					"The Billing Address title should be displayed as it is configured",
					"The Payment Details title is displayed",
					"The Payment Details title is not displayed", driver);
	
			//5 - Verify the display of Edit link
			Log.softAssertThat(checkoutPage.verifyEditLinkRightOfBillingAddressTitle(),
					"The Edit link should be displayed to the right of Billing Address title",
					"The Edit link is displayed to the right of Billing Address title",
					"The Edit link is not displayed to the right of Billing Address title", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C22510
}// search
