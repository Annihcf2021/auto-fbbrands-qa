package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C19720 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "low", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C19720(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation1");
		String credential;
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		credential = AccountUtils.generateEmail(driver) + "|test123@";
		//Prerequisite: A registered user account if not exists
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, credential);
		}
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_any, i, credential);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
			//1
			String primaryLogo;
			if(Utils.isDesktop()) {
				primaryLogo = "brandLogo";
			} else {
				primaryLogo = "brandLogoMobTab";
			}
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "headerHelpSection", primaryLogo, checkoutPage), 
						"Logo should be positioned to the top left side in the header.", 
						"Logo is positioned to the top left side in the header.", 
						"Logo is not positioned to the top left side in the header.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, primaryLogo, "stickyNavigationBar", checkoutPage), 
						"Sticky Navigation icons should be positioned below the Brand Logo.", 
						"Sticky Navigation icons is positioned below the Brand Logo.", 
						"Sticky Navigation icons is not positioned below the Brand Logo.", driver);
			} else {
				BrowserActions.scrollToTopOfPage(driver);
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "headerRightSection", primaryLogo, checkoutPage), 
						"Logo should be positioned to the top left side in the header.", 
						"Logo is positioned to the top left side in the header.", 
						"Logo is not positioned to the top left side in the header.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "stickyNavigationBar", primaryLogo, checkoutPage), 
						"Sticky Navigation icons should be positioned beside the Brand Logo.", 
						"Sticky Navigation icons is positioned beside the Brand Logo.", 
						"Sticky Navigation icons is not positioned beside the Brand Logo.", driver);
			}
			
	
			/*checkoutPage = homepage.headers.navigateToCheckOut();*/
	
			if(checkoutPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkLiveChat"), checkoutPage)){
	
	
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "headerRightSection", "lnkLiveChat", checkoutPage), 
						"Live chat should be displayed under customer service section.",
						"Live chat is displayed under customer service section.",
						"Live chat is not displayed under customer service section.", driver);
	
				checkoutPage.openCloseLiveChat("open");
				Log.message(i++ + ". Clicked on Live Chat link.", driver);
	
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlLiveChat"), checkoutPage), 
						"On click/tap of the Live Chat launcher, it should open the Live Chat Modal (if available)",
						"On click/tap of the Live Chat launcher, it opens the Live Chat Modal (if available)",
						"On click/tap of the Live Chat launcher, it did not open the Live Chat Modal (if available)", driver);
	
				checkoutPage.openCloseLiveChat("close");
				Log.message(i++ + ". Bold Chat - Live Chat modal closed.", driver);
			}else{
				Log.reference(i++ + ". Live chat verifications skipped due to chat not available at this time.");
			}
			if(Utils.isMobile()){
				checkoutPage.expandCollapseNeedHelp("open");
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("cntNeedHelp"), checkoutPage), 
						"On tapping 'Need help' header expands and the expanded header should remains open until the user collapses it",
						"On tapping 'Need help' header expands and the expanded header remains open until the user collapses it",
						"On tapping 'Need help' header expands and the expanded header not remains open until the user collapses it", driver);
	
				//checkoutPage.footers.scrollToFooter();
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("cntNeedHelp"), checkoutPage), 
						"When User scrolls up, the expanded header should remains sticky as a part of sticky header",
						"When User scrolls up, the expanded header remains sticky as a part of sticky header",
						"When User scrolls up, the expanded header not remains sticky as a part of sticky header", driver);
	
				if(checkoutPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkLiveChat"), checkoutPage)){
					checkoutPage.openCloseLiveChat("open");
					Log.message(i++ + ". Bold Chat - Live Chat modal opened.", driver);
					Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlLiveChat"), checkoutPage), 
							"On tapping the Live Chat icon, it should open Live Chat modal",
							"On tapping the Live Chat icon, it opens Live Chat modal",
							"On tapping the Live Chat icon, it didn't open Live Chat modal", driver);
	
					Log.softAssertThat(checkoutPage.elementLayer.verifyElementTextWithDWREProp(Arrays.asList("headerBoldChat"),"checkout.header.help",checkoutPage), 
							"The text in the launcher should be handled by the property 'checkout.header.help' from 'checkout.properties'",
							"The text in the launcher handled by the property 'checkout.header.help' from 'checkout.properties'",
							"The text in the launcher didn't handled by the property 'checkout.header.help' from 'checkout.properties'", driver);
	
					checkoutPage.openCloseLiveChat("close");
					Log.message(i++ + ". Bold Chat - Live Chat modal closed.", driver);
				}else{
					Log.reference(i++ + ". Live chat verifications skipped due to chat not available at this time.");
				}
	
			}
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("divWrapper", "position", "relative", checkoutPage), 
					"The contents below header line should be scrollable underneath the Sticky Navigation icons",
					"The contents below header line should be scrollable underneath the Sticky Navigation icons",
					"The contents below header line should be scrollable underneath the Sticky Navigation icons", driver);
	
			Log.softAssertThat(checkoutPage.getHeaderStepStatus(1).equals("active"), 
					"System should highlight the corresponding step in the Checkout process in bold for which the User is currently active",
					"System highlight the corresponding step in the Checkout process in bold for which the User is currently active",
					"System not highlight the corresponding step in the Checkout process in bold for which the User is currently active", driver);
	
			//Log.softAssertThat(checkoutPage.getHeaderStepStatus(2).equals("inactive") && checkoutPage.getHeaderStepStatus(3).equals("inactive"), 
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForListOfElement(Arrays.asList("headerChkoutStep2","headerChkoutStep3"), "class", "inactive", checkoutPage),
					"System should grey out the remaining steps which are inactive",
					"System grey out the remaining steps which are inactive",
					"System not grey out the remaining steps which are inactive", driver);
	
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", ShippingMethod.Standard, "valid_address7");
			Log.message(i++ + ". Shipping Details are filled successfully!", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Step-2: Payments Section", driver);
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("headerChkoutStep1", "innerHTML", "href", checkoutPage), 
					"if the User is in Step 2, then System should allow the User to click on Step 1 and User should redirect to corresponding step",
					"if the User is in Step 2, then System allow the User to click on Step 1 and User should redirect to corresponding step",
					"if the User is in Step 2, then System not allow the User to click on Step 1 and User should redirect to corresponding step", driver);
	
			checkoutPage.clickOnCheckoutStep(1);
			Log.message(i++ + ". Clicked on Checkout Step (1)", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("headerChkoutStep1", "innerHTML", "href", checkoutPage), 
					"If a User has interacted with a page in the checkout process, the icon/text should be clickable",
					"If a User has interacted with a page in the checkout process, the icon/text was clickable",
					"If a User has interacted with a page in the checkout process, the icon/text not clickable", driver);
	
			Log.softAssertThat(!checkoutPage.elementLayer.verifyAttributeForElement("headerChkoutStep3", "innerHTML", "href", checkoutPage), 
					"If there has not been any interaction with a page in the Checkout process, the icon/text should not be clickable",
					"If there has not been any interaction with a page in the Checkout process, the icon/text not be clickable",
					"If there has not been any interaction with a page in the Checkout process, the icon/text be clickable", driver);
			
			HomePage homepage = checkoutPage.clickPrimaryLogo();
			
			Log.softAssertThat(homepage.elementLayer.verifyPageElements(Arrays.asList("divMain"),homepage), 
					"On click/tap of the Brand Logo, the User should navigate to the home page of the Site.", 
					"On click/tap of the Brand Logo, the User is navigate to the home page of the Site.",
					"On click/tap of the Brand Logo, the User is not navigate to the home page of the Site.", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C19720
}// search
