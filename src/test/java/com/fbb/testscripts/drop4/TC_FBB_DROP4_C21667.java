package com.fbb.testscripts.drop4;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C21667 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C21667(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prodId = TestData.get("prd_variation1");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		//Prerequisite: A register user account 
		{
			GlobalNavigation.registerNewUser(driver, 2, 2, AccountUtils.generateEmail(driver) + "|test123@");
		}
	
		int i = 1;
		try {
	
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prodId, i, AccountUtils.generateEmail(driver) + "|test123@");
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (Integer) obj[1];
	
			if(Utils.isDesktop()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "checkoutOrderTotalTable", "signedInSection", checkoutPage),
						"To check the Order summary section is present in the right side of Signed In section.",
						"The Order summary section is present in the right side of Signed In section",
						"The Order summary section is not present in the right side of Signed In section", driver);
			}
	
			//Step 1
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("step3Active"), checkoutPage),
					"To check the system directly gone to 'Review and Place Order' step.",
					"The system directly gone to 'Review and Place Order' step",
					"The system not gone to 'Review and Place Order' step", driver);
			//Step 2
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "signedInSection", "divCheckoutShipping", checkoutPage),
					"The 'Signed in as' section should be displayed above the 'Shipping Details' section.", 
					"The 'Signed in as' section is displayed above the 'Shipping Details' section.", 
					"The 'Signed in as' section is not displayed above the 'Shipping Details' section.", driver);
			//Step 3
			Log.softAssertThat(checkoutPage.verifyUseThisAsBillingAddressCheckBox(),
					"To check the default shipping address is selected.",
					"The default shipping address is selected",
					"The default shipping address is not selected", driver);
	
			Log.softAssertThat(checkoutPage.verifyUseThisAsBillingAddressCheckBox(),
					"To check the 'Use This As Billing Address' is selected.",
					"The 'Use This As Billing Address' is selected by default",
					"The 'Use This As Billing Address' is not selected by default", driver);
	
			Log.softAssertThat(checkoutPage.verifyDefaultDeliveryOptionChecked(),
					"To check the 'Default delivery option' is selected.",
					"The 'Default delivery option' is selected by default",
					"The 'Default delivery option' is not selected by default", driver);
	
			Log.softAssertThat(!checkoutPage.verifyGiftReceiptCheckBoxIsChecked(),
					"To check the 'Gift Receipt Checkbox' is checked/not.",
					"The 'Gift Receipt checkbox' is not checked!",
					"The 'Gift Receipt checkbox' is checked", driver);
	
			Log.softAssertThat(!checkoutPage.verifyGiftMessageCheckBoxIsChecked(),
					"To check the 'Gift Message Checkbox' is checked/not.",
					"The 'Gift Message checkbox' is not checked!",
					"The 'Gift Message checkbox' is checked", driver);
	
			//Step 4
			String shippingAddr = checkoutPage.getselectedShippingAddress();
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Section!", driver);
	
			Log.softAssertThat(checkoutPage.getSelectedOptionBilling().split("\\)")[1].trim().equals(shippingAddr.split("\\)")[1].trim()),
					"To check the Shipping address is reflected in Billing address.",
					"The Shipping address is reflected in Billing address!",
					"The Shipping address is not reflected in Billing address", driver);
	
			Log.softAssertThat(checkoutPage.getCvvEntered().equals(""),
					"To check the CVV is empty by default.",
					"The CVV is empty by default!",
					"The CVV is not empty by default", driver);
	
			Log.softAssertThat((!(checkoutPage.checkPromoCodeSectionExpadedOrNot())),
					"To verify the 'Have a Promo Code' section is collapsed.",
					"The 'Have a Promo Code' section is collapsed.",
					"The 'Have a Promo Code' section is not collapsed.", driver);
	
			Log.softAssertThat((!(checkoutPage.checkRewardSectionExpadedOrNot())),
					"To verify the 'Have a Reward Certificate' section is collapsed.",
					"The 'Have a Reward Certificate' section is collapsed.",
					"The 'Have a Reward Certificate' section is not collapsed.", driver);
	
			Log.softAssertThat((!(checkoutPage.checkGiftCardSectionExpadedOrNot())),
					"To verify the 'Have a Gift Card' section is collapsed.",
					"The 'Have a Gift Card' section is collapsed.",
					"The 'Have a Gift Card' section is not collapsed.", driver);
	
			//Step 5
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnPlaceOrder"), checkoutPage),
					"To check the 'place order' button is present in 'Review and Place Order' section.",
					"The 'place order' button is present in 'Review and Place Order' section",
					"The 'place order' button is not present in 'Review and Place Order' section", driver);
	
			Log.softAssertThat(checkoutPage.verifyPlaceOrderBtnisAboveItemsInBag(),
					"To check Items in bag is displaying below the 'Place Order' button.",
					"The Items in bag is displaying below the 'Place Order' button",
					"The Items in bag is not displaying below the 'Place Order' button", driver);
	
			Log.softAssertThat(checkoutPage.verifyPositionOfItemsInBagEditAndHeading(),
					"To check 'Item bag Edit' is right side to the Item bag heading.",
					"The 'Item bag Edit' is right side to the Item bag heading",
					"The 'Item bag Edit' is not in the right side of the Item bag heading", driver);
	
			//Step 6b
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "checkoutSummaryHeading", "signedInSection", checkoutPage),
					"The Order Summary section should be present on the top of the page to the right of the sections 'Signed in as' and 'Shipping Details'",
					"The Order Summary section is present on the top of the page to the right of the sections 'Signed in as' and 'Shipping Details'",
					"The Order Summary section is not present on the top of the page to the right of the sections 'Signed in as' and 'Shipping Details'",driver);
	
	
			//Step 7
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnEditShippingAddressLogIn","btnAddNewShippingAddress",
					"lnkEditBilling","lnkAddNewBilling","lnkAddNewCredit","lnkItemsInBagEdit"), checkoutPage),
					"To check the edit link and add new link are present in the shipping section and billing section in checkout page.",
					"The edit link and add new link are present in the shipping section and billing section in checkout page",
					"The edit link and add new link are not present in the shipping page and billing section in checkout page", driver);
			//Checking editing shipping address
			checkoutPage.clickOnBrandLogo();
			Log.message(i++ + ". Clicked on Brand Logo!", driver);
			Log.softAssertThat(new HomePage(driver).get().getPageLoadStatus(),
					"System should navigate to Home Page instead of placing the order.",
					"System navigated to Home Page and didn't place the order.",
					"System not navigated to Home Page. Something went wrong.", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	}// TC_FBB_DROP4_C21667
}// search
