package com.fbb.testscripts.drop4;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22491 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "critical", "desktop", "tablet"}, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22491(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String addressUsed = "valid_address7";
	
		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		
		
		//Prerequisite: A register user account 
		{
			GlobalNavigation.registerNewUser(driver, 2, 0, email +"|"+ password);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAccountPage = homePage.headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			AddressesPage addPage = myAccountPage.navigateToAddressPage();
			addPage.fillingAddressDetails("address_1", false);
			addPage.clickSaveAddress();
			Log.message(i++ + ". Address Saved.", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added To Bag.", driver);
	
			//Step-1: Verify the functionality of editing the address with unchecked save this address checkbox
			ShoppingBagPage shoppingBag = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated To Shopping Cart Page", driver);
	
			CheckoutPage checkoutPage = shoppingBag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated To Checkout - Shipping Page", driver);
	
			String nickNameBefore = checkoutPage.getEnteredNickName();
			
			checkoutPage.selectValueFromSavedAddressesDropdownByIndex(2);
			Log.message(i++ + ". Selected Addresss from dropdown.", driver);
			
			String nickNameAfter = checkoutPage.getEnteredNickName();
			
			Log.softAssertThat(!nickNameBefore.equals(nickNameAfter),
					"The saved Address dropdown should be expanded",
					"The saved Address dropdown is expanded",
					"The saved Address dropdown is not expanded", driver);
	
			checkoutPage.selectValueFromSavedAddressesDropdownByIndex(1);
			Log.message(i++ + ". Selected Addresss from dropdown.", driver);
	
			checkoutPage.clickEditShippingAddress();			
			Log.message(i++ + ". Clicked on 'Edit' button in shipping address", driver);
	
			String nickName = checkoutPage.getEnteredNickName();
	
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "YES", ShippingMethod.Standard, addressUsed);
			Log.message(i++ + ". Filled in address details!", driver);
	
			String nickNameUpdate = checkoutPage.getEnteredNickName();
			Log.softAssertThat(!nickName.equals(nickNameUpdate),					
					"Nick Name should be updated",
					"Nick Name is updated",
					"Nick Name is not updated", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Section!", driver);
	
			String addressEntered = checkoutProperty.get(addressUsed);
			
			String addressFromPage = checkoutPage.getSavedAddressFromShippingDetails().replace(",", "|").toLowerCase();
			Log.message("Shipping Details from StoreFront :: "+ addressFromPage);
			Log.message("Shipping Details from User :: "+ addressEntered);
	
			//For debugging purpose
			/*String address = checkoutProperty.get("address_format");
			String addressFormated = GlobalNavigation.formatToBillingAddress(address).toLowerCase().replace(" ", "");
			String savedAddress = checkoutPage.getSavedAddressFromShippingDetails();
			savedAddress = savedAddress.replace("(Default)", "").replace("|", "").replace(" ", "").replace(",", "").replaceAll("-//d//d//d//d", "").trim().toLowerCase();
			Log.message("Shipping Details from StoreFront :: "+ savedAddress);
			Log.message("Shipping Details form User :: "+ addressFormated);*/
	
	
			Log.softAssertThat(checkoutPage.compareTwoAddress(addressEntered, addressFromPage),				
					"The entered address should be displayed",
					"The entered address is displayed",
					"The entered address is not displayed", driver);
	
			checkoutPage.clickOnBrandLogo();
			Log.message(i++ + ". Navigated To Home Page!", driver);
	
			if (Utils.isMobile()) {
				homePage.headers.openCloseHamburgerMenu("open");
				Log.message(i++ + ". Hamburger Menu Opened!", driver);
			}
	
			myAccountPage = homePage.headers.navigateToMyAccount();
			Log.message(i++ + ". Navigated to 'My Account' Page", driver);
	
			AddressesPage addressPage = myAccountPage.clickOnAddressLink();
			Log.message(i++ + ". Navigated to Address Page!", driver);
			
			addressPage.deleteAddress(addressPage.getDefaultAddressNickName());
			Log.message(i++ + ". Deleted existing default address.", driver);
	
			Log.softAssertThat(!addressPage.getSavedAddress().equals(addressEntered),
					"The entered address should not be displayed",
					"The entered address is not displayed",
					"The entered address is displayed", driver);
	
			homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
	
			shoppingBag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to Checkout - Shipping Page", driver);
	
			Log.softAssertThat(checkoutPage.getValueFromSavedAddressDropdown().contains("New Address"),					
					"'New Address' should be displayed in dropdown",
					"'New Address' is displayed in dropdown",
					"'New Address' is not displayed in dropdown", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	}// M1_FBB_DROP4_C22491
	
	@Test(groups = { "critical", "desktop", "tablet"}, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP4_C22491(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String addressUsed = "valid_address7";
		String testNickName = "TestNick";
	
		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		email = email.replaceAll("1@", "2@");
		String password = accountData.get("password_global");
		
		//Prerequisite: A register user account 
		{
			GlobalNavigation.registerNewUser(driver, 1, 0, email +"|"+ password);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAccountPage = homePage.headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			AddressesPage addPage = myAccountPage.navigateToAddressPage();
			addPage.fillingAddressDetails("address_1", false);
			addPage.clickSaveAddress();
			Log.message(i++ + ". Address Saved.", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added To Bag.", driver);
	
			ShoppingBagPage shoppingBag = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated To Shopping Cart Page", driver);
	
			CheckoutPage checkoutPage = shoppingBag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated To Checkout - Shipping Page", driver);
	
			//Step-2: Verify the functionality of editing the address with checked save this address menu
			checkoutPage.expandCollapseSavedAddressDropdown(true);
			Log.message(i++ + ". Saved Address dropdown expanded.", driver);
			checkoutPage.selectValueFromSavedAddressesDropdownByIndex(1);
			Log.message(i++ + ". Selected Addresss from dropdown.", driver);
			checkoutPage.clickEditShippingAddress();			
			Log.message(i++ + ". Clicked on 'Edit' button in shipping address");
			checkoutPage.fillingShippingDetailsAsSignedInUser("YES", "YES", ShippingMethod.Standard, addressUsed);
			Log.message(i++ + ". Filled in address details!", driver);
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Section!", driver);
			String addressNickNameFromPage = checkoutPage.getSavedAddressNicknameFromShippingDetails();
			checkoutPage.clickOnBrandLogo();
			Log.message(i++ + ". Navigated To Home Page!", driver);
			if (Utils.isMobile()) {
				homePage.headers.openCloseHamburgerMenu("open");
				Log.message(i++ + ". Hamburger Menu Opened!");
			}
			myAccountPage = homePage.headers.navigateToMyAccount();
			Log.message(i++ + ". Navigated to 'My Account' Page");
			AddressesPage addressPage = myAccountPage.clickOnAddressLink();
			Log.message(i++ + ". Navigated to Address Page!", driver);
	
			Log.softAssertThat(addressPage.verifyNickNameIsPresent(addressNickNameFromPage.replaceAll("\\(Default\\)", "")),
					"The entered address should be displayed",
					"The entered address is displayed",
					"The entered address is not displayed", driver);
	
			homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page");
	
			shoppingBag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Clicked on 'Checkout Now' button in shopping bag page");
	
			Log.softAssertThat(checkoutPage.getValueFromSavedAddressDropdown().toUpperCase().contains(addressNickNameFromPage.toUpperCase()),					
					addressNickNameFromPage + " should be displayed in dropdown",
					addressNickNameFromPage + " is displayed in dropdown",
					addressNickNameFromPage + " is not displayed in dropdown", driver);
			
			//Step-3: Verify behavior when the User tries to update this address nickname
			checkoutPage.clickOnBrandLogo();
			Log.message(i++ + ". Navigated To Home Page!", driver);
			if (Utils.isMobile()) {
				homePage.headers.openCloseHamburgerMenu("open");
				Log.message(i++ + ". Hamburger Menu Opened!");
			}
			myAccountPage = homePage.headers.navigateToMyAccount();
			Log.message(i++ + ". Navigated to 'My Account' Page");
			addressPage = myAccountPage.clickOnAddressLink();
			Log.message(i++ + ". Navigated to Address Page!", driver);
			addressPage.clickEditAddress(addressNickNameFromPage.replaceAll("\\(Default\\)", ""));
			Log.message(i++ + ". Clicked Edit Address that matches Nickname(" + addressNickNameFromPage + ")", driver);
			
			addressPage.editNickName(testNickName);
			Log.softAssertThat(addressPage.verifyNickNameIsPresent(testNickName),
					"The System should be allow the User to update to a nickname ",
					"The System allowed the User to update to a nickname ",
					"The System did not allow the User to update to a nickname ", driver);
			
			//Finishing with deleting last added address
			addressPage.deleteAddress(testNickName);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	}// M1_FBB_DROP4_C22491
}// search
