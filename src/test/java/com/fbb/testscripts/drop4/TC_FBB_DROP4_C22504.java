package com.fbb.testscripts.drop4;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.GiftCardPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22504 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22504(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prdExcludeAllShipment = TestData.get("prd_exclude_all_ship");
	
		//Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver).replace("@", "1@");
		String password = accountData.get("password_global");
		
		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
	
			//#item 4 - Product Excluded from all Shipping Methods
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdExcludeAllShipment);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			//Navigate to cart
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			CheckoutPage checkoutPage = (CheckoutPage)cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
	
			//2 - Verify the functionality of No Methods Available State
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("radioExpressCheckout"), checkoutPage),
					"The cart should have only standard delivery, as No shipping method is available for the product",
					"The cart is only having standard delivery, as No shipping method is available for the product",
					"The cart is having otherthan standard delivery shipping option", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C22504
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP4_C22504(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_surcharge");
		String prd_back_order = TestData.get("prd_back-order").split("\\|")[0];
	
		//Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver).replace("@", "2@");
		String password = accountData.get("password_global");
		
		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
	
			//#item 1 - In stock
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			//#item 2 - backordered
			homePage.headers.redirectToPDP(prd_back_order, driver);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			//#item 4 - Surcharge
			GiftCardPage gcPage = homePage.footers.navigateToGiftCardLandingPage();
			Log.message(i++ + ". Navigated to Gift Card landing page.", driver);
			
			pdpPage = gcPage.navigateToGiftCards();
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			//Navigate to cart
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			CheckoutPage checkoutPage = (CheckoutPage)cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
	
			//3 - Verify the functionality of Shipping Method Name
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblDeliveryOptionHeading","shippingMethods", checkoutPage), 
					"Shipping Method Name should be displayed below the Delivery Options Heading.", 
					"Shipping Method Name is displayed below the Delivery Options Heading.",
					"Shipping Method Name is not displayed below the Delivery Options Heading.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "shiipingMethodCost", "shippingMethods", checkoutPage), 
					"Shipping Method Cost should be displayed in black to the right of the Shipping Method Name", 
					"Shipping Method Cost is displayed in black to the right of the Shipping Method Name",
					"Shipping Method Cost is displayed in black to the right of the Shipping Method Name", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("shiipingMethodCost", "rgba(0, 0, 0, 1)", checkoutPage), 
					"Shipping Method Cost should be displayed in black to the right of the Shipping Method Name", 
					"Shipping Method Cost is displayed in black to the right of the Shipping Method Name",
					"Shipping Method Cost is displayed in black to the right of the Shipping Method Name", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblDeliveryOptionHeading","shiipingMethodDeliveryDetail", checkoutPage), 
					"Shipping Method Details should be displayed under the Shipping Promotion Callout..", 
					"Shipping Method Details is displayed under the Shipping Promotion Callout.",
					"Shipping Method Details is not displayed under the Shipping Promotion Callout..", driver);

			//4 - Verify the functionality of Shipping Method Cost
	
			Log.softAssertThat(checkoutPage.verifyShippingMethodCostIsDisplayed(),
					"The Shipping Method cost should be displayed",
					"The Shipping Method cost is displayed",
					"The Shipping Method cost is not displayed", driver);
	
			//5 - Verify the functionality of Shipping Surcharge
	
			Log.softAssertThat(checkoutPage.verifyShippingSurcharge(),
					"The Shipping Surcharge should be displayed",
					"The Shipping Surcharge is displayed",
					"The Shipping Surcharge is not displayed", driver);
	
			//7 - Verify the functionality of Shipping Method Details
	
			Log.softAssertThat(checkoutPage.verifyShippingMethodDetailsIsDisplayed(),
					"The Shipping Method details should be displayed",
					"The Shipping Method details is displayed",
					"The Shipping Method details is not displayed", driver);
	
			//8 - Verify the functionality of Estimated Delivery
			Log.reference("The Estimated Delivery displayed based on Zipcode : PXSFCC-2108");
	
			//10 - Verify the functionality of Shipping Tool Tip
			//Log.failsoft("Tool tip is not getting displayed under Delivery Options section : PXSFCC-1320");
			checkoutPage.clickDeliveryOptionToolTip();
			Log.message(i++ + ". Clicked on Tool Tip icon.", driver);
	
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("divDeliveryOptionsToolTipDialog"), checkoutPage),
					"The Delivery Options tool tip dialog should be displayed",
					"The Delivery Options tool tip dialog is displayed",
					"The Delivery Options tool tip dialog is not displayed", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C22504
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP4_C22504(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver).replace("@", "3@");
		String password = accountData.get("password_global");
		
		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
	
			//#item 1 - Plastic Gift Card
			GiftCardPage gcPage = homePage.footers.navigateToGiftCardLandingPage();
			Log.message(i++ + ". Navigated to Gift Card landing page.", driver);
			
			PdpPage pdpPage = gcPage.navigateToGiftCards();
			Log.message(i++ + ". Navigated to PDP Page", driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			CheckoutPage checkoutPage = (CheckoutPage)cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
	
			Log.softAssertThat(checkoutPage.getNumberOfShippingMethodsAvailable() == 1,
					"The number of availble shipping methods should be 1",
					"The number of availble shipping methods is 1",
					"The number of availble shipping methods is not 1", driver);
	
			Log.softAssertThat(checkoutPage.getShippingMethodsName(0).contains("Standard Delivery:"),
					"Only Standard Delivery should be available for gift cards",
					"Only Standard Delivery is available",
					"Wrong shipping method is displayed", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C22504
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP4_C22504(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
	
		//Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver).replace("@", "4@");
		String password = accountData.get("password_global");
		
		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
	
			//#item 1 - Plastic Gift Card
			GiftCardPage gcPage = homePage.footers.navigateToGiftCardLandingPage();
			Log.message(i++ + ". Navigated to Gift Card landing page.", driver);
			
			PdpPage pdpPage = gcPage.navigateToGiftCards();
			Log.message(i++ + ". Navigated to PDP Page", driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
			
			//#item 1 - In stock
			pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			CheckoutPage checkoutPage = (CheckoutPage)cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
	
			Log.softAssertThat(checkoutPage.verifyShippingMethodExceptionDisplayed("Standard Delivery"),
					"Shipping methods other than Standard Delivery should have exceptions",
					"Shipping methods exception is displayed",
					"Shipping methods exception is not displayed", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C22504
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M5_FBB_DROP4_C22504(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String prd_ship_promo = TestData.get("cpn_freeshipping");
	
		//Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver).replace("@", "5@");
		String password = accountData.get("password_global");
		
		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
	
			//6 - Verify the functionality of Shipping Promotion Callout
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			cartPage.enterPromoCode(prd_ship_promo);
			cartPage.clickOnApplycouponButton();
			Log.message(i++ + ". Shipping Promotion Coupon Applied.", driver);
	
			CheckoutPage checkoutPage = (CheckoutPage)cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("lblRegularShipping_standard_cost", "text-decoration", "line-through", checkoutPage) &&
					checkoutPage.elementLayer.verifyElementColor("lblRegularShipping_discount_cost", "e700", checkoutPage),
					"The Shipping Method promotion should be displayed with strikethrough and in red color",
					"The Shipping Method promotion is displayed",
					"The Shipping Method promotion is not displayed", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("txtPromotionCalloutMessage"), checkoutPage),
					"The promotion callout message should be displayed",
					"The promotion callout message is displayed",
					"The promotion callout message is not displayed", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C22504
}// search
