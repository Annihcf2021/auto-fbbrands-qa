package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22550 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22550(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_monogram = TestData.get("prd_monogram");
		String prd_variation = TestData.get("prd_variation2");
		String rewardCertificate = TestData.get("reward_valid-2");
		String couponCode1 = TestData.get("cpn_freeshipping");
		String couponCode2 = TestData.get("cpn_off_10_percentage");
		String colorCode = TestData.get("orderSummaryColorCode");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String credential = AccountUtils.generateEmail(driver) + "|test123@";
		
		//Pre-requisite - Account Should have more than one payment information
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, credential);
		}
		
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home page.", driver);
	
			homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_variation);
			Log.message(i++ + ". Navigated to PDP for :: " + pdpPage.getProductName());
	
			pdpPage.selectColor();
			pdpPage.selectSize();
			pdpPage.addToBagKeepOverlay();
			Log.message(i++ + ". Product added to Bag.", driver);
	
			pdpPage = homePage.headers.navigateToPDP(prd_monogram);
			Log.message(i++ + ". Navigated to PDP for :: " + pdpPage.getProductName(), driver);
	
			pdpPage.selectColor();
			pdpPage.selectSize();
			boolean orderOptionAvailable = pdpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("chkEnableMonogramming"), pdpPage);
			if(orderOptionAvailable) {
				pdpPage.clickOnMonogrammingCheckbox("enable");
				pdpPage.selectMonogrammingFontValue(1);
				pdpPage.selectMonogrammingColorValue(1);
				pdpPage.selectMonogrammingLocationValue(1);
				pdpPage.enterTextInMonogramFirstTextBox("TextEnter");
			}else {
				Log.reference("Monogramming currently out of scope.");
			}
			
			pdpPage.selectQty("6");
			pdpPage.addToBagKeepOverlay();
			Log.message(i++ + ". Product added to Bag.", driver);
	
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			CheckoutPage checkoutPage = (CheckoutPage) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
			/*Object[]obj = GlobalNavigation.addProduct_Checkout(driver, items_to_cart, i, credentials);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (Integer) obj[1];*/
	
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", ShippingMethod.Standard, "valid_address7", "Gift Message Goes Here.");
			Log.message(i++ + ". Shipping details filled successfully.", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continuing to Payment Section.", driver);
	
			checkoutPage.applyGiftCardByValue(TestGiftCardValue.valid);
			Log.message(i++ + ". Gift card applied.", driver);
			
			if(!BrandUtils.isBrand(Brand.el)){
			checkoutPage.applyRewardCertificate(rewardCertificate);
			Log.message(i++ + ". Reward Certificate Applied.", driver);
			}
			
			checkoutPage.applyPromoCouponCode(couponCode1);
			Log.message(i++ + ". Coupon-1 Applied.", driver);
	
			checkoutPage.applyPromoCouponCode(couponCode2);
			Log.message(i++ + ". Coupon-2 Applied.", driver);
			
			checkoutPage.fillingCardDetails1("card_Visa1", false, false);
			Log.message(i++ + ". Card Details filled successfully.", driver);
	
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Navigated to Order Review Page.", driver);
			
			//Step-1: Verify the components available in the Order Summary
				
			
			cartPage = checkoutPage.clickEditItemSummary();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
			
			//Step-1: Verify the components available in the Order Summary
			if(BrandUtils.isBrand(Brand.el)){
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("divOrderSubTotal", "orderSummaryDiscount", "orderSummaryShippingDetail", "orderSummaryShippingDiscountDetail", "orderSummarySalesTax", "orderSummaryOrderTotal", "orderSummaryGiftCardDiscount", "orderSummaryRemainingTotal", "orderSummaryTotalSavings"), cartPage),
					"SubTotal, Discount, Shipping Cost, Sales Tax, Order total, Gift card applied, Remaining total, total savings, checkout now button should be displayed.",
					"All above items displayed as expected.",
					"Not all above mentioned items displayed.", driver);
					
			}else{
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("divOrderSubTotal", "orderSummaryDiscount", "orderSummaryShippingDetail", "orderSummaryShippingDiscountDetail", "orderSummarySalesTax", "orderSummaryOrderTotal", "orderSummaryRewardDiscount", "orderSummaryGiftCardDiscount", "orderSummaryRemainingTotal", "orderSummaryTotalSavings"), cartPage),
					"SubTotal, Discount, Shipping Cost, Sales Tax, Order total, Gift card applied, Rewards Applied, Remaining total, total savings, checkout now button should be displayed.",
					"All above items displayed as expected.",
					"Not all above mentioned items displayed.", driver);
			
			}
			//Step-2: Verify the display of Subtotal
			Log.softAssertThat(cartPage.getClassNamesFromOrderTotalsListByIndex(1).contains("order-subtotal"),
					"The Subtotal field should display on the first line in the Cost Summary",
					"Subtotal displayed on First Line.",
					"Subtotal not displayed on First Line.", driver);
	
			//Step-3: Verify the display of Discount

			
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divOrderSubTotal", "orderSummaryDiscount", cartPage) &&
					cartPage.elementLayer.verifyElementColor("orderSummaryDiscount", colorCode, cartPage),
					"System should display discount below the Subtotal & Should be displayed in red font color",
					"Discount displayed below Subtotal in red code font.",
					"Discount not displayed as expected.", driver);
	
			//Option cost is out of scope
			/*//Step-4: Verify the display of Option Cost
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryDiscount", "orderSummaryOptionCost", cartPage),
					"System should display Option Cost below the Discount field",
					"Option cost displayed below Discount field.",
					"Option cost not displayed as expected.", driver);*/
	
			//Step-5: Verify the display of Shipping Cost
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryDiscount", "orderSummaryShippingDetail", cartPage),
					"System should display Shipping Cost below the Discount",
					"Shipping cost should be displayed below Discount.",
					"Shipping cost not displayed as expected.", driver);
	
			//Step-6: Verify the display of Shipping message
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryDiscount", "divShippingMessage", cartPage),
					"System should display Shipping message below the Discount field",
					"Shipping message displayed below Discount.",
					"Shipping message not displayed as expected.", driver);
	
			//Step-7: Verify the display of Shipping Cost Tool Tip
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("spanCostToolTip"), cartPage),
					"The Shipping Cost Tool Tip should appear at the end of the Shipping message",
					"Shipping cost tool tip displayed.",
					"Shipping cost tool tip not displayed.", driver);
	
			//Step-8: Verify the display of Shipping Discount
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divShippingMessage", "orderSummaryShippingDiscountDetail", cartPage) &&
					cartPage.elementLayer.verifyElementColor("orderSummaryShippingDiscountDetail", colorCode, cartPage),
					"System should display the Shipping discount below the Shipping Message field & Field should be displayed in red font color",
					"Shipping discount displayed below shipping message field and displayed in red font.",
					"Shipping discount not displayed as expected.", driver);
	
			//Step-9: Verify the display of Sales Tax
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryShippingDiscountDetail", "orderSummarySalesTax", cartPage),
					"The Sales Tax should appear below the Shipping Discount field",
					"Sales Tax displayed below shipping discount.",
					"Sales Tax not displayed as expected", driver);
	
			//Step-10: Verify the display of Order Total
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummarySalesTax", "orderSummaryOrderTotal", cartPage),
					"System should display the Order Total below the Sales Tax field",
					"Order total displayed below Sales Tax.",
					"Order total not displayed as expected.", driver);
	
			//Step-11: Verify the display of Gift Cards Applied field
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryOrderTotal", "orderSummaryGiftCardDiscount", cartPage) &&
					cartPage.elementLayer.verifyElementColor("orderSummaryGiftCardDiscount", colorCode, cartPage),
					"System should display the Gift Cards Applied field below the Order Total field & Should be displayed in red font color",
					"Gift cards applies displayed below order total field and in red color font.",
					"Gift cards not displayed as expected.", driver);
			if(BrandUtils.isBrand(Brand.el)){
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryGiftCardDiscount", "orderSummaryRemainingTotal", cartPage) &&
					cartPage.elementLayer.verifyCssPropertyForElement("orderSummaryRemainingTotal", "border-top", "1px solid", cartPage),
					"The Remaining Total field should be present below the Gift card Applied field, after a faint line break",
					"Remaining total field displayed below Gift card applied after a faint line break.",
					"Remaining total not displayed as expected.", driver);
					
			}else{
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryGiftCardDiscount", "orderSummaryRewardDiscount", cartPage) &&
					cartPage.elementLayer.verifyElementColor("orderSummaryRewardDiscount", colorCode, cartPage),
					"System should display the Rewards Applied field below the Gift Cards Applied field & Should be displayed in red font color",
					"Rewards applied displayed below gift cards applied.",
					"Rewards applied not displayed as expected.", driver);
	
			//Step-13: Verify the display of Remaining Total field
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryRewardDiscount", "orderSummaryRemainingTotal", cartPage) &&
					cartPage.elementLayer.verifyCssPropertyForElement("orderSummaryRemainingTotal", "border-top", "1px solid", cartPage),
					"The Remaining Total field should be present below the Rewards Applied field, after a faint line break",
					"Remaining total field displayed below rewards applied after a faint line break.",
					"Remaining total not displayed as expected.", driver);
			}
			//Step-14: Verify the display of Total Savings
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryRemainingTotal", "orderSummaryTotalSavings", cartPage) &&
					cartPage.elementLayer.verifyElementColor("orderSummaryTotalSavings", colorCode, cartPage),
					"System should display the Total Savings below the Remaining Total (or Order Total) field",
					"Total saving displayed below remaining total.",
					"Total saving not displayed as expected", driver);
	
			//Step-15: Verify the display of 'CHECKOUT NOW' button
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryTotalSavings", "btnCheckoutNowFooter", cartPage),
					"The 'CHECKOUT NOW' button should be displayed below the Total Savings field",
					"Checkout now button displayed below total savings",
					"Checkout now button not displayed as expected.", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP_C21550
}// search
