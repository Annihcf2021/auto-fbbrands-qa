package com.fbb.testscripts.drop4;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22493 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(enabled = false, groups = { "low", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22493(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_po-box-restricted") + "|" + TestData.get("prd_po-box-restricted-1") + "|" + TestData.get("prd_po-box-unrestricted");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, searchKey, i, AccountUtils.generateEmail(driver));
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (Integer) obj[1];
	
			checkoutPage.enterShippingAddress1("Po Box 304");
	
			//checkoutPage.enterShippingAddress2("Add2");
	
			//Step 1
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtPoBoxBlockOverlayHeading","txtPoBoxBlockOverlayIntro","poBoxBlockOverlayItemsSection",
					"scrPoBoxBlockOverlayScrollBar","btnPoBoxBlockOverlayRemoveItems","btnPoBoxBlockOverlayEditShipAdd"), checkoutPage),
					"To check fields of the 'PO Box Exception' overlay.",
					"The 'PO Box Exception' overlay should have 'PO Box Heading', 'Item Introduction Text', 'Item Information', 'Remove Items', 'Return to Cart Action Button'"
							+ "'Edit Shipping Address Action Button' and 'Scroll Bar' are present in the 'Have a Gift card' section!",
							"Some fields are not present in the 'PO Box Exception' overlay", driver);
	
			//Step 2a
			Log.softAssertThat(checkoutPage.verifyHeadingisAboveTheIntroInPoBoxExcludeOverlay(),
					"To check heading of 'PO Box Exception' overlay.",
					"The heading should be displayed on the top of the overlay and color should be grey",
					"The heading is not displaying on the top or the color is not grey", driver);
	
			//Step 2b			
			Log.softAssertThat(checkoutPage.verifyHeadingTextComingFromPropertyInPoBoxOverlay(),
					"To check heading message is displaying correctly in 'PO Box Exception' overlay.",
					"The heading message is displaying correctly from property file",
					"The heading message is not displaying correctly from property file", driver);
	
			//Step 3a
			Log.softAssertThat(checkoutPage.verifyHeadingisAboveTheIntroInPoBoxExcludeOverlay(),
					"To check item introduction text in 'PO Box Exception' overlay.",
					"The item introduction text is displaying below the heading of the overlay",
					"The item introduction text is not displaying below the heading of the overlay", driver);
	
			//Step 3b
			Log.softAssertThat(checkoutPage.verifyItemIntroTextComingFromPropertyInPoBoxOverlay(),
					"To check item introduction text is displaying correctly in 'PO Box Exception' overlay.",
					"The item introduction text is displaying correctly from property file",
					"The item introduction text is not displaying correctly from property file", driver);
	
			//Step 4a
			Log.softAssertThat(checkoutPage.verifyIntroIsAboveItemsSectionInPoBoxExcludeOverlay(),
					"To check item information is displaing below introduction text in 'PO Box Exception' overlay.",
					"The item information is displaing below introduction text in 'PO Box Exception' overlay",
					"The item information is not displaing below introduction text in 'PO Box Exception' overlay", driver);
	
			//Step 4b
			if(!(Utils.isMobile())){
				Log.softAssertThat(checkoutPage.verifyItemsSectionProductsAlignmentInPoBoxExcludeOverlay(),
						"To check item information have two items displaying in a row.",
						"The item information have two items displaying in a row",
						"The item information is not having two items displaying in a row", driver);
			} else {
				Log.softAssertThat(checkoutPage.verifyItemsSectionProductsAlignmentInPoBoxExcludeOverlay(),
						"To check item information have one item displaying in a row.",
						"The item information have one item displaying in a row",
						"The item information is not having one item displaying in a row", driver);
			}
	
			//Step 4c
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("mainImageInPoBoxOverlay","brandImageInPoBoxOverlay","productNameInPoBoxOverlay",
					"productShoeSizeInPoBoxOverlay","productColorInPoBoxOverlay","productPriceInPoBoxOverlay"), checkoutPage),
					"To check item information components in 'PO Box Exception' overlay.",
					"The item information components are displaying correctly in 'PO Box Exception' overlay",
					"Some item information components are not displaying correctly in 'PO Box Exception' overlay", driver);
	
			//Step 5a
			Log.softAssertThat(checkoutPage.verifyItemsSectionIsAboveRemoveBtnInPoBoxExcludeOverlay(),
					"To check remove items button in 'PO Box Exception' overlay.",
					"The remove items button is displaying below the item information of the overlay",
					"The remove items button is not displaying below the item information of the overlay", driver);
	
			//Step 5b
			if(!(Utils.isMobile())){
				Log.softAssertThat(checkoutPage.verifyPositionOfRemoveAndEditBtnInPoBoxOverlay(),
						"To check remove items button is indented to the left in 'PO Box Exception' overlay.",
						"The remove items button is indented to the left",
						"The remove items button is not indented to the left", driver);
			}
	
			//Step 6a
			if(!(Utils.isMobile())){
				Log.softAssertThat(checkoutPage.verifyPositionOfRemoveAndEditBtnInPoBoxOverlay(),
						"To check Edit Your Shipping Address button is displaying right side to remove items button.",
						"The Edit Your Shipping Address button is displaying right side to remove items button",
						"The Edit Your Shipping Address button is not displaying right side to remove items button", driver);
			} else {
				Log.softAssertThat(checkoutPage.verifyPositionOfRemoveAndEditBtnInPoBoxOverlay(),
						"To check Edit Your Shipping Address button is displaying below to remove items button.",
						"The Edit Your Shipping Address button is displaying below to remove items button",
						"The Edit Your Shipping Address button is not displaying below to remove items button", driver);
			}
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// TC_FBB_DROP4_C22493
}// search
