package com.fbb.testscripts.drop4;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C21643 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C21643(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Approach Discount Message test
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any); //Add Item To cart with the Quantity of 20
			Log.message(i++ + ". Navigated to Pdp Page with the product!"+ prd_any, driver);

			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			if(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("divDiscountApproach"), cartPage)) {
				if(Utils.isMobile()) {
					Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnCheckoutNow", "divDiscountApproach", cartPage)
							&& cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divDiscountApproach", "divCartColumnHeadersMobile", cartPage), 
					"System should display the alert message between the cart level messages and the PLCC Recommendation module", 
					"System display the alert message between the cart level messages and the PLCC Recommendation module", 
					"System not display the alert message between the cart level messages and the PLCC Recommendation module", driver);
				}else {
					Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnCheckoutNow", "divDiscountApproach", cartPage)
							&& cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divDiscountApproach", "divCartColumnHeaders", cartPage), 
					"System should display the alert message between the cart level messages and the PLCC Recommendation module", 
					"System display the alert message between the cart level messages and the PLCC Recommendation module", 
					"System not display the alert message between the cart level messages and the PLCC Recommendation module", driver);
				}
			}else
				Log.reference("Discount Approiaching is currently not configured for " + Utils.getCurrentBrand().getConfiguration());
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP_C21643
}// search
