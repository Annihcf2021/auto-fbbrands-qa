package com.fbb.testscripts.drop4;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C19713 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C19713(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String username;
		String inValidusername = "sadf!";
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_any, i, username);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
	
			checkoutPage.clickOnEditEmailLnk();
			Log.message(i++ + ". Clicked on Edit Link in Email Section", driver);
	
			//Step-1: Verify the functionality of Checkout as a Guest in the Checkout as a Guest Section of Checkout page
			if(Utils.isMobile())
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "signInLoginBox", "lblCheckoutAsGuest",checkoutPage), 
						"For mobile device , 'Checkout as a Guest' heading should be displayed below the 'Sign in ' section.",
						"For mobile device , 'Checkout as a Guest' heading is displayed below the 'Sign in ' section.", 
						"For mobile device , 'Checkout as a Guest' heading is not displayed below the 'Sign in ' section.", driver);
			}
			else
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lblCheckoutAsGuest", "signInLoginBox", checkoutPage), 
						"Checkout section should be right of the Sign in module in the checkout page",
						"Checkout section to the right of the Sign in module in the checkout page",
						"Checkout section is not to the right of the Sign in module in the checkout page", driver);
			}
	
			//Step-2: Verify the functionality of Sub-Copy message element in the Checkout as a Guest Section of Checkout page
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblCheckoutAsGuest", "lblSubCopyMsgInCheckoutAsGuest",checkoutPage), 
					"Sub-Copy text should be displayed below the Guest Checkout heading.",
					"Sub-Copy text is displayed below the Guest Checkout heading.", 
					"Sub-Copy text is not displayed below the Guest Checkout heading.", driver);
	
			//Step-3: Verify the Email Address field in the Checkout as a Guest Section of Checkout page
			checkoutPage.enterGuestUserEmail(username);
			Log.message(i++ + ". Entered Email Address in Guest Email Address.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementTextEqualTo("txtGuestEmail", username, checkoutPage),
					"The field should allow the User to enter the email address to be used for the current Order",
					"The field allows the User to enter the email address to be used for the current Order",
					"The field not allow the User to enter the email address to be used for the current Order", driver);
	
			checkoutPage.enterInvalidGuestUserEmail(inValidusername);
			Log.message(i++ + ". Entered Email Address in Guest Email Address.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("lblGuestEmail", "class", "input-focus", checkoutPage),
					"The placeholder text should move above when the User starts typing the value in this field",
					"The placeholder text moved above when the User starts typing the value in this field",
					"The placeholder text not move above when the User starts typing the value in this field", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("txtGuestEmail", "class", "require", checkoutPage),
					"This is a mandatory field, so it should not be left blank",
					"This is a mandatory field, so it cannot be left blank",
					"This is a mandatory field, even though It can be left blank", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lblGuestEmailError"), checkoutPage),
					"The System should display an appropriate error message when an invalid value is entered for the email address field",
					"The System displays an appropriate error message when an invalid value is entered for the email address field",
					"The System not display an appropriate error message when an invalid value is entered for the email address field", driver);
	
			//Step-6: Verify the functionality of Checkout Steps
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblCheckoutAsGuest", "lblShippingTab",checkoutPage), 
					"Checkout Steps should be displayed below the 'sign in to your account' & checkout as guest section.",
					"Checkout Steps should be displayed below the 'sign in to your account' & checkout as guest section.", 
					"Checkout Steps should be displayed below the 'sign in to your account' & checkout as guest section.", driver);
	
	
			//Step-4: Verify the functionality of Newsletter Signup check box in the Checkout as a Guest Section of Checkout page
			//Cannot be automated due to e-mail validations
	
			checkoutPage.checkNewsLetterSignUp("check");
			Log.message(i++ + ". NewsLetter checkbox checked!", driver);
	
			checkoutPage.enterGuestUserEmail(username);
			Log.message(i++ + ". Entered Email Address in Guest Email Address.", driver);
	
			checkoutPage.continueToShipping();
			Log.message(i++ + ". Clicked on continue in Guest Login.", driver);
	
			//Step-5: Verify the functionality of Continue button in the Checkout as a Guest Section of Checkout page
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("shippingSection"), checkoutPage),
					"With valid email id and Newsletter checkbox checked & click on continue button, user should be navigated to shipping section.",
					"With valid email id and Newsletter checkbox checked & click on continue button, user navigated to shipping section.",
					"With valid email id and Newsletter checkbox checked & click on continue button, user not navigated to shipping section.", driver);
	
			checkoutPage.clickOnEditEmailLnk();
			Log.message(i++ + ". Clicked on Edit in Login As Section.", driver);
	
			checkoutPage.checkNewsLetterSignUp("uncheck");
			Log.message(i++ + ". NewsLetter checkbox unchecked!", driver);
	
			checkoutPage.continueToShipping(username);
			Log.message(i++ + ". Continued with Credentials(" + username + ")", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("shippingSection"), checkoutPage),
					"With valid email id and Newsletter checkbox unchecked & click on continue button, user should be navigated to shipping section.",
					"With valid email id and Newsletter checkbox unchecked & click on continue button, user navigated to shipping section.",
					"With valid email id and Newsletter checkbox unchecked & click on continue button, user not navigated to shipping section.", driver);
	
			//When the User enters an invalid email ID, then it should display an appropriate error message in red font color
			//Already verified
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementTextEqualTo("headerCustomerSignInSection", "Guest Checkout as", checkoutPage),
					"The System should replace the Checkout Login section with the 'Guest Checkout as' section with the email address value entered by the User",
					"The System replaced the Checkout Login section with the 'Guest Checkout as' section with the email address value entered by the User",
					"The System not replaced the Checkout Login section with the 'Guest Checkout as' section with the email address value entered by the User", driver);
	
	
			//Step-7: Verify the functionality when user click/tap on Items in Shopping Bag
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("lstItemsInCartList"), checkoutPage),
					"System should display items from shopping bag",
					"System displays items from shopping bag",
					"System not display items from shopping bag", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblShippingTab", "ItemsInCartList",checkoutPage), 
					"Checkout Steps should be displayed below the 'sign in to your account' & checkout as guest section.",
					"Checkout Steps should be displayed below the 'sign in to your account' & checkout as guest section.", 
					"Checkout Steps should be displayed below the 'sign in to your account' & checkout as guest section.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkItemsInBagEdit", "shoppingBagHeading", checkoutPage), 
					"Edit option should be displayed right hand-side of the Items in Shopping Bag section.",
					"Edit option is displayed right hand-side of the Items in Shopping Bag section.",
					"Edit option is not displayed right hand-side of the Items in Shopping Bag section", driver);
	
			ShoppingBagPage cartPage = checkoutPage.clickItemsInBagEdit();
			Log.message(i++ + ". Clicked on Edit link.", driver);
			//Step-8: System should navigate back to the Shopping Cart page when the User clicks/taps the Edit link
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("miniCartContent"), cartPage),
					"System should navigate back to the Shopping Cart page when the User clicks/taps the Edit link",
					"System navigated back to the Shopping Cart page when the User clicks/taps the Edit link",
					"System not navigate back to the Shopping Cart page when the User clicks/taps the Edit link", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C19713
}// search
