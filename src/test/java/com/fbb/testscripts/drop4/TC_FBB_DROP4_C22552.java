package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22552 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22552(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String plccPreapproved = accountData.get("plcc_preapproved_" + Utils.getCurrentBrandShort().substring(0, 2));
		String plccEmail = plccPreapproved.split("\\|")[0];
		String password = plccPreapproved.split("\\|")[1];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			homePage.headers.navigateToMyAccount(plccEmail, password);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + prd_any, driver);
	
			pdpPage.addToBagCloseOverlay();		
			Log.message(i++ + ". Product Added to Bag.", driver);
	
			ShoppingBagPage shoppingbagPage= homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			/*if(Utils.getRunPlatForm().equalsIgnoreCase("desktop"))
			{
				shoppingbagPage.clickplccExpand();
				Log.message(i++ + ". PLCC Content Slot expanded!", driver);
			}*/
	
			//Step-1:Verify the functionality of the 'LEARN MORE' link in the PLCC content section
			shoppingbagPage.clickOnLearnmorelinkPlcc();
			Utils.waitForPageLoad(driver);
			Log.softAssertThat(shoppingbagPage.elementLayer.verifyPageElements(Arrays.asList("divplccLearnmorePage"), shoppingbagPage),					 
					"The 'LEARN MORE' link should take the User to a PLCC content ", 
					"The 'LEARN MORE' link took the User to a PLCC content ", 
					"The 'LEARN MORE' link should did not take the User to a PLCC content ", driver);
			
			Log.reference("'Learn More' covered in details in Drop 5.");
			driver.navigate().back();
			
			//Step-2: Verify the functionality of the expand/collapse arrow in the PLCC content section (Desktop only step)
			if(Utils.isDesktop()) {
				//Expanded state
				Log.softAssertThat(shoppingbagPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "headingPLCCcontentslot", "logoPlccBrand", shoppingbagPage), 
						"An image of the brand logo should be present to the left when expanded", 
						"An image of the brand logo is present to the left", 
						"An image of the brand logo is not present to the left", driver);
				
				Log.softAssertThat(shoppingbagPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("creditlimitPlcc"), shoppingbagPage), 
						"Credit limit should be included with the PLCC message", 
						"Credit limit is included with the PLCC message", 
						"Credit limit is not included with the PLCC message", driver);
				
				Log.softAssertThat(shoppingbagPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "textPLCCcontentslotExpanded", "learnmorelinkPlcc", shoppingbagPage), 
						"The 'LEARN MORE' link should be present beneath the PLCC text", 
						"The 'LEARN MORE' link is present beneath the PLCC text", 
						"The 'LEARN MORE' link is not present beneath the PLCC text", driver);
				
				Log.softAssertThat(shoppingbagPage.elementLayer.verifyCssPropertyForElement("expandclosePLCCcontentslot", "background-image", "x-icon", shoppingbagPage), 
						"There should be a cross icon for the collapse button.", 
						"There is a cross icon for the collapse button.", 
						"There is no cross icon for the collapse button.", driver);
				
				
				shoppingbagPage.clickplccExpand();
				Log.message(i++ + ". PLCC Content Slot collapsed!", driver);
				//Collapsed state
				Log.softAssertThat(shoppingbagPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "headingPLCCcontentslot", "logoPlccBrand", shoppingbagPage), 
						"An image of the brand logo should be present to the left when collapsed", 
						"An image of the brand logo is present to the left", 
						"An image of the brand logo is not present to the left", driver);
				
				Log.softAssertThat(shoppingbagPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("textPLCCcontentslot"), shoppingbagPage), 
						"A configurable text should inform the User about the pre-approved offer for the PLCC", 
						"A configurable text informs the User about the pre-approved offer for the PLCC", 
						"A configurable text does not inform the User about the pre-approved offer for the PLCC", driver);
				
				Log.softAssertThat(shoppingbagPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "textPLCCcontentslot", "learnmorelinkPlcc", shoppingbagPage), 
						"The 'LEARN MORE' link should be present beneath the PLCC text", 
						"The 'LEARN MORE' link is present beneath the PLCC text", 
						"The 'LEARN MORE' link is not present beneath the PLCC text", driver);
				
				Log.softAssertThat(shoppingbagPage.elementLayer.verifyCssPropertyForElement("expandclosePLCCcontentslot", "background-image", "arrow-down", shoppingbagPage), 
						"There should be a expand arrow in the same place as the collapse button.", 
						"There is a expand arrow in the same place as the collapse button.", 
						"There is no expand arrow in the same place as the collapse button.", driver);
			}
			
			Log.testCaseResult();

	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	
	}
}// search
