package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22761 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22761(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_gift_certificate = TestData.get("prd_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		String credential = AccountUtils.generateEmail(driver) + "|test123@";
		//Pre-requisite - Account Should have more than one payment information
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, credential);
		}
	
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.checkout(driver, prd_gift_certificate, i, credential);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "NO", ShippingMethod.Standard, "valid_address7");
			checkoutPage.continueToPaymentWithoutClickingAVS();
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("mdlAddressSuggestion"), checkoutPage), 
					"Address suggestion modal should display after filling the shipping address.", 
					"Address suggestion modal should display after filling the shipping address.", 
					"Address suggestion modal should display after filling the shipping address.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "mdlAddressSuggestionHeading", "mdlAddressSuggestionInformation", checkoutPage),
					"The Message Heading element should be present at the top of the modal in a grayed section box.",
					"The Message Heading element is present at the top of the modal in a grayed section box.",
					"The Message Heading element is not present at the top of the modal in a grayed section box.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "mdlAddressSuggestionHeading", "addressSugRadioButton", checkoutPage),
					"The Address Suggestion radio button should appear below the Message Heading element.",
					"The Address Suggestion radio button is appear below the Message Heading element.",
					"The Address Suggestion radio button is not appear below the Message Heading element.", driver);
			
			Log.softAssertThat(checkoutPage.getAVSHeadingforRadioButton().equalsIgnoreCase("Please select the best match below:"), 
					"The Address Suggestion should appear below a text message saying 'Please select the best match below:",
					"The Address Suggestion is appear below a text message saying 'Please select the best match below: ",
					"The Address Suggestion is not appear below a text message saying 'Please select the best match below:' ", driver);
			
			if(Utils.isMobile())
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "cancelAvsButton", "btnContinueInAddressSuggestionModal", checkoutPage),
						"The 'Cancel' link should be present at the bottom of the modal, to the left of the 'Continue' button.", 
						"The 'Cancel' link is present at the bottom of the modal, to the left of the 'Continue' button.",
						"The 'Cancel' link is not present at the bottom of the modal, to the left of the 'Continue' button.", driver);
			}
			else
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnContinueInAddressSuggestionModal", "cancelAvsButton", checkoutPage),
						"The 'Cancel' link should be present at the bottom of the modal, to the left of the 'Continue' button.", 
						"The 'Cancel' link is present at the bottom of the modal, to the left of the 'Continue' button.",
						"The 'Cancel' link is not present at the bottom of the modal, to the left of the 'Continue' button.", driver);
			}
			if(Utils.isMobile())
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "cancelAvsButton", "btnContinueInAddressSuggestionModal", checkoutPage),
						" 'Continue' button should be present to the right of the 'Cancel' link.", 
						" 'Continue' button is present to the right of the 'Cancel' link.",
						" 'Continue' button is not present to the right of the 'Cancel' link.", driver);
			}
			else
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnContinueInAddressSuggestionModal", "cancelAvsButton", checkoutPage),
						" 'Continue' button should be present to the right of the 'Cancel' link.", 
						" 'Continue' button is present to the right of the 'Cancel' link.",
						" 'Continue' button is not present to the right of the 'Cancel' link.", driver);
			}
			Log.softAssertThat(checkoutPage.getSaleTax().equals("-") || checkoutPage.getSaleTax().contains("0.00"), 
					"Tax values should not calculated when confirming the address.", 
					"Tax values is not calculated when confirming the address.", 
					"Tax values is calculated when confirming the address.", driver);
			
			checkoutPage.clickCancelAvsButton();
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtFirstnameShipping"), checkoutPage), 
					"Cancel Button should clicked successfully and go back to the shipping address section.", 
					"Cancel Button is clicked successfully and go back to the shipping address section.", 
					"Cancel Button is not clicked successfully and go back to the shipping address section.", driver);
			BrowserActions.refreshPage(driver);
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "NO", ShippingMethod.Standard, "taxerror_address7");
			
			checkoutPage.continueToPaymentWithoutClickingAVS();
			
			checkoutPage.clickCorrectAddress();
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("taxErrorMessage"),checkoutPage), 
					"Tax Error Message is displayed when giving the invalid address. ",
					"Tax Error Message is displayed when giving the invalid address.",
					"Tax Error Message is displayed when giving the invalid address.", driver);
			BrowserActions.refreshPage(driver);
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "NO", ShippingMethod.Standard, "valid_address7");
			
			checkoutPage.continueToPaymentWithoutClickingAVS();
			
			checkoutPage.clickAvsContinueButton();
			
			Log.softAssertThat(!(checkoutPage.getSaleTax().equals("-")|| checkoutPage.getSaleTax().contains("0.00")), 
					"Tax values should calculated when confirming the address.", 
					"Tax values is calculated when confirming the address.", 
					"Tax values is not calculated when confirming the address.", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C19710
}// search
