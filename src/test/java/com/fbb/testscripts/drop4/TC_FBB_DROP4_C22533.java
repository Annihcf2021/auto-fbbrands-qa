package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22533 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22533(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey_MarkDownPrice = TestData.get("prd_markdown-single-price");
		String searchKey_SinglePrice = TestData.get("prd_regular_single_price");
		String searchKey_Brand = TestData.get("prd_all_brands");
		String colorCode = TestData.get("SalePriceColorRGB");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String initialQuantity = "1";
		
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		
		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey_MarkDownPrice); //Add Item To cart with the Quantity of 20
			Log.message(i++ + ". Navigated to Pdp Page with the product! "+  searchKey_MarkDownPrice, driver);
	
			String productPrice = pdpPage.getProductPrice();
	                  
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". product added to cart", driver);
			
			String productColor = pdpPage.getSelectedColor();
			Log.message(i++ + ". Selected color: " + productColor, driver);
			
			String productSize = pdpPage.getSelectedSize();
			Log.message(i++ + ". Selected size: " + productSize, driver);
				
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			//Step-1: Verify the functionality of Item Image in the Cart
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayedleft("miniCartContent", "imgProductSection", cartPage), 
					"The Product image should display in the left side", 
					"The Product image is displaying left side!", 
					"The Product image is not displaying left side!", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("divDecreaseArrow"), cartPage),
					"The Decremental arrow should be disabled when quantity reached lower limit ", 
					"The Decremental arrow id disabled when quantity reached lower limit!", 
					"The Decremental arrow is not disabled when quantity reached lower limit!", driver);
	
			Log.softAssertThat(cartPage.verifyProductColor(productColor) , 
					"The Product should be displayed with selected color ", 
					"The Product displayed with selected color as expected!!", 
					"The Product notdisplayed with selected color !!", driver);
	
			cartPage.clickonImageInCart();
	
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("cntPdpContent"), pdpPage),
					"The Page should be redirect to Pdp Page when click the product Image ", 
					"The Page is redirected to Pdp Page  as expected!!", 
					"The Page is not redirected to Pdp Page !!", driver);
	
			//Step-2: Verify the functionality of Add to Cart Brand in the Cart
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			//SM-3841 - Brand logos is removed - Change request
			/*Log.softAssertThat(cartPage.elementLayer.VerifyElementDisplayed(Arrays.asList("imgBrandLogo"), cartPage), 
					"The Product brand image should display", 
					"The Product brand image is displaying!", 
					"The Product brand image is not displaying!", driver);*/
	
			//Step-3: Verify the functionality of Item Name
			cartPage.clickonProductNameInCart();
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("cntPdpContent"), pdpPage),
					"The Page should be redirect to Pdp Page when click the Product Name ", 
					"The Page is redirected to Pdp Page as expected!!", 
					"The Page is not redirected to Pdp Page !!", driver);
	
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productNameSection", "imgProductSection", cartPage),
					"Check the product name is in right to the product image", 
					"The product name is in right to the product image", 
					"The product name is not displaying right to the product image", driver);
	
			//Step-4: Verify the display of Product ID
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productNameSection", "txtProductSetID", cartPage),
					"Check the product name is above the product Id", 
					"The product name is above the product Id", 
					"The product name is not displaying above the product Id", driver);
	
			//Step-5: Verify the functionality of Variation Selections
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductSetID", "productColor", cartPage)
					&&cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductSetID", "productAvailability", cartPage),
					"Variation Selections such as  'COLOR' and 'Availability' should be displayed below the Product ID",
					"Variation Selections such as  'COLOR' and 'Availability' are displayed below the Product ID",
					"Variation Selections such as  'COLOR' and 'Availability' are not displayed below the Product ID",driver);
			
			if(cartPage.productIsShoeType()){
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductSetID", "productShoeWidth", cartPage)
							&& cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductSetID", "productShoeSize", cartPage),
					"Variation Selections such as 'Shoe Width' and 'Shoe SIZE' should be displayed below the Product ID", 
					"Variation Selections such as 'Shoe Width' and 'Shoe SIZE' are displayed below the Product ID", 
					"Variation Selections such as 'Shoe Width' and 'Shoe SIZE' are not displayed below the Product ID", driver);
			}else{
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductSetID", "productSize", cartPage), 
					"Variation Selections such as 'SIZE' should be displayed below the Product ID", 
					"Variation Selections such as 'SIZE' is displayed below the Product ID", 
					"Variation Selections such as 'SIZE' is not displayed below the Product ID", driver);
			}
			
			Log.softAssertThat(cartPage.verifyProductPrice(productPrice), 
					"The product Price should be display only 1 product cost ", 
					"The product Price displayed only 1 product cost as expected!!", 
					"The product Price not displayed only 1 product cost !!", driver);
	
			Log.softAssertThat(cartPage.elementLayer.verifyTxtElementsNotEmpty(Arrays.asList("productColor"), cartPage),
					"Check the product color is displaying in the cart page", 
					"The product color is displaying in the cart page", 
					"The product color is not displaying in the cart page", driver);
			
			if(cartPage.productIsShoeType()){
				Log.softAssertThat(cartPage.elementLayer.verifyTxtElementsNotEmpty(Arrays.asList("productShoeWidth"), cartPage),
						"Check the product Shoe Width is displaying in the cart page", 
						"The product Shoe Width is displaying in the cart page", 
						"The product Shoe Width is not displaying in the cart page", driver);
			
				Log.softAssertThat(cartPage.elementLayer.verifyTxtElementsNotEmpty(Arrays.asList("productShoeSize"), cartPage),
						"Check the product size is displaying in the cart page", 
						"The product size is displaying in the cart page", 
						"The product size is not displaying in the cart page", driver);
			}else{
				Log.softAssertThat(cartPage.elementLayer.verifyTxtElementsNotEmpty(Arrays.asList("productSize"), cartPage),
						"Check the product size is displaying in the cart page", 
						"The product size is displaying in the cart page", 
						"The product size is not displaying in the cart page", driver);
			}			
	
			//Step-6: Verify the functionality of Availability Messaging
			Log.softAssertThat(cartPage.elementLayer.verifyElementTextContains("productAvailability", "In Stock", cartPage) ||
					(cartPage.elementLayer.verifyElementTextContains("productAvailability", "Only", cartPage) && 
							cartPage.elementLayer.verifyElementTextContains("productAvailability", "Left", cartPage)) ||
					cartPage.elementLayer.verifyElementTextContains("productAvailability", "Expected to ship by", cartPage) ||
					cartPage.elementLayer.verifyElementTextContains("productAvailability", "Ships Soon", cartPage) ||
					cartPage.elementLayer.verifyElementTextContains("productAvailability", "will ship on", cartPage),
					"Check the availability is dispalying in the shopping bag page", 
					"The availability is dispalying in the shopping bag page", 
					"The availability is not dispalying in the shopping bag page", driver);
			
			if(cartPage.productIsShoeType()){
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productShoeSize", "productAvailability", cartPage), 
						"Availability Messaging should be displayed below the Variation Selections", 
						"Availability Messaging is displayed below the Variation Selections", 
						"Availability Messaging is not displayed below the Variation Selections", driver);
			}else{
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productSize", "productAvailability", cartPage), 
						"Availability Messaging should be displayed below the Variation Selections", 
						"Availability Messaging is displayed below the Variation Selections", 
						"Availability Messaging is not displayed below the Variation Selections", driver);
			}
			Log.softAssertThat(cartPage.verifyAvailabilityMessageForOOS(), 
					"Availability Messaging should not display for Out of stock item on the cart page.", 
					"Availability Messaging is not displayed for Out of stock item on the cart page.", 
					"Availability Messaging is displayed for Out of stock item on the cart page.", driver);
	
			//Step-7: Verify the functionality of Item Price
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productAvailability", "productUnitPrice", cartPage),
					"Check the product price is displaying below the availability message", 
					"The product price is displaying below the availability message", 
					"The product price is not displaying below the availability message", driver);

			Log.softAssertThat(cartPage.elementLayer.verifyElementColor("spanProductPrice", colorCode, cartPage),
					"Check the product price is displaying markdown product and is in red color", 
					"The product price is displaying markdown product and is in red color", 
					"The product price is not displaying markdown product", driver);
           
			if (!Utils.isMobile()) {
				Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productQuantityColumn", "productDetails", cartPage) &&
						cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productTotalPrice", "productQuantityColumn", cartPage),
						"Check the product quantity is in between the product details and sub-total", 
						"The product quantity is in between the product details and sub-total", 
						"The product quantity is not in between the product details and sub-total", driver);
			} else {
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productDetails", "productQuantityColumn", cartPage),
						"Check the product quantity is displaying below the product details", 
						"The product quantity is displaying below the product details", 
						"The product quantity is not displaying below the product details", driver);
	
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productQuantityColumn", "productTotalPrice", cartPage),
						"Check the product quantity is displaying above the product quantity", 
						"The product quantity is displaying above the product quantity", 
						"The product quantity is not displaying above the product quantity", driver);
			}
	
			pdpPage = homePage.headers.navigateToPDP(searchKey_SinglePrice);
			Log.message(i++ + ". Navigated to Pdp Page with the product! "+  searchKey_SinglePrice, driver);
	
			productPrice=pdpPage.getProductPrice();
	
			productColor=pdpPage.selectColor();
			Log.message(i++ + ". Select coor: "+ productColor, driver);
	
			productSize=pdpPage.selectSize();
			Log.message(i++ + ". Select size: "+ productSize, driver);
	
			pdpPage.clickAddProductToBag();
			pdpPage.closeAddToBagOverlay();
	
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("productMarkDownPrice"), cartPage) &&
					cartPage.elementLayer.verifyListElementSize("productSalesPrice", 2, cartPage),
					"Check the product price is displaying markdown product and regular size product", 
					"The product price is displaying markdown product and regular size product", 
					"The product price is not displaying markdown product and regular size product", driver);
	
			Log.softAssertThat(cartPage.elementLayer.verifyElementColor("productItemTotalMarkDown", colorCode, cartPage),
					"Check the sub total is displaying markdown product and is in red color", 
					"The sub total is displaying markdown product and is in red color", 
					"The sub total is not displaying markdown product in red color", driver);
	
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("productItemTotalRegular"), cartPage),
					"Check the product total is displaying regular product price", 
					"The product total is displaying regular product price", 
					"The product total is displaying regular product price", driver);
	
			//Step-8: Verify the functionality of Quantity Selected
			if(!Utils.isMobile()) {
				Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productQuantityColumn", "productDetails", cartPage)
								&& cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productTotalPrice", "productQuantityColumn", cartPage), 
						"Quantity Selected should be positioned in between the product information and sub total with increment", 
						"Quantity Selected is positioned in between the product information and sub total with increment", 
						"Quantity Selected is not positioned in between the product information and sub total with increment", driver);
				
				Log.softAssertThat(cartPage.verifyQuanityEditArrowposition(), 
						"Increase/decrease arrows should be stacked on at same side of quantity.", 
						"Increase/decrease arrows are stacked on at same side of quantity.", 
						"Increase/decrease arrows are not stacked on at same side of quantity.", driver);

			}else {
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productDetails", "productQuantityColumn", cartPage)
								&& cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productQuantityColumn", "productTotalPrice", cartPage), 
						"Quantity Selected should be positioned to the below the Item Price", 
						"Quantity Selected is positioned to the below the Item Price", 
						"Quantity Selected is not positioned to the below the Item Price", driver);
				
				if(BrandUtils.isBrand(Brand.rm) || BrandUtils.isBrand(Brand.jl)){				
					Log.softAssertThat(cartPage.verifyQuanityEditArrowposition(), 
							"Quantity Selected should be displayed with increase/decrease arrows at both sides.", 
							"Quantity Selected is displayed with increase/decrease arrows at both sides.", 
							"Quantity Selected is not displayed with increase/decrease arrows at both sides.", driver);
			}else{
				Log.softAssertThat(cartPage.verifyQuanityEditArrowposition(), 
						"Increase/decrease arrows should be stacked on at same side of quantity.", 
						"Increase/decrease arrows are stacked on at same side of quantity.", 
						"Increase/decrease arrows are not stacked on at same side of quantity.", driver);
				}
			}
			
			if(Utils.isDesktop()){
				cartPage.clickOnEditLink(0);
				Log.message(i++ + ". Navigated to Quick shop page when click the Edit link", driver);
	
				cartPage.selectQtyInEditOverlay(Integer.parseInt(initialQuantity));
	
				cartPage.clickOnUpdateInEditOverlay();
	
				Log.softAssertThat(cartPage.verifyProductQuantity(Integer.parseInt(initialQuantity),0),
						"The changes of Quickshop should be reflect in the cart page", 
						"The changes of Quickshop is reflected in the cart page as expected!", 
						"The changes of Quickshop not reflected in the cart page!", driver);
	
			} else {
				pdpPage=(PdpPage)cartPage.clickOnEditLink(0);
				Log.message(i++ + ". Navigated to Pdp page when click the Edit link ");
	
				pdpPage.selectQty(initialQuantity);
	
				pdpPage.clickAddProductToBag();
	
				cartPage = homePage.headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
			}
	
			cartPage.clickOnAddToWishlist();
	
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("txtInWishList"), cartPage),
					"Check a product is added in wishlist", 
					"The product is added in wishlist", 
					"The product is not added in wishlist", driver);
	
			if(Utils.isDesktop()) {
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkEdit", "lnkAddToWishlist", cartPage),
						"Check the edit link is displaying above the wishlist link", 
						"The edit link is displaying above the wishlist link", 
						"The edit link is not displaying above the wishlist link", driver);
			} else {
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkEdit_Tablet_Mobile", "lnkAddToWishlist", cartPage),
						"Check the edit link is displaying above the wishlist link", 
						"The edit link is displaying above the wishlist link", 
						"The edit link is not displaying above the wishlist link", driver);
			}
			
			cartPage.removeAllCartProduct();
	
			pdpPage = homePage.headers.navigateToPDP(searchKey_Brand); 
			Log.message(i++ + ". Navigated to Pdp Page with the product! "+  searchKey_Brand, driver);
	
			productColor=pdpPage.selectColor(); //select Color
			Log.message(i++ + ". Selected the "+  productColor +" Color", driver);
	
			productSize=pdpPage.selectSize(); //select size
			Log.message(i++ + ". Selected the "+  productSize +" Size", driver);
	
			pdpPage.selectQty("10");
			for (int addItem = 0; addItem < 4; addItem++) {
				pdpPage.clickAddProductToBag();
				pdpPage.closeAddToBagOverlay();
			}
	
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("divIncreaseArrow"), cartPage),
					"The Incremental arrow should be disabled when quantity reached higher limit ", 
					"The Incremental arrow id disabled when quantity reached higher limit!", 
					"The Incremental arrow is not disabled when quantity reached higher limit!", driver);
	
			cartPage.removeAllCartProduct();
	
			Log.softAssertThat(cartPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("productNameSection"), cartPage),
					"The product should remove when clicking on remove button", 
					"The product is removing when clicking on remove button!", 
					"The product is not removing when clicking on remove button!", driver);
	
			//Step-9: Verify the functionality of Item Total
			pdpPage = homePage.headers.navigateToPDP(searchKey_Brand); 
			Log.message(i++ + ". Navigated to Pdp Page with the product! "+  searchKey_Brand, driver);
	
			productColor=pdpPage.selectColor(); //select Color
			Log.message(i++ + ". Selected color: " +  productColor, driver);
	
			productSize=pdpPage.selectSize(); //select size
			Log.message(i++ + ". Selected size: " +  productSize, driver);
	
			pdpPage.selectQty("10");
			for (int addItem = 0; addItem < 4; addItem++) {
				pdpPage.clickAddProductToBag();
				pdpPage.closeAddToBagOverlay();
			}
	
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productTotalPrice", "productQuantityColumn", cartPage), 
						"Item total should be displayed to the right side of the 'Qty' column with heading 'Subtotal'", 
						"Item total is displayed to the right side of the 'Qty' column with heading 'Subtotal'", 
						"Item total is not displayed to the right side of the 'Qty' column with heading 'Subtotal'", driver);
			}
			else {
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productQuantityColumn", "productTotalPrice", cartPage), 
						"The 'Subtotal' field should be displayed below the 'Qty' field", 
						"The 'Subtotal' field is displayed below the 'Qty' field", 
						"The 'Subtotal' field is not displayed below the 'Qty' field", driver);
			}
			
			//Step-10: Verify the functionality of Edit Link
			if(Utils.isDesktop()) {
				Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnEditDetails", "productTotalPrice", cartPage), 
						"The 'Edit' link should be displayed to the right hand side of the 'Subtotal' field.", 
						"The 'Edit' link is displayed to the right hand side of the 'Subtotal' field.", 
						"The 'Edit' link is not displayed to the right hand side of the 'Subtotal' field.", driver);
			}
			else if(Utils.isTablet()){
				Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnEditDetails_Mobile_Tablet", "txtSubtotal", cartPage), 
						"The 'Edit' link should be displayed to the right hand side of the 'Subtotal' field.", 
						"The 'Edit' link is displayed to the right hand side of the 'Subtotal' field.", 
						"The 'Edit' link is not displayed to the right hand side of the 'Subtotal' field.", driver);
				
			}else{
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgProductSection", "btnEditDetails_Mobile_Tablet", cartPage), 
						"The 'Edit' link should be displayed below the 'Add to Cart Brand' logo.", 
						"The 'Edit' link is displayed below the 'Add to Cart Brand' logo.", 
						"The 'Edit' link is not displayed below the 'Add to Cart Brand' logo.", driver);
			}
			
			cartPage.clickOnEditLink(0);
			if(Utils.isDesktop()) {
				Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("viewEditModal"), cartPage), 
						"User should navigate to quickview", 
						"User navigated to quickview", 
						"User did not navigate to quickview", driver);
				if(cartPage.productIsShoeType()){
					Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("modalSelectedShoeSize", "modalSelectedSize"), cartPage), 
							"Product variation should be pre-selected.", 
							"Product variation is pre-selected.", 
							"Product variation is not pre-selected.", driver);
					
				}else{
					Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("modalSelectedColor", "modalSelectedSize"), cartPage), 
							"Product variation should be pre-selected.", 
							"Product variation is pre-selected.", 
							"Product variation is not pre-selected.", driver);
				}
				
				cartPage.clickCloseModal();
			}
			else {
				if(cartPage.productIsShoeType()){
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("selectedColor", "selectedshoeSize"), pdpPage), 
							"user should navigate to the PDP and Product variation should be pre-selected.", 
							"user should navigate to the PDP Product variation is pre-selected.", 
							"user should navigate to the PDP Product variation is not pre-selected.", driver);
					
					
				}else{
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("selectedColor", "selectedSize"), pdpPage), 
							"user should navigate to the PDP and Product variation should be pre-selected.", 
							"user should navigate to the PDP Product variation is pre-selected.", 
							"user should navigate to the PDP Product variation is not pre-selected.", driver);
				}
				driver.navigate().back();
			}
			
			//Step-11: Verify the functionality of Add to Wishlist Link
			if(Utils.isDesktop()){
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnEditDetails", "lnkAddToWishlist", cartPage), 
						"'Add to Wishlist' link should be displayed below the Edit link", 
						"'Add to Wishlist' link is displayed below the Edit link", 
						"'Add to Wishlist' link is not displayed below the Edit link", driver);
			}else{
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnEditDetails_Mobile_Tablet", "lnkAddToWishlist", cartPage), 
					"'Add to Wishlist' link should be displayed below the Edit link", 
					"'Add to Wishlist' link is displayed below the Edit link", 
					"'Add to Wishlist' link is not displayed below the Edit link", driver);	
			}
			//Step-12: Verify the functionality of Remove Link
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkAddToWishlist", "btnRemoveInCartPage", cartPage), 
					"'Remove' link should be displayed below the 'Add to Wishlist' link.", 
					"'Remove' link is displayed below the 'Add to Wishlist' link.", 
					"'Remove' link is not displayed below the 'Add to Wishlist' link.", driver);
			
			int itemCountBefore = cartPage.getCartItemNameList().size();
			cartPage.removeCartProduct(0);
			int itemCountAfter = cartPage.getCartItemNameList().size();
			Log.softAssertThat((itemCountAfter == itemCountBefore-1), 
					"Click/Tap on remove link it should remove the item from the cart", 
					"Click/Tap on remove link it removes the item from the cart", 
					"Click/Tap on remove link it does not removes the item from the cart", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveWishListProducts(driver);
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP_C22533
}// search