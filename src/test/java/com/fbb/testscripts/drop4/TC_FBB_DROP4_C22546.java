package com.fbb.testscripts.drop4;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22546 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22546(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_variation");
		String promocode = TestData.get("cpn_freefixed");
		String promocode1 = TestData.get("cpn_promo-ship-level");
		//String promocode2 = TestData.get("cpn_free20");
		String errorMessageColor = TestData.get("couponErrorColorRGB");
		//Create web driver
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!",
					driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.getProductName();	
	
			String selectedColor = pdpPage.selectColor();
			Log.message(i++ + ". Selected Color :: '"+selectedColor+"'", driver);
			
			String selectedSize = pdpPage.selectSize();
			Log.message(i++ + ". Size Selected :: '"+selectedSize+"'", driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Added product to cart.", driver);
	
			ShoppingBagPage shoppingBag = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on 'My Bag' icon in header");
			
			//Step-1: Verify the display of coupon heading
			if(Utils.isMobile()) {
				Log.softAssertThat(shoppingBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtPromoHeading", "txtPromoCode", shoppingBag), 
						"The Coupon heading should be displayed on top of the coupon entry field", 
						"The Coupon heading is displayed on top of the coupon entry field", 
						"The Coupon heading is not displayed on top of the coupon entry field", driver);
			}
			else {
				Log.softAssertThat(shoppingBag.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtPromoCode", "txtPromoHeading", shoppingBag), 
						"Coupon heading should be displayed as a heading left side to the coupon entry field", 
						"Coupon heading is displayed on the left side to the coupon entry field", 
						"Coupon heading is not displayed on the left side to the coupon entry field", driver);
			}
			
			//Step-2: Verify the functionality of Tool tip
			Log.softAssertThat(shoppingBag.elementLayer.verifyHorizontalAllignmentOfElements(driver, "couponToolTip", "txtPromoHeading", shoppingBag), 
					"The Tool tip should be displayed as icon beside the coupon heading", 
					"The Tool tip should be displayed as icon beside the coupon heading", 
					"The Tool tip should be displayed as icon beside the coupon heading", driver);
			
			shoppingBag.clickOnCouponToolTip();
			Log.message(i++ + ". Clicked on Coupon Tool tip"); 
			
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("couponToolTipContent"), shoppingBag),
					"Coupon tool tip content should be displayed", 
					"Coupon tool tip content is displayed", 
					"Coupon tool tip content is not displayed", driver);
	
			shoppingBag.clickOnCouponToolTipclose();
			Log.message(i++ + ". Clicked on Coupon Tool Tip Close.", driver); 
	
			//Step-3: Verify the functionality of Enter Coupon field
			Log.softAssertThat(shoppingBag.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnApplycoupon", "promoInputfield", shoppingBag),
					"The Coupon Entry Field should be displayed to right side of the Coupon heading with Apply button", 
					"The Coupon Entry Field is displayed to right side of the Coupon heading with Apply button",
					"The Coupon Entry Field is not displayed to right side of the Coupon heading with Apply button", driver);
			
			Log.reference("Rest of step covered in Steps 4, 5, 6");
			
			//Step-4: Verify the display of reward certificate error
			shoppingBag.enterPromoCode("rw");
			Log.message(i++ + ". Entered 'rw' Promo Code in shopping bag page to verify reward ceritifcate error", driver);
	
			if(Utils.isMobile()){
				Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("txtRewardCertificateErrorMobile"), shoppingBag),
						"Reward Certificate Error should be displayed when entering 'rw' in promo code section", 
						"Reward Certificate Error is displayed when entering 'rw' in promo code section", 
						"Reward Certificate Error is not displayed when entering 'rw' in promo code section", driver);
			}
			else{
				Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("txtRewardCertificateError"), shoppingBag),
						"Reward Certificate Error should be displayed when entering 'rw' in promo code section", 
						"Reward Certificate Error is displayed when entering 'rw' in promo code section", 
						"Reward Certificate Error is not displayed when entering 'rw' in promo code section", driver);
			}
			
			shoppingBag.enterPromoCode("r");
			Log.message(i++ + ". Removed 'w' from Prefix entered.", driver);
			
			if(Utils.isMobile()){
				Log.softAssertThat(!shoppingBag.elementLayer.verifyPageElements(Arrays.asList("txtRewardCertificateErrorMobile"), shoppingBag),
						"Removing the 'w' from prefix entered should remove the Reward Certificate error.", 
						"Removing the 'w' from prefix entered removed the Reward Certificate error.", 
						"Removing the 'w' from prefix entered did not remove the Reward Certificate error.", driver);
			}
			else{
				Log.softAssertThat(!shoppingBag.elementLayer.verifyPageElements(Arrays.asList("txtRewardCertificateError"), shoppingBag),
						"Removing the 'w' from prefix entered should remove the Reward Certificate error.", 
						"Removing the 'w' from prefix entered removed the Reward Certificate error.", 
						"Removing the 'w' from prefix entered did not remove the Reward Certificate error.", driver);
			}
			
			shoppingBag.enterPromoCode(promocode1);
			Log.message(i++ + ". Entered Promo Code in shopping bag page", driver);
	
			shoppingBag.clickOnApplycouponButton();
			Log.message(i++ + ". Clicked on Apply button", driver);
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtRewardCertificateError"), shoppingBag),
					"Reward Certificate Error should not displayed when not entering 'rw' in promo code section", 
					"Reward Certificate Error is not displayed when not entering 'rw' in promo code section", 
					"Reward Certificate Error is displayed when not entering 'rw' in promo code section", driver);
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("txtpromotionCalloutMsg"), shoppingBag),
					"Promotion Callout message should be displayed in Shopping bag", 
					"Promotion Callout message is displayed in Shopping bag", 
					"Promotion Callout message is not displayed in Shopping bag", driver);
			
			//Step-5: Verify the display of coupon error
			shoppingBag.enterPromoCode(promocode1);
			Log.message(i++ + ". Entered Promo Code in shopping bag page", driver);
	
			shoppingBag.clickOnApplycouponButton();
			Log.message(i++ + ". Clicked on Apply button", driver);
			
			shoppingBag.enterPromoCode(promocode1);
			Log.message(i++ + ". Entered suplicate coupon.", driver);
	
			shoppingBag.clickOnApplycouponButton();
			Log.message(i++ + ". Clicked on Apply button", driver);
			
			if(Utils.isMobile()){
				Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("txtcouponErrorMobile"), shoppingBag)
								&& shoppingBag.elementLayer.verifyElementColor("txtcouponErrorMobile", errorMessageColor, shoppingBag),
						"Coupon Error should be displayed in red font color for duplicate coupon.", 
						"Coupon Error is displayed in red font color for duplicate coupon.", 
						"Coupon Error is not displayed in red font color for duplicate coupon. SM-1107", driver);
			}
			else{
				Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("txtcouponError"), shoppingBag)
								&& shoppingBag.elementLayer.verifyElementColor("txtcouponError", errorMessageColor, shoppingBag),
						"Coupon Error should be displayed in red font color for duplicate coupon.", 
						"Coupon Error is displayed in red font color for duplicate coupon.", 
						"Coupon Error is not displayed in red font color for duplicate coupon. SM-1107", driver);
			}
			
			shoppingBag.enterPromoCode(promocode);
			Log.message(i++ + ". Entered second Promo Code.", driver);
	
			shoppingBag.clickOnApplycouponButton();
			Log.message(i++ + ". Clicked on Apply button", driver);	
	
			//SM-4824: Number of coupons used in checkout currently limited to 1
			/*shoppingBag.enterPromoCode(promocode2);
			Log.message(i++ + ". Entered third Promo Code.");
	
			shoppingBag.clickOnApplycouponButton();
			Log.message(i++ + ". Clicked on Apply button");*/
			
			if(Utils.isMobile()){
				Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("txtcouponErrorMobile"), shoppingBag)
								&& shoppingBag.elementLayer.verifyElementColor("txtcouponErrorMobile",  errorMessageColor, shoppingBag),
						"Coupon Error should be displayed in red font color for more than limit.", 
						"Coupon Error is displayed in red font color for more than limit.", 
						"Coupon Error is not displayed for more than limit.", driver);
			}
			else{
				Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("txtcouponError"), shoppingBag)
								&& shoppingBag.elementLayer.verifyElementColor("txtcouponError", errorMessageColor, shoppingBag),
						"Coupon Error should be displayed in red font color for more than limit.", 
						"Coupon Error is displayed in red font color for more than limit.", 
						"Coupon Error is not displayed in red font color for more than limit.", driver);
			}
			
			//Step-6: Verify the display of multiple coupons when applied successfully
			Log.reference("SM-4824: Number of coupons used in checkout currently limited to 1");
			/*Log.softAssertThat(shoppingBag.elementLayer.verifyListElementSize("coupounAppliedCount", 2, shoppingBag), 
					"Shopping bag should displayed the multiple coupons when applied successfully.",
					"Shopping bag is displayed the multiple coupons when applied successfully.", 
					"Shopping bag is not displayed the multiple coupons when applied successfully.", driver);
			
			Log.softAssertThat(shoppingBag.elementLayer.verifyVerticalAlignmentOfListElementItem("coupounAppliedCount", shoppingBag),
					"The System should stack the applied coupons vertically ", 
					"The System stacked the applied coupons vertically ", 
					"The System didn't stack the applied coupons vertically ", driver);*/
	
			//Step-7: Verify the functionality of 'See Details' link in the Applied coupon section
			shoppingBag.clickOnCouponSeeDetails();
			Log.message(i++ + ". Clicked on See details Tool tip", driver);
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("txtPromotionDescription"), shoppingBag),
					"Promotion Description should be displayed", 
					"Promotion Description is displayed", 
					"Promotion Description is not displayed", driver);
			
			//Step-8: Verify the functionality of 'X' link in the Applied coupon section
			shoppingBag.removeAllAppliedCoupons();
			Log.message(i++ + ". Clicked on Remove button", driver);
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("appliedCouponRow"), shoppingBag),
					"Applied Coupon should be removed when clicking on remove button", 
					"Applied Coupon is removed when clicking on remove button", 
					"Applied Coupon is not removed when clicking on remove button", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}//TC_FBB_DROP4_C22546
}// search
