package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22490 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22490(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String username;
		String password = accountData.get("password_global");
		String address = checkoutProperty.get("valid_address7");
		String nickName = "Nick";
		String firstName = address.split("\\|")[7];
		String lastName = address.split("\\|")[8];
		String address1 = address.split("\\|")[0];
		String address2 = address.split("\\|")[1];
		String city = address.split("\\|")[2];
		String zipcode = address.split("\\|")[4];
		String phone = address.split("\\|")[5];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
	
		//Prerequisite: A Registered user account with 1 saved address
		{
			GlobalNavigation.registerNewUser(driver, 1, 0, AccountUtils.generateEmail(driver) + "|test123@");
		}
	
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.checkout(driver, prd_any, i, username + "|" + password);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (Integer) obj[1];
	
			//1 - Verify the functionality of Saved Address Menu
			//Log.failsoft("Unable to add address for customer : PXSFCC-1863");
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "shippingDetailsHeader", "savedAddressCountWithText", checkoutPage),
					"Saved Address Menu should be displayed below the Shipping Address Heading.",
					"Saved Address Menu displayed below the Shipping Address Heading.",
					"Saved Address Menu not displayed below the Shipping Address Heading.", driver);
			checkoutPage.clickAddNewAddress();
			Log.message(i++ + ". Clicked Add New Address.", driver);
	
			Log.softAssertThat(checkoutPage.getValueFromSavedAddressDropdown().equals("New Address"),
					"The text 'New Address' should be displayed",
					"The text 'New Address' is displayed",
					"The text 'New Address' is not displayed", driver);
	
			//2
			//Placeholder Verification
	
			Log.softAssertThat(checkoutPage.verifyPlaceHolderForFirstNameField(),
					"The place holder for First Name field should be displayed correctly",
					"The place holder for First Name field is displayed correctly",
					"The place holder for First Name field is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.verifyPlaceHolderForLastNameField(),
					"The place holder for Last Name field should be displayed correctly",
					"The place holder for Last Name field is displayed correctly",
					"The place holder for Last Name field is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.verifyPlaceHolderForAddressline1Field(),
					"The place holder for Address Line 1 field should be displayed correctly",
					"The place holder for Address Line 1 field is displayed correctly",
					"The place holder for Address Line 1 field is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.verifyPlaceHolderForAddressline2Field(),
					"The place holder for Address Line 2 field should be displayed correctly",
					"The place holder for Address Line 2 field is displayed correctly",
					"The place holder for Address Line 2 field is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.verifyPlaceHolderForZipcodeField(),
					"The place holder for Zipcode field should be displayed correctly",
					"The place holder for Zipcode field is displayed correctly",
					"The place holder for Zipcode field is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.verifyPlaceHolderForCityField(),
					"The place holder for City field should be displayed correctly",
					"The place holder for City field is displayed correctly",
					"The place holder for City field is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.verifyPlaceHolderForPhoneNumberField(),
					"The place holder for Phone Number field should be displayed correctly",
					"The place holder for Phone Number field is displayed correctly",
					"The place holder for Phone Number field is not displayed correctly", driver);
	
			Log.softAssertThat(checkoutPage.verifyCountryHolderForPhoneNumberField(),
					"The place holder for Country field should be displayed correctly",
					"The place holder for Country field is displayed correctly",
					"The place holder for Country field is not displayed correctly", driver);
	
			//Log.failsoft("The Country field should have placeholder text as 'United States' : PXSFCC-2353");
	
			//Invalid Data Verification
			checkoutPage.typeTextInNickNameField("#%^$^66");
			Log.message(i++ + ". Typed Invalid Nickname.", driver);
	
			checkoutPage.typeTextInFirstNameField("#$%@");
			Log.message(i++ + ". Typed invalid first name.", driver);
	
			checkoutPage.typeTextInLastNameField("!@#43");
			Log.message(i++ + ". Typed invalid last name.", driver);
	
			checkoutPage.typeTextInAddressLine1Field("#%^776");
			Log.message(i++ + ". Typed invalid Address Line 1.", driver);
	
			checkoutPage.typeTextInAddressLine2Field("#%^776");
			Log.message(i++ + ". Typed invalid Address Line 2.", driver);
	
			checkoutPage.typeTextInZipcodeField("1234@");
			Log.message(i++ + ". Typed Zipcode.", driver);
	
			checkoutPage.typeTextInPhoneNumberField("dfghh6985");
			Log.message(i++ + ". Typed invalid Phone Number.", driver);
	
			Headers headers = new Headers(driver).get();
			if(Utils.isDesktop())
			{
				headers.clickOutSide();
			}
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("errShippingFirstName", "errShippingLastName", "errShippingZipcode", "errShippingPhone"), checkoutPage),
					"Error should be displayed when trying to enter invalid data",
					"Error is displayed when trying to enter invalid data",
					"Error is not displayed when trying to enter invalid data", driver);
	
			//Log.failsoft("Incorrect data is accepted : PXSFCC-1316 Please see comments in bug");
			Log.message("Special characters are accepted in the Address line and city fields. As per Ravi's comments in the bug PXSFCC-1294, this is as designed.");
	
			checkoutPage.typeTextInNickNameField(nickName);
			Log.message(i++ + ". Typed unique Nickname.", driver);
	
			checkoutPage.typeTextInFirstNameField(firstName);
			Log.message(i++ + ". Typed valid first name.", driver);
	
			checkoutPage.typeTextInLastNameField(lastName);
			Log.message(i++ + ". Typed valid last name.", driver);
	
			checkoutPage.typeTextInAddressLine1Field(address1);
			Log.message(i++ + ". Typed valid Address Line 1.", driver);
	
			checkoutPage.typeTextInAddressLine2Field(address2);
			Log.message(i++ + ". Typed valid Address Line 2.", driver);
	
			checkoutPage.typeTextInZipcodeField(zipcode);
			Log.message(i++ + ". Typed valid hyphenated Zipcode.", driver);
	
			checkoutPage.typeTextInCityField(city);
			Log.message(i++ + ". Typed valid City.", driver);
	
			checkoutPage.typeTextInPhoneNumberField(phone);
			Log.message(i++ + ". Typed valid Phone Number.", driver);
	
			try{
				Log.softAssertThat(checkoutPage.getSelectedCountry().equalsIgnoreCase("United States"),
						"country should be displayed as United States by default",
						"country is displayed as United States by default",
						"country is not displayed as United States by default", driver);
			}
			catch(Exception e)
			{
				Log.failsoft("The Country field should be pre-selected with value 'United States' in Shipping Address section : PXSFCC-3900");
	
				checkoutPage.selectCountry(0);
			}
			
			checkoutPage.selectState("Select...");
			Log.message(i++ + ". Select state to Select...", driver);
	
			Log.softAssertThat(checkoutPage.verifyContinueButtonEnabled() == false,
					"The State field should be mandatory",
					"The State field is mandatory",
					"The State field is not mandatory", driver);
	
			String expected = checkoutPage.selectState("Florida");
			Log.message(i++ + ". Selected State value from dropdown.", driver);
	
			Log.softAssertThat(checkoutPage.getSelectedState().equalsIgnoreCase(expected),
					"The selected State should be displayed",
					"The selected State is displayed",
					"The selected State is not displayed", driver);
	
			checkoutPage.typeTextInNickNameField("");
			Log.message(i++ + ". Empty Nickname.", driver);
			
			if(Utils.isDesktop())
			{
				headers.clickOutSide();
			}
	
			checkoutPage.typeTextInNickNameField(nickName);
			Log.message(i++ + ". Typed unique Nickname.", driver);
	
			checkoutPage.typeTextInFirstNameField("");
			Log.message(i++ + ". empty first name.", driver);
	
			Log.softAssertThat(checkoutPage.verifyContinueButtonEnabled() == false,
					"The firstname field should be mandatory",
					"The firstname field is mandatory",
					"The firstname field is not mandatory", driver);
	
			checkoutPage.typeTextInFirstNameField(firstName);
			Log.message(i++ + ". Typed valid first name.", driver);
	
			checkoutPage.typeTextInLastNameField("");
			Log.message(i++ + ". empty last name.", driver);
	
			Log.softAssertThat(checkoutPage.verifyContinueButtonEnabled() == false,
					"The lastname field should be mandatory",
					"The lastname field is mandatory",
					"The lastname field is not mandatory", driver);
	
			checkoutPage.typeTextInLastNameField(lastName);
			Log.message(i++ + ". Typed valid last name.", driver);
	
			checkoutPage.typeTextInAddressLine1Field("");
			Log.message(i++ + ". empty Address Line 1.", driver);
	
			Log.softAssertThat(checkoutPage.verifyContinueButtonEnabled() == false,
					"The address1 field should be mandatory",
					"The address1 field is mandatory",
					"The address1 field is not mandatory", driver);
	
			checkoutPage.typeTextInAddressLine1Field("3850 E Independence St Blvd");
			Log.message(i++ + ". Typed valid Address Line 1.", driver);
	
			checkoutPage.typeTextInAddressLine2Field("");
			Log.message(i++ + ". empty Address Line 2.", driver);
	
			Log.softAssertThat(checkoutPage.verifyContinueButtonEnabled() == true,
					"The address2 field should be optional",
					"The address2 field is optional",
					"The address2 field is not optional", driver);
	
			checkoutPage.typeTextInAddressLine2Field(address2);
			Log.message(i++ + ". Typed valid Address Line 2.", driver);
	
			checkoutPage.typeTextInZipcodeField("");
			Log.message(i++ + ". empty Zipcode.", driver);
	
			Log.softAssertThat(checkoutPage.verifyContinueButtonEnabled() == false,
					"The zipcode field should be mandatory",
					"The zipcode field is mandatory",
					"The zipcode field is not mandatory", driver);
	
			checkoutPage.typeTextInZipcodeField(zipcode);
			Log.message(i++ + ". Typed valid Zipcode.", driver);
	
			checkoutPage.typeTextInCityField("");
			checkoutPage.typeTextInCityField("");
			Log.message(i++ + ". Empty City.", driver);
	
			Log.softAssertThat(checkoutPage.verifyContinueButtonEnabled() == false,
					"The city field should be mandatory",
					"The city field is mandatory",
					"The city field is not mandatory", driver);
	
			checkoutPage.typeTextInCityField(city);
			Log.message(i++ + ". Typed valid City.", driver);
	
			checkoutPage.typeTextInPhoneNumberField("");
			Log.message(i++ + ". empty Phone Number.", driver);
	
			Log.softAssertThat(checkoutPage.verifyContinueButtonEnabled() == false,
					"The phone number field should be mandatory",
					"The phone number field is mandatory",
					"The phone number field is not mandatory", driver);
	
			checkoutPage.typeTextInPhoneNumberField(phone);
			Log.message(i++ + ". Typed valid Phone Number.", driver);
	
			//4 - Verify the functionality of 'Make it Default' checkbox
			//6 - Verify the functionality of 'Save this Address' checkbox
	
			Log.softAssertThat(checkoutPage.verifySaveThisAddressIsChecked() == false,
					"The Save This Address chkbox should be unselected by default",
					"The Save This Address chkbox is unselected by default",
					"The Save This Address chkbox is not unselected by default", driver);
	
			Log.softAssertThat(checkoutPage.verifyMakeItDefaultIsChecked() == false,
					"The Make It Default chkbox should be unselected by default",
					"The Make It Default chkbox is unselected by default",
					"The Make It Default chkbox is selected by default", driver);
	
			checkoutPage.checkUncheckMakeItDefault(true);
			Log.message(i++ + ". Selected make It Default chkbox.", driver);
	
			Log.softAssertThat(checkoutPage.verifyMakeItDefaultIsChecked() == true,
					"The Make It Default chkbox should be selected",
					"The Make It Default chkbox is selected",
					"The Make It Default chkbox is not selected", driver);
	
			Log.softAssertThat(checkoutPage.verifySaveThisAddressIsChecked() == true,
					"The Save This Address chkbox should be selected automatically",
					"The Save This Address chkbox is selected automatically",
					"The Save This Address chkbox is not selected automatically", driver);
	
			Log.softAssertThat(checkoutPage.verifySaveThisAddressIsEnabled() == false,
					"The Save This Address chkbox should be disabled",
					"The Save This Address chkbox is disabled",
					"The Save This Address chkbox is not disabled", driver);
	
			checkoutPage.checkUncheckMakeItDefault(false);
			Log.message(i++ + ". Deselected make It Default chkbox.", driver);
	
			Log.softAssertThat(checkoutPage.verifyMakeItDefaultIsChecked() == false,
					"The Make It Default chkbox should be deselected",
					"The Make It Default chkbox is deselected",
					"The Make It Default chkbox is not deselected", driver);
			
			Log.softAssertThat(checkoutPage.verifySaveThisAddressIsChecked() == false,
					"The Save This Address chkbox should be deselected automatically",
					"The Save This Address chkbox is deselected automatically",
					"The Save This Address chkbox is not deselected automatically", driver);
	
			//Verify if the default address is displayed in Checkout page. but not able to add address now. So holding step.
			checkoutPage.checkUncheckMakeItDefault(true);
			Log.message(i++ + ". Selected make It Default chkbox.", driver);
	
			//9
			checkoutPage.clickOnContinue();
			Log.message(i++ + ". Clicked on Continue button.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("divPaymentDetailsSection"), checkoutPage),
					"The Payement section should be displayed",
					"The Payement section is displayed",
					"The Payement section is not displayed", driver);
	
			//4 - contd.
			headers.chooseBrandFromHeader(Utils.getCurrentBrand());
			HomePage homePage = new HomePage(driver).get();
	
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			checkoutPage = (CheckoutPage)cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
	
			Log.softAssertThat(checkoutPage.getValueFromSavedAddressDropdown().contains("(Default)"),
					"The saved default address should be displayed",
					"The saved default address is displayed",
					"The saved default address is not displayed", driver);
	
			//Log.failsoft("Default address is not pre-selected in collapsed saved address dropbox : PXSFCC-2354");
			//5 - Verify the functionality of 'Use This as Billing Address' checkbox
	
			Log.softAssertThat(checkoutPage.verifyUseThisAsBillingAddressIsChecked() == true,
					"The Use This As Billing Address chkbox should be selected by default",
					"The Use This As Billing Address chkbox is selected by default",
					"The Use This As Billing Address chkbox is not selected by default", driver);
	
			//7 - Verify the functionality of Shipping Methods
			Log.message("Steps covered in C22504");
	
			Log.message("Steps covered in C22502"); 
			//10 - Verify the functionality of International Slot
			Log.message("Step 10 cannot be automated. BM configuration needs to be changed at runtime");
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C22490
}// search
