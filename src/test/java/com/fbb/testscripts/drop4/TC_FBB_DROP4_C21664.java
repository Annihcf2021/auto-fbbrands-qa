package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C21664 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C21664(String browser) throws Exception {
		Log.testCaseInfo();
		if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
			throw new SkipException("Test not supported in automation for " + Utils.getCurrentEnv());
		}
	
		//Load Test Data
		String variation_product = TestData.get("prd_variation1");
		String prd_monogram = TestData.get("prd_monogram");
		String couponCode1 = TestData.get("cpn_freeshipping");
		String couponCode2 = TestData.get("cpn_off_10_percentage");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		
		//Pre-requisite - Account Should have more than one payment information
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username +"|"+ password );
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home page.", driver);
	
			homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(variation_product);
			Log.message(i++ + ". Navigated to PDP for :: " + pdpPage.getProductName());
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
			
			//Adding Monogram product
			pdpPage = homePage.headers.navigateToPDP(prd_monogram);
			Log.message(i++ + ". Navigated to PDP for :: " + pdpPage.getProductName(), driver);
			
			pdpPage.selectColor();
			pdpPage.selectSize();
			boolean orderOptionAvailable = pdpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("chkEnableMonogramming"), pdpPage);
			if(orderOptionAvailable) {
				pdpPage.clickOnMonogrammingCheckbox("enable");
				pdpPage.selectMonogrammingFontValue(1);
				pdpPage.selectMonogrammingColorValue(1);
				pdpPage.selectMonogrammingLocationValue(1);
				pdpPage.enterTextInMonogramFirstTextBox("TextEnter");
			}else {
				Log.reference("Monogramming currently out of scope.");
			}
			pdpPage.selectQty("1");
			pdpPage.addToBagKeepOverlay();
			Log.message(i++ + ". Product added to Bag.", driver);
	
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			CheckoutPage checkoutPage = (CheckoutPage) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
			
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", ShippingMethod.Standard, "valid_address7", "Gift Message Goes Here.");
			Log.message(i++ + ". Shipping details filled successfully.", driver);
			
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Billing/Payment Section.", driver);
				
			checkoutPage.applyPromoCouponCode(couponCode1);
			Log.message(i++ + ". Coupon-1 Applied.", driver);
	
			checkoutPage.applyPromoCouponCode(couponCode2);
			Log.message(i++ + ". Coupon-2 Applied.", driver);
			
			checkoutPage.fillingCardDetails1("card_Visa", false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
	
			OrderConfirmationPage ordConfPage = checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//1
			Log.softAssertThat(ordConfPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderDetails", "orderSummary", ordConfPage), 
					"Order Summary should be displayed below the item Information", 
					"Order Summary is displayed below the item Information", 
					"Order Summary is not displayed below the item Information", driver);
			
			//2
			Log.softAssertThat(ordConfPage.elementLayer.verifyPageElements(Arrays.asList("orderShippingAddress"), ordConfPage),
					"Shipping method should be displayed order summary and it does not contain any tooltip.",
					"Shipping method is displayed order summary and it does not contain any tooltip.",
					"Shipping method is not displayed order summary and it does not contain any tooltip.", driver);
			
			Log.softAssertThat(ordConfPage.getOrderTotalInSummary() == ordConfPage.calculateTotalInOrderSummaryList(),
					"Order total should be calculated as the sum of Extended item total, shipping , tax and the promotions are deducted from it.", 
					"Order total is calculated as the sum of Extended item total, shipping , tax and the promotions are deducted from it.",
					"Order total is not calculated as the sum of Extended item total, shipping , tax and the promotions are deducted from it.", driver);
			
			//4
			Log.softAssertThat(ordConfPage.elementLayer.verifyPageElements(Arrays.asList("confrmCotentSlot", "contentAssetHeader"), ordConfPage),
					"Content slot should be displayed in the order confirmation page.",
					"Content slot is displayed in the order confirmation page.",
					"Content slot is not displayed in the order confirmation page.", driver);
			
			Log.softAssertThat(ordConfPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummary", "confrmCotentSlot", ordConfPage), 
					"Content module should be displayed below the order summary.", 
					"Content module is displayed below the order summary.", 
					"Content module is not displayed below the order summary.", driver);
			
			Log.softAssertThat(ordConfPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "confrmCotentSlot", "orderCnfQASection", ordConfPage), 
					"Question and Answer module should be displayed below the Content module.", 
					"Question and Answer module is displayed below the Content module.", 
					"Question and Answer module is not displayed below the Content module.", driver);
			
			Log.softAssertThat(ordConfPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("qaAnswerActive"), ordConfPage),
					"By Default the question and answer should be in collapse status.",
					"By Default the question and answer in the collapse status.",
					"By Default the question and answer not in the collapse status.", driver);
			
			ordConfPage.clickonQuestion();
			
			Log.softAssertThat(ordConfPage.elementLayer.verifyPageElements(Arrays.asList("qaAnswerActive"), ordConfPage),
					"Question and answer should be in Expanded status.",
					"After clicking the question and answer in the Expanded status.",
					"After clicking the question and answer not in the Expanded status.", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C19710
}// search
