package com.fbb.testscripts.drop4;
import java.util.Arrays;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C19715 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader demandwareProperty = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C19715(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		
		String[] validAdr = checkoutProperty.get("valid_address7").split("\\|");
		String checkoutAdd = "address_withtax";
		
		String firstNamePlaceHolder = demandwareProperty.get("FirstNamePlaceHolder");
		
		String lastNamePlaceHolder = demandwareProperty.get("LastNamePlaceHolder");
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			String username = AccountUtils.generateEmail(driver);
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_any, i, "test@yopmail.com");
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkEditSigninSection", "lblGuestHeading", checkoutPage), 
					"The 'Edit' link should be displayed on the right end of the gray section box.",
					"The 'Edit' link is displayed on the right end of the gray section box.",
					"The 'Edit' link is not displayed on the right end of the gray section box", driver);
			
			checkoutPage.clickEditSignIn();
			
			checkoutPage.continueToShipping(username);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyTextContains("lblGuestUserEmail", username, checkoutPage), 
					"If user want to user another email address or update their email address, they can do that by clicking on Edit button.",
					"By clicking on Edit button the user can modify the email address.",
					"By clicking on Edit button the user cannot modify the email address", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblGuestHeading", "divCheckoutShipping", checkoutPage),
					"The Customer Type Label should be displayed above the Shipping Details section",
					"The Customer Type Label is displayed above the Shipping Details section",
					"The Customer Type Label is not displayed above the Shipping Details section.", driver);
	
			//Step-2: Verify the functionality of Continue button when the user leave the mandatory fields empty and click/tap on Continue button
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDisabled(Arrays.asList("btnShippingContinue"), checkoutPage),
					"Continue button should be disabled and it should not be clickable",
					"Continue button disabled and not clickable.",
					"Coutinue button not working as expected.", driver);
			
			Log.softAssertThat(checkoutPage.verifyPlaceHolderState(validAdr[3]),
					"The place holder text in state should moves up when selecting value.",
					"The place holder text in state is moving up when selecting value.",
					"The place holder text in state is not moving up when selecting value.", driver);
			
			if (Utils.isDesktop() || Utils.isTablet()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblShippingHeading", "txtFirstnameShipping", checkoutPage),
						"First Name should displayed below the shipping heading",
						"First name is displaying below the shipping heading.",
						"First name is not displaying below the shipping heading.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtFirstnameShipping", "txtAddressline1Shipping", checkoutPage),
						"Address Line 1 should be displayed below the First Name field",
						"Address Line 1 is displayed below the First Name field.",
						"Address Line 1 is not displayed below the First Name field.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtAddressline1Shipping", "txtZipcode", checkoutPage),
						"Zip code should be displayed below the Address Line 1",
						"Zip code is displayed below the Address Line 1.",
						"Zip code is not displayed below the Address Line 1.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtZipcode", "selectShippingCountry", checkoutPage) &&
						checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "selectStateShipping", "selectShippingCountry", checkoutPage),
						"Country should be displayed below the Zipcode and State",
						"Country is displayed below the Zipcode and State.",
						"Country is not displayed below the Zipcode and State.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtLastnameShipping", "txtFirstnameShipping", checkoutPage),
						"Last Name should be displayed right of the First Name",
						"Last Name is displayed right of the First Name",
						"Last Name is not displayed right of the First Name.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtShippingAdd2", "txtAddressline1Shipping", checkoutPage),
						"Address Line 2 should be displayed right of the Address Line 1 field",
						"Address Line 2 is displayed right of the Address Line 1 field",
						"Address Line 2 is not displayed right of the Address Line 1 field.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtCity", "selectStateShipping", checkoutPage),
						"City should be displayed right of the State field",
						"City is displayed right of the State field",
						"City is not displayed right of the State field.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "selectStateShipping", "txtZipcode", checkoutPage),
						"State should be displayed right of the Zip code field",
						"State is displayed right of the Zip code field",
						"State is not displayed right of the Zip code field.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtPhone", "selectShippingCountry", checkoutPage),
						"Phone Number should be displayed right of the Country field",
						"Phone Number is displayed right of the Country field",
						"Phone Number is not displayed right of the Country field.", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblShippingHeading", "txtFirstnameShipping", checkoutPage),
						"First Name should displayed below the shipping heading",
						"First name is displaying below the shipping heading.",
						"First name is not displaying below the shipping heading.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtFirstnameShipping", "txtLastnameShipping", checkoutPage),
						"Last Name should be displayed below the First Name",
						"Last Name is displayed below the First Name.",
						"Last Name is not displayed below the First Name.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtLastnameShipping", "txtAddressline1Shipping", checkoutPage),
						"Address Line 1 should be displayed below the Last Name field",
						"Address Line 1 is displayed below the Last Name field.",
						"Address Line 1 is not displayed below the Last Name field.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtAddressline1Shipping", "txtShippingAdd2", checkoutPage),
						"Address Line 2 should be displayed below the Address Line 1.",
						"Address Line 2 is displayed below the Address Line 1.",
						"Address Line 2 is not displayed below the Address Line 1.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtShippingAdd2", "txtZipcode", checkoutPage),
						"Zip code should be displayed below the Address Line 2",
						"Zip code is displayed below the Address Line 2.",
						"Zip code is not displayed below the Address Line 2.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "selectStateShipping", "txtCity", checkoutPage),
						"City should be displayed below the State",
						"City is displayed below the State.",
						"City is not displayed below the State.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "selectStateShipping", "txtZipcode", checkoutPage),
						"State should be displayed right of the Zip code field",
						"State is displayed right of the Zip code field",
						"State is not displayed right of the Zip code field.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtCity", "selectShippingCountry", checkoutPage),
						"Country should be displayed below the City",
						"Country is displayed below the City.",
						"Country is not displayed below the City.", driver);
				
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "selectShippingCountry", "txtPhone", checkoutPage),
						"Phone Number should be displayed below the Country",
						"Phone Number is displayed below the Country.",
						"Phone Number is not displayed below the Country.", driver);
			}
			
			Map<String, String> placeHolderLoc = checkoutPage.getPlaceHolderPosition();
			
			checkoutPage.fillingShippingDetailsAsGuest("NO", checkoutAdd, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			Map<String, String> placeHolderLoc1 = checkoutPage.getPlaceHolderPosition();
			
			Log.softAssertThat(checkoutPage.verifyPlaceHolderPosition(placeHolderLoc, placeHolderLoc1),
					"The place holder text should go up in all the text boxes.",
					"The place holder text is going up in all the text boxes.",
					"The place holder text is not going up in all the text boxes.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyTextContains("lblFirstNamePlaceHolder", firstNamePlaceHolder, checkoutPage),
					"The place holder text in first name should comes from property.",
					"The place holder text in first name is comes from property.",
					"The place holder text in first name is not comes from property.", driver);

			Log.softAssertThat(checkoutPage.elementLayer.verifyTextContains("lblLastNamePlaceHolder", lastNamePlaceHolder, checkoutPage),
					"The place holder text in last name should comes from property.",
					"The place holder text in last name is comes from property.",
					"The place holder text in last name is not comes from property.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForListOfElement(Arrays.asList("txtFirstnameShipping","txtLastnameShipping",
					"txtAddressline1Shipping","txtZipcode","txtCity","txtPhone","selectStateShipping"), "aria-required", "true", checkoutPage),
					"'First Name', 'Last name', 'Address Line 1', 'Zip Code', "
					+ "'Phone number', 'State', 'City' field should be a mandatory field",
					"'First Name', 'Last name', 'Address Line 1', 'Zip Code', "
					+ "'Phone number', 'State', 'City' field are mandatory field",
					"Some fields are not mandatory.", driver);
			
			checkoutPage.fillFirstnameShipping("!@1234asdf");
			checkoutPage.fillLastnameShipping("!@1234asdf");
			checkoutPage.fillAddressline1Shipping("!@1234asdf");
			checkoutPage.fillAddressline2Shipping("!@1234asdf");
			checkoutPage.fillZipCodeShipping("!@1234asdf");
			checkoutPage.fillCityShipping("!@1234asdf");
			checkoutPage.fillPhoneShipping("1223332233");
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("errShippingFirstName", "errShippingLastName", 
					"errShippingZipcode", "errShippingPhone", "errShippingCity", "errShippingAddress1"), checkoutPage),
					"The appropriate error message, shown as red text, will be shown in the text box for the respective field when invalid values entered.",
					"The appropriate error message, is displayed in the respective fields when invalid values entered.",
					"The appropriate error message, is not displayed in the respective fields when invalid values entered.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("errShippingFirstName", "errShippingLastName", 
					"errShippingZipcode", "errShippingPhone", "errShippingCity", "errShippingAddress1"), "color", "230, 0, 60", checkoutPage),
					"System should display the appropriate error message as a place holder text in the text box highlighted in red color.",
					"System is displaying the error message as a place holder text in the text box highlighted in red color.",
					"System is not displaying the error message as a place holder text in the text box highlighted in red color.", driver);
			
			checkoutPage.fillingShippingDetailsAsGuest("NO", "valid_address7", ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			Log.softAssertThat(!(checkoutPage.elementLayer.verifyPageElementsDisabled(Arrays.asList("btnShippingContinue"), checkoutPage)),
					"Until user enters valid information in all of the fields, the Continue button will be disabled.",
					"Until user enters valid information in all of the fields, the Continue button is disabled.",
					"After entering valid values the continue button is enabled.", driver);
			
			Log.softAssertThat(!(checkoutPage.elementLayer.verifyPageElementsDisabled(Arrays.asList("btnShippingContinue"), checkoutPage)),
					"User should be able to enter text in all the fields.",
					"User is able to enter text in all the fields.",
					"User is not able to enter text in all the fields.", driver);
			
			checkoutPage.fillAddressline2Shipping("");
			
			Log.softAssertThat(!(checkoutPage.elementLayer.verifyPageElementsDisabled(Arrays.asList("btnShippingContinue"), checkoutPage)),
					"Address line 2 should be an optional field.",
					"Address line 2 is an optional field.",
					"Address line 2 is not an optional field.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("errShippingZipcode"), checkoutPage),
					"System should allow the User to enter zip code as 5 digits (Ex: 55555).",
					"System is allow the User to enter zip code as 5 digits (Ex: 55555).",
					"System is not allow the User to enter zip code as 5 digits (Ex: 55555).", driver);
			
			checkoutPage.fillZipCodeShipping("55555-4444");
			
			Log.softAssertThat(checkoutPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("errShippingZipcode"), checkoutPage),
					"System should allow the User to enter zip code as hyphenated 9 Digits (55555-4444).",
					"System is allow the User to enter zip code as or hyphenated 9 Digits (55555-4444).",
					"System is not allow the User to enter zip code as or hyphenated 9 Digits (55555-4444).", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("selectShippingDefCountry", "label", validAdr[6], checkoutPage),
					"By default the value should be United States.",
					"By default the value is United States.",
					"By default the value is not United States.", driver);
			
			Log.softAssertThat(!(checkoutPage.verifyGiftReceiptCheckBoxIsChecked()) &&
					!(checkoutPage.verifyGiftMessageCheckBoxIsChecked()),
					"By default no gifting options should be selected.",
					"By default no gifting options is selected.",
					"By default gifting options is selected.", driver);
			
			if(Utils.isDesktop() || Utils.isTablet()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblGiftOptionsHeading", "chkGiftReceiptCheckBox", checkoutPage) &&
						checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblGiftOptionsHeading", "chkGiftMessageCheckBox", checkoutPage),
						"Free Gift Receipt and Message check box should be displayed down to the Gift options heading",
						"Free Gift Receipt and Message check box is displayed down to the Gift options heading.",
						"Free Gift Receipt and Message check box is not displayed down to the Gift options heading.", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblGiftOptionsHeading", "chkGiftReceiptCheckBox", checkoutPage) &&
						checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblGiftOptionsHeading", "chkGiftMessageCheckBox", checkoutPage),
						"Free Gift Receipt and Message check box should be displayed down to the Gift options heading",
						"Free Gift Receipt and Message check box is displayed down to the Gift options heading.",
						"Free Gift Receipt and Message check box is not displayed down to the Gift options heading.", driver);
			}
			
			checkoutPage.clickGiftReceiptCheckBox(true);
			
			checkoutPage.clickGiftMessageCheckBox(true);
			
			Log.softAssertThat(checkoutPage.verifyGiftReceiptCheckBoxIsChecked() &&
					checkoutPage.verifyGiftMessageCheckBoxIsChecked(),
					"User should be able to check this box Gift Receipt and Gift Message.",
					"User is able to check this box Gift Receipt and Gift Message.",
					"User is not able to check this box Gift Receipt and Gift Message.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("txtFormGiftMessage1", "txtFormGiftMessage2", "txtFormGiftMessage3"), checkoutPage),
					"Selecting the Free Gift Message check box should reveal three form fields.",
					"Selecting the Free Gift Message check box is revealing three form fields.",
					"Selecting the Free Gift Message check box is not revealing three form fields.", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "chkGiftReceiptCheckBox", "chkGiftMessageCheckBox", checkoutPage),
						"Free Gift Receipt check box should be displayed below to the Free Gift Message",
						"Free Gift Receipt check box is displayed below to the Free Gift Message.",
						"Free Gift Receipt check box is not displayed below to the Free Gift Message.", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "chkGiftReceiptCheckBox", "chkGiftMessageCheckBox", checkoutPage),
						"Free Gift Receipt check box should be displayed right to the Free Gift Message",
						"Free Gift Receipt check box is displayed right to the Free Gift Message",
						"Free Gift Receipt check box is not displayed right to the Free Gift Message.", driver);
			}
			
			checkoutPage.clickGiftMessageCheckBox(false);
			Log.message(i++ + ". Unchecked Free Gift Message", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblGiftOptionsHeading", "btnShippingContinue", checkoutPage),
						"Continue Button should be displayed right of the Gift Options section",
						"Continue Button is displayed right of the Gift Options section.",
						"Continue Button is not displayed right of the Gift Options section.", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnShippingContinue", "lblGiftOptionsHeading", checkoutPage),
						"Continue Button should be displayed right of the Gift Options section",
						"Continue Button is displayed right of the Gift Options section.",
						"Continue Button is not displayed right of the Gift Options section.", driver);
			}
			
			Log.reference("Step 8 - Full functionality covered in the Test Case Id: C22504 Step 1 to 11");
			
			Log.reference("Full functionality covered in the Test Case Id: C22501 Step 13");
			
			checkoutPage.checkUncheckUseThisAsBillingAddress(false);
			Log.message(i++ + ". Disabled use this as billing address.", driver);
			
			checkoutPage.continueToBilling();
			Log.message(i++ + ". Clicked on Continue in Shipping Section.", driver);
			
			if(BrandUtils.isBrand(Brand.jl)) {
				if(Utils.isTablet()){
					Log.softAssertThat(checkoutPage.elementLayer.verifyTextContains("txtGiftReceiptValue_Jl_Tablet", "Yes", checkoutPage),
							"If the user checks the box the System should flag the order to indicate a gift receipt has been requested for the order.",
							"System is flaged the order to indicate a gift receipt has been requested for the order.",
							"System is not flaged the order to indicate a gift receipt has been requested for the order.", driver);
					
				}else{
				Log.softAssertThat(checkoutPage.elementLayer.verifyTextContains("txtGiftReceiptValue", "Yes", checkoutPage),
						"If the user checks the box the System should flag the order to indicate a gift receipt has been requested for the order.",
						"System is flaged the order to indicate a gift receipt has been requested for the order.",
						"System is not flaged the order to indicate a gift receipt has been requested for the order.", driver);
				}
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyTextContains("txtGiftReceiptValue", "Yes", checkoutPage),
						"If the user checks the box the System should flag the order to indicate a gift receipt has been requested for the order.",
						"System is flaged the order to indicate a gift receipt has been requested for the order.",
						"System is not flaged the order to indicate a gift receipt has been requested for the order.", driver);
			}
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("sectionBillingAddress"), checkoutPage),
					"The Billing Address section should be displayed.",
					"The Billing Address section is displayed.",
					"The Billing Address section is not displayed.", driver);
			
			checkoutPage.fillingBillingDetailsAsGuest("valid_address8");
			
			checkoutPage.continueToPaymentFromBilling();
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkEditBilling", "lblBillingHeading", checkoutPage),
					"The Edit link should be displayed to the right of Billing Address Heading",
					"The Edit link is displayed to the right of Billing Address Heading.",
					"The Edit link is not displayed to the right of Billing Address Heading.", driver);
			
			Log.reference("Login Billing address validations covered in the Test Case Id: C22514");
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C19715
}// search
