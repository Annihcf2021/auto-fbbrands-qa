package com.fbb.testscripts.drop4;
import com.fbb.reusablecomponents.TestData;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C21649 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C21649(String browser) throws Exception {
		Log.testCaseInfo();
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		//Load Test Data
		String prd_any_1 = TestData.get("prd_variation");
		String prd_any_2 = TestData.get("prd_variation1");
	    
		String credentials=AccountUtils.generateEmail(driver) +"|"+accountData.get("password_global");
		String username=credentials.split("\\|")[0];
		String password= credentials.split("\\|")[1];
	
		
		GlobalNavigation.registerNewUser(driver, 0, 0, credentials);
	
		int i = 1;
		try {
	
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAccount=homePage.headers.navigateToMyAccount(username, password, true); //Navigate to My Account Page
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			cartPage.removeAllItemsFromCart();
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any_1); //Add Item To cart with the Quantity of 20
			Log.message(i++ + ". Navigated to Pdp Page with the product!"+  prd_any_1, driver);
	
			pdpPage.selectQty("2");
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Change the size of product to 2", driver); //Change Product Quantity
	
			myAccount.signOutAccount(); //SignOut the My Account page
			Log.message(i++ + ". SignOut the My Account Page", driver);

			pdpPage = homePage.headers.navigateToPDP(prd_any_2); //Add the product without Sing in
			Log.message(i++ + ". Navigated to Pdp Page with the product! "+  prd_any_2, driver);
	
			pdpPage.selectQty("2");
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Bag with Quantity 2"  , driver);
			
			homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Navigated to Signin page ",driver);
			
			myAccount=homePage.headers.navigateToMyAccount(username, password); //Navigate to My Account Page
			Log.message(i++ + ". Navigated to My Account page ",driver);
	
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
			//verify the Cart product Quantity
			//Log.softAssertThat(cartPage.verifyMergedProduct(Integer.parseInt(productQuantityNew), Integer.parseInt(productQuantity)),
			Log.softAssertThat(cartPage.verifyMergedProduct(1, 1),
					"The New product should be updated with Quantity in cart (Product A and Product B)", 
					"The New product is updated with Quantity in cart  as expected !!", 
					"The New product not updated with Quantity in cart !!", driver);
	
			//Verify 'Cart Maximum Quantity Reached' message
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver,"divHeadSection", "divQuantityMessage", cartPage), 
					"The Product cart merge message should be display below the Cart Header section", 
					"The Product cart merge message is display below the Cart Header section as expected!!", 
					"The Product cart merge message should be display below the Cart Header section!!", driver);
	
			cartPage.removeAllItemsFromCart();
	
			pdpPage = homePage.headers.navigateToPDP(prd_any_2); //Add the product without Sing in
			Log.message(i++ + ". Navigated to Pdp Page with the product! "+  prd_any_2, driver);
	
			String size = pdpPage.selectSize(); //select size
			String color = pdpPage.selectColor();
			pdpPage.selectQty("2");
			pdpPage.clickAddProductToBag();
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Change the size of product to " +2  , driver);
	
			myAccount.signOutAccount();
			Log.message(i++ +". Sign-Out the My Account Page", driver);
	
			pdpPage = homePage.headers.navigateToPDP(prd_any_2); //Add the product without Sing in
			Log.message(i++ + ". Navigated to Pdp Page with the product! "+  prd_any_2, driver);		
	
			pdpPage.selectColor(color);
			pdpPage.selectSize(size);
			pdpPage.selectQty("3");
			pdpPage.clickAddProductToBag();
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Change the size of product to " +3  , driver);
	
			myAccount=homePage.headers.navigateToMyAccount(username, password); //Navigate to My Account Page
			Log.message(i++ + ". Navigated to My Account page ",driver);

			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
	
			//verify the Cart product Quantity
			Log.softAssertThat(cartPage.verifyProductQuantity(5 ,0) , 
					"The Product Quantity should be Added with the existing Quantity", 
					"The product Quantity is Added as expected !!", 
					"The product Quantity is not added with new quantity!!", driver);
	
			//Log.message("Quantity is not to be Expected  .  Bug Id is PXSFCC-1867");
			Log.softAssertThat(cartPage.verifyElementDisplayedBelow("divHeadSection", "divQuantityMessage", cartPage), 
					"The Product cart merge message should be display below the Cart Header section", 
					"The Product cart merge message is display below the Cart Header section as expected!!", 
					"The Product cart merge message should be display below the Cart Header section!!", driver);
	
			cartPage.removeAllItemsFromCart();
			//cart-promo cart-promo-approaching
			myAccount.signOutAccount();
			Log.message(i++ + ". SignOut the My Account Page", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			//cartPage.removeAllItemsFromCart();
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP_C21649
}// search
