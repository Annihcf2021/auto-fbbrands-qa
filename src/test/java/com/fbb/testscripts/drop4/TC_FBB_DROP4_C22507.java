package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22507 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22507(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_expedited-ship-eligible") + "|" + TestData.get("prd_expedited-ship-ineligible");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, searchKey, i, AccountUtils.generateEmail(driver));
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (Integer) obj[1];
	
			checkoutPage.fillingShippingDetailsAsGuest("YES", "plcc_address", ShippingMethod.Express);
			Log.message(i++ + ". PLCC address filled in sipping details section.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shippingmethodExceptionOverlay"),checkoutPage),
					"Shipping method Exception overlay should be displayed",
					"Shipping method Exception overlay is displayed",
					"Shipping method Exception overlay is not displayed", driver); 
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "shippingmethodExceptionOverlayHeading", "itemIntroductionText", checkoutPage), 
					"Item Introduction Text should be displayed below the Method Exception Heading message.", 
					"Item Introduction Text is displayed below the Method Exception Heading message.", 
					"Item Introduction Text is not displayed below the Method Exception Heading message.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "shippingMethodExceptionItemDeliveryoption", "itemIntroductionText", checkoutPage),
					"The Updated Shipping Method should be displayed next to the Item introduction text",
					"The Updated Shipping Method is displayed next to the Item introduction text.",
					"The Updated Shipping Method is not displayed next to the Item introduction text.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("shippingMethodExceptionItemDeliveryoption", "rgba(230, 0, 60, 1)", checkoutPage),
					"The Updated Shipping Method should be displayed next to the Item introduction text in red font color.",
					"The Updated Shipping Method is displayed next to the Item introduction text in red font color.",
					"The Updated Shipping Method is not displayed next to the Item introduction text in red font color.", driver);
			
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "itemIntroductionText", "OverlayCancelButton", checkoutPage), 
					"Cancel button should be displayed below the Item Information section box.", 
					"Cancel button is displayed below the Item Information section box.", 
					"Cancel button is displayed below the Item Information section box.", driver);
			
			if(Utils.isMobile())
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "OverlayContinueButton", "OverlayCancelButton", checkoutPage),
						"Continue button should be displayed to the right of the Cancel button.", 
						"Continue button is displayed to the right of the Cancel button.", 
						"Continue button is not displayed to the right of the Cancel button.", driver);
			}
			else
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "OverlayContinueButton", "OverlayCancelButton", checkoutPage),
					"Continue button should be displayed to the right of the Cancel button.", 
					"Continue button is displayed to the right of the Cancel button.", 
					"Continue button is not displayed to the right of the Cancel button.", driver);
			}
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shippingmethodExceptionOverlay"),checkoutPage),
					"Shipping method Exception overlay should be displayed",
					"Shipping method Exception overlay is displayed",
					"Shipping method Exception overlay is not displayed", driver); 
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("OverlayContinueButton", "OverlayCancelButton"),checkoutPage),
					"Continue button and close button displayed in  the Method Exception Overlay",
					"Continue button and close button displayed in  the Method Exception Overlay",
					"Continue button and close button not displayed in  the Method Exception Overlay", driver);
	
			checkoutPage.getContinueOverlayButtonText();
			Log.message(i++ + ". Clicked on Continue in Overlay.");
			//Step ;5
			Log.softAssertThat(!checkoutPage.elementLayer.verifyPageElements(Arrays.asList("OverlayContinueButton"),checkoutPage),
					"Continue button should close the Method Exception Overlay",
					"Continue button  closed the Method Exception Overlay",
					"Continue button not closed the Method Exception Overlay ", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("deliveryOptionHeading"),checkoutPage),
					"it should retain the Expedited Shipping Method and the shipping cost for the eligible items",
					"it retains the retain the Expedited Shipping Method and the shipping cost for the eligible items",
					"it not retain the Expedited Shipping Method and the shipping cost for the eligible items", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_HEADER_C22507
}// search
