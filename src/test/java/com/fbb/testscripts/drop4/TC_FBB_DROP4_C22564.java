package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22564 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22564(String browser) throws Exception {
		Log.testCaseInfo();

		//Load Test Data
		String prd_expedited_ship_eligible = TestData.get("prd_expedited-ship-eligible");
		String guestEmail;

		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		guestEmail = AccountUtils.generateEmail(driver);

		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_expedited_ship_eligible, i, guestEmail);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (Integer) obj[1];

			checkoutPage.fillingShippingDetailsAsGuest("NO", "valid_address1", ShippingMethod.SuperFast);
			Log.message(i++ + ". Entered shipping address and 'Super fast delivery' is selected as Shipping method");

			Log.softAssertThat(checkoutPage.checkShippingMethodSuperfastRadioButtonIsSelected(), 
					"System should allow the User to select any of the Expedited Shipping methods", 
					"System allow the User to select any of the Expedited Shipping methods", 
					"System not allowing the User to select any of the Expedited Shipping methods", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally

	}// M1_FBB_DROP4_C22564

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP4_C22564(String browser) throws Exception {
		Log.testCaseInfo();

		//Load Test Data
		String prd_expedited_ship_ineligible = TestData.get("prd_expedited-ship-ineligible");
		String guestEmail;

		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		guestEmail = AccountUtils.generateEmail(driver);

		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_expedited_ship_ineligible, i, guestEmail);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (Integer) obj[1];

			checkoutPage.fillingShippingDetailsAsGuest("NO", "valid_address1", ShippingMethod.Standard);
			Log.message(i++ + ". Entered shipping address and 'Super fast delivery' is selected as Shipping method");

			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lblShippingMethodExcep"), checkoutPage), 
					"Shipping method exception should not display", 
					"Shipping method exception is not displayed", 
					"Shipping method exception is displayed", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally

	}// M2_FBB_DROP4_C22564

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP4_C22564(String browser) throws Exception {
		Log.testCaseInfo();

		//Load Test Data
		String searchKey = TestData.get("prd_expedited-ship-eligible-variation"); 
		String searchKey1 = TestData.get("prd_expedited-ship-ineligible-variation");
		String guestEmail;

		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		guestEmail = AccountUtils.generateEmail(driver);

		int i = 1;
		try {
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey.split("\\|")[0]);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
			
			String color = pdpPage.selectColor(searchKey.split("\\|")[1]);
			Log.message(i++ + ". Selected Color :"+color, driver);
			
			String size = null;
			if(pdpPage.isShoeSizeAvailable())
				size = pdpPage.selectshoeSizeWidth(searchKey.split("\\|")[2]);
			else 
				size = pdpPage.selectSize(searchKey.split("\\|")[2]);
			
			Log.message(i++ + ". Selected Size :"+size, driver);
			
			String sizeFam = pdpPage.selectSizeFamily();
			Log.message(i++ + ". Selected Size Fam :"+sizeFam, driver);
			
			pdpPage.addToBagKeepOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Add to bag overlay closed", driver);
			
			pdpPage = homePage.headers.navigateToPDP(searchKey1.split("\\|")[0]);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);
			
			color = pdpPage.selectColor(searchKey1.split("\\|")[1]);
			Log.message(i++ + ". Selected Color :"+color, driver);
			
			size = null;
			if(pdpPage.isShoeSizeAvailable())
				size = pdpPage.selectshoeSizeWidth(searchKey1.split("\\|")[2]);
			else 
				size = pdpPage.selectSize(searchKey1.split("\\|")[2]);
			
			Log.message(i++ + ". Selected Size :"+size, driver);
			
			sizeFam = pdpPage.selectSizeFamily();
			Log.message(i++ + ". Selected Size Fam :"+sizeFam, driver);
			
			pdpPage.addToBagKeepOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Add to bag overlay closed", driver);
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();//new ShoppingBagPage(driver).get();
			Log.message(i++ + ". Navigated to Cart Page.", driver);

			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to Checkout Page - SignIn Section", driver);
			
			checkoutPage.continueToShipping(guestEmail);
			Log.message(i++ + ". Checkout As :: " + guestEmail, driver);
			
			checkoutPage.fillingShippingDetailsAsGuest("NO", "valid_address1", ShippingMethod.SuperFast);
			Log.message(i++ + ". Entered shipping address and 'Super fast delivery' is selected as Shipping method");

			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblShippingMethodExcep"), checkoutPage), 
					"Shipping method exception should display", 
					"Shipping method exception is displayed", 
					"Shipping method exception is not displayed - SM-1619", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally

	}// M3_FBB_DROP4_C22564
}// search

