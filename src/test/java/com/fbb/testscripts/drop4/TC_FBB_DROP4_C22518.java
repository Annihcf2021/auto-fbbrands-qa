package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22518 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22518(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prodId = TestData.get("prd_variation");
		String reward_valid = TestData.get("reward_valid-1");
		String reward_invalid = "asdf234";
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prodId, i, AccountUtils.generateEmail(driver));
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (Integer) obj[1];
	
			checkoutPage.fillingShippingDetailsAsGuest("YES", "valid_address7", ShippingMethod.Standard);
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Section.", driver);
			//Step 1a
	
			Log.softAssertThat((!(checkoutPage.checkRewardSectionExpadedOrNot())),
					"To verify the 'Have a Reward Certificate' section is collapsed.",
					"The 'Have a Reward Certificate' section is collapsed and the arrow is downward.",
					"The 'Have a Reward Certificate' section is not collapsed and the arrow is not in downward.", driver);
	
			if (!(checkoutPage.checkRewardSectionExpadedOrNot())) { 
				checkoutPage.expandOrColapseRewardSection(true);
			}
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtRewardText","btnRewardApply"), checkoutPage),
					"To check fields of the 'Have a Reward certificate' section.",
					"The 'Reward Code' text box and 'Apply Button' are present in the 'Have a Reward certificate' section!",
					"Some fields are not present in the 'Have a Reward certificate' section", driver);
	
			//Step 3
			checkoutPage.applyRewardCertificate(reward_valid); //correct Certificate
	
			if(driver.getCurrentUrl().contains("Billing")) {
				Log.failsoft("Reward Certificate is not applying - PXSFCC-2814");
			} else {
				//Step 4 and 5
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("rewardAppliedValue","txtRewardValueApply"), checkoutPage),
						"To check the reward certificate components are displaying.",
						"The reward certificate components are displaying!",
						"The reward certificate components are is not displaying", driver);
	
				//Step 3
				checkoutPage.removeAppliedReward();
	
				Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("rewardAppliedValue"), checkoutPage),
						"To check the reward certificate is removed or not.",
						"Remove link is clickable and the reward certificate is removed!",
						"The reward certificate is not removed!", driver);
	
				//Step 4 and 5
				checkoutPage.expandOrColapseRewardSection(true);
				checkoutPage.typeOnRewardTxtbox(reward_invalid);
				checkoutPage.clickOnRewardTxtbox();
	
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtRewardError"), checkoutPage),
						"To check the reward certificate error message is displaying.",
						"The reward certificate error message is displaying!",
						"The reward certificate error message is not displaying", driver);
	
				//Step 5 is verified in Step 4 itself
	
				//Step 2
				checkoutPage.clickRewardSectionToolTip();
				Utils.waitForPageLoad(driver);
	
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("rewardToolTipPopUp"), checkoutPage),
						"To check system redirected to tooltip section.",
						"The 'Tooltip' section is displaying!",
						"The 'Tooltip' section is not displaying", driver);
	
				if (driver.getCurrentUrl().contains("tooltip")) {
					Log.failsoft("Page navigating to new tab for tooltip - PXSFCC-1881");
				} else {
					checkoutPage.closeRewardSectionToolTip();
				}
			}
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// TC_FBB_DROP4_C22518
}// search
