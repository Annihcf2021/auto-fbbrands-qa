package com.fbb.testscripts.drop4;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22752 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22752(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		// Load Test Data
		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		String searchKey = TestData.get("prd_high_cost");
		String valid_reward = TestData.get("reward_valid-1");
		String valid_coupon = TestData.get("cpn_promo-ship-level");
		String credential = username + "|" + password;
		
		//Pre-requisite - Account Should have more than one payment information
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, credential);
		}
		
		int i = 1;
		try {
			
			Object[] obj = GlobalNavigation.checkout(driver, searchKey, i, credential);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
			
			
			checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", ShippingMethod.Standard, "valid_address7");
			Log.message(i++ + ". Shipping Address entered successfully", driver);

			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);

			checkoutPage.fillingCardDetails1("card_Visa", false, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);

			//Adding the new reward certificate
			if(!BrandUtils.isBrand(Brand.el)) {
				checkoutPage.expandCollapseRewardCertSection("expand");
				checkoutPage.applyRewardCertificate(valid_reward);
			}
			
			//Adding the new promo code
			checkoutPage.expandCollapsePromoCodeSection("expand");
			checkoutPage.applyPromoCouponCode(valid_coupon);
			
			//Adding the new gift card
			checkoutPage.expandCollapseGiftCardSection("expand");
			checkoutPage.applyGiftCardByValue(TestGiftCardValue.$10);
			
			checkoutPage.fillingCardDetails1("card_Visa", false, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "placeOrderlnkEditBillingAddress", "placeOrderBillingHeading", checkoutPage), 
					"Payment Details section Heading should be displayed on the top left corner Payment Details section.", 
					"Payment Details section Heading is displayed on the top left corner Payment Details section.", 
					"Payment Details section Heading is not displayed on the top left corner Payment Details section", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "placeOrderlnkEditBillingAddress", "placeOrderBillingHeading", checkoutPage), 
					"The Edit link should be displayed on the right side end of the Payment Details section Heading.", 
					"The Edit link is displayed on the right side end of the Payment Details section Heading.", 
					"The Edit link is not displayed on the right side end of the Payment Details section Heading.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "placeOrderBillingHeading", "placeorderBillingSectionHeading", checkoutPage),
					"The Billing Address Module should be displayed below the payment heading.",
					"The Billing Address Module is displayed below the payment heading.",
					"The Billing Address Module is not displayed below the payment heading.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "placeorderBillingSectionHeading", "placeorderCardSection", checkoutPage),
					"The Credit Card/PayPal Module should be displayed below the Billing Address Module.",
					"The Credit Card/PayPal Module is displayed below the Billing Address Module.",
					"The Credit Card/PayPal Module is not displayed below the Billing Address Module", driver);
			
			if(!BrandUtils.isBrand(Brand.el)) {
				if(Utils.isMobile()) {
					Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "placeorderCardSection", "placeorderRewardCertificate", checkoutPage),
							"The Reward Certificate Module should be displayed below the Billing Address Module.",
							"The Reward Certificate Module is displayed below the Billing Address Module.",
							"The Reward Certificate Module is not displayed below the Billing Address Module", driver);
				}
				else {
					Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "placeorderRewardCertificate", "placeorderCardSection", checkoutPage), 
							"The Reward Certificate Module should be displayed right side the Billing Address Module", 
							"The Reward Certificate Module is displayed right side the Billing Address Module", 
							"The Reward Certificate Module is displayed right side the Billing Address Module", driver);
				}
			}
			
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "placeorderCardSection", "placeorderpromoCode", checkoutPage),
						"The Promo code Module should be displayed below the Billing Address Module.",
						"The Promo code Module is displayed below the Billing Address Module.",
						"The Promo code Module is not displayed below the Billing Address Module", driver);
			}
			else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "placeorderpromoCode", "placeorderCardSection", checkoutPage), 
						"The Promo code Module should be displayed right side the Billing Address Module", 
						"The Promo code Module is displayed right side the Billing Address Module", 
						"The Promo code Module is displayed right side the Billing Address Module", driver);
			}
			
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "placeorderCardSection", "placeorderGiftCard", checkoutPage),
						"The gift card Module should be displayed below the Billing Address Module.",
						"The gift card Module is displayed below the Billing Address Module.",
						"The gift card Module is not displayed below the Billing Address Module", driver);
			}
			else{
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "placeorderGiftCard", "placeorderCardSection", checkoutPage), 
						"The gift card Module should be displayed right side the Billing Address Module", 
						"The gift card Module is displayed right side the Billing Address Module", 
						"The gift card Module is displayed right side the Billing Address Module", driver);
			}
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
		GlobalNavigation.RemoveAllProducts(driver);
		Log.endTestCase(driver);
		} // finally
	
	}
}// search
