package com.fbb.testscripts.drop4;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22500 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22500(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + prd_any, driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Bag.", driver);
			
			ShoppingBagPage shoppingbagPage=homePage.headers.clickOnBagLink();
			Log.message(i++ + ". Navigated to Shipping Cart page.", driver);
			
			SignIn signin=(SignIn)shoppingbagPage.navigateToCheckout();
			Log.message(i++ +". Clicked checkout now button.", driver);
	
			//Step-1: Verify the functionality of Account Heading
	
			Log.softAssertThat(signin.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "stickyNavigationBar", "accountHeading", signin), 
					"Account heading should be displayed down to the sticky nav.",
					"Account heading is displayed down to the sticky nav.",
					"Account heading is not displayed down to the sticky nav.", driver);
	
			//Step-2: Verify the functionality of Account Sub Heading				
	
			Log.softAssertThat(signin.elementLayer.verifyVerticalAllignmentOfElements(driver, "accountHeading", "accountSubHeading", signin), 
					"Account Sub Heading should be displayed down to the Account Heading.",
					"Account Sub Heading is displayed down to the Account Heading.",
					"Account Sub Heading is not displayed down to the Account Heading.", driver);
	
			//Step-3: Verify the functionality of Checkout Brands			
	
			Log.softAssertThat(signin.elementLayer.verifyVerticalAllignmentOfElements(driver, "accountSubHeading", "checkoutBrands", signin), 
					"Checkout Brands should be displayed down to the Account Sub Heading.",
					"Checkout Brands is displayed down to the Account Sub Heading",
					"Checkout Brands is not displayed down to the Account Sub Heading", driver);
	
			//Step-4: Verify the functionality of Registered User Sign-in Heading 
	
			Log.softAssertThat(signin.elementLayer.verifyVerticalAllignmentOfElements(driver, "checkoutBrands", "registeredUser", signin), 
					"Registered Sign-in Heading should be displayed down to the Checkout Brands.",
					"Registered Sign-in Heading is displayed down to the Checkout Brands.",
					"Registered Sign-in Heading is not displayed down to the Checkout Brands.", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	}
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP4_C22500(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String[] unregistered_email_pass = {"unregisterd@yopmail.com", "test123#"};
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String invalidEmail = "abcdef";
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + prd_any, driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Bag.", driver);
			
			ShoppingBagPage shoppingbagPage=homePage.headers.clickOnBagLink();
			Log.message(i++ + ". Navigated to Shipping Cart page.", driver);
			
			SignIn signin=(SignIn)shoppingbagPage.navigateToCheckout();
			Log.message(i++ +". Clicked checkout now button.", driver);
			
			//Step-5: Verify the functionality of Email Address Field and Password
			Log.softAssertThat(signin.verifyEmailAndPasswordFielsareEditable(),
					"User should be able to enter text in the Email Address and Password",
					"User is able to enter text in the Email Address and Password",
					"User is not able to enter text in the Email Address and Password",driver);
			
			//mandatory field span.error
			signin.clickSignInButton();
			Log.message(i++ +". Clicked SignIn button.");
			Log.softAssertThat(signin.verifyEmailandPasswordareMandatory(),
					"User should be able to enter text in the Email Address and Password",
					"User is able to enter text in the Email Address and Password",
					"User is not able to enter text in the Email Address and Password",driver);
			//
			Log.softAssertThat(signin.verifyEmailPlaceholderMovestotheTop(),					 
					"Place holder text \"Email Address\" should move to the top portion of the text box when User starts typing in the Email Address field", 
					"Place holder text moved to the top", 
					"Place holder text did not move to the top", driver);
	
			signin.typeOnEmail(invalidEmail);
			Log.message(i++ +". Enterd invalid EMail");
			signin.typeOnPassword("");
			Log.softAssertThat(signin.elementLayer.verifyElementTextContains("lblLoginEmailPlaceholder", "Please enter a valid email", signin), 
					"System should perform regex validation for properly formatted email address", 
					"System performed regex validation for properly formatted email address", 
					"System didn't perform regex validation for properly formatted email address", driver);
			
			signin.typeOnEmail(unregistered_email_pass[0]);
			Log.message(i++ +". Filled Username");
			signin.typeOnPassword(unregistered_email_pass[1]);
			Log.message(i++ +". Filled Password");
			signin.clickSignInButton();
			Log.message(i++ +". Clicked SignIn button.");
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("txtSignInError"), signin),					 
					"If user try to use a non register email address, system will display error message", 
					"Error message is displayed for a non registered user", 
					"Error message is not displayed for a non registered user", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	}
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP4_C22500(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + prd_any, driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Bag.", driver);
			
			ShoppingBagPage shoppingbagPage=homePage.headers.clickOnBagLink();
			Log.message(i++ + ". Navigated to Shipping Cart page.", driver);
			
			SignIn signin=(SignIn)shoppingbagPage.navigateToCheckout();
			Log.message(i++ +". Clicked checkout now button.", driver);
	
			//If a registered email address is entered and an invalid password is supplied, the User 
			//should be taken to the 'Forgot Password' page
			//step-6: Verify the functionality of Password Field
			//mask
			signin.typeOnPassword("asdfghij");
			Log.message(i++ +". Successfully Entered Password.", driver);
			signin.clickSignInButton();
			Log.softAssertThat(signin.verifyPasswordisMaskedorUnmasked("mask"),					 
					"By default the User entered password should be masked", 
					"The password is masked", 
					"The password is not masked", driver);
	
			//blank
			signin.typeOnPassword("");
			Log.message(i++ + ". Password field cleared.", driver);
			signin.clickSignInButton();
			Log.softAssertThat(!(signin.elementLayer.verifyTextContains("txtEmailError","Password", signin)),
					"On leaving password field blank, appropriate error message should be displayed", 
					"Appropriate error message is displayed", 
					"Appropriate error message is not displayed", driver);
			//invalid
			signin.typeOnPassword("asdfasdf");
			Log.message(i++ +". Invalid password entered.", driver);
			signin.clickSignInButton();
			Log.softAssertThat(!(signin.elementLayer.verifyTextContains("txtinvalidpassword","Please Enter at least 1 letter and 1 number.", signin)),
					"For invalid Password, appropriate error message should be displayed", 
					"Appropriate error message is displayed", 
					"Appropriate error message is not displayed", driver);
	
			//
			signin.typeOnPassword(accountData.get("password_global"));
			Log.message(i++ +". Password entered successfully.", driver);
			signin.clickSignInButton();
			Log.softAssertThat(signin.verifyPasswordPlaceholderMovestotheTop(),					 
					"Place holder text \"Password\" should move to the top portion of the text box when User starts typing in the Password field", 
					"Place holder text moved to the top", 
					"Place holder text did not move to the top", driver);
	
			//Step-7: Verify the functionality of Show/Hide password Button
			Log.softAssertThat((signin.elementLayer.verifyPageElements(Arrays.asList("showpassword"), signin)&&
					signin.elementLayer.verifyTextContains("showpassword","Show", signin)),					 
					"Show Password button should be visible", 
					"Show Password button is visible", 
					"Show Password button is not visible", driver);
	
			signin.clickShowPassword();
			Log.message(i++ +". Clicked Show Password", driver);
			Log.softAssertThat(signin.verifyPasswordisMaskedorUnmasked("unmask"),					 
					"The User can use the Show Password link to reveal the entered value", 
					"The password is unmasked", 
					"The password is not unmasked", driver);
	
			Log.softAssertThat((signin.elementLayer.verifyTextContains("showpassword","Hide", signin)),
					"The link text 'Show' should be replaced with the link text 'Hide'", 
					"The link text 'Show' is replaced with the link text 'Hide'", 
					"The link text 'Show' is not replaced with the link text 'Hide'", driver);
	
			signin.clickShowPassword(); 
			Log.message(i++ +". Clicked Hide Password", driver);
			Log.softAssertThat(signin.verifyPasswordisMaskedorUnmasked("mask"),					 
					"On clicking the Hide link, System should mask the password and hide the User entered data", 
					"The password is masked", 
					"The password is not masked", driver);
	
			Log.softAssertThat((signin.elementLayer.verifyTextContains("showpassword","Show", signin)),
					"The link text 'Hide' should be replaced with the link text 'Show'", 
					"The link text 'Hide' is replaced with the link text 'Show'", 
					"The link text 'Hide' is not replaced with the link text 'Show'", driver);
	
			//Step-8: Verify the functionality of Remember Me check box
			//verifyPageElementsChecked
			Log.softAssertThat(!signin.elementLayer.verifyPageElementsChecked(Arrays.asList("cbRememberme"), signin),
					"By default, the 'Remember Me' checkbox should be unchecked", 
					"The 'Remember Me' checkbox is unchecked", 
					"The 'Remember Me' checkbox is not unchecked", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	}
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP4_C22500(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String[] unregistered_email_pass = {"unregisterd@yopmail.com", "test123#"};
		String validPassword = accountData.get("password_global");
		//Password error color
		String pwderrorclr="E603C";
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + prd_any, driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Bag.", driver);
			
			ShoppingBagPage shoppingbagPage=homePage.headers.clickOnBagLink();
			Log.message(i++ + ". Navigated to Shipping Cart page.", driver);
			
			SignIn signin=(SignIn)shoppingbagPage.navigateToCheckout();
			Log.message(i++ +". Clicked checkout now button.", driver);
	
			//Step-9: Verify the functionality of Sign In Button
			//Invalid Email Address and Invalid Password
			signin.typeOnEmail("asdf@yop");
			signin.typeOnPassword("asdf");
			Log.message(i++ +". Invalid email address and password entered.", driver);
			
			signin.clickSignInButton();
			//signin  = new SignIn(driver).get();
			Log.message(i++ +". Clicked SignIn button.", driver);
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("txtEmailError"), signin),					 
					"User should not be able to sign in for Invalid Email Address and Invalid Password", 
					"User is not able to sign in for Invalid Email Address and Invalid Password", 
					"User is able to sign in for Invalid Email Address and Invalid Password", driver);
	
			//Invalid Email Address and Valid Password
			signin.typeOnEmail("asdf@yop");
			signin.typeOnPassword(accountData.get("password_global"));
			Log.message(i++ +". Invalid email address and valid password entered.", driver);
			
			signin.clickSignInButton();
			//signin  = new SignIn(driver).get();
			Log.message(i++ +". Clicked SignIn button.", driver);
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("txtEmailError"), signin),					 
					"User should not be able to sign in for Invalid Email Address and Valid Password", 
					"User is not able to sign in for Invalid Email Address and Valid Password", 
					"User is able to sign in for Invalid Email Address and Valid Password", driver);
	
			//Invalid Email Address and Password Blank
			signin.typeOnEmail("asdf@yop");
			signin.typeOnPassword("");
			Log.message(i++ +". Invalid email address entered and password field cleared.", driver);
			
			signin.clickSignInButton();
			//signin  = new SignIn(driver).get();
			Log.message(i++ +". Clicked SignIn button.", driver);
			
			
	        Log.softAssertThat((signin.elementLayer.verifyPageElements(Arrays.asList("txtEmailError"), signin))&& (signin.elementLayer.verifyElementColor("errorpasswordlabel", pwderrorclr, signin)),					 
					"User should not be able to sign in for Invalid Email Address and Password Blank", 
					"User is not able to sign in for Invalid Email Address and Password Blank", 
					"User is able to sign in for Invalid Email Address and Password Blank", driver);
			
				
			//Valid Email Address and Invalid Password
			signin.typeOnEmail(unregistered_email_pass[0]);
			signin.typeOnPassword("asdfasdf");
			Log.message(i++ +". Valid email address and invalid password entered.", driver);
			
			signin.clickSignInButton();
			//signin  = new SignIn(driver).get();
			Log.message(i++ +". Clicked SignIn button.", driver);
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("txtSignInError"), signin),					 
					"User should not be able to sign in for Valid Email Address and Invalid Password", 
					"User is not able to sign in for Valid Email Address and Invalid Password", 
					"User is able to sign in for Valid Email Address and Invalid Password", driver);
	
	
			//Valid Email Address and Password Blank
			signin.typeOnEmail(unregistered_email_pass[0]);
			signin.typeOnPassword("");
			Log.message(i++ +". Valid email address entered and password field cleared.", driver);
			
			signin.clickSignInButton();
			//signin  = new SignIn(driver).get();
			Log.message(i++ +". Clicked SignIn button.", driver);
			
			Log.softAssertThat(signin.elementLayer.verifyElementColor("errorpasswordlabel", pwderrorclr, signin),					 
					"User should not be able to sign in for Valid Email Address and Password Blank", 
					"User is not able to sign in for Valid Email Address and Password Blank", 
					"User is able to sign in for Valid Email Address and Password Blank", driver);
	
			//Email Address Blank and Invalid Password
			signin.typeOnEmail("");
			signin.typeOnPassword("asdfasdf");
			Log.message(i++ +". Invalid password entered and Email field left blank.", driver);
			
			signin.clickSignInButton();
			//signin  = new SignIn(driver).get();
			Log.message(i++ +". Clicked SignIn button.", driver);
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("txtEmailError"), signin),					 
					"User should not be able to sign in for Email Address Blank and Invalid Password", 
					"User is not able to sign in for Email Address Blank and Invalid Password", 
					"User is able to sign in for Email Address Blank and Invalid Password", driver);
	
			//Email Address Blank and Valid Password
			signin.typeOnEmail("");
			signin.typeOnPassword(validPassword);
			Log.message(i++ +". Valid password entered and Email field left blank", driver);
			
			signin.clickSignInButton();
			//signin  = new SignIn(driver).get();
			Log.message(i++ +". Clicked SignIn button.", driver);
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("txtEmailError"), signin),					 
					"User should not be able to sign in for Email Address Blank and Valid Password", 
					"User is not able to sign in for Email Address Blank and Valid Password", 
					"User is able to sign in for Email Address Blank and Valid Password", driver);
	
			//Email Address Blank and Password Blank
			signin.typeOnEmail("");
			signin.typeOnPassword("");
			Log.message(i++ +". Both email and password fields left blanks", driver);
			
			signin.clickSignInButton();
			//signin  = new SignIn(driver).get();
			Log.message(i++ +". Clicked SignIn button.", driver);
			Log.softAssertThat((signin.elementLayer.verifyPageElements(Arrays.asList("txtEmailError"), signin))&& (signin.elementLayer.verifyElementColor("errorpasswordlabel", pwderrorclr, signin)),					 
					"User should not be able to sign in for Email Address Blank and Password Blank", 
					"User is not able to sign in for Email Address Blank and Password Blank", 
					"User is able to sign in for Email Address Blank and Password Blank", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	}
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M5_FBB_DROP4_C22500(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		//String InvalidEmail = "abc@def.com";
		
		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, email + "|" + password);
		}
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prd_any);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + prd_any, driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Bag.", driver);
			
			ShoppingBagPage shoppingbagPage=homePage.headers.clickOnBagLink();
			Log.message(i++ + ". Navigated to Shipping Cart page.", driver);
			
			SignIn signin=(SignIn)shoppingbagPage.navigateToCheckout();
			Log.message(i++ +". Clicked checkout now button.", driver);
	
			//Step-10: Verify the functionality of Forgot Password Button
			signin.clickOnForgetPassword();
			Utils.waitForPageLoad(driver);
			Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("divForgotPassword"), signin),					 
					"On clicking/tapping the 'Forgot your password?' link, the User should be navigated to the \"Password Recovery landing page\"", 
					"User navigated to the \"Password Recovery landing page\"", 
					"User could not navigated to the \"Password Recovery landing page\"", driver);
	
			//If User entered email address is not an existing customer account, then system should display the appropriate error message - CANNOT be validated in QA
			/*signin.typeEmailaddrsinRequestPassword(InvalidEmail);
			Log.softAssertThat(!(signin.elementLayer.verifyPageElements(Arrays.asList("emailTbErrorRequestPassword"), signin)),					 
					"On clicking/tapping the 'Forgot your password?' link, the User should be navigated to the \"Password Recovery landing page\"", 
					"User navigated to the \"Password Recovery landing page\"", 
					"User could not navigated to the \"Password Recovery landing page\"", driver);*/
	
			signin.typeEmailaddrsinRequestPassword(email);
			signin.clickSendRequestPassword();
			Log.softAssertThat(signin.elementLayer.verifyElementDisplayed(Arrays.asList("headingRequestPassword"), signin),
					"If User entered email address is an existing customer account, then user should be able to recover the password", 
					"The user is able to recover the password", 
					"The user is not able to recover the password", driver);
			
			//Step-11: Verify the functionality of Facebook Login
			Log.reference("Facebook login currently not available in build.");
			
			shoppingbagPage = homePage.headers.navigateToShoppingBagPage();
			
			signin=(SignIn)shoppingbagPage.navigateToCheckout();
			Log.message(i++ +". Clicked checkout now button.", driver);
			
			//Step-12: Verify the functionality of Order Summary Module
			if(Utils.isDesktop()) {
				Log.softAssertThat(signin.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divCheckoutLoginOrderSummery", "accountHeading", signin), 
						"Order Summary Module should be displayed to the top right corner of the check out login page.", 
						"Order Summary Module is displayed to the top right corner of the check out login page.", 
						"Order Summary Module is not displayed to the top right corner of the check out login page.", driver);
			} else {
				Log.softAssertThat(signin.elementLayer.verifyVerticalAllignmentOfElements(driver, "accountHeading", "divCheckoutLoginOrderSummery", signin), 
						"Order Summary Module should be displayed below the items in the shopping bag information.", 
						"Order Summary Module is displayed below the items in the shopping bag information.", 
						"Order Summary Module is not displayed below the items in the shopping bag information.", driver);
			}
			
			BrowserActions.scrollToBottomOfPage(driver);
			
			if (Utils.isDesktop()) {
				Log.softAssertThat(signin.elementLayer.verifyCssPropertyForElement("divCheckoutLoginOrderSummery", "position", "Fixed", signin),  
						"On scrolling, Order summary module should remain fixed.", 
						"On scrolling, Order summary module remained fixed.", 
						"On scrolling, Order summary module didn't remain fixed.", driver);
			}
			
			Log.reference("Full functionality covered in Test Case Id: C22550 Step 1 to 15");
			Log.reference("Full functionality covered in Test Case Id: C22551 Step 1 to 4");
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	}
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M6_FBB_DROP4_C22500(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String validPassword = accountData.get("password_global");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String[] registered_email_pass = {AccountUtils.generateEmail(driver), validPassword};
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//User Registration
			GlobalNavigation.registerNewUser(driver,0,0,AccountUtils.generateEmail(driver)+"|"+validPassword);
	
			PdpPage pdpPage = homePage.headers.redirectToPDP(prd_any, driver);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + prd_any, driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Bag.", driver);
			
			ShoppingBagPage shoppingbagPage=homePage.headers.clickOnBagLink();
			Log.message(i++ + ". Navigated to Shipping Cart page.", driver);
			
			SignIn signin=(SignIn)shoppingbagPage.navigateToCheckout();
			Log.message(i++ +". Clicked checkout now button.", driver);
	
			//Step-13: Verify the functionality of 'Signed In As' Heading
			signin.typeOnEmail(registered_email_pass[0]);
			Log.message(i++ +". Filled Username");
			signin.typeOnPassword(registered_email_pass[1]);
			Log.message(i++ +". Filled Password");
			signin.clickSignInButton();
			
			Log.softAssertThat(signin.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("accountHeading"), signin), 
					"Once the User is successfully logged in, the Sign In section should vanish", 
					"Once the User is successfully logged in, the Sign In section vanished", 
					"Once the User is successfully logged in, the Sign In section did not vanish", driver);
			
			BrowserActions.scrollToTopOfPage(driver);
			Log.softAssertThat(signin.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "stickyNavigationBar", "lblSignedInAsHeading", signin), 
					"Signed In As Heading should be displayed below the sticky nav when the user is logged in",
					"Signed In As Heading is displayed below the sticky nav when the user is logged in",
					"Signed In As Heading is not displayed below the sticky nav when the user is logged in", driver);
			
			//Step-14: Verify the display of Signed In Email Address
			Log.softAssertThat(signin.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblSignedInAsHeading", "lblWelcomeEmail", signin), 
					"Signed In Email Address should be displayed under Signed In As Heading",
					"Signed In Email Address is displayed under Signed In As Heading",
					"Signed In Email Address is not displayed under Signed In As Heading", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	}
}// search
