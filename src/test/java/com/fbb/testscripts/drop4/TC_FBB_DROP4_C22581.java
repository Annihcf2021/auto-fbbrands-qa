package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22581 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22581(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_expedited-ship-eligible") + "|" + TestData.get("prd_expedited-ship-ineligible");  
		String email;
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		email = AccountUtils.generateEmail(driver);
	
		int i=1;
		try {
			Object[] obj = GlobalNavigation.checkout(driver, searchKey, i, email);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (Integer) obj[1];
	
			checkoutPage.fillingShippingDetailsAsGuest("YES", "valid_address1", ShippingMethod.Express);
			Log.message(i++ + ". Shipping details filled successfully.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shippingmethodExceptionOverlay"), checkoutPage), 
					"Shipping Method Exception overlay should be displayed.", 
					"Shipping Method Exception overlay is dsiplayed.", 
					"Shipping Method Exception overlay is not displayed.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "shippingmethodExceptionOverlayHeading", "shippingMethodExceptionItemIntroduction", checkoutPage), 
					"Method Exception Heading message should be displayed on the top of the Shipping Method exception overlay in a gray boxed section", 
					"Method Exception Heading message is displayed on the top of the Shipping Method exception overlay in a gray boxed section",
					"Method Exception Heading message is not displayed on the top of the Shipping Method exception overlay in a gray boxed section", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "shippingmethodExceptionOverlayHeading", "shippingMethodExceptionItemIntroduction", checkoutPage), 
					"Item Introduction Text should be displayed below the Method Exception Heading message", 
					"Item Introduction Text is displayed below the Method Exception Heading message",
					"Item Introduction Text is not displayed below the Method Exception Heading message", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("updateShippingMethodName", "rgba(230, 0, 60, 1)", checkoutPage), 
					"The Updated Shipping Method should be displayed next to the Item introduction text in red font color.", 
					"The Updated Shipping Method is displayed next to the Item introduction text in red font color.",
					"The Updated Shipping Method is not displayed next to the Item introduction text in red font color.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("OverlayCancelButton"), checkoutPage), 
					"Cancel button should displayed in the shipping method overlay!", 
					"Cancel button is displayed in the shipping method overlay!", 
					"Cancel button is not displayed in the shipping method overlay!", driver);
	
			checkoutPage.clickShippingMethoCancelBtn();
			Log.message(i++ + ". Clicked on Cancel button.", driver);
	
			Log.softAssertThat(!checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shippingmethodExceptionOverlay"), checkoutPage), 
					"Shipping Method Exception overlay should not displayed.", 
					"Shipping Method Exception overlay is not dsiplayed.", 
					"Shipping Method Exception overlay is displayed.", driver);
	
			checkoutPage.checkUncheckExpressDelivery(true);
			Log.message(i++ + ". Checked on Express delivery option", driver);
	
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnContinueExpeditedOverlay", "OverlayCancelButton", checkoutPage), 
						"Continue button should be displayed to the right of the Cancel button.", 
						"Continue button is displayed to the right of the Cancel button",
						"Continue button is not displayed to the right of the Cancel button", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnContinueExpeditedOverlay", "OverlayCancelButton", checkoutPage), 
						"Continue button should be displayed to the right of the Cancel button.", 
						"Continue button is displayed to the right of the Cancel button",
						"Continue button is not displayed to the right of the Cancel button", driver);
			}
	
			checkoutPage.clickContinueInPOBoxOverlay();
			Log.message(i++ + ". Clicked on Continue in PO Box overlay.", driver);
	
			Log.softAssertThat(!checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shippingmethodExceptionOverlay"), checkoutPage), 
					"Shipping Method Exception overlay should not displayed after clicking the continue button.", 
					"Shipping Method Exception overlay is not dsiplayed after clicking the continue button.", 
					"Shipping Method Exception overlay is displayed after clicking the continue button.", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Billing/Payment Page.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("divDeliveryOptions"), checkoutPage), 
					"Delivery option should displayed after continue the payment.", 
					"Delivery option is displayed after continue the payment.", 
					"Delivery option is not displayed after continue the payment.", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}
}// search
