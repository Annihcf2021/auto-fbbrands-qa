package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22520 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22520(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prodId = TestData.get("prd_variation");
		String guestEmail;
		String[] valid_gift_card = TestData.get("gift_card_valid_1").split("\\|"); 
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		guestEmail = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			ShoppingBagPage shoppingBagPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ +". Navigated to the shopping bag page", driver);
	
			shoppingBagPage.removeAllItemsFromCart();
			Log.message(i++ +". Removing all the cart products", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prodId);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			/*pdpPage.selectSizeBasedOnIndex(0);
			Log.message(i++ +". Selected a size", driver);
	
			pdpPage.selectColorBasedOnIndex(0);
			Log.message(i++ +". Selected a color", driver);
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ +". Click on add product to bag", driver);
	
			shoppingBagPage = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ +". Click on Checkout now in overlay", driver);*/
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". Product added to bag", driver);
			
			homePage.headers.clickOnBagLink();
			Log.message(i++ +". Navigated to cart page", driver);
			
	
			CheckoutPage checkoutPage = shoppingBagPage.clickOnCheckoutNowBtn();
			Log.message(i++ +". Click on Checkout now", driver);
	
			checkoutPage.enterGuestUserEmail(guestEmail);
			Log.message(i++ +". Entering Guest email", driver);
	
			checkoutPage.continueToShipping();
			Log.message(i++ +". Click on Continue to Shipping", driver);
	
			checkoutPage.fillingShippingDetailsAsGuest("No" , "valid_address7", ShippingMethod.Standard);
			Log.message(i++ +". Filling shipping details", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ +". Click on Continue to payment", driver);
			//Step 1a
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("giftCardSectionHeading"), checkoutPage),
					"To check gift card heading 'Have a Gift Card' is getting displayed.",
					"The gift card heading 'Have a Gift Card' is displaying!",
					"The gift card heading 'Have a Gift Card' is not displaying", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementTextEqualTo("giftCardSectionHeading", "Have a gift Card?", checkoutPage),
					"To check gift card heading is correct.",
					"The gift card heading is correct!",
					"The gift card heading is not correct", driver);
	
			Log.softAssertThat((!(checkoutPage.checkGiftCardSectionExpadedOrNot())),
					"To verify the 'Have a Gift Card' section is collapsed.",
					"The 'Have a Gift Card' section is collapsed and arrow is downward.",
					"The 'Have a Gift Card' section is not collapsed and arrow is not downward.", driver);
	
			checkoutPage.expandOrColapseGiftCardSection(true);
			Log.message(i++ +". Expanding Gift Card section", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lnkGiftCardRemove"), checkoutPage),
					"To check the gift card is collapsed when gift card not applied.",
					"The gift card is collapsed when no gift card applied!",
					"The gift card is not collapsed when no gift card applied!", driver);
	
			Log.softAssertThat((checkoutPage.checkGiftCardSectionExpadedOrNot()),
					"To verify the 'Have a Gift Card' section is collapsed.",
					"The 'Have a Gift Card' section is expanded and arrow is upward.",
					"The 'Have a Gift Card' section is not expanded and arrow is not upward.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtGiftCardNumb","txtGiftCardPin","btnGiftCardApply"), checkoutPage),
					"To check fields of the 'Have a Gift Card' section.",
					"The 'Gift card Number' text box, 'Pin Number' Text box and 'Apply Button' are present in the 'Have a Gift card' section!",
					"Some fields are not present in the 'Have a Gift card' section", driver);
	
			//Step 2
			checkoutPage.clickGiftCardSectionToolTip();
			Log.message(i++ +". Opening Gift card tooltip", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkGiftCardToolTipClose"), checkoutPage),
					"To check system redirected to tooltip section.",
					"The 'Tooltip' section is displaying!",
					"The 'Tooltip' section is not displaying", driver);
	
			checkoutPage.closeGiftCardSectionToolTip();
			Log.message(i++ +". Closing Gift card tooltip", driver);
			//Step 3
			checkoutPage.expandOrColapseGiftCardSection(true);
			Log.message(i++ +". Expanding Gift Card section", driver);
	
			checkoutPage.applyGiftCardByValue(TestGiftCardValue.valid);
	
			checkoutPage.expandOrColapseGiftCardSection(false);
			Log.message(i++ +". Collapsing Gift Card section", driver);
	
			Log.softAssertThat(!checkoutPage.checkGiftCardSectionExpadedOrNot(),
					"To check gift card section can be collapsed after applying the gift card.",
					"The gift card section can be collapsed after applying the gift card!",
					"The gift card section cannot collapse after applying the gift card", driver);
	
			//Step 4 and 5
	
			checkoutPage.expandOrColapseGiftCardSection(true);
			Log.message(i++ +". Expanding Gift Card section", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "giftCardSectionHeading", "txtAppliedGiftCardNumber", checkoutPage),
					"To check the applied gift card should be display below the heading.",
					"The applied gift card is displaying below the heading!",
					"The applied gift card is not displaying below the heading", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtAppliedGiftCardNumber", "lnkGiftCardValueApplied", checkoutPage),
					"To check the applied gift card value should be display below gift card number.",
					"The applied gift card value is displaying below gift card number!",
					"The applied gift card value is not displaying below gift card number", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkGiftCardValueApplied", "lnkGiftCardRemainingBalance", checkoutPage),
					"To check the applied gift card balance should be display below gift card value.",
					"The applied gift card balance is displaying below gift card value!",
					"The applied gift card balance is not displaying below gift card value", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkGiftCardRemove", "txtAppliedGiftCardNumber", checkoutPage),
					"To check the applied gift card number should be display right to gift card remove link.",
					"The applied gift card number is displaying right to gift card remove link!",
					"The applied gift card number is not displaying right to gift card remove link", driver);
	
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtAppliedGiftCardNumber", "txtGiftCardNumb", checkoutPage),
						"To check the applied gift card should be display above to gift card fields.",
						"The applied gift card is displaying above to gift card fields!",
						"The applied gift card is not displaying above to gift card fields", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtGiftCardNumb", "txtAppliedGiftCardNumber", checkoutPage),
						"To check the applied gift card should be display left to gift card fields.",
						"The applied gift card is displaying left to gift card fields!",
						"The applied gift card is not displaying left to gift card fields", driver);
			}
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtGiftCardNumbPlaceHolder","txtGiftCardPinPlaceHolder"), checkoutPage),
					"To check the gift card number and pin text field should display placeholder.",
					"The gift card number and pin text field is displaying placeholder!",
					"The gift card number and pin text field is not displaying placeholder", driver);
	
			checkoutPage.clickApplyGiftCard();
			Log.message(i++ +". Clicked on Apply Gift card", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkInvalidGiftCardError"), checkoutPage),
					"To check the gift card error message is displaying.",
					"The gift card error message is displaying!",
					"The gift card error message is not displaying", driver);
	
			checkoutPage.applyGiftCardNumbAndPin(valid_gift_card[0], "1111"); //wrong Certificate
			Log.message(i++ +". Applying wrong Gift card", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkInvalidGiftCardError"), checkoutPage),
					"To check the gift card error message is displaying.",
					"The gift card wrong error message is displaying!",
					"The gift card wrong error message is not displaying", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
	
			if (checkoutPage.getRemainingOrderAmount() == 0.00) {
				checkoutPage.clickOnPaymentDetailsContinueBtn();
				Log.message(i++ +". Click on the Continue button in Payment", driver);
	
				checkoutPage.clickOnPlaceOrderButton();
				Log.message(i++ +". Click on the Place order button", driver);
	
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElemForOrdCnfPg"), checkoutPage),
						"To check the order is placed.",
						"The order is placed!",
						"The order is not placed", driver);
			} else {
				Log.message("The Order amount is not reduced to zero!", driver);
			}
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// TC_FBB_DROP4_C22520
}// search
