package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C21552 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C21552(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_markdown_single_price = TestData.get("prd_markdown-single-price");
		String[] payment = checkoutProperty.get("card_Visa").split("\\|");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			String credential = AccountUtils.generateEmail(driver);
			//Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_markdown_single_price, i, credential);
			Object[] obj = GlobalNavigation.checkout(driver, prd_markdown_single_price, i, credential);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			
			String cardNumberTooLong = "01020304050607080910";
			String inputTextOnly = "abcd";
			String inputNumberOnly = "1234";
			
			i = (int) obj[1];
	
			checkoutPage.fillingShippingDetailsAsGuest("YES", "valid_address7", ShippingMethod.Standard);
			Log.message(i++ + ". Entered Shipping Address successfully", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment section.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("chkBoxMakeThisDefaultPayment","chkBoxSaveCard","savedCards","lnkCancelInPayments"), checkoutPage), 
					"To check 'Make Default', 'Save Address', 'Cancel Link', 'Saved Card Info' are not displaying if user checked out as guest user",
					"The 'Make Default', 'Save Address', 'Cancel Link', 'Saved Card Info' are not displaying if user checked out as guest user",
					"The some fields are displaying", driver);
			
			//Step-1: Verify the functionality of Select Credit Card Type dropdown
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divAcceptedPayments", "selectCardType", checkoutPage), 
						"The Select Card Type dropdown should be displayed bellow the Payment Methods Accepted content", 
						"The Select Card Type dropdown is displayed bellow the Payment Methods Accepted content", 
						"The Select Card Type dropdown is not displayed bellow the Payment Methods Accepted content", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "radioCreditCard", "selectCardType", checkoutPage) ||
						checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "radioCreditCard", "drpCardType", checkoutPage), 
						"The Select Card Type dropdown should be displayed below the Credit Card radio button", 
						"The Select Card Type dropdown is displayed below the Credit Card radio button", 
						"The Select Card Type dropdown is not displayed below the Credit Card radio button", driver);
			}
			
			Log.softAssertThat(checkoutPage.verifyCardOrder(), 
					"To check Non-Plcc card should display first and Plcc card should below the Non-Plcc card list in Credit card dropdown",
					"Non-Plcc card is displaying first and Plcc card is below to Non-Plcc card list in Credit card dropdown",
					"The card order is not correct in Credit card dropdown", driver);
	
			Log.softAssertThat(checkoutPage.getNoOfCardsInPaymentPgCardTypeDropDown() > 0, 
					"To check payment page card type dropdown have list of card types",
					"The payment page card type dropdown have list of card types",
					"The payment page card type dropdown not having the list of card types", driver);
	
			checkoutPage.selectCardType("Visa");
			Log.softAssertThat(checkoutPage.getCreditCardType().equalsIgnoreCase("Visa"), 
					"To check payment page card type dropdown is displaying the selected card type",
					"The payment page card type dropdown is displaying the selected card type",
					"The payment page card type dropdown is not displaying the selected card type", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("radioCreditCard"), checkoutPage), 
					"The Credit card radio button should be selected by default",
					"The Credit card radio button is selected by default",
					"The Credit card radio button is not selected by default", driver);
	
			//Step-2: Verify the functionality of the Name on Card field
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionCardType", "fldTxtNameOnCard", checkoutPage), 
					"To check payment page card type dropdown is displaying above the owner of the card text field",
					"The payment page card type dropdown is displaying above the owner of the card text field",
					"The payment page card type dropdown is not displaying above the owner of the card text field", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("fldTxtNameOnCard", "lblNameOnCard", payment[1], checkoutPage), 
					"To check placeholder moving up when Name is typed in the owner of the card field",
					"The placeholder moving up when name is typed in the owner of the card field",
					"The placeholder not moving up when name is typed in the owner of the card field", driver);
			
			checkoutPage.enterTextInCardNameField(inputNumberOnly);
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblNameOnCardError"), checkoutPage),
					"To check name field only allows alphabets to write on.",
					"The name field only allows alphabets!",
					"The name field is allowing numeric", driver);
			
			checkoutPage.enterTextInCardNameField(payment[1]);
			
			checkoutPage.selectExpiryMonth(payment[3]);
			
			//Step-3: Verify the functionality of Card NumberfldTxtCardNo
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldTxtNameOnCard", "fldTxtCardNo", checkoutPage), 
					"The Card Number field should be displayed below the Name on Card field", 
					"The Card Number field is displayed below the Name on Card field", 
					"The Card Number field is not displayed below the Name on Card field", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("fldTxtCardNo", "lblTxtCardNo", payment[2], checkoutPage), 
					"To check placeholder moving up when card number is typed in the card number field",
					"The placeholder moving up when card number is typed in the card number field",
					"The placeholder not moving up when card number is typed in the card number field", driver);
			
			checkoutPage.enterTextInCardNumberField(inputTextOnly);
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblTxtCardNoError"), checkoutPage),
					"To check card number field only allows numerics to write on.",
					"The card number field only allows numerics!",
					"The card number field is allowing alphabets", driver);
			
			checkoutPage.enterTextInCardNumberField(cardNumberTooLong);
			Log.softAssertThat(checkoutPage.elementLayer.VerifyElementTextCountEqualTo("fldTxtCardNo", 16, checkoutPage),
					"The length of the card number should not exceed 16 digits",
					"The length of the card number does not exceed 16 digits",
					"The length of the card number exceeds 16 digits", driver);
	
			//Step-4: Verify the functionality of Expiration Month
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldTxtCardNo", "sectionExpMonth", checkoutPage), 
					"To check expiry month field is displaying below the card number field",
					"The expiry month field is displaying below the card number field",
					"The expiry month field is not displaying below the card number field", driver);
	
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "radioCreditCard", "sectionPaymentOrder", checkoutPage), 
						"To check payment order is displaying below the credit card radio button",
						"The payment order is displaying below the credit card radio button",
						"The payment order is not displaying below the credit card radio button", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "sectionPaymentOrder", "sectionCardType",  checkoutPage), 
						"To check payment order is displaying right side to the card type dropdown",
						"The payment order is displaying right side to the card type dropdown",
						"The payment order is not displaying right side to the card type dropdown", driver);
			}
	
			checkoutPage.selectExpiryMonth(payment[3]);
			
			Log.softAssertThat(checkoutPage.verifySelectedMonthDisplayedInExpiryMonth(payment[3]), 
					"To check expiry month show the months when it clicked and expiry month selected is displayed",
					"The expiry month show the months when it clicked and expiry month selected is displayed",
					"The expiry month not shows the months when it clicked and expiry month selected is not displayed", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("fldTxtCardNo"), 16, checkoutPage),
					"To check card number field only allows 16 characters.",
					"The card number field only allows 16 characters!",
					"The card number field is allowing more than 16 characters", driver);
			
			//Step-5: Verify the functionality of Expiration Year
			checkoutPage.selectExpiryYear(payment[4]);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementTextContains("selectExpYear",payment[4],checkoutPage) ||
					checkoutPage.elementLayer.verifyElementTextContains("drpExpYear",payment[4],checkoutPage), 
					"To check expiry year show the years list when it clicked and expiry year selected is displayed",
					"The expiry year show the years list when it clicked and expiry year selected is displayed",
					"The expiry year not shows the years list when it clicked and expiry year selected is not displayed", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionExpMonth", "fldTxtCvvNo", checkoutPage), 
					"To check expiry month field is displaying above the cvv field",
					"The expiry month field is displaying above the cvv field",
					"The expiry month field is not displaying above the cvv field", driver);
	
			//Step-6: Verify the functionality of CVV field
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("fldTxtCvvNo", "lblTxtCvvNo", payment[5], checkoutPage), 
					"To check placeholder moving up when cvv is typed in the cvv field",
					"The placeholder moving up when cvv is typed in the cvv field",
					"The placeholder not moving up when cvv is typed in the cvv field", driver);
			
			checkoutPage.enterTextInCardCvvField("CVV");
			Log.softAssertThat(!checkoutPage.elementLayer.verifyElementTextContains("fldTxtCvvNo","CVV",checkoutPage), 
					"To check cvv field is allowing alphabets",
					"The cvv field is allowing alphabets",
					"The cvv field is not allowing alphabets", driver);
			
			//Step-7: Verify the display of Payment Methods Accepted
			if(Utils.isDesktop() || Utils.isTablet()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divAcceptedPayments", "drpCardTypeSection", checkoutPage), 
						"Payment Methods Accepted section should be displayed to the right of the Select Card Type dropdown", 
						"Payment Methods Accepted section is displayed to the right of the Select Card Type dropdown", 
						"Payment Methods Accepted section is not displayed to the right of the Select Card Type dropdown", driver);
			}
			else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "radioCreditCard", "divAcceptedPayments", checkoutPage), 
						"Payment Methods Accepted section should be below the Credit Card Heading", 
						"Payment Methods Accepted section is below the Credit Card Heading", 
						"Payment Methods Accepted section is not below the Credit Card Heading", driver);
			}
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C21552
}// search
