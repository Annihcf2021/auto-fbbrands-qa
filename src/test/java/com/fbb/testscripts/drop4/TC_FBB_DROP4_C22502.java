package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22502 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22502(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prodId = TestData.get("prd_variation");
		String guestEmail;
		String txtToCheck = TestData.get("txt_gift_message_counter_text");
		String maxTxtCount = TestData.get("gift_message_max_char_count");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		guestEmail = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(prodId);
			Log.message(i++ + ". Navigated to PDP Page.", driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to Bag.", driver);
	
			CheckoutPage checkoutPage = homePage.headers.navigateToShoppingBagPage().clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to Checkout page.", driver);
	
			char[] charArray = { 'a', 'e', 'i', 'o', 'u' };
	
			String randomString = RandomStringUtils.random(5, charArray);
	
			checkoutPage.enterGuestUserEmail(randomString + guestEmail);
			checkoutPage.continueToShipping();
			//Step 1 
			Log.softAssertThat(!checkoutPage.verifyGiftReceiptCheckBoxIsChecked(),
					"To check the 'Gift Receipt Checkbox' is checked/not.",
					"The 'Gift Receipt checkbox' is not checked!",
					"The 'Gift Receipt checkbox' is checked", driver);
	
			Log.softAssertThat(!checkoutPage.verifyGiftMessageCheckBoxIsChecked(),
					"To check the 'Gift Message Checkbox' is checked/not.",
					"The 'Gift Message checkbox' is not checked!",
					"The 'Gift Message checkbox' is checked", driver);
			//Step 2b 
			Log.softAssertThat(checkoutPage.verifyGiftHeadingTextComingFromProperty(),
					"To check the gift heading text is coming from property forms.properties.",
					"The text for gift heading is coming from property",
					"The text for gift heading is not coming from property", driver);
	
			//Step 3a - Verified with step 1
	
			//Step 5e 
			Log.softAssertThat(checkoutPage.verifyGiftMessage1TextComingFromProperty(),
					"To check the gift message1 placeholder coming from property forms.properties.",
					"The gift message1 placeholder coming from property forms.properties",
					"The gift message1 placeholder is not coming from property forms.properties", driver);
	
			Log.softAssertThat(checkoutPage.verifyGiftMessage2TextComingFromProperty(),
					"To check the gift message2 placeholder coming from property forms.properties.",
					"The gift message2 placeholder coming from property forms.properties",
					"The gift message2 placeholder is not coming from property forms.properties", driver);
	
			Log.softAssertThat(checkoutPage.verifyGiftMessage3TextComingFromProperty(),
					"To check the gift message3 placeholder coming from property forms.properties.",
					"The gift message3 placeholder coming from property forms.properties",
					"The gift message3 placeholder is not coming from property forms.properties", driver);
	
			//Step 3b
			/*Log.softAssertThat(checkoutPage.verifyGiftReceiptCheckBoxIsClickable(),
					"To check whether the user can select gift receipt or not.",
					"The user can select gift receipt checkbox!",
					"The user cannot be able to select gift receipt checkbox", driver);*/
	
			checkoutPage.clickGiftMessageCheckBox();
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtFormGiftMessage1", "txtFormGiftMessage2", "txtFormGiftMessage3"), checkoutPage),
					"To check the gift message text fields are displaying.",
					"All the 3 text fields are displaying after gift message check box is checked",
					"Text fields are not displaying after gift message check box is checked", driver);
			//Step 3c
			Log.softAssertThat(checkoutPage.verifyGiftMessageTextComingFromProperty(),
					"To check the gift message text is coming from property forms.properties.",
					"The text for gift message is coming from property",
					"The text for gift message is not coming from property", driver);
	
			//Step 4a - Verified with step 1
	
			//Step 4b
			Log.softAssertThat(checkoutPage.verifyGiftMessageCheckBoxIsClickable(),
					"To check whether the user can select gift message or not.",
					"The user can select gift message checkbox!",
					"The user cannot be able to select gift message checkbox", driver);
	
			//Step 4c done below - finished
	
			//Step 4d 
			Log.softAssertThat(checkoutPage.verifyGiftReceiptTextComingFromProperty(),
					"To check the gift receipt text is coming from property forms.properties.",
					"The text for gift receipt is coming from property",
					"The text for gift receipt is not coming from property", driver);
	
			//Step 5a
			Log.softAssertThat(checkoutPage.checkGiftMessage1CounterTextIsCorrect(txtToCheck),
					"To check counter text before entering any character in Gift Message1 Text Area.",
					"The Counter text '"+txtToCheck+"' is displayed correctly in Gift Message1 Text Area!",
					"The Counter text '"+txtToCheck+"' is not displaying correctly in Gift Message1 Text Area!", driver);
	
			Log.softAssertThat(checkoutPage.checkGiftMessage2CounterTextIsCorrect(txtToCheck),
					"To check counter text before entering any character in Gift Message2 Text Area.",
					"The Counter text '"+txtToCheck+"' is displayed correctly in Gift Message2 Text Area!",
					"The Counter text '"+txtToCheck+"' is not displaying correctly in Gift Message2 Text Area!", driver);
	
			Log.softAssertThat(checkoutPage.checkGiftMessage3CounterTextIsCorrect(txtToCheck),
					"To check counter text before entering any character in Gift Message3 Text Area.",
					"The Counter text '"+txtToCheck+"' is displayed correctly in Gift Message3 Text Area!",
					"The Counter text '"+txtToCheck+"' is not displaying correctly in Gift Message3 Text Area!", driver);
	
			Log.softAssertThat(checkoutPage.checkGiftMessage1AllowMoreThanMaxChar(Integer.parseInt(maxTxtCount)),
					"For text count verification in Gift Message1 Text Area.",
					"After maximum character the TextArea didnt allow to enter text!",
					"After maximum character the TextArea allowing to enter text!", driver);
	
			Log.softAssertThat(checkoutPage.checkGiftMessage2AllowMoreThanMaxChar(Integer.parseInt(maxTxtCount)),
					"For text count verification in Gift Message2 Text Area.",
					"After maximum character the TextArea didnt allow to enter text!",
					"After maximum character the TextArea allowing to enter text!", driver);
	
			Log.softAssertThat(checkoutPage.checkGiftMessage3AllowMoreThanMaxChar(Integer.parseInt(maxTxtCount)),
					"For text count verification in Gift Message3 Text Area.",
					"After maximum character the TextArea didnt allow to enter text!",
					"After maximum character the TextArea allowing to enter text!", driver);
	
			Log.softAssertThat(checkoutPage.verifyTextInGiftMessage1CounterIncreaseAndDecrease(Integer.parseInt("10"),  Integer.parseInt(maxTxtCount)),
					"To check the gift message1 text counter is increasing and decreasing when character entered",
					"The gift message1 text counter is increasing and decreasing when character entered",
					"The gift message1 text counter is not increasing and decreasing when character entered correctly", driver);
	
			Log.softAssertThat(checkoutPage.verifyTextInGiftMessage2CounterIncreaseAndDecrease(Integer.parseInt("10"),  Integer.parseInt(maxTxtCount)),
					"To check the gift message2 text counter is increasing and decreasing when character entered",
					"The gift message2 text counter is increasing and decreasing when character entered",
					"The gift message2 text counter is not increasing and decreasing when character entered correctly", driver);
	
			Log.softAssertThat(checkoutPage.verifyTextInGiftMessage3CounterIncreaseAndDecrease(Integer.parseInt("10"),  Integer.parseInt(maxTxtCount)),
					"To check the gift message3 text counter is increasing and decreasing when character entered",
					"The gift message3 text counter is increasing and decreasing when character entered",
					"The gift message3 text counter is not increasing and decreasing when character entered correctly", driver);
	
			//Step 5d
			checkoutPage.fillingShippingDetailsAsGuest("No", "valid_address7", ShippingMethod.Standard);
	
			checkoutPage.clickGiftReceiptCheckBox(true);
	
			checkoutPage.enterTextInGiftMessage1Text("Message 1 text");
	
			checkoutPage.enterTextInGiftMessage2Text("Message 2 text");
	
			checkoutPage.enterTextInGiftMessage3Text("Message 3 text");
	
			checkoutPage.continueToPayment();
	
			Log.softAssertThat(checkoutPage.checkGiftMessage1TextApplied("Message 1 text"),
					"To check the given text in gift message1 is applied",
					"The given text in gift message1 is applied",
					"The given text in gift message1 is not applied", driver);
	
			Log.softAssertThat(checkoutPage.checkGiftMessage2TextApplied("Message 2 text"),
					"To check the given text in gift message2 is applied",
					"The given text in gift message2 is applied",
					"The given text in gift message2 is not applied", driver);
	
			Log.softAssertThat(checkoutPage.checkGiftMessage3TextApplied("Message 3 text"),
					"To check the given text in gift message3 is applied",
					"The given text in gift message3 is applied",
					"The given text in gift message3 is not applied", driver);
			//Step 4c
			Log.softAssertThat(checkoutPage.checkGiftReceiptAcceptedOrNot(),
					"To check gift receipt is accepted or not",
					"Gift receipt is accepted",
					"Gift receipt is not accepted", driver);
			Utils.waitForPageLoad(driver);
			//Step 6
			checkoutPage.clickEditShippingLink();
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divShippingSectionContent", "txtGiftMessage", checkoutPage),
					"To check the gift message is displayed under Shipping section.",
					"The gift message is displayed under Shipping section",
					"The gift message is not displaying under Shipping section", driver);

			Log.message("The Step 6b is covered in Test case 22572");

			//Step 7
			try {
				Log.softAssertThat("Message 1 text".equals(checkoutPage.getEnteredTextInGiftMessage1()),
						"To check the gift message1 text is retained when editing.",
						"The gift message1 text is retained when editing.",
						"The gift message1 text is not retained when editing - PXSFCC-4040", driver);
			}catch (Exception e) {
				Log.fail("The edit shipping is not working properly - PXSFCC-4040", driver);
			}
	
			try {
				Log.softAssertThat("Message 2 text".equals(checkoutPage.getEnteredTextInGiftMessage2()),
						"To check the gift message2 text is retained when editing.",
						"The gift message2 text is retained when editing.",
						"The gift message2 text is not retained when editing.", driver);
			}catch (Exception e) {
				Log.fail("The edit shipping is not working properly - PXSFCC-4040", driver);
			}
	
			Log.softAssertThat("Message 2 text".equals(checkoutPage.getEnteredTextInGiftMessage2()),
					"To check the gift message2 text is retained when editing.",
					"The gift message2 text is retained when editing.",
					"The gift message2 text is not retained when editing.", driver);
	
			Log.softAssertThat("Message 3 text".equals(checkoutPage.getEnteredTextInGiftMessage3()),
					"To check the gift message3 text is retained when editing.",
					"The gift message3 text is retained when editing.",
					"The gift message3 text is not retained when editing.", driver);
	
			checkoutPage.enterTextInGiftMessage1Text("Message 1 text edit");
	
			checkoutPage.enterTextInGiftMessage2Text("Message 2 text edit");
	
			checkoutPage.enterTextInGiftMessage3Text("Message 3 text edit");
	
			checkoutPage.continueToPayment();
	
			Log.softAssertThat(checkoutPage.checkGiftMessage1TextApplied("Message 1 text edit"),
					"To check the givent text in gift message1 is applied",
					"The givent text in gift message1 is applied",
					"The givent text in gift message1 is not applied", driver);
	
			Log.softAssertThat(checkoutPage.checkGiftMessage2TextApplied("Message 2 text edit"),
					"To check the givent text in gift message2 is applied",
					"The givent text in gift message2 is applied",
					"The givent text in gift message2 is not applied", driver);
	
			Log.softAssertThat(checkoutPage.checkGiftMessage3TextApplied("Message 3 text edit"),
					"To check the givent text in gift message3 is applied",
					"The givent text in gift message3 is applied",
					"The givent text in gift message3 is not applied", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// TC_FBB_DROP4_C22502
}// search
