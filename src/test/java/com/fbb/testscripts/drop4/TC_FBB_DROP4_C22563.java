package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22563 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22563(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test data
		String searchKey = TestData.get("prd_po-box-unrestricted");
		
		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String guestEmail = AccountUtils.generateEmail(driver);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			//Headers headers = homePage.headers;
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.getProductName();
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			ShoppingBagPage shoppingBag = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on 'My Bag' icon in header");
	
			CheckoutPage checkoutPage = shoppingBag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Clicked on 'Checkout Now' button in shopping bag page");
	
			checkoutPage.enterGuestUserEmail(guestEmail);
			Log.message(i++ + ". Entered guest email address in Checkout Page");
	
			checkoutPage.continueToShipping();
			Log.message(i++ + ". Clicked on 'Continue' button in Checkout Page");
	
			checkoutPage.fillingShippingDetailsAsGuestForPORestrictedItems();
			Log.message(i++ + ". Entered shipping address");
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("editShippingAddressOverlay"), checkoutPage), 
					"Default Edit shipping address overlay should not be displayed!", 
					"Default Edit shipping address overlay is not displayed", 
					"Default Edit shipping address overlay is displayed", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			//Filling PO Box address completely
			checkoutPage.fillingShippingDetailsAsGuest("NO", "valid_address3", ShippingMethod.Standard);
			Log.message(i++ + ". Entered shipping address");
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Clicked on 'Continue' button after filling shipping address");
	
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message(i++ + ". Entered Payment details",driver);
	
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Clicked on 'Continue' button after entering payment details");
	
			OrderConfirmationPage ordCnfPage = checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on 'Place Order' button!");
	
			Log.softAssertThat(ordCnfPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), ordCnfPage),
					"Order should be placed successfully as a Guest user when no PO box restricted product is added in the cart!", 
					"Order is placed successfully as a Guest user when no PO box restricted product is added in the cart!",
					"Order is not placed successfully as a Guest user when no PO box restricted product is added in the cart!", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M1_FBB_DROP4_C22563
	
	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP4_C22563(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test data
		String searchKey = TestData.get("prd_po-box-restricted");
		String guestEmail;
	
		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		guestEmail = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			//Headers headers = homePage.headers;
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.getProductName();
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			ShoppingBagPage shoppingBag = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on 'My Bag' icon in header");
	
			CheckoutPage checkoutPage = shoppingBag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Clicked on 'Checkout Now' button in shopping bag page");
	
			checkoutPage.enterGuestUserEmail(guestEmail);
			Log.message(i++ + ". Entered guest email address in Checkout Page");
	
			checkoutPage.continueToShipping();
			Log.message(i++ + ". Clicked on 'Continue' button in Checkout Page");
	
			checkoutPage.fillingShippingDetailsAsGuestForPORestrictedItems();
			Log.message(i++ + ". Entered shipping address");
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("editShippingAddressOverlay"), checkoutPage), 
					"Default Edit shipping address overlay should be displayed!", 
					"Default Edit shipping address overlay is displayed", 
					"Default Edit shipping address overlay is not displayed", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			checkoutPage.clickEditInPORestrictedOverlay();
			Log.message(i++ + ". Clicked on 'Edit' button in PO Box Overlay");
	
			checkoutPage.fillingShippingDetailsAsGuest("NO", "valid_address1", ShippingMethod.Standard);
			Log.message(i++ + ". Entered shipping address");
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Clicked on 'Continue' button after filling shipping address");
	
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message(i++ + ". Entered Payment details",driver);
	
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Clicked on 'Continue' button after entering payment details");
	
			OrderConfirmationPage ordCnfPage = checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on 'Place Order' button!");
	
			Log.softAssertThat(ordCnfPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), ordCnfPage),
					"Order should be placed after editing different address successfully as a Guest user when all the items are PO box restricted!", 
					"Order is placed after editing different address successfully as a Guest user when all the items are PO box restricted!",
					"Order is not placed after editing different address successfully as a Guest user when all the items are PO box restricted!", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M2_FBB_DROP4_C22563
	
	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP4_C22563(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test data
		String[] searchKey = {TestData.get("prd_po-box-restricted"), TestData.get("prd_po-box-unrestricted")};
		String guestEmail;
	
		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		guestEmail = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			//Headers headers = homePage.headers;
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey[0]);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.getProductName();			
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
			
			pdpPage = homePage.headers.navigateToPDP(searchKey[1]);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.getProductName();			
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);
	
			ShoppingBagPage shoppingBag = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on 'My Bag' icon in header");
	
			CheckoutPage checkoutPage = shoppingBag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Clicked on 'Checkout Now' button in shopping bag page");
	
			checkoutPage.enterGuestUserEmail(guestEmail);
			Log.message(i++ + ". Entered guest email address in Checkout Page");
	
			checkoutPage.continueToShipping();
			Log.message(i++ + ". Clicked on 'Continue' button in Checkout Page");
	
			//Filling PO Box address till Address Line 1
			checkoutPage.fillingShippingDetailsAsGuestForPORestrictedItems();
			Log.message(i++ + ". Entered shipping address");
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("editShippingAddressOverlay"), checkoutPage), 
					"Default Edit shipping address overlay should be displayed!", 
					"Default Edit shipping address overlay is displayed", 
					"Default Edit shipping address overlay is not displayed", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnEditPOBoxOverlay","btnPoBoxBlockOverlayRemoveItems"), checkoutPage), 
					"Edit button and Remove button should display in default shipping address overlay!", 
					"Edit button and Remove button is displaying in default Edit shipping address overlay", 
					"Edit button and Remove button is not displaying in default Edit shipping address overlay", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
	
			checkoutPage.clickRemoveItemsPOBoxOverlay();
			Log.message(i++ + ". Clicked Remove Items in PO Box overlay",driver);
			
			//Filling PO Box address completely
			checkoutPage.fillingShippingDetailsAsGuest("NO", "valid_address3", ShippingMethod.Standard);
			Log.message(i++ + ". Entered shipping address");
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Clicked on 'Continue' button after filling shipping address");
	
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message(i++ + ". Entered Payment details",driver);
	
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Clicked on 'Continue' button after entering payment details");
	
			OrderConfirmationPage ordCnfPage = checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on 'Place Order' button!");
	
			Log.softAssertThat(ordCnfPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), ordCnfPage),
					"Order should be placed only after removing the PO box restricted product!", 
					"Order is placed after removing the PO box restricted product!",
					"Order is not placed after removing the PO box restricted product!", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M3_FBB_DROP4_C22563
}// search
