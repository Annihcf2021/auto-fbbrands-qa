package com.fbb.testscripts.drop4;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22526 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22526(String browser) throws Exception {
		Log.testCaseInfo();

		//Load Test Data
		String variation_product = TestData.get("prd_variation1");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String username = accountData.get("credential_saved_payment_plcc_non-plcc").split("\\|")[0];
		String password = accountData.get("credential_saved_payment_plcc_non-plcc").split("\\|")[1];
		String credential = username+"|"+password;
		int i = 1;
		try {

			Object[] obj = GlobalNavigation.checkout(driver, variation_product, i, credential);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];

			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("tenderRebuttalMsg"), checkoutPage),
					"When the User has a saved PLCC payment instrument but has selected a non-PLCC credit card, then the System should display the tender rebuttal message below the selected.", 
					"When the User has a saved PLCC payment instrument but has selected a non-PLCC credit card, then the System is display the tender rebuttal message below the selected.", 
					"When the User has a saved PLCC payment instrument but has selected a non-PLCC credit card, then the System is not display the tender rebuttal message below the selected.", driver);

			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("tenderRebuttalMsg", "rgba(197, 83, 0, 1)", checkoutPage),
					"Rebuttal message below the selected payment instrument should be (in yellow font color).", 
					"Rebuttal message below the selected payment instrument is (in yellow font color).", 
					"Rebuttal message below the selected payment instrument is not (in yellow font color).", driver);

			if(Utils.isMobile())
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "cvvNumber", "tenderRebuttalMsg", checkoutPage),
						"Rebuttal message should be displayed below of the CVV.", 
						"Rebuttal message is displayed below of the CVV.",
						"Rebuttal message is not displayed below of the CVV.", driver);
			}
			else
			{
				Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "tenderRebuttalMsg", "fldCVV", checkoutPage),
						"Rebuttal message should be displayed right side of the CVV.", 
						"Rebuttal message is displayed right side of the CVV.",
						"Rebuttal message is not displayed right side of the CVV.", driver);
			}

			Log.softAssertThat(checkoutPage.elementLayer.verifyTextContains("tenderRebuttalMsg", TestData.get("brandNameInPLCCRebutalMsg"), checkoutPage),
					"The PLCC brand should be displayed in the Rebuttal message.",
					"The PLCC brand is displayed in the Rebuttal message.",
					"The PLCC brand is not displayed in the Rebuttal message.", driver);

			checkoutPage.clickFirstPlccCard();

			Log.softAssertThat(!checkoutPage.elementLayer.verifyPageElements(Arrays.asList("tenderRebuttalMsg"), checkoutPage),
					"When the User has a saved PLCC payment instrument but has selected a non-PLCC credit card, then the System should display the tender rebuttal message below the selected.", 
					"When the User has a saved PLCC payment instrument but has selected a non-PLCC credit card, then the System is display the tender rebuttal message below the selected.", 
					"When the User has a saved PLCC payment instrument but has selected a non-PLCC credit card, then the System is not display the tender rebuttal message below the selected.", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_DROP4_C19710
}// search
