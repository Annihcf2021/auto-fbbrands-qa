package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22551 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22551(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_monogram");
		String username;
		String rewardcertificate =TestData.get("reward_valid-2"); 
		String[] coupons =  TestData.get("cpn_discount_ship").split("\\|");
		String colorOrderPromo = TestData.get("couponErrorColorRGB");
	
		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			String selectedSize = pdpPage.selectSize();
			Log.message(i++ + ". Size Selected :: '"+selectedSize+"'", driver);
	
			String selectedColor = pdpPage.selectColor();
			Log.message(i++ + ". Selected Color :: '"+selectedColor+"'");
	
			String width = pdpPage.selectSizeFamily();
			Log.message(i++ + ". Selected Color :: '"+width+"'");
	
			boolean orderOptionAvailable = pdpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("chkEnableMonogramming"), pdpPage);
			if(orderOptionAvailable) {
				pdpPage.clickOnMonogrammingCheckbox("enable");
				pdpPage.selectMonogrammingFontValue(1);
				pdpPage.selectMonogrammingColorValue(1);
				pdpPage.selectMonogrammingLocationValue(1);
				pdpPage.enterTextInMonogramFirstTextBox("TextEnter");
			}else {
				Log.reference("Monogramming currently disabled for site.");
			}
	
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add to cart button", driver);
	
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Clicked on 'X' in mini cart overlay", driver);
	
			ShoppingBagPage shoppingBag = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on 'My Bag' icon in header", driver);
			
			//Step-1: Verify shopping cart header
			Log.softAssertThat(shoppingBag.elementLayer.verifyElementWithinElement(driver, "headerShoppingBag", "divCartTopBanner", shoppingBag)
							&& shoppingBag.elementLayer.verifyElementWithinElement(driver, "btnCheckoutNow", "divCartTopBanner", shoppingBag)
							&& shoppingBag.elementLayer.verifyElementWithinElement(driver, "lnkShowBagID", "divCartTopBanner", shoppingBag),
					"Shopping bag title, Checkout Now button and shopping bag ID should be displayed for cart header.",
					"Shopping bag title, Checkout Now button and shopping bag ID are displayed for cart header.",
					"Shopping bag title, Checkout Now button and shopping bag ID are not displayed for cart header.", driver);
			
			Log.softAssertThat(shoppingBag.elementLayer.verifyTextEquals("headerShoppingBag", "MY SHOPPING BAG", shoppingBag),
					"Shopping bag page title should display as MY SHOPPING BAG.",
					"Shopping bag page title displays as MY SHOPPING BAG.",
					"Shopping bag page title does not display as MY SHOPPING BAG.", driver);
			
			//Step-2: Verify "View shopping bag ID" link
			shoppingBag.getShoppingBagID();
			Log.message(i++ + ". Clicked on Show Bag ID", driver);
			
			Log.softAssertThat(shoppingBag.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("spanBagID"), shoppingBag),
					"On click / tab system should display shopping bag ID number",
					"On click / tab system displays shopping bag ID number",
					"On click / tab system does not display shopping bag ID number", driver);
	
			//Step-3 Verify the functionality of the 'CHECKOUT NOW' button
			shoppingBag.clickOnArrowContious("up", 5);
			Log.message(i++ + ". Increasing Qty for the product", driver);
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("txtSeeDetails"), shoppingBag),
					"Shipping Tool cost should be displayed in Shopping bag", 
					"Shipping Tool cost is displayed in Shopping bag", 
					"Shipping Tool cost is not displayed in Shopping bag", driver);
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("btnCheckoutNow"), shoppingBag),
					"'Checkout Now' button should be displayed in Shopping bag", 
					"'Checkout Now' button is displayed in Shopping bag", 
					"'Checkout Now' button is not displayed in Shopping bag", driver);
	
			shoppingBag.clickOnShippingCostTool();
			Log.message(i++ + ". Clicked on 'Shipping cost tool' in Shopping bag page", driver);
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("shippingCostOverlay"), shoppingBag), 
					"Shipping Cost Overlay should be displayed!", 
					"Shipping Cost Overlay is displayed!",
					"Shipping Cost Overlay is not displayed!", driver);
	
			shoppingBag.clickOnShippingCostToolClose();
			Log.message(i++ + ". Clicked on 'Shipping cost tool close' in Shopping bag page", driver);
	
			CheckoutPage checkoutPage = shoppingBag.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Clicked on 'Checkout Now' button in shopping bag page", driver);
	
			//For Guest User
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtGuestEmail"), checkoutPage), 
					"For a guest User, the System should navigate to the 'Shipping Details' section of the Checkout page!", 
					"For a guest User, the System will navigate to the 'Shipping Details' section of the Checkout page!",
					"For a guest User, the System is not navigating to the 'Shipping Details' section of the Checkout page!", driver);
			
			checkoutPage.enterGuestUserEmail(username);
			Log.message(i++ + ". Entered guest email address in Checkout Page", driver);
	
			checkoutPage.continueToShipping();
			Log.message(i++ + ". Clicked on 'Continue' button in Checkout Page", driver);
	
			Log.softAssertThat(checkoutPage.getShippingDetailsHeaderTitle().equals("Shipping Address")
							|| checkoutPage.getShippingDetailsHeaderTitle().contains("Shipping Address"), 
					"Page should be redirected to shipping details section for guest user!", 
					"Page is redirected to shipping details section for guest user!", 
					"Page is not redirected to shipping details section for guest user!", driver);
	
			checkoutPage.fillingShippingDetailsAsGuest("No" ,"valid_address7", ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address filled successfully.", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Section.", driver);
	
			//Adding the rewards certificates
			if(!BrandUtils.isBrand(Brand.el)) {
				checkoutPage.applyRewardCertificate(rewardcertificate);
				Log.message(i++ + ". Rewards applied.", driver);
			}
			//Adding the gift cards and pin
			checkoutPage.applyGiftCardByValue(TestGiftCardValue.valid);
			Log.message(i++ + ". Gift cards applied.", driver);
			//Adding the multiple coupons
			checkoutPage.applyMultiplePromoCouponCode(coupons);
			Log.message(i++ + ". Coupons applied.", driver);
	
			//Step-4. Verify the display of Subtotal.
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "checkoutSummaryHeading", "divOrderSubTotal", checkoutPage), 
					"The Subtotal field should display on the first line in the Cost Summary.",
					"The Subtotal field is display on the first line in the Cost Summary.", 
					"The Subtotal field is not display on the first line in the Cost Summary.", driver);
	
			//Step-5. Verify the display of Discount
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divOrderSubTotal", "orderSummaryDiscount", checkoutPage), 
					"System should display Discount below the Subtotal.",
					"System is display Discount below the Subtotal.", 
					"System is not display Discount below the Subtotal.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("orderSummaryDiscount", colorOrderPromo,checkoutPage), 
					"Discount should be displayed in red color!", 
					"Discount is displayed in red color!",
					"Discount is not displayed in red color!", driver);
	
			if(orderOptionAvailable) {
				//Step-6. Verify the display of Option Cost
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryDiscount", "orderSummaryOptionCost", checkoutPage), 
						"System should display Option Cost below the Discount field.",
						"System is display Option Cost below the Discount field.", 
						"System is not display Option Cost below the Discount field.", driver);
		
				//Step-7.Verify the display of Shipping Cost
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryOptionCost", "orderSummaryShippingDetail", checkoutPage), 
						"System should display Shipping Cost below the Option cost.",
						"System is display Shipping Cost below the Option cost.", 
						"System is not display Shipping Cost below the Option cost.", driver);
			}else {
				Log.reference("Option Cost is not available for this site/purchase.");
			}
	
			//6 and 7
			checkoutPage.clickOnShippingCostTool();
			Log.message(i++ + ". Clicked on Shipping Cost Tooltip.", driver);
	
			Log.softAssertThat(shoppingBag.elementLayer.verifyPageElements(Arrays.asList("shippingCostOverlay"), shoppingBag), 
					"Shipping Cost Overlay should be displayed!", 
					"Shipping Cost Overlay is displayed!",
					"Shipping Cost Overlay is not displayed!", driver);
	
			checkoutPage.clickOnShippingCostToolClose();
			Log.message(i++ + ". Clicked on Shipping Cost Tooltip close button.", driver);
	
			//Step-11.Verify the display of Shipping Discount
			checkoutPage.removeAppliedCoupon();
			Log.message(i++ + ". Removed coupons.", driver);
			
			if(checkoutPage.getRemainingOrderAmount() != 00.00) {
				checkoutPage.fillingCardDetails("NO", "card_Visa");
				Log.message(i++ + ". Entered Payment details",driver);
			}
			
			checkoutPage.expandOrColapsePromoCodeSection(true);
			Log.message(i++ + ". Expanded coupon code section.", driver);
			checkoutPage.enterPromoCouponCode(coupons[1]);
			Log.message(i++ + ". Coupon entered.", driver);
			checkoutPage.clickApplyPromoCouponCode();
			Log.message(i++ + ". Coupon applied.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryShippingDetail", "orderSummaryShippingDiscountDetail", checkoutPage), 
					"System should display the Shipping discount below the Shipping Message field.",
					"System is display the Shipping discount below the Shipping Message field.", 
					"System is not display the Shipping discount below the Shipping Message field.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("orderSummaryShippingDiscountDetail", colorOrderPromo, checkoutPage), 
					"Shipping discount should be displayed in red color!", 
					"Shipping discount is displayed in red color!",
					"Shipping discount is not displayed in red color!", driver);
	
			//Step-12. Verify the display of Sales Tax
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryShippingDiscountDetail", "orderSummarySalesTax", checkoutPage), 
					"The Sales Tax should appear below the Shipping Discount field.",
					"The Sales Tax is appear below the Shipping Discount field.", 
					"The Sales Tax is not appear below the Shipping Discount field.", driver);
	
			//Step-13.
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummarySalesTax", "orderSummaryOrderTotal", checkoutPage), 
					"System should display the Order Total below the Sales Tax field.",
					"System is display the Order Total below the Sales Tax field.", 
					"System is not display the Order Total below the Sales Tax field.", driver);
			//Step-14
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryOrderTotal","orderSummaryGiftCardDiscount", checkoutPage), 
					"System should display the Gift Cards Applied field below the Order Total field.",
					"System is display the Gift Cards Applied field below the Order Total field.", 
					"System is not display the Gift Cards Applied field below the Order Total field.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("orderSummaryGiftCardDiscount", colorOrderPromo, checkoutPage), 
					"Gift card discount should be displayed in red color!", 
					"Gift card discount is displayed in red color!",
					"Gift card discount is not displayed in red color!", driver);
			//Step-15
			if(!BrandUtils.isBrand(Brand.el)){
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryGiftCardDiscount", "orderSummaryRewardDiscount", checkoutPage), 
					"System should display the Rewards Applied field below the Gift Cards Applied field.",
					"System is display the Rewards Applied field below the Gift Cards Applied field.", 
					"System is not display the Rewards Applied field below the Gift Cards Applied field.", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("orderSummaryRewardDiscount", colorOrderPromo, checkoutPage), 
					"Reward certificate discount should be displayed in red color!", 
					"Reward certificate discount is displayed in red color!",
					"Reward certificate discount is not displayed in red color!", driver);
			}
			
			//Step-16
			if(BrandUtils.isBrand(Brand.el)){
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryGiftCardDiscount","orderSummaryRemainingTotal", checkoutPage), 
						"The Remaining Total field should be present below the Rewards Applied field, after a faint line break.",
						"The Remaining Total field is present below the Rewards Applied field, after a faint line break.", 
						"The Remaining Total field is not present below the Rewards Applied field, after a faint line break.", driver);
				
			}else{
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryRewardDiscount","orderSummaryRemainingTotal", checkoutPage), 
					"The Remaining Total field should be present below the Rewards Applied field, after a faint line break.",
					"The Remaining Total field is present below the Rewards Applied field, after a faint line break.", 
					"The Remaining Total field is not present below the Rewards Applied field, after a faint line break.", driver);
			}
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("orderSummaryRemainingTotalValue", "font-size","20px", checkoutPage), 
					"The remaining total should be present in a slightly bigger and bolder font compared to the rest of others !", 
					"The remaining total is present in a slightly bigger and bolder font compared to the rest of others !",
					"The remaining total is not present in a slightly bigger and bolder font compared to the rest of others !!", driver);
			
			//Step-17
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryOrderTotal", "orderSummaryTotalSavings", checkoutPage), 
					"System should display the Total Savings below the Remaining Total (or Order Total) field.",
					"System is display the Total Savings below the Remaining Total (or Order Total) field.", 
					"System is not display the Total Savings below the Remaining Total (or Order Total) field.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("orderSummaryTotalSavings", colorOrderPromo, checkoutPage), 
					"Total Saving should be displayed in red color!", 
					"Total Saving is displayed in red color!",
					"Total Saving is not displayed in red color!", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP4_C22551(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_monogram");
		
		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String credentials = AccountUtils.generateEmail(driver).replaceAll("1@", "2@") + "|" + accountData.get("password_global");
		
		//Prerequisite: To create a user account if already not exists.
		{	
			GlobalNavigation.registerNewUser(driver, 2, 2, credentials);
		}
	
		int i = 1;
		try {
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			homePage.headers.navigateToMyAccount(AccountUtils.generateEmail(driver), accountData.get("password_global"));
			Log.message(i++ + ". Navigated to My Account Page.", driver);
			
			Object[] obj = GlobalNavigation.shoppingCart(driver, searchKey, i);
			
			ShoppingBagPage cartPage = (ShoppingBagPage) obj[0];
			Log.message(i++ + ". Navigated to Shopping Bag page", driver);
			
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Clicked on Checkout Now button", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtGuestEmail"), checkoutPage), 
					"For a User who is logged-in and has a default shipping/billing address, System should navigate to 'Review & Place Order' section!", 
					"For a User who is logged-in and has a default shipping/billing address, System will navigate to 'Review & Place Order' section!",
					"System is not navigate to 'Review & Place Order' section!", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	}
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP4_C22551(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_monogram");
		
		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String credentials = AccountUtils.generateEmail(driver).replaceAll("1@", "3@") + "|" + accountData.get("password_global");
		
		//Prerequisite: To create a user account if already not exists.
		{	
			GlobalNavigation.registerNewUser(driver, 0, 0, credentials);
		}
	
		int i = 1;
		try {
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			homePage.headers.navigateToMyAccount("new" + AccountUtils.generateEmail(driver), accountData.get("password_global"));
			
			Object[] obj = GlobalNavigation.shoppingCart(driver, searchKey, i);
			
			ShoppingBagPage cartPage = (ShoppingBagPage) obj[0];
			cartPage = new ShoppingBagPage(driver).get();
			Log.message(i++ + ". Navigated to Shopping Bag page", driver);
			
			//Step-2: Verify "View shopping bag ID" link
			cartPage.getShoppingBagID();
			Log.message(i++ + ". Clicked on Show Bag ID", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("spanBagID"), cartPage),
					"On click / tab cartPage should display shopping bag ID number",
					"On click / tab system displays shopping bag ID number",
					"On click / tab system does not display shopping bag ID number", driver);
			
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Clicked on Checkout Now", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shippingDetailsHeader"), checkoutPage), 
					"For a User who is logged in, does not have any default shipping/billing address or payment method, System should navigate to 'Shipping Details' section!", 
					"For a User who is logged in, does not have any default shipping/billing address or payment method, System will navigate to 'Shipping Details' section!",
					"System is not navigate to 'Shipping Details' section!", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	}
}// search
