package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22560 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22560(String browser) throws Exception {
		Log.testCaseInfo();
	
		String rewardcertificate =TestData.get("reward_valid-2"); 
		String coupon =  TestData.get("cpn_promo-ship-level");
		String searchKey = TestData.get("prd_variation") + "|" + TestData.get("prd_promo-prd-level");
		String colorOrderPromo = TestData.get("promo_color_CartPg");
	
		//Create the Web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, searchKey, i, AccountUtils.generateEmail(driver));
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
	
			checkoutPage.fillingShippingDetailsAsGuest("No" ,"valid_address7", ShippingMethod.Standard);
	
			checkoutPage.continueToPayment();
	
			if(!BrandUtils.isBrand(Brand.el)) {
				//Adding the rewards certificates
				checkoutPage.applyRewardCertificate(rewardcertificate);
				Log.message(i++ + ". Applied a reward certificate", driver);
			}
	
			//Adding the gift cards and pin
			checkoutPage.expandCollapseGiftCardSection("true");
			Log.message(i++ + ". Expanded GC section.", driver);
			
			checkoutPage.applyGiftCardByValue(TestGiftCardValue.valid);
			Log.message(i++ + ". Applied a gift card", driver);
	
			checkoutPage.applyPromoCouponCode(coupon);
			Log.message(i++ + ". Applied a coupon code", driver);
	
			checkoutPage.clickOnShippingCostTool();
			Log.message(i++ + ". Clicked on Shipping cost tool tip", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("shippingCostOverlay"), checkoutPage), 
					"Shipping Cost Overlay should be displayed!", 
					"Shipping Cost Overlay is displayed!",
					"Shipping Cost Overlay is not displayed!", driver);
	
			checkoutPage.clickOnShippingCostToolClose();
			Log.message(i++ + ". Shipping cost tool tip is closed", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryShippingDetail", "orderSummaryShippingDiscountDetail", checkoutPage), 
					"System should display the Shipping discount below the Shipping Message field.",
					"System is display the Shipping discount below the Shipping Message field.", 
					"System is not display the Shipping discount below the Shipping Message field.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("orderSummaryShippingDiscountDetail", colorOrderPromo, checkoutPage), 
					"Shipping discount should be displayed in red color!", 
					"Shipping discount is displayed in red color!",
					"Shipping discount is not displayed in red color!", driver);
	
			//9. Verify the display of Sales Tax
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryShippingDiscountDetail", "orderSummarySalesTax", checkoutPage), 
					"The Sales Tax should appear below the Shipping Discount field.",
					"The Sales Tax is appear below the Shipping Discount field.", 
					"The Sales Tax is not appear below the Shipping Discount field.", driver);
	
			//10.
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummarySalesTax", "orderSummaryOrderTotal", checkoutPage), 
					"System should display the Order Total below the Sales Tax field.",
					"System is display the Order Total below the Sales Tax field.", 
					"System is not display the Order Total below the Sales Tax field.", driver);
			//11
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryOrderTotal", "orderSummaryGiftCardDiscount", checkoutPage), 
					"System should display the Gift Cards Applied field below the Order Total field.",
					"System is display the Gift Cards Applied field below the Order Total field.", 
					"System is not display the Gift Cards Applied field below the Order Total field.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("orderSummaryGiftCardDiscount", colorOrderPromo, checkoutPage), 
					"Gift card discount should be displayed in red color!", 
					"Gift card discount is displayed in red color!",
					"Gift card discount is not displayed in red color!", driver);
			//12
			if(!BrandUtils.isBrand(Brand.el)){				
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryGiftCardDiscount", "orderSummaryRewardDiscount", checkoutPage), 
					"System should display the Rewards Applied field below the Gift Cards Applied field.",
					"System is display the Rewards Applied field below the Gift Cards Applied field.", 
					"System is not display the Rewards Applied field below the Gift Cards Applied field.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("orderSummaryRewardDiscount", colorOrderPromo, checkoutPage), 
					"Reward certificate discount should be displayed in red color!", 
					"Reward certificate discount is displayed in red color!",
					"Reward certificate discount is not displayed in red color!", driver);
			}
			
			//13
			if(BrandUtils.isBrand(Brand.el)){
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryGiftCardDiscount", "orderSummaryRemainingTotal", checkoutPage), 
					"The Remaining Total field should be present below the Gift card Applied field, after a faint line break.",
					"The Remaining Total field is present below the Gift card Applied field, after a faint line break.", 
					"The Remaining Total field is not present below the Gift card Applied field, after a faint line break.", driver);
					
			}else{
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryRewardDiscount", "orderSummaryRemainingTotal", checkoutPage), 
					"The Remaining Total field should be present below the Rewards Applied field, after a faint line break.",
					"The Remaining Total field is present below the Rewards Applied field, after a faint line break.", 
					"The Remaining Total field is not present below the Rewards Applied field, after a faint line break.", driver);
			}
			//14
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryOrderTotal", "orderSummaryTotalSavings", checkoutPage), 
					"System should display the Total Savings below the Remaining Total (or Order Total) field.",
					"System is display the Total Savings below the Remaining Total (or Order Total) field.", 
					"System is not display the Total Savings below the Remaining Total (or Order Total) field.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("orderSummaryTotalSavings", colorOrderPromo, checkoutPage), 
					"Total Saving should be displayed in red color!", 
					"Total Saving is displayed in red color!",
					"Total Saving is not displayed in red color!", driver);
	
			checkoutPage.removeAppliedCoupon();
			Log.message(i++ +". Remove applied coupon code", driver);
	
			if(!BrandUtils.isBrand(Brand.el)) {
				checkoutPage.removeAppliedReward();
				Log.message(i++ +". Remove applied reward certificate", driver);
			}
	
			checkoutPage.removeAppliedGiftCard();
			Log.message(i++ +". Remove applied gift card", driver);
	
			checkoutPage.fillingCardDetails("NO", "card_Visa");
			Log.message(i++ +". Filling Card details", driver);
	
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ +". Continue to review order", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrder"), checkoutPage), 
					"Place order button should get displayed!", 
					"Place order button is displaying!",
					"Place order button is not displaying!", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("secureMsgBelowReviewOrder","lblCheckout_Review_Desclaimer_Message"), checkoutPage), 
					"Secure message and Disclaimer should get displayed!", 
					"Secure message and Disclaimer is displaying!",
					"Secure message and Disclaimer is not displaying!", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
	
			OrderConfirmationPage ordConfirmationPg = checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ +". Clicked on place order button", driver);
	
			Log.softAssertThat(ordConfirmationPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), ordConfirmationPg), 
					"Order should be placed!", 
					"Order is placed sucessfully!",
					"Order is not placed!", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C22560
}// search
