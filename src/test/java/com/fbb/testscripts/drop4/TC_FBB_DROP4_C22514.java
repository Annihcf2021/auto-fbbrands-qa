package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22514 extends BaseTest {

    EnvironmentPropertiesReader environmentPropertiesReader;
    private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

    @Test(groups = { "critical", "desktop", "tablet", "mobile" }, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
    public void M1_FBB_DROP4_C22514(String browser) throws Exception {
        Log.testCaseInfo();

        //Load Test Data
        String searchKey = TestData.get("prd_low_cost");

        // Get the web driver instance
        final WebDriver driver = WebDriverFactory.get(browser);
        String email = AccountUtils.generateEmail(driver);
        String password = accountData.get("password_global");
        String credentials = email +"|"+ password;
        String phoneinvalid = "+333-444-5555";
        String postalinvalid = "28205-73051";
        //Prerequisite: To create a user account if already not exists.
        {
            GlobalNavigation.registerNewUser(driver, 1, 0, credentials);
        }

        int i = 1;
        try {

            Object[] obj = GlobalNavigation.addProduct_Checkout(driver, searchKey, i, credentials);
            CheckoutPage checkoutPage = (CheckoutPage) obj[0];
            i = (int) obj[1];

            checkoutPage.clickEditShippingAddress();
            Log.message(i++ + ". Clicked on Edit in Shipping Address.", driver);

            checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "NO", ShippingMethod.Standard, "valid_address7");
            checkoutPage.continueToPayment();
            Log.message(i++ + ". Continued to Billing/Payment Section.", driver);

            checkoutPage.clickEditAddressInBillingSection();
            Log.message(i++ + ". Clicked on Edit.", driver);

            //1
            Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkSaveBillingDetails", "billingAddressHeading", checkoutPage),
                    "The 'Save' Button should be displayed to the right of the Billing Address Heading",
                    "The 'Save' Button is displayed to the right of the Billing Address Heading",
                    "The 'Save' Button is not displayed to the right of the Billing Address Heading", driver);

            //2
            Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkCancelBillingDetails", "lnkSaveBillingDetails", checkoutPage),
                    "The Cancel Button should be displayed right to the Save Button, separated by a pipe (|) symbol",
                    "The Cancel Button is displayed right to the Save Button, separated by a pipe (|) symbol",
                    "The Cancel Button is not displayed right to the Save Button, separated by a pipe (|) symbol", driver);

            checkoutPage.clickCancelInBillingDetails();
            Log.message(i++ + ". Clicked on Cancel.", driver);


            if (checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lnkCancelBillingDetails"), checkoutPage)) {
                Log.softAssertThat(!checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lnkCancelBillingDetails"), checkoutPage),
                        "On clicking Cancel Button System should discard any entered form data.",
                        "On clicking Cancel Button System is discard any entered form data",
                        "On clicking Cancel Button System is not discard any entered form data", driver);

                Log.softAssertThat(!checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lnkCancelBillingDetails"), checkoutPage),
                        "The System should collapse the form fields and the Address Selector displays the previous selection before entering the Add New Address form",
                        "The System is collapse the form fields and the Address Selector displays the previous selection before entering the Add New Address form",
                        "The System is not collapse the form fields and the Address Selector displays the previous selection before entering the Add New Address form", driver);
            }

            checkoutPage.clickEditAddressInBillingSection();
            Log.message(i++ + ". Clicked on Edit.", driver);
            //4

            Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "billingHeading", "billingEditFirstname", checkoutPage),
                    "First Name should be displayed below the Billing Address heading.",
                    "First Name is displayed below the Billing Address heading.",
                    "First Name is not displayed below the Billing Address heading.", driver);

            Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("billingEditFirstname", "aria-required", "true", checkoutPage),
                    "First Name field should be a mandatory field.",
                    "First Name field is a mandatory field.",
                    "First Name field is not a mandatory field", driver);

            Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("billingEditFirstname", "billingEDitFirstNamePlaceHolder", "newFName", checkoutPage),
                    "First name placeholder should goes up when entering the text.",
                    "First name placeholder is goes up when entering the text.",
                    "First name placeholder is not going up when entering the text.", driver);

            if (Utils.isMobile()) {
                Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "billingEditFirstname", "billingEditLastname", checkoutPage),
                        "Last Name should be displayed below the First Name",
                        "Last Name is displayed below the First Name",
                        "Last Name is not displayed below the First Name", driver);
            } else {
                Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "billingEditLastname", "billingEditFirstname", checkoutPage),
                        "Last Name should be displayed right of the First Name",
                        "Last Name is displayed right of the First Name",
                        "Last Name is not displayed right of the First Name", driver);
            }

            Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("billingEditLastname", "aria-required", "true", checkoutPage),
                    "Last Name field should be a mandatory field.",
                    "Last Name field is a mandatory field.",
                    "Last Name field is not a mandatory field", driver);

            Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("billingEditLastname", "billingEDitLastNamePlaceHolder", "newLName", checkoutPage),
                    "LastName placeholder should goes up when entering the text.",
                    "LastName placeholder is going up when entering the text.",
                    "LastName placeholder is not going up when entering the text.", driver);

            if (Utils.isMobile()) {
                Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "billingEditLastname", "txtFieldAddressline1", checkoutPage),
                        "Address Line 1 should be displayed below the Last Name",
                        "Address Line 1 is displayed below the Last Name",
                        "Address Line 1 is not displayed below the Last Name", driver);
            } else {
                Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtFirstNameBillingDetails", "txtFieldAddressline1", checkoutPage),
                        "Address Line 1 should be displayed below the First Name",
                        "Address Line 1 is displayed below the First Name",
                        "Address Line 1 is not displayed below the First Name", driver);
            }

            Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("txtFieldAddressline1", "aria-required", "true", checkoutPage),
                    "Address Line 1 field should be a mandatory field.",
                    "Address Line 1 is a mandatory field.",
                    "Address Line 1 field is not a mandatory field", driver);

            Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("txtFieldAddressline1", "billingAddress1PlaceHolder", "3850 E Independence Blvd", checkoutPage),
                    "Address Line 1 placeholder should goes up when entering the text.",
                    "Address Line 1 placeholder is going up when entering the text.",
                    "Address Line 1 placeholder is not going up when entering the text.", driver);

            if (Utils.isMobile()) {
                Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtFieldAddressline1", "txtFieldAddressline2", checkoutPage),
                        "Address Line 2 should be displayed below of the Address Line 1",
                        "Address Line 2 is displayed below of the Address Line 1",
                        "Address Line 2 is not displayed below of the Address Line 1", driver);
            } else {
                Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtFieldAddressline2", "txtFieldAddressline1", checkoutPage),
                        "Address Line 2 should be displayed right of the Address Line 1",
                        "Address Line 2 is displayed right of the Address Line 1",
                        "Address Line 2 is not displayed right of the Address Line 1", driver);
            }

            Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("txtFieldAddressline2", "billingAddress2PlaceHolder", "west", checkoutPage),
                    "Address Line 2 placeholder should goes up when entering the text.",
                    "Address Line 2 placeholder is going up when entering the text.",
                    "Address Line 2 placeholder is not going up when entering the text.", driver);

            if (Utils.isMobile()) {
                Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtFieldAddressline2", "txtZipcodeBillingDetails", checkoutPage),
                        "Zip code should be displayed below the Address Line 2",
                        "Zip code is displayed below the Address Line 2",
                        "Zip code is not displayed below the Address Line 2", driver);
            } else {
                Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtFieldAddressline1", "txtZipcodeBillingDetails", checkoutPage),
                        "Zip code should be displayed below the Address Line 1",
                        "Zip code is displayed below the Address Line 1",
                        "Zip code is not displayed below the Address Line 1", driver);
            }

            Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("txtZipcodeBillingDetails", "aria-required", "true", checkoutPage),
                    "Zip code field should be a mandatory field.",
                    "Zip code is a mandatory field.",
                    "Zip code field is not a mandatory field", driver);

            Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "selectStateShipping", "txtZipcodeBillingDetails", checkoutPage),
                    "State should be displayed right of the Zip code",
                    "State is displayed right of the Zip code",
                    "State is not displayed right of the Zip code", driver);

            Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("selectStateShipping", "aria-required", "true", checkoutPage),
                    "State field should be a mandatory field.",
                    "State field is a mandatory field.",
                    "State field is not a mandatory field", driver);

            if (Utils.isMobile()) {
                Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtZipcodeBillingDetails", "txtCityBillingDetails", checkoutPage),
                        "City should be displayed below the Zip code",
                        "City is displayed below the Zip code",
                        "City is not displayed below the Zip code", driver);
            } else {
                Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtCityBillingDetails", "selectStateShipping", checkoutPage),
                        "City should be displayed right of the State",
                        "City is displayed right of the State",
                        "City is not displayed right of the State", driver);
            }

            Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("txtCityBillingDetails", "aria-required", "true", checkoutPage),
                    "City field should be a mandatory field.",
                    "City field is a mandatory field.",
                    "City field is not a mandatory field", driver);

            Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("txtCityBillingDetails", "billingCityPlaceHolder", "newName", checkoutPage),
                    "City placeholder should goes up when entering the text.",
                    "City placeholder is going up when entering the text.",
                    "City placeholder not going up when entering the text.", driver);

            if (Utils.isMobile()) {
                Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtFieldPhone", "drpCountryBilling", checkoutPage),
                        "Phone Number should be displayed above the United States (Country)",
                        "Phone Number is displayed above the United States (Country)",
                        "Phone Number is not displayed above the United States (Country)", driver);
            } else {
                Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "drpCountryBilling", "txtFieldPhone", checkoutPage),
                        "Phone Number should be displayed left to the United States (Country)",
                        "Phone Number is displayed left to the United States (Country)",
                        "Phone Number is not displayed left to the United States (Country)", driver);
            }

            Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForElement("txtFieldPhone", "aria-required", "true", checkoutPage),
                    "Phone Number field should be a mandatory field.",
                    "Phone Number field is a mandatory field.",
                    "Phone Number field is not a mandatory field", driver);

            Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("txtFieldPhone", "billingPhonePlaceHolder", "2233322333", checkoutPage),
                    "Phone Number placeholder should goes up when entering the text.",
                    "Phone Number placeholder is going up when entering the text.",
                    "Phone Number placeholder is not up when entering the text.", driver);

            Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("txtZipcodeBillingDetails", "billingZipPlaceHolder", "28502", checkoutPage),
                    "Zip code placeholder should goes up when entering the text.",
                    "Zip code placeholder is going up when entering the text.",
                    "Zip code placeholder should goes up when entering the text.", driver);

            checkoutPage.enterFirstName("$#$%");
            checkoutPage.enterLastName("$#$%");
            checkoutPage.enterCity("A");
            checkoutPage.enterPostalCode("$#$%");
            checkoutPage.enterPhoneCode("###");
            checkoutPage.enterAddress1("Adress");

            Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("errorMsgPostalcode"
                    , "errorMsgPhone", "errorMsgFirstName", "errorMsgcity", "errorMsgLastName"), checkoutPage),
                    "To check the invalid error message is displaying.",
                    "The invalid error message is displaying!",
                    "The invalid error message is not displaying", driver);

            checkoutPage.enterPhoneCode(phoneinvalid);
            Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("errorMsgPhone"), checkoutPage),
                    "User should receive Invalid phone number error unless they enter 10 numerical digits (or 13 digits with dashes).",
                    "User is receive Invalid phone number error unless they enter 10 numerical digits (or 13 digits with dashes).",
                    "User is not receive Invalid phone number error unless they enter 10 numerical digits (or 13 digits with dashes).", driver);

            checkoutPage.enterPostalCode(postalinvalid);
            Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("errorMsgPhone"), checkoutPage),
                    "System should allow the User to enter zip code as 5 digits (Ex: 55555) or hyphenated 9 Digits (55555-4444).",
                    "System is allow the User to enter zip code as 5 digits (Ex: 55555) or hyphenated 9 Digits (55555-4444).",
                    "System is not allow the User to enter zip code as 5 digits (Ex: 55555) or hyphenated 9 Digits (55555-4444)", driver);

            Log.testCaseResult();

        } // try
        catch (Exception e) {
            Log.exception(e, driver);
        } // catch
        finally {
            GlobalNavigation.RemoveAllProducts(driver);
            GlobalNavigation.removeAllSavedAddress(driver);
            Log.endTestCase(driver);
        } // finally
    }// M1_FBB_DROP4_C22514
    
    @Test(groups = { "critical", "desktop", "tablet", "mobile" }, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
    public void M2_FBB_DROP4_C22514(String browser) throws Exception {
        Log.testCaseInfo();

        //Load Test Data
        String searchKey = TestData.get("prd_markdown-single-price");

        // Get the web driver instance
        final WebDriver driver = WebDriverFactory.get(browser);
        String email = AccountUtils.generateEmail(driver);
        email =email.replaceAll("1@", "2@");
        String password = accountData.get("password_global");
        String credentials = email +"|"+ password;
        //Prerequisite: To create a user account if already not exists.
        {
            GlobalNavigation.registerNewUser(driver, 1, 0, credentials);
        }

        int i = 1;
        try {

            Object[] obj = GlobalNavigation.addProduct_Checkout(driver, searchKey, i, credentials);
            CheckoutPage checkoutPage = (CheckoutPage) obj[0];
            i = (int) obj[1];

            checkoutPage.clickEditShippingAddress();
            Log.message(i++ + ". Clicked on Edit in Shipping Address.", driver);

            checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "NO", ShippingMethod.Standard, "valid_address7");
            checkoutPage.continueToPayment();
            Log.message(i++ + ". Continued to Billing/Payment Section.", driver);

            checkoutPage.clickEditAddressInBillingSection();
            Log.message(i++ + ". Clicked on Edit.", driver);
          
            //5 - Verify the functionality of Make it Default checkbox
            Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "drpCountryBilling", "chkMakeDefaultInBillingDetails", checkoutPage),
                    "The 'Make it Default' should be displayed below the Address Form",
                    "The 'Make it Default' is displayed below the Address Form",
                    "The 'Make it Default' is not displayed below the Address Form", driver);

            Log.softAssertThat(checkoutPage.verifyMakeItDefaultIsCheckedInBillingDetails() == false,
                    "The Make It Default chkbox should be deselected",
                    "The Make It Default chkbox is deselected",
                    "The Make It Default chkbox is not deselected", driver);

            Log.softAssertThat(checkoutPage.verifySaveThisAddressIsCheckedInBillingDetails() == false,
                    "The Save This Address chkbox should not be selected automatically",
                    "The Save This Address chkbox is not selected automatically",
                    "The Save This Address chkbox is selected automatically", driver);

            checkoutPage.checkUncheckMakeItDefaultInBillingDetails(true);
            Log.message(i++ + ". Selected make It Default chkbox.", driver);

            Log.softAssertThat(checkoutPage.verifyMakeItDefaultIsCheckedInBillingDetails() == true,
                    "The Make It Default chkbox should be selected",
                    "The Make It Default chkbox is selected",
                    "The Make It Default chkbox is not selected", driver);

            Log.softAssertThat(checkoutPage.verifySaveThisAddressIsCheckedInBillingDetails() == true,
                    "The Save This Address chkbox should be selected automatically",
                    "The Save This Address chkbox is selected automatically",
                    "The Save This Address chkbox is not selected automatically", driver);

            Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDisabled(Arrays.asList("chkSaveThisAddressInBillingDetails"), checkoutPage),
                    "User should not be able to deselect the Save this address checkbox when the Make it Default checkbox is selected.",
                    "User is not be able to deselect the Save this address checkbox when the Make it Default checkbox is selected.",
                    "User able to deselect the Save this address checkbox when the Make it Default checkbox is selected.", driver);

            checkoutPage.checkUncheckMakeItDefaultInBillingDetails(false);
            Log.message(i++ + ". Deselected make It Default chkbox.", driver);

            Log.softAssertThat(checkoutPage.verifyMakeItDefaultIsCheckedInBillingDetails() == false,
                    "The Make It Default chkbox should be deselected",
                    "The Make It Default chkbox is deselected",
                    "The Make It Default chkbox is not deselected", driver);

            Log.softAssertThat(checkoutPage.verifySaveThisAddressIsCheckedInBillingDetails() == false,
                    "The Save This Address chkbox should be deselected automatically",
                    "The Save This Address chkbox is deselected automatically",
                    "The Save This Address chkbox is not deselected automatically", driver);

            checkoutPage.checkUncheckMakeItDefaultInBillingDetails(true);
            Log.message(i++ + ". Deselected make It Default chkbox.", driver);
            
            if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
            	Log.reference("Further verfication steps are not supported in current environment.");
            	Log.testCaseResult();
            	return;
            }

            //7 - Verify the functionality of Select Payment Method
            checkoutPage.continueToPayment();

            checkoutPage.fillingCardDetails1("card_Visa", false);
            Log.message(i++ + ". Card Details filling Successfully", driver);

            checkoutPage.clickOnPaymentDetailsContinueBtn();
            Log.message(i++ + ". Continued to Review & Place Order", driver);

            OrderConfirmationPage receipt = checkoutPage.clickOnPlaceOrderButton();
            Log.message(i++ + ". Order Placed successfully", driver);

            Log.softAssertThat(receipt.elementLayer.verifyPageElements(Arrays.asList("readyElement"), receipt),
                    "Order should be placed!",
                    "Order is placed sucessfully!",
                    "Order is not placed!", driver);

            Log.testCaseResult();

        } // try
        catch (Exception e) {
            Log.exception(e, driver);
        } // catch
        finally {
            GlobalNavigation.RemoveAllProducts(driver);
            GlobalNavigation.removeAllSavedAddress(driver);
            Log.endTestCase(driver);
        } // finally

    }// M2_FBB_DROP4_C22514
    
    @Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
    public void M3_FBB_DROP4_C22514(String browser) throws Exception {
        Log.testCaseInfo();

        //Load Test Data
        String searchKey = TestData.get("prd_markdown-single-price");

        // Get the web driver instance
        final WebDriver driver = WebDriverFactory.get(browser);
        String email = AccountUtils.generateEmail(driver);
        email = email.replaceAll("1@", "3@");
        String password = accountData.get("password_global");
        String credentials = email +"|"+ password;
        //Prerequisite: To create a user account if already not exists.
        {
            GlobalNavigation.registerNewUser(driver, 1, 0, credentials);
        }

        int i = 1;
        try {

            Object[] obj = GlobalNavigation.addProduct_Checkout(driver, searchKey, i, credentials);
            CheckoutPage checkoutPage = (CheckoutPage) obj[0];
            i = (int) obj[1];

            Log.softAssertThat(checkoutPage.getselectedShippingAddress().toLowerCase().contains("john"),
                    "Saved address should be displayed in the shipping address for next product checkout.",
                    "Saved address is displayed in the shipping address for next product checkout.",
                    "Saved address is not displayed in the shipping address for next product checkout.", driver);

            checkoutPage.continueToPayment();
            Log.message(i++ + ". Continued to payment", driver);
            checkoutPage.clickEditAddressInBillingSection();
            Log.message(i++ + ". Clicked on Edit.", driver);
            checkoutPage.selectValueFromSavedBillingAddressesDropdownByIndex(1);
            Log.message(i++ + ". Selected value from dropdown menu.", driver);
            checkoutPage.clickEditAddressInBillingSection();
            Log.message(i++ + ". Clicked on Edit.", driver);

            if (Utils.isMobile()) {
                Log.softAssertThat(checkoutPage.elementLayer.verifyElementTextContains("selectedBillingAddressMobile", "|", checkoutPage),
                        "Selected address should displayed with the pipe symbol.",
                        "Selected address is displayed with the pipe symbol.",
                        "Selected address is not displayed with the pipe symbol.", driver);
            } else {
                Log.softAssertThat(checkoutPage.elementLayer.verifyElementTextContains("selectedBillingAddress", "|", checkoutPage),
                        "Selected address should displayed with the pipe symbol.",
                        "Selected address is displayed with the pipe symbol.",
                        "Selected address is not displayed with the pipe symbol.", driver);
            }

            Log.testCaseResult();

        } // try
        catch (Exception e) {
            Log.exception(e, driver);
        } // catch
        finally {
            GlobalNavigation.RemoveAllProducts(driver);
            Log.endTestCase(driver);
        } // finally
    }// M3_FBB_DROP4_C22514
    
}// search
