package com.fbb.testscripts.drop4;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.GiftCardPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22508 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22508(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test data
		String searchKey = TestData.get("prd_expedited-ship-eligible") + "|" + TestData.get("prd_expedited-ship-ineligible");
		String normalPrd = TestData.get("prd_expedited-ship-eligible");
		String shpRestrictPrd = TestData.get("prd_expedited-ship-ineligible");
		ShippingMethod shippingMethod = ShippingMethod.Express;
		String shippingMethodText = checkoutProperty.get("ship_2-Day_Express");
		String address = "valid_address7";
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			String username = AccountUtils.generateEmail(driver);
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Fullbeauty Home Page.", driver);

			GiftCardPage gcPage = homePage.footers.navigateToGiftCardLandingPage();
			Log.message(i++ + ". Navigated to Gift Card landing page.", driver);

			PdpPage pdpPage = gcPage.navigateToGiftCards();
			Log.message(i++ + ". Navigated to PDP page for :: " + pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Bag!", driver);
			
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, searchKey, i, username, true);
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
			@SuppressWarnings("unchecked")
			LinkedHashMap<String, String> prdNames= (LinkedHashMap<String, String>) obj[2];
	
			//Step-1: Verify the functionality of Items Heading
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "legendItemHeading", "divCheckoutSummeryContent", checkoutPage), 
					"Item Heading should be displayed on the top of the Item summary section.", 
					"Item Heading is displayed on the top of the Item summary section.", 
					"Item Heading is not displayed on the top of the Item summary section.", driver);
	
			//Step-2: Verify the functionality of Edit Link
			Log.softAssertThat(checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkEditInShoppingBagList", "legendItemHeading", checkoutPage), 
					"Edit Link should be displayed on top right of the Item summary section", 
					"Edit Link is displayed on top right of the Item summary section", 
					"Edit Link is not displayed on top right of the Item summary section", driver);
			
			HashSet<String> checkoutList = checkoutPage.getCheckoutSummeryNameList();
			System.out.println("checkoutList:: " + checkoutList);
			
			checkoutPage.scrollToItemsListSection();
			ShoppingBagPage cartPage = checkoutPage.clickOnEditInShoppingBagList();
			Log.message(i++ + ". Clicked on Edit Link in Shopping bag List", driver);
			
			HashSet<String> cartList = cartPage.getCartItemNameList();
			System.out.println("cartList:: " + cartList);
			
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("miniCartContent"), cartPage)
							&& cartPage.elementLayer.compareTwoHashSet(checkoutList, cartList), 
					"On clicking the Edit link, the User should be taken back to the Cart with the same item information as in Item Summary Section",
					"On clicking the Edit link, the User taken back to the Cart with the same item information as in Item Summary Section",
					"On clicking the Edit link, the User not taken back to the Cart with the same item information as in Item Summary Section", driver);
	
			//Step-3: Verify the functionality of Item Row
			Log.reference("Shipping Method Exception verifications covered in C22507(Steps-1 to 6)");
	
			SignIn signIn = (SignIn)cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page - Login Section.", driver);
	
			signIn.typeGuestEmail(username);
			checkoutPage = signIn.clickOnContinueInGuestLogin();
			Log.message(i++ + ". Continued to Checkout Page - Shipping Section.", driver);
	
			checkoutPage.fillingShippingDetailsAsGuest("YES", address, shippingMethod);
			Log.message(i++ + ". Shipping Address filled successfully!", driver);
	
			checkoutPage.continuePOBoxValidation();
			Log.message(i++ + ". Continuing with PO Box modal", driver);
	
			checkoutPage.scrollToItemsListSection();
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageListElements(Arrays.asList("lblShippingMethodInCartList"), checkoutPage), 
					"Item row should be displayed with shipping methods",
					"Item row displayed with shipping methods",
					"Item row not displayed with shipping methods", driver);
	
			Log.softAssertThat(checkoutPage.getShippingMethodOfProduct(prdNames.get(shpRestrictPrd)).toLowerCase().contains("GROUND".toLowerCase()), 
					"System should display standard method for the exception or restricted items",
					"System display standard method for the exception or restricted items",
					"System not display standard method for the exception or restricted items", driver);
	
			Log.softAssertThat(checkoutPage.getShippingMethodOfProduct(prdNames.get(normalPrd)).toLowerCase().equals(shippingMethodText.toLowerCase()), 
					"System should display the upgraded method for the user selected items",
					"System display the upgraded method for the user selected items",
					"System not display the upgraded method for the user selected items", driver);
	
			//Step-4: Verify the functionality of Gift Certificate Row - verified along with Step-3
	
			//Step-3: ER-a
			pdpPage = checkoutPage.navigateToPDPByProductImg(prdNames.get(normalPrd));
			Log.message(i++ + ". Navigated To PDP Page for Product :: " + pdpPage.getProductName());
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage),
					"Clicking on the Products's Image should take the User to the PDP",
					"Clicking on the Products's Image take the User to the PDP",
					"Clicking on the Products's Image not take the User to the PDP", driver);
	
			driver.get(Utils.getWebSite() + "/checkout");
			checkoutPage.scrollToItemsListSection();
			Log.message(i++ + ". Navigated Back to Checkout Page", driver);
	
			pdpPage = checkoutPage.navigateToPDPByProductName(prdNames.get(normalPrd));
			Log.message(i++ + ". Navigated To PDP Page for Product :: " + pdpPage.getProductName());
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage), 
					"Clicking on the Products's Name should take the User to the PDP",
					"Clicking on the Products's Name take the User to the PDP",
					"Clicking on the Products's Name not take the User to the PDP", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C22508
}// search
