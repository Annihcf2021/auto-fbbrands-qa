package com.fbb.testscripts.drop4;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22556 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22556(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String searchKey = TestData.get("prd_variation1");
		String[] password = {accountData.get("password_global"), "123", "123456789123456789123456"};
	
		//Create the webdriver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	

		int i = 1;
		try {
			char[] charArray = { 'a', 'e', 'i', 'o', 'u', 'd' , 'b' };
			String rand = RandomStringUtils.random(6,charArray);
			
			Object[] obj = GlobalNavigation.addProduct_PlaceOrder_G(driver, searchKey, i, "a" + rand + AccountUtils.generateEmail(driver));
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
	
			Log.softAssertThat(driver.getCurrentUrl().contains("orderconfirmation") &&
					checkoutPage.getOrderConfirmationMessage().equals("Thank you for your order"), 
					"Order should be placed successfully as a Guest user and Order confirmation page should be displayed!", 
					"Order is placed successfully as a Guest user and Order confirmation page is displayed!",
					"Order is not placed successfully as a Guest user and Order confirmation page is not displayed!", driver);
	
			//Step -1 :
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtCreateAccountMobile"), checkoutPage), 
						"Create Account text should be displayed!", 
						"Create Account text is displayed!",
						"Create Account text is not displayed!", driver);
			} else {
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtCreateAccount"), checkoutPage), 
						"Create Account text should be displayed!", 
						"Create Account text is displayed!",
						"Create Account text is not displayed!", driver);
			}
			
			//Step-2
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtLoginContent"), checkoutPage), 
					"Subline heading text should be displayed!", 
					"Subline heading text is displayed!",
					"Subline heading text is not displayed!", driver);
			
			//Step-4
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProfileName"), checkoutPage), 
					"Profile name should be displayed!", 
					"Profile name is displayed!",
					"Profile name is not displayed!", driver);
			
			//Step-5
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProfileEmail"), checkoutPage), 
					"Profile Email should be displayed!", 
					"Profile Email is displayed!",
					"Profile Email is not displayed!", driver);
			
			//Step-6
			checkoutPage.clickOnEditLinkInOrderReceiptAccountCreation();
			Log.message(i++ + ". Clicked on 'Email Edit Link'");
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("contentCreateAccountSection"), checkoutPage), 
					"Create Account content section should be displayed!", 
					"Create Account content section is displayed!",
					"Create Account content section is not displayed!", driver);
	
			//Step-7
			checkoutPage.clickOnCreateAccountButtonWithoutEnteringPassword();
			Log.message(i++ + ". Clicked on 'Create Account' button without entering paswword");
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("txtPasswordError", "rgba(255, 58, 81, 1)", checkoutPage), 
					"Password Error message should be displayed!", 
					"Password Error message is displayed!",
					"Password Error message is not displayed!", driver);
	
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("txtConfirmPasswordError", "rgba(255, 58, 81, 1)", checkoutPage), 
					"Confirm Password Error message should be displayed!", 
					"Confirm Password Error message is displayed!",
					"Confirm Password Error message is not displayed!", driver);
	
			checkoutPage.enterPassword(password[1]);
			Log.message(i++ + ". Entered Password less than 7 charcters");
	
			checkoutPage.enterconfirmPassword(password[1]);
			Log.message(i++ + ". Entered confirm Password less than 7 charcters");
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("txtPasswordError", "rgba(255, 58, 81, 1)", checkoutPage), 
					"Password Error message should be displayed when entering less than 7 characters!", 
					"Password Error message is displayed when entering less than 7 characters!",
					"Password Error message is not displayed when entering less than 7 characters!", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("txtConfirmPasswordError", "rgba(255, 58, 81, 1)", checkoutPage), 
					"Confirm Password Error message should be displayed when entering less than 7 characters!", 
					"Confirm Password Error message is displayed when entering less than 7 characters!",
					"Confirm Password Error message is not displayed when entering less than 7 characters!", driver);
	
			checkoutPage.clearPasswordTextFields();
			Log.message(i++ + ". Cleared Password and confirm Password fields");
	
			checkoutPage.enterPassword(password[2]);
			Log.message(i++ + ". Entered Password greater than 20 charcters");
	
			checkoutPage.enterconfirmPassword(password[2]);
			Log.message(i++ + ". Entered Password greater than 20 charcters");
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("txtPasswordError", "rgba(255, 58, 81, 1)", checkoutPage), 
					"Password Error message should be displayed when entering greater than 20 characters!", 
					"Password Error message is displayed when entering greater than 20 characters!",
					"Password Error message is not displayed when entering greater than 20 characters!", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("txtConfirmPasswordError", "rgba(255, 58, 81, 1)", checkoutPage), 
					"Confirm Password Error message should be displayed when entering greater than 20 characters!", 
					"Confirm Password Error message is displayed when entering greater than 20 characters!",
					"Confirm Password Error message is not displayed when entering greater than 20 characters!", driver);
	
			checkoutPage.clearPasswordTextFields();
			Log.message(i++ + ". Cleared Password and confirm Password fields");
	
			checkoutPage.enterPassword(password[0]);
			Log.message(i++ + ". Entered Password ");
	
			checkoutPage.enterconfirmPassword(password[1]);
			Log.message(i++ + ". Entered confirm Password ");
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("txtConfirmPasswordError", "rgba(255, 58, 81, 1)", checkoutPage), 
					"Confirm Password Error message should be displayed when entering password greater than 20 characters!", 
					"Confirm Password Error message is displayed when entering password greater than 20 characters!",
					"Confirm Password Error message is not displayed when entering password greater than 20 characters!", driver);
	
			checkoutPage.clearPasswordTextFields();
			Log.message(i++ + ". Cleared Password and confirm Password fields");
	
			checkoutPage.enterPassword(password[0]);
			Log.message(i++ + ". Entered Password between 7 -20 characters");
	
			checkoutPage.enterconfirmPassword(password[0]);
			Log.message(i++ + ". Entered Password between 7 -20 characters");
	
			Log.softAssertThat(checkoutPage.getEnteredPassword().equals(checkoutPage.getEnteredConfirmPassword()), 
					"Password and Confirm Password should be same!", 
					"Password and Confirm Password is same!",
					"Password and Confirm Password is not same!", driver);
	
			MyAccountPage myaccountPage = checkoutPage.clickOnCreateAccountButton();
			Log.message(i++ + ". Clicked on create account button !", driver);
	
			Log.softAssertThat(myaccountPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myaccountPage), 
					"User Account should be navigated to Account Register page!", 
					"User Account is navigated to Account Register page!", 
					"User Account is not navigated to Account Register page!", 
					driver );	
			
			AddressesPage addressesPage = myaccountPage.clickOnAddressLink();
			Log.message(i++ + ". Clicked on Address tab in my account page !");
	
			Log.softAssertThat(addressesPage.elementLayer.verifyPageElements(Arrays.asList("addressSaved"), addressesPage), 
					"Default Address should be saved in address book !", 
					"Default Address is saved in address book !",
					"Default Address is not saved in address book !", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C22556
}// search
