package com.fbb.testscripts.drop4;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP4_C22536 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP4_C22536(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String comboProductPromo = TestData.get("prd_promo_product_promo");
		String searchKey = comboProductPromo.split("\\|")[0];
		String promoCode = comboProductPromo.split("\\|")[1];
	
		//Create web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			//Load Regular Price Product
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to bag.", driver);
	
			ShoppingBagPage shoppingBag = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on 'My Bag' icon in header");
			
			shoppingBag.updateQuantityByPrdIndex(0, "3");
			Log.message(i++ + ". Set quantity.", driver);
	
			Log.reference("Refer TC# 22533 for Step 1");
			
			double unitPrice = shoppingBag.getProductSubtotal();
			
			double SubtotalBefApplyPrCde = Math.round(shoppingBag.getOrderTotal());
			Log.event("Subtotal before promocode: " + SubtotalBefApplyPrCde);
			
			shoppingBag.enterPromoCode(promoCode);
			Log.message(i++ + ". Entered Promo Code '"+promoCode+"' ", driver);
	
			shoppingBag.clickOnApplycouponButton();
			Log.message(i++ + ". Coupon Applied", driver);
	
			double SubtotalAfApplyPrCde = Math.round(shoppingBag.getOrderTotal());
			Log.event("Subtotal after promocode: " + SubtotalAfApplyPrCde);
			
			Log.softAssertThat(SubtotalBefApplyPrCde != SubtotalAfApplyPrCde, 
					"Subtotal Price should be updated based on the coupon added", 
					"Subtotal Price is updated based on the coupon added",
					"Subtotal Price is not updated based on the coupon added", driver);
			
			shoppingBag.clickOnSplProductEdit();
			Log.message(i++ + ". Clicked on Edit.", driver);
			
			int qtyToSelect = 5;
			if(Utils.isDesktop()) {
				shoppingBag.selectQtyInEditOverlay(qtyToSelect);
			} else {
				pdpPage.selectQty(""+qtyToSelect);
			}
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Updated quantity.", driver);
	
			//Step - 1: Verify the display of Promotion Callout Message
			if(Utils.isMobile()) {
				Log.softAssertThat(shoppingBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "promotionCalloutMessage", "txtUnitPrice", shoppingBag),
						"Promotion Callout Message should be displayed below the 'PRICE' field",
						"Promotion Callout Message is displayed below the 'PRICE' field",
						"Promotion Callout Message is not displayed below the 'PRICE' field", driver);
			}else {
				Log.softAssertThat(shoppingBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtUnitPrice", "promotionCalloutMessage", shoppingBag),
						"Promotion Callout Message should be displayed below the 'PRICE' field",
						"Promotion Callout Message is displayed below the 'PRICE' field",
						"Promotion Callout Message is not displayed below the 'PRICE' field", driver);
			}
	
			//Step - 2: Verify the functionality of Item Total
			if(Utils.isMobile()) {
				Log.softAssertThat(shoppingBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "qtyDropDownInCart", "productTotalPrice", shoppingBag), 
						"Item Total should be displayed to the right hand side of the Quantity Selected.", 
						"Item Total is displayed to the right hand side of the Quantity Selected.", 
						"Item Total is not displayed to the right hand side of the Quantity Selected", driver);
			} else {
				Log.softAssertThat(shoppingBag.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productTotalPrice", "sectionQtyDropDown", shoppingBag), 
						"Item Total should be displayed to the right hand side of the Quantity Selected.", 
						"Item Total is displayed to the right hand side of the Quantity Selected.", 
						"Item Total is not displayed to the right hand side of the Quantity Selected", driver);
			}
			
			Log.softAssertThat(shoppingBag.elementLayer.verifyElementDisplayed(Arrays.asList("txtUpdatedSubTotal"), shoppingBag),
					"Discounted Price should be displayed in Shopping bag", 
					"Discounted Price is displayed in Shopping bag", 
					"Discounted Price is not displayed in Shopping bag", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(shoppingBag.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtUpdatedSubTotal", "totalProductPriceUnAdjusted", shoppingBag), 
						"New price should be displayed right side to the old price.", 
						"New price is displayed right side to the old price.", 
						"New price is not displayed right side to the old price", driver);
			} else {
				Log.softAssertThat(shoppingBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "totalProductPriceUnAdjusted", "txtUpdatedSubTotal", shoppingBag), 
						"New price should be displayed below the old price.", 
						"New price is displayed below the old price.", 
						"New price is not displayed below the old price", driver);
			}
			
			Log.softAssertThat(unitPrice != shoppingBag.getProductSubtotal(), 
					"Price should be updated based on the updated qty.", 
					"Price is updated based on the updated qty.",
					"Price is not updated based on the updated qty.",driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C22536
}// search