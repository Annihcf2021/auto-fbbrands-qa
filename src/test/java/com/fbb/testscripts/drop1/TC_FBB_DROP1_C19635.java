package com.fbb.testscripts.drop1;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP1_C19635 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP1_C19635(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		String productPriceRange = TestData.get("prd_price-range");
		
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, email + "|" + password);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			//Step-1: Check the contents in the Header
			//Before login
			boolean promoStatus = false;
			if(!BrandUtils.isBrand(Brand.el)) {
				promoStatus = headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divPromoContent"), headers);
			} else {
				promoStatus = true;
			}
			
			String primaryLogo;
			if(Utils.isDesktop()) {
				primaryLogo = "brandLogo";
			} else {
				primaryLogo = "brandLogoMobTab";
			}

			Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(
						Arrays.asList("lnkCustomerServiceDesktop", primaryLogo, "lnkSignInDesktop", 
								"lnkViewBagDesktop", "txtSearch", "divUtilityBar", "divOfferFlyout"), headers) && promoStatus, 
					"The contents of Header should contain stated elements.", 
					"The contents of Header contains all mentioned elements.", 
					"The contents of Header does not conatin all given elements.", driver);
			
			if(Utils.isTablet()) {
				Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(
						Arrays.asList("lnkHamburger"), headers), 
					"Header in tablet should contain Hamburger menu toggle.", 
					"Header in tablet contains Hamburger menu toggle.", 
					"Header in tablet does not contain Hamburger menu toggle.", driver);
			}
			
			//Step-2: Verify the components in Utility Bar
			Log.reference("Step 2 covered as part of Step 1.");
			
			//Step-3: Verify the Utility Bar - Customer Service
			Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkSignInDesktop", "lnkCustomerServiceDesktop", headers), 
					"Customer service link should display to the left side of Sign In link in the Utility Bar.", 
					"Customer service link displays to the left side of Sign In link in the Utility Bar.", 
					"Customer service link does not display to the left side of Sign In link.", driver);
			
			//Step-4: Verify the Sign in Guest State
			headers.mouseOverAccountMenu();
			Log.message(i++ + ". Moused hovered/tapped on sign in link", driver);
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("flytSignInUnregisteredDesktop"), headers), 
					"Tapping on SIGN IN , Account Sign-In fly out should be opened", 
					"Account Sign-In fly out is opened", 
					"Account Sign-In fly out has not opened", driver);
			
			//Step-1 continued
			headers.navigateToMyAccount(email, password);
			headers.navigateToHome();
			
			//After login
			if(BrandUtils.isBrand(Brand.el)) {
				Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(
						Arrays.asList("lnkCustomerServiceDesktop", primaryLogo, "lblMyAccDesktop", 
								"lnkViewBagDesktop", "txtSearch", "divUtilityBar", "divOfferFlyout"), headers), 
					"The contents of Header should contain stated elements.", 
					"The contents of Header contains all mentioned elements.", 
					"The contents of Header does not conatin all given elements.", driver);
			} else {
				Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(
						Arrays.asList("lnkCustomerServiceDesktop", primaryLogo, "lblMyAccDesktop", "divPromoContent", 
								"lnkViewBagDesktop", "txtSearch", "divUtilityBar", "divOfferFlyout"), headers), 
					"The contents of Header should contain stated elements.", 
					"The contents of Header contains all mentioned elements.", 
					"The contents of Header does not conatin all given elements.", driver);
			}
			
			if(Utils.isTablet()) {
				Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(
						Arrays.asList("lnkHamburger"), headers), 
					"Header in tablet should contain Hamburger menu toggle.", 
					"Header in tablet contains Hamburger menu toggle.", 
					"Header in tablet does not contain Hamburger menu toggle.", driver);
			}
			
			//Step-5: Check the Sign in Authenticated state
			Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkMiniCart", "lblMyAccDesktop", headers), 
					"The My account section should be available at left side of the My bag link.", 
					"The My account section is available at left side of the My bag link.", 
					"The My account section is not available at left side of the My bag link.", driver);
			
			//Step-6: Verify the Mini cart
			ShoppingBagPage shoppingBag = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to shopping bag.", driver);
			shoppingBag.removeAllCartProduct();
			Log.message(i++ + ". Emptied the cart.", driver);
			
			PdpPage pdp = headers.navigateToPDP(productPriceRange);
			Log.message(i++ + ". Navigate to product page.", driver);
			
			pdp.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to bag.", driver);

			shoppingBag = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to shopping bag.", driver);
			int cartCount = shoppingBag.getProductCountInCart();
			
			Log.softAssertThat(headers.elementLayer.verifyAttributeForElement("lblMiniCartCountDesktop", "innerHTML", ""+cartCount, headers), 
					"If the customer has items in their cart, then the mini cart should display the number of items in the shopping cart as an icon with the numeric representation.", 
					"the mini cart displays the number of items in the shopping cart as an icon with the numeric representation.", 
					"the mini cart does not display the number of items in the shopping cart as an icon with the numeric representation.", driver);
			
			//Step-7: Check the hamburger icon
			headers.navigateToHome();
			if(Utils.isTablet()) {
				Log.softAssertThat(headers.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divUtilityBar", "lnkHamburger", headers), 
						"The Hamburger menu icon should be available below the utility bar.", 
						"The Hamburger menu icon is available below the utility bar.", 
						"The Hamburger menu icon is not available below the utility bar.", driver);
				
				if(BrandUtils.isBrand(Brand.rm)) {
					Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, primaryLogo, "lnkHamburger", headers) 
									&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divOfferFlyout", primaryLogo, headers), 
							"The Hamburger menu icon should be available left side to the Brand logo", 
							"The Hamburger menu icon is available left side to the Brand logo", 
							"The Hamburger menu icon is not available left side to the Brand logo", driver);
				}
				if(BrandUtils.isBrand(Brand.ww)) {
					Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divOfferFlyout", "lnkHamburger", headers) 
									&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, primaryLogo, "divOfferFlyout", headers), 
							"The Hamburger menu icon should be left to the Offers link", 
							"The Hamburger menu icon is on left to the Offers link", 
							"The Hamburger menu icon is not left to the Offers link", driver);
				}
				if(BrandUtils.isBrand(Brand.ks)) {
					Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divOfferFlyout", "lnkHamburger", headers) 
									&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, primaryLogo, "divOfferFlyout", headers), 
							"The Hamburger menu icon should be left to the Offers link", 
							"The Hamburger menu icon is on left to the Offers link", 
							"The Hamburger menu icon is not left to the Offers link", driver);
				}
				if(BrandUtils.isBrand(Brand.el)) {
					Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divOfferFlyout", "lnkHamburger", headers) 
									&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, primaryLogo, "divOfferFlyout", headers), 
							"The Hamburger menu icon should be left to the Offers link", 
							"The Hamburger menu icon is on left to the Offers link", 
							"The Hamburger menu icon is not left to the Offers link", driver);
				}
			}
			
			//Step-8: Tap on selected brand logo
			if(Utils.isTablet()) {
				Log.reference("Step 8 Tablet part covered as part of step 7.");
			}
			if(Utils.isDesktop()) {
				if(BrandUtils.isBrand(Brand.rm)) {
					Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divOfferFlyout", primaryLogo, headers), 
							"Brand logo should display to the left-hand side below the Utility Bar", 
							"Brand logo displays to the left-hand side below the Utility Bar", 
							"Brand logo does not display to the left-hand side below the Utility Bar", driver);
				}
				if(BrandUtils.isBrand(Brand.ww)) {
					Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, primaryLogo, "divOfferFlyout", headers)
									&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSearch", primaryLogo,  headers), 
							"The brand logo should display in the center.", 
							"The brand logo displays in the center", 
							"The brand logo does not display in the center.", driver);
				}
				if(BrandUtils.isBrand(Brand.ks)) {
					Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, primaryLogo, "divOfferFlyout", headers)
									&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSearch", primaryLogo,  headers), 
							"The brand logo should display in the center.", 
							"The brand logo displays in the center", 
							"The brand logo does not display in the center.", driver);
				}
				if(BrandUtils.isBrand(Brand.el)) {
					Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, primaryLogo, "divOfferFlyout", headers)
									&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSearch", primaryLogo,  headers), 
							"The brand logo should display in the center.", 
							"The brand logo displays in the center", 
							"The brand logo does not display in the center.", driver);
				}
			}
			
			//Step-9: Check the offers link in the home page
			if(BrandUtils.isBrand(Brand.rm)) {
				Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSearch", "divOfferFlyout", headers) 
								&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divOfferFlyout", primaryLogo, headers), 
						"Offer callout should display next by the search field.", 
						"Offer callout displays next by the search field.", 
						"Offer callout does not display next by the search field.", driver);
			}
			if(BrandUtils.isBrand(Brand.ww)) {
				Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSearch", "divOfferFlyout", headers) 
								&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, primaryLogo, "divOfferFlyout", headers), 
						"Offers should be display at the left side of the utility bar.", 
						"Offers is displayed at the left side of the utility bar.", 
						"Offers is not displayed at the left side of the utility bar.", driver);
			}
			if(BrandUtils.isBrand(Brand.ks)) {
				Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSearch", "divOfferFlyout", headers) 
								&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, primaryLogo, "divOfferFlyout", headers), 
						"Offers should be display at the left side of the utility bar.", 
						"Offers is displayed at the left side of the utility bar.", 
						"Offers is not displayed at the left side of the utility bar.", driver);
			}
			if(BrandUtils.isBrand(Brand.el)) {
				Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSearch", "divOfferFlyout", headers) 
								&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, primaryLogo, "divOfferFlyout", headers), 
						"Offers should be display at the left side of the utility bar.", 
						"Offers is displayed at the left side of the utility bar.", 
						"Offers is not displayed at the left side of the utility bar.", driver);
			}
			
			//Step-10: Check CATALOG QUICK ORDER link on the homepage
			if(BrandUtils.isBrand(Brand.rm)) {
				if(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("spanCatalogQuickOrder"), headers)) {
					Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "spanCatalogQuickOrder", "divOfferFlyout", headers) 
									&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSearch", "spanCatalogQuickOrder", headers),
							"CATALOG QUICK ORDER link should display next to the offer link", 
							"CATALOG QUICK ORDER link displays next to the offer link", 
							"CATALOG QUICK ORDER link does not display next to the offer link", driver);
				}
			}else if(!BrandUtils.isBrand(Brand.el)) {
				if(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("spanCatalogQuickOrder"), headers)) {
					Log.softAssertThat(headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkSignInDesktop", "spanCatalogQuickOrder", headers) 
									&& headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "spanCatalogQuickOrder", "txtSearch", headers),
							"CATALOG QUICK ORDER link should display next to the offer link", 
							"CATALOG QUICK ORDER link displays next to the offer link", 
							"CATALOG QUICK ORDER link does not display next to the offer link", driver);
				}
			}
			
			//Step-11: Check the search box in the Home page
			if(Utils.isTablet()) {
				int searchWidthBefore = Integer.parseInt(headers.elementLayer.getElementCSSValue("divSearchBox", "width", headers).replaceAll("[^0-9]", ""));
				headers.typeTextInSearchField("a");
				int searchWidthAfter = Integer.parseInt(headers.elementLayer.getElementCSSValue("divSearchBox", "width", headers).replaceAll("[^0-9]", ""));
				Log.softAssertThat(searchWidthAfter > searchWidthBefore, 
						"When user search with search term and tap, search box should expand horizontally to the left.", 
						"When user search with search term and tap, search box expands horizontally to the left.", 
						"When user search with search term and tap, search box does not expand.", driver);
			}
			
			//Step-12: Verify Global navigation bar
			BrowserActions.scrollToTopOfPage(driver);
			if(Utils.isDesktop()) {
				Log.softAssertThat(headers.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, primaryLogo, "divGlobalNavigation", headers)
								&& headers.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divOfferFlyout", "divGlobalNavigation", headers)
								&& headers.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "txtSearch", "divGlobalNavigation", headers), 
						"Global navigation bar should be displayed below the brand logo, offers link & search box.", 
						"Global navigation bar is displayed below the brand logo, offers link & search box.", 
						"Global navigation bar is not displayed below the brand logo, offers link & search box.", driver);
			}
			
			if(!BrandUtils.isBrand(Brand.el)) { //Promo content not in scope for Ellos
				//Step-13: Verify bottom Promotional Content below the Offers link
				Log.softAssertThat(headers.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divOfferFlyout", "divPromoContent", headers), 
						"Bottom Promotional Content should be displayed below Offers link.", 
						"Bottom Promotional Content is displayed below Offers link.", 
						"Bottom Promotional Content is not displayed below Offers link.", driver);
			
				//Step-14: Verify Top Promotional Content above utility bar
				if(headers.elementLayer.getElementHeight("divTopPromoBanner", headers)>0) {
					Log.softAssertThat(headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "divTopPromoBanner", "divUtilityBar", headers), 
							"When displayed, top Promotional Content should be displayed above utility bar.", 
							"Top Promotional Content is displayed above utility bar.", 
							"Top Promotional Content is not displayed above utility bar.", driver);
				}
			}
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	} // M1_FBB_DROP1_C19635

}
