package com.fbb.testscripts.drop1;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.headers.HamburgerMenu;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP1_C19654 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	@Test(groups = { "critical", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP1_C19654(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String level1 = TestData.get("level-1");
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			Headers headers = homePage.headers;
			
			//Step-1: Verify the expanded hamburger menu navigation when tapped on it.
			HamburgerMenu hMenu = (HamburgerMenu) headers.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger Menu opened.", driver);
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayedWithoutScrolling(
						Arrays.asList("btnHamburgerClose","navigationCategory1","fldEmailSignUp","divFooterCustomerService"), hMenu), 
					"Hamburger menu should display close button, global navigation, email sign up, and customer serivice links.", 
					"Hamburger menu displays close button, global navigation, email sign up, and customer serivice links.", 
					"Hamburger menu does not display close button, global navigation, email sign up, and customer serivice links.", driver);
			
			//Step-2: Verify menu close button in the hamburger icon page.
			headers.openCloseHamburgerMenu("close");
			Log.message(i++ + ". Hamburger Menu closed.", driver);
			Log.softAssertThat(hMenu.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("divHamburgerOverlay"), hMenu), 
					"Closing hamburger menu should reveal the original page.", 
					"Closing hamburger menu reveals the original page.", 
					"Closing hamburger menu does not reveal the original page.", driver);
			
			//Step-3: Check the Global navigation in the hamburger icon page
			headers.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger Menu opened.", driver);
			Log.softAssertThat(hMenu.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("lnkMenuItems","lstCategoryArrow"), hMenu), 
					"The system should display the L1 categories.", 
					"The system displays the L1 categories.", 
					"The system does not display the L1 categories.", driver);
			
			//Step-4: Check the Email Sign up in the humburger icon page
			Log.softAssertThat(hMenu.elementLayer.verifyVerticalAllignmentOfElements(driver, "navigationCategory1", "fldEmailSignUp", hMenu), 
					"Email sign up button should be displayed below global navigation under hamburger menu.", 
					"Email sign up button is displayed below global navigation under hamburger menu.", 
					"Email sign up button is not displayed below global navigation under hamburger menu.", driver);
			
			//Step-5: Check the customer service links
			Log.softAssertThat(hMenu.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldEmailSignUp", "divFooterCustomerService", hMenu), 
					"Customer service links should be displayed below the signup field", 
					"Customer service links is displayed below the signup field", 
					"Customer service links is not displayed below the signup field", driver);
			
			//Step-6: Check the selected category in the hamburger menu.
			hMenu.navigateToSubCategory(level1);
			Log.message(i++ + ". Navigated to "+ level1 + " submenu.", driver);
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("activeCat2Header","lblActiveMenuToggleBack","lnkLevel2ViewAll"), hMenu), 
					"Category submenu should show category selected, back arrow adn view all link.", 
					"Category submenu show category selected, back arrow adn view all link.", 
					"Category submenu does not show category selected, back arrow adn view all link.", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyVerticalAllignmentOfElements(driver, "activeCat2Header", "activeCat2Content", hMenu), 
					"The child categories should be displayed below the Selected category", 
					"The child categories are displayed below the Selected category", 
					"The child categories are not displayed below the Selected category", driver);
			
			//Step-7: Check the back arrow symbol and tap on the back arrow
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayedleft("activeCat2Header", "lblActiveMenuToggleBack", hMenu), 
					"The back arrow should be displayed left side of the category header with symbol like \"<\"", 
					"The back arrow is displayed left side of the category header with symbol like \"<\"", 
					"The back arrow is not displayed left side of the category header with symbol like \"<\"", driver);
			
			hMenu.clickOnBackArrowInActiveLevel2Category();
			Log.message(i++ + ". Clicked on back button menu button on" + level1 + " submenu.", driver);
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("navigationCategory1"), hMenu), 
					"On tapping menu back button the page should navigate to previous menu state.", 
					"On tapping menu back button the page navigates to previous menu state.", 
					"On tapping menu back button the page does not navigate to previous menu state.", driver);
			
			//Step-8: Check the View all link
			hMenu.navigateToSubCategory(level1);
			Log.message(i++ + ". Navigated to "+ level1 + " submenu.", driver);
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkViewAllActive"), hMenu), 
					"View All should be displayed while on Level 2 navigation.", 
					"View All is displayed while on Level 2 navigation.", 
					"View All is not displayed while on Level 2 navigation.", driver);
			
			hMenu.clickOnActiveViewAll();
			Log.message(i++ + ". Clicked on View All button on " + level1 + " submenu.", driver);
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("lnkHamburger"), headers), 
					"Hamburger menu should now be closed", 
					"hamburger menu is now closed.", 
					"hamburger menu is now not closed.", driver);
			
			Log.softAssertThat(driver.getCurrentUrl().contains(level1.toLowerCase()), 
					"On tap the user should be taken to the L2 category ", 
					"On tap the user is taken to the L2 category ", 
					"On tap the user is not taken to the L2 category ", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	} // M1_FBB_DROP1_C19654

}
