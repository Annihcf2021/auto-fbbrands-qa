package com.fbb.testscripts.drop1;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.OfferPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.customerservice.ContactUsPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.customerservice.TrackOrderPage;
import com.fbb.pages.headers.HamburgerMenu;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP1_C19653 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "critical", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP1_C19653(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String credentials= AccountUtils.generateEmail(driver) +"|"+accountData.get("password_global");
		String email=credentials.split("\\|")[0];
        String password=credentials.split("\\|")[1];
        
        GlobalNavigation.registerNewUser(driver,0,0, credentials);

		String categoryName = TestData.get("categoryName");
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			//Step-1
			Headers headers = homePage.headers;
			HamburgerMenu hMenu = (HamburgerMenu) headers.hamburgerMenu.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger menu opened!", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayed(Arrays.asList("btnCloseHamburger","lnkOffers","lnkSignIn","navigationCategory1", "fldEmailSignUp", "divOtherBrandLinksUnderNav", "lnkCustomerService_Mobile"), hMenu)
							&& hMenu.elementLayer.verifyPageListElements(Arrays.asList("lstCategoryArrow", "lstCategoryName"), hMenu), 
					"The Hamburger menu contents should be displayed",
					"The Hamburger menu contents are displayed", 
					"The Hamburger menu contents are not displayed - SM-1991", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkSignIn","lnkOffers",hMenu),
					"To check the Offers button position in the hamburger menu",
					"The 'Offer' button is displaying on left side of the 'Sign In' button", 
					"The 'Offer' button is not displaying on left side of the 'Sign In' button - SM-1991", driver);
			
			OfferPage offersPage = null;
			
			if(headers.elementLayer.verifyElementDisplayed(Arrays.asList("lnkOffer"), hMenu)) {
				offersPage = headers.navigateToOfferPage();
				Log.message(i++ + ". Clicked on Offers link", driver);
				
				Log.softAssertThat(offersPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), offersPage),
						"To check the Offers button redirected to offer page",
						"The 'Offer' button is redirecting to offer Page", 
						"The 'Offer' button is not redirecting to offer Page", driver);
			} else {
				Log.failsoft("Offers link is not displayed - SM-1991");
			}
			
			hMenu.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger menu opened!", driver);
			SignIn signIn = hMenu.navigateToSignIn();
			
			Log.softAssertThat(signIn.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), signIn),
					"To check the SignIn button redirected to sign in page",
					"The 'SignIn' button is redirecting to sign in Page", 
					"The 'SignIn' button is not redirecting to sign in Page", driver);			
			//Step-2
			hMenu.openCloseHamburgerMenu("close");
			Log.message(i++ + ". Hamburger menu closed!", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyPageElements(Arrays.asList("btnOpenHamburger"), hMenu),
					"The Hamburger menu contents should be closed",
					"The Hamburger menu is closed", 
					"The Hamburger menu is not closed", driver);
			//Step-3
			hMenu = (HamburgerMenu) headers.hamburgerMenu.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger menu opened!", driver);

			if(headers.elementLayer.verifyElementDisplayed(Arrays.asList("lnkOffer"), hMenu)) {
				offersPage = headers.navigateToOfferPage();
				Log.message(i++ + ". Navigated to Offers page!", driver);
	
				Log.softAssertThat(offersPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), offersPage), 
						"On tap the Hamburger Navigation should be closed and displays the offers page.", 
						"On tap the Hamburger Navigation closed and displays the offers page.", 
						"On tap the Hamburger Navigation not closed and not displays the offers page.", driver);
			} else {
				Log.failsoft("Offers link is not displayed - SM-1991");
			}
			//Step-4
			hMenu = (HamburgerMenu) headers.hamburgerMenu.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger menu opened!", driver);

			signIn = hMenu.navigateToSignIn();
			Log.message(i++ + ". Navigated to Sign In Page!");

			Log.softAssertThat(hMenu.elementLayer.verifyPageElements(Arrays.asList("btnOpenHamburger"), hMenu), 
					"The Hamburger menu contents should be closed",
					"The Hamburger menu is closed", 
					"The Hamburger menu is not closed", driver);
			//Step-5
			headers.navigateToMyAccount(email, password, true);
			Log.message(i++ + ". Valid Email and Password entered and Clicked on Login Button!");

			hMenu = (HamburgerMenu) headers.hamburgerMenu.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger menu opened!", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayed(Arrays.asList("imgUserProfile", "lblUserName", "lnkMyAccount"), hMenu), 
					"The User Profile pic, User Name and My Account link should be displayed",
					"The User Profile pic, User Name and My Account link are displayed",
					"The User Profile pic, User Name and My Account link are not displayed", driver);

			hMenu.ClickOnMyAccount();

			Log.softAssertThat(hMenu.elementLayer.verifyPageElements(Arrays.asList("lnkSignOut"), hMenu), 
					"System should display sign-out button.", 
					"System display sign-out button.", 
					"System not display sign-out button.", driver);

			hMenu.clickOnBackArrow();
			hMenu.navigateToSubCategory(categoryName);
			Log.message(i++ + ". Clicked on Sub-Category(2)", driver);

			Log.softAssertThat(hMenu.elementLayer.verifyPageListElements(Arrays.asList("activeCategoryNav2"), hMenu), 
					"Sub category menu should be displayed in the hamburger menu.", 
					"Sub category menu displayed in the hamburger menu.", 
					"Sub category menu not displayed in the hamburger menu.", driver);

			if(hMenu.elementLayer.verifyPageListElements(Arrays.asList("activeCategoryNav2"), hMenu)){
				//Step-8
				Log.softAssertThat(hMenu.elementLayer.verifyPageElements(Arrays.asList("lnkViewAllActive"), hMenu)
								|| hMenu.elementLayer.verifyPageElements(Arrays.asList("lnkAllActiveCategory"), hMenu), 
						"The View all/All Category link should be displayed with the L2 navigation", 
						"The View all/All Category link displayed with the L2 navigation", 
						"The View all/All Category link not displayed with the L2 navigation", driver);

				hMenu.clickOnActiveViewAll();
				Log.softAssertThat(hMenu.elementLayer.verifyPageElements(Arrays.asList("btnOpenHamburger"), hMenu), 
						"On tap the user should be taken to the L2 category and the Hamburger Menu should be closed.", 
						"On tap the user taken to the L2 category and the Hamburger Menu closed.", 
						"On tap the user not taken to the L2 category or the Hamburger Menu not closed.", driver);

				//Step-9
				hMenu.openCloseHamburgerMenu("open");
				Log.message(i++ + ". Hamburger menu opened", driver);

				hMenu.navigateToSubCategory(1);
				Log.message(i++ + ". Clicked on Sub-Category(2)", driver);

				hMenu.clickOnActiveCat2Header();
				Log.message(i++ + ". Clicked On Sub Category Header", driver);

				Log.softAssertThat(hMenu.elementLayer.verifyPageElements(Arrays.asList("navigationCategory1"), hMenu), 
						"The user should navigates to previous menu state", 
						"The user navigates to previous menu state", 
						"The user not navigates to previous menu state", driver);
			}else{
				Log.failsoft("For this Category there is no Level-2 Category. So Skipping the related Steps.", driver);
			}
			
			//Step-11
			//hMenu.openCloseHamburgerMenu("open");
			hMenu.typeInEmailSignUp("automation@yopmail.com");
			Log.message(i++ + ". Typed email address on Sign-Up Field", driver);

			hMenu.clickOnEmailSignUp();
			Log.message(i++ + ". Clicked on Email Sign Up Button", driver);

			Log.softAssertThat(hMenu.elementLayer.verifyPageElements(Arrays.asList("lblEmailSignUpThankYou"), hMenu), 
					"Thank you message should be displayed for valid email address.", 
					"Thank you message displayed for valid email address.", 
					"Thank you message not displayed for valid email address.", driver);

			BrowserActions.scrollToBottomOfPage(driver);

			//Step-12
			Log.softAssertThat(hMenu.elementLayer.verifyPageElements(Arrays.asList("divCustomerService", "hMenuQuickLinks"), hMenu), 
					"Customer Service & Quick Links Should be displayed.", 
					"Customer Service & Quick Links displayed.", 
					"Customer Service & Quick Links not displayed.", driver);
			//Step-13
			hMenu.clickOnEmailUs();
			Log.message(i++ + ". Clicked on Email Us Link", driver);

			ContactUsPage contactUs = new ContactUsPage(driver).get();
			Log.softAssertThat(contactUs.elementLayer.verifyPageElements(Arrays.asList("readyElement"), contactUs), 
					"The page should redirected to Contact us page", 
					"The page redirected to Contact us page", 
					"The page not redirected to Contact us page", driver);

			//Step-14
			CustomerService customerServicePage = homePage.footers.navigateToCustomerService();
			Log.message(i++ + ". Navigated to Customer Service page!", driver);

			if(!customerServicePage.getLiveChatStatus()) {
				Log.message(i++ + ". Live Chat service not available.");
			}else{
				hMenu.openCloseHamburgerMenu("open");
				Log.message(i++ + ". Hamburger Menu Opened!", driver);

				hMenu.clickOnChatUs();
				Log.message(i++ + ". Clicked on Chat Link", driver);

				Log.softAssertThat(hMenu.elementLayer.verifyPageElements(Arrays.asList("mdlLiveChat"), hMenu), 
						"It should display a chat modal popup", 
						"It display a chat modal popup", 
						"It not display a chat modal popup", driver);

				headers.closeLiveChat();
			}

			//Step-15
			//hMenu.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger Menu Opened!", driver);

			BrowserActions.scrollToBottomOfPage(driver);
			TrackOrderPage orderPage = hMenu.clickOnTrackOrder();
			Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage), 
					"Page should be redirected to 'Track my order' page.", 
					"Page is redirected to 'Track my order' page.", 
					"Page is not redirected to 'Track my order' page.", driver);

			hMenu.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger Menu Opened!", driver);

			BrowserActions.scrollToBottomOfPage(driver);
			orderPage = hMenu.clickOnReturnItems();
			Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage), 
					"Page should be redirected to 'Return Items' page.", 
					"Page is redirected to 'Return Items' page.", 
					"Page is not redirected to 'Return Items' page.", driver);

			hMenu.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger Menu Opened!", driver);

			BrowserActions.scrollToBottomOfPage(driver);
			ProfilePage profPage = hMenu.clickOnUdateInfo();
			Log.softAssertThat(profPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), profPage), 
					"Page should be redirected to 'Profile' page.", 
					"Page redirected to 'Profile' page.", 
					"Page not redirected to 'Profile' page.", driver);
			
			//Step-10
			hMenu.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger Menu Opened!", driver);
			
			hMenu.navigateToSubCategory(categoryName);
			Log.message("Navigated to " + categoryName, driver);
			
			Log.softAssertThat(hMenu.checkNavigatedCategoryName(categoryName),
					"The category navigated should be displayed in the hamburger menu.",
					"The category navigated is displayed in the hamburger menu.", 
					"The category navigated is not displayed in the hamburger menu.", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyPageElements(Arrays.asList("lnkLevel2ViewAll"), hMenu),
					"'View All' level 2 category should be displayed in the subcategory.",
					"'View All' level 2 category is displayed in the subcategory.", 
					"'View All' level 2 category is not displayed in the subcategory.", driver);
			
			hMenu.clickOnBackArrowInActiveLevel2Category();
			Log.message(i++ + ". Navigated to level 1 Category!", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lblActiveMenuHeading"), hMenu),
					"Level 1 category should be displayed",
					"Level 1 category is getting displayed",
					"Level 1 category is not displayed", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_GLOBALNAV_C19653


}// search
