package com.fbb.testscripts.drop1;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.CollectionUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP1_C19650 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP1_C19650(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		//Load test data
		String globalNavCategories = TestData.get("global_nav_categories");
		String navLevel1 = TestData.get("level-1");
		String navLevels = TestData.get("level-2");
		String navLevel1Underline = TestData.get("level1_highlight");
		String navLevel1CategoryColor = TestData.get("level1_currentCategory");
		String mouseOverColor = TestData.get("level1_highlight_color");
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			Headers headers = homePage.headers;
			
			//Step-1: Verify the options available in Global Navigation Bar.
			List<String> categoriesList= Arrays.asList(globalNavCategories.toLowerCase().split("\\|"));
			Log.softAssertThat(CollectionUtils.compareTwoList(categoriesList, headers.getGlobalNavCategoriesList()), 
					"Global Navigation Bar should display given categories.", 
					"Global Navigation Bar displays given categories.", 
					"Global Navigation Bar does not display given categories.", driver);
			
			headers.mouseHoverOnCategory(navLevels.split("\\|")[0].trim());
			Log.message(i++ + ". Mouse hovered on:: " + navLevels.split("\\|")[0].trim(), driver);
			Log.softAssertThat(headers.elementLayer.verifyCssPropertyForPsuedoElement("hoveredGlobalNavigationLevel1", ":hover", mouseOverColor, navLevel1Underline, headers)
							|| headers.elementLayer.verifyCssPropertyForPsuedoElement("hoveredGlobalNavigationLevel1", ":after", mouseOverColor, navLevel1Underline, headers)
							|| headers.elementLayer.verifyCssPropertyForElement("hoveredGlobalNavigationLevel1", mouseOverColor, navLevel1Underline, headers), 
					"When hovered on any root category the link is highlighted & the highlight bar should appear in that hovered category", 
					"Gray high light bar appears in hovered category", 
					"Gray high light bar does not appear in hovered category", driver);
			
			headers.mouseHoverOnCategory(navLevels.split("\\|")[0].trim());
			Log.message(i++ + ". Mouse hovered on:: " + navLevels.split("\\|")[0].trim(), driver);
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("currentLevel2"), headers), 
					"The flyout should display when mouse hover on the root category", 
					"The flyout displays when mouse hover on the root category", 
					"The flyout does not display when mouse hover on the root category", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("currentLevel2Banner"), headers), 
					"The category image should displayed in the child category when hovered.", 
					"The category image displayed in the child category when hovered.", 
					"The category image does not display in the child category when hovered.", driver);

			PlpPage plp;
			if (BrandUtils.isBrand(Brand.bh)) {
				plp = headers.navigateTo(navLevels);
				Log.message(i++ + ". Navigated to:: " + navLevels.split("\\|")[1].trim(), driver);
				Log.softAssertThat(plp.getCategoryName().trim().equalsIgnoreCase(navLevels.split("\\|")[1].trim()),
						"If user clicks on any root category, system should navigate user to the category list page",
						"System navigates user to the category list page",
						"System does not navigate user to the category list page", driver);
			} else {
				plp = headers.navigateTo(navLevel1);
				Log.message(i++ + ". Navigated to:: " + navLevel1.split("\\|")[0].trim(), driver);
				Log.softAssertThat(plp.getCategoryName().trim().equalsIgnoreCase(navLevel1.split("\\|")[0].trim()),
						"If user clicks on any root category, system should navigate user to the category list page",
						"System navigates user to the category list page",
						"System does not navigate user to the category list page", driver);
			}
			
			//Step-2, 4, 5
			Log.softAssertThat(headers.verifySubMenuColumnSize(navLevels.split("\\|")[0].trim()), 
					"Each column should contain maximum of 8 child categories.", 
					"Column has a max of 8 child categories", 
					"Column has more than 8 child categories", driver);
			
			Log.message(i++ + ". '9th child category will display on the top of the next column' is covered in step above.");
			
			headers.clickOutSide();
			headers.navigateTo(navLevels.split("\\|")[0].trim(), navLevels.split("\\|")[1].trim());
			Log.message(i++ + ". Navigated to:: " + navLevels.split("\\|")[0].trim() + " --> " + navLevels.split("\\|")[1].trim(), driver);
			Log.softAssertThat(plp.getCategoryName().equalsIgnoreCase(navLevels.split("\\|")[1].trim()), 
					"If user clicks on any Child category, system navigates user to the category list page", 
					"system navigated user to the category list page", 
					"system did not navigate user to the category list page", driver);
			
			BrowserActions.scrollToTopOfPage(driver);
			Log.softAssertThat(headers.elementLayer.verifyCssPropertyForPsuedoElement("navGlobalNavigationSelected", ":after", mouseOverColor, navLevel1CategoryColor, headers)
							|| headers.elementLayer.verifyCssPropertyForElement("navGlobalNavigationSelected", mouseOverColor, navLevel1CategoryColor, headers), 
					"In the header, root category should be display as highlighted & highlight bar should appear in that category.", 
					"Selected root category is underligned correctly with gray highlight bar.", 
					"Selected root category is not underligned correctly.", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	} // M1_FBB_DROP1_C19650

}
