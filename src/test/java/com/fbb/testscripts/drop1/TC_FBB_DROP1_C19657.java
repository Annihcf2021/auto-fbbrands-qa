package com.fbb.testscripts.drop1;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.footers.Footers;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP1_C19657 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader envData = EnvironmentPropertiesReader.getInstance("env");
	
	//Few parts of this test case been commented due to improper URLs
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP1_C19657(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		WebDriver driver = WebDriverFactory.get(browser);
		
		//Test Data
		String footerBGColor = TestData.get("footer_background");
		String baseWebSiteURL = Utils.getWebSite();
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			Footers footers = homePage.footers;
			footers.scrollToFooter();
			Log.message(i++ + ". Scrolled to Footer section!", driver);

			//Step-1: Verify the email sign up section
			Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("fldEmailSignUpFooter"), homePage.footers), 
					"Check the Email signup text box is displaying in the page", 
					"The Email signup text box is displaying in the page", 
					"The Email signup text box is not displaying in the page", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("btnEmailSignUpMobile"), homePage.footers), 
						"Check the Email signup button is displaying in the page", 
						"The Email signup button is displaying in the page", 
						"The Email signup button is not displaying in the page", driver);
			} else {
				Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("btnFooterEmailSignUp"), homePage.footers), 
						"Check the Email signup text box is displaying in the page", 
						"The Email signup text box is displaying in the page", 
						"The Email signup text box is not displaying in the page", driver);
			}
			
			homePage.footers.clickOnEmailSignUp();
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("txtEmailSignUpError"), homePage.footers), 
					"Check the Email signup text box is displaying error when email ID field is Empty", 
					"The Email signup text box is displaying an error when email ID field is Empty", 
					"The Email signup text box is not displaying an error when email ID field is Empty", driver);
			
			//Invalid Email Id format
			homePage.footers.typeInEmailSignUp("invalidemail");
			
			homePage.footers.clickOnEmailSignUp();
			
			Log.softAssertThat(homePage.footers.getErrorMessage(), 
					"Check the Email signup text box is displaying error when invalid email ID is entered", 
					"The Email signup text box is displaying error when invalid email ID is entered", 
					"The Email signup text box is not displaying error when invalid email ID is entered", driver);
			
			//Valid Email ID
			homePage.footers.typeInEmailSignUp("automationfbb@yopmail.com");
			homePage.footers.clickOnEmailSignUp();
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("lblEmailSignUpSuccessMsg"), homePage.footers), 
					"Check the success message is displaying when valid email ID is entered and signup", 
					"The success message is displaying when valid email ID is entered and signup", 
					"The success message is displaying when valid email ID is entered and signup", driver);
			
			//Step-7: Verify the background display in Footer Section
			Log.softAssertThat(homePage.footers.elementLayer.verifyCssPropertyForElement("footerContainer","background-color", footerBGColor, homePage.footers), 
					"Check the footer container is displaying in grey background", 
					"The footer container is displaying in grey background", 
					"The footer container is not displaying in grey background", driver);
			
			//Step-5: Click/Tap on any Brand logos in footer section
			Footers footer= homePage.footers;
			Log.softAssertThat(footer.verifyBrandImages(footer), 
					"Check the brand images are displaying in the footers", 
					"The other two brand images are displaying in the footers", 
					"The brand images are not displaying in the footers", driver);
			
			Log.softAssertThat(footer.verifyFooterBrandNavigation(), 
					"Check the footer brand links navigations should point the appropriate brand", 
					"The footer brand links navigations is pointed the appropriate brand", 
					"The footer brand links navigations is not pointed the appropriate brand", driver);
			
			//Step-2: Scroll down to the Footer and click/Tap Social links 
			footers.navigateToInstagram();
			Log.message(i++ + ". Navigated to 'Instagram'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("instagram"), 
					"Page should be redirected to Instagram Page", 
					"Page redirected to Instagram Page", 
					"Page Not Redirected to Instagram Page", driver);

			driver = Utils.switchWindows(driver, baseWebSiteURL, "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);

			footers.navigateToFaceBook();
			Log.message(i++ + ". Clicked on 'Facebook'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("facebook"), 
					"Page should be redirected to Facebook Page", 
					"Page redirected to Facebook Page", 
					"Page Not Redirected to Facebook Page", driver);

			driver = Utils.switchWindows(driver, baseWebSiteURL, "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);

			footers.navigateToPinterest();
			Log.message(i++ + ". Clicked on 'Pinterest'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("pinterest"), 
					"Page should be redirected to Pintrest Page", 
					"Page redirected to Pintrest Page", 
					"Page Not Redirected to Pintrest Page", driver);

			driver = Utils.switchWindows(driver, baseWebSiteURL, "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);

			footers.navigateToTwitter();
			Log.message(i++ + ". Clicked on 'Twitter'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("twitter"), 
					"Page should be redirected to Twitter Page", 
					"Page redirected to Twitter Page", 
					"Page Not Redirected to Twitter Page", driver);

			driver = Utils.switchWindows(driver, baseWebSiteURL, "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);

			if(envData.get("noYouTubeChannel").contains(Utils.getCurrentBrandShort().substring(0, 2))) {
				Log.reference(Utils.getCurrentBrand().getConfiguration() + " does not have a YouTube channel.");
				
			}else {
				footers.navigateToYoutube();
				Log.message(i++ + ". Clicked on 'Youtube'!", driver);

				Log.softAssertThat(driver.getCurrentUrl().contains("youtube"), 
						"Page should be redirected to Youtube Page", 
						"Page redirected to Youtube Page", 
						"Page Not Redirected to Youtube Page", driver);

				driver = Utils.switchWindows(driver, baseWebSiteURL, "url", "NO");
				Log.message(i++ + ". Switched To FullBeauty Website.", driver);
			}
			
			//Step-4: Click/Tap on the Footer links in the Footer section
			footers.navigateToTopQuestions();
			Log.message(i++ + ". Clicked on 'Top Questions'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().toLowerCase().contains("faq"), 
					"Page should be redirected to 'Top Questions' Page", 
					"Page redirected to 'Top Questions' Page", 
					"Page Not Redirected to 'Top Questions' Page", driver);

			if(footers.elementLayer.verifyElementDisplayed(Arrays.asList("lnkContactUs"), footers)) {
				footers.navigateToContactUs();
				Log.message(i++ + ". Clicked on 'Contact Us'!", driver);

				Log.softAssertThat(driver.getCurrentUrl().toLowerCase().contains("contactus"), 
						"Page should be redirected to 'Contact Us' Page", 
						"Page redirected to 'Contact Us' Page", 
						"Page Not Redirected to 'Contact Us' Page", driver);
			}else {
				Log.reference("Contact Us removed from footer as it's not part of CS page left navigation");
			}
			
			CustomerService customerServicePage = homePage.footers.navigateToCustomerService();
			Log.message(i++ + ". Navigated to Customer Service page!", driver);

			if(!customerServicePage.getLiveChatStatus()) {
				Log.message(i++ + ". Live Chat service will be available during working hours only.");
			}else{
				Log.softAssertThat(customerServicePage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "liveChatAvailableLnk", "lnkMailWithUs", customerServicePage), 
						"LIVE CHAT text and icon should be shown next to the 'Email Us' icon", 
						"LIVE CHAT text and icon shown next to the 'Email Us' icon", 
						"LIVE CHAT text and icon not shown next to the 'Email Us' icon", driver);

				customerServicePage.clickOnLiveChatLink();
				Log.message(i++ + ". Clicked on Live Chat Icon!", driver);

				Log.softAssertThat(customerServicePage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlLiveChat"), customerServicePage), 
						"Chat modal window should opens as a pop up modal", 
						"Chat modal window should opened as pop up modal!",
						"Chat modal window should did not open as expected.", driver);
				
				customerServicePage.closeLiveChat();
			}
			driver.get(Utils.getWebSite());
			BrowserActions.scrollToBottomOfPage(driver);
			
			footers.navigateToReturnCenter();
			Log.message(i++ + ". Clicked on 'Return Center'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().toLowerCase().contains("returnexchange"), 
					"Page should be redirected to 'Return & Exchanges' Page", 
					"Page redirected to 'Return & Exchanges", 
					"Page Not Redirected to 'Return & Exchanges' Page", driver);

			//Since these URL's aren't configured properly, skipping these lines
			/*footers.navigateToEmailSubscription();
			Log.message(i++ + ". Clicked on 'Email-Subscription'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains(""), 
					"Page should be redirected to 'EMail Subscription' Page", 
					"Page redirected to 'EMail Subscription' Page", 
					"Page Not Redirected to 'EMail Subscription' Page", driver);
			BrowserActions.navigateToBack(driver);*/

			footers.navigateToCustomerService();
			Log.message(i++ + ". Clicked on 'Customer Service'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("help"), 
					"Page should be redirected to 'Customer Service' Page", 
					"Page redirected to 'Customer Service' Page", 
					"Page Not Redirected to 'Customer Service' Page", driver);

			footers.navigateToGiftCardPage();
			Log.message(i++ + ". Clicked on 'Gift Card Page'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().toLowerCase().contains("giftcard"), 
					"Page should be redirected to 'Gift Card Balance' Page", 
					"Page redirected to 'Gift Card Balance' Page", 
					"Page Not Redirected to 'Gift Card Balance' Page", driver);

			//Temporarily Commented for currently link not available 
			//Ordering section
			/*footers.navigateToShippingAndHandling();
			Log.message(i++ + ". Clicked on 'Shipping & Handling'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains(""), 
					"Page should be redirected to 'Shipping & Handling' Page", 
					"Page redirected to 'Shipping & Handling' Page", 
					"Page Not Redirected to 'Shipping & Handling' Page", driver);
			BrowserActions.navigateToBack(driver);*/

			footers.navigateToInternationalShipping();
			Log.message(i++ + ". Clicked on 'International Shipping'!", driver);

			Log.softAssertThat(footer.verifyQASection("International Customers"), 
					"Page should be redirected to 'International Shipping' Page", 
					"Page redirected to 'International Shipping' Page", 
					"Page Not Redirected to 'International Shipping' Page", driver);

			//Temporarily Commented for currently link not available 
			/*footers.navigateToEasyReturns();
			Log.message(i++ + ". Clicked on 'Easy Returns'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains(""), 
					"Page should be redirected to 'Easy Returns' Page", 
					"Page redirected to 'Easy Returns' Page", 
					"Page Not Redirected to 'Easy Returns' Page", driver);
			BrowserActions.navigateToBack(driver);*/

			/*footers.navigateToOrderStatus();
			Log.message(i++ + ". Clicked on 'Order Status'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains(""), 
					"Page should be redirected to 'Order Status' Page", 
					"Page redirected to 'Order Status' Page", 
					"Page Not Redirected to 'Order Status' Page", driver);
			BrowserActions.navigateToBack(driver);*/

			//Account section
			footers.navigateToYourAccount();
			Log.message(i++ + ". Clicked on 'Your Account'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("Login-Show") || driver.getCurrentUrl().contains("account"), 
					"Page should be redirected to 'Your Account' Page", 
					"Page redirected to 'Your Account' Page", 
					"Page Not Redirected to 'Your Account' Page", driver);

			footers.navigateToOrderStatusUnderAccountSection();
			Log.message(i++ + ". Clicked on 'Order Status' under Account section!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("order"), 
					"Page should be redirected to 'Order Status' Page", 
					"Page redirected to 'Order Status' Page", 
					"Page Not Redirected to 'Order Status' Page", driver);

			footers.navigateToWishLists();
			Log.message(i++ + ". Clicked on 'Wish List'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("wishlist"), 
					"Page should be redirected to 'Wish List'! Page", 
					"Page redirected to 'Wish List'! Page", 
					"Page Not Redirected to 'Wish List'! Page", driver);

			if(BrandUtils.isBrand(Brand.el)) {
				Log.reference("Rewards Points is not applicable for Ellos.");
			}else {
				footers.navigateToRewardPoints();
				Log.message(i++ + ". Clicked on 'Reward Points'!", driver);

				Log.softAssertThat(driver.getCurrentUrl().contains("platinum-card"), 
						"Page should be redirected to 'Reward Points' Page", 
						"Page redirected to 'Reward Points' Page", 
						"Page Not Redirected to 'Reward Points' Page", driver);
			}

			//About section
			footers.navigateToAboutBrand();
			Log.message(i++ + ". Clicked on 'About Us", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("about"), 
					"Page should be redirected to About Page", 
					"Page redirected to About Page", 
					"Page Not Redirected to About Page", driver);

			driver.get(Utils.getWebSite());
			BrowserActions.scrollToBottomOfPage(driver);
			footers.navigateToFullBeautyBrandCareer();
			driver = Utils.switchWindows(driver, "careers/where-would-you-fit", "url", "false");
			Log.message(i++ + ". Clicked on 'Full Beauty Brand Career'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("career"), 
					"Page should be redirected to 'Full Beauty Brand Career' Page", 
					"Page redirected to 'Full Beauty Brand Career' Page", 
					"Page Not Redirected to 'Full Beauty Brand Career' Page", driver);

			driver = Utils.switchWindows(driver, baseWebSiteURL, "url", "NO");
			driver.get(Utils.getWebSite());
			BrowserActions.scrollToBottomOfPage(driver);
			footers.navigateToAboutFullBeautyBrands();
			driver = Utils.switchWindows(driver, "company-profile", "url", "NO");
			Log.message(i++ + ". Clicked on 'About Full Beauty Brands'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("company-profile"), 
					"Page should be redirected to 'About Full Beauty Brands' Page", 
					"Page redirected to 'About Full Beauty Brands' Page", 
					"Page Not Redirected to 'About Full Beauty Brands' Page", driver);
			//String currentURL = driver.getCurrentUrl();
			driver = Utils.switchWindows(driver, baseWebSiteURL, "url", "NO");
			/*footers.navigateToInTheCommunity();
			Log.message(i++ + ". Clicked on 'In The Community'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("community"), 
					"Page should be redirected to 'In The Community' Page", 
					"Page redirected to 'In The Community' Page", 
					"Page Not Redirected to 'In The Community' Page", driver);

			//3
			driver.get(Utils.getWebSite());
			BrowserActions.scrollToBottomOfPage(driver);
			footers.navigateToNthBrand(2);
			Log.message(i++ + ". Navigated to Brand Home Page!", driver);

			Log.softAssertThat(!driver.getCurrentUrl().equals(currentURL), 
					"Page should be redirected to "+ Utils.getCurrentBrand()+" Page", 
					"Page redirected to "+ Utils.getCurrentBrand()+" Page", 
					"Page Not Redirected to "+ Utils.getCurrentBrand()+" Page", driver);

			driver.get(Utils.getWebSite());
			BrowserActions.scrollToBottomOfPage(driver);
			currentURL = driver.getCurrentUrl();
			footers.navigateToNthBrand(3);
			Log.message(i++ + ". Clicked on 'Woman Within Brands'!", driver);

			Log.softAssertThat(!driver.getCurrentUrl().equals(currentURL), 
					"Page should be redirected to "+ Utils.getCurrentBrand()+" Page", 
					"Page redirected to "+ Utils.getCurrentBrand()+" Page", 
					"Page Not Redirected to "+ Utils.getCurrentBrand()+" Page", driver);*/

			Log.reference("Other brand navigation validation skipped.");
			
			//Step-6: Click/Tap the Legal links in the footer
			driver.get(Utils.getWebSite());
			BrowserActions.scrollToBottomOfPage(driver);
			footers.navigateToPrivacyPolicy();
			Log.message(i++ + ". Clicked on 'Privacy Policy'!", driver);

			driver = Utils.switchWindows(driver, "privacy-policy", "url", "NO");
			
			Log.softAssertThat(driver.getCurrentUrl().contains("privacy-policy"), 
					"Page should be redirected to Privacy Policy Page", 
					"Page redirected to Privacy Policy Page", 
					"Page Not Redirected to Privacy Policy Page", driver);

			driver = Utils.switchWindows(driver, baseWebSiteURL, "url", "NO");
			BrowserActions.scrollToBottomOfPage(driver);
			footers.navigateToTermsOfUse();
			Log.message(i++ + ". Clicked on 'Terms of use'!", driver);

			driver = Utils.switchWindows(driver, "terms-of-use", "url", "NO");
			Log.softAssertThat(driver.getCurrentUrl().contains("terms"), 
					"Page should be redirected to 'Terms of use' Page", 
					"Page redirected to 'Terms of use' Page", 
					"Page Not Redirected to 'Terms of use' Page", driver);

			//Temporarily Commented for currently link not available 
			/*BrowserActions.scrollToBottomOfPage(driver);
			footers.navigateToSupplyChainsAct();
			Log.message(i++ + ". Clicked on 'Supply Chains Act'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("supply"), 
					"Page should be redirected to 'Supply Chains Act' Page", 
					"Page redirected to 'Supply Chains Act' Page", 
					"Page Not Redirected to 'Supply Chains Act' Page", driver);*/

			//Log.failsoft("Failing test script since list of links aren't available yet...");
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_FOOTER_C19657


}// search
