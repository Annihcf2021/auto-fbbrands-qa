package com.fbb.testscripts.drop1;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.SkipException;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.customerservice.ContactUsPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.customerservice.TrackOrderPage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP1_C19614 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP1_C19614(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		String username = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");

		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
		
		if(Utils.getRunBrowser(driver).equals("ie")){
			throw new SkipException("Due Mouse Hover feature limitation this TC cannot be run in Safari/IE");
		}

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			Headers headers = homePage.headers;
			Footers footers = homePage.footers;
			homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to My Account Page with Credentials("+username+"/"+password+")");
			
			//1
			headers.mouseOverCustomerService();
			Log.message(i++ + ". Hovered/Tapped on Customer Service button", driver);

			Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("CustomerSerivceFlyOut"), headers), 
					"Customer Service flyout should display", 
					"Customer Service flyout is displaying", 
					"Customer Service flyout is not displaying", driver);

			//2
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkEmailUsDesktop","imgEmailUsDesktop"), headers), 
					"EMAIL US text & EMAIL US icon should be displayed", 
					"EMAIL US text & EMAIL US icon displayed", 
					"EMAIL US text & EMAIL US icon not displayed", driver);

			ContactUsPage contactUsPage = footers.navigateToContactUs();
			Log.message(i++ + ". Clicked on Email Us Icon.", driver);

			Log.softAssertThat(contactUsPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), contactUsPage), 
					"The Contact Us page should be displayed!",
					"The Contact Us page is displayed!", 
					"The Contact Us page is not displayed.", driver);

			//3
			BrowserActions.scrollToTopOfPage(driver);
			CustomerService customerServicePage = footers.navigateToCustomerService();
			Log.message(i++ + ". Navigated to Customer Service page!", driver);

			if(customerServicePage.getLiveChatStatus()){
				Log.softAssertThat(customerServicePage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "liveChatAvailableLnk", "lnkMailWithUs", customerServicePage), 
						"LIVE CHAT text and icon should be shown next to the 'Email Us' icon", 
						"LIVE CHAT text and icon shown next to the 'Email Us' icon", 
						"LIVE CHAT text and icon not shown next to the 'Email Us' icon", driver);

				customerServicePage.clickOnLiveChatLink();
				Log.message(i++ + ". Clicked on Live Chat Icon!", driver);

				Log.softAssertThat(customerServicePage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlLiveChat"), customerServicePage), 
						"Chat modal window should opens as a pop up modal", 
						"Chat modal window should opened as pop up modal!",
						"Chat modal window should did not open as expected.", driver);
				
				headers.closeLiveChat();
			}else{
				Log.reference("--->>> Live chat is not available at this time. Skipping related validations.");
			}

			//4
			headers.navigateToHome();
			headers.mouseOverCustomerService();
			Log.message(i++ + ". Customer Service Flyout menu opened!", driver);

			List<String> elementsToBeVerified = Arrays.asList("lnkTrackOrderDesktop", "lnkReturnItemsDesktop", "lnkUpdateMyInfoDesktop");
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(elementsToBeVerified, headers), 
					"The links for Track My Order, Return Items, Update My Information should be displayed below the Quick Links section.",
					"The links are displayed below the Quick Links section!", 
					"The links are not displayed below the Quick Links section.", driver);

			//5
			TrackOrderPage ordersPage = footers.navigateToTrackMyOrder();
			Log.message(i++ + ". Clicked On Track My Order Link!", driver);

			Log.softAssertThat(ordersPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), ordersPage),
					"Page should be Redirected to Orders Page and Order history should be shown.",
					"Page Redirected to Orders Page and Order history shown",
					"Page not Redirected to Orders Page as expected", driver);

			//6
			headers.navigateToHome();
			headers.mouseOverCustomerService();
			Log.message(i++ + ". Customer Service Flyout menu opened!", driver);

			ordersPage = footers.navigateToReturnItems();
			Log.message(i++ + ". User navigated to Return Items!", driver);

			Log.softAssertThat(ordersPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), ordersPage),
					"Page should be Redirected to Orders Page and Order history should be shown.",
					"Page Redirected to Orders Page and Order history shown",
					"Page not Redirected to Orders Page as expected", driver);
			//7
			headers.navigateToHome();
			headers.mouseOverCustomerService();
			Log.message(i++ + ". Customer Service Flyout menu opened!", driver);

			ProfilePage profPage = headers.clickOnProfileLink();
			Log.message(i++ + ". User navigated to Update My Info!", driver);

			Log.softAssertThat(profPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), profPage),
					"Page should be Redirected to My Acccount Page to Update information.",
					"Page Redirected to My Account Page",
					"Page not Redirected to My Account Page as expected", driver);
			//8
			headers.navigateToHome();
			customerServicePage = footers.navigateToCustomerService();
			Log.message(i++ + ". Navigated to Customer Service page!", driver);

			if(customerServicePage.getLiveChatStatus()) {
				Log.softAssertThat(customerServicePage.elementLayer.verifyAttributeForElement("lnkChatWithUs", "src", "clear.gif", customerServicePage), 
						"Live Chat link should not be display in the Customer service section", 
						"Live Chat link not displayed in the Customer service section", 
						"Live Chat link displayed in the Customer service section", driver);

				Log.softAssertThat(customerServicePage.elementLayer.verifyComputedCssPropertyForElement("lnkMailWithUs", "display", "inline-block", customerServicePage), 
						"Email us link should be displayed in the middle of the flyout.", 
						"Email us link displayed in the middle of the flyout.", 
						"Email us link not displayed in the middle of the flyout.", driver);
			}else{
				Log.reference("--->>> Live chat is not available at this time. Skipping related validations.");
			}
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_HEADER_C19614


}// search
