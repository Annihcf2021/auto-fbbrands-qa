package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlatinumCardLandingPage;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22735 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22735(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		// Test data
		String credentials = accountData.get("credential_saved_" + Utils.getCurrentBrandShort().substring(0, 2));
		String email = credentials.split("\\|")[0];
		String password = credentials.split("\\|")[1];
		String comenityURL = redirectData.get("comenity_manage") + Utils.getCurrentBrand().getConfiguration().toLowerCase();
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged saved PLCC account.", driver);
			
			PlatinumCardLandingPage platinumLanding = headers.navigateToApprovedPlatinumCreditCardLanding();
			Log.message(i++ + ". Navigate to PLCC landing page.", driver);
			
			//Currently in footer section rewards link is not displayed
			/*Footers footers = myAcc.footers;
			PlatinumCardLandingPage platinumLanding = footers.navigateToPlatinumCreditCardLanding();
			Log.message(i++ + ". Navigate to PLCC landing page.", driver);*/
			
			//Step-1: Verify the display of components available in PLCC Cardholder Users Landing Page
			Log.softAssertThat(platinumLanding.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divSavedPLCCMessage", "applyButton"), platinumLanding), 
					"PLCC Cardholder Users Landing Page should be available with components Available Rewards and Manage Your Account.", 
					"PLCC Cardholder Users Landing Page is available with components Available Rewards and Manage Your Account.", 
					"PLCC Cardholder Users Landing Page is not available with components Available Rewards and Manage Your Account.", driver);
			
			//Step-2: Verify the functionality of Available Rewards & Open To Buy
			Log.softAssertThat(platinumLanding.elementLayer.verifyElementWithinElement(driver, "divSavedPLCCMessage", "divBannerElementAlt", platinumLanding), 
					"Available Rewards should be displayed withinin the Banner available in the Top Content Slot", 
					"Available Rewards is displayed withinin the Banner available in the Top Content Slot", 
					"Available Rewards is not displayed withinin the Banner available in the Top Content Slot", driver);
			
			Log.softAssertThat(platinumLanding.elementLayer.verifyElementWithinElement(driver, "applyButton", "divBannerElementAlt", platinumLanding), 
					"Manage Your Account should be displayed withinin the Banner available in the Top Content Slot", 
					"Manage Your Account is displayed withinin the Banner available in the Top Content Slot", 
					"Manage Your Account is not displayed withinin the Banner available in the Top Content Slot", driver);
			
			Log.softAssertThat(platinumLanding.elementLayer.verifyVerticalAllignmentOfElements(driver, "divSavedPLCCMessage", "applyButton", platinumLanding), 
					"Available Rewards should be displayed above Manage Your Account.", 
					"Available Rewards is displayed above Manage Your Account.", 
					"Available Rewards is not displayed above Manage Your Account.", driver);
			
			Log.softAssertThat(platinumLanding.elementLayer.verifyTextContains("divSavedPLCCMessage", "Rewards", platinumLanding)
							&& platinumLanding.elementLayer.verifyTextContains("divSavedPLCCMessage", "Credit ", platinumLanding), 
					"Both the available reward points and credit limit should appear within PLCC message for user with saved PLCC cards.", 
					"Both the available reward points and credit limit appears within PLCC message for user with saved PLCC cards.", 
					"Available reward points and credit limit do not appear within PLCC message for user with saved PLCC cards.", driver);
			
			//Step-3: Verify the functionality of Manage Your Account
			Log.softAssertThat(platinumLanding.elementLayer.verifyAttributeForElement("applyButton", "target", "_blank", platinumLanding)
							&& platinumLanding.elementLayer.verifyAttributeForElement("applyButton", "href", comenityURL, platinumLanding), 
					"On clicking the Manage Your Account, System should open link to Comenity’s site based on brand in a new tab", 
					"System opened link to Comenity’s site based on brand in a new tab", 
					"System did not open link to Comenity’s site based on brand in a new tab", driver);
			
			Log.softAssertThat(platinumLanding.verifyManageYourAccount(comenityURL), 
					"On clicking the Manage Your Account, System should open link to Comenity's site based on brand in a new tab", 
					"System opened link to Comenity's site based on brand in a new tab", 
					"System did not open link to Comenity's site based on brand in a new tab", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	} // M1_FBB_DROP5_C22735

}
