package com.fbb.testscripts.drop5;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlatinumCardApplication;
import com.fbb.pages.PlatinumCardLandingPage;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22741 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	

	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	private static EnvironmentPropertiesReader dwreData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22741(String browser) throws Exception {
		Log.testCaseInfo();
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PlatinumCardApplication plcc = homePage.footers.navigateToPlatinumCreditCard();
	
			//step 1: Verify the functionality of Banner
			if(plcc.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("promotionalContent"), plcc)) {
				Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "promotionalContent", "divBannerElement", plcc), 
						"Banner should be displayed below the PLCC Promotional Content ", 
						"Banner is displayed below the PLCC Promotional Content ", 
						"Banner is not displayed below the PLCC Promotional Content ", driver);
			}
			else if(!Utils.isMobile()){
				Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divGlobalNavigation", "divBannerElement", plcc), 
						"Banner should be displayed below the PLCC Promotional Content ", 
						"Banner is displayed below the PLCC Promotional Content ", 
						"Banner is not displayed below the PLCC Promotional Content ", driver);
			}
			
			//step 2: Verify the functionality of See Benefits link
			Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "divBannerElement", "divSeeBenifits", plcc), 
					"The See Benefits link should be displayed below the Banner on left side.", 
					"The See Benefits link is displayed below the Banner on left side.", 
					"The See Benefits link is not displayed below the Banner on left side.", driver);
			
			Log.softAssertThat(plcc.ClickOnBenefitsButton(), 
					"On clicking See Benefits link, System should navigate the user to PLCC landing page", 
					"On clicking See Benefits link, System is navigate the user to PLCC landing page as expected!!", 
					"On clicking See Benefits link, System is not navigate the user to PLCC landing page !!", driver);
			
			PlatinumCardLandingPage platinumLanding = new PlatinumCardLandingPage(driver).get();
			plcc = platinumLanding.clickOnApplyButton();
			
			//step 3: Verify the functionality of Information and Rules
			Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "divSeeBenifits", "divInfoMessage", plcc), 
					"Information & Rules message element should be displayed below the See Benefits link", 
					"Information & Rules message element is displayed below the See Benefits link", 
					"Information & Rules message element is not displayed below the See Benefits link", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP5_C22741
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP5_C22741(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			List<String> elementsToBeVerified = null;
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PlatinumCardApplication plcc = homePage.footers.navigateToPlatinumCreditCard();
			
			//Step-8: Verify the functionality of components available in the Form Fields
			if(Utils.isDesktop()) {
				Log.softAssertThat(plcc.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divPLCCForm", "divPLCCMessages", plcc), 
						"Form Fields should be displayed on the right side of Information & Rules message element", 
						"Form Fields is displayed on the right side of Information & Rules message element", 
						"Form Fields is not displayed on the right side of Information & Rules message element", driver);
			}else {
				Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "divPLCCMessages", "divPLCCForm", plcc), 
						"Form Fields should be displayed on the right side of Information & Rules message element", 
						"Form Fields is displayed on the right side of Information & Rules message element", 
						"Form Fields is not displayed on the right side of Information & Rules message element", driver);
			}
			
			//Placeholder text
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("spanPlaceHolderFirstName", "innerHTML", "First Name", plcc), 
					"First Name should be displayed with the place holder text First Name.", 
					"First Name is displayed with the place holder text First Name.", 
					"First Name is not displayed with the place holder text First Name.", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("spanPlaceHolderLastName", "innerHTML", "Last Name", plcc), 
					"Last Name should be displayed with correct placeholder text.", 
					"Last Name is displayed with correct placeholder text.", 
					"Last Name is not displayed with correct placeholder text.", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("spanPlaceHolderAddressOne", "innerHTML", "Address Line1", plcc), 
					"Address Line 1 should be displayed with correct placeholder text.", 
					"Address Line 1 is displayed with correct placeholder text.", 
					"Address Line 1 is not displayed with correct placeholder text.", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("spanPlaceHolderAddressTwo", "innerHTML", "Address Line2", plcc), 
					"Address Line 2 should be displayed with correct placeholder text.", 
					"Address Line 2 is displayed with correct placeholder text.", 
					"Address Line 2 is not displayed with correct placeholder text.", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("spanPlaceHolderCity", "innerHTML", "city", plcc)
						&& plcc.elementLayer.verifyCssPropertyForElement("spanPlaceHolderCity", "text-transform", "capitalize", plcc), 
					"City should be displayed with correct placeholder text.", 
					"City is displayed with correct placeholder text.", 
					"City is not displayed with correct placeholder text.", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("spanPlaceHolderState", "innerHTML", "State", plcc), 
					"State should be displayed with correct placeholder text.", 
					"State is displayed with correct placeholder text.", 
					"State is not displayed with correct placeholder text.", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("spanPlaceHolderZipcode", "innerHTML", "Zip Code", plcc), 
					"Zip Code should be displayed with correct placeholder text.", 
					"Zip Code is displayed with correct placeholder text.", 
					"Zip Code is not displayed with correct placeholder text.", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("spanPlaceHolderEmail", "innerHTML", "Email Address", plcc), 
					"Email should be displayed with correct placeholder text.", 
					"Email is displayed with correct placeholder text.", 
					"Email is not displayed with correct placeholder text.", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("spanPlaceHolderPhone", "innerHTML", "Mobile Number", plcc), 
					"Phone should be displayed with correct placeholder text.", 
					"Phone is displayed with correct placeholder text.", 
					"Phone is not displayed with correct placeholder text.", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("spanPlaceHolderaAlternativePhone", "innerHTML", "Alternate Phone Number", plcc), 
					"Alternate Phone should be displayed with correct placeholder text.", 
					"Alternate Phone is displayed with correct placeholder text.", 
					"Alternate Phone is not displayed with correct placeholder text.", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("spanPlaceHolderSSN", "innerHTML", "XXX-XX-XXXX", plcc), 
					"SSN should be displayed with correct placeholder text.", 
					"SSN is displayed with correct placeholder text.", 
					"SSN is not displayed with correct placeholder text.", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("spanPlaceHolderMonth", "innerHTML", "MM", plcc), 
					"Month should be displayed with correct placeholder text.", 
					"Month is displayed with correct placeholder text.", 
					"Month is not displayed with correct placeholder text.", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("spanPlaceHolderDay", "innerHTML", "DD", plcc), 
					"Day should be displayed with correct placeholder text.", 
					"Day is displayed with correct placeholder text.", 
					"Day is not displayed with correct placeholder text.", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("spanPlaceHolderYear", "innerHTML", "YYYY", plcc), 
					"Year should be displayed with correct placeholder text.", 
					"Year is displayed with correct placeholder text.", 
					"Year is not displayed with correct placeholder text.", driver);
			
			//Alignment
			elementsToBeVerified = Arrays.asList("spanPlaceHolderFirstName", "spanPlaceHolderLastName", "spanPlaceHolderAddressOne", 
						"spanPlaceHolderAddressTwo", "spanPlaceHolderCity", "spanPlaceHolderState", "spanPlaceHolderZipcode");
			Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfListOfElements(elementsToBeVerified, plcc), 
					"Form fields should be located according to spec", 
					"Form fields are located according to spec", 
					"Form fields are not located according to spec", driver);
			
			elementsToBeVerified = Arrays.asList("spanPlaceHolderEmail", "spanPlaceHolderPhone", 
						"spanPlaceHolderaAlternativePhone", "spanPlaceHolderSSN", "spanPlaceHolderMonth");
			Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfListOfElements(elementsToBeVerified, plcc), 
					"Form part 2 fields should be located according to spec", 
					"Form part 2 fields are located according to spec", 
					"Form part 2 fields are not located according to spec", driver);
			
			elementsToBeVerified = Arrays.asList("spanPlaceHolderMonth", "spanPlaceHolderDay", "spanPlaceHolderYear");
			Log.softAssertThat(plcc.elementLayer.verifyHorizondalAllignmentOfListOfElements(elementsToBeVerified, plcc), 
					"Date fields should be located according to spec", 
					"Date fields are located according to spec", 
					"Date fields are not located according to spec", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M2_FBB_DROP5_C22741
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP5_C22741(String browser) throws Exception {
		Log.testCaseInfo();
		
		//Load Test Data
		String invalidSsn = "1234";
		String validSsn = "666085785";
		String invalidSsnMessage = dwreData.get("InvalidSSNError");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			List<String> elementsToBeVerified = null;
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PlatinumCardApplication plcc=	homePage.footers.navigateToPlatinumCreditCard();
			
			plcc.clickOnRegisterBtn();
			Log.message(i++ + ". Clicked on regsiter button while all fields are empty.", driver);
	
			//Step 8 continued
			//SSN Number
			elementsToBeVerified = Arrays.asList("txtSsnMandatoryError");
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The SSN Number Field should be show as mandatory in the Web Instant Credit page", 
					"The SSN Number is showing as mandatory in the Web Instant Credit page as expected!!", 
					"The SSN Number is not show as mandatory in the Web Instant Credit page!!", driver);
	
			Log.softAssertThat(plcc.verifyFieldErrorMessages(invalidSsn, invalidSsnMessage, "SSN"), 
					"System should display an appropriate error message for Incorrect format of data enter into the Alternative phone number", 
					"System is displayed an appropriate error message for Incorrect format of data enter into the Alternative phone number as expected!!", 
					"System is not display an appropriate error message for Incorrect format of data enter into the Alternative phone number", driver);
	
			BrowserActions.refreshPage(driver);
			Log.softAssertThat(plcc.enterTextOnField("txtSsnNO", validSsn, "SSN", plcc), 
					"User should be able to enter text in the Ssn Number text box", 
					"User is able to enter text in the Ssn Number text box as expected!!", 
					"User is not able to enter text in the Ssn Number text box", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtSsnNO", "spanPlaceHolderSSN", plcc),
					"Ssn Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"Ssn Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"Ssn Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			String viewableSSN = plcc.elementLayer.getAttributeForElement("txtSsnNO", "value", plcc);
			String expectedSSN = "XXX-XX-" + validSsn.substring(5);
			
			Log.softAssertThat(viewableSSN.equals(expectedSSN),
					"By default, all the digits of SSN should be masked except the last 4 digits", 
					"By default, all the digits of SSN is masked except the last 4 digits as expected!!", 
					"By default, all the digits of SSN is not masked except the last 4 digits", driver);
	
			//Date
			plcc.clickOnRegisterBtn();
			Log.message(i++ + ". Clicked on regsiter button while date fields are empty.", driver);
			
			elementsToBeVerified = Arrays.asList("selectMonthMandatoryError","selectDateMandatoryError","selectYearMandatoryError");
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The Day , Month , Year  should be show as Mandatory field in the Web Instant Credit page", 
					"The Day , Month , Year is showing as mandatory in the Web Instant Credit page as expected!!", 
					"The Day , Month , Year is not show as mandatory in the Web Instant Credit page!!", driver);
	
			BrowserActions.refreshPage(driver);
			Log.softAssertThat(plcc.verifyandSelectDateElements("month","12"), 
					"System should display the Month (1 to 12) accordingly in the respective order", 
					"System is display the Month (1 to 12) accordingly in the respective order as expected!!", 
					"System is not display the Month (1 to 12) accordingly in the respective order", driver);
	
			Log.softAssertThat(plcc.verifyandSelectDateElements("day","15"), 
					"System should display the Day (1 to 31) accordingly in the respective order and User should able to select the value", 
					"System is display the Day (1 to 31) accordingly in the respective order and User should able to select the value as expected!!", 
					"System is not display the Day (1 to 31) accordingly in the respective order and User should able to select the value", driver);
	
			Log.softAssertThat(plcc.verifyandSelectDateElements("year","1965"), 
					"System should display the Year accordingly in the respective order and User should able to select the value", 
					"System is display the Year  accordingly in the respective order and User should able to select the value as expected!!", 
					"System is not display the Year  accordingly in the respective order and User should able to select the value", driver);
	
			if(Utils.isDesktop()) {
				Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("selectDay", "spanPlaceHolderDay", plcc), 
						"Day Place holder text should move to the top portion of the text box when the User starts typing into the field", 
						"Day Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
						"Day Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
		
				Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("selectMonth", "spanPlaceHolderMonth", plcc), 
						"Month Place holder text should move to the top portion of the text box when the User starts typing into the field", 
						"Month Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
						"Month Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
		
				Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("selectYear", "spanPlaceHolderYear", plcc), 
						"Year Place holder text should move to the top portion of the text box when the User starts typing into the field", 
						"Year Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
						"Year Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
			}
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M3_FBB_DROP5_C22741
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP5_C22741(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			List<String> elementsToBeVerified = null;
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PlatinumCardApplication plcc=	homePage.footers.navigateToPlatinumCreditCard();
			
			//step-9: Authorized Buyer
			Log.softAssertThat(plcc.verifyAuthorizeBuyerState("open",plcc), 
					"The module should dropdown the hidden elements upon clicking the '+' symbol", 
					"The module is dropdown the hidden elements upon clicking the '+' symbol as expected!!", 
					"The module is not dropdown the hidden elements upon clicking the '+' symbol", driver);
	
			Log.softAssertThat(plcc.verifyAuthorizeBuyerState("close",plcc), 
					"The module should dropdown the display elements upon clicking the '-' symbol", 
					"The module is dropdown the display elements upon clicking the '-' symbol as expected!!", 
					"The module is not dropdown the display elements upon clicking the '-' symbol", driver);
			
			//Step-12: Verify the functionality of Credit Card Disclaimer
			Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "consenttoFinance", "divCreditCardDisclaimer", plcc), 
					"Credit Card Disclaimer should be displayed below the Consent to Financial Terms.", 
					"Credit Card Disclaimer is displayed below the Consent to Financial Terms.", 
					"Credit Card Disclaimer is not displayed below the Consent to Financial Terms.", driver);
	
			//Step-13: Verify the functionality of Consent Checkbox
			if(Utils.isDesktop()) {
				Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "divCreditCardDisclaimer", "selectCheckboxMandatory", plcc), 
						"Consent Checkbox should be displayed below the Credit Card Disclaimer.", 
						"Consent Checkbox is displayed below the Credit Card Disclaimer.", 
						"Consent Checkbox is not displayed below the Credit Card Disclaimer.", driver);
			}else {
				Log.softAssertThat(plcc.elementLayer.verifyCssPropertyForElement("divSubmitBlock", "position", "fixed", plcc), 
						"Consent Checkbox should be displayed below the Credit Card Disclaimer.", 
						"Consent Checkbox is displayed below the Credit Card Disclaimer.", 
						"Consent Checkbox is not displayed below the Credit Card Disclaimer.", driver);
			}
			
			Log.softAssertThat(!plcc.elementLayer.verifyAttributeForElement("selectCheckboxMandatory", "outerHTML", "checked", plcc), 
					"By default, the Consent Checkbox should be unchecked.", 
					"By default, the Consent Checkbox is unchecked.", 
					"By default, the Consent Checkbox is not unchecked.", driver);
			
			plcc.clickOnRegisterBtn();
			elementsToBeVerified = Arrays.asList("selectCheckboxMandatoryError");
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The Consent Checkbox should be show as Mandatory field in the Web Instant Credit page", 
					"The Consent Checkbox is showing as Optional in the Web Instant Credit page as expected!!", 
					"The Consent Checkbox is not show as mandatory in the Web Instant Credit page!!", driver);
	
			Log.softAssertThat(plcc.clickOnConsentCheckbox(plcc), 
					"User should select the Consent Checkbox prior to application submission in the Web Instant Credit page", 
					"User is select the Consent Checkbox prior to application submission in the Web Instant Credit page as expected!!", 
					"User is not select the Consent Checkbox prior to application submission in the Web Instant Credit page!!", driver);
	
			//Step-14: Verify the functionality of Submit button
			Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "selectCheckboxMandatory", "btnInstantSubmit", plcc), 
					"Submit button should be displayed below the Consent Checkbox.", 
					"Submit button should be displayed below the Consent Checkbox.", 
					"Submit button should be displayed below the Consent Checkbox.", driver);
			
			Log.reference("Test step further covered in C22751");
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M4_FBB_DROP5_C22741
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M5_FBB_DROP5_C22741(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PlatinumCardApplication plcc=	homePage.footers.navigateToPlatinumCreditCard();
			
			//step 4-7: Verify pre-population of the form fields
			
			
			//Step-15: Verify the functionality of Cancel button
			Log.softAssertThat(plcc.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnInstantSubmit", "btnCancel", plcc), 
					"Cancel button should be displayed left of the Submit button", 
					"Cancel button is displayed left of the Submit button", 
					"Cancel button is not displayed left of the Submit button", driver);
			
			plcc.clickCancelbtn();
			Log.softAssertThat(driver.getCurrentUrl().contains("platinum-card"), 
					"On clicking Cancel button, system should be navigated to the PLCC Landing Page.", 
					"On clicking Cancel button, system is navigated to the PLCC Landing Page.", 
					"On clicking Cancel button, system is not navigated to the PLCC Landing Page.", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M5_FBB_DROP5_C22741
	
	@Test(enabled = false, groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M6_FBB_DROP5_C22741(String browser) throws Exception {
		Log.testCaseInfo();
		
		//Load Test Data
		String[] addressDetails = checkoutData.get("valid_address7").split("\\|");
		String firstName = addressDetails[7];
		String lastName = addressDetails[8];
		String city = addressDetails[2];
		String zipCode = addressDetails[4];
		String random = RandomStringUtils.randomNumeric(5);
		String invalidEmailErrorMsg = dwreData.get("EmailError");
		String txtEmail = "automationdrop5"+random+"@yopmail.com";
		String invalidFirstName = "##@!";
		String validAddress = addressDetails[8];
		String invalidFirstNameError = dwreData.get("FirstNameError");
		String invalidLastName = dwreData.get("LastNameError");
		String phoneNumber = addressDetails[5];
		String invalidPhoneNumber = "123";
		String AlterNumber = addressDetails[5];
		String invalidPhoneNumberMsg = dwreData.get("PhoneError");
		String invalidzipcode = "##2";
		String invalidzipcodeMessage = dwreData.get("ZIPError2");
		String state = addressDetails[3];
		String colorPlaceholderText = TestData.get("placeholder_txt_color");
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			List<String> elementsToBeVerified = null;
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			PlatinumCardApplication plcc=	homePage.footers.navigateToPlatinumCreditCard();
			
			plcc.clickOnRegisterBtn();
			Log.message(i++ + ". Clicked on regsiter button while all fields are empty.", driver);
			
			//Step-16: Verify the display of Error Message
			Log.softAssertThat(plcc.elementLayer.verifyAttributeForElement("errorFormReview", "innerHTML", "Please review all forms", plcc), 
					"Error Message should be displayed below the Submit button highlighted in Red Color as \"Please review all forms\"", 
					"Appropriate error message is shown", 
					"Appropriate error message is not shown", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnInstantSubmit", "errorFormReview", plcc), 
					"Form Review error should be shown below the submit button.", 
					"Form Review error is shown below the submit button.", 
					"Form Review error is not shown below the submit button.", driver);
			
			//Step 8 continued
			//first Name
			elementsToBeVerified = Arrays.asList("txtFirstNameMandatoryError");
	
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The First Name Field should be show as mandatory in the Web Instant Credit page", 
					"The First Name is showing as mandatory in the Web Instant Credit page as expected!!", 
					"The First Name is not show as mandatory in the Web Instant Credit page!!", driver);
	
			Log.softAssertThat(plcc.verifyFieldErrorMessages(invalidFirstName,invalidFirstNameError,"FirstName"), 
					"System should display an appropriate error message for Incorrect format of data enter into the FirstName", 
					"System is displayed an appropriate error message for Incorrect format of data enter into the FirstName as expected!!", 
					"System is not display an appropriate error message for Incorrect format of data enter into the FirstName", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtFirstName",firstName,"First Name",plcc), 
					"User should be able to enter text in the First Name text box", 
					"User is able to enter text in the First Name text box as expected!!", 
					"User is not able to enter text in the First Name text box", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtFirstName", "spanPlaceHolderFirstName", plcc), 
					"FirstName Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"FirstName Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"FirstName Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//lastName
			elementsToBeVerified = Arrays.asList("txtLastNameMandatoryError");
	
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The Last Name Field should be show as mandatory in the Web Instant Credit page", 
					"The Last Name is showing as mandatory in the Web Instant Credit page as expected!!", 
					"The Last Name is not show as mandatory in the Web Instant Credit page!!", driver);
	
			Log.softAssertThat(plcc.verifyFieldErrorMessages(invalidFirstName,invalidLastName,"LastName"), 
					"System should display an appropriate error message for Incorrect format of data enter into the LastName", 
					"System is displayed an appropriate error message for Incorrect format of data enter into the LastName as expected!!", 
					"System is not display an appropriate error message for Incorrect format of data enter into the LastName", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtLastName",lastName,"Last name",plcc), 
					"User should be able to enter text in the Last Name text box", 
					"User is able to enter text in the Last Name text box as expected!!", 
					"User is not able to enter text in the Last Name text box", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtLastName", "spanPlaceHolderLastName", plcc), 
					"LastName Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"LastName Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"LastName Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//AddressLine 1
			elementsToBeVerified = Arrays.asList("txtAddressOneMandatoryError");
	
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The Address line 1 Field should be show as mandatory in the Create Account page", 
					"Address line 1 is showing as mandatory in the Create Account page as expected!!", 
					"Address line 1 is not show as mandatory in the Create Account page!!", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtaddressOne",validAddress,"Address 1",plcc), 
					"User should be able to enter text in the Address line 1 text box", 
					"User is able to enter text in the Address line 1 text box as expected!!", 
					"User is not able to enter text in the Address line 1 text box", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtaddressOne", "spanPlaceHolderAddressOne", plcc), 
					"Address line 1 Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"Email Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"Email Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//Address line 2
			elementsToBeVerified = Arrays.asList("");
	
			Log.softAssertThat(plcc.elementLayer.verifyElementColor("spanPlaceHolderAddressTwo", colorPlaceholderText, plcc), 
					"The Address line 2 Field should be optional in the Create Account page", 
					"The Address line 2 Field should is optional in the Create Account page as expected!!", 
					"The Address line 2 Field should is not optional in the Create Account page!!", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtaddressTwo","aspire123","AddressLine2",plcc), 
					"User should be able to enter text in the Address line 2 text box", 
					"User is able to enter text in the Address line 2 box as expected!!", 
					"User is not able to enter text in the Address line 2 text box", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtaddressTwo", "spanPlaceHolderAddressTwo", plcc), 
					"Address line 2 Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"Address line 2 Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"Address line 2 Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//City
			elementsToBeVerified = Arrays.asList("txtcityMandatoryError");
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The City Field should be show as mandatory in the Web Instant Credit page", 
					"The City is showing as mandatory in the Web Instant Credit page as expected!!", 
					"The City is not show as mandatory in the Web Instant Credit page!!", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtcity",city,"city",plcc), 
					"User should be able to enter text in the City text box", 
					"User is able to enter text in the City text box as expected!!", 
					"User is not able to enter text in the City text box", driver);
			
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtcity", "spanPlaceHolderCity", plcc), 
					"City Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"City Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"City Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//State
			elementsToBeVerified = Arrays.asList("selectStateMandatoryError");
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The State Field should be show as mandatory in the Web Instant Credit page", 
					"The State is showing as mandatory in the Web Instant Credit page as expected!!", 
					"The State is not show as mandatory in the Web Instant Credit page!!", driver);
	
			Log.softAssertThat(plcc.selectState(state), 
					"User should be able to select a value from the State drop down", 
					"User can be able to select a value from the State drop down as expected!!", 
					"User is not able to select a value from the State drop down", driver);
	
			Log.softAssertThat( plcc.elementLayer.verifyElementDisplayedBelow("selectStateDrop", "spanPlaceHolderState", plcc),
					"State field Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"State field Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"State field Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//Zip Code
			elementsToBeVerified = Arrays.asList("txtzipcodeMandatoryError");
	
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The Zip code Field should be show as mandatory in the Web Instant Credit page", 
					"The Zip code is showing as mandatory in the Web Instant Credit page as expected!!", 
					"The Zip code is not show as mandatory in the Web Instant Credit page!!", driver);
	
			Log.softAssertThat(plcc.verifyFieldErrorMessages(invalidzipcode,invalidzipcodeMessage,"zipCode"), 
					"System should display an appropriate error message for Incorrect format of data enter into the Zip code", 
					"System is displayed an appropriate error message for Incorrect format of data enter into the Zip code as expected!!", 
					"System is not display an appropriate error message for Incorrect format of data enter into the Zip code", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtZipCode",zipCode,"zipcode",plcc), 
					"User should be able to enter text in the Zip code text box", 
					"User is able to enter text in the zip code text box as expected!!", 
					"User is not able to enter text in the zip code text box", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtZipCode", "spanPlaceHolderZipcode", plcc), 
					"Zip code Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"Zip code Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"Zip code Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//Email Address
			elementsToBeVerified = Arrays.asList("selectEmailOptionalTxt");
	
			Log.softAssertThat(plcc.elementLayer.verifyElementColor("spanPlaceHolderEmail", colorPlaceholderText, plcc), 
					"The Email Id Field should be show as Optional in the Web Instant Credit page", 
					"The Email Id is showing as Optional in the Web Instant Credit page as expected!!", 
					"The Email Id is not show as mandatory in the Web Instant Credit page!!", driver);
	
			Log.softAssertThat(plcc.verifyFieldErrorMessages(invalidFirstName,invalidEmailErrorMsg,"Email"), 
					"System should display an appropriate error message for Incorrect format of data enter into the Email", 
					"System is displayed an appropriate error message for Incorrect format of data enter into the Email as expected!!", 
					"System is not display an appropriate error message for Incorrect format of data enter into the Email", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtEmail",txtEmail,"Email",plcc), 
					"User should be able to enter text in the Email text box", 
					"User is able to enter text in the Email text box as expected!!", 
					"User is not able to enter text in the Email text box", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtEmail", "spanPlaceHolderEmail", plcc), 
					"Email Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"Email Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"Email Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//Phone number
			elementsToBeVerified = Arrays.asList("txtPhoneMandatoryError");
	
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The Phone Number Field should be show as mandatory in the Web Instant Credit page", 
					"The Phone Number is showing as mandatory in the Web Instant Credit page as expected!!", 
					"The Phone Number is not show as mandatory in the Web Instant Credit page!!", driver);
	
			Log.softAssertThat(plcc.verifyFieldErrorMessages(invalidPhoneNumber,invalidPhoneNumberMsg,"Phone Number"), 
					"System should display an appropriate error message for Incorrect format of data enter into the Phone Number", 
					"System is displayed an appropriate error message for Incorrect format of data enter into the Phone Number as expected!!", 
					"System is not display an appropriate error message for Incorrect format of data enter into the Phone Number", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtphone",phoneNumber,"Phone Number",plcc), 
					"User should be able to enter text in the Phone Number text box", 
					"User is able to enter text in the Phone Number text box as expected!!", 
					"User is not able to enter text in the Phone Number text box", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtphone", "spanPlaceHolderPhone", plcc), 
					"Phone Number Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"Phone Number Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"Phone Number Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//Alternative Phone number
			Log.softAssertThat(plcc.elementLayer.verifyElementColor("spanPlaceHolderaAlternativePhone", colorPlaceholderText, plcc), 
					"The Alternative phone number Field should show as Optional in the Web Instant Credit page", 
					"The Alternative phone number is showing as Optional in the Web Instant Credit page as expected!!", 
					"The Alternative phone number is not show as mandatory in the Web Instant Credit page!!", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtAlternativePhone",AlterNumber,"Alternative phone",plcc), 
					"User should be able to enter text in the Alternative phone number text box", 
					"User is able to enter text in the Alternative phone number text box as expected!!", 
					"User is not able to enter text in the Alternative phone number text box", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtEmail", "spanPlaceHolderaAlternativePhone", plcc),
					"Alternative phone number Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"Alternative phone number Place holder text is moved to the top portion of the text box when the User starts typing into the fieldas expected!!", 
					"Alternative phone number Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M6_FBB_DROP5_C22741

}// search
