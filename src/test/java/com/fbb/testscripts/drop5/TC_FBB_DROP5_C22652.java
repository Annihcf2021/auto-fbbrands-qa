package com.fbb.testscripts.drop5;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.PasswordResetPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22652 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22652(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Create the webdriver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = accountData.get("valid_registered_email").split("\\|")[0];
	
		int i = 1;
		try{
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			CustomerService customerService = homePage.footers.navigateToCustomerService();
			Log.message(i++ + ". Clicked on 'Customer Service'", driver);
	
			customerService.clickOnForgotMyPassword();
			Log.message(i++ + ". Clicked on 'Forgot Section' in Faq", driver);
	
			PasswordResetPage pwdResetPage = customerService.clickOnForgotPwdLnk();
			Log.message(i++ + ". Clicked on 'Forgot my password' link", driver);
	
			//Step-1: Verify the functionality of Breadcrumb
			if(!Utils.isMobile()) {
				if(!BrandUtils.isBrand(Brand.el)) {
					Log.softAssertThat(pwdResetPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "promoContent", "breadcrumbCurrentElement", pwdResetPage),
							"Breadcrumb should be displayed below the Promotional Content",
							"Breadcrumb is displayed below the Promotional Content",
							"Breadcrumb is not displayed below the Promotional Content", driver);
				}
	
				List<String> txtInBreadCrumb = pwdResetPage.getTextInBreadcrumb();
				Log.softAssertThat(txtInBreadCrumb.get(0).equalsIgnoreCase("HOME")
						&& txtInBreadCrumb.get(1).equalsIgnoreCase("MY ACCOUNT")
						&& txtInBreadCrumb.get(2).equalsIgnoreCase("FORGOT PASSWORD"),
						"The breadcrumb should be displayed as - 'HOME > MY ACCOUNT > FORGOT PASSWORD'",
						"The breadcrumb is displayed as expected - 'HOME > MY ACCOUNT > FORGOT PASSWORD'",
						"The breadcrumb is not displayed as expected - 'HOME > MY ACCOUNT > FORGOT PASSWORD' and the actual is : '"
								+ txtInBreadCrumb.get(0)
								+ " > "
								+ txtInBreadCrumb.get(1) + "'" 
								+ " > "
								+ txtInBreadCrumb.get(2) + "'",
								driver);
	
				pwdResetPage.clickOnBreakCrumbValue(1);		
				Log.message(i++ + ". Clicked on 'My Account' in breadcrumb!", driver);
	
				Log.softAssertThat(pwdResetPage.elementLayer.verifyPageElements(Arrays.asList("myAccount"), pwdResetPage), 
						"Page should be redirected to 'My Account' Page", 
						"Page is redirected to 'My Account' Page",
						"Page is not redirected to 'My Account' Page",
						driver);
	
				pwdResetPage.clickOnBreakCrumbValue(0);		
				Log.message(i++ + ". Clicked on 'Home' in breadcrumb!", driver);
	
				Log.softAssertThat(pwdResetPage.elementLayer.verifyPageElements(Arrays.asList("homePage"), pwdResetPage), 
						"Page should be navigated to home page when clicking on 'Home' in breadcrumb", 
						"Page is navigated to home page when clicking on 'Home' in breadcrumb",
						"Page is note navigated to home page when clicking on 'Home' in breadcrumb",
						driver);
			} else {
				Log.softAssertThat(pwdResetPage.getTextInBreadcrumbMobile().equalsIgnoreCase("MY ACCOUNT"),
						"The breadcrumb should be displayed as - ' < MY ACCOUNT'",
						"The breadcrumb is displayed as expected - ' < MY ACCOUNT'",
						"The breadcrumb is not displayed as expected - ' < MY ACCOUNT' and the actual is : '"
								+ pwdResetPage.getTextInBreadcrumbMobile(), driver);
	
				pwdResetPage.clickOnBreakCrumbMobile();
				Log.message(i++ + ". Clicked on breadcrumb!", driver);
	
				Log.softAssertThat(pwdResetPage.elementLayer.verifyPageElements(Arrays.asList("myAccount"), pwdResetPage), 
						"Page should be navigated to 'My account' page when clicking on 'MY ACCOUNT' in breadcrumb", 
						"Page is navigated to 'My account' page when clicking on 'MY ACCOUNT' in breadcrumb",
						"Page is note navigated to 'My account' page when clicking on 'MY ACCOUNT' in breadcrumb",
						driver);
			}
	
			customerService = homePage.footers.navigateToCustomerService();
			Log.message(i++ + ". Clicked on 'Customer Service'", driver);
	
			customerService.clickOnForgotMyPassword();
			Log.message(i++ + ". Clicked on 'Forgot Section' in Faq", driver);
	
			pwdResetPage = customerService.clickOnForgotPwdLnk();
			Log.message(i++ + ". Clicked on 'Forgot my password' link", driver);
	
			//Step-2: Verify the functionality of Content Slot
			if(!Utils.isMobile()) {
				Log.softAssertThat(pwdResetPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "breadcrumbCurrentElement", "contentSlot", pwdResetPage),
						"Content Slot should be displayed below the Breadcrumb",
						"Content Slot is displayed below the Breadcrumb",
						"Content Slot is not displayed below the Breadcrumb", driver);
			}
			else {
				Log.softAssertThat(pwdResetPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "breadcrumbCurrentElementMobile", "contentSlot", pwdResetPage),
						"Content Slot should be displayed below the Breadcrumb",
						"Content Slot is displayed below the Breadcrumb",
						"Content Slot is not displayed below the Breadcrumb", driver);
			}
	
			//Step-3: Verify the functionality of Header
			Log.softAssertThat(pwdResetPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "contentSlot", "pwdResetHeader", pwdResetPage),
					"Header should be displayed below the Content Slot",
					"Header is displayed below the Content Slot",
					"Header is not displayed below the Content Slot", driver);
	
			//Step-4: Verify the functionality of Subhead
			Log.softAssertThat(pwdResetPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "pwdResetHeader", "pwdResetSubHead", pwdResetPage),
					"Subhead should be displayed below the Header.",
					"Subhead is displayed below the Header.",
					"Subhead is not displayed below the Header.", driver);
	
			//Step-5: Verify the functionality of Email Address
			Log.softAssertThat(pwdResetPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "pwdResetSubHead", "txtEmailId", pwdResetPage),
					"Email Address text box should be displayed below the Sub head.",
					"Email Address text box is displayed below the Sub head.",
					"Email Address text box is not displayed below the Sub head.", driver);
	
			pwdResetPage.clickOnSendBtn();
			Log.message(i++ + ". Clicked on Send button!", driver);
	
			Log.softAssertThat(pwdResetPage.elementLayer.verifyPageElements(Arrays.asList("txtEmailError"), pwdResetPage), 
					"Mandatory Error message should be displayed when clicking on send without entering Email Id", 
					"Mandatory Error message is displayed when clicking on send without entering Email Id",
					"Mandatory Error message is not displayed when clicking on send without entering Email Id",
					driver);
	
			pwdResetPage.enterEmailId("invalid");
			Log.message(i++ + ". Entered Invalid Email Address!", driver);
	
			Log.softAssertThat(pwdResetPage.elementLayer.verifyPageElements(Arrays.asList("txtEmailError"), pwdResetPage), 
					"Invalid Error message should be displayed when wrong email entered", 
					"Invalid Error message is displayed when wrong email entered",
					"Invalid Error message is not displayed when wrong email entered",
					driver);
	
			pwdResetPage.enterEmailId(email);
			Log.message(i++ + ". Entered Email Address!", driver);
	
			Log.softAssertThat(!pwdResetPage.getEnteredEmailId().isEmpty(), 
					"Check Email Address is editable", 
					"Email Address is editable",
					"Email Address is not editable", driver);
	
			Log.softAssertThat(pwdResetPage.getEnteredEmailId().contains(".com") && pwdResetPage.getEnteredEmailId().contains("@"), 
					"Email Address Should be in proper format", 
					"Email Address is in proper format",
					"Email Address is not in proper format", driver);
	
			//Step-6
			Log.softAssertThat(pwdResetPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtEmailId", "btnSend", pwdResetPage), 
					"Send button should display below the email address field", 
					"Send button is displaying below the email address field",
					"Send button is not displaying below the email address field", driver);
	
			pwdResetPage.clickOnSendBtn();
			Log.message(i++ + ". Clicked on Send button!", driver);
	
			Log.softAssertThat(pwdResetPage.elementLayer.verifyPageElements(Arrays.asList("lblSuccessMsg"), pwdResetPage), 
					"Success message should be displayed when clicking on send after entering Email Id!", 
					"Success message is displayed when clicking on send after entering Email Id!",
					"Success message is not displayed when clicking on send after entering Email Id!",
					driver);
	
			customerService = homePage.footers.navigateToCustomerService();
			Log.message(i++ + ". Clicked on 'Customer Service'", driver);
	
			customerService.clickOnForgotMyPassword();
			Log.message(i++ + ". Clicked on 'Forgot Section' in Faq", driver);
	
			pwdResetPage = customerService.clickOnForgotPwdLnk();
			Log.message(i++ + ". Clicked on 'Forgot my password' link", driver);
	
			Log.softAssertThat(pwdResetPage.elementLayer.verifyPlaceHolderMovesAbove("txtEmailId", "lblEmailPlaceHolder", email, pwdResetPage),
					"Check the placeholder moving upwards when entering email id",
					"The placeholder moving upwards when entering email id",
					"The placeholder not moving upwards when entering email id", driver);
	
			pwdResetPage.clickOnBackToLoginBtn();
			Log.message(i++ + ". Clicked on 'Back To Login' button ", driver);
	
			Log.softAssertThat(pwdResetPage.elementLayer.verifyPageElements(Arrays.asList("myAccount"), pwdResetPage), 
					"Page should be navigated to 'Login' page when clicking on 'Back to Login' button", 
					"Page is navigated to 'Login' page when clicking on 'Back to Login' button",
					"Page is not navigated to 'Login' page when clicking on 'Back to Login' button",
					driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C22652

}// search
