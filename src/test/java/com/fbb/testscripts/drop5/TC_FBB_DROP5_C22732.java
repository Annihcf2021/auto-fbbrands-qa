package com.fbb.testscripts.drop5;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SignIn;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22732 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22732(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String registeredEmail = accountData.get("valid_registered_email").split("\\|")[0];
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			SignIn signIn = headers.navigateToSignInPage();
			Log.message(i++ + ". Navigated to Sign In page.", driver);
			
			signIn.clickForgotPwdLink();
			Log.message(i++ + ". Clicked on Forgot Passward link.", driver);
			
			signIn.enterEmailInForgotPasswordMdl(registeredEmail);
			Log.message(i++ + ". Typed in Email.", driver);
			
			signIn.clickSendRequestPassword();
			Log.message(i++ + ". Clicked send.", driver);
			
			//Step-1: Verify the display and functionality of Headline
			Log.softAssertThat(signIn.elementLayer.verifyElementsAreInSameRow("headingRequestPassword", "divForgotPassword", signIn), 
					"Headline should be displayed on top left corner of the Forgot Password Request Received Modal", 
					"Headline is displayed on top left corner of the Forgot Password Request Received Modal", 
					"Headline is not displayed on top left corner of the Forgot Password Request Received Modal", driver);
			
			//Step-2: Verify the display and functionality of Subhead
			Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "headingRequestPassword", "subHeadPasswordResetConfirmation", signIn), 
					"Subhead should be displayed below the Headline", 
					"Subhead is displayed below the Headline", 
					"Subhead is not displayed below the Headline", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	} // M1_FBB_DROP5_C22732
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP5_C22732(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String unregisteredEmail = accountData.get("valid_format_email");
		String unregisteredEmailMessage = TestData.get("pwResetNoEmailFound");
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			SignIn signIn = headers.navigateToSignInPage();
			Log.message(i++ + ". Navigated to Sign In page.", driver);
			
			signIn.clickForgotPwdLink();
			Log.message(i++ + ". Clicked on Forgot Passward link.", driver);
			
			signIn.enterEmailInForgotPasswordMdl(unregisteredEmail);
			Log.message(i++ + ". Typed in Email.", driver);
			
			signIn.clickSendRequestPassword();
			Log.message(i++ + ". Clicked send.", driver);
			
			//Step-3: Verify if Unregistered email address is checked.
			Log.softAssertThat(signIn.elementLayer.verifyTextEquals("subHeadRequestPasswordModal", unregisteredEmailMessage, signIn), 
					"Password reset request for unregisterd email address will return approproate messgae.", 
					"Appropriate messgae is displayed for unregisterd email address.", 
					"Appropriate messgae is not displayed for unregisterd email address.", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	} // M2_FBB_DROP5_C22732

}
