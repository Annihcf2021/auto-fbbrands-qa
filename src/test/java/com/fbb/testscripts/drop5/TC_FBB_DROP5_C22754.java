package com.fbb.testscripts.drop5;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PLCC;
import com.fbb.pages.SignIn;
import com.fbb.support.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Arrays;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22754 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22754(String browser) throws Exception {
		//Pre-condition: Successfully apply for a PLCC through either the Web Instant Credit module or the PLCC application form for pre-approved Users
		//System launches the PLCC Approved Modal
	
		Log.testCaseInfo();
		String firstName = "";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page.", driver);

            PLCC plccModules = new PLCC(driver).get();
            Log.message(i++ + ". PLCC Contents Loaded.", driver);

            //Verify the display of Approval Heading
            Log.softAssertThat(plccModules.elementLayer.verifyTextContains("lblModelHeader", firstName, plccModules),
                    "System should insert the user’s first name in the Approval Heading",
                    "System should insert the user’s first name in the Approval Heading",
                    "System should insert the user’s first name in the Approval Heading", driver);

            Log.softAssertThat(plccModules.elementLayer.verifyTextContains("lblModelHeader", "Welcome, " + firstName + "!", plccModules),
                    "User's first name has to be in the Approval Heading,  it should say \"Welcome, (preapproved customer name)!\"",
                    "User's first name has to be in the Approval Heading,  it should say \"Welcome, (preapproved customer name)!\"",
                    "User's first name has to be in the Approval Heading,  it should say \"Welcome, (preapproved customer name)!\"", driver);

            //Verify the display of Success Message
            Log.softAssertThat(plccModules.elementLayer.verifyTextContains("lblModelSubHeader", "$", plccModules),
                    "System should insert the user’s credit amount in the Success Message.",
                    "System should insert the user’s credit amount in the Success Message.",
                    "System should insert the user’s credit amount in the Success Message.", driver);

            Log.softAssertThat(plccModules.elementLayer.verifyTextContains("lblModelSubHeader", "$", plccModules),
                    "Credit Amount should be a monetary value.",
                    "Credit Amount should be a monetary value.",
                    "Credit Amount should be a monetary value.", driver);

            Log.softAssertThat(plccModules.elementLayer.verifyElementTextWithDWREProp(Arrays.asList("lblModelSubHeader"), "plcc_success_message_"+Utils.getCurrentBrand().name().toLowerCase(), plccModules),
                    "Text in the Success Message should be handled by the brand-specific property files from locale.properties.",
                    "Text in the Success Message should be handled by the brand-specific property files from locale.properties.",
                    "Text in the Success Message should be handled by the brand-specific property files from locale.properties.", driver);

            //Verify the functionality of Continue Messaging
            Log.softAssertThat(plccModules.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblModelSubHeader", "lblContinueMessage", plccModules),
                    "Continue Messaging should be displayed below the Success Message",
                    "Continue Messaging should be displayed below the Success Message",
                    "Continue Messaging should be displayed below the Success Message", driver);

            Log.message("If the PLCC Approved Modal is accessed through Checkout Page, then:");
            Log.softAssertThat(plccModules.elementLayer.verifyTextContains("lblContinueMessage", "Continue to checkout and use your new card.", plccModules),
                    "Continue Messaging should be displayed as \"Continue to checkout and use your new card.\"",
                    "Continue Messaging should be displayed as \"Continue to checkout and use your new card.\"",
                    "Continue Messaging should be displayed as \"Continue to checkout and use your new card.\"", driver);

            Log.message("If this PLCC Approved Modal is accessed through My Account or Web Instant Credit, then:");
            Log.softAssertThat(plccModules.elementLayer.verifyTextContains("lblContinueMessage", "Continue to Shopping and use your new card.", plccModules),
                    "Continue Messaging should be displayed as \"Continue to Shopping and use your new card.\"",
                    "Continue Messaging should be displayed as \"Continue to Shopping and use your new card.\"",
                    "Continue Messaging should be displayed as \"Continue to Shopping and use your new card.\"", driver);

            if(Utils.isMobile())
                Log.softAssertThat(plccModules.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnSignInToSaveCard", "btnContinueTo", plccModules),
                        "Continue should be displayed below the Sign In to save this card button.",
                        "Continue should be displayed below the Sign In to save this card button.",
                        "Continue should be displayed below the Sign In to save this card button.", driver);

            else
                Log.softAssertThat(plccModules.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnContinueTo", "btnSignInToSaveCard", plccModules),
                        "Continue should be displayed to the right of the Sign In to save this card button",
                        "Continue should be displayed to the right of the Sign In to save this card button",
                        "Continue should be displayed to the right of the Sign In to save this card button", driver);

            //Verify the functionality of Sign In to save this card button
            Log.message("<b><u>**For Unauthenticated User**</u></b>");
            Log.softAssertThat(plccModules.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblContinueMessage", "btnSignInToSaveCard", plccModules),
                    "If user in unauthenticated, then Sign In to save this Card button should be displayed below the Continue Messaging element",
                    "If user in unauthenticated, then Sign In to save this Card button should be displayed below the Continue Messaging element",
                    "If user in unauthenticated, then Sign In to save this Card button should be displayed below the Continue Messaging element", driver);

            plccModules.clickOnSignInToSaveThisCard();
            Log.message(i++ + ". Clicked on Sign In To Save This Card button");
            SignIn signInPage = new SignIn(driver).get();
            Log.softAssertThat(signInPage.getPageLoadStatus(),
                    "On clicking the button, User should be navigated to the My Account Login Page",
                    "On clicking the button, User should be navigated to the My Account Login Page",
                    "On clicking the button, User should be navigated to the My Account Login Page", driver);

            Log.softAssertThat(true,
                    "When the user already has an account and signs in to the application with valid credentials then the new PLCC cards should be saved to the respective account which can be viewed in the My Account -- Overview",
                    "When the user already has an account and signs in to the application with valid credentials then the new PLCC cards should be saved to the respective account which can be viewed in the My Account -- Overview",
                    "When the user already has an account and signs in to the application with valid credentials then the new PLCC cards should be saved to the respective account which can be viewed in the My Account -- Overview", driver);

            Log.softAssertThat(true,
                    "User should also be able to view the PLCC Card in the Saved Cards List in the Payment Methods Page of My Account",
                    "User should also be able to view the PLCC Card in the Saved Cards List in the Payment Methods Page of My Account",
                    "User should also be able to view the PLCC Card in the Saved Cards List in the Payment Methods Page of My Account", driver);

            Log.message("If the PLCC Approved Modal is accessed through Web Instant Credit or My Account Page");
            Log.softAssertThat(plccModules.elementLayer.verifyTextContains("btnContinueTo", "Continue To Shopping", plccModules),
                    "Continue button should be displayed as Continue Shopping",
                    "Continue button should be displayed as Continue Shopping",
                    "Continue button should be displayed as Continue Shopping", driver);

            plccModules.clickOnContinueTo();
            Log.message(i++ + ". Clicked on Continue To.", driver);

            homePage = new HomePage(driver).get();
            Log.softAssertThat(homePage.getPageLoadStatus(),
                    "On clicking the Continue Shopping button, user should be navigated to the brand homepage",
                    "On clicking the Continue Shopping button, user should be navigated to the brand homepage",
                    "On clicking the Continue Shopping button, user should be navigated to the brand homepage", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP5_C22754

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
    public void M2_FBB_DROP5_C22754(String browser) throws Exception {
        //Pre-condition: Successfully apply for a PLCC through either the Web Instant Credit module or the PLCC application form for pre-approved Users
        //System launches the PLCC Approved Modal

        Log.testCaseInfo();

        // Get the web driver instance
        final WebDriver driver = WebDriverFactory.get(browser);
        int i = 1;
        try {
            new HomePage(driver, Utils.getWebSite()).get();
            Log.message(i++ + ". Navigated to Home Page.", driver);

            PLCC plccModules = new PLCC(driver).get();
            Log.message(i++ + ". PLCC Contents Loaded.", driver);

            Log.message("<b><u>**For Authenticated User**</u></b>");
            Log.softAssertThat(true,
                    "If user in authenticated and is accessing this modal from checkout, then Sign In to save this Card button should not be displayed",
                    "If user in authenticated and is accessing this modal from checkout, then Sign In to save this Card button should not be displayed",
                    "If user in authenticated and is accessing this modal from checkout, then Sign In to save this Card button should not be displayed", driver);

            Log.softAssertThat(plccModules.elementLayer.verifyCssPropertyForElement("", "text-align", "center", plccModules),
            "Continue to Checkout should be displayed aligned at the center",
            "Continue to Checkout should be displayed aligned at the center",
            "Continue to Checkout should be displayed aligned at the center", driver);

            Log.softAssertThat(true,
                    "Card should have been saved in to the respective account which can be viewed in the My Account -- Overview",
                    "Card should have been saved in to the respective account which can be viewed in the My Account -- Overview",
                    "Card should have been saved in to the respective account which can be viewed in the My Account -- Overview", driver);

            Log.softAssertThat(true,
                    "User should also be able to view the PLCC Card in the Saved Cards List in the Payment Methods Page of My Account",
                    "User should also be able to view the PLCC Card in the Saved Cards List in the Payment Methods Page of My Account",
                    "User should also be able to view the PLCC Card in the Saved Cards List in the Payment Methods Page of My Account", driver);

        	Log.testCaseResult();

        } // try
        catch (Exception e) {
            Log.exception(e, driver);
        } // catch
        finally {
        	Log.endTestCase(driver);
        } // finally

    }// TC_FBB_DROP5_C22754

    @Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
    public void M3_FBB_DROP5_C22754(String browser) throws Exception {
        //Pre-condition: Successfully apply for a PLCC through either the Web Instant Credit module or the PLCC application form for pre-approved Users
        //System launches the PLCC Approved Modal

        Log.testCaseInfo();

        // Get the web driver instance
        final WebDriver driver = WebDriverFactory.get(browser);
        int i = 1;
        try {
            new HomePage(driver, Utils.getWebSite()).get();
            Log.message(i++ + ". Navigated to Home Page.", driver);

            PLCC plccModules = new PLCC(driver).get();
            Log.message(i++ + ". PLCC Contents Loaded.", driver);

            //Verify the functionality of Continue button in the PLCC Approved Modal
            Log.message("If the PLCC Approved Modal is accessed through Checkout Page");
            Log.message("If the 'Sign in to save card' button is absent", driver);
            Log.softAssertThat(plccModules.elementLayer.verifyInsideElementAlligned("","","",plccModules),
                    "Continue button will be aligned centrally at the bottom of the modal",
                    "Continue button will be aligned centrally at the bottom of the modal",
                    "Continue button will be aligned centrally at the bottom of the modal", driver);

            Log.softAssertThat(plccModules.elementLayer.verifyTextContains("btnContinueTo", "Continue To Checkout", plccModules),
                    "Continue button should be displayed as Continue To Checkout",
                    "Continue button should be displayed as Continue To Checkout",
                    "Continue button should be displayed as Continue To Checkout", driver);

            plccModules.clickOnContinueTo();
            Log.message(i++ + ". Clicked on Continue To.", driver);

            CheckoutPage checkoutPage = new CheckoutPage(driver).get();
            Log.softAssertThat(checkoutPage.getPageLoadStatus(),
                    "On clicking the Continue To Checkout button, user should be navigated to the Checkout page",
                    "On clicking the Continue To Checkout button, user should be navigated to the Checkout page",
                    "On clicking the Continue To Checkout button, user should be navigated to the Checkout page", driver);

            //Verify the information available in Checkout Page on clicking the Continue to Checkout button when User does not have a default address saved and is not just ordering a Gift Certificate
            Log.message("If the user does not have a default address saved");
            Log.softAssertThat(checkoutPage.getCurrentSection().equals("shipping"),
                    "User should be navigated to the Checkout Step 1 where the user has to Select or enter a shipping address",
                    "User should be navigated to the Checkout Step 1 where the user has to Select or enter a shipping address",
                    "User should be navigated to the Checkout Step 1 where the user has to Select or enter a shipping address", driver);

            Log.softAssertThat(checkoutPage.getSelectedCardType().equals("plcc"),
                    "The new PLCC Card should be selected by default for the user when they navigate to the select payment method step.",
                    "The new PLCC Card should be selected by default for the user when they navigate to the select payment method step.",
                    "The new PLCC Card should be selected by default for the user when they navigate to the select payment method step.", driver);

            Log.message("When user successfully signs up for PLCC, the user will receive a flat $10 discount.");
            Log.softAssertThat(true,
                    "discount will be shown when user navigates to checkout",
                    "discount will be shown when user navigates to checkout",
                    "discount will be shown when user navigates to checkout", driver);

        	Log.testCaseResult();

        } // try
        catch (Exception e) {
            Log.exception(e, driver);
        } // catch
        finally {
        	Log.endTestCase(driver);
        } // finally

    }// TC_FBB_DROP5_C22754

    @Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
    public void M4_FBB_DROP5_C22754(String browser) throws Exception {
        //Pre-condition: Successfully apply for a PLCC through either the Web Instant Credit module or the PLCC application form for pre-approved Users
        //System launches the PLCC Approved Modal

        Log.testCaseInfo();

        // Get the web driver instance
        final WebDriver driver = WebDriverFactory.get(browser);
        int i = 1;
        try {
            new HomePage(driver, Utils.getWebSite()).get();
            Log.message(i++ + ". Navigated to Home Page.", driver);

            PLCC plccModules = new PLCC(driver).get();
            Log.message(i++ + ". PLCC Contents Loaded.", driver);

            plccModules.clickOnContinueTo();
            Log.message(i++ + ". Clicked on Continue To.", driver);

            CheckoutPage checkoutPage = new CheckoutPage(driver).get();
            //Verify the information available in Checkout Page on clicking the Continue to Checkout button when User has ordered only for a gift certificate
            Log.message("If the user has ordered only for a gift certificate");
            Log.softAssertThat(checkoutPage.getCurrentSection().equals("payment"),
                    "User should be navigated to the Step 2 Payment Details",
                    "User should be navigated to the Step 2 Payment Details",
                    "User should be navigated to the Step 2 Payment Details", driver);

            Log.softAssertThat(true,
                    "the delivery method should be Electronic delivery in Step 1",
                    "the delivery method should be Electronic delivery in Step 1",
                    "the delivery method should be Electronic delivery in Step 1", driver);

            Log.softAssertThat(true,
                    "The new PLCC Card should be selected by default for the user when they navigate to the select payment method step",
                    "The new PLCC Card should be selected by default for the user when they navigate to the select payment method step",
                    "The new PLCC Card should be selected by default for the user when they navigate to the select payment method step", driver);

        	Log.testCaseResult();

        } // try
        catch (Exception e) {
            Log.exception(e, driver);
        } // catch
        finally {
        	Log.endTestCase(driver);
        } // finally

    }// TC_FBB_DROP5_C22754

    @Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
    public void M5_FBB_DROP5_C22754(String browser) throws Exception {
        //Pre-condition: Successfully apply for a PLCC through either the Web Instant Credit module or the PLCC application form for pre-approved Users
        //System launches the PLCC Approved Modal

        Log.testCaseInfo();

        // Get the web driver instance
        final WebDriver driver = WebDriverFactory.get(browser);
        int i = 1;
        try {
            new HomePage(driver, Utils.getWebSite()).get();
            Log.message(i++ + ". Navigated to Home Page.", driver);

            PLCC plccModules = new PLCC(driver).get();
            Log.message(i++ + ". PLCC Contents Loaded.", driver);

            plccModules.clickOnContinueTo();
            Log.message(i++ + ". Clicked on Continue To.", driver);

            CheckoutPage checkoutPage = new CheckoutPage(driver).get();
            //Verify the information available in Checkout Page on clicking the Continue to Checkout button when User has a default address and default payment already saved
            Log.message("If the user has default address and default payment already saved");
            Log.softAssertThat(checkoutPage.getCurrentSection().equals("review"),
                    "User should be navigated to the Place Order Step with Shipping Address pre-populated.",
                    "User should be navigated to the Place Order Step with Shipping Address pre-populated.",
                    "User should be navigated to the Place Order Step with Shipping Address pre-populated.", driver);

            Log.softAssertThat(checkoutPage.getSelectedCardType().equals("plcc"),
                    "The new PLCC Card should be selected as the payment method for the user.",
                    "The new PLCC Card should be selected as the payment method for the user.",
                    "The new PLCC Card should be selected as the payment method for the user.", driver);

            //Verify the functionality of Closing the PLCC Approved Modal
            Log.softAssertThat(true,
                    "Clicking anywhere outside the modal should not close the PLCC Approved Modal.",
                    "Clicking anywhere outside the modal should not close the PLCC Approved Modal.",
                    "Clicking anywhere outside the modal should not close the PLCC Approved Modal.", driver);

            Log.softAssertThat(true,
                    "System should allow the user to close this modal either by clicking on Sign in to save this Card button or Continue Shopping button.",
                    "System should allow the user to close this modal either by clicking on Sign in to save this Card button or Continue Shopping button.",
                    "System should allow the user to close this modal either by clicking on Sign in to save this Card button or Continue Shopping button.", driver);

        	Log.testCaseResult();

        } // try
        catch (Exception e) {
            Log.exception(e, driver);
        } // catch
        finally {
        	Log.endTestCase(driver);
        } // finally

    }// TC_FBB_DROP5_C22754
}// search
