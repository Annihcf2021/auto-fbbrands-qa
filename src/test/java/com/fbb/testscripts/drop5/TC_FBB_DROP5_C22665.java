package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22665 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22665(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		
		{
			GlobalNavigation.registerNewUser(driver, 0, 2, email + "|" + password);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			MyAccountPage myAcc = headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged in to user account.", driver);
			PaymentMethodsPage payment = myAcc.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to payment methods page.", driver);
			
			//Step-1, 2: Verify the of Delete Confirmation and Subhead
			int cardCount = payment.getNumberOfSavedCards();
			String cardNumber = payment.clickDeleteOnSavedCard(1);
			Log.message(i++ + ". Clicked on delete button on card.", driver);
			
			Log.softAssertThat(payment.elementLayer.verifyVerticalAllignmentOfElements(driver, "overlayDeleteConfHeading", "overlayDeleteConfSubHeading", payment), 
					"Delete Confirmation heading should be displayed on top left of the Delete Card Confirmation modal", 
					"Delete Confirmation heading is displayed on top left of the Delete Card Confirmation modal", 
					"Delete Confirmation heading is not displayed on top left of the Delete Card Confirmation modal", driver);

			String overlaySubhead = payment.elementLayer.getAttributeForElement("overlayDeleteConfSubHeading", "innerHTML", payment);
			String overlaySubheadCardNumber = payment.elementLayer.getAttributeForElement("spanDeleteCardNumber", "innerHTML", payment);
			
			Log.softAssertThat(overlaySubhead.contains(overlaySubheadCardNumber), 
					"Subhead should be displayed in 2 lines.", 
					"Subhead is displayed in 2 lines.", 
					"Subhead is not displayed in 2 lines.", driver);
			
			Log.softAssertThat(StringUtils.getNumberInString(cardNumber) == StringUtils.getNumberInString(overlaySubheadCardNumber), 
					"Masked credit card number should be dynamically inserted into the file but last 4 numbers should be visible", 
					"Last 4 numbers is visible", 
					"Last 4 numbers is not visible", driver);
			
			//Step-3: Verify the functionality of Close Modal Options
			Log.softAssertThat(payment.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnCancelDelete","btnOverlayClose"), payment), 
					"Close Modal Options should be available with an X button and a Cancel button", 
					"Close Modal Options are available with an X button and a Cancel button", 
					"Close Modal Options are not available with an X button and a Cancel button", driver);
			
			payment.clickCancelOnDeleteConfirmation();
			Log.message(i++ + ". Clicked cancel on Overlay.", driver);
			
			Log.softAssertThat(!payment.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("ovrlayDeleteCard"), payment), 
					"System should close the \"Delete Card Confirmation\"modal", 
					"System closed the \"Delete Card Confirmation\"modal", 
					"System did not close the \"Delete Card Confirmation\"modal", driver);
			
			Log.softAssertThat(payment.getNumberOfSavedCards() == cardCount, 
					"Closing the modal should not delete the card from the customer profile. ", 
					"Closing the modal did not delete the card from the customer profile. ", 
					"Closing the modal deleted the card from the customer profile. ", driver);
			
			cardNumber = payment.clickDeleteOnSavedCard(1);
			Log.message(i++ + ". Clicked on delete button on card.", driver);
			
			payment.clickCloseConfirmationOverlay();
			Log.message(i++ + ". Clicked Close button on overlay.", driver);
			
			Log.softAssertThat(!payment.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("ovrlayDeleteCard"), payment), 
					"system should close the \"Delete Card Confirmation\"modal", 
					"system closed the \"Delete Card Confirmation\"modal", 
					"system did not close the \"Delete Card Confirmation\"modal", driver);
			
			Log.softAssertThat(payment.getNumberOfSavedCards() == cardCount, 
					"Closing the modal should not delete the card from the customer profile. ", 
					"Closing the modal did not delete the card from the customer profile. ", 
					"Closing the modal deleted the card from the customer profile. ", driver);
			
			cardNumber = payment.clickDeleteOnSavedCard(1);
			Log.message(i++ + ". Clicked on delete button on card.", driver);
			
			payment.clickOutSideOfDeleteOverlay();
			Log.message(i++ + ". Clicked on empty space.", driver);
			
			Log.softAssertThat(!payment.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("ovrlayDeleteCard"), payment), 
					"System should close the \"Delete Card Confirmation\"modal", 
					"System closed the \"Delete Card Confirmation\"modal", 
					"System did not close the \"Delete Card Confirmation\"modal", driver);
			
			//Step-4: Verify the functionality of Confirm button
			cardCount = payment.getNumberOfSavedCards();
			cardNumber = payment.clickDeleteOnSavedCard(1);
			Log.message(i++ + ". Clicked on delete button on card.", driver);
			
			Log.softAssertThat(payment.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnConfirmDelete", "btnCancelDelete", payment), 
					"Confirm should be displayed right of the Cancel Button.", 
					"Confirm is displayed right of the Cancel Button.", 
					"Confirm is not displayed right of the Cancel Button.", driver);
			
			payment.clickDeleteOnDeleteConfirmation();
			Log.message(i++ + ". Clicked DELETE on delete confirmation.", driver);
			
			Log.event("Card count: " + payment.getNumberOfSavedCards());
			Log.softAssertThat(payment.getNumberOfSavedCards() == (cardCount-1), 
					"When user clicks on Confirm button, System should remove payment method.", 
					"When user clicks on Confirm button, System removed payment method.", 
					"When user clicks on Confirm button, System did not remove payment method.", driver);
			
			Log.softAssertThat(!payment.verifyCardPresent(cardNumber), 
					"System should remove the respective payment instrument from the Saved Payment Methods.", 
					"System removed the respective payment instrument from the Saved Payment Methods.", 
					"System did not remove the respective payment instrument from the Saved Payment Methods.", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.removeAllPaymentMethods(driver);
			Log.endTestCase(driver);
		} // finally
	} // M1_FBB_DROP5_C22665

}
