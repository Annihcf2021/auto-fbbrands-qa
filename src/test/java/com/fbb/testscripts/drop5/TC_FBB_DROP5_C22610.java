package com.fbb.testscripts.drop5;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22610 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22610(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String email;
		String password = accountData.get("password_global");

		String divBackgroundColor = TestData.get("ReturnningSection_background");
		
		final WebDriver driver = WebDriverFactory.get(browser);
		email = AccountUtils.generateEmail(driver);
		
		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, email + "|" + password);
		}
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Clicked on Sign in link and navigated to sign in page!", driver);
	
			//Step-1: Verify the functionality of Breadcrumb
			if(Utils.isDesktop()) {
				if(signIn.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("txtPLCCPromoBanner"), signIn)) {
					Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "txtPLCCPromoBanner", "breadCrumb", signIn), 
							"Breadcrumb should be displayed below the Promotion Content ", 
							"Breadcrumb is displayed below the Promotion Content ", 
							"Breadcrumb is not displayed below the Promotion Content ", driver);
				}
				else {
					Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divGlobalNavigation", "breadCrumb", signIn), 
							"Breadcrumbs should appear below the Global Navigation menu.", 
							"Breadcrumbs appears below the Global Navigation menu.", 
							"Breadcrumbs does not appear below the Global Navigation menu.", driver);
				}
			}
	
			if(!Utils.isMobile()){
				List<String> txtInBreadCrumb = signIn.getTextInBreadcrumb();
				Log.softAssertThat(txtInBreadCrumb.get(0).equalsIgnoreCase("HOME")
						&& txtInBreadCrumb.get(1).equalsIgnoreCase("MY ACCOUNT"),
						"The breadcrumb should be displayed as - 'HOME > MY ACCOUNT'",
						"The breadcrumb is displayed as expected - 'HOME > MY ACCOUNT'",
						"The breadcrumb is not displayed as expected - 'HOME > MY ACCOUNT' and the actual is : '"
								+ txtInBreadCrumb.get(0) + " > " + txtInBreadCrumb.get(1) + "'", driver);
	
				String beforeURL = driver.getCurrentUrl();
				signIn.clickOnBreadCrumbValue(2);		
				Log.message(i++ + ". Clicked on 'My Account' in breadcrumb!", driver);
				String afterURL = driver.getCurrentUrl();
				Log.softAssertThat(beforeURL.equals(afterURL), 
						"'My Account' should not be clickable", 
						"'My Account' is not clickable",
						"'My Account' is clickable", driver);
	
				signIn.clickOnBreadCrumbValue(1);		
				Log.message(i++ + ". Clicked on 'Home' in breadcrumb!", driver);
	
				Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
						"Page should be navigated to home page when clicking on 'Home' in breadcrumb", 
						"Page is navigated to home page when clicking on 'Home' in breadcrumb",
						"Page is note navigated to home page when clicking on 'Home' in breadcrumb", driver);
			}else{
				Log.softAssertThat(signIn.getTextInBreadcrumbMobile().equalsIgnoreCase("HOME"),
						"The breadcrumb should be displayed as - ' < HOME'",
						"The breadcrumb is displayed as expected - ' < HOME'",
						"The breadcrumb is not displayed as expected - ' < HOME' and the actual is : '"+ signIn.getTextInBreadcrumbMobile(), 
						driver);
	
				signIn.clickOnBreakCrumbMobile();
				Log.message(i++ + ". Clicked on 'Home' in breadcrumb!", driver);
	
				Log.softAssertThat(new HomePage(driver).get().getPageLoadStatus(), 
						"Page should be navigated to home page when clicking on 'Home' in breadcrumb", 
						"Page is navigated to home page when clicking on 'Home' in breadcrumb",
						"Page is note navigated to home page when clicking on 'Home' in breadcrumb",
						driver);
			}
	
			signIn= homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Clicked on Sign in link and navigated to sign in page!", driver);
	
			//Step-2: Verify the functionality of Returning Customer in My Account Login page
			Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "breadCrumb", "loginCustomerHeading", signIn), 
					"The Returning Customer Heading should be displayed below the breadcrumb. ", 
					"The Returning Customer Heading is displayed below the breadcrumb. ", 
					"The Returning Customer Heading is not displayed below the breadcrumb. ", driver);
	
			Log.softAssertThat(signIn.elementLayer.verifyCssPropertyForElement("divLoginBoxDesktop", "background-color", divBackgroundColor, signIn), 
					"The Returning Customer module should be enclosed in a gray section box", 
					"The Returning Customer module is enclosed in a gray section box", 
					"The Returning Customer module is not enclosed in a gray section box", driver);
	
			//Step-3: Verify the functionality of Returning Customer Sub-head in My Account Login page
			Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "loginCustomerHeading", "loginCustomerSubHeading", signIn), 
					"Customer Subheading should be displayed below the Returning Customer Heading", 
					"Customer Subheading is displayed below the Returning Customer Heading", 
					"Customer Subheading is not displayed below the Returning Customer Heading", driver);
	
			//Step-4: Verify the functionality of the Email Address form field in the Returning Customer module
			Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "loginCustomerSubHeading", "fldEmail", signIn), 
					"The email address form field should be present below the Returning Customer subheading", 
					"The email address form field is below the Returning Customer subheading", 
					"The email address form field is not below the Returning Customer subheading", driver);
	
			signIn.typeOnEmail(email);
			Log.message(i++ + ". Entered Email address!", driver);
	
			Log.assertThat(signIn.elementLayer.verifyAttributeForElement("fldEmail", "value", email, signIn), 
					"The System should allow the User to enter the email address in the form field", 
					"The System should allow the User to enter the email address in the form field", 
					"The System should allow the User to enter the email address in the form field", driver);
	
			//Step-5: Verify the functionality of the Password form field in the Returning Customer module
			Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldEmail", "fldPassword", signIn), 
					"The Password entry form field will be present below the email address form field.", 
					"The Password entry form field is below the email address form field.", 
					"The Password entry form field is not below the email address form field.", driver);
	
			signIn.typeOnEmail(password);
			Log.message(i++ + ". Entered password!", driver);
	
			Log.softAssertThat(signIn.verifyPasswordisMaskedorUnmasked("mask"), 
					"Password should be masked after entering!", 
					"Password is masked after entering!",
					"Password is not masked after entering!", driver);
	
			Log.softAssertThat(signIn.verifyShowLabelDisplayedRightOfPassword(),
					"Show Label should be displayed on the right of password entered!",
					"Show Label is displayed on the right of password entered!",
					"Show Label is not displayed on the right of password entered!", driver);
	
			signIn.clickOnShowPasswordLabel();
			Log.message(i++ + ". Clicked on Show Password!", driver);
	
			Log.softAssertThat(signIn.verifyPasswordisMaskedorUnmasked("unmask"), 
					"Password should be unmasked after entering!", 
					"Password is unmasked after entering!",
					"Password is not unmasked after entering!", driver);
	
			//Step-8: Verify the functionality of the 'Forgot Your Password?' link
			Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnSignIn", "lnkForgotPwd", signIn), 
					"The Forgot Your Password link should be present under the 'Sign In' button", 
					"The Forgot Your Password link is present under the 'Sign In' button", 
					"The Forgot Your Password link is not present under the 'Sign In' button", driver);
	
			signIn.clickForgotPwdLink();
			Log.message(i++ + ". Clicked on Forgot Password link!", driver);
	
			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("divForgotPasswordModal"), signIn), 
					"Forgot Password Modal should be displayed", 
					"Forgot Password Modal is displayed",
					"Forgot Password Modal is not displayed", driver);
	
			signIn.closeForgotPasswordDialog();
			Log.message(i++ + ". Clicked on anywhere on the page to close forgot password pop up!", driver);
	
			//Step-9: Verify the functionality of New Customer in My Account Login page
			if(Utils.isMobile()){
				Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divLoginBoxContent", "divCreateAccountBoxMobile", signIn), 
						"The New Customer Heading should be displayed below the Returning Customer Section", 
						"The New Customer Heading is displayed below the Returning Customer Section", 
						"The New Customer Heading is not displayed below the Returning Customer Section", driver);
			}
			else {
				Log.softAssertThat(signIn.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divCreateAccountBox", "divReturningCustomer", signIn), 
						"The New Customer Heading should be displayed right of the Returning Customer Section", 
						"The New Customer Heading is displayed right of the Returning Customer Section", 
						"The New Customer Heading is not displayed right of the Returning Customer Section", driver);
			}
			
			if(!Utils.isMobile()) {
				//Step-10: Verify the functionality of New Customer Subhead in My Account Login page
				Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "createAccountHeading", "createAccountSubHeading", signIn), 
						"The New Customer Subheading should be displayed below the New Customer Heading", 
						"The New Customer Subheading is displayed below the New Customer Heading", 
						"The New Customer Subheading is not displayed below the New Customer Heading", driver);
	
				//Step-11: Verify the functionality of Create Account button in My Account Login page
				Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "createAccountSubHeading", "btnCreateAccount", signIn), 
						"The Create Account button should be displayed below the New Customer Subheading.", 
						"The Create Account button is displayed below the New Customer Subheading.", 
						"The Create Account button is not displayed below the New Customer Subheading.", driver);
	
				//Step-12: Verify the functionality of Account Benefit Message content in My Account Login page
				Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnCreateAccount", "createBenifitsContent", signIn), 
						"The Account Benefit message content should be displayed below the Create Account button.", 
						"The Account Benefit message content should be displayed below the Create Account button.", 
						"The Account Benefit message content should be displayed below the Create Account button.", driver);
			}
			else {
				//Step-10: Verify the functionality of New Customer Subhead in My Account Login page
				Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "createAccountHeadingMobile", "createAccountSubHeadingMobile", signIn), 
						"The New Customer Subheading should be displayed below the New Customer Heading", 
						"The New Customer Subheading is displayed below the New Customer Heading", 
						"The New Customer Subheading is not displayed below the New Customer Heading", driver);
	
				//Step-11: Verify the functionality of Create Account button in My Account Login page
				Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "createAccountSubHeadingMobile", "btnCreateAccountMobile", signIn), 
						"The Create Account button should be displayed below the New Customer Subheading.", 
						"The Create Account button is displayed below the New Customer Subheading.", 
						"The Create Account button is not displayed below the New Customer Subheading.", driver);
			}
	
			//Step-11 contd.
			signIn.clickOnCreateAccount();
			Log.message(i++ + ". Clicked on 'Create Account' button!", driver);
	
			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("createAccountSection"), signIn), 
					"Create Account form fields should be displayed", 
					"Create Account form fields is displayed",
					"Create Account form fields is not displayed", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M1_FBB_DROP4_C22610
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP5_C22610(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String email;
		String password = accountData.get("password_global");

		String divBackgroundColor = TestData.get("ReturnningSection_background");
			
		final WebDriver driver = WebDriverFactory.get(browser);
		email = AccountUtils.generateEmail(driver);
		
		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, email + "|" + password);
		}
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Clicked on Sign in link and navigated to sign in page!", driver);
	
			//Step-16: Verify the functionality of the 'View More' button in My Account Login page (Only for Mobile)
			if(Utils.isMobile()){
				BrowserActions.scrollToTopOfPage(driver);
				Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnCreateAccountMobile", "lnkViewMoreMobile", signIn), 
						"The 'View More' link should be displayed below the Create Account button", 
						"The 'View More' link should be displayed below the Create Account button", 
						"The 'View More' link should be displayed below the Create Account button", driver);
	
				signIn.clickOnViewMoreInMobile();
				Log.message(i++ + ". Clicked on 'View More' button in mobile!");
				
				//Step-14: Verify the functionality of Account Benefit Message content in My Account Login page
				Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnCreateAccountMobile", "createBenifitsContentMobile", signIn), 
						"The Account Benefit message content should be displayed below the Create Account button.", 
						"The Account Benefit message content should be displayed below the Create Account button.", 
						"The Account Benefit message content should be displayed below the Create Account button.", driver);
				
				signIn.clickOnCreateAccount();
				Log.message(i++ + ". Clicked on 'Create Account' button");
				
				Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("createAccountSection"), signIn), 
						"Create Account form fields should be displayed", 
						"Create Account form fields is displayed",
						"Create Account form fields is not displayed", driver);
			}
	
			//Step-6: Verify the functionality of Account Sign-In in My Account Login page
			signIn.navigateToMyAccount(email, password);
			Log.message(i++ + ". Entered valid credentials and navigated to 'My Account' page!", driver);
	
			homePage.headers.signOut();
			Log.message(i++ + ". Clicked on sign out!", driver);
	
			//Step-7: Verify the functionality of the 'Remember Me' checkbox		
			signIn.typeOnEmail(email);
			Log.message(i++ + ". Entered Email address!", driver);
	
			signIn.typeOnPassword(password);
			Log.message(i++ + ". Entered password!", driver);
	
			signIn.checkRememberMe(true);
			Log.message(i++ + ". Check 'Remember Me' checkbox!", driver);
	
			signIn.clickSignInButton();
			Log.message(i++ + ". Clicked on 'Sign In' button!", driver);
	
			homePage.headers.signOut();
			Log.message(i++ + ". Clicked on sign out!", driver);
	
			Log.softAssertThat(signIn.getEnteredEmailAddress().equals(email), 
					"Email Address should be prepopulated", 
					"Email Address is prepopulated",
					"Email Address is not prepopulated", driver);
			
			//Step 15: Verify the functionality of Check an Order Module in My Account Login page
			Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "divLoginBox", "divOrderStatusBox", signIn), 
					"The Check an Order Module should be displayed below the Returning Customer Section.", 
					"The Check an Order Module is displayed below the Returning Customer Section.", 
					"The Check an Order Module is not displayed below the Returning Customer Section.", driver);
	
			Log.softAssertThat(signIn.elementLayer.verifyCssPropertyForElement("divOrderStatusBox", "background-color", divBackgroundColor, signIn), 
					"The Check an Order module should be enclosed in a gray section box", 
					"The Check an Orderr module is enclosed in a gray section box", 
					"The Check an Order module is not enclosed in a gray section box", driver);
	
			List<String> elementsToBeVerified= Arrays.asList("txtFldOrderNumber", "txtFldOrderEmail", "txtFldBillingZipcode", "checkorderstatusbtn");
			Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfListOfElements(elementsToBeVerified, signIn), 
					"Order status fields should be displayed according to list", 
					"Order status fields are displayed according to list", 
					"Order status fields are not displayed according to list", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M2_FBB_DROP4_C22610

}// search
