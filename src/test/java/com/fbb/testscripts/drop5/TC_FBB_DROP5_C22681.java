package com.fbb.testscripts.drop5;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22681 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;

	@Test(enabled = false, groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22681(String browser) throws Exception {
		Log.testCaseInfo();
		try {
			//Step-1: Verify the functionality of available components in Order Details section
			Log.reference("<br>Verify the functionality of available components in Order Details section is covered in  C22671 ");
	
			//Step-2: Verify the functionality of available components in Shipment & Line Item Details
			Log.reference("<br>Verify the functionality of available components in Shipment & Line Item Details is covered in C22673");
	
			//Step-3: Verify the functionality of available components in Sale Pricing & Product Options
			Log.reference("<br>Verify the functionality of available components in Sale Pricing & Product Options is covered in C22675");
	
			//Step-4: Verify the functionality of available components in Electronic Gift Certificates & Gift Cards
			Log.reference("<br>Verify the functionality of available components in Electronic Gift Certificates & Gift Cards is covered in C22677");
	
			//Step-5: Verify the functionality of available components in Order Summary & FAQ
			Log.reference("<br>Verify the functionality of available components in Order Summary & FAQ is covered in C22679");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
		} // catch
		finally {
			Log.endTestCase();
		} // finally
	
	}// TC_FBB_DROP_C21550

}// search
