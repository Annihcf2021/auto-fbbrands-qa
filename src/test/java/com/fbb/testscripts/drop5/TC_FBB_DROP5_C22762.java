package com.fbb.testscripts.drop5;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22762 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22762(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String incorrectSSN = "123S";
		String address = "plcc_address_"+Utils.getCurrentBrandShort().substring(0, 2);
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			String Credential = AccountUtils.generateEmail(driver);
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_any, i, Credential);
	
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
	
			checkoutPage.fillingShippingDetailsAsGuest("YES", address, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping details entered successfully!", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Section!", driver);
	
			checkoutPage.continueToPLCCStep2();
			Log.message(i++ + ". Continued to PLCC Step-2", driver);
	
			//Step 1: Verify the functionality of Accept Offer Copy
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblAcceptOffCopy", "txtSSNinPLCCStep2", checkoutPage), 
					"Accept Offer Copy shoudl be located on the top of the page. ", 
					"Accept Offer Copy is located on the top of the page. ", 
					"Accept Offer Copy is not located on the top of the page. ", driver);
			
			//Step 2: Verify the functionality of Social Security Number
			Log.message("Step 2: Verify the functionality of Social Security Number", driver);
			checkoutPage.clickOnAcceptInPLCC();
			Log.message(i++ + ". Clicked on Accept Button in PLCC modal with empty SSN.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForListOfElement(Arrays.asList("txtSSNinPLCCStep2"), "aria-required", "true", checkoutPage),
					"SSN should be mandatory field",
					"SSN is mandatory field",
					"SSN not mandatory field", driver);
	
			checkoutPage.typeTextInSSN("1234");
			Log.message(i++ + ". SSN entered into SSN Field.", driver);
			Log.softAssertThat(checkoutPage.elementLayer.verifyTxtFldElementAllows("txtSSNinPLCCStep2", "number", checkoutPage, "SSN Number field"),
					"User should be able to enter numbers in the Social Security Number Field",
					"User be able to enter numbers in the Social Security Number Field",
					"User not able to enter numbers in the Social Security Number Field", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("txtSSNinPLCCStep2"), 4, checkoutPage),
					"System should allow the user to enter only 4 digits.",
					"System allow the user to enter only 4 digits.",
					"System allow the user to enter more than 4 digits.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("txtSSNinPLCCStep2", "placeHolderSSNinPLCCStep2", "1234", checkoutPage),
					"Place holder text 'Last 4 digits' should be positioned on top when the user starts typing in the text box.",
					"Place holder text 'Last 4 digits' positioned on top when the user starts typing in the text box.",
					"Place holder text 'Last 4 digits' not positioned on top when the user starts typing in the text box.", driver);
	
			checkoutPage.typeTextInSSN(incorrectSSN);
			Log.message(i++ + ". Incorrect SSN ["+ incorrectSSN +"] entered into SSN Field.", driver);
			checkoutPage.clickOnAcceptInPLCC();
			Log.message(i++ + ". Clicked on Accept Button in PLCC modal with Incorrect SSN.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("errSSNinPLCCStep2"), checkoutPage),
					"If invalid SSN is entered by the user, System should display appropriate error message",
					"If invalid SSN is entered by the user, System displays appropriate error message",
					"If invalid SSN is entered by the user, System not display appropriate error message", driver);
	
			checkoutPage.clearPLCCAccFields();
			Log.message(i++ + ". Clearing all fields.", driver);
			checkoutPage.clickOnAcceptInPLCC();
			Log.message(i++ + ". Clicked on Accept Button in PLCC modal without filling any fields.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("txtSSNinPLCCStep2","txtMobileNoInPLCCStep2"), checkoutPage) &&
					(checkoutPage.elementLayer.verifyAttributeForListOfElement(Arrays.asList("selectPLCCBirthDateDrp","selectPLCCBirthMonthDrp","selectPLCCBirthYearDrp"), "class", "error", checkoutPage) ||
							checkoutPage.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("divPLCCBirthDateDrp","divPLCCBirthMonthDrp","divPLCCBirthYearDrp"), "color", "rgba(153, 0, 0, 1)", checkoutPage)),
					"If user tries to leave the field blank, System should display appropriate error message",
					"If user tries to leave the field blank, System displays appropriate error message",
					"If user tries to leave the field blank, System not display appropriate error message", driver);
	
			checkoutPage.clickOnSSNToolTip();
			Log.message(i++ + ". Clicked on Tool tip icon in SSN Lable", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("toolTipForPLCC"), checkoutPage),
					"On clicking Tool Tip, System should display the Tool Tip for SSN",
					"On clicking Tool Tip, System display the Tool Tip for SSN",
					"On clicking Tool Tip, System not display the Tool Tip for SSN", driver);
	
			checkoutPage.clickOnSSNToolTipClose();
			Log.message(i++ + ". Closed SSN Tool tip", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("lblSSNMasked"), checkoutPage),
					"By default, all the digits of SSN should be masked except the last 4 digits",
					"By default, all the digits of SSN masked except the last 4 digits",
					"By default, all the digits of SSN not masked as excepted", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP5_C22762
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP5_C22762(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String address = "plcc_address_"+Utils.getCurrentBrandShort().substring(0, 2);
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			String Credential = AccountUtils.generateEmail(driver);
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_any, i, Credential);
			
			String date = "15";
			String month = "12";
			String year = "1965";
	
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
	
			checkoutPage.fillingShippingDetailsAsGuest("YES", address, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping details entered successfully!", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Section!", driver);
	
			checkoutPage.continueToPLCCStep2();
			Log.message(i++ + ". Continued to PLCC Step-2", driver);
	
			//Step 3: Verify the functionality of components available in Birth Date (Month, Day and Year)
			Log.message("Step 3: Verify the functionality of components available in Birth Date (Month, Day and Year)", driver);
			
			checkoutPage.clickOnAcceptInPLCC();
			Log.message(i++ + ". Clicked on Accept Button in PLCC Modal.", driver);
	
			//Step-3 and 13c
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForListOfElement(Arrays.asList("selectPLCCBirthDateDrp","selectPLCCBirthMonthDrp","selectPLCCBirthYearDrp"),"class","error", checkoutPage) ||
					checkoutPage.elementLayer.verifyListOfElementColor(Arrays.asList("selectPLCCBirthDateDrp","selectPLCCBirthMonthDrp","selectPLCCBirthYearDrp"),"#990000", checkoutPage),
					"If user tries to leave the field blank, System should display appropriate error message",
					"If user tries to leave the field blank, System displays appropriate error message",
					"If user tries to leave the field blank, System did not display appropriate error message", driver);
			
			checkoutPage.selectDateMonthYearInPLCC2(date, month, year);
			Log.message(i++ + ". Selected Birth Date["+date + "-" + month + "-"+ year +"] from Dropdown.", driver);
	
			Log.softAssertThat(checkoutPage.getDateFromPLCCBirthDate().equals(date) && checkoutPage.getMonthFromPLCCBirthDate().equals(month) && checkoutPage.getYearFromPLCCBirthDate().equals(year), 
					"User should be able to select the Day, Month, Year from the respective drop down", 
					"User able to select the Day, Month, Year from the respective drop down", 
					"User not able to select the Day, Month, Year from the respective drop down", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForListOfElement(Arrays.asList("selectPLCCBirthDateDrp","selectPLCCBirthMonthDrp","selectPLCCBirthYearDrp"), "aria-required", "true", checkoutPage),
					"Date, Month, Year should be mandatory field",
					"Date, Month, Year is mandatory field",
					"Date, Month, Year not mandatory field", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForPsuedoElement("divPLCCBirthDateDrp", "before", "background", "carat-down", checkoutPage) &&
					checkoutPage.elementLayer.verifyCssPropertyForPsuedoElement("divPLCCBirthDateDrp", "before", "background", "carat-down", checkoutPage) &&
					checkoutPage.elementLayer.verifyCssPropertyForPsuedoElement("divPLCCBirthDateDrp", "before", "background", "carat-down", checkoutPage), 
					"Carat Symbol should be displayed on right of the place holder text positioned downward", 
					"Carat Symbol displayed on right of the place holder text positioned downward", 
					"Carat Symbol not displayed on right of the place holder text positioned downward", driver);
	
			Log.softAssertThat(checkoutPage.verifyDateDropdown(), 
					"On click, System should display the Day (1 to 31) accordingly in the respective order and the carat symbol should be positioned upward", 
					"On click, System display the Day (1 to 31) accordingly in the respective order and the carat symbol should be positioned upward", 
					"On click, System not display the Day (1 to 31) accordingly in the respective order and the carat symbol should be positioned upward", driver);
	
			Log.softAssertThat(checkoutPage.verifyMonthDropdown(), 
					"On click, System should display the Month (1 to 12) in the respective order and the carat symbol should be positioned upward", 
					"On click, System display the Month (1 to 12) in the respective order and the carat symbol should be positioned upward", 
					"On click, System not display the Month (1 to 12) in the respective order and the carat symbol should be positioned upward", driver);
	
			Log.softAssertThat(checkoutPage.verifyYearDropdown(), 
					"On click, System should display the Year (Current Year to Current Year - 100) accordingly in the respective order and the carat symbol should be positioned upward", 
					"On click, System display the Year (Current Year to Current Year - 100) accordingly in the respective order and the carat symbol should be positioned upward", 
					"On click, System not display the Year (Current Year to Current Year - 100) accordingly in the respective order and the carat symbol should be positioned upward", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblNameInPLCCPreAddress","lblAddressInPLCCPreAddress","lblCityInPLCCPreAddress"), checkoutPage),
					"Name, Address and Email should be pre-populated with the pre-approval record data",
					"Name, Address and Email pre-populated with the pre-approval record data",
					"Name, Address and Email not pre-populated with the pre-approval record data", driver);
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.testCaseResult();
			Log.endTestCase(driver);
		} // finally
	
	}// M2_FBB_DROP5_C22762
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP5_C22762(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String plccAddress = "plcc_address_"+Utils.getCurrentBrandShort().substring(0, 2);
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			String Credential = AccountUtils.generateEmail(driver);
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_any, i, Credential);
	
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
	
			checkoutPage.fillingShippingDetailsAsGuest("YES", plccAddress, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping details entered successfully!", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Section!", driver);
	
			checkoutPage.continueToPLCCStep2();
			Log.message(i++ + ". Continued to PLCC Step-2", driver);
	
			//Step 4: Verify the functionality of Personal Information
			Log.message("Step 4: Verify the functionality of Personal Information", driver);
			checkoutPage.clickonEditAddressInPLCC();
			Log.message(i++ + ". Clicked on Edit link in PLCC Address", driver);
	
			//Step 5: Verify the functionality of Edit
			Log.message("Step 5: Verify the functionality of Edit", driver);
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtFirstNameInPLCCStep2","txtLastNameInPLCCStep2","txtAddress1InPLCCStep2","txtCityInPLCCStep2","txtZipcodeInPLCCStep2","txtEmailInPLCCStep2"), checkoutPage) && 
					checkoutPage.elementLayer.verifyTxtElementsNotEmpty(Arrays.asList("txtFirstNameInPLCCStep2","txtLastNameInPLCCStep2","txtAddress1InPLCCStep2","txtCityInPLCCStep2","txtZipcodeInPLCCStep2","txtEmailInPLCCStep2"), checkoutPage),
					"On clicking Edit, all the address fields should be exposed with the data pre-populated",
					"On clicking Edit, all the address fields exposed with the data pre-populated",
					"On clicking Edit, all the address fields not exposed with the data pre-populated", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAboveForList(Arrays.asList("txtFirstNameInPLCCStep2","txtLastNameInPLCCStep2","txtAddress1InPLCCStep2","txtAddress2InPLCCStep2","txtCityInPLCCStep2","txtZipcodeInPLCCStep2","txtEmailInPLCCStep2"), 
					Arrays.asList("placeHolderFirstNameInPLCCStep2","placeHolderLastNameInPLCCStep2","placeHolderAddress1InPLCCStep2","placeHolderAddress2InPLCCStep2","placeHolderCityInPLCCStep2","placeHolderZipcodeInPLCCStep2","placeHolderEmailInPLCCStep2"), 
					Arrays.asList("JOHN","GKPNGFF","7735 N Tryon St","NW 77TH","MIAMI LAKES","33016","automation4c22524@yopmail.com"), checkoutPage),
					"Place holder text in these fields should move above if user starts typing - {First Name, Last Name, Address Line, State, Zip Code, Email Address}",
					"Place holder text were moved above for all the fields.",
					"Place holder text were not moving above for all the fields.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyListOfTxtFldElementAllows(Arrays.asList("txtFirstNameInPLCCStep2","txtLastNameInPLCCStep2","txtAddress1InPLCCStep2","txtAddress2InPLCCStep2","txtCityInPLCCStep2","txtZipcodeInPLCCStep2","txtEmailInPLCCStep2"), 
					Arrays.asList("alphabet","alphabet","alphabet","alphabet","alphabet","11211","alphabet"), checkoutPage),
					"User Should be able to edit & enter text in {First Name, Last Name, Address Line, State, Zip Code, Email Address} fields.",
					"User is able to edit all the above fields",
					"User cannot edit all the above fields.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyAttributeForListOfElement(Arrays.asList("txtFirstNameInPLCCStep2","txtLastNameInPLCCStep2","txtAddress1InPLCCStep2","txtCityInPLCCStep2","txtZipcodeInPLCCStep2"), "aria-required", "true", checkoutPage),
					"First Name, Last Name, Address Line, State, Zip Code fields should be mandatory.",
					"All the above said fields are mandatory",
					"Not all of the above said fields are mandatory", driver);
	
			checkoutPage.elementLayer.clearTextFields(Arrays.asList("txtFirstNameInPLCCStep2","txtLastNameInPLCCStep2","txtAddress1InPLCCStep2","txtAddress2InPLCCStep2","txtCityInPLCCStep2","txtZipcodeInPLCCStep2","txtEmailInPLCCStep2"), checkoutPage);
			Log.message(i++ + ". All fields are cleared.", driver);
	
			checkoutPage.clickOnAcceptInPLCC();
			Log.message(i++ + ". Clicked on Accept button.", driver);
	
			checkoutPage.selectStateInPLCC2("Select...");
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("errFirstNameInPLCCStep2","errLastNameInPLCCStep2","errAddress1InPLCCStep2","errCityInPLCCStep2","errZipcodeInPLCCStep2"), checkoutPage),
					"If the user tries to leave these fields, appropriate error message should be displayed - {First Name, Last Name, Address Line, State, Zip Code}",
					"If the user tries to leave these fields, appropriate error message displayed",
					"If the user tried to leave these fields, appropriate error message not displayed for all the fields.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyTextFieldAllowSpecialCharacters(Arrays.asList("txtZipcodeInPLCCStep2"), Arrays.asList("55555", "55555-4444"), checkoutPage),
					"System should allow the user to enter zip code as 5 digits (Ex: 55555) or 5 + 4 Digits (55555 4444)",
					"System allow the user to enter zip code as 5 digits (Ex: 55555) or 5 + 4 Digits (55555 4444)",
					"System not allow the user to enter zip code as 5 digits (Ex: 55555) or 5 + 4 Digits (55555 4444)", driver);
	
			checkoutPage.typeTextInEmailInPLCC("aut@");
			Log.message(i++ + ". Invalid Email[aut@] address entered.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("errEmailInPLCCStep2"), checkoutPage),
					"System should perform the regex validation for the correct format of the Email Field",
					"System performs the regex validation for the correct format of the Email Field",
					"System didn't perform the regex validation for the correct format of the Email Field", driver);
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.testCaseResult();
			Log.endTestCase(driver);
		} // finally
	
	}// M3_FBB_DROP5_C22762
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP5_C22762(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String incorrectMobile = "223332233";
		String address = "plcc_address_"+Utils.getCurrentBrandShort().substring(0, 2);
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			String Credential = AccountUtils.generateEmail(driver);
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_any, i, Credential);
	
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
	
			checkoutPage.fillingShippingDetailsAsGuest("YES", address, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping details entered successfully!", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Section!", driver);
	
			checkoutPage.continueToPLCCStep2();
			Log.message(i++ + ". Continued to PLCC Step-2", driver);
	
			//Step 6: Verify the functionality of Mobile Number
			Log.message("Step 6: Verify the functionality of Mobile Number", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyTxtFldElementAllows("txtMobileNoInPLCCStep2", "2233322333", checkoutPage, "Mobile Number IN PLCC"),
					"User should be able to enter number in the Mobile Number",
					"User able to enter number in the Mobile Number",
					"User not able to enter number in the Mobile Number", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("txtMobileNoInPLCCStep2", "placeHolderMobileNoInPLCCStep2", "2233322333", checkoutPage),
					"Place holder text in these fields should move above if user starts typing in Mobile Number field",
					"Place holder text in these fields move above if user starts typing in Mobile Number field",
					"Place holder text in these fields not move above if user starts typing in Mobile Number field", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("txtMobileNoInPLCCStep2"), 10, checkoutPage),
					"System should allow the user to enter only 10 digits",
					"System allows the user to enter only 10 digits",
					"System not allows the user to enter only 10 digits", driver);
	
			checkoutPage.typeTextInMobileInPLCC("");
			checkoutPage.clickOnAcceptInPLCC();
			Log.message(i++ + ". Clicked on Accept button in PLCC Modal", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("errMobileNoInPLCCStep2"), checkoutPage),
					"If user tries to leave the Mobile number field blank, an appropriate error message should be displayed",
					"If user tries to leave the Mobile number field blank, an appropriate error message is displayed",
					"If user tries to leave the Mobile number field blank, an appropriate error message not displayed", driver);
	
			checkoutPage.typeTextInMobileInPLCC(incorrectMobile);
			Log.message(i++ + ". Invalid Email["+incorrectMobile+"] address entered.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("errMobileNoInPLCCStep2"), checkoutPage),
					"If any incorrect data format is entered, an appropriate error message should be displayed",
					"If any incorrect data format is entered, an appropriate error message is displayed",
					"If any incorrect data format is entered, an appropriate error message not displayed", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyTxtFldElementAllows("txtAltMobileNoInPLCCStep2", "2233322332", checkoutPage, "Alternate Mobile"),
					"User should be able to enter/edit number in the Alternate Phone Number",
					"User able to enter/edit number in the Alternate Phone Number",
					"User not able to enter/edit number in the Alternate Phone Number", driver);
			
			//Step 8: Verify the functionality of Contact Disclaimer
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "icoSSNToolTip", "lblContactDisclaimer", checkoutPage)
							|| checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "icoSSNToolTip", "lblContactDisclaimerAlt", checkoutPage),
					"Contact Disclaimer should be located underneath the SSN, DOB and Phone Number form fields.",
					"Contact Disclaimer is located underneath the SSN, DOB and Phone Number form fields.",
					"Contact Disclaimer is not located underneath the SSN, DOB and Phone Number form fields.", driver);
	
			//Step 9-10: Verify the display of Consent to Financial Terms.
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblContactDisclaimer", "chkConsonent", checkoutPage)
							|| checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblContactDisclaimerAlt", "chkConsonent", checkoutPage),
					"Consent to Financial Terms section should be located underneath the Contact Disclaimer.",
					"Consent to Financial Terms section is located underneath the Contact Disclaimer.",
					"Consent to Financial Terms section is not located underneath the Contact Disclaimer.", driver);
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.testCaseResult();
			Log.endTestCase(driver);
		} // finally
	
	}// M4_FBB_DROP5_C22762
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M5_FBB_DROP5_C22762(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String incorrectMobile = "223332233";
		String address = "plcc_address_"+Utils.getCurrentBrandShort().substring(0, 2);
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			String Credential = AccountUtils.generateEmail(driver);
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_any, i, Credential);
	
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
	
			checkoutPage.fillingShippingDetailsAsGuest("YES", address, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping details entered successfully!", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Section!", driver);
	
			checkoutPage.continueToPLCCStep2();
			Log.message(i++ + ". Continued to PLCC Step-2", driver);
	
			//Step 7: Verify the functionality of Alternate Phone Number
			Log.message("Step 7: Verify the functionality of Alternate Phone Number", driver);
			Log.softAssertThat(!checkoutPage.elementLayer.verifyAttributeForListOfElement(Arrays.asList("txtAltMobileNoInPLCCStep2"), "aria-required", "false", checkoutPage),
					"Alternate Phone number should be a optional field",
					"Alternate Phone number is an optional field",
					"Alternate Phone number not an optional field", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("txtAltMobileNoInPLCCStep2"), 10, checkoutPage),
					"System should allow the user to enter only 10 digits",
					"System allow the user to enter only 10 digits",
					"System not allow the user to enter only 10 digits", driver);
	
			checkoutPage.typeTextInAltMobileInPLCC(incorrectMobile);
			Log.message(i++ + ". Incorrect Mobile number ["+incorrectMobile+"] entered.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("errAltMobileNoInPLCCStep2"), checkoutPage),
					"If any incorrect data format is entered, then appropriate error message should be displayed",
					"If any incorrect data format is entered, then appropriate error message displayed",
					"If any incorrect data format is entered, appropriate error message not displayed", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyPlaceHolderMovesAbove("txtAltMobileNoInPLCCStep2", "placeHolderAltMobileNoInPLCCStep2", "2233322331", checkoutPage),
					"Place holder text in these fields should move above if user starts typing in Alternate Mobile Number field",
					"Place holder text in these fields move above if user starts typing in Alternate Mobile Number field",
					"Place holder text in these fields not move above if user starts typing in Alternate Mobile Number field", driver);
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.testCaseResult();
			Log.endTestCase(driver);
		} // finally
	
	}// M5_FBB_DROP5_C22762
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M6_FBB_DROP5_C22762(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String plccAddress = "plcc-user-rm";
		String accountDetails = "plcc-account";
		String address = "plcc_address_"+Utils.getCurrentBrandShort().substring(0, 2);
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			String Credential = AccountUtils.generateEmail(driver);
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_any, i, Credential);
	
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
	
			checkoutPage.fillingShippingDetailsAsGuest("YES", address, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping details entered successfully!", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Section!", driver);
	
			checkoutPage.continueToPLCCStep2();
			Log.message(i++ + ". Continued to PLCC Step-2", driver);
	
			//Step 11: Verify the functionality of Consent Checkbox
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsUnChecked(Arrays.asList("chkConsonent"), checkoutPage),
					"By default, the Consent Checkbox should be unchecked",
					"By default, the Consent Checkbox was unchecked",
					"By default, the Consent Checkbox not unchecked", driver);
	
			checkoutPage.checkConsentInPLCC("YES");
			Log.message(i++ + ". Consent Checkbox checked.", driver);
	
			checkoutPage.clickOnAcceptInPLCC();
			Log.message(i++ + ". Clicked on Accept Offer Button in PLCC.", driver);
	
			Log.assertThat(checkoutPage.elementLayer.verifyPageElementsChecked(Arrays.asList("chkConsonent"), checkoutPage),
					"User should select the Consent Checkbox prior to application submission",
					"User can select the Consent Checkbox prior to application submission",
					"User cannot select the Consent Checkbox prior to application submission", driver);
	
			checkoutPage.checkConsentInPLCC("NO");
			Log.message(i++ + ". Consent Checkbox un-checked.", driver);
	
			checkoutPage.clickOnAcceptInPLCC();
			Log.message(i++ + ". Clicked on Accept Offer Button in PLCC.", driver);
	
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementColor("chkConsonent", "#bd34", checkoutPage)
							|| checkoutPage.elementLayer.verifyElementColor("chkConsonent", "#9900", checkoutPage),
					"If user does not select the Consent Checkbox prior to application submission then the System should present the checkbox in a red outline",
					"System presented the checkbox in a red outline",
					"System did not present the checkbox in a red outline", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyCssPropertyForElement("chkConsonent", "border", "2px solid", checkoutPage),
					"If user does not select the Consent Checkbox prior to application submission then the System should present the checkbox in a red outline",
					"System presented the checkbox in a red outline",
					"System did not present the checkbox in a red outline", driver);
	
			//Step 14: Verify the functionality of Consent to Financial Terms
			Log.message("Step 13: Verify that user can Edit Shipping and/or Billing information.");
			checkoutPage.clickonEditAddressInPLCC();
	
			checkoutPage.clearPLCCAccFields();
			Log.message(i++ + ". Cleared All PLCC Fields.", driver);
	
			checkoutPage.fillPLCCAccountDetails(accountDetails);
			Log.message(i++ + ". Filled PLCC Account Details.", driver);
	
			checkoutPage.fillingAddressInPLCC(plccAddress);
			Log.message(i++ + ". Filled Address Details in PLCC.", driver);
	
			checkoutPage.checkConsentInPLCC("YES");
	
			checkoutPage.clickOnAcceptInPLCC();
			Log.message(i++ + ". Clicked on Accept in PLCC Modal.", driver);
			
			//Step 13: Verify the functionality of Yes, I Accept button
			checkoutPage.dismissCongratulationModal();
			Log.message(i++ + ". Close Congratulations Modal.", driver);
			
			checkoutPage.clickHeaderShipping();
			checkoutPage.continueToPayment();
			
			Log.softAssertThat(!checkoutPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnNoThanksInPLCC2"), checkoutPage), 
					"Once dismissed, the System should no longer display the pre-approval modal for the duration of the session for that particular user.", 
					"System should no longer display the pre-approval modal for the duration of the session", 
					"System should displayed the pre-approval modal for the duration of the session", driver);
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.testCaseResult();
			Log.endTestCase(driver);
		} // finally
	
	}// M6_FBB_DROP5_C22762
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M7_FBB_DROP5_C22762(String browser) throws Exception{
		Log.testCaseInfo();
		
		//Load Test Data
		String prd_any = TestData.get("prd_variation");
		String address = "plcc_address_"+Utils.getCurrentBrandShort().substring(0, 2);
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			String Credential = AccountUtils.generateEmail(driver);
			Object[] obj = GlobalNavigation.addProduct_Checkout(driver, prd_any, i, Credential);
	
			CheckoutPage checkoutPage = (CheckoutPage) obj[0];
			i = (int) obj[1];
	
			//Step 12: Verify the functionality of No Thanks
			Log.message("Step 11: Verify the functionality of No Thanks");
			
			checkoutPage.fillingShippingDetailsAsGuest("YES", address, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping details entered successfully!", driver);
	
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Section!", driver);
			
			checkoutPage.closePLCCOfferByNoThanks();
			Log.message(i++ + ". Clicked on No Thanks", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("mdlPLCCRebuttal"), checkoutPage), 
					"On clicking No Thanks button, system should close the modal and should store the session attribute to invoke Acquisition Rebuttal content slot", 
					"System shows the acquisition rebuttal slot.", 
					"System does not show the acquisition rebuttal slot.", driver);
			
			checkoutPage.openClosePLCCRebuttal("open");
			checkoutPage.closePLCCOfferByNoThanks();
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("mdlPLCCRebuttal"), checkoutPage), 
					"Acquisiotion rebuttal should no longer be shown.",
					"System does not show the acquisition rebuttal slot.",
					"System shows the acquisition rebuttal slot.", driver);
			
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.testCaseResult();
			Log.endTestCase(driver);
		} // finally
	}// M7_FBB_DROP5_C22762

}// search
