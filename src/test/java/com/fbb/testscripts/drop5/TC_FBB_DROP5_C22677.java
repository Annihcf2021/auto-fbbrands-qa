package com.fbb.testscripts.drop5;
import com.fbb.reusablecomponents.GlobalNavigation;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22677 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22677(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String credentials = accountData.get("giftcard_order_history");
		String email = credentials.split("\\|")[0];
		String password = credentials.split("\\|")[1];
		{
		GlobalNavigation.addGCProducts_PlaceOrder(driver, true, true, 0, email+"|"+password);
		}
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			MyAccountPage myAcc = headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged into account.", driver);
			
			myAcc.navigateTo("order-history");
			OrderHistoryPage orders = new OrderHistoryPage(driver).get();
			Log.message(i++ + ". Navigated to Order History page", driver);
			
			orders.clickOrderByIndex(0);
			Log.message(i++ + ". Opened up Order details.", driver);
			
			//Step-1: Verify the functionality of Method
			Log.softAssertThat(orders.elementLayer.verifyVerticalAllignmentOfElements(driver, "gcShimpmentHeading", "gcShipmentMethod", orders)
							&& orders.elementLayer.verifyVerticalAllignmentOfElements(driver, "egcShimpmentHeading", "egcShipmentMethod", orders),
					"Method should be displayed below the Shipment Heading.",
					"Method is displayed below the Shipment Heading.",
					"Method is not displayed below the Shipment Heading.", driver);

			Log.softAssertThat(orders.elementLayer.verifyTextContains("gcShipmentMethod", "Standard Delivery", orders), 
					"By default for Gift Cards, Method should be displayed as Standard Delivery",
					"Method is displayed as Standard Delivery",
					"Method is not displayed as Standard Delivery", driver);
			
			Log.softAssertThat(orders.elementLayer.verifyTextContains("egcShipmentMethod", "Electronic Delivery", orders), 
					"By default for Electronic Gift Certificates, Method should be displayed as Electronic Delivery",
					"Method is displayed as Electronic Delivery",
					"Method is not displayed as Electronic Delivery", driver);

			//Step-2: Verify the functionality of Delivery Address
			//Gift Card
			Log.softAssertThat(orders.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("gcShipmentName", "gcShipmentAddress1", "gcShipmentAddress2"), orders), 
					"Delivery Address should display the correct components",
					"Delivery Address displays the correct components",
					"Delivery Address does not display the correct components", driver);
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(orders.elementLayer.verifyHorizontalAllignmentOfElements(driver, "gcShipmentAddress", "gcShipmentMethod", orders), 
						"Delivery Address (applicable for Gift Cards) should be displayed right of Methods for Gift Cards",
						"Delivery Address (applicable for Gift Cards) is displayed right of Methods for Gift Cards",
						"Delivery Address (applicable for Gift Cards) is not displayed right of Methods for Gift Cards", driver);
				
				
			}else {
				Log.softAssertThat(orders.elementLayer.verifyVerticalAllignmentOfElements(driver, "gcShipmentMethod", "gcShipmentAddress", orders), 
						"Delivery Address should be displayed below the Methods for Gift Cards",
						"Delivery Address is displayed below the Methods for Gift Cards",
						"Delivery Address is not displayed below the Methods for Gift Cards", driver);
			}
			
			String shipName = orders.elementLayer.getAttributeForElement("gcShipmentName", "innerHTML", orders);
			Log.softAssertThat(shipName.split("\\s").length == 2,
					"FirstName and LastName should be separated by Space", 
					"FirstName and LastName are separated by Space",
					"FirstName and LastName are not separated by Space", driver);
			
			Log.softAssertThat(orders.elementLayer.verifyCssPropertyForElement("gcShipmentName", "border-right", "1px solid", orders), 
					"Name and Address fields should be separated by \"|\" Symbol", 
					"Name and Address fields are separated by \"|\" Symbol", 
					"Name and Address fields are not separated by \"|\" Symbol", driver);
			
			String shipAddress = orders.elementLayer.getElementText("gcShipmentAddress", orders);
			Log.softAssertThat(shipAddress.split(",").length >= 4, 
					"Address fields should be separated by Comma", 
					"Address fields are separated by Comma", 
					"Address fields are not separated by Comma", driver);
			
			//E-Gift Card
			Log.softAssertThat(orders.elementLayer.verifyTextContains("egcShipmentMethod", "Electronic Delivery", orders), 
					"For Electronic Gift Certificates the Shipping Method should show as Electronic Delivery", 
					"For Electronic Gift Certificates the Shipping Method shows as Electronic Delivery", 
					"For Electronic Gift Certificates the Shipping Method does not show as Electronic Delivery", driver);
			
			//Step-3: Verify the functionality of Shipment Status
			if(!Utils.isMobile()) {
				Log.softAssertThat(orders.elementLayer.verifyHorizontalAllignmentOfElements(driver, "gcShipmentStatus", "gcShipmentAddress", orders)
								&& orders.elementLayer.verifyHorizontalAllignmentOfElements(driver, "egcShipmentStatus", "egcShipmentMethod", orders),
						"Shipment Status should be displayed right corner of Method", 
						"Shipment Status is displayed right corner of Method", 
						"Shipment Status is not displayed right corner of Method", driver);
			}else {
				Log.softAssertThat(orders.elementLayer.verifyVerticalAllignmentOfElements(driver, "gcShipmentAddress", "gcShipmentStatus", orders)
								&& orders.elementLayer.verifyVerticalAllignmentOfElements(driver, "egcShipmentMethod", "egcShipmentStatus", orders),
						"Shipment Status should be displayed below the Method", 
						"Shipment Status is displayed below the Method", 
						"Shipment Status is not displayed below the Method", driver);
			}
			
			String gcShipmentStatus = orders.elementLayer.getElementText("gcShipmentStatus", orders);
			Log.softAssertThat(gcShipmentStatus.contains("Shipped") || gcShipmentStatus.contains("NotShipped") || gcShipmentStatus.contains("Not Shipped Yet"), 
					"System should show shipment status of Gift Card as shipped or not shipped yet.", 
					"System shows shipment status of Gift Card as shipped or not shipped yet.", 
					"System does not show shipment status of Gift Card as shipped or not shipped yet.", driver);
			
			String egcShipmentStatus = orders.elementLayer.getElementText("gcShipmentStatus", orders);
			Log.softAssertThat(egcShipmentStatus.contains("Sent") || egcShipmentStatus.contains("Not Sent Yet") || egcShipmentStatus.contains("Shipped") || egcShipmentStatus.contains("NotShipped") || egcShipmentStatus.contains("Not Shipped Yet"), 
					"System should show shipment status of EGift Card as sent or not sent yet.", 
					"System shows shipment status of EGift Card as sent or not sent yet.", 
					"System does not show shipment status of EGift Card as sent or not sent yet.", driver);
			
			//Step-4: Verify the functionality of Gift Card Line Item Components
			Log.softAssertThat(orders.elementLayer.verifyHorizontalAllignmentOfElements(driver, "gcName", "gcItemImage", orders)
							&& orders.elementLayer.verifyHorizontalAllignmentOfElements(driver, "egcName", "egcItemImage", orders),
					"Gift Card Name should be displayed right of Gift Card Image.",
					"Gift Card Name is displayed right of Gift Card Image.",
					"Gift Card Name is not displayed right of Gift Card Image.", driver);
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(orders.elementLayer.verifyHorizontalAllignmentOfElements(driver, "gcQtyDesktop", "gcName", orders)
								&& orders.elementLayer.verifyHorizontalAllignmentOfElements(driver, "egcQtyDesktop", "egcName", orders),
						"Quantity Selected should be displayed right of Gift Card Details",
						"Quantity Selected is displayed right of Gift Card Details",
						"Quantity Selected is not displayed right of Gift Card Details", driver);
				
				Log.softAssertThat(orders.elementLayer.verifyHorizontalAllignmentOfElements(driver, "gcPrice","gcQtyDesktop", orders)
								&& orders.elementLayer.verifyHorizontalAllignmentOfElements(driver, "egcPrice","egcQtyDesktop", orders),
						"Total should be displayed right of Quantity Selected",
						"Total is displayed right of Quantity Selected",
						"Total is not displayed right of Quantity Selected", driver);
			}else {
				Log.softAssertThat(orders.elementLayer.verifyVerticalAllignmentOfElements(driver, "gcName", "gcQtyMobile", orders)
								&& orders.elementLayer.verifyVerticalAllignmentOfElements(driver, "egcName", "egcQtyMobile", orders),
						"Quantity Selected should be displayed below Price",
						"Quantity Selected is displayed below Price",
						"Quantity Selected is not displayed below Price", driver);
				
				Log.softAssertThat(orders.elementLayer.verifyVerticalAllignmentOfElements(driver, "gcQtyMobile", "gcPrice", orders)
								&& orders.elementLayer.verifyVerticalAllignmentOfElements(driver, "egcQtyMobile", "egcPrice", orders),
						"Total should be displayed below the Quantity Selected",
						"Total is displayed below the Quantity Selected",
						"Total is not displayed below the Quantity Selected", driver);
			}
			
			//Step-5: Verify the functionality of Return & Review
			Log.softAssertThat(!(orders.elementLayer.verifyTextContains("divGiftCard", "review", orders) || orders.elementLayer.verifyTextContains("divGiftCard", "return", orders))
							&& !(orders.elementLayer.verifyTextContains("divElectronicGiftCard", "review", orders) || orders.elementLayer.verifyTextContains("divElectronicGiftCard", "return", orders)),
					"System should not display the options to review or return gift cards or electronic gift certificate",
					"System does not display the options to review or return gift cards or electronic gift certificate",
					"System displays the options to review or return gift cards or electronic gift certificate", driver);
			
			//Step-4
			if(orders.elementLayer.verifyElementDisplayed(Arrays.asList("gcItemImageLink"), orders)) {
				BrowserActions.clickOnElementX(driver, "gcItemImageLink", orders, "Gift Card");
				Log.message(i++ + ". Clicked on Gift Card image.", driver);
				PdpPage gcPDP = new PdpPage(driver).get();
				
				Log.softAssertThat(gcPDP.getPageLoadStatus(), 
						"When user clicks on Gift Card Image/Name on the Gift Card Line Item, the user should be taken to the PDP.",
						"The user is taken to PDP",
						"The user is not taken to PDP", driver);
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	} // M1_FBB_DROP5_C22677

}
