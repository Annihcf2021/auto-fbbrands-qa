package com.fbb.testscripts.drop5;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PreApprovedPlatinumCardPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22763 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22763(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String[] account_plcc_preapproved = accountData.get("credential_plcc_preapproved").split("\\|");
		List<String> elementsToBeVerified = null;
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(account_plcc_preapproved[0], account_plcc_preapproved[1]);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			PreApprovedPlatinumCardPage plcc = myAcc.navigateToPreApprovedPlatinumCreditCard();
	
			//step 1 Verify the functionality of Banner
			Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "plccBannerElement", "plccCreditLimit", plcc)
					&& plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "plccCreditLimit", "linkSeeBenefits", plcc), 
					"System should insert the user’s credit limit in the Banner", 
					"System inserted the user’s credit limit in the Banner", 
					"System did not insert the user’s credit limit in the Banner", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementTextContains("plccCreditLimit", "$", plcc), 
					"Credit limit should be displayed beginning with $", 
					"Credit limit is displayed beginning with $", 
					"Credit limit is not displayed beginning with $", driver);
	
			//step 2 Verify the functionality of See Benefits
			Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "plccBannerElement", "linkSeeBenefits", plcc), 
					"See Benefits should be displayed below the Banner", 
					"See Benefits is displayed below the Banner", 
					"See Benefits is not displayed below the Banner", driver);
	
			plcc.clickSeeBenefits();
			elementsToBeVerified = Arrays.asList("divplccLandingPage");	
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"On clicking Cancel button, system should be navigated to the PLCC Landing Page", 
					"System navigated to the PLCC Landing Page.", 
					"System did not navigate to the PLCC Landing Page.", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}//M1_FBB_DROP5_C22763
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP5_C22763(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String[] account_plcc_preapproved = accountData.get("credential_plcc_preapproved").split("\\|");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(account_plcc_preapproved[0], account_plcc_preapproved[1]);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			PreApprovedPlatinumCardPage plcc = myAcc.navigateToPreApprovedPlatinumCreditCard();
	
			//Step 3: Verify the functionality of Information & Rules
			Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "linkSeeBenefits", "divInfoMessage", plcc), 
					"Information and Rules should be located underneath See Benefits.", 
					"Information and Rules is located underneath See Benefits.", 
					"Information and Rules is not located underneath See Benefits.", driver);
	
			//Step 4: Verify the functionality of Form Heading
			if(Utils.isDesktop()) {
				Log.softAssertThat(plcc.elementLayer.verifyHorizontalAllignmentOfElements(driver, "formHeading", "divInfoMessage", plcc), 
						"The form heading should be located on the right side of the page", 
						"The form heading is located on the right side of the page", 
						"The form heading is not located on the right side of the page", driver);
	
				Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "plccBannerElement", "formHeading", plcc), 
						"The form heading should be located underneath the banner.", 
						"The form heading is located underneath the banner.", 
						"The form heading is not located underneath the banner.", driver);
	
				Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "formHeading", "txtSsnNO", plcc), 
						"The form heading should be above the PLCC form.", 
						"The form heading is above the PLCC form.", 
						"The form heading is not above the PLCC form.", driver);
	
			}
			//Mobile and Tablet
			else {
				Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "divInfoMessage", "icoSSNToolTip", plcc), 
						"Form Heading should be displayed below the Information & Rules", 
						"Form Heading is displayed below the Information & Rules", 
						"Form Heading is not displayed below the Information & Rules", driver);
			}
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}//M2_FBB_DROP5_C22763
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP5_C22763(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String[] account_plcc_preapproved = accountData.get("credential_plcc_preapproved").split("\\|");
		List<String> elementsToBeVerified = null;
		String ssn="333224444";
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(account_plcc_preapproved[0], account_plcc_preapproved[1]);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			PreApprovedPlatinumCardPage plcc = myAcc.navigateToPreApprovedPlatinumCreditCard();
	
			//step-5: Verify the functionality of Social Security Number in Form Fields
			Log.softAssertThat(plcc.enterTextOnField("txtSsnNO",ssn.substring(5),"Ssn Number",plcc), 
					"User should be able to enter numbers in the Social Security Number Field", 
					"User is able to enter numbers in the Social Security Number Field", 
					"User is not able to enter text in the Social Security Number Field", driver);
			Log.message("<br>"); 
	
			Log.softAssertThat(plcc.elementLayer.verifyPlaceHolderMovesAbove("txtSsnNO", "ssnplaceholder", ssn, plcc), 
					"Place holder text 'last 4 digits' should be positioned on top when the user starts typing in the text box.", 
					"Place holder text 'last 4 digits' should be positioned on top", 
					"Place holder text 'last 4 digits' should be positioned on top", driver);
	
			//SSN is the mandatory field.
			elementsToBeVerified = Arrays.asList("txtSsnMandatoryError");
			plcc.enterTextOnField("txtSsnNO","","Ssn Number",plcc);
			plcc.clickSubmit();
	
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The Ssn Number Field should be show as mandatory in the Web Instant Credit page", 
					"The Ssn Number is showing as mandatory in the Web Instant Credit page.", 
					"The Ssn Number is not show as mandatory in the Web Instant Credit page.", driver);
	
			//On clicking Tool Tip, System should display the Tool Tip for SSN handled by Content asset ID tips-plcc-ssn
			plcc.clickOnSSNToolTip();
			Log.message(i++ + ". Clicked on Tool tip icon in SSN Lable", driver);
	
			elementsToBeVerified = Arrays.asList("toolTipForPLCC");
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc),
					"On clicking Tool Tip, System should display the Tool Tip for SSN",
					"On clicking Tool Tip, System display the Tool Tip for SSN",
					"On clicking Tool Tip, System not display the Tool Tip for SSN", driver);
	
	
			plcc.clickOnSSNToolTipClose();
			Log.message(i++ + ". Closed SSN Tool tip", driver);
	
			//System should display appropriate error message
			elementsToBeVerified = Arrays.asList("selectMonthMandatoryError","selectDateMandatoryError","selectYearMandatoryError");
	
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The Day , Month , Year  should be show as Mandatory field in the Web Instant Credit page", 
					" The Ssn Number is showing as Optional in the Web Instant Credit page.", 
					" The Ssn Number is not show as mandatory in the Web Instant Credit page.", driver);
	
			//Step 6: Verify the functionality of Birth Date in Form Fields
			//Carat Symbol should be displayed on right of the place holder text positioned downward
	
			//month
			Log.softAssertThat(plcc.elementLayer.verifyCssPropertyForPsuedoElement("selectMonth", ":before", "background-image", "carat-down", plcc), 
					"Carat Symbol for Month should be displayed positioned downward.", 
					"Carat Symbol is displayed positioned downward.", 
					"Carat Symbol is not displayed positioned downward.", driver);
	
			Log.softAssertThat(plcc.verifyandSelectDateElements("month","01"), 
					"System should display the Month (1 to 12) accordingly in the respective order and \n select the Month from the respective drop down", 
					"System is display the Month (1 to 12) accordingly in the respective order select the Month.", 
					"System does not display the Month (1 to 12) accordingly in the respective order and select the Month", driver);
	
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("selectMonth", "spanPlaceHolderMonth", plcc), 
					"Month Placeholder text should move to the top when the User has selected a month", 
					"Month Placeholder text has moved to the top after the User has selected a month.", 
					"Month Placeholder text has not moved to the top.", driver);
	
			//day
			Log.softAssertThat(plcc.elementLayer.verifyCssPropertyForPsuedoElement("selectDay", ":before", "background-image", "carat-down", plcc), 
					"Carat Symbol for day should be displayed positioned downward.", 
					"Carat Symbol is displayed positioned downward.", 
					"Carat Symbol is not displayed positioned downward.", driver);
	
			Log.softAssertThat(plcc.verifyandSelectDateElements("day","15"), 
					"System should display the Day (1 to 31) accordingly in the respective order and User should able to select the value", 
					"System displayed the Day (1 to 31) accordingly in the respective order and User should able to select the value.", 
					"System did not display the Day (1 to 31) accordingly in the respective order and User should able to select the value", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("selectDay", "spanPlaceHolderDay", plcc), 
					"Date Placeholder text should move to the top when the User has selected a date", 
					"Date Placeholder text has moved to the top after the User has selected a date.", 
					"Date Placeholder text has not moved to the top.", driver);
	
			//year
			Log.softAssertThat(plcc.elementLayer.verifyCssPropertyForPsuedoElement("selectYear", ":before", "background-image", "carat-down", plcc), 
					"Carat Symbol for year should be displayed positioned downward.", 
					"Carat Symbol is displayed positioned downward.", 
					"Carat Symbol is not displayed positioned downward.", driver);
	
			Log.softAssertThat(plcc.verifyandSelectDateElements("year","1965"), 
					"System should display the Year accordingly in the respective order and User should able to select the value", 
					"System is display the Year  accordingly in the respective order and User should able to select the value.", 
					"System is not display the Year  accordingly in the respective order and User should able to select the value", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("selectYear", "spanPlaceHolderYear", plcc), 
					"LastName Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"LastName Place holder text is moved to the top portion of the text box when the User starts typing into the field.", 
					"LastName Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}//M3_FBB_DROP5_C22763
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP5_C22763(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String[] account_plcc_preapproved = accountData.get("credential_plcc_preapproved").split("\\|");
		List<String> elementsToBeVerified = null;
		
		String invalidPhoneNumber="1111111111";
		String phone= "2233322555";
		String alternatePhone= "2233322444";
		String errorMsgPhone= demandwareData.get("PhoneError");
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(account_plcc_preapproved[0], account_plcc_preapproved[1]);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			PreApprovedPlatinumCardPage plcc = myAcc.navigateToPreApprovedPlatinumCreditCard();
	
			//step-7: Verify the functionality of Mobile Number in Form Fields
			Log.softAssertThat(plcc.verifyFieldIsPrePopulated("txtphone", plcc), 
					"Platinum Card Page should load with phone number field pre-populated.", 
					"Platinum Card Page loaded with phone number field pre-populated.", 
					"Platinum Card Page did not with phone number field pre-populated.", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtphone",phone,"Phone",plcc), 
					"User should be able to enter number in the Mobile Number", 
					"User is able to enter number in the Mobile Number.", 
					"User is not able to enter tnumber in the Mobile Number", driver);
	
			//System should allow the user to enter only 10 digits
			Log.softAssertThat(plcc.elementLayer.verifyMaximumLengthOfTxtFldsEqualsTo(Arrays.asList("txtMobileNoInPLCCStep2"), 10, plcc),
					"System should allow the user to enter only 10 digits",
					"System allows the user to enter only 10 digits",
					"System not allows the user to enter only 10 digits", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyPlaceHolderMovesAbove("txtMobileNoInPLCCStep2", "phoneplaceholder", phone, plcc), 
					"Place holder text \"Mobile Number\" should move to the top portion of the form field when the user starts typing in the Mobile Number field", 
					"Place holder text moved to the top portion of the text box when the User starts typing into the field.", 
					"Place holder text did not move to the top portion of the text box when the User starts typing into the field", driver);
	
			plcc.enterTextOnField("txtphone","","Phoner Number",plcc);
			plcc.clickSubmit();
			elementsToBeVerified = Arrays.asList("txtPhoneMandatoryError");
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"If user tries to leave the Mobile number field blank or if any incorrect data format is entered, then appropriate error message should be displayed", 
					"Appropriate error message is displayed.", 
					"Appropriate error message is not displayed", driver);
	
			//step-8: Verify the functionality of Alternate Phone Number in Form Fields
			elementsToBeVerified = Arrays.asList("selectAlternativePhoneOptionalTxt");
	
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The Alternative phone number Field should be show as Optional in the Web Instant Credit page", 
					"The Alternative phone number is showing as Optional in the Web Instant Credit page.", 
					"The Alternative phone number is not show as mandatory in the Web Instant Credit page.", driver);
	
			Log.softAssertThat(plcc.verifyFieldErrorMessages(invalidPhoneNumber, errorMsgPhone,"AlternatePhoneNumber"), 
					"System should display an appropriate error message for Incorrect format of data enter into the Alternative phone number", 
					"System is displayed an appropriate error message for Incorrect format of data enter into the Alternative phone number.", 
					"System is not display an appropriate error message for Incorrect format of data enter into the Alternative phone number", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtAlternativePhone",invalidPhoneNumber, "Alternate Phone Number",plcc), 
					"User should be able to enter text in the Alternate Phone Number text box", 
					"User is able to enter text in the Alternate Phone Number text box.", 
					"User is not able to enter text in the Alternate Phone Number text box", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyPlaceHolderMovesAbove("txtAlternativePhone", "alternativePhoneplaceholder", alternatePhone, plcc), 
					"Alternate Phone Number Place holder text should move to top when the User starts typing into the field", 
					"Alternate Phone Number Place holder text is moved to the top when the User starts typing into the field.", 
					"Alternate Phone Number Place holder text is not moved to the top when the User starts typing into the field", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}//M4_FBB_DROP5_C22763
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M5_FBB_DROP5_C22763(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String[] account_plcc_preapproved = accountData.get("credential_plcc_preapproved").split("\\|");
		List<String> elementsToBeVerified = null;
		
		String firstName="firstName";
		String lastName="LastName";
		String invalidInput="##@!";
		String zipCode="66063";
		String invalidAddress="Address";
		
		String invalidFirstNameError= demandwareData.get("FirstNameError");
		String invalidLastNameError= demandwareData.get("LastNameError");
		String invalidEmailErrorMsg= demandwareData.get("EmailError");
		String invalidzipcodeMessage= demandwareData.get("ZIPError");
		
		String random=RandomStringUtils.randomNumeric(5);
		String txtEmail="automationdrop5"+random+"@yopmail.com";
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(account_plcc_preapproved[0], account_plcc_preapproved[1]);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			PreApprovedPlatinumCardPage plcc = myAcc.navigateToPreApprovedPlatinumCreditCard();
	
			//step-9 Verify the functionality of Edit Button (Personal Information)
			plcc.clickEditProfile();
	
			//first Name
			elementsToBeVerified = Arrays.asList("txtFirstNameMandatoryError");
	
			Log.softAssertThat(plcc.verifyFieldIsPrePopulated("txtFirstName", plcc), 
					"Edit personal information should open with first name field pre-populated.", 
					"Edit personal information opened with first name field pre-populated.", 
					"Edit personal information did not open with first name field pre-populated.", driver);
	
			plcc.enterTextOnField("txtFirstName","","Empty First Name",plcc);
			plcc.clickSubmit();
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The First Name Field should be shown as mandatory in the Web Instant Credit page", 
					"The First Name is showing as mandatory in the Web Instant Credit page.", 
					"The First Name is not show as mandatory in the Web Instant Credit page.", driver);
	
			Log.softAssertThat(plcc.verifyFieldErrorMessages(invalidInput, invalidFirstNameError, "FirstName"), 
					"System should display an appropriate error message for Incorrect format of data enter into the FirstName", 
					"System displayed an appropriate error message for Incorrect format of data enter into the FirstName.", 
					"System did not display an appropriate error message for Incorrect format of data enter into the FirstName", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtFirstName",firstName,"First Name",plcc), 
					"User should be able to enter text in the First Name text box", 
					"User is able to enter text in the First Name text box.", 
					"User is not able to enter text in the First Name text box", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtFirstName", "spanPlaceHolderFirstName", plcc), 
					"FirstName Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"FirstName Place holder text is moved to the top portion of the text box when the User starts typing into the field.", 
					"FirstName Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			// lastName
			elementsToBeVerified = Arrays.asList("txtLastNameMandatoryError");
	
			Log.softAssertThat(plcc.verifyFieldIsPrePopulated("txtLastName", plcc), 
					"Edit personal information should open with last name field pre-populated.", 
					"Edit personal information opened with last name field pre-populated.", 
					"Edit personal information did not open with last name field pre-populated.", driver);
	
			plcc.enterTextOnField("txtLastName","","Empty Last Name",plcc);
			plcc.clickSubmit();
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The Last Name Field should be show as mandatory in the Web Instant Credit page", 
					"The Last Name is showing as mandatory in the Web Instant Credit page.", 
					"The Last Name is not show as mandatory in the Web Instant Credit page.", driver);
	
			Log.softAssertThat(plcc.verifyFieldErrorMessages(invalidInput, invalidLastNameError, "LastName"), 
					"System should display an appropriate error message for Incorrect format of data enter into the Last Name", 
					"System displayed appropriate error message for Incorrect format of data enter into the Last Name.", 
					"System did not display an appropriate error message for Incorrect format of data enter into the Last Name", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtLastName",lastName,"Last name",plcc), 
					"User should be able to enter text in the Last Name text box", 
					"User is able to enter text in the Last Name text box.", 
					"User is not able to enter text in the Last Name text box", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtLastName", "spanPlaceHolderLastName", plcc), 
					"LastName Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"LastName Place holder text is moved to the top portion of the text box when the User starts typing into the field.", 
					"LastName Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//**************************************				
			//AddressLine
			elementsToBeVerified = Arrays.asList("txtAddressOneMandatoryError");
	
			Log.softAssertThat(plcc.verifyFieldIsPrePopulated("txtaddressOne", plcc), 
					"Edit personal information should open with Address Line 1 field pre-populated.", 
					"Edit personal information opened with Address Line 1 field pre-populated.", 
					"Edit personal information did not open with Address Line 1 field pre-populated.", driver);
	
			plcc.enterTextOnField("txtaddressOne","","Emplty Address Line 1",plcc);
			plcc.clickSubmit();
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The Address line 1 Field should be show as mandatory in the Create Account page", 
					"Address line 1 is showing as mandatory in the Create Account page.", 
					"Address line 1 is not show as mandatory in the Create Account page.", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtaddressOne",invalidAddress,"Address 1",plcc), 
					"User should be able to edit and enter text in the Address Line", 
					"User is able to edit and enter text in the Address Line.", 
					"User is not able to edit and enter text in the Address Line", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtaddressOne", "spanPlaceHolderAddressOne", plcc), 
					"Email Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"Email Place holder text is moved to the top portion of the text box when the User starts typing into the field.", 
					"Email Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//State
			elementsToBeVerified = Arrays.asList("selectStateMandatoryError");
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("selectStateDrop", "spanPlaceHolderState", plcc), 
					"User's State should be pre-selected", 
					"User's State is pre-selected.", 
					"User's State is not pre-selected", driver);
	
			plcc.selectMonth("Select...");
			plcc.clickSubmit();
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The State Field should be show as mandatory in the Web Instant Credit page", 
					"The State is showing as mandatory in the Web Instant Credit page.", 
					"The State is not show as mandatory in the Web Instant Credit page.", driver);
	
			Log.softAssertThat(plcc.selectMonth("Ohio"), 
					"User should be able to select a value from the State drop down", 
					"User can be able to select a value from the State drop down.", 
					"User is not able to select a value from the State drop down", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("selectStateDrop", "spanPlaceHolderState", plcc), 
					"State Place holder text should move to the top portion of the text box when the User selects a State", 
					"State Place holder text is moved to the top portion of the text box when the User has selected a State.", 
					"State Place holder text is not moved to the top portion of the text box when the User has selected a State", driver);
	
			//zipcode
			elementsToBeVerified = Arrays.asList("txtzipcodeMandatoryError");
	
			Log.softAssertThat(plcc.verifyFieldIsPrePopulated("txtZipCode", plcc), 
					"Edit personal information should open with Zip Code field pre-populated.", 
					"Edit personal information opened with Zip Code field pre-populated.", 
					"Edit personal information did not open with Zip Code field pre-populated.", driver);
	
			plcc.enterTextOnField("txtZipCode","","Empty ZIP code",plcc);
			plcc.clickSubmit();
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"The Zip code Field should be show as mandatory in the Web Instant Credit page", 
					"The Zip code is showing as mandatory in the Web Instant Credit page.", 
					"The Zip code is not show as mandatory in the Web Instant Credit page.", driver);
	
			Log.softAssertThat(plcc.verifyFieldErrorMessages(invalidInput,invalidzipcodeMessage,"zipCode"), 
					"System should display an appropriate error message for Incorrect format of data enter into the Zip code", 
					"System is displayed an appropriate error message for Incorrect format of data enter into the Zip code.", 
					"System is not display an appropriate error message for Incorrect format of data enter into the Zip code", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtZipCode",zipCode,"zipcode",plcc), 
					"User should be able to enter text in the Zip code text box", 
					"User is able to enter text in the zip code text box.", 
					"User is not able to enter text in the zip code text box", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtZipCode", "spanPlaceHolderZipcode", plcc), 
					"Zipcode Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"Zipcode Place holder text is moved to the top portion of the text box when the User starts typing into the field.", 
					"Zipcode Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			//Email Address
			elementsToBeVerified = Arrays.asList("selectEmailOptionalTxt");
	
			Log.softAssertThat(plcc.verifyFieldIsPrePopulated("txtEmail", plcc), 
					"Edit personal information should open with Email field pre-populated.", 
					"Edit personal information opened with Email field pre-populated.", 
					"Edit personal information did not open with Email field pre-populated.", driver);
	
			Log.message("According to ADS implementation guide and API documentation, email address is optional."); 
	
			Log.softAssertThat(plcc.verifyFieldErrorMessages(invalidInput, invalidEmailErrorMsg,"Email"), 
					"System should display an appropriate error message for Incorrect format of data enter into Email", 
					"System is displayed an appropriate error message for Incorrect format of data enter into Email", 
					"System is not display an appropriate error message for Incorrect format of data enter into Email", driver);
	
			Log.softAssertThat(plcc.enterTextOnField("txtEmail",txtEmail,"Email",plcc), 
					"User should be able to enter text in the Zip code text box", 
					"User is able to enter text in the zip code text box.", 
					"User is not able to enter text in the zip code text box", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyElementDisplayedBelow("txtEmail", "spanPlaceHolderEmail", plcc), 
					"LastName Place holder text should move to the top portion of the text box when the User starts typing into the field", 
					"LastName Place holder text is moved to the top portion of the text box when the User starts typing into the field.", 
					"LastName Place holder text is not moved to the top portion of the text box when the User starts typing into the field", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}//M5_FBB_DROP5_C22763
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M6_FBB_DROP5_C22763(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String[] account_plcc_preapproved = accountData.get("credential_plcc_preapproved").split("\\|");
		List<String> elementsToBeVerified = null;
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(account_plcc_preapproved[0], account_plcc_preapproved[1]);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			PreApprovedPlatinumCardPage plcc = myAcc.navigateToPreApprovedPlatinumCreditCard();
	
			//Step-10: Verify the functionality of Phone Contact Disclaimer 
			//Implemented below
			Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "divPlccFormSection", "phoneContactDisclaimer", plcc)
							|| plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "divPlccFormSection", "phoneContactDisclaimerAlt", plcc),
					"Phone Contact Disclaimer should be located below the SSN and DoB forms",
					"Phone Contact Disclaimer is located below the SSN and DoB forms",
					"Phone Contact Disclaimer is not located below the SSN and DoB forms", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "phoneContactDisclaimer", "accountTerms", plcc)
							|| plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "phoneContactDisclaimerAlt", "accountTerms", plcc),
					"Phone Contact Disclaimer should be located above the Consent to Account terms",
					"Phone Contact Disclaimer is located above the Consent to Account term",
					"Phone Contact Disclaimer is not located above the Consent to Account term", driver);
	
			//Step-11 Verify the functionality of Consent to Account Terms		
			String tagname=plcc.elementLayer.getTagName("consenttoAccount", plcc);
			Log.softAssertThat(tagname.equals("iframe"),					 
					"consent to Account should be am iframe element", 
					"consent to Account is an iframe element", 
					"consent to Account does not display inside iframe", driver);
	
			//Step-12&13: Verify the functionality of Consent to Financial Terms, Credit Card Disclaimer
			tagname=plcc.elementLayer.getTagName("consenttoFinance", plcc);
			Log.softAssertThat(tagname.equals("iframe"),					 
					"Consent to Financial Terms should be am iframe element", 
					"Consent to Financial Terms is an iframe element", 
					"Consent to Financial Terms doesnot display inside iframe", driver);
	
			Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "consenttoAccount", "creditcardDisclaimer", plcc),					 
					"Credit Card Disclaimer should be located under the Consent to Financial Terms iframe.", 
					"Credit Card Disclaimer is located under the Consent to Financial Terms iframe. ", 
					"Credit Card Disclaimer is not located under the Consent to Financial Terms iframe. ", driver);
	
			//Step-14: Verify the functionality of Consent Checkbox	
			if(Utils.isDesktop()) {
				Log.softAssertThat(plcc.elementLayer.verifyVerticalAllignmentOfElements(driver, "creditcardDisclaimer", "consentChkBox", plcc),					 
						"The Consent Checkbox should be located underneath the Credit Card disclaimer.", 
						"The Consent Checkbox is located underneath the Credit Card disclaimer.", 
						"The Consent Checkbox is not located underneath the Credit Card disclaimer.", driver);
			}else {
				Log.softAssertThat(plcc.elementLayer.verifyCssPropertyForElement("divBottomButtons", "position", "fixed", plcc),					 
						"The Consent Checkbox should be located underneath the Credit Card disclaimer.", 
						"The Consent Checkbox is located underneath the Credit Card disclaimer.", 
						"The Consent Checkbox is not located underneath the Credit Card disclaimer.", driver);
			}
			
	
			elementsToBeVerified = Arrays.asList("consentChkBox");					
			Log.softAssertThat(!(plcc.elementLayer.verifyPageElementsChecked(elementsToBeVerified, plcc)),					 
					"Credit Card Disclaimer should not be clickable/editable", 
					"Credit Card Disclaimer is not clickable/editable", 
					"Credit Card Disclaimer is clickable/editable", driver);
	
			//Step-15: Verify the functionality of Cancel Button
			plcc.clickCancelbtn();
			elementsToBeVerified = Arrays.asList("divplccLandingPage");	
			Log.softAssertThat(plcc.elementLayer.verifyPageElements(elementsToBeVerified, plcc), 
					"On clicking Cancel button, system should be navigated to the PLCC Landing Page", 
					"System navigated to the PLCC Landing Page.", 
					"System did not navigate to the PLCC Landing Page.", driver);
			
			plcc.clickOnApplyButton();
			elementsToBeVerified = Arrays.asList("consentChkBox");
			Log.softAssertThat(!plcc.verifyFieldIsPrePopulated("ssnInputText", plcc)
							&& !plcc.verifyFieldIsPrePopulated("txtAlternativePhone", plcc)
							//&& !plcc.elementLayer.verifyAttributeForElement("selectCheckboxMandatory", "outerHTML", "checked", plcc)
							&& !plcc.elementLayer.verifyPageElementsChecked(elementsToBeVerified, plcc),
					"System should have cleared any user entered values in the Form Fields and Consent Checkbox.", 
					"System have cleared user entered values in the Form Fields and Consent Checkbox.", 
					"System have not cleared user entered values in the Form Fields and Consent Checkbox.", driver);
	
			//Step-16: Verify the functionality of Submit Button
			Log.message("Step covered on TC22641");
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}//M6_FBB_DROP5_C22763

}// search
