package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PreApprovedAppPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22644 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, priority = 1, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22644(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String credential = accountData.get("plcc_preapproved_" + Utils.getCurrentBrandShort().substring(0, 2));
		String username = credential.split("\\|")[0];
		String password = credential.split("\\|")[1];
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAccount = homePage.headers.navigateToMyAccount(username, password); //Navigate to My Account Page
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			//Step 1: Verify the display and functionality of Content Slot
			Brand brand = Utils.getCurrentBrand();
			Log.softAssertThat(myAccount.elementLayer.verifyElementDisplayed(Arrays.asList("imgCreditPLCCHolder"), myAccount),
					"To check 'PLCC Card Holder' brand image is displayed.",
					"'PLCC Card Holder' brand image is displayed!",
					"'PLCC Card Holder' brand image is not displayed", driver);
	
			Log.softAssertThat(myAccount.verifyPreApprovedTextPresentForApprovedUser(),
					"To check 'Content Copy' is displayed.",
					"'Content Copy' text is displayed.!",
					"'Content Copy' text is not displayed", driver);
	
			Log.softAssertThat(myAccount.verifyAcceptThisOfferLinkPresentForApprovedUser(),
					"To check 'Accept This Offer' is displayed.",
					"'Accept This Offer' link is displayed.!",
					"'Accept This Offer' link is not displayed", driver);
	
			Log.softAssertThat(myAccount.verifyPLCCContentSlotAlignedCenter(),
					"To check PLCC content slot is aligned center.",
					"PLCC content slot is aligned center.!",
					"PLCC content slot is not aligned center", driver);
	
			PreApprovedAppPage preApprovedPg = myAccount.clickAcceptThisOfferLink();
			Log.softAssertThat(preApprovedPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), preApprovedPg),
					"To check 'Pre Approved App' Page is displayed.",
					"'Pre Approved App' Page is displayed.!",
					"'Pre Approved App' Page is not displayed", driver);
	
			Log.reference("The test case is referenced to C22767, which is not in automation scope");
			Log.softAssertThat(brand.equals(Utils.getCurrentBrand()),
					"To check current brand maintained through out the session.",
					"Current brand maintained through out the session.!",
					"Current brand is not maintained through out the session", driver);
	
			if(Utils.getRunBrowser(driver).toLowerCase().equals("safari")) {
				if(!Utils.isMobile()) {
					String url = homePage.headers.getSignOutUrl();
					driver.get(url);
					Utils.waitForPageLoad(driver);
				}
			} 
			else{
				homePage.headers.signOut();
				Log.message(i++ + ". Clicked on sign out!", driver);
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// TC_FBB_DROP4_C22644

}// search
