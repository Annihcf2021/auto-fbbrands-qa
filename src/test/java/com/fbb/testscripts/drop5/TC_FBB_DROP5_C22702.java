package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.EmailPreferencePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22702 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22702(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		int indexCurrentBrand = 1;
		int indexOtherBrand = 2;
		
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, email + "|" + password);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			MyAccountPage myAcc = headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged in to user account.", driver);
			
			EmailPreferencePage emailPrefs = myAcc.clickOnEmailPrefLink();
			Log.message(i++ + ". Navigated to Email preference page.", driver);
			
			//Step-1: Verify the functionality of Headline
			Log.softAssertThat(emailPrefs.verifySubscribeLabelOfTheBrands(indexCurrentBrand-1, "Subscribed"),
					"Label of the respective brand should say Subscribed", 
					"Label of the respective brand is Subscribed", 
					"Label of the respective brand is not Subscribed", driver);
			
			emailPrefs.clickSubscriptionSlider(indexCurrentBrand);
			Log.message(i++ + ". Clicked unsubscribe for current brand.", driver);
			
			Log.softAssertThat(emailPrefs.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("modalEmailFrequency"), emailPrefs), 
					"Unsubscribe modal should be displayed.", 
					"Unsubscribe modal is displayed.", 
					"Unsubscribe modal is not displayed.", driver);
			
			Log.softAssertThat(emailPrefs.elementLayer.verifyVerticalAllignmentOfElementsWithoutPadding(driver, "modalEmailFrequency", "modalHeader", emailPrefs)
							&& emailPrefs.elementLayer.verifyVerticalAllignmentOfElements(driver, "modalHeader", "divFrequencyOptions", emailPrefs), 
					"Headline should be displayed on top of the Email Frequency Modal", 
					"Headline is displayed on top of the Email Frequency Modal", 
					"Headline is not displayed on top of the Email Frequency Modal", driver);
			
			//Step-2: Verify the functionality of Email Frequency Options
			Log.softAssertThat(emailPrefs.elementLayer.verifyVerticalAllignmentOfElements(driver, "modalHeader", "radioFewerContact", emailPrefs)
							&& emailPrefs.elementLayer.verifyVerticalAllignmentOfElements(driver, "modalHeader", "chkUnsubscribe", emailPrefs), 
					"Email Frequency Options radio buttons should be displayed below the Headline", 
					"Email Frequency Options radio buttons are displayed below the Headline", 
					"Email Frequency Options radio buttons are not displayed below the Headline", driver);
			
			Log.softAssertThat(emailPrefs.elementLayer.verifyRadioButtonSelection("radioFewerContact", true, emailPrefs), 
					"\"Email less frequently\" option should be selected by default.", 
					"\"Email less frequently\" option is selected by default.", 
					"\"Email less frequently\" option is not selected by default.", driver);
			
			emailPrefs.selectRadioOnUnsubscribeModal("none");
			Log.message(i++ + ". Clicked on 'Unsubscribe'.", driver);
			
			Log.softAssertThat(emailPrefs.elementLayer.verifyRadioButtonSelection("chkUnsubscribe", true, emailPrefs)
							&& emailPrefs.elementLayer.verifyRadioButtonSelection("radioFewerContact", false, emailPrefs), 
					"The User should be able to select any one of the Email Frequency Options by clicking on the radio buttons", 
					"The User is able to select any one of the Email Frequency Options by clicking on the radio buttons", 
					"The User is not able to select any one of the Email Frequency Options by clicking on the radio buttons", driver);
			
			Log.softAssertThat(emailPrefs.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divUnsubscribeReasons"), emailPrefs), 
					"On selecting the Unsubscribe radio button, System should display the reason options with dropdown animation", 
					"System displayed the reason options with dropdown animation", 
					"System did not display the reason options with dropdown animation", driver);
			
			emailPrefs.selectRadioOnUnsubscribeModal("less");
			Log.message(i++ + ". Clicked on 'Please email me less frequently'");
			
			Log.softAssertThat(!emailPrefs.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divUnsubscribeReasons"), emailPrefs), 
					"Again on unselecting the Unsubscribe radio button, System should hide the reason options.", 
					"System hid the reason options.", 
					"System did not hide the reason options.", driver);
			
			//Step-3: Verify the functionality of Close Modal
			BrowserActions.clickOnElementX(driver, "btnCloseModal", emailPrefs, "Close button.");
			Log.message(i++ + ". Clicked close on modal.", driver);
			
			Log.softAssertThat(emailPrefs.verifySubscribedStatus(indexCurrentBrand-1), 
					"No changes should be made on email subscription state.", 
					"No changes are made on email subscription state.", 
					"Changes are made on email subscription state.", driver);
			
			emailPrefs.clickSubscriptionSlider(indexCurrentBrand);
			Log.message(i++ + ". Clicked unsubscribe for current brand.", driver);
			
			emailPrefs.clickOutSideOfEmailPreferenceModal();
			Log.message(i++ + ". Clicked Out side of the email prequency modal.", driver);
			
			Log.softAssertThat(!emailPrefs.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("modalEmailFrequency"), emailPrefs), 
					"Clicking anywhere outside of the modal should navigate back to the Email Preference page", 
					"System navigated back to the Email Preference page", 
					"System did not navigate back to the Email Preference page", driver);
			
			Log.softAssertThat(emailPrefs.verifySubscribedStatus(indexCurrentBrand-1), 
					"No changes should be made on email subscription state.", 
					"No changes are made on email subscription state.", 
					"Changes are made on email subscription state.", driver);
			
			//Step-4: Verify the functionality of Unsubscribe Options
			emailPrefs.clickSubscriptionSlider(indexCurrentBrand);
			Log.message(i++ + ". Clicked unsubscribe for current brand.", driver);
			
			emailPrefs.selectRadioOnUnsubscribeModal("none");
			Log.message(i++ + ". Clicked on 'Unsubscribe'.", driver);
			
			Log.softAssertThat(emailPrefs.verifyUnsubReasonRadio(emailPrefs, 1), 
					"User should be able to select any one of the reason by clicking on the respective radio button", 
					"User is able to select any one of the reason by clicking on the respective radio button", 
					"User is not able to select all of the reason by clicking on the respective radio button", driver);
			
			Log.softAssertThat(emailPrefs.verifyUnsubReasonRadio(emailPrefs, 1, 2), 
					"System should not allow to user to select more than one reason for unsubscribing", 
					"System does not allow user to select more than one reason for unsubscribing", 
					"System allows user to select more than one reason for unsubscribing", driver);
			
			Log.softAssertThat(emailPrefs.elementLayer.verifyVerticalAllignmentOfElements(driver, "radioUnsubOtherReason", "txtOtherReason", emailPrefs), 
					"Text box should be displayed below the 'Others' option (Please explain below) option", 
					"Text box is displayed below the 'Others' option (Please explain below) option", 
					"Text box is not displayed below the 'Others' option (Please explain below) option", driver);
			
			Log.softAssertThat(!emailPrefs.verifyOtherReasonTextboxEnabled(), 
					"The text box below the 'Others' option should be disabled when user has selected any one of the available reason except Others.", 
					"The text box below the 'Others' option is disabled", 
					"The text box below the 'Others' option is not disabled", driver);
			
			BrowserActions.clickOnElementX(driver, "radioUnsubOtherReason", emailPrefs, "Other(Please Explain Below)");
			Log.message(i++ + ". Clicked on Other", driver);
			
			Log.softAssertThat(emailPrefs.verifyOtherReasonTextboxEnabled(), 
					"The System should allow the User to enter data into the text box when the 'Others' option is selected", 
					"The System allows the User to enter data", 
					"The System does not allow the User to enter data", driver);
			
			//Step-5: Verify the functionality of Submit
			emailPrefs.selectRadioOnUnsubscribeModal("none");
			Log.message(i++ + ". Clicked on 'Unsubscribe'.", driver);
			
			emailPrefs.verifyUnsubReasonRadio(1);
			Log.message(i++ + ". Selected Unsuscribe reason", driver);
			
			BrowserActions.clickOnElementX(driver, "btnReasonForUnsubscribeSubmit", emailPrefs, "Submit button");
			Log.message(i++ + ". Clicked on Submit button", driver);
			
			Log.softAssertThat(!emailPrefs.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("modalEmailFrequency"), emailPrefs), 
					"Modal should be closed.", 
					"Modal is closed.", 
					"Modal is not closed.", driver);
			
			Log.softAssertThat(emailPrefs.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divPreferenceOptions"), emailPrefs), 
					"User should be unsubscribed from email.", 
					"User is unsubscribed from email.", 
					"User is not unsubscribed from email.", driver);
			
			emailPrefs.clickSubscriptionSlider(indexOtherBrand);
			Log.message(i++ + ". Clicked subscribe for sister brand.", driver);
			
			Log.softAssertThat(emailPrefs.verifySubscribeLabelOfTheBrands(indexOtherBrand-1, "Subscribed"),
					"Label of the respective brand should change to Subscribed", 
					"Label of the respective brand is changed to Subscribed", 
					"Label of the respective brand is not changed to Subscribed", driver);
			
			emailPrefs.clickSubscriptionSlider(indexOtherBrand);
			Log.message(i++ + ". Clicked unsubscribe for sister brand.", driver);
			
			emailPrefs.selectRadioOnUnsubscribeModal("none");
			Log.message(i++ + ". Clicked on 'Unsubscribe'.", driver);
			
			emailPrefs.verifyUnsubReasonRadio(indexOtherBrand-1);
			Log.message(i++ + ". Selected Unsuscribe reason", driver);
			
			BrowserActions.clickOnElementX(driver, "btnReasonForUnsubscribeSubmit", emailPrefs, "Submit button");
			Log.message(i++ + ". Clicked on Submit button", driver);
			
			Log.softAssertThat(emailPrefs.verifySubscribeLabelOfTheBrands(indexOtherBrand-1, "Subscribe"),
					"Label of the respective brand should change to Subscribe", 
					"Label of the respective brand is changed to Subscribe", 
					"Label of the respective brand is not changed to Subscribe", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.resetSubscriptionStatus(driver);
			Log.endTestCase(driver);
		} // finally
	} // M1_FBB_DROP5_C22702

}
