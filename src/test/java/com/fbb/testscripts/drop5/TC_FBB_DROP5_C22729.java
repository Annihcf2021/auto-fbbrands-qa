package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlatinumCardApplication;
import com.fbb.pages.PlatinumCardLandingPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.PreApprovedPlatinumCardPage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22729 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22729(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		//Load test data
		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		String credentialsPreApproved = accountData.get("plcc_preapproved_" + Utils.getCurrentBrandShort().substring(0, 2));
		String comenityURL = redirectData.get("comenity_manage") + Utils.getCurrentBrand().getConfiguration().toLowerCase();
		
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, email + "|" + password);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Footers footers = homePage.footers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			PlatinumCardLandingPage platinumLanding = footers.navigateToPlatinumCreditCardLanding();
			Log.message(i++ + ". Navigated to PLCC landing page.", driver);
			
			//Step-1: Verify the display of components available in from Unauthenticated User Landing page.
			if(!Utils.isDesktop()) {
				Log.softAssertThat(platinumLanding.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("applyButton", "lnkSignInCompleteMobile", "lnkManageAccount"), platinumLanding), 
						"Unauthenticated User Landing Page should be displayed with the correct components.", 
						"Unauthenticated User Landing Page is displayed with the correct components.", 
						"Unauthenticated User Landing Page is not displayed with the correct components.", driver);
			}else {
				Log.softAssertThat(platinumLanding.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("applyButton", "lnkSignInCompleteDesktop", "lnkManageAccount"), platinumLanding), 
						"Unauthenticated User Landing Page should be displayed with the correct components.", 
						"Unauthenticated User Landing Page is displayed with the correct components.", 
						"Unauthenticated User Landing Page is not displayed with the correct components.", driver);
			}
			
			//Step-2: Verify the functionality of Apply Now button
			if(!Utils.isDesktop()) {
				Log.softAssertThat(platinumLanding.elementLayer.verifyVerticalAllignmentOfElements(driver, "applyButton", "lnkSignInCompleteMobile", platinumLanding)
						&& platinumLanding.elementLayer.verifyVerticalAllignmentOfElements(driver, "applyButton", "lnkManageAccount", platinumLanding), 
						"Apply Now button should be displayed above \"Sign In to Complete\" and \"Manage Your Account\" ", 
						"Apply Now button is displayed above \"Sign In to Complete\" and \"Manage Your Account\" ", 
						"Apply Now button is not displayed above \"Sign In to Complete\" and \"Manage Your Account\" ", driver);
			}else {
				Log.softAssertThat(platinumLanding.elementLayer.verifyVerticalAllignmentOfElements(driver, "applyButton", "lnkSignInCompleteDesktop", platinumLanding)
						&& platinumLanding.elementLayer.verifyVerticalAllignmentOfElements(driver, "applyButton", "lnkManageAccount", platinumLanding), 
						"Apply Now button should be displayed above \"Sign In to Complete\" and \"Manage Your Account\" ", 
						"Apply Now button is displayed above \"Sign In to Complete\" and \"Manage Your Account\" ", 
						"Apply Now button is not displayed above \"Sign In to Complete\" and \"Manage Your Account\" ", driver);
			}
			
			
			if(Utils.isDesktop()){
				Log.softAssertThat(platinumLanding.elementLayer.verifyHorizontalAllignmentOfElements(driver, "applyButton", "lnkSignInCompleteDesktop", platinumLanding)
						&& platinumLanding.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkManageAccount", "applyButton", platinumLanding), 
						"Apply button should be in a center position.", 
						"Apply button is in a center position.", 
						"Apply button is not in a center position.", driver);
			}
			if(Utils.isTablet()){
				Log.softAssertThat(platinumLanding.elementLayer.verifyHorizontalAllignmentOfElements(driver, "applyButton", "lnkSignInCompleteMobile", platinumLanding)
						&& platinumLanding.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkManageAccount", "applyButton", platinumLanding), 
						"Apply button should be in a center position.", 
						"Apply button is in a center position.", 
						"Apply button is not in a center position.", driver);
			}
			
			PlatinumCardApplication platinumApplication = platinumLanding.clickOnApplyButton();
			Log.message(i++ + ". Clicked on apply button", driver);
			
			Log.softAssertThat(platinumApplication.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("readyElement"), platinumApplication), 
					"On clicking Apply Now, user should be navigated to the PLCC Application page (Web Instant Credit module)", 
					"User is navigated to the PLCC Application page (Web Instant Credit module)", 
					"User is not navigated to the PLCC Application page (Web Instant Credit module)", driver);
			
			driver.navigate().back();
			Log.message(i++ + ". Navigated back to PLCC landing page", driver);
			
			//Step-4: Verify the functionality of Manage Your Account after authentication
			Log.softAssertThat(platinumLanding.elementLayer.verifyAttributeForElement("lnkManageAccount", "target", "_blank", platinumLanding)
							&& platinumLanding.elementLayer.verifyAttributeForElement("lnkManageAccount", "href", comenityURL, platinumLanding), 
					"On clicking the Manage Your Account, System should open link to Comenity's site based on brand in a new tab", 
					"System opened link to Comenity's site based on brand in a new tab", 
					"System did not open link to Comenity's site based on brand in a new tab", driver);
			
			if(Utils.isDesktop()) {
				Log.softAssertThat(platinumLanding.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkManageAccount", "lnkSignInCompleteDesktop", platinumLanding), 
						"Manage Your Account should be displayed right of the Sign In To Complete link", 
						"Manage Your Account is displayed right of the Sign In To Complete link", 
						"Manage Your Account is not displayed right of the Sign In To Complete link", driver);
			}
			if(Utils.isTablet()) {
				Log.softAssertThat(platinumLanding.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkManageAccount", "lnkSignInCompleteMobile", platinumLanding), 
						"Manage Your Account should be displayed right of the Sign In To Complete link", 
						"Manage Your Account is displayed right of the Sign In To Complete link", 
						"Manage Your Account is not displayed right of the Sign In To Complete link", driver);
			}
			if(Utils.isMobile()) {
				Log.softAssertThat(platinumLanding.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkManageAccount", "lnkSignInCompleteMobile", platinumLanding), 
						"Manage Your Account should be displayed right of the Sign In To Complete link", 
						"Manage Your Account is displayed right of the Sign In To Complete link", 
						"Manage Your Account is not displayed right of the Sign In To Complete link", driver);
			}
			
			Log.softAssertThat(platinumLanding.verifyManageYourAccount(comenityURL), 
					"On clicking the Manage Your Account, System should open link to Comenity's site based on brand in a new tab", 
					"System opened link to Comenity's site based on brand in a new tab", 
					"System did not open link to Comenity's site based on brand in a new tab", driver);
			
			//Step-3: Verify the functionality of Sign In To Complete link
			SignIn signIn = platinumLanding.clickSignInToComplete();
			Log.message(i++ + ". Clicked on Sign In To Complete.", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("readyElement"), signIn), 
					"On clicking Sign In To Complete, user should be navigated to the My Account Sign-In page", 
					"User is navigated to the My Account Sign-In page", 
					"User is not navigated to the My Account Sign-In page", driver);
			
			//Step 3 sub sections  - Log in as a pre-approved user
			signIn.typeOnEmail(credentialsPreApproved.split("\\|")[0]);
			signIn.typeOnPassword(credentialsPreApproved.split("\\|")[1]);
			signIn.clickSignInButton();
			Log.message(i++ + ". Logged in as pre-approved user.", driver);
			
			PreApprovedPlatinumCardPage plccPreApproved = new PreApprovedPlatinumCardPage(driver).get();
			Log.softAssertThat(plccPreApproved.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnEditProfile"), plccPreApproved), 
					"If the user is Pre-Approved, the system should navigate to the Pre-Approved Form", 
					"The system navigated to the Pre-Approved Form", 
					"The system did not navigate to the Pre-Approved Form", driver);
			
			//Step 3 sub sections  - Log in as a non pre-approved user
			headers.signOut();
			Log.message(i++ + ". Signed out.", driver);
			
			homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			platinumLanding = footers.navigateToPlatinumCreditCardLanding();
			Log.message(i++ + ". Navigated to PLCC landing page.", driver);
			
			signIn = platinumLanding.clickSignInToComplete();
			Log.message(i++ + ". Clicked on Sign In To Complete.", driver);
			
			signIn.typeOnEmail(email);
			signIn.typeOnPassword(password);
			Log.message(i++ + ". Filled in credentilas.", driver);
			signIn.clickSignInButton();
			Log.message(i++ + ". Logged in as non-approved user.", driver);
			
			Log.softAssertThat(platinumApplication.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divContactDisclaimer"), platinumApplication), 
					"If the user is not Pre-Approved or not a Current Brand Card holder, then the System should navigate the User to Non-Pre-Approved form", 
					"The the System navigated the User to Non-Pre-Approved form", 
					"The the System did not navigate the User to Non-Pre-Approved form", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	} // M1_FBB_DROP5_C22729

}
