package com.fbb.testscripts.drop5;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22599 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(enabled = false, groups = { "high", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22599(String browser) throws Exception {
		//This test case not required
		Log.testCaseInfo();
	
		//Load Test Data
		String[] Bredcrum = {"HOME", "MY ACCOUNT", "Wishlist"};
		String username;
		String password = accountData.get("password_global");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
		
		//Pre-requisite - A registered user account required
		{
			GlobalNavigation.registerNewUser(driver, 0, 0, username + "|" + password);
		}
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAccount=homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
			OrderHistoryPage orderHistory= myAccount.clickOnOrderHistoryLink();
	
			//Step-1: Verify the functionality of Breadcrumb
	
			Log.softAssertThat(orderHistory.validateBreadcrum(Bredcrum[1],"displayed"),					 
					"Breadcrumb should be displayed as ‘< My Account’.", 
					"Breadcrumb is displayed as ‘< My Account’", 
					"Breadcrumb is not displayed as ‘< My Account’", driver);
	
	
			orderHistory.clickOnBreakCrumbMobile();
	
			Log.softAssertThat(myAccount.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAccount),					 
					"On click, user should be navigated to the brand-category-root page (Home Page) ", 
					"On click, user navigated to the brand-category-root page (Home Page) ", 
					"On click, did not navigate to the brand-category-root page (Home Page) ", driver);
	
			//Step-2: Verify the functionality of Active State
	
			orderHistory= myAccount.clickOnOrderHistoryLink();
	
			Log.softAssertThat(!orderHistory.validateBreadcrum(Bredcrum[2],"clickable"),					 
					"Since user is in Order History Page, Order History should then not be clickable in My Account Navigation Drawer", 
					"Order History is not clickable in My Account Navigation Drawer", 
					"Order History is clickable in My Account Navigation Drawer", driver);
	
	
			//Step-3: Verify the functionality of Order History
			Log.message("Verify the functionality of Order History: \nCovered functionality in the test case id: C22598 Step 2");
	
			//Step-4: Verify the functionality of Subhead
			Log.message("Verify the functionality of Subhead: \nCovered functionality in the test case id: C22598 Step 3");
	
			//Step-5: Verify the functionality of Default Closed State and Order History Summary
			//a)
			List<String> elementsToBeVerified = null;
			elementsToBeVerified = Arrays.asList("ordersummaryopened");
			Log.softAssertThat(orderHistory.elementLayer.verifyPageListElementsDoNotExist(elementsToBeVerified, orderHistory),					 
					"By default, Order History Summary should be in a closed state", 
					"By default, Order History Summary is in closed state", 
					"By default, Order History Summary is not in closed state", driver);
	
			//b)
			boolean temp1,temp2,temp3;
			temp1=temp2=temp3=false;
			elementsToBeVerified = Arrays.asList("listOrdernumber");
			temp1=orderHistory.elementLayer.VerifyPageListElementDisplayed(elementsToBeVerified, orderHistory);
	
			elementsToBeVerified = Arrays.asList("listOrderDate");
			temp2=orderHistory.elementLayer.VerifyPageListElementDisplayed(elementsToBeVerified, orderHistory);
	
			elementsToBeVerified = Arrays.asList("listOrderDate");
			temp3=orderHistory.elementLayer.VerifyPageListElementDisplayed(elementsToBeVerified, orderHistory);
	
			Log.softAssertThat(temp1&&temp2&&temp3,					 
					"Order #,Date and Order Total should be displayed.", 
					"Order #,Date and Order Total are displayed.", 
					"Order #,Date and Order Total are not displayed.", driver);
	
			//c)
			orderHistory.opencloseOrderSummarytray(0);
			Log.softAssertThat(orderHistory.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("viewdetails"), orderHistory),					 
					"On clicking any where in the Order History Summary or on the carat symbol which is positioned downward should display viewdetails", 
					"Viewdetails is displayed", 
					"Viewdetails is not displayed", driver);
	
	
			//d)  Expanding the drawer should not overlap on any other components. 
	
			//e) If any further orders are available, then they should be pushed down to accommodate the expansion of drawer
	
			//f)
			orderHistory.closesummary();
			Log.softAssertThat(!orderHistory.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("viewdetails"), orderHistory),					 
					"On clicking any where in the Order History Summary or on the carat symbol which is positioned downward should display viewdetails", 
					"Viewdetails is displayed", 
					"Viewdetails is not displayed", driver);
	
	
			elementsToBeVerified = Arrays.asList("listOrdernumber");
			temp1=orderHistory.elementLayer.VerifyPageListElementDisplayed(elementsToBeVerified, orderHistory);
	
			elementsToBeVerified = Arrays.asList("listOrderDate");
			temp2=orderHistory.elementLayer.VerifyPageListElementDisplayed(elementsToBeVerified, orderHistory);
	
			elementsToBeVerified = Arrays.asList("listOrderDate");
			temp3=orderHistory.elementLayer.VerifyPageListElementDisplayed(elementsToBeVerified, orderHistory);
	
			Log.softAssertThat(temp1&&temp2&&temp3,					 
					"Order #,Date and Order Total should be displayed.", 
					"Order #,Date and Order Total are displayed.", 
					"Order #,Date and Order Total are not displayed.", driver);
	
			//h) System should display orders in SFCC that haven’t been exported yet and should merge that with order history from MW
	
	
			//Step-6: Verify the functionality of Order #
			Log.message("Verify the functionality of Order #:  \nCovered functionality in the test case id: C22598 Step 10");
	
			Log.message("Verify the functionality of Date:  \nCovered functionality in the test case id: C22598 Step 11");
	
			Log.message("Verify the functionality of Total:  \nCovered functionality in the test case id: C22598 Step 12");
	
			Log.message("Verify the functionality of Return Items:  \nCovered functionality in the test case id: C22598 Step 14");
	
			Log.message("Verify the functionality of View Details link:  \nCovered functionality in the test case id: C22598 Step 15");
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}

}// search
