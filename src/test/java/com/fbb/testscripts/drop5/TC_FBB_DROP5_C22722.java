package com.fbb.testscripts.drop5;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.CatalogPreferencePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22722 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader testData = EnvironmentPropertiesReader.getInstance("demandware");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22722(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Footers footers = homePage.footers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			//Step-1: Verify the display of Header
			CatalogPreferencePage catalogPrefs = footers.navigateToCatalogPreferences();
			Log.message(i++ + ". Navigated to Catalog preference page for guest user.", driver);
			
			Log.softAssertThat(catalogPrefs.elementLayer.verifyVerticalAllignmentOfElements(driver, "divBreadcrumb", "lblCatalogPref", catalogPrefs), 
					"For Unauthenticated Users: Header should be displayed below the Breadcrumb", 
					"Header is displayed below the Breadcrumb", 
					"Header is not displayed below the Breadcrumb", driver);
			
			driver.navigate().to(Utils.getWebSite());
			{
				GlobalNavigation.registerNewUser(driver, 0, 0, email + "|" + password);
			}
			MyAccountPage myAcc = headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged in to user account.", driver);

			catalogPrefs = myAcc.clickOnCatalogPrefLink();
			Log.message(i++ + ". Navigated to Catalog preference page for authenticated user.", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(catalogPrefs.elementLayer.verifyVerticalAllignmentOfElements(driver, "divBreadcrumb", "lblCatalogPref", catalogPrefs), 
						"For Unauthenticated Users: Header should be displayed below the Breadcrumb", 
						"Header is displayed below the Breadcrumb", 
						"Header is not displayed below the Breadcrumb", driver);
			}
			else {
				Log.softAssertThat(catalogPrefs.elementLayer.verifyVerticalAllignmentOfElements(driver, "divAccountNavLinks", "lblCatalogPref", catalogPrefs), 
						"For Authenticated Users: Header should be displayed below the My Account Navigation", 
						"Header is displayed below the My Account Navigation", 
						"Header is not displayed below the My Account Navigation", driver);
			}
			
			Log.softAssertThat(catalogPrefs.elementLayer.verifyTextEquals("lblCatalogPref", testData.get("catalog.header"), catalogPrefs), 
					"Text in the Header should be handled by the property catalog.unsubscribe.header from account.properties.", 
					"Text in the Header is handled by the property catalog.unsubscribe.header from account.properties.", 
					"Text in the Header is not handled by the property catalog.unsubscribe.header from account.properties.", driver);
			
			//Step-2: Verify the display of Thank You Copy
			String[] catalogAddress = checkoutData.get("address_1").split("\\|");
			HashMap<String, String> addressDetails = new HashMap<String, String>();
			
			addressDetails.put("firstName", catalogAddress[7]);
			addressDetails.put("lastName", catalogAddress[8]);
			addressDetails.put("addrLine1", catalogAddress[0]);
			addressDetails.put("addrLine2", catalogAddress[1]);
			addressDetails.put("ZIP", catalogAddress[4]);
			addressDetails.put("State", catalogAddress[3]);
			addressDetails.put("City", catalogAddress[2]);
			
			catalogPrefs.enterFirstName(addressDetails.get("firstName"));
			catalogPrefs.enterLastName(addressDetails.get("lastName"));
			catalogPrefs.enterAddress1(addressDetails.get("addrLine1"));
			catalogPrefs.enterAddress2(addressDetails.get("addrLine2"));
			catalogPrefs.enterPostalCode(addressDetails.get("ZIP"));
			catalogPrefs.enterCity(addressDetails.get("City"));
			Log.message(i++ + ". Filled address", driver);
			
			catalogPrefs.clickSaveCatalogue();
			Log.message(i++ + ". Cllicked save.", driver);
					
			if(Utils.isMobile()) {
				Log.softAssertThat(catalogPrefs.elementLayer.verifyVerticalAllignmentOfElements(driver, "divBreadcrumb", "divCatalogThankyou", catalogPrefs), 
						"Thank You Copy should be displayed below the Header.", 
						"Thank You Copy is displayed below the Header.", 
						"Thank You Copy is not displayed below the Header.", driver);
			}else {
				Log.softAssertThat(catalogPrefs.elementLayer.verifyVerticalAllignmentOfElements(driver, "divAccountNavigation", "divCatalogThankyou", catalogPrefs), 
						"Thank You Copy should be displayed below the Header.", 
						"Thank You Copy is displayed below the Header.", 
						"Thank You Copy is not displayed below the Header.", driver);
			}
			
			Log.softAssertThat(catalogPrefs.elementLayer.verifyTextContains("divCatalogThankyou", testData.get("catalog.thankyou"), catalogPrefs), 
					"Text in the Thank You Copy should be handled by the property catalog.unsubscribe.confirmation from account.properties.", 
					"Text in the Thank You Copy is handled by the property catalog.unsubscribe.confirmation from account.properties.", 
					"Text in the Thank You Copy is not handled by the property catalog.unsubscribe.confirmation from account.properties.", driver);
			
			//Step and verifications disabled until content asset is enabled build.
			/*
			//Step-3: Verify the display of Questions and Answers
			Log.softAssertThat(false, 
					"Questions and Answers should be displayed below the Thank You Copy", 
					"Questions and Answers is displayed below the Thank You Copy", 
					"Questions and Answers is not displayed below the Thank You Copy", driver);
			
			Log.softAssertThat(false, 
					"System should display the answer for each question in collapsed state", 
					"System displays the answer for each question in collapsed state", 
					"System does not display the answer for each question in collapsed state", driver);
			
			Log.softAssertThat(false, 
					"Carat symbol should be available on the right corner for each Question", 
					"Carat symbol is available on the right corner for each Question", 
					"Carat symbol is not available on the right corner for each Question", driver);
			
			Log.softAssertThat(false, 
					"On clicking the carat symbol or anywhere in the question content, answer for the respective question should be displayed", 
					"Answer for the respective question is displayed", 
					"Answer for the respective question is not displayed", driver);
			
			//Click on the question.
			Log.softAssertThat(false, 
					"Again on clicking the carat symbol or anywhere in the question content, answer for the respective question should be collapsed", 
					"Answer for the respective question is collapsed", 
					"Answer for the respective question is not collapsed", driver);
			
			Log.softAssertThat(false, 
					"Text in the Questions and Answers should be handled via global content asset ID catalog-question-answer", 
					"Text in the Questions and Answers is handled via global content asset ID catalog-question-answer", 
					"Text in the Questions and Answers is not handled via global content asset ID catalog-question-answer", driver);*/
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	} // M1_FBB_DROP5_C22722

}
