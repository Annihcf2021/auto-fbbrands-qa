package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.GuestEmailPreferencesPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22708 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;

	@Test(enabled = false, groups = { "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22708(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String username; // Guest Email
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
	
		int i = 1;
		try {
	
			new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			driver.get("https://sfcc.qa." + Utils.getCurrentBrand() + ".plussizetech.com/on/demandware.store/Sites-fbbrands-Site/default/EmailSubscription-GuestEmailPreferences");
			Log.message(i++ + ". Navigated to Email Preferences Page!", driver);
			GuestEmailPreferencesPage guestEmailPreference = new GuestEmailPreferencesPage(driver).get();
	
			//2 - Verify the functionality of Email Preferences
			if(!Utils.isMobile()) {
				Log.softAssertThat(guestEmailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "homeBreadcrum", "lblEmailPreferencesTitle", guestEmailPreference),
						"Email Preferences should be located underneath the breadcrumb.",
						"Email Preferences is located underneath the breadcrumb.",
						"Email Preferences is not located underneath the breadcrumb.", driver);
			}else {
				Log.softAssertThat(guestEmailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "homeBreadcrumbMobile", "lblEmailPreferencesTitle", guestEmailPreference),
						"Email Preferences should be located underneath the breadcrumb.",
						"Email Preferences is located underneath the breadcrumb.",
						"Email Preferences is not located underneath the breadcrumb.", driver);
			}
	
			//3 - Verify the functionality of Subhead
			Log.softAssertThat(guestEmailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblEmailPreferencesTitle", "lblEmailPreferencesSubHeading", guestEmailPreference),
					"Subhead  should be located underneath the Email Preferences.",
					"Subhead  is located underneath the Email Preferences.",
					"Subhead  is not located underneath the Email Preferences.", driver);
	
			//4 - Verify the functionality of Email Address Field
			Log.softAssertThat(guestEmailPreference.verifyPlaceHolder("normal"),
					"The placeholder text should be displayed correctly",
					"The placeholder text is displayed correctly",
					"The placeholder text is not displayed correctly", driver);
	
			Log.softAssertThat(guestEmailPreference.verifyEmailFieldEditable(),
					"User should be able to enter text in the Email Address.",
					"User should is able to enter text in the Email Address",
					"User should was not able to enter text in the Email Address", driver);
	
			guestEmailPreference.typeEmail("");
			Log.message(i++ + ". Email is empty!", driver);
			guestEmailPreference.clickSubmit();
			Log.message(i++ + ". Clicked submit!", driver);
	
			Log.softAssertThat(guestEmailPreference.verifyPlaceHolder("blank"),
					"Email address field should not be left blank",
					"Email address field shows as manadatory if left blank.",
					"Email address field does not show as manadatory if left blank.", driver);
	
			//5 - Verify the functionality of Submit part 1
			guestEmailPreference.typeEmail(username.split("@")[0]);
			Log.message(i++ + ". Typed invalid email!", driver);
			guestEmailPreference.clickSubmit();
			Log.message(i++ + ". Clicked submit!", driver);
	
			Log.softAssertThat(guestEmailPreference.verifyPlaceHolder("invalid"),
					"The placeholder text should be displayed correctly",
					"The placeholder text is displayed correctly",
					"The placeholder text is not displayed correctly", driver);
	
			//6 - Verify the functionality of Questions and Answers
			Log.softAssertThat(guestEmailPreference.elementLayer.verifyElementDisplayed(Arrays.asList("divQuestionAnswerSection"), guestEmailPreference),
					"The Q&A section should be displayed",
					"The Q&A section is displayed",
					"The Q&A section is not displayed", driver);
	
			Log.softAssertThat(guestEmailPreference.verifyAnswerDisplayed(0) == false,
					"The answers must be collapsed by default",
					"The answers is collapsed by default",
					"The answers is not collapsed by default", driver);
	
			guestEmailPreference.toggleQuestion(0, true);
			Log.message(i++ + ". Expanded the question!", driver);
	
			Log.softAssertThat(guestEmailPreference.verifyAnswerDisplayed(0) == true,
					"The answers must be displayed",
					"The answers is displayed",
					"The answers is not displayed", driver);
	
			guestEmailPreference.toggleQuestion(0, false);
			Log.message(i++ + ". Collapsed the question!", driver);
	
			Log.softAssertThat(guestEmailPreference.verifyAnswerDisplayed(0) == false,
					"The answers must be collapsed",
					"The answers is collapsed",
					"The answers is not collapsed", driver);
	
			//7 - Verify the functionality of Disclaimer
			Log.softAssertThat(guestEmailPreference.elementLayer.verifyVerticalAllignmentOfElements(driver, "emailAddressTxtField", "txtDisclaimer", guestEmailPreference),
					"Disclaimer should be located underneath the Email Address textbox",
					"Disclaimer is located underneath the Email Address textbox",
					"Disclaimer is not located underneath the Email Address textbox", driver);
	
	
			//1 - Verify the functionality of Breadcrumb
			if(Utils.isDesktop() || Utils.isTablet())
			{
				Log.softAssertThat(guestEmailPreference.verifyBreadCrumb(),
						"The breadcrumb should be displayed",
						"The breadcrumb is displayed",
						"The breadcrumb is not displayed", driver);
	
				guestEmailPreference.clickBreadCrumb();
				Log.message(i++ + ". Clicked on Home in bread crumb!", driver);
	
			}
			else
			{
				Log.softAssertThat(guestEmailPreference.getMobileBreadCrumb().trim().equalsIgnoreCase("Home"),
						"The breadcrumb current element should be displayed",
						"The breadcrumb current element is displayed",
						"The breadcrumb current element is not displayed", driver);
	
				guestEmailPreference.clickBackArrowInBreadcrumb();
				Log.message(i++ + ". Clicked on Home Page in bread crumb!", driver);
			}
	
			//5 - Verify the functionality of Submit part2
			driver.get("https://sfcc.qa." + Utils.getCurrentBrand() + ".plussizetech.com/on/demandware.store/Sites-fbbrands-Site/default/EmailSubscription-GuestEmailPreferences");
			Log.message(i++ + ". Navigated to Email Preferences Page!", driver);
			guestEmailPreference = new GuestEmailPreferencesPage(driver).get();
	
			guestEmailPreference.typeEmail(username);
			Log.message(i++ + ". Typed valid email!", driver);
			guestEmailPreference.clickSubmit();
			guestEmailPreference.clickOverlaySubmit();
			Log.message(i++ + ". Clicked submit!", driver);
	
			Log.softAssertThat(guestEmailPreference.elementLayer.verifyElementDisplayed(Arrays.asList("divUnSubscribeConfirmation"), guestEmailPreference),
					"The user's preferences should be displayed",
					"The user's preferences is displayed",
					"The user's preferences is not displayed", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP5_C22708

}// search
