package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SignIn;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22731 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	

	@Test(groups = { "critical", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22731(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Clicked on Sign in link and navigated to sign in page!", driver);
	
			signIn.clickForgotPwdLink();
			Log.message(i++ + ". Clicked on Forgot Password link!", driver);
	
			//Step-1: functionality of Header
			Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfListOfElements(Arrays.asList("headingRequestPassword", "subHeadRequestPasswordModal", "txtEmailInForgotPassword", "btnSendRequestPassword"), signIn),
					"Header should be displayed on top left of the Forgot Password Request Modal.",
					"Header is displayed on top left of the Forgot Password Request Modal.",
					"Header is not displayed on top left of the Forgot Password Request Modal.", driver);
	
			//Step-2: functionality of Subhead
			Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "headingRequestPassword", "subHeadRequestPasswordModal", signIn),
					"Subhead should be displayed below the Header.",
					"Subhead is displayed below the Header.",
					"Subhead is not displayed below the Header.", driver);
	
			signIn.closeForgotPasswordDialog();
			Log.message(i++ + ". Clicked on anywhere on the page to close forgot password pop up!", driver);
	
			//Step-3: Functionality of Email Address
			signIn.clickSignInButton();
			Log.message(i++ + ". Clicked on 'sign in' button Without entering Email ID!", driver);
	
			signIn.enterEmailID("sdfsfsdf");
			Log.message(i++ + ". Entered Invalid Email Id to validate error message!", driver);
	
			signIn.clickSignInButton();
			Log.message(i++ + ". Clicked on 'sign in' button!", driver);
	
			Log.softAssertThat((signIn.getDisplayedErrorMessage().contains("Please enter a valid Email Address.") || signIn.getDisplayedErrorMessage().contains("Please enter a valid email.")), 
					"Appropriate Error message should be displayed when entering invalid Email Id", 
					"Appropriate Error message is displayed when entering invalid Email Id",
					"Appropriate Error message is not displayed when entering invalid Email Id",
					driver);
	
			signIn.enterEmailID("abc@gmail.com");
			Log.message(i++ + ". Entered valid Email Id!", driver);
	
			Log.softAssertThat(signIn.getEnteredEmailID().contains("@") && signIn.getEnteredEmailID().contains(".com"), 
					"Email Address should be in proper format", 
					"Email Address is in proper format",
					"Email Address is not in proper format",
					driver);
	
			Log.reference("Refer test case # 22732");
	
			//Step-4: functionality of Send button
			Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtForgotEmail", "btnPasswordResetSubmit", signIn), 
					"Send button should be displayed below the the Email Address field.", 
					"Send button is displayed below the the Email Address field.",
					"Send button is not displayed below the the Email Address field.",
					driver);
	
			signIn.clickForgotPwdSubmit();
			Log.message(i++ + ". Clicked on Forgot Password Submit!", driver);
	
			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("forgotPwdSuccessMessageContainer"), signIn), 
					"Confirm message should be displayed when entering valid Email Id", 
					"Confirm message is displayed when entering valid Email Id",
					"Confirm message is not displayed when entering valid Email Id",
					driver);
	
			signIn.closeForgotPasswordDialog();
			Log.message(i++ + ". Clicked on anywhere on the page to close forgot password pop up!", driver);
	
			Log.softAssertThat(signIn.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("divForgotPassword"), signIn), 
					"Forgot Password Modal should not be displayed", 
					"Forgot Password Modal is not displayed",
					"Forgot Password Modal is displayed",driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C22731

}// search
