package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22659 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader dwreData = EnvironmentPropertiesReader.getInstance("demandware");

	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22659(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String email;
		String password = accountData.get("password_global");
		String errorShortPW = dwreData.get("ShortPasswordError");
		String errorUnsecurePW = dwreData.get("UnsecurePasswordError");
		String newEmail = "abc@def.com";
		String newPasswrod = "123a123a";
		String invalidEmail = "abcdefcom";
		String passwordShort = "test@";
		String passwordNoNumber = "fbbtestfbbtest";
	
		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		email = AccountUtils.generateEmail(driver);
		
		//Create new account
		GlobalNavigation.registerNewUser(driver, 0, 0, email+"|"+password);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAccountPage = homePage.headers.navigateToMyAccount(email, password, true);
			Log.message(i++ + ". Navigated to 'My Account' page ",driver);
	
			ProfilePage profilePage = myAccountPage.navigateToUpdateProfile();
			Log.message(i++ + ". Navigated to 'Profile' Page ",driver);
	
			//Step-1: Verify the functionality of My Profile heading element in My Account Profile screen
			if(!Utils.isMobile()) {
				Log.softAssertThat((profilePage.elementLayer.verifyVerticalAllignmentOfElements(driver, "breadcrumbCurrentElement", "lblProfileHeading", profilePage)),
						"The My Profile heading element should be displayed below the My Account navigation elements ",
						"The My Profile heading element is displayed below the My Account navigation elements ",
						"The My Profile heading element is not displayed below the My Account navigation elements ", driver);
				
				Log.softAssertThat(profilePage.elementLayer.verifyVerticalAllignmentOfElements(driver, "mainHeading", "lblProfileHeading", profilePage), 
						"The My Profile heading element should be displayed below the My Account navigation", 
						"The My Profile heading element is displayed below the My Account navigation", 
						"The My Profile heading element is not displayed below the My Account navigation", driver);
			}
			else{
				Log.softAssertThat((profilePage.elementLayer.verifyVerticalAllignmentOfElements(driver, "breadcrumbMobile", "lblProfileHeading", profilePage)),
						"The My Profile heading element should be displayed below the My Account navigation elements ",
						"The My Profile heading element is displayed below the My Account navigation elements ",
						"The My Profile heading element is not displayed below the My Account navigation elements ", driver);
			}
	
			//Step-2: Verify the functionality of Subheading element in My Account Profile screen
			Log.softAssertThat((profilePage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblProfileHeading", "profileSubHeading", profilePage)),
					"The Subheading element should be displayed below the My profile heading element",
					"The Subheading element is displayed below the My profile heading element",
					"The Subheading element is not displayed below the My profile heading element", driver);
	
			//Step-3: Verify the functionality of Personal Information heading element in My Account Profile screen
			Log.softAssertThat((profilePage.elementLayer.verifyVerticalAllignmentOfElements(driver, "profileSubHeading", "PersonalInformationHeading", profilePage)),
					"The Personal Information heading should be displayed below the subheading element.",
					"The Personal Information heading is displayed below the subheading element",
					"The Personal Information heading is displayed below the subheading element", driver);
	
			//Step-4: Verify the functionality of First Name form field in My Account Profile screen
			Log.softAssertThat(profilePage.elementLayer.verifyVerticalAllignmentOfElements(driver, "PersonalInformationHeading", "txtFirstName", profilePage), 
					"The First name text box should be displayed below the Personal Information heading", 
					"The First name text box is displayed below the Personal Information heading", 
					"The First name text box is not displayed below the Personal Information heading", driver);
	
			Log.softAssertThat(profilePage.verifyEnteredFirstNameIsCorrect(),
					"Entered first name should be updated in first name field!",
					"Entered first name should be updated in first name field!",
					"Entered first name should be updated in first name field!", driver);
	
			Log.softAssertThat(profilePage.elementLayer.verifyPageElements(Arrays.asList("firstNamePlaceHolderText"), profilePage), 
					"Place Holder Text should be displayed at the top when entering first name", 
					"Place Holder Text is displayed at the top when entering first name",
					"Place Holder Text is not displayed at the top when when entering first name", driver);
	
			//Step-5: Verify the functionality of Last Name form field in My Account Profile screen
			Log.softAssertThat(profilePage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtFirstName", "txtLastName", profilePage), 
					"The Last name text box should be displayed below the First name field", 
					"The Last name text box is displayed below the First name field", 
					"The Last name text box is not displayed below the First name field", driver);
	
			Log.softAssertThat(profilePage.verifyEnteredLastNameIsCorrect(),
					"User should be able to enter text in the Last Name form field",
					"User is able to enter text in the Last Name form field",
					"User is not able to enter text in the Last Name form field", driver);
	
			Log.softAssertThat(profilePage.elementLayer.verifyPageElements(Arrays.asList("lastNamePlaceHolderText"), profilePage), 
					"Place Holder Text should be displayed at the top when entering last name", 
					"Place Holder Text is displayed when at the top entering last name",
					"Place Holder Text is not displayed at the top when when entering last name", driver);
	
			//Step-6: Verify the functionality of Email Address form field in My Account Profile screen
			Log.softAssertThat(profilePage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtLastName", "txtEmail", profilePage), 
					"The Email address should be displayed below the Last Name field", 
					"The Email address is displayed below the Last Name field", 
					"The Email address is not displayed below the Last Name field", driver);
	
			Log.softAssertThat(profilePage.verifyEmailAddressIsCorrect(email), 
					"User should be able to enter text in the Email Address form field", 
					"User is able to enter text in the Email Address form field", 
					"User is able to enter text in the Email Address form field", driver);
	
			profilePage.typeOnEmailAddress(invalidEmail);
			Log.softAssertThat(profilePage.elementLayer.verifyPageElements(Arrays.asList("emailAddressError"), profilePage), 
					"Error should be displayed when entering invalid email address!", 
					"Error is displayed when entering invalid email address!",
					"Error is not displayed when entering invalid email address!", driver);
	
			Log.softAssertThat(profilePage.verifyEmailAddressIsCorrect(newEmail),
					"Entered email address should be updated in first name field!",
					"Entered email address is updated in first name field!",
					"Entered email address is not updated in first name field!", driver);
	
			Log.softAssertThat(profilePage.elementLayer.verifyPageElements(Arrays.asList("emailPlaceHolderText"), profilePage), 
					"Place Holder Text should be displayed at the top when entering email address", 
					"Place Holder Text is displayed when at the top entering email address",
					"Place Holder Text is not displayed at the top when when email address", driver);
	
			Log.softAssertThat(profilePage.getEmailAddress().contains("@") 
					&& profilePage.getEmailAddress().contains("."),
					"Entered email address should be in correct format!",
					"Entered email address is in correct format!!",
					"Entered email address is not in correct format!!", driver);
	
			//Step-7: Verify the functionality of Confirm Email Address form field in My Account Profile screen
			Log.softAssertThat(profilePage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtEmail", "txtConfirmEmail", profilePage), 
					"The Confirm Email Address form field should be displayed below the Email address field", 
					"The Confirm Email Address form field is displayed below the Email address field", 
					"The Confirm Email Address form field is not displayed below the Email address field", driver);
	
			Log.softAssertThat(profilePage.getConfirmEmailAddress().equals(""),
					"Confirm email address should not be pre populated",
					"Confirm email address is not pre populated!",
					"Confirm email address is pre populated!", driver);
	
			profilePage.typeOnConfirmEmailAddress(invalidEmail);
			profilePage.typeOnPassword("qatest123");
			Log.message(i++ + ". Entered Confirm Password ",driver);
			profilePage.typeOnPhoneNo();
			Log.message(i++ + ". Entered Valid Phone No. ",driver);
			profilePage.clickOnUpdateInfoBtn();
			Log.message(i++ + ". Clicked on Update Information.", driver);
	
			Log.softAssertThat(profilePage.elementLayer.verifyPageElements(Arrays.asList("confirmEmailAddressError"), profilePage), 
					"Error should be displayed when entering invalid confirm email address!", 
					"Error is displayed when entering invalid confirm email address!",
					"Error is not displayed when entering invalid confirm email address!", driver);
	
			Log.softAssertThat(profilePage.verifyConfirmEmailAddressIsCorrect(newEmail),
					"User should be able to enter text in the Confirm Email Address form field",
					"User is able to enter text in the Confirm Email Address form field",
					"User is not able to enter text in the Confirm Email Address form field", driver);
	
			Log.softAssertThat(profilePage.elementLayer.verifyPageElements(Arrays.asList("confirmEmailPlaceHolderText"), profilePage), 
					"Place Holder Text should be displayed at the top when entering confirm email address", 
					"Place Holder Text is displayed at the top when entering confirm email address",
					"Place Holder Text is not displayed at the top when when confirm email address", driver);
	
			Log.softAssertThat(profilePage.verifyEmailAndConfirmEmailSame(),
					"Entered email address and confirm email address should be same!",
					"Entered email address and confirm email address is same!",
					"Entered email address and confirm email address is not same!", driver);
	
			Log.softAssertThat(profilePage.getConfirmEmailAddress().contains("@") &&
					profilePage.getConfirmEmailAddress().contains("."),
					"Entered email address should be in correct format!",
					"Entered email address is in correct format!!",
					"Entered email address is not in correct format!!", driver);
	
			//Step-8: Verify the functionality of Current Password form field in My Account Profile screen
			Log.softAssertThat(profilePage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtConfirmEmail", "txtPassword", profilePage), 
					"The Current Password form field should be displayed below the Confirm Email Address field.", 
					"The Current Password form field is displayed below the Confirm Email Address field.", 
					"The Current Password form field is not displayed below the Confirm Email Address field.", driver);
	
			profilePage.typeOnPassword("qatest123");
			Log.message(i++ + ". Entered Password and Confirm Password ",driver);
	
			Log.softAssertThat(profilePage.verifyPasswordisMaskedorUnmasked("mask"), 
					"Password should be masked after entering!", 
					"Password is masked after entering!",
					"Password is not masked after entering!", driver);
	
			Log.softAssertThat(profilePage.elementLayer.verifyPageElements(Arrays.asList("pwdPlaceHolderText"), profilePage), 
					"Place Holder Text should be displayed at the top when password field is not empty", 
					"Place Holder Text is displayed at the top when password field is not empty",
					"Place Holder Text is not displayed at the top when password field is not empty", driver);
	
			//Step-9: Verify the functionality of Password Requirements message element in My Account Profile screen.
			profilePage.typeOnPassword(passwordShort);
			profilePage.typeOnConfirmEmailAddress("");

			Log.softAssertThat(profilePage.verifyPasswordError(errorShortPW), 
					"Appropriate error message should be displayed for short password.", 
					"Appropriate error message is displayed for short password.", 
					"Appropriate error message is not displayed for short password.", driver);
	
			profilePage.typeOnPassword(passwordNoNumber);
			profilePage.typeOnConfirmEmailAddress("");
			Log.softAssertThat(profilePage.verifyPasswordError(errorUnsecurePW), 
					"Appropriate error message should be displayed for no number password.", 
					"Appropriate error message should be displayed for no number password.", 
					"Appropriate error message should be displayed for no number password.", driver);
	
			//Step-10: Verify the functionality of Phone Number form field in My Account Profile screen
			profilePage.typeOnConfirmPassword("qatest123");
			Log.softAssertThat(profilePage.verifyConfirmPasswordisMaskedorUnmasked("mask"), 
					"Confirm Password should be masked after entering!", 
					"Confirm Password is masked after entering!",
					"Confirm Password is not masked after entering!", driver);
	
			Log.softAssertThat(profilePage.elementLayer.verifyPageElements(Arrays.asList("confirmPwdPlaceHolderText"), profilePage), 
					"Place Holder Text should be displayed at the top when confirm password field is not empty", 
					"Place Holder Text is displayed at the top when confirm password field is not empty",
					"Place Holder Text is not displayed at the top when confirm password field is not empty", driver);
	
			//Step-11: Verify the functionality of Birth Month dropdown in My Account Profile screen
			Log.softAssertThat(profilePage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtPhoneNo", "drpBirthMonth", profilePage), 
					"The Birth Month dropdown should be present below the phone number form field", 
					"The Birth Month dropdown is present below the phone number form field", 
					"The Birth Month dropdown is not present below the phone number form field", driver);
	
			//Step-12: Verify the functionality of the Update Information button in My Account Profile screen
			profilePage.typeOnConfirmEmailAddress("different" + newEmail);
	
			Log.softAssertThat(!profilePage.verifyEmailAndConfirmEmailSame(),
					"Entered email address and confirm email address should not be same!",
					"Entered email address and confirm email address is not same!",
					"Entered email address and confirm email address is same!", driver);
	
			profilePage.enterPassword(newPasswrod);
			Log.message(i++ + ". Entered Password ",driver);
	
			profilePage.enterConfirmPassword("different" + newPasswrod);
			Log.message(i++ + ". Entered Confirm Password ",driver);
	
			Log.softAssertThat(!profilePage.verifyPwdConfirmPwdSame(),
					"Entered password and confirm password should not be same!",
					"Entered password and confirm password is not same!",
					"Entered password and confirm password is same!", driver);
	
			profilePage.typeOnConfirmEmailAddress(newEmail);
			Log.softAssertThat(profilePage.verifyEmailAndConfirmEmailSame(),
					"Entered email address and confirm email address should be same!",
					"Entered email address and confirm email address is same!",
					"Entered email address and confirm email address is not same!", driver);
	
			profilePage.enterNewPassword(newPasswrod);
			Log.message(i++ + ". Entered Password ",driver);
			profilePage.enterConfirmPassword(newPasswrod);
			Log.message(i++ + ". Entered Confirm Password ",driver);
	
			Log.softAssertThat(profilePage.verifyPwdConfirmPwdSame(),
					"Entered password and confirm password should be same!",
					"Entered password and confirm password is same!",
					"Entered password and confirm password is not same!", driver);
	
			profilePage.clickUpdateInfoBtn();
			Log.message(i++ + ". Clicked on 'Update Information' button!", driver);
	
			//Step-13: Verify the functionality of Breadcrumb
			if(!Utils.isMobile()){
				Log.softAssertThat(homePage.headers.getBreadCrumbText().equalsIgnoreCase("HOME MY ACCOUNT PROFILE"),
						"The breadcrumb should be displayed as - 'HOME > MY ACCOUNT > PROFILE'",
						"The breadcrumb is displayed as expected - 'HOME > MY ACCOUNT > PROFILE'",
						"The breadcrumb is not displayed as expected - 'HOME > MY ACCOUNT > PROFILE' and the actual is : '"
								+ homePage.headers.getBreadCrumbText() + "'",driver);
	
				String beforeURL = driver.getCurrentUrl();
	
				profilePage.clickOnBreakCrumbValue(2);		
				Log.message(i++ + ". Clicked on 'Profile' in breadcrumb!", driver);
	
				String afterURL = driver.getCurrentUrl();
	
				Log.softAssertThat(beforeURL.equals(afterURL) || driver.getCurrentUrl().contains("/profile"), 
						"Page should not be redirected from 'Profile' page", 
						"Page is not redirected from 'Profile' page",
						"Page is redirected from 'Profile' page", driver);
	
				profilePage.clickOnBreakCrumbValue(1);		
				Log.message(i++ + ". Clicked on 'My Account' in breadcrumb!", driver);
	
				Log.softAssertThat(driver.getCurrentUrl().contains("/my-account")
								|| (BrandUtils.isBrand(Brand.el) && driver.getCurrentUrl().contains("/profile")), 
						"Page should be redirected to 'My Account' page", 
						"Page is redirected to 'My Account' page",
						"Page is not redirected to 'My Account' page", driver);
	
				profilePage.clickOnBreakCrumbValue(0);		
				Log.message(i++ + ". Clicked on 'Home' in breadcrumb!", driver);
				
				Log.softAssertThat(driver.getCurrentUrl().equals("https://sfcc.qa." + Utils.getCurrentBrandShort() +".plussizetech.com/"), 
						"Page should be navigated to home page when clicking on 'Home' in breadcrumb", 
						"Page is navigated to home page when clicking on 'Home' in breadcrumb",
						"Page is note navigated to home page when clicking on 'Home' in breadcrumb", driver);
			}
			else{
				Log.softAssertThat(
						homePage.headers.getBreadCrumbText().equalsIgnoreCase("MY ACCOUNT"),
						"The breadcrumb should be displayed as - ' < MY ACCOUNT'",
						"The breadcrumb is displayed as expected - ' < MY ACCOUNT'",
						"The breadcrumb is not displayed as expected - ' < MY ACCOUNT' and the actual is : < "
								+ homePage.headers.getBreadCrumbText(), driver);
	
				profilePage.clickOnBreadcrumbMobile();
				Log.message(i++ + ". Clicked on '<' in breadcrumb!", driver);
				Log.softAssertThat(driver.getCurrentUrl().contains("my-account")
							|| (BrandUtils.isBrand(Brand.el) && driver.getCurrentUrl().contains("profile"))
							|| profilePage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divOverviewHeading"), profilePage), 
						"Page should be navigated to Account page when clicking on '<' in breadcrumb", 
						"Page is navigated to Account page when clicking on '<' in breadcrumb",
						"Page is not navigated to Account page when clicking on '<' in breadcrumb", driver);
			}
	
			//Step-14: Verify the functionality of Navigation in My Profile overview page
			Log.reference("Reference TC #22631, step 3");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP4_C22659

}// search
