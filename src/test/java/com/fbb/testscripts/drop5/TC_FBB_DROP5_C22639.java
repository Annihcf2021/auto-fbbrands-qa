package com.fbb.testscripts.drop5;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.CatalogPreferencePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.CollectionUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22639 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader dwreData = EnvironmentPropertiesReader.getInstance("demandware");

	@Test(groups = { "high", "desktop", "tablet" , "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22639(String browser) throws Exception{
		Log.testCaseInfo();
	
		//Load Test Data
		String username;
		String password = accountData.get("password_global");
	
		//Create the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		username = AccountUtils.generateEmail(driver);
		String HighlightColor = TestData.get("highlight_color_2");
		String invalidData = "$#$%";
		
		//Registering new account
		GlobalNavigation.registerNewUser(driver, 0, 0, username+"|"+password);
	
		int i=1;
		try
		{
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			MyAccountPage myAccountPage = homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			Log.softAssertThat(myAccountPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAccountPage), 
					"User Should login successfully", 
					"User logged successfully.",
					"User not logged successfully", driver);
	
			CatalogPreferencePage catlogPrePage = myAccountPage.clickOnCatalogPrefLink();
			catlogPrePage.navigateToMyAccountPage(1);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
			myAccountPage.clickOnCatalogPrefLink();
			Log.message(i++ + ". Catalog Preference screen can be accessed through Catalog preference header.",driver);
	
			//Step-1: Verify the functionality of Breadcrumb
			Log.softAssertThat(myAccountPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divPromoContent", "breadCrumb", homePage.headers), 
					"Breadcrumb should be displayed below the Promotional Content.", 
					"Breadcrumb is displayed below the Promotional Content.", 
					"Breadcrumb is not displayed below the Promotional Content.", driver);
	
			if(Utils.isMobile()) {
				Log.softAssertThat(homePage.headers.getBreadCrumbText().equalsIgnoreCase("MY ACCOUNT"),
						"Breadcrum element should displayed the MY ACCOUNT.", 
						"Breadcrum element is displayed the MY ACCOUNT.", 
						"Breadcrum element is not displayed the MY ACCOUNT.", driver);
			}
			else {
				Log.softAssertThat(homePage.headers.getBreadCrumbText().equalsIgnoreCase("HOME MY ACCOUNT CATALOG PREFERENCES"),
						"Breadcrum element should displayed the Home / My Account / Catalog Preferences.", 
						"Breadcrum element is displayed the Home / My Account / Catalog Preferences.", 
						"Breadcrum element is not displayed the Home / My Account / Catalog Preferences.", driver);
			}
	
			catlogPrePage.navigateToMyHometPage(0);
			Log.message(i++ + ". Navigated to 'Homme page after clicking the Home link ",driver);
			BrowserActions.navigateToBack(driver);
			catlogPrePage.navigateToMyAccountPage(1);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
			BrowserActions.navigateToBack(driver);
			String brand=Utils.getCurrentBrandShort();
			System.out.println(brand);
			driver.get("https://sfcc.qa."+brand+".plussizetech.com/on/demandware.store/Sites-fbbrands-Site/default/PrintPreferences-Show");
			Log.message(i++ + ". Catalog Preference screen can be accessed through direct link",driver);
	
			//Step-2: Verify the functionality of Active state in My Account Navigation Pane 
			Log.softAssertThat(myAccountPage.elementLayer.verifyElementColor("lnkCatalogPref", HighlightColor, myAccountPage),
					"Catlog Prefence should be highlighted once it is selected.",
					"Catlog Prefence is highlighted once it is selected.",
					"Catlog Prefence is not highlighted once it is selected.", driver);
	
			Log.softAssertThat(myAccountPage.elementLayer.verifyAttributeForElement("lnkCatalogPref", "style", "cursor: text", myAccountPage), 
					"Catalog Preferences should not be clickable in My Account Navigation",
					"Catalog Preferences is not be clickable in My Account Navigation", 
					"Catalog Preferences is clickable in My Account Navigation", driver);
	
			//Step-3: Verify the functionality of Catalog Preference Label
			if(Utils.isMobile()) {
				Log.softAssertThat(myAccountPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "btnToggleHamburgerMenu", "catalogRequestHeader", myAccountPage), 
						"Catalog Preference label should be displayed below the My Account Navigation Drawer", 
						"Catalog Preference label is displayed below the My Account Navigation Drawer", 
						"Catalog Preference label is not displayed below the My Account Navigation Drawer", driver);
			}
			else {
				Log.softAssertThat(myAccountPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkCatalogPref", "catalogRequestHeader", myAccountPage), 
						"Catalog Preference label should be displayed below the My Account Navigation Pane ", 
						"Catalog Preference label is displayed below the My Account Navigation Pane ", 
						"Catalog Preference label is not displayed below the My Account Navigation Pane ", driver);
			}
	
			Log.softAssertThat(catlogPrePage.verifyCatloguePrefHeaderTextIsComingFromProperty(), 
					"Text in the Catalog Preference Label should be handled by the property catalog.header from account.properties",
					"Text in the Catalog Preference Label is handled by the property catalog.header from account.properties", 
					"Text in the Catalog Preference Label is not handled by the property catalog.header from account.properties", driver);
	
			//Step-4, 5: Verify the functionality of Content Slot in Catalog request screen.
			Log.softAssertThat(myAccountPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "catalogRequestHeader", "copycatlogimage", myAccountPage), 
					"Content slot should be displayed below the Catalog Preference label ", 
					"Content slot is displayed below the Catalog Preference label ", 
					"Content slot is not displayed below the Catalog Preference label ", driver);
	
			//Step-6: Verify the functionality of Step 1 Label
			Log.softAssertThat(myAccountPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "copycatlogimage", "lblStep1", myAccountPage), 
					"Step 1 label should be displayed below the Content Slot", 
					"Step 1 label is displayed below the Content Slot", 
					"Step 1 label is not displayed below the Content Slot", driver);
	
			Log.softAssertThat(catlogPrePage.verifyCatloguePrefStep1FromProperty(), 
					"Text in the Step 1 Label should be handled by the Property catalog.step1 from account.properties",
					"Text in the Step 1 Label is handled by the Property catalog.step1 from account.properties", 
					"Text in the Step 1 Label is not handled by the Property catalog.step1 from account.properties", driver);
	
			//Step-7: Verify the functionality of Catalog Preferences
			Log.softAssertThat(catlogPrePage.verifyCatloguePrefStep2FromProperty(), 
					"Text in the Step 2 Label should be handled by the Property catalog.step2 from account.properties",
					"Text in the Step 2 Label is handled by the Property catalog.step2 from account.properties", 
					"Text in the Step 2 Label is not handled by the Property catalog.step2 from account.properties", driver);
	
			List<String> Lable1CatlogueTestdata = Arrays.asList(TestData.get("catalog_list").split("\\|"));
			Log.softAssertThat(CollectionUtils.compareTwoList(Lable1CatlogueTestdata, catlogPrePage.catlogueStep1List()),
					"Tiles should be arranged left to right based on the sort sequence ", 
					"Tiles arranged left to right based on the sort sequence", 
					"Tiles are not arranged left to right based on the sort sequence",driver);
	
			Log.softAssertThat(catlogPrePage.elementLayer.verifyAttributeForElement("requestCatluge", "checked", "true", myAccountPage), 
					"By default, Request a Catalog option should pre-selected.", 
					"By default, Request a Catalog option is pre-selected.", 
					"By default, Request a Catalog option is not pre-selected.", driver);
	
			//Step-8: Verify the functionality of Step 2 Label
			Log.softAssertThat(myAccountPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblStep1", "lblStep2", myAccountPage), 
					"Step 2 Label should be displayed below Step 1", 
					"Step 2 Label is displayed below Step 1", 
					"Step 2 Label is not displayed below Step 1", driver);
	
			Log.softAssertThat(catlogPrePage.verifyCatloguePrefStep2FromProperty(), 
					"Text in the Step 2 Label should be handled by the Property catalog.step2 from account.properties",
					"Text in the Step 2 Label is handled by the Property catalog.step2 from account.properties", 
					"Text in the Step 2 Label is not handled by the Property catalog.step2 from account.properties", driver);
	
			//Step-9: Verify the functionality of Address Fields
			Log.softAssertThat(catlogPrePage.elementLayer.verifyAttributeForElement("firstName", "aria-required", "true", catlogPrePage), 
					"First Name field should be a mandatory field.", 
					"First Name field  is a mandatory field.", 
					"First Name field is not a mandatory field.", driver);
	
			Log.softAssertThat(catlogPrePage.elementLayer.verifyAttributeForElement("lastName", "aria-required", "true", catlogPrePage), 
					"Last Name field should be a mandatory field.", 
					"Last Name field  is a mandatory field.", 
					"Last Name field is not a mandatory field.", driver);
	
			Log.softAssertThat(catlogPrePage.elementLayer.verifyAttributeForElement("address1", "aria-required", "true", catlogPrePage), 
					"Address1 field should be a mandatory field.", 
					"Address1 field  is a mandatory field.", 
					"Address1 field is not a mandatory field.", driver);
	
			Log.softAssertThat(catlogPrePage.elementLayer.verifyAttributeForElement("postalCode", "aria-required", "true", catlogPrePage), 
					"PostalCode field should be a mandatory field.", 
					"PostalCode field  is a mandatory field.", 
					"PostalCode field is not a mandatory field.", driver);
	
			Log.softAssertThat(catlogPrePage.elementLayer.verifyAttributeForElement("city", "aria-required", "true", catlogPrePage), 
					"City field should be a mandatory field.", 
					"City field  is a mandatory field.", 
					"City field is not a mandatory field.", driver);
	
			//Step-10: Verify the functionality of Submit button
			catlogPrePage.clickSaveCatalogue();
			Log.softAssertThat(catlogPrePage.getErrorMessageFirstname().contains("First Name"),
					"To check the Firstname should displaying the error when submiting without values.",
					"To check the Firstname displaying the error when submiting without values!",
					"To check the Firstname not displaying the error when submiting without values", driver);
	
			Log.softAssertThat(catlogPrePage.getErrorMessageLastname().contains("Last Name"),
					"To check the Lastname should displaying the error when submiting without values.",
					"To check the Lastname displaying the error when submiting without values!",
					"To check the Lastname not displaying the error when submiting without values", driver);
	
			Log.softAssertThat(catlogPrePage.getErrorMessageAddress1().contains("Address Line1"),
					"To check the Address1 should displaying the error when submiting without values.",
					"To check the Address1 displaying the error when submiting without values!",
					"To check the Address1 not displaying the error when submiting without values", driver);
	
			Log.softAssertThat(catlogPrePage.getErrorMessageCity().contains("City"),
					"To check the City should displaying the error when submiting without values.",
					"To check the City displaying the error when submiting without values!",
					"To check the City not displaying the error when submiting without values", driver);
	
			Log.softAssertThat(catlogPrePage.getErrorMessagePostalcode().contains("Zip Code"),
					"To check the PostalCode should displaying the error when submiting without values.",
					"To check the PostalCode displaying the error when submiting without values!",
					"To check the PostalCode not displaying the error when submiting without values", driver);
	
			Log.softAssertThat(catlogPrePage.getErrorMessageState().contains("State"),
					"To check the State should displaying the error when submiting without values.",
					"To check the State displaying the error when submiting without values!",
					"To check the State not displaying the error when submiting without values", driver);
	
			catlogPrePage.enterFirstName("a");
			Log.softAssertThat(catlogPrePage.elementLayer.verifyPageElements(Arrays.asList("firstNamePlaceholder"), catlogPrePage), 
					"To check the Firstname placeholder is going up when entering value.",
					"The Firstname placeholder is going up when entering value!",
					"The Firstname placeholder is not going up when entering value", driver);
	
			catlogPrePage.enterLastName("l");
			Log.softAssertThat(catlogPrePage.elementLayer.verifyPageElements(Arrays.asList("lastNamePlaceholder"), catlogPrePage), 
					"To check the Lastname placeholder is going up when entering value.",
					"The Lastname placeholder is going up when entering value!",
					"The Lastname placeholder is not going up when entering value", driver);
	
			catlogPrePage.enterAddress1("adress1");
			Log.softAssertThat(catlogPrePage.elementLayer.verifyPageElements(Arrays.asList("address1Placeholder"), catlogPrePage), 
					"To check the Address1 placeholder is going up when entering value.",
					"The Address1 placeholder is going up when entering value!",
					"The Address1 placeholder is not going up when entering value", driver);
	
			catlogPrePage.enterAddress2("adress1");
			Log.softAssertThat(catlogPrePage.elementLayer.verifyPageElements(Arrays.asList("address2Placeholder"), catlogPrePage), 
					"To check the Address2 placeholder is going up when entering value.",
					"The Address2 placeholder is going up when entering value!",
					"The Address2 placeholder is not going up when entering value", driver);
			catlogPrePage.enterPostalCode("123456");
			Log.softAssertThat(catlogPrePage.elementLayer.verifyPageElements(Arrays.asList("postalPlaceholder"), catlogPrePage), 
					"To check the Postalcode placeholder is going up when entering value.",
					"The Postalcode placeholder is going up when entering value!",
					"The Postalcode placeholder is not going up when entering value", driver);
	
			catlogPrePage.enterCity("City1");
			Log.softAssertThat(catlogPrePage.elementLayer.verifyPageElements(Arrays.asList("cityPlaceholder"), catlogPrePage), 
					"To check the City placeholder is going up when entering value.",
					"The City placeholder is going up when entering value!",
					"The City placeholder is not going up when entering value", driver);
	
			catlogPrePage.enterFirstName(invalidData);
			catlogPrePage.enterLastName(invalidData);
			catlogPrePage.enterCity("A");
			catlogPrePage.enterPostalCode(invalidData);
			catlogPrePage.clickSaveCatalogue();
			Log.softAssertThat(catlogPrePage.getErrorMessageFirstname().equals(dwreData.get("FirstNameError")),
					"To check the Firstname should displaying the error when submiting with invalid values.",
					"To check the Firstname displaying the error when submiting with invalid values!",
					"To check the Firstname not displaying the error when submiting with invalid values", driver);
			
			Log.softAssertThat(catlogPrePage.getErrorMessageLastname().equals(dwreData.get("LastNameError")),
					"To check the Firstname should displaying the error when submiting with invalid values.",
					"To check the Firstname displaying the error when submiting with invalid values!",
					"To check the Firstname not displaying the error when submiting with invalid values", driver);
			
			Log.softAssertThat(catlogPrePage.getErrorMessageCity().contains("Please enter at least 2 characters."),
					"To check the City should displaying the error when submiting less than 2 character.",
					"To check the City displaying the error when submiting less than 2 character.!",
					"To check the City not displaying the error when submiting less than 2 character.", driver);
			
			Log.softAssertThat(catlogPrePage.getErrorMessagePostalcode().contains("Invalid Zip"),
					"To check the City should displaying the error when submiting with invalid zip code.",
					"To check the City displaying the error when submiting with invalid zip code.!",
					"To check the City not displaying the error when submiting with invalid zip code.", driver);
	
			Log.message(i++ + " Functionality of Submit button is verified in the C22641 and C22722",driver);
	
			//Steps as unauthenticated user
			homePage.headers.signOut();
	
			Log.message(i++ + " Verifying the Catlogue Preference using direct URL",driver);
			driver.get("https://sfcc.qa."+brand+".plussizetech.com/on/demandware.store/Sites-fbbrands-Site/default/PrintPreferences-Show");
			Log.message(i++ + ". Catalog Preference screen can be accessed through direct link",driver);
	
			if(!Utils.isMobile()) {
				Log.softAssertThat(homePage.headers.getBreadCrumbText().equalsIgnoreCase("HOME CATALOG PREFERENCES"),
						"Breadcrum element should displayed the Home/ Catalog Preferences.", 
						"Breadcrum element is displayed the Home / Catalog Preferences.", 
						"Breadcrum element is not displayed the Home /Catalog Preferences.", driver);
			}
			else {
				Log.softAssertThat(homePage.headers.getBreadCrumbText().equalsIgnoreCase("HOME"),
						"Breadcrum element should display 'HOME'", 
						"Breadcrum element is displayed as Home", 
						"Breadcrum element is not displayed as Home", driver);
			}
	
			Log.softAssertThat(catlogPrePage.verifyCatloguePrefHeaderTextIsComingFromProperty(), 
					"Text in the Catalog Preference Label should be handled by the property catalog.header from account.properties",
					"Text in the Catalog Preference Label is handled by the property catalog.header from account.properties", 
					"Text in the Catalog Preference Label is not handled by the property catalog.header from account.properties", driver);
	
			Log.softAssertThat(catlogPrePage.verifyCatloguePrefStep1FromProperty(), 
					"Text in the Step 1 Label should be handled by the Property catalog.step1 from account.properties",
					"Text in the Step 1 Label is handled by the Property catalog.step1 from account.properties", 
					"Text in the Step 1 Label is not handled by the Property catalog.step1 from account.properties", driver);
	
			Log.softAssertThat(catlogPrePage.verifyCatloguePrefStep2FromProperty(), 
					"Text in the Step 2 Label should be handled by the Property catalog.step2 from account.properties",
					"Text in the Step 2 Label is handled by the Property catalog.step2 from account.properties", 
					"Text in the Step 2 Label is not handled by the Property catalog.step2 from account.properties", driver);
	
			Log.softAssertThat(catlogPrePage.elementLayer.verifyAttributeForElement("requestCatluge", "checked", "true", myAccountPage), 
					"By default, Request a Catalog option should pre-selected.", 
					"By default, Request a Catalog option is pre-selected.", 
					"By default, Request a Catalog option is not pre-selected.", driver);
	
			Log.message(i++ + ". Label2 name and address field validation ",driver);
	
			Log.softAssertThat(catlogPrePage.elementLayer.verifyAttributeForElement("firstName", "aria-required", "true", catlogPrePage), 
					"First Name field should be a mandatory field.", 
					"First Name field  is a mandatory field.", 
					"First Name field is not a mandatory field.", driver);
			
			Log.softAssertThat(catlogPrePage.elementLayer.verifyAttributeForElement("lastName", "aria-required", "true", catlogPrePage), 
					"Last Name field should be a mandatory field.", 
					"Last Name field  is a mandatory field.", 
					"Last Name field is not a mandatory field.", driver);
			
			Log.softAssertThat(catlogPrePage.elementLayer.verifyAttributeForElement("address1", "aria-required", "true", catlogPrePage), 
					"Address1 field should be a mandatory field.", 
					"Address1 field  is a mandatory field.", 
					"Address1 field is not a mandatory field.", driver);
			
			Log.softAssertThat(catlogPrePage.elementLayer.verifyAttributeForElement("postalCode", "aria-required", "true", catlogPrePage), 
					"PostalCode field should be a mandatory field.", 
					"PostalCode field  is a mandatory field.", 
					"PostalCode field is not a mandatory field.", driver);
			
			Log.softAssertThat(catlogPrePage.elementLayer.verifyAttributeForElement("city", "aria-required", "true", catlogPrePage), 
					"City field should be a mandatory field.", 
					"City field  is a mandatory field.", 
					"City field is not a mandatory field.", driver);
	
			catlogPrePage.clickSaveCatalogue();
			Log.softAssertThat(catlogPrePage.getErrorMessageFirstname().contains("First Name"),
					"To check the Firstname should displaying the error when submiting without values.",
					"To check the Firstname displaying the error when submiting without values!",
					"To check the Firstname not displaying the error when submiting without values", driver);
			
			Log.softAssertThat(catlogPrePage.getErrorMessageLastname().contains("Last Name"),
					"To check the Lastname should displaying the error when submiting without values.",
					"To check the Lastname displaying the error when submiting without values!",
					"To check the Lastname not displaying the error when submiting without values", driver);
			
			Log.softAssertThat(catlogPrePage.getErrorMessageAddress1().contains("Address Line1"),
					"To check the Address1 should displaying the error when submiting without values.",
					"To check the Address1 displaying the error when submiting without values!",
					"To check the Address1 not displaying the error when submiting without values", driver);
			
			Log.softAssertThat(catlogPrePage.getErrorMessageCity().contains("City"),
					"To check the City should displaying the error when submiting without values.",
					"To check the City displaying the error when submiting without values!",
					"To check the City not displaying the error when submiting without values", driver);
			
			Log.softAssertThat(catlogPrePage.getErrorMessagePostalcode().contains("Zip Code"),
					"To check the PostalCode should displaying the error when submiting without values.",
					"To check the PostalCode displaying the error when submiting without values!",
					"To check the PostalCode not displaying the error when submiting without values", driver);
			
			Log.softAssertThat(catlogPrePage.getErrorMessageState().contains("State"),
					"To check the State should displaying the error when submiting without values.",
					"To check the State displaying the error when submiting without values!",
					"To check the State not displaying the error when submiting without values", driver);
	
			catlogPrePage.enterFirstName("a");
			Log.softAssertThat(catlogPrePage.elementLayer.verifyPageElements(Arrays.asList("firstNamePlaceholder"), catlogPrePage), 
					"To check the Firstname placeholder is going up when entering value.",
					"The Firstname placeholder is going up when entering value!",
					"The Firstname placeholder is not going up when entering value", driver);
			
			catlogPrePage.enterLastName("l");
			Log.softAssertThat(catlogPrePage.elementLayer.verifyPageElements(Arrays.asList("lastNamePlaceholder"), catlogPrePage), 
					"To check the Lastname placeholder is going up when entering value.",
					"The Lastname placeholder is going up when entering value!",
					"The Lastname placeholder is not going up when entering value", driver);	
			
			catlogPrePage.enterAddress1("adress1");
			Log.softAssertThat(catlogPrePage.elementLayer.verifyPageElements(Arrays.asList("address1Placeholder"), catlogPrePage), 
					"To check the Address1 placeholder is going up when entering value.",
					"The Address1 placeholder is going up when entering value!",
					"The Address1 placeholder is not going up when entering value", driver);
			
			catlogPrePage.enterAddress2("adress1");
			Log.softAssertThat(catlogPrePage.elementLayer.verifyPageElements(Arrays.asList("address2Placeholder"), catlogPrePage), 
					"To check the Address2 placeholder is going up when entering value.",
					"The Address2 placeholder is going up when entering value!",
					"The Address2 placeholder is not going up when entering value", driver);
			
			catlogPrePage.enterPostalCode("123456");
			Log.softAssertThat(catlogPrePage.elementLayer.verifyPageElements(Arrays.asList("postalPlaceholder"), catlogPrePage), 
					"To check the Postalcode placeholder is going up when entering value.",
					"The Postalcode placeholder is going up when entering value!",
					"The Postalcode placeholder is not going up when entering value", driver);
			
			catlogPrePage.enterCity("City1");
			Log.softAssertThat(catlogPrePage.elementLayer.verifyPageElements(Arrays.asList("cityPlaceholder"), catlogPrePage), 
					"To check the City placeholder is going up when entering value.",
					"The City placeholder is going up when entering value!",
					"The City placeholder is not going up when entering value", driver);
	
			catlogPrePage.enterFirstName(invalidData);
			catlogPrePage.enterLastName(invalidData);
			catlogPrePage.enterCity("A");
			catlogPrePage.enterPostalCode(invalidData);
			catlogPrePage.clickSaveCatalogue();
			
			Log.softAssertThat(catlogPrePage.getErrorMessageFirstname().equals(dwreData.get("FirstNameError")),
					"To check the Firstname should displaying the error when submiting with invalid values.",
					"To check the Firstname displaying the error when submiting with invalid values!",
					"To check the Firstname not displaying the error when submiting with invalid values", driver);
			
			Log.softAssertThat(catlogPrePage.getErrorMessageLastname().equals(dwreData.get("LastNameError")),
					"To check the Firstname should displaying the error when submiting with invalid values.",
					"To check the Firstname displaying the error when submiting with invalid values!",
					"To check the Firstname not displaying the error when submiting with invalid values", driver);
			
			Log.softAssertThat(catlogPrePage.getErrorMessageCity().contains("Please enter at least 2 characters."),
					"To check the City should displaying the error when submiting less than 2 character.",
					"To check the City displaying the error when submiting less than 2 character.!",
					"To check the City not displaying the error when submiting less than 2 character.", driver);
			
			Log.softAssertThat(catlogPrePage.getErrorMessagePostalcode().contains("Invalid Zip"),
					"To check the City should displaying the error when submiting with invalid zip code.",
					"To check the City displaying the error when submiting with invalid zip code.!",
					"To check the City not displaying the error when submiting with invalid zip code.", driver);
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e)
		{
			Log.exception(e, driver);
		} // Ending catch block
		finally{
			Log.endTestCase(driver);
		}// Ending finally
	
	}

}// search
