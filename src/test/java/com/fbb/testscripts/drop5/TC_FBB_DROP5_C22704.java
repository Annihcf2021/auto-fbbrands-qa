package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.customerservice.EmailSubscriptionGuest;
import com.fbb.pages.customerservice.EmailSubscriptionThankyouPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22704 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;

	@Test(enabled = false, groups = { "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22704(String browser) throws Exception {
	
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
	
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.reference("***Guest email navigation is currently not implemented, using direct navigation.***");
			driver.get("https://sfcc.qa.rm.plussizetech.com/on/demandware.store/Sites-fbbrands-Site/default/EmailSubscription-Guest");
			Log.message(i++ + ". Navigated to Guest Email Preferences page", driver);
			EmailSubscriptionGuest emailSubscriptionGuest = new EmailSubscriptionGuest(driver);
	
			//Step-1: Verify the functionality of Breadcrumb
			if(!Utils.isMobile()) {
				Log.softAssertThat(emailSubscriptionGuest.verifyBreadcrumText().equalsIgnoreCase("Home Email Preferences"), 
						"Breadcrumb should be displayed as Home / Email Preferences", 
						"Breadcrumb is displayed as Home / Email Preferences", 
						"Breadcrumb is not displayed as Home / Email Preferences", driver);
			}else {
				Log.softAssertThat(emailSubscriptionGuest.verifyBreadcrumText().equalsIgnoreCase("Back Home"), 
						"Breadcrumb should be displayed as Home", 
						"Breadcrumb is displayed as Home", 
						"Breadcrumb is not displayed as Home", driver);
			}
	
			emailSubscriptionGuest.clickBreadcrumbHome();
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divMain"), homePage), 
					"When user clicks Home, they should be taken to the brand-category-root page.", 
					"When user clicks Home, they are taken to the brand-category-root page.", 
					"When user clicks Home, they are not taken to the brand-category-root page.", driver);
			driver.navigate().back();
	
			//Step-2: Verify the functionality of Banner
			if(Utils.isMobile()) {
				Log.softAssertThat(emailSubscriptionGuest.elementLayer.verifyVerticalAllignmentOfElements(driver, "breadcrumbHomeMobile", "bannerElement", emailSubscriptionGuest), 
						"The banner element should show on top of the page, underneath the breadcrumb.", 
						"The banner element shows on top of the page, underneath the breadcrumb.", 
						"The banner element doesn't show on top of the page, underneath the breadcrumb.", driver);
			}
			else {
				Log.softAssertThat(emailSubscriptionGuest.elementLayer.verifyVerticalAllignmentOfElements(driver, "breadcrumHome", "bannerElement", emailSubscriptionGuest), 
						"The banner element should show on top of the page, underneath the breadcrumb.", 
						"The banner element shows on top of the page, underneath the breadcrumb.", 
						"The banner element doesn't show on top of the page, underneath the breadcrumb.", driver);
			}
			
	
			//Step-3: Verify the functionality of Email Benefits Copy
			Log.softAssertThat(emailSubscriptionGuest.elementLayer.verifyVerticalAllignmentOfElements(driver, "bannerElement", "emailBenefitsCopy", emailSubscriptionGuest), 
					"The Email Benefits Copy should be located underneath the Banner.", 
					"The Email Benefits Copy is located underneath the Banner.", 
					"The Email Benefits Copy is not located underneath the Banner.", driver);
	
			//Step-4: Verify the functionality of Preference Options
			Log.softAssertThat(emailSubscriptionGuest.verifyNoPreferenceOptionsSelectedByDefault(), 
					"No preference options should be selected by default", 
					"No preference options is selected by default", 
					"Preference options is selected by default", driver);
	
			Log.softAssertThat(emailSubscriptionGuest.clickOnPreferenceCheckbox(), 
					"User should be able to select any preference as they are independent of each other", 
					"User is able to select any preference independent of each other", 
					"User is not able to select any preference independent of each other", driver);
	
			Log.softAssertThat(emailSubscriptionGuest.uncheckPreferenceCheckbox(), 
					"User should be able to unselect preference", 
					"User is able to unselect preference", 
					"User is not able to unselect preference", driver);
	
			//Step-5: Verify the functionality of Subscribe
			Log.softAssertThat(emailSubscriptionGuest.verifyEmailFieldEditable(), 
					"Email field should be editable", 
					"Email field is editable", 
					"Email field is not editable", driver);
	
			Log.softAssertThat(emailSubscriptionGuest.verifyEmailFieldBlankError().equalsIgnoreCase("Enter your email address"),
					"Error should be displayed when email field is left blank", 
					"Error is displayed when email field is left blank",
					"Error is not displayed when email field is left blank", driver);
	
			Log.softAssertThat(emailSubscriptionGuest.verifyEmailFieldErrorIncorrectValue().equalsIgnoreCase("Please enter a valid email."),
					"Error should be displayed when email field is passed with incorrect email", 
					"Error is displayed when email field is passed with incorrect email",
					"Error is not displayed when email field is passed with incorrect email", driver);
	
			//Step-6: Verify the functionality of Legal Copy
			Log.softAssertThat(emailSubscriptionGuest.elementLayer.verifyVerticalAllignmentOfElements(driver, "emailTextBox", "legalCopy", emailSubscriptionGuest), 
					"Legal Copy should be located underneath the email sign-up textbox.", 
					"Legal Copy is located underneath the email sign-up textbox.", 
					"Legal Copy is not located underneath the email sign-up textbox.", driver);
	
			//Step-7: Verify the functionality of Questions and Answers
			Log.softAssertThat(emailSubscriptionGuest.verifyExpandedState(), 
					"Question should be ExpandedState", 
					"Question is ExpandedState", 
					"Question is not ExpandedState", driver);
	
			Log.softAssertThat(emailSubscriptionGuest.verifyCollapsedState(), 
					"Question should be collapsed state", 
					"Question is collapsed state", 
					"Question is not collapsed state", driver);
	
			EmailSubscriptionThankyouPage emailSubscriptionThankyouPage = emailSubscriptionGuest.navigateToEmailSubscriptionThankyouPage();
			Log.message("thankyou");
			Log.softAssertThat(emailSubscriptionThankyouPage.elementLayer.verifyPageElements(Arrays.asList("thankyouMessage"), emailSubscriptionThankyouPage),
					"User should be landed in email subscription thankyou page",
					"User is landed in email subscription thankyou page",
					"User is not landed in email subscription thankyou page", driver);
			Log.testCaseResult();
	
		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}//TC_FBB_DROP5_C22704

}// search
