package com.fbb.testscripts.drop5;

import java.util.Arrays;
import java.util.LinkedHashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22687 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22687(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		
		{
			GlobalNavigation.registerNewUser(driver, 2, 0, email + "|" + password);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			MyAccountPage myAcc = headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged in to user account.", driver);
			
			AddressesPage addrPage = myAcc.navigateToAddressPage();
			Log.message(i++ + ". Navigated to address page.", driver);
			
			//Step-1: Verify the functionality of Add New Address
			Log.softAssertThat(!(addrPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnAddNewAddress"), addrPage) ^ addrPage.getNonDefaultAddressesCount() > 0), 
					"Add New Address should be enabled only when there is at least 1 address is already saved", 
					"Add New Address is enabled only when there is at least 1 address is already saved", 
					"Add New Address is not enabled only when there is at least 1 address is already saved", driver);
			
			Log.softAssertThat(addrPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnAddNewAddress", "addressDefaultIndicator", addrPage), 
					"Add New Address should be displayed right of the Default Address Label.", 
					"Add New Address is displayed right of the Default Address Label.", 
					"Add New Address is not displayed right of the Default Address Label.", driver);
			
			addrPage.clickOnAddNewAddress();
			Log.message(i++ + ". Clicked on Add New Address.", driver);
			
			Log.softAssertThat(addrPage.verifyAddressFormLocation(), 
					"System should display the Add New Address module in the second address card position and push the second address tile down.", 
					"System displays the Add New Address module in the second address card position and pushes the second address tile down.", 
					"System does not display the Add New Address module in the second address card position and push the second address tile down.", driver);
			
			Log.softAssertThat(addrPage.elementLayer.verifyCssPropertyForElement("btnAddNewAddress", "cursor", "not-allowed", addrPage), 
					"When the add new address module is open then system should then display the add new Address link in a disabled state", 
					"The add new Address link is in disabled state", 
					"The add new Address link is not in disabled state", driver);
			
			//Step-2: Verify the functionality of New Address Module
			Log.softAssertThat(addrPage.elementLayer.verifyElementDisplayed(Arrays.asList("inputAddressRequired","inputAddressFirstName","inputAddressLastName",
									"inputAddressPhone","inputAddressLine1","inputAddressLine2","inputAddressPostal","inputAddressCity"), addrPage)
							&& (addrPage.elementLayer.verifyElementDisplayed(Arrays.asList("drpState","drpCountry"), addrPage)
							|| addrPage.elementLayer.verifyElementDisplayed(Arrays.asList("selectState","selectCountry"), addrPage)), 
					"Correct address fields should be displayed.", 
					"Correct address fields are displayed.", 
					"Correct address fields are not displayed.", driver);
			
			Log.softAssertThat(addrPage.verifyPlaceHolderComingFromProperty(), 
					"Address fields should be displayed with correct placeholders.", 
					"Address fields are displayed with correct placeholders.", 
					"Address fields are not displayed with correct placeholders.", driver);
						
			//Step-3: Verify the functionality of Default Address Checkbox
			Log.softAssertThat(addrPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "inputAddressCity", "checkBoxDefaultAddress", addrPage), 
					"Default Address Checkbox should be displayed below the New Address Module.", 
					"Default Address Checkbox is displayed below the New Address Module.", 
					"Default Address Checkbox is not displayed below the New Address Module.", driver);
			
			Log.softAssertThat(addrPage.verifyCheckboxChecked("checkBoxDefaultAddress", addrPage)==false, 
					"By default, the Default Address Checkbox should be unchecked", 
					"The Default Address Checkbox is unchecked by default", 
					"The Default Address Checkbox is not unchecked by default", driver);
			
			int noAddressSaved1 = addrPage.getNonDefaultAddressesCount();
			
			LinkedHashMap<String, String> addressFilled1 = addrPage.fillingAddressDetails("address_3", true);
			Log.message(i++ + ". Filled in address details.", driver);
			
			addrPage.checkUncheckMakeDefaultCheckbox(false, addrPage);
			Log.message(i++ + ". Unchecked default checkbox.", driver);
			
			Log.softAssertThat(addrPage.verifyCheckboxChecked("checkBoxDefaultAddress", addrPage)==false, 
					"User should be able to uncheck the checkbox if the user does not wants to make the newly saved address card as default.", 
					"User is able to uncheck the make default.", 
					"User is not able to uncheck the make default.", driver);
			
			addrPage.checkUncheckMakeDefaultCheckbox(true, addrPage);
			Log.message(i++ + ". Checked default checkbox", driver);
			addrPage.clickSaveAddress();
			Log.message(i++ + ". Clicked Save on new address module.", driver);
			
			Log.softAssertThat(addrPage.getNickNameOfDefaultAddress().equalsIgnoreCase(addressFilled1.get("AddressTitle")), 
					"If user checks the Default Address Checkbox, then that particular address card should become the default address once created and saved", 
					"New address is saved as default", 
					"New address is not saved as default", driver);
			
			Log.softAssertThat(addrPage.getNickNameOfAnAddress(1).equalsIgnoreCase(addressFilled1.get("AddressTitle")), 
					"Should move the respective address card to the first position on page load", 
					"Address is moved to first position.", 
					"Address is not moved to first position.", driver);
			
			Log.softAssertThat(addrPage.verifyDefaultAddressIndicatorPosition(), 
					"Default address card should be displayed first with the default address indicator (Green dot)", 
					"Default address card is displayed first with the default address indicator (Green dot)", 
					"Default address card is not displayed first with the default address indicator (Green dot)", driver);
			
			//Step-4: Verify the functionality of Cancel
			addrPage.clickOnAddNewAddress();
			Log.message(i++ + ". Clicked on Add New Address.", driver);
			
			Log.softAssertThat(addrPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "checkBoxDefaultAddress", "btnAddAddressCancel", addrPage), 
					"Cancel should be displayed below the Default Address Checkbox.", 
					"Cancel is displayed below the Default Address Checkbox.", 
					"Cancel is not displayed below the Default Address Checkbox.", driver);
			
			addrPage.clickCancelAddressCreation();
			Log.message(i++ + ". Clicked on cancel button on new address module.", driver);
			
			Log.softAssertThat(!addrPage.elementLayer.verifyElementDisplayed(Arrays.asList("divAddressForm"), addrPage), 
					"On clicking the Cancel button, the system should close the Add New Address Module.", 
					"On clicking the Cancel button, the system closes the Add New Address Module.", 
					"On clicking the Cancel button, the system does not close the Add New Address Module.", driver);
			
			Log.softAssertThat(!(addrPage.getNonDefaultAddressesCount() > 0 ^ addrPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnAddNewAddress"), addrPage)), 
					"System should enable the add new card link.", 
					"System enabled the add new card link.", 
					"System did not enable the add new card link.", driver);
			
			//Step-5: Verify the functionality of Save
			addrPage.clickOnAddNewAddress();
			Log.message(i++ + ". Clicked on Add New Address.", driver);
			
			Log.softAssertThat(addrPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "buttonSaveAddress", "btnAddAddressCancel", addrPage), 
					"Save button should be displayed right of the Cancel.", 
					"Save button is displayed right of the Cancel.", 
					"Save button is not displayed right of the Cancel.", driver);
			
			addrPage.clickCancelAddressCreation();
			
			int noAddressSaved2 = addrPage.getNonDefaultAddressesCount();
			Log.softAssertThat(noAddressSaved2 > noAddressSaved1, 
					"If user has entered data in all the fields, System should add the currently saved address card to the customer profile.", 
					"System added the currently saved address card to the customer profile.", 
					"System did not add the currently saved address card to the customer profile.", driver);
			
			Log.softAssertThat(addrPage.verifyEditDeleteSavedCards(), 
					"Saved address card should have the fields Edit and Delete at the bottom of the respective address cardas.", 
					"Saved address card have the fields Edit and Delete at the bottom of the respective address cardas.", 
					"Saved address card do not have the fields Edit and Delete at the bottom of the respective address cardas.", driver);
			
			Log.softAssertThat(addrPage.verifyMakeDefaultButton(), 
					"Saved non-default address card should have the field Make Default at the bottom of the respective address cardas.", 
					"Saved non-default address card have the field Make Default at the bottom of the respective address cardas.", 
					"Saved non-default address card do not have the field Make Default at the bottom of the respective address cardas.", driver);
			
			//Step-1 cont.
			addrPage.deleteAllTheAddresses();
			Log.message(i++ + ". Deleted all saved addresses.", driver);
			
			Log.softAssertThat(!(addrPage.getNonDefaultAddressesCount() > 0 ^ addrPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnAddNewAddress"), addrPage)), 
					"Add New Address should be enabled only when there is at least 1 address is already saved", 
					"Add New Address is enabled only when there is at least 1 address is already saved", 
					"Add New Address is not enabled only when there is at least 1 address is already saved", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.removeAllSavedAddress(driver);
			Log.endTestCase(driver);
		} // finally
	} // M1_FBB_DROP5_C22687

}
