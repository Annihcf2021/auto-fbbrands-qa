package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlatinumCardApplication;
import com.fbb.pages.PlatinumCardLandingPage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22764 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22764(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		String email = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		
		{
			GlobalNavigation.registerNewUser(driver, 1, 0, email + "|" + password);
		}
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			headers.navigateToMyAccount(email, password);
			Log.message(i++ + ". Logged in to pre-screened non approved user account", driver);
			
			Footers footers = homePage.footers;
			PlatinumCardLandingPage platinumLanding = footers.navigateToPlatinumCreditCardLanding();
			Log.message(i++ + ". Navigated to PLCC landing page.", driver);
			
			Log.softAssertThat(platinumLanding.elementLayer.verifyElementWithinElement(driver, "applyButton", "divBannerElementAlt", platinumLanding), 
					"Apply Now button should be displayed in the Banner available in the Top Content Slot", 
					"Apply Now button is displayed in the Banner available in the Top Content Slot", 
					"Apply Now button is not displayed in the Banner available in the Top Content Slot", driver);
			
			PlatinumCardApplication platinumApply = platinumLanding.clickOnApplyButton();
			Log.message(i++ + ". Navigated to PLCC application page.", driver);
			
			Log.softAssertThat(platinumApply.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("linkSeeBenefits"), platinumApply)
							&& platinumApply.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divContactDisclaimer"), platinumApply), 
					"On clicking Apply Now, user should be navigated to the Non-Pre Approved PLCC Application page", 
					"On clicking Apply Now, user is navigated to the Non-Pre Approved PLCC Application page", 
					"On clicking Apply Now, user is not navigated to the Non-Pre Approved PLCC Application page", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	} // M1_FBB_DROP5_C22764

}
