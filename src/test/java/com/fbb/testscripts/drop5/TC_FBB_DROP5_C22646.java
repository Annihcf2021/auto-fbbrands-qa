package com.fbb.testscripts.drop5;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.CatalogPreferencePage;
import com.fbb.pages.account.EmailPreferencePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.headers.Headers;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP5_C22646 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "medium", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP5_C22646(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String username = accountData.getProperty("credential_plcc_preapproved_all").split("\\|")[0];
		String password = accountData.getProperty("credential_plcc_preapproved_all").split("\\|")[1];	    
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			Headers headers = homePage.headers;
			MyAccountPage myAccount=homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to 'My Account page ",driver);
	
			myAccount.clickOnWishListLink();
			Log.message(i++ + ". Navigated to 'Wishlist'. ", driver);
	
			//Step-2: Verify the functionality of Breadcrumb
			if (Utils.isMobile()) {
				headers.clickBackArrowOnBreadCrumbMobile();
			} else {
				headers.clickNthBreadcrumb(0);
				Log.message(i++ + ". Clicked on breadcrumb 'Home'. ", driver);
	
				Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage),					 
						"When User clicks on Home in breadcrumb 'Homepage' should get displayed", 
						"When User clicks on Home in breadcrumb 'Homepage' is displayed", 
						"When User clicks on Home in breadcrumb 'Homepage' is not displayed", driver);
			}
			homePage.navigateToMyAccount();
			Log.message(i++ + ". Navigated to 'My Account'. ", driver);
	
			Log.softAssertThat(myAccount.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAccount),					 
					"When User navigate to my account Overview page should be displayed", 
					"When User navigate to my account Overview page is displaying", 
					"When User navigate to my account Overview page is not displaying", driver);
	
			if (Utils.isDesktop() || Utils.isTablet()) {
				headers.clickNthBreadcrumb(0);
				Log.message(i++ + ". Clicked on breadcrumb 'Home'. ", driver);
	
				Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage),					 
						"When User clicks on Breadcrumb Home from Overview page System should navigate to Homepage", 
						"When User clicks on Breadcrumb Home from Overview page System is navigating to Homepage", 
						"When User clicks on Breadcrumb Home from Overview page System is not navigating to Homepage", driver);
			}
	
			
			//Step-1: Verify the functionality of My Account Navigation and Active State
			homePage.navigateToMyAccount();
			Log.message(i++ + ". Navigated to 'My Account'. ", driver);
	
			ProfilePage profilePage = myAccount.navigateToUpdateProfile();
			Log.message(i++ + ". Navigated to 'Profile'. ", driver);
	
			Log.softAssertThat(profilePage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), profilePage),					 
					"Check profile page gets displayed", 
					"The profile page is getting displayed", 
					"The profile page is not getting displayed", driver);
	
			if (Utils.isMobile()) {
				headers.clickBackArrowOnBreadCrumbMobile();
			} else {
				headers.clickNthBreadcrumb(1);
			}
			Log.message(i++ + ". Clicked on breadcrumb 'My Account'. ", driver);
	
			Log.softAssertThat(myAccount.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAccount),					 
					"Check myaccount is navigated when clicking on 'My account' in breadcrumb", 
					"Myaccount is navigated when clicking on 'My account' in breadcrumb", 
					"myaccount is not navigated when clicking on 'My account' in breadcrumb", driver);
	
			if (Utils.isDesktop() || Utils.isTablet()) {
				headers.clickNthBreadcrumb(0);
				Log.message(i++ + ". Clicked on breadcrumb 'Home'. ", driver);
	
				Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage),					 
						"When User clicks on Breadcrumb Home from profile page System should navigate to Homepage", 
						"When User clicks on Breadcrumb Home from profile page System is navigating to Homepage", 
						"When User clicks on Breadcrumb Home from profile page System is not navigating to Homepage", driver);
			}
	
			homePage.navigateToMyAccount();
			Log.message(i++ + ". Navigated to 'My Account'. ", driver);
	
			AddressesPage addressesPage = myAccount.navigateToAddressPage();
			Log.message(i++ + ". Navigated to 'Address'. ", driver);
	
			Log.softAssertThat(addressesPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), addressesPage),					 
					"Check address page is navigated", 
					"Addresses page is navigated", 
					"Addresses page is not navigated", driver);
	
			if (Utils.isMobile()) {
				headers.clickBackArrowOnBreadCrumbMobile();
			} else {
				headers.clickNthBreadcrumb(1);
			}
			Log.message(i++ + ". Clicked on breadcrumb 'My Account'. ", driver);
	
			Log.softAssertThat(myAccount.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAccount),					 
					"When User navigate to my account Overview page should be displayed", 
					"When User navigate to my account Overview page is displaying", 
					"When User navigate to my account Overview page is not displaying", driver);
	
			if (Utils.isDesktop() || Utils.isTablet()) {
				headers.clickNthBreadcrumb(0);
				Log.message(i++ + ". Clicked on breadcrumb 'Home'. ", driver);
	
				Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage),					 
						"When User clicks on Breadcrumb Home from addresses page System should navigate to Homepage", 
						"When User clicks on Breadcrumb Home from addresses page System is navigating to Homepage", 
						"When User clicks on Breadcrumb Home from addresses page System is not navigating to Homepage", driver);
			}
	
			homePage.navigateToMyAccount();
			Log.message(i++ + ". Navigated to 'My Account'. ", driver);
	
			OrderHistoryPage orderHistoryPg = myAccount.clickOnOrderHistoryLink();
			Log.message(i++ + ". Navigated to 'Order History'. ", driver);
	
			Log.softAssertThat(orderHistoryPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderHistoryPg),					 
					"Check order history page is navigated", 
					"Order history page is navigated", 
					"Order history page is not navigated", driver);
	
			if (Utils.isMobile()) {
				headers.clickBackArrowOnBreadCrumbMobile();
			} else {
				headers.clickNthBreadcrumb(1);
			}
			Log.message(i++ + ". Clicked on breadcrumb 'My Account'. ", driver);
	
			Log.softAssertThat(myAccount.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAccount),					 
					"When User navigate to my account Overview page should be displayed", 
					"When User navigate to my account Overview page is displaying", 
					"When User navigate to my account Overview page is not displaying", driver);
	
			if (Utils.isDesktop() || Utils.isTablet()) {
				headers.clickNthBreadcrumb(0);
				Log.message(i++ + ". Clicked on breadcrumb 'Home'. ", driver);
	
				Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage),					 
						"When User clicks on Breadcrumb Home from order-history page System should navigate to Homepage", 
						"When User clicks on Breadcrumb Home from order-history page System is navigating to Homepage", 
						"When User clicks on Breadcrumb Home from order-history page System is not navigating to Homepage", driver);
			}
	
			homePage.navigateToMyAccount();
			Log.message(i++ + ". Navigated to 'My Account'. ", driver);
	
			EmailPreferencePage EmailPrefPg = myAccount.clickOnEmailPrefLink();
			Log.message(i++ + ". Navigated to 'Email Preference'. ", driver);
	
			Log.softAssertThat(EmailPrefPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), EmailPrefPg),					 
					"Check Email Pref page is navigated", 
					"Email Pref page is navigated", 
					"Email Pref page is not navigated", driver);
	
			if (Utils.isMobile()) {
				headers.clickBackArrowOnBreadCrumbMobile();
			} else {
				headers.clickNthBreadcrumb(1);
			}
			Log.message(i++ + ". Clicked on breadcrumb 'My Account'. ", driver);
	
			Log.softAssertThat(myAccount.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAccount),					 
					"When User navigate to my account Overview page should be displayed", 
					"When User navigate to my account Overview page is displaying", 
					"When User navigate to my account Overview page is not displaying", driver);
	
			if (Utils.isDesktop() || Utils.isTablet()) {
				headers.clickNthBreadcrumb(0);
				Log.message(i++ + ". Clicked on breadcrumb 'Home'. ", driver);
	
				Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage),					 
						"When User clicks on Breadcrumb Home from order-history page System should navigate to Homepage", 
						"When User clicks on Breadcrumb Home from order-history page System is navigating to Homepage", 
						"When User clicks on Breadcrumb Home from order-history page System is not navigating to Homepage", driver);
			}
	
			homePage.navigateToMyAccount();
			Log.message(i++ + ". Navigated to 'My Account'. ", driver);
	
			CatalogPreferencePage CatalogPrefPg = myAccount.clickOnCatalogPrefLink();
			Log.message(i++ + ". Navigated to 'Catalog Preference'. ", driver);
	
			Log.softAssertThat(CatalogPrefPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), CatalogPrefPg),					 
					"Check Catalog Pref page is navigated", 
					"Catalog Pref page is navigated", 
					"Catalog Pref page is not navigated", driver);
	
			if (Utils.isMobile()) {
				headers.clickBackArrowOnBreadCrumbMobile();
			} else {
				headers.clickNthBreadcrumb(1);
			}
			Log.message(i++ + ". Clicked on breadcrumb 'My Account'. ", driver);
	
			Log.softAssertThat(myAccount.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAccount),					 
					"When User navigate to my account Overview page should be displayed", 
					"When User navigate to my account Overview page is displaying", 
					"When User navigate to my account Overview page is not displaying", driver);
	
			if (Utils.isDesktop() || Utils.isTablet()) {
				headers.clickNthBreadcrumb(0);
				Log.message(i++ + ". Clicked on breadcrumb 'Home'. ", driver);
	
				Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage),					 
						"When User clicks on Breadcrumb Home from order-history page System should navigate to Homepage", 
						"When User clicks on Breadcrumb Home from order-history page System is navigating to Homepage", 
						"When User clicks on Breadcrumb Home from order-history page System is not navigating to Homepage", driver);
			}
	
			homePage.navigateToMyAccount();
			Log.message(i++ + ". Navigated to 'My Account'. ", driver);
	
			PaymentMethodsPage PaymentMethodPg = myAccount.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to 'Payment methods'. ", driver);
	
			Log.softAssertThat(PaymentMethodPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), PaymentMethodPg),					 
					"Check Payment Method page is navigated", 
					"Payment Method page is navigated", 
					"Payment Method page is not navigated", driver);
	
			if (Utils.isMobile()) {
				headers.clickBackArrowOnBreadCrumbMobile();
			} else {
				headers.clickNthBreadcrumb(1);
			}
			Log.message(i++ + ". Clicked on breadcrumb 'My Account'. ", driver);
	
			Log.softAssertThat(myAccount.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAccount),					 
					"When User navigate to my account Overview page should be displayed", 
					"When User navigate to my account Overview page is displaying", 
					"When User navigate to my account Overview page is not displaying", driver);
	
			if (Utils.isDesktop() || Utils.isTablet()) {
				headers.clickNthBreadcrumb(0);
				Log.message(i++ + ". Clicked on breadcrumb 'Home'. ", driver);
	
				Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage),					 
						"When User clicks on Breadcrumb Home from order-history page System should navigate to Homepage", 
						"When User clicks on Breadcrumb Home from order-history page System is navigating to Homepage", 
						"When User clicks on Breadcrumb Home from order-history page System is not navigating to Homepage", driver);
			}
	
			//Step-3: Verify the functionality of other components available in the Public View Authenticated Users Page
			Log.message("Verify the functionality of other components--Covered functionality in the test case: C22628 Step 1, 3 to 13");
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}

}// search
