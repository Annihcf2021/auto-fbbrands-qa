package com.fbb.testscripts.drop2;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlpPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.CollectionUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19659 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "stable", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19659(String browser) throws Exception {
		Log.testCaseInfo();
		
		String category1 = TestData.get("level-2").split("\\|")[0];
		String category2 = TestData.get("level-2").split("\\|")[1];
		String refinement = "Price";
		List<String> refinementList = Arrays.asList();

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		
		int i = 1;
		try {
			//Load Home Page
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			//Step-1: Verify 'Filter By' menu and 'Sort By' in product list page
			PlpPage plpPage = homePage.headers.navigateTo(category1, category2);
			Log.message(i++ + ". Navigated to Product Listing Page for " + category1, driver);

			Log.softAssertThat(plpPage.refinements.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("drpFilterByCollapsed","drpSortByCollapseMobileTablet"), plpPage.refinements),
					"'Filter By' menu and 'Sort By' menu should be displayed in Horizontal refinement bar",
					"'Filter By' menu and 'Sort By' menu displayed in Horizontal refinement bar",
					"'Filter By' menu or/and 'Sort By' menu not displayed in Horizontal refinement bar", driver);

			//Step-2: Verify the behavior of 'Filter By' menu
			Log.softAssertThat(plpPage.refinements.getFilterByState().equals("collapsed"),
					"Filter By menu should be in closed state by default",
					"Filter By menu in closed state by default",
					"Filter By menu in opened state by default", driver);

			//Step-3: Click on downward arrow in Filter By menu and verify attribute refinements
			plpPage.refinements.clickFilterArrowDown();

			Log.message(i++ + ". Clicked on Filter By menu.", driver);

			Log.softAssertThat(plpPage.refinements.getFilterByState().equals("expanded") && 
					plpPage.refinements.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("lstRefinementFilterOptionsMobile"), plpPage.refinements),
					"The 'Filter By' menu should be opened and Refinements should be displayed!",
					"The 'Filter By' menu opened and Refinements displayed!",
					"The 'Filter By' menu not working as Expected", driver);

			homePage.footers.scrollToFooter();
			Log.message(i++ + ". Scrolled to Footer.", driver);
			Log.softAssertThat(homePage.footers.elementLayer.verifyElementDisplayed(Arrays.asList("footerPanel"), homePage.footers),
					"Leaving the Fly out in open state, the user should be able to scroll to the end of the page",
					"The user can able to scroll to the end of the page",
					"The user not able to scroll to the end of the page", driver);

			plpPage.refinements.elementLayer.scrollToViewElement("drpFilterByCollapsed", plpPage.refinements);

			//Step-4: Verify the attribute refinements are configured according to the sequence in the storefront catalog
			Log.softAssertThat(CollectionUtils.compareTwoList1(plpPage.refinements.getRefinementList(), refinementList),
					"Refinements should be listed according to the configured sequence in the storefront catalog.",
					"Refinements listed according to the configured sequence in the storefront catalog.",
					"Refinements not listed according to the configured sequence in the storefront catalog.", driver);

			//Step-6,7,8: Verify Clear All button below the attribute Refinements
			int prdCnt1 = plpPage.refinements.getProductCount();
			plpPage.refinements.selectSubRefinementInFilterBy(refinement, 1);
			Log.message(i++ + ". Filtered by "+ refinement, driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(plpPage.refinements.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkRefinementMenuBackMobile"), plpPage.refinements),
						"Back Button should be displayed at the bottom of the selected Refinement box.",
						"Back Button is displayed at the bottom of the selected Refinement box",
						"Back Button is not displayed at the bottom of the selected Refinement box", driver);
			} else {
				Log.softAssertThat(plpPage.refinements.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lblSelectedSubRefCount", "iconReturnToMainRefinementInMobile", plpPage.refinements),
						"Back button should be displayed before Refinement name in Sub refinement menu",
						"Back button displayed as expected",
						"Back button not displayed as expected", driver);
			}
			
			Log.softAssertThat(plpPage.refinements.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divExpandedRefinementSubListTablet"), plpPage.refinements),
					"Values of the selected Refinement should be displayed in the Refinement box",
					"Values of the selected Refinement displayed in the Refinement box",
					"Values of the selected Refinement not displayed in the Refinement box", driver);
			
			int prdCnt2 = plpPage.refinements.getProductCount();
			Log.message(i++ + ". First Refinement in Refinement(" + refinement + ") selected.", driver);

			if(BrandUtils.isBrand(Brand.rm) && (Utils.getRunBrowser(driver).toLowerCase().contains("safari") == false))
			{
				driver.navigate().refresh();

				plpPage.refinements.openFilterBy();
				Log.message(i++ + ". Clicked on Filter By menu.", driver);

				plpPage.refinements.selectRefinementInFilterBy(refinement);
				Log.message(i++ + ". Refinement(" + refinement + ") clicked in FilterBy.", driver);

			}
			
			plpPage.refinements.returnToMainRefineMentInFilterBy();

			Log.softAssertThat(plpPage.refinements.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("lstRefinementFilterOptionsInMobile"), plpPage.refinements),
					"Tap on the 'Back button', user redirected to refinements configured page.",
					"User redirected to refinements configured page as expected",
					"User not redirected to refinements configured page as expected", driver);

			if(Utils.isTablet())
			{
			Log.softAssertThat(plpPage.refinements.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnClearAllInTablet"), plpPage.refinements),
					"Clear All button should be displayed",
					"Clear All button displayed",
					"Clear All button not displayed", driver);
			}
			Log.event("Available Product count before refinement selection :: " + prdCnt1);
			Log.event("Available Product count after refinement selection :: " + prdCnt2);

			Log.softAssertThat(prdCnt1 != prdCnt2,
					"Available product count should be updated.",
					"Available Product count updated.",
					"Available product count not updated.", driver);

			//Step-9: Verify clear button right side of the Refinement box
			
			plpPage.refinements.openCloseFilterBy("expanded");
			Log.message(i++ + ". Filter is expanded", driver);
			
			if(Utils.isTablet()) {
				Log.softAssertThat(plpPage.refinements.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnClearInActiveRefInTablet"), plpPage.refinements),
						"Clear button should be displayed for active refinement",
						"Clear button displayed for active refinement",
						"Clear button not displayed for active refinement", driver);
			}
			if(Utils.isMobile()) {
				Log.softAssertThat(plpPage.refinements.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnClearInActiveRefInMobile"), plpPage.refinements),
						"Clear button should be displayed for active refinement",
						"Clear button displayed for active refinement",
						"Clear button not displayed for active refinement", driver);
			}
			
			plpPage.refinements.openCloseFilterBy("collapsed");
			Log.message(i++ + ". Filter is collapsed", driver);

			prdCnt1 = plpPage.refinements.getProductCount();
			plpPage.refinements.clickOnClearInSubRefinement();
			prdCnt2 = plpPage.refinements.getProductCount();

			Log.softAssertThat(prdCnt1 != prdCnt2,
					"Available product count should be updated.",
					"Product count updated as Expected",
					"Product count not updated as Expected", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_DROP2_C19659


}// search


