package com.fbb.testscripts.drop2;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.QuickShop;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19669 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19669(String browser) throws Exception {
		Log.testCaseInfo();
	
		String lvlClearence = TestData.get("level_with_clearence");
		String lvlClearence_SKU = TestData.get("level_clearence_SingleSKU");
		String lvlMoreColor = TestData.get("level_with_moreColor"); 
		String lvl1 = "";
		String lvl2 = "";
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!",driver);
			
			PlpPage plpPage = null;
			
			lvl1 = lvlClearence.split("\\|")[0];
			lvl2 = lvlClearence.split("\\|")[1];
			
			if(Utils.isDesktop()) {
				if (lvl2.equalsIgnoreCase("view all")) {
					plpPage = homePage.headers.navigateTo(lvl1);
				} else {
					plpPage = homePage.headers.navigateTo(lvl1, lvl2);
				}
			} else {
				plpPage = homePage.headers.navigateTo(lvl1, lvl2);
			}
			Log.message(i++ + ". Navigated to PLP Page with Search Keyword",driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProductName","txtProductImages",
					"txtSingleProdPrice","txtClearencePromoMessage","txtSwatchList"), plpPage),
					"'Product name','Product image','Product Price','Clearance message','Swatch list' should get displayed",
					"'Product name','Product image','Product Price','Clearance message','Swatch list' is getting displayed",
					"'Product name','Product image','Product Price','Clearance message','Swatch list' is not getting displayed", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtClearencePromoMessage"), plpPage),
					"If the discount % is the different across SKUs then, 'Clearance: Up to X% off' message should be displayed",
					"'Clearance: Up to X% off' message is displayed when the discount % is the different across SKUs",
					"'Clearance: Up to X% off' message is not displayed when the discount % is the different across SKUs", driver);
			
			lvl1 = lvlClearence_SKU.split("\\|")[0];
			lvl2 = lvlClearence_SKU.split("\\|")[1];
			
			if(Utils.isDesktop()) {
				if (lvl2.equalsIgnoreCase("view all")) {
					plpPage = homePage.headers.navigateTo(lvl1);
				} else {
					plpPage = homePage.headers.navigateTo(lvl1, lvl2);
				}
			} else {
				plpPage = homePage.headers.navigateTo(lvl1, lvl2);
			}
			Log.message(i++ + ". Navigated to PLP Page with Search Keyword",driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementTextContains("txtClearencePromoMessage", "up to", plpPage),
					"If the discount % is the same across SKUs then, 'Clearance: X% off' message should be displayed",
					"'Clearance: X% off' message is displayed when the discount % is the same across SKUs",
					"'Clearance: X% off' message is not displayed when the discount % is the same across SKUs", driver);
			
			lvl1 = lvlMoreColor.split("\\|")[0];
			lvl2 = lvlMoreColor.split("\\|")[1];
			
			if(Utils.isDesktop()) {
				if (lvl2.equalsIgnoreCase("view all")) {
					plpPage = homePage.headers.navigateTo(lvl1);
				} else {
					plpPage = homePage.headers.navigateTo(lvl1, lvl2);
				}
			} else {
				plpPage = homePage.headers.navigateTo(lvl1, lvl2);
			}
			Log.message(i++ + ". Navigated to PLP Page with More Color.",driver);
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkMoreColors_Desk_Tab"), plpPage),
						"'More colors link' should get displayed",
						"'More colors link' is getting displayed",
						"'More colors link' is not getting displayed", driver);
			} else {
				Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkMoreColors_Mobile"), plpPage),
						"'More colors link' should get displayed",
						"'More colors link' is getting displayed",
						"'More colors link' is not getting displayed", driver);
			}
			
			if(Utils.isDesktop()) {
				QuickShop quickShop = plpPage.clickOnQuickShop();
				Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("divQuickShop"), quickShop),
						"Quick shop should be displayed in PLP page",
						"Quick shop is getting displayed",
						"Quick shop is not getting displayed", driver);
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// M1_FBB_DROP2_C19669
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_DROP2_C19669(String browser) throws Exception {
		Log.testCaseInfo();
	
		String lvlPromoMsg = TestData.get("level_with_promomsg");
		/*String lvlPromoMsg = TestData.get("level_with_promomsg");
		String lvlBadge = TestData.get("level_with_badge");
		String lvlPrdFeatureMsg = TestData.get("level_with_prodfeaturemsg");
		String lvlTypeOfPrice = TestData.get("level_with_typesOfPrice");*/
		String lvl1 = "";
		String lvl2 = "";
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!",driver);
			
			PlpPage plpPage = null;
			
			lvl1 = lvlPromoMsg.split("\\|")[0];
			lvl2 = lvlPromoMsg.split("\\|")[1];
			
			if(Utils.isDesktop()) {
				if (lvl2.equalsIgnoreCase("view all")) {
					plpPage = homePage.headers.navigateTo(lvl1);
				} else {
					plpPage = homePage.headers.navigateTo(lvl1, lvl2);
				}
			} else {
				plpPage = homePage.headers.navigateTo(lvl1, lvl2);
			}
			Log.message(i++ + ". Navigated to PLP Page with Search Keyword",driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProdPromoMessage"), plpPage),
					"'Promotion call out message' should get displayed",
					"'Promotion call out message' is getting displayed",
					"'Promotion call out message' is not getting displayed", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductImages", "txtProductName", plpPage),
					"Product images should get displayed above the product name",
					"Product images is displayed above the product name",
					"Product images is not displayed above the product name", driver);
			
			Object[] obj = plpPage.checkProdImgIsEqualToColorSwatchImage();
			
			boolean primaryImgIsEqual = (boolean) obj[0];
			
			Log.softAssertThat(primaryImgIsEqual,
					"First image of the color variation selected in the category assignment should be displayed",
					"First image of the color variation selected is getting displayed",
					"First image of the color variation selected is not displayed", driver);
			
			String prodImgInPLP = plpPage.getProdImgInPLPOfAProduct(1);
			
			PdpPage pdpPage = plpPage.navigateToPdp(1);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage),
					"Product detail page should be navigated",
					"Product detail page is navigated",
					"Product detail page is not navigated", driver);
			
			System.out.println(prodImgInPLP +"---"+ pdpPage.getProdImageName());
			Log.softAssertThat(pdpPage.getProdImageName().contains(prodImgInPLP),
					"First image of the color variation selected in the category assignment should be displayed",
					"First image of the color variation selected in the category assignment is displayed",
					"First image of the color variation selected in the category assignment is not displayed", driver);
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// M2_FBB_DROP2_C19669
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M3_FBB_DROP2_C19669(String browser) throws Exception {
		Log.testCaseInfo();
	
		String lvlBadge = TestData.get("level_with_badge");
		/*String lvlPrdFeatureMsg = TestData.get("level_with_prodfeaturemsg");
		String lvlTypeOfPrice = TestData.get("level_with_typesOfPrice");*/
		String lvl1 = "";
		String lvl2 = "";
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!",driver);
			
			PlpPage plpPage = null;
			
			lvl1 = lvlBadge.split("\\|")[0];
			lvl2 = lvlBadge.split("\\|")[1];
			String prod = lvlBadge.split("\\|")[2];
			String badge = lvlBadge.split("\\|")[3];
			
			if(Utils.isDesktop()) {
				if (lvl2.equalsIgnoreCase("view all")) {
					plpPage = homePage.headers.navigateTo(lvl1);
				} else {
					plpPage = homePage.headers.navigateTo(lvl1, lvl2);
				}
			} else {
				plpPage = homePage.headers.navigateTo(lvl1, lvl2);
			}
			Log.message(i++ + ". Navigated to PLP Page with Search Keyword",driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("imgProductBadge"), plpPage),
					"'Product badge' should get displayed",
					"'Product badge' is getting displayed",
					"'Product badge' is not getting displayed", driver);
			
			Log.softAssertThat(plpPage.verifyProdBadgeImgInParticularProd(prod, badge),
					"Check the 'New' badge is displaying correctly",
					"The 'New' badge is getting displayed",
					"The 'New' badge is not getting displayed", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// M3_FBB_DROP2_C19669
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M4_FBB_DROP2_C19669(String browser) throws Exception {
		Log.testCaseInfo();
	
		String lvlPrdFeatureMsg = TestData.get("level_with_prodfeaturemsg");
		String lv1SpecialPrdMsg = TestData.get("level_with_specialprdmsg");
		
		/*
		String lvlTypeOfPrice = TestData.get("level_with_typesOfPrice");*/
		String lvl1 = "";
		String lvl2 = "";
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!",driver);
			
			PlpPage plpPage = null;
			
			lvl1 = lvlPrdFeatureMsg.split("\\|")[0];
			lvl2 = lvlPrdFeatureMsg.split("\\|")[1];
			String prod = lvlPrdFeatureMsg.split("\\|")[2];
			
			if(Utils.isDesktop()) {
				if (lvl2.equalsIgnoreCase("view all")) {
					plpPage = homePage.headers.navigateTo(lvl1);
				} else {
					plpPage = homePage.headers.navigateTo(lvl1, lvl2);
				}
			} else {
				plpPage = homePage.headers.navigateTo(lvl1, lvl2);
			}
			Log.message(i++ + ". Navigated to PLP Page with Search Keyword", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProdFeatureMsg"), plpPage),
					"Product feature message should get displayed",
					"Product feature message is getting displayed",
					"Product feature message is not getting displayed", driver);
			
			if(BrandUtils.isBrand(Brand.ks)) {
				Log.reference("Swatch should display curvy in PLP Page - Verification not applicable for KingSize");
			}else {
				Log.softAssertThat(plpPage.elementLayer.verifyCssPropertyForElement("lnkSwatchColor", "border-radius", TestData.get("swatchCurvy"), plpPage),
						"Swatch should display curvy in PLP Page",
						"Swatch is displaying curvy in PLP Page",
						"Swatch is not displaying curvy in PLP Page", driver);	
			}
			
			PdpPage pdpPage = plpPage.navigateToPdp(prod);
			Log.message(i++ + ". Opened Product page.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("pdpMain"), pdpPage),
					"Product detail page should be navigated",
					"Product detail page is navigated",
					"Product detail page is not navigated", driver);
			
			lvl1 = lv1SpecialPrdMsg.split("\\|")[0];
			lvl2 = lv1SpecialPrdMsg.split("\\|")[1];
			prod = lv1SpecialPrdMsg.split("\\|")[2];
			
			if(Utils.isDesktop()) {
				if (lvl2.equalsIgnoreCase("view all")) {
					plpPage = homePage.headers.navigateTo(lvl1);
				} else {
					plpPage = homePage.headers.navigateTo(lvl1, lvl2);
				}
			} else {
				plpPage = homePage.headers.navigateTo(lvl1, lvl2);
			}
			Log.message(i++ + ". Navigated to PLP Page with Search Keyword",driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtSpecialProductMsg"), plpPage),
					"Special product message should get displayed",
					"Special product message is getting displayed",
					"Special product message is not getting displayed", driver);
			
			pdpPage = plpPage.navigateToPdp(prod);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("pdpMain"), pdpPage),
					"Product detail page should be navigated",
					"Product detail page is navigated",
					"Product detail page is not navigated", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// M4_FBB_DROP2_C19669
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M5_FBB_DROP2_C19669(String browser) throws Exception {
		Log.testCaseInfo();
	
		String lvlTypeOfPrice = TestData.get("level_with_typesOfPrice");
		String lvl1 = "";
		String lvl2 = "";
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!",driver);
			
			PlpPage plpPage = null;
			
			lvl1 = lvlTypeOfPrice.split("\\|")[0];
			lvl2 = lvlTypeOfPrice.split("\\|")[1];
			
			if(Utils.isDesktop()) {
				if (lvl2.equalsIgnoreCase("view all")) {
					plpPage = homePage.headers.navigateTo(lvl1);
				} else {
					plpPage = homePage.headers.navigateTo(lvl1, lvl2);
				}
			} else {
				plpPage = homePage.headers.navigateTo(lvl1, lvl2);
			}
			Log.message(i++ + ". Navigated to PLP Page with Search Keyword",driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProductStdPrice","txtProductSalesPrice",
					"txtProductActualSalesPrice"), plpPage),
					"Different types of Product should be displayed",
					"Different types of Product is displayed",
					"Different types of Product is not displayed", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtPriceRange"), plpPage),
					"Product Sales price should have range",
					"Product Sales price is having range",
					"Product Sales price is not having range", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// M5_FBB_DROP2_C19669
}// search