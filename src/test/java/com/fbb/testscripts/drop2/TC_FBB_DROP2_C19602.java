package com.fbb.testscripts.drop2;

import java.util.Arrays;
import com.fbb.reusablecomponents.TestData;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SearchResultPage;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.CollectionUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19602 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19602(String browser) throws Exception {
		Log.testCaseInfo();

		//Load Test Data
		String testSearchKey = TestData.get("product_search_terms");
		String searchKey1 = testSearchKey.split("\\|")[0];
		String searchKey2 = testSearchKey.split("\\|")[1];
		String searchKey3 = testSearchKey.split("\\|")[2];
		String searchKey4 = testSearchKey.split("\\|")[3];
		int rowSizeWide = StringUtils.getNumberInString(TestData.get("plp_rowSize").split("\\|")[0]) + 1;		//SLP row size for desktop is 1 larger than PLP row size 
		int rowSizeMedium = StringUtils.getNumberInString(TestData.get("plp_rowSize").split("\\|")[1]);

		List<String> elementsToBeVerified = Arrays.asList("txtSeacondarySearch", "secondarySearchIcon", "lblresultCountAboveSecondarySearchBox",
				"refinementSection", "lblSortByOptionSelected", "sectionSearchResult");

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			//Step-1: Verify entering a search term by clicking on Search icon
			SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey1);
			Log.message(i++ + ". Enetered the search term in the Search Textbox!", driver);

			Log.softAssertThat(searchResultPage.elementLayer.verifyElementDisplayedWithoutScrolling(elementsToBeVerified, searchResultPage),
					"the Search results page should be displayed with necessary contents.",
					"The Search results page, is displayed with necessary contents", 
					"The Search results page, is not displayed with necessary contents.", driver);
			
			BrowserActions.scrollToTopOfPage(driver);

			Log.softAssertThat(searchResultPage.elementLayer.verifyElementDisplayedBelow("refinementSection","lblSortByOptionSelected",searchResultPage),
					"Sort by dropdown in the Search results page, should be displayed below the refinement menu.",
					"Sort by dropdown in the Search results page, displayed below the refinement menu.", 
					"Sort by dropdown in the Search results page, not displayed below the refinement menu.", driver);

			Log.softAssertThat(searchResultPage.verifySortByDisplayedToTheRightOfResultCount(),
					"Sort by dropdown in the Search results page, should be displayed to the right of result count!",
					"Sort by dropdown in the Search results page, displayed to the right of result count!",
					"Sort by dropdown in the Search results page, not displayed to the right of result count!", driver);

			//Step-2: Verify Primary search box displayed in search result page
			Log.softAssertThat(homePage.headers.getEnteredTextFromSearchTextBox().equalsIgnoreCase(searchKey1),
					"The Search term initiated by the user should be displayed in the primary search box",
					"The Search term initiated by the user is displayed in the primary search box", 
					"The Search term initiated by the user is not displayed in the primary search box", driver);

			searchResultPage = homePage.headers.searchProductKeyword(searchKey2);
			Log.message(i++ + ". Enetered the new search term in the Search Textbox!", driver);

			List<String> actualText = searchResultPage.getProductNamesFromSearchResults();
			Log.message(i++ + ". Got the product names of all the products displayed in the search results!", driver);

			Log.softAssertThat(actualText.toString().toLowerCase().contains(searchKey2.toLowerCase()),
					"Products in the Search results page should be relevant to the search term.!",
					"Products in the Search results page is relevant to the search term!",
					"Products in the Search results page is not relevant to the search term!", driver);
			
			//Step-3: Verify secondary search box displayed in search result page
			Log.softAssertThat(searchResultPage.getTextFromSecondarySearchBox().equalsIgnoreCase(searchKey2),
					"The Search term in the secondary search box should be the same as in the primary search box",
					"The Search term in the secondary search box is the same as in the primary search box", 
					"The Search term in the secondary search box is not the same as in the primary search box", driver);

			searchResultPage = searchResultPage.secondarySearchProductKeyword(searchKey1);
			Log.message(i++ + ". Enetered the new search term in the Secondary Search Textbox!", driver);

			Log.softAssertThat(searchResultPage.verifySecondarySearchIconIsRightToSecondarySearchBox(),
					"The Search icon should be displayed to the right of the Secondary search box",
					"The Search icon is displayed to the right of the Secondary search box", 
					"The Search icon is not displayed to the right of the Secondary search box", driver);

			actualText = searchResultPage.getProductNamesFromSearchResults();
			Log.message(i++ + ". Got the product names of all the products displayed in the search results!", driver);

			Log.softAssertThat(actualText.toString().toLowerCase().contains(searchKey1.toLowerCase()),
					"Products in the Search results page should be relevant to the search term.!",
					"Products in the Search results page is relevant to the search term!",
					"Products in the Search results page is not relevant to the search term!", driver);

			searchResultPage.clearSecondarySearchText();
			Log.message(i++ + ". cleared the search term in the Secondary Search Textbox!");

			Log.softAssertThat(searchResultPage.getTextFromSecondarySearchBox().equals(""),
					"The Search term in the secondary search box should be cleared",
					"The Search term in the secondary search box is cleared", 
					"The Search term in the secondary search box is not cleared", driver);

			searchResultPage = homePage.headers.searchProductKeyword(searchKey2);
			Log.message(i++ + ". Enetered the new search term in the Search Textbox!");

			//Step-4: Verify Result Count displayed in the search result page
			Log.softAssertThat(searchResultPage.elementLayer.verifyElementDisplayedBelow("lblresultCountAboveSecondarySearchBox","txtSeacondarySearch",searchResultPage),
					"Result count in the Search results page, should be displayed above the secondary search box.",
					"Result count in the Search results page, displayed above the secondary search box.", 
					"Result count in the Search results page, not displayed above the secondary search box.", driver);

			Log.reference(i++ + ". As per ticket PXSFCC-2484, the result count below the refinment menu is removed.");
			
			//Step-5: Verify search icon near secondary search box with second search string
			searchResultPage = searchResultPage.secondarySearchProductKeyword(searchKey3);
			Log.message(i++ + ". Enetered the two search terms in the Search Textbox!", driver);
			
			String searchKey3_1 = searchKey3.split("\\,")[0].trim();
			String searchKey3_2 = "";
			if(searchKey3.split("\\,").length >1)
				searchKey3_2 = searchKey3.split("\\,")[1].trim();

			actualText = searchResultPage.getProductNamesFromSearchResults();
			Log.message(i++ + ". Got the product names of all the products displayed in the search results!", driver);

			Log.softAssertThat(actualText.toString().toLowerCase().contains(searchKey3_1.toLowerCase())
							&& actualText.toString().toLowerCase().contains(searchKey3_2.toLowerCase()),
					"Products in the Search results page should be relevant to the search term.!",
					"Products in the Search results page is relevant to the search term!",
					"Products in the Search results page is not relevant to the search term!", driver);

			Log.softAssertThat(searchResultPage.elementLayer.verifyElementDisplayedBelow("lblresultCountAboveSecondarySearchBox","txtSeacondarySearch",searchResultPage),
					"Result count in the Search results page, should be displayed above the secondary search box.",
					"Result count in the Search results page, displayed above the secondary search box.", 
					"Result count in the Search results page, not displayed above the secondary search box.", driver);
			
			//Step-6: Verify search icon near secondary search box with empty search string
			List<String> actualTextBefore = searchResultPage.getProductNamesFromSearchResults();
			homePage.headers.searchProductKeyword("");
			List<String> actualTextAfter = searchResultPage.getProductNamesFromSearchResults();
			Log.softAssertThat(CollectionUtils.compareTwoList1(actualTextBefore, actualTextAfter), 
					"Searched with empty seacrh string should keep user on same page", 
					"User remains on the same page.", 
					"User did not remains on the same page.", driver);
			
			//Step-7: Verify search icon near secondary search box
			Log.softAssertThat(searchResultPage.verifySecondarySearchIconIsRightToSecondarySearchBox(),
					"The Search icon should be displayed to the right of the Secondary search box",
					"The Search icon is displayed to the right of the Secondary search box", 
					"The Search icon is not displayed to the right of the Secondary search box", driver);
			
			//Step-8: Scroll the mouse from search result page
			Log.softAssertThat(searchResultPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("refinementSection"), searchResultPage),
					"The refinement section should change to a sticky bar while scrolling page!",
					"The refinement section changes to a sticky bar while scrolling page!",
					"The refinement section does not change to a sticky bar while scrolling page!", driver);
			
			//Step-9: Verify Sort by bar in search result page
			searchResultPage.scrollToTopOfPage();
			Log.message(i++ + ". Scrolled to top of the page!");

			Log.message(i++ + ". As per comments in PXSFCC-3388, Sort By default option is not pre-determined. It can be set to any of the options available.");
			
			//Step-10: Verify product grid in search result page.
			searchResultPage = homePage.headers.searchProductKeyword(searchKey2);
			Log.message(i++ + ". Enetered the new search term in the Secondary Search Textbox!");

			int screenResolution = driver.manage().window().getSize().width;
			if (screenResolution > 1024) {
				Log.softAssertThat(searchResultPage.getNumberOfProductTilesPerRow() == rowSizeWide,
						"The number of product tiles displayed per row should be " + rowSizeWide,
						"The number of product tiles displayed per row is " + rowSizeWide,
						"The number of product tiles displayed per row is not equal to " + rowSizeWide, driver);
			} else if (screenResolution <= 1024) {
				Log.softAssertThat(searchResultPage.getNumberOfProductTilesPerRow() == rowSizeMedium,
						"In BH,The number of product tiles displayed per row should be " + rowSizeMedium,
						"In BH,The number of product tiles displayed per row is " + rowSizeMedium,
						"In BH,The number of product tiles displayed per row is not equal to " + rowSizeMedium, driver);
			}
			
			searchResultPage = homePage.headers.searchProductKeyword(searchKey4);
			int resultCount = searchResultPage.getSearchResultCount();
			Log.reference("getSearchResultCount:: " + resultCount);
			if(resultCount >=60) {
				Log.softAssertThat(searchResultPage.verifyNumberOfProductTilesDisplayed(60),
						"In BH,The number of product tiles displayed in a page should be 60!",
						"In BH,The number of product tiles displayed in a page is 60!",
						"In BH,The number of product tiles displayed in a page is not equal to 60!", driver);
			}else {
				Log.softAssertThat(searchResultPage.verifyNumberOfProductTilesDisplayed(resultCount),
						"The number of product tiles displayed in a page should be " + resultCount,
						"The number of product tiles displayed in a page is " + resultCount,
						"The number of product tiles displayed in a page is not equal to " + resultCount, driver);
			}

			elementsToBeVerified = Arrays.asList("leftHandNavigation");
			Log.softAssertThat(searchResultPage.elementLayer.verifyPageElementsDoNotExist(elementsToBeVerified, searchResultPage),
					"The left hand navigation should not be displayed in Search results page.",
					"The left hand navigation is not displayed in Search results page", 
					"The left hand navigation is displayed in Search results page.", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_DROP2_C19602


}// search


