package com.fbb.testscripts.drop2;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlpPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19620 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "high", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19620(String browser) throws Exception {
		Log.testCaseInfo();
		
		String category1 = TestData.get("level-1");
		String refinement = TestData.get("filter_options").split("\\|")[0];

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			//Navigate to PLP Page for given category
			PlpPage plpPage = homePage.headers.navigateTo(category1);
			Log.message(i++ + ". Navigated to PLP page for category :: " + category1, driver);

			Log.softAssertThat(plpPage.getRefinementList().isEmpty() == false,
					"Refinement options should be displayed.",
					"Refinement options are displayed.",
					"Refinement options are not displayed.", driver);

			if(Utils.getRunBrowser(driver).equals("firefox")){
				Log.softAssertThat(plpPage.elementLayer.verifyCssPropertyForListElement("lstRefinementFilterOptionsInMobile", "background", "transparent", plpPage)
								&& plpPage.elementLayer.verifyCssPropertyForListElement("lstRefinementFilterOptionsInMobile", "color", "rgba(0, 0, 0, 1)", plpPage),
						"By default, refinement name should be displayed with White back ground with black text.",
						"Refinement name displayed as expected!",
						"Refinement name not displayed as Expected.", driver);
			}
			else{
				Log.softAssertThat(plpPage.elementLayer.verifyCssPropertyForListElement("lstRefinementFilterOptionsInMobile", "background", "rgba(0, 0, 0, 0)", plpPage)
								&& plpPage.elementLayer.verifyCssPropertyForListElement("lstRefinementFilterOptionsInMobile", "color", "rgba(0, 0, 0, 1)", plpPage),
						"By default, refinement name should be displayed with White back ground with black text.",
						"Refinement name displayed as expected!",
						"Refinement name not displayed as Expected.", driver);
			}

			Log.softAssertThat(plpPage.elementLayer.verifyPageListElements(Arrays.asList("lstRefinementDownArrow"), plpPage),
					"In horizontal refinement near attribute name downward arrow should be display.",
					"Downward arrow mark icon displayed!",
					"Downward arrow mark icon not displayed", driver);

			//Step-2: Click on downward arrow mark near the attribute name
			plpPage.refinements.clickOnRefinementToggle(refinement);
			Log.message(i++ + ". Clicking on Refinement :: " + refinement, driver);

			if(Utils.getRunBrowser(driver).toLowerCase().contains("edge")){
				Log.fail("The refinement is not expeneded if resolution is 1024x768 or less : PXSFCC-3827");

			}
			
			Log.softAssertThat(plpPage.refinements.getRefinementStatus(refinement).equals("expanded"),
					"System should be expand the refinement to display the values",
					"System expanded and refinement displays the values",
					"System did not expanded.", driver);

			if (BrandUtils.isBrand(Brand.rm)) {
				Log.softAssertThat(plpPage.elementLayer.verifyElementColor("drpExpandedRefinement","rgba(255, 255, 255, 1)", plpPage),
						"In horizontal refinement selected attribute name should be displayed with black ground with white text",
						"In horizontal refinement selected attribute name displayed with black ground with white text",
						"In horizontal refinement selected attribute name not displayed with black ground with white text", driver);

			} else {
				Log.softAssertThat(plpPage.elementLayer.verifyElementColor("drpExpandedRefinement", "rgba(255, 255, 255, 1)", plpPage),
						"In horizontal refinement selected attribute name should be displayed with black ground with white text",
						"In horizontal refinement selected attribute name displayed with black ground with white text",
						"In horizontal refinement selected attribute name not displayed with black ground with white text", driver);
			}

			Log.softAssertThat(plpPage.refinements.getRefinementArrowState(refinement).get(1).equals("upward"),
					"In horizontal refinement near selected attribute name upward arrow mark should be shown.",
					"In horizontal refinement near selected attribute name upward arrow mark shown.",
					"In horizontal refinement near selected attribute name upward arrow mark not shown.", driver);

			Log.softAssertThat(plpPage.elementLayer.verifyPageListElements(Arrays.asList("lblRefinementCountDesktop"), plpPage),
					"System should display the count of applied refinement values next to the refinement heading",
					"System displays the count of applied refinement values next to the refinement heading",
					"System does not displays the count of applied refinement values next to the refinement heading", driver);

			//Step-3: Select sub categories values from the drop down box.
			String selectedSubRef1 = plpPage.refinements.selectRandomSubRefinementOf(refinement);			
			Log.message(i++ + ". Clicking Sub-Refinement(" + selectedSubRef1 + ") on Refinement" + refinement);

			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("lblResultCountDesktop"), plpPage),
					"Available product count should be displayed below the horizontal refinement pane.",
					"Available product count displayed below the horizontal refinement pane.",
					"Available product count not displayed below the horizontal refinement pane.", driver);

			//Step-4: Select multiple sub categories values from dropdown box
			String selectedSubRef2 = plpPage.refinements.selectSubRefinementOf(refinement,1);			
			Log.message(i++ + ". Clicking Sub-Refinement(" + selectedSubRef1 + "," + selectedSubRef2 + ") on Refinement" + refinement, driver);

			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("lblResultCountDesktop"), plpPage),
					"Available product count should be displayed below the horizontal refinement pane.",
					"Available product count displayed below the horizontal refinement pane.",
					"Available product count not displayed below the horizontal refinement pane.", driver);

			//Step-5: Click on upward arrow mark near attribute name by without uncheck any selected value
			List<String> prdListBeforeClosing = plpPage.getProductNames();
			int prdCountBeforeClosing = plpPage.getProductTileCount();
			plpPage.refinements.closeOpenedRefinement();
			List<String> prdListAfterClosing = plpPage.getProductNames();
			int prdCountAfterClosing = plpPage.getProductTileCount();
			//Log.message(i++ + ". Refinement Dropdown closed!", driver);

			Log.softAssertThat(plpPage.refinements.verifyRefinementSection(refinement).equals("closed"),
					"Opened refinement dropdown box should be properly closed",
					"Opened refinement dropdown box properly closed",
					"Opened refinement dropdown box not closed", driver);

			Log.softAssertThat(prdListBeforeClosing.equals(prdListAfterClosing),
					"Products should be remains as same in product list page",
					"Products remains as same in product list page",
					"Products changed in product list page", driver);

			Log.softAssertThat(prdCountBeforeClosing == prdCountAfterClosing,
					"Available product count should remain as same in product list page",
					"Available product count remains as same in product list page",
					"Available product count changed in product list page", driver);

			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// M1_FBB_DROP2_C19620

}// TC_FBB_DROP2_C19620


