package com.fbb.testscripts.drop2;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19591 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	@Test(groups = { "critical", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19591(String browser) throws Exception {
		Log.testCaseInfo();
	
		String level1 = TestData.get("level-1");
		String level1_breadcrumb = TestData.get("level-1_breadcrumb");
		String rowSize = TestData.get("plp_rowSize"); //desktop|tablet|mobile
	
		List<String> elementsToBeVerified = Arrays.asList("divBreadcrumb", "divContentSlotHeader", "divHorizontalRefinement", 
														"drpSortBySelectedDesktop","txtResultCount", "divVerticalRefinement");
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
	
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
	
			PlpPage plpPage = homePage.headers.navigateTo(level1);
			Log.message(i++ + ". Navigated to Product listing Page!", driver);
	
			//1: Verify the available components in Product Listing Page
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(elementsToBeVerified, plpPage),
					"The Plp page elements should be displayed!",
					"The Plp page elements are displayed!",
					"The Plp page elements are not displayed!", driver);
	
			if(plpPage.getProductTileCount() > 60)
			{
				elementsToBeVerified = Arrays.asList("lstPageNos");
				Log.softAssertThat(plpPage.elementLayer.VerifyPageListElementDisplayed(elementsToBeVerified, plpPage),
						"The pagination should be displayed!",
						"The pagination is displayed!",
						"The pagination is not displayed!", driver);
			}
			else
				Log.reference("The number of products in page not greater than 60. Hence pagination will not be displayed");
	
			//2: Verify display formatting of products on listing page
			int screenResolution = driver.manage().window().getSize().width;
			if(screenResolution >= 1400)
			{
				int widthDesktop = StringUtils.getNumberInString(rowSize.split("\\|")[0]);
				Log.softAssertThat(plpPage.getNumberOfProductTilesPerRow() == widthDesktop,
						"The number of product tiles displayed per row should be " + widthDesktop,
						"The number of product tiles displayed per row is " + widthDesktop,
						"The number of product tiles displayed per row is not " + widthDesktop, driver);
			}
			else if(screenResolution >= 1024)
			{
				int widthTablet = StringUtils.getNumberInString(rowSize.split("\\|")[1]);
				Log.softAssertThat(plpPage.getNumberOfProductTilesPerRow() == widthTablet,
						"The number of product tiles displayed per row should be " + widthTablet,
						"The number of product tiles displayed per row is " + widthTablet,
						"The number of product tiles displayed per row is not " + widthTablet, driver);
			}
	
			//3: Verify the display of Breadcrumb in the Product Listing Page
			List<String> breadcrumb = plpPage.getBreadcrumbText();
			Log.message("Got the breadcrumb text!", driver);
	
			Log.softAssertThat(breadcrumb.get(0).trim().equalsIgnoreCase("HOME"),
					"The brand category root should be displayed as 'HOME'!",
					"The brand category root is displayed as 'HOME'!",
					"The brand category root is not displayed as 'HOME'!", driver);
			
			Log.softAssertThat(breadcrumb.get(breadcrumb.size()-1).trim().equalsIgnoreCase(level1_breadcrumb),
					"The category name "+ level1_breadcrumb +" should be displayed as breadcrumb value!",
					"The category name "+ level1_breadcrumb +" is displayed as breadcrumb value!",
					"The category name "+ level1_breadcrumb +" is not displayed. " + breadcrumb.get(1).trim() + " is displayed", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayedBelow("divBreadcrumb","divHorizontalRefinement",plpPage),
					"Breadcrumb should be displayed above the refinement menu.",
					"Breadcrumb is displayed above the refinement menu.", 
					"Breadcrumb is not displayed above the refinement menu.", driver);
			
			//4: Verify the Content Slot | Slot ID cat-banner
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayedBelow("divContentSlotHeader","divHorizontalRefinement",plpPage),
					"Content Slot Header should be displayed above the refinement menu.",
					"Content Slot Header is displayed above the refinement menu.", 
					"Content Slot Header is not displayed above the refinement menu.", driver);
			
			//5, 6, 7: Verify the Content Slot | Slot ID cat-body-1,2,3. Covered in Step 1
			
			BrowserActions.scrollToBottomOfPage(driver);
			//8: Verify the display of 'Top' button 
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnBackToTop"), plpPage), 
					"The Top button should be displayed.",
					"The Top button is displayed.", 
					"The Top button is not displayed.", driver);
			
			//Step 9: Main image update based on the selected color.
			Log.message("Step-9 cannot be automated because of unavailable feed in environment. Step disabled");
			/*Log.softAssertThat(false,
					"Product image should update based on the selected color.", 
					"Product image is updated based on the selected color.",
					"Product image is not updated based on the selected color.",driver);*/
			
			//step 11: Mouse hover on top of the Main image
			plpPage.mouseHoverProductByIndex(1);
			Log.softAssertThat(plpPage.verifyQuickShopButtonDisplayed(1),
					"Quickshop button should displayed when mouseover on tiles", 
					"Quickshop button is displayed when mouseover on tiles",
					"Quickshop button is displayed when mouseover on tiles",driver);
			
			//step 10: Click on product main image or product name
			PdpPage pdppage = plpPage.navigateToPdp(1);
			Log.softAssertThat(pdppage.elementLayer.verifyPageElements(Arrays.asList("cntPdpContent"), pdppage), 
				"Navigated to PDP page after clicking the tiles", 
				"Naivgated to PDP page after clicking the tiles", 
				"Not navigated to PDP page after clicking the tiles", driver);
	
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_DROP2_C19591


}// search


