package com.fbb.testscripts.drop2;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19617 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	//private static EnvironmentPropertiesReader prdData = EnvironmentPropertiesReader.getInstance("data");

	@Test(groups = { "medium", "stable", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19617(String browser) throws Exception {
		Log.testCaseInfo();

		//Load Test data
		String searchKey = TestData.get("Pagination");
		String level1 = TestData.get("level-1");
		String level2 = "View All";

		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);

		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			//Navigating to Product List Page for given category level-1
			if(searchKey.equals("") || searchKey.equals(null)){
				PlpPage plpPage = homePage.headers.navigateTo(level1, level2);
				Log.message(i++ + ". Navigated to PLP page for category :: " + level1 + " >>> " + level2, driver);

				int resultCount = plpPage.getSearchResultCount();
				if(resultCount <= 60) {
					Log.fail("The category you have entered is not applicable for this test cases. Please try with category which have products more than 60.");
				}

				//Step-1: Scroll down the mouse from Product list page
				plpPage.scrollToViewMore();
				Log.message(i++ + ". Scrolled down to ViewMore button", driver);

				int count1 = plpPage.getProductTileCount();
				Log.message("Product currently displayed on page: " + count1);
				Log.softAssertThat(plpPage.elementLayer.verifyPageListElements(Arrays.asList("btnViewMore"), plpPage),
						"\"View More\" link should be displayed below the product list.",
						"\"View More\" link should be displayed below the product list!",
						"\"View More\" link not displayed below the product list.", driver);

				//Step-2: Click on View More link (1st time) below the product grid.
				plpPage.clickViewMoreOrLessLnk();
				Log.message(i++ + ". ViewMore button clicked.", driver);
				int count2 = plpPage.getProductTileCount();
				Log.message("Product displayed on page after clikcing View More: " + count2);
				Log.softAssertThat(count2 > count1,
						"Page should be reloaded and next set of products should be displayed in Product list page.",
						"Page reloaded and next set of products should be displayed in Product list page",
						"Page did not reload with more product", driver);

				if(count2 < resultCount){
					//Step-3: Click on View More link (2nd time) below the product grid.
					plpPage.scrollToViewMore();
					plpPage.clickViewMoreOrLessLnk();
					Log.message(i++ + ". ViewMore button clicked.", driver);
					int count3 = plpPage.getProductTileCount();
					Log.message("Product displayed on page after clikcing View More: " + count3);
					Log.softAssertThat(count3 > count2,
							"Page should be reloaded and next set of products should be displayed in Product list page.",
							"Page reloaded and next set of products should be displayed in Product list page",
							"Page did not reload with more product", driver);
					if(count3 < resultCount){
						plpPage.scrollToViewMore();
						//Step-4: Click on "View More" link (3rd time) below the product grid.
						plpPage.clickViewMoreOrLessLnk();
						Log.message(i++ + ". ViewMore button clicked.", driver);
						int count4 = plpPage.getProductTileCount();
						Log.message("Product displayed on page after clikcing View More: " + count4);
						Log.softAssertThat(count4 > count3,
								"Page should be reloaded and next set of products should be displayed in Product list page.",
								"Page reloaded and next set of products should be displayed in Product list page",
								"Page did not reload with more product", driver);
					}else{
						Log.message("No More Products...");
					}
				}else{
					Log.message("No More Products...");
				}
				//Step-4(2): "View More" link should be disabled in the product list
				Log.message("The View More button should not be displayed if number of products is less than 60");

				Log.softAssertThat(plpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnViewMoreButton"), plpPage),
						"\"View More\" link should be disabled in the product list",
						"\"View More\" link disabled in the product list",
						"\"View More\" link not disabled in the product list", driver);
			}else{
				SearchResultPage searchResultPage = homePage.headers.searchProductKeyword(searchKey);
				Log.message(i++ + ". Navigated to PLP page for category :: " + level1, driver);

				int resultCount = searchResultPage.getSearchResultCount();
				if(resultCount <= 60) {
					Log.fail("The category you have entered is not applicable for this test cases. Please try with category which have products more than 60.");
				}

				//Step-1: Scroll down the mouse from Product list page
				searchResultPage.scrollToViewMore();
				Log.message(i++ + ". Scrolled down to ViewMore button", driver);

				int count1 = searchResultPage.getProductTileCount();
				Log.message("Product currently displayed on page: " + count1);
				
				Log.softAssertThat(searchResultPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "searchResultContent", "btnViewMoreButton", searchResultPage), 
						"\"View More\" link should be displayed below the product list.",
						"\"View More\" link is displayed below the product list!",
						"\"View More\" link not displayed below the product list.", driver);

				//Step-2: Click on View More link (1st time) below the product grid.
				searchResultPage.scrollToViewMore();
				searchResultPage.clickViewMoreOrLessLnk();
				Log.message(i++ + ". View More button clicked.", driver);
				int count2 = searchResultPage.getProductTileCount();
				Log.message("Product displayed on page after clikcing View More: " + count2);
				Log.softAssertThat(count2 > count1,
						"Page should be reloaded and next set of products should be displayed in Product list page.",
						"Page reloaded and next set of products should be displayed in Product list page",
						"Page did not reload with more product", driver);

				if(count2 < resultCount){
					searchResultPage.scrollToViewMore();
					//Step-3: Click on View More link (2nd time) below the product grid.
					searchResultPage.clickViewMoreOrLessLnk();
					Log.message(i++ + ". View More button clicked.", driver);
					int count3 = searchResultPage.getProductTileCount();
					Log.message("Product displayed on page after clikcing View More: " + count3);
					Log.softAssertThat(count3 > count2,
							"Page should be reloaded and next set of products should be displayed in Product list page.",
							"Page reloaded and next set of products should be displayed in Product list page",
							"Page did not reload with more product", driver);
					if(count3 < resultCount){
						searchResultPage.scrollToViewMore();
						//Step-4: Click on "View More" link (3rd time) below the product grid.
						searchResultPage.clickViewMoreOrLessLnk();
						Log.message(i++ + ". ViewMore button clicked.", driver);
						int count4 = searchResultPage.getProductTileCount();
						Log.message("Product displayed on page after clikcing View More: " + count4);
						Log.softAssertThat(count4 > count3,
								"Page should be reloaded and next set of products should be displayed in Product list page.",
								"Page reloaded and next set of products should be displayed in Product list page",
								"Page did not reload with more product", driver);
					}else{
						Log.message("No More Products...");
					}
				}else{
					Log.message("No More Products...");
				}
				
				//Step-4(2): "View More" link should be disabled in the product list
				Log.message("The View More button should not be displayed if number of products is less than 60.");

				Log.softAssertThat(searchResultPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnViewMoreButton"), searchResultPage),
						"\"View More\" link should be disabled in the product list",
						"\"View More\" link disabled in the product list",
						"\"View More\" link not disabled in the product list", driver);
			}
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally

	}// TC_FBB_DROP2_C19617


}// search


