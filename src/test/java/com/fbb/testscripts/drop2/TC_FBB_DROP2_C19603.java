package com.fbb.testscripts.drop2;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.Refinements;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.CollectionUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;
@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19603 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	@Test(groups = { "critical", "mobile", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19603(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		// Load test data
		String[] searchTerm = TestData.get("product_search_terms").split("\\|");
		String searchPlaceholder = TestData.get("searchbox_placeholder");
		String[] filterList = TestData.get("filter_options").split("\\|");
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			//Step-1: Tap on search icon near brand name
			int widthSearchBoxBefore= 0, widthSearchBoxAfter= 0;
			
			headers.clickOnSearchIcon();
			Log.message(i++ + ". Clicked on search icon", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnHeaderSearchIcon", "divSearchBox", headers), 
					"Search box should be displayed below the search icon.", 
					"Search box is displayed below the search icon.", 
					"Search box is not displayed below the search icon.", driver);
			
			widthSearchBoxBefore = StringUtils.getNumberInString(headers.elementLayer.getElementCSSValue("divSearchBox", "width", headers));
			headers.clickFlyoutSearchButton();
			Log.message(i++ + ". Clicked on search icon.", driver);
			widthSearchBoxAfter = StringUtils.getNumberInString(headers.elementLayer.getElementCSSValue("divSearchBox", "width", headers));
			
			Log.softAssertThat(widthSearchBoxAfter > widthSearchBoxBefore,
					"Search box should be expand horizontally.",
					"Search box expands horizontally.", 
					"Search box does not expand horizontally.", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyAttributeForElement("txtSearchMobile", "placeholder", searchPlaceholder, headers), 
					"Search box placeholder should display as " + searchPlaceholder, 
					"Search box placeholder is displayed as " + searchPlaceholder, 
					"Search box placeholder is not displayed as " + searchPlaceholder, driver);
			
			//Step-2: Enter a search term (ex: Dress) click on Search icon
			SearchResultPage slp = headers.navigateToSLP(searchTerm[1]);
			Log.message(i++ + ". Searched for " + searchTerm[1], driver);
			Refinements refinements = slp.refinements;
			
			if(!Utils.isMobile()) {
				widthSearchBoxAfter = StringUtils.getNumberInString(headers.elementLayer.getElementCSSValue("divSearchBox", "width", headers));
				Log.softAssertThat(widthSearchBoxBefore == widthSearchBoxAfter, 
						"After search execution, the search should collaps back to the original state.", 
						"After search execution, the search collapses back to the original state.", 
						"After search execution, the search does not collaps back to the original state.", driver);
			}else {
				Log.softAssertThat(!headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("txtSearch"), headers), 
						"After search execution, the search should collaps back to the original state.", 
						"After search execution, the search collapses back to the original state.", 
						"After search execution, the search does not collaps back to the original state.", driver);
			}
			
			Log.softAssertThat(slp.elementLayer.verifyAttributeForElement("secondarySearchSection", "value", searchTerm[1], slp), 
					"Should Displays the search term initiated by the user in secondary search box.", 
					"Displays the search term initiated by the user in secondary search box.", 
					"Does not displays the search term initiated by the user in secondary search box.", driver);
			
			//Step-3: Verify the refinement bar in search result page
			Log.softAssertThat(slp.verifySecondarySearchIsAboveRefinementBar(), 
					"Horizontal refinement bar should be displayed below the secondary search box", 
					"Horizontal refinement bar is displayed below the secondary search box", 
					"Horizontal refinement bar is not displayed below the secondary search box", driver);
			
			Log.softAssertThat(slp.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divRefinementFilter"), slp)
							&& slp.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divRefinementSortBy"), slp), 
					"\"Filter By\" and  \"Sort By\" should be displayed in horizontal refinement pane", 
					"\"Filter By\" and  \"Sort By\" is displayed in horizontal refinement pane", 
					"\"Filter By\" and  \"Sort By\" is not displayed in horizontal refinement pane", driver);
			
			int widthRefinements = StringUtils.getNumberInString(refinements.elementLayer.getComputedCssValueForElement("refinementSection", "width", refinements));
			int widthFilter = StringUtils.getNumberInString(refinements.elementLayer.getComputedCssValueForElement("drpFilterByCollapsed", "width", refinements));
			int widthSortBy = StringUtils.getNumberInString(refinements.elementLayer.getComputedCssValueForElement("drpSortByCollapseMobileTablet", "width", refinements));
			
			Log.softAssertThat(widthRefinements == widthFilter + widthSortBy, 
					"Refinement bar should consist of Filter and Sort By.", 
					"Refinement bar consists of Filter and Sort By.", 
					"Refinement bar does not consist of Filter and Sort By.", driver);
			
			//Step-4: Tap on downward arrow mark in "Filter By" menu
			refinements.openCloseFilterBy("expanded");
			Log.message(i++ + ". Clicked on Filter Down arrow.", driver);
			
			Log.softAssertThat(refinements.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("drpFilterByExpanded"), refinements), 
					"Filter By Menu should be Opened", 
					"Filter By Menu is Opened", 
					"Filter By Menu is not Opened", driver);

			Log.softAssertThat(CollectionUtils.listContainsList(Arrays.asList(filterList), refinements.getListFilterOptions()), 
					"Filter options should be properly displayed in \"Filter By\" dropdown box", 
					"Filter options are properly displayed in \"Filter By\" dropdown box", 
					"Filter options are not properly displayed in \"Filter By\" dropdown box", driver);
			
			//Step-5: Select multiple filter option from "Filter By" menu
			refinements.closeOpenedRefinement();
			Log.message(i++ + ". Closed refinement", driver);
			int countProdcutBefore = refinements.getProductCount();
			
			refinements.openCloseFilterBy("expanded");
			Log.message(i++ + ". Opened filter options.", driver);

			refinements.selectSubRefinementInFilterBy(filterList[0].toLowerCase());
			Log.message(i++ + ". Filtered search result by "+ filterList[0], driver);
			
			if(Utils.isTablet()) {
				refinements.openCloseFilterBy("collapsed");
				Log.message(i++ + ". Closed filter options.", driver);
				
				Log.softAssertThat(refinements.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnClearAllInTablet"), refinements), 
						"Clear All link should be displayed below when one or more refinement values has been selected.", 
						"Clear All link is displayed below when one or more refinement values has been selected.", 
						"Clear All link is not displayed below when one or more refinement values has been selected.", driver);
				
				refinements.openCloseFilterBy("expanded");
				Log.message(i++ + ". Opened filter options.", driver);
			}
			
			refinements.openCloseFilterBy("collapsed");
			Log.message(i++ + ". Closed refinement menu.", driver);
			
			int countProdcutAfter = refinements.getProductCount();
			Log.softAssertThat(countProdcutBefore > countProdcutAfter, 
					"Updated products should be displayed in search result page.", 
					"Updated products are displayed in search result page.", 
					"Updated products are not displayed in search result page.", driver);
			
			//Step-6: Tap on "Clear All" link
			refinements.openCloseFilterBy("expanded");
			Log.message(i++ + ". Opened filter options.", driver);
			refinements.clickOnClearAllInRefinement();
			Log.message(i++ + ". Clicked on Clear Filters.", driver);
			refinements.closeOpenedRefinement();
			Log.message(i++ + ". Closed refinement menu.", driver);
			
			int countProdcutAfterClear = refinements.getProductCount();
			
			refinements.openCloseFilterBy("expanded");
			Log.message(i++ + ". Opened filter options.", driver);
			
			Log.softAssertThat(!refinements.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnClearAllInTablet"), refinements), 
					"Selected refinement should be removed from Filter by dropdown box.", 
					"Selected refinement are removed from Filter by dropdown box.", 
					"Selected refinement are not removed from Filter by dropdown box.", driver);
			
			refinements.openCloseFilterBy("collapsed");
			Log.message(i++ + ". Closed refinement menu.", driver);
			
			Log.softAssertThat(countProdcutAfterClear == countProdcutBefore, 
					"Updated products should be displayed in search result page.", 
					"Updated products are displayed in search result page.", 
					"Updated products are not displayed in search result page.", driver);
			
			//Step-7: Verify product grid in search result page.
			if(Utils.isTablet()) {
				Log.softAssertThat(slp.getNumberOfProductTilesPerRow() == 3, 
						"3 product tiles should be displayed per row in search result page", 
						"3 product tiles are displayed per row in search result page", 
						"3 product tiles are not displayed per row in search result page", driver);
			}
			if(Utils.isMobile()) {
				Log.softAssertThat(slp.getNumberOfProductTilesPerRow() == 2, 
						"2 product tiles should be displayed per row in search result page", 
						"2 product tiles are displayed per row in search result page", 
						"2 product tiles are not displayed per row in search result page", driver);
			}
			//Step-8: Verify the result count in search result page
			Log.softAssertThat(slp.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblresultCountAboveSecondarySearchBox", "secondarySearchSection", slp), 
					"Search Term product count should be shown above the search term.", 
					"Search Term product count is shown above the search term.", 
					"Search Term product count is not shown above the search term.", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	} // M1_FBB_DROP2_C19603

}
