package com.fbb.testscripts.drop2;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_DROP2_C19599 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	@Test(groups = { "critical", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_DROP2_C19599(String browser) throws Exception {
		Log.testCaseInfo();
		
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		
		//Test data
		String searchTerm = TestData.get("search_suggestion_terms").split("\\|")[1];
		
		int i = 1;
		try {
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.message(i++ + ". Navigated to " + headers.elementLayer.getAttributeForElement("brandLogo", "title", headers), driver);
			
			int widthSearchBoxBefore= 0, widthSearchBoxAfter= 0;
			if(!Utils.isMobile()) {
				widthSearchBoxBefore = StringUtils.getNumberInString(headers.elementLayer.getElementCSSValue("divSearchBox", "width", headers));
			}
			
			//Step-1: Verify click search field in the header.
			headers.clickOnSearchIcon();
			Log.message(i++ + ". Opened search box.", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyAttributeForElement("txtSearch", "type", "text", headers), 
					"The cursor should display in the search box.", 
					"The cursor displays in the search box.", 
					"The cursor does not display in the search box.", driver);
			
			headers.typeTextInSearchField(searchTerm);
			Log.message(i++ + ". Entered search term.", driver);
			widthSearchBoxAfter = StringUtils.getNumberInString(headers.elementLayer.getElementCSSValue("divSearchBox", "width", headers));
			
			Log.softAssertThat(headers.getTextFromPrimarySearchBox().equalsIgnoreCase(searchTerm), 
					"The cursor should display in the search box.", 
					"The cursor displays in the search box.", 
					"The cursor does not display in the search box.", driver);
			
			//Step-2: Verify search field text
			if(Utils.isTablet()) {
				Log.softAssertThat(widthSearchBoxAfter > widthSearchBoxBefore, 
					"Search box should be expand horizontally.", 
					"Search box expands horizontally.", 
					"Search box does not expand horizontally.", driver);
			}
			if(Utils.isMobile()) {
				Log.softAssertThat(headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnHeaderSearchIcon", "txtSearch", headers), 
						"The search box should appear below the search icon", 
						"The search box appears below the search icon", 
						"The search box does not appear below the search icon", driver);
			}
			
			//Step-3: Verify search suggestion on text update
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divProductSuggestions"), headers), 
					"The system will display Product Suggestions.", 
					"The system displays Product Suggestions.", 
					"The system does not display Product Suggestions.", driver);
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divPhraseSuggestions"), headers), 
						"The system will display Category Suggestions.", 
						"The system displays Category Suggestions.", 
						"The system does not display Category Suggestions.", driver);
			}
			
			Log.softAssertThat(headers.verifySearchPhraseInEnhancedSearch(searchTerm), 
					"Letters typed into search box should be appear highlighted in suggestions.", 
					"Letters typed into search box appears highlighted in suggestions.", 
					"Letters typed into search box does not appear highlighted in suggestions.", driver);
			
			Log.softAssertThat(headers.verifySearchSuggestions(), 
					"Product image, name, and price should be displayed in search suggesion box.", 
					"Product image, name, and price is displayed in search suggesion box.", 
					"Product image, name, and price is not displayed in search suggesion box.", driver);
			
			//Step-4: Verify suggested links on click
			SearchResultPage slp = headers.clickSearchTextFromSearchSuggestion();
			Log.message(i++ + ". Clicked on primary search suggestion.", driver);
			
			Log.softAssertThat(new SearchResultPage(driver).get().getPageLoadStatus(), 
					"The page should be redirected to search result page.", 
					"The page is redirected to search result page.", 
					"The page is not redirected to search result page.", driver);
			
			Log.softAssertThat(slp.getTextFromSecondarySearchBox().contains(searchTerm), 
					"Dress categories should display as a search result.", 
					"Dress categories displays as a search result.", 
					"Dress categories does not display as a search result.", driver);
			
			//Step-5: Verify navigation to suggested links
			Log.softAssertThat(slp.elementLayer.verifyHorizontalAllignmentOfElements(driver, "secondarySearchIcon", "lblresultCountAboveSecondarySearchBox", slp), 
					"Search icon should be displayed in right side of the search result count.", 
					"Search icon is displayed in right side of the search result count.", 
					"Search icon is not displayed in right side of the search result count.", driver);
			
			if(Utils.isDesktop()) {
				if(BrandUtils.isBrand(Brand.bh)){
				Log.softAssertThat(!slp.elementLayer.verifyElementDisplayed(Arrays.asList("leftHandNavigation"), slp)
									&& slp.getNumberOfProductTilesPerRow() == 4, 
							"Left navigation pane should not be displayed in product list page", 
							"Left navigation pane is not displayed in product list page", 
							"Left navigation pane is displayed in product list page", driver);
				}else{
				Log.softAssertThat(!slp.elementLayer.verifyElementDisplayed(Arrays.asList("leftHandNavigation"), slp)
									&& slp.getNumberOfProductTilesPerRow() == 5, 
						"Left navigation pane should not be displayed in product list page", 
						"Left navigation pane is not displayed in product list page", 
						"Left navigation pane is displayed in product list page", driver);
				}
			}
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	} // M1_FBB_DROP2_C19599

}
