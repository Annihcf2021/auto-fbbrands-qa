package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C25215 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C25215(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
						
		int i=1;
		try
		{	
			String userEMailIdADS = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountData.get("password_global");
			String credential = userEMailIdADS + "|" + password;
			String prdVariation = TestData.get("prd_variation");
			String firstName = "ALWAYS"; 
			String lastName = "APPROVE";
			String cardType = "cards_2";
			String alwaysApproveDate = "plcc_always_approve_address";
							
			{
				GlobalNavigation.registerNewUserWithUserDetail(driver, 0, 0, firstName, lastName, credential);
				GlobalNavigation.addNewAddressToAccount(driver, alwaysApproveDate, false, credential);
			}
				
			//Step1
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step2
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(userEMailIdADS, password);
			Log.message(i++ + ". Navigated to My Account page as : " +userEMailIdADS, driver);
			
			Log.softAssertThat(myAcc.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), myAcc), 
					"User should be successfully created", 
					"User is created", 
					"User is not created", driver);
			
			//Step3
			AddressesPage addrPg = myAcc.navigateToAddressPage();
			Log.message(i++ + ". Navigated to Address page!", driver);
			
			Log.softAssertThat(addrPg.getSavedAddressesCount() > 0, 
					"Address should be successfully created", 
					"Address is created", 
					"Address is not created", driver);
			
			//Step4
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			checkoutPg.continueToBilling();
			Log.message(i++ +". Clicked continue button from shipping address section", driver);
			
			if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("modalCheckoutPlcc"), checkoutPg)) {
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("modalCheckoutPlcc"), checkoutPg), 
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed", 
						"PLCC modal is not getting displayed", driver);
				
				//Step5
				checkoutPg.clickNoThanksInPLCC();
				Log.message(i++ + ". Click on 'No Thanks' in PLCC!", driver);
				
				//Step6
				checkoutPg.clickOnBrandLogo();
				Log.message(i++ + ". Click on Brand logo!", driver);
				
				homePage.headers.signOut();
				Log.message(i++ + ". User signed out!", driver);
				
				homePage = BrowserActions.clearCookies(driver);
				Log.message(i++ + ". Cleared cookies and navigated to Homepage", driver);

				//Step7
				//Step8
				homePage = homePage.headers.navigateToHome();
				Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
				
				myAcc = homePage.headers.navigateToMyAccount(userEMailIdADS, password);
				Log.message(i++ + ". Navigated back to My Account page as : " +credential.split("\\|")[0], driver);
				
				//Step9
				pdpPage = homePage.headers.navigateToPDP(prdVariation);
				Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
				
				pdpPage.addToBagCloseOverlay();
				Log.message(i++ + ". Product added to cart", driver);
				
				shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping bag page", driver);
				
				checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
				Log.message(i++ + ". Navigated to Checkout page", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("modalCheckoutPlcc"), checkoutPg), 
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed", 
						"PLCC modal is not getting displayed", driver);
				
				//Step10
				checkoutPg.clickNoThanksInPLCC();
				Log.message(i++ + ". Click on 'No Thanks' in PLCC!", driver);
				
				//Step11
				checkoutPg.continueToPayment();
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "mdlPLCCRebuttal", "paymentmethodSection", checkoutPg),
						"The acquisition rebuttal modal should be displayed above the Add New Credit Card form.",
						"The acquisition rebuttal modal is displayed above the Add New Credit Card form.",
						"The acquisition rebuttal modal is not displayed above the Add New Credit Card form.", driver);
				
				//Step12
				checkoutPg.openClosePLCCRebuttal("open");
				Log.message(i++ +". Clicked learn more link from rebuttal solot", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCAcquisitionRebuttal"),checkoutPg),
						"The acquisition rebuttal over lay should opens up.",
						"The acquisition rebuttal over lay is opens up.",
						"The acquisition rebuttal over lay is not opens up.", driver);
				
				//Step13
				checkoutPg.continueToPLCCStep2InPLCCACQ();
				Log.message(i++ +". Clicked get it today button from rebuttal modal", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApprovalStep2"),checkoutPg),
						"The acquisition rebuttal step 2 overlay should opens up.",
						"The acquisition rebuttal step 2 overlay is opens up.",
						"The acquisition rebuttal step 2 overlay is not opens up.", driver);
				
				//Step14
				checkoutPg.typeTextInSSN("0000");
				Log.message(i++ +". Entered SSN number", driver);
				
				checkoutPg.selectDateMonthYearInPLCC2("15", "08", "1947");
				Log.message(i++ +". Selected date, month, year", driver);
				
				checkoutPg.typeTextInMobileInPLCC("3334445555");
				Log.message(i++ +". Updated mobile number", driver);
				
				checkoutPg.checkConsentInPLCC("YES");
				Log.message(i++ +". Checked Consent checkbox", driver);
				
				checkoutPg.clickOnAcceptInPLCC();
				Log.message(i++ +". Clicked yes, i accept button", driver);
					
				if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("underReviewModal"),checkoutPg)) {
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("underReviewModal"),checkoutPg),
							"The Under Review modal should be displayed",
							"The Under Review modal is displayed",
							"The Under Review modal is not displayed", driver);
				} else {
					Log.reference("Under review modal is not displayed", driver);
				}
				//Step15
				checkoutPg.dismissCongratulationModal();
				Log.message(i++ +". Clicking the Continue to Checkout button", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg), 
						"User will be taken to the payment method section of checkout.",
						"User is taken to the payment method section of checkout.",
						"User is not taken to the payment method section of checkout.", driver);
				
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
				
				if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("lnkAddNewCredit"), checkoutPg)) {
					checkoutPg.clickAddNewCredit();
				}
				
				//Step16
				checkoutPg.fillingCardDetails1(cardType, false, false);
				Log.message(i++ +". Entered credit card details", driver);
			
				checkoutPg.clickOnPaymentDetailsContinueBtn();
				Log.message(i++ +". Navigated to checkout review and place order page", driver);
				
				OrderConfirmationPage order= checkoutPg.clickOnPlaceOrderButton();
				
				Log.softAssertThat(order.elementLayer.verifyPageElements(Arrays.asList("readyElement"), order),
						"User should taken to the order confirmation page.",
						"User is taken to the order confirmation page.",
						"User is not taken to the order confirmation page.",driver);
				
			} else {
				Log.fail("Checkout step-1 overlay is not displayed hence cant proceed further", driver);
			}
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}
}