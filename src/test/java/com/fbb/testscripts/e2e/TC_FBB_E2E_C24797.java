package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24797 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	

	@Test(groups = { "footer", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24797(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		WebDriver driver = WebDriverFactory.get(browser);
		String baseWebSiteURL = Utils.getWebSite();
		
		int i=1;
		try
		{
			String errorColor=TestData.get("signupfield_errorcolor");
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("footerPanel"), homePage.footers), 
					"Footer top sould be displayed.", 
					"Footer top is displayed.", 
					"Footer top is not displayed.", driver);
			
			//Step 1 - Verify the email sign up section
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("emailSignupMessage","emailSignupMessageValues"), homePage.footers), 
					"Email Signup message should followed with Stay up to date with all of our great fitting styles and new season arrivals. Plus enjoy XX% off.", 
					"Email Signup message is followed with Stay up to date with all of our great fitting styles and new season arrivals. Plus enjoy XX% off.", 
					"Email Signup message is not with Stay up to date with all of our great fitting styles and new season arrivals. Plus enjoy XX% off.", driver);
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyVerticalAllignmentOfElements(driver, "emailSignupMessage", "btnFooterEmailSignUp", homePage.footers), 
					"The Email Sign Up Button / Field displays below the Email signup messaging.", 
					"The Email Sign Up Button / Field displayed below the Email signup messaging.", 
					"The Email Sign Up Button / Field is not displayed below the Email signup messaging.", driver);
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnFooterEmailSignUp", "fldEmailSignUpFooter", homePage.footers), 
					"The Email Signup button should displayed beside (right side) of the 'Email Address' field.", 
					"The Email Signup button is displayed beside (right side) of the 'Email Address' field.", 
					"The Email Signup button is not displayed beside (right side) of the 'Email Address' field.", driver);
			
			Log.softAssertThat(homePage.footers.elementLayer.getElementText("btnFooterEmailSignUp", homePage.footers).equalsIgnoreCase("SIGN UP"), 
					"The text should displayed on Email Signup button is 'SIGN UP'.", 
					"The text is displayed on Email Signup button is 'SIGN UP'.", 
					"The text is not displayed on Email Signup button is 'SIGN UP'.", driver);
			
			homePage.footers.clickOnEmailSignUp();
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyElementColor("txtEmailSignUpError", errorColor, homePage.footers), 
					"Click/Tap on sign up button without giving any Email address-The words Email Address in the textbox will turn red.", 
					"Click/Tap on sign up button without giving any Email address-The words Email Address in the textbox is turn red.", 
					"Click/Tap on sign up button without giving any Email address-The words Email Address in the textbox is not turn red.", driver);
			
			
			homePage.footers.typeInEmailSignUp("255.50.40.28");
			homePage.footers.clickOnEmailSignUp();
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyElementColor("txtEmailSignUpError", errorColor, homePage.footers), 
					"Enter invalid IP format with the email and click/Tap Signup-Error message should be display(Please enter a valid email).", 
					"Enter invalid IP format with the email and click/Tap Signup-Error message is display(Please enter a valid email).", 
					"Enter invalid IP format with the email and click/Tap Signup-Error message is not display(Please enter a valid email).", driver);
			
			homePage.footers.typeInEmailSignUp("fbbqa");
			homePage.footers.clickOnEmailSignUp();
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyElementColor("txtEmailSignUpError", errorColor, homePage.footers),
			"Enter invalid IP format with the email and click/Tap Signup-Error message should be display(Please enter a valid email).", 
			"Enter invalid IP format with the email and click/Tap Signup-Error message is display(Please enter a valid email).", 
			"Enter invalid IP format with the email and click/Tap Signup-Error message is not display(Please enter a valid email).", driver);
			
			homePage.footers.typeInEmailSignUp("fbbqa@gmail.com");
			homePage.footers.clickOnEmailSignUp();
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyPageElements(Arrays.asList("lblEmailSignUpSuccessMsg"), homePage.footers), 
					"Thank you message should be display and the message should be brand-specific.", 
					"Thank you message should be display and the message is brand-specific.", 
					"Thank you message should be display and the message is not brand-specific.", driver);
			
			homePage.headers.navigateToHome();
			//Step - 2 Scroll down to the Footer and click/Tap Social links
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldEmailSignUpFooter", "imgInstagram", homePage.footers), 
					"Instagram should be displayed below the Email SignUp field.", 
					"Instagram is displayed below the Email SignUp field.", 
					"Instagram is not displayed below the Email SignUp field.", driver);
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldEmailSignUpFooter", "imgPinterest", homePage.footers), 
					"Pintrest should be displayed below the Email SignUp field.", 
					"Pintrest is displayed below the Email SignUp field.", 
					"Pintrest is not displayed below the Email SignUp field.", driver);
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldEmailSignUpFooter", "imgFacebook", homePage.footers), 
					"Facebook should be displayed below the Email SignUp field.", 
					"Facebook is displayed below the Email SignUp field.", 
					"Facebook is not displayed below the Email SignUp field.", driver);
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldEmailSignUpFooter", "imgTwitter", homePage.footers), 
					"Twitter should be displayed below the Email SignUp field.", 
					"Twitter is displayed below the Email SignUp field.", 
					"Twitter is not displayed below the Email SignUp field.", driver);
			
			if (!(BrandUtils.isBrand(Brand.bh) || BrandUtils.isBrand(Brand.ks))) {
				Log.softAssertThat(homePage.footers.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldEmailSignUpFooter", "imgYoutube", homePage.footers), 
						"Youtube should be displayed below the Email SignUp field.", 
						"Youtube is displayed below the Email SignUp field.", 
						"Youtube is not displayed below the Email SignUp field.", driver);
			} else
				Log.message(i++ + ". Youtube is not available for " + Utils.getCurrentBrand(), driver);
				
			homePage.footers.navigateToInstagram();
			Log.message(i++ + ". Navigated to 'Instagram'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("instagram"), 
					"Page should be redirected to Instagram Page", 
					"Page redirected to Instagram Page", 
					"Page Not Redirected to Instagram Page", driver);

			driver = Utils.switchWindows(driver, baseWebSiteURL, "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);

			homePage.footers.navigateToFaceBook();
			Log.message(i++ + ". Clicked on 'Facebook'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("facebook"), 
					"Page should be redirected to Facebook Page", 
					"Page redirected to Facebook Page", 
					"Page Not Redirected to Facebook Page", driver);

			driver = Utils.switchWindows(driver, baseWebSiteURL, "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);

			homePage.footers.navigateToPinterest();
			Log.message(i++ + ". Clicked on 'Pinterest'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("pinterest"), 
					"Page should be redirected to Pintrest Page", 
					"Page redirected to Pintrest Page", 
					"Page Not Redirected to Pintrest Page", driver);

			driver = Utils.switchWindows(driver, baseWebSiteURL, "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);

			homePage.footers.navigateToTwitter();
			Log.message(i++ + ". Clicked on 'Twitter'!", driver);

			Log.softAssertThat(driver.getCurrentUrl().contains("twitter"), 
					"Page should be redirected to Twitter Page", 
					"Page redirected to Twitter Page", 
					"Page Not Redirected to Twitter Page", driver);

			driver = Utils.switchWindows(driver, baseWebSiteURL, "url", "NO");
			Log.message(i++ + ". Switched To FullBeauty Website.", driver);               

			//Step - 3 Click/Tap on the Footer links in the Footer section
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyVerticalAllignmentOfElements(driver, "footerPlcc", "footerLink", homePage.footers), 
					"The Footer links should be present below the marketing messages.", 
					"The Footer links is present below the marketing messages.", 
					"The Footer links is not present below the marketing messages.", driver);
			
			Log.reference("Skipping step --The links are arranged column-wise-- since it is unable to automate."); 
			
			//Step 4 - Click/Tap on any Brand logos in footer section
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyPageListElements(Arrays.asList("brandLogoList"), homePage.footers)
							|| homePage.footers.elementLayer.verifyPageListElements(Arrays.asList("lstfooterBrandLogos"), homePage.footers)
							|| homePage.footers.elementLayer.verifyPageListElements(Arrays.asList("footerBrandList"), homePage.footers), 
					"Below Brand logos should be displayed in Footer..", 
					"Below Brand logos is displayed in Footer.", 
					"Below Brand logos is not displayed in Footer.", driver);
			
			Log.softAssertThat(homePage.footers.verifyBrandImages(homePage.footers), 
					"Should not display logo of site the user is currently on.", 
					"Not display logo of site the user is currently on.", 
					"Is display logo of site the user is currently on.", driver);
			
			Log.reference("Other brand navigation validation skipped.");
			
			//Step 5 - Click/Tap the Legal links in the footer
			
			Log.softAssertThat(homePage.footers.elementLayer.verifyVerticalAllignmentOfElements(driver, "footerBrandLogo", "footerCopyRights", homePage.footers)
							|| homePage.footers.elementLayer.verifyVerticalAllignmentOfElements(driver, "footerBrandLogoSection", "footerCopyRights", homePage.footers)
							|| homePage.footers.elementLayer.verifyVerticalAllignmentOfElements(driver, "footerBrandSection", "footerCopyRights", homePage.footers),
					"The alignment of legal links should be displayed below brand logos.", 
					"The alignment of legal links is displayed below brand logos", 
					"The alignment of legal links is not displayed below brand logos", driver);
			driver.get(Utils.getWebSite());
			BrowserActions.scrollToBottomOfPage(driver);
			homePage.footers.navigateToPrivacyPolicy();
			Log.message(i++ + ". Clicked on 'Privacy Policy'!", driver);

			driver = Utils.switchWindows(driver, "privacy-policy", "url", "NO");
			
			Log.softAssertThat(driver.getCurrentUrl().contains("privacy-policy"), 
					"Page should be redirected to Privacy Policy Page", 
					"Page redirected to Privacy Policy Page", 
					"Page Not Redirected to Privacy Policy Page", driver);

			driver = Utils.switchWindows(driver, baseWebSiteURL, "url", "NO");
			BrowserActions.scrollToBottomOfPage(driver);
			homePage.footers.navigateToTermsOfUse();
			Log.message(i++ + ". Clicked on 'Terms of use'!", driver);

			driver = Utils.switchWindows(driver, "terms-of-use", "url", "NO");
			Log.softAssertThat(driver.getCurrentUrl().contains("terms"), 
					"Page should be redirected to 'Terms of use' Page", 
					"Page redirected to 'Terms of use' Page", 
					"Page Not Redirected to 'Terms of use' Page", driver);

			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24110
