package com.fbb.testscripts.e2e;
import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;


@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24402 extends BaseTest {
	
	EnvironmentPropertiesReader environmentPropertiesReader;
		
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24402(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{	
			String guestEmail=AccountUtils.generateEmail(driver);
			String plpCategories= TestData.get("fixed_bonus_level2_cat");
			String prdPromoDisplayed = TestData.get("prd_promotion_withoutCoupon");
			String fixedBonus = TestData.get("prd_fixed-bonus");
			String fixedBonusPromo = TestData.get("cpn_fixed_bonus");
			String checkoutShipAdd = "taxless_address";
			String checkoutBillpAdd = "valid_address1";
			String paymentCard = "card_Visa";
			
			//Step-1 Navigate to the web site
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Navigated to home page", driver);
			
			//Step-2 Click on category name from any category header
			// Step 2 covered in C24120
			
			PlpPage plpPage = homePage.headers.navigateTo(plpCategories);
			Log.message(i++ +". Navigeted to PLP", driver);
			
			//Step-3 Navigate to Product listing page
			Log.softAssertThat(plpPage.getCategoryName().toLowerCase().equalsIgnoreCase(plpCategories.split("\\|")[1].toLowerCase()), 
					"The system should navigate to respective category",
					"The syatem is navigated to right category",
					"The syatem is not navigated to right category", driver);
			
			//Step- 4 Navigate to Product detail page
			//Bonus product should be available in PLP
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdPromoDisplayed);
			Log.message(i++ +". Navigated to PDP", driver);
			
			if(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtSavingStory"), pdpPage)) {
				String savingStory = pdpPage.elementLayer.getElementText("txtSavingStory", pdpPage).toLowerCase();
				Log.softAssertThat(savingStory.contains("off"), 
						"The system should display "+savingStory,
						"The system is displayed "+savingStory,
						"The system is not displayed "+savingStory, driver);	
				
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtSavingStory", "txtPromotionCalloutMsg",pdpPage),
						"Promotion callout message should be displayed below the saving story",
						"Promotion callout message is displayed below the saving story",
						"Promotion callout message is not displayed below the saving story", driver);
			} else {					
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divPricing", "txtPromotionCalloutMsg",pdpPage),
						"Promotion callout message should be displayed below the price",
						"Promotion callout message is displayed below the saving price",
						"Promotion callout message is not displayed below the price", driver);						
			}
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtPromotionCalloutMsg", "txtSeeDetails"), pdpPage), 
					"In PDP page \"Promotional Callout Message\" with see details link should be shown.", 
					"In PDP page \"Promotional Callout Message\" with see details link is shown.",
					"In PDP page \"Promotional Callout Message\" with see details link is not shown.", driver);
			
			pdpPage = homePage.headers.navigateToPDP(fixedBonus);
			Log.message(i++ +". Navigated to PDP", driver);
			
			String prdName = pdpPage.getProductName().toLowerCase();
			
			pdpPage.selectAllSwatches();
			Log.message(i++ +". Selected all attributes", driver);
			
			pdpPage.selectQty("1");
			
			pdpPage.clickAddProductToBag();	
			Log.message(i++ +". Clicked add to bag button",driver);
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("recommenationMCOverlay"),pdpPage),
						"Recommendation section should be displayed in Add to bag overlay",
						"Recommendation section is displayed in Add to bag overlay",
						"Recommendation section is not displayed in Add to bag overlay", driver);
			}
			
			ShoppingBagPage cartPage = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ +". The system navigates the user to the cart page", driver);
			//Step-5 Navigate to the cart page
			
			Log.softAssertThat(cartPage.getCartItemNameList().contains(prdName), 
					"The selected product should be displayed on the cart page.",
					"The selected product is displayed on the cart page.",
					"The selected product is not displayed on the cart page.", driver);
			
			boolean bonusPrdWithCpn = false; 
			if(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnBonusProductAdd"),cartPage)) {
				cartPage.clickOnBonusProductAdd();
				Log.message(i++ + ". Clicked on add bonus product", driver);
				cartPage.addBonusProductToBagByIndex(0);
				Log.message(i++ + ". Added bonus product to bag.", driver);
			} else {
				bonusPrdWithCpn = true;
				cartPage.applyPromoCouponCode(fixedBonusPromo);
				Log.message(i++ +". Applied promo code.", driver);
			}
			
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtBonusPrdAvailability", "txtCartBonusPrdPrice", cartPage),
					"The Bonus Product price should be displayed below the Product Availability field.", 
					"The Bonus Product price ie displayed below the Product Availability field.",
					"The Bonus Product price is not displayed below the Product Availability field.", driver);
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "fixedBonusPrdQty", "fixedBonusPrdDetailsSection", cartPage), 
						"The Quantity Selector should be displayed to the right of the Variation Attributes for the Bonus Item",
						"The Quantity Selector is displayed to the right of the Variation Attributes for the Bonus Item",
						"The Quantity Selector is not displayed to the right of the Variation Attributes for the Bonus Item", driver);
				
				Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("bonusPrdQtyUpArrow","bonusPrdQtyDownArrow"), cartPage),
						"The Quantity Selector should have both its increment/decrement arrows disabled",
						"The Quantity Selector  have both its increment/decrement arrows disabled",
						"The Quantity Selector not have both its increment/decrement arrows disabled", driver);
			} else {
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "fixedBonusPrdDetailsSection", "fixedBonusPrdQtyMobile", cartPage), 
						"The Quantity Selector should be displayed to the right of the Variation Attributes for the Bonus Item",
						"The Quantity Selector is displayed to the right of the Variation Attributes for the Bonus Item",
						"The Quantity Selector is not displayed to the right of the Variation Attributes for the Bonus Item", driver);
				
				Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("bonusPrdQtyUpArrowMobile","bonusPrdQtyDownArrowMobile"), cartPage),
						"The Quantity Selector should have both its increment/decrement arrows disabled",
						"The Quantity Selector  have both its increment/decrement arrows disabled",
						"The Quantity Selector not have both its increment/decrement arrows disabled", driver);
			}
			
			String bonusItemPrice = Utils.isMobile() ? "bonusItemPriceMob" : "bonusItemPriceDeskAndTab";
			Log.softAssertThat(cartPage.elementLayer.verifyTextContains(bonusItemPrice,"FREE",cartPage),
					"The Price value of the Bonus Products should be displayed as 'FREE'", 
					"The Price value of the Bonus Products is displayed as 'FREE'",
					"The Price value of the Bonus Products is not displayed as 'FREE'", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("bonusCalloutMsg"), cartPage),
					"The Promotional Callout message should be present for each Bonus Product in the Cart",
					"The Promotional Callout message is present for each Bonus Product in the Cart",
					"The Promotional Callout message is not present for each Bonus Product in the Cart", driver);
			
			HashSet<String> prdNames = cartPage.getCartItemNameList();
			
			SignIn signIn = (SignIn) cartPage.navigateToCheckout();
			CheckoutPage checkoutPage = (CheckoutPage) signIn.clickSignInButton();
			Log.message(i++ +". navigated to checkout page", driver);
			
			//Step-6 Navigate to the checkout sign in page
			checkoutPage.enterGuestUserEmail("aspire.fbb@");
			Log.message(i++ +". Entered Invalid mail id", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblGuestEmailError"),checkoutPage),
					"The system should display an error message",
					"The system is displayed an error message",
					"The system is not displayed an error message", driver);
			
			//Step-7 Navigate to Checkout page - step 1
			checkoutPage.continueToShipping(guestEmail); 
			Log.message(i++ +". Entered valid Email Address and navigated to shipping section",driver);
			
			String lblSubtotalBonusPrd = "lblSubtotalBonusPrdDesk_Tab";
			if(Utils.isMobile()) {
				lblSubtotalBonusPrd = "lblSubtotalBonusPrdMobile";
			}
			Log.softAssertThat(checkoutPage.elementLayer.verifyTextContains(lblSubtotalBonusPrd, "FREE",checkoutPage),
					"The Price value of the Bonus Products should be displayed as 'FREE'", 
					"The Price value of the Bonus Products is displayed as 'FREE'",
					"The Price value of the Bonus Products is not displayed as 'FREE'", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("bonusCalloutMsg"), checkoutPage),
					"The Promotional Callout message should be present for each Bonus Product in the Cart",
					"The Promotional Callout message is present for each Bonus Product in the Cart",
					"The Promotional Callout message is not present for each Bonus Product in the Cart", driver);
			
			checkoutPage.fillingShippingDetailsAsGuest("No", checkoutShipAdd, ShippingMethod.Standard);
			Log.message(i++ +". Filled Shipping address and deselected \"Use this as a billing address\" checkbox",driver);
			
			checkoutPage.checkUncheckUseThisAsBillingAddress(false);
			
			checkoutPage.continueToBilling();
			Log.message(i++ +". Clicked continue button", driver);
			
			//Step-8 Navigate to Checkout page - step 2
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("billingAddressFields"),checkoutPage),
					"The system should display the billing address form page",
					"The system should display the billing address form page",
					"The system should display the billing address form page",driver);
			
			checkoutPage.fillingBillingDetailsAsGuest(checkoutBillpAdd);
			Log.message(i++ +". Entered biling address", driver);
			
			checkoutPage.continueToPayment();
			Log.message(i++ +". Navigated to payment details section", driver);
			
			if(bonusPrdWithCpn) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("appiledCoupon"),checkoutPage),
						"Applied promo code should display on the checkout page", 
						"Applied promo code is displayed on the checkout page",
						"Applied promo code is not displayed on the checkout page", driver);
			}
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			checkoutPage.applyGiftCardByValue(TestGiftCardValue.$20);
			Log.message(i++ +". Applied valid gift card", driver);
			
			if(!(checkoutPage.getRemainingOrderAmount() == 0.00)) {
				checkoutPage.fillingCardDetails1(paymentCard, false);
				Log.message(i++ +". Added new credit card", driver);			
			}
			
			checkoutPage.continueToReivewOrder();
			Log.message(i++ +". Navigated to review and place order screen", driver);
			
			// Step-9 Navigate to Checkout page - step 3
			
			OrderConfirmationPage orderConfirm = checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ +". Order placed successfully", driver);

			// Step-10 Order confirmation page
			// Order summary, Shipping method, delivery method, Payment method,Billing Address,Shipping Addres are covered on other test cases.
			Log.softAssertThat(orderConfirm.getOrderedPrdListNames().containsAll(prdNames),
					"All products should be displayed in order confirmation page",
					"All products are displayed in order confirmation page",
					"All products are not displayed in order confirmation page", driver);
			
			String bonusProdSubTotal = "bonusPrdSubTotalDesk_Tab";
			if(Utils.isMobile()) {
				bonusProdSubTotal = "bonusPrdSubTotalMobile";
			}
			Log.softAssertThat(orderConfirm.elementLayer.verifyTextContains(bonusProdSubTotal, "Bonus", orderConfirm),
					"Bonus product should be displayed on the order confirmation page. ",
					"Bonus product is displayed on the order confirmation page. ",
					"Bonus product is not displayed on the order confirmation page. ", driver);	
			
			Log.testCaseResult();
			
		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}	
}//TC_FBB_E2E_C24402
