package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.OfferPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.headers.HamburgerMenu;
import com.fbb.pages.headers.Headers;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.reusablecomponents.Enumerations.Toggle;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24815 extends BaseTest{
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	
	@Test(groups = { "header", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24815(String browser) throws Exception{
		Log.testCaseInfo(); 
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String validUsername = AccountUtils.generateEmail(driver);
		String validPassword = accountData.get("password_global");
		String inValidIP = "444.222.111.333";
		String productKey = TestData.get("prd_variation");
		String searchKey = TestData.get("product_search_terms").split("\\|")[0];
		
		GlobalNavigation.registerNewUser(driver, 0, 0, validUsername+"|"+validPassword);
		int i=1;
		try{			
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Navigates to  brand home page. ", driver);
			
			Headers headers = homePage.headers;
			
			String CQOLink = "quickOrederIcon";
			
			//Step-1 Check the contents in the Header section
			if(headers.elementLayer.verifyPageElements(Arrays.asList("topPromotionalBanner"), headers)) {
				Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("topPromotionalBanner", "utilityBarDesktop", "lnkHamburger", "lnkOffer", "btnHeaderSearchIcon", "quickOrederIcon", "lnkCardPLCC", "lnkMiniCart" ), headers),
						"Following contents should be displayed in the Header section :'Top promotional Content','Utility Bar','Hamburger menu','Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon'",
						"Following contents are displayed in the Header section :'Top promotional Content','Utility Bar','Hamburger menu','Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon'",
						"Following contents are nor displayed in the Header section :'Top promotional Content','Utility Bar','Hamburger menu','Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon'",driver);				
			} else {
				Log.reference("Top promotional banner is not displayed on the current brand");
				Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("utilityBarDesktop", "lnkHamburger", "lnkOffer", "btnHeaderSearchIcon", "quickOrederIcon", "lnkCardPLCC", "lnkMiniCart" ), headers),
						"Following contents should be displayed in the Header section :'Utility Bar','Hamburger menu','Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon'",
						"Following contents are displayed in the Header section :'Utility Bar','Hamburger menu','Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon'",
						"Following contents are not be displayed in the Header section :'Utility Bar','Hamburger menu','Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon'",driver);
			}
			
			//Step-2 Verify hamburger menu.
			Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "brandLogoMobTab", "lnkHamburger", headers), 
					"The Hamburger menu icon should be available left side of brand log", 
					"The Hamburger menu icon is available left side of brand log",
					"The Hamburger menu icon is not available left side of brand log",driver);
			
			HamburgerMenu hMenu = (HamburgerMenu) headers.openCloseHamburgerMenu("open");
			Log.message(i++ +". Opened hambuger menu", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayed(Arrays.asList("btnHamburgerClose", "lnkAccountButton","navigationCategory1","categoryName", "utilityBar","fldEmailSignUp","lnkCustomerService_Mobile"),hMenu)
						&& hMenu.verifyCategoryRightArrowDisplayed(), 
					"Hamburger icon should display the following contents:1. menu close button. 2.Sign in button. 3.Global navigation. 4.Category arrow. 5. category Name. 6. utility bar 7. Email sign up. 8. customer service" ,
					"Hamburger icon is displayed the following contents:1. menu close button,2.Sign in button. 3.Global navigation. 4.Category arrow. 5. category Name. 6. utility bar 7. Email sign up. 8. customer service",
					"Hamburger icon is not displayed the following contents:1. menu close button,2.Sign in button. 3.Global navigation. 4.Category arrow. 5. category Name. 6. utility bar 7. Email sign up. 8. customer service", driver);
			
			hMenu.openCloseHamburgerMenu("close");
			Log.message(i++ + ". Clicked Hamburger Menu close button.", driver);
			
			Log.softAssertThat(hMenu.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("divHamburgerOverlay"), hMenu), 
					"Closing hamburger menu should reveal the original page.", 
					"Closing hamburger menu reveals the original page.", 
					"Closing hamburger menu does not reveal the original page.", driver);
			
			OfferPage offersPage = headers.navigateToOfferPage();
			Log.message(i++ +". Tapped offers link", driver);
			
			Log.softAssertThat(offersPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), offersPage),
					"page should be redirect to offers and coupons landing page.", 
					"page is redirected to offers and coupons landing page.",
					"page is not redirected to offers and coupons landing page.", driver);
			
			headers.openCloseHamburgerMenu("open");
			Log.softAssertThat(hMenu.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkMyAccount","lnkOffers",hMenu),
					"Sign in link should be displayed right side of the offers link",
					"Sign in link is displayed right side of the offers link", 
					"Sign in link is not displayed right side of the offers link", driver);
			
			SignIn signIn = hMenu.navigateToSignIn();
			Log.message(i++ +". Clicked sign in button", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("readyElement"), signIn), 
					"page should be navigate to My Account sign in page",
					"page should be navigate to My Account sign in page",
					"page should be navigate to My Account sign in page", driver);
			
			headers.navigateToHome();
			
			homePage.headers.openCloseHamburgerMenu("open");
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayed(Arrays.asList("lnkCategoryName"), hMenu)
							&& hMenu.verifyCategoryRightArrowDisplayed(),
						"category names and right arrows should be displayed in Hamburger menu", 
						"category names and right arrows is displayed in Hamburger menu", 
						"category names and right arrows not displayed in Hamburger menu", driver);
			
			if(hMenu.verifyLevel1CategoryWithoutLevel2Availability()) {
				Log.softAssertThat(hMenu.verifyLevel1CategoryWithoutLevel2Availability(), 
					"Category without child categories should not display category arrow.",
					"Category arrow is not shown for categories without child categories.",
					"Category arrow is shown for categories without child categories.", driver);
			
				Log.softAssertThat(hMenu.verifyArrowWhenChildCategoriesNotPresent(),
					"Page should be navigated to CLP or PLP.", 
					"Page is navigated to CLP or PLP.",
					"Page is not be navigated to CLP or PLP.", driver);
			} else {
				Log.reference("Category landing page is not available for this brand");
			}
			
			headers.navigateToHome();
			headers.openCloseHamburgerMenu("open");
			
			Log.softAssertThat(hMenu.verifyArrowWhenChildCategoriesPresent(),
					"Category arrow should be shown",
					"Category arrow is shown",
					"Category arrow is not shown", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyVerticalAllignmentOfElements(driver, "activeCat2Header", "activeCategoryNavLevel2", hMenu),
					"Selected category should be displayed in the hamburger menu and the child categories should be displayed below the Selected category.",
					"Selected category is displayed in the hamburger menu and the child categories are displayed below the Selected category.",
					"Selected category is not displayed in the hamburger menu and the child categories are not displayed below the Selected category.", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayed(Arrays.asList("lblActiveMenuHeading"), hMenu)
						&& hMenu.verifyCategoryBackArrowDisplayed(),
					"The back arrow should be displayed at the left side of the category header withe symbol like '<' .",
					"The back arrow is displayed at the left side of the category header withe symbol like '<' .",
					"The back arrow is not displayed at the left side of the category header withe symbol like '<' .", driver);
			
			hMenu.clickOnBackArrowInActiveLevel2Category();
			Log.message(i++ + ". Clicked on back button menu button on submenu.", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("navigationCategory1"), hMenu), 
					"On tapping menu back button the page should navigate to previous menu state.", 
					"On tapping menu back button the page navigates to previous menu state.", 
					"On tapping menu back button the page does not navigate to previous menu state.", driver);
			
			hMenu.verifyArrowWhenChildCategoriesPresent();
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkViewAllActive","activeCategoryNavLevel2" ), hMenu),
					"The View all link should be displayed with the L2 navigation",
					"The View all link should be displayed with the L2 navigation",
					"The View all link should be displayed with the L2 navigation", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkViewAllActive"), hMenu), 
					"The view all link should be available only when user able to tap L2 category.", 
					"The view all link is available only when user able to tap L2 category.", 
					"The view all link is not available only when user able to tap L2 category.", driver);
		
			hMenu.clickOnActiveViewAll();
			Log.message(i++ + ". Clicked on View All button on submenu.", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("lnkHamburger"),headers), 
					"On tap the user should be taken to the L2 category and the Hamburger Menu should be closed.", 
					"On tap the user is taken to the L2 category and the Hamburger Menu is closed.", 
					"On tap the user is not taken to the L2 category and the Hamburger Menu is not closed.", driver);
			
			headers.navigateToHome();
			
			homePage.headers.openCloseHamburgerMenu("open");
			
			Log.softAssertThat(hMenu.elementLayer.verifyVerticalAllignmentOfElements(driver, "navigationCategory1", "utilityBar", hMenu),
					"Utility bar should be displayed below the category pane.",
					"Utility bar is displayed below the category pane.",
					"Utility bar is not displayed below the category pane.", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyVerticalAllignmentOfElements(driver, "utilityBar", "fldEmailSignUp", hMenu),
					"Email sign up button should be displayed below the Utility bar under hamburger menu.",
					"Email sign up button IS displayed below the Utility bar under hamburger menu.",
					"Email sign up button is not displayed below the Utility bar under hamburger menu.", driver);
			
			hMenu.typeInEmailSignUp(inValidIP);
			Log.message(i++ +". Entered invalid format email address", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayed(Arrays.asList("txtEmailSignUpError"), hMenu),
					"Error message should display when user try to sign up with incorrect  or invalid email addres", 
					"Error message is displayed when user try to sign up with incorrect  or invalid email addres",
					"Error message is not displayed when user try to sign up with incorrect  or invalid email address", driver);
			
			hMenu.typeInEmailSignUp(validUsername);
			Log.message(i++ +". Entered valid email address", driver);
			
			hMenu.clickOnEmailSignUp();
			Log.message(i++ +". Clicked email sign up button", driver);			

			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayed(Arrays.asList("lblEmailSignUpThankYou"), hMenu),
					"User should able to sign up with valid email address.", 
					"User is able to sign up with valid email address.",
					"User is not able to sign up with valid email address.", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblEmailSignUpThankYou", "customerService_Mobile", hMenu),
					"Customer service links should be displayed below the email sign up section, ",
					"Customer service links is displayed below the email sign up section, ",
					"Customer service links is not displayed below the email sign up section, ", driver);
			
			boolean navigation = hMenu.verifyCustomerServiceLinksNaviagtion();
			boolean quickLinksNavigation1 = hMenu.verifyquicklinksNaviagtion();
			
			CustomerService customerService = hMenu.navigateToCustomerService();
			Log.message(i++ +". Clicked customer service links", driver);
			
			Log.softAssertThat(navigation && quickLinksNavigation1 &&
							customerService.elementLayer.verifyPageElements(Arrays.asList("readyElement"), customerService),
					"Page should be navigated to respective page.",
					"Page is navigated to respective page.",
					"Page is not navigated to respective page.", driver);
			
			headers.navigateToHome();
			//Step-3 Verify Sign In Button | Recognized & Authenticated State.
			homePage.headers.navigateToSignInPage();
			Log.message(i++ +". Navigated to sign in Page", driver);
			
			signIn.navigateToMyAccount(validUsername, validPassword);
			Log.message(i++ +". Logged in to account", driver);
			
			headers.openCloseHamburgerMenu("open");
			
			Log.softAssertThat(hMenu.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgUserProfile", "lblUserName", hMenu),
					"Account text should be displayed below of the Account icon.", 
					"Account text is displayed below of the Account icon.",
					"Account text is not displayed below of the Account icon.", driver);
			
			hMenu.ClickOnMyAccount();
			Log.message(i++ +". Tapped on My account link", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayed(Arrays.asList("lblMyAccountHeader"), hMenu),
					"My account navigation should be displayed",
					"My account navigation is displayed",
					"My account navigation is not displayed", driver);
			
			if (BrandUtils.isBrand(Brand.el)) {
				Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayed(Arrays.asList("lnkProfile", "lnkWishList", "lnkOrderHistory", "lnkSavedPayment", "lnkAddressbook", "lnkEmailPrefs", "lnkCatalogPrefs", "lnkSignOut"), hMenu),
						"My account navigation pane consist of : 1.PROFILE, 2.WISHLIST, 3.ORDER HISTORY, 4.SAVED PAYMENTS 5.ADDRESS BOOK 6.EMAIL PREFERENCE 7.CATALOG PREFERENCE 8.SIGN OUT",
						"My account navigation pane is consist of : 1.PROFILE, 2.WISHLIST, 3.ORDER HISTORY, 4.SAVED PAYMENTS 5.ADDRESS BOOK 6.EMAIL PREFERENCE 7.CATALOG PREFERENCE 8.SIGN OUT",
						"My account navigation pane is not consist of : 1.PROFILE, 2.WISHLIST, 3.ORDER HISTORY, 4.SAVED PAYMENTS 5.ADDRESS BOOK 6.EMAIL PREFERENCE 7.CATALOG PREFERENCE 8.SIGN OUT", driver);
			} else {
				Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayed(Arrays.asList("lnkProfile", "lnkWishList", "lnkOrderHistory", "lnkRewardPoints", "lnkSavedPayment", "lnkAddressbook", "lnkEmailPrefs", "lnkCatalogPrefs", "lnkSignOut"), hMenu),
						"My account navigation pane consist of : 1.PROFILE, 2.WISHLIST, 3.ORDER HISTORY, 4.REWARDS, 5.SAVED PAYMENTS 6.ADDRESS BOOK 7.EMAIL PREFERENCE 8.CATALOG PREFERENCE 9.SIGN OUT",
						"My account navigation pane is consist of : 1.PROFILE, 2.WISHLIST, 3.ORDER HISTORY, 4.REWARDS, 5.SAVED PAYMENTS 6.ADDRESS BOOK 7.EMAIL PREFERENCE 8.CATALOG PREFERENCE 9.SIGN OUT",
						"My account navigation pane is not consist of : 1.PROFILE, 2.WISHLIST, 3.ORDER HISTORY, 4.REWARDS, 5.SAVED PAYMENTS 6.ADDRESS BOOK 7.EMAIL PREFERENCE 8.CATALOG PREFERENCE 9.SIGN OUT", driver);
			}
			
			hMenu.clickOnProfile();
			Log.message(i++ +". Tapped on profile link", driver);
			
			Log.softAssertThat(new ProfilePage(driver).get().getPageLoadStatus(),
					"Page should be navigate to Profile page", 
					"Page is navigateed to Profile page",
					"Page is not navigateed to Profile page", driver);
			
			headers.openCloseHamburgerMenu("open");
			hMenu.ClickOnMyAccount();
			
			hMenu.clickOnOrderHistory();			
			Log.message(i++ +". Tapped on Order history link", driver);
			OrderHistoryPage orderPage = new OrderHistoryPage(driver).get();
			
			Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage),
					"Page should be navigate to Order History page", 
					"Page is navigateed to Order History page",
					"Page is not navigateed to Order History page", driver);
			
			headers.openCloseHamburgerMenu("open");
			hMenu.ClickOnMyAccount();
			WishListPage wishlistPage = hMenu.clickOnWishList();
			Log.message(i++ +". Tapped on Wishlist link", driver);
			
			Log.softAssertThat(wishlistPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), wishlistPage),
					"Page should be navigate to Wishlist page", 
					"Page is navigateed to Wishlist page",
					"Page is not navigateed to Wishlist page", driver);
			
			QuickOrderPage qcPage = headers.navigateToQuickOrder();
			Log.softAssertThat(qcPage.elementLayer.verifyPageElements(Arrays.asList("containerElement"), qcPage),
					"Page should be navigate to QuickOrder page", 
					"Page is navigateed to QuickOrder page",
					"Page is not navigateed to QuickOrder page", driver);
			
			headers.signOut();
			Log.message(i++ +". Tapped on sign out link", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("readyElement"), signIn),
					"Page should be navigate to sign in page", 
					"Page is navigateed to sign in page",
					"Page is not navigateed to sign in page", driver);
			
			//Step-4:  Verify the brand logo
			Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver,"brandLogoMobTab", "lnkHamburger", headers), 
					"Brand logo should display to the left-hand side below the Utility Bar.", 
					"Brand logo is displayed to the left-hand side below the Utility Bar.", 
					"Brand logo is not displayed to the left-hand side below the Utility Bar.", driver);
			
			headers.clickOnBrandFromHeader();
			Log.message(i++ +". Clicked on brand logo", driver);
			
			Log.softAssertThat(new HomePage(driver).get().getPageLoadStatus(), 
					"Page should navigate to brad home page.", 
					"Page is navigated to brad home page.", 
					"Page is not navigated to brad home page.", driver);
			
			//Step-5: Verify catalog quick order icon.(Skip this step for Ellso).
			Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, CQOLink, "btnHeaderSearchIcon", headers), 
					"CATALOG QUICK ORDER link should display next to the search icon.", 
					"CATALOG QUICK ORDER link is displayed next to the search icon.", 
					"CATALOG QUICK ORDER link is not displayed next to the search icon.", driver);
						
			Log.softAssertThat(homePage.headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkCardPLCC", CQOLink, homePage.headers), 
					"Search icon should be displayed right of the card icon.", 
					"Search icon is displayed right of the card icon.",
					"Search icon is not displayed right of the card icon.", driver);
				
			qcPage = headers.navigateToQuickOrder();
			Log.message(i++ +". Clicked on Catalog icon", driver);
				
			Log.softAssertThat(qcPage.elementLayer.verifyElementDisplayed(Arrays.asList("containerElement"), qcPage), 
					"Page should navigate to catalog quick order landing page.", 
					"Page is navigated to catalog quick order landing page.", 
					"Page is not navigated to catalog quick order landing page.", driver);
				
			headers.navigateToHome();
			//Step-6 Verify search icon
			
			homePage.headers.toggleSearchPanel(Toggle.Open);
			Log.message(i++ +". Tapped search icon", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkHamburger", "txtSearch", headers)&&
					headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "brandLogoMobTab", "txtSearch", headers)&&
					headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnHeaderSearchIcon", "txtSearch",headers),
					"Search box should be displayed below the hamburger, brand logo, search icon.",
					"Search box is displayed below the hamburger, brand logo, search icon.",
					"Search box is not displayed below the hamburger, brand logo, search icon.", driver);
			
			headers.typeTextOnSearchField("ab");
			Log.message(i++ +". Entered two charactes in search field", driver);
			
			Log.softAssertThat(! headers.elementLayer.verifyElementDisplayed(Arrays.asList("divProductSuggestions"), headers),
					"Search flyout should not show when user enters less than three characters",
					"Search flyout is not shown when user enters less than three characters",
					"Search flyout is shown when user enters less than three characters", driver);
			
			headers.typeTextInSearchField(searchKey);
			Log.message(i++ +". Typed  valid 4 charactes in search field", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("divProductSuggestions"), headers),
					"Search suggestion flyout should be displayed after entering at least three valid letters.",
					"Search suggestion flyout is displayed after entering at least three valid letters.",
					"Search suggestion flyout is not displayed after entering at least three valid letters.", driver);
			
			Log.softAssertThat(headers.verifySearchSuggestions(), 
					"Search flyout should consist of Suggested search term, product image, product name, price and suggested categories ..etc",
					"Search flyout is consist of Suggested search term, product image, product name, price and suggested categories ..etc",
					"Search flyout is not consist of Suggested search term, product image, product name, price and suggested categories ..etc", driver);
			
			headers.clickGoButton();
			Log.message(i++ +". Hitting Go button from keypad after entering search term", driver);
			
			Log.softAssertThat(new SearchResultPage(driver).get().getPageLoadStatus(),
					"Page should be navigate to either Search result page or No Search result page based on search term",
					"Page should be navigate to either Search result page or No Search result page based on search term",
					"Page should be navigate to either Search result page or No Search result page based on search term", driver);
					
			
			//Step-7: Verify my bag icon.
			Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkMiniCart", "btnHeaderSearchIcon", headers), 
					"My Bag icon should be displayed right side of the search icon", 
					"My Bag icon is displayed right side of the search icon", 
					"My Bag icon is not displayed right side of the search icon", driver);
			
			if(!headers.getMiniCartCount().contains("0")) {
				ShoppingBagPage cartPage = headers.clickOnBagLink();				
				cartPage.removeAllItemsFromCart();
				Log.message(i++ +". Removed all products from cart page", driver);
			}
			
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("lnkMiniCart"), headers)&&
					headers.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("miniCartQty"), headers),
					"When user doesn't have any products then only my bag icon should be displayed.",
					"When user doesn't have any products then only my bag icon is displayed.",
					"When user doesn't have any products then only my bag icon is not displayed.", driver);	
			
			ShoppingBagPage cartPage = headers.navigateToShoppingBagPage();
			Log.message(i++ +". Tapped on My Bag icon", driver);
			
			Log.softAssertThat(new ShoppingBagPage(driver).get().getPageLoadStatus(), 
					"page should be naviageted to empty cart page.", 
					"page is naviageted to empty cart page.", 
					"page is not naviageted to empty cart page.", driver);
				
			PdpPage pdpPage = headers.navigateToPDP(productKey);
			Log.message(i++ +". Navigated to product.", driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". Product added to bag", driver);
			
			headers.navigateToShoppingBagPage();
			Log.message(i++ +". Clicked on My Bag icon", driver);
			
			String myBagCount = headers.getMiniCartCount();
			int cartqty = Integer.parseInt(myBagCount);
				
			Log.softAssertThat(cartPage.getTotalQtyInCart() == cartqty, 
					"When user have products in cart then the mini cart in the top navigation should displays as an icon with the numeric representation of the number of items in the shopping cart.",
					"When user have products in cart then the mini cart in the top navigation is displays as an icon with the numeric representation of the number of items in the shopping cart.",
					"When user have products in cart then the mini cart in the top navigation is not displays as an icon with the numeric representation of the number of items in the shopping cart.", driver);
			
			headers.clickOnBagLink();
			Log.message(i++ +". Clicked on My Bag icon", driver);
			
			Log.softAssertThat(new ShoppingBagPage(driver).get().getPageLoadStatus(), 
					"page should be naviageted to cart page.", 
					"page is naviageted to cart page.", 
					"page is not naviageted to cart page.", driver);
			
			headers.navigateToHome();
						
			//Step-8 Verify Top promotional banner.
			if(homePage.headers.elementLayer.verifyPageElements(Arrays.asList("topPromotionalBanner"),homePage.headers)) {
				Log.softAssertThat(headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "topPromotionalBanner", "lnkHamburger", headers)&&
						headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "topPromotionalBanner", "brandLogoMobTab", headers),
						"Top Promotional Content should be displayed above hamburger menu, brand logos.",
						"Top Promotional Content is displayed above hamburger menu, brand logos.", 
						"Top Promotional Content is not displayed above hamburger menu, brand logos.", driver);
			} else {
				Log.reference("Top promotional banner is not configured to"+Utils.getCurrentBrand()+" brand");
			}
			
			headers.navigateToHome();
			BrowserActions.scrollToTopOfPage(driver);
			
			//Step-9 Verify bottom Promotional Content
			if(!BrandUtils.isBrand(Brand.el) && !BrandUtils.isBrand(Brand.fb)) {
				Log.softAssertThat(headers.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "lnkHamburger", "bottomPromotionalBanner", headers),
						"Bottom Promotional Content should be displayed below hamburger menu link.",
						"Bottom Promotional Content is displayed below hamburger menu link.",
						"Bottom Promotional Content is not displayed below hamburger menu link.", driver);
			}
			
			Log.testCaseResult();
			
		}//End of Try
		catch(Exception e){
			Log.exception(e, driver);
		}//End of catch
		finally{
			Log.endTestCase(driver);
		}//End of finally block
	}//M1_FBB_E2EC24815

}//TC_FBB_E2E_C24815
