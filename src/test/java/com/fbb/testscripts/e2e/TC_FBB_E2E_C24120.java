package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.AddressesPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24120 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24120(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{
			//Step 1 - Navigate to the website
			
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String mouseOverColor = TestData.get("level1_highlight_color");
			String navLevels = TestData.get("level-2");
			String prdFreeExch = TestData.get("prd_freeexchange");
			String prdSizeChart = TestData.get("prd_size-chart");
			String navLevel1Underline = TestData.get("level1_highlight");
			String payment = "card_Discover";
						
			{
				HashMap<String, String> accountDetails = new HashMap<>();
				accountDetails.put(AddressesPage.class.getName(), "1");
				GlobalNavigation.setupTestUserAccount(driver, accountDetails, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Click on category name from any category header
			if(Utils.isDesktop()){
				homePage.headers.mouseHoverOnCategory(navLevels.split("\\|")[0].trim());
				Log.message(i++ + ". Mouse hovered on:: " + navLevels.split("\\|")[0].trim(), driver);
				
				Log.softAssertThat(homePage.headers.elementLayer.verifyCssPropertyForPsuedoElement("hoveredGlobalNavigationLevel1", ":hover", mouseOverColor, navLevel1Underline, homePage.headers, false)
								|| homePage.headers.elementLayer.verifyCssPropertyForPsuedoElement("hoveredGlobalNavigationLevel1", ":after", mouseOverColor, navLevel1Underline, homePage.headers, false)
								|| homePage.headers.elementLayer.verifyCssPropertyForElement("hoveredGlobalNavigationLevel1", mouseOverColor, navLevel1Underline, homePage.headers), 
						"When hovered on any root category the link is highlighted & the highlight bar should appear in that hovered category", 
						"Gray high light bar appears in hovered category", 
						"Gray high light bar does not appear in hovered category", driver);
				
				homePage.headers.mouseHoverOnCategory(navLevels.split("\\|")[0].trim());
				Log.message(i++ + ". Mouse hovered on:: " + navLevels.split("\\|")[0].trim(), driver);
				
				Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("currentLevel2"), homePage.headers), 
						"The flyout should display when mouse hover on the root category", 
						"The flyout displays when mouse hover on the root category", 
						"The flyout does not display when mouse hover on the root category", driver);
			}
			//Step 3 - Navigate to Product listing page

			PlpPage plpPage = homePage.headers.navigateTo(navLevels.split("\\|")[0],navLevels.split("\\|")[1]);
			Log.message(i++ + ". Navigated to:: " + navLevels, driver);
			
			Log.softAssertThat(plpPage.getCategoryName().trim().equalsIgnoreCase(navLevels.split("\\|")[1].trim())
							|| (navLevels.contains("|")&&plpPage.getCategoryName().trim().equalsIgnoreCase(navLevels.split("\\|")[1].trim())), 
					"If user clicks on any root category, system should navigate user to the category list page", 
					"System navigates user to the category list page", 
					"System does not navigate user to the category list page", driver);
			
			if(plpPage.elementLayer.verifyPageElements(Arrays.asList("colorSwatchShowMoreLess"), plpPage)){
				if(Utils.isDesktop()) {
					plpPage.mouseHoverOnShowColors();
					Log.message(i++ + ".  Mouse hovered on Color Swatch.", driver);
				} else {
					plpPage.clickOnViewMoreORShowLessColorsLink(0, "more colours");
					Log.message(i++ + ". Clicked on More Color",driver);
				}
				
				Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("colorSwatchShownMore"), plpPage, false), 
						"More Colors should be displayed", 
						"More Colors are displayed",  
						"More Colors are not displayed", driver);
			}
			
			//Step 4 - Navigate to Product detail page
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdFreeExch);
			Log.message(i++ + ". Navigated to PDP page", driver);
			
			if(!Utils.isMobile()) {
				if (pdpPage.getNoOfAlternateImage() > 4) {
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnPrevImageDisable","btnNextImageEnable"), pdpPage),
							"To check the arrows displaying for product image thumbnails.",
							"The 'Prev arrow', 'Next arrow' are displaying when the product have 5 or more thumbnail images",
							"The arrow is not displaying even the thumbnails are having 5 or more product images", driver);
		
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnPrevImageDisable"), pdpPage),
							"To check the previous arrow is disabled in the begining of image sequence.",
							"The 'Prev arrow' is displaying and disabled in the begining of image sequence.",
							"The 'Prev arrow' is not disabled", driver);
		
					Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnNextImageDisable"), pdpPage),
							"To check the next arrow is enabled in the begining of image sequence.",
							"The 'Next arrow' is displaying and enabled in the begining of image sequence.",
							"The 'Next arrow' is not enabled", driver);
		
					pdpPage.scrollAlternateImageInSpecifiedDirection("Next");
		
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnNextImageDisable"), pdpPage),
							"To check the next arrow is disabled when the alternate image sequence reached the end.",
							"The 'Next arrow' is displaying and disabled in the end of image sequence.",
							"The 'Next arrow' is not disabled", driver);
		
					Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnPrevImageDisable"), pdpPage),
							"To check the Prev arrow is enabled at the end of image sequence.",
							"The 'Prev arrow' is displaying and enabled at the end of image sequence.",
							"The 'Prev arrow' is not enabled", driver);
				} else {
					Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnPrevImageEnable","btnNextImageEnable"), pdpPage),
							"To check the arrows displaying for product image thumbnails.",
							"The product only have 4 or less thumbnail images, thus the arrows are not displayed",
							"The arrow is displaying even the product only have 4 or less thumbnail images", driver);
				}
			}
			
			homePage.headers.navigateToPDP(prdSizeChart);
			Log.message(i++ + ". Navigated to Preoduct with size chart.", driver);
									
			Log.softAssertThat(pdpPage.verifyInitalImageMapping(),
					"Clicking on alternate image should populate in the main image space", 
					"Alternate image is populated in the main image space", 
					"Alternate image is not populated in the main image space", driver);
			
			pdpPage.selectSize();
			Log.message(i++ + ". Size Selected", driver);
			
			Log.softAssertThat(pdpPage.verifyColorSwatchesUpdation(),
					"Based on the size available color swatches should be display",
					"Based on the size available color swatches is displayed",
					"Based on the size available color swatches is not displayed", driver);
			
			if(!BrandUtils.isBrand(Brand.bh)) {
				pdpPage.clickSizeChartLink();
				Log.message(i++ + ". Clicked on Size chart.", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("modalSizechart"), pdpPage),
						"The Size Chart modal should be displayed!",
						"The Size Chart modal is displayed!",
						"The Size Chart modal is not displayed!", driver);

				pdpPage.closeSizeChartModalPopup();
				Log.message(i++ + ". Clicked on close button in Size Chart modal!", driver);
			}
			
			//Out of stock color and size variations are verified in 24800 test case
			pdpPage.selectColor();
			Log.message(i++ + ". Color Selected", driver);
								
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Product Added to cart", driver);
					
			//Step 5 - Navigate to the cart page
			
			ShoppingBagPage shoppingBagPg = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			shoppingBagPg.clickCouponToolTip();
			Log.message(i++ + ". Open coupon tool tip", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("lnkCloseToolTipCoupon"), shoppingBagPg), 
					"Coupon Tool tip should be displayed", 
					"Coupon Tool tip is getting displayed", 
					"Coupon Tool tip is not getting displayed", driver);
			
			shoppingBagPg.closeCouponToolTip();
			Log.message(i++ + ". Close coupon tool tip", driver);
			
			shoppingBagPg.applyPromoCouponCode("invalid");
			Log.message(i++ + ". Applying invalid coupon", driver);
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("txtCouponErrorDeskTab"), shoppingBagPg), 
						"Coupon Error should be displayed", 
						"Coupon Error is getting displayed", 
						"Coupon Error is not getting displayed", driver);
			
			} else {
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("txtCouponErrorMob"), shoppingBagPg), 
						"Coupon Error should be displayed", 
						"Coupon Error is getting displayed", 
						"Coupon Error is not getting displayed", driver);
			}
			
			SignIn signin = (SignIn) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			//Step 6 - Navigate to the checkout sign in page
			
			CheckoutPage checkoutPg = (CheckoutPage)signin.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);

			checkoutPg.continueToShipping("validformat@gmail.com|test123@");
			Log.message(i++ + ". Valid unregistered username", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("errUnregisteredUser"), checkoutPg), 
					"Error should be thrown when unregistered user try logging in", 
					"Error is thrown when unregistered user try logging in", 
					"Error is not thrown when unregistered user try logging in", driver);
			
			checkoutPg.continueToShipping("invalidformat@gmail|test123@");
			Log.message(i++ + ". Navigated to Shipping section", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("errUsername"), checkoutPg), 
					"Error should be thrown when invalid email id entered", 
					"Error is thrown when invalid email id entered", 
					"Error is not thrown when invalid email id entered", driver);
			
			//Step 7 - Navigate to Checkout page - step 1
			
			checkoutPg.continueToShipping(credential);
			Log.message(i++ + ". Navigated to Shipping section", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			//Step 8 - Navigate to Checkout page - step 2
			
			checkoutPg.clickEditAddressInBillingSection();
			Log.message(i++ + ". Editing Billing Address", driver);
			
			String postalCode = checkoutPg.getPostalCodeFromBillingAdd();
			
			String city = checkoutPg.getCityFromBillingAdd();
			
			checkoutPg.enterPostalCodeInBillingAdd("99999");
			Log.message(i++ + ". Entering invalid postal code", driver);
			
			Log.softAssertThat(checkoutPg.getCityFromBillingAdd().toLowerCase().contains(city.toLowerCase()), 
					"The city should not be auto populate when invalid postal code entered", 
					"The city is not auto populate when invalid postal code entered", 
					"The city is auto populate when invalid postal code entered", driver);
			
			checkoutPg.enterPostalCodeInBillingAdd("12121");
			Log.message(i++ + ". Entering valid postal code", driver);
			
			Log.softAssertThat(!(checkoutPg.getCityFromBillingAdd().toLowerCase().contains(city.toLowerCase())), 
					"The city should be auto populate when valid postal code entered", 
					"The city is auto populate when valid postal code entered", 
					"The city is not auto populate when valid postal code entered", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			checkoutPg.enterPostalCodeInBillingAdd(postalCode);
			Log.message(i++ + ". Entered postal code:: " + postalCode, driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continue to payment section", driver);
								
			//Verifying validation of credit fields are done in the C24111 
			checkoutPg.fillingCardDetails1(payment, false, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			//Step 9 - Navigate to Checkout page - step 3
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
						
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 10 - Order confirmation page
					
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Object obj = receipt.clickOnproductImageByIndex(0);
			Log.message(i++ + ". Clicked on product image link from order confirmation page", driver);
			
			Log.softAssertThat(obj.getClass().getName().contains("PdpPage"),
					"After clicking the product image link, User should redirects to the PDP page",
					"After clicking the product image link, User is redirected to the PDP page",
					"After clicking the product image link, User not redirected to the PDP page", driver);
			
			Log.testCaseResult();
		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // Ending finally
	} //M1_FBB_E2E_C24120
} //TC_FBB_E2E_C24120