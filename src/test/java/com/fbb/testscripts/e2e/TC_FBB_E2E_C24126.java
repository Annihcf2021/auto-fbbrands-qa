package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PaypalPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlatinumCardLandingPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.WishlistLoginPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24126 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24126(String browser) throws Exception {
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try {
			String usernameWishlistAccount = AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credentialWishlistAccount = usernameWishlistAccount + "|" + password;
			String checkoutShipAdd = "taxless_address";
			String prdDropship = TestData.get("prd_dropship_name");
			String prdVariation = TestData.get("prd_variation");
			String errorColor = TestData.get("error_color_2");
			String errorTextBoxColor = TestData.get("error_box_color");
			String dropShipMsg = demandwareData.get("DropShipMessage");
			String cardVisa = "card_Visa";
			
			{
				GlobalNavigation.createAccountAddProductsWishList(driver, prdVariation, false, credentialWishlistAccount);
			}
			
			//Step 1 - Navigate to the website
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Click on Wishlist link from footer section
			
			WishlistLoginPage wishListLogin = (WishlistLoginPage) homePage.footers.navigateToWishLists();
			Log.message(i++ + ". Navigated to 'Wishlist Login' page!", driver);
			
			//Step 4a - Click on search field in the header
			
			wishListLogin.clickSearchWishlist();
			Log.message(i++ + ". Clicked on Search Wishlist!", driver);
			
			Log.softAssertThat(wishListLogin.elementLayer.verifyElementDisplayed(Arrays.asList("txtWishlistSearchError"), wishListLogin), 
					"Wishlist search error should be displayed", 
					"Wishlist search error is displayed", 
					"Wishlist search error is not displayed", driver);
			
			wishListLogin.enterInSearchMail("invalid");
			Log.message(i++ + ". Entered email Id", driver);
			
			//Step 4b - Click on search field in the header
			
			wishListLogin.clickSearchWishlist();
			Log.message(i++ + ". Clicked on Search Wishlist!", driver);
			
			Log.softAssertThat(wishListLogin.elementLayer.verifyElementDisplayed(Arrays.asList("txtWishlistSearchError"), wishListLogin), 
					"Wishlist search error should be displayed", 
					"Wishlist search error is displayed", 
					"Wishlist search error is not displayed", driver);
			
			wishListLogin.enterInSearchMail(usernameWishlistAccount);
			Log.message(i++ + ". Entered email Id:: " + usernameWishlistAccount, driver);
			
			wishListLogin.clickSearchWishlist();
			Log.message(i++ + ". Clicked on Search Wishlist!", driver);
			
			Log.softAssertThat(wishListLogin.elementLayer.verifyElementDisplayed(Arrays.asList("txtWishlistSearchError"), wishListLogin), 
					"Wishlist should not be displayed when list is private", 
					"Wishlist is not displayed when list is private", 
					"Wishlist is displayed when list is private", driver);
			
			//step 2 - Navigate to user's Wish List
			
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(AccountUtils.generateEmail(driver), accountData.get("password_global"));
			Log.message(i++ + ". Navigated to My Account page as : " +AccountUtils.generateEmail(driver), driver);
			
			WishListPage wishlistPg = myAcc.navigateToWishListPage();
			Log.message(i++ + ". Navigated To Wishlist page", driver);
			
			wishlistPg.clickMakeWishlistPublicToggle("public");
			Log.message(i++ + ". Wishlist made as public!", driver);
			
			myAcc.signOutAccount();
			Log.message(i++ + ". Sign out account!", driver);
			
			wishListLogin = (WishlistLoginPage) homePage.footers.navigateToWishLists();
			Log.message(i++ + ". Navigated to 'Wishlist Login' page!", driver);
			
			wishlistPg = wishListLogin.searchWishlistUsingEmailAndViewWishlist(AccountUtils.generateEmail(driver));
			Log.message(i++ + ". Navigated to 'Wishlist' page!", driver);
			
			//Step 3 - Navigate to user's Wish List
			
			Log.softAssertThat(wishlistPg.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), wishlistPg), 
					"Wishlist should be displayed when list is public", 
					"Wishlist is displayed when list is public", 
					"Wishlist is not displayed when list is public", driver);
			
			Log.softAssertThat(wishlistPg.elementLayer.verifyTextContains("txtTitleWishlist","'s", wishlistPg), 
					"Wishlist should display name of the wishlist owner in heading", 
					"Wishlist is displaying name of the wishlist owner in heading", 
					"Wishlist is not displaying name of the wishlist owner", driver);
			
			Log.softAssertThat(wishlistPg.elementLayer.verifyElementDisplayed(Arrays.asList("txtDateAdded"), wishlistPg), 
					"Wishlist date added should be displayed", 
					"Wishlist date added is displayed", 
					"Wishlist date added is not displayed", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(wishlistPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnAddtoCart", "txtDateAdded", wishlistPg), 
						"Wishlist date added should be displayed below add to bag button", 
						"Wishlist date added is displayed below add to bag button", 
						"Wishlist date added is not displayed below add to bag button", driver);
			}else {
				Log.softAssertThat(wishlistPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtDateAdded", "btnAddtoCart", wishlistPg), 
						"Wishlist date added should be displayed above add to bag button", 
						"Wishlist date added is displayed above add to bag button", 
						"Wishlist date added is not displayed above add to bag button", driver);
			}
			
			Log.softAssertThat(wishlistPg.elementLayer.verifyElementDisplayed(Arrays.asList("txtProductRow"), wishlistPg), 
					"Wishlist product should be displayed", 
					"Wishlist product is displayed", 
					"Wishlist product is not displayed", driver);
			
			Log.softAssertThat(wishlistPg.elementLayer.verifyElementDisplayed(Arrays.asList("txtProductAttribute"), wishlistPg), 
					"Wishlist product attribute should be displayed", 
					"Wishlist product attribute is displayed", 
					"Wishlist product attribute is not displayed", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(wishlistPg.elementLayer.verifyElementDisplayed(Arrays.asList("txtProductAvailability_Mob"), wishlistPg), 
						"Wishlist product availability should be displayed", 
						"Wishlist product availability is displayed", 
						"Wishlist product availability is not displayed", driver);
			} else {
				Log.softAssertThat(wishlistPg.elementLayer.verifyElementDisplayed(Arrays.asList("txtProductAvailability_Desk_Tab"), wishlistPg), 
						"Wishlist product availability should be displayed", 
						"Wishlist product availability is displayed", 
						"Wishlist product availability is not displayed", driver);
			}
			
			wishlistPg.clickAddtoBagByIndex(0);
			Log.message(i++ + ". Product added to bag!", driver);
			
			Log.softAssertThat(wishlistPg.elementLayer.verifyElementDisplayed(Arrays.asList("btnCheckoutInMCOverlay"), wishlistPg), 
					"Checkout overlay should be displayed", 
					"Checkout overlay is displayed", 
					"Checkout overlay is not displayed", driver);
			
			ShoppingBagPage shoppingBagPg = wishlistPg.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Clicked in checkout overlay!", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementTextContains("txtWishlistItem", "wishlist", shoppingBagPg), 
					"'This is wishlist item' message should display in that product", 
					"'This is wishlist item' message is displaying in that product", 
					"'This is wishlist item' message is not displaying in that product", driver);
			
			//step 5 - Navigate to Product detail page
			
			PdpPage pdpPage = homePage.headers.navigateToPDPByProdName(prdDropship);
			Log.message(i++ + ". Navigated to Pdp page!", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyTextContains("txtProdInfos",dropShipMsg, pdpPage),
					"'This items ship directly from a third party brand' should be shown", 
					"'This items ship directly from a third party brand' is shown", 
					"'This items ship directly from a third party brand' is not shown", driver);
			
			pdpPage.selectAllSwatches();
			Log.message(i++ +". Selected all the swatches", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ +". Product added to cart", driver);
			
			shoppingBagPg = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ +". Clicked checkout now button from add to bag overlay", driver);
			
			//Step 6 - Navigate to the cart page
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyTextContains("txtWishlistItem", "This is a wishlist item.",shoppingBagPg),
					"\"This is a Wishlist item\" label should be displayed on the Regular item.",
					"\"This is a Wishlist item\" label is displayed on the Regular item.",
					"\"This is a Wishlist item\" label is not displayed on the Regular item.", driver);
			
			if(!BrandUtils.isBrand(Brand.el)) {
				PlatinumCardLandingPage plccLandPg = shoppingBagPg.navigateToPlatinumCardLandingPage();
				Log.message(i++ +". Clicked see details link", driver);
				
				Log.softAssertThat(plccLandPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), plccLandPg), 
						" The system should redirect the user to the PLCC Landing page", 
						" The system is redirected the user to the PLCC Landing page",
						" The system is not redirect the user to the PLCC Landing page", driver);
				
				shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
				Log.message(i++ +". Clicked on my bag icon on the header & navigated to the cart page",driver);
			}
			
			//step 7 - Navigate to the checkout sign in page
			SignIn signin = (SignIn) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			Log.softAssertThat(signin.elementLayer.verifyElementDisplayed(Arrays.asList("checkoutOrderTotalTable"), signin),
					"Order Summary section should be displaying on checkout sign in page",
					"Order Summary section is displaying on checkout sign in page",
					"Order Summary section is not displaying on checkout sign in page", driver);
			
			CheckoutPage checkoutPg = signin.checkoutAsGuest(usernameWishlistAccount);
			Log.message(i++ + ". Continued to guest checkout with: " + usernameWishlistAccount, driver);
			
			//Step 8 - Navigate to Checkout page - step 1
			
			checkoutPg.clickOnContinue();
			Log.message(i++ +". Clicked continue button", driver);			
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyCssPropertyForElement("errorLastName", "outline", errorTextBoxColor, checkoutPg),
					"Shipping fields should have red outline", 
					"Shipping fields have red outline",
					"Shipping fields don't have red outline", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementColor("errShippingFirstName", errorColor, checkoutPg),
					"Placeholders should turn red", 
					"Placeholders turned red",
					"Placeholders did not turn red", driver);
			
			checkoutPg.fillingShippingDetailsAsGuest("NO", checkoutShipAdd, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(false);
			Log.message(i++ + ". Use this as a Billing address check box disabled", driver);
			
			checkoutPg.continueToBilling();
			Log.message(i++ + ". Continued to Billing address", driver);
						
			//Step 9 - Navigate to Checkout page - step 2
			
			checkoutPg.enterFirstName("John");
			checkoutPg.enterLastName("Willian");
			Log.message(i++ +". Entered first name, Last name, and address line 1", driver);

			checkoutPg.enterPostalCodeInBillingAdd("Auto1");
			checkoutPg.enterAddress1("1 central park west");
			Log.message(i++ +". Entered invalid postal code", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyTextContains("txtZipcodeErrorInBilling","Invalid Zip", checkoutPg)
							&& checkoutPg.elementLayer.verifyElementColor("txtZipcodeErrorInBilling", errorColor, checkoutPg),
					"The system should display an error message as a placeholder text & text should display red color.", 
					"The system is displayed an error message as a placeholder text & text should display red color.",
					"The system is not displayed an error message as a placeholder text & text should display red color.", driver);
			
			if(Utils.isDesktop()) {
				Log.softAssertThat((checkoutPg.elementLayer.verifyElementEmpty("billingAddSelectedState",checkoutPg)) && (checkoutPg.getCityFromBillingAdd().isEmpty()), 
						" Auto state & city should not appear on the state & the city field.",
						" Auto state & city is not appear on the state & the city field.",
						" Auto state & city is appear on the state & the city field.", driver);
			} else {
				Log.softAssertThat((checkoutPg.elementLayer.verifyElementEmpty("billingAddSelectedState_Tablet",checkoutPg)) && (checkoutPg.getCityFromBillingAdd().isEmpty()), 
						" Auto state & city should not appear on the state & the city field.",
						" Auto state & city is not appear on the state & the city field.",
						" Auto state & city is appear on the state & the city field.", driver);
			}
			
			checkoutPg.enterPostalCodeInBillingAdd("10023");
			Log.message(i++ +". Entered valid postal code", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtZipcodeErrorInBilling"), checkoutPg),
					"The system should not display an error message as a placeholder text & text should display red color.", 
					"The system is not displayed an error message as a placeholder text & text should display red color.",
					"The system is displayed an error message as a placeholder text & text should display red color.", driver);
						
			Log.softAssertThat((!checkoutPg.getBillingAddSelectedState().isEmpty()) && (!checkoutPg.getCityFromBillingAdd().isEmpty()), 
					" Auto state & city should not appear on the state & the city field.",
					" Auto state & city is appear on the state & the city field.",
					" Auto state & city is not appear on the state & the city field.", driver);
			
			checkoutPg.enterPhoneCode("8015841847");
			Log.message(i++ +". Entered phone number field",driver);			
			
			checkoutPg.continueToPayment();
			Log.message(i++ +". Clicked continue button from billing section", driver);
			
			HashMap<String, String> cardDetails = checkoutPg.fillingCardDetails1(cardVisa, false, false);
			Log.message(i++ + ". Filled in Card details.", driver);
			
			boolean expiredDatePossible = checkoutPg.setExpiredDateInPayment();
			if(expiredDatePossible) {
				Log.message(i++ +". Set expired date", driver);
				
				checkoutPg.clickOnPaymentDetailsContinueBtn();
				Log.message(i++ +". Clicked continue button", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyTextContains("lblExpiredMonthMsg", "card expired",checkoutPg),
						"The system should display an error message card expired",
						"The system is displaying an error message card expired",
						"The system is not displaying an error message card expired", driver);
				
				checkoutPg.setCardNumberInPayment(cardDetails.get("CardNumber"));
				Log.message(i++ + ". Filled card number.", driver);
				
				checkoutPg.selectExpiryMonth(cardDetails.get("ExpMonth"));
				checkoutPg.selectExpiryYear(cardDetails.get("ExpYear"));
				Log.message(i++ +". Selected non-expired date", driver);
				
				checkoutPg.enterCVV(cardDetails.get("CVV"));
				Log.message(i++ + ". Entered CVV", driver);
			} else {
				Log.reference("Cannot set expired date when testing in January.");
			}
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			//step 10 - Navigate to Checkout page - step 3
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			checkoutPg.clickEditPaymentDetail();
			Log.message(i++ + ". Clicked edit in Payment Details.", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to payment.", driver);
			
			checkoutPg.selectPaypalPayment();
			Log.message(i++ + ". Selected PayPal radio button", driver);
			
			PaypalPage paypalPage = checkoutPg.clickOnPaypalButton();
			Log.message(i++ + ". Continued with PayPal", driver);
			
			checkoutPg = paypalPage.cancelPaypalByCancelLink();
			Log.message(i++ + ". Cancelled PayPal", driver);
			
			checkoutPg.selectCreditCardPayment();
			Log.message(i++ + ". Selected credit card radio button.", driver);
			
			cardDetails = checkoutPg.fillingCardDetails1(cardVisa, false, false);
			Log.message(i++ + ". Filled in Card details.", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			OrderConfirmationPage receipt = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 11 - Order confirmation page
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally
	}

}//M1_FBB_E2E_C24126