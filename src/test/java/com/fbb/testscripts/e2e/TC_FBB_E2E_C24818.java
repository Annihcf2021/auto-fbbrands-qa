package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PaypalConfirmationPage;
import com.fbb.pages.PaypalPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24818 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24818(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{	
			String paypalEmail = accountData.get("credential_alwaysapprove_paypal").split("\\|")[0];
			String paypalPassword = accountData.get("credential_alwaysapprove_paypal").split("\\|")[1];
			String prdVariation1 = TestData.get("prd_variation1");
						
			//Always Approve
			String addressAlwaysApprove = "plcc_always_approve_address";			
			
			//Step1
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to the 'Full Beauty Brands' Home Page!", driver);
			
			//Step2
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation1);
			Log.message(i++ + ". Navigated the To PDP page : "+pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to the bag", driver);
			
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to the shopping bag page", driver);
			
			
			PaypalPage paypalPage = shoppingBagPg.clickOnPaypalButton();
			Log.message(i++ + ". Clicked on Paypal button in shopping bag page", driver);
			
			//Step 3
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			PaypalConfirmationPage pcp = paypalPage.enterPayapalCredentials(paypalEmail, paypalPassword);
			Log.message(i++ + ". Continued with Paypal Credentials.", driver);
							
			pcp.addNewPaypalAddress(addressAlwaysApprove);
			Log.message(i++ + ". Entered Always Approve address", driver);
				
			CheckoutPage checkoutPg = pcp.clickContinueConfirmation();
			Log.message(i++ + ". Clicked on Continue button.", driver);
						
			if (checkoutPg.elementLayer.verifyPageElements(Arrays.asList("modalCheckoutPlcc"), checkoutPg)) {
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("modalCheckoutPlcc"), checkoutPg),
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed",
						"PLCC modal is not getting displayed", driver);
					
				//Step5
				checkoutPg.clickGetItTodayInPLCC();
				Log.message(i++ + ". Click on 'Get It Today' in PLCC!", driver);
				
				//Step-6 
				checkoutPg.typeTextInSSN("0001");
				Log.message(i++ +". Entered SSN number", driver);
					
				checkoutPg.selectDateMonthYearInPLCC2("01","01","1990");
				Log.message(i++ +". Selected date of birth", driver);
				
				checkoutPg.typeTextInMobileInPLCC("3334445555");
				Log.message(i++ +". Updated phone number", driver);
				
				checkoutPg.checkConsentInPLCC("YES");
				Log.message(i++ +". Selected consonent checbox", driver);				
					
				checkoutPg.clickOnAcceptInPLCC();
				Log.message(i++ +". Clicked Yes, I Accept button", driver);					
				
				if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("underReviewModal"), checkoutPg)) {
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("underReviewModal"), checkoutPg),
							"The Application Under Review modal will pop up.",
							"The Application Under Review modal is pop up.",
							"The Application Under Review modal is not pop up.", driver);
				} else {
					Log.warning("Under review modal is not displayed");
				}
				
				checkoutPg.dismissCongratulationModal();
				Log.message(i++ +". Clicking the Continue to Checkout button", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("step3Active", "btnPlaceOrder"), checkoutPg), 
						"User should be taken to Checkout 'Review and Place order' section(Step-3)", 
						"User is taken to Checkout 'Review and Place order' section(Step-3)", 
						"User is not taken to Checkout 'Review and Place order' section(Step-3)", driver);
				
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
					
				//Step 9 - Review the order and select the "Place Order" button.
				
				checkoutPg.clickOnPlaceOrder();
				Log.message(i++ + ". Clicked on place order button", driver);
				
				if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg)) {
					Log.warning("ALWAYS APPROVE address did not return valid PLCC card number.");
				} else {
					Log.message(i++ + ". Order Placed successfully", driver);
					
					OrderConfirmationPage orderPage = new OrderConfirmationPage(driver).get();
					Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage),
							"The last 4 digits of the PLCC card should be be displayed.",
							"The last 4 digits of the PLCC card is be displayed.",
							"The last 4 digits of the PLCC card is not displayed.", driver);
				}	
			} else {
				Log.fail("Checkout step-1 overlay is not displayed hence cant proceed further", driver);
			}
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally
	}
}
