package com.fbb.testscripts.e2e;

import java.util.Arrays;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.fbb.pages.HomePage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.EmailPreferencePage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24598 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	

	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "myaccount", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24598(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			String username = AccountUtils.generateEmail(driver);
			String username1 = accountData.get("temproryEmail");
			String password = accountData.get("password_global");
			
			String credential = username + "|" + password;
			String credential1 = username1 + "|" + password;
			
			{
				GlobalNavigation.registerNewUser(driver, 0, 0, credential);
				GlobalNavigation.registerNewUser(driver, 0, 0, credential1);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Browser launched with " + Utils.getWebSite(), driver);
			
			//Step 1 - Verify the functionality Email & Edit link

			EmailPreferencePage eppage = homePage.headers.navigateToMyAccount(username, password, true).clickOnEmailPrefLink();
			Log.message(i++ + ". Navigated to Email Preference Page.", driver);
			
			if(eppage.currentBrandSubscribtionStatus()) {
				eppage.unSubscribeCurrentbrand();
			}
			
			Log.softAssertThat(eppage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkEditCurrentEmailIcon", "lblCurrentEmail", eppage), 
					"Edit link should be displayed to the right side of the User's email address.", driver);
			
			ProfilePage profile = eppage.clickEdit();
			Log.message(i++  + ". Clicked on Edit link.", driver);

			Log.softAssertThat(profile.elementLayer.verifyPageElements(Arrays.asList("readyElement"), profile), 
					"On clicking the Edit, \r\n" + 
					"-The user should be taken to My Account - Profile Page to edit the email address.", driver);
			
			eppage = profile.clickOnEmailPrefLink();
			Log.message(i++ + ". Clicked on Email preference link.", driver);

			//Step  2 - Verify the functionality of Subscribe button
			
			if (Utils.isMobile()) {
				Log.softAssertThat(eppage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtEmailBenefits", "btnCurrentBrandEmailSubscribe", eppage)
								|| eppage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtEmailBenefits", "toggleCurrentBrandEmailSubscription", eppage), 
						"Subscribe button should be displayed below the Preference options", driver);
			} else {
				Log.softAssertThat(eppage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtEmailBenefits", "btnCurrentBrandEmailSubscribe", eppage)
								|| eppage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "toggleCurrentBrandEmailSubscription", "txtEmailBenefits", eppage), 
						"Subscribe button should be displayed below the Preference options", driver);
			}
			
			eppage.clickOnSubscribeRegisterUser();
			Log.message(i++ + ". Clicked on Subscribe button.", driver);

			Log.softAssertThat(eppage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), eppage), 
					"Then System should display the Email Subscription Thank You page.", driver);
			
			//step 3 - Verify the functionality of Other Brand Copy
			
			Log.softAssertThat(eppage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionCurrentBrand", "sisterBrands", eppage), 
					"Other Brand Copy should be displayed below the Subscribed Brand Details", driver);
			
			Log.softAssertThat(eppage.verifySubscribePosition(), 
					"Subscription-State should be displayed right of Other Brand Description", driver);
			
			eppage.clickSubscriptionSliderForSisterBrands(1);
			Log.message(i++ + ". Clicked on 1st sister brand slider icon.", driver);
			
			Log.softAssertThat(eppage.getSubscriptionStatusSisterBrands(1), 
					"Once the user click on Subscription toggle , it should display as active state", driver);
			
			//Step 4 - Verify the functionality of Unsubscribed
			
			eppage.unSubscribeSisterbrand(1);
			Log.message(i++ + ". Unsubscribed 1st sister brand.", driver);
			
			SignIn signIn = homePage.headers.signOut();
			Log.message(i++ + ". Clicked on signed out from account.", driver);
						
			eppage = signIn.navigateToMyAccount(username1, password).clickOnEmailPrefLink();
			Log.message(i++ + ". Navigated to Email preference page.", driver);
			
			if(!eppage.getSubscriptionStatusSisterBrands(1)) {
				eppage.clickSubscriptionSliderForSisterBrands(1);
			}
					
			Log.softAssertThat(eppage.getSubscriptionStatusSisterBrands(1), 
					"The system should display Subscription toggle as an active state.", driver);
			
			eppage.clickSubscriptionSliderForSisterBrands(1);
			Log.message(i++ + ". Clicked on subscription button.", driver);
			
			Log.softAssertThat(eppage.elementLayer.verifyPageElements(Arrays.asList("emailUnSubscribePopup"), eppage), 
					"The system launch the Email Frequency Modal", driver);
			
			Log.softAssertThat(eppage.elementLayer.verifyPageElements(Arrays.asList("radioFewerContact"), eppage), 
					"The system should display all the reason to Unsubscribe", driver);
			
			eppage.selectRadioOnUnsubscribeModal("none");
			Log.message(i++ + ". Clicked on Unsubscribe radion button.", driver);
			
			eppage.selectOptionInUnSubscribeByIndex(1);
			Log.message(i++ + ". Clicked on " + 1 + "st option in unsubscribe list.", driver);
			
			eppage.clickOnSubmit();
			Log.message(i++ + ". Clicked on submit button.", driver);
			
			Log.softAssertThat(!eppage.getSubscriptionStatusSisterBrands(1), 
					"Toggle of the respective brand should change to Unsubscribed State", driver);
			
			eppage.clickSubscriptionSliderForSisterBrands(1);

			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24595
