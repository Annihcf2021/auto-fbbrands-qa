package com.fbb.testscripts.e2e;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.iParcelCheckoutPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.CollectionUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C25176 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C25176(String browser) throws Exception
	{
		Log.testCaseInfo();
	
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{	
			String searchText = TestData.get("product_search_terms").split("\\|")[0];
			String intrnAddress = "intrn_shipping_canada";
			String prdBonus = TestData.get("prd_fixed-bonus");
			String prdVariation = TestData.get("prd_variation");
			String payment = "card_InternationalShipp";
			String userEmailId = AccountUtils.generateEmail(driver);
			String userPassword = accountData.get("password_global");
			
			{
				HashMap<String, String> accountDetails = new HashMap<>();
				GlobalNavigation.setupTestUserAccount(driver, accountDetails, userEmailId+"|"+userPassword);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			homePage.headers.typeTextInSearchField(searchText);
			Log.message(i++ + ". Typed in the Search Field!", driver);
			
			Log.softAssertThat(homePage.headers.getEnteredTextFromSearchTextBox().trim().equals(searchText.trim()), 
					"Text should be entered in the Search text box", 
					"Text is entered in the Search text box", 
					"Text is not entered in the Search text box", driver);
			
			homePage.headers.navigateToMyAccount(userEmailId, userPassword);
			Log.message(i++ + ". Navigated to My Account page as : " + userEmailId, driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			List<String> prdNames = new ArrayList<String>();
			prdNames.add(pdpPage.getProductName().toLowerCase());
		
			pdpPage.selectAllSwatches();
			Log.message(i++ + ". Selected all the swatches in PDP page!", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked Add to cart button!", driver);
			
			pdpPage.clickOnContinueShoppingInMCOverlay();
			Log.message(i++ + ". Clicked on Continue Shopping button!", driver);
			
			pdpPage = homePage.headers.navigateToPDP(prdBonus);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			prdNames.add(pdpPage.getProductName().toLowerCase());
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart!", driver);
			
			ShoppingBagPage cartPage = pdpPage.navigateToShoppingBag();
			Log.message(i++ + ". Clicked on 'Checkout Now' button!", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnCheckoutNowFooter", "lnkInternationalShipping", cartPage), 
					"'International Shipping' icon and link should displaying below the 'Checkout now' button", 
					"'International Shipping' icon and link is displaying below the 'Checkout now' button",
					"'International Shipping' icon and link is not displaying below the 'Checkout now' button", driver);
			
			cartPage.clickOnInternationalShippingLink();
			Log.message(i++ + ". Clicked on 'International Shipping' button!", driver);
			
			if(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("modalInternationalExclusion"), cartPage)) {
				Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("modalInternationalExclusion"), cartPage), 
						"International Exclusion modal should be displayed", 
						"International Exclusion modal is displayed", 
						"International Exclusion modal is not displayed", driver);
				
				List<String> name=cartPage.getproductNamesInInternationalExclusion();				
				for(String prdName:name) {
					prdNames.remove(prdName);
				}
				
				cartPage.clickOnRemoveItemBtnInternationalExclusion();
				Log.message(i++ + ". Clicked on 'Remove Items' in International Shipping!", driver);
			}
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblInternShipPromotionExclusion"), cartPage), 
					"Verify the promotions and exclusion message on International Shipping modal", 
					"The promotions and exclusion message is displayed on International Shipping modal", 
					"The promotions and exclusion message is not displayed on International Shipping modal", driver);
			
			cartPage.selectConutryOnInternationalShipping(1);
			Log.message(i++ + ". Selected country!", driver);
			
			iParcelCheckoutPage iParcelChkoutPg = cartPage.clickOnContinueInInternationalShipping();
			Log.message(i++ + ". Clicked on Continue in International shipping modal!", driver);
			
			Log.softAssertThat(iParcelChkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), iParcelChkoutPg), 
					"iParcel Checkout Page should be displayed", 
					"iParcel Checkout Page is displayed", 
					"iParcel Checkout Page is not displayed", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			LinkedHashMap<String, String> shippingDetails1 = new LinkedHashMap<String, String>();
			shippingDetails1 = iParcelChkoutPg.fillingShippingDetails(intrnAddress);
			
			String userEmail = null;
			for(String key:shippingDetails1.keySet()){			
				if(key.contains("email"))
					userEmail = key.split("\\_")[2];	
			}
			
			iParcelChkoutPg.fillingCardDetails(payment);
			Log.message(i++ + ". Credit card fields filled as guest!", driver);
			
			iParcelChkoutPg.selectUnSelectTermsAndCondition(true);
			Log.message(i++ + ". Accepted Terms and Condition!", driver);
			
			iParcelChkoutPg.selectUpdateButton();
			Log.message(i++ + ". Clicked on update button!", driver);
			
			OrderConfirmationPage receipt = iParcelChkoutPg.clickOnContinueButtonAfterEnteringShippingAndCardDetails();
			Log.message(i++ + ". Clicked on submit button!", driver);
			
			Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			Log.softAssertThat(receipt.checkEnteredShippingAddressReflectedInInternationalOrderReceipt(intrnAddress), 
					"Same shipping address should display in the receipt which is entered in checkout shipping detail", 
					"Same shipping address is displaying", 
					"Different shipping address is displaying", driver);
			
			Log.softAssertThat(receipt.checkEnteredBillingAddressReflectedInInternationalOrderReceipt(intrnAddress), 
					"Same billing address should display in the receipt which is entered in checkout billing detail", 
					"Same billing address is displaying", 
					"Different billing address is displaying", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblOrderReceipt", userEmail, receipt), 
					"The order receipt should send to the entered mail ID", 
					"The order receipt is send to the entered mail ID", 
					"The order receipt is not send to the entered mail ID", driver);
				
			Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt), 
					"The user can able to take print out of the order receipt", 
					"The user can able to print the order receipt", 
					"The user cannot able to print the order receipt", driver);
			
			List<String> receiptPrdNames = receipt.getOrderedPrdListNames();
			
			Log.softAssertThat(CollectionUtils.compareTwoList(receiptPrdNames, prdNames), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24022
