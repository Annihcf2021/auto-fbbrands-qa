package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24540 extends BaseTest{
	
	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24540(String browser) throws Exception
	{
		Log.testCaseInfo(); 
		
		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			//Step 1 - Navigate to the website
			
			String prdVideo = TestData.get("prd_video");
			String prdDropShip = TestData.get("prd_dropship");
			String payment = "card_Visa";
			String userMail = accountData.get("credential_plcc_" + Utils.getCurrentBrandShort().substring(0, 2));
			String dropShipMsg = demandwareData.get("DropShipMessage");
			
			{
				GlobalNavigation.loginAddNewAddress(driver, 1, userMail);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 3 - Navigate to Product detail page.
			PdpPage pdpPage;
			if(envProperties.get("videoTestEnabled").contains(Utils.getCurrentBrandShort())) {
				pdpPage = homePage.headers.navigateToPDP(prdVideo);
				Log.message(i++ + ". Navigated To PDP page : " + pdpPage.getProductName(), driver);
				
				if(!Utils.isMobile()){
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkVideoIcon_Des_Tab"), pdpPage), 
							"Video link should be displayed", 
							"Video link is getting displayed", 
							"Video link is not getting displayed", driver);
				} else {
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkVideoIcon_Mob"), pdpPage), 
							"Video link should be displayed", 
							"Video link is getting displayed", 
							"Video link is not getting displayed", driver);
				}
				
				pdpPage.clickMainVideoLink();
				Log.message(i++ + ". Video link clicked", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("videoBoxPlayer"), pdpPage), 
						"Video player box should be displayed", 
						"Video player box is getting displayed", 
						"Video player box is not getting displayed", driver);
				
				if(pdpPage.getNoOfAlternateImage() > 1) {
					if(Utils.isMobile()) {
						pdpPage.clickOnAlternateImages(0);
						Log.message(i++ + ". Alternative image selected", driver);
						
						Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("videoBoxPlayer"), pdpPage), 
								"Video player box should not displayed", 
								"Video player box is not getting displayed", 
								"Video player box is getting displayed", driver);
					}
				}
				pdpPage.clickMainVideoLink();
				Log.message(i++ + ". Video link clicked", driver);
				
				pdpPage.selectColor();
				Log.message(i++ + ". Selected Color", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("videoBoxPlayer"), pdpPage), 
						"Video player box should not displayed", 
						"Video player box is not getting displayed", 
						"Video player box is getting displayed", driver);
			} else {
				pdpPage = homePage.headers.navigateToPDP(prdDropShip);
				Log.message(i++ + ". Navigated To PDP page : " + pdpPage.getProductName(), driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyTextContains("txtProdInfos",dropShipMsg, pdpPage),
						"'This items ship directly from a third party brand' should be shown", 
						"'This items ship directly from a third party brand' is shown", 
						"'This items ship directly from a third party brand' is not shown", driver);
			}
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to bag", driver);
			
			//Step 4 - Navigate to the cart page
			
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			String imgAmexCard;
			if(BrandUtils.isBrand(Brand.bh)){
				imgAmexCard = "imgPaymentAmex_BH";
			}else{
				imgAmexCard = "imgPaymentAmex";
			}
			
			if (BrandUtils.isBrand(Brand.el)) {
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyPageElements(Arrays.asList("imgPaymentVisa","imgPaymentMaster",imgAmexCard,"imgPaymentDiscover","imgPaymentPaypal"), shoppingBagPg), 
						"Mode of payment should be displayed - 'Visa', 'Master', 'Amex', 'Discover', 'Paypal'",
						"Mode of payment is displayed - 'Visa', 'Master', 'Amex', 'Discover', 'Paypal'",
						"Some Mode of payment is not displayed", driver);
			} else {
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyPageElements(Arrays.asList("imgPaymentVisa","imgPaymentMaster",imgAmexCard,"imgPaymentDiscover","imgPaymentPaypal","imgPaymentPlatinum"), shoppingBagPg), 
						"Mode of payment should be displayed - 'Visa', 'Master', 'Amex', 'Discover', 'Paypal', 'Platinum'",
						"Mode of payment is displayed - 'Visa', 'Master', 'Amex', 'Discover', 'Paypal', 'Platinum'",
						"Some Mode of payment is not displayed", driver);
			}
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyHorizontalAllignmentOfElements(driver, "divCartSummary", "paymentHeading", shoppingBagPg), 
						"The Payment Accepted content should be present in the Cart to the left of the Cost Summary section.",
						"The Payment Accepted content is present in the Cart to the left of the Cost Summary section.",
						"The Payment Accepted content is not present in the Cart to the left of the Cost Summary section.", driver);
			} else {
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "paymentHeading", "divCartSummary", shoppingBagPg), 
						"The Payment Accepted content should be present in the Cart to the left of the Cost Summary section.",
						"The Payment Accepted content is present in the Cart to the left of the Cost Summary section.",
						"The Payment Accepted content is not present in the Cart to the left of the Cost Summary section.", driver);
			}
			
			SignIn signin = (SignIn) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			//Step 5 - Navigate to the checkout sign in page
			
			CheckoutPage checkoutPg = (CheckoutPage)signin.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);
			
			checkoutPg.continueToShipping(userMail);
			Log.message(i++ + ". Navigated to Shipping section", driver);
			
			if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("modalCheckoutPlcc"),checkoutPg)){
				checkoutPg.closePLCCOfferByNoThanks();
				Log.message(i++ +". Clicked No thanks button from PLCC Overlay", driver);
			}
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
			
			//Step 6 - Navigate to Checkout page - step 3
			checkoutPg.continueToPayment();
			Log.message(i++ +". Navigated to payment details section", driver);
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("plccSelectedCard"),checkoutPg)) {
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
				
				checkoutPg.clickOnPlaceOrder();
				Log.message(i++ +". Click on Place order with PLCC address!", driver);
			}
			
			OrderConfirmationPage receipt = null;
			
			if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("readyElemForOrdCnfPg"),checkoutPg)) {
				receipt = new OrderConfirmationPage(driver).get();
				Log.message(i++ + ". Order Placed successfully", driver);
				
			} else if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("errPlaceOrder"),checkoutPg)
					|| !(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("plccSelectedCard"),checkoutPg))){
				
				checkoutPg.clickEditBillingAddressPLCCError();
				Log.message(i++ +". Click on edit Billing address!", driver);
				
				checkoutPg.clickOnAddNewPaymentMethod();
				Log.message(i++ +". Click on add new payment method!", driver);
				
				checkoutPg.fillingCardDetails1(payment, false, false);
				Log.message(i++ + ". Credit Card Details filling Successfully", driver);
				
				checkoutPg.clickOnPaymentDetailsContinueBtn();
				Log.message(i++ + ". Continued to Review & Place Order", driver);
					
				Log.softAssertThat(checkoutPg.comparePaymentBeforeOrderWithEnteredPaymentMethod(payment), 
						"The system should keep the payment details", 
						"Payment method is saved and displayed", 
						"Payment method is not saved correctly", driver);
				
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
				
				OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
				
				receipt = checkoutPg.clickOnPlaceOrderButton();
				Log.message(i++ + ". Order Placed successfully", driver);
			}
			
			//Step 7 - Order confirmation page
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally
	}
}//TC_FBB_E2E_C24540
