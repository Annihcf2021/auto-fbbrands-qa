package com.fbb.testscripts.e2e;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24580 extends BaseTest {
	

	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader envData = EnvironmentPropertiesReader.getInstance("env");
	
	@Test(groups = { "pdp", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24580(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			String divBackgroundColor = TestData.get("recommendation_background");
			String productKey= TestData.get("prd_variation");
			String brandsReccommendationAlignmentHorizantally = envData.get("reccommendationAlignmentHorizantally");
			String recommendation, recommendationHeading, recommendationHorizontal, recommendationSalePrice, recommendationOriginalPrice, recommendationProductBatch, recommendationVertical = null;
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(productKey);
			Log.message(i++ +". Navigated to PDP page" + pdpPage.getProductName(), driver);
			
			//Step-1 Verify the product recommendation headline in PDP page.
			
			if(BrandUtils.isBrand(Brand.bh) || BrandUtils.isBrand(Brand.bhx)) {
				recommendation = "sectionRecommendation_BH";
				recommendationHeading = "txtRecommendationHeadingBH";
				recommendationHorizontal = "sectionRecommendationMobileTabletBH";
				recommendationSalePrice = "prodSalePriceRecommendation_BH";
				recommendationOriginalPrice = "recommendationlblOriginalPriceInSalePrice_BH";
				recommendationProductBatch = "productBadgeInRecommendationDesktop_BH";
			} else if(Utils.isDesktop()) {
				recommendation = "sectionRecommendationDesktop";
				recommendationHeading = "txtRecommendationHeadingDesktop";
				recommendationHorizontal = "sectionRecommendationDesktopHorizantal";
				recommendationVertical = "sectionRecommendationDesktopVertical";
				recommendationSalePrice = "prodSalePriceRecommendationDesktop";
				recommendationOriginalPrice = "recommendationlblOriginalPriceInSalePrice";
				recommendationProductBatch = "productBadgeInRecommendationDesktop";
			} else {
				recommendation = "sectionRecommendationMobileTablet";
				recommendationHeading = "txtRecommendationHeadingTabMobile";
				recommendationHorizontal = "sectionRecommendationHorizantal";
				recommendationSalePrice = "prodSalePriceRecommendationMobileTablet";
				recommendationOriginalPrice = "recommendationlblOriginalPriceInSalePriceMobile";
				recommendationProductBatch = "productBadgeInRecommendationMobileTablet";
			}
			
			Log.softAssertThat(pdpPage.elementLayer.verifyTextContains(recommendationHeading, "You may also like", pdpPage),
					"\"You may also like \" label should be displayed as Headline", 
					"\"You may also like \" label is displayed as Headline", 
					"\"You may also like \" label is not displayed as Headline", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement(recommendation, "background-color", divBackgroundColor, pdpPage), 
					"The Recommendation section should be enclosed in a gray section box", 
					"The Recommendation section is enclosed in a gray section box", 
					"The Recommendation section is not enclosed in a gray section box", driver);
			
			if((!Utils.isDesktop()) || brandsReccommendationAlignmentHorizantally.contains(Utils.getCurrentBrandShort().substring(0, 2))) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionPrdDetails", recommendationHorizontal, pdpPage),
						"Product Recommendation pane should be displayed below the product details in Horizondal view." ,
						"Product Recommendation pane is displayed below the product details in Horizondal view.",
						"Product Recommendation pane is not displayed below the product details in Horizondal view.", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, recommendationVertical, "sectionPrdDetails", pdpPage),
						"Product Recommendation pane should be displayed right side of the product details in vertical view." ,
						"Product Recommendation pane is displayed right side of the product details in vertical view.",
						"Product Recommendation pane is not displayed right side of the product details in vertical view.", driver);
			}

			//Step-2 & 3 Verify the attributes of the recommendation product tile in PDP page.
			
			if(pdpPage.elementLayer.verifyPageElements(Arrays.asList(recommendationSalePrice), pdpPage)) {
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList(recommendationOriginalPrice, recommendationSalePrice), pdpPage), 
						"For Sale Price Product, It should show Original Price & Sales Price.", 
						"Original Price & Sales Price displayed!", 
						"Original Price or Sales price Not displayed", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement(recommendationOriginalPrice, "text-decoration", "line-through", pdpPage), 
						"Original Price should displayed with strikethrough", 
						"Original Price displayed with strikethrough!", 
						"Original Price not displayed with strikethrough.", driver);
			} else {
				Log.reference("Product is not a sale price product");
			}
			
			Log.softAssertThat(!pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList(recommendationProductBatch),pdpPage),
					"'Product badge' should not be displayed",
					"'Product badge' is not displayed",
					"'Product badge' is getting displayed", driver);
			
			String recommendationPrdId = pdpPage.clickInStockRecommendationPrdImage();
			Log.message(i++ +". Clicked recommendation product", driver);
			
			String prdName = pdpPage.getProductName();
			String prdId = pdpPage.getProductID().trim();

			Log.softAssertThat(prdId.equalsIgnoreCase(recommendationPrdId), 
					"When user click / tab on the product main image or product title, the system should navigate the user to the selected product detail page",
					"When user click / tab on the product main image or product title, the system is navigating to the selected product detail page",
					"When user click / tab on the product main image or product title, the system is not navigating to the selected product detail page", driver);
			
			Log.softAssertThat(pdpPage.verifyRecommendationTileContent(), 
					"Attributes displayed for the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
					"Attributes displayed in the recommendations product tile: \"Product Main image\", \"Name of the Product\", \"Price of the product.\"",
					"Attributes not displayed recommendations product tile", driver);
			
			pdpPage.selectAllSwatches();
			Log.message(i++ + ". All swatches in PDP are selected", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ +". Clicked add to bag button", driver);
			
			if(pdpPage.elementLayer.isElementsDisplayed(Arrays.asList("mdlMiniCartOverLay"), pdpPage)) {
				pdpPage.clickOnContinueShoppingInMCOverlay();
				Log.message(i++ +". Clicked Continue shopping button from overlay", driver);
			} else {
				Log.ticket("SC-5885");
			}
			
			ShoppingBagPage cart=homePage.headers.navigateToShoppingBagPage();
			Log.softAssertThat(cart.getProductName(0).equalsIgnoreCase(prdName),
					"Added recommended product should be displayed in cart page", 
					"Added recommended product is displayed in cart page", 
					"Added recommended product is not displayed in cart page", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	}// M1_FBB_E2E_C24580
			
		
	@Test(groups = { "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M2_FBB_E2E_C24580(String browser) throws Exception
	{
		Log.testCaseInfo();
		
		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			String lvl1 = TestData.get("level-1");
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			PlpPage plpPage = homePage.headers.navigateTo(lvl1);
			Log.message(i++ +". Navigated to PLP", driver);
			
			String[] prodId = plpPage.getListOfProductId(20);
			String[] url = plpPage.getProductUrl(20);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prodId[0]);
			Log.message(i++ + ". Navigated to " + pdpPage.getProductName(), driver);
			List<String> prdNames = new ArrayList<String>();
			
			String productimage = pdpPage.getProdImageName();
			
			String color = pdpPage.selectColor();
			Log.message(i++ + ". Color selected: " + color, driver);
						
			//Step-5 Navigates to different product PDPs & view some products through PDP
			
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("recentlyViewedSection"), pdpPage),
					"Check the recently viewed section is not displaying when user searches first product",
					"The recently viewed section is not displaying when user searches first product",
					"The recently viewed section is displaying when user searches first product", driver);
			
			pdpPage = homePage.headers.navigateToPDP(prodId[1]);
			Log.message(i++ + ". Navigated to: " + pdpPage.getProductName(), driver);
			
			String recentlyprodimage = pdpPage.getRecentlyViewedSectionProdImageName();
			Log.message("PDP product image: " + productimage);
			Log.message("Recenltly Viewed product image: " + recentlyprodimage);
			
			if(productimage.contains("noimagefound")) {
				productimage = productimage.split("-")[0];
				recentlyprodimage = recentlyprodimage.split("-")[0];
			}
			
			if(recentlyprodimage.contains("noimagefound") && productimage.contains("noimagefound")) {
				Log.warning("Unconfigured image or data feed.");
			} else {
				Log.softAssertThat(recentlyprodimage.contains(productimage), 
						"Product Default image should be displayed in recently viewed section when user selects different color from PDP",
						"Product Default image is displayed in recently viewed section when user selects different color from PDP",
						"Product Default image is not displayed in recently viewed section when user selects different color from PDP", driver);
			}
		
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "detailsReviewSection", "recentlyViewedSection", pdpPage),
					"To check the recently viewed section is displayed below the reviews and details section!",
					"The recently viewed section is displayed below the reviews and details section!",
					"The recently viewed section is not displayed below the reviews and details section!", driver);
					
			for (int w = 2; w < url.length - 3; w++) {
				Utils.openNewTab(driver);
				List<String> handle = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(handle.get(handle.size() - 1));
				driver.get(url[w]);
				prdNames.add(pdpPage.getProductName());
				Utils.waitForPageLoad(driver);
				
				if(Utils.isDesktop()) {
					if(pdpPage.getNoOfProdDisplayInRecentView()==4){
						Log.softAssertThat(!pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("recentlyViewSectionNextArrow","recentlyViewSectionPrevArrow"), pdpPage),
							"Check the recently viewed section should not have 'Next' and 'Prev' arrows until 4 products",
							"The recently viewed section is not have 'Next' and 'Prev' arrows until 4 products",
							"The recently viewed section is  having 'Next' and 'Prev' arrows until 4 products", driver);
					}
				}
			}
			pdpPage = homePage.headers.navigateToPDP(prodId[url.length - 1]);
			int size = pdpPage.getNoOfProdDisplayInRecentView();
			List<String> productList = pdpPage.getRecentlyViewedsectionAllProdImageName();
			
			Log.softAssertThat(pdpPage.verifyProductNames(productList, prdNames),
					"To check the recently viewed section is having visited product!",
					"The recently viewed section is having visited product!",
					"The recently viewed section is not having visited product!", driver);
	
			Log.softAssertThat(( size <= 12 ) && ( size != 0 ),
					"To check the recently viewed section displaying the 12 or less new viewed product!",
					"The recently viewed section is displaying the 12 or less new viewed product",
					"The recently viewed section is not displaying the 12 or less new viewed product!", driver);
	
			if(Utils.isDesktop()) {
			Log.softAssertThat(pdpPage.elementLayer.verifyListElementSize("recentlyViewedCurrentProd", 4, pdpPage),
					"To check the recently viewed section visible 4 product!",
					"The recently viewed section is visible 4 product",
					"The recently viewed section is not visible 4 product!", driver);
			}
			
			Log.softAssertThat(pdpPage.verifyRecentlyViewedTileContent(),
					"Check 'Recently Viewed' section have 'Product Name', 'Product price' and 'Product image'",
					"'Recently Viewed' section have 'Product Name', 'Product price' and 'Product image'",
					"'Recently Viewed' section is not having 'Product Name', 'Product price' and 'Product image'", driver);
			
					
			if(pdpPage.elementLayer.verifyPageElements(Arrays.asList("recentlyViewed1stStdPrice","recentlyViewed1stSalePrice"), pdpPage)){
				Log.softAssertThat(pdpPage.elementLayer.verifyListElementSize("recentlyViewedSalePrice", pdpPage.getNoOfSalePricesInRecenltyViewed(), pdpPage)
					&& pdpPage.elementLayer.verifyListElementSize("recentlyViewedStdPrice", pdpPage.getNoOfStdPricesInRecenltyViewed(), pdpPage), 
					"For Sale Price Product, It should show Original Price & Sales Price.", 
					"Original Price & Sales Price displayed!", 
					"Original Price or Sales price Not displayed", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("recentlyViewedStdPrice", "text-decoration", "line-through", pdpPage), 
					"Original Price should displayed with strikethrough", 
					"Original Price displayed with strikethrough!", 
					"Original Price not displayed with strikethrough.", driver);
			
			}else{
				Log.reference("Product is not a sale price product");
			}
			if(!Utils.isMobile()) {	
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("recentlyViewSectionNextArrow","recentlyViewSectionPrevArrow"), pdpPage),
						"Check the recently viewed section is having 'Next' and 'Prev' arrow",
						"The recently viewed section is having 'Next' and 'Prev' arrow",
						"The recently viewed section is not having 'Next' and 'Prev' arrow", driver);
			}
			
			String name1=pdpPage.clickRecentlyViewedPrdImageByIndex(1);
			String productName=pdpPage.getProductName();

			Log.softAssertThat(productName.toLowerCase().contains(name1.toLowerCase()), 
					"When user click / tab on the product main image or product title, the system should navigate the user to the selected product detail page",
					"When user click / tab on the product main image or product title, the system is navigating to the selected product detail page",
					"When user click / tab on the product main image or product title, the system is not navigating to the selected product detail page", driver);
			
			pdpPage.selectAllSwatches();
			Log.message(i++ +". All swatches are selected in PDP ", driver);
			
			//Step 6 - Navigates to the cart page
			
			pdpPage.clickAddProductToBag();
			ShoppingBagPage cart = pdpPage.navigateToShoppingBag();
		
			Log.softAssertThat(cart.getProductName(0).equalsIgnoreCase(productName), 
					"From  recently viewed section added products should display on the cart page ", 
					"Added products are properly displaying on cart page",
					"Added products are not displaying on cart page", driver);
			
			Log.testCaseResult();
		}
		catch (Exception e) {
			Log.exception(e, driver);
		}//try-catch
		finally {
			Log.endTestCase(driver);
		}//Finally
	}//M2_FBB_E2E_C24580
}//TC_FBB_E2E_C24580
