package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24808 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24808(String browser) throws Exception{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{	
			String userEMailIdAlwaysApprove = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountData.get("password_global");
			String credentialAlwaysApprove = userEMailIdAlwaysApprove +"|"+ password;
			
			String addressAlwaysApprove = "plcc_always_approve_address";
			String firstNameAlwaysApprove = checkoutData.get("plcc_always_approve_address").split("\\|")[7];
			String lastNameAlwaysApprove = checkoutData.get("plcc_always_approve_address").split("\\|")[8];
			
			String prdVariation = TestData.get("prd_variation");
			String prdVariation1 = TestData.get("prd_variation1");
									
			{
				GlobalNavigation.registerNewUserWithUserDetail(driver, 0, 0, firstNameAlwaysApprove, lastNameAlwaysApprove, credentialAlwaysApprove);
				
				GlobalNavigation.addNewAddressToAccount(driver, addressAlwaysApprove, true, credentialAlwaysApprove);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(userEMailIdAlwaysApprove, password);
			Log.message(i++ + ". Navigated to My Account page as : " +credentialAlwaysApprove.split("\\|")[0], driver);
			
			Log.softAssertThat(myAcc.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), myAcc), 
					"User should be successfully created", 
					"User is created", 
					"User is not created", driver);
			
			AddressesPage addrPg = myAcc.navigateToAddressPage();
			Log.message(i++ + ". Navigated to Address page!", driver);
			
			Log.softAssertThat(addrPg.getSavedAddressesCount() > 0, 
					"Address should be successfully created", 
					"Address is created", 
					"Address is not created", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("modalCheckoutPlcc"), checkoutPg)){
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("modalCheckoutPlcc"), checkoutPg), 
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed", 
						"PLCC modal is not getting displayed", driver);
			
				checkoutPg.clickNoThanksInPLCC();
				Log.message(i++ + ". Click on 'No Thanks' in PLCC!", driver);
			
				checkoutPg.clickOnBrandLogo();
				Log.message(i++ + ". Click on Brand logo!", driver);
				
				homePage.headers.signOut();
				Log.message(i++ + ". User signed out!", driver);
				
				homePage = BrowserActions.clearCookies(driver);
				Log.message(i++ + ". Cleared cookies and navigated to Homepage", driver);
				
				myAcc = homePage.headers.navigateToMyAccount(userEMailIdAlwaysApprove, password);
				Log.message(i++ + ". Navigated back to My Account page as : " +userEMailIdAlwaysApprove, driver);
				
				pdpPage = homePage.headers.navigateToPDP(prdVariation1);
				Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
				pdpPage.addToBagCloseOverlay();
				Log.message(i++ + ". Product added to cart", driver);
			
				shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
				checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
				Log.message(i++ + ". Navigated to Checkout page", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("modalCheckoutPlcc"), checkoutPg), 
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed", 
						"PLCC modal is not getting displayed", driver);
			
				checkoutPg.clickGetItTodayInPLCC();
				Log.message(i++ + ". Click on 'Get it Today' in PLCC!", driver);
			
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("plccProfileSummary"), checkoutPg), 
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed", 
						"PLCC modal is not getting displayed", driver);
			
				checkoutPg.typeSocialSecurityNumberInPlcc("1234");
				Log.message(i++ + ". Entered Social Security Number!", driver);
			
				checkoutPg.selectDateMonthYearInPLCC2("01", "01", "1999");
				Log.message(i++ + ". Selected Birth date in PLCC!", driver);
				
				checkoutPg.typeTextInMobileInPLCC("8015841844");
				Log.message(i++ +". Updated phone number", driver);
			
				checkoutPg.checkConsentInPLCC("yes");
				Log.message(i++ + ". Checked Terms and condition checkbox!", driver);
			
				checkoutPg.clickOnAcceptInPLCC();
				Log.message(i++ + ". Accept button is clicked!", driver);
				
				if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("approvedModal"), checkoutPg)) {
												
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"), checkoutPg), 
							"PLCC Congratulations modal should be displayed", 
							"PLCC Congratulations modal is displayed", 
							"PLCC Congratulations modal not displayed", driver);
				
					checkoutPg.dismissCongratulationModal();
					Log.message(i++ + ". Clicked on 'Continue To Checkout' button!", driver);
				
					checkoutPg.continueToPayment(true);
					Log.message(i++ + ". Clicked on 'Continue' button and navigated to Payment!", driver);
					if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("selectedPLCCCard"), checkoutPg)) {
						Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("selectedPLCCCard"), checkoutPg), 
								"PLCC card should be selected in saved list ", 
								"PLCC card is getting selected in saved list ", 
								"PLCC card is not getting selected in saved list ", driver);
						
					} else {							
						Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("drpCardType"), checkoutPg), 
								"PLCC card should be selected in credit card dropdown", 
								"PLCC card is getting selected in credit card dropdown", 
								"PLCC card is not getting selected in credit card dropdown", driver);
						
						Log.softAssertThat(checkoutPg.elementLayer.verifyNumberMaskedWithSpecifiedDigits("fldTxtCardNo",4, checkoutPg),
								"The masked number and last 4 digits of the PLCC card should be displayed in the \"Card Number\" field.",
								"The masked number and last 4 digits of the PLCC card is displayed in the \"Card Number\" field.",
								"The masked number and last 4 digits of the PLCC card is not displayed in the \"Card Number\" field.", driver);
					}
					
					checkoutPg.clickOnPaymentDetailsContinueBtn();
					Log.message(i++ + ". Continued to Review & Place Order", driver);
					
					if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("plccCardNoError"), checkoutPg)) {
						Log.reference("PLCC card error message is displayed, hence can't proceed further", driver);
					
					} else {
						
						Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("btnPlaceOrder"), checkoutPg), 
								"Place order button should be displayed", 
								"Place order button is getting displayed", 
								"Place order button is not getting displayed", driver);
						
						if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
							Log.reference("Further verfication steps are not supported in current environment.");
							Log.testCaseResult();
							return;
						}
						
						OrderConfirmationPage orderPage = null;
						
						checkoutPg.clickOnPlaceOrder();
						Log.message(i++ +". Clicked place order button", driver);
						
						if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("errPlaceOrder"), checkoutPg)) {
							Log.reference("By Using Always Approve card, User can't place order", driver);
						
						} else {
							orderPage = new OrderConfirmationPage(driver).get();
							
							Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage),
									"user should taken to the order confirmation page.",
									"user is taken to the order confirmation page.",
									"user is not taken to the order confirmation page.",driver);
							
							Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("OrderDetailsSection"), orderPage),
									" the order details should be displayed",
									" the order details is displayed",
									" the order details is not displayed",driver);
							
							if(Utils.isMobile()) {
								orderPage.clickOnViewDetailsMobile();
								Log.message("Clicked on the view details link in mobile view", driver);
							}
							
							Log.softAssertThat(orderPage.elementLayer.verifyNumberMaskedWithSpecifiedDigits("lblEnteredCardNo", 4, orderPage),
									"The last 4 digits of the PLCC card should be be displayed.",
									"The last 4 digits of the PLCC card is be displayed.",
									"The last 4 digits of the PLCC card is not displayed.", driver);
						}						
					}
				
				}else {
					Log.reference("Approval modal is not displayed hence can't proceed further", driver);
				}
			} else {
				Log.fail("PLCC Step-1 overlay is not displayed with ALWAYS APPROVE address data, Futher verification is not proceeded");
			}
			
			Log.testCaseResult();
		}// Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally
	}
}
