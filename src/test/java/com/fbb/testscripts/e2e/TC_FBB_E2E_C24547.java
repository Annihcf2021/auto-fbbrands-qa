package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PaypalConfirmationPage;
import com.fbb.pages.PaypalPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.headers.HamburgerMenu;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.SystemProperties;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;
import com.fbb.support.SystemProperties.Platform;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24547 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "tablet" }, priority = 10, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24547(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		Utils.setDeviceTypeExecutionProperties(SystemProperties.getRunPlatform(), browser);
		WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{

			//Step 1 - Navigate to the website from Tablet
			
			String level2Cat = TestData.get("level-2");
			String surchargePrd = TestData.get("prd_surcharge");
			String checkoutAdd = "address_withtax";
			String billingAdd = "taxless_address";
			String[] payPalCredentials = accountData.get("credential_paypal").split("\\|");
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			
			{
				GlobalNavigation.registerNewUser(driver, 0, 0, credential);
				GlobalNavigation.emptyCartForUser(username, password, driver);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Mouse hover on the sign in link & login from the sign in flyout
			
			SignIn signIn = homePage.headers.navigateToSignInPage();

			signIn.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to My account!", driver);
			
			//Step 3 - The category header
			
			if(Utils.isTablet()) {
				HamburgerMenu hMenu = (HamburgerMenu) homePage.headers.openCloseHamburgerMenu("open");
				
				Log.softAssertThat(hMenu.verifyCategoryRightArrowDisplayed(), 
						"Hamburger menu should have arrow for categories", 
						"Hamburger menu is having arrow for categories", 
						"Hamburger menu is not having arrow for categories", driver);
				
				hMenu.openCloseHamburgerMenu("close");
				Log.message("Closed hamburger menu", driver);
			}
			
			PlpPage plpPage = homePage.headers.navigateTo(level2Cat.split("\\|")[0], level2Cat.split("\\|")[1]);
			
			BrowserActions.scrollToBottomOfPage(driver);
			Log.softAssertThat(Integer.parseInt(plpPage.elementLayer.getElementCSSValue("btnBackToTop", "right", plpPage).replaceAll("[^0-9]", "")) >=
							Integer.parseInt(plpPage.elementLayer.getElementCSSValue("divProductTileContainer", "padding-right", plpPage).replaceAll("[^0-9]", "")), 
					"Back to Top button should be displayed at right side of the product grid.", 
					"Back to Top button is displayed at right side of the product grid.", 
					"Back to Top button is not displayed at right side of the product grid.", driver);
			
			plpPage.clickBackToTopButton();
			Log.message(i++ + ". Clicked on the Back To Top Button", driver);
			
			Log.softAssertThat(plpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("btnBackToTop"), plpPage),
					"Back To Top button should not be visible.",
					"Back To Top button is not visible.", 
					"Back To Top button is visible.", driver);
			
			//Step 4 - Navigate to Product listing page
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage), 
					"PLP page should display", 
					"PLP page is displayed", 
					"PLP page is not displayed", driver);
			
			PdpPage pdpPage = plpPage.navigateToPdp(1);
			Log.message(i++ + ". Navigated to PDP page!", driver);
			
			HashSet<String> productId = new HashSet<String>();
			productId.add(pdpPage.getProductID().trim());
					
			Log.softAssertThat(pdpPage.verifyQtyDrp(), 
					"Drop down should display 1-10", 
					"Drop down displayed 1-10", 
					"Drop down not displayed as expected", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblRegularPrice"), pdpPage) ||
					pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblRegularPriceRange"), pdpPage) || 
					pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblSalePrice","lblOriginalPriceInSalePrice"), pdpPage), 
					"Price should get display.", 
					"Price is displayed!", 
					"Price is Not displayed", driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". Product added to cart", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("divItemId"), pdpPage), 
					"Item id should be displayed", 
					"Item id is getting displayed", 
					"Item id is not getting displayed", driver);
			
			homePage.headers.signOut();
			Log.message(i++ +". log out from the application", driver); 
			
			driver = Utils.changeUADeviceTypeTo(Platform.desktop, browser, driver);
			Log.message(i++ + ". Changed device type to Desktop.");
			
			homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page in Desktop window.", driver);
			
			plpPage = homePage.headers.navigateTo(level2Cat.split("\\|")[0], level2Cat.split("\\|")[1]);
			
			double prdTile = plpPage.getSearchResultCount();
			
			if (prdTile <= 60) {
				if(Utils.isDesktop()){
					Log.softAssertThat(plpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtCurrentPageNo"), plpPage), 
							"PLP page should not have pagination if product count is less than 60", 
							"PLP page is not having pagination when product count is less than 60", 
							"PLP page is having pagination when product count is less than 60", driver);
					}else{
						Log.softAssertThat(plpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("divViewMore"), plpPage), 
								"PLP page should not have view more if product count is less than 60",
								"PLP page should not have view more if product count is less than 60",
								"PLP page should not have view more if product count is less than 60", driver);
					}			
			} else {
				if(Utils.isDesktop()){
					Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("paginationSection"), plpPage), 
							"PLP page should have pagination if product count is more than 60", 
							"PLP page is having pagination when product count is more than 60", 
							"PLP page is not having pagination when product count is more than 60", driver);
					
					Log.softAssertThat(plpPage.getLastPageNumber() == Math.ceil(prdTile/60) , 
							"PLP page should numbers should correctly displayed", 
							"PLP page numbers is displaying correctly", 
							"PLP page numbers is not displaying correctly", driver);
					
					int prevPgNo = plpPage.getCurrentPageNumber();
					plpPage.clickNextButtonInPagination();
					
					Log.softAssertThat(plpPage.getCurrentPageNumber() != prevPgNo , 
							"Clicking on Next or Page number should navigate to the correct page", 
							"Clicking on Next or Page number is navigated to the correct page", 
							"Clicking on Next or Page number is not navigated to the correct page", driver);
				}
			}
			
			//Step 5 - Navigate to Product detail page
			pdpPage= homePage.headers.navigateToPDP(surchargePrd);
			
			productId.add(pdpPage.getProductID().trim());
			
			Log.message(i++ + ". Navigated to PDP page!", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkShippingExpand"), pdpPage), 
					"Shipping and returns section should be displayed", 
					"Shipping and returns section is displayed", 
					"Shipping and returns section is not displayed", driver);
			
			pdpPage.selectClickHereInShippingSection();
			Log.message(i++ + ". Clicked for more info!", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtShippingInfo"), pdpPage),
					"The full shipping info should be displayed!",
					"The full shipping info is displayed!",
					"The full shipping info is not displayed!", driver);
	
			pdpPage.clickCloseInShippingSection();
			Log.message(i++ + ". Collapsed more info!", driver);
			
			//Step 6 - Navigate to the cart page
			pdpPage.refreshPage();
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". Product added to cart", driver);
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated To Shopping Bag page", driver);
			
			signIn = (SignIn) cartPage.navigateToCheckout();
			
			signIn.enterEmailID(username);
			
			signIn.typeOnPassword(password);
			
			CheckoutPage checkoutPg = (CheckoutPage) signIn.clickSignInButton();
			
			Log.message(i++ + ". Navigated to Shipping section", driver);
			
			cartPage = checkoutPg.clickEditItemSummary();
			Log.message(i++ + ". Clicked on edit link in Items in your bag section.", driver);
			
			//step 12 - Navigate to the cart page
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("miniCartContent"), cartPage), 
					"System should be navigated To Shopping Bag page", 
					"System is navigated To Shopping Bag page", 
					"System is not navigated To Shopping Bag page", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("divQuantityMessage"), cartPage), 
					"Product merge message should displayed", 
					"Product merge message is displayed", 
					"Product merge message is not displayed", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("miniCartContent"), cartPage), 
					"Cart page should get display", 
					"Cart page is getting display", 
					"Cart page is not getting display", driver);
			
			HashSet<String> productIdList = new HashSet<String>();
			productIdList = cartPage.getProductIdList();
			
			Log.softAssertThat(cartPage.elementLayer.compareTwoHashSet(productId, productIdList), 
					"Added products from tablet and desktop should be displayed in the cart page", 
					"Added products is displayed in the cart page", 
					"Added products is not in the cart page", driver);
			
			//Step 13 - Navigate to Checkout page - step 1
			
			checkoutPg = (CheckoutPage) cartPage.navigateToCheckout();
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("billingEditFirstname"), checkoutPg), 
					"The system should navigate the user to checkout page - Step 1", 
					"The system is navigate the user to checkout page - Step 1", 
					"The system is not navigate the user to checkout page - Step 1", driver);

			checkoutPg.fillingShippingDetailsAsSignedInUser("NO", "NO", "NO", ShippingMethod.Standard, checkoutAdd);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			//Step 14 - Navigate to Checkout page - step 2
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(false);
			Log.message(i++ + ". Use this as a Billing address check box disbaled", driver);
						
			Log.softAssertThat(!checkoutPg.getSaleTax().isEmpty(),
					"Sales tax should be displayed in order summary pane",
					"Sales tax should be displayed in order summary pane",
					"Sales tax should be displayed in order summary pane", driver);
			
			checkoutPg.continueToBilling();
			Log.message(i++ +". Navigated to Billing address section", driver);
			
			checkoutPg.fillingBillingDetailsAsSignedUser("NO", "NO", billingAdd);
			Log.message(i++ +". Filled different Billing address", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyTextContains("txtProductInfo", "Additional Shipping", checkoutPg), 
					"Product info should have 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", 
					"Product info is having 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", 
					"Product info is not having 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
						
			checkoutPg.selectPaypalPayment();
			Log.message(i++ +". Selected paypal radio button");
			
			PaypalPage payPal=checkoutPg.clickOnPaypalButton();
			Log.message(i++ +". Clicked PayPal button button", driver);
			
			PaypalConfirmationPage paypalconfoPage= payPal.enterPayapalCredentials(payPalCredentials[0],payPalCredentials[1]);
			Log.message(i++ +". Entered Paypal credntials", driver);
			
			paypalconfoPage.clickContinueConfirmation();
			Log.message(i++ +". Clicked continue from paypal window", driver);
			
			//Step 15 - Navigate to Checkout page - step 3
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//step 16 - Order confirmation page
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24547
