package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C25966 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C25966(String browser) throws Exception
	{
		Log.testCaseInfo(); 
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{
			//Step 1 - Navigate to the website

			String level2Cat = TestData.get("level_with_productID_color");
			String username= AccountUtils.generateEmail(driver);
			String checkoutAdd = "valid_address1";
			String billingAdd = "valid_address8";
			String cardDetail = "card_FullBeautyPlatinum";
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Click on category header and select the category name.
			if(Utils.isDesktop()) {
				Log.softAssertThat(homePage.headers.verifyRootCategoryHoverFlyout(), 
						"Category flyout should displayed and category should highlighted", 
						"Category flyout is displayed and category should highlighted", 
						"Category flyout is not displayed and category should highlighted", driver);
			}
			
			//Step 3 - Navigate to Product listing page
			PlpPage plpPage = homePage.headers.navigateTo(level2Cat.split("\\|")[0], level2Cat.split("\\|")[1]);
			Log.message(i++ + ". Navigated to PLP", driver);
			
			Log.softAssertThat(plpPage.verifyCategoryHeader(level2Cat.split("\\|")[1]), 
					"User should be navigated to correct category page.", 
					"Navigated to correct category.", 
					"User not navigated to correct category.", driver);
			
			plpPage.clickSortbyArrow();
			Log.message(i++ + ". Clicked on sort by drop-down arrow", driver);
			
			plpPage.sortBy("Lowest Price");
			Log.message(i++ + ". Selected Lowest price sort option", driver);
			
			Object[] obj = plpPage.checkProdImgIsEqualToColorSwatchImage();
			
			boolean primaryImgIsEqual = (boolean) obj[0];
			String prodId = (String) obj[1];
			String colorSelected = (String) obj[2];
			
			Log.softAssertThat(primaryImgIsEqual,
					"The system should display the selected colorized image on the main image area.", 
					"The system is displayed the selected colorized image on the main image area.",
					"The system is not displayed the selected colorized image on the main image area.", driver);
			
			//Step 4 - Navigate to Product detail page
			PdpPage pdpPage = plpPage.navigateToPdp(prodId);
			Log.message(i++ + ". Navigated to PDP", driver);
			
			Log.softAssertThat(pdpPage.getSelectedColor().toLowerCase().trim().contains(colorSelected.toLowerCase().trim()),
					"The system displaying the colorized image on PDP & color swatch is preselected.", 
					"The system is displaying the colorized image on PDP & color swatch is preselected.",
					"The system is not displaying the colorized image on PDP & color swatch is preselected.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lblSelectedColor", "colorLabel", pdpPage),
					"The color name should be displayed next to the color label.", 
					"The color name is displayed next to the color label.",
					"The color name is not displayed next to the color label.", driver);
			
			pdpPage.selectAllSwatches();
			Log.message(i++ + ". Selected all swatches", driver);
			
			Log.softAssertThat(!(pdpPage.getSelectedSize().isEmpty()),
					"The system displaying the selected size", 
					"The system is displaying the size",
					"The system is not displaying the size", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Click add product to bag", driver);
			
			//Step 5 - Navigate to Cart page
			ShoppingBagPage cartPage = pdpPage.navigateToShoppingBag();
			Log.message(i++ + ". Navigate to shopping bag", driver);
			
			//Step 6 - Navigate to checkout sign in page
			SignIn signin = (SignIn) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			CheckoutPage checkoutPg = (CheckoutPage)signin.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);
			
			checkoutPg.continueToShipping(username);
			Log.message(i++ + ". Navigated to Shipping section", driver);
			
			if(Utils.isDesktop()) {
				checkoutPg.mouseHoverShippingContinueButton();
				Log.message(i++ + ". Mouse hovered on shipping continue button", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("errShippingMissingFields"), checkoutPg),
						"Shipping missing fields error should display", 
						"Shipping missing fields error is display",
						"Shipping missing fields error is not display", driver);
			}
			
			//Step 7 - Navigate to Checkout page - step 1
			String address = checkoutProperty.getProperty(checkoutAdd);
			
			String firstName = address.split("\\|")[0];
			String lastName = address.split("\\|")[1];
			String zipCode = address.split("\\|")[4];
			
			checkoutPg.enterFirstName(firstName);
			checkoutPg.enterLastName(lastName);
			checkoutPg.fillZipCodeShipping(zipCode);
			Log.message(i++ + ". Entered Zip code", driver);
			
			Log.softAssertThat(!(checkoutPg.getSelectedState().isEmpty()) && !(checkoutPg.getCity().isEmpty()),
					"City and State should auto populated", 
					"City and State is auto populated",
					"City and State is not auto populated", driver);
			
			checkoutPg.fillingShippingDetailsAsGuest("No", checkoutAdd, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(false);
			Log.message(i++ + ". Use this as a Billing address check box disabled", driver);
			
			checkoutPg.continueToBilling();
			Log.message(i++ + ". Continued to Billing address", driver);
			
			//Step 8 - Navigate to Checkout page - step 2
			checkoutPg.fillingBillingDetailsAsGuest(billingAdd);
			Log.message(i++ + ". Billing Address entered successfully", driver);
			
			checkoutPg.continueToPaymentFromBilling();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			//Step 9 - Navigate to Checkout page - step 3
			checkoutPg.fillingCardDetails1(cardDetail, false, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("btnPlaceOrder"), checkoutPg),
                    "Place order button should get display", 
                    "Place order button is displayed",
                    "Place order button is not displayed", driver);
            
            if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
                Log.reference("Further verfication steps are not supported in current environment.");
                Log.testCaseResult();
                return;
            }
            
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			//Step 10 - Order confirmation page
			checkoutPg.clickOnPlaceOrder();
			Log.message(i++ + ". Clicked on place order button", driver);
			
			if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg)) {
				Log.reference("ALWAYS APPROVE address not returned valid PLCC card", driver);
			} else {
				Log.message(i++ + ". Order Placed successfully", driver);
				
				OrderConfirmationPage receipt = new OrderConfirmationPage(driver).get();
				HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
				
				Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
						"The product added should be present in the receipt", 
						"The product added is present in the receipt", 
						"The product added is not present in the receipt", driver);
				
				String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
				
				Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
						"The order total should display correctly", 
						"The order total is displaying correctly", 
						"The order total is not displaying correctly", driver);
			}
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C25966