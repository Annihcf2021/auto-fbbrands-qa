package com.fbb.testscripts.e2e;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.CollectionUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24124 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24124(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			//step 1 - Navigate to the website
			String userMail = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String userPassword = accountData.get("password_global");
			String checkoutShipAdd = "taxless_address";
			String payment = "card_Visa";
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Click on Sign in link from header & Click on "Create Account" button I'm a New Customer section from Sign in page.
			//Step 3 - Navigate to My Account - Overview page
			MyAccountPage myAcc = homePage.headers.navigateToMyAccountWithRememberChkBox(userMail, userPassword, true, true);
			
			Log.softAssertThat(myAcc.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), myAcc), 
					"User should created and My account page should displayed", 
					"User created and My account page is displayed", 
					"User is not created", driver);
			
			homePage.headers.clickOnBrandFromHeader();
			Log.message(i++ + ". Navigated to Home Page!", driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			//Step 4a - Navigate to Product detail page ( Select a variant product )
			HashSet<String> prdNameList = new LinkedHashSet<String> () ;

			PdpPage pdpPage = homePage.navigateToTrendingNowPDPByIndex(2);
			if (pdpPage == null) {
				Log.reference("Trending now is empty. Navigating to random PDP.");
				PlpPage plpPage = homePage.headers.navigateToRandomCategory();
	            Log.message(i++ + ". Navigated to PLP: " + plpPage.getCategoryName(), driver);
				pdpPage = plpPage.navigateToPdp();
			}
			
			String nameBag = pdpPage.getProductName();
			prdNameList.add(pdpPage.getProductName());
			Log.message(i++ + ". Navigated to PDP #" + prdNameList.size() + " via recommendation in Homepage", driver);
			
			pdpPage.addToBagKeepOverlay();
			Log.message(i++ + ". Added product to cart!", driver);
			
			if(pdpPage.elementLayer.isElementsDisplayed(Arrays.asList("mdlMiniCartOverLay"), pdpPage)) {
				pdpPage.closeAddToBagOverlay();
			} else {
				Log.ticket("SC-5885");
			}
			
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("recentlyViewSectionNextArrow"),pdpPage)
							|| pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("recentlyViewSectionPrevArrow"),pdpPage),
					"Carousal link should not be displayed when less than 4 product is available in the PDP-Recently viewed section.",
					"Carousal link are not displayed when less than 4 product is available in the PDP-Recently viewed section.",
					"Carousal link are displayed when less than 4 product is available in the PDP-Recently viewed section.",driver);
			
			String lastProduct = null;
			int productCount = 1;
			
			while(productCount < 6) {
				pdpPage.clickInStockRecommendationPrdImage();
				pdpPage = new PdpPage(driver).get();
				if(productCount == 5) {
					lastProduct = pdpPage.getProductName();
				} else {
					prdNameList.add(pdpPage.getProductName());
				}
				productCount++;
				Log.message(i++ + ". Navigated to PDP #" + productCount + ": " + pdpPage.getProductName(), driver);
				
				int countPDPRecentylViewed = pdpPage.getNoOfProdDisplayInRecentView();
				Log.softAssertThat((countPDPRecentylViewed == (productCount-1)), 
						"The recently viewed section should only display when the user has previously viewed an item during the current session",
						"All recently viewed products are displayed in PDP",
						"All recently viewed products are not displayed in PDP", driver);
				
				if( (!Utils.isMobile() && countPDPRecentylViewed > 3)) {
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("recentlyViewSectionPrevArrow"), pdpPage)
									|| pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("recentlyViewSectionNextArrow"), pdpPage),
							"Carousal link should be displayed when "+ countPDPRecentylViewed +" product are available in the recently viewed section.",
							"Carousal link are displayed when "+ countPDPRecentylViewed +" product available in the recently viewed section.",
							"Carousal link are not displayed "+ countPDPRecentylViewed +" product are available in the recently viewed section.", driver);
				} else if( (!Utils.isMobile() && countPDPRecentylViewed <= 3)) {
					Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("recentlyViewSectionPrevArrow"), pdpPage)
									|| pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("recentlyViewSectionNextArrow"), pdpPage),
							"Carousal link not should be displayed when "+ countPDPRecentylViewed +" product is available in the recently viewed section.",
							"Carousal link are not displayed when "+ countPDPRecentylViewed +" product is available in the recently viewed section.",
							"Carousal link are displayed when "+ countPDPRecentylViewed +" product available in the recently viewed section.", driver);
				}
			}
			
			Log.softAssertThat(CollectionUtils.listContainsList(pdpPage.getRecentlyViewedsectionAllProdImageName(), new ArrayList<String>(prdNameList)), 
					"All recently viewed products should be displayed in PDP",
					"All recently viewed products are displayed in PDP",
					"All recently viewed products are not displayed in PDP", driver);
			
			prdNameList.add(lastProduct);
			Log.event("Adding last product to the list");
			
			//Step 5 - Navigate to the cart page
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ +". Navigated to shopping cart page", driver);
			
			Log.softAssertThat(shoppingBagPg.verifyProductAddedInCart(nameBag), 
					"Product should added to cart", 
					"Product is added to cart", 
					"Product is not added to cart", driver);
			
			int countCartRecentylViewed = shoppingBagPg.getNoOfProdDisplayInRecentView();
			if (!Utils.isMobile() && countCartRecentylViewed > 4) {
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("recentlyViewSectionPrevArrow"),shoppingBagPg)
								|| shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("recentlyViewSectionNextArrow"),shoppingBagPg),
						"Carousal link should be displayed when "+countCartRecentylViewed+" are available in the recently viewed section.",
						"Carousal link are displayed when "+countCartRecentylViewed+" available in the recently viewed section.",
						"Carousal link are not displayed "+countCartRecentylViewed+" are available in the recently viewed section.",driver);
			} else if(!Utils.isMobile()){
				Log.softAssertThat(!shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("recentlyViewSectionPrevArrow"),shoppingBagPg)
								|| !shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("recentlyViewSectionNextArrow"),shoppingBagPg),
						"Carousal link not should be displayed when "+countCartRecentylViewed+" is available in the recently viewed section.",
						"Carousal link are not displayed when "+countCartRecentylViewed+" is available in the recently viewed section.",
						"Carousal link are displayed when "+countCartRecentylViewed+" available in the recently viewed section.",driver);
			}
			
			Log.softAssertThat(CollectionUtils.listContainsList(shoppingBagPg.getCurrentRecentViewProdName(), new ArrayList<String>(prdNameList)), 
					"All recently viewed products should be displayed in Cart",
					"All recently viewed products are displayed in Cart",
					"All recently viewed products are not displayed in Cart", driver);

			//Step 6 - Navigate to Checkout page - step 1
			
			CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			checkoutPg.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", ShippingMethod.Standard, checkoutShipAdd);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			if(checkoutPg.getNumberOfShippingMethodsAvailable() > 1){
				checkoutPg.selectShippingMethod(ShippingMethod.Express);
				Log.message(i++ + ". Express Delivery radio button enabled", driver);
			}
			
			if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("shippingmethodExceptionOverlay"), checkoutPg))
				checkoutPg.clickOnContinueInShippingMethodExceptionOverlay();
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
			
			//Step 7 Navigate to Checkout page - step 2
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			Log.softAssertThat(checkoutPg.compareBillingAddressWithEnteredShippingAddress(checkoutShipAdd), 
					"The system should keep the billing address same as shipping address", 
					"Billing address same as shipping address", 
					"Billing address not same as shipping address", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continue to payment section", driver);
			
			checkoutPg.applyGiftCardByValue(TestGiftCardValue.$10);
			Log.message(i++ + ". $10 Gift Card is applied", driver);
			
			checkoutPg.applyGiftCardByValue(TestGiftCardValue.$20);
			Log.message(i++ + ". $20 Gift Card is applied", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryGiftCardDiscount"), checkoutPg), 
					"Applied gift card should display on order summary", 
					"Applied gift card is displayed on order summary", 
					"Applied gift card not displayed on order summary", driver);
			
			checkoutPg.fillingCardDetails1(payment, true, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			Log.softAssertThat(checkoutPg.comparePaymentBeforeOrderWithEnteredPaymentMethod(payment), 
					"The system should keep the payment details", 
					"Payment method is saved and displayed", 
					"Payment method is not saved correctly", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			//Step 8 -Navigate to Checkout page - step 3
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);

			//Step 9 - Order confirmation page
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);

			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24124
