package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24274 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24274(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			String level2Cat = TestData.get("level-2");
			String level1Cat = TestData.get("level-1_with_more_pages");
			String backorderedPrdID = TestData.get("prd_back-order");
			String variationPrdID = TestData.get("prd_variation");
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String checkoutAdd = "address_withtax";
			String payment = "cards_5";
			
			{
				GlobalNavigation.registerNewUser(driver, 1, 0, credential);
			}
			
			//step 1 - Navigate to the website
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//step 2 - Mouse hover on category name from any category header
			//Step 3 - Navigate to Product listing page
			PlpPage plpPage = homePage.headers.navigateTo(level1Cat);
			Log.message(i++ + ". Navigated to PLP:: " + level1Cat, driver);
			
			double prdTile = plpPage.getSearchResultCount();
			
			if (prdTile <= 60) {
				if(Utils.isDesktop()) {
					Log.softAssertThat(plpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lblCurrentPage"), plpPage), 
							"PLP page should not have pagination if product count is less than 60", 
							"PLP page is not having pagination when product count is less than 60", 
							"PLP page is having pagination when product count is less than 60", driver);
				}else {
					Log.softAssertThat(plpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("divViewMore"), plpPage), 
							"View more button should not be displayed when product count is less than 60",
							"View more button is not displayed when product count is less than 60",
							"View more button is displayed when product count is less than 60", driver);
				}
			} else {
				if(Utils.isDesktop()){
					Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblCurrentPage"), plpPage), 
							"PLP page should have pagination if product count is more than 60", 
							"PLP page is having pagination when product count is more than 60", 
							"PLP page is not having pagination when product count is more than 60", driver);
					
					double expected = Math.ceil(prdTile/60);
					double actual = plpPage.getLastPageNumber();
					Log.softAssertThat(actual ==  expected, 
							"PLP page numbers should correctly displayed", 
							"PLP page numbers is displaying correctly", 
							"PLP page numbers is not displaying correctly", driver);
					
					int prevPgNo = plpPage.getCurrentPageNumber();
					
					plpPage.clickNextButtonInPagination();
					
					Log.softAssertThat(plpPage.getCurrentPageNumber() != prevPgNo , 
							"Clicking on Next or Page number should navigate to the correct page", 
							"Clicking on Next or Page number is navigated to the correct page", 
							"Clicking on Next or Page number is not navigated to the correct page", driver);
					
					Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("iconPreviousPage"), plpPage), 
							"After clicking Next button 'Prev' button should get display", 
							"After clicking Next button 'Prev' button is getting displayed", 
							"After clicking Next button 'Prev' button is not getting displayed", driver);
					
					int lastPageNumber = plpPage.getLastPageNumber();
				
					if(lastPageNumber >= 5) {
						Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("dotPages"), plpPage), 
								"Confirm the Dot symbols display between pages if more than 5 pages available", 
								"The Dot symbols is displaying between pages", 
								"The Dot symbols is not displaying between pages", driver);
					} else {
						Log.softAssertThat(plpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("dotPages"), plpPage), 
								"Confirm the Dot symbols display between pages if more than 5 pages available", 
								"The Dot symbols is not displaying between pages", 
								"The Dot symbols is displaying between pages", driver);
					}
					
					plpPage.clickOnPageNumberInPagination(lastPageNumber);
					Log.message(i++ +". Clicked on last page number link", driver);
					
					int lastPageProductCount = plpPage.getProductTileCount();
					
					Log.softAssertThat((lastPageProductCount <= 60), 
							"On the last page, the system should display less than or equal to 60 products", 
							"On the last page, the system is displaying less than or equal to 60 products", 
							"On the last page, the system is not displayed less than or equal to 60 products", driver);
					
					plpPage.clickPrevButtonInPagination();
					Log.message(i++ +". Clicked on 'Prev' link from the last page", driver);
					
					Log.softAssertThat(plpPage.getCurrentPageNumber() != lastPageNumber , 
							"Clicking on Prev link, system should navigate to the correct page", 
							"Clicking on Prev link, system is navigated to the correct page", 
							"Clicking on Prev link, system is not navigated to the correct page", driver);
				} else {
					Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("divViewMore"), plpPage),
							"View more button should be displayed if more than 60 products available",
							"View more button should be displayed if more than 60 products available",
							"View more button should be displayed if more than 60 products available", driver);
					
					int prdCountBefore = plpPage.getProductTileCount();
					
					plpPage.clickOnViewMore();
					Log.message(i++ +". Clicked on view more link", driver);
					
					Log.softAssertThat(plpPage.getProductTileCount() > prdCountBefore, 
							"The system should load 60 more products & display on PLP",
							"The system is loaded 60 more products & display on PLP", 
							"The system is not loaded 60 more products & display on PLP",driver);				
				}				
			}
			
			if(Utils.isDesktop()) {
				Log.softAssertThat(homePage.headers.verifyRootCategoryHoverFlyout(), 
						"Category flyout should displayed and category should highlighted", 
						"Category flyout is displayed and category should highlighted", 
						"Category flyout is not displayed and category should highlighted", driver);
			}
			plpPage = homePage.headers.navigateTo(level2Cat);
			Log.message(i++ + ". Navigated to PLP:: " + level2Cat, driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage), 
					"PLP page should display", 
					"PLP page is displayed", 
					"PLP page is not displayed", driver);
			
			//Step 4 Navigate to the Product detail page
			PdpPage pdpPage = homePage.headers.navigateToPDP(variationPrdID);
			Log.message(i++ + ". Navigated To variation PDP page : " + pdpPage.getProductName(), driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementEmpty("txtProdAvailStatus", pdpPage), 
					"Product availability should not displayed", 
					"Product availability is not displayed", 
					"Product availability is displayed", driver);
			
			pdpPage.selectAllSwatches();
			Log.message(i++ + ". Selected product varation.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtProdAvailStatus"), pdpPage)
						&& !(pdpPage.elementLayer.verifyElementEmpty("txtProdAvailStatus", pdpPage)), 
					"Product availability should be displayed", 
					"Product availability is displayed", 
					"Product availability is not displayed", driver);
			
			pdpPage.addToBagKeepOverlay();
			Log.message(i++ + ". Clicked on add to bag", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("divATBOverlayContent"), pdpPage), 
					"ATB overlay should be displayed", 
					"ATB overlay is displayed", 
					"ATB overlay is not displayed", driver);
			
			pdpPage.clickOnContinueShoppingInMCOverlay();
			Log.message(i++ + ". Clicked on continue shopping", driver);
			
			//Step 5 Navigate to PDP for a back-ordered product
			
			pdpPage = homePage.headers.navigateToPDP(backorderedPrdID);
			Log.message(i++ + ". Naviagetd to backordered PDP with variation selected.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtProdAvailStatus"), pdpPage)
						&& !(pdpPage.elementLayer.verifyElementEmpty("txtProdAvailStatus", pdpPage)), 
					"Product availability should be displayed", 
					"Product availability is displayed", 
					"Product availability is not displayed", driver);
			
			String msgAvailabilityPDP = pdpPage.getProdAvailStatus();
			Log.softAssertThat(msgAvailabilityPDP.toLowerCase().contains("Expected to ship by".toLowerCase()), 
					"Back order availability status should correctly displayed", 
					"Back order availability status is correctly displayed", 
					"Back order availability status is not correctly displayed", driver);
			
			pdpPage.addToBagKeepOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			//Step 6 - Navigate to the cart page
			
			ShoppingBagPage shoppingBagPg = pdpPage.navigateToShoppingBag();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			Log.softAssertThat(msgAvailabilityPDP.equalsIgnoreCase(shoppingBagPg.getAvailabilityMessageByID(backorderedPrdID)), 
					"Back order availability status should correctly displayed", 
					"Back order availability status is correctly displayed", 
					"Back order availability status is not correctly displayed", driver);
			
			//step 7 - Navigate to the checkout sign in page
			SignIn signin = (SignIn) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout sign in page", driver);
			
			CheckoutPage checkoutPg = (CheckoutPage) signin.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);
			
			checkoutPg.continueToShipping(credential);
			Log.message(i++ + ". Signed into account for checkout.", driver);
			
			checkoutPg.EditShippingAddress();
			Log.message(i++ + ". Clicked on Edit shipping address", driver);
			
			//step 8 - Navigate to Checkout page - step 1
			
			Log.softAssertThat(checkoutPg.verifyNavigatedToShippingSection(), 
					"The system should navigate the user to checkout page - Step 1", 
					"The system is navigate the user to checkout page - Step 1", 
					"The system is not navigate the user to checkout page - Step 1", driver);
						
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("lblShippingMethodExcep"), checkoutPg), 
					"Shipping method exception label should get displayed", 
					"Shipping method exception label is displayed", 
					"Shipping method exception label is not displayed", driver);
			
			checkoutPg.selectShippingMethodOnly(ShippingMethod.SuperFast);
			Log.message(i++ + ". Select the superfast shippment method", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("divShipMedOverlayCont"), checkoutPg), 
					"Shipping method exception overlay should get displayed", 
					"Shipping method exception overlay is displayed", 
					"Shipping method exception overlay is not displayed", driver);
			
			checkoutPg.clickOnContinueInShippingMethodExceptionOverlay();
			Log.message(i++ + ". Clicked on continue in shipping method exception overlay", driver);
			
			//step 9 - Navigate to Checkout page - step 2
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			checkoutPg.clickBillingAddressEdit();
			Log.message(i++ + ". Clicked on Edit link of Billing address section.", driver);
			
			checkoutPg.fillingBillingDetailsAsSignedUser("NO", "NO", checkoutAdd);
			Log.message(i++ + ". Updated the billing address.", driver);
			
			checkoutPg.clickSelectPaymentMethod();
			Log.message(i++ + ". Continued to Payment Page.", driver);
							
			checkoutPg.fillingCardDetails1(payment, true, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			Log.softAssertThat(checkoutPg.comparePaymentBeforeOrderWithEnteredPaymentMethod(payment), 
					"The system should keep the payment details", 
					"Payment method is saved and displayed", 
					"Payment method is not saved correctly", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			//step 10 - Navigate to Checkout page - step 3
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//step 11 - Order confirmation page
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24274
