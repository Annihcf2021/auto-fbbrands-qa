package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24806 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24806(String browser) throws Exception
	{
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			//Step 1 - Navigate to the website
			
			String userEMailId = AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = userEMailId + "|" + password;
			String checkoutAdd = "address_withtax";
			String billingAdd = "valid_address1";
			String payment = "card_Visa";
			String level2Cat = TestData.get("level-2");
			String choiceCat = TestData.get("choice_of_bonus_level1_cat");
			String choiceBonus = TestData.get("choice_bonus_coupon");
			
			{
				GlobalNavigation.registerNewUserAddress(driver, checkoutAdd, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Hovered on any root category the link
			
			if(Utils.isDesktop()) {
				homePage.headers.mouseHoverOnCategoryByCategoryName(level2Cat.split("\\|")[0]);
				
				Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayed(Arrays.asList("currentLevel2"), homePage.headers), 
						"Category flyout should dispayed", 
						"Category flyout is dispayed", 
						"Category flyout is not dispayed", driver);
			}
			
			homePage.headers.navigateToMyAccount(userEMailId, password);
			Log.message(i++ + ". Navigated to My Account page as : " + userEMailId, driver);
			
			//Step 3 - Navigate to Product listing page
			
			PlpPage plpPage = homePage.headers.navigateTo(level2Cat.split("\\|")[0], level2Cat.split("\\|")[1]);
							
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage), 
					"PLP page should display", 
					"PLP page is displayed", 
					"PLP page is not displayed", driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(choiceCat.split("\\|")[1]);
			Log.message(i++ + ". Navigated to PDP Page!", driver);
			
			pdpPage.selectAllSwatches();
			Log.message(i++ + ". Selected all the swatches in PDP page!", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyTxtElementsNotEmpty(Arrays.asList("selectedColorVariant"), pdpPage),
					"Selected Color should displayed",
					"Selected Color is displayed",
					"Selected Color is not displayed", driver);
			
			String sizeLabel;
			if(pdpPage.isShoeSizeAvailable()) {
				sizeLabel="selectedShoeSize";
			}else {
				sizeLabel="selectedSizeLabel";
			}
			
			Log.softAssertThat(pdpPage.elementLayer.verifyTxtElementsNotEmpty(Arrays.asList(sizeLabel), pdpPage),
					"Selected Size should displayed",
					"Selected Size is displayed",
					"Selected Size is not displayed", driver);
			
			//Step 5 - Navigate to the cart page
			
			pdpPage.selectQuantity(2);
			Log.message(i++ +". Selected qty", driver);
			
			pdpPage.addToBagKeepOverlay();
			Log.message(i++ + ". Clicked Add to cart button!", driver);
			
			ShoppingBagPage cartPage = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Clicked on 'Checkout Now' button!", driver);
			
			cartPage.applyPromoCouponCode(choiceBonus);
			Log.message(i++ + ". Promo Code is applied!", driver);
			
			//Step 6 - Verify the display of Choice of Bonus Product Modal.
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblBonusProductCallout"), cartPage), 
					"Choice of bonus callout banner should display", 
					"Choice of bonus callout banner is displayed", 
					"Choice of bonus callout banner is not displayed", driver);
			
			cartPage.clickOnBonusProductAdd();
			Log.message(i++ +". Clicked add button from cart page", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnBonusHeading"), cartPage), 
					"Bonus heading should display", 
					"Bonus heading is displayed", 
					"Bonus heading is not displayed", driver);
	
			if(Utils.isDesktop()) {
				cartPage.mouseHoverOnDetailsInChoiceOfBonusModal();
				Log.message(i++ + ". Mouse Hovered on Promo Details!", driver);
				
				Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkPromoContent"), cartPage), 
						"Promotion content should display", 
						"Promotion content is displayed", 
						"Promotion content is not displayed", driver);
				
			}else {
				cartPage.clickOnDetailsInChoiceOfBonusModal();
				Log.message(i++ + ". Clicked on Promo Details!", driver);
			}
				
			if(Utils.isMobile()) {
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtBonusPrdNameMob", "imgBonusPrd", cartPage), 
						"The product name should display above the product image in choice of bonus modal", 
						"The product name is displaying above the product image in choice of bonus modal", 
						"The product name is not displaying above the product image in choice of bonus modal", driver);
			
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtBonusPrdNameMob", "txtBonusPrdDesc", cartPage), 
						"The product name should display above the product desription in choice of bonus modal", 
						"The product name is displaying above the product desription in choice of bonus modal", 
						"The product name is not displaying above the product desription in choice of bonus modal", driver);
			} else {
				Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElementsWithoutScroll(driver, "txtBonusPrdName", "imgBonusPrd", cartPage), 
						"The product name should be right side of the product image in choice of bonus modal", 
						"The product name is right side of the product image in choice of bonus modal", 
						"The product name is not right side of the product image in choice of bonus modal", driver);
			
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "txtBonusPrdName", "txtBonusPrdDesc", cartPage), 
						"The product name should display above the product desription in choice of bonus modal", 
						"The product name is displaying above the product desription in choice of bonus modal", 
						"The product name is not displaying above the product desription in choice of bonus modal", driver);
			}			
			
			if(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkBonusViewMore"), cartPage)) {
				cartPage.clickOnViewMoreInChoiceOfBonusOverlay();
				Log.message(i++ + ". Clicked on View more link!", driver);
			
				Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("lnkBonusViewMoreExpanded"), cartPage), 
						"Description should expanded", 
						"Description is expanded", 
						"Description is not expanded", driver);
			} else {
				Log.message("The 'view more' link is not present",driver);
			}
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtBonusPrdPrice"), cartPage) &&
					cartPage.elementLayer.verifyTextContains("txtBonusPrdPrice", "Free", cartPage), 
					"Price should get displayed as 'FREE'", 
					"Price is displayed as 'FREE'", 
					"Price is not displayed as 'FREE'", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "checkBonusRadio", "imgBonusPrd", cartPage), 
						"The product image should be below of the checkbox in choice of bonus modal", 
						"The product image is below of the checkbox in choice of bonus modal", 
						"The product image is not below of the checkbox in choice of bonus modal", driver);
				
			}else {
				Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "imgBonusPrd", "checkBonusRadio", cartPage), 
						"The product image should be right side of the checkbox in choice of bonus modal", 
						"The product image is right side of the checkbox in choice of bonus modal", 
						"The product image is not right side of the checkbox in choice of bonus modal", driver);
			}
			
			int prdSize = cartPage.getNoOfBonusProducts();
			
			//Step 7 - Verify the functionality of selection checkbox
			
			Log.softAssertThat(cartPage.elementLayer.verifyListElementSize("checkbonusProducts", prdSize, cartPage), 
					"All the products should have check box", 
					"All the products is having check box", 
					"All the products is having check box", driver);
			
			//Step 8 - Verify the functionality of Variation Attributes
			
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtBonusPrdPrice", "sectionBonusColorSwatch", cartPage), 
					"The product price should display above the color swatch in choice of bonus modal", 
					"The product price is displaying above the color swatch in choice of bonus modal", 
					"The product price is not displaying above the color swatch in choice of bonus modal", driver);
			
			String sizeAttribute;
			if(cartPage.producthaveShoeWidth(0)) {
				sizeAttribute = "fstBonusShoeSize";
			} else if(cartPage.productHaveSizeFamily(0)) {
				sizeAttribute = "sectionBonusSizeFamilySwatch";
			} else if(cartPage.productHaveBandSize(0)) {
				sizeAttribute = "sectionBonusBandSizeSwatch";
			} else {
				sizeAttribute = "sectionBonusSizeSwatch";
			}
			
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtBonusPrdPrice", sizeAttribute, cartPage), 
					"The product price should display above the size swatch in choice of bonus modal", 
					"The product price is displaying above the size swatch in choice of bonus modal", 
					"The product price is not displaying above the size swatch in choice of bonus modal", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionBonusColorSwatch", sizeAttribute, cartPage), 
						"The color swatch should display above the size swatch in choice of bonus modal", 
						"The color swatch is displaying above the size swatch in choice of bonus modal", 
						"The color swatch is not displaying above the size swatch in choice of bonus modal", driver);
			} else {
				Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, sizeAttribute, "sectionBonusColorSwatch", cartPage), 
						"The size swatch should be right side of the color swatch in choice of bonus modal", 
						"The size swatch is right side of the color swatch in choice of bonus modal", 
						"The size swatch is not right side of the color swatch in choice of bonus modal", driver);
			}
			
			cartPage.selectBonusProductInModal(0);
			Log.message(i++ +". Selected choice of bonus product", driver);
			
			cartPage.selectBonusProductSize(0); 
			Log.message(i++ +". Selected size swatch", driver);
			
			cartPage.selectBonusProductColor(0);
			Log.message(i++ +". Selected color",driver);
			
			//Step 9 - Verify the functionality of 'Add to Cart' button
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnBonusProductAddToBag"), cartPage), 
					"Bonus product add to bag should displayed", 
					"Bonus product add to bag is displayed", 
					"Bonus product add to bag is not displayed", driver);
			
			cartPage.clickOnBonusProductAddToBag();
			Log.message("Clicked on bonus product add to bag!",driver);
			
			Log.softAssertThat(cartPage.getNoOfProductsInCart() > 1, 
					"Bonus product should added to bag", 
					"Bonus product added to bag", 
					"Bonus product not added to bag", driver);
			
			//Step 10 - Click on checkout now button from cart page.
			//Navigate to Checkout page - step 1
			
			CheckoutPage checkoutPg = (CheckoutPage) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(false);
			Log.message(i++ + ". Use this as a Billing address check box disabled", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			//step 11 - Navigate to Checkout page - step 2
			
			checkoutPg.clickAddNewBillingAddress();
			Log.message(i++ + ". Clicked on Add new billing address!", driver);
			
			checkoutPg.fillingBillingDetailsAsGuest(billingAdd);
			Log.message(i++ + ". Billing Address entered successfully", driver);
			
			checkoutPg.checkUncheckSaveThisAddressInBillingDetails(true);
			Log.message(i++ + ". Save This Address in Billing address enbled", driver);
			
			checkoutPg.continueToPaymentFromBilling();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			checkoutPg.fillingCardDetails1(payment, true, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			Log.softAssertThat(checkoutPg.comparePaymentBeforeOrderWithEnteredPaymentMethod(payment), 
					"The system should keep the payment details", 
					"Payment method is saved and displayed", 
					"Payment method is not saved correctly", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			//Step 12 - Navigate to Checkout page - step 3
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//step 13 - Order confirmation page
			
			Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			Log.softAssertThat(receipt.checkEnteredShippingAddressReflectedInOrderReceipt(checkoutAdd), 
					"Same shipping address should display in the receipt which is entered in checkout shipping detail", 
					"Same shipping address is displaying", 
					"Different shipping address is displaying", driver);
			
			Log.softAssertThat(receipt.checkEnteredBillingAddressReflectedInOrderReceipt(billingAdd), 
					"Same billing address should display in the receipt which is entered in checkout billing detail", 
					"Same billing address is displaying", 
					"Different billing address is displaying", driver);
			
			Log.softAssertThat(receipt.comparePaymentAfterOrderWithEnteredPaymentMethod(payment), 
					"Same payment method should display in the receipt which is entered in checkout payment section", 
					"Same payment method is displaying", 
					"Different payment method is displaying", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblOrderReceipt", userEMailId, receipt), 
					"The order receipt should send to the entered mail ID", 
					"The order receipt is send to the entered mail ID", 
					"The order receipt is not send to the entered mail ID", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt), 
					"The user can able to take print out of the order receipt", 
					"The user can able to print the order receipt", 
					"The user cannot able to print the order receipt", driver);
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount();
			Log.message(i++ + ". Navigated to My account!", driver);
			
			AddressesPage addrPage = myAcc.navigateToAddressPage();
			Log.message(i++ + ". Navigated to Address page!", driver);
			
			Log.softAssertThat(addrPage.getSavedAddressesCount() > 1, 
					"Billing address should added in the address page", 
					"Billing address is added in the address page", 
					"Billing address is not added in the address page", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			GlobalNavigation.removeAllPaymentMethods(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24110
