package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.EnlargeViewPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24127 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop","tablet","mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24127(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{

			//Step 1 - Navigate to the website
			
			String levelProductSetCat = TestData.get("ps_product-set_category");
			String ProductSetName = TestData.get("ps_product-set");
			String checkoutAdd = "address_withtax";
			String checkoutAdd1 = "address_4";
			String payment = "card_Amex";
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			
			{
				GlobalNavigation.registerNewUserAddressPayment(driver, checkoutAdd+ "|"+checkoutAdd1, payment, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2: Click on Sign in link from the header
			
			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Navigated to Sign In Page!", driver);
			
			signIn.clickSignInButton();
			Log.message(i++ + ". Click on sign in button!", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyElementDisplayed(Arrays.asList("errUsernameTxtBox", "errPasswordTxtBox"), signIn), 
					"Validation failure message should be shown for empty username/password", 
					"Validation failure message shown for empty username/password", 
					"Validation failure message not shown for empty username/password", driver);
			
			MyAccountPage myAcc = signIn.navigateToMyAccount(username, password);
			
			Log.softAssertThat(myAcc.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), myAcc), 
					"The system should navigate the user to My account page", 
					"The system navigated the user to My account page", 
					"The system not navigated the user to My account page", driver);
			
			//Step 3 - mouse hover on category name from any category header
			
			if(Utils.isDesktop()) {
				homePage.headers.mouseHoverOnCategoryByCategoryName(levelProductSetCat.split("\\|")[0]);
				
				Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayed(Arrays.asList("currentLevel2"), homePage.headers), 
						"Category flyout should dispayed", 
						"Category flyout is dispayed", 
						"Category flyout is not dispayed", driver);
			}
			
			//Step 4 - Navigate to Product listing page
			
			PlpPage plpPage = homePage.headers.navigateTo(levelProductSetCat.split("\\|")[0], levelProductSetCat.split("\\|")[1]);
			Log.message(i++ +". Navigated to PLP", driver);
			BrowserActions.scrollToBottomOfPage(driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage), 
					"PLP page should display", 
					"PLP page is displayed", 
					"PLP page is not displayed", driver);
			
			if(Utils.isDesktop()) {
				Log.softAssertThat(plpPage.verifyProductSetNoQuickshop(),
						"Quick Shop button should not be displayed while hover mouse on the product set tile.", 
						"Quickshop button is not displayed.", 
						"Quickshop button is displayed.", driver);
			}
			
			//Step 5 - Navigate to product set detail page.
			
			PdpPage pdpPage = homePage.headers.navigateToPDPByProdName(ProductSetName);
			Log.message(i++ +". Navigated to Product Set", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("divProductSet"), pdpPage), 
					"The System should display Product set page.", 
					"The System displayed Product set page.",
					"The System not displayed Product set page.", driver);
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "alternateImagesProductSet", "mainImageProductSet", pdpPage), 
						"Alternate images should display only in product Set level", 
						"Alternate images displayed only in product Set level", 
						"Alternate images not displayed only in product Set level", driver);
				
				pdpPage.clickOnAlternateImages(1);
				Log.message(i++ + ". Click on Alternate Image.", driver);
			}
			
			if(!Utils.isTablet()) {	
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("chooseItemsBelowProductSetBHDeskMobile"), pdpPage), 
						"'Choose Items Bellow' should display", 
						"'Choose Items Bellow' is displayed", 
						"'Choose Items Bellow' mot displayed", driver);
				
				pdpPage.clickChooseItemsBelowInProductSet();
				Log.message(i++ + ". Click on Choose Items Bellow button", driver);
		
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("fistItemInProductSet"), pdpPage),
						"The system should redirects the user on the 1st product listed on the product set",
						"The system redirects the user on the 1st product listed on the product set",
						"The system not redirects the user on the 1st product listed on the product set", driver);
				
			}else {
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("chooseItemsBelowProductSetBHTablet"), pdpPage), 
						"'Choose Items Bellow' should display", 
						"'Choose Items Bellow' is displayed", 
						"'Choose Items Bellow' mot displayed", driver);
				
				Log.message(i++ + ". In Tablet, 'choose items below' is not actionable. it is just a static word..", driver);
			}
			
			pdpPage.selectProductSetColorSwatchBasedOnIndex(0);
			Log.message(i++ + ". Selected color.", driver); 
			
			pdpPage.selectProductSetSizeSwatchBasedOnIndex(0);
			Log.message(i++ + ". Selected size.", driver);
			
			pdpPage.clickProductSetAddToBagBasedOnIndex(0);
			Log.message(i++ + ". Click on Add to Bag button.", driver);
			
			pdpPage.clickOnContinueShoppingInMCOverlay();
			Log.message(i++ + ". Click on Continue button from ATB overlay.", driver);
			
			if(!Utils.isMobile()) {
				EnlargeViewPage enlargeViewPage = pdpPage.clickOnEnlargeButtonProductSetBasedOnIndex(1);
				Log.message(i++ + ". Clicked on Enlarge button.", driver);
				
				if(enlargeViewPage != null) {
					
					Log.softAssertThat(enlargeViewPage.elementLayer.verifyPageElements(Arrays.asList("mdlEnlargeWindow"), enlargeViewPage),					 
							"Enlarge Modal window should be displayed when clicking on enlarge button on PDP", 
							"Enlarge Modal window is displayed", 
							"Enlarge Modal window is not displayed", driver);
					
					enlargeViewPage.selectColor(0);
					Log.message(i++ + ". First color swatch selected.", driver);
					
					Log.softAssertThat(enlargeViewPage.verifySwatchAndMainImage(),					 
							"The colorized image should display on the main image area.", 
							"The colorized image displayed on the main image area.", 
							"the colorized image not displayed on the main image area.", driver);
					
					int altImageCount = enlargeViewPage.getAlternateImagesCount();
					
					if (altImageCount>1) {
						enlargeViewPage.clickOnAlternateImage(altImageCount-1);
						Log.message(i++ + ". Clicked on alternate image");
				
						Log.softAssertThat(enlargeViewPage.verifyMainAndAlternateImage(),					 
								"Alternate Image and Main Product image should be same", 
								"Alternate Image and Main Product image is same", 
								"Alternate Image and Main Product image is not same", driver);
					} else {
						Log.message(i++ + ". Alternate image not available");
					}
					
					enlargeViewPage.clickOnAlternateImage(altImageCount-1);
					Log.message(i++ + ". Clicked on alternate image");
			
					Log.softAssertThat(enlargeViewPage.verifyMainAndAlternateImage(),					 
							"Alternate Image and Main Product image should be same", 
							"Alternate Image and Main Product image is same", 
							"Alternate Image and Main Product image is not same", driver);
					
					enlargeViewPage.closeTheEnlargeView();
					Log.message(i++ + ". Clicked on 'Close' on Enlarge View modal");
					
				} else {
					Log.reference("Enlarge Modal didn't open. SC-272");
				}
			}
			
			pdpPage.selectProductSetColorSwatchBasedOnIndex(1);
			Log.message(i++ + ". Selected color.", driver);
			
			pdpPage.selectProductSetSizeSwatchBasedOnIndex(1);
			Log.message(i++ + ". Selected size.", driver);
			
			pdpPage.clickProductSetAddToBagBasedOnIndex(1);
			Log.message(i++ + ". Click on Add to Bag button.", driver);
			
			pdpPage.clickOnContinueShoppingInMCOverlay();
			Log.message(i++ + ". Click on Continue button from ATB overlay.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver,"lnkReviewsTab", "recommendationSectioonPdpProductSet", pdpPage),
					"on product set, PDP recommendation section should display below the reviews & Shipping And Returns section", 
					"on product set, PDP recommendation section is displayed below the reviews & Shipping And Returns section", 
					"on product set, PDP recommendation section is not displayed below the reviews & Shipping And Returns section", driver);
			
			if(Utils.isDesktop()) {
				pdpPage.clickLastBreadCrumbLink();
				plpPage.mouseHoverProductSetByIndex(1);
				                
				Log.softAssertThat(!(plpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnQuickView"), plpPage)), 
						"Quick Shop button should not be displayed while hover mouse on the product set tile.", 
						"Quick Shop button not displayed while hover mouse on the product set tile.", 
						"Quick Shop button is displayed while hover mouse on the product set tile.", driver);
			}
			
			//Step 6 - Navigate to the cart page
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated To Shopping Bag page", driver);
			
			CheckoutPage checkoutPg = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ +". Clicked on Checkout Now button", driver);	
			
			Log.softAssertThat(checkoutPg.getValueFromSavedAddressDropdown().toLowerCase().contains("default"),
					"The Default Address should be displayed in Shipping Address drop box",
					"The Default Address is displayed in Shipping Address drop box",
					"The Default Address is not displayed in Shipping Address drop box",driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("shippingAddressNickNamebold"), checkoutPg) ||
					checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("drpSelectAddressCurrentItem"), checkoutPg),
					"City name (Default) text should be displayed in bold in shipping Address drop box",
					"City name (Default) text is displayed in bold in shipping Address drop box",
					"City name (Default) text is not displayed in bold in shipping Address drop box", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver,"savedAddressCount","customDrpShippingAddress", checkoutPg) ||
					checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver,"savedAddressCount","shippingAddSectionMob", checkoutPg),
					"No of saved address count should be displayed above the shipping address drop box",
					"No of saved address count should be displayed above the shipping address drop box",
					"No of saved address count should be displayed above the shipping address drop box", driver);
			
			checkoutPg.checkUncheckExpressDelivery(true);
			Log.message(i++ +". Selected Express delivery method", driver);
			
			if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("divPoBoxBlockOverlay"), checkoutPg)) {
				checkoutPg.clickOnContinueInShippingMethodExceptionOverlay();
				Log.message(i++ +". Clicked continue in shipping exception overlay.", driver);
			}
			
			checkoutPg.clickDeliveryOptionToolTip();
			Log.message(i++ +". Clicked delivery option tooltip", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("divDeliveryOptionsToolTipDialog"), checkoutPg),
					"The tooltip should be opened properly while clicking the icon on the right side of the Delivery option",
					"The tooltip opened properly while clicking the icon on the right side of the Delivery option",
					"The tooltip not opened properly while clicking the icon on the right side of the Delivery option", driver);
			
			checkoutPg.closeDeliveryOptionToolTip();
			Log.message(i++ +". Closed delivery option tooltip", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyTextContains("txtBillingaddressNickName", "default", checkoutPg) ||
					checkoutPg.elementLayer.verifyTextContains("txtBillingaddressNickNameMob", "default", checkoutPg),
					"The Default Address should be displayed in Billing Address drop box",
					"The Default Address is displayed in Billing Address drop box",
					"The Default Address is not displayed in Billing Address drop box",driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("txtBillingaddressNickName"), checkoutPg) ||
					checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("txtBillingaddressNickNameMob"), checkoutPg),
					"City name (Default) text should be displayed in bold in Billing Address drop box",
					"City name (Default) text is displayed in bold in Billing Address drop box",
					"City name (Default) text is not displayed in bold in Billing Address drop box", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver,"billingAddressSavedCount", "txtBillingaddressNickName",  checkoutPg) ||
					checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver,"billingAddressSavedCount", "txtBillingaddressNickNameMob",  checkoutPg),
					"No of saved address count should be displayed above the Billing address drop box",
					"No of saved address count should be displayed above the Billing address drop box",
					"No of saved address count should be displayed above the Billing address drop box", driver);
			
			checkoutPg.selectValueFromSavedBillingAddressesDropdownByIndex(1);
			Log.message(i++ +". Selected saved address from the dropdown", driver);
			
			if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("divPoBoxBlockOverlay"), checkoutPg)) {
				checkoutPg.clickOnContinueInShippingMethodExceptionOverlay();
				Log.message(i++ +". Clicked continue in shipping exception overlay.", driver);
			}
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("lblSelectedCard"), checkoutPg),
					"the default Credit card should be displaying as selected",
					"the default Credit card is displaying as selected",
					"the default Credit card is not displaying as selected", driver);
				
			checkoutPg.clickOnPlaceOrder();
			Log.message(i++ +". Clicked place order button", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("lblTxtCvvNoError"),checkoutPg),
					"The system should display an error message",
					"The system is displayed an error message",
					"The system is not displayed an error message", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			checkoutPg.enterCVV("3333");
			Log.message(i++ +". Entered Cvv number",driver);
						
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal(); 
			
			OrderConfirmationPage receipt = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//step 11 - Order confirmation page
			
			Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			if(Utils.isMobile()){
				receipt.clickOnViewDetailsMobile();
				Log.message(i++ +". Clicked view details link", driver);
			}
			
			Log.softAssertThat(receipt.checkEnteredShippingAddressReflectedInOrderReceipt(checkoutAdd), 
					"Same shipping address should display in the receipt which is entered in checkout shipping detail", 
					"Same shipping address is displaying", 
					"Different shipping address is displaying", driver);
			
			Log.softAssertThat(receipt.checkEnteredBillingAddressReflectedInOrderReceipt(checkoutAdd1), 
					"Same billing address should display in the receipt which is entered in checkout billing detail", 
					"Same billing address is displaying", 
					"Different billing address is displaying", driver);
			
			Log.softAssertThat(receipt.comparePaymentAfterOrderWithEnteredPaymentMethod(payment), 
					"Same payment method should display in the receipt which is entered in checkout payment section", 
					"Same payment method is displaying", 
					"Different payment method is displaying", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblOrderReceipt", username, receipt), 
					"The order receipt should send to the entered mail ID", 
					"The order receipt is send to the entered mail ID", 
					"The order receipt is not send to the entered mail ID", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt), 
					"The user can able to take print out of the order receipt", 
					"The user can able to print the order receipt", 
					"The user cannot able to print the order receipt", driver);
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);  

			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24127