package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.GiftCardPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24117 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	//private static EnvironmentPropertiesReader checkout = EnvironmentPropertiesReader.getInstance("checkout");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24117(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{
			//Step 1 - Navigate to the website
			String username = AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String checkoutTaxedAddress = "address_withtax";
			String payment = "card_Visa";
			String level2Cat = TestData.get("level_with_productID_color");
			String prdPlp = TestData.get("level_with_productID_color").split("\\|")[2];
			String regularPriceColor = TestData.get("price_color_CartPg");
			String salePriceColor = TestData.get("SalePriceColorRGB");
			String detailsTabHighlight = TestData.get("prd_details_tab");
			String attrTabHighlight = detailsTabHighlight.split("\\|")[0];
			String valueTabHighlight = detailsTabHighlight.split("\\|")[1];
			String validGiftCardNumber = TestData.get("gift_card_$20_1").split("\\|")[0];
			String validGiftCardPin = TestData.get("gift_card_$20_1").split("\\|")[1];
			String invalidGiftCardNumber = "6004492145109992601";
			String colorGiftCartFieldError = TestData.get("giftCardPinError_color");
			
			{
				HashMap<String, String> accountDetails = new HashMap<>();
				accountDetails.put(AddressesPage.class.getName(), checkoutTaxedAddress);
				accountDetails.put(PaymentMethodsPage.class.getName(), payment);
				GlobalNavigation.setupTestUserAccount(driver, accountDetails, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			//step 2 - From the header click on Sign in link & navigate to my account sign in page
			
			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Navigated to signin.", driver);
			
			signIn.clickSignInButton();
			Log.message(i++ + ". Click on sign in button!", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyElementDisplayed(Arrays.asList("errUsernameTxtBox", "errPasswordTxtBox"), signIn), 
					"System should not login for invalid username/password", 
					"System is not login for invalid username/password", 
					"System is login for invalid username/password", driver);
			
			signIn.typeOnEmail("abcd@abcd.com");
			Log.message(i++ + ". Give invalid username!", driver);
			
			signIn.typeOnPassword("abcd@123");
			Log.message(i++ + ". Give invalid password!", driver);
			
			signIn.clickSignInButton();
			Log.message(i++ + ". Click on sign in button!", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyElementDisplayed(Arrays.asList("errLoginErrorMsg"), signIn), 
					"System should not login for invalid username/password", 
					"System is not login for invalid username/password", 
					"System is login for invalid username/password", driver);
			
			signIn.typeOnEmail(username);
			Log.message(i++ + ". Give valid username!", driver);
			
			signIn.typeOnPassword(password);
			Log.message(i++ + ". Give valid password!", driver);
			
			signIn.clickShowPassword();
			Log.message(i++ + ". Clicked on 'Show' password!", driver);
			
			Log.softAssertThat(signIn.verifyPasswordisMaskedorUnmasked("unmask"),					 
					"The User can use the Show Password link to reveal the entered value", 
					"The password is unmasked", 
					"The password is not unmasked", driver);
	
			Log.softAssertThat((signIn.elementLayer.verifyTextContains("showpassword","Hide", signIn)),
					"The link text 'Show' should be replaced with the link text 'Hide'", 
					"The link text 'Show' is replaced with the link text 'Hide'", 
					"The link text 'Show' is not replaced with the link text 'Hide'", driver);
	
			signIn.clickShowPassword(); 
			Log.message(i++ +". Clicked Hide Password", driver);
			
			Log.softAssertThat(signIn.verifyPasswordisMaskedorUnmasked("mask"),					 
					"On clicking the Hide link, System should mask the password and hide the User entered data", 
					"The password is masked", 
					"The password is not masked", driver);
	
			Log.softAssertThat((signIn.elementLayer.verifyTextContains("showpassword", "Show", signIn)),
					"The link text 'Hide' should be replaced with the link text 'Show'", 
					"The link text 'Hide' is replaced with the link text 'Show'", 
					"The link text 'Hide' is not replaced with the link text 'Show'", driver);
			
			signIn = homePage.headers.navigateToSignInPage();
			MyAccountPage myAcc = signIn.navigateToMyAccount(username, password);
			Log.message(i++ +". Navigated To My Account", driver);
			
			Log.softAssertThat(myAcc.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), myAcc), 
					"System should Navigated To My Account", 
					"System is Navigated To My Account", 
					"System is not Navigated To My Account", driver);
			
			//Step 3 - Click on category header and select the category name.
			if(Utils.isDesktop()) {
				Log.softAssertThat(homePage.headers.verifyRootCategoryHoverFlyout(), 
						"Category flyout should displayed and category should highlighted", 
						"Category flyout is displayed and category should highlighted", 
						"Category flyout is not displayed and category should highlighted", driver);
			}
			
			//Step 4 - Navigate to Product listing page
			PlpPage plpPage = homePage.headers.navigateTo(level2Cat.split("\\|")[0], level2Cat.split("\\|")[1]);
			Log.message(i++ + ". Navigated to PLP", driver);
			
			String[] appliedFilter = plpPage.refinements.selectSubRefinementInFilterBy("price", 2);
			double priceFilter = StringUtils.getPriceFromString(appliedFilter[1]);
			Log.message(i++ + ". Applied price filter is " + priceFilter, driver);
			
			plpPage.refinements.selectSortBy("highest priced");
			Log.message(i++ + ". Applied highest priced sort option", driver);
			
			double prdPrice = StringUtils.getRangePriceCeiling(plpPage.getSalePriceProduct(1));
			Log.message(i++ + ". First product price(ceiling) " + prdPrice, driver);
			
			Log.softAssertThat(prdPrice <= priceFilter, 
					"Product price in PLP should be limited to " + priceFilter, 
					"Product price in PLP is limited to " + priceFilter, 
					"Product price in PLP is not limited to " + priceFilter, driver);
			
			plpPage = homePage.headers.navigateTo(level2Cat.split("\\|")[0], level2Cat.split("\\|")[1]);
			Log.message(i++ + ". Navigated to PLP", driver);
			
			Log.softAssertThat(plpPage.verifyCategoryHeader(level2Cat.split("\\|")[1]), 
					"User should be navigated to correct category page.", 
					"Navigated to correct category.", 
					"User not navigated to correct category.", driver);
			
			int prdTile = plpPage.getSearchResultCount();
			int paginationSize = plpPage.getPaginationSize();
			
			if (prdTile <= paginationSize) {
				if(Utils.isDesktop()){
					Log.softAssertThat(plpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtCurrentPageNo"), plpPage), 
							"PLP page should not have pagination if product count is less than 60", 
							"PLP page is not having pagination when product count is less than 60", 
							"PLP page is having pagination when product count is less than 60", driver);
				} else {
					Log.softAssertThat(plpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("divViewMore"), plpPage), 
							"PLP page should not have pagination if product count is less than 60", 
							"PLP page is not having pagination when product count is less than 60", 
							"PLP page is having pagination when product count is less than 60", driver);
				}
			} else {
				if(Utils.isDesktop()) {
					Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("paginationSection"), plpPage), 
							"PLP page should have pagination if product count is more than 60", 
							"PLP page is having pagination when product count is more than 60", 
							"PLP page is not having pagination when product count is more than 60", driver);
					
					int quotient = prdTile/paginationSize;
					if((prdTile % paginationSize) > 0) {
						quotient = quotient +1;
					}
					
					Log.softAssertThat(plpPage.getLastPageNumber() == quotient , 
							"PLP page should numbers should correctly displayed", 
							"PLP page numbers is displaying correctly", 
							"PLP page numbers is not displaying correctly", driver);
					
					plpPage.clickOnPageNumberInPagination(2);
					Log.message(i++ + ". Navigated to next PLP page", driver);
					
				} else {
					Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("divViewMore"), plpPage), 
							"PLP page should have pagination if product count is more than 60", 
							"PLP page is having pagination when product count is more than 60", 
							"PLP page is not having pagination when product count is more than 60", driver);
					
					plpPage.clickOnViewMore();
					Log.message(i++ + ". Clicked on View more button", driver);
				}
			}
						
			String  prdPlpColor = plpPage.selectProductColorInPLP(prdPlp);
			Log.message(i++ + ". Selected product color: " + prdPlpColor, driver);
			
			//Step 5 - Navigate to Product detail page

			PdpPage pdpPage = plpPage.navigateToPdp(prdPlp);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyTextContains("lnkDetailsTab","Details", pdpPage),
					"The product description details tab should displays on page load", 
					"The product description details tab is displaying on page load", 
					"The product description details tab is not displaying on page load", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("lnkDetailsTab", attrTabHighlight, valueTabHighlight, pdpPage),
					"The product description details tab should underlined/highlighted", 
					"The product description details tab is underlined/highlighted", 
					"The product description details tab is not underlined/highlighted", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblCurrentSectionContent"), pdpPage), 
					"The product description should be displayed", 
					"The product description is displayed", 
					"The product description is not displayed", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkDetailsTab", "lblCurrentSectionContent", pdpPage), 
					"Product description should display under the details section", 
					"Product description is displaying under the details section", 
					"Product description is not displaying under the details section", driver);
			//Step 6 - Navigate to the cart page
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Added product to bag", driver);
			
			GiftCardPage giftLandingPage = homePage.footers.navigateToGiftCardLandingPage();
			Log.message(i++ + ". Navigated to gift card landing page", driver);
			
			giftLandingPage.clickOnCheckYourBalance();
			Log.message(i++ + ". Clicked on \"check your balance\" button", driver);
			
			Log.softAssertThat(giftLandingPage.elementLayer.verifyElementDisplayed(Arrays.asList("divChkBalanceModelPopup"), giftLandingPage), 
					"The system should display a gift card balance window", 
					"The system displays a gift card balance window", 
					"The system is not displayed a gift card balance window", driver);
			
			giftLandingPage.clickApplyInCheckBalanceModal();
			Log.message(i++ + ". Do not provided any information & clicked on apply", driver);
			
			Log.softAssertThat(giftLandingPage.elementLayer.verifyElementColor("txtInvalidCardNumberErr", colorGiftCartFieldError, giftLandingPage)
					&& giftLandingPage.elementLayer.verifyElementColor("txtInvalidPinNumberErr", colorGiftCartFieldError, giftLandingPage), 
					"The place-holder text should become red for both card number & pin field.", 
					"The place-holder text become red for both card number & pin field.", 
					"The place-holder text not become red for both card number & pin field.", driver);
			
			giftLandingPage.checkGiftCardBalance(invalidGiftCardNumber, validGiftCardPin);
			Log.message(i++ + ". Provided 16 digits invalid card number & good pin number & clicked on apply", driver);
			
			Log.softAssertThat(giftLandingPage.elementLayer.verifyElementTextContains("invalidcardMsg", "Please enter valid Gift Card Number", giftLandingPage), 
					"The system should display \"Please enter valid Gift Card Number\" message", 
					"The system is displayed \"Please enter valid Gift Card Number\" message", 
					"The system is not displayed \"Please enter valid Gift Card Number\" message", driver);
			
			giftLandingPage.checkGiftCardBalance(validGiftCardNumber, validGiftCardPin);
			Log.message(i++ + ". Provided a valid gift card number & pin number & clicked on apply", driver);
			
			Log.softAssertThat(giftLandingPage.elementLayer.verifyElementTextContains("totalBalance", "Total Balance", giftLandingPage), 
					"The system should display the balance of the gift card.", 
					"The system is displayed the balance of the gift card.", 
					"The system is not displayed the balance of the gift card.", driver);
			
			giftLandingPage.clickClose();
			Log.message(i++ + ". Closed the check your balance modal", driver);
			
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on mini cart icon in the header.", driver);
			
			if (shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("lblPriceStrikeDown"), shoppingBagPg)) {
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("lblPriceStrikeDown", "lblSalesPrice"), shoppingBagPg), 
						"Striked down price and sales price should displaying", 
						"Striked down price and sales price is displaying", 
						"Striked down price and sales price is not displaying", driver);
				
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyCssPropertyForElement("lblPriceStrikeDown", "text-decoration", "line-through", shoppingBagPg), 
						"Marked down price should be striked", 
						"Marked down price is striked", 
						"Marked down price is not striked", driver);
				
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementColor("lblSalesPrice", salePriceColor, shoppingBagPg), 
						"Sales price should be displayed in red", 
						"Sales price is displayed in red", 
						"Sales price is not displayed in red", driver);
			} else {
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementColor("lblSalesPrice", regularPriceColor, shoppingBagPg), 
						"Sales price should be displayed in black", 
						"Sales price is displayed in black", 
						"Sales price is not displayed in black", driver);
			}
			
			double orderPrice = shoppingBagPg.getOrderSubTotal();
			
			shoppingBagPg.updateQuantityByPrdIndex(0, "2");
			Log.message(i++ + ". Product qty updated in cart.", driver);
			
			double orderUpdatedPrice = shoppingBagPg.getOrderSubTotal();
			
			Log.softAssertThat(orderUpdatedPrice > orderPrice, 
					"Subtotal in order sumary should be increased when the user updates the QTY", 
					"Subtotal in order sumary increased when the user updates the QTY", 
					"Subtotal in order sumary not increased when the user updates the QTY", driver);
			
			//Step 7 - The system should navigate the user to Step 3
			
			CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			//step 8 - scroll up & navigate to Checkout page - step 2
			
			checkoutPg.applyGiftCardByValue(TestGiftCardValue.$1000);
			Log.message(i++ + ". Gift Card is applied", driver);
			
			if(!(checkoutPg.getRemainingOrderAmount() == 00.00)) {
				Log.fail("Order Summary Total is not 0, hence cannot continue!",driver);
			}
			
			Log.softAssertThat((checkoutPg.getRemainingOrderAmount()==00.00), 
					"Order summary total should get 0",
					"Order summary total is 0",
					"Order summary total is not 0", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			//HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			//Step 9 - Navigate to Checkout page - step 3
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);

			//Step 10 - Order confirmation page
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);

			Log.testCaseResult();

		 // Ending try block
			}catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally
	}

}//M1_FBB_E2E_C24117
