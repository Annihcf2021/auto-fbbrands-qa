package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.Enumerations.Toggle;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.SystemProperties;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

import com.fbb.support.SystemProperties.Platform;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24537 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	

	@SuppressWarnings("unchecked")
	@Test(groups = { "checkout", "desktop" }, priority = 10, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24537(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		Utils.setDeviceTypeExecutionProperties(SystemProperties.getRunPlatform(), browser);
		WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			//Step 1 - Launch website through Desktop
			
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String cqPrd = TestData.get("qc_valid").split("\\|")[0];
			HashMap<String, String> productCatalogVariatons = new HashMap<String, String>();
			String cqPrd2 = TestData.get("qc_valid_2").split("\\|")[0];
			HashMap<String, String> productCatalogVariatons2 = new HashMap<String, String>();
			String cqValidPrds[] = {TestData.get("qc_valid").split("\\|")[0], TestData.get("qc_valid_1").split("\\|")[0]};
			String cqInvalidPrds[] = {TestData.get("qc_invalid"), TestData.get("qc_invalid")};
			
			{
				GlobalNavigation.registerNewUser(driver, 3, 0, credential);
				GlobalNavigation.deleteDefaultAddress(driver);
				GlobalNavigation.emptyCartForUser(username, password, driver);
				productCatalogVariatons = Utils.getCatalogVariationFromTestData(TestData.get("qc_valid"));
				productCatalogVariatons2 = Utils.getCatalogVariationFromTestData(TestData.get("qc_valid_2"));
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Browser launched with " + Utils.getWebSite(), driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage), 
					"The system should display the Homepage", driver);
			
			//Step 2 - Click on Sign in link from the header
			
			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Clicked on SignIn link.", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divLoginBox"), signIn), 
					"Verify link should be redirected to Sign in page", driver);
			
			MyAccountPage myAcc = signIn.navigateToMyAccount(username, password);
			Log.message(i++ + ". Logged in with user account having multiple addresses and none of them are default", driver);
			
			Log.softAssertThat(myAcc.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAcc), 
					"The page should be redirected to My account page", driver);
			
			//Step 3 - Navigate to catalog quick order page.
			
			QuickOrderPage qcPage = homePage.headers.navigateToQuickOrder();
			Log.message(i++ + ". Navigated to Quick Order Catalog Page", driver);
			
			qcPage.clickOnSearchButton();
			Log.message(i++ + ". Clicked on Search Button.", driver);
			
			Log.softAssertThat(qcPage.elementLayer.verifyPageElements(Arrays.asList("genericErrorRequired"), qcPage), 
					"Validation failure message should display when searching with empty CQO field.", driver);
			
			qcPage.removeAllProducts();
			Log.message(i++ + ". Cleared searched CQO items.", driver);
			
			qcPage.searchItemInQuickOrder(cqPrd);
			Log.message(i++ + ". Searched with a single product.", driver);
			
			qcPage.selectVariation(0, productCatalogVariatons);
			Log.message(i++ + ". Selected color and size.", driver);
			
			Log.softAssertThat((qcPage.getSelectedColorCode().contains(qcPage.getPrimaryImgCode())), 
					"Primary image color should be changed to selected color swatch.", driver);
			
			qcPage.selectAlternateImageByIndex(1);
			Log.message(i++ + ". Selected alt-image.", driver);
			
			Log.softAssertThat(qcPage.getPrimaryImgCode().equals(qcPage.getselectedAltImgCode()), 
					"Selected alt image should be displayed as primary image.", driver);
			
			
			Log.softAssertThat(qcPage.verifyZoomLensAndZoomWindowAreDisplayed(), 
						"Verify zoom image is working properly.", driver);			
			
			Log.softAssertThat(qcPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("lblReview"), qcPage), 
					"The product review should not display for CQO product", driver);
			
			qcPage.clickAddProductToBag(0);
			Log.message(i++ + ". Clicked on add to bag button.", driver);
			
			Log.softAssertThat(qcPage.elementLayer.verifyPageElements(Arrays.asList("mdlMiniCartOverLay"), qcPage), 
					"Add to bag overlay page should be opened with the added item.", driver);
			
			qcPage.closeAddToBagOverlay();
			Log.message(i++ + ". Closed add to bag overlay", driver);
			
			qcPage.removeAllProducts();
			Log.message(i++ + ". Removed all cqo products", driver);
			
			Log.softAssertThat(qcPage.verifyValidCatalogSearchResults(cqValidPrds, cqInvalidPrds), 
					"For valid CQO number, CQO product should be displayed", driver);
			
			qcPage.searchItemInQuickOrder(cqInvalidPrds[0]);
			Log.softAssertThat(qcPage.elementLayer.verifyPageElements(Arrays.asList("genericError"), qcPage), 
					"For invalid CQO product - the system should display the error message.", driver);
			
			qcPage.selectVariationAllCatalogItem();
			Log.message(i++ + ". Variation selected for all the product.", driver);
			
			qcPage.addAllPrdToBag();
			Log.message(i++ + ". Clicked on add all to bag button.", driver);
			
			//Step 4 - Navigate to cart page
			
			List<String> names = qcPage.getProductNames();
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart Page.", driver);
			
			Log.softAssertThat(cartPage.getCartItemNameList().containsAll(names), 
					"Make sure added products are properly displaying in the cart page.", driver);
			
			signIn = homePage.headers.signOut();
			Log.message(i++ +". Logged out from site.", driver);
			
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart page.", driver);
			
			Log.softAssertThat(!homePage.headers.verifyUserIsLoggedIn(), 
					"Confirm the user is logged off from the system", driver);
			
			//Step 5 - Launch the website through Mobile
			driver = Utils.changeUADeviceTypeTo(Platform.mobile, browser, driver);
			homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page in mobile window.", driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage), 
					"The system should display the Homepage", driver);
			
			//Step 6 - Click on catalog quick order link nearer to the search icon.
			
			qcPage = homePage.footers.navigateToQuickOrder();
			Log.message(i++ + ". Navigated to Quick Order Page.", driver);
			
			qcPage.searchItemInQuickOrder(cqPrd2);
			Log.message(i++ + ". Searched with a single product.", driver);
			
			names.addAll(qcPage.getProductNames());
			Log.message(i++ + ". Added CQO product to cart.", driver);
			
			Log.softAssertThat(qcPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgPrimaryImage_Mobile", "icoSlickDots", qcPage), 
					"Alternate image dots should be displayed below the product main image", driver);
									
			Log.softAssertThat(qcPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtPrice", "btnShopNow", qcPage), 
					"'Product Details' drop box should be displayed below the price range.", driver);
			
			qcPage.toggleProductDetails(0, Toggle.Open);
			Log.message(i++ + ". Clicked on expand product detail", driver);
			
			Log.softAssertThat(qcPage.elementLayer.verifyPageElements(Arrays.asList("shopNowWindow_ProductSet_Open"), qcPage),
					"System should Expand the variation section after click product details", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(qcPage.elementLayer.verifyPageElements(Arrays.asList("btnShopNowClose"), qcPage) &&
						qcPage.elementLayer.verifyCssPropertyForPsuedoElement("btnShopNowClose", "after", 
								"background", "select_arrow", qcPage), 
						"'Hide Details' text should be displayed and upward arrow mark should be displayed", driver);
			}
						
			qcPage.selectVariation(0, productCatalogVariatons2);
			Log.message(i++ + ". Variation selected for all the product.", driver);
			
			Log.softAssertThat(qcPage.getSelectedColorCode().contains(qcPage.getPrimaryImgCode()), 
					"Primary image color should be changed to selected color swatch.", driver);
						
			Log.softAssertThat(qcPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("catalogPriceExpired"), qcPage), 
					"The system should retrieve the product along with its quick order pricing.", driver);
			
			qcPage.clickAddProductToBag(0);
			Log.message(i++ + ". Clicked on add to bag button.", driver);
			
			Log.softAssertThat(qcPage.elementLayer.verifyPageElements(Arrays.asList("mdlMiniCartOverLay"), qcPage), 
					"Add to bag overlay page should be opened with the added item.", driver);
			
			//Step 7 - Navigate to the cart page
			
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart Page.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyPageElements(Arrays.asList("quickOrderCatalogBagdeMobile"), cartPage) &&
					cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, 
							"quickOrderCatalogBagdeMobile", "productNameSection", cartPage), 
					"\"CATALOG QUICK ORDER\" text and icon should be displayed above the product attributes.", driver);
			
			cartPage.clickOnProductName();
			Log.message(i++ + ". Clicked on product name in list.", driver);
			
			qcPage = new QuickOrderPage(driver).get();
			Log.softAssertThat(qcPage.elementLayer.verifyPageElements(Arrays.asList("divCatalogQuickOrder"), qcPage), 
					"When clicking on the product name should be redirected to CQO page.", driver);
			
			//Step 8 - Navigate to the checkout sign in page
			
			signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Navigated to Checkout Sign Page.", driver);
			
			myAcc = signIn.navigateToMyAccount(username, password);
			Log.message(i++ + ". User name and password entered.", driver);
			
			Log.softAssertThat(myAcc.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAcc), 
					"The system navigates the user to my account page.", driver);
			
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyPageListElements(Arrays.asList("divCartRow"), cartPage), 
					"The system navigates the user to cart page.", driver);
			
			Log.event("Added catalog products from desktop & mobile" + names);
			Log.softAssertThat(cartPage.getCartItemNameList().containsAll(names), 
					"The products added through Desktop and Mobile should be displayed in the cart page.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyTextContains("divQuantityMessage", "YOUR SHOPPING BAG HAS BEEN MERGED", cartPage), 
					"The system should display a merge message on the cart page", driver);
			
			//Step 9 - Navigate to Checkout page step -1
			
			CheckoutPage checkout = cartPage.clickOnCheckoutNowBtn(); 
			Log.message(i++ + ". Navigated to Checkout Shipping Page.", driver);
			
			driver = Utils.openNewTab(driver, Utils.getWebSite() + TestData.redirectData.get("addressBook"));
			AddressesPage addressBook = new AddressesPage(driver).get();
			int noOfAddress = addressBook.getSavedAddressesCount();
			driver = Utils.switchToNewWindow(driver);
			
			checkout.fillingShippingDetailsAsSignedInUser("no", "yes", "yes", ShippingMethod.Standard, "valid_address_long");
			Log.message(i++ + ". Shipping details filled successfully.", driver);

			checkout.continueToBilling();
			Log.message(i++ + ". Continued to billing.", driver);
			
			driver = Utils.openNewTab(driver, Utils.getWebSite() + TestData.redirectData.get("addressBook"));
			addressBook = new AddressesPage(driver).get();
			int noOfAddress1 = addressBook.getSavedAddressesCount();
			driver = Utils.switchToFirstWindow(driver);
			
			Log.softAssertThat(noOfAddress1 == (noOfAddress + 1), 
					"This newly entered address should be saved into the address book", driver);
			
			//Step 10 - Navigate to Checkout page - step 2
			Log.softAssertThat(checkout.elementLayer.getElementHeight("lblBillingAddress", checkout) > 25, 
					"Long addresses should be displayed in 2 lines.", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			checkout.fillingCardDetails1("card_Visa", false);
			Log.message(i++ + ". Selected a saved credit card", driver);
			
			checkout.continueToReivewOrder();
			Log.message(i++ + ". Credit card details submitted.", driver);
			
			//Step 11 - Navigate to Checkout page - step 3
			
			OrderConfirmationPage orderConfirm = checkout.clickOnPlaceOrderButton();
			Log.message(i++ + ". Navigated to order confirmation page.", driver);
			
			Log.softAssertThat(orderConfirm.elementLayer.verifyPageElements(Arrays.asList("lblOrderNumber", 
					"lblOrderDate", "lblOrderTotal", "orderSummary", "orderShippingAddress"), orderConfirm), 
					"The following information should match, with the steps that the user selected previously " + 
					"Total, " + 
					"Delivery Method, " + 
					"Payment Method, " + 
					"Billing Address, " + 
					"Shipping Address, " + 
					"Product Information, " + 
					"Order Summary", driver);
			
			Log.softAssertThat(orderConfirm.elementLayer.verifyPageElements(Arrays.asList("quickOrderCatalogBagdeMobile"), orderConfirm) &&
					orderConfirm.elementLayer.verifyVerticalAllignmentOfElements(driver, "quickOrderCatalogBagdeMobile", "productNameSection", orderConfirm), 
					"\"CATALOG QUICK ORDER\" text and icon should be displayed above the product attributes.", driver);
			
			orderConfirm.clickOnProductNameByIndex(0);
			Log.message(i++ + ". Clicked on product name.", driver);
			
			qcPage = new QuickOrderPage(driver).get();
			Log.softAssertThat(qcPage.elementLayer.verifyPageElements(Arrays.asList("divCatalogQuickOrder"), qcPage), 
					"When clicking on the product name should be redirected to CQO page.", driver);

			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.removeAllItemsFromQuickOrder(driver);
			Log.endTestCase(driver);
		}// Ending finally
	}

}//M1_FBB_E2E_C24537
