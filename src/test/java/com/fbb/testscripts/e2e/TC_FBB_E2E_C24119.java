package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PaypalConfirmationPage;
import com.fbb.pages.PaypalPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24119 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "desktop", "mobile" }, priority = 1, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24119(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{
			//Step 1 - Navigate to the website
			
			String productID = TestData.get("prd_monogram");
			String userMail = AccountUtils.generateEmail(driver);
			String checkoutShipAdd = "address_withtax";
			String checkoutBillAdd = "valid_address8";
			String paypalemail = accountData.get("credential_paypal").split("\\|")[0];
			String paypalpassword = accountData.get("credential_paypal").split("\\|")[1];
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			PdpPage pdpPage = homePage.headers.navigateToPDP(productID);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			PlpPage plpPage = pdpPage.clickLastBreadCrumbLink();
			
			//Step 4 - Navigate to Quick Shop window
			//Please note: The product should be a Monogramming product
			
			QuickShop quickShop = plpPage.clickOnQuickShop(productID);
			Log.message(i++ + ". Quick shop overlay is opened", driver);
			
			if(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("divMonogrammingOptions"), quickShop)){
				quickShop.selectColor();
				Log.message(i++ + ". Color selected", driver);
				
				quickShop.selectSize();
				Log.message(i++ + ". Size selected", driver);
				
				quickShop.clickOnMonogrammingCheckbox("enable");
				Log.message(i++ + ". Monogram option enabled", driver);
				
				quickShop.selectMonogrammingColorValue(0);
				Log.message(i++ + ". Monogram Color selected", driver);
				
				quickShop.selectMonogrammingFontValue(0);
				Log.message(i++ + ". Monogram Font selected", driver);
				
				quickShop.selectMonogrammingLocationValue(0);
				Log.message(i++ + ". Monogram Position/Location selected", driver);
				
				quickShop.typeInMonogrammingTextField("monogram");
				Log.message(i++ + ". Entered in Monogram Text Field", driver);
				
				quickShop.addToBag();
				Log.message(i++ + ". Product Added to cart", driver);
				
				//Step 5 - Navigate to the cart page
				
				ShoppingBagPage shoppingBagPg = quickShop.clickOnCheckoutInMCOverlay();
				Log.message(i++ + ". Navigated to Shopping bag page", driver);
				
				double price = shoppingBagPg.getUpdatedSubtotalPrice();
				double orderPrice = shoppingBagPg.getOrderSubTotal();
				
				shoppingBagPg.updateQuantityByPrdIndex(0, "2");
				Log.message(i++ + ". Updated the quantity", driver);
				
				double UpdatedPrice = shoppingBagPg.getUpdatedSubtotalPrice();
				double orderUpdatedPrice = shoppingBagPg.getOrderSubTotal();
				
				Log.softAssertThat(UpdatedPrice > price, 
						"Subtotal should be increased when the user updates the QTY", 
						"Subtotal is increased when the user updates the QTY", 
						"Subtotal is not increased when the user updates the QTY", driver);
				
				Log.softAssertThat(orderUpdatedPrice > orderPrice, 
						"Subtotal in order sumary should be increased when the user updates the QTY", 
						"Subtotal in order sumary increased when the user updates the QTY", 
						"Subtotal in order sumary not increased when the user updates the QTY", driver);
				
				SignIn signin = (SignIn) shoppingBagPg.navigateToCheckout();
				Log.message(i++ + ". Navigated to Checkout page", driver);
				
				//step 6 - Navigate to the checkout sign in page
				
				CheckoutPage checkoutPg = (CheckoutPage)signin.clickSignInButton();
				Log.message(i++ + ". Clicked Signin button.", driver);
				
				//step 7 - Navigate to Checkout page - step 1
				
				checkoutPg.continueToShipping(userMail);
				Log.message(i++ + ". Navigated to Shipping section", driver);
				
				checkoutPg.fillingShippingDetailsAsGuest("No", checkoutShipAdd, ShippingMethod.Standard);
				Log.message(i++ + ". Shipping Address entered successfully", driver);
				
				checkoutPg.checkUncheckUseThisAsBillingAddress(false);
				Log.message(i++ + ". Use this as a Billing address check box disabled", driver);
				
				//Step 8 - Navigate to Checkout page - step 2
				
				checkoutPg.continueToBilling();
				Log.message(i++ + ". Continued to Billing address", driver);
				
				checkoutPg.fillingBillingDetailsAsGuest(checkoutBillAdd);
				Log.message(i++ + ". Billing Address entered successfully", driver);
				
				checkoutPg.continueToPaymentFromBilling();
				Log.message(i++ + ". Continued to Payment Page", driver);
				
				checkoutPg.selectPaypalPayment();
				Log.message(i++ + ". Selected 'Paypal' as payment method", driver);
				
				PaypalPage paypalPage = checkoutPg.clickOnPaypalButton();
				Log.message(i++ + ". Clicked on Paypal button in shopping bag page", driver);
		
				PaypalConfirmationPage pcp=paypalPage.enterPayapalCredentials(paypalemail,paypalpassword);
				Log.message(i++ + ". Continued with Paypal Credentials.", driver);
				
				pcp.clickContinueConfirmation();
				Log.message(i++ + ". Clicked on Continue button.", driver);
		
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg), 
						"Checkout page should be displayed!", 
						"Checkout page is displayed",
						"Checkout page is not displayed", driver);
				
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
				
				//Step 9 - Navigate to Checkout page - step 3
				
				HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
				
				String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
				
				OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
				Log.message(i++ + ". Order Placed successfully", driver);
				
				//Step 10- Order confirmation page
					
				HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
				
				Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
						"The product added should be present in the receipt", 
						"The product added is present in the receipt", 
						"The product added is not present in the receipt", driver);
				
				String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
				
				Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
						"The order total should display correctly", 
						"The order total is displaying correctly", 
						"The order total is not displaying correctly", driver);
			} else{ 
				Log.fail("Product is not a Monogramming product", driver);
			}
			
			Log.testCaseResult();


		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24119
