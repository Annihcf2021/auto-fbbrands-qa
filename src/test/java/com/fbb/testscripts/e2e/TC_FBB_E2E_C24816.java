package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24816 extends BaseTest{
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	@Test(groups={ "plcc", "desktop", "tablet", "mobile" }, dataProviderClass=DataProviderUtils.class , dataProvider="parallelTestDataProvider")
	public void M1_FBB_E2E_C24816(String browser) throws Exception{
	Log.testCaseInfo();  
	
	final WebDriver driver= WebDriverFactory.get(browser);
			
	int i=1;
	try{	
			String searchKey = TestData.get("prd_variation");
			String guestEmail = AccountUtils.generateEmail(driver);
			String nonPLCCcard = "cards_2";
			
			//Always Approve Data
			String addressAlwaysApprove = "plcc_always_approve_address";
			
			//Step-1 & 2
			Object[] obj= GlobalNavigation.addProduct_Checkout(driver, searchKey, i, guestEmail);
			CheckoutPage checkoutPg= (CheckoutPage) obj[0];
			i=(int) obj[1];
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("btnShippingContinue"),checkoutPg),
					"user should be in the shipping information section.",
					"user is in the shipping information section.",
					"user is not in the shipping information section.", driver);
			
			//Step-3
			checkoutPg.fillingShippingDetailsAsGuest(addressAlwaysApprove, ShippingMethod.Standard);
			Log.message(i++ +". Entered shipping address", driver);
			
			Log.softAssertThat(checkoutPg.verifyUseThisAsBillingAddressIsChecked(), 
					"The Use this as billing address checkbox should be checked",
					"The Use this as billing address checkbox is checked",
					"The Use this as billing address checkbox is not checked", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ +". Clicked continue button from shipping address section", driver);
			
			if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkoutPg)){
										
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkoutPg),
						"The preapproval modal should display. ",
						"The preapproval modal is display. ",
						"The preapproval modal is not displayed. ", driver);
				
				//Step-4
				checkoutPg.closePLCCOfferByNoThanks1();
				Log.message(i++ +". Clicked no thanks button from PLCC overlay", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg), 
						"user will be taken to the payment method section of checkout.",
						"user will be taken to the payment method section of checkout.",
						"user will be taken to the payment method section of checkout.", driver);
				

				Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "mdlPLCCRebuttal", "paymentmethodSection", checkoutPg),
						"The acquisition rebuttal modal should be displayed above the Add New Credit Card form.",
						"The acquisition rebuttal modal is displayed above the Add New Credit Card form.",
						"The acquisition rebuttal modal is not displayed above the Add New Credit Card form.", driver);
				
				//Step-5
				checkoutPg.openClosePLCCRebuttal("open");
				Log.message(i++ +". Clicked learn more link from rebuttal solot", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCAcquisitionRebuttal"),checkoutPg),
						"The acquisition rebuttal over lay should opens up.",
						"The acquisition rebuttal over lay is opens up.",
						"The acquisition rebuttal over lay is not opens up.", driver);
				
				//Step-6 
				checkoutPg.continueToPLCCStep2InPLCCACQ();
				Log.message(i++ +". Clicked get it today button from rebuttal modal", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApprovalStep2"),checkoutPg),
						"The acquisition rebuttal step 2 overlay should opens up.",
						"The acquisition rebuttal step 2 overlay is opens up.",
						"The acquisition rebuttal step 2 overlay is not opens up.", driver);
				
				//Step-7
				checkoutPg.closePLCCOfferByNoThanks();
				Log.message(i++ +". Clicked no thanks button from PLCC step-2 overlay", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg),  
						"User should be taken to the payment method section of Step 2",
						"User should be taken to the payment method section of Step 2",
						"User should be taken to the payment method section of Step 2",driver);
			
				Log.softAssertThat(checkoutPg.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("mdlPLCCRebuttal"), checkoutPg),
						"On checkout, the acquisition rebuttal slot will no longer display",
						"On checkout, the acquisition rebuttal slot is no longer displayed", 
						"On checkout, the acquisition rebuttal slot is still displayed", driver);
						
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
			
				//step-8
				checkoutPg.fillingCardDetails1(nonPLCCcard, false);
				Log.message(i++ +". Filled credit card details", driver);
			
				checkoutPg.continueToReivewOrder();
				Log.message(i++ +". Clicked continue button", driver);
			
				OrderConfirmationPage orderPg= checkoutPg.clickOnPlaceOrderButton();
				Log.message(i++ +". Clicked Place order button", driver);
			
				Log.softAssertThat(orderPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPg),
						"user should taken to the order confirmation page.",
						"user is taken to the order confirmation page.",
						"user is not taken to the order confirmation page.",driver);
			
			} else {
				Log.fail("Checkout step 1 ovelay is not displayed hence can't proceed further", driver);
			}
			
			Log.testCaseResult();
						
		}//End of try 
		catch(Exception e){
			Log.exception(e, driver);
		}//End of catch
		finally{
			Log.endTestCase(driver);
		}//End of finally
	}
}
