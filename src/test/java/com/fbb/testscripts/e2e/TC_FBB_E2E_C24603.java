package com.fbb.testscripts.e2e;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.fbb.pages.HomePage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MailDropPage;
import com.fbb.pages.account.PasswordResetPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24603 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "email", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24603(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			String validPassword = accountData.get("password_global"); 
			String registeredEmail = TestData.get("registered_mailId");
			String iinValidPasswordTooLong = "invalidpassword";
			String inValidPasswordTooShort = "pass123";
			String inValidPasswordAlphaOnly = "invalidpas";
			String mismatchConfirmPassword = "mismatch1";
			String textErrorShortPassword = demandData.get("ShortPasswordError");
			String expiredColor = demandData.get("expired_pw_reset_color");
			String passwordErrorColor = demandData.get("password_error_color");
			String emailNotRegistered = demandData.get("EmailNotRegisteredMsg");
			int inboxCount = 0;
			
			{
				try {
					inboxCount = GlobalNavigation.getInboxCountFromMailDrop(registeredEmail, driver);
					Log.message("Inbox count for " + registeredEmail + " is " + inboxCount);
				} catch(Exception e) {
					Log.message("Inbox count for " + registeredEmail + " is " + inboxCount);
				}
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Clicked on Sign in link and navigated to sign in page!", driver);
	
			signIn.clickForgotPwdLink();
			Log.message(i++ + ". Clicked on Forgot Password link!", driver);
	
			//Step-1: functionality of Header
			if(!Utils.isMobile()) {
                Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfListOfElements(Arrays.asList("headingRequestPassword", "subHeadRequestPasswordModal", "txtEmailInForgotPassword", "btnSendRequestPassword"), signIn),
                        "Header should be displayed on top left of the Forgot Password Request Modal.",
                        "Header is displayed on top left of the Forgot Password Request Modal.",
                        "Header is not displayed on top left of the Forgot Password Request Modal.", driver);
            } else {
            	Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfListOfElements(Arrays.asList("headingRequestPasswordMobile", "subHeadRequestPasswordModalMobile", "txtEmailInForgotPassword", "btnSendRequestPassword"), signIn),
            			"Header should be displayed on top left of the Forgot Password Request Modal.",
            			"Header is displayed on top left of the Forgot Password Request Modal.",
            			"Header is not displayed on top left of the Forgot Password Request Modal.", driver);
            }
	
			//Step-2: functionality of Subhead
			if(!Utils.isMobile()) {
				Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "headingRequestPassword", "subHeadRequestPasswordModal", signIn),
						"Subhead should be displayed below the Header.",
						"Subhead is displayed below the Header.",
						"Subhead is not displayed below the Header.", driver);
			} else {
				Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "headingRequestPasswordMobile", "subHeadRequestPasswordModalMobile", signIn),
						"Subhead should be displayed below the Header.",
						"Subhead is displayed below the Header.",
						"Subhead is not displayed below the Header.", driver);
			}
	
			if(!Utils.isMobile()) {
				signIn.closeForgotPasswordDialog();
				Log.message(i++ + ". Clicked on anywhere on the page to close forgot password pop up!", driver);
	
				//Step-3: Functionality of Email Address
				signIn.clickForgotPwdLink();
				Log.message(i++ + ". Clicked on 'Forgot password' link", driver);
			}
	
			signIn.enterForgotPasswordEmailID("sdfsfsdf");
			Log.message(i++ + ". Entered Invalid Email Id to validate error message!", driver);
	
			signIn.clickSendRequestPassword();
			Log.message(i++ + ". Clicked on 'Send' button!", driver);
	
			if(!Utils.isMobile()) {
				Log.softAssertThat((signIn.elementLayer.verifyElementTextContains("passwordFieldError", "Enter a valid email address.", signIn)), 
						"Appropriate Error message should be displayed when entering invalid Email Id", 
						"Appropriate Error message is displayed when entering invalid Email Id",
						"Appropriate Error message is not displayed when entering invalid Email Id", driver);
			} else {
				Log.softAssertThat((signIn.elementLayer.verifyElementTextContains("passwordFieldErrorMobile", "Enter a valid email address.", signIn)), 
						"Appropriate Error message should be displayed when entering invalid Email Id", 
						"Appropriate Error message is displayed when entering invalid Email Id",
						"Appropriate Error message is not displayed when entering invalid Email Id", driver);
			}
	
			signIn.enterForgotPasswordEmailID("abc@gmail.com");
			Log.message(i++ + ". Entered valid Email Id!", driver);
	
			Log.softAssertThat(signIn.getEnteredEmailID().contains("@") && signIn.getEnteredEmailID().contains(".com"), 
					"Email Address should be in proper format", 
					"Email Address is in proper format",
					"Email Address is not in proper format", driver);
	
			//Step-4: functionality of Send button
			Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtEmailInForgotPassword", "btnSendInForgotPassword", signIn),
					"Send button should be displayed below the the Email Address field.", 
					"Send button is displayed below the the Email Address field.",
					"Send button is not displayed below the the Email Address field.", driver);
			
			if(Utils.isMobile()) {				
				Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnSendRequestPassword", "lnkBackToLoginMobile", signIn), 
						"Cancel button should be displayed below the Send Button", 
						"Cancel button is displayed below the Send Button.",
						"Cancel button not displayed below the Send Button", driver);
			}
	
			if(!Utils.isMobile()) {
				signIn.closeForgotPasswordDialog();
				Log.message(i++ + ". Clicked on anywhere on the page to close forgot password pop up!", driver);
			} else {
				signIn.closeForgotPasswordPageMobile();
				Log.message(i++ + ". Clicked on back to login link to close forgot password page in mobile view!", driver);
			}
	
			if(Utils.isMobile()) {
				Log.softAssertThat(signIn.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("headingRequestPassword"), signIn),
						"Forgot Password Modal should not be displayed", 
						"Forgot Password Modal is not displayed",
						"Forgot Password Modal is displayed", driver);
			} else {
				Log.softAssertThat(signIn.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("headingRequestPasswordMobile"), signIn),
						"Forgot Password Modal should not be displayed", 
						"Forgot Password Modal is not displayed",
						"Forgot Password Modal is displayed", driver);
			}
			
			signIn.clickForgotPwdLink();
			Log.message(i++ + ". Clicked on Forgot Password link!", driver);
			
			signIn.enterEmailInForgotPasswordMdl("aspire121212@aspire.com");
			Log.message(i++ + ". Typed in Email.", driver);
			
			signIn.clickSendRequestPassword();
			Log.message(i++ + ". Clicked send.", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyTextEquals("txtUnRegisteredEmailModal", emailNotRegistered, signIn) ,
					"'This email is not registered for an account. Please check your spelling or create an account.' should be displayed if Unregistered email address is given",
					"'This email is not registered for an account. Please check your spelling or create an account.' is displayed if Unregistered email address is given",
					"'This email is not registered for an account. Please check your spelling or create an account.' not displayed if Unregistered email address is given", driver);
			
			if(!Utils.isMobile()) {
				signIn.closeForgotPasswordDialog();
				Log.message(i++ + ". Clicked on anywhere on the page to close forgot password pop up!", driver);
			} else {
				signIn.closeForgotPasswordPageMobile();
				Log.message(i++ + ". Clicked on back to login link to close forgot password page in mobile view!", driver);
			}
			
			signIn.clickForgotPwdLink();
			Log.message(i++ + ". Clicked on Forgot Password link!", driver);
			
			signIn.enterEmailInForgotPasswordMdl(registeredEmail);
			Log.message(i++ + ". Typed in Email.", driver);
			
			signIn.clickSendRequestPassword();
			Log.message(i++ + ". Clicked send.", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyElementDisplayedleft("passwordRequestReceivedContainer", "headingRequestPassword",signIn),
					"Headline should be displayed on top left corner of the Forgot Password Request Received Modal",
					"Headline is displayed on top left corner of the Forgot Password Request Received Modal",
					"Headline is not displayed on top left corner of the Forgot Password Request Received Modal", driver);
			
			//Step-2: Verify the display and functionality of Subhead
			Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElements(driver, "headingRequestPassword", "newSubHeadPasswordResetConfirmation", signIn), 
					"Subhead should be displayed below the Headline", 
					"Subhead is displayed below the Headline", 
					"Subhead is not displayed below the Headline", driver);
			
			if(!Utils.isMobile()) {
				signIn.closeForgotPasswordDialog();
				Log.message(i++ + ". Clicked on anywhere on the page to close forgot password pop up!", driver);
			} else {
				signIn.closeForgotPasswordPageMobile();
				Log.message(i++ + ". Clicked on back to login link to close forgot password page in mobile view!", driver);
			}
			
			Utils.openNewTab(driver);
			List<String> handle = new ArrayList<String> (driver.getWindowHandles());
			driver.switchTo().window(handle.get(handle.size() - 1));
			
			MailDropPage mail = new MailDropPage(driver).get();
			Log.message(i++ + ". MailDrop page loaded successfully", driver);
			
			mail.navigateToDropMailBox(registeredEmail);
			Log.message(i++ + ". Navigated to usermail inbox ", driver);
						
			mail.openReceivedMail("password", inboxCount, "Password reset email");
			Log.message(i++ + ". Navigated to Forgot password request mail ", driver);
			
			PasswordResetPage passwordReset = mail.clickResetMyPasswordInEmail();
			Log.message(i++ + ". Clicked Reset my password button in Email", driver);
			
			Log.softAssertThat(passwordReset.elementLayer.verifyVerticalAllignmentOfElements(driver, "resetPasswordPageHeading", "resetPasswordPageNewPassword", passwordReset), 
					"New Password field should be displayed below the Subhead.", 
					"New Password field is displayed below the Subhead.", 
					"New Password field is not displayed below the Subhead.", driver);	
			
			passwordReset.enterPasswordInResetPage("resetPasswordPageNewPassword", iinValidPasswordTooLong, passwordReset);
			Log.message(i++ + ". Typed invalid password in New Password field", driver);
			
			passwordReset.enterPasswordInResetPage("resetPasswordPageConfirmPassword", iinValidPasswordTooLong, passwordReset);
			Log.message(i++ + ". Typed invalid password in Confirm Password field", driver);
			
			Log.softAssertThat(passwordReset.checkPasswordTextIsMaskOrVisibleInResetPage("resetPasswordPageNewPassword", "mask", passwordReset)
					&& passwordReset.checkPasswordTextIsMaskOrVisibleInResetPage("resetPasswordPageConfirmPassword", "mask", passwordReset), 
				"By default, the user entered password should be masked.", 
				"By default, the user entered password is masked.", 
				"By default, the user entered password is not masked.", driver);
			
			passwordReset.clickResetInResetPage();
			Log.message(i++ + ". Clicked on Reset button in Reset password page", driver);
			
			Log.softAssertThat(passwordReset.getErrorTextNewPasswordInResetPage().equalsIgnoreCase(textErrorShortPassword), 
					"System should perform validation that password meets 8-10 characters", 
					"System performed validation that password meets 8-10 characters", 
					"System not performed validation that password meets 8-10 characters", driver);
			
			passwordReset.enterPasswordInResetPage("resetPasswordPageNewPassword", inValidPasswordTooShort, passwordReset);
			Log.message(i++ + ". Typed invalid password in New Password field", driver);
			
			passwordReset.enterPasswordInResetPage("resetPasswordPageConfirmPassword", inValidPasswordTooShort, passwordReset);
			Log.message(i++ + ". Typed invalid password in Confirm Password field", driver);
			
			passwordReset.clickResetInResetPage();
			Log.message(i++ + ". Clicked on Reset button in Reset password page", driver);
			
			Log.softAssertThat(passwordReset.getErrorTextNewPasswordInResetPage().equalsIgnoreCase(textErrorShortPassword), 
						"System should perform validation that password meets 8-10 characters", 
						"System performed validation that password meets 8-10 characters", 
						"System not performed validation that password meets 8-10 characters", driver);
			
			passwordReset.enterPasswordInResetPage("resetPasswordPageNewPassword", inValidPasswordAlphaOnly, passwordReset);
			Log.message(i++ + ". Typed invalid password in New Password field", driver);
			
			passwordReset.enterPasswordInResetPage("resetPasswordPageConfirmPassword", inValidPasswordAlphaOnly, passwordReset);
			Log.message(i++ + ". Typed invalid password in Confirm Password field", driver);
			
			passwordReset.clickResetInResetPage();
			Log.message(i++ + ". Clicked on Reset button in Reset password page", driver);
			
			Log.softAssertThat(passwordReset.getErrorTextNewPasswordInResetPage().equalsIgnoreCase(textErrorShortPassword), 
					"System should perform validation that password meets at least 1 number and 1 letter", 
					"System is performed validation that password meets at least 1 number and 1 letter", 
					"System not performed validation that password meets at least 1 number and 1 letter", driver);
			
			String showHideNewPassword = passwordReset.clickShowPasswordInResetPage("resetPasswordPageNewPasswordShow", passwordReset);
			Log.message(i++ + ". Clicked on Show button in New password field", driver);
			
			String showHideConfirmPassword = passwordReset.clickShowPasswordInResetPage("resetPasswordPageConfirmPasswordShow", passwordReset);
			Log.message(i++ + ". Clicked on Show button in Confirm password field", driver);
			
			Log.softAssertThat(showHideNewPassword.equalsIgnoreCase("hide") && showHideConfirmPassword.equalsIgnoreCase("hide"), 
					"On clicking Show place holder should change to Hide.", 
					"On clicking Show place holder is changed to Hide.", 
					"On clicking Show place holder is not changed to Hide.", driver);
			
			Log.softAssertThat(passwordReset.checkPasswordTextIsMaskOrVisibleInResetPage("resetPasswordPageNewPassword", "unmask", passwordReset) 
									&& passwordReset.checkPasswordTextIsMaskOrVisibleInResetPage("resetPasswordPageConfirmPassword", "unmask", passwordReset), 
					"On clicking Show User entered data should be visible instead of masked.", 
					"On clicking Show User entered data is visible instead of masked.", 
					"On clicking Show User entered data is not visible instead of masked.", driver);
			
			showHideNewPassword = passwordReset.clickShowPasswordInResetPage("resetPasswordPageNewPasswordShow", passwordReset);
			Log.message(i++ + ". Clicked on Hide button in New password field", driver);
			
			showHideConfirmPassword = passwordReset.clickShowPasswordInResetPage("resetPasswordPageConfirmPasswordShow", passwordReset);
			Log.message(i++ + ". Clicked on Hide button in Confirm password field", driver);
			
			Log.softAssertThat(showHideNewPassword.equalsIgnoreCase("show") && showHideConfirmPassword.equalsIgnoreCase("show"), 
					"On clicking Hide, place holder should change to show.", 
					"On clicking Hide, place holder is changed to Show.", 
					"On clicking Hide, place holder is not changed to Show.", driver);
			
			Log.softAssertThat(passwordReset.checkPasswordTextIsMaskOrVisibleInResetPage("resetPasswordPageNewPassword", "mask", passwordReset) 
									&& passwordReset.checkPasswordTextIsMaskOrVisibleInResetPage("resetPasswordPageConfirmPassword", "mask", passwordReset), 
					"On clicking Hide, the data entered by the user should be masked again", 
					"On clicking Hide, the data entered by the user should be masked again", 
					"On clicking Hide, the data entered by the user should be masked again", driver);
			
			passwordReset.enterPasswordInResetPage("resetPasswordPageNewPassword", validPassword, passwordReset);
			Log.message(i++ + ". Typed valid password in New Password field", driver);
			
			passwordReset.enterPasswordInResetPage("resetPasswordPageConfirmPassword", mismatchConfirmPassword, passwordReset);
			Log.message(i++ + ". Typed mismatch password in Confirm Password field", driver);
			
			passwordReset.clickResetInResetPage();
			Log.message(i++ + ". Clicked on Reset button in Reset password page", driver);  
			
			Log.softAssertThat(passwordReset.elementLayer.verifyElementColor("errorMismatchConfirmPassword", passwordErrorColor, passwordReset) && 
					passwordReset.elementLayer.verifyElementColor("errorMismatchConfirmPassword", passwordErrorColor, passwordReset), 
					"Data entered in the Confirm New Password field should match with the data entered in the New Password field.", 
					"Data entered in the Confirm New Password field is matched with the data entered in the New Password field.", 
					"Data entered in the Confirm New Password field is not matched with the data entered in the New Password field.", driver);
			
			passwordReset.enterPasswordInResetPage("resetPasswordPageNewPassword", validPassword, passwordReset);
			Log.message(i++ + ". Typed valid password in New Password field", driver);
			
			passwordReset.enterPasswordInResetPage("resetPasswordPageConfirmPassword", validPassword, passwordReset);
			Log.message(i++ + ". Typed valid password in Confirm Password field", driver);
			
			passwordReset.clickResetInResetPage();
			Log.message(i++ + ". Clicked on Reset button in Reset password page", driver);  
			
			Log.softAssertThat(passwordReset.elementLayer.isElementAvailable(Arrays.asList("msgPasswordUpdated", "myAccount"), passwordReset), 
					"System should navigate user to the Account overview page with the confirmation message on that page.", 
					"System is navigateed user to the Account overview page with the confirmation message on that page.", 
					"System is not navigated user to the Account overview page with the confirmation message on that page.", driver);		
			
			//Step 9- Verify the display and functionality of Expired Copy
			driver.switchTo().window(handle.get(1));
			passwordReset = mail.clickResetMyPasswordInEmail();
			Log.message(i++ + ". Clicked Reset my password button in Email", driver);
			Log.message(i++ + ". Navigated to password reset link.", driver);
			
			Log.softAssertThat(passwordReset.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("frmPasswordReset"), passwordReset), 
					"System should navigate to the Forgot Password Request Page", 
					"System navigated to the Forgot Password Request Page", 
					"System did not navigate to the Forgot Password Request Page", driver);
			
			Log.softAssertThat(passwordReset.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("breadcrumbMobile","pwdResetHeader","pwdResetSubHead","txtEmailId","btnSend","btnBackToLogin"), passwordReset), 
					"System should display Breadcrumb, Header, Subhead(Expired copy), Email address, Send, Back to Login in Forgot Password Request Page", 
					"System shows correct components in Password Request Page", 
					"System does not show correct components in Password Request Page", driver);
			
			Log.softAssertThat(passwordReset.elementLayer.verifyVerticalAllignmentOfElements(driver, "pwdResetHeader", "pwdResetSubHead", passwordReset), 
					"Password reset subhead should be displayed below the header.", 
					"Password reset subhead is displayed below the header.", 
					"Password reset subhead is not displayed below the header.", driver);
			
			Log.softAssertThat(!passwordReset.elementLayer.verifyElementColor("pwdResetSubHead", expiredColor, passwordReset), 
					"Password reset subhead should not be displayed in red color.", 
					"Password reset subhead is not displayed in red color.", 
					"Password reset subhead is displayed in red color.", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24125
