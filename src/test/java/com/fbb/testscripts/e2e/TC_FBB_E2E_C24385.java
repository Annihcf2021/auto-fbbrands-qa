package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.EnlargeViewPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.Refinements;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.CollectionUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24385 extends BaseTest {

EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop"}, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24385(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {

			String username = accountData.get("Expire_Payment_user").split("\\|")[0];
			String password = accountData.get("Expire_Payment_user").split("\\|")[1];
			String promoCode = TestData.get("cpn_off_10_percentage");
			String lev1 = TestData.get("level-1");
			String prd_id = TestData.product("prd_freeexchange");

			// Step 1 - Navigate to the website
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			Headers headers = homePage.headers;
			
			// Step 2 - Click on Sign in button from header
			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Navigated to Sign in Page!", driver);

			MyAccountPage accountPage = signIn.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to My Account page as : " + username, driver);
			
			ProfilePage profilePg = accountPage.navigateToUpdateProfile();
			String firstName = profilePg.getFirstName();
			Log.message(i++ + ". Navigated to Profile page", driver);
			
			AddressesPage addressPage = accountPage.navigateToAddressPage();
			Log.message(i++ + ". Navigated to address page", driver);
			
			addressPage.verifyAddressIsDefault("Default");
			Log.message(i++ + ". Default saved address is displayed", driver);

			PaymentMethodsPage paymentMethod = accountPage.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to the payment page!", driver);

			paymentMethod.verifyExpiryDefaultCard();
			Log.message(i++ + ". Expired defualt payment method exists", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyPageElements(Arrays.asList("spanAccountButton"),headers)
							&& headers.getAccountMenuHeader().toLowerCase().contains(firstName.toLowerCase())
							&& headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "iconUserDesktop", "spanAccountButton", headers),
					"Sign in link should change to My Account with a text Hello 'Firstname' and user logo to the left in utility bar.",
					"Sign in link change to My Account with a text Hello 'Firstname' and user logo to the left in utility bar.",
					"Sign in link didn't change to My Account with a text Hello 'Firstname' and user logo to the left in utility bar.", driver);
			
			PlpPage plpPage = null;
			PdpPage pdpPage = null;
			SearchResultPage slpPage = null;

			// Step 4 - The system navigates to the PLP

			if (TestData.get("global_nav_categories").toLowerCase().contains("lingerie")) {
				plpPage = headers.navigateTo("Lingerie");
				Refinements refinements = new Refinements(driver).get();
				Log.message(i++ + ". Navigated to PLP Page for Lingerie", driver);
				if(plpPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), plpPage)) {
					if(!(Utils.isDesktop())){
						refinements.selectFirstBraBrand_CubSizeInMobAndTab();
					} else {
						refinements.selectFirstBraBrand_CubSizeInDesk(1, 0);
					}
					Log.message(i++ + ". First Bra Brand Size & Brand Cub Size Selected.", driver);

					String sortOption = refinements.selectSortBy("Lowest Priced");
					Log.message(i++ + ". Sort Option(" + sortOption + ") selected from Sorting menu.", driver);

					Log.softAssertThat(refinements.verifySortApplied(sortOption),
							"Products should be sorted and displayed in product list page based on the sort by option",
							"Products are sorted and displayed in product list page based on the sort by option",
							"Products are not sorted and displayed in product list page based on the sort by option",	driver);

					Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("sectionSearchResults"), plpPage),
							"The system should display the listed bras under that bra size",
							"The system displays the listed bras under that bra size",
							"The system does not display the listed bras under that bra size", driver);

					pdpPage = plpPage.navigateToPdp(1);
					Log.message(i++ + ". Navigated to PDP Page.", driver);
					
				} else {
					slpPage = headers.searchProductKeyword("Bra");
					refinements = new Refinements(driver).get();
					String sortOption = refinements.selectSortBy("Lowest Priced");
					Log.message(i++ + ". Sort Option(" + sortOption + ") selected from Sorting menu.", driver);
					
					Log.softAssertThat(refinements.verifySortApplied(sortOption),
							"Products should be sorted and displayed in product list page based on the sort by option",
							"Products are sorted and displayed in product list page based on the sort by option",
							"Products are not sorted and displayed in product list page based on the sort by option",	driver);
					
					Log.softAssertThat(slpPage.elementLayer.verifyPageElements(Arrays.asList("sectionSearchResults"), slpPage),
							"The system should display the listed bras under that bra size",
							"The system displays the listed bras under that bra size",
							"The system does not display the listed bras under that bra size", driver);
					
					pdpPage = slpPage.clickOnProductBasedOnIndex(1);
					Log.message(i++ + ". Navigated to PDP Page.", driver);
				}

				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("sectionBraBrandSize", "sectionBraCupSize"), pdpPage),
						"For Bra the system should display Band size & Cup size",
						"For Bra the system should display Band size & Cup size",
						"For Bra the system should display Band size & Cup size", driver);

				// Step 5 - The system navigates to the PDP

				pdpPage.selectColor();
				Log.message(i++ + ". Selected Color", driver);

				pdpPage.mouseHoverAddToBag();
				Log.message(i++ + ". Mouse hovered on Add To bag button.", driver);

				Log.softAssertThat(pdpPage.elementLayer.getAttributeForElement("btnAddToCart", "disabled", pdpPage).contains("true")
								&& pdpPage.getAdd2BagBtnText().toLowerCase().contains("please select size"),
						"Add to bag button should display the message 'Please select the size'",
						"Add to bag button should display the message 'Please select the size'",
						"Add to bag button should display the message 'Please select the size'", driver);

				pdpPage.selectBraBandSize();
				pdpPage.selectCupBasedOnIndex();
				pdpPage.clickAddProductToBag();
				Log.message(i++ + ". Product added to bag.", driver);

				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("mdlMiniCartOverLay"), pdpPage),
						"Add To Bag overlay should be displayed.", "Add To Bag overlay should be displayed.",
						"Add To Bag overlay should be displayed.", driver);

				pdpPage.closeAddToBagOverlay();

			} else {
				plpPage = headers.navigateTo(lev1);
				Log.message(i++ + ". Navigated to categroy", driver);

				Refinements refinements = new Refinements(driver).get();
				String sortOption = refinements.selectSortBy("Lowest Price");
				Log.message(i++ + ". Sort Option(" + sortOption + ") selected from Sorting menu.", driver);

				Log.softAssertThat(refinements.verifySortApplied(sortOption),
						"Products should be sorted and displayed in product list page based on the sort by option",
						"Products should be sorted and displayed in product list page based on the sort by option",
						"Products should be sorted and displayed in product list page based on the sort by option", driver);

				pdpPage = plpPage.navigateToPdp(1);
				Log.message(i++ + ". Navigated to PDP", driver);

				pdpPage.addToBagCloseOverlay();
				Log.message(i++ + ". Product added to bag", driver);

			}
			headers.navigateToMyAccount();

			// step 6 - Hover mouse on My Account fly out.

			signIn = accountPage.signOutAccount();
			Log.message(i++ + ". Signed out and navigated to sigh in page.", driver);

			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("divLoginBoxDesktop"), signIn),
					"The user should be signed out from the registered user.",
					"The user is signed out from the registered user.",
					"The user is not signed out from the registered user.", driver);

			// Step 7 - Enter product id for (Eligible free exchange product) in the search
			// box and click on search icon.

			pdpPage = headers.navigateToPDP(prd_id);
			Log.message(i++ + ". Navigated to Pdp page.", driver);

			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("btnEnlarge"), pdpPage)
					&& pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgProductPrimaryImage","btnEnlarge", pdpPage),
					"Enlarge button should be displayed below Product image",
					"Enlarge button is displayed below Product image",
					"Enlarge button not displayed below Product image", driver);

			List<String> pdpColors = pdpPage.getColorsAsList();
			EnlargeViewPage enlargePage = pdpPage.clickOnEnlargeButton();
			Log.message(i++ + ". Enlarge Window Opened.", driver);

			List<String> enlargeColors = enlargePage.getColorsAsList();

			Log.softAssertThat(CollectionUtils.compareTwoList(pdpColors, enlargeColors),
					"PDP Page colors & Enlarge colors should be same.",
					"PDP Page colors & Enlarge colors are same.",
					"PDP Page colors & Enlarge colors is not same.", driver);

			enlargePage.closeEnlargeViewModal();
			Log.message(i++ + ". Enlarge Window Closed.", driver);

			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ".Product added to cart", driver);

			// Step 8 - Navigate to the cart page

			ShoppingBagPage shoppingBagPg = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);

			Log.softAssertThat(shoppingBagPg.elementLayer.verifyTextContains("lblTaxDisclaimer", "Tax fees are estimated",shoppingBagPg)
					&& shoppingBagPg.elementLayer.verifyTextContains("lblTaxDisclaimer","Final tax fees will be calculated", shoppingBagPg),
					"Tax disclaimer should be displayed.", "Tax disclaimer is getting displayed.",
					"Tax disclaimer is not getting displayed.", driver);

			boolean cpnApplied = shoppingBagPg.applyPromoCouponCode(promoCode);
			Log.message(i++ + ". Promo code applied", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkCheckoutPaypal","lblTaxDisclaimer", shoppingBagPg),
					"Paypal button should be displayed above tax disclaimer",
					"Paypal button is displayed above tax disclaimer",
					"Paypal button is not displayed above tax disclaimer", driver);
			
			if(cpnApplied) {
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyPageElements(Arrays.asList("lnkCouponRemove"), shoppingBagPg),
						"Remove button should get displayed.", "Remove button is getting displayed.",
						"Remove button is not getting displayed.", driver);

				Log.softAssertThat(shoppingBagPg.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkCouponRemove","txtCouponApplied", shoppingBagPg),
						"Remove button should be displayed right to coupon applied value",
						"Remove button is displayed right to coupon applied value",
						"Remove button is not displayed right to coupon applied value", driver);
			}

			// Step 9 - Navigate to my account sign in page

			accountPage = homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to My Account Page with registered user.", driver);

			// Step 10 - Navigate to the cart page

			shoppingBagPg = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);

			Log.softAssertThat(shoppingBagPg.elementLayer.verifyPageElements(Arrays.asList("txtQtyMergeMsg"), shoppingBagPg),
					"Merge message should get displayed.", "Merge message is getting displayed.",
					"Merge message is not getting displayed.", driver);

			// Step 11 - Navigate to Checkout page - step 1

			CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);

			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("step1Active"), checkoutPg),
					"Shipping address page displayed.", "Shipping address page is displayed.",
					"Shipping address page is not getting displayed.", driver);

			checkoutPg.selectValueFromSavedAddressesDropdownByIndex(2);
			Log.message(i++ + ". Selected saved address in shipping", driver);

			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("savedAddressCount"), checkoutPg)
							&& checkoutPg.elementLayer.verifyElementTextContains("savedAddressCount", "3", checkoutPg),
					"Saved address count should be displayed", "Saved address count is correctly displayed",
					"Saved address count is not displaying correctly", driver);

			if (checkoutPg.getNumberOfShippingMethodsAvailable() > 2) {
				checkoutPg.selectShippingMethod(ShippingMethod.NextDay);
				Log.message(i++ + ". Select the superfast shippment method", driver);

			} else {
				Log.warning("Only standard delivery is available, so selection of next day shipping method is not possible");
			}
			
			if (checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("shippingmethodExceptionOverlay"),checkoutPg)) {
				checkoutPg.clickOnContinueInShippingMethodExceptionOverlay();
				Log.message(i++ + ". Closed shipping method exception overlay", driver);
			}

			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box disabled", driver);

			// Step 12 - Navigate to Checkout page - step 2

			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);

			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("expireCard"), checkoutPg),
					"Expired CC card should be displayed in the checkout page",
					"Expired CC card is displayed in the checkout page",
					"Expired CC card is not displayed in the checkout page", driver);

			Log.softAssertThat(checkoutPg.verifyExpirationDateColor(),
					"The expired month & year will display a red color.",
					"The expired month & year is displaying in red color.",
					"The expired month & year is not displayed a red color.", driver);

			BrowserActions.clickOnElementX(driver, "expireCard", checkoutPg, "Expired card");

			Log.softAssertThat(checkoutPg.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("lblSelectedCard"), checkoutPg),
					"the user should not able to select the expired CC",
					"the user should able to select the expired CC");
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}

			if(checkoutPg.elementLayer.isElementsDisplayed(Arrays.asList("lnkAddNewCredit"),checkoutPg)) {
				checkoutPg.clickAddNewCredit();
				Log.message(i++ + ". Clicked on the Add new credit card button", driver);
			}

			checkoutPg.fillingCardDetails1("card_Visa", false);
			Log.message(i++ + ". Filled card details", driver);

			// Step 12 - Navigate to Checkout page - step 3

			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);

			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();

			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();

			OrderConfirmationPage receipt = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);

			// Step 13 - Order confirmation page
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();

			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId),
					"The product added should be present in the receipt", "The product added is present in the receipt",
					"The product added is not present in the receipt", driver);

			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();

			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal),
					"The order total should display correctly", "The order total is displaying correctly",
					"The order total is not displaying correctly", driver);

			Log.testCaseResult();

		} // Ending try block
		catch (Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // Ending finally

	}

}
