package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PaypalConfirmationPage;
import com.fbb.pages.PaypalPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24819 extends BaseTest{
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24819(String browser) throws Exception {
		Log.testCaseInfo();
		
		final WebDriver driver = WebDriverFactory.get(browser);

		int i = 1;
		try {
			String userEmail = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountData.get("password_global");
			String credential = userEmail + "|" + password;
			String addressAlwaysApprove = "plcc_always_approve_address";
			String prdVariation = TestData.get("prd_variation");
			String firstName = addressAlwaysApprove.split("|")[7];
			String lastName = addressAlwaysApprove.split("|")[8];
			
			String paypalEmail = accountData.get("credential_alwaysapprove_paypal").split("\\|")[0];
			String paypalPassword = accountData.get("credential_alwaysapprove_paypal").split("\\|")[1];
			
			{
				//Step 2
				GlobalNavigation.registerNewUserWithUserDetail(driver, 0, 0, firstName, lastName, credential);
				//Step 3
				GlobalNavigation.addNewAddressToAccount(driver, addressAlwaysApprove, true, credential);
			}
			
			//Step1
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to the 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2
			homePage.headers.navigateToMyAccount(userEmail, password);
			Log.message(i++ + ". Navigated to My Account page as : " + userEmail, driver);
			
			//Step-4
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation);
			Log.message(i++ + ". Navigated To PDP page : " + pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			CheckoutPage checkoutPg = (CheckoutPage) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to checkout page", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Clicked continue button ", driver);
			
			if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkoutPg)) {
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("mdlPLCCApproval"), checkoutPg), 
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed", 
						"PLCC modal is not getting displayed", driver);
				
				//Step 5
				checkoutPg.clickNoThanksInPLCC();
				Log.message(i++ + ". Clicked on No Thanks.", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("headerChkoutStep2"), checkoutPg), 
						"User should be taken to Checkout step 2.", 
						"User is taken to Checkout step 2.", 
						"User is not taken to Checkout step 2.", driver);
				
			} else {
				Log.failsoft("PLCC approval modal didn't show up.");
				return;
			}
			
			//Step 6
			checkoutPg.clickOnBrandLogo();
			Log.message(i++ + ". Click on Brand logo!", driver);
			
			homePage.headers.signOut();
			Log.message(i++ + ". Logged out.", driver);
			
			homePage = BrowserActions.clearCookies(driver);
			Log.message(i++ + ". Cleared cookies and navigated to Homepage", driver);
			
			//Step 7
			homePage = homePage.headers.navigateToHome();
			Log.message(i++ + ". Navigated to the 'Full Beauty Brands' Home Page!", driver);
			
			//Step 8
			homePage.headers.navigateToMyAccount(userEmail, password);
			Log.message(i++ + ". Navigated to My Account page as : " + userEmail, driver);
			
			//Step 9
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			if(cartPage.getQtyInCart() == 0) {
				pdpPage = homePage.headers.navigateToPDP(prdVariation);
				Log.message(i++ + ". Navigated To PDP page : " + pdpPage.getProductName(), driver);
				
				pdpPage.addToBagCloseOverlay();
				Log.message(i++ + ". Product added to cart", driver);
				
				cartPage = homePage.headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping bag page", driver);
			}
			
			if(cartPage.getOrderSubTotal() < 25) {
				int multiply = (int) (1 + (25 / cartPage.getOrderSubTotal()));
				cartPage.clickOnArrowContious("up", multiply);
				Log.message(i++ + ". Increased order total to over $25.00", driver);
			}
			
			PaypalPage paypalPage = cartPage.clickOnPaypalButton();
			Log.message(i++ + ". Clicked on Paypal Express button in shopping bag page", driver);
			
			Log.softAssertThat(paypalPage.elementLayer.verifyPageElements(Arrays.asList("paypalContent"), paypalPage), 
					"PayPal login window should open.", 
					"PayPal login windows opened", 
					"PayPal login window didn't open", driver);
			
			//Step 10
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			PaypalConfirmationPage pcPage = paypalPage.enterPayapalCredentials(paypalEmail, paypalPassword);
			Log.message(i++ + ". Continued with Paypal Credentials.", driver);
			
			if(!pcPage.elementLayer.verifyElementDisplayed(Arrays.asList("changeAddress"), pcPage)){
				pcPage.clickContinueConfirmation();
			}
			
			Log.softAssertThat(pcPage.elementLayer.verifyElementDisplayed(Arrays.asList("changeAddress"), pcPage), 
					"User will have the option of changing their shipping address and payment method", 
					"User has option.", 
					"User doesn't have option.", driver);
			
			//Step 11
			checkoutPg = pcPage.clickContinueConfirmation();
			Log.message(i++ + ". Clicked on Continue button.", driver);
	
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg), 
					"Checkout page should be displayed!", 
					"Checkout page is displayed",
					"Checkout page is not displayed", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("mdlPLCCApproval"), checkoutPg), 
					"PLCC modal should be displayed", 
					"PLCC modal is getting displayed", 
					"PLCC modal is not getting displayed", driver);
			
			//Step 12
			checkoutPg.clickGetItTodayInPLCC();
			Log.message(i++ + ". Clicked on 'Get it Today' in PLCC!", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("plccProfileSummary"), checkoutPg), 
					"User should be taken to the Step 2 overlay", 
					"User is taken to the Step 2 overlay", 
					"User is not taken to the Step 2 overlay", driver);
			
			//Step 13
			checkoutPg.typeSocialSecurityNumberInPlcc("1234");
			Log.message(i++ + ". Entered Social Security Number!", driver);
		
			checkoutPg.selectDateMonthYearInPLCC2("01", "01", "1980");
			Log.message(i++ + ". Selected Birth date in PLCC!", driver);
			
			checkoutPg.typeTextInMobileInPLCC("8015841844");
			Log.message(i++ +". Updated phone number", driver);
			
			checkoutPg.checkConsentInPLCC("yes");
			Log.message(i++ + ". Checked Terms and condition checkbox!", driver);
		
			checkoutPg.clickOnAcceptInPLCC();
			Log.message(i++ + ". Accept button is clicked!", driver);
			
			if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"), checkoutPg)) {
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"), checkoutPg), 
						"Congratulations modal should be displayed", 
						"Congratulations modal is displayed", 
						"Congratulations modal is not displayed", driver);
			} else {
				Log.reference("Congradulation modal is not displyed with ALWAYS APPROVE data", driver);
			}
			//Step 14
			checkoutPg.dismissCongratulationModal();
			Log.message(i++ + ". Clicked on 'Continue To Checkout' button!", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("step3Active"), checkoutPg), 
					"User should be taken to Checkout step 3", 
					"User is taken to Checkout step 3", 
					"User is not taken to Checkout step 3", driver);
			
			String plccVerification = Utils.getCurrentBrandShort().substring(0, 2) + "_plcc";
			Log.softAssertThat(checkoutPg.elementLayer.verifyAttributeForElement("logoPaymentType", "class", plccVerification, checkoutPg), 
					"PLCC card should be selected  when PLCC card approved", 
					"PLCC card is selected when PLCC card approved", 
					"PLCC card is not selected when PLCC card approved", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyNumberMaskedWithSpecifiedDigits("lblCardNumberInMiniPayment", 4, checkoutPg),
					"The masked number and last 4 digits of the PLCC card should be displayed in the \"Card Number\" field.",
					"The masked number and last 4 digits of the PLCC card is displayed in the \"Card Number\" field.",
					"The masked number and last 4 digits of the PLCC card is not displayed in the \"Card Number\" field.", driver);
			
			if(checkoutPg.dummyDataRecieved()) {
				Log.reference("Dummy data has been sent by ADS. Order not possible with provided data.");
				
			} else {
				//Step 15
				checkoutPg.clickOnPaymentDetailsContinueBtn();
				Log.message(i++ + ". Continued to Review & Place Order", driver);

				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("btnPlaceOrder"), checkoutPg),
						"Place order button should be displayed", 
						"Place order button is getting displayed",
						"Place order button is not getting displayed", driver);
				
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
				
				//Step 16
				checkoutPg.clickOnPlaceOrder();
				Log.message(i++ + ". Clicked on place order button", driver);
				
				if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"),checkoutPg)) {
					Log.reference("By Using always approve card user can't place order", driver);
				} else {
					Log.message(i++ + ". Clicked place order button", driver);

					OrderConfirmationPage orderPage = new OrderConfirmationPage(driver).get();
					Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage),
							"user should taken to the order confirmation page.",
							"user is taken to the order confirmation page.",
							"user is not taken to the order confirmation page.", driver);
				}
				
			}
			Log.testCaseResult();

		} // Ending try block
		catch (Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		} // Ending finally
	}
}
