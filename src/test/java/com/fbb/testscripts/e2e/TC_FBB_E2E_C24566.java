package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24566 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
		
	@Test(groups = { "checkout", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24566(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		String userMail = AccountUtils.generateEmail(driver);
		String checkoutShipAdd = "taxless_address";
		String checkoutBillpAdd = "valid_address1";
		String payment = "card_Visa";
		String reward = TestData.get(Utils.getRewardCertificate());
		
		int i=1;
		try
		{
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2: Click on my bag icon
			
			ShoppingBagPage shoppingBag = homePage.headers.clickOnBagLink();
			
			Log.softAssertThat(shoppingBag.verifyEmptyCart(), 
					"Page should be redirected to the empty cart page.", 
					"Page redirected to the empty cart page.", 
					"Page not redirected to the empty cart page.", driver);
			
			Log.message(i++ + ". Navigated to Empty cart Page!", driver);
			
			Log.softAssertThat(shoppingBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "emptyShoppingHeader", "emptyCartMsg", shoppingBag), 
					"Below 'My Shopping bag' header, 'Your Shopping bag is empty' message should be shown", 
					"Below 'My Shopping bag' header, 'Your Shopping bag is empty' message is shown", 
					"Below 'My Shopping bag' header, 'Your Shopping bag is empty' message not shown", driver);			
			
			Log.softAssertThat(shoppingBag.elementLayer.verifyVerticalAllignmentOfElements(driver, "emptyCartMsg", "btnEmptyWhatsNew", shoppingBag), 
					"Below 'Your Shopping bag is empty' message 'Shop What's New' button should be shown", 
					"Below 'Your Shopping bag is empty' message 'Shop What's New' button is shown", 
					"Below 'Your Shopping bag is empty' message 'Shop What's New' button not shown", driver);
			
			PlpPage plpPage = shoppingBag.clickShopNewArrivalButton();
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage), 
					"Page should be redirected to What's New PLP page.", 
					"Page redirected to What's New PLP page.", 
					"Page not redirected to What's New PLP page.", driver);
					
			// Step 3: Product listing page
			
			//Verify Horizontal refinement bar
			
			Log.softAssertThat(plpPage.refinements.elementLayer.verifyElementDisplayed(Arrays.asList("drpFilterByCollapsed","drpSortByCollapseMobileTablet"), plpPage.refinements) , 
					"'Filter By' menu and 'Sort By' menu should be displayed in Horizontal refinement bar", 
					"'Filter By' menu and 'Sort By' menu is displayed in Horizontal refinement bar", 
					"'Filter By' menu and 'Sort By' menu not displayed in Horizontal refinement bar", driver);
			
			plpPage.refinements.clickFilterArrowDown();		
			Log.message(i++ + ". Clicked on the downward arrow in Filter By menu", driver);
			
			Log.softAssertThat(plpPage.refinements.getFilterByState().equals("expanded") && 
					plpPage.refinements.elementLayer.VerifyPageListElementDisplayed(Arrays.asList("lstRefinementFilterOptionsMobile"), plpPage.refinements),
					"The 'Filter By' menu should be opened and Refinements should be displayed!",
					"The 'Filter By' menu opened and Refinements displayed!",
					"The 'Filter By' menu not working as Expected", driver);
			
			int refinementWidth= plpPage.refinements.elementLayer.getElementWidth("refinementSection", plpPage.refinements);
			
			int refinementFilterDropDownWidth= plpPage.refinements.elementLayer.getElementWidth("filterExpandedDropdown", plpPage.refinements);
			
			Log.softAssertThat(refinementFilterDropDownWidth == refinementWidth,
					"Drop down width should be 100% that of the viewport", 
					"Drop down width is 100% that of the viewport",
					"Drop down width is not 100% that of the viewport", driver);
			
			plpPage.refinements.openCloseFilterBy("collapsed");
			
			int totalCount = plpPage.getSearchResultCount();
			
			plpPage.refinements.clickFilterArrowDown();
			
			List<String> refinementList = plpPage.getRefinementList();
			
			if(refinementList.size() > 1) {
				refinementList = plpPage.getRefinementList();
				
				plpPage.refinements.selectSubRefinementInFilterBy(refinementList.get(refinementList.size()-1), 1);
				Log.message(i++ + ". Selected "+refinementList.get(refinementList.size()-1)+" option in filter flyout", driver);
				
				if(!Utils.isDesktop()) {
					plpPage.refinements.clickOnBackButtonOnMobileAndTab();
					Log.message(i++ + ". Clicked on back button in filter flyout", driver);
				}
				
				refinementList = plpPage.getRefinementList();
				
				plpPage.refinements.selectSubRefinementInFilterBy(refinementList.get(0), 1);
				Log.message(i++ + ". Selected "+refinementList.get(0)+" option in filter flyout", driver);
				
			} else {
				plpPage.refinements.selectSubRefinementInFilterBy(refinementList.get(0), 1);
				Log.message(i++ + ". Selected 1st "+refinementList.get(0)+" in filter flyout", driver);
			}
			
			if(!Utils.isDesktop()) {
				plpPage.refinements.clickOnBackButtonOnMobileAndTab();
				Log.message(i++ + ". Clicked on back button in filter flyout", driver);
			}
			
			plpPage.refinements.clickapplyButtonFromFilterInMobile();	
			Log.message(i++ + ". Clicked on view result button", driver);
			
			// to do -  The system displays the products on PLP based on the filter by option.
				
			plpPage.refinements.clickFilterArrowDown();
			Log.message(i++ + ". Clicked on the 'Filter' drop down arrow button", driver);
						
			plpPage.refinements.clickOnClearAllInRefinement();			
			Log.message(i++ + ". Clicked on the 'Clear All' button", driver);
			
			plpPage.refinements.clickFilterArrowDown();
			Log.message(i++ + ". Clicked on the 'Filter' drop down arrow button", driver);
			
			Log.softAssertThat(plpPage.refinements.getAppliedFilterCount()==0, 
					"The system should clear all the filter option", 
					"The system cleared all the filter option", 
					"The system not cleared all the filter option", driver);
			
			Log.softAssertThat(plpPage.refinements.getFilterByState().equals("collapsed"),
					"The system should close the menu.",
					"The system closed the menu.",
					"The system not closed the menu.", driver);
			
			Log.softAssertThat(plpPage.getSearchResultCount()==totalCount, 
					"All the product should display on PLP", 
					"All the product displayed on PLP", 
					"All the product not displayed on PLP", driver);
			
			plpPage.refinements.openCloseSortBy("expanded");			
			Log.message(i++ + ". Clicked on the sort by down arrow", driver);
			
			Log.softAssertThat(plpPage.verifySortByOptions(),
					"Sort by options are properly displayed on 'Sort By' drop-down box",
					"Sort by options are properly displayed on 'Sort By' drop-down box",
					"Sort by options are properly displayed on 'Sort By' drop-down box", driver);
			
			plpPage.refinements.selectSortBy("highest price");
			
			String prdId = plpPage.getOriginalPriceProductIdByIndex(1);
			
			PdpPage pdpPage=plpPage.clickAnOriginalPriceProduct(1);			
			Log.message(i++ + ". Clicked on Regular priced product", driver);
						
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("pdpMain"), pdpPage), 
					"Should be Navigate to PDP page.", 
					"Navigated to PDP page.", 
					"Not navigated to PDP page.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "breadcrumbElement", "backArrowIcon", pdpPage), 
					"< sign should be displayed before the category name", 
					"< sign is displayed before the category name", 
					"< sign not displayed before the category name", driver);
			
			homePage.headers.clickBackArrowOnBreadCrumbMobile();			
			Log.message(i++ + ". Clicked on back arrow '<' sign", driver);
						
			pdpPage=plpPage.navigateToPdp(prdId);			
			Log.message(i++ + ". Navigated to same PDP page", driver);
			
			if(pdpPage.getNoOfAlternateImage() > 1) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "mainProdImageActive_Mobile", "btnSlickDotSection", pdpPage), 
						"Below the product image dot should be displayed for the alternate images", 
						"Below the product image dot displayed for the alternate images", 
						"Below the product image dot not displayed for the alternate images", driver);
				
				pdpPage.clickOnAltImageSlickDot(2);			
				Log.message(i++ + "clicked on alternate dot below the product image", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("mainProdImageActive_Mobile"), pdpPage), 
						"When clicking on any dot below the product image appropriate image should display on the main image area.", 
						"When clicking on any dot below the product image appropriate image displayed on the main image area.", 
						"When clicking on any dot below the product image appropriate image not displayed on the main image area.", driver);
			} else {
				Log.reference("Alternate images are not available for this product, hence alternate image functionalities can't verified");
			}
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divShippingInfo", "tabReviews", pdpPage), 
					"Details and Review section should be displayed below the shipping & return section", 
					"Details and Review section displayed below the shipping & return section", 
					"Details and Review section not displayed below the shipping & return section", driver);
			
			pdpPage.addToBagCloseOverlay();		
			Log.message(i++ + ". Product added to bag and clicked on 'continue shopping' button from Add to bag overly", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("pdpMain"), pdpPage), 
					"The system should navigates the user to the previous page", 
					"The system navigated the user to the previous page", 
					"The system not navigated the user to the previous page", driver);
			
			if(BrandUtils.isBrand(Brand.bh)) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver,"tabReviews" ,"sectionRecommendation_BH", pdpPage), 
						"Recommendation section should be displayed below the social icons.", 
						"Recommendation section displayed below the social icons.", 
						"Recommendation section not displayed below the social icons.", driver);
				
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver,"sectionRecommendationMobileTablet", "tabReviews", pdpPage), 
						"Recommendation section should be displayed below the social icons.", 
						"Recommendation section displayed below the social icons.", 
						"Recommendation section not displayed below the social icons.", driver);
			}
			
			pdpPage.clickInStockRecommendationPrdImage();
			Log.message(i++ + ". Clicked on a product from the Recommendation section.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("pdpMain"), pdpPage), 
					"Should be Navigate to PDP page.", 
					"Navigated to PDP page.", 
					"Not navigated to PDP page.", driver);
			
			pdpPage.selectAllSwatches();
			Log.message(i++ + ". Selected a size & color from the product", driver);
			
			pdpPage.clickAddProductToBag();			
			Log.message(i++ + ". Clicked on Add to bag button", driver);
			
			ShoppingBagPage cartPage;
			if(pdpPage.elementLayer.isElementsDisplayed(Arrays.asList("mdlMiniCartOverLay"), pdpPage)) {
				cartPage = pdpPage.clickOnCheckoutInMCOverlay();
				Log.message(i++ + ". Clicked on Checkout Now button from Add to Bag overlay", driver);
			} else {
				Log.ticket("SC-5885");
				cartPage = homePage.headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to cart page", driver);
			}
			
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProductNameDesktop", "txtproductId", cartPage), 
					"Product ID should be positioned below the Item name", 
					"Product ID is positioned below the Item name", 
					"Product ID not positioned below the Item name", driver);
			
			String[] productIdCount = cartPage.getListOfRecommendationProductId(2);
			
			Log.softAssertThat(productIdCount.length==2, 
					"Product ID should be displayed for both products.", 
					"Product ID is displayed for both products.", 
					"Product ID not displayed for both products.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productAvailability", "productUnitPrice", cartPage), 
					"Item Price should be displayed below the Availability Messaging", 
					"Item Price is displayed below the Availability Messaging", 
					"Item Price not displayed below the Availability Messaging", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "lnkCheckoutPaypal", "sectionRecommendation", cartPage), 
					"Recommendation section should be displayed below PayPal Express button.", 
					"Recommendation section is displayed below PayPal Express button.", 
					"Recommendation section not displayed below PayPal Express button.", driver);
			
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			
			checkoutPage.enterGuestUserEmail(userMail);
			checkoutPage.clickOnContinueInGuestLogin();
			Log.message(i++ + ". Clicked on Continue button from Guest login", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "brandLogo", "stickyNavigationBar", checkoutPage)
							&& checkoutPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "headerHelpSection", "brandLogo", checkoutPage), 
					"The logo should be positioned to the top left side in the header", 
					"The logo is positioned to the top left side in the header", 
					"The logo not positioned to the top left side in the header", driver);
			
			checkoutPage.expandCollapseNeedHelp("open");
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("cntNeedHelp"), checkoutPage)
							&& checkoutPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "brandLogo", "cntNeedHelp", checkoutPage), 
					"The Help Drawer should push down the rest of the content and display its contents below the brand logo", 
					"The Help Drawer push down the rest of the content and display its contents below the brand logo", 
					"The Help Drawer not push down the rest of the content and display its contents below the brand logo", driver);
			
			checkoutPage.scrollToItemsListSection();
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("cntNeedHelp"), checkoutPage), 
					"When User scrolls down, Expanded header should remains sticky as a part of the sticky header", 
					"When User scrolls down, Expanded header remains sticky as a part of the sticky header", 
					"When User scrolls down, Expanded header not remains sticky as a part of the sticky header", driver);
			
			checkoutPage.fillingShippingDetailsAsGuest("No", checkoutShipAdd, ShippingMethod.Standard);
			Log.message(i++ +". Filled Shipping address and deselected \"Use this as a billing address\" checkbox",driver);
			
			checkoutPage.checkUncheckUseThisAsBillingAddress(false);
			
			checkoutPage.continueToPaymentByChoosingOriginalAddress(true);
			Log.message(i++ +". Clicked continue button", driver);
			
			//Step-8 Navigate to Checkout page - step 2
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("billingAddressFields"),checkoutPage),
					"The system should display the billing address form page",
					"The system should display the billing address form page",
					"The system should display the billing address form page",driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("headerCountChkoutStep2", "headerTextChkoutStep2"), checkoutPage), 
					"For an active step, Section number and the section name should be displayed ", 
					"For an active step, Section number and the section name is displayed ", 
					"For an active step, Section number and the section name not displayed ", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("headerCountChkoutStep1"), checkoutPage)
							&& !(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("headerTextChkoutStep1"), checkoutPage)), 
					"For an inactive step, Section number alone should be displayed and the section name should disappear ", 
					"For an inactive step, Section number alone is displayed and the section name is disappear ", 
					"For an inactive step, Section number alone not displayed and the section name not disappear ", driver);
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("headerInactiveChkoutStep3"), checkoutPage)
							&& !(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("headerInactiveSubmittedChkoutStep3"), checkoutPage)), 
					"For the sections that are not yet visited by the User, Section number will appear but will be displayed only in outline", 
					"For the sections that are not yet visited by the User, Section number appeared but displayed only in outline", 
					"For the sections that are not yet visited by the User, Section number not appeared but not displayed only in outline", driver);
			
			checkoutPage.fillingBillingDetailsAsGuest(checkoutBillpAdd);
			Log.message(i++ +". Entered biling address", driver);
			
			checkoutPage.continueToPayment();
			Log.message(i++ +". Navigated to payment details section", driver);
			
			checkoutPage.fillingCardDetails1(payment, false, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPage.applyRewardCertificate(reward);
			Log.message(i++ + ". Applied Reward Certificate", driver);
			
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			Log.softAssertThat(checkoutPage.comparePaymentBeforeOrderWithEnteredPaymentMethod(payment), 
					"The system should keep the payment details", 
					"Payment method is saved and displayed", 
					"Payment method is not saved correctly", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			//step 9 - Navigate to Checkout page - step 3
			HashSet<String> ProductId = checkoutPage.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPage.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 10 - Order confirmation page
			Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			Log.softAssertThat(receipt.checkEnteredShippingAddressReflectedInOrderReceipt(checkoutShipAdd), 
					"Same shipping address should display in the receipt which is entered in checkout shipping detail", 
					"Same shipping address is displaying", 
					"Different shipping address is displaying", driver);
			
			Log.softAssertThat(receipt.checkEnteredBillingAddressReflectedInOrderReceipt(checkoutBillpAdd), 
					"Same billing address should display in the receipt which is entered in checkout billing detail", 
					"Same billing address is displaying", 
					"Different billing address is displaying", driver);
			
			Log.softAssertThat(receipt.comparePaymentAfterOrderWithEnteredPaymentMethod(payment), 
					"Same payment method should display in the receipt which is entered in checkout payment section", 
					"Same payment method is displaying", 
					"Different payment method is displaying", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblProfileEmail", userMail, receipt), 
					"The mail ID entered should reflected in receipt", 
					"The mail ID entered is reflected in receipt", 
					"The mail ID entered is not reflected in receipt", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblOrderReceipt", userMail, receipt), 
					"The order receipt should send to the entered mail ID", 
					"The order receipt is send to the entered mail ID", 
					"The order receipt is not send to the entered mail ID", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt), 
					"The user can able to take print out of the order receipt", 
					"The user can able to print the order receipt", 
					"The user cannot able to print the order receipt", driver);
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24022