package com.fbb.testscripts.e2e;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.ReturnOrderPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.pages.account.OrderHistoryPage.OrderAttribute;
import com.fbb.pages.customerservice.ReturnAndExchangePage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24549 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");

	@Test( groups = { "checkout", "high", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24549(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String returnStatus = demandwareData.get("ReturnOrderStatus");
		String credentialReturn = accountData.get("credentialReturnBH");
		String emailIDReturn = credentialReturn.split("\\|")[0];
		String passwordReturn = credentialReturn.split("\\|")[1];
		String errorMessageColor = TestData.get("orderHistoryError_color");
		String orderNumberINTL = TestData.get("orderDetailsINTL").split("\\|")[0];
		String orderEmailINTL = TestData.get("orderDetailsINTL").split("\\|")[1];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Navigated to SignIn Page.", driver);
			
			MyAccountPage accountPage = signIn.navigateToMyAccount(emailIDReturn, passwordReturn);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
			
			OrderHistoryPage orderHistory = accountPage.clickOnOrderHistoryLink();
			Log.message(i++ + ". Navigated to Order History Page.", driver);
			
			if (orderHistory.getOrderNumersList().size() > 0) {
				String orderNumber = orderHistory.getOrderNumersList().get(0);
				
				Log.softAssertThat(orderHistory.getOrderNumersList().size() > 0, 
						"The system should display all the orders number that being placed under this account", 
						"The system is displayed all the orders number that being placed under this account", 
						"The system is not displayed all the orders number that being placed under this account", driver);
				
				if (orderHistory.getReturnableOrderNumersList().size() > 0) {
					String returnableOrderNumber = orderHistory.getReturnableOrderNumersList().get(0);
					
					if(!Utils.isMobile()) {
						Log.softAssertThat(orderHistory.getOrderAttributeFromSummary(returnableOrderNumber, OrderAttribute.Status).equalsIgnoreCase(returnStatus), 
								"The returned order status should be 'Processed'", 
								"The returned order status is 'Processed'", 
								"The returned order status is not 'Processed'", driver);
						
						orderHistory.clickOnReturnStatusToolTip(returnableOrderNumber);
						Log.message(i++ + ". Clicked on Return order status tooltip", driver);
						
						Log.softAssertThat(orderHistory.elementLayer.verifyTextContains("returnOrderStatusToolTipPopUp", returnStatus, orderHistory), 
								"The tooltip popup window should open & the system should display the order status information.", 
								"The tooltip popup window is opened & the system should display the order status information.", 
								"The tooltip popup window is not opened & the system should display the order status information.", driver);
						
						orderHistory.clickOnCloseReturnStatusToolTip();
						Log.message(i++ + ". Clicked on Close icon in Return order status tooltip", driver);
						
						Log.softAssertThat(!orderHistory.elementLayer.verifyTextContains("returnOrderStatusToolTipPopUp", returnStatus, orderHistory), 
								"The system should close the tooltip.", 
								"The system is closed the tooltip.", 
								"The system is not closed the tooltip.", driver);
					}
					orderNumber = returnableOrderNumber;
				} else {
					Log.warning("No retaurnable order found.");
				}
				
				orderHistory.clickOrderByNumber(orderNumber);
				Log.message(i++ + ". Clicked on View Details link for:: " + orderNumber, driver);
				
				Log.softAssertThat(orderHistory.elementLayer.verifyElementDisplayed(Arrays.asList("divOrderNumberPrint"), orderHistory), 
						"Order Number should display", 
						"Order Number is displayed", 
						"Order Number not displayed", driver);
				
				Log.softAssertThat(orderHistory.elementLayer.verifyTextContains("orderNumberValue", orderNumber, orderHistory), 
						"Confirm the order number, should be the same order number ( as provided in Step 2)", 
						"Confirm the order number is the same order number ( as provided in Step 2)", 
						"Confirm the order number is not the same order number ( as provided in Step 2)", driver);
				
				Log.softAssertThat(orderHistory.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divShipment", "divShipmentHeading"), orderHistory), 
						"The item should display under the shipment", 
						"The item is displayed under the shipment", 
						"The item is not displayed under the shipment", driver);
				
				String trackPackage = Utils.isMobile() ? "lnkTrackMyPackageMobile" : "lnkTrackMyPackage";
				Log.softAssertThat(orderHistory.elementLayer.verifyElementDisplayed(Arrays.asList(trackPackage), orderHistory), 
						"Each shipment should display a 'track my package' link", 
						"Each shipment is displayed a 'track my package' link", 
						"Each shipment is not displayed a 'track my package' link", driver);
				
				orderHistory.clickOnTrackMyPackage();
				Log.message(i++ + ". Clicked on Track My Package link", driver);
				
				//Step 4: The system navigates the user to Order return page
				
				ReturnOrderPage returnPage = orderHistory.clickOnReturn();
				Log.message(i++ + ". Clicked on Return button", driver);
					
				Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), returnPage), 
						"Return page should display", 
						"Return page is displayed", 
						"Return page is not displayed", driver);
				
				if(Utils.isMobile()) {
					Log.softAssertThat(returnPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "checkboxReturn1", "returnProductName", returnPage), 
							"Checkbox should be displayed in the left side of the Item Name", 
							"Checkbox is displayed in the left side of the Item Name", 
							"Checkbox is not displayed in the left side of the Item Name", driver);
					
					Log.softAssertThat(returnPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "returnProductImage", "returnProductDetails", returnPage), 
							"Product details should be displayed right of Item Image", 
							"Product details is displayed right of Item Image", 
							"Product details is not displayed right of Item Image", driver);
					
					Log.softAssertThat(returnPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "btnReturnSelectedItems", "btnCancel", returnPage), 
							"Cancel link should be displayed below the Return Selected Items button.", 
							"Cancel link is displayed below the Return Selected Items button.", 
							"Cancel link is not displayed below the Return Selected Items button.", driver);
				}
				
				returnPage.clickReturnSelectedItemButton();
				Log.message(i++ + ". Clicked on Return Selected Items button", driver);
				
				Log.softAssertThat(returnPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnReturnSelectedItems", "lblErorMessage", returnPage), 
						"The system should display an error message below the Return selected items button.", 
						"The system should display an error message below the Return selected items button.", 
						"The system should display an error message below the Return selected items button.", driver);
				
				returnPage.clickCheckboxBasedOnIndex(0);
				Log.message(i++ + ". Clicked checkbox for return items", driver);
				
				Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("returnQty", "returnReason"), returnPage), 
						"Return QTY & return reason drop-down should display", 
						"Return QTY & return reason drop-down is displayed", 
						"Return QTY & return reason drop-down is not displayed", driver);
				
				returnPage.clickReturnSelectedItemButton();
				Log.message(i++ + ". Clicked on Return Selected Items button", driver);
				
				Log.softAssertThat(returnPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnReturnSelectedItems", "lblErorMessage", returnPage), 
						"The system should display an error message below the Return selected items button.", 
						"The system should display an error message below the Return selected items button.", 
						"The system should display an error message below the Return selected items button.", driver);
				
				returnPage.clickCheckboxBasedOnIndex(0);
				Log.message(i++ + ". Selected checkbox of return item", driver);
				
				returnPage.slectReturnQtyBasedOnProductIndexAndQtyIndex(0, 0);
				Log.message(i++ + ". Selected Return Qty", driver);
				
				returnPage.slectReturnReasonBasedOnProductIndexAndReasonIndex(0, 4);
				Log.message(i++ + ". Selected Return Reason", driver);
				
				returnPage.clickReturnSelectedItemButton();
				Log.message(i++ + ". Clicked on Return Selected Items button", driver);
				
				Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("divDownloadReturnLablePage"), returnPage), 
						"The system should navigate the user to download a return label page", 
						"The system is navigated the user to download a return label page", 
						"The system is not navigated the user to download a return label page", driver);
				
				returnPage.clickOnDownloadReturnLable();
				Log.message(i++ + ". Clicked on Download Return lable button", driver);
			} else {
				Log.failsoft("There is no order found to perform verification.", driver);
			}  
			
			// Step 6 - Verify the International orders section.
			accountPage = homePage.headers.navigateToMyAccount();
			Log.message(i++ + ". Navigated to my account page", driver);
			
			orderHistory = accountPage.clickOnOrderHistoryLink();
			Log.message(i++ + ". Navigated to Order history", driver);
			
			if(orderHistory.elementLayer.verifyElementDisplayed(Arrays.asList("divInternationalSection"), orderHistory)) {
				orderHistory.clickOnSubmitINTL();
				Log.message(i++ + ". click on the 'submit' button without any information", driver);
				
				Log.softAssertThat(orderHistory.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("errorOrderNumberINTL", "errorOrderEmailINTL"), "color", errorMessageColor, orderHistory), 
						"Placeholder for the Order number & order email should become red", 
						"Placeholder for the Order number & order email become red", 
						"Placeholder for the Order number & order email not in red", driver);
				
				orderHistory.findOrderINTL(orderNumberINTL, orderEmailINTL);
				Log.message(i++ + ". Navigated to order tracking page", driver);
				
				Log.softAssertThat(orderHistory.elementLayer.verifyElementDisplayed(Arrays.asList("orderDetailsPage"), orderHistory), 
						"The system should redirect the user to the order tracking page.", 
						"The system is redirected the user to the order tracking page.", 
						"The system is not redirected the user to the order tracking page.", driver);
				
				if(orderHistory.elementLayer.verifyElementTextEqualTo("statusShipmentINTL", "Shipped", orderHistory)) {
					Log.softAssertThat(orderHistory.elementLayer.verifyElementDisplayed(Arrays.asList("txtReturnItem"), orderHistory), 
							"The system should be display a Return button.", 
							"The system is displayed a Return button.", 
							"The system is not displayed a Return button.", driver);
					
					ReturnAndExchangePage returnAndExchangePage = orderHistory.clickOnReturnINTL();
					Log.message(i++ + ". Clicked on Return link", driver);
					
					Log.softAssertThat(returnAndExchangePage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), returnAndExchangePage), 
							"The system should redirect the user to the 'customer service - returnexchange' page.", 
							"The system is redirected the user to the 'customer service - returnexchange' page.", 
							"The system is not redirected the user to the 'customer service - returnexchange' page.", driver);
					
					String trackLink;
					if(Utils.isMobile()) {
						trackLink = "lnkTrackMyPackageMobile";
					} else {
						trackLink = "lnkTrackMyPackage";
					}
					Log.softAssertThat(orderHistory.elementLayer.verifyElementDisplayed(Arrays.asList(trackLink), orderHistory), 
							"The system should be display a tracking link.", 
							"The system is displayed a tracking link.", 
							"The system is not displayed a tracking link.", driver);
					
					boolean navigatedToTrackPage = orderHistory.clickOnTrackMyPackage();
					Log.message(i++ + ". Clicked on Tracking link", driver);
					
					Log.softAssertThat(navigatedToTrackPage, 
							"The system should redirect the user to i-parcel tracking page.", 
							"The system is redirected the user to i-parcel tracking page.", 
							"The system is not redirected the user to i-parcel tracking page.", driver);
				} else {
					Log.reference("Shipment is not shipped yet, hence can't verify the returns", driver);
				}
			} else {
				Log.failsoft("International orders section is not available.", driver);
			}
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_HEADER_C22548

}// search
