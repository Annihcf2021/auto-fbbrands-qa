package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24276 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24276(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{

			//Step 1 - Navigate to the website
			
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String paymentMethod = "card_Visa";
			String level1Cat = TestData.get("level-1");
			String checkoutAdd = "valid_address1";
			String addressDetails = checkoutData.get(checkoutAdd);
			String checkoutAdd2 = "taxless_address";
			String prdSurcharge = TestData.get("prd_surcharge");
			
			{
				GlobalNavigation.registerNewUserAddress(driver, checkoutAdd +"|"+ checkoutAdd2, credential);
				GlobalNavigation.addNewPaymentToAccount(driver, paymentMethod, true, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Mouse hover on category name from any category header
			//step 3 - Navigate to Product listing page
			
			PlpPage plpPage = homePage.headers.navigateTo(level1Cat);
			Log.message(i++ +". Navigated to PLP", driver);			
			//Step 4 - Navigate to Product detail page ( Select a Surcharge item)
			
			PdpPage pdpPage = plpPage.navigateToPdp(1);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage), 
					"PDP page should be displayed", 
					"PDP page is getting displayed", 
					"PDP page is not getting displayed", driver);
			
			pdpPage = homePage.headers.navigateToPDP(prdSurcharge);
			Log.message(i++ + ". Navigated To PDP page : " + pdpPage.getProductName(), driver);
			
			if(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProdFeatureMsg"), pdpPage)) {
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProdFeatureMsg"), pdpPage), 
						"Product feature message should get display if it is available in PDP", 
						"Product feature message is getting display if it is available in PDP", 
						"Product feature message is not getting display if it is available in PDP", driver);
			}
			
			Log.softAssertThat(pdpPage.elementLayer.verifyTextContains("txtProdInfos", "Additional Shipping & Handling Charges", pdpPage), 
					"Product info should have 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", 
					"Product info is having 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", 
					"Product info is not having 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", driver);
			
			//Step 5 - Navigate to the cart page
				
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "inventoryState", "btnAddToCart", pdpPage), 
					"The Inventory state should display above the 'Add to Bag' button", 
					"The Inventory state is displaying above the 'Add to Bag' button", 
					"The Inventory state is not displaying above the 'Add to Bag' button", driver);
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated To Shopping Bag page", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("miniCartContent"), cartPage), 
					"Cart page should get display", 
					"Cart page is getting display", 
					"Cart page is not getting display", driver);
			
			String[] nam = {prdSurcharge};
			
			Log.softAssertThat(cartPage.verifyProductsAddedToCart(nam), 
					"Product should added in the cart page", 
					"Product is added in the cart page", 
					"Product is not added in the cart page", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyTextContains("txtSplMessage", "Additional Shipping", cartPage), 
					"Product info should have 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", 
					"Product info is having 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", 
					"Product info is not having 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", driver);
			
			cartPage.openShippingOverlayToolTip();
			Log.message(i++ + ". Shopping Overlay tool tip opened", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtSurchargeProductRate"), cartPage), 
					"Surcharge should get display in tooltip", 
					"Surcharge is getting display in tooltip", 
					"Surcharge is not getting display in tooltip", driver);
			
			cartPage.closeShippingOverlayToolTip();
			Log.message(i++ + ". Shopping Overlay tool tip opened", driver);
			
			SignIn signin = (SignIn) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			//step 6 - Navigate to the checkout sign in page
			
			CheckoutPage checkoutPg = (CheckoutPage)signin.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);
			
			//Step 7 - Navigate to Checkout page - step 1
			
			checkoutPg.continueToShipping(credential);
			Log.message(i++ + ". Navigated to Shipping section", driver);
			String addressSelectedOnShipping = checkoutPg.getselectedShippingAddress();
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("savedAddressCount"), checkoutPg) && 
					checkoutPg.elementLayer.verifyElementTextContains("savedAddressCount", "2", checkoutPg), 
					"Saved address count should be displayed", 
					"Saved address count is correctly displayed", 
					"Saved address count is not displaying correctly", driver);

			Log.softAssertThat(checkoutPg.elementLayer.verifyTwoAddressMatch(addressDetails, addressSelectedOnShipping), 
					"Shipping address (State, city & Zip code ) should display correctly. it should match previous steps.", 
					"Shipping address (State, city & Zip code ) is displaying correctly.", 
					"Shipping address (State, city & Zip code ) is not displaying correctly.", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
						
			//step 8 - Navigate to Checkout page - step 2
			checkoutPg.selectValueFromSavedBillingAddressesDropdownByIndex(1);
			Log.message(i++ +". Selected different NON-default address from billing address");
			
			checkoutPg.enterCVV("333");
			Log.message(i++ +". Entered CVV number ", driver);
								
			//Step 9 - Navigate to Checkout page - step 3
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			String elemProductInfo;
			if(Utils.isMobile()) {
				elemProductInfo = "txtProductInfoMobile";
			} else {
				elemProductInfo = "txtProductInfo";
			}
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyTextContains(elemProductInfo, "Additional Shipping & Handling Charges", checkoutPg), 
					"Product info should have 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", 
					"Product info is having 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", 
					"Product info is not having 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 10 - Order confirmation page
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24276
