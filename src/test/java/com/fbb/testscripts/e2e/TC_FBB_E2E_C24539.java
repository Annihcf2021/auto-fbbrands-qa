package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24539 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	
	@Test(groups = { "checkout", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24539(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			//Step 1 - Navigate to the website
			
			String userEMailId = AccountUtils.generateEmail(driver);
			String prdCallout = TestData.get("prd_promo-prd-level");
			String Coupon = TestData.get("cpn_off_10_percentage");
			String checkoutAdd = "taxless_address";
			String checkoutBillAdd = "valid_address8";
			String addressIncorrect = "incorrect_address1";
			String incorrectAddressFromProperty = checkoutProperty.getProperty(addressIncorrect);
			String level1 = TestData.get("level-1");
			String payment = "card_Visa";
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 3 - Navigate to Product listing page
			
			PlpPage plpPage = homePage.headers.navigateTo(level1);
			Log.message(i++ + ". Navigated to PLP page!", driver);
			
			String sortOption = plpPage.refinements.selectSortBy("Highest Price");
			Log.message(i++ + ". Sort Option(" + sortOption + ") selected from Sorting menu.", driver);
			
			Log.softAssertThat(plpPage.refinements.verifySortApplied(sortOption), 
					"Products should be sorted and displayed in product list page based on the sort by option", 
					"Products is sorted and displayed in product list page based on the sort by option", 
					"Products is not sorted and displayed in product list page based on the sort by option", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProductActualSalesPrice"), plpPage), 
					"Original price should displayed", 
					"Original price is displayed", 
					"Original price is not displayed", driver);
			
			//Step 4 - Navigate to Product detail page.
			
			String prdPrice = plpPage.getOriginalPriceProduct(1);
			
			PdpPage pdpPage = plpPage.clickAnOriginalPriceProduct(1);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementTextContains("lblPrice", prdPrice, pdpPage), 
					"Price should be same in pdp and plp page", 
					"Price is same in pdp and plp page", 
					"Price is not same in pdp and plp page", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("backArrowIcon"), pdpPage), 
					"Back arrow should be displayed", 
					"Back arrow is displayed", 
					"Back arrow is not displayed", driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart!", driver);
			
			plpPage = pdpPage.clickOnBreadCrumb(1);	
			Log.message(i++ + ". Clicked on breadcrumb!", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage), 
					"PLP page should be displayed", 
					"PLP page is displayed", 
					"PLP page is not displayed", driver);
			
			pdpPage = plpPage.navigateToPdp(1);
			Log.message(i++ + ". Clicked on product tile.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage), 
					"PDP page should be displayed", 
					"PDP page is getting displayed", 
					"PDP page is not getting displayed", driver);
			
			//Step 5 - Navigate to Product detail page.
			
			pdpPage = homePage.headers.navigateToPDP(prdCallout); 
			Log.message(i++ + ". Navigated to PDP page!", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtPromotionCalloutMsg"), pdpPage), 
					"Promotion callout message should be displayed", 
					"Promotion callout message is getting displayed", 
					"Promotion callout message is not getting displayed", driver);

			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productName", "lnkReviews_mobile", pdpPage), 
					"Product name should be displayed above product review", 
					"Product name is displayed above product review", 
					"Product name is not displayed above product review", driver);
			
			//Step 6 - Navigate the cart page
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart!", driver);
			
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			shoppingBagPg.enterCouponCode("rw");
			Log.message(i++ +". Entered \"rw\" in the promo code field", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("rwCouponErrorMobile"), shoppingBagPg), 
					"Reward error should be displayed", 
					"Reward error is getting displayed", 
					"Reward error is not getting displayed", driver);

			shoppingBagPg.applyPromoCouponCode(Coupon);
			Log.message(i++ + ". Promo code applied", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("txtCouponApplied"), shoppingBagPg), 
					"Coupon should get applied", 
					"Coupon is applied", 
					"Coupon is not applied", driver);
			
			shoppingBagPg.removeAllAppliedCoupons();
			Log.message(i++ + ". Coupon is removed!", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("txtCouponApplied"), shoppingBagPg), 
					"Coupon should get removed", 
					"Coupon is removed", 
					"Coupon is not removed", driver);
			
			shoppingBagPg.applyPromoCouponCode(Coupon);
			Log.message(i++ + ". Promo code re-applied", driver);
			
			//Step 7 - Navigate to the checkout sign in page
			
			SignIn signin = (SignIn) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			CheckoutPage checkoutPg = (CheckoutPage)signin.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);
			
			//Step 8 - Navigate to Checkout page - step 1
			
			checkoutPg.continueToShipping(userEMailId);
			Log.message(i++ + ". Navigated to Shipping section", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("lblGuestUserEmail"), checkoutPg), 
					"The system should accept the valid email address", 
					"The system accepts the valid email address", 
					"The system not accepts the valid email address", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("billingEditFirstname"), checkoutPg), 
					"The system should navigate the user to checkout page - Step 1", 
					"The system is navigate the user to checkout page - Step 1", 
					"The system is not navigate the user to checkout page - Step 1", driver);
			
			checkoutPg.fillingShippingDetailsAsGuest("NO", addressIncorrect, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address(invalid) entered successfully", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box checked", driver);
			
			checkoutPg.clickShippingContinue();
			Log.message(i++ + ". Click continue with invalid address.", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlAddressSuggestion"), checkoutPg), 
					"AVS Address modal should popup.",
					"AVS Address modal is displayed.",
					"AVS Address modal is not displayed for invalid address.", driver);
			
			if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlAddressSuggestion"), checkoutPg)) {
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("btnThatsMyRightAddress", "btnUpdateMyAddress"), checkoutPg), 
						"\"THAT'S THE RIGHT ADDRESS\" and \"UPDATE MY ADDRESS\" buttons should be displayed", 
						"Correct button are displayed", 
						"Correct button are not displayed", driver);
				
				checkoutPg.clickCorrectAddress();
				Log.message(i++ + ". Clicked on That's Correct Address.", driver);
			}
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("lblSavedAddressInBillingDetails"), checkoutPg),
					"User should be navigated to billing address.",
					"User is navigated to billing address",
					"User is not navigated to billing address", driver);
			
			String addressFromBillingPage = checkoutPg.getSavedAddressFromShippingDetails();
			Log.softAssertThat(checkoutPg.compareTwoAddress(incorrectAddressFromProperty, addressFromBillingPage),
					"Billing address should be same as the incorrect shipping address.",
					"Billing address is same as the incorrect shipping address.",
					"Billing address is not same as the incorrect shipping address.", driver);
			
			checkoutPg.EditShippingAddress();
			Log.message(i++ + ". Clicked edit shipping address.", driver);
			
			checkoutPg.fillingShippingDetailsAsGuest("NO", checkoutAdd, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(false);
			Log.message(i++ + ". Use this as a Billing address check box disabled", driver);
			
			checkoutPg.continueToPaymentByChoosingOriginalAddress(true);
			Log.message(i++ + ". Continued to Billing address", driver);
			
			//Step 9 - Navigate to Checkout page - step 2
			checkoutPg.clickOnSaveAddrBtn();
			Log.message(i++ + ". Clicked save billing address button.");
			
			Log.softAssertThat(!checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlAddressSuggestion"), checkoutPg), 
					"User should be able to navigate to payment method options without seeing any popup.", 
					"User is navigated to payment method options without seeing any popup.", 
					"User is not navigated to payment method options and AVS popus is displayed.", driver);
						
			addressFromBillingPage = checkoutPg.getSavedAddressFromShippingDetails();
			Log.softAssertThat(checkoutPg.compareTwoAddress(incorrectAddressFromProperty, addressFromBillingPage),
					"Billing address should still be populated with previously used incorrect shipping address.", 
					"Billing address is populated with previously used incorrect shipping address.", 
					"Billing address is not populated with previously used incorrect shipping address.", driver);
			
			checkoutPg.continueToPaymentFromBilling();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			Log.softAssertThat(!checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlAddressSuggestion"), checkoutPg), 
					"User should be able to navigate to payment method options without seeing any popup.", 
					"User is navigated to payment method options without seeing any popup.", 
					"User is not navigated to payment method options and AVS popus is displayed.", driver);
			
			checkoutPg.clickEditAddressInBillingSection();
			Log.message(i++ + ". Clicked edit billing address to enter valid address.", driver);
			
			checkoutPg.fillingBillingDetailsAsGuest(checkoutBillAdd);
			Log.message(i++ + ". Billing Address(valid) entered successfully", driver);
			
			checkoutPg.continueToPaymentFromBilling();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			checkoutPg.clickCVVToolTip();
			Log.message(i++ + ". Clicked on CVV tool tip", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("cvvToolTipContentClose"), checkoutPg), 
					"The system should display the popup window, with CVV information", 
					"Pop up window is displayed", 
					"Pop up window is not displayed", driver);
			
			checkoutPg.clickCloseOnCVVToolTip();
			Log.message(i++ + ". CVV tool tip closed", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("cvvToolTipContentClose"), checkoutPg), 
					"The system should close the tooltip popup window", 
					"Pop up window is not displayed", 
					"Pop up window is displayed", driver);
			
			checkoutPg.fillingCardDetails1(payment, false, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			Log.softAssertThat(checkoutPg.comparePaymentBeforeOrderWithEnteredPaymentMethod(payment), 
					"The system should keep the payment details", 
					"Payment method is saved and displayed", 
					"Payment method is not saved correctly", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			//Step 10 - Navigate to Checkout page - step 3
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 11 - Order confirmation page
			
			Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			Log.softAssertThat(receipt.checkEnteredShippingAddressReflectedInOrderReceipt(checkoutAdd), 
					"Same shipping address should display in the receipt which is entered in checkout shipping detail", 
					"Same shipping address is displaying", 
					"Different shipping address is displaying", driver);
						
			Log.softAssertThat(receipt.comparePaymentAfterOrderWithEnteredPaymentMethod(payment), 
					"Same payment method should display in the receipt which is entered in checkout payment section", 
					"Same payment method is displaying", 
					"Different payment method is displaying", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblProfileEmail", userEMailId, receipt), 
					"The mail ID entered should reflected in receipt", 
					"The mail ID entered is reflected in receipt", 
					"The mail ID entered is not reflected in receipt", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblOrderReceipt", userEMailId, receipt), 
					"The order receipt should send to the entered mail ID", 
					"The order receipt is send to the entered mail ID", 
					"The order receipt is not send to the entered mail ID", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt), 
					"The user can able to take print out of the order receipt", 
					"The user can able to print the order receipt", 
					"The user cannot able to print the order receipt", driver);
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24111
