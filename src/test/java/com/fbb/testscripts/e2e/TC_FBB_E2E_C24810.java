package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24810 extends BaseTest {

	EnvironmentPropertiesReader environmentPropertiesReader;

	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");

	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24810(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
						
		int i = 1;
		try {
			String userEMailIdADS = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountData.get("password_global");
			String credential = userEMailIdADS + "|" + password;
			String prdVariation = TestData.get("prd_variation");
			String cardType = "cards_2";

			// Always Approve Data
			String[] addressAlwaysApprove = checkoutData.get("plcc_always_approve_address").split("\\|");
			String firstName = addressAlwaysApprove[7];
			String lastName = addressAlwaysApprove[8];
			String alwaysApproveAddress = "plcc_always_approve_address";

			{
				GlobalNavigation.registerNewUserWithUserDetail(driver, 0, 0, firstName, lastName, credential);
				
				GlobalNavigation.addNewAddressToAccount(driver, alwaysApproveAddress, false, credential);

			}

			// Step1
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			// Step2
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(userEMailIdADS, password);
			Log.message(i++ + ". Navigated to My Account page as : " + userEMailIdADS, driver);

			Log.softAssertThat(myAcc.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), myAcc),
					"User should be successfully created", 
					"User is created", "User is not created", driver);

			// Step3
			AddressesPage addrPg = myAcc.navigateToAddressPage();
			Log.message(i++ + ". Navigated to Address page!", driver);

			Log.softAssertThat(addrPg.getSavedAddressesCount() > 0, 
					"Address should be successfully created",
					"Address is created", 
					"Address is not created", driver);

			// Step4
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation);
			Log.message(i++ + ". Navigated To PDP page : " + pdpPage.getProductName(), driver);

			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);

			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);

			CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);

			if (checkoutPg.elementLayer.verifyPageElements(Arrays.asList("modalCheckoutPlcc"), checkoutPg)) {
								
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("modalCheckoutPlcc"), checkoutPg),
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed",
						"PLCC modal is not getting displayed", driver);

				// Step5
				checkoutPg.clickNoThanksInPLCC();
				Log.message(i++ + ". Click on 'No Thanks' in PLCC!", driver);

				// Step6
				checkoutPg.clickOnBrandLogo();
				Log.message(i++ + ". Click on Brand logo!", driver);

				homePage.headers.signOut();
				Log.message(i++ + ". User signed out!", driver);

				homePage = BrowserActions.clearCookies(driver);
				Log.message(i++ + ". Cleared cookies and navigated to Homepage", driver);

				// Step7
				// Step8
				homePage = homePage.headers.navigateToHome();
				Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

				myAcc = homePage.headers.navigateToMyAccount(userEMailIdADS, password);
				Log.message(i++ + ". Navigated back to My Account page as : " + credential.split("\\|")[0], driver);

				// Step9
				pdpPage = homePage.headers.navigateToPDP(prdVariation);
				Log.message(i++ + ". Navigated To PDP page : " + pdpPage.getProductName(), driver);

				pdpPage.addToBagCloseOverlay();
				Log.message(i++ + ". Product added to cart", driver);

				shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping bag page", driver);

				checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
				Log.message(i++ + ". Navigated to Checkout page", driver);

				Log.softAssertThat(
						checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("modalCheckoutPlcc"), checkoutPg),
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed",
						"PLCC modal is not getting displayed", driver);

				// Step10
				checkoutPg.clickNoThanksInPLCC();
				Log.message(i++ + ". Click on 'No Thanks' in PLCC!", driver);

				checkoutPg.continueToPayment();

				Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "mdlPLCCRebuttal", "paymentmethodSection", checkoutPg),
						"The acquisition rebuttal modal should be displayed above the Add New Credit Card form.",
						"The acquisition rebuttal modal is displayed above the Add New Credit Card form.",
						"The acquisition rebuttal modal is not displayed above the Add New Credit Card form.", driver);
				
				// Step11
				checkoutPg.fillingCardDetails1(cardType, false);

				checkoutPg.clickOnPaymentDetailsContinueBtn();
				Log.message(i++ + ". Continued to Review & Place Order", driver);

				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("btnPlaceOrder"), checkoutPg),
						"Place order button should be displayed",
						"Place order button is getting displayed",
						"Place order button is not getting displayed", driver);
				
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}

				OrderConfirmationPage orderPage = checkoutPg.clickOnPlaceOrderButton();
				Log.message(i++ + ". Clicked place order button", driver);

				Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage),
						"user should taken to the order confirmation page.",
						"user is taken to the order confirmation page.",
						"user is not taken to the order confirmation page.", driver);
			} else {
				Log.message("<br>");
				Log.fail("Checkout step-1 overlay is not displayed");
				Log.message("<br>");
			}
			
			Log.testCaseResult();

		} // Ending try block
		catch (Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		} // Ending finally
	}
}
