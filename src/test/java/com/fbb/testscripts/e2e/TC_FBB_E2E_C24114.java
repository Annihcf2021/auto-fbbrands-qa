package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24114 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandware= EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24114(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{

			//Step 1 - Navigate to the website
			
			String searchText = TestData.get("product_search_terms").split("\\|")[0];
			String productID = TestData.get("prd_video");
			String prdPoBoxRes = TestData.get("prd_po-box-restricted");
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String checkoutPOBoxAdd = "valid_address3";
			String payment = "card_Visa";
			String poBoxMsg = demandware.get("PoBoxMsg");
			{
				GlobalNavigation.registerNewUser(driver, 0, 0, credential);
				GlobalNavigation.RemoveAllProducts(username, password, driver);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Mouse hover on the sign in link & login from the sign in flyout
			
			SignIn signIn = homePage.headers.navigateToSignInPage();
			
			signIn.clickSignInButton();
			Log.message(i++ + ". Click on sign in button!", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyElementDisplayed(Arrays.asList("errUsernameTxtBox", "errPasswordTxtBox"), signIn), 
					"System should not login for invalid username/password", 
					"System is not login for invalid username/password", 
					"System is login for invalid username/password", driver);
			
			signIn.typeOnEmail("abcd@abcd.com");
			Log.message(i++ + ". Give invalid username!", driver);
			
			signIn.typeOnPassword("abcd@123");
			Log.message(i++ + ". Give invalid password!", driver);
			
			signIn.clickSignInButton();
			Log.message(i++ + ". Click on sign in button!", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyElementDisplayed(Arrays.asList("errLoginErrorMsg"), signIn), 
					"System should not login for invalid username/password", 
					"System is not login for invalid username/password", 
					"System is login for invalid username/password", driver);
			
			signIn.navigateToMyAccount(username, password);
						
			//Step 3 - Verify category header
			if(Utils.isDesktop()) {
				Log.softAssertThat(homePage.headers.verifyRootCategoryHoverFlyout(), 
						"Category flyout should displayed and category should highlighted", 
						"Category flyout is displayed and category should highlighted", 
						"Category flyout is not displayed and category should highlighted", driver);
			}
			//Step 4 - Navigate to Product listing page
			
			homePage.headers.typeTextInSearchField(searchText);
			Log.message(i++ + ". Typed in the Search Field!", driver);
			
			Log.softAssertThat(homePage.headers.getEnteredTextFromSearchTextBox().trim().equals(searchText.trim()), 
					"Text should be entered in the Search text box", 
					"Text is entered in the Search text box", 
					"Text is not entered in the Search text box", driver);
			
			SearchResultPage searchResultPg = homePage.headers.searchProductKeyword(searchText);
			Log.message(i++ + ". Navigated to Search Result Page!", driver);

			Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayed(Arrays.asList("sectionSearchResult"), searchResultPg), 
					"Search result page should be displayed", 
					"Search result page is getting displayed", 
					"Search result page is not getting displayed", driver);
						
			PdpPage pdpPage = homePage.headers.navigateToPDP(productID);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			// Step 6a - Navigate to the cart page
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			//Step 5b - Navigate to Product detail page 
			
			pdpPage = homePage.headers.navigateToPDP(prdPoBoxRes);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			pdpPage.selectAllSwatches();
			Log.message(i++ + ". Selected product variations.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementTextContains("lblProductInfo", poBoxMsg, pdpPage), 
					"The product should have PO box excluded message", 
					"The product is having PO box excluded message", 
					"The product is not having PO box excluded message", driver);

			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Product Added to cart", driver);

			// Step 6b - Navigate to the cart page
			
			ShoppingBagPage shoppingBagPg = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
				
			int cartNoOfProducts=shoppingBagPg.getNoOfProductsInCart();
			
			CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			/*checkoutPg.clickAddNewAddress();
			Log.message(i++ + ". Clicked on add new address in shipping address", driver);*/
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("billingEditFirstname"), checkoutPg), 
					"The system should navigate the user to checkout page - Step 1", 
					"The system is navigate the user to checkout page - Step 1", 
					"The system is not navigate the user to checkout page - Step 1", driver);

			//Step 7 - Navigate to Checkout page - step 1
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("radioStandardDelivery", "radioExpressDelivery"), checkoutPg) ||
					checkoutPg.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("radioSuperFastDelivery", "radioNextDayDelivery"), checkoutPg), 
					"'Standard Delivery' and 'Express delivery' options only should get displayed", 
					"'Standard Delivery' and 'Express delivery' options are displayed", 
					"'Standard Delivery' and 'Express delivery' options are not displayed", driver);
			
			checkoutPg.enterAddress1("Po Box 1123");
			Log.message(i++ + ". Typed Po Box address on Address1", driver);
			
			int poBoxExcludedNoOfProducts = checkoutPg.getNoOfProductsInPoBoxExcludeOverlay();
			
			checkoutPg.clickRemoveItemsPOBoxOverlay();
			Log.message(i++ + ". Remove button clicked!", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyListElementSize("lstCartedItems", (cartNoOfProducts-poBoxExcludedNoOfProducts), checkoutPg), 
					"Po Box restricted product should be removed", 
					"Po Box restricted product is removed", 
					"Po Box restricted product is not removed", driver);
			
			checkoutPg.fillingShippingDetailsAsSignedInUser("no","yes", ShippingMethod.Standard, checkoutPOBoxAdd);
			
			// step 8 - Navigate to Checkout page - step 2
								
			checkoutPg.continueToPaymentByChoosingOriginalAddress(true);
			Log.message(i++ + ". Continued to Payment Page", driver);				
			
			Log.softAssertThat(checkoutPg.compareBillingAddressWithEnteredShippingAddress(checkoutPOBoxAdd), 
					"The system should keep the billing address same as shipping address", 
					"Billing address same as shipping address", 
					"Billing address not same as shipping address", driver);
							
			//Step 9 - Navigate to Checkout page - step 3
			
			checkoutPg.fillingCardDetails1(payment, true, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			Log.softAssertThat(checkoutPg.comparePaymentBeforeOrderWithEnteredPaymentMethod(payment), 
					"The system should keep the payment details", 
					"Payment method is saved and displayed", 
					"Payment method is not saved correctly", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
								
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
								
			//Step 10 - Order confirmation page
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Object obj = receipt.clickOnproductNameByIndex(0);
			Log.message(i++ + ". Clicked on product name link from order confirmation page", driver);
			
			Log.softAssertThat(obj.getClass().getName().contains("PdpPage"),
					"After clicking the product name link, User should redirects to the PDP page",
					"After clicking the product name link, User is redirected to the PDP page",
					"After clicking the product name link, User not redirected to the PDP page", driver);
			
			Log.testCaseResult();
		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // Ending finally
	} //M1_FBB_E2E_C24114
} //TC_FBB_E2E_C24114