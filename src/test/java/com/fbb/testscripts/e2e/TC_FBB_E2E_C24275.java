package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlatinumCardApplication;
import com.fbb.pages.PlatinumCardLandingPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24275 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24275(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{

			//Step 1 - Navigate to the website
			
			String searchText = TestData.get("prd_standard_slp_search").split("\\|")[0];
			String productID = TestData.get("prd_standard_slp_search").split("\\|")[1];
			String userEMailId = AccountUtils.generateEmail(driver);
			String checkoutAdd = "taxless_address";
			String addressDetails = checkoutData.get("plcc_address");
			String ssnDetails = checkoutData.get("SSNdetails");
			
			HashMap<String, String> userAddressM1 = GlobalNavigation.formatPLCCAddressToMapWithSSN(addressDetails, ssnDetails, driver, userEMailId);
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - For Desktop - Click on search field in the header  ,
			//For Tablet & mobile-Scroll down & confirm sticky header displaying & click on the search field from the sticky header
	
			homePage.headers.typeTextInSearchField(searchText);
			Log.message(i++ + ". Typed in the Search Field!", driver);

			//Step 3 - Navigate to search result page
			
			SearchResultPage searchResultPg = homePage.headers.searchProductKeyword(searchText);
			Log.message(i++ + ". Navigated to Search Result Page!", driver);
					
			Log.softAssertThat(!(searchResultPg.verifyColorSwatchDisplaysForAProduct(productID)), 
					"Color swatch should not display for standard product", 
					"Color swatch is not displaying for standard product", 
					"Color swatch is displaying for standard product", driver);
			
			Log.softAssertThat(searchResultPg.verifyProdBadgeImgInParticularProd(productID, "any"), 
					"Product badge should display for the product", 
					"Product badge is displaying for the product", 
					"Product badge is not displaying for the product", driver);
			
			//Step 4 -Navigate to Product detail page
			
			PdpPage pdpPage = searchResultPg.navigateToPdpByPrdID(productID);
			Log.message(i++ + ". Navigated to PDP!", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("selectedSize"), pdpPage), 
					"Size should pre-selected", 
					"Size is pre-selected", 
					"Size is not pre-selected", driver);
			
			if(pdpPage.getNoOfAlternateImage() > 1) {
				Log.softAssertThat(pdpPage.verifyNewProdImageLoaded(2),
						"To check the selected image is populated in place of main product image",
						"The selected image is populated in place of main product image.",
						"The selected image is not populated in place of main product image", driver);
			}
			
			if (!Utils.isMobile()) {
				if(pdpPage.getNoOfRecommendationProduct() > 5) {
					if(BrandUtils.isBrand(Brand.bh)){
						Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("nextArrowRecommendationBH","prevArrowDisableRecommendationBH"), pdpPage), 
								"Next and Prev arrow should get displayed if 5 or more products are displayed in recommendation section", 
								"Next and Prev arrow is getting displayed", 
								"Next and Prev arrow is not getting displayed", driver);
							
					} else if (Utils.isDesktop() && (!BrandUtils.isBrand(Brand.bh))) {
						Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("nextArrowRecommendationDesktop","prevArrowDisableRecommendationDesktop"), pdpPage), 
								"Next and Prev arrow should get displayed if 5 or more products are displayed in recommendation section", 
								"Next and Prev arrow is getting displayed", 
								"Next and Prev arrow is not getting displayed", driver);
					} else {
						Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("nextArrowRecommendationMobileTablet","prevArrowDisableRecommendationMobileTablet"), pdpPage), 
								"Next and Prev arrow should get displayed if 5 or more products are displayed in recommendation section", 
								"Next and Prev arrow is getting displayed", 
								"Next and Prev arrow is not getting displayed", driver);
					}
					String nextEnable = null;
					if(BrandUtils.isBrand(Brand.bh)) {
			    		nextEnable = "nextArrowRecommendationBH";
			    	} else {
			    		if (Utils.isDesktop()) {
			    			nextEnable = "nextArrowRecommendationDesktop";
			    		} else {
			    			nextEnable = "nextArrowRecommendationMobileTablet";
			    		}
			    	}
					
					while(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList(nextEnable), pdpPage)) {
						pdpPage.scrollRecommendationImageInSpecifiedDirection("Next");
						Log.message(i++ + ". Clicked next on recommendation.", driver);
					}
					
					if(BrandUtils.isBrand(Brand.bh)){
						Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("nextArrowDisableRecommendationBH","prevArrowRecommendationBH"), pdpPage), 
								"When clicking on next arrow new product should get displayed", 
								"When clicking on next arrow new product is getting displayed", 
								"When clicking on next arrow new product is not getting displayed", driver);
					} else if (Utils.isDesktop() && (!BrandUtils.isBrand(Brand.bh)) ) {
						Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("nextArrowDisableRecommendationDesktop","prevArrowRecommendationDesktop"), pdpPage), 
								"When clicking on next arrow new product should get displayed", 
								"When clicking on next arrow new product is getting displayed", 
								"When clicking on next arrow new product is not getting displayed", driver);
					} else {
						Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("nextArrowDisableRecommendationMobileTablet","prevArrowRecommendationMobileTablet"), pdpPage), 
								"When clicking on next arrow new product should get displayed", 
								"When clicking on next arrow new product is getting displayed", 
								"When clicking on next arrow new product is not getting displayed", driver);
					}
				}
			}
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Clicked on 'Add To Bag' Button", driver);
			
			//Step 5 - Navigate to the cart page

			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to cart page",driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("miniCartContent"), cartPage), 
					"Cart page should get display", 
					"Cart page is getting display", 
					"Cart page is not getting display", driver);
			
			String[] nam = {productID};
			
			Log.softAssertThat(cartPage.verifyProductsAddedToCart(nam), 
					"Product should added in the cart page", 
					"Product is added in the cart page", 
					"Product is not added in the cart page", driver);
			
			PlatinumCardLandingPage platinumCardLandPg = cartPage.navigateToPlatinumCardLandingPage();
			Log.message(i++ + ". Navigated to Platinum Card Landing page",driver);
			
			Log.softAssertThat(platinumCardLandPg.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), platinumCardLandPg), 
					"PlatinumCardLanding page should get display", 
					"PlatinumCardLanding page is getting display", 
					"PlatinumCardLanding page is not getting display", driver);
			
			PlatinumCardApplication plccApplication = platinumCardLandPg.clickOnApplyButton();
			Log.message(i++ + ". Navigated to Platinum Card Application page",driver);
			
			plccApplication.fillPLCCApplication(userAddressM1);
			Log.message(i++ + ". Filled PLCC application", driver);
			
			plccApplication.clickCheckUncheckConsent(true);
			Log.message(i++ + ". Checked concent.", driver);
			
			plccApplication.clickOnRegisterBtn();
			Log.message(i++ + ". Clicked on SUBMIT button.", driver);
			
			if(plccApplication.elementLayer.verifyElementDisplayed(Arrays.asList("divApprovedModal"), plccApplication)) {
				Log.softAssertThat(plccApplication.elementLayer.verifyElementDisplayed(Arrays.asList("divApprovedModal"), plccApplication), 
						"PlatinumCard approved modal should get display", 
						"PlatinumCard approved modal is getting display", 
						"PlatinumCard approved modal is not getting display", driver);
				
				plccApplication.clickApprovedModalContinueShopping();
				Log.message(i++ + ". Clicked on Continue Shopping button.", driver);
				
				Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
						"Home page should be displayed", 
						"Home page is getting displayed", 
						"Home page is not getting displayed", driver);
				
				cartPage = homePage.headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping Bag page",driver);
				
				//Step 6 -Navigate to the checkout sign in page
				
				SignIn signin = (SignIn) cartPage.navigateToCheckout();
				Log.message(i++ + ". Navigated to Checkout page", driver);
				
				CheckoutPage checkoutPg = (CheckoutPage)signin.clickSignInButton();
				Log.message(i++ + ". Clicked Signin button.", driver);
				
				//Step 7 - Navigate to Checkout page - step 1
				
				checkoutPg.continueToShipping(userEMailId);
				Log.message(i++ + ". Navigated to Shipping section", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("billingEditFirstname"), checkoutPg), 
						"The system should navigate the user to checkout page - Step 1", 
						"The system is navigate the user to checkout page - Step 1", 
						"The system is not navigate the user to checkout page - Step 1", driver);
				
				checkoutPg.fillingShippingDetailsAsGuest("NO", checkoutAdd, ShippingMethod.Standard);
				Log.message(i++ + ". Shipping Address entered successfully", driver);
				
				checkoutPg.checkUncheckUseThisAsBillingAddress(true);
				Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
				
				checkoutPg.continueToPayment();
				Log.message(i++ + ". Continued to Payment Page", driver);
				
				//Step 8 - Navigate to Checkout page - step 2
				
				Log.softAssertThat(checkoutPg.compareBillingAddressWithEnteredShippingAddress(checkoutAdd), 
						"The system should keep the billing address same as shipping address", 
						"Billing address same as shipping address", 
						"Billing address not same as shipping address", driver);
				
				if(Utils.isDesktop()) {
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementTextContains("drpCardType", "Credit Card", checkoutPg), 
							"PLCC card should be selected automatically when PLCC card approved", 
							"PLCC card is selected automatically when PLCC card approved", 
							"PLCC card is not selected automatically when PLCC card approved", driver);
				} else {
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementTextContains("drpCardTypePaymentSection", "PLCC", checkoutPg), 
							"PLCC card should be selected automatically when PLCC card approved", 
							"PLCC card is selected automatically when PLCC card approved", 
							"PLCC card is not selected automatically when PLCC card approved", driver);
				}
				
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
				
				boolean dummyDataSentByADS = checkoutPg.dummyDataRecieved();
				if(dummyDataSentByADS) {
					Log.reference("Dummy data has been sent by ADS. Order not possible with provided data.");
				} else {
					checkoutPg.clickOnPaymentDetailsContinueBtn();
					Log.message(i++ + ". Continued to Review & Place Order", driver);
					
					//Step 9 - Navigate to Checkout page - step 3
					String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
					
					checkoutPg.clickOnPlaceOrder();
					Log.message(i++ + ". Clicked on Place order button", driver);
					
					if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"),checkoutPg)) {
						Log.reference("By Using always approve card user can't place order", driver);				
					} else {
						Log.message(i++ + ". Order Placed successfully", driver);
						
						//Step 10 - Order confirmation page
						OrderConfirmationPage receipt = new OrderConfirmationPage(driver).get();
						String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
						
						Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
								"The order total should display correctly", 
								"The order total is displaying correctly", 
								"The order total is not displaying correctly", driver);
					}
				}
			} else {
				Log.warning("PLCC approved modal is ot displayed hence cant proceed further");
			}
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24275
