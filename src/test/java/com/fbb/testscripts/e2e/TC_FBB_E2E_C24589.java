package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24589 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	@Test(groups = { "plp", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24589(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			
			String searchText1 = TestData.get("prd_multiple_searches").split("\\|")[0];
			String searchText2 =TestData.get("prd_multiple_searches").split("\\|")[1];
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 1 - Enter a search term (ex: blue Dress) click on the Search icon
			
			SearchResultPage searchResultPg = homePage.headers.searchProductKeyword(searchText1);
			Log.message(i++ + ". Navigated to Search Result Page!", driver);
			
			Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("sectionSearchResult"), searchResultPg), 
					"Search result page should be displayed", 
					"Search result page is getting displayed", 
					"Search result page is not getting displayed", driver);
			
			BrowserActions.scrollToTopOfPage(driver);
			
			headers.clickOnSearchIcon();
			Log.message(i++ + ". Clicked on search icon.", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling((Arrays.asList("txtSearch")), headers), 
					"Primary search box should be displayed in header section", 
					"Primary search box is getting displayed in header section", 
					"Primary search box is not getting displayed in header section", driver);
			
			headers.clickOnSearchIcon();
			Log.message(i++ + ". Clicked on search icon to close search flyout.", driver);
			
			Log.softAssertThat(searchResultPg.getSearchResultCount() > 0, 
					"Search result page product count should displayed", 
					"Search result page product count is displayed", 
					"Search result page product count is not displayed", driver);
			
			Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayed(Arrays.asList("inpSecondrySearch", "lnkProdTile", "divRefinementFilter"), searchResultPg)
							&& (searchResultPg.elementLayer.verifyElementDisplayed(Arrays.asList("divRefinementSortBy"), searchResultPg) 
							|| searchResultPg.elementLayer.verifyElementDisplayed(Arrays.asList("divRefinementSortByTabMob"), searchResultPg)),
					"'Secondary Search box','Refinement Filter','Product Grid','Sort by option' should be displayed", 
					"'Secondary Search box','Refinement Filter','Product Grid','Sort by option' is getting displayed", 
					"'Secondary Search box','Refinement Filter','Product Grid','Sort by option' is not getting displayed", driver);
			
			Log.softAssertThat(headers.getEnteredTextFromSearchTextBox().trim().equals(searchText1.trim()), 
					"Text entered in the Search text box should display after searching", 
					"Text is displaying after searching in primary search box", 
					"Text is not displaying after searching in primary search box", driver);
			
			String[] prodId1 = searchResultPg.getListOfProductId(1);
			int searchCount = searchResultPg.getSearchResultCount();
			
			headers.searchProductKeyword(searchText2);
			Log.message(i++ + ". Navigated to Search Result Page!", driver);
			
			Log.softAssertThat(!(prodId1[0].equals(searchResultPg.getListOfProductId(1)[0]))||
					searchResultPg.getSearchResultCount()!=searchCount,
					"Searching with different keyword should give result accordingly using primary search box", 
					"Searching with different keyword is giving result accordingly", 
					"Searching with different keyword is not giving result accordingly", driver);
			
			Log.softAssertThat(searchResultPg.elementLayer.verifyHorizontalAllignmentOfElements(driver, "secondarySearchButton", "secondarySearchSection", searchResultPg), 
					"Secondary search button should be displayed right side to the Secondary search text box", 
					"Secondary search button is displayed right side to the Secondary search text box", 
					"Secondary search button is not displayed right side to the Secondary search text box", driver);
			
			prodId1 = searchResultPg.getListOfProductId(1);
			searchCount = searchResultPg.getSearchResultCount();
			//Step 3 - Verify secondary search box displayed in the search result page
			
			searchResultPg.secondarySearchProductKeyword(searchText1);
			Log.message(i++ + ". Search using Secondary Search Field!", driver);
			
			Log.softAssertThat(!(prodId1[0].equals(searchResultPg.getListOfProductId(1)[0]))||
					searchResultPg.getSearchResultCount()!=searchCount, 
					"Searching with different keyword should give result accordingly using secondary search box", 
					"Searching with different keyword is giving result accordingly", 
					"Searching with different keyword is not giving result accordingly", driver);
			
			Log.softAssertThat(headers.getEnteredTextFromSearchTextBox().trim().equals(searchText1.trim()), 
					"Text entered in the secondary Search text box should display in primary search box after searching", 
					"Text entered in the secondary Search text box is displayed in primary search box after searching", 
					"Text entered in the secondary Search text box is not displayed in primary search box after searching", driver);
			
			//Step 4 - Verify Result Count displayed in the search result page
			
			if(Utils.isMobile()) {
				Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayed(Arrays.asList("searchResultCountMob"), searchResultPg), 
						"'Search result count' should be displayed", 
						"'Search result count' is getting displayed", 
						"'Search result count' is not getting displayed", driver);
				
				Log.softAssertThat(searchResultPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "searchResultCountMob", "secondarySearchSection", searchResultPg), 
						"'Search result count' should be displayed above secondary search text box", 
						"'Search result count' is getting displayed above secondary search text box", 
						"'Search result count' is not getting displayed above secondary search text box", driver);
				
			} else {
				Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayed(Arrays.asList("searchResultCountDeskTab"), searchResultPg), 
						"'Search result count' should be displayed", 
						"'Search result count' is getting displayed", 
						"'Search result count' is not getting displayed", driver);
				
				Log.softAssertThat(searchResultPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "searchResultCountDeskTab", "secondarySearchSection", searchResultPg), 
						"'Search result count' should be displayed above secondary search text box", 
						"'Search result count' is getting displayed above secondary search text box", 
						"'Search result count' is not getting displayed above secondary search text box", driver);
			}
			
			int count = searchResultPg.getSearchResultCount();
			
			headers.searchProductKeyword(searchText2 +" / "+ searchText1);
			Log.message(i++ + ". Navigated to Search Result Page!", driver);
			
			Log.softAssertThat(count != searchResultPg.getSearchResultCount(), 
					"Adding another name for search product should give different result", 
					"Adding another name for search product is giving different result", 
					"Adding another name for search product is not giving different result", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(searchResultPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "searchResultCountMob", "secondarySearchSection", searchResultPg), 
						"'Updated Search result count' should be displayed above secondary search text box", 
						"'Updated Search result count' is getting displayed above secondary search text box", 
						"'Updated Search result count' is not getting displayed above secondary search text box", driver);
			} else {
				Log.softAssertThat(searchResultPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "searchResultCountDeskTab", "secondarySearchSection", searchResultPg), 
						"'Updated Search result count' should be displayed above secondary search text box", 
						"'Updated Search result count' is getting displayed above secondary search text box", 
						"'Updated Search result count' is not getting displayed above secondary search text box", driver);
			}
			
			count = searchResultPg.getSearchResultCount();
			
			//Step 6 - Verify secondary search does not function when there are no terms in the secondary search box. 
			
			headers.searchProductKeyword("");
			Log.message(i++ + ". Search without entereing value in text box!", driver);
			
			Log.softAssertThat(count == searchResultPg.getSearchResultCount(), 
					"When the user clicks the search with no search terms in the secondary search box the page should not change", 
					"When the user clicks the magnifying glass with no search terms in the secondary search box, nothing happens", 
					"When the user clicks the magnifying glass with no search terms in the secondary search box different result displays", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24126
