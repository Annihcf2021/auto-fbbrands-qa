package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24575 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	@Test(groups = { "pdp", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24575(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			
			String level1Cat = TestData.get("level-1");
			String level2Cat = TestData.get("level-2");
			String prodId = TestData.get("prd_reviewed");
			String reviewSearchTerm = TestData.get("review_search");
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();			
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			if(Utils.isDesktop()) {
				homePage.headers.mouseHoverOnCategoryByCategoryName(level1Cat.split("\\|")[0]);
				Log.message(i++ + ". Mouse hovered on " + level1Cat.split("\\|")[0], driver);
				
				Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("currentLevel2"), homePage.headers), 
						"Category flyout should dispayed", 
						"Category flyout is dispayed", 
						"Category flyout is not dispayed", driver);
			}
			
			PlpPage plpPage = homePage.headers.navigateTo(level2Cat.split("\\|")[0], level2Cat.split("\\|")[1]);
			
			BrowserActions.scrollToBottomOfPage(driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage), 
					"PLP page should display", 
					"PLP page is displayed", 
					"PLP page is not displayed", driver);	
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prodId);
			Log.message(i++ + ". Navigated to PDP Page for product :: " + pdpPage.getProductName(), driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("sectionReviewTab","sectionDetailsTab"), pdpPage), 
					"Check the review and detail sections are displayed",
					"The review and detail sections are displayed",
					"The review and detail sections are not displayed", driver);
	
			pdpPage.clickOnDetailsTab();
			Log.message(i++ + ". The detail message are changing for product to product, hence verifying only whether the details is displaying or not", driver);
	
			//Based on SC-112 story, Social icons removed from PDP. Hence to check content gallery displayed below Shipping info
			if (pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtCustomerGalleryHeading"), pdpPage)) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divShippingInfo", "txtCustomerGalleryHeading", pdpPage), 
						"Check the Content Gallery displayed below the Shipping Info",
						"The Content Gallery displayed below the Shipping Info",
						"The Content Gallery is not displayed below the Shipping Info", driver);
				
				pdpPage.clickCustomerGalleryImageByIndex(1);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("custGalleryContainer"), pdpPage), 
						"Customer gallery overlay should display",
						"Customer gallery overlay is displayed",
						"Customer gallery overlay is not displayed", driver);
				
				pdpPage.clickCloseCustomerGallery();
				
				Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("custGalleryContainer"), pdpPage), 
						"Customer gallery overlay should display",
						"Customer gallery overlay is not displayed",
						"Customer gallery overlay is displayed", driver);
			} else {
				Log.reference("Customer Gallery is present in the Product Detail page");
			}
			
			pdpPage.clickOnReviewTab();
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("sectionReviewVisible"), pdpPage), 
					"Check the review section is opened",
					"The review section is opened",
					"The review section is not opened", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtReviewCount","txtAvgRatingsReview","txtAvgRatingBox"
					,"lnkReviewMyPost","lnkReviewMorePurchase"), pdpPage),
					"Check the review section has average rating, the number of reviews, Comfort and Service meter, overall sizes,"
							+ " recommend the product, best uses for the product, and links for Review More Purchases and My Posts.",
							"The review section has average rating, the number of reviews, Comfort and Service meter, overall sizes" + 
									" recommend the product, best uses for the product, and links for Review More Purchases and My Posts.",
									"Some components are missing in the review section", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("cmbSortReview","txtSearchReview"), pdpPage), 
					"Check the review section has Sort review combo box and Search review text box",
					"The review section has Sort review combo box and Search review text box",
					"The review section is not having Sort review combo box or Search review text box", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionReviewChart", "txtSearchReview", pdpPage), 
					"Check the search review displayed below the review section",
					"The search review is displayed below the review section",
					"The search review is not displayed below the review section", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionReviewChart", "cmbSortReview", pdpPage),
					"Check the sort review displayed below the review section",
					"The sort review is displayed below the review section",
					"The sort review is not displayed below the review section", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtReviewAuthor"), pdpPage), 
					"Check all the review has review name",
					"The reviews has reviewer name",
					"The reviews is not having reviewer name", driver);
	
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtReviewTitle", "txtReviewCustomerStar"), pdpPage), 
					"Check the review section has Sort review combo box and Search review text box",
					"The review section has Sort review combo box and Search review text box",
					"The review section is not having Sort review combo box or Search review text box", driver);
			
			pdpPage.selectSortBasedOnIndex(3);
			
			String rate = pdpPage.getRatingFromFirstReview();
			
			pdpPage.selectSortBasedOnIndex(4);
			
			Log.softAssertThat(!(pdpPage.getRatingFromFirstReview().equals(rate)), 
					"Check the review section has Sort should work according to the option selection",
					"Check the review section has Sort is working according to the option selection",
					"Check the review section has Sort is not working according to the option selection", driver);
			
			if(pdpPage.getNoOfReviews() > 10) {
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkShowMoreReview"), pdpPage), 
						"Show more link should get displayed",
						"Show more link is displayed",
						"Show more link is not displayed", driver);
				
				int beforeShowMore = pdpPage.getNoOfDisplayingReviews();
				
				pdpPage.clickShowMoreReviews();
				Log.message(i++ + ".  Clicked on Show more review link", driver);
				
				int afterShowMore = pdpPage.getNoOfDisplayingReviews();
				
				Log.softAssertThat(beforeShowMore != afterShowMore, 
						"The Show more link load should load more reviews when clicked",
						"The Show more link load is loading more reviews when clicked",
						"The Show more link load is not loading more reviews", driver);
			} else {
				Log.reference("Product is not having enough review");
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lnkShowMoreReview"), pdpPage), 
						"Show more link should not displayed",
						"Show more link is not displayed",
						"Show more link is displayed", driver);
			}
			
			pdpPage.enterReviewSearchTerm(reviewSearchTerm);
			Log.message(i++ + ".  Entered in review search", driver);
			
			pdpPage.clickOnSearchedReviewBasedOnIndex(0);
			
			Log.softAssertThat( (pdpPage.elementLayer.verifyElementTextContains("lblReviewSectionCmts", reviewSearchTerm, pdpPage)) || 
					(pdpPage.elementLayer.verifyElementTextContains("txtReviewSearchBody", reviewSearchTerm, pdpPage)), 
					"Review should display based on searched term", 
					"Review is displaying based on searched term", 
					"Review is not displaying based on searched term", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24575