package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24805 extends BaseTest{
	
	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups= { "plp", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24805(String browser) throws Exception{
		Log.testCaseInfo(); 

		final WebDriver driver =  WebDriverFactory.get(browser);
		int i=1;
		try{
			String level1_hemming = TestData.get("level_with_hemming").split("\\|")[0];
			String level2_hemming = TestData.get("level_with_hemming").split("\\|")[1];
			String productId = TestData.get("level_with_hemming").split("\\|")[2];
			String hemmingMsg = demandwareData.get("hemmingMsg");
			String hemmingColor = TestData.get("hemming_Highlight");
			
			//Step-1 Navigate to the web site
			HomePage homePage= new HomePage(driver, Utils.getWebSite()).get();
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"The system should display the Homepage", 
					"The system is displayed the Homepage", 
					"The system is not displayed the Homepage", driver);
			
			//Step-2 Mouse hover on category name from any category header	
			Log.softAssertThat(homePage.headers.verifyCategoryFlyOutDisplays(level1_hemming),
					"The flyout should displays when the mouse hovers on the root category.", 
					"The flyout is displayed when the mouse hovers on the root category.",
					"The flyout is not displayed when the mouse hovers on the root category.",driver);
			
			PlpPage plpPage=homePage.headers.navigateTo(level1_hemming, level2_hemming);
			Log.message(i++ +". Clicked on " + level2_hemming + " subcategory name, where we have hemming product available ", driver);
			
			//Step-3 Navigate to PLP
			plpPage.mouseHoveredOnProduct(productId);
			Log.message(i++ +". Clicked Mouse hovered on main image ", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnQuickView"),plpPage), 
					"The system should display the 'quick shop' button link on top of the main image.", 
					"The system is displayed the 'quick shop' button link on top of the main image.",
					"The system is not displayed the 'quick shop' button link on top of the main image.", driver);
			
			QuickShop quickShop=plpPage.clickOnQuickShopButton();
			Log.message(i++ +". Clicked quick shop button ", driver);
			
			Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("divQuickShop"),quickShop), 
					"Quick shop modal should be opened.", 
					"Quick shop modal is opened.",
					"Quick shop modal is not opened.", driver);
			
			Log.softAssertThat(quickShop.gethemmingMessage().contains(hemmingMsg),
					"'PLEASE SELECT COLOR & SIZE TO SEE HEMMING OPTIONS.' label should be shown.", 
					"'PLEASE SELECT COLOR & SIZE TO SEE HEMMING OPTIONS.' label is shown.",
					"'PLEASE SELECT COLOR & SIZE TO SEE HEMMING OPTIONS.' label is not shown.", driver);
			
			Log.softAssertThat(quickShop.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkHemmingTips", "txtHemmingTitle", quickShop),
					"The tooltip should be displayed on the right side of the label.",
					"The tooltip is displayed on the right side of the label.",
					"The tooltip is not displayed on the right side of the label.", driver);
			
			quickShop.clickOnTipsInHemmingSection();
			Log.message(i++ +". Clicked on the tooltip ", driver);
			
			Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("hemmingtipsFlyout"),quickShop), 
					"Tooltip window should be properly opened.", 
					"Tooltip window is properly opened.",
					"Tooltip window is not properly opened.", driver);
			
			Log.softAssertThat(!quickShop.gettextFromHemmingFlyout().isEmpty(),
					"Flyout should display additional information about hemming option",
					"Flyout is displayed additional information about hemming option",
					"Flyout is not displayed additional information about hemming option", driver);
			
			quickShop.clickOnTipsFlyoutCloseButton();
			
			//Step-4 Select color and size swatches in Quick shop window.
			String productName = quickShop.getProductName().toLowerCase();
			
			String color = quickShop.selectColor();
			Log.message(i++ +". Selected color: " + color, driver);
			
			String size = quickShop.selectSize();
			Log.message(i++ +". Selected Size: " + size, driver);
			
			Log.softAssertThat(quickShop.elementLayer.verifyVerticalAllignmentOfElements(driver, "tblSizeSwatch", "txtHemmingTitle", quickShop),
					"Below size attributes 'Hemming' header should be shown.", 
					"Below size attributes 'Hemming' header is shown.",
					"Below size attributes 'Hemming' header is not shown.", driver);
			
			Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("checkBoxHemmingSection", "txtHemmingMessaging"), quickShop),
					"Checkbox with 'Hem pants length (additional $5.99 per pair)' label should be shown.",
					"Checkbox with 'Hem pants length (additional $5.99 per pair)' label is shown.",
					"Checkbox with 'Hem pants length (additional $5.99 per pair)' label is not shown.", driver);
			
			quickShop.clickOnHemmingCheckbox("enable");
			Log.message(i++ +". Clicked hemming checkbox", driver);
			
			Log.softAssertThat(quickShop.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtHemmingMessaging", "divHemmingDrawer", quickShop),
					"Below 'Hem pants length (additional $5.99 per pair)' label, Hemming values should be displayed.",
					"Below 'Hem pants length (additional $5.99 per pair)' label, Hemming values is displayed.",
					"Below 'Hem pants length (additional $5.99 per pair)' label, Hemming values is not displayed.", driver);
			
			Log.softAssertThat(quickShop.elementLayer.verifyVerticalAllignmentOfElements(driver, "lstHemmingOption", "txtHemmingDisclaimer", quickShop),
					"Below hemming value, personalized products cannot be returned or exchanged. the message should be shown.",
					"Below hemming value, personalized products cannot be returned or exchanged. the message is shown.",
					"Below hemming value, personalized products cannot be returned or exchanged. the message is not shown.", driver);
			
			quickShop.clickOnHemmingCheckbox("disable");
			Log.message(i++ +". Deselected hemming checkbox", driver);
			
			Log.softAssertThat(quickShop.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("lstHemmingOption","txtHemmingDisclaimer"), quickShop),
					"Hemming values and 'Personalized products cannot be returned or exchanged.' message should not be displayed below hemming value.", 
					"Hemming values and 'Personalized products cannot be returned or exchanged.' message is not  displayed below hemming value.",
					"Hemming values and 'Personalized products cannot be returned or exchanged.' message is displayed below hemming value.", driver);
			
	
			Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("checkBoxHemmingSection","txtHemmingMessaging"), quickShop),
					"Checkbox with 'Hem pants length (additional $5.99 per pair)' label should be shown.",
					"Checkbox with 'Hem pants length (additional $5.99 per pair)' label is shown.",
					"Checkbox with 'Hem pants length (additional $5.99 per pair)' label is not shown.", driver);
	
			quickShop.clickOnHemmingCheckbox("enable");
			Log.message(i++ +". Clicked hemming checkbox", driver);
			
			quickShop.addToBag();
			Log.message(i++ +". Clicked Add to bag button", driver);
			
			Log.softAssertThat(quickShop.elementLayer.verifyElementColor("txtHemmingTitle", hemmingColor, quickShop),
					"Hemming header should be highlighted in red color.",
					"Hemming header is highlighted in red color.",
					"Hemming header is not highlighted in red color." , driver);
			
			Log.softAssertThat(quickShop.verifyErrorMessageInHemmingOptions() &&
					quickShop.elementLayer.verifyVerticalAllignmentOfElements(driver, "lstHemmingOption", "txtHemmingErrorMessage",quickShop),
					"Below hemming options, 'Please select hemming option.' validation failure message should be shown.",
					"Below hemming options, 'Please select hemming option.' validation failure message is shown.",
					"Below hemming options, 'Please select hemming option.' validation failure message is not shown.", driver);
			
			//Step-5 Navigate to the cart page
			quickShop.selectHemmingOption(0);
			String hemmingOption = quickShop.getSelectedHemmingValue();
			Log.message(i++ +". Selected first Hemming option: " + hemmingOption, driver);
			
			String salePrice = quickShop.getSalePrice().replace("$", "");
			float prdPrice = Float.parseFloat(salePrice);
			Log.message(i++ +". ProductPrice is: " + salePrice);
			
			quickShop.addToBag();
			Log.message(i++ +". Clicked Add to bag button", driver);
			
			Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("flytMiniCart"), quickShop),
					"Add to bag overlay should be opened.",
					"Add to bag overlay is opened.",
					"Add to bag overlay is not be opened.", driver);
			
			Log.softAssertThat(quickShop.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("selectedHemmingValueInMCOverlay"), quickShop),
					"Selected hemming option should not be displayed on ATB",
					"Selected hemming option is not displayed on ATB",
					"Selected hemming option is displayed on ATB", driver);
			
			String total = quickShop.getTotalInMcOverlay().replace("$", "");
			Float cartTotal = Float .parseFloat(total);
			Log.message(i++ +".Total price is: "+total);
			
			Log.softAssertThat(cartTotal>prdPrice, 
					"Near 'Total' hemming price should be auto calculated",
					"Near 'Total' hemming price is auto calculated",
					"Near 'Total' hemming price is not auto calculated", driver);
			
			ShoppingBagPage cart = quickShop.clickOnCheckoutInMCOverlay();
			Log.message(i++ +".Navigated to cart page", driver);
			
			String cartPrdName = cart.getProductName().toLowerCase();
			Log.message("Cart is:  " + cartPrdName + "..... quick shop is: " + productName);
			
			
			Log.softAssertThat(cartPrdName.contains(productName), 
					"The product should be added to the cart page",
					"The product is added to the cart page",
					"The product is not added to the cart page",driver);
			
			Log.softAssertThat(cart.getHemmingValue().contains(hemmingOption),
					"selected hemming option is displaying for the product in cart Page.",
					"selected hemming option is displaying for the product in cart Page.",
					"selected hemming option is displaying for the product in cart Page.", driver);			
	
			Log.testCaseResult();

		}//End try block
		catch(Exception e){
			Log.exception(e, driver);
		}// end catch block
		finally{
			Log.endTestCase(driver);
		} //end finally block
	}//M1_FBB_E2E_C24805

}//TC_FBB_E2E_C24805
