package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CategoryLandingPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24800 extends BaseTest {
	
	EnvironmentPropertiesReader environmentPropertiesReader;	
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");

	@Test(groups = { "pdp", "desktop", "tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24800(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			
			String outOfStockPrd = TestData.get("prd_out-of-stock").split("\\|")[0];
			String outofstockColor = TestData.get("prd_out-of-stock").split("\\|")[1];
			String outofstockSize = TestData.get("prd_out-of-stock").split("\\|")[2];
			String salePriceColor = TestData.get("SalesPriceColor");
			String singleColorPrd = TestData.get("prd_single_Color");
			String sizeFamilyPrd = TestData.get("prd_sizefamily");
			String btnColorErrorMsg = demandwareData.get("AddToBagErrMsg");
			String itemAddedMsg = demandwareData.get("ItemAddedMsg");
			String prdSalePrice = TestData.get("prd_salePrice");
					
			//Step-1 Navigate to the web site
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Headers headers = homePage.headers;
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			//Step-2 Verify the functionality of the search field.
			PdpPage pdpPage = homePage.headers.navigateToPDP(singleColorPrd);
			Log.message(i++ +". Navigated to sliced product PDP", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("pdpMain"), pdpPage),
					"The page should be redirected to the PDP page. ",
					"The page is redirected to the PDP page.",
					"The page is not redirected to the PDP page.", driver);			
			
			int colorSwatches = pdpPage.colorSwatchCount();
			if(colorSwatches==1){
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("selectedColor"), pdpPage),
						"Product should be sliced product and color should be pre-Selected",
						"Product is sliced product and color is pre-Selected",
						"Product is sliced product but color is not be pre-Selected", driver);
				
				Log.reference("Product is sliced product.");
			}			
			pdpPage = headers.navigateToPDP(outOfStockPrd);
			//Step-3 Verify the Bread crumb in PDP
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedleft("cntPdpContent", "divBreadcrumb", pdpPage) &&
					pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divBreadcrumb", "mainProdImage", pdpPage),
					"he breadcrumb should be displayed to the top left-hand side of the page and Above the product main image",
					"The breadcrumb is displayed in left side of the product details page and Above the product main image",
					"The breadcrumb is not displayed in left side of the product details page and Above the product main image", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyCssPropertyForPsuedoElement("breadcrumbDivider", ":after", "content", "/",headers)
					|| headers.elementLayer.verifyCssPropertyForPsuedoElement("breadcrumbDivider", ":after", "content", ">",headers), 
					" breadcrumb category root should be delimited with the symbol ' / ' or '<'", 
					"breadcrumb category root is delimited with the symbol ' / ' or '<'", 
					"breadcrumb category root is not delimited with the symbol ' / ' or '<'", driver);
			
			String prodName=pdpPage.getProductName();	
			int breadcrumbSize= pdpPage.getTotalNoOfBreadCrumb();			
			
			for(int breadcrumb=breadcrumbSize-1 ;breadcrumb>1;breadcrumb--){				
				String category= pdpPage.getBreadcrumbValueOf(breadcrumb).trim().toLowerCase();
				 pdpPage.clickOnBreadCrumbByIndexValue(breadcrumb);
					
				if(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("plpReadyElement"), pdpPage)){
					PlpPage plpPage= new PlpPage(driver).get();
					String	plpcategory= plpPage.getCategoryName().trim().toLowerCase();					
					
					Log.softAssertThat(plpcategory.contains(category),
							"On clicking "+breadcrumb+" th breadcrumb '"+category+"', system should navigates the user to the selected category PLP",
							"On clicking "+breadcrumb+" th breadcrumb '"+category+"', system is navigates the user to the selected category PLP", 
							"On clicking "+breadcrumb+" th breadcrumb '"+category+"', system is not navigates the user to the selected category PLP", driver);
				}else{
					CategoryLandingPage categoryPage= new CategoryLandingPage(driver).get();
					Log.softAssertThat(categoryPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"),categoryPage),
							"On clicking "+breadcrumb+" th breadcrumb '"+category+"', system should navigates the user to the selected category landing Page",
							"On clicking "+breadcrumb+" th breadcrumb '"+category+"', system is navigates the user to the selected category landing Page", 
							"On clicking "+breadcrumb+" th breadcrumb '"+category+"', system is not navigates the user to the selected category landing Page", driver);
				}				
				driver.navigate().back();
			}
			
			pdpPage.clickOnHomeBC();
			Log.message(i++ + ". Clicked home page link from PDP breadcrumb");
			
			Log.softAssertThat(new HomePage(driver).get().getPageLoadStatus(),
					"If user tabs/click on home in breadcrumb, system should navigate user to the homepage",
					"If user tabs/click on home in breadcrumb, system navigate user to the homepage",
					"If user tabs/click on home in breadcrumb, system not navigate user to the homepage", driver); 
			
			pdpPage = homePage.headers.navigateToPDP(outOfStockPrd);
			Log.softAssertThat(pdpPage.getProductName().equalsIgnoreCase(prodName),
					"Same PDP should be displayed when user searches with same product id", 
					"Same PDP is displayed when user searches with same product id",
					"Same PDP is not displayed when user searches with same product id", driver);
			
			//Step-4 Verify Product Name & price
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtProductName", "mainProdImage", pdpPage),
					"The product name should be displayed to the right-hand side of the product imagery",
					"The product name is displayed to the right-hand side of the product imagery",
					"The product name is not displayed to the right-hand side of the product imagery", driver);
			
			Log.message("<br><b><u>Sale Price Product</u></b>");
			pdpPage = homePage.headers.navigateToPDP(prdSalePrice);
			Log.message(i++ + ". Navigated to Sale product", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("lblSalePrice","lblOriginalPriceInSalePrice"), pdpPage), 
					"For Sale Price Product, It should show Original Price & Sales Price.", 
					"Original Price & Sales Price displayed!", 
					"Original Price or Sales price Not displayed", driver);
			try {
				double originalPrice = StringUtils.getPriceFromString(pdpPage.getOriginalPrice());
				double salePrice = StringUtils.getPriceFromString(pdpPage.getSalePrice());
			
				Log.softAssertThat(originalPrice > salePrice,
						"Sale price will only display when the sale price is different from the original price.",
						"Orginal Price and Sale Price are different!",
						"Orignal Price and Slae Price are not different.", driver);
			} catch (NumberFormatException e) {
				Log.reference("Skipping this validation due to Given product doesn't have single markdown price.");
			}
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementColor("lblSalePrice", salePriceColor, pdpPage),
					"Sale Price should be displayed in Red Color",
					"Sale Price displayed in Red Color!",
					"Sale Price Not displayed in Red Color.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyCssPropertyForElement("lblOriginalPriceInSalePrice", "text-decoration", "line-through", pdpPage), 
					"Original Price should displayed with strikethrough", 
					"Original Price displayed with strikethrough!", 
					"Original Price not displayed with strikethrough.", driver);
			
			//Step-5 Verify the color attributes
			pdpPage = homePage.headers.navigateToPDP(outOfStockPrd);
			Log.message(i++ + ". Navigated to OOS pdp page", driver);
			
			if(!pdpPage.elementLayer.isElementsDisplayed(Arrays.asList("defaultSwatchColor"), pdpPage)) {
				Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("selectedColor"), pdpPage),
						"No Color Swatch should be pre-selected on the PDP",
						"No Color Swatch is pre-selected on the PDP",
						"Color Swatch is pre-selected on the PDP", driver);
			} else {
				Log.message(i++ + ". Since it is a sliced product, color is per-selected.", driver);
			}
			
			if(Utils.isDesktop()){
				String colorSwatchimg = pdpPage.mouseHoverOnColorswatch(1).replaceAll("[^0-9|\\-]", "");
				Log.message(i++ +". Mouse hovered on first color: "+ colorSwatchimg, driver);
				
				String prdMainimg = pdpPage.getProdImageName().replaceAll("[^0-9|\\-]", "");
				Log.message(i++ +". Product image is: "+ prdMainimg);
				
				if(colorSwatchimg.contains("NO-SWATCH-TAB") || colorSwatchimg.contains("noimagefound")) {
					Log.reference("Skipping the verification of colorized image due to color is not available for the respective color swatch");
				
				} else {
					if(prdMainimg.contains("noimagefound")) {
						Log.reference("Skipping the verification of colorized image due to main image is not available");
					} else {
						Log.softAssertThat(prdMainimg.contains(colorSwatchimg),
								"System should display the colorized image on the main image area",
								"System is displaying the colorized image on the main image area", 
								"System is not displaying the colorized image on the main image area", driver);
					}
				}
				String colorSwatchimg1= pdpPage.mouseHoverOnColorswatch(2).replaceAll("[^0-9|\\-]", "");
				Log.message(i++ +". Mouse hovered Second color image is: "+colorSwatchimg1, driver);
				
				String prdMainimg1 = pdpPage.getProdImageName().replaceAll("[^0-9|\\-]", "");
				Log.message(i++ +". Product image is: "+ prdMainimg1);
				
				if(colorSwatchimg.contains("NO-SWATCH-TAB") || colorSwatchimg.contains("noimagefound")) {
					Log.reference("Skipping the verification of colorized image due to color is not available for the respective color swatch");
				
				} else {
					if(prdMainimg1.contains("noimagefound")) {
						Log.reference("Skipping the verification of colorized image due to main image is not available");
					} else {
						Log.softAssertThat(prdMainimg1.contains(colorSwatchimg1),
								"System should display the colorized image on the main image area",
								"System is displaying the colorized image on the main image area", 
								"System is not displaying the colorized image on the main image area", driver);
					}	
				}
				
				pdpPage.selectColor(outofstockColor, "yes");
				Log.message(i++ +". Selected color", driver);
				
				String selectedColor = pdpPage.getSelectedColor();
				Log.message(i++ +". Selected Color: "+selectedColor, driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "pdpSelectedColor", "colorlabel", pdpPage),
						"color name should be display next by the color level",
						"color name is displaying next by the color level",
						"color name is not displaying next by the color level", driver);
				
				pdpPage.selectSize(outofstockSize, "yes");
				Log.message(i++ +". Selected out of stock size: "+pdpPage.getSelectedSize());
				
				Log.softAssertThat(pdpPage.verifygryedOutColorSwatchesAvailability(),
						"once user select the Out of stock size variation the color swatches should become grey color",
						"once user select the Out of stock size variation the color swatches is become grey color",
						"once user select the Out of stock size variation the color swatches is not become grey color", driver);
				
				pdpPage.mouseHoverAddToBag();
				Log.message(i++ +". Mose hovered on add to bag button");
				
				Log.softAssertThat(pdpPage.addToCartButtonDisabled().equalsIgnoreCase("true") &&
						(pdpPage.elementLayer.verifyTextContains("btnAddToBag", btnColorErrorMsg, pdpPage)),
						"Add to bag button should display as gray out & message should display 'please select a color & size'",
						"Add to bag button is displayed as gray out & message is displayed 'please select a color & size'",
						"Add to bag button is displayed as gray out & message is displayed 'please select a color & size'", driver);
			} 
						
			//Step-6 Verify the "size family" attributes
			if(!BrandUtils.isBrand(Brand.bh)) {
				pdpPage = headers.navigateToPDP(sizeFamilyPrd);
				Log.message(i++ +". Navigated to size family product PDP", driver);
				
				if(pdpPage.elementLayer.verifyPageElements(Arrays.asList("divSizeFamily"), pdpPage)) {
					if(BrandUtils.isBrand(Brand.ks)) {
						Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("selectedSizeFamily_KS"), pdpPage),
								"One size family should be selected by default",
								"One size family is selected by default",
								"One size family is not selected by default", driver);
					} else {
						Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("selectedSizeFamily"),pdpPage),
								"One size family should be selected by default",
								"One size family is selected by default",
								"One size family is not selected by default", driver);
					}
					pdpPage.selectColorBasedOnIndex(0);
					Log.message(i++ +". Selected color", driver);		
					
					if(pdpPage.getSizeFamilyCount()>1) {
						String selectedsize = pdpPage.selectSize();
						Log.message(i++ +". Selected Size: "+selectedsize, driver);
						
						String defaultSizefamily = pdpPage.getSelectedSizeFamily();
						
						String selectedSizeFamily = pdpPage.selectUnSelectedSizeFamily();
						Log.message(i++ +". Selected SizeFamily: "+selectedSizeFamily, driver);
						
						Log.softAssertThat(! pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("selectedColor","selectedSize"), pdpPage),
								"color & size should not display as selected when selected new size family", 
								"color & size is not displayed as selected when selected new size family",
								"color & size is displayed as selected when selected new size family", driver);
						
						pdpPage.selectUnSelectedSizeFamily(defaultSizefamily);
					} else {
						Log.reference("Only one size family swatch is available");
					}
				} else {
					Log.reference(" Size family is not available for "+pdpPage.getProductName()+" product");
				}
			} else {
				Log.reference(" Size family is not applicable for this "+Utils.getCurrentBrand()+" brand, Hence skipping the step.");
			}
						
			//Step-7 Verify the "size" attributes
			pdpPage = headers.navigateToPDP(outOfStockPrd);
			Log.message(i++ + ". Navigated to OOS product", driver);
			
			String selectedColor1 = pdpPage.selectColor(outofstockColor, "Yes");
			Log.message(i++ +". Selected Out of stock Color: "+selectedColor1, driver);
			
			Log.softAssertThat(pdpPage.verifygryedOutSizeSwatchesAvailability(),
					"Out of stock size variations for the color should be greyed out",
					"Out of stock size variations for the color is greyed out",
					"Out of stock size variations for the color is not greyed out", driver);
			
			
			Log.softAssertThat(pdpPage.verifyActiveSizeSwatchesAvailability(),
					"In stock, size attributes should display in an active state",
					"In stock, size attributes are displayed in an active state",
					"In stock, size attributes are not displayed in an active state", driver);
			
			if(Utils.isDesktop()) {
				String selectedSize1 = pdpPage.selectSize(outofstockSize, "Yes");
				Log.message(i++ +". Selected Out of stock size: "+selectedSize1, driver);
				
				pdpPage.clickAddToBagForErrorMsg();
				Log.message(i++ +". Clicked add to bag button", driver);
				
				Log.softAssertThat(pdpPage.addToCartButtonDisabled().equalsIgnoreCase("true")
								&& (pdpPage.elementLayer.verifyTextContains("btnAddToBag", btnColorErrorMsg, pdpPage)),
						"Add to bag button should display as gray out & message should display 'please select a color & size'",
						"Add to bag button is displayed as gray out & message is displayed 'please select a color & size'",
						"Add to bag button is displayed as gray out & message is displayed 'please select a color & size'", driver);
			}
			
			pdpPage = headers.navigateToPDP(singleColorPrd);
			Log.message(i++ + ". Navigated to PDP", driver);
			
			pdpPage.selectAllSwatches();
			Log.message(i++ +". Selected Color swatch which is active", driver);
			
			String productName = pdpPage.getProductName();
			String productimg = pdpPage.getProdImageName();			
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ +". Clicked add to bag button", driver);
			
			//Step-8 Verify add to bag overly
			Log.softAssertThat(pdpPage.elementLayer.verifyInsideElementAlligned("lblMiniCartOverlayTitle", "mdlMiniCartOverLay", "top", pdpPage) &&
					(pdpPage.elementLayer.verifyTextContains("lblMiniCartOverlayTitle", itemAddedMsg.split("\\|")[0], pdpPage, false) ||
							pdpPage.elementLayer.verifyTextContains("lblMiniCartOverlayTitle", itemAddedMsg.split("\\|")[1], pdpPage, false)), 
					"Item Added Message should be displayed to the top of the Add to Bag overlay", 
					"Item Added Message is displayed to the top of the Add to Bag overlay", 
					"Item Added Message is not displayed to the top of the Add to Bag overlay", driver);
						
			Log.softAssertThat(pdpPage.getMiniCartOverLayImgSource().contains(productimg) 
					&& pdpPage.verifyProductDetailsDisplayedMCOverLay(),
						"'selected colorized image', 'product name', 'color name', 'size', 'QTY','Availability', 'Price' Should be displayed in Add to bag overlay",
						"'selected colorized image', 'product name', 'color name', 'size', 'QTY','Availability', 'Price' is displayed in Add to bag overlay",
						"'selected colorized image', 'product name', 'color name', 'size', 'QTY','Availability', 'Price' is not displayed in Add to bag overlay", driver);
			 	
			ShoppingBagPage cart = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ +". Clicked checkout now button from add to cart overlay");
			
			//Step-9 Navigates to the Cart page
			Log.softAssertThat(cart.getProductName().toLowerCase().contains(productName.toLowerCase()),
					"Added product should be displayed in cart page",
					"Added product is displayed in cart page",
					"Added product is not displayed in cart page", driver);	
	
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally
	}//M1_FBB_E2E_C24800

}//TC_FBB_E2E_C24800
