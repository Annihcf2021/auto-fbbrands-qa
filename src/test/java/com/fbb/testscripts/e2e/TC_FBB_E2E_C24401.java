package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.Direction;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24401 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "checkout", "desktop","tablet","mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24401(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{

			//Step 1 - Navigate to the website
			
			String level1Cat = TestData.get("level-1");
			String level2Cat = TestData.get("levels");
			String errColorCode = TestData.get("cart_limited_inventory");
			String[] prdLimitedStock = TestData.get("sku_stock<5_variation").split("\\|");
			String checkoutAdd = "address_withtax";
			String payment = "card_MasterCard";
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String inventoryMessageTxt = demandwareData.get("inventory_message");
			
			{
				GlobalNavigation.registerNewUserAddressPayment(driver, checkoutAdd, payment, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Click on category name from any category header
			
			if(Utils.isDesktop()) {
				homePage.headers.mouseHoverOnCategoryByCategoryName(level1Cat.split("\\|")[0]);
				Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayed(Arrays.asList("currentLevel2"), homePage.headers), 
						"Category flyout should dispayed", 
						"Category flyout is dispayed", 
						"Category flyout is not dispayed", driver);
			}
			
			//step 3 - Navigate to Product listing page
			
			PlpPage plpPage = homePage.headers.navigateTo(level2Cat.split("\\|")[0], level2Cat.split("\\|")[1]);
			
			BrowserActions.scrollToBottomOfPage(driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage), 
					"PLP page should display", 
					"PLP page is displayed", 
					"PLP page is not displayed", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProductActualSalesPrice"), plpPage), 
					"Original price should displayed", 
					"Original price is displayed", 
					"Original price is not displayed", driver);
			
			//Step 4 - Navigate to Product listing page
			
			String prdPrice = plpPage.getOriginalPriceProduct(1);
			
			String prdNam1 = plpPage.getOriginalPriceProductNameByIndex(1);
			
			PdpPage pdpPage = plpPage.clickAnOriginalPriceProduct(1);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementTextContains("lblPrice", prdPrice, pdpPage), 
					"Price should be same in pdp and plp page", 
					"Price is same in pdp and plp page", 
					"Price is not same in pdp and plp page", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementTextContains("productName", prdNam1, pdpPage), 
					"Name should be same in pdp and plp page", 
					"Name is same in pdp and plp page", 
					"Name is not same in pdp and plp page", driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart.", driver);
			
			//Step 5 - Navigate to the Product detail page, Please Note: Please select a limited inventory product
			
			pdpPage = homePage.headers.navigateToPDP(prdLimitedStock[0]);
			Log.message(i++ + ". Navigated To PDP page : " + pdpPage.getProdImageName(), driver);
			
			String nameImage = pdpPage.getProdImageName();
		
			if(!Utils.isMobile()) {
				if(pdpPage.getNoOfAlternateImage()>2) {
					pdpPage.clickOnAlternateImages(1);
					
					Log.softAssertThat(!(pdpPage.getProdImageName().equalsIgnoreCase(nameImage)), 
							"The alternative image should be populate in the main image", 
							"The alternative image is populate in the main image", 
							"The alternative image is not populate in the main image", driver);
				}else {
					Log.message(i++ + ". Alternate images are not available in "+pdpPage.getProductName(), driver);
				}
			}
			
			pdpPage.selectColor(prdLimitedStock[1]);
			Log.message(i++ +". Selected Color :: " + prdLimitedStock[1]);
			
			pdpPage.selectSize(prdLimitedStock[2]);
			Log.message(i++ +". Selected Size :: " + prdLimitedStock[2]);
			
			Log.softAssertThat(pdpPage.verifyDisplayImageMatchesSwatchColor(),
					"The system should display the selected colorized image on the main image area.", 
					"The system is displayed the selected colorized image on the main image area.",
					"The system is not displayed the selected colorized image on the main image area.", driver);
            
            int stockValue = pdpPage.getStockValuefrmInventoryMsg();
            if(stockValue == 0) {
            	Log.reference(i++ + ". Given product have out of stock", driver);
            } else {
            	String inventoryMessage = inventoryMessageTxt.replace("#", Integer.toString(stockValue));
            
	            Log.softAssertThat((stockValue <= 5) && pdpPage.elementLayer.isElementsDisplayed(Arrays.asList("inventoryState"), pdpPage),
	            		"Limited inventory state message should be displayed only when available quantity is less than ATS value (i.e. '5')",
	            		"Limited inventory state message should display only when available quantity is less than ATS value (i.e. '5')",
	            		"Limited inventory state message should display only when available quantity is less than ATS value (i.e. '5')", driver);
	            
	            Log.softAssertThat(pdpPage.elementLayer.getElementText("inventoryState", pdpPage).equalsIgnoreCase(inventoryMessage),
	            		"Inventory message should be displayed with a prefix “Only {# of ATS units} left, In Stock”",
	            		"Inventory message is displayed with a prefix “Only {# of ATS units} left, In Stock”",
	            		"Inventory message is not display with a prefix “Only {# of ATS units} left, In Stock”", driver);
	            
	            Log.softAssertThat(pdpPage.verifyQuantityMatches(stockValue),
	            		"QTY should be displayed (# of ATS units ) in the dropdown list",
	            		"QTY should is displayed (# of ATS units ) in the dropdown list",
	            		"QTY should is not displayed (# of ATS units ) in the dropdown list", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("inventoryState"), pdpPage),
						"Inventory message should be displayed!",
						"Inventory message is displayed!",
						"Inventory message is not displayed!", driver);
            }
            
            if(Utils.isDesktop()) {
            	//Mouse hover on color swatch verification covered in C24800
            	
            	pdpPage.mouseHoverPrimaryImage();
            	Log.message(i++ + ". Mouse hovered on the product primary image", driver);
            	
            	Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("divZoomImage"), pdpPage),
            			"Transparent overlay will be displayed on the left hand side.",
            			"Transparent overlay is displayed on the left hand side.",
            			"Transparent overlay is not displayed on the left hand side.", driver);
            	
            	Log.softAssertThat(pdpPage.verifyTopOfZoomedImageAllignsWithTopOfProductImage() 
            			&& pdpPage.verifyZoomedImageDisplayedRightOfPrimaryProductImage() 
            			&& pdpPage.verifyZoomImageFitWithProductDetailSection(),
            			"Zoom image will be displayed on the right hand side.",
            			"Zoom image is displayed on the right hand side.",
            			"Zoom image is not displayed on the right hand side.", driver);
			}
			
            pdpPage.selectQty(String.valueOf(stockValue));
            Log.message(i++ + ". Updated quantity for Product.", driver);
            
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart.", driver);
			
			//Step 6 - Navigate to the cart page
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated To Shopping Bag page", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementColor("inventoryMsg", errColorCode, cartPage),
					"System should display the Limited Inventory message in yellow font color",
					"System should display the Limited Inventory message in yellow font color",
					"System should display the Limited Inventory message in yellow font color", driver);
			
			cartPage.clickOnProductNameByID(prdLimitedStock[0]);
			Log.message(i++ + ". Navigated To PDP page", driver);
			
			pdpPage.selectQty(String.valueOf(stockValue));
            Log.message(i++ + ". Updated quantity for Product.", driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart.", driver);
			
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated To Shopping Bag page", driver);
			
			Log.softAssertThat(cartPage.verifyInventoryMsgBelowQty(prdLimitedStock[0]),
					"Limited Inventory should be displayed below the Quantity field in the cart page",
					"Limited Inventory is displayed below the Quantity field in the cart page",
					"Limited Inventory is not displayed below the Quantity field in the cart page", driver);
			
			String qtyToUpdate = Integer.toString(stockValue);
			String updatedQty = cartPage.updateQuantityByPrdID(prdLimitedStock[0], qtyToUpdate);
			Log.message(i++ +". Product qty is updated to " + updatedQty, driver);
			
			Log.softAssertThat(cartPage.verifyDisabledQtyUpdateArrow(prdLimitedStock[0], Direction.Up),
					"The Quantity selector should be disabled the increment arrow when the User arrives at the value that is equal/more than the ATS value",
					"The Quantity selector is disabled the increment arrow when the User arrives at the value that is equal/more than the ATS value",
					"The Quantity selector is not disabled the increment arrow when the User arrives at the value that is equal/more than the ATS value", driver);
			
			cartPage.updateQuantityByPrdID(prdLimitedStock[0], "1");
			Log.message(i++ +". Product qty is updated to 1", driver);
			
			//Step 7 - The system navigates the user to checkout sign in page
			SignIn signIn = (SignIn) cartPage.navigateToCheckout();
			
			signIn.enterEmailID(username);
			Log.message(i++ +". Entered email Address",driver);
			
			signIn.typeOnPassword(password);
			Log.message(i++ +". Entered password",driver);
			
			//Step 8 - Navigate to Checkout page - step 3 - (Express checkout)
			CheckoutPage checkoutPg = (CheckoutPage) signIn.clickSignInButton();
			Log.message(i++ +". Clicked sign in button", driver);	
			
			Log.softAssertThat(checkoutPg.getValueFromSavedAddressDropdown().toLowerCase().contains("default"),
					"The Default Address should be displayed in Shipping Address drop box",
					"The Default Address is displayed in Shipping Address drop box",
					"The Default Address is not displayed in Shipping Address drop box",driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("shippingAddressNickNamebold"), checkoutPg)||
					checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("savedAddressNickNameMobile"), checkoutPg),
					"City name (Default) text should be displayed in bold in shipping Address drop box",
					"City name (Default) text is displayed in bold in shipping Address drop box",
					"City name (Default) text is not displayed in bold in shipping Address drop box", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver,"savedAddressCount","shippingAddressNickNamebold", checkoutPg) ||
					checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver,"savedAddressCount","savedAddressNickNameMobile", checkoutPg),
					"No of saved address count should be displayed above the shipping address drop box",
					"No of saved address count is displayed above the shipping address drop box",
					"No of saved address count is not displayed above the shipping address drop box", driver);
						
			Log.softAssertThat(checkoutPg.elementLayer.verifyTextContains("txtBillingaddressNickName", "default", checkoutPg) ||
					checkoutPg.elementLayer.verifyTextContains("txtBillingaddressNickNameMob", "default", checkoutPg),
					"The Default Address should be displayed in Billing Address drop box",
					"The Default Address is displayed in Billing Address drop box",
					"The Default Address is not displayed in Billing Address drop box",driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("txtBillingaddressNickName"), checkoutPg)|| 
					checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("txtBillingaddressNickNameMob"), checkoutPg),
					"City name (Default) text should be displayed in bold in Billing Address drop box",
					"City name (Default) text is displayed in bold in Billing Address drop box",
					"City name (Default) text is not displayed in bold in Billing Address drop box", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver,"billingAddressSavedCount", "txtBillingaddressNickName",  checkoutPg)||
					checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver,"billingAddressSavedCount", "txtBillingaddressNickNameMob",  checkoutPg),
					"No of saved address count should be displayed above the Billing address drop box",
					"No of saved address count should be displayed above the Billing address drop box",
					"No of saved address count should be displayed above the Billing address drop box", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("lblSelectedCard"), checkoutPg),
					"the default Credit card should be displaying as selected",
					"the default Credit card is displaying as selected",
					"the default Credit card is not displaying as selected", driver);
				
			checkoutPg.clickOnPlaceOrder();
			Log.message(i++ +". Clicked place order button", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("lblTxtCvvNoError"),checkoutPg),
					"The system should display an error message",
					"The system is displayed an error message",
					"The system is not displayed an error message", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			checkoutPg.enterCVV("333");
			Log.message(i++ +". Entered Cvv number",driver);
						
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt = checkoutPg.clickOnOrderSummaryPlaceOrderButton();
			Log.message(i++ + ". Clicked on order summary place order button", driver);
			
			//step 9 - Order confirmation page
			Log.softAssertThat(receipt != null, 
					"Verify user is taken to order confirmation page.",
					"User is taken to order confirmation page.",
					"User is not taken to order confirmation page.", driver);
			
			if(receipt != null) {
				Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()), 
						"Order number is generated automatically when order placed",
						"Order is placed and order number generated",
						"Order number is not generated", driver);
				
				Log.softAssertThat(receipt.checkEnteredShippingAddressReflectedInOrderReceipt(checkoutAdd), 
						"Same shipping address should display in the receipt which is entered in checkout shipping detail", 
						"Same shipping address is displaying", 
						"Different shipping address is displaying", driver);
				
				Log.softAssertThat(receipt.checkEnteredBillingAddressReflectedInOrderReceipt(checkoutAdd), 
						"Same billing address should display in the receipt which is entered in checkout billing detail", 
						"Same billing address is displaying", 
						"Different billing address is displaying", driver);
				
				Log.softAssertThat(receipt.comparePaymentAfterOrderWithEnteredPaymentMethod(payment), 
						"Same payment method should display in the receipt which is entered in checkout payment section", 
						"Same payment method is displaying", 
						"Different payment method is displaying", driver);
				
				Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblOrderReceipt", username, receipt), 
						"The order receipt should send to the entered mail ID", 
						"The order receipt is send to the entered mail ID", 
						"The order receipt is not send to the entered mail ID", driver);
				
				Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt), 
						"The user can able to take print out of the order receipt", 
						"The user can able to print the order receipt", 
						"The user cannot able to print the order receipt", driver);
				
				HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
				
				Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
						"The product added should be present in the receipt", 
						"The product added is present in the receipt", 
						"The product added is not present in the receipt", driver);
				
				String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
				
				Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
						"The order total should display correctly", 
						"The order total is displaying correctly", 
						"The order total is not displaying correctly", driver);
			} else {
				Log.failsoft("Order confirmation page is not loaded. Further verificartion is not possible.");
			}
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally
	}//M1_FBB_E2E_C24401
}//TC_FBB_E2E_C24401