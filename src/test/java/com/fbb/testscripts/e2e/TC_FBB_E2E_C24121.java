package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PaypalConfirmationPage;
import com.fbb.pages.PaypalPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24121 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24121(String browser) throws Exception
	{
		Log.testCaseInfo(); 
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{
			//Step 1 - Navigate to the website

			String productBackOrdered = TestData.get("prd_back-order");
			String paypalemail = accountData.get("credential_paypal").split("\\|")[0];
			String paypalpassword = accountData.get("credential_paypal").split("\\|")[1];
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Click on search field in the header
			
			//Step 3 - Navigate to Product detail page
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(productBackOrdered.split("\\|")[0]);
			Log.message(i++ + ". Navigated To PDP without selecting variation: " + pdpPage.getProductName(), driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementEmpty("txtProdAvailStatus", pdpPage), 
					"Product availability should not displayed", 
					"Product availability is not displayed", 
					"Product availability is displayed", driver);
			
			pdpPage = homePage.headers.navigateToPDP(productBackOrdered);
			Log.message(i++ + ". Navigated To PDP with variation selected.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("txtProdAvailStatus"), pdpPage)
						&& !(pdpPage.elementLayer.verifyElementEmpty("txtProdAvailStatus", pdpPage)), 
					"Product availability should be displayed", 
					"Product availability is displayed", 
					"Product availability is not displayed", driver);
			
			Log.softAssertThat(pdpPage.getProdAvailStatus().toLowerCase().contains("Expected to ship by".toLowerCase()), 
					"Back order availability status should correctly displayed", 
					"Back order availability status is correctly displayed", 
					"Back order availability status is not correctly displayed", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtProdAvailStatus", "btnAddToCart", pdpPage), 
					"The Inventory state should display above the 'Add to Bag' button", 
					"The Inventory state is display above the 'Add to Bag' button", 
					"The Inventory state is not display above the 'Add to Bag' button", driver);
			
			Log.softAssertThat(pdpPage.verifyNewProdImageLoaded(1), 
					"Alternate image should be displayed in the main image area.", driver);
							
			//Step 4 - Navigate to the cart page
			
			pdpPage.selectAllSwatches();
			Log.message(i++ +". Selected all swatches", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ +". Product added to cart", driver);
			
			ShoppingBagPage shoppingBagPg = pdpPage.navigateToShoppingBag();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			//Step 5 - Navigate to the Pay pal sign in page
			
			PaypalPage paypalPage = shoppingBagPg.clickOnPaypalButton();
			Log.message(i++ + ". Clicked on Paypal button in shopping bag page", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
	
			PaypalConfirmationPage pcp = paypalPage.enterPayapalCredentials(paypalemail, paypalpassword);
			Log.message(i++ + ". Continued with Paypal Credentials.", driver);
			
			CheckoutPage checkoutPg = pcp.clickContinueConfirmation();
			Log.message(i++ + ". Clicked on Continue button.", driver);
	
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg), 
					"Checkout page should be displayed!", 
					"Checkout page is displayed",
					"Checkout page is not displayed", driver);
			
			//Step 7 - Order confirmation page
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24121