package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.LinkedHashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24596 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	
	@Test(groups = { "myaccount", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24596(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			
			String userEMailId = AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = userEMailId + "|" + password;
			String address1 = "valid_address1";
			String address2 = "taxless_address";
			String addressDetails1 = checkoutData.get(address1);
			String addressDetails2 = checkoutData.get(address2);
			
			{
				GlobalNavigation.registerNewUser(driver, 0, 0, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//step 1 - Verify the components in My Account Address page
			
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(AccountUtils.generateEmail(driver), accountData.get("password_global"));
			Log.message(i++ + ". Navigated to My Account page as : " +AccountUtils.generateEmail(driver), driver);
			
			AddressesPage addrPg = myAcc.navigateToAddressPage();
			
			Log.softAssertThat(addrPg.elementLayer.verifyElementDisplayed(Arrays.asList("inputAddressFirstName","inputAddressLastName","inputAddressPhone","inputAddressLine1","inputAddressPostal","inputAddressCity"), addrPg),
					"'Address Nickname','First Name','Last Name','Phone Number','Country','Address Line 1 and 2','Zip Code','State','City' Fields should display",
					"'Address Nickname','First Name','Last Name','Phone Number','Country','Address Line 1 and 2','Zip Code','State','City' Fields is displayed",
					"Some fields are not displayed", driver);
			
			//Step 2 -Verify the address fields
			
			addrPg.clickSaveAddressForError();
			Log.message(i++ + ". Clicked on save button!",driver);
			
			Log.softAssertThat(addrPg.elementLayer.verifyElementDisplayed(Arrays.asList("errorFirstName","errorLastName","errorAddress1","errorMsgPhoneNo","errorPostalCodeInvalid","errorCity","errorMsgAddressValidation"), addrPg),
					"If user tries to leave the field blank appropriate error message should be displayed",
					"If user tries to leave the field blank appropriate error message is displayed",
					"If user tries to leave the field blank appropriate error message is not displayed", driver);
			
			addrPg.enterPostal(addressDetails1.split("\\|")[4]);
			Log.message(i++ + ". Entered postal code!",driver);
			
			String populatedCity = addrPg.getEnteredCity();
			Log.softAssertThat(populatedCity.contains(addressDetails1.split("\\|")[2]),
					"City should be automatically populated when entering postal code",
					"City is automatically populated when entering postal code",
					"City is not automatically populated when entering postal code", driver);
			
			addrPg.clickSaveAddressForError();
			Log.message(i++ + ". Clicked on save button!",driver);
			
			Log.softAssertThat(addrPg.elementLayer.verifyElementDisplayed(Arrays.asList("errorMsgAddressValidation"), addrPg),
					"On unsuccessful validation the System should display the appropriate error message",
					"On unsuccessful validation the System is displaying the appropriate error message",
					"On unsuccessful validation the System is not displaying the appropriate error message", driver);
			
			LinkedHashMap<String, String> enteredAddressDetail = addrPg.fillingAddressDetailsWithoutEnteringNickName(address1, true);
			Log.message(i++ + ". Filled in the address",driver);
			
			addrPg.clickSaveAddress();
			Log.message(i++ + ". Clicked on save button!",driver);
			
			Log.softAssertThat(addrPg.elementLayer.verifyElementDisplayed(Arrays.asList("defaultAddress"), addrPg),
					"On successful validation the System should save the address",
					"On successful validation the System saves the address",
					"On successful validation the System not saves the address", driver);
			
			String title = enteredAddressDetail.get("city").trim().toLowerCase();
			
			Log.softAssertThat(addrPg.getDefaultAddressTitle().trim().toLowerCase().contains(title),
					"If Nick name left empty 'city' name should be the nick name",
					"If Nick name left empty 'city' name is displaying as nick name",
					"If Nick name left empty 'city' name is not displaying as nick name", driver);
			
			//step 3 -Verify Add new Address link
			
			addrPg.clickOnAddNewAddress();
			Log.message(i++ + ". Clicked on add new address button!",driver);
			
			Log.softAssertThat(addrPg.elementLayer.verifyElementDisplayed(Arrays.asList("inputAddressFirstName","inputAddressLastName","inputAddressPhone","inputAddressLine1","inputAddressPostal","inputAddressCity"), addrPg),
					"'Address Nickname','First Name','Last Name','Phone Number','Country','Address Line 1 and 2','Zip Code','State','City' Fields should display on clicking add new adderess",
					"'Address Nickname','First Name','Last Name','Phone Number','Country','Address Line 1 and 2','Zip Code','State','City' Fields is displayed on clicking add new adderess",
					"Some fields are not displayed on clicking add new adderess", driver);
			
			addrPg.enterPostal(addressDetails2.split("\\|")[4]);
			Log.message(i++ + ". Entered postal code!",driver);
			
			String populatedCity1 = addrPg.getEnteredCity();
			Log.softAssertThat(populatedCity1.contains(addressDetails2.split("\\|")[2]),
					"City should be automatically populated when entering postal code",
					"City is automatically populated when entering postal code",
					"City is not automatically populated when entering postal code", driver);
			
			addrPg.clickSaveAddressForError();
			Log.message(i++ + ". Clicked on save button!",driver);
			
			Log.softAssertThat(addrPg.elementLayer.verifyElementDisplayed(Arrays.asList("errorMsgAddressValidation"), addrPg),
					"On unsuccessful validation the System should display the appropriate error message",
					"On unsuccessful validation the System is displaying the appropriate error message",
					"On unsuccessful validation the System is not displaying the appropriate error message", driver);
			
			addrPg.clickOnAddNewAddress();
			Log.message(i++ + ". Clicked on add new address button!",driver);
			
			enteredAddressDetail = addrPg.fillingAddressDetailsWithoutEnteringNickName(address2, true);
			Log.message(i++ + ". Filling the address!",driver);
			
			addrPg.clickSaveAddress();
			Log.message(i++ + ". Clicked on save button!",driver);
			
			Log.softAssertThat(addrPg.elementLayer.verifyElementDisplayed(Arrays.asList("defaultAddress"), addrPg),
					"On successful validation the System should save the address",
					"On successful validation the System saves the address",
					"On successful validation the System not saves the address", driver);
			
			title = enteredAddressDetail.get("city").trim().toLowerCase();
			
			Log.softAssertThat(addrPg.getDefaultAddressTitle().trim().toLowerCase().contains(title),
					"If Nick name left empty 'city' name should be the nick name",
					"If Nick name left empty 'city' name is displaying as nick name",
					"If Nick name left empty 'city' name is not displaying as nick name", driver);
			
			addrPg.clickOnAddNewAddress();
			Log.message(i++ + ". Clicked on add new address button!",driver);
			
			enteredAddressDetail = addrPg.fillingAddressDetailsWithoutEnteringNickName(address2, true);
			Log.message(i++ + ". Filling the address!",driver);
			
			addrPg.clickSaveAddress();
			Log.message(i++ + ". Clicked on save button!",driver);
			
			title = enteredAddressDetail.get("city").trim().toLowerCase();
			
			Log.softAssertThat(addrPg.getDefaultAddressTitle().trim().toLowerCase().contains(title)
							&& addrPg.getDefaultAddressTitle().trim().toLowerCase().endsWith("-1"),
					"If Nick name have same 'city' name then '-1' should append with the nickname",
					"If Nick name have same 'city' name then '-1' is append with the nickname",
					"If Nick name have same 'city' name then '-1' is not append with the nickname", driver);
			
			Log.softAssertThat(addrPg.getDefaultAddressTitle().trim().toLowerCase().contains("-1"),
					"When checking the 'make it default' checkbox system should make last entered address as default address",
					"When checking the 'make it default' checkbox system made last entered address as default address",
					"When checking the 'make it default' checkbox system didnt change last entered address as default address", driver);
			
			//step 4 - Verify the functionality of Edit
			
			addrPg.clickAddressEdit(1);
			Log.message(i++ + ". Clicked on edit link!",driver);
			
			addrPg.enterCity("Los Angeles");
			Log.message(i++ + ". Entered city!",driver);
			
			addrPg.clickOnUpdateButton();
			Log.message(i++ + ". Clicked on update button!",driver);
			
			Log.softAssertThat(addrPg.elementLayer.verifyParticularListElementTextContains("listOfAddressLocation", 1 , "Los Angeles", addrPg),
					"City should be changed when editing the city name",
					"City is changed when editing the city name",
					"City is not changed when editing the city name", driver);
			
			String address = addrPg.getAddressInfo(1);
			
			addrPg.clickAddressEdit(1);
			Log.message(i++ + ". Clicked on edit link!",driver);
			
			addrPg.enterCity("Venice");
			Log.message(i++ + ". Entered city!",driver);
			
			addrPg.clickCancelAddressCreation();
			Log.message(i++ + ". Clicked on update button!",driver);
			
			Log.softAssertThat(addrPg.elementLayer.verifyParticularListElementTextContains("txtAddressInfo", 1 , address, addrPg),
					"Cancelling the edit should not change the address",
					"Cancelling the edit is not changing the address",
					"Cancelling the edit is changing the address", driver);
			
			int addrCount = addrPg.getSavedAddressesCount();
			
			//Step 5 - Verify the functionality of Delete
			
			addrPg.clickDeleteAddress(1);
			Log.message(i++ + ". Delete address!",driver);
			
			Log.softAssertThat(addrPg.elementLayer.verifyElementDisplayed(Arrays.asList("modalDeleteControl"), addrPg),
					"On clicking delete 'Confirmation modal' should get displayed",
					"On clicking delete 'Confirmation modal' is getting displayed",
					"On clicking delete 'Confirmation modal' is not getting displayed", driver);
			
			addrPg.clickCancelOnConfirmation(1);
			Log.message(i++ + ". Click cancel in delete confirmation modal!",driver);
			
			Log.softAssertThat(addrCount == addrPg.getSavedAddressesCount(),
					"Address should not get deleted",
					"Address is not deleted",
					"Address is deleted", driver);
			
			addrCount = addrPg.getSavedAddressesCount();
				
			addrPg.clickDeleteAddress(1);
			Log.message(i++ + ". Delete address!",driver);
			
			Log.softAssertThat(addrPg.elementLayer.verifyElementDisplayed(Arrays.asList("modalDeleteControl"), addrPg),
					"On clicking delete 'Confirmation modal' should get displayed",
					"On clicking delete 'Confirmation modal' is getting displayed",
					"On clicking delete 'Confirmation modal' is not getting displayed", driver);
			
			addrPg.clickYesOnConfirmation(1);
			Log.message(i++ + ". Click YES in delete confirmation modal!",driver);
			
			Log.softAssertThat(addrCount > addrPg.getSavedAddressesCount(),
					"Address should get deleted",
					"Address is deleted",
					"Address is not deleted", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24125