package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24587 extends BaseTest{
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");

	@Test(groups= { "plp", "mobile" }, dataProviderClass= DataProviderUtils.class , dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24587(String browser) throws Exception {
		Log.testCaseInfo(); 

		final WebDriver driver =  WebDriverFactory.get(browser);
		int i=1;
		try{
			
			String level_with_moreColor = TestData.get("level_with_moreColor");
			String lvl_Clearance = TestData.get("level_with_clearence").split("\\|")[0];
			String lvl1_Promomsg = TestData.get("level_with_promomsg").split("\\|")[0];
			String lvl2_Promomsg = TestData.get("level_with_promomsg").split("\\|")[1];
			String lvl1_Prdmsg = TestData.get("level_with_specialprdmsg").split("\\|")[0];
			String lvl2_Prdmsg = TestData.get("level_with_specialprdmsg").split("\\|")[1];
			String[] lvl1_Prdbadge = TestData.get("level_with_badge").split("\\|");			
			String sortOrder = demandwareData.get("sortOrder");
			
			HomePage homePage= new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Home Page launched successfully ", driver);
			Headers headers = homePage.headers;
			PlpPage plpPage = homePage.headers.navigateTo(level_with_moreColor);
			Log.message(i++ +". Navigated to PLP ", driver);
			
			//step-1 Verify the number of products displayed in Product Listing Page on initial load
			int plpCount = plpPage.getSearchResultCount();
			if(plpCount > 60) {
				Log.softAssertThat(plpPage.getProductTileCount() == 60,
						"Product Listing Page should show only up to 60 products on initial load. ", 
						"Product Listing Page is shown only up to 60 products on initial load. ",
						"Product Listing Page is not shown up to 60 products on initial load. ", driver);
			} else {
				Log.reference(". PLP have only: " + plpCount);	
			}
			//Step-2 Verify the component in PLP page
			Log.softAssertThat(plpPage.getNumberOfProductTilesPerRow() == 2,
					"Only 2 product tiles should be displayed per row.", 
					"Only 2 product tiles is displayed per row.",
					"Only 2 product tiles is not displayed per row.", driver);
						
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProductImages", "txtProductName","txtProdPrice1", "SwatchColor","lnkMoreColors_Mobile" ), plpPage),
					"'Product Image', 'Product Name', 'Product Pricing ', 'Color Swatches', 'More Colors Indicator' should display",
					"'Product Image', 'Product Name', 'Product Pricing ', 'Color Swatches', 'More Colors Indicator' is displayed",
					"'Product Image', 'Product Name', 'Product Pricing ', 'Color Swatches', 'More Colors Indicator' is not displayed", driver);
			
			Object[] obj = plpPage.checkProdImgIsEqualToColorSwatchImage();
			
			boolean primaryImgIsEqual = (boolean) obj[0];
			
			Log.softAssertThat(primaryImgIsEqual,
					"The system should display the selected colorized image on the main image area.", 
					"The system is displayed the selected colorized image on the main image area.",
					"The system is not displayed the selected colorized image on the main image area.", driver);
			
			Log.softAssertThat(plpPage.verifyMoreLessColorsAvailability(),
					"+ COLORS should be displayed only if the product has more than 6 color swatches in the product list page.",
					"+ COLORS is displayed only if the product has more than 6 color swatches in the product list page.",
					"+ COLORS is not displayed only if the product has more than 6 color swatches in the product list page.", driver);
			
			plpPage.clickOnViewMoreORShowLessColorsLink(0, "more colours");
			Log.message(i++ +".  Clicked on  View More Colors link",driver);
			
			String txtLess, txtMore;
			if(BrandUtils.isBrand(Brand.rm) || BrandUtils.isBrand(Brand.jl)) {
				txtLess = "SHOW LESS";
				txtMore = "+ COLORS";
			} else {
				txtLess = "- LESS COLORS";
				txtMore = "+ MORE COLORS";
			}
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementTextContains("colorSwatchShowLess", txtLess, plpPage),
					"On-tap of '+ colors' the tile expands and It overlaps on top of the row below. '+ Colors' updates to “SHOW LESS”", 
					"On-tap of '+ colors' the tile is expanded and It overlaps on top of the row below. '+ Colors' is updates to “SHOW LESS”",
					"On-tap of '+ colors' the tile is not expanded and It overlaps on top of the row below. '+ Colors' is not updates to “SHOW LESS”", driver);
			
			plpPage.clickOnViewMoreORShowLessColorsLink(0, "less colours");
			Log.message(i++ +".  Clicekd Show less colors link",driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementTextContains("colorSwatchShowMore", txtMore, plpPage),
					"On-tap “SHOW LESS”, close additional swatches and return button to + colors state.", 
					"On-tap “SHOW LESS”, closed additional swatches and return button to + colors state.",
					"On-tap “SHOW LESS”, not closed additional swatches and not return button to + colors state.", driver);
						
			//Step-3 Go to BM & setup some products to verify the following elements on PLP
			//To Verify promotional message
			if(lvl2_Promomsg.trim().contains("View All")) {				
				headers.navigateTo(lvl1_Promomsg);
				Log.message(i++ +". Navigated to PLP", driver);
			}else {
				headers.navigateTo(lvl1_Promomsg, lvl2_Promomsg);
				Log.message(i++ +". Navigated to PLP", driver);
			}
			
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("txtProdPromoMessage"), plpPage)
							|| plpPage.verifyProdMsgInPLP("promotion"),
					"Promoional Messaging should be displayed",
					"Promoional Messaging is displayed",
					"Promoional Messaging is not displayed", driver);
			
			// To Verify Product message
			if(lvl2_Prdmsg.trim().contains("View All")) {
				headers.navigateTo(lvl1_Prdmsg);
				Log.message(i++ +". Navigated to PLP", driver);
			
			}else {
				headers.navigateTo(lvl1_Prdmsg, lvl2_Prdmsg);
				Log.message(i++ +". Navigated to PLP", driver);
			}
			if (plpPage.elementLayer.verifyPageElements(Arrays.asList("txtSpecialProductMsg"), plpPage)) {
				Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("txtSpecialProductMsg"), plpPage)
								|| plpPage.verifyProdMsgInPLP("product"),
						"Product Messaging should be displayed",
						"Product Messaging is displayed",
						"Product Messaging is not displayed", driver);
			} else {
				Log.reference("Special Product Message is not configured for the products in current category", driver);
			}
			
			//To verify Product badge
			if(lvl1_Prdbadge[1].trim().contains("View All")) {
				headers.navigateTo(lvl1_Prdbadge[0]);
				Log.message(i++ +". Navigated to PLP", driver);
			}else {
				headers.navigateTo(lvl1_Prdbadge[0], lvl1_Prdbadge[1]);
				Log.message(i++ +". Navigated to PLP", driver);
			}
			
			if(plpPage.elementLayer.verifyPageElements(Arrays.asList("imgProductBadge"),plpPage) ||
					plpPage.verifyProdMsgInPLP("badge")) {
				 Log.softAssertThat(plpPage.verifyBadgeImageLocation(),
						"Product badge should be displayed",
						"Product badge is displayed",
						"Product badge is not displayed", driver);
			}else {				
				Log.failsoft("Product badge is not available in this page", driver);
			}
			
			headers.navigateTo(lvl_Clearance);
			Log.message(i++ +". Navigated to clearance category", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("txtClearencePromoMessage"), plpPage),
					"Clearance Messaging should be displayed",
					"Clearance Messaging is displayed",
					"Clearance Messaging is not displayed", driver);
			
			//Step-4 Verify the display of Breadcrumb in the Product Listing Page
			
			Log.softAssertThat(plpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divCurrentBreadCrumbMobile", "divContentSlotHeader", plpPage), 
					"Alignment of a breadcrumb should be displayed above the content slot",
					"Alignment of a breadcrumb is displayed above the content slot", 
					"Alignment of a breadcrumb is not displayed above the content slot", driver);
		
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("lnkArrowBreadCrumb_mobile"), headers), 
					"Brand category root should be displayed as '<'.", 
					"Brand category root is displayed as '<'.", 
					"Brand category root is not displayed as '<'.", driver);
			
			String breadcrumbText= headers.getBreadCrumbText(); 
			Log.softAssertThat(!breadcrumbText.contains(lvl_Clearance),
					"Category name should not be displayed as the breadcrumb value", 
					"Category name is not displayed as the breadcrumb value",
					"Category name is displayed as the breadcrumb value", driver);
			
			String breadcrumb=headers.getBreadCrumbText().toLowerCase();
			if(!breadcrumb.contains("home")) {
				
				headers.clickBackArrowOnBreadCrumbMobile();
				Log.message(i++ +". Tapped the back arrow (<)", driver);
				
				Log.softAssertThat(plpPage.getCategoryName().toLowerCase().contains(breadcrumb), 
						"the user should navigate to the previous category.",
						"the user is navigated to the previous category.",
						"the user is not navigated to the previous category.", driver);
			}else {
				Log.reference("Only \"Home \" category available in breadcrumb");
			}
			
			int productsCount= plpPage.getSearchResultCount();
			
			//Step-5 Click on the downward arrow in Filter By menu and verify attribute refinements
			plpPage.refinements.clickFilterArrowDown();
			Log.message(i++ +". Clicked on the downward arrow", driver);
			
			int refinementWidth= plpPage.refinements.elementLayer.getElementWidth("refinementSection", plpPage.refinements);
			
			int refinementFilterDropDownWidth= plpPage.refinements.elementLayer.getElementWidth("filterExpandedDropdown", plpPage.refinements);
			
			Log.softAssertThat(refinementFilterDropDownWidth==refinementWidth,
					"Drop down width should be 100% that of the viewport", 
					"Drop down width is 100% that of the viewport",
					"Drop down width is not 100% that of the viewport", driver);
			
			List<String> filterOptions = plpPage.refinements.getListFilterOptions();
			int count=1;
			boolean appliedFilter = false;
			if(filterOptions.contains("color")) {
				plpPage.refinements.selectSubRefinementInFilterBy("color", 1);
				Log.message(i++ +". Selected color from filter", driver);
				
				appliedFilter = true;
				plpPage.refinements.clickApplyResultsOnMobile();
				Log.message(i++ +".  Clicked apply Result link", driver);
			}else if(filterOptions.contains("size")) {
				plpPage.refinements.selectSubRefinementInFilterBy("size", 1);
				Log.message(i++ +". Selected size from filter", driver);
				
				appliedFilter = true;
				plpPage.refinements.clickApplyResultsOnMobile();
				Log.message(i++ +".  Clicked apply Result link", driver);
				count=count++;			
			}
			
			if(appliedFilter) {
				int countAfter = plpPage.refinements.getProductCount();
				int filterCount = plpPage.refinements.getAppliedFilterCount();
			
				Log.softAssertThat( countAfter<productsCount && 
						(filterCount==count),
						"Selected values product should be filtered and displayed in the Product list page", 
						"Selected values product should be filtered and displayed in the Product list page",
						"Selected values product should be filtered and displayed in the Product list page", driver);
			} else {
				Log.reference(i++ + "The required filter is not available", driver);
			}
			
			plpPage.refinements.openCloseFilterBy("collapsed");
			
			//Step-6 Verify Sort By bar.
			Log.softAssertThat(plpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "categorySlotBanner", "drpSortByCollapsedMobile", plpPage) &&
					plpPage.refinements.elementLayer.verifyHorizontalAllignmentOfElements(driver, "drpSortByCollapsedMobile", "drpFilterByCollapsed", plpPage.refinements), 
					"The Sort By bar should be located below the content slot & next by the Filter By menu",
					"The Sort By bar is located below the content slot & next by the Filter By menu",
					"The Sort By bar is not located below the content slot & next by the Filter By menu", driver);
			
			plpPage.refinements.openCloseSortBy("expanded");
			Log.message(i++ +".  Opened sort by dropdown", driver);
			
			int refinementSortDropDownWidth = plpPage.refinements.elementLayer.getElementWidth("divSortMenu", plpPage.refinements);
			
			Log.softAssertThat(refinementSortDropDownWidth==refinementWidth,
					"Drop down width should be 100% that of the viewport", 
					"Drop down width is 100% that of the viewport",
					"Drop down width is not 100% that of the viewport", driver);
			
			String sortOption = plpPage.refinements.selectSortBy(sortOrder);
			Log.message(i++ + ". Sort Option(" + sortOption + ") selected from Sorting menu.", driver);

			Log.softAssertThat(plpPage.refinements.verifySortApplied(sortOption),
					"Products should be sorted as per the selected option and listed in PLP",
					"Products sorted as per the selected option",
					"Products not sorted as per the selected sorting option", driver);
			
			String SortText= plpPage.refinements.elementLayer.getElementText("drpSortByCollapsedMobile", plpPage.refinements).toUpperCase();
			
			Log.softAssertThat(!SortText.contains(sortOrder),
					"The selected Sort By option does not display in the Sort By header", 
					"The selected Sort By option  not displayed in the Sort By header",
					"The selected Sort By option ist displayed in the Sort By header", driver);
			
			plpPage.refinements.openCloseSortBy("expanded");
			Log.message(i++ +".  Opened sort by dropdown", driver);
			
			String SortTextInDropBox= plpPage.refinements.elementLayer.getElementText("selectedSortingOptionInDrp", plpPage.refinements).toUpperCase();
			
			Log.softAssertThat(SortTextInDropBox.toLowerCase().contains(sortOrder.toLowerCase())&&
					plpPage.refinements.elementLayer.verifyElementDisplayed(Arrays.asList("selectedSortingOptionInDrp"), plpPage.refinements),
					"The Sort option should appears as checked in the on expanding the dropdown.", 
					"The Sort option is appears as checked in the on expanding the dropdown.",
					"The Sort option is not appears as checked in the on expanding the dropdown.", driver);
			
			plpPage.refinements.openCloseSortBy("collapsed");
			
			//Step-7 Verify Back to Top button
			Log.softAssertThat(plpPage.verifyBackToTopButton(),
					"Back to Top button should be displayed on the right side of the product grid.",
					"Back to Top button is displayed on the right side of the product grid.",
					"Back to Top button is not displayed on the right side of the product grid.", driver);
						
			BrowserActions.scrollToTopOfPage(driver);
			Log.message(i++ +". Scrolled to top of the page", driver);
			
			Log.softAssertThat(plpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("btnBackToTop"),plpPage),
					"Once the user scrolls up 'Back to Top' button then disappears.",
					"Once the user scrolls up 'Back to Top' button is disappears.",
					"Once the user scrolls up 'Back to Top' button is not disappears.", driver);
						
			plpPage.scrollTogetStickyAndBacktoTopButton();
			Log.message(i++ +".   scroll down again & reach in the middle of the page",driver);
			
			plpPage.clickBackToTopButton();
			Log.message(i++ +".   Clicked back to top button",driver);
			
			Log.softAssertThat(!plpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnBackToTop"),plpPage),
					"The user should take back to the top of the page and the button should be removed.",
					"The user should take back to the top of the page and the button is removed.",
					"The user should take back to the top of the page and the button is not removed.", driver);
						
			//Step-8 Verify "View More" button in Product list page
			int totalCount=plpPage.getSearchResultCount();
			
			if(totalCount>60){
				Log.softAssertThat(plpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionSearchResults", "btnViewMoreButton", plpPage),
						"View More button should be displayed below the product grid.", 
						"View More button is displayed below the product grid.",
						"View More button is not displayed below the product grid.", driver);			
			
				int prdTileCount= plpPage.getProductTileCount();
				Log.message("No of products are: "+prdTileCount);
				
				plpPage.clickOnViewMore();
				Log.message(i++ +". Clicked view more link", driver);
				
				if(totalCount-60>60) {
					Log.softAssertThat(plpPage.verifyLoadedProductsHaveImgNameAndprice()==120,
							"The system should load 60 more products and Product image, name & price should be displayed properly on the PLP for new products",
							"The system is loaded 60 more products and Product image, name & price should be displayed properly on the PLP for new products",
							"The system is not loaded 60 more products and Product image, name & price should be displayed properly on the PLP for new products", driver);
				} else {					
					Log.softAssertThat(plpPage.getProductTileCount()==totalCount,						
							"The system should load all products in the PLP", 
							"The system is loaded all products in the PLP",
							"The system is not loaded all products in the PLP", driver);
					
					Log.softAssertThat(plpPage.verifyLoadedProductsHaveImgNameAndprice()==totalCount,
							"Product image, name & price should be displayed properly on the PLP for new products",
							"Product image, name & price should be displayed properly on the PLP for new products",
							"Product image, name & price should be displayed properly on the PLP for new products", driver);
				}
			} else {
				Log.reference("View more functionality not tested due to less number of products");	
			}
			
			Log.reference("Landscape view functionality is need to be developed");
			
			Log.testCaseResult();

		}//End of try
		catch(Exception e){
			Log.exception(e, driver);
		}// End of catch 
		finally{
			Log.endTestCase(driver);
		}//End of finally
	}//M1_FBB_E2E_C24587
}//TC_FBB_C24587
