package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.ReturnOrderPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24551 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");

	@Test( groups = { "checkout", "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24551(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String returnStatus = demandwareData.get("ReturnOrderStatus");
		String credentialReturn = accountData.get("credentialReturnBH");
		String emailIDReturn = credentialReturn.split("\\|")[0];
		String passwordReturn = credentialReturn.split("\\|")[1];
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Navigated to SignIn Page.", driver);
			
			MyAccountPage accountPage = signIn.navigateToMyAccount(emailIDReturn, passwordReturn);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
			
			OrderHistoryPage orderHistory = accountPage.clickOnOrderHistoryLink();
			Log.message(i++ + ". Navigated to Order History Page.", driver);
			
			int orderList = orderHistory.getOrderNumersList().size();
			Log.softAssertThat(orderList>0, 
					"The system should display all the orders number that being placed under this account", 
					"The system is displayed all the orders number that being placed under this account", 
					"The system is not displayed all the orders number that being placed under this account", driver);
			
			Log.softAssertThat(orderHistory.elementLayer.verifyTextContains("returnOrderStatus", returnStatus, orderHistory), 
					"The returned order status should be 'Processed'", 
					"The returned order status is 'Processed'", 
					"The returned order status is not 'Processed'", driver);
			
			ReturnOrderPage returnPage = orderHistory.clickOnReturn();
			Log.message(i++ + ". Clicked on Return Items link", driver);
				
			Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("divReturnPage"), returnPage), 
					"Return page should display", 
					"Return page is displayed", 
					"Return page is not displayed", driver);
			
			Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("eligibleForReturn"), returnPage), 
					"All the Eligible for Returns order should be the list together", 
					"All the Eligible for Returns order is listed together", 
					"All the Eligible for Returns order not the list together", driver);
			
			Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("nonEligibleForReturn"), returnPage), 
					"All the Not Eligible for Returns products should be listed together.", 
					"All the Not Eligible for Returns products are listed together.", 
					"All the Not Eligible for Returns products are not listed together.", driver);
			
			Log.softAssertThat(!(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("nonEligibleForReturnCheckBox"), returnPage)), 
					"Confirm the return checkbox should not displaying for Not Eligible products.", 
					"Confirm the return checkbox is not displaying for Not Eligible products.", 
					"Confirm the return checkbox is displaying for Not Eligible products.", driver);
			
			//Step 2: Verify the Heavy Item which is Eligible for Returns
			
			returnPage.clickCheckboxBasedOnIndex(0);
			Log.message(i++ + ". Selected checkbox of Heavy item", driver);
			
			returnPage.slectReturnQtyBasedOnProductIndexAndQtyIndex(0, 0);
			Log.message(i++ + ". Selected Return Qty", driver);
			
			returnPage.slectReturnReasonBasedOnProductIndexAndReasonIndex(0, 4);
			Log.message(i++ + ". Selected Return Reason", driver);
			
			returnPage.clickCheckboxBasedOnIndex(1);
			Log.message(i++ + ". Selected checkbox of Regular return item", driver);
			
			returnPage.slectReturnQtyBasedOnProductIndexAndQtyIndex(1, 0);
			Log.message(i++ + ". Selected Return Qty", driver);
			
			returnPage.slectReturnReasonBasedOnProductIndexAndReasonIndex(1, 2);
			Log.message(i++ + ". Selected Return Reason", driver);
			
			returnPage.clickReturnSelectedItemButton();
			Log.message(i++ + ". Clicked on Return Selected Items button", driver);
			
			Log.softAssertThat(returnPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnReturnSelectedItems", "lblErorMessage", returnPage), 
					"The system should display an error message ", 
					"The system is displayed an error message", 
					"The system not displayed an error message.", driver);
			
			Log.message(i++ + ". The user will not be able to combine a heavy item ( which is Eligible for Return) & regular product together.", driver);
			
			returnPage.uncheckCheckboxBasedOnIndex(1);
			Log.message(i++ + ". Unchecked checkbox of Regular return item", driver);
			
			returnPage.clickReturnSelectedItemButton();
			Log.message(i++ + ". Clicked on Return Selected Items button", driver);
			
			//Step 3: Verify download a return label
			
			Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("divDownloadReturnLablePage"), returnPage), 
					"The system should navigate the user to download a return label page", 
					"The system is navigated the user to download a return label page", 
					"The system is not navigated the user to download a return label page", driver);
			
			returnPage.clickOnDownloadReturnLable();
			Log.message(i++ + ". Clicked on Download Return lable button", driver);
			
			//Step 4: Now go back again on order detail page
			
			orderHistory = returnPage.clickOnOrderHistoryLink();
			Log.message(i++ + ". Clicked on Order History link", driver);
			
			returnPage = orderHistory.clickOnReturn();
			Log.message(i++ + ". Clicked on Return Items link", driver);
			
			returnPage.clickCheckboxBasedOnIndex(1);
			Log.message(i++ + ". Selected checkbox of Regular return item", driver);
			
			returnPage.slectReturnQtyBasedOnProductIndexAndQtyIndex(1, 0);
			Log.message(i++ + ". Selected Return Qty", driver);
			
			returnPage.slectReturnReasonBasedOnProductIndexAndReasonIndex(1, 2);
			Log.message(i++ + ". Selected Return Reason", driver);
			
			returnPage.clickReturnSelectedItemButton();
			Log.message(i++ + ". Clicked on Return Selected Items button", driver);
			
			Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("divDownloadReturnLablePage"), returnPage), 
					"The system should navigate the user to download a return label page", 
					"The system is navigated the user to download a return label page", 
					"The system is not navigated the user to download a return label page", driver);
			
			returnPage.clickOnDownloadReturnLable();
			Log.message(i++ + ". Clicked on Download Return lable button", driver);			
				
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_HEADER_C22551

}// search
