package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlatinumCardApplication;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C25903 extends BaseTest {

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C25903(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{
			
			String prdVariation = TestData.get("prd_variation");
			String userEMailId = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountData.get("password_global");
			String checkoutAdd = "taxless_address";
								
			//Always Approve Data
			String addressDetails1 = checkoutData.get("plcc_always_approve_address");
			String ssnDetails = checkoutData.get("SSNdetails");
			HashMap<String, String> userAddressM2 = GlobalNavigation.formatPLCCAddressToMapWithSSN(addressDetails1, ssnDetails, driver, userEMailId);
			
			//Step 1 - Navigate to the website
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - As a guest, click "Apply Now" from the PLCC header.

			PlatinumCardApplication plccApplication = homePage.navigateToPLCCApplication();
			Log.message(i++ + ". Navigated to Platinum Card Application page",driver);
			
			//Step - 3
			plccApplication.fillPLCCApplication(userAddressM2);
			Log.message(i++ +". Entered Always, approve address", driver);
			
			plccApplication.clickCheckUncheckConsent(true);
			plccApplication.clickOnRegisterBtn();
			Log.message(i++ + ". Clicked on SUBMIT button.", driver);
			
			if(plccApplication.elementLayer.verifyElementDisplayed(Arrays.asList("divApprovedModal"), plccApplication)) {		
				Log.softAssertThat(plccApplication.elementLayer.verifyElementDisplayed(Arrays.asList("divApprovedModal"), plccApplication), 
						"PlatinumCard approved modal should get display", 
						"PlatinumCard approved modal is getting display", 
						"PlatinumCard approved modal is not getting display", driver);
				
				//Step - 4
				plccApplication.clickApprovedModalContinueShopping();
				Log.message(i++ + ". Clicked on Continue Shopping button.", driver);
				
				Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
						"Home page should be displayed", 
						"Home page is getting displayed", 
						"Home page is not getting displayed", driver);
				
				//Step - 5
				PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation);
				Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
				
				pdpPage.addToBagCloseOverlay();
				Log.message(i++ + ". Product added to cart", driver);
				
				ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to cart page", driver);
				
				cartPage.updateOrderTotalMeetPlccDiscount(prdVariation);
				Log.message(i++ + ". Updated product quantity to meet PLCC discount", driver);
				
				//Step 6 -Navigate to the checkout sign in page
				
				SignIn signin = (SignIn) cartPage.navigateToCheckout();
				Log.message(i++ + ". Navigated to Checkout page", driver);
				
				CheckoutPage checkoutPg = (CheckoutPage)signin.clickSignInButton();
				Log.message(i++ + ". Clicked Signin button.", driver);
				
				//Step 7 - Entering shipping and billing address.
				
				checkoutPg.continueToShipping(userEMailId);
				Log.message(i++ + ". Navigated to Shipping section", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("billingEditFirstname"), checkoutPg), 
						"The system should navigate the user to checkout page - Step 1", 
						"The system is navigate the user to checkout page - Step 1", 
						"The system is not navigate the user to checkout page - Step 1", driver);
				
				checkoutPg.fillingShippingDetailsAsGuest("NO", checkoutAdd, ShippingMethod.Standard);
				Log.message(i++ + ". Shipping Address entered successfully", driver);
				
				checkoutPg.checkUncheckUseThisAsBillingAddress(true);
				Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
				
				checkoutPg.continueToPayment();
				Log.message(i++ + ". Continued to Payment Page", driver);
				
				if(checkoutPg.getCreditCardType().contains("plcc") || checkoutPg.getCreditCardType().contains("Credit Card")) {
				
					Log.softAssertThat(checkoutPg.getCreditCardType().contains("Credit Card"), 
							"PLCC card should be selected  when PLCC card approved", 
							checkoutPg.getCreditCardType() + " is selected when PLCC card approved", 
							"PLCC card is not selected when PLCC card approved", driver);
					
					Log.softAssertThat(checkoutPg.elementLayer.verifyNumberMaskedWithSpecifiedDigits("fldTxtCardNo", 4, checkoutPg),
								"The masked number and last 4 digits of the PLCC card should be displayed in the \"Card Number\" field.",
								"The masked number and last 4 digits of the PLCC card is displayed in the \"Card Number\" field.",
								"The masked number and last 4 digits of the PLCC card is not displayed in the \"Card Number\" field.", driver);
					
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryDiscount"), checkoutPg),
							"The $10 discount should be displayed in the Order Summary section.", 
							"The $10 discount is displayed in the Order Summary section.",
							"The $10 discount is not displayed in the Order Summary section.", driver);
					
					Log.softAssertThat(checkoutPg.verifyOrderTotalCalculation(),
							"The Order summary should be calculated properly", 
							"The Order summary is calculated properly",
							"The Order summary is not calculated properly", driver);
					
					if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
						Log.reference("Further verfication steps are not supported in current environment.");
						Log.testCaseResult();
						return;
					}
						
					//Step 8 - Navigate to Step 3 - the "Review and Place Order" section.
						
					checkoutPg.clickOnPaymentDetailsContinueBtn();
					Log.message(i++ + ". Continued to Review & Place Order", driver);
					
					//Step 9 - Place the order.
					checkoutPg.clickOnPlaceOrder();
					Log.message(i++ + ". Clicked on place order button", driver);
					
					if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"),checkoutPg)) {
						Log.reference("By Using always approve card user can't place order", driver);
					} else {
						Log.message(i++ + ". Order Placed successfully", driver);	
							
						//Step 10 - Creating account in order confirmation page.
						OrderConfirmationPage orderPage = new OrderConfirmationPage(driver).get();
						orderPage.enterValueInPassword(password);
						orderPage.enterValueInConfirmPassword(password);
						Log.message(i++ +". Entered details in password and confirma password field", driver);
							
						orderPage.clickOnCreateAccount();	
						Log.message(i++ +". Clicked create account button", driver);
							
						MyAccountPage myAccount = new MyAccountPage(driver).get();
						Log.softAssertThat(myAccount.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAccount),
								"Page should be navigated to my accountoverview page",
								"Page is navigated to my accountoverview page",
								"Page is not navigated to my accountoverview page", driver);
					}
				} else {
					Log.reference("PLCC card is not added, Hence can't proceed further", driver);
				}
			} else {
				Log.reference("PLCC Approval modal is not displayed hence can't proceed further", driver);
			}
			
			Log.testCaseResult();
		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}
