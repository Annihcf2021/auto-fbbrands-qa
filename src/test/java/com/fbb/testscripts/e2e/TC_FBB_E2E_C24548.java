package com.fbb.testscripts.e2e;
import com.fbb.reusablecomponents.TestData;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.ReturnOrderPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.ordering.GuestOrderStatusLandingPage;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24548 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;

	@Test( groups = { "checkout", "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24548(String browser) throws Exception {
		Log.testCaseInfo();
	
		//Load Test Data
		String orderDetails = TestData.get("guest_order_return");
		String orderNumber = orderDetails.split("\\|")[0];
		String orderEmail = orderDetails.split("\\|")[1];
		String billingZipCode = orderDetails.split("\\|")[2];
		String invalidBillingZipCode = "##@$";
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
	
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
	
			SignIn signin = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Navigated to SignIn Page.", driver);
			
			//Step 2: Verify "Check an order" section
			
			signin.clickOnCheckInOrder();
			Log.message(i++ + ". Clicked Check button without entering order number, order email address & billing zip code.", driver);
			
			Log.softAssertThat(signin.elementLayer.verifyElementDisplayed(Arrays.asList("orderNoInvalidErrorMsg", "orderEmailErrorMsg", "orderPostalZipCodeErrorMsg"), signin), 
					"Placeholder for Order number, order email & Billing zip code should become red", 
					"Placeholder for Order number, order email & Billing zip code become red", 
					"Placeholder for Order number, order email & Billing zip code not become red", driver);
			
			Log.softAssertThat(signin.orderBillingZipCodeMissmatch(orderNumber,orderEmail,invalidBillingZipCode),
					"Proper Error message should be displayed for Invalid Zipcode ",
					"Proper Error message displyed for invalid  Invalid Zipcode",
					"No Proper Error message displyed for Invalid Zipcode", driver); 
			
			GuestOrderStatusLandingPage guestOrder = signin.navigateToGuestOrderStatusPage(orderNumber,orderEmail,billingZipCode);
			Log.message(i++ + ". Provide a valid order number, valid order email address & invalid billing zip code.", driver);
			
			if(guestOrder != null) {
				Log.softAssertThat(signin.elementLayer.verifyPageElements(Arrays.asList("lnkguestOrderStatusLandingPage"),signin),
						"While clicking submit  button it should navigated to order detail page",
						"Navigation to order detail page is succesfull",
						"Navigation to order detail page is not succesfull", driver);
				
				// Step 3: The system navigates the user to " check an order" page
				
				Log.softAssertThat(signin.elementLayer.verifyElementDisplayed(Arrays.asList("divOrderNumberPrint"), signin), 
						"Order Number should display", 
						"Order Number is displayed", 
						"Order Number not displayed", driver);
				
				Log.softAssertThat(signin.elementLayer.verifyTextContains("orderNumberValue", orderNumber, signin), 
						"confirm the order number, should be the same order number ( as provided in Step 2)", 
						"confirm the order number is the same order number ( as provided in Step 2)", 
						"confirm the order number is not the same order number ( as provided in Step 2)", driver);
				
				Log.softAssertThat(signin.elementLayer.verifyElementDisplayed(Arrays.asList("divShipment", "divShipmentHeading"), signin), 
						"The item should display under the shipment", 
						"The item is displayed under the shipment", 
						"The item is not displayed under the shipment", driver);
				
				Log.softAssertThat(signin.elementLayer.verifyElementDisplayed(Arrays.asList("lnkTrackMyPackage"), signin)
								|| signin.elementLayer.verifyTextContains("divShipmentStatus", "Not", signin),
						"Each shipment should display a 'track my package' link", 
						"Each shipment is displayed a 'track my package' link, or order has not shipped yet.", 
						"Each shipment is not displayed a 'track my package' link", driver);
				
				signin.clickOnTrackMyPackage();
				Log.message(i++ + ". Clicked on Track My Package link", driver);
				
				//Step 4: The system navigates the user to Order return page
				
				ReturnOrderPage returnPage = signin.clickOnReturn();
				Log.message(i++ + ". Clicked on Return button", driver);
				
				if(returnPage != null) {
					Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("divReturnPage"), returnPage), 
							"Return page should display", 
							"Return page is displayed", 
							"Return page is not displayed", driver);
					
					Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnCancel", "btnReturnSelectedItems"), returnPage), 
							"Cancel & return selected items button should display", 
							"Cancel & return selected items button is displayed", 
							"Cancel & return selected items button not displayed", driver);
					
					returnPage.clickCancelButton();
					Log.message(i++ + ". Clicked on Cancel button", driver);
					
					Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("divOrderDetails"), returnPage), 
							"The system should redirect the user to the previous page", 
							"The system is redirected the user to the previous page", 
							"the system is not redirected the user to the previous page", driver);
					
					returnPage.clickOnReturn();
					Log.message(i++ + ". Clicked on Return button", driver);
					
					Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("divReturnPage"), returnPage), 
							"The system should navigates the user on order return page", 
							"the system is navigated the user on order return page", 
							"the system is not navigated the user on order return page", driver);
					
					returnPage.clickReturnSelectedItemButton();
					Log.message(i++ + ". Clicked on Return Selected Items button", driver);
					
					Log.softAssertThat(returnPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnReturnSelectedItems", "lblErorMessage", returnPage), 
							"The system should display an error message below the Return selected items button.", 
							"The system should display an error message below the Return selected items button.", 
							"The system should display an error message below the Return selected items button.", driver);
					
					//Step 5: Verify the return reason dropdown
					
					returnPage.clickCheckboxBasedOnIndex(0);
					Log.message(i++ + ". Clicked checkbox for return items", driver);
					
					Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("returnQty", "returnReason"), returnPage), 
							"Return QTY & return reason drop-down should display", 
							"Return QTY & return reason drop-down is displayed", 
							"Return QTY & return reason drop-down is not displayed", driver);
					
					returnPage.clickReturnSelectedItemButton();
					Log.message(i++ + ". Clicked on Return Selected Items button", driver);
					
					Log.softAssertThat(returnPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnReturnSelectedItems", "lblErorMessage", returnPage), 
							"The system should display an error message below the Return selected items button.", 
							"The system should display an error message below the Return selected items button.", 
							"The system should display an error message below the Return selected items button.", driver);
					
					returnPage.slectReturnQtyBasedOnProductIndexAndQtyIndex(0, 0);
					Log.message(i++ + ". Selected Return Qty", driver);
					
					returnPage.slectReturnReasonBasedOnProductIndexAndReasonIndex(0, 4);
					Log.message(i++ + ". Selected Return Reason", driver);
					
					returnPage.clickReturnSelectedItemButton();
					Log.message(i++ + ". Clicked on Return Selected Items button", driver);
					
					Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("divDownloadReturnLablePage"), returnPage), 
							"The system should navigate the user to download a return label page", 
							"The system is navigated the user to download a return label page", 
							"The system is not navigated the user to download a return label page", driver);
					
					//Step 6: Verify download a return label
					
					returnPage.clickOnDownloadReturnLable();
					Log.message(i++ + ". Clicked on Download Return lable button", driver);
					
					Log.softAssertThat(returnPage.elementLayer.verifyElementDisplayed(Arrays.asList("textInstructionForReturn"), returnPage), 
							"Instructions For Return/Exchange should display for the user", 
							"Instructions For Return/Exchange is displayed for the user", 
							"Instructions For Return/Exchange is not displayed for the user", driver);	 
				} else {
					Log.reference("Order return is not available. MW data is not exported.");
				}
				
			} else {
				Log.failsoft("Order Not found.", driver);
			}
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_HEADER_C22548

}// search
