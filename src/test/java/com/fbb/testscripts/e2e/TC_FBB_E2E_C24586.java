package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CategoryLandingPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24586 extends BaseTest {
	
	EnvironmentPropertiesReader environmentPropertiesReader;

	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups= {"plp", "tablet" }, dataProviderClass= DataProviderUtils.class , dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24586(String browser) throws Exception {
	
		Log.testCaseInfo(); 

		final WebDriver driver =  WebDriverFactory.get(browser);
		int i=1;
		try{
			
			String lvl_Clearance = TestData.get("level_with_clearence").split("\\|")[0];
			String lvl1_Promomsg = TestData.get("level_with_promomsg").split("\\|")[0];
			String lvl2_Promomsg = TestData.get("level_with_promomsg").split("\\|")[1];
			String lvl1_Prdmsg = TestData.get("level_with_specialprdmsg").split("\\|")[0];
			String lvl2_Prdmsg = TestData.get("level_with_specialprdmsg").split("\\|")[1];
			String lvl1_Prdbadge = TestData.get("level_with_badge").split("\\|")[0];
			String lvl2_Prdbadge = TestData.get("level_with_badge").split("\\|")[1];
			String level1 = TestData.get("level_with_moreColor").split("\\|")[0];
			String level2 = TestData.get("level_with_moreColor").split("\\|")[1];
			String username = AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String sortOrder = demandwareData.get("sortOrder");
			
			{
				GlobalNavigation.registerNewUser(driver, 0, 0, credential);
			}
		
			HomePage homePage= new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Home Page launched successfully ", driver);
			Headers headers = homePage.headers;
			PlpPage plpPage = homePage.headers.navigateTo(level1, level2);
			Log.message(i++ +". Navigated to PLP ", driver);
			
			//Step-1 Verify the number of products displayed in Product Listing Page on initial load
			
			if(plpPage.getSearchResultCount() > 60) {
				Log.softAssertThat(plpPage.getProductTileCount() == 60,
						"Product Listing Page should show only up to 60 products on initial load. ", 
						"Product Listing Page is shown only up to 60 products on initial load. ",
						"Product Listing Page is not shown up to 60 products on initial load. ", driver);
			} else {
				Log.reference("PLP have only: " + plpPage.getSearchResultCount() + " products. Maximum number of products on initial load cannot be verified.");	
			}
			
			//Step-2 Verify the display of Breadcrumb in the Product Listing Page
			
			String breadcrumbText= plpPage.getBreadcrumbFullText(); 
			Log.softAssertThat(breadcrumbText.toLowerCase().contains(level1.toLowerCase()),
					"Category name should be displayed as the breadcrumb value", 
					"Category name is displayed as the breadcrumb value",
					"Category name is not displayed as the breadcrumb value", driver);
		
			Log.softAssertThat(plpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divBreadcrumb", "divContentSlotHeader", plpPage), 
					"Breadcrumb should be displayed on top of the content slot content when populated.",
					"Breadcrumb is displayed on top of the content slot content when populated.", 
					"Breadcrumb is not displayed on top of the content slot content when populated.", driver);
			
			int breadcrumbSize = plpPage.getBreadcrumbcount();
			
			if(breadcrumbSize > 2) {
				for(int cate=breadcrumbSize-1;cate>1;cate--) {
					String category = plpPage.getBreadcrumbValueOf(cate);
					plpPage.clickOnBreadCrumbByIndexValue(cate);
										
					if(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage)) {
						Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage),
								"On clicking "+cate+" th breadcrumb '"+category+"', system should navigates the user to the selected category PLP",
								"On clicking "+cate+" th breadcrumb '"+category+"', system is navigates the user to the selected category PLP", 
								"On clicking "+cate+" th breadcrumb '"+category+"', system is not navigates the user to the selected category PLP", driver);
					} else {
						CategoryLandingPage categoryPage = new CategoryLandingPage(driver).get();
						Log.softAssertThat(categoryPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"),categoryPage),
								"On clicking "+cate+" th breadcrumb '"+category+"', system should navigates the user to the selected category landing Page",
								"On clicking "+cate+" th breadcrumb '"+category+"', system is navigates the user to the selected category landing Page", 
								"On clicking "+cate+" th breadcrumb '"+category+"', system is not navigates the user to the selected category landing Page", driver);
					}
				driver.navigate().back();
				}
			}
			
			//Step-3 Verify the sticky header
			
			Log.softAssertThat(headers.checkGlobalHeaderIsStickyWhenScrollDown(),
					"Once the user scrolled down the system should display the Sticky Header",
					"Once the user scrolled down the system is displayed the Sticky Header",
					"Once the user scrolled down the system is not displayed the Sticky Header", driver);
			
			plpPage.scrollTogetStickyAndBacktoTopButton();
			Log.message(i++ +". Scrolled to get sticky header", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkHamburger", "brandLogoMobTab", "btnHeaderSearchIcon", "iconMyBag", "stckyRefinement" ), headers), 
					"Hamburger menu, Brand logo, Search button, Bag icon and refinement bar should be visible as sticky.", 
					"Hamburger menu, Brand logo, Search button, Bag icon and refinement bar are visible as sticky.", 
					"Hamburger menu, Brand logo, Search button, Bag icon and refinement bar are not visible as sticky.", driver);
		
			headers.mouseOverAccountMenu();
			Log.message(i++ +". From the sticky header tap on 'sign in' icon", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("flytSignInUnregisteredDesktop"), headers), 
					"The system should display 'sign in' flyout", 
					"The system is displayed 'sign in' flyout", 
					"The system is not displayed 'sign in' flyout.", driver);
		
			MyAccountPage myAccount = headers.navigateToMyAccount(username, password);
			Log.message(i++ +". Provide a valid email & password", driver);
			
			Log.softAssertThat(myAccount.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAccount) ,
					"The system navigates the user to My account page",
					"The system is navigates the user to My account page",
					"The system is not navigates the user to My account page", driver);  
			
			if(BrandUtils.isBrand(Brand.bh) || BrandUtils.isBrand(Brand.fb)) {
				plpPage = homePage.headers.navigateTo(level1, level2);
			} else {
				plpPage = homePage.headers.navigateTo(level1);
			}
			Log.message(i++ +". Select a category & navigates to the PLP throug hamburger menu", driver);
			
			//Step-4 Verify the Sort By
			
			Log.softAssertThat(plpPage.refinements.elementLayer.verifyElementDisplayed(Arrays.asList("sortByRefinemnet", "drpFilterByCollapsed"), plpPage.refinements), 
					"'Filter By' menu and 'Sort By' menu should be displayed in Horizontal refinement bar",
					"'Filter By' menu and 'Sort By' menu is displayed in Horizontal refinement bar",
					"'Filter By' menu and 'Sort By' menu is not displayed in Horizontal refinement bar", driver);
			
			Log.softAssertThat(plpPage.refinements.elementLayer.verifyHorizontalAllignmentOfElements(driver, "sortByRefinemnet", "drpFilterByCollapsed", plpPage.refinements), 
					"The Sort By bar should display to the right of the refinement bar",
					"The Sort By bar is displayed to the right of the refinement bar",
					"The Sort By bar is not displayed to the right of the refinement bar", driver);
		
			plpPage.refinements.openCloseSortBy("expanded");
			
			String sortOption = plpPage.refinements.selectSortBy(sortOrder);
			Log.message(i++ + ". Sort Option(" + sortOption + ") selected from Sorting menu.", driver);
			
			Log.softAssertThat(plpPage.refinements.verifySortApplied(sortOrder), 
					"Products should be sorted and displayed in product list page based on the sort by option", 
					"Products are sorted and displayed in product list page based on the sort by option", 
					"Products are not sorted and displayed in product list page based on the sort by option", driver);
			
			// Step-5 Verify the functionality of the filter by
			
			plpPage.refinements.clickFilterArrowDown();
			Log.message(i++ + ". opened Filter By menu.", driver);
		
			String filteroption = plpPage.refinements.getFilterByState();
			
			Log.softAssertThat(filteroption.equalsIgnoreCase("expanded"), 
					"Filter By Menu should be Opened",
					"Filter By Menu is Opened",
					"Filter By Menu is not be Opened", driver);
			
			Log.softAssertThat(!plpPage.refinements.getListFilterOptions().isEmpty(),
					"Filter option should be properly displayed in 'Filter By' drop-down box",
					"Filter option is properly displayed in 'Filter By' drop-down box",
					"Filter option is not properly displayed in 'Filter By' drop-down box", driver);
		
			int countBefore = plpPage.refinements.getProductCount();
			
			List<String> filterOptions = plpPage.getRefinementList();
			
			String[] selected1stOption = plpPage.refinements.selectSubRefinementInFilterBy(filterOptions.get(filterOptions.size()-1), 1);
			Log.message(i++ + ". Selected value from "+filterOptions.get(filterOptions.size()-1)+" dropdown.", driver);
			
			plpPage.refinements.clickOnBackButtonOnMobileAndTab();
			Log.message(i++ + ". Clicked on back button in filter flyout", driver);
						
			if(filterOptions.contains("SIZE")) {
				String[] selectedSize =  plpPage.refinements.selectSubRefinementInFilterBy("Size", 2);
				Log.message(i++ + ". Selected value from size dropdown.", driver);
				
				plpPage.refinements.openCloseFilterBy("collapsed");
				Log.message(i++ +". Closed filter by dropdown", driver);
				
				int countAfter = plpPage.refinements.getProductCount();
				String[]selectedvaues = plpPage.refinements.getSelectedrefinments();
				
				Log.softAssertThat(countAfter < countBefore && 
						((selectedvaues[0].contains(selectedSize[1]) && selectedvaues[1].toUpperCase().contains(selected1stOption[1].toUpperCase()))||
						selectedvaues[0].toUpperCase().contains(selected1stOption[1].toUpperCase()) && selectedvaues[1].contains(selectedSize[1])),
						"Selected values product should be filtered and displayed in the Product list page", 
						"Selected values product should be filtered and displayed in the Product list page",
						"Selected values product should be filtered and displayed in the Product list page", driver);
			} else {
				int countAfter = plpPage.refinements.getProductCount();
				String[]selectedvaues = plpPage.refinements.getSelectedrefinments();
				
				Log.softAssertThat(countAfter < countBefore && 
						(selectedvaues[0].toLowerCase().contains(selected1stOption[1].toLowerCase())),
						"Selected values product should be filtered and displayed in the Product list page", 
						"Selected values product should be filtered and displayed in the Product list page",
						"Selected values product should be filtered and displayed in the Product list page", driver);
			}
			
			Log.softAssertThat(plpPage.refinements.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnClearInActiveRefInTablet", "spanFilteredBy", plpPage.refinements),
					"Clear All link should be displayed next by the selected filter by option", 
					"Clear All link is displayed next by the selected filter by option",
					"Clear All link is not displayed next by the selected filter by option", driver);
			
			int filterselected = plpPage.refinements.getAppliedFilterCount();
			Log.message(i++ +". Number of applied filter count is: " + filterselected, driver);
			
			BrowserActions.scrollToTopOfPage(driver);
			Log.message(i++ +". Scrolled to top of the page", driver);
			
			plpPage.refinements.clickOnClearAllInRefinement();
			Log.message(i++ +". Click on Clear All link on the refinement bar option", driver);
			
			int countAfterClear = plpPage.refinements.getProductCount();
			
			Log.softAssertThat((countAfterClear == countBefore), 
					"Updated products should be displayed in the PLP",
					"Updated products is displayed in the PLP",
					"Updated products is not displayed in the PLP", driver);
			
			Log.softAssertThat(plpPage.refinements.getAppliedFilterCount() == 0,
					"Selected refinement should be removed from Filter by drop down box", 
					"Selected refinement is removed from Filter by drop down box",
					"Selected refinement is not removed from Filter by drop down box", driver);
			
			plpPage.scrollTogetStickyAndBacktoTopButton();
			Log.message(i++ +". Scroll down a little bit", driver);
			
			//Step-6 Verify Back to Top button
			
			Log.softAssertThat(plpPage.elementLayer.verifyInsideElementAlligned("btnBackToTop", "sectionSearchResults", "right", plpPage),
					"Back to Top button should be displayed on the right side of the product grid.",
					"Back to Top button is displayed on the right side of the product grid.",
					"Back to Top button is not displayed on the right side of the product grid.", driver);
					
			BrowserActions.scrollToTopOfPage(driver);
			Log.message(i++ +". Scrolled to top of the page", driver);
			
			Log.softAssertThat(plpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("btnBackToTop"),plpPage),
					"The Top Button should disappears when the user scrolls up passing the trigger point for the top button to display",
					"The Top Button is disappears when the user scrolls up passing the trigger point for the top button to display",
					"The Top Button is not disappears when the user scrolls up passing the trigger point for the top button to display", driver);
						
			plpPage.scrollTogetStickyAndBacktoTopButton();
			Log.message(i++ +". Scroll down again & reach in the middle of the page",driver);
			
			plpPage.clickBackToTopButton();
			Log.softAssertThat(plpPage.elementLayer.verifyAttributeForElement("btnBackToTop", "style", "display: none", plpPage)
							|| !plpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnBackToTop"),plpPage),
					"The user should take back to the top of the page and the button should be removed.",
					"The user should take back to the top of the page and the button is removed.",
					"The user should take back to the top of the page and the button is not removed.", driver);
			
			//Step-7 Verify Product tile in product list page
			
			Log.softAssertThat(plpPage.getNumberOfProductTilesPerRow() == 3,
					"The number of product tiles displayed per row should be 3!",
					"The number of product tiles displayed per row is 3!",
					"The number of product tiles displayed per row is not 3!", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProductImages", "txtProductName","txtProdPrice1", "txtSwatchList","lnkMoreColors_Mobile" ), plpPage),
					"'Product Image', 'Product Name', 'Product Pricing ', 'Color Swatches', 'More Colors Indicator' should display",
					"'Product Image', 'Product Name', 'Product Pricing ', 'Color Swatches', 'More Colors Indicator' is displayed",
					"'Product Image', 'Product Name', 'Product Pricing ', 'Color Swatches', 'More Colors Indicator' is not displayed", driver);
			
			Object[] obj = plpPage.checkProdImgIsEqualToColorSwatchImage();
			
			boolean primaryImgIsEqual = (boolean) obj[0];
			
			Log.softAssertThat(primaryImgIsEqual,
					"The system should display the selected colorized image on the main image area.", 
					"The system is displayed the selected colorized image on the main image area.",
					"The system is not displayed the selected colorized image on the main image area.", driver);
			
			Log.softAssertThat(plpPage.verifyMoreLessColorsAvailability(),
					"+ COLORS should be displayed only if the product has more than 6 color swatches in the product list page.",
					"+ COLORS is displayed only if the product has more than 6 color swatches in the product list page.",
					"+ COLORS is not displayed only if the product has more than 6 color swatches in the product list page.", driver);
			
			int beforeClickWidth = plpPage.getHeightOfProductTile(0, "viewMoreProduct");
			
			plpPage.clickOnViewMoreORShowLessColorsLink(0, "more colours");
			Log.message(i++ +". Clicekd view more colors link",driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("colorSwatchShownMore"),plpPage),
					"On-tap of '+ colors' the tile expands and It overlaps on top of the row below. '+ Colors' updates to “SHOW LESS”", 
					"On-tap of '+ colors' the tile is expanded and It overlaps on top of the row below. '+ Colors' is updates to “SHOW LESS”",
					"On-tap of '+ colors' the tile is not expanded and It overlaps on top of the row below. '+ Colors' is not updates to “SHOW LESS”", driver);
			
			plpPage.clickOnViewMoreORShowLessColorsLink(0, "less colours");
			Log.message(i++ +". Clicked Show less colors link",driver);
			
			Log.softAssertThat(plpPage.getHeightOfProductTile(0, "viewMoreProduct") == beforeClickWidth &&
					plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkMoreColors_Desk_Tab"),plpPage),
					"On-tap “SHOW LESS”, close additional swatches and return button to + colors state.", 
					"On-tap “SHOW LESS”, closed additional swatches and return button to + colors state.",
					"On-tap “SHOW LESS”, not closed additional swatches and not return button to + colors state.", driver);			
			
			//Step-8 Go to BM & setup some products to verify the following elements on PLP
			
			// To Verify Product message
			if(lvl2_Prdmsg.trim().contains("View All")) {
				headers.navigateTo(lvl1_Prdmsg);
				Log.message(i++ +". Navigated to PLP", driver);
			} else {
				headers.navigateTo(lvl1_Prdmsg, lvl2_Prdmsg);
				Log.message(i++ +". Navigated to PLP", driver);
			}
			
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("txtSpecialProductMsg"), plpPage)
							|| plpPage.elementLayer.verifyPageElements(Arrays.asList("txtProdPromoMessage"), plpPage)
							|| plpPage.verifyProdMsgInPLP("product"),
					"Product Messaging should be displayed",
					"Product Messaging is displayed",
					"Product Messaging is not displayed", driver);
			
			//To verify Product badge
			if(lvl2_Prdbadge.trim().contains("View All")) {
				headers.navigateTo(lvl1_Prdbadge);
				Log.message(i++ +". Navigated to PLP", driver);
			} else {
				headers.navigateTo(lvl1_Prdbadge, lvl2_Prdbadge);
				Log.message(i++ +". Navigated to PLP", driver);
			}
			if(plpPage.elementLayer.verifyPageElements(Arrays.asList("imgProductBadge"),plpPage) ||
				plpPage.verifyProdMsgInPLP("badge")) {
				Log.softAssertThat(plpPage.verifyBadgeImageLocation(),
						"Product badge should be displayed",
						"Product badge is displayed",
						"Product badge is not displayed", driver);
			} else {
				Log.failsoft("Product badge is not available in this page", driver);
			}
			
			headers.navigateTo(lvl_Clearance);
			Log.message(i++ +". Navigated to clearance category", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("txtClearencePromoMessage"), plpPage),
					"Clearance Messaging should be displayed",
					"Clearance Messaging is displayed",
					"Clearance Messaging is not displayed", driver); 
			
			//To Verify promotional message
			if(lvl2_Promomsg.trim().contains("View All")) {
				headers.navigateTo(lvl1_Promomsg);
				Log.message(i++ +". Navigated to PLP", driver);
			} else {
				headers.navigateTo(lvl1_Promomsg, lvl2_Promomsg);
				Log.message(i++ +". Navigated to PLP", driver);
			}
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("txtProdPromoMessage"), plpPage)||
				plpPage.verifyProdMsgInPLP("promotion"),
				"Promoional Messaging should be displayed",
				"Promoional Messaging is displayed",
				"Promoional Messaging is not displayed", driver);
			
			int totalCount = plpPage.getSearchResultCount();
			if(totalCount>60) {
				plpPage.scrollToViewMore();
				Log.message(i++ +". Scroll down to the footer section", driver);
				
				//Step-9 Verify the "View More" button in PLP Select a category where we have lot's of product
				
				Log.softAssertThat(plpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionSearchResults", "btnViewMoreButton", plpPage),
						"View More button should be displayed below the product grid.", 
						"View More button is displayed below the product grid.",
						"View More button is not displayed below the product grid.", driver);
							
				int prdTileCount = plpPage.getProductTileCount();
				Log.message("No of products are: "+ prdTileCount);
				
				plpPage.clickOnViewMore();
				Log.message(i++ +". Clicked view more link", driver);
				
				if((totalCount-60) >= 60) {
					Log.softAssertThat(plpPage.getProductTileCount() == 120,						
							"the system should load 60 more products in the PLP", 
							"the system is loaded 60 more products in the PLP",
							"the system is not loaded  60  more products in the PLP", driver);
					
					Log.softAssertThat(plpPage.verifyLoadedProductsHaveImgNameAndprice() == 120,
							"Product image, name & price should be displayed properly on the PLP for new products",
							"Product image, name & price should be displayed properly on the PLP for new products",
							"Product image, name & price should be displayed properly on the PLP for new products", driver);
				} else {					
					Log.softAssertThat(plpPage.getProductTileCount() == totalCount,						
							"The system should load all products in the PLP", 
							"The system is loaded all products in the PLP",
							"The system is not loaded all products in the PLP", driver);
					
					Log.softAssertThat(plpPage.verifyLoadedProductsHaveImgNameAndprice() == totalCount,
							"Product image, name & price should be displayed properly on the PLP for new products",
							"Product image, name & price should be displayed properly on the PLP for new products",
							"Product image, name & price should be displayed properly on the PLP for new products", driver);
				}
			} else {
				Log.reference("View more functionality not tested due to less number of products");
			}	
			
			//Step-10 Verify the functionality of the vertical refinement bar ( Only for landscape mode)
			
			Log.testCaseResult();

		}//End of try
		catch(Exception e){
			Log.exception(e, driver);
		}// End of catch 
		finally{
			Log.endTestCase(driver);
		}//End of finally
	}//M1_FBB_E2E_C24586
}//TC_FBB_E2E_C24586
