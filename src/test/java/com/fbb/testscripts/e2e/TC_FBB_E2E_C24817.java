package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PaypalConfirmationPage;
import com.fbb.pages.PaypalPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24817 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24817(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
						
		int i=1;
		try
		{	
			String paypalEmail = accountData.get("credential_alwaysapprove_paypal").split("\\|")[0];
			String paypalPassword = accountData.get("credential_alwaysapprove_paypal").split("\\|")[1];
			String prdVariation1 = TestData.get("prd_variation1");
			String plccSSN = checkoutData.get("plcc-account");
			
			//Step1
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to the 'Full Beauty Brands' Home Page!", driver);
			
			//Step2
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation1);
			Log.message(i++ + ". Navigated the To PDP page : "+pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to the bag", driver);
			
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to the shopping bag page", driver);
			
			
			PaypalPage paypalPage = shoppingBagPg.clickOnPaypalButton();
			Log.message(i++ + ". Clicked on Paypal button in shopping bag page", driver);
			
			//Step 3
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			PaypalConfirmationPage pcp = paypalPage.enterPayapalCredentials(paypalEmail, paypalPassword);
			Log.message(i++ + ". Continued with Paypal Credentials.", driver);
			
			CheckoutPage checkoutPg = pcp.clickContinueConfirmation();
			Log.message(i++ + ". Clicked on Continue button.", driver);
						
			if (checkoutPg.elementLayer.verifyPageElements(Arrays.asList("modalCheckoutPlcc"), checkoutPg)) {
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("modalCheckoutPlcc"), checkoutPg),
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed",
						"PLCC modal is not getting displayed", driver);
					
				//Step5
				checkoutPg.clickGetItTodayInPLCC();
				Log.message(i++ + ". Click on 'Get It Today' in PLCC!", driver);
				
				//Step6
				checkoutPg.fillPLCCAccountDetails(plccSSN);
				Log.message(i++ + ". Filled PLCC account details.", driver);
				
				checkoutPg.typeTextInMobileInPLCC("3334445555");	
				
				checkoutPg.checkConsentInPLCC("YES");
				Log.message(i++ +". Checked Consent checkbox", driver);
				
				checkoutPg.clickOnAcceptInPLCC();
				Log.message(i++ + ". Clicked on accept.", driver);
				
				if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("approvedModal"), checkoutPg)) {
					
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"), checkoutPg),
							"Approval modal should be displayed", 
							"Approval modal is getting displayed",
							"Approval modal is not getting displayed", driver);
									
					//Step7
					checkoutPg.dismissCongratulationModal();
					Log.message(i++ + ". Closed congratulation modal", driver);
					
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("step3Active", "btnPlaceOrder"), checkoutPg), 
							"User should be taken to Checkout 'Review and Place order' section(Step-3)", 
							"User is taken to Checkout 'Review and Place order' section(Step-3)", 
							"User is not taken to Checkout 'Review and Place order' section(Step-3)", driver);
					
					String plccVerification = Utils.getCurrentBrandShort().substring(0, 2) + "_plcc";
					Log.softAssertThat(checkoutPg.elementLayer.verifyAttributeForElement("logoPaymentType", "class", plccVerification, checkoutPg), 
							"PLCC card should be selected  when PLCC card approved", 
							"PLCC card is selected when PLCC card approved", 
							"PLCC card is not selected when PLCC card approved", driver);
					
					Log.softAssertThat(checkoutPg.elementLayer.verifyNumberMaskedWithSpecifiedDigits("lblCardNumberInMiniPayment", 4, checkoutPg),
							"The masked number and last 4 digits of the PLCC card should be displayed in the \"Card Number\" field.",
							"The masked number and last 4 digits of the PLCC card is displayed in the \"Card Number\" field.",
							"The masked number and last 4 digits of the PLCC card is not displayed in the \"Card Number\" field.", driver);	
			
					if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
						Log.reference("Further verfication steps are not supported in current environment.");
						Log.testCaseResult();
						return;
					}
					
					//Step 9 - Review the order and select the "Place Order" button.
				
					 checkoutPg.clickOnPlaceOrder();
					Log.message(i++ + ". Clicked on place order button", driver);
					
					if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"),checkoutPg)) {
						Log.reference("By Using always approve card user can't place order", driver);
					} else {
						Log.message(i++ + ". Order Placed successfully", driver);
						
						OrderConfirmationPage orderPage = new OrderConfirmationPage(driver).get();
						if(Utils.isMobile()) {
							orderPage.clickOnViewDetailsMobile();
						}
							
						Log.softAssertThat(orderPage.elementLayer.verifyNumberMaskedWithSpecifiedDigits("lblEnteredCardNo", 4, orderPage),
								"The last 4 digits of the PLCC card should be be displayed.",
								"The last 4 digits of the PLCC card is be displayed.",
								"The last 4 digits of the PLCC card is not displayed.", driver);
					}
				} else {
					Log.message("<br>");
					Log.reference("PLCC Approval modal is not getting displayed, Hence cant proceed further.");
					Log.message("<br>");
				}
			} else {
				Log.fail("PLCC chekout step-1 overlay is not displayed", driver);
			}
			
			Log.testCaseResult();
			
		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally
	}
}
