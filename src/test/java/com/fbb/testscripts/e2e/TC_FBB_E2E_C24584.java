package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.account.MailDropPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24584 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24584(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{		
			String emailId = AccountUtils.generateEmail(driver,"@maildrop.cc");
			String prdVariation = TestData.get("prd_variation");
			int inboxCount = 0;
			
			OrderConfirmationPage receipt;
			LinkedList<LinkedHashMap<String, String>> prdDetails = new LinkedList<LinkedHashMap<String, String>>();

			{
				try {
					inboxCount = GlobalNavigation.getInboxCountFromMailDrop(emailId, driver);
					Log.message("Inbox count for " + emailId + " is " + inboxCount);
				} catch(Exception e) {
					Log.message("Inbox count for " + emailId + " is " + inboxCount);
				}
				Object[] obj = GlobalNavigation.addProduct_PlaceOrder_G(driver, prdVariation, i, emailId);
				receipt = (OrderConfirmationPage) obj[5];
				prdDetails = receipt.getProductDetails();
			}
			
			MailDropPage mail = new MailDropPage(driver).get();
			Log.message(i++ + ". MailDrop page loaded successfully", driver);
			
			mail.navigateToDropMailBox(emailId);
			Log.message(i++ + ". Navigated to usermail inbox for::" + emailId, driver);
			
			mail.openReceivedMail("order", inboxCount, "Order confirmation email");
			Log.message(i++ + ". Navigated to order confirmation mail ", driver);
			
			//step 1 - Verify view browser link
			Log.softAssertThat(mail.verifyViewBrowserLink(),
					"On clicking 'View in browser' link, Page should be redirected to appropriate brand site.",
					"On clicking 'View in browser' link, Page is redirected to appropriate brand site.",
					"On clicking 'View in browser' link, Page is not redirected to appropriate brand site.", driver); 
			
			//step 2 - Verify brand logo link	
			Log.softAssertThat(mail.verifyVerticalAllignment(driver, "brandLogoLinkAll", "topPromoBanner", mail),
						"'Promo Banner' should be displayed below the Brand logo",
						"'Promo Banner' is displayed below the Brand logo", 
						"'Promo Banner' is not displayed below the Brand logo", driver);
			
			Log.softAssertThat(mail.verifyLinkRedirectedRespectiveBrand("brandLogoLinkAll", "Brand logo"),
					"On clicking 'Brand name' link, Page should be redirected to appropriate brand site.",
					"On clicking 'Brand name' link, Page is redirected to appropriate brand site.",
					"On clicking 'Brand name' link, Page is not redirected to appropriate brand site.", driver);
			
			//step 4 - Verify promo banner
			if(mail.verifyElementDisplayed(Arrays.asList("topPromoBanner"), mail)) {
				Log.softAssertThat(mail.verifyLinkRedirectedRespectiveBrand("topPromoBannerLink", "Promo banner"),
						"On clicking 'Promo banner' link, Page should be redirected to appropriate brand site.",
						"On clicking 'Promo banner' link, Page is redirected to appropriate brand site.",
						"On clicking 'Promo banner' link, Page is not redirected to appropriate brand site.", driver); 
				
				//step 5 - Verify "Thank you for your order" message
				Log.softAssertThat(mail.verifyVerticalAllignment(driver, "topPromoBanner", "thankYouHeader", mail),
						"'Thank you for your order' should be displayed below the 'Promo banner'",
						"'Thank you for your order' is displayed below the 'Promo banner'", 
						"'Thank you for your order' is not displayed below the 'Promo banner'", driver);
			} else {
				//step 5 - Verify "Thank you for your order" message
				Log.softAssertThat(mail.verifyVerticalAllignment(driver, "brandLogoLinkAll", "thankYouHeader", mail),
						"'Thank you for your order' should be displayed below the Brand logo",
						"'Thank you for your order' is displayed below the Brand logo", 
						"'Thank you for your order' is not displayed below the Brand logo", driver);
			}
			
			//Step 6 - Verify Order details
			Log.softAssertThat(mail.elementLayer.verifyVerticalAllignmentOfElements(driver, "thankYouHeader", "hiUserName", mail),
					"Order Information should be displayed below the Thank you message.",
					"Order Information is displayed below the Thank you message.", 
					"Order Information is not displayed below the Thank you message.", driver);
			
			Log.softAssertThat(mail.verifyVerticalAllignment(driver, "orderInformationMsg", "orderNumber", mail),
					"Order number should be displayed below the order information message.",
					"Order number should is displayed below the order information message.", 
					"Order number should is not displayed below the order information message.", driver); 
			
			if(Utils.isMobile()) {
				Log.softAssertThat(mail.verifyVerticalAllignment(driver, "shippingAddress", "paymentMethod", mail)
						&& mail.verifyVerticalAllignment(driver, "paymentMethod", "shippingMethod", mail)
						&& mail.verifyVerticalAllignment(driver, "shippingMethod", "billingAddress", mail)
						&& mail.verifyVerticalAllignment(driver, "billingAddress", "giftCard", mail)
						&& mail.verifyVerticalAllignment(driver, "giftCard", "rewardCertificate", mail)
						&& mail.verifyVerticalAllignment(driver, "rewardCertificate", "promotions", mail),
				"Shipping Address, Payment method, shipping method, Billing Address, Gift Card, Reward Certificate &" 
						+ " Promotions should be displayed one by one in the Order details section",
				"Shipping Address, Payment method, shipping method, Billing Address, Gift Card, Reward Certificate &" 
						+ " Promotions are displayed one by one in the Order details section", 
				"Shipping Address, Payment method, shipping method, Billing Address, Gift Card, Reward Certificate &" 
						+ " Promotions are not displayed one by one in the Order details section", driver);
			} else {
				Log.softAssertThat(mail.verifyHorizontalAllignment(driver, "billingAddress", "shippingAddress", mail)
						&& mail.verifyHorizontalAllignment(driver, "giftCard", "paymentMethod", mail)
						&& mail.verifyHorizontalAllignment(driver, "rewardCertificate", "paymentMethod", mail)
						&& mail.verifyHorizontalAllignment(driver, "promotions", "shippingMethod", mail),
				"Shipping Address, Payment method and shipping method should be displayed on the left side & "
						+ "Billing Address, Gift Card, Reward Certificate & Promotions should be displayed on the right side of the Order details section",
				"Shipping Address, Payment method and shipping method should be displayed on the left side & "
						+ "Billing Address, Gift Card, Reward Certificate & Promotions should be displayed on the right side of the Order details section", 
				"Shipping Address, Payment method and shipping method should be displayed on the left side & "
						+ "Billing Address, Gift Card, Reward Certificate & Promotions should be displayed on the right side of the Order details section", driver);
			}
			
			//step 7 - Verify Order status link.
			Log.softAssertThat(mail.verifyLinkRedirectedRespectiveBrand("orderStatusLink", "Order status"),
					"On clicking 'Order status' link, Page should be redirected to appropriate brand site.",
					"On clicking 'Order status' link, Page is redirected to appropriate brand site.",
					"On clicking 'Order status' link, Page is not redirected to appropriate brand site.", driver);
			
			//step 8 - Verify Billing Payment link.
			Log.softAssertThat(mail.verifyLinkRedirectedRespectiveBrand("billingPaymentLink", "Billing & Payment"),
					"On clicking 'Billing Payment' link, Page should be redirected to appropriate brand site.",
					"On clicking 'Billing Payment' link, Page is redirected to appropriate brand site.",
					"On clicking 'Billing Payment' link, Page is not redirected to appropriate brand site.", driver);
			
			//step 9 - Verify "Return & Exchange" link.
			Log.softAssertThat(mail.verifyLinkRedirectedRespectiveBrand("retrunAndExchangeLink", "Return & Exchange"),
					"On clicking 'Return & Exchange' link, Page should be redirected to appropriate brand site.",
					"On clicking 'Return & Exchange' link, Page is redirected to appropriate brand site.",
					"On clicking 'Return & Exchange' link, Page is not redirected to appropriate brand site.", driver); 
			
			//step 10 - "Click here to Enroll" link
			Log.softAssertThat(mail.verifyEnrollLink(),
					"On clicking 'Click here to Enroll' link, Page should be redirected to appropriate brand site.",
					"On clicking 'Click here to Enroll' link, Page is redirected to appropriate brand site.",
					"On clicking 'Click here to Enroll' link, Page is not redirected to appropriate brand site.", driver);  
			
			//Step 11 - Verify product details
			Log.softAssertThat(mail.verifyVerticalAllignment(driver, "orderStatusLink", "enrollLink", mail)
						&& mail.verifyVerticalAllignment(driver, "billingPaymentLink", "enrollLink", mail)
						&& mail.verifyVerticalAllignment(driver, "retrunAndExchangeLink", "enrollLink", mail),
					"The 'Enroll' link should be display below the order status, billing payment and return exchange links",
					"The 'Enroll' link is displayed below the order status, billing payment and return exchange links", 
					"The 'Enroll' link is not displayed below the order status, billing payment and return exchange links", driver);
			
			Log.softAssertThat(mail.verifyProductDetailsDisplayed(driver, prdDetails, mail),
				"Product Information should be displayed properly. 1.Product name 2.Product id 3.Color 4.Size 5.Quantity 6.Price",
				"Product Information is displayed properly.", 
				"Product Information is not displayed properly.", driver);
			
			//Step 12 - Verify Order Total section
			Log.softAssertThat(mail.verifyVerticalAllignment(driver, "merchandiseSubtotalText", "shippingAndHandlingText", mail),
					"The Shipping & Handling should be display below the Merchandise Subtotal",
					"The Shipping & Handling is displayed below the Merchandise Subtotal", 
					"The Shipping & Handling is not displayed below the Merchandise Subtotal", driver);
			
			Log.softAssertThat(mail.verifyVerticalAllignment(driver, "shippingAndHandlingText", "estimatedTaxText", mail),
					"The Estimated tax should be display below the Shipping & Handling",
					"The Estimated tax is displayed below the Shipping charge", 
					"The Estimated tax is not displayed below the Shipping charge", driver);
			
			Log.softAssertThat(mail.verifyVerticalAllignment(driver, "estimatedTaxText", "subtotalText", mail),
					"The Subtotal should be display below the Estimated tax",
					"The Subtotal is displayed below the Estimated tax", 
					"The Subtotal is not displayed below the Estimated tax", driver);
			
			Log.softAssertThat(mail.verifyVerticalAllignment(driver, "subtotalText", "TotalText", mail),
					"The Total should be display below the Subtotal",
					"The Total is displayed below the Subtotal", 
					"The Total is not displayed below the Subtotal", driver);
			
			//step 13 - Verify Apply link 
			Log.softAssertThat(mail.verifyLinkRedirectedRespectiveBrand("applyLink", "Apply"),
					"On clicking 'Apply' link, Page should be redirected to appropriate brand site.",
					"On clicking 'Apply' link, Page is redirected to appropriate brand site.",
					"On clicking 'Apply' link, Page is not redirected to appropriate brand site.", driver); 
			
            Log.softAssertThat(mail.verifyVerticalAllignment(driver, "plccBanner", "applyLink", mail),
            		"'Apply' link should be displayed below the PLCC promo banner",
            		"'Apply' link is displayed below the PLCC promo banner", 
            		"'Apply' link is not displayed below the PLCC promo banner", driver);
			
			Log.softAssertThat(mail.verifyVerticalAllignment(driver, "plccBanner", "categoryHeader", mail),
					"'Category header' should be displayed below the PLCC promo banner",
					"'Category header' is displayed below the PLCC promo banner", 
					"'Category header' is not displayed below the PLCC promo banner", driver);
		
			Log.softAssertThat(mail.verifyLinkRedirectedRespectiveBrand("categoryHeaderLink", "Category header"),
					"On clicking 'category header' link, Page should be redirected to appropriate brand site.",
					"On clicking 'category header' link, Page is redirected to appropriate brand site.",
					"On clicking 'category header' link, Page is not redirected to appropriate brand site.", driver); 
			
	
			Log.testCaseResult();
		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally
	}//M1_FBB_E2E_C24584
}//TC_FBB_E2E_C24584
