package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.account.CatalogPreferencePage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24601 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	
	@Test(groups = { "myaccount", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24601(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			
			String userMail = AccountUtils.generateEmail(driver) + "|" + accountData.get("password_global");
			String catalogPgError = TestData.get("catalogPgError");
			String address1 = "valid_address1";
			String addressDetails1 = checkoutData.get(address1);
			
			{
				GlobalNavigation.registerNewUser(driver, 0, 0, userMail);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Navigate to My Account Page
			
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(AccountUtils.generateEmail(driver), accountData.get("password_global"));
			Log.message(i++ + ". Navigated to My Account page as : " +AccountUtils.generateEmail(driver), driver);
			
			//Step 1 - Verify the functionality of Step 1 Label and below options.
			CatalogPreferencePage CatalogPrefPg=null;
			if(myAcc.elementLayer.verifyElementDisplayed(Arrays.asList("lnkCatalogPref"),myAcc))
			CatalogPrefPg = myAcc.clickOnCatalogPrefLink();
			else
				CatalogPrefPg =  homePage.footers.navigateToCatalogPreferences();
			Log.message(i++ + ". Navigated to 'Catalog Preference' Page!", driver);
			
			Log.softAssertThat(CatalogPrefPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblStep1", "catalogPreference", CatalogPrefPg),
					"Step1 should display above catalog preference",
					"Step1 is displaying above catalog preference",
					"Step1 is not displaying above catalog preference", driver);
			
			Log.softAssertThat(CatalogPrefPg.elementLayer.verifyElementDisplayed(Arrays.asList("requestCatluge","stopReceivingCat","receiveLessCat"), CatalogPrefPg), 
					"'Request', 'Receive Less', 'Stop' catalog options should be displayed", 
					"'Request', 'Receive Less', 'Stop' catalog options are displayed", 
					"'Request', 'Receive Less', 'Stop' catalog options are not displayed", driver);
			
			Log.softAssertThat(CatalogPrefPg.checkRequestCatalogIsSelected(), 
					"'Request' catalog option should selected by default", 
					"'Request', catalog option is selected by default", 
					"'Request', catalog option is not selected by default", driver);
			
			//Step 2 - Verify the functionality of Step 2 Label and below options.
			
			Log.softAssertThat(CatalogPrefPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "catalogPreference", "lblStep2", CatalogPrefPg),
					"Step2 should display below catalog preference",
					"Step2 is displaying below catalog preference",
					"Step2 is not displaying below catalog preference", driver);
			
			Log.softAssertThat(CatalogPrefPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblStep2", "firstName", CatalogPrefPg),
					"Step2 should display above first name",
					"Step2 is displaying above first name",
					"Step2 is not displaying above first name", driver);
			
			Log.softAssertThat(CatalogPrefPg.elementLayer.verifyElementDisplayed(Arrays.asList("firstName", "lastName","address1","address2","stateRequired","postalCode","city"), CatalogPrefPg), 
					"'First & Last Name', 'Address 1 & 2', 'City', 'State', 'Postal' fields should be displayed", 
					"'First & Last Name', 'Address 1 & 2', 'City', 'State', 'Postal' fields are displayed", 
					"'First & Last Name', 'Address 1 & 2', 'City', 'State', 'Postal' fields are not displayed", driver);
			
			CatalogPrefPg.clickSaveCatalogue();
			Log.softAssertThat(CatalogPrefPg.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("errorMsgFirstName","errorMsgState","errorMsgPostalcode","errorMsgcity","errorMsgAddress1","errorMsgLastName"), "color", catalogPgError, CatalogPrefPg),
					"'First & Last Name', 'Address 1', 'City', 'State', 'Postal' placeholders should display in red",
					"'First & Last Name', 'Address 1', 'City', 'State', 'Postal' placeholders are displayed in red",
					"Some placeholders are not displayed in red", driver);
			
			CatalogPrefPg.enterFirstName("U&^&787");
			CatalogPrefPg.enterLastName("U&^&787");
			CatalogPrefPg.enterAddress1("U&^&787");
			CatalogPrefPg.enterAddress2("U&^&787");
			CatalogPrefPg.enterPostalCode("U&^&787");
			CatalogPrefPg.enterCity("U&^&787");
			Log.message(i++ + ". Entered 'Invalid' values in all of the fields!", driver);
			
			Log.softAssertThat(CatalogPrefPg.elementLayer.verifyElementDisplayed(Arrays.asList("errorMsgAddress2","errorMsgFirstName","errorMsgPostalcode","errorMsgcity","errorMsgAddress1","errorMsgLastName"), CatalogPrefPg), 
					"'First & Last Name', 'Address 1 & 2', 'City', 'Postal' fields should throw error", 
					"'First & Last Name', 'Address 1 & 2', 'City', 'Postal' fields throws error", 
					"Some fields not throws error", driver);
			
			CatalogPrefPg.enterPostalCode(addressDetails1.split("\\|")[4]);
			CatalogPrefPg.enterFirstName(addressDetails1.split("\\|")[7]);
			CatalogPrefPg.enterLastName(addressDetails1.split("\\|")[8]);
			CatalogPrefPg.enterAddress1(addressDetails1.split("\\|")[0]);
			CatalogPrefPg.enterAddress2(addressDetails1.split("\\|")[1]);
			Log.message(i++ + ". Entered 'Valid' values in all of the fields!", driver);
			
			CatalogPrefPg.clickSaveCatalogue();
			Log.message(i++ + ". Clicked on Submit button!", driver);
			
			Log.softAssertThat(CatalogPrefPg.elementLayer.verifyElementDisplayed(Arrays.asList("divCatalogThankyou"), CatalogPrefPg), 
					"Catalog should get submitted and 'Thankyou' message is displayed", 
					"Catalog is submitted and 'Thankyou' message is displayed", 
					"Catalog is not submitted", driver);
			
			//Step 3 - Verify the functionality Get offers & updates from our sister brands
			
			Log.softAssertThat(CatalogPrefPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("sisterBrandfullbeautyDeskTab", "sisterBrandfullbeautyMob"), CatalogPrefPg), 
					"Fullbeauty should not be displayed in sisterbrands",
					"Fullbeauty is not displayed in sisterbrands",
					"Fullbeauty is displayed in sisterbrands", driver);
			
			homePage.footers.navigateToCatalogPreferences();
			Log.message(i++ + ". Navigated to catalog page!", driver);
			
			CatalogPrefPg.selectCatalogPrefRadioButton("Stop");
			Log.message(i++ + ". Select 'Stop' Catalog!", driver);
			
			CatalogPrefPg.enterPostalCode(addressDetails1.split("\\|")[4]);
			CatalogPrefPg.enterFirstName(addressDetails1.split("\\|")[7]);
			CatalogPrefPg.enterLastName(addressDetails1.split("\\|")[8]);
			CatalogPrefPg.enterAddress1(addressDetails1.split("\\|")[0]);
			Log.message(i++ + ". Entered 'Valid' values in all of the fields!", driver);
			
			CatalogPrefPg.clickSaveCatalogue();
			Log.message(i++ + ". Clicked on Submit button!", driver);
			
			//Step 4 - Verify the Catalog Unsubscribe functionality
			
			Log.softAssertThat(CatalogPrefPg.elementLayer.verifyElementDisplayed(Arrays.asList("catalogRequestHeader"), CatalogPrefPg), 
					"Catalog unsubscribe message should get displayed", 
					"Catalog unsubscribe message is getting displayed", 
					"Catalog unsubscribe message is not getting displayed", driver);

			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24125
