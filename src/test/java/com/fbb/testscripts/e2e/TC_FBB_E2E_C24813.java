package com.fbb.testscripts.e2e;

import java.util.Arrays;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.MiniCartPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.customerservice.ContactUsPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.customerservice.TrackOrderPage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24813 extends BaseTest {
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");

	@Test(groups = { "header", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24813(String browser) throws Exception {
		Log.testCaseInfo(); 

		final WebDriver driver= WebDriverFactory.get(browser);
		
		String validUsername = AccountUtils.generateEmail(driver);
		String validPassword = accountData.get("password_global");
		String credential = validUsername + "|" + validPassword;
		String errColorCode = TestData.get("password_error");
		String inValidIP = "444.222.111.333";
		String emailError = demandwareData.get("EmailFlyoutError");
		String inValidUsername = "test@yop.com";
		String inValidPassword = "asdf123asd";
		String productKey = TestData.get("prd_variation");
		String utilityBarColor = TestData.get("utilityBarColor");
		String SignInDesktopColor = TestData.get("SignInDesktopColor");
		
		GlobalNavigation.registerNewUser(driver, 0, 0, credential);
		
		int i=1;
		try {
			
			HomePage homePage= new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Navigates to  brand home page. ", driver);
			
			Headers headers = homePage.headers;
			Footers footers = homePage.footers;
			
			//step-1 Check the contents in the Header section
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("utilityBarDesktop", "lnkOffer", "btnHeaderSearchIcon", "quickOrederIcon", "lnkCardPLCC", "lnkAccountButton", "lnkMiniCart" ), headers), 
					"'Utility Bar','Deals','Search icon','Quick order icon', 'Card icon', 'Account icon' and 'My Bag icon' contents should be displayed in the Header section", 
					"'Utility Bar','Deals','Search icon','Quick order icon', 'Card icon', 'Account icon' and 'My Bag icon' contents are displayed in the Header section",
					"'Utility Bar','Deals','Search icon','Quick order icon', 'Card icon', 'Account icon' and 'My Bag icon' contents are not displayed in the Header section", driver);
	
			//step-2 Verify Utility bar in the header
			Log.softAssertThat(headers.elementLayer.verifyInsideElementAlligned("utilityBarDesktop", "fullPage", "top", headers)
					&& headers.elementLayer.verifyElementColor("utilityBarDesktop", utilityBarColor ,headers),
					"Utility bar should be displayed on top of the page with black color.",
					"Utility bar is displayed on top of the page with black color.",
					"Utility bar is not displayed on top of the page with black color.", driver);
			
			//Step-3 Verify customer service link in utility bar
			
			Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkMiniCart", "lnkAccountButton", headers)
						&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkAccountButton", "lnkCardPLCC", headers)
						&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkCardPLCC", "quickOrederIcon", headers)
						&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "quickOrederIcon", "btnHeaderSearchIcon", headers)
						&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnHeaderSearchIcon", "lnkOffer", headers),
					"Deals','Search icon','Quick order icon', 'Card icon', 'Account icon' and 'My Bag icon' contents should be displayed horizontal.",
					"Customer service link is displayed next by the sign in link in the Utility Bar.",
					"Customer service link is not displayed next by the sign in link in the Utility Bar.", driver);
			
			CustomerService customerServicePage= footers.navigateToCustomerService();
			Log.message(i++ +". Clicked Customer service link form header. ", driver);
			
			Log.softAssertThat(customerServicePage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), customerServicePage),
					"The Page should be redirected to Customer service landing page.", 
					"The Page is redirected to Customer service landing page.",
					"The Page is not redirected to Customer service landing page.", driver);
			
			ContactUsPage contactUsPage = customerServicePage.clickOnMailWithUs();
			Log.message(i++ +". Clicked on Email Us link ", driver);

			Log.softAssertThat(contactUsPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), contactUsPage), 
					"The Contact Us page should be displayed!",
					"The Contact Us page is displayed!", 
					"The Contact Us page is not displayed.", driver);

			BrowserActions.scrollToTopOfPage(driver);
			customerServicePage = footers.navigateToCustomerService();
			Log.message(i++ + ". Navigated to Customer Service page!", driver);

			if(customerServicePage.getLiveChatStatus()) {
				Log.softAssertThat(customerServicePage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "liveChatAvailableLnk", "lnkMailWithUs", customerServicePage), 
						"LIVE CHAT text and icon should be shown next to the 'Email Us' icon", 
						"LIVE CHAT text and icon shown next to the 'Email Us' icon", 
						"LIVE CHAT text and icon not shown next to the 'Email Us' icon", driver);

				customerServicePage.clickOnLiveChatLink();
				Log.message(i++ + ". Clicked on Live Chat Icon!", driver);

				Log.softAssertThat(customerServicePage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlLiveChat"), customerServicePage), 
						"Chat modal window should opens as a pop up modal", 
						"Chat modal window should opened as pop up modal!",
						"Chat modal window should did not open as expected.", driver);
				
				customerServicePage.closeLiveChat();
			} else {
				//Step-10 Verify Live chat unavailable state
				Log.softAssertThat(customerServicePage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("liveChatAvailableLnk"), customerServicePage),
						"Live Chat link should not be display in the Customer service section", 
						"Live Chat link is not displayed in the Customer service section",
						"Live Chat link is displayed in the Customer service section", driver);	
				
				Log.softAssertThat(customerServicePage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkMailWithUs"), customerServicePage),
						"Email us link should be displayed in the middle of the flyout.",
						"Email us link is displayed in the middle of the flyout.",
						"Email us link is not displayed in the middle of the flyout.", driver);
				
				Log.reference("--->>> Live chat is not available at this time. Skipping related validations.");
			}

			headers.navigateToHome();
			
			customerServicePage = footers.navigateToCustomerService();
			Log.message(i++ + ". Navigated to Customer Service page!", driver);
			
			Log.softAssertThat(customerServicePage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkCallUs"), customerServicePage),
					"call us section with a text as 'CALL US'and a telephone number 'x-xxx-xxx-xxxx' format should be displayed ", 
					"call us section with a text as 'CALL US'and a telephone number 'x-xxx-xxx-xxxx' format is displayed ",
					"call us section with a text as 'CALL US'and a telephone number 'x-xxx-xxx-xxxx' format is not displayed", driver);
			
			TrackOrderPage ordersPage = footers.navigateToTrackMyOrder();
			Log.message(i++ + ". Clicked On Track My Order Link!", driver);
			
			Log.softAssertThat(ordersPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), ordersPage),
					"The page should be redirected to the login page.",
					"The page is redirected to the login page.",
					"The page is not redirected to the login page.", driver);
			
			ordersPage = footers.navigateToReturnItems();
			Log.message(i++ + ". User navigated to Return Items!", driver);
			
			Log.softAssertThat(ordersPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), ordersPage),
					"The page should be redirected to the login page.",
					"The page is redirected to the login page.",
					"The page is not redirected to the login page.", driver);
			
			footers.navigateToYourAccount();
			Log.message(i++ + ". Clicked On My Account link!", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyPageElements(Arrays.asList("readyElementAccountNotLoggedIn"), headers),
					"The page should be redirected to the login page.",
					"The page is redirected to the login page.",
					"The page is not redirected to the login page.", driver);
			
			headers.navigateToHome();
			
			//Step-5 Verify Sign in link in Utility bar
			Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkMiniCart", "lnkAccountButton", headers),
					"Sign In should display next to the mini cart on the left-hand side.", 
					"Sign In is displayed next to the mini cart on the left-hand side.",
					"Sign In is not displayed next to the mini cart on the left-hand side.", driver);
			
			SignIn signIn= headers.navigateToSignInPage();
			Log.message(i++ + ". Clicked On Sign in Link!", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("readyElement"), signIn),
					"The page should be redirected to the login page.",
					"The page is redirected to the login page.",
					"The page is not redirected to the login page.", driver);
			
			headers.navigateToHome();
			Log.message(i++ +". Navigated to Home Page", driver);
			
			Log.softAssertThat(homePage.headers.mouseOverAccountMenu(), 
					"On Mouse hobver, Fly out should be displayed on the homepage.",
					"On Mouse hobver, Fly out is displayed on the homepage.",
					"On Mouse hobver, Fly out is not displayed on the homepage.", driver);
					
			Log.softAssertThat(homePage.headers.elementLayer.verifyElementColor("lnkAccountButton", SignInDesktopColor, homePage.headers)
					&& homePage.headers.elementLayer.verifyCssPropertyForElement("lnkAccountButton", "background:", "", homePage.headers),
							"The Hover state of the Sign In link should be displayed the white utility bar and the black Sign In text.", 
							"The Hover state of the Sign In link is displayed the white utility bar and the black Sign In text.", 
							"The Hover state of the Sign In link is not displayed the white utility bar and the black Sign In text.", driver);	

			headers.clickOnSignInDesktop();
			Log.message(i++ +". Clicked sign in link without any details", driver);
			
			Log.softAssertThat(homePage.headers.elementLayer.verifyElementColor("lblUserNameErrorDesktop", errColorCode, homePage.headers)
					&& homePage.headers.elementLayer.verifyElementColor("lblPasswordInSignInFlyOut", errColorCode, homePage.headers),
					"Email address field and password fields are mandatory & Fields placeholder text should be turned to red color when login user",
					"Email address field and password fields are mandatory & Fields placeholder text is turned to red color when login user",
					"Email address field and password fields are mandatory & Fields placeholder text is not turned to red color when login user", driver);
			
			headers.mouseOverAccountMenu();
			
			homePage.headers.typeUserNameInFlyout(inValidIP);
			Log.message(i++ + ". Entered Invalid IP Format into Email Field", driver);
			String formatErro= homePage.headers.elementLayer.getElementText("invalidEmailErrorTxt", homePage.headers).toLowerCase();
			
			Log.softAssertThat(formatErro.equalsIgnoreCase(emailError),
					"The error message should be displayed as “Please enter a valid email” above the entered text in the email field", 
					"The error message is displayed as “Please enter a valid email” above the entered text in the email field",
					"The error message is not displayed as “Please enter a valid email” above the entered text in the email field", driver);
			
			Log.softAssertThat(headers.verifyPresenceOfShowInPassword(),
					"The SHOW Link should be present at the corner of the password field",
					"The SHOW Link present at the corner of the password field ",
					"The SHOW Link did not present at the corner of the password field ", driver);
			
			headers.typePasswordInFlyout("abcde");
			Log.softAssertThat(headers.verifyPasswordFieldTextMode().toLowerCase().equals("password"),
					"Initially entered password should be displayed as masked",
					"Initially entered password is displayed as masked",
					"Initially entered password is not displayed as masked", driver);
			
			String currentState = headers.clickOnShowHideInPassword();
			Log.message(i++ + ". Show Link Clicked!", driver);

			Log.softAssertThat(currentState.toLowerCase().equals("hide") 
					&& headers.verifyPasswordFieldTextMode().toLowerCase().equals("text"),
					"Values should be visible and the option should be changed to HIDE",
					"Values are visible and the option should be changed to HIDE",
					"Values are not visible and the option should be changed to HIDE", driver);
			
			headers.typeUserNameInFlyout(inValidUsername);
			Log.message(i++ +". typed invalid username: "+ inValidUsername, driver);
			
			headers.typePasswordInFlyout(inValidPassword);
			Log.message(i++ +". typed invalid Password: "+ inValidPassword, driver);
			
			headers.clickOnSignInDesktop();
			Log.message(i++ +". Clicked sign in link without any details", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("readyElement","errLoginErrorMsg"), signIn),
					"User should be navigated to Account-Show-Controller page with error condition messaging", 
					"User navigated to Account-Show-Controller page with error condition messaging", 
					"User not navigated to Account-Show-Controller page with error condition messaging", driver);

			headers.mouseOverAccountMenu();
			Log.softAssertThat(headers.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "btnFlyoutSignIn", "chkRememberMeDesktop", headers)
					&& !(headers.verifyRememberMeCheckboxSelectedOrNot()),
					"Remember Me checkbox should display on the left side of the 'sign in' button with the unchecked state.",
					"Remember Me checkbox is displayed on the left side of the 'sign in' button with the unchecked state.",
					"Remember Me checkbox is not displayed on the left side of the 'sign in' button with the unchecked state.", driver);
			
			headers.typeUserNameInFlyout(validUsername);
			headers.typePasswordInFlyout(validPassword);
			headers.checkOnRememberMeDesktop(true);
			headers.clickOnSignInDesktop();
			
			//Step-5 Check the Sign in Authenticated state
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("utilityBarDesktop", "lblhlloMyAccDesktop", "btnHeaderSearchIcon", "quickOrederIcon", "lnkCardPLCC", "iconUserDesktop", "lnkOffer", "lnkMiniCart" ), headers), 
					"'Utility Bar','Utility Bar - Customer Service','Account - Authenticated State',''Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon'  contents should be displayed in the Header section", 
					"'Utility Bar','Utility Bar - Customer Service','Account - Authenticated State',''Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon'  contents are displayed in the Header section",
					"'Utility Bar','Utility Bar - Customer Service','Account - Authenticated State',''Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon'  contents are not displayed in the Header section", driver);
			
			
			Log.softAssertThat(headers.elementLayer.verifyPageElements(Arrays.asList("lblhlloMyAccDesktop","iconUserDesktop" ), headers)
					&& headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "iconUserDesktop", "lblhlloMyAccDesktop",headers),
					 "Sign in link will convert to user Icon, Hello (User 1st name) & below that icon system should display My account link",
					 "Sign in link will convert to user Icon, Hello (User 1st name) & below that icon system is displayed My account link",
					 "Sign in link will convert to user Icon, Hello (User 1st name) & below that icon system is not displayed My account link", driver);
			
			headers.navigateToMyAccount();
			Log.message(i++ +". Clicked My Account link from the header", driver);
			
			headers.navigateToHome();
			homePage.headers.mouseOverAccountMenu();
			Log.message(i++ +". Mouse hovered on My account", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("flytSignInRegisteredDesktop"), headers),
					"Flyout should be displayed.",
					"Flyout is displayed.", 
					"Flyout is not displayed.", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkProfileMyAccFlytDesktop","lnkOrderHistoryMyAccFlytDesktop","lnkQuickOrderMyAccFlytDesktop", "lnkWishListMyAccFlytDesktop","lnkSignOutMyAccFlytDesktop"), headers),
					"Flyout should consist of following items:PROFILE,ORDER HISTORY,QUICK ORDER,WISHLIST,EMAIL PREFERENCES,SIGN OUT ",
					"Flyout is consist of following items:PROFILE,ORDER HISTORY,QUICK ORDER,WISHLIST,EMAIL PREFERENCES,SIGN OUT ", 
					"Flyout is not consist of following items:PROFILE,ORDER HISTORY,QUICK ORDER,WISHLIST,EMAIL PREFERENCES,SIGN OUT ", driver);
			
			ProfilePage profilePage = headers.clickOnProfileLink();
			Log.message(i++ +". Clicked profile link", driver);
			
			Log.softAssertThat(profilePage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), profilePage),
					"Page should be navigate to Profile page", 
					"Page is navigateed to Profile page",
					"Page is not navigateed to Profile page", driver);

			headers.mouseOverAccountMenu();
			Log.message(i++ +". Mouse hovered on My account", driver);
			
			OrderHistoryPage orderPage = headers.clickOnOrderHistoryLink();
			Log.message(i++ +". Clicked Order history link", driver);
			
			Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage),
					"Page should be navigate to Order History page", 
					"Page is navigateed to Order History page",
					"Page is not navigateed to Order History page", driver);

			headers.mouseOverAccountMenu();
			Log.message(i++ +". Mouse hovered on My account", driver);

			WishListPage wishlistPage = headers.clickOnWishList();
			Log.message(i++ +". Clicked Order Wishlist link", driver);
			
			Log.softAssertThat(wishlistPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), wishlistPage),
					"Page should be navigate to Wishlist page", 
					"Page is navigateed to Wishlist page",
					"Page is not navigateed to Wishlist page", driver);

			headers.mouseOverAccountMenu();
			Log.message(i++ +". Mouse hovered on My account", driver);

			QuickOrderPage qcPage = headers.navigateToQuickOrder();
			Log.message(i++ +". Clicked Order Wishlist link", driver);
			
			Log.softAssertThat(qcPage.elementLayer.verifyPageElements(Arrays.asList("containerElement"), qcPage),
					"Page should be navigate to QuickOrder page", 
					"Page is navigateed to QuickOrder page",
					"Page is not navigateed to QuickOrder page", driver);
			
			headers.navigateToHome();
			
			//Step-6 As register user verify the Track my order & Update my information link under customer service flyout
			ordersPage = footers.navigateToTrackMyOrder();
			Log.message(i++ + ". Clicked On Track My Order Link!", driver);

			Log.softAssertThat(ordersPage.elementLayer.verifyPageElements(Arrays.asList("orderHistoryReadyElement"), ordersPage),
					"Page should be Redirected to Orders Page and Order history should be shown.",
					"Page Redirected to Orders Page and Order history shown",
					"Page not Redirected to Orders Page as expected", driver);
			
			ordersPage = footers.navigateToReturnItems();
			Log.message(i++ + ". User navigated to Return Items!", driver);

			Log.softAssertThat(ordersPage.elementLayer.verifyPageElements(Arrays.asList("orderHistoryReadyElement"), ordersPage),
					"Page should be Redirected to Orders Page and Order history should be shown.",
					"Page Redirected to Orders Page and Order history shown",
					"Page not Redirected to Orders Page as expected", driver);

			profilePage = headers.clickOnProfileLink();
			Log.message(i++ + ". User navigated to Update My Info!", driver);

			Log.softAssertThat(profilePage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), profilePage),
					"Page should be Redirected to My Acccount Page to Update information.",
					"Page Redirected to My Account Page",
					"Page not Redirected to My Account Page as expected", driver);
			
			headers.navigateToHome();
			headers.signOut();
			Log.message(i++ +". clicked sign out link", driver);  
			
			//Step-7 Verify Recognized unauthenticated state.
			
			if(headers.elementLayer.verifyElementDisplayed(Arrays.asList("lblhlloMyAccDesktop"), headers)) {
				Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("utilityBarDesktop", "lblhlloMyAccDesktop", "btnHeaderSearchIcon", "quickOrederIcon", "lnkCardPLCC", "iconUserDesktop", "lnkOffer", "lnkMiniCart"), headers), 
						"'Utility Bar','Utility Bar - Customer Service','Account - Recognized & Authenticated State', 'Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon'  contents should be displayed in the Header section", 
						"'Utility Bar','Utility Bar - Customer Service','Account - Recognized & Authenticated State', 'Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon' contents are displayed in the Header section",
						"'Utility Bar','Utility Bar - Customer Service','Account - Recognized & Authenticated State', 'Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon' contents are not displayed in the Header section", driver);
				
			headers.mouseOverAccountMenu();
			Log.message(i++ +". Mouse hovered on My account", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("fldUserNameDesktop"), headers),
					"in the email address field, the system should display the email address",
					"in the email address field, the system should display the email address",
					"in the email address field, the system should display the email address", driver);		
			} else {
				Log.failsoft("Recognized unauthenticated state is not available");
			}
			headers.navigateToHome();
			//Step-8 Verify My bag link in Utility bar.
			Log.softAssertThat(homePage.headers.elementLayer.verifyInsideElementAlligned("lnkMiniCart", "utilityBarDesktop", "right", homePage.headers)
					&& homePage.headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkMiniCart", "lnkAccountButton", homePage.headers),
					"My bag/Mini cart should display all the way to the right-hand side in utility bar.",
					"My bag/Mini cart is displayed all the way to the right-hand side in utility bar.",
					"My bag/Mini cart is not displayed all the way to the right-hand side in utility bar.", driver); 
			
			ShoppingBagPage cart= homePage.headers.clickOnBagLink();
			Log.softAssertThat(cart.verifyEmptyCart(),
					"The page should be navigated to empty cart page",
					"The page is navigated to empty cart page",
					"The page is not navigated to empty cart page", driver);    
			
			PdpPage pdpPage = headers.navigateToPDP(productKey);
			Log.message(i++ +". Navigated to PDP", driver);
			
			String productName= pdpPage.getProductName().toLowerCase();
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". Added product to bag", driver);
			 
			cart = homePage.headers.clickOnBagLink();
			
			String myBagCount= headers.getMiniCartCount();
			int cartqty= Integer.parseInt(myBagCount);
			
			Log.softAssertThat(cart.getTotalQtyInCart()==cartqty
					&& headers.elementLayer.verifyElementDisplayed(Arrays.asList("miniCartQty"), headers), 
					"Then the mini cart in the top navigation should displays as an icon with the numeric representation of the number of items in the shopping cart.",
					"Then the mini cart in the top navigation is displays as an icon with the numeric representation of the number of items in the shopping cart.",
					"Then the mini cart in the top navigation is not displays as an icon with the numeric representation of the number of items in the shopping cart.", driver);
			
			headers.navigateToHome();
			headers.mouseOverMyBag();
			Log.message(i++ +". Mouse hovered on bag", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyElementColor("lnkMiniCart", "gba(0, 0, 0, 1)", homePage.headers)
					&& homePage.headers.elementLayer.verifyCssPropertyForElement("lnkMiniCart", "background:", "", homePage.headers),
							"The Hover state of the Bag link should be displayed the white utility bar and the black Sign In text.", 
							"The Hover state of the Bag link is displayed the white utility bar and the black Sign In text.", 
							"The Hover state of the Bag link is not displayed the white utility bar and the black Sign In text.", driver);
			
			MiniCartPage miniCart= new MiniCartPage(driver).get();
			String flyoutName= miniCart.getProductName(0).toLowerCase();
			headers.mouseOverMyBag();
			
			Log.softAssertThat(flyoutName.contains(productName)
					&& miniCart.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("miniCartfluoytProductName1st","divProductAttributes","lblQuantity","lblPrice"),miniCart),
					"Mini cart flyout should have all carted products with product information.",
					"Mini cart flyout  have all carted products with product information.",
					"Mini cart flyout not have all carted products with product information.", driver);
			
			miniCart.clickOnProductName();
			Log.message(i++ +". Clicked product name from the flyout", driver);
			
			Log.softAssertThat(pdpPage.getProductName().toLowerCase().contains(productName),
					"The page should be navigated to PDP of the respective product",
					"The page is navigated to PDP of the respective product",
					"The page is not navigated to PDP of the respective product", driver);
			
			homePage.headers.mouseOverMyBag();			
			Log.softAssertThat(miniCart.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "divTopCheckOutButton", "divBottomCheckOutButton", miniCart),
					"Both top & bottom checkout button should be displayed in the flyout",
					"Both top & bottom checkout button are displayed in the flyout",
					"Both top & bottom checkout button are not displayed in the flyout", driver);
			
			miniCart.clickOnCheckOut();
			Log.message(i++ +". Clicked checkout button from the flyout", driver);
			
			Log.softAssertThat(cart.elementLayer.verifyPageElements(Arrays.asList("miniCartContent"), cart),
					"On click, page should be redirected to cart page.",
					"On click, page is redirected to cart page.",
					"On click, page is not redirected to cart page.", driver);
			
			double cartDiscount= cart.getOrderDiscount();
			headers.navigateToHome();
			headers.mouseOverMyBag();
			Log.softAssertThat(miniCart.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divSubTotal"),miniCart), 
					"Order subtotal should be displayed on the mini cart flyout",
					"Order subtotal is displayed on the mini cart flyout",
					"Order subtotal is not displayed on the mini cart flyout", driver);
			
			String subTotal = Double.toString(miniCart.getSubTotal());
			String calculatedSubTotal = Double.toString(miniCart.calculateSubTotal(cartDiscount));
			Log.softAssertThat(subTotal.equals(calculatedSubTotal),
					"Subtotal should be calculated as the total amount minus discounts.", 
					"Subtotal is calculated as the total amount minus discounts.",
					"Subtotal is not calculated as the total amount minus discounts.", driver);
			
			if(miniCart.elementLayer.verifyPageElements(Arrays.asList("scrollBar"), miniCart)) {
				Log.softAssertThat(miniCart.elementLayer.verifyPageElements(Arrays.asList("scrollBar"), miniCart),
						"Scroll Bar should be displayed to the right extreme side of the Mini Cart when the number of products are more than the fixed height of the fly-out in the mini cart.",
						"Scroll Bar displayed to the right extreme side of the Mini Cart when the number of products are more than the fixed height of the fly-out in the mini cart.",
						"Scroll Bar not displayed to the right extreme side of the Mini Cart when the number of products are more than the fixed height of the fly-out in the mini cart.", driver);
			} else {
				Log.reference("Only few products are there so scroll bar won't display");
			}
			Log.testCaseResult();

		}//End Try block
		catch(Exception e){
			Log.exception(e);
		}//End of catch block
		finally{
			Log.endTestCase(driver);
		}//End of finally block
	}//M1_FBB_E2E_C24813
}//TC_FBB_E2E_C24813
