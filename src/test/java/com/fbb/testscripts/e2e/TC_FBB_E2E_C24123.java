package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CategoryLandingPage;
import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PaypalConfirmationPage;
import com.fbb.pages.PaypalPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.TestData;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24123 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, priority = 2, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24123(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{

			//Step 1 - Navigate to the website
			
			String userEMailId = AccountUtils.generateEmail(driver);
			String checkoutShipAdd = "address_withtax";
			String checkoutBillAdd = "valid_address8";
			String level2Cat = TestData.get("level-2");
			String prodBadge = TestData.get("prd_badge").split("\\|")[0];
			//String prdDropship = TestData.get("prd_dropship");
			String prdVariation = TestData.get("prd_variation");
			String cpnFreeShip = TestData.get("cpn_freeshipping");
			String paypalemail = accountData.get("credential_paypal").split("\\|")[0];
			String paypalpassword = accountData.get("credential_paypal").split("\\|")[1];
			

			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 3 - Navigate to Product listing page

			PlpPage plpPage = homePage.headers.navigateTo(level2Cat);
			Log.message(i++ + ". Navigated to " + level2Cat.split("\\|")[1].trim(), driver);
			
			Log.softAssertThat((plpPage.verifyCategoryHeader(level2Cat.split("\\|")[1].trim())),
					"User should have navigated to correct category page.", 
					"User navigated to correct category page.", 
					"User has not navigated to correct category page.", driver);
			
			if(!Utils.isMobile()) {
				Log.softAssertThat((plpPage.elementLayer.verifyTextContains("divBreadcrumb",level2Cat.split("\\|")[0].trim(), plpPage)),
						"The current category should have category name visited", 
						"The current category is having category name visited", 
						"The current category is not having category name visited", driver);
			}
			
			double prdTile = plpPage.getSearchResultCount();
						
			if (prdTile <= 60) {
				if(Utils.isDesktop()){
					Log.softAssertThat(plpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtCurrentPageNo"), plpPage), 
							"PLP page should not have pagination if product count is less than 60", 
							"PLP page is not having pagination when product count is less than 60", 
							"PLP page is having pagination when product count is less than 60", driver);
				}else{
					Log.softAssertThat(plpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("divViewMore"), plpPage), 
							"View more button should not be displayed when product count is less than 60",
							"View more button is not displayed when product count is less than 60",
							"View more button is displayed when product count is less than 60", driver);
				}
			} else {
				if(Utils.isDesktop()){
					Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("paginationSection"), plpPage), 
							"PLP page should have pagination if product count is more than 60", 
							"PLP page is having pagination when product count is more than 60", 
							"PLP page is not having pagination when product count is more than 60", driver);
					
					Log.softAssertThat(plpPage.getLastPageNumber() == Math.ceil(prdTile/60) , 
							"PLP page should numbers should correctly displayed", 
							"PLP page numbers is displaying correctly", 
							"PLP page numbers is not displaying correctly", driver);
					
					int prevPgNo = plpPage.getCurrentPageNumber();
					
					plpPage.clickNextButtonInPagination();
					Log.message(i++ + ". Clicked on Next button in pagination",driver);
					
					Log.softAssertThat(plpPage.getCurrentPageNumber() != prevPgNo , 
							"Clicking on Next or Page number should navigate to the correct page", 
							"Clicking on Next or Page number is navigated to the correct page", 
							"Clicking on Next or Page number is not navigated to the correct page", driver);
					
					plpPage.clickOnPageNumberInPagination(1);
					Log.message(i++ + ". Clicked on page number in pagination",driver);
				}
			}
			
			int breadcrumbCount = plpPage.getBreadcrumbcount();
			if(!Utils.isMobile()) {
				if(breadcrumbCount>2) {
					for(int cate=breadcrumbCount-1;cate>1;cate--) {
						
						String category=plpPage.getBreadcrumbValueOf(cate);
						Log.message(i++ + ". Clicked on breadcrumb value :"+cate,driver);
						
						plpPage.clickOnBreadCrumbByIndexValue(cate);
						if(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage)) {
							String plpcategory= plpPage.getCategoryName();					
							Log.softAssertThat(plpcategory.equalsIgnoreCase(category),
									"On clicking "+cate+" th breadcrumb '"+category+"', system should navigates the user to the selected category PLP",
									"On clicking "+cate+" th breadcrumb '"+category+"', system is navigates the user to the selected category PLP", 
									"On clicking "+cate+" th breadcrumb '"+category+"', system is not navigates the user to the selected category PLP", driver);
						}else {
							CategoryLandingPage categoryPage= new CategoryLandingPage(driver).get();
							Log.softAssertThat(categoryPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"),categoryPage),
									"On clicking "+cate+" th breadcrumb '"+category+"', system should navigates the user to the selected category landing Page",
									"On clicking "+cate+" th breadcrumb '"+category+"', system is navigates the user to the selected category landing Page", 
									"On clicking "+cate+" th breadcrumb '"+category+"', system is not navigates the user to the selected category landing Page", driver);
						}
						plpPage = homePage.headers.navigateTo(level2Cat);
						Log.message(i++ + ". Navigated to " + level2Cat.split("\\|")[1].trim(), driver);
					}
				}
			}
			
			String colorShowMore;
			if(!Utils.isMobile()) {
				colorShowMore = "colorSwatchShowMoreLess";
			} else {
				colorShowMore = "colorSwatchShowMore";
			}
			if(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList(colorShowMore), plpPage)) {
				Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList(colorShowMore), plpPage), 
						"More Colors should be displayed", 
						"More Colors are displayed", 
						"More Colors are not displayed", driver);
			} else {
				Log.reference("Colors link is not displaying in any of the product");
			}
			
			//Step 4a - Navigate to Product detail page.

			PdpPage pdpPage = plpPage.navigateToPdp(1);
			Log.message(i++ + ". Navigated To PDP page : " + pdpPage.getProductName(), driver);
									
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			//Step 4b - Navigate to Product detail page.
			
			pdpPage = homePage.headers.navigateToPDP(prodBadge);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElements(Arrays.asList("prodBadge"), pdpPage), 
					"Badge should get display", 
					"Badge is getting displayed", 
					"Badge is not getting displayed", driver);
			
			//Step 6a - Navigate to the cart page
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			//Step 4c - Navigate to Product detail page.
			
			pdpPage = homePage.headers.navigateToPDP(prdVariation);
			Log.message(i++ + ". Navigated To PDP page : " + pdpPage.getProductName(), driver);
			
			//Step 6b - Navigate to the cart page
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Click on checkout in add to bag overlay!", driver);
			
			shoppingBagPg.applyPromoCouponCode(cpnFreeShip);
			Log.message(i++ + ". Promo coupon is applied!", driver);
			
			shoppingBagPg.clickOnCouponSeeDetails();
			Log.message(i++ + ". Clicked on See details!", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyPageElements(Arrays.asList("lnkHideCpnDetails"), shoppingBagPg)
							&& shoppingBagPg.elementLayer.verifyPageElements(Arrays.asList("txtDiscountDetails"), shoppingBagPg),
					"On clicking 'See Details' link Standard delivery message should be displayed with 'Hide Details' link", 
					"On clicking 'See Details' link Standard delivery message is displayed with 'Hide Details' link", 
					"On clicking 'See Details' link Standard delivery message is not displayed with 'Hide Details' link", driver);
			
			shoppingBagPg.clickOnCouponHideDetails();
			Log.message(i++ + ". Clicked on See details!", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyPageElements(Arrays.asList("txtCouponSeeDetails"), shoppingBagPg)
							&& shoppingBagPg.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("txtDiscountDetails"), shoppingBagPg),
					"On clicking 'Hide Details' link Standard delivery message should not be displayed", 
					"On clicking 'Hide Details' link Standard delivery message is not displayed", 
					"On clicking 'Hide Details' link Standard delivery message is displayed", driver);
			
			double orderPrice = shoppingBagPg.getOrderSubTotal();
			
			shoppingBagPg.updateQuantityByPrdIndex(1, "3");
			Log.message(i++ + ". Updated the quantity",driver);
			
			double orderUpdatedPrice = shoppingBagPg.getOrderSubTotal();
			
			Log.softAssertThat(orderUpdatedPrice > orderPrice, 
					"Subtotal in order sumary should be increased when the user updates the QTY", 
					"Subtotal in order sumary increased when the user updates the QTY", 
					"Subtotal in order sumary not increased when the user updates the QTY", driver);
			
			SignIn signin = (SignIn) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			//Step 7 - Navigate to the checkout login page
			
			CheckoutPage checkoutPg = (CheckoutPage)signin.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);
			
			//Step 8 - Navigate to Checkout page - step 1
			
			checkoutPg.continueToShipping(userEMailId);
			Log.message(i++ + ". Navigated to Shipping section", driver);
			
			if(!(checkoutPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("lblFreeShipping"), checkoutPg))) {
				Log.softAssertThat((checkoutPg.elementLayer.verifyTextContains("ordShippingDiscount", "-", checkoutPg)),
						"Shipping charge should be displaying with minus value ", 
						"Shipping charge is displaying with minus value ", 
						"Shipping charge is not displaying with minus value ", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("lblFreeShipping"), checkoutPg) && 
						checkoutPg.elementLayer.verifyTextContains("lblFreeShipping", "Free", checkoutPg),
						"'Free standard shipping' should display", 
						"'Free standard shipping' is displaying", 
						"'Free standard shipping' is not displaying", driver);
			
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("ordShippingDiscount"), checkoutPg),
						"The shipping cost should reduced to 0.00 in order pane", 
						"The shipping cost is reduced to 0.00 in order pane", 
						"The shipping cost is not reduced to 0.00 in order pane", driver);
				
				double totalStandardDelivery = StringUtils.getPriceFromString(checkoutPg.getOrderSummaryTotal());
				
				checkoutPg.selectShippingMethod(ShippingMethod.Express);
				Log.message(i++ + ". Selected to Express delivery option", driver);
				
				double totalOtherDelivery = StringUtils.getPriceFromString(checkoutPg.getOrderSummaryTotal());
				
				Log.softAssertThat(checkoutPg.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("ordShippingDiscount"), checkoutPg)
							&& (totalStandardDelivery < totalOtherDelivery),
						"Shipping cost should not be free anymore for other than standard delivery.", 
						"Shipping cost is not be free anymore for other than standard delivery.", 
						"Shipping cost still shows free for other than standard delivery.", driver);
				
				checkoutPg.selectShippingMethod(ShippingMethod.Standard);
				Log.message(i++ + ". Selected to standard delivery option", driver);
				
				double totalStandardDeliveryUpdate = StringUtils.getPriceFromString(checkoutPg.getOrderSummaryTotal());
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("ordShippingDiscount"), checkoutPg)
						&& (totalStandardDelivery == totalStandardDeliveryUpdate),
					"Free Standard Delivery message should updated properly under Order summary pane.", 
					"Free Standard Delivery message is updated properly under Order summary pane.", 
					"Free Standard Delivery message is not updated properly under Order summary pane.", driver);
				
			} else {
				Log.failsoft("'Free standard shipping' is not displaying",driver);
			}
			
			checkoutPg.fillingShippingDetailsAsGuest("NO", checkoutShipAdd, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(false);
			Log.message(i++ + ". Use this as a Billing address check box disabled", driver);
					
			//Step 9 - Navigate to Checkout page - step 2
			
			checkoutPg.continueToBilling();
			Log.message(i++ + ". Continued to Billing address", driver);
			
			checkoutPg.fillingBillingDetailsAsGuest(checkoutBillAdd);
			Log.message(i++ + ". Billing Address entered successfully", driver);
			
			checkoutPg.continueToPaymentFromBilling();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			checkoutPg.selectPaypalPayment();
			Log.message(i++ + ". Selected 'Paypal' as payment method", driver);
			
			PaypalPage paypalPage = checkoutPg.clickOnPaypalButton();
			Log.message(i++ + ". Clicked on Paypal button in shopping bag page", driver);
	
			PaypalConfirmationPage pcp = paypalPage.enterPayapalCredentials(paypalemail,paypalpassword);
			Log.message(i++ + ". Continued with Paypal Credentials.", driver);
			
			pcp.clickContinueConfirmation();
			Log.message(i++ + ". Clicked on Continue button.", driver);
	
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg), 
					"Checkout page should be displayed!", 
					"Checkout page is displayed",
					"Checkout page is not displayed", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			//Step 10 - Navigate to Checkout page - step 3
			
			OrderConfirmationPage receipt = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 11 - Order confirmation page
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally
	}
}//M1_FBB_E2E_C24123