package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24594 extends BaseTest {
	
	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	
		
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24594(String browser) throws Exception
	{	
		Log.testCaseInfo(); 
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		
		int i=1;
		try
		{
			String userEMailIdAlwaysApprove = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountData.get("password_global");
			String credentialAlwaysApprove = userEMailIdAlwaysApprove +"|"+ password;
			
			String addressAlwaysApprove = "plcc_always_approve_address";
			String firstNameAlwaysApprove = checkoutData.get("plcc_always_approve_address").split("\\|")[7];
			String lastNameAlwaysApprove = checkoutData.get("plcc_always_approve_address").split("\\|")[8];
			
			String prdVariation = TestData.get("prd_variation");
			String cardType = "cards_2";
			
			{
				//Step-1
				GlobalNavigation.registerNewUserWithUserDetail(driver, 0, 0, firstNameAlwaysApprove, lastNameAlwaysApprove, credentialAlwaysApprove);
				
				//Step-2
				GlobalNavigation.addNewAddressToAccount(driver, addressAlwaysApprove, true, credentialAlwaysApprove);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			MyAccountPage myAccount = homePage.headers.navigateToMyAccount(userEMailIdAlwaysApprove, password);
			Log.message(i++ + ". Navigated to My Account page as : " +userEMailIdAlwaysApprove, driver);
			
			Log.softAssertThat(myAccount.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), myAccount), 
					"User should be successfully created", 
					"User is created", 
					"User is not created", driver);
			
			//Step-3
			AddressesPage addressPg = myAccount.navigateToAddressPage();
			Log.message(i++ + ". Navigated to Address page!", driver);
			
			Log.softAssertThat(addressPg.getSavedAddressesCount() > 0, 
					"Address should be successfully created", 
					"Address is created", 
					"Address is not created", driver);
			
			//Step-4
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);		
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			CheckoutPage checkoutPg= (CheckoutPage) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to checkout page", driver);
			
			if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkoutPg)){
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("mdlPLCCApproval"), checkoutPg), 
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed", 
						"PLCC modal is not getting displayed", driver);
				
				//Step-5
				checkoutPg.continueToPLCCStep2();
				Log.message(i++ +". clicked Get, it today button", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApprovalStep2"), checkoutPg),
						"When user selects Get It Today, user will be taken to the Step 2 overlay. ",
						"When user selects Get It Today, user is taken to the Step 2 overlay. ",
						"When user selects Get It Today, user is not taken to the Step 2 overlay. ", driver);
				
				checkoutPg.typeTextInSSN("0000");
				Log.message(i++ +". Entered SSN number");
				
				checkoutPg.selectDateMonthYearInPLCC2("01","05","1947");
				Log.message(i++ +". Selected date of birth");
				
				checkoutPg.typeTextInMobileInPLCC("8015841844");
				Log.message(i++ +". Updated phone number", driver);
				
				checkoutPg.checkConsentInPLCC("YES");
				Log.message(i++ +". Selected consonent checbox", driver);
					
				checkoutPg.clickOnAcceptInPLCC();
				Log.message(i++ +". Clicked Yes, I Accept button", driver);
				
				if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("underReviewModal"), checkoutPg)) {
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("underReviewModal"), checkoutPg),
							"The Application Under Review modal will pop up.",
							"The Application Under Review modal is pop up.",
							"The Application Under Review modal is not pop up.", driver);
				} else {
					Log.reference("Under review modal is not displayed with \"Always Approve\" data", driver);
				}
				
				checkoutPg.dismissCongratulationModal();
				Log.message(i++ +". Clicking the Continue to Checkout button", driver);
				
				if(!checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("btnPaymentDetailsContinue"), checkoutPg)) {
					checkoutPg.continueToPayment();
				}
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("btnPaymentDetailsContinue"), checkoutPg), 
						"user will be taken to the payment method section of checkout.",
						"user will be taken to the payment method section of checkout.",
						"user will be taken to the payment method section of checkout.", driver);
				
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
				if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("lnkAddNewCredit"), checkoutPg)) {
					checkoutPg.clickAddNewCredit();
				}
			
				//Step-7 
				checkoutPg.fillingCardDetails1(cardType, false, false);
				Log.message(i++ +". Entered credit card details", driver);
				
				checkoutPg.clickOnPaymentDetailsContinueBtn();
				Log.message(i++ +". Clicked continue button", driver);
				
				OrderConfirmationPage order = checkoutPg.clickOnPlaceOrderButton();
				Log.softAssertThat(order.elementLayer.verifyPageElements(Arrays.asList("readyElement"), order),
						"user should taken to the order confirmation page.",
						"user is taken to the order confirmation page.",
						"user is not taken to the order confirmation page.",driver);
			} else{
				Log.fail("Checkout step-1 overlay is not displayed hence can't proceed further", driver);
			}	
						
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally
	}//M1_FBB_E2E_C24594
}//TC_FBB_E2E_C24594
