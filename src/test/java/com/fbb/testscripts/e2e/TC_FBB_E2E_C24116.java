package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24116 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24116(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{
			//Step 1 - Navigate to the website
			
			String userEMailId = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String checkoutAdd = "address_withtax";
			String payment = "card_Discover1";
			String eGiftCardPrd = TestData.get("prd_egift_card");
			String eGiftEmailAddress = "test@yopmail.com";
			String eGiftEmailAddress1 = "testqa@yopmail.com";
			String authenticated_egc_url = UrlUtils.getProtectedWebSiteWithYotta(Utils.getWebSite() + UrlUtils.getPath(TestData.get("prd_egift_card_url")));
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Navigate to the footer section
			PdpPage pdpPage = null;
			try {
				pdpPage = homePage.footers.navigateToEGiftCardPDP();
			} catch (Exception e) {
				try {
					driver.get(authenticated_egc_url);
				} catch(Exception ex) {
					pdpPage = homePage.headers.navigateToPDP(eGiftCardPrd);
				}
				pdpPage = new PdpPage(driver).get();
			}
			
			String prdName = pdpPage.getProductName();
			Log.message(i++ + ". Navigated to " + prdName, driver);
			
			//Step 4 - Navigate to Product detail page
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("drpGiftCardSize"), pdpPage), 
					"The system should display Amount drop-down box.", 
					"Amount drop-down box is displayed.", 
					"Amount drop-down box is not displayed.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("detailsReviewSection"), pdpPage), 
					"Reviews should not be displayed for E-gift card", 
					"Reviews are not displayed for E-gift card", 
					"Reviews are displayed for E-gift card", driver);
			
			pdpPage.openCloseToolTipOverlay("open");
			Log.message(i++ + ". Terms and Condition tool tip overlay is opened", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("closeToolTipOverLay"), pdpPage), 
					"Terms and Condition overlay should displaying", 
					"Terms and Condition overlay is displaying", 
					"Terms and Condition overlay is not displaying", driver);
			
			String[] itemIDs = {pdpPage.getProductID()};
			
			pdpPage.openCloseToolTipOverlay("close");
			Log.message(i++ + ". Terms and Condition tool tip overlay is closed", driver);
			
			pdpPage.selectGCSize();
			Log.message(i++ + ". Gift card amount selected", driver);
			
			Log.softAssertThat(Integer.parseInt(pdpPage.getSelectedGCAmount().trim().split("\\.")[0]) > 0, 
					"Amount should be selected", 
					"Amount is selected", 
					"Amount is not selected", driver);
			
			pdpPage.checkGCPersonalizedMessage("check");
			Log.message(i++ + ". 'Add a personalized message' check box checked", driver);
			
			pdpPage.clickAddToBagForErrorMsg();
			Log.message(i++ + ". Click add to bag button!", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("errEmailNotFill","errPleaseSelectConfirmMail","errConfirmMailEmpty","errMessageNotFill","errFromNotFill"), pdpPage), 
					"Error should get displayed when from, to and personel message is not entered", 
					"Error is getting displayed when from, to and personel message is not entered", 
					"Error is not getting displayed when from, to and personel message is not entered", driver);
			
			pdpPage.checkGCPersonalizedMessage("uncheck");
			Log.message(i++ + ". 'Add a personalized message' check box unchecked", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtAreaPersonalMsg","txtGCPersonalMsgFrom"), pdpPage), 
					"From and Message fields should not display when 'Add a personalized message' check box is unchecked", 
					"From and Message fields is not display when 'Add a personalized message' check box is unchecked", 
					"From and Message fields is display when 'Add a personalized message' check box is unchecked", driver);
			
			pdpPage.enterRecepientEmailAdd("invalid");
			Log.message(i++ + ". Entered invalid address in Recepient Email address filled", driver);
			
			pdpPage.enterRecepientEmailConfirmAdd("invalid");
			Log.message(i++ + ". Entered invalid address in Recepient Confirm Email address filled", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("errEmailNotFill", "errConfirmMailEmpty"), pdpPage), 
					"Error should get displayed when invalid address is entered", 
					"Error is displayed when invalid address is entered", 
					"Error is not displayed when invalid address is entered", driver);
			
			pdpPage.enterRecepientEmailAdd(eGiftEmailAddress);
			Log.message(i++ + ". Entered valid address in Recepient Email address filled", driver);
			
			pdpPage.enterRecepientEmailConfirmAdd("mismatchString" + eGiftEmailAddress);
			Log.message(i++ + ". Entered mismatch address in Recepient Confirm Email address filled", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("errConfirmMailEmpty"), pdpPage), 
					"Error should get displayed when address is mismatched in confirm mail address", 
					"Error is displayed when address is mismatched in confirm mail address", 
					"Error is not displayed when address is mismatched in confirm mail address", driver);
			
			pdpPage.enterRecepientEmailConfirmAdd(eGiftEmailAddress);
			Log.message(i++ + ". Entered mismatch address in Recepient Confirm Email address filled", driver);
			
			//Step 5 - //Navigate to the cart page
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Click add to bag button!", driver);
			
			ShoppingBagPage shoppingBagPg = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Click on checkout in add to bag overlay!", driver);
			
			Log.softAssertThat(!shoppingBagPg.verifyProductQuantityField(prdName), 
					"Quantity field should not be displayed for E-Gift Card", 
					"Quantity field is not displayed for E-Gift Card", 
					"Quantity field is displayed for E-Gift Card", driver);
			
			Log.softAssertThat(shoppingBagPg.verifyProductsAddedToCart(itemIDs), 
					"The system should allow the user to cart an Egift card without selecting the personalized message", 
					"The system allows the user to cart an Egift card without selecting the personalized message", 
					"The system is not allowing the user to cart an Egift card without selecting the personalized message", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("productNameSection","productUnitPrice","txtproductId"), shoppingBagPg), 
					"Gift card name, Product id and Price alone should be displayed", 
					"Gift card name, Product id and Price alone is displayed", 
					"Gift card name, Product id and Price alone is not displayed", driver);
			
			Object obj = shoppingBagPg.clickOnEditLink(0);
			
			if(Utils.isDesktop()) {
				QuickShop qsModal = (QuickShop) obj;
				
				qsModal.chkOrUnchkAddPersonalMsg(true);
				Log.message(i++ + ". 'Add a personalized message' check box checked", driver);
				
				qsModal.enterPersonalMessage("Personal Message");
				Log.message(i++ + ". Added Personal message!", driver);
				
				qsModal.enterFromAddress(eGiftEmailAddress1);
				Log.message(i++ + ". Added From address!", driver);
				
				qsModal.addToBag();
				Log.message(i++ + ". Updated the carted product!", driver);
			} else {
				pdpPage = (PdpPage) obj;
				pdpPage.checkGCPersonalizedMessage("check");
				Log.message(i++ + ". 'Add a personalized message' check box checked", driver);
				
				pdpPage.enterGCPersonalMessage("Personal Message");
				Log.message(i++ + ". Added Personal message!", driver);
				
				pdpPage.enterGCFromAddress(eGiftEmailAddress1);
				Log.message(i++ + ". Added From address!", driver);
				
				pdpPage.clickOnUpdate();
				Log.message(i++ + ". Updated the carted product!", driver);
			}
			
			//Step 6 - Navigate to the checkout sign in page
			
			SignIn signin = (SignIn) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			CheckoutPage checkoutPg = (CheckoutPage)signin.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);

			checkoutPg.continueToShipping(userEMailId);
			Log.message(i++ + ". Navigated to Shipping section with:: " + userEMailId, driver);

			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("lblGuestUserEmail"), checkoutPg), 
					"The system should accept the valid email address", 
					"The system accepts the valid email address", 
					"The system not accepts the valid email address", driver);
			
			Log.softAssertThat(checkoutPg.verifyNavigatedToBillingSection(),
					"The system should navigate the user to the checkout page -Step 2",
					"The system is navigated the user to the checkout page -Step 2",
					"The system is not navigated the user to the checkout page -Step 2", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementTextContains("txtDeliveryViaEmail", "Delivery via email", checkoutPg), 
					"The shipping address should be same as delivery Email address", 
					"The shipping address is same as delivery Email address", 
					"The shipping address is not same as delivery Email address", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementTextContains("lblEmailInShippingDetails", eGiftEmailAddress, checkoutPg), 
					"The shipping address should be same as delivery Email address", 
					"The shipping address is same as delivery Email address", 
					"The shipping address is not same as delivery Email address", driver);
			
			//Step 7 - Navigate to Checkout page - step 2
			checkoutPg.fillingBillingDetailsAsGuest(checkoutAdd);
			Log.message(i++ + ". Billing Address entered successfully", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			checkoutPg.fillingCardDetails1(payment, false, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			//Step 8 - Navigate to Checkout page - step 3
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			Log.softAssertThat(checkoutPg.comparePaymentBeforeOrderWithEnteredPaymentMethod(payment), 
					"The system should keep the payment details", 
					"Payment method is saved and displayed", 
					"Payment method is not saved correctly", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
							
			//Step 9 - Order confirmation page
			
			OrderConfirmationPage receipt = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			MyAccountPage myAcc = receipt.createUserInOrderSummary(accountData.get("password_global"));
			
			Log.softAssertThat(myAcc.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), myAcc), 
					"User account should get created", 
					"User account created", 
					"User account not created", driver);
			
			AddressesPage addrPg = myAcc.navigateToAddressPage();
			
			Log.softAssertThat(addrPg.getSavedAddressesCount() > 0 &&
							addrPg.checkEnteredBillingAddressReflectedInOrderReceipt(checkoutAdd),
					"Billing address should get saved", 
					"Billing address is saved", 
					"Billing address is not saved", driver);
			
			PaymentMethodsPage paymentPg = myAcc.navigateToPaymentMethods();
			
			Log.softAssertThat(paymentPg.getNumberOfSavedCards() > 0 &&
							paymentPg.elementLayer.verifyElementDisplayed(Arrays.asList("crdDiscover"), paymentPg) &&
							paymentPg.comparePaymentAfterOrderWithEnteredPaymentMethod(payment), 
					"Card details should get saved", 
					"Card details is saved", 
					"Card details is not saved", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24116
