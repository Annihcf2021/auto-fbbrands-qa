package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlatinumCardApplication;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C25905 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C25905(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
				
		int i=1;
		try
		{	
			String userEMailId = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountData.get("password_global");
			String credential = userEMailId + "|" + password;
			String prdVariation1 = TestData.get("prd_variation1");
			String prdVariation = TestData.get("prd_variation");
			String plCCAddress1 = checkoutData.get("plcc_always_approve_address");
			String firstName = plCCAddress1.split("\\|")[7];
			String lastName = plCCAddress1.split("\\|")[8];
			boolean reviewPlcaeOrderStatus = false;
			
			//Always Approve Data
			String plccAddress = "plcc_always_approve_address";
			String ssnDetails = checkoutData.get("SSNdetails");
			HashMap<String, String> userAddressM2 = GlobalNavigation.formatPLCCAddressToMapWithSSN(plCCAddress1, ssnDetails, driver, userEMailId);
			
			{
				GlobalNavigation.registerNewUserWithUserDetail(driver, 0, 0, firstName, lastName, credential);
			}
			
			//Step1
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to the 'Full Beauty Brands' Home Page!", driver);
			
			//Step2
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(userEMailId, password);
			Log.message(i++ + ". Navigated to My Account page as : " +userEMailId, driver);
			
			Log.softAssertThat(myAcc.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), myAcc),
					"User should be successfully created", 
					"User is created", 
					"User is not created", driver);
			
			//Step3
			AddressesPage addrPg = myAcc.navigateToAddressPage();
			Log.message(i++ + ". Navigated to Address page!", driver);
			
			addrPg.fillingAddressDetails(plccAddress, true);
			Log.message(i++ +". Added address to account", driver);
			
			addrPg.clickSaveAddress();
			
			Log.softAssertThat(addrPg.getSavedAddressesCount() > 0, 
					"Address should be successfully created", 
					"Address is created", 
					"Address is not created", driver);
			
			//Step4
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation1);
			Log.message(i++ + ". Navigated the To PDP page : "+pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to the bag", driver);
			
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to the shopping bag page", driver);
			
			CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to the Checkout page", driver);
			
			if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("modalCheckoutPlcc"), checkoutPg)) {
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("modalCheckoutPlcc"), checkoutPg), 
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed", 
						"PLCC modal is not getting displayed", driver);
				
				//Step5
				checkoutPg.clickNoThanksInPLCC();
				Log.message(i++ + ". Click on 'No Thanks' in PLCC!", driver);
				
				//Step6
				checkoutPg.clickOnBrandLogo();
				Log.message(i++ + ". Click on Brand logo!", driver);
				
				homePage.headers.signOut();
				Log.message(i++ + ". User signed out!", driver);
				
				homePage = BrowserActions.clearCookies(driver);
				Log.message(i++ + ". Cleared cookies and navigated to Homepage", driver);
				
				//Step7
				//Step8
				homePage = homePage.headers.navigateToHome();
				Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
				
				myAcc = homePage.headers.navigateToMyAccount(userEMailId, password);
				Log.message(i++ + ". Navigated back to My Account page as : " +credential.split("\\|")[0], driver);
				
				//Step9
				pdpPage = homePage.headers.navigateToPDP(prdVariation1);
				Log.message(i++ + ". Navigated to PDP page : "+pdpPage.getProductName(), driver);
				
				pdpPage.addToBagCloseOverlay();
				Log.message(i++ + ". Product added to cart", driver);
				
				shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping bag page", driver);
				
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("headingPLCCcontentslot"), shoppingBagPg), 
						"On the top of the model should display congratulations with firstname", 
						"On the top of the model displaying congratulations with firstname", 
						"On the top of the model is not displayed congratulations with firstname", driver);
				
				//Step10
				PlatinumCardApplication plccApplication  = shoppingBagPg.clickOnLearnmorelinkPlcc();
				Log.message(i++ + ". Navigated to Platinum Card Application page",driver);
				
				//Step11
				plccApplication.fillPreApprovedPLCCApplication(userAddressM2);
				Log.message(i++ +". Entered address in WIC", driver);
				
				plccApplication.enterTextOnField("txtMobileNoInPLCCStep2", "3334445555", "phone number", plccApplication);
				Log.message(i++ +". updated phone number", driver);
							
				plccApplication.clickCheckUncheckConsent(true);
				Log.message(i++ + ". Checked concent.", driver);
				
				plccApplication.clickOnPreApprovedRegisterBtn();
				Log.message(i++ + ". Clicked on SUBMIT button.", driver);
				
				if(plccApplication.elementLayer.verifyElementDisplayed(Arrays.asList("divApprovedModal"), plccApplication)) {
					Log.softAssertThat(plccApplication.elementLayer.verifyElementDisplayed(Arrays.asList("divApprovedModal"), plccApplication), 
							"PlatinumCard approved modal should get display", 
							"PlatinumCard approved modal is getting display", 
							"PlatinumCard approved modal is not getting displayed", driver);
				
					//Step12
					plccApplication.clickApprovedModalContinueShopping();
					Log.message(i++ + ". Clicked on Continue Shopping button.", driver);
				
				} else {
					
					plccApplication.dismissReviewModal();
					Log.message(i++ +". Dismissed plcc modal", driver);
					
					homePage.headers.navigateToHome();
				}
				
				Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
						"Home page should be displayed", 
						"Home page is getting displayed", 
						"Home page is not getting displayed", driver);
				
				//Step13
				pdpPage = homePage.headers.navigateToPDP(prdVariation);
				Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
				
				pdpPage.addToBagCloseOverlay();
				Log.message(i++ + ". Product added to cart", driver);
				
				ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
				
				//Step14
				checkoutPg = (CheckoutPage) cartPage.navigateToCheckout();
				Log.message(i++ + ". Navigated to Checkout page", driver);
				
				//Step15 - Filling shipping and billing address
				BrowserActions.scrollToTopOfPage(driver);
				
				if(checkoutPg.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("btnPlaceOrder"), checkoutPg)) {
					
					checkoutPg.continueToBilling();
				} else {
					reviewPlcaeOrderStatus = true;
				}
				
				if(checkoutPg.getCreditCardType().contains("plcc") || checkoutPg.getCreditCardType().contains("Credit Card")) {
					
					if(!(Utils.isMobile())) {
						Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("shippingAddressNickNamebold","selectedBillingAddress","selectedPLCCCard"), checkoutPg), 
							"The user's shipping and billing info is shown as well as the selected payment method should be displayed", 
							"The user's shipping and billing info is shown as well as the selected payment method is displayed", 
							"The user's shipping and billing info is shown as well as the selected payment method is not displayed", driver);
					}else {
						Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("shippingAddressNickNameboldMob","txtBillingaddressNickNameMob","selectedPLCCCard"), checkoutPg), 
							"The user's shipping and billing info is shown as well as the selected payment method should be displayed in mobile", 
							"The user's shipping and billing info is shown as well as the selected payment method is displayed in mobile", 
							"The user's shipping and billing info is shown as well as the selected payment method is not displayed in mobile", driver);
					}
					
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("selectedPLCCCard"), checkoutPg), 
							"PLCC card should be selected when PLCC card approved", 
							"PLCC card is selected when PLCC card approved", 
							"PLCC card is not selected when PLCC card approved", driver);
					
					checkoutPg.expandCollapsePromoCodeSection("expand");
					Log.message(i++ +". Opened promo code section", driver);
					
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementWithinElement(driver, "divDeferPayment", "couponSection", checkoutPg),
							"Deferred payment section should be displayed under promo code section.",
							"Deferred payment section is displayed under promo code section.",
							"Deferred payment section is not displayed under promo code section.", driver);
					
					if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
						Log.reference("Further verfication steps are not supported in current environment.");
						Log.testCaseResult();
						return;
					}
					
					if(!reviewPlcaeOrderStatus) {
						checkoutPg.continueToReivewOrder();
						if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("plccCardNoError"), checkoutPg)){
							Log.reference("PLCC card error hence can't proceed further", driver);
						} else{
							reviewPlcaeOrderStatus = true;
						}
					}
					
					if(reviewPlcaeOrderStatus) {
						//Step 17 - Place the order.
						OrderConfirmationPage orderPage = null;
						
						checkoutPg.clickOnPlaceOrder();
						Log.message(i++ + ". Clicked place order button", driver);			
						
						if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg)) {
							Log.reference("Always Approve PLCC card is not valid hence can't place order");
						} else {
							orderPage = new OrderConfirmationPage(driver).get();
							
							if(Utils.isMobile()) {
								orderPage.clickOnViewDetailsMobile();
							}
								
							Log.softAssertThat(orderPage.elementLayer.verifyNumberMaskedWithSpecifiedDigits("lblEnteredCardNo", 4, orderPage),
									"The last 4 digits of the PLCC card should be be displayed.",
									"The last 4 digits of the PLCC card is be displayed.",
									"The last 4 digits of the PLCC card is not displayed.", driver);
						}	
					}
				} else {
					Log.reference("PLCC card is not added hence can't proceed further", driver);
				}
			}else {
				Log.reference("PLCC Approval modal is not displayed hence can't proceed further", driver);
			}
		Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}
