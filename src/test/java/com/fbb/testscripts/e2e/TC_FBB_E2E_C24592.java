package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24592 extends BaseTest{
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups={ "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class , dataProvider="parallelTestDataProvider")
	public void M1_FBB_E2E_C24592(String browser) throws Exception{
		Log.testCaseInfo();
 
		final WebDriver driver= WebDriverFactory.get(browser);
					
		int i=1;
		try{
			String userEMailIdADS = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password=accountData.get("password_global");
			String rebuttalSlotMsg= "PRE-APPROVED";
			String rebuttalSlotMsg2= "Credit Card";
			String eGiftCardPrd = TestData.get("prd_egift_card");
						
			//Always Approve Data
			String billingAddressAlwaysApprove = "plcc_always_approve_address";
			
			//Step-1 & 2
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Navigate to the footer section
			PdpPage pdpPage = null;
			
			try {
				pdpPage = homePage.footers.navigateToEGiftCardPDP();
				Log.message(i++ + ". Navigated to gift card PDP page", driver);
			} catch (IndexOutOfBoundsException e) {
				Log.reference("E-Gift card page didn't have any product");
				pdpPage = homePage.headers.navigateToPDP(eGiftCardPrd);
				Log.message(i++ + ". Navigated to Pdp Page!", driver);
			}
			
			pdpPage.addToBagCloseOverlay();
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart Page.", driver);

			CheckoutPage checkoutPg = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to Checkout Page - SignIn Section", driver);
			
			checkoutPg.continueToShipping(userEMailIdADS);
			Log.message(i++ + ". Checkout As :: " + userEMailIdADS, driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("sectionBillingAddress"), checkoutPg),
					" user should be in the billing information section.",
					" user is in the billing information section.",
					" user is not in the billing information section.", driver);
			
			//Step-3
			checkoutPg.fillingBillingDetailsAsGuest(billingAddressAlwaysApprove);
			Log.message(i++ +". Filled billing address fields", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ +". Clicked continue button", driver);
			
			if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("mdlPLCCApproval"), checkoutPg)){
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("mdlPLCCApproval"), checkoutPg),
						"The preapproval modal should display.", 
						"The preapproval modal is displayed.",
						"The preapproval modal is not displayed.", driver);
				
				String firstName= checkoutPg.elementLayer.getElementText("firstNameInPLCCModal", checkoutPg);
				
				//Step-4 
				checkoutPg.closePLCCOfferByNoThanks1();
				Log.message(i++ +". Clicked no thanks button from PLCC overlay", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("mdlPLCCApproval"), checkoutPg)&& 
						checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"),checkoutPg),
						"The preapproval modal should be close and User should be taken to the payment section in checkout.",
						"The preapproval modal is closed and User is taken to the payment section in checkout.",
						"The preapproval modal is not closed and User is not taken to the payment section in checkout.", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "mdlPLCCRebuttal", "paymentmethodSection", checkoutPg),
						"The acquisition rebuttal modal should be displayed above the Add New Credit Card form.",
						"The acquisition rebuttal modal is displayed above the Add New Credit Card form.",
						"The acquisition rebuttal modal is not displayed above the Add New Credit Card form.", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyTextContains("mdlPLCCRebuttal", firstName, checkoutPg)
						&& checkoutPg.elementLayer.verifyTextContains("mdlPLCCRebuttal", rebuttalSlotMsg, checkoutPg)
						&& checkoutPg.elementLayer.verifyTextContains("mdlPLCCRebuttal", rebuttalSlotMsg2, checkoutPg),
						"The slot should consist first name and the offer content",
						"The slot is consist first name and the offer content",
						"The slot is not consist first name and the offer content", driver);
				
				//Step-5 
				checkoutPg.openClosePLCCRebuttal("open");
				Log.message(i++ +". Clicked learn more link from rebuttal solot", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCAcquisitionRebuttal"),checkoutPg),
						"The acquisition rebuttal over lay should opens up.",
						"The acquisition rebuttal over lay is opens up.",
						"The acquisition rebuttal over lay is not opens up.", driver);
				
				//Step-6 
				checkoutPg.continueToPLCCStep2InPLCCACQ();
				Log.message(i++ +". Clicked get it today button from rebuttal modal", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApprovalStep2"),checkoutPg),
						"The acquisition rebuttal step 2 overlay should opens up.",
						"The acquisition rebuttal step 2 overlay is opens up.",
						"The acquisition rebuttal step 2 overlay is not opens up.", driver);
				
				//Step-7
				checkoutPg.typeTextInSSN("1234");
				Log.message(i++ +". Entered SSN number", driver);
				
				checkoutPg.selectDateMonthYearInPLCC2("15", "08", "1947");
				Log.message(i++ +". Selected date, month, year", driver);
				
				checkoutPg.typeTextInMobileInPLCC("3334445555");
				Log.message(i++ +". Updated phone number", driver);
				
				checkoutPg.checkConsentInPLCC("YES");
				Log.message(i++ +". Checked Consent checkbox", driver);
				
				checkoutPg.clickOnAcceptInPLCC();
				Log.message(i++ +". Clicked yes, i accept button", driver);
				
				if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"),checkoutPg)) {
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"),checkoutPg),
						"The Approval modal should be displayed",
						"The Approval modal is displayed",
						"The Approval modal is not displayed", driver);
				
					//Step-8
					checkoutPg.dismissCongratulationModal();
					Log.message(i++ +". Clicking the Continue to Checkout button", driver);
					
					
					Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg), 
							"user will be taken to the payment method section of checkout.",
							"user will be taken to the payment method section of checkout.",
							"user will be taken to the payment method section of checkout.", driver);
					
					Log.softAssertThat(checkoutPg.checkPLCCCardSelectedBasedOnBrand(Utils.getCurrentBrandShort().substring(0, 2)) ||
							checkoutPg.checkPLCCCardSelectedBasedOnBrand(BrandUtils.getBrandFullName()), 
							" The PLCC type that the user just signed up for will be selected in the Selected Credit Card Type field.", 
							" The PLCC type that the user just signed up for selected in the Selected Credit Card Type field.",
							" The PLCC type that the user just signed up for will not be selected in the Selected Credit Card Type field.", driver);
					
					Log.softAssertThat(checkoutPg.elementLayer.verifyNumberMaskedWithSpecifiedDigits("fldTxtCardNo",4, checkoutPg),
							"The masked number and last 4 digits of the PLCC card should be displayed in the \"Card Number\" field.",
							"The masked number and last 4 digits of the PLCC card is displayed in the \"Card Number\" field.",
							"The masked number and last 4 digits of the PLCC card is not displayed in the \"Card Number\" field.", driver);
					
					Log.softAssertThat(checkoutPg.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("orderSummaryDiscount"), checkoutPg),
							"The $10 discount should not displayed in the Order Summary section.", 
							"The $10 discount is not displayed in the Order Summary section.",
							"The $10 discount is displayed in the Order Summary section.", driver);
									
					//Step-9
					checkoutPg.continueToReivewOrder();
					Log.message(i++ +". Navigated to review and place order screeen", driver);
					
					if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
						Log.reference("Further verfication steps are not supported in current environment.");
						Log.testCaseResult();
						return;
					}
					
					if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("plccCardNoError"), checkoutPg)) {
						Log.reference("plcc card numer error hence can't proceed further");
					
					} else {					
					
						Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrder"), checkoutPg),
								"Page should navigated to review and place order screen",
								"Page should navigated to review and place order screen",
								"Page should navigated to review and place order screen", driver);
						
						//Step-10
						OrderConfirmationPage orderPage= null;
						checkoutPg.clickOnPlaceOrder();
						Log.message(i++ +". Clicked place order button", driver);
						
						if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg)) {
							Log.message("<br>");
							Log.reference("Note that if the user signs up with Always Approve, "
									+ "the user will not be able to proceed to place an order with the card. "
									+ "This is because signing up with Always Approve returns a card number that's longer than our system accepts.");
							Log.message("<br>");
						} else {						
							orderPage = new OrderConfirmationPage(driver).get();
							
							Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage),
									"user should taken to the order confirmation page.",
									"user is taken to the order confirmation page.",
									"user is not taken to the order confirmation page.",driver);
							
							Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("OrderDetailsSection"), orderPage),
									" the order details should be displayed",
									" the order details is displayed",
									" the order details is not displayed",driver);
							
							if(Utils.isMobile()){
								orderPage.clickOnViewDetailsMobile();
								Log.message(i++ +". Clicked view details link", driver);
							}
							
							Log.softAssertThat(orderPage.elementLayer.verifyNumberMaskedWithSpecifiedDigits("lblEnteredCardNo", 4, orderPage),
									"The last 4 digits of the PLCC card should be be displayed.",
									"The last 4 digits of the PLCC card is be displayed.",
									"The last 4 digits of the PLCC card is not displayed.", driver);	
							
							//Step-11
							if(!Utils.isMobile()) {
								Log.softAssertThat(orderPage.elementLayer.verifyInsideElementAlligned("txtPasswordFld", "OrderDetailsSection","right" , orderPage)&&
										orderPage.elementLayer.verifyInsideElementAlligned("txtConfirmPasswordFld", "OrderDetailsSection","right" , orderPage),
										"On the right side of the order confirmation page, there should be a \"Password\" and \"Confirm Password\" field",
										"On the right side of the order confirmation page, there is a \"Password\" and \"Confirm Password\" field",
										"On the right side of the order confirmation page, there not a \"Password\" and \"Confirm Password\" field", driver);
							} else {
								Log.softAssertThat(orderPage.elementLayer.verifyInsideElementAlligned("txtPasswordFld", "creatAccountSection","bottom" , orderPage)&&
										orderPage.elementLayer.verifyInsideElementAlligned("txtConfirmPasswordFld", "creatAccountSection","bottom" , orderPage),
										"On the bottom side of the create account section, there should be a \"Password\" and \"Confirm Password\" field",
										"On the bottom side of the create account section, there is a \"Password\" and \"Confirm Password\" field",
										"On the bottom side of the create account section, there not a \"Password\" and \"Confirm Password\" field", driver);
							}
							
							orderPage.enterValueInPassword(password);
							orderPage.enterValueInConfirmPassword(password);
							Log.message(i++ +". Entered details in password and confirma password field", driver);
							
							orderPage.clickOnCreateAccount();	
							Log.message(i++ +". Clicked create account button", driver);
							
							Log.message("<br>");
							Log.reference("Order placed and account created with \"Always Approve\" non-ADS address data, "
								+ "So validation not applicable in My account and My address pages");
							Log.message("<br>");
						}
					}
				} else {
					Log.reference("Approval modal is not displayed", driver);
				}
			} else {
				Log.fail("Checkout step-1 overlay is not displayed hence can't proceed further", driver);
			}
				
		Log.testCaseResult();
				
		}//End of try
		catch(Exception e){
			Log.exception(e, driver);	
		}//End of catch
		finally{
			Log.endTestCase(driver);
		}//End of finally block
	}//M1_FBB_E2E_C24592
}//TC_FBB_E2E_C24592
