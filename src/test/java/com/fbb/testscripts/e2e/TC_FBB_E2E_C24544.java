package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;
@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24544 extends BaseTest{
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	@Test(groups= { "checkout", "mobile" }, dataProviderClass= DataProviderUtils.class , dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24544(String browser) throws Exception {
	
		Log.testCaseInfo(); 

		final WebDriver driver =  WebDriverFactory.get(browser);
		int i=1;
		try{
			
			String searchPrdSetTerm = TestData.get("ps_product-set");
			String guestEmail = AccountUtils.generateEmail(driver);
			String taxableAddress = "address_withtax";
			String cardType = "card_Visa";
			
			//Step-1 Navigate to the website
			HomePage homePage= new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Home Page launched successfully ", driver);
			
			//Step-2 Click on search field in the header
				
			PdpPage pdpPage = homePage.headers.navigateToPDPByProdName(searchPrdSetTerm);
			Log.message(i++ +". Searched with product name", driver);
			
			//Step-3 Navigate to Product set page
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "prodBreadCrumb","productSetproductName", pdpPage),
					"Product set name should display below the breadcrumb",
					"Product set name should display below the breadcrumb",
					"Product set name should display below the breadcrumb", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productSetproductName","productReviewStars", pdpPage),
					"Review stars should display below Product set name",
					"Review stars is displayed below Product set name",
					"Review stars is not displayed below Product set name", driver);
				
			if(Utils.isMobile()) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "prodImage_Mobile", "btnSlickDotSection", pdpPage) ,
						"Below the product set main image alternate images dots should be shown.",
						"Below the product set main image alternate images dots is shown.",
						"Below the product set main image alternate images dots is not shown.", driver);
			}
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnSlickDotSection", "lblMainPriceDetailsPrdSet_Tab_Mob", pdpPage) ,
					"Pricing should be displayed below the alternate images.",
					"Pricing is displayed below the alternate images.",
					"Pricing is not displayed below the alternate images.", driver);
			
			pdpPage.clickChooseitemsBelowProductSet();
			Log.message(i++ +". Clicked choose items below button", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("fistItemInProductSet"), pdpPage),
					" the system navigates the user to the 1st product on the list", 
					" the system is navigates the user to the 1st product on the list",
					" the system is not navigates the user to the 1st product on the list", driver);
			
			Log.softAssertThat(pdpPage.verifyPrdImgAndPriceDisplayedAboveShopNowButtonOrNot(),
					"Product image name and Pricing should be displayed above the \"Product Details\" link.",
					"Product image name and Pricing should be displayed above the \"Product Details\" link.",
					"Product image name and Pricing should be displayed above the \"Product Details\" link.", driver);
			
			List<String> pdpPrdNames =pdpPage.getChildProductNamesOfProductSet();
			
			pdpPage.OpenOrCloseShopNowDrawerBasedOnIndex(0, "opened");
			Log.message(i++ +". Clicked Product Details button of the second product",driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver,"productColorSwatches","productSizeSwatches",pdpPage),
					"Color and size swatches should be displayed and below size swatches, ",
					"Color and size swatches is displayed and below size swatches, ",
					"Color and size swatches is not displayed and below size swatches, ", driver);
			
			pdpPage.selectProductSetColorSwatchBasedOnIndex(0, 0);
			Log.message(i++ +". Selected color seatch for the first product", driver);
			
			Log.softAssertThat(pdpPage.verifyIfSelectedColorDisplayedAsPrimaryProductImagePrdSet(1), 
					"The colorized image should display on the main image section",
					"The colorized image is displayed on the main image section",
					"The colorized image is not displayed on the main image section", driver);
			
			pdpPage.selectProductSetSizeSwatchBasedOnIndex(0, 0);
			Log.message(i++ +". Selected size swatche for the product", driver);
			
			pdpPage.clickProductSetAddToBagBasedOnIndex(0);
			Log.message(i++ +". Clicke add to bag button of first product set", driver);
			
			pdpPage.clickOnContinueShoppingInMCOverlay();
			Log.message(i++ +". Clicked continue shopping button from ATB overlay", driver);
			
			Log.softAssertThat(pdpPage.getShopNowDrawerState(0).equalsIgnoreCase("opened"),
					"Verify below the Add to bag button, \"Close\" button should be displayed", 
					"Verify below the Add to bag button, \"Close\" button is displayed",
					"Verify below the Add to bag button, \"Close\" button is not displayed", driver);
			
			pdpPage.OpenOrCloseShopNowDrawerBasedOnIndex(0, "closed");
			Log.message(i++ +". Closed Product Details drawer", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("firstProductSetDetails","productColorSwatches","productSizeSwatches","btnFirstProductSetAddToBag"), pdpPage),
					" color & size swatches, product details and \"Add to bag\" button should be hidden and Product Details button should be shown",
					" color & size swatches, product details and \"Add to bag\" button is hidden and Product Details button is shown",
					" color & size swatches, product details and \"Add to bag\" button is not hidden and Product Details button is not shown", driver);
			
			pdpPage.addAllProductsToCartFromPrdSet();
			Log.message(i++ +". Added all products to cart", driver);
			
			//Step-4 Navigate to the cart page
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ +". Navigated to shopping bag page", driver);
			
			int cartItemsSize = cartPage.getCartItemNameList().size();
			
			Log.softAssertThat(cartPage.getCartItemNameList().containsAll(pdpPrdNames), 
					"Added products should be displayed on the cart page.",
					"Added products are displayed on the cart page.",
					"Added products are not displayed on the cart page.", driver);
					
			Log.softAssertThat(cartPage.elementLayer.verifyListElementSize("lstProductDetails", cartItemsSize, cartPage),
					"Added product details should be displayed in the Item section.",
					"Added product details are displayed in the Item section.",
					"Added product details are not displayed in the Item section.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "productDetails", "imgProductSection", cartPage),
					"Product image should be displayed on the left side of the product details",
					"Product image is displayed on the left side of the product details",
					"Product image is not displayed on the left side of the product details", driver);
			
			Log.softAssertThat((cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgProductSection", "btnEditDetails_Mobile_Tablet", cartPage) &&
					cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgProductSection", "lnkAddToWishlist", cartPage)) &&
					cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "imgProductSection", "btnRemove", cartPage), 
					"Below the product image \"Edit\", \"Add to Wishlist\", \"Remove\" button should be shown.",
					"Below the product image \"Edit\", \"Add to Wishlist\", \"Remove\" button is shown.", 
					"Below the product image \"Edit\", \"Add to Wishlist\", \"Remove\" button is not shown.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productDetails", "divItemQuantity", cartPage)&&
					cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divItemQuantity", "txtSubtotal", cartPage),
					"Qty drop box should be displayed below the product details.Below the Qty drop box Subtotal should be shown.",
					"Qty drop box is displayed below the product details.Below the Qty drop box Subtotal is shown.", 
					"Qty drop box is not displayed below the product details.Below the Qty drop box Subtotal is not shown.", driver);			
			
			double beforeUpdateTotal = cartPage.getOrderTotal();
						
			cartPage.updateQuantityByPrdIndex(0,"3");
			Log.message(i++ +". Updated qty of the second product", driver);
			
			Log.softAssertThat( cartPage.getOrderTotal()>beforeUpdateTotal, 
					"Verify the order summary section & confirm the price is updated & subtotal is correct.",
					"the order summary section & the price is updated & subtotal is correct.",
					"the order summary section & the price is not updated & subtotal is correct.", driver);
			
			//Step-5 Navigate to the checkout sign in page
			
			SignIn signIn = (SignIn) cartPage.navigateToCheckout();
			
			CheckoutPage checkoutPg = (CheckoutPage) signIn.clickSignInButton();
			Log.message(i++ +". Navigated to checkout login page", driver);
			
			Log.softAssertThat((checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "guestLoginBox", "lblShippingTab", checkoutPg)&&
					checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "guestLoginBox", "divCheckoutBilling", checkoutPg)) &&
					checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "guestLoginBox", "divCheckoutReviewPlaceOrder", checkoutPg),
					"Below \"checkout as guest\" section, Shipping Details, Payment Details, Review & Place order section should be displayed.",
					"Below \"checkout as guest\" section, Shipping Details, Payment Details, Review & Place order section is displayed.",
					"Below \"checkout as guest\" section, Shipping Details, Payment Details, Review & Place order section is not displayed.", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "guestLoginBox", "headerShoppingCartList", checkoutPg),
					" Below \"checkout as guest\" section, Item in shopping bag section should be displayed",
					" Below \"checkout as guest\" section, Item in shopping bag section is displayed",
					" Below \"checkout as guest\" section, Item in shopping bag section is not displayed", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "headerShoppingCartList", "checkoutOrderSummary", checkoutPg),
					" Below Item, in the shopping bag, section Order summary pane should be displayed.",
					" Below Item, in the shopping bag, section Order summary pane is displayed.",
					" Below Item, in the shopping bag, section Order summary pane is not displayed.", driver);
			
			checkoutPg.continueToShipping(guestEmail);
			Log.message(i++ +".  Added a valid email address and clicked on the continue button.", driver);
			
			//Step-6 Navigate to Checkout page step -1
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("lblShippingTab"), checkoutPg),
					"Shipping Details Header should be shown.",
					"Shipping Details Header is shown.",
					"Shipping Details Header is not shown.", driver);
			
			checkoutPg.fillingShippingDetailsAsGuest(taxableAddress, ShippingMethod.Standard);
			Log.message(i++ +". Filled shipping address details", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "txtFirstnameShipping", "chkUseBillingAddress", checkoutPg),
					"\"Use this as Billing Address\" checkbox should be displayed below the fields.",
					"\"Use this as Billing Address\" checkbox is displayed below the fields.",
					"\"Use this as Billing Address\" checkbox is not displayed below the fields.", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "shippingSection", "lblDeliveryOptionHeading", checkoutPg),
					" The Delivery option should be displayed below the Shipping address section.",
					" The Delivery option is displayed below the Shipping address section.",
					" The Delivery option is not displayed below the Shipping address section.", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "shippingSection", "lblGiftOptionsHeading", checkoutPg),
					"Gift option should be displayed below the shipping address section.",
					"Gift option is displayed below the shipping address section.",
					"Gift option is not displayed below the shipping address section.", driver);
			
			checkoutPg.selectShippingMethod(ShippingMethod.Express);
			Log.message(i++ +". Selected the express delivery option", driver);
			
			if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("divShipMedOverlayCont"),checkoutPg)) {
				checkoutPg.clickOnContinueInShippingMethodExceptionOverlay();
			}
			checkoutPg.continueToPayment();
			Log.message(i++ +". Navigated to payment details section", driver);
			
			//Step-7 Navigate to Checkout page - step 2
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("divShippingAddress"), checkoutPg),
					" Address details should be properly displayed in the Shipping Details section.",
					" Address details are properly displayed in the Shipping Details section.",
					" Address details are not properly displayed in the Shipping Details section.", driver);

			Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblBillingAddressSelected", "radioCreditCard", checkoutPg)&&
					checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblBillingAddressSelected", "rdoPaypal", checkoutPg),
					" Below the billing address section, payment details credit card and pay pal options should be displayed.",
					" Below the billing address section, payment details credit card and pay pal options is displayed.",
					" Below the billing address section, payment details credit card and pay pal options is not displayed.", driver);
						
			checkoutPg.fillingCardDetails1(cardType, false);
			Log.message(i++ +". filled CC card details", driver);
			
			Log.softAssertThat(!checkoutPg.getSaleTax().replace("$","").equalsIgnoreCase("0.00"),
					"Tax amount should be properly calculated and displayed in the order summary section.",
					"Tax amount is properly calculated and displayed in the order summary section.",
					"Tax amount is not properly calculated and displayed in the order summary section.", driver);
							
			checkoutPg.continueToReivewOrder();
			Log.message(i++ +". Clicked continue button", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			//Step-8 Navigate to Checkout page - step 3
			
			String orderTotal = (checkoutPg.getOrderSummaryTotal());
			OrderConfirmationPage orderConfirm = checkoutPg.clickOnPlaceOrderButton();
			
			//Step-9 Order confirmation page
			Log.softAssertThat(orderConfirm.getOrderSummaryTotal().equals(orderTotal),
					"Order total amount same",
					"Order total amount same",
					"Order total amount same", driver);
			
			Log.softAssertThat(!(orderConfirm.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);

			if(orderConfirm.elementLayer.verifyPageElements(Arrays.asList("purchaseSection"), orderConfirm)){
				Log.softAssertThat(orderConfirm.elementLayer.verifyPageElements(Arrays.asList("purchaseblock"), orderConfirm),
						"The system should display a review section ",
						"The system is displayed a review section ",
						"The system is not displayed a review section ", driver);
				
				Log.softAssertThat(orderConfirm.elementLayer.verifyPageElements(Arrays.asList("purchasedCommentSection"), orderConfirm),
						" User allowed to add the comments on the purchased item.",
						" User is allowed to add the comments on the purchased item.",
						" User is not allowed to add the comments on the purchased item.", driver);
				
				orderConfirm.addMsgInPurchasedItemSection("Added message in purchase section");
				
				Log.softAssertThat(orderConfirm.elementLayer.verifyPageElements(Arrays.asList("thanksMsgInPurchaseSection"), orderConfirm),
						"Added review for the purchased item and Thank you message should be displayed.",
						"Added review for the purchased item and Thank you message is displayed.",
						"Added review for the purchased item and Thank you message is not displayed.", driver);
			}else{
				Log.reference("Purchase section is not displayed on order confirmation page");
			}
			
			Log.testCaseResult();
			
		}//End of try
		catch(Exception e){
			Log.exception(e, driver);
		}// End of catch 
		finally{
			Log.endTestCase(driver);
		}//End of finally
	}//M1_FBB_E2E_C24544

}//TC_FBB_E2E_C24544
