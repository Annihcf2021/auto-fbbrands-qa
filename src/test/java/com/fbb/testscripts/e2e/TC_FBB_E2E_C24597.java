package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24597 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	
	@Test(groups = { "myaccount", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24597(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			String userEMailId = AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = userEMailId + "|" + password;
			String card1 = "card_MasterCard";
			String card2 = "card_FullBeautyPlatinum";
			String card3 = "card_Visa";
			String cardDetails1 = checkoutData.get(card1);
			String cardDetails2 = checkoutData.get(card2);
			String cardDetails3 = checkoutData.get(card3);
			String checkoutAdd = "address_withtax";
			String paymentError = TestData.get("paymentError");
			String prdVariation = TestData.get("prd_variation");
			
			
			{
				GlobalNavigation.registerNewUser(driver, 0, 0, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(AccountUtils.generateEmail(driver), accountData.get("password_global"));
			Log.message(i++ + ". Navigated to My Account page as : " +AccountUtils.generateEmail(driver), driver);
			
			//step 1 - Verify the functionality of Credit Card Information
			
			PaymentMethodsPage paymentMethods = myAcc.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to 'Payment methods' Page!", driver);
			
			Log.softAssertThat(paymentMethods.elementLayer.verifyElementDisplayed(Arrays.asList("lblNoSavedCardMessage"), paymentMethods),
					"The No Saved Cards message should be displayed",
					"The No Saved Cards message is displayed",
					"The No Saved Cards message is not displayed", driver);
			
			Log.softAssertThat(paymentMethods.elementLayer.verifyPageElements(Arrays.asList("txtNameOnCard","txtCardNumber","selectCardTypeField","selectMonthField","selectYearField"), paymentMethods),
					"'Card type', 'Month', 'Year', 'Name on card', 'Card Number' fields should be display",
					"'Card type', 'Month', 'Year', 'Name on card', 'Card Number' fields is displayed",
					"'Card type', 'Month', 'Year', 'Name on card', 'Card Number' fields is not displayed", driver);
			
			paymentMethods.savePaymentMethod();
			Log.message(i++ + ". Clicked on save payment!", driver);
			
			homePage.footers.clickOnEmailSignUp();
			Log.message(i++ +". Closed credit dropdown list", driver);
			
			Log.softAssertThat(paymentMethods.elementLayer.verifyElementDisplayed(Arrays.asList("selectCardTypeError","selectMonthError","selectYearError","inpOwnerError","inpCardNumberError"), paymentMethods),
					"'Card type error', 'Month error', 'Year error', 'Name on card error', 'Card Number error' should be display",
					"'Card type error', 'Month error', 'Year error', 'Name on card error', 'Card Number error' is displayed",
					"'Card type error', 'Month error', 'Year error', 'Name on card error', 'Card Number error' is not displayed", driver);
			
			Log.softAssertThat(paymentMethods.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("selectCardTypeError","selectMonthError","selectYearError","nameOnCardError","cardNumberError"), "color", paymentError, paymentMethods),
					"'Card type', 'Month', 'Year', 'Name on card', 'Card Number' placeholders should display in red",
					"'Card type', 'Month', 'Year', 'Name on card', 'Card Number' placeholders is displayed in red",
					"'Card type', 'Month', 'Year', 'Name on card', 'Card Number' placeholders is not displayed in red", driver);
			
			paymentMethods.typeName("$##@");
			Log.message(i++ + ". Typed invalid characters in Name!", driver);
			
			paymentMethods.savePaymentMethod();
			
			homePage.footers.clickOnEmailSignUp();
			
			Log.softAssertThat(paymentMethods.elementLayer.verifyElementDisplayed(Arrays.asList("inpOwnerError"), paymentMethods),
					"'Name on card' should be display an error",
					"Error is displayed on 'Name on card' field",
					"Error is not displayed on 'Name on card' field", driver);
			
			homePage.headers.navigateToMyAccount().navigateToPaymentMethods();
						
			paymentMethods.typeCardNumber("1234567891234567232");
			Log.message(i++ + ". Card number with extra length typed in.", driver);
			
			Log.softAssertThat(paymentMethods.getCardNumberLength() == 19, 
					"System should not allow the user to enters more than 19 digits.", 
					"Number limit is 19.", 
					"Number limit is not 19.", driver);
			
			Log.softAssertThat(paymentMethods.isMakeDefaultPaymentChecked(), 
					"Make default should be checked by default", 
					"Make default is checked by default", 
					"Make default is not checked by default.", driver);
			
			paymentMethods.fillCardDetailsUsingProperty(card1, false, true);
			Log.message(i++ + ". Filled in card details.", driver);
			
			paymentMethods.typeName("");
			Log.message(i++ + ". Removed name!", driver);
			
			paymentMethods.savePaymentMethod();
			Log.message(i++ + ". Clciked on save payment!", driver);
			
			Log.softAssertThat(paymentMethods.elementLayer.verifyElementDisplayed(Arrays.asList("inpOwnerError"), paymentMethods),
					"'Name on card' should be display an error",
					"Error is displayed on 'Name on card' field",
					"Error is not displayed on 'Name on card' field", driver);
			
			paymentMethods.typeName(cardDetails1.split("\\|")[1]);
			Log.message(i++ + ". Added name!", driver);
			
			paymentMethods.savePaymentMethod();
			Log.message(i++ + ". Clicked on save payment!", driver);
			
			Log.softAssertThat(paymentMethods.elementLayer.verifyHorizontalAllignmentOfElements(driver, "cardName", "cardImage", paymentMethods),
					"Card logo should display before the card name",
					"Card logo is displayed before the card name",
					"Card logo is not displayed before the card name", driver);	
			
			if(Utils.isDesktop() || Utils.isTablet()) {
				Log.softAssertThat(paymentMethods.elementLayer.verifyHorizontalAllignmentOfElements(driver, "cardHolderName", "cardName", paymentMethods),
						"Card holder name should display next to the card name",
						"Card holder name is displayed next to the card name",
						"Card holder name is not displayed next to the card name", driver);
				
				Log.softAssertThat(paymentMethods.elementLayer.verifyHorizontalAllignmentOfElements(driver, "cardNumber", "cardHolderName", paymentMethods),
						"Card holder name should display before the card number",
						"Card holder name is displayed before the card number",
						"Card holder name is not displayed before the card number", driver);
				
				Log.softAssertThat(paymentMethods.elementLayer.verifyHorizontalAllignmentOfElements(driver, "cardExpDate", "cardNumber", paymentMethods),
						"Card ExpDate should display next to the card number",
						"Card ExpDate is displayed next to the card number",
						"Card ExpDate is not displayed next to the card number", driver);
			} else {
				Log.softAssertThat(paymentMethods.elementLayer.verifyVerticalAllignmentOfElements(driver, "cardName", "cardHolderName", paymentMethods),
						"Card holder name should display next to the card name",
						"Card holder name is displayed next to the card name",
						"Card holder name is not displayed next to the card name", driver);
				
				Log.softAssertThat(paymentMethods.elementLayer.verifyVerticalAllignmentOfElements(driver, "cardHolderName", "cardNumber", paymentMethods),
						"Card holder name should display before the card number",
						"Card holder name is displayed before the card number",
						"Card holder name is not displayed before the card number", driver);
				
				Log.softAssertThat(paymentMethods.elementLayer.verifyVerticalAllignmentOfElements(driver, "cardNumber", "cardExpDate", paymentMethods),
						"Card ExpDate should display next to the card number",
						"Card ExpDate is displayed next to the card number",
						"Card ExpDate is not displayed next to the card number", driver);
			}
			
			Log.softAssertThat(paymentMethods.elementLayer.verifyTextContains("cardNumber", "**", paymentMethods) &&
					paymentMethods.verifyCardPresent(cardDetails1.split("\\|")[2]),
					"Card number should be masked",
					"Card number is masked",
					"Card number is not masked", driver);
			
			//step 2 -Verify the checkout page
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			ShoppingBagPage shoppingBagPg = pdpPage.navigateToShoppingBag();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			checkoutPg.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", ShippingMethod.Standard, checkoutAdd);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("lblSelectedCard"), checkoutPg),
					"Card should be saved and selected as default",
					"Card is saved and selected as default",
					"Card is not selected as default", driver);
			
			homePage = checkoutPg.clickPrimaryLogo();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			homePage.headers.navigateToMyAccount();
			Log.message(i++ + ". Navigated to 'My Account' Page!", driver);
			
			//step 3 - Navigate to My account Payment tab again
			
			paymentMethods = myAcc.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to 'Payment methods' Page!", driver);
			
			paymentMethods.clickAddNewCardLink();
			Log.message(i++ + ". Clicked on!", driver);
			
			paymentMethods.selectCardType(cardDetails2.split("\\|")[0]);
			Log.message(i++ + ". Selected a PLCC card type!", driver);
			
			Log.softAssertThat(paymentMethods.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtNameOnCard","drpExpiryMonth","drpExpiryYear"), paymentMethods),
					"'Month', 'Year', 'Name on card' fields should not display",
					"'Month', 'Year', 'Name on card' fields is not displayed",
					"'Month', 'Year', 'Name on card' fields is displayed", driver);
			
			paymentMethods.fillCardDetailsUsingProperty(card3,false,true);
			paymentMethods.savePaymentMethod();
			
			Log.softAssertThat(paymentMethods.verifyCardPresent(cardDetails3.split("\\|")[2]),
					"Card should be added",
					"Card is added",
					"Card is not added", driver);
			
			shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			checkoutPg.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", ShippingMethod.Standard, checkoutAdd);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyListElementSize("lblSavedCardDetails", 2, checkoutPg),
					"All saved Card should be display",
					"All saved Card is displaying",
					"All saved Card is not displaying", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyTextContains("lblSelectedCard", cardDetails3.split("\\|")[0], checkoutPg),
					"Default address should be selected in payment section",
					"Default address is selected in payment section",
					"Default address is not selected in payment section", driver);
			
			homePage = checkoutPg.clickPrimaryLogo();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			homePage.headers.navigateToMyAccount();
			Log.message(i++ + ". Navigated to 'My Account' Page!", driver);
			
			paymentMethods = myAcc.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to 'Payment methods' Page!", driver);
			
			//step 5 - Verify the 'Delete' link
			
			int cardCount = paymentMethods.getNumberOfSavedCards();
			paymentMethods.clickDeleteOnSavedCard(1);
			Log.message(i++ + ". Clicked on delete button on card.", driver);
			
			Log.softAssertThat(paymentMethods.elementLayer.verifyElementDisplayed(Arrays.asList("btnConfirmDelete", "btnCancelDelete"), paymentMethods), 
					"Confirm and Cancel buttons should be displayed", 
					"Confirm and Cancel buttons are displayed", 
					"Confirm and Cancel buttons are not displayed", driver);
			
			paymentMethods.clickCancelOnDeleteConfirmation();
			Log.message(i++ + ". Clicked CANCEL on delete confirmation.", driver);
			
			Log.event("Card count: " + paymentMethods.getNumberOfSavedCards());
			Log.softAssertThat(paymentMethods.getNumberOfSavedCards() == cardCount, 
					"When user clicks on Cancel button, System should not remove payment method.", 
					"When user clicks on Cancel button, System did not remove payment method.", 
					"When user clicks on Cancel button, System removed payment method.", driver);
			
			cardCount = paymentMethods.getNumberOfSavedCards();
			paymentMethods.clickDeleteOnSavedCard(1);
			Log.message(i++ + ". Clicked on delete button on card.", driver);
			
			paymentMethods.clickDeleteOnDeleteConfirmation();
			Log.message(i++ + ". Clicked DELETE on delete confirmation.", driver);
			
			Log.event("Card count: " + paymentMethods.getNumberOfSavedCards());
			Log.softAssertThat(paymentMethods.getNumberOfSavedCards() == (cardCount-1), 
					"When user clicks on Confirm button, System should remove payment method.", 
					"When user clicks on Confirm button, System removed payment method.", 
					"When user clicks on Confirm button, System did not remove payment method.", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24125
