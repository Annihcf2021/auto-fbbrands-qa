package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.TestData;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24804 extends BaseTest{
	
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");

	@Test(groups = { "internationalslots", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24804(String browser) throws Exception
	{
		Log.testCaseInfo();			 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		String searchText = TestData.get("prd_variation");
		String userEMailId = AccountUtils.generateEmail(driver);
		String checkoutAdd = "taxless_address";
		String zipCode = checkoutData.get(checkoutAdd).split("\\|")[4];
		String payment = "card_Visa";
		
		int i=1;
		try
		{
			
			HomePage homePage= new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Home Page launched successfully ", driver);
		
			PdpPage pdpPage = homePage.headers.navigateToPDP(searchText);
			Log.message(i++ +". Navigated to PDP", driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". Added product to bag", driver);
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ +". Clicked on mini bag icon & navigates to the cart page", driver);
			
			boolean internationalDetected = cartPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "internationalCartBanner", "headerShoppingBag", cartPage);
			
			if(internationalDetected) {
				Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "internationalCartBanner", "headerShoppingBag", cartPage),
						"The International Shipping message slot should be displayed above the Cart Heading.",
						"The International Shipping message slot is displayed above the Cart Heading.",
						"The International Shipping message slot not displayed above the Cart Heading.", driver);
			}
			
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ +". Navigated to checkout page", driver);
			
			checkoutPage.clickOnContinueInGuestLogin();
			
			Log.softAssertThat(checkoutPage.elementLayer.isElementsDisplayed(Arrays.asList("lblGuestEmailError"), checkoutPage),
					"The system should display the email address placeholder field as the red color",
					"The system is displaying the email address placeholder field as the red color",
					"The system is not displaying the email address placeholder field as the red color", driver);
			
			checkoutPage.continueToShipping(userEMailId);
			Log.message(i++ +". Navigated to shipping details section", driver);
			
			if(internationalDetected) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "lblShippingDetailsHeading", "internationalCheckoutBanner", checkoutPage),
						"International slot should be displayed below the Shipping Address Heading.",
						"International slot is displayed below the Shipping Address Heading.",
						"International slot not displayed below the Shipping Address Heading.", driver);
			}
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			checkoutPage.fillingShippingDetailsAsGuest("yes", checkoutAdd, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPage.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			checkoutPage.fillingCardDetails1(payment, false, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			OrderConfirmationPage orderConfirm = checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ +". Placed order", driver);
			
			Log.softAssertThat(!(orderConfirm.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			String orderNumber = orderConfirm.getOrderNumber();
			
			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Navigated to the sign in page", driver);
		    
		    signIn.fillOrderFields(orderNumber, userEMailId, zipCode);
		    Log.message(i++ + ". Filled order details in check an order section", driver);
		    
		    signIn.clickOnCheckInOrder();
		    Log.message(i++ + ". Navigated to order details page", driver);
		    
		    Log.softAssertThat(signIn.elementLayer.verifyElementDisplayed(Arrays.asList("divOrderNumberPrint"), signIn),
					"Order Number should be displayed",
					"Order Number is displayed",
					"Order Number not displayed", driver);
		    
		    if(internationalDetected) {
			    if(!Utils.isMobile()){
					Log.softAssertThat((signIn.elementLayer.verifyElementDisplayedleft("divOrderNumberPrint", "internationalBannerLink", signIn) &&
										signIn.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "internationalOrderPageBanner", "divOrderNumberPrint", signIn)),
							"Order Number should be displayed top left below the My Account Navigation Pane or below the International Slot",
							"Order Number is displayed top left below the My Account Navigation Pane or below the International Slot",
							"Order Number not displayed top left below the My Account Navigation Pane or below the International Slot", driver);
				} else {
			    	Log.softAssertThat(signIn.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "internationalOrderPageBanner", "divOrderNumberPrint", signIn),
							"Order Number should be displayed below the International Slot",
							"Order Number is displayed below the International Slot",
							"Order Number not displayed below the International Slot", driver);		
				}
			} else {
		   		Log.reference("International slot is not displaying", driver);
		    }
		    
			Log.testCaseResult();
			
		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally
	}
}