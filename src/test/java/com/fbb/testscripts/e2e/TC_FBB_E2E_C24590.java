package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24590 extends BaseTest{
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24590(String browser) throws Exception{
		Log.testCaseInfo(); 
		
		final WebDriver driver =WebDriverFactory.get(browser);
				
		String userEMailId = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
		String password = accountData.get("password_global");
		
		
		String addressAlwaysApprove = "plcc_always_approve_address";
		
		String prdVariation = TestData.get("prd_variation1");
						
		int i=1;
		try{			
			//Step-1
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step-2
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			cartPage.updateOrderTotalMeetPlccDiscount(prdVariation);
			Log.message(i++ + ". Updated product quantity to meet PLCC discount", driver);
			
			CheckoutPage checkoutPg = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to Checkout Page - SignIn Section", driver);
			
			checkoutPg.continueToShipping(userEMailId);
			Log.message(i++ + ". Checkout As :: " + userEMailId, driver);
			
			//Step-3
			checkoutPg.fillingShippingDetailsAsGuest("yes", addressAlwaysApprove, ShippingMethod.Standard);
			Log.message(i++ +". Entered shipping details", driver);
			
			Log.softAssertThat(checkoutPg.verifyUseThisAsBillingAddressIsChecked(), 
					"The Use this as billing address checkbox should be checked",
					"The Use this as billing address checkbox is checked",
					"The Use this as billing address checkbox is not checked", driver);
				
			checkoutPg.continueToPayment();
			Log.message(i++ +". Clicked continue button from shipping address section", driver);
				
			if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkoutPg)){
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkoutPg),
						"The preapproval modal should display. ",
						"The preapproval modal is display. ",
						"The preapproval modal is not displayed. ", driver);
				
				//Step-4 
				checkoutPg.continueToPLCCStep2();
				Log.message(i++ +". clicked Get, it today button", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApprovalStep2"), checkoutPg),
						"When user selects Get It Today, user will be taken to the Step 2 overlay. ",
						"When user selects Get It Today, user is taken to the Step 2 overlay. ",
						"When user selects Get It Today, user is not taken to the Step 2 overlay. ", driver);
						
				//Step-5 
				checkoutPg.typeTextInSSN("1234");
				Log.message(i++ +". Entered SSN number");
				
				checkoutPg.selectDateMonthYearInPLCC2("01","05","1947");
				Log.message(i++ +". Selected date of birth");
				
				checkoutPg.typeTextInMobileInPLCC("8015841844");
				Log.message(i++ +". Updated phone number", driver);
						
				checkoutPg.checkConsentInPLCC("yes");
				Log.message(i++ +". Selected consonent checbox", driver);
						
				checkoutPg.clickOnAcceptInPLCC();
				Log.message(i++ +". Clicked Yes, I Accept button", driver);
				
				if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("approvedModal"), checkoutPg)) {
										
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"), checkoutPg),
							"The Congratulations modal will pop up.",
							"The Congratulations modal is pop up.",
							"The Congratulations modal is not pop up.", driver);
							
					//Step-6 
					checkoutPg.dismissCongratulationModal();
					Log.message(i++ +". Clicking the Continue to Checkout button", driver);
							
					Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg), 
							"user will be taken to the payment method section of checkout.",
							"user will be taken to the payment method section of checkout.",
							"user will be taken to the payment method section of checkout.", driver);
					
					Log.softAssertThat(checkoutPg.checkPLCCCardSelectedBasedOnBrand(Utils.getCurrentBrandShort().substring(0, 2))
									|| checkoutPg.checkPLCCCardSelectedBasedOnBrand(BrandUtils.getBrandFullName()), 
							"The PLCC type that the user just signed up for will be selected in the Selected Credit Card Type field.", 
							"The PLCC type that the user just signed up for selected in the Selected Credit Card Type field.",
							"The PLCC type that the user just signed up for will not be selected in the Selected Credit Card Type field.", driver);
					
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryDiscount"), checkoutPg)
									&& checkoutPg.getOrderSummaryDiscount() == -10.0d, 
							"$10 PLCC discount should be displayed in the Order Summary section.", 
							"$10 PLCC discount is displayed in the Order Summary section.",
							"$10 PLCC discount is not displayed in the Order Summary section.", driver);
				
					if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
						Log.reference("Further verfication steps are not supported in current environment.");
						Log.testCaseResult();
						return;
					}
					
					if(!checkoutPg.dummyDataRecieved()) {
						checkoutPg.clickOnPaymentDetailsContinueBtn();
						Log.message("Clicked on continue button from payment section", driver);
						
						if (checkoutPg.elementLayer.verifyPageElements(Arrays.asList("plccCardNoError"), checkoutPg)) {
							Log.failsoft("PLCC card number error, hence can't proceed further", driver);

						} else {
							OrderConfirmationPage orderPage = null;

							checkoutPg.clickOnPlaceOrder();
							Log.message(i++ + ". Clicked on place order.", driver);

							if (checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("errPlaceOrder"), checkoutPg)) {
								Log.reference("By using Always Approve data, user can't place order", driver);

							} else {
								orderPage = new OrderConfirmationPage(driver).get();

								Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"),orderPage),
										"User should be taken to the order confirmation page.",
										"User is taken to the order confirmation page.",
										"User is not taken to the order confirmation page.", driver);

								Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("OrderDetailsSection"), orderPage),
										" the order details should be displayed",
										" the order details is displayed",
										" the order details is not displayed", driver);

								if (Utils.isMobile()) {
									orderPage.clickOnViewDetailsMobile();
									Log.message("Clicked view details link in mobile view", driver);
								}

								Log.softAssertThat(orderPage.elementLayer.verifyNumberMaskedWithSpecifiedDigits("lblEnteredCardNo", 4, orderPage),
										"The last 4 digits of the PLCC card should be be displayed.",
										"The last 4 digits of the PLCC card is be displayed.",
										"The last 4 digits of the PLCC card is not displayed.", driver);

								orderPage.enterValueInPassword(password);
								orderPage.enterValueInConfirmPassword(password);
								Log.message(i++ + ". Entered details in password and confirma password field", driver);

								orderPage.clickOnCreateAccount();
								Log.message(i++ + ". Clicked create account button", driver);

								Log.reference("Order placed and account created with \"Always Approve\" address data, "
										+ "So validation not applicable in My account and My address pages");
							}
						}

					}
				} else {
					Log.failsoft("Congradulation model not displayed with ALWAYS APPROVE address data", driver);
				}
			} else {
				Log.fail("PLCC Step-1 overlay is not displayed with ALWAYS APPROVE address data, Futher verification is not proceeded");
			}
		
		Log.testCaseResult();	
		}// End of try
		catch(Exception e){
			Log.exception(e, driver);
		}//End of catch
		finally{
			Log.endTestCase(driver);
		}//End of finally		
	}
}
