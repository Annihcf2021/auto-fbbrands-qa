package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24807 extends BaseTest {
	
	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	
		
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24807(String browser) throws Exception{	
		Log.testCaseInfo(); 
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{
			String userEMailIdAlwaysApprove = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountData.get("password_global");
			String credentialAlwaysApprove = userEMailIdAlwaysApprove +"|"+ password;
			
			String addressAlwaysApprove = "plcc_always_approve_address";
			String firstNameAlwaysApprove = checkoutData.get("plcc_always_approve_address").split("\\|")[7];
			String lastNameAlwaysApprove = checkoutData.get("plcc_always_approve_address").split("\\|")[8];
			
			String prdNoPLCCDiscount = TestData.get("prd_noDiscountPLCC");
							
			{
				//Step-1
				GlobalNavigation.registerNewUserWithUserDetail(driver, 1, 0, firstNameAlwaysApprove, lastNameAlwaysApprove, credentialAlwaysApprove);
				
				//Step-2
				GlobalNavigation.addNewAddressToAccount(driver, addressAlwaysApprove, false, credentialAlwaysApprove);
			}
			
			//Step1
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step2
			MyAccountPage myAccount = homePage.headers.navigateToMyAccount(userEMailIdAlwaysApprove, password);
			Log.message(i++ + ". Navigated to My Account page as : " +userEMailIdAlwaysApprove, driver);
			
			
			Log.softAssertThat(myAccount.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), myAccount), 
					"User should be successfully created", 
					"User is created", 
					"User is not created", driver);
			
			//Step3
			//Step4
			AddressesPage addressPg = myAccount.navigateToAddressPage();
			Log.message(i++ + ". Navigated to Address page!", driver);
			
			//Step5
			addressPg.deleteDefaultAddress();
			
			Log.softAssertThat(addressPg.getSavedAddressesCount() > 0, 
					"Address should be successfully created", 
					"Address is created", 
					"Address is not created", driver);
			
			//Step6
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdNoPLCCDiscount);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			cartPage.updateOrderTotalMeetPlccDiscount(prdNoPLCCDiscount);
			Log.message(i++ + ". Updated product quantity to meet PLCC discount.", driver);
			
			CheckoutPage checkoutPg= (CheckoutPage) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to checkout page", driver);
			
			//Step7
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Clicked continue button ", driver);
			
			if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkoutPg)){				
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("mdlPLCCApproval"), checkoutPg), 
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed", 
						"PLCC modal is not getting displayed", driver);
				//Step8
				checkoutPg.continueToPLCCStep2();
				Log.message(i++ +". clicked Get, it today button", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApprovalStep2"), checkoutPg),
						"When user selects Get It Today, user will be taken to the Step 2 overlay. ",
						"When user selects Get It Today, user is taken to the Step 2 overlay. ",
						"When user selects Get It Today, user is not taken to the Step 2 overlay. ", driver);
			
				//Step9
				checkoutPg.typeTextInSSN("1234");
				Log.message(i++ +". Entered SSN number");
				
				checkoutPg.selectDateMonthYearInPLCC2("01","05","1947");
				Log.message(i++ +". Selected date of birth");
				
				checkoutPg.typeTextInMobileInPLCC("8015841844");
				Log.message(i++ +". Updated phone number", driver);
				
				checkoutPg.checkConsentInPLCC("YES");
				Log.message(i++ +". Selected consonent checbox", driver);
				
				checkoutPg.clickOnAcceptInPLCC();
				Log.message(i++ +". Clicked Yes, I Accept button", driver);
			
				if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("approvedModal"), checkoutPg)) {
									
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"), checkoutPg),
							"The Congratulations modal will pop up.",
							"The Congratulations modal is pop up.",
							"The Congratulations modal is not pop up.", driver);
					
					//Step10
					checkoutPg.dismissCongratulationModal();
					Log.message(i++ +". Clicking the Continue to Checkout button", driver);				
					
					Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg) &&
							checkoutPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("approvedModal"), checkoutPg), 
							"The Approval modal should be closed and user will be taken to the payment method section of checkout.",
							"The Approval modal is closed and user will be taken to the payment method section of checkout.",
							"The Approval modal is not closed and user will be taken to the payment method section of checkout.", driver);
					
					Log.softAssertThat(checkoutPg.checkPLCCCardSelectedBasedOnBrand(Utils.getCurrentBrandShort().substring(0, 2)) ||
							checkoutPg.checkPLCCCardSelectedBasedOnBrand(BrandUtils.getBrandFullName()), 
							" The PLCC type that the user just signed up for will be selected in the Selected Credit Card Type field.", 
							" The PLCC type that the user just signed up for selected in the Selected Credit Card Type field.",
							" The PLCC type that the user just signed up for will not be selected in the Selected Credit Card Type field.", driver);
					
					if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("fldTxtCardNo"), checkoutPg)) {
						Log.softAssertThat(checkoutPg.elementLayer.verifyNumberMaskedWithSpecifiedDigits("fldTxtCardNo",4, checkoutPg),
								"The masked number and last 4 digits of the PLCC card should be displayed in the \"Card Number\" field.",
								"The masked number and last 4 digits of the PLCC card is displayed in the \"Card Number\" field.",
								"The masked number and last 4 digits of the PLCC card is not displayed in the \"Card Number\" field.", driver);
					} else {
						Log.reference("PLCC card is added to account, so verifying last 4 digits with Always Approve card is not possible");
					}				
				
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryDiscount"), checkoutPg),
							"The $10 discount should be displayed in the Order Summary section.", 
							"The $10 discount is displayed in the Order Summary section.",
							"The $10 discount is not displayed in the Order Summary section.", driver);
					
				//Step-8
				checkoutPg.continueToReivewOrder();
				Log.message(i++ +". Navigated to review and place order screeen", driver);
			
				if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("plccCardNoError"),checkoutPg)) {
					Log.reference("PLCC card error, hence can't proceed further", driver);
				
				} else {
					Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrder"), checkoutPg),
							"Page should navigated to review and place order screen",
							"Page should navigated to review and place order screen",
							"Page should navigated to review and place order screen", driver);
					
					if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
						Log.reference("Further verfication steps are not supported in current environment.");
						Log.testCaseResult();
						return;
					}
					//Step-9
					OrderConfirmationPage orderPage = null;
					
					checkoutPg.clickOnPlaceOrder();
					Log.message(i++ +". Clicked place order button", driver);
					
					if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("errPlaceOrder"), checkoutPg)) {
						Log.reference("By useing Always Approve card, User can't place order", driver);
					
					} else {
						orderPage = new OrderConfirmationPage(driver).get();
						
						Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage),
								"user should taken to the order confirmation page.",
								"user is taken to the order confirmation page.",
								"user is not taken to the order confirmation page.",driver);
						
						Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("OrderDetailsSection"), orderPage),
								" the order details should be displayed",
								" the order details is displayed",
								" the order details is not displayed",driver);
						
						if(Utils.isMobile()) {
							orderPage.clickOnViewDetailsMobile();
							Log.message("Clicked on the view details link in mobile view", driver);
						}
						
						Log.softAssertThat(orderPage.elementLayer.verifyNumberMaskedWithSpecifiedDigits("lblEnteredCardNo", 4, orderPage),
								"The last 4 digits of the PLCC card should be be displayed.",
								"The last 4 digits of the PLCC card is be displayed.",
								"The last 4 digits of the PLCC card is not displayed.", driver);						
					}					
				}
			} else {
				Log.reference("Congradulation model not displayed with ALWAYS APPROVE address data, Hence can't proceed further", driver);
			}
		} else {
			Log.fail("PLCC Step-1 overlay is not displayed with ALWAYS APPROVE address data, Futher verification is not proceeded");
		}
					
		Log.testCaseResult();
		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally
	}//M1_FBB_E2E_C24807
}//TC_FBB_E2E_C24807
