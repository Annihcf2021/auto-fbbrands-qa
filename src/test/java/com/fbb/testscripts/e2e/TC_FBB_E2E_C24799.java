package com.fbb.testscripts.e2e;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.account.MailDropPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24799 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "email", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24799(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			String prodId = TestData.get("prd_reviewed");
			String emailIdReview1 = accountData.get("emailIdDropMail");
			int inboxCount = 0;
			
			{
				try {
					inboxCount = GlobalNavigation.getInboxCountFromMailDrop(emailIdReview1, driver);
					Log.message("Inbox count for " + emailIdReview1 + " is " + inboxCount);
				} catch(Exception e) {
					Log.message("Inbox count for " + emailIdReview1 + " is " + inboxCount);
				}
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);	
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prodId);
			String productName = pdpPage.getProductName();
			Log.message(i++ + ". Navigated to PDP Page for product :: " + productName, driver);
			
			pdpPage.clickOnReviewTab();
			Log.message(i++ +". Clicked on review tab", driver);
			
			//Step 3: Verify write a Review Module.			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("sectionReviewVisible"), pdpPage), 
					"Check the review section is opened",
					"The review section is opened",
					"The review section is not opened", driver);
			
			pdpPage.clickOnWriteAReview();
			Log.message(i++ +". Clicked on write review", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("sectionWriteAReviewScreen"), pdpPage), 
					"Write Review modal window should be opened",
					"Write Review modal window is opened",
					"Write Review modal window not opened", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtReviewProductName","reviewProductImage"), pdpPage), 
					"Product image and product name should display in the review modal.",
					"Product image and product name is displayed in the review modal.",
					"Product image and product name not displayed in the review modal.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "reviewProductImage", "txtReviewProductName", pdpPage), 
					"The product image should display on the right-hand side of the product name",
					"The product image is displayed on the right-hand side of the product name",
					"The product image not displayed on the right-hand side of the product name", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementTextEqualTo("txtReviewProductName", productName, pdpPage), 
					"Product name in the review modal should matches with the product name of the item being reviewed.",
					"Product name in the review modal matches with the product name of the item being reviewed.",
					"Product name in the review modal not matches with the product name of the item being reviewed.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "reviewTxtShareExperience", "txtReviewProductName", pdpPage), 
					"The product name should display below the 'Please share your experience'",
					"The product name is displayed below the 'Please share your experience'",
					"The product name not displayed below the 'Please share your experience'", driver);
			
			//Step 4: Verify different Attributes on the write a Review Module.		
			pdpPage.clickOnReviewStar(4);
			Log.message(i++ + ". Selected the Review stars", driver);
			
			pdpPage.clickOnFitRadioButton(1);
			Log.message(i++ + ". Selected the Review Fit ", driver);
			
			pdpPage.clickOnProsAndConsCheckBox(1, 1);
			Log.message(i++ + ". Selected the Review Pros & cons ", driver);
			
			pdpPage.enterWriteReviewTitle("Aspire QA");
			Log.message(i++ + ". Entered a title for the review in the Title text box.", driver);
			
			pdpPage.enterWriteReviewDesc();
			Log.message(i++ + ". Entered comments in the Comments text box.", driver); 
			
			pdpPage.clickAddPhoto();
			Log.message(i++ + ". Clicked on Add Photo button", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("modalAddPhotoVideo"), pdpPage), 
					"When the user clicks on Add Photos/ Add Videos buttons, Popup window should appears",
					"When the user clicks on Add Photos/ Add Videos buttons, Popup window appears'",
					"When the user clicks on Add Photos/ Add Videos buttons, Popup window not appears'", driver);
			
			pdpPage.clickAddVideo();
			
			//Skipped attach media from computer steps.
			
			pdpPage.closeAddPhotoVideoModal();
			Log.message(i++ + ". Clicked on Close button of Add Photo/ Video modal", driver);		
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("sectionWriteAReviewScreen"), pdpPage), 
					"If user click on cancel button or X link System should redirects user to the write a review modal.",
					"If user click on cancel button or X link System redirects user to the write a review modal.",
					"If user click on cancel button or X link System not redirects user to the write a review modal.", driver);
			
			pdpPage.clickOnSubmitReview();
			Log.message(i++ + ". Clicked on Submit button", driver);
			
			Utils.openNewTab(driver);
			List<String> handle = new ArrayList<String> (driver.getWindowHandles());
			driver.switchTo().window(handle.get(handle.size() - 1));
			
			MailDropPage mail = new MailDropPage(driver).get();
			Log.message(i++ + ". MailDrop page loaded successfully", driver);
			
			mail.navigateToDropMailBox(emailIdReview1);
			Log.message(i++ + ". Navigated to usermail inbox ", driver);
			
			mail.openReceivedMail(emailIdReview1, inboxCount, "Review confirmation email");
			Log.message(i++ + ". Navigated to Review confirmation mail ", driver);
			
			pdpPage = mail.clickReviewConfirmEmailAddress();
			Log.message(i++ + ". Clicked Confirm email address button", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("divEmailConfirmationModal"), pdpPage), 
					"Email confirmation Thank you' message should be displayed.",
					"Email confirmation Thank you' message is displayed.",
					"Email confirmation Thank you' message is not displayed.", driver);
			
			pdpPage.clickContinueShoppingEmailConfirmationModal();
			Log.message(i++ + ". Clicked on Continue My shopping button from Email confirmation modal", driver);
			
			pdpPage.clickReviewMyPost();
			Log.message(i++ + ". Clicked on My Post link", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("modalReviewMyPost"), pdpPage), 
					"Click on My Posts link popup window should appears.",
					"Click on My Posts link popup window appears.",
					"Click on My Posts link popup window not appears.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "sectionStatistics", "textYourPost", pdpPage), 
					"The 'correct statistics' should be the right of 'Your Posts'",
					"The 'correct statistics' are there to the right of 'Your Posts'",
					"The 'correct statistics' are not there to the right of 'Your Posts'", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "topLinksMyPost", "textPoweredByTurnTo", pdpPage) &&
					pdpPage.elementLayer.verifyCssPropertyForElement("textPoweredByTurnTo", "color", "", pdpPage), 
					"'Powered by TurnTo' should written in grey in the top left corner.",
					"'Powered by TurnTo' is written in grey in the top left corner.",
					"'Powered by TurnTo' not written in grey in the top left corner.", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyListElementTextEqualTo(Arrays.asList("textStatistics"), 0, "Reviews", pdpPage), 
					"Correct statistics that should be displayed 'Reviews'",
					"Correct statistics that displayed 'Reviews'",
					"Correct statistics that not displayed 'Reviews'", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyListElementTextEqualTo(Arrays.asList("textStatistics"), 2, "Questions", pdpPage), 
					"Correct statistics that should be displayed 'Questions'",
					"Correct statistics that displayed 'Questions'",
					"Correct statistics that not displayed 'Questions'", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyListElementTextEqualTo(Arrays.asList("textStatistics"), 4, "Answers", pdpPage), 
					"Correct statistics that should be displayed 'Answers'",
					"Correct statistics that displayed 'Answers'",
					"Correct statistics that not displayed 'Answers'", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyListElementTextEqualTo(Arrays.asList("textStatistics"), 1, "Helpful Vote", pdpPage), 
					"Correct statistics that should be displayed 'Helpful Vote'",
					"Correct statistics that displayed 'Helpful Vote'",
					"Correct statistics that not displayed 'Helpful Vote'", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyListElementTextEqualTo(Arrays.asList("textStatistics"), 3, "Others Had This Question", pdpPage), 
					"Correct statistics that should be displayed 'Others Had this Question'",
					"Correct statistics that displayed 'Others Had this Question'",
					"Correct statistics that not displayed 'Others Had this Question'", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyListElementTextEqualTo(Arrays.asList("textStatistics"), 5, "Best Answer Votes", pdpPage), 
					"Correct statistics that should be displayed 'Best Answer Votes'",
					"Correct statistics that displayed 'Best Answer Votes'",
					"Correct statistics that not displayed 'Best Answer Votes'", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyListElementTextEqualTo(Arrays.asList("textStatistics"), 6, "Checkout Comments", pdpPage), 
					"Correct statistics that should be displayed 'Checkout Comments'",
					"Correct statistics that displayed 'Checkout Comments'",
					"Correct statistics that not displayed 'Checkout Comments'", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("reviewsBottomTabsMyPost", "questionsBottomTabsMyPost", "answersBottomTabsMyPost", "commentsBottomTabsMyPost"), pdpPage), 
					"On the bottom, there should be tabs for Reviews, Questions, Answers, and Comments. ",
					"On the bottom, there are tabs for Reviews, Questions, Answers, and Comments. ",
					"On the bottom, there are no tabs for Reviews, Questions, Answers, and Comments. ", driver);
			
			pdpPage.clickMyPostBottomTabBasedOnIndex(1, "Reviews");
			
			pdpPage.clickMyPostBottomTabBasedOnIndex(2, "Questions");
			
			pdpPage.clickMyPostBottomTabBasedOnIndex(3, "Answers");
			
			pdpPage.clickMyPostBottomTabBasedOnIndex(4, "Checkout Comments");
			Log.message(i++ + ". Verified user can navigate through Reviews, Questions, Comments, and Answers tabs", driver); 
				
			pdpPage.clickMySettings();
			Log.message(i++ + ". Clicked on My Settings link.", driver); 
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "textPoweredByTurnTo", "textBasicInformation", pdpPage), 
					"'Basic Information' should be on the top left, underneath Powered by TurnTo.",
					"'Basic Information' is on the top left, underneath Powered by TurnTo.",
					"'Basic Information' not on the top left, underneath Powered by TurnTo.", driver);
			
			pdpPage.elementLayer.clearTextFields(Arrays.asList("firstNameMySettings","lstNameMySettings"), pdpPage);
			pdpPage.clickUpdateMySettings();
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("textErrorMySettings"), pdpPage), 
					"First Name and Last Name should be mandatory",
					"First Name and Last Name are mandatory",
					"First Name and Last Name are not mandatory", driver);
			
			pdpPage.addFirstNameAndLastName("Aspire", "QA");
			
			pdpPage.addEmailInMySettings("aspire@");
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("textErrorMySettings"), pdpPage), 
					"Invalid email address error message should display",
					"Invalid email address error message is displayed",
					"Invalid email address error message not displayed", driver);
			
			pdpPage.addEmailInMySettings("qaautomation@gmail.com");
			
			pdpPage.clickUpdateMySettings();
		
			pdpPage.removeEmailInMySettings();
			
			pdpPage.clickAddPhotoMySettings();
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("chooseFileMySettings"), pdpPage), 
					"Under 'Photo', there should be a grey 'Choose file' link in the smaller font",
					"Under 'Photo', there is a grey 'Choose file' link in the smaller font",
					"Under 'Photo', there is no grey 'Choose file' link in the smaller font", driver);
			
			pdpPage.clickChangePasswordMySettings();
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("currentPasswordMySettings", "newPasswordMySettings", "conifrmPasswordMySettings"), pdpPage), 
					"Clicks on change password link Current password, new password & confirm password field should appears.",
					"Clicks on change password link Current password, new password & confirm password field appears.",
					"Clicks on change password link Current password, new password & confirm password field not appears.", driver);
			
			pdpPage.clickNotFirstName();
			Log.message(i++ + ". Clicked on 'Not FirstName?' link", driver);
			
			pdpPage.clickReviewMyPost();
			Log.message(i++ + ". Clicked on My Post link", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("modalFindUrProfile"),pdpPage),
					"You should prompted to log into another account.",
					"You should prompted to log into another account.",
					"You should prompted to log into another account.", driver);

			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24799
