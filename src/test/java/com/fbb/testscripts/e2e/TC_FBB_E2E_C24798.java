package com.fbb.testscripts.e2e;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.footers.Curalate;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24798 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");

	@Test( groups = { "footer", "high", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24798(String browser) throws Exception {
		Log.testCaseInfo();
	
		// Get the web driver instance
		final WebDriver driver = WebDriverFactory.get(browser);
		int i = 1;
		try {
			
			//Load Test Data
			String emailReportMedia = "qatest@gmail.com";
			String reasonReportMedia = "This is not relevant to me";
			String caption = "looks nice and comfortable";
			String displayName = "FbbAspire";
			String imagePath = redirectData.get("imagePathToUpload");
		
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			Curalate curalate = homePage.scrollToCuralate();
			
			Log.softAssertThat(curalate.elementLayer.verifyVerticalAllignmentOfElements(driver, "curalatePanel1", "footerSection", curalate), 
					"The curalate content slot should display above the footer section.", 
					"The curalate content slot is displayed above the footer section.", 
					"The curalate content slot is not displayed above the footer section.", driver);
			
			if(curalate.elementLayer.verifyPageElements(Arrays.asList("txtSEO"), curalate)){
				Log.softAssertThat(curalate.elementLayer.verifyVerticalAllignmentOfElements(driver, "curalatePanel1", "txtSEO", curalate), 
						"The curalate content slot should display above the SEO footer text content slot", 
						"The curalate content slot is displayed above the SEO footer text content slot", 
						"The curalate content slot is not displayed above the SEO footer text content slot", driver);
			} else {
				Log.reference("SEO text is not displayed in footer section");
			}
			

			Log.softAssertThat(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("slotCustomerPicture"), curalate), 
					"Customer picture slot should display in the curalate content slot.", 
					"Customer picture slot is displayed in the curalate content slot.", 
					"Customer picture slot is not displayed in the curalate content slot.", driver);
			
			curalate.mouseHoverOnPictureByIndex(2);
			Log.message(i++ + ". Mouse hovered on costomer picture", driver);
			
			curalate.clickOnPictureByIndex(2);
			Log.message(i++ + ". Clicked on customer picture", driver);
			
			Log.softAssertThat(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("popupcustomerEnlargeModal"), curalate), 
					"The system should open in a pop-up modal displaying the enlarged image", 
					"The system is opened in a pop-up modal displaying the enlarged image", 
					"The system is not opened in a pop-up modal displaying the enlarged image", driver);
			
			if(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("popupcustomerEnlargeModal"), curalate)) {
				Log.softAssertThat(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("slotCustomerPicture"), curalate), 
						"Customer picture slot should display in the curalate content slot.", 
						"Customer picture slot is displayed in the curalate content slot.", 
						"Customer picture slot is not displayed in the curalate content slot.", driver);
				
				Log.softAssertThat(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("nextArrow"), curalate)
								|| curalate.elementLayer.verifyElementDisplayed(Arrays.asList("prevArrow"), curalate), 
						"Arrows should display to the left and right of the modal", 
						"Arrows are displayed to the left and right of the modal", 
						"Arrows are not displayed to the left and right of the modal", driver);
				
				String customerID = curalate.getCustomerNameID();

				curalate.clickOnNextArrow();
				Log.message(i++ + ". Clicked on Next Arrow", driver);
				
				String customerID1 = curalate.getCustomerNameID();
				
				Log.softAssertThat(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("customerMainImage"), curalate)
						&& !(customerID.equalsIgnoreCase(customerID1)), 
						"System should display the next image", 
						"System is displayed the next image", 
						"System is not displayed the next image", driver);
				
				Log.softAssertThat(curalate.elementLayer.verifyVerticalAllignmentOfElements(driver, "closeCustomerEnlargeModal", "customerNameID", curalate)
						&& curalate.elementLayer.verifyHorizontalAllignmentOfElements(driver, "closeCustomerEnlargeModal", "customerMainImage", curalate), 
						" 'X' to close the modal should be in the upper right corner,", 
						" 'X' to close the modal is in the upper right corner,", 
						" 'X' to close the modal is not in the upper right corner,", driver);
				
				curalate.closeCustomerEnlargeModal();
				Log.message(i++ + ". Clicked on Close icon in Customer Enlarge modal", driver);
				
				Log.softAssertThat(!(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("popupcustomerEnlargeModal"), curalate)), 
						"Close link system should close the close the pop up modal.", 
						"Close link system is closed the close the pop up modal.", 
						"Close link system is not closed the close the pop up modal.", driver);
				
				curalate.clickOnPictureByIndex(2);
				Log.message(i++ + ". Clicked on customer picture", driver);

				boolean foundNonInstagramUpload = curalate.navigateToNonInstagramUpload();
				Log.message(i++ + ". Navigated to first non Instagram picture", driver);
				
				if(foundNonInstagramUpload) {
					Log.softAssertThat(!curalate.elementLayer.verifyElementDisplayed(Arrays.asList("heartNoOfLikes"), curalate), 
							"Number of likes (displayed as a heart image) should display only if the user submits their image through Instagram", 
							"Number of likes (displayed as a heart image) is displayed only if the user submits their image through Instagram", 
							"Number of likes (displayed as a heart image) is not displayed only if the user submits their image through Instagram", driver);
				}
				
				boolean foundInstagramUpload = curalate.navigateToInstagramUpload();
				Log.message(i++ + ". Navigated to Instagram picture.", driver);
				
				if(foundInstagramUpload) {
					if(curalate.clickOnHeart()) {
						Log.message(i++ + ". If the user profile is not private: On click on heart, System redirected the user to the social media platform.", driver);
					}else {
						Log.message(i++ + ". If the user profile is private: On click on heart, Heart should not work as a link & System not redirected the user to the social media platform.", driver);
					}
				}
				
				Log.softAssertThat(curalate.elementLayer.verifyHorizontalAllignmentOfElements(driver, "storefrontImage", "customerMainImage", curalate), 
						"Storefront item's main image/title should display to the right of the user's posted image ", 
						"Storefront item's main image/title is displayed to the right of the user's posted image ", 
						"Storefront item's main image/title is not displayed to the right of the user's posted image ", driver);
				
				Log.softAssertThat(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("divSocialIcon"), curalate), 
						"System should display social media icons - Facebook, Instagram, Twitter & pinterest", 
						"System is displayed social media icons - Facebook, Instagram, Twitter & pinterest", 
						"System is not displayed social media icons - Facebook, Instagram, Twitter & pinterest", driver);
				
				curalate.clickOnFacebook();
				Log.message(i++ + ". Clicked on Facebook link", driver);
				
				curalate.clickOnTwitter();
				Log.message(i++ + ". Clicked on Twitter link", driver);
				
				curalate.clickOnPinterest();
				Log.message(i++ + ". Clicked on Pinterest link", driver);
				
				curalate.clickReportMediaLink();
				Log.message(i++ + ". Clicked on Report media link", driver);
				
				Log.softAssertThat(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("divReportMediaWindow"), curalate), 
						"When clicked on the REPORT MEDIA link, REPORT MEDIA window should display.", 
						"When clicked on the REPORT MEDIA link, REPORT MEDIA window is displyed.", 
						"When clicked on the REPORT MEDIA link, REPORT MEDIA window is not displayed.", driver);
				
				curalate.typeInEmailReportMedia(emailReportMedia);
				Log.message(i++ + ". Typed in Email in Report media window", driver);
				
				curalate.typeInReasonReportMedia(reasonReportMedia);
				Log.message(i++ + ". Typed in Reason in Report media window", driver);
				
				curalate.clickOnReportButtonInReportMediaWindow();
				Log.message(i++ + ". Clicked on Report button in Report media window", driver);
				
				Log.softAssertThat(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("divThankYouReportMediaWindow"), curalate), 
						"Click on report button & success message should display", 
						"Click on report button & success message is displayed", 
						"Click on report button & success message is not displayed", driver);
				
				curalate.closeThankYouModal();
				Log.message(i++ + ". Clicked on close Thank you modal", driver);
				
				curalate.closeCustomerEnlargeModal();
				Log.message(i++ + ". Clicked on close Customer Enlarge modal", driver);
				
			} else {
				Log.reference("Customer picture slot curently not enabled. Reletaed verifications skipped.");
			}
			
			Log.softAssertThat(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("buttonViewGallery"), curalate), 
					"Enter now(View Gallery) button should display in the Curalate sections.", 
					"Enter now(View Gallery) button is displayed in the Curalate sections.", 
					"Enter now(View Gallery) button is not displayed in the Curalate sections.", driver);
			
			curalate.clickOnViewGallery();
			Log.message(i++ + ". Clicked on Enter Now button(View Gallery)", driver);
			
			Log.softAssertThat(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("divCuralateLandingPage"), curalate), 
					"System should redirects the user to the Curalate landing page.", 
					"System is redirected the user to the Curalate landing page.", 
					"System is not redirected the user to the Curalate landing page.", driver);
			
			Log.softAssertThat(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("btnSubmitYourPhoto", "termsAndCondition", "slotCustomerImage", "btnLoadMore"), curalate), 
					"The curalate landing page should have \"Submit your photo\" button, \"TERMS & CONDITIONS\" ,customer image slot & load more button.", 
					"The curalate landing page have \"Submit your photo\" button, \"TERMS & CONDITIONS\" ,customer image slot & load more button.", 
					"The curalate landing page not have \"Submit your photo\" button, \"TERMS & CONDITIONS\" ,customer image slot & load more button.", driver);
			
			curalate.clickOnSubmitYourPhoto();
			Log.message(i++ + ". Clicked on Submit Your Photo button", driver);
			
			Log.softAssertThat(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("popupSelectYourContent"), curalate), 
					"System should open the pop-up modal, where the user will able to upload their pictures.", 
					"System is opened the pop-up modal, where the user will able to upload their pictures.", 
					"System is not opened the pop-up modal, where the user will able to upload their pictures.", driver);
			
			curalate.uploadPhoto(imagePath);
			Log.message(i++ + ". Uploaded a Photo", driver);
			
			curalate.typeInAddCaption(caption);
			Log.message(i++ + ". Added Captions", driver);
			
			curalate.clickOnNextInSelectYourContent();
			Log.message(i++ + ". Clicked on Next button", driver);
			
			curalate.typeInEmailSelectYourContent(emailReportMedia);
			Log.message(i++ + ". Typed mail in Select your content modal", driver);
			
			curalate.typeInNameSelectYourContent(displayName);
			Log.message(i++ + ". Typed Display Name in Select your content modal", driver);
			
			curalate.SelectCheckboxTermsAndCondition("yes");
			Log.message(i++ + ". Selected Terms and condition checkbox", driver);
			
			curalate.clickOnSubmitButton();
			Log.message(i++ + ". Clicked on Submit button", driver);
			
			Log.softAssertThat(curalate.elementLayer.verifyElementDisplayed(Arrays.asList("divSuccessModal"), curalate), 
					"System should display Success modal.", 
					"System is displayed Success modal.", 
					"System is not displayed Success modal.", driver);
			
			curalate.closeSuccessModal();
			Log.message(i++ + ". Closed success modal", driver);
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			Log.endTestCase(driver);
		} // finally
	
	}// TC_FBB_HEADER_C22548

}// search
