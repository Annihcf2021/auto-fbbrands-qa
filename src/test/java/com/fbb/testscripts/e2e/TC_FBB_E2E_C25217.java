package com.fbb.testscripts.e2e;

import java.util.Arrays;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.fbb.pages.CheckoutPage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C25217 extends BaseTest {
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C25217(String browser) throws Exception{
		Log.testCaseInfo(); 
		
		final WebDriver driver = WebDriverFactory.get(browser);
				
		String guestEmail = AccountUtils.generateEmail(driver);
		String alwaysApproveAddress = "plcc_always_approve_address";
		String cardType = "cards_2";
		
		int i=1;
		try{
			
			//Step-1 & 2
			Object[] obj=GlobalNavigation.addGCProduct_Checkout(driver, false, true, i, guestEmail);
			
			CheckoutPage checkout= (CheckoutPage) obj[0];
			i=(int) obj[1];
				
			Log.softAssertThat(checkout.elementLayer.verifyElementDisplayed(Arrays.asList("divPaymentDetailsSection"), checkout),
					"user should be in the billing information section.",
					"user is in the billing information section.",
					"user is not in the billing information section.", driver);
			
			//Step-3
			checkout.fillingBillingDetailsAsGuest(alwaysApproveAddress);
			Log.message(i++ +". Entered billing address", driver);
			
			checkout.continueToPayment();
			Log.message(i++ +". Clicked continue button from billing address", driver);
						
			if(checkout.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkout)) {
				Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkout),
						"The preapproval modal should display. ",
						"The preapproval modal is display. ",
						"The preapproval modal is not displayed. ", driver);
					
				//Step-4 
				checkout.continueToPLCCStep2();
				Log.message(i++ +". clicked Get, it today button", driver);
					
				Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApprovalStep2"), checkout),
						"When user selects Get It Today, user will be taken to the Step 2 overlay. ",
						"When user selects Get It Today, user is taken to the Step 2 overlay. ",
						"When user selects Get It Today, user is not taken to the Step 2 overlay. ", driver);
				
				//Step-5 
				checkout.typeTextInSSN("1234");
				Log.message(i++ +". Entered SSN number");
					
				checkout.selectDateMonthYearInPLCC2("01","05","1947");
				Log.message(i++ +". Selected date of birth");
				
				checkout.typeTextInMobileInPLCC("3334445555");
				Log.message(i++ +". Updated mobile number", driver);
				
				checkout.checkConsentInPLCC("YES");
				Log.message(i++ +". Selected consonent checbox", driver);				
					
				checkout.clickOnAcceptInPLCC();
				Log.message(i++ +". Clicked Yes, I Accept button", driver);
					
				if(checkout.elementLayer.verifyElementDisplayed(Arrays.asList("underReviewModal"), checkout)) {
					Log.softAssertThat(checkout.elementLayer.verifyElementDisplayed(Arrays.asList("underReviewModal"), checkout),
							"The Application Under Review modal will pop up.",
							"The Application Under Review modal is pop up.",
							"The Application Under Review modal is not pop up.", driver);
				} else {
					Log.reference("under review modal is not displayed with Always approve data", driver);
				}
					
				checkout.dismissCongratulationModal();
				Log.message(i++ +". Clicking the Continue to Checkout button", driver);
				
				Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("sectionPaymentCard"), checkout), 
						"user will be taken to the payment method section of checkout.",
						"user will be taken to the payment method section of checkout.",
						"user will be taken to the payment method section of checkout.", driver);
				
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
				
				if(checkout.elementLayer.verifyElementDisplayed(Arrays.asList("btnAddNewCreditCard"), checkout)) {
					checkout.clickAddNewCredit();
					Log.message(i++ +". Clicked add new credit card link", driver);
				}
					
				//Step-7 
				checkout.fillingCardDetails1(cardType, false, false);
				Log.message(i++ +". Entered credit card details", driver);
				
				checkout.clickOnPaymentDetailsContinueBtn();
				
				OrderConfirmationPage order = checkout.clickOnPlaceOrderButton();
				Log.softAssertThat(order.elementLayer.verifyPageElements(Arrays.asList("readyElement"), order),
						"user should taken to the order confirmation page.",
						"user is taken to the order confirmation page.",
						"user is not taken to the order confirmation page.",driver);
			} else {
				Log.fail("Checkout step-1 overlay is not displayed hence can't proceed further", driver);
			}
			
			Log.testCaseResult();

		}// End of try
		catch(Exception e){
			Log.exception(e, driver);
		}//End of catch
		finally{
			Log.endTestCase(driver);
		}//End of finally		
	}
}
