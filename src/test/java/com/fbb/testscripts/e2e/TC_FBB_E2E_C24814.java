package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.MiniCartPage;
import com.fbb.pages.OfferPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.customerservice.ContactUsPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.customerservice.TrackOrderPage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.HamburgerMenu;
import com.fbb.pages.headers.Headers;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24814 extends BaseTest {

	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups={ "header", "tablet" }, dataProviderClass= DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24814(String browser) throws Exception{
		Log.testCaseInfo();

		final WebDriver driver= WebDriverFactory.get(browser);
		
		String validUsername = AccountUtils.generateEmail(driver);
		String validPassword = accountData.get("password_global");
		String errColorCode = TestData.get("passwordERR_Flyout");
		String inValidIP = "444.222.111.333";
		String emailError = demandData.get("EmailFlyoutError");
		String inValidUsername = "test@yop.com";
		String inValidPassword = "asdf123asd";
		String productKey= TestData.get("prd_variation");
		
		GlobalNavigation.registerNewUser(driver, 0, 0, validUsername+"|"+validPassword);
		
		int i=1;
		try{
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Navigates to  brand home page. ", driver);
			
			Headers headers = homePage.headers;
			Footers footers = homePage.footers;
			
			//step-1 Check the contents in the Header section
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("utilityBarDesktop", "lnkOffer", "btnHeaderSearchIcon", "quickOrederIcon", "lnkCardPLCC", "lnkAccountButton", "lnkMiniCart" ), headers), 
					"'Utility Bar','Deals','Search icon','Quick order icon', 'Card icon', 'Account icon' and 'My Bag icon' contents should be displayed in the Header section", 
					"'Utility Bar','Deals','Search icon','Quick order icon', 'Card icon', 'Account icon' and 'My Bag icon' contents are displayed in the Header section",
					"'Utility Bar','Deals','Search icon','Quick order icon', 'Card icon', 'Account icon' and 'My Bag icon' contents are not displayed in the Header section", driver);
			
			//step-2 Verify Utility bar in the header
			Log.softAssertThat(headers.elementLayer.verifyInsideElementAlligned("utilityBarDesktop", "fullPage", "top", headers)
							&& headers.elementLayer.verifyElementColor("utilityBarDesktop", "rgba(0, 0, 0, 1)",headers),
					"Utility bar should be displayed on top of the page with black color.",
					"Utility bar is displayed on top of the page with black color.",
					"Utility bar is not displayed on top of the page with black color.", driver);
					
			//Step-3 Verify customer service link in utility bar
			
			Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkMiniCart", "lnkAccountButton", headers)
					&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkAccountButton", "lnkCardPLCC", headers)
					&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkCardPLCC", "quickOrederIcon", headers)
					&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "quickOrederIcon", "btnHeaderSearchIcon", headers)
					&& headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnHeaderSearchIcon", "lnkOffer", headers),
				"Deals','Search icon','Quick order icon', 'Card icon', 'Account icon' and 'My Bag icon' contents should be displayed horizontal.",
				"Customer service link is displayed next by the sign in link in the Utility Bar.",
				"Customer service link is not displayed next by the sign in link in the Utility Bar.", driver);
			
			CustomerService customerServicePage= footers.navigateToCustomerService();
			Log.message(i++ +"On double tap,Page should be redirect to Customer service landing page.");
			
			Log.softAssertThat(customerServicePage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), customerServicePage),
					"The Page should be redirected to Customer service landing page.", 
					"The Page is redirected to Customer service landing page.",
					"The Page is not redirected to Customer service landing page.", driver);
			
			ContactUsPage contactUsPage = customerServicePage.clickOnMailWithUs();
			Log.message(i++ +". Tapped on the Email us link ", driver);
			
			Log.softAssertThat(contactUsPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), contactUsPage), 
					"The Contact Us page should be displayed!",
					"The Contact Us page is displayed!", 
					"The Contact Us page is not displayed.", driver);
			
			customerServicePage = footers.navigateToCustomerService();
			Log.message(i++ + ". Navigated to Customer Service page!", driver);

			if(customerServicePage.getLiveChatStatus()) {
				Log.softAssertThat(customerServicePage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "liveChatAvailableLnk", "lnkMailWithUs", customerServicePage), 
						"LIVE CHAT text and icon should be shown next to the 'Email Us' icon", 
						"LIVE CHAT text and icon shown next to the 'Email Us' icon", 
						"LIVE CHAT text and icon not shown next to the 'Email Us' icon", driver);

				customerServicePage.clickOnLiveChatLink();
				Log.message(i++ + ". Clicked on Live Chat Icon!", driver);

				Log.softAssertThat(customerServicePage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlLiveChat"), customerServicePage), 
						"Chat modal window should opens as a pop up modal", 
						"Chat modal window should opened as pop up modal!",
						"Chat modal window should did not open as expected.", driver);
				
				customerServicePage.closeLiveChat();
			} else {
				//Step-10 Verify Live chat unavailable state
				Log.softAssertThat(customerServicePage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("liveChatAvailableLnk"), customerServicePage),
						"Live Chat link should not be display in the Customer service section", 
						"Live Chat link is not displayed in the Customer service section",
						"Live Chat link is displayed in the Customer service section", driver);	
				
				Log.softAssertThat(customerServicePage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkMailWithUs"), customerServicePage),
						"Email us link should be displayed in the middle of the flyout.",
						"Email us link is displayed in the middle of the flyout.",
						"Email us link is not displayed in the middle of the flyout.", driver);
				
				Log.reference("--->>> Live chat is not available at this time. Skipping related validations.");
			}
			
			headers.navigateToHome();
			Log.message(i++ + ". Navigated to home page.", driver);
			
			customerServicePage = footers.navigateToCustomerService();
			Log.message(i++ + ". Navigated to Customer Service page!", driver);
			
			Log.softAssertThat(customerServicePage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkCallUs"), customerServicePage),
					"call us section with a text as 'CALL US'and a telephone number 'x-xxx-xxx-xxxx' format should be displayed ", 
					"call us section with a text as 'CALL US'and a telephone number 'x-xxx-xxx-xxxx' format is displayed ",
					"call us section with a text as 'CALL US'and a telephone number 'x-xxx-xxx-xxxx' format is not displayed", driver);
			
			TrackOrderPage ordersPage = footers.navigateToTrackMyOrder();
			Log.message(i++ + ". Clicked On Track My Order Link!", driver);
			
			Log.softAssertThat(ordersPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), ordersPage),
					"The page should be redirected to the login page.",
					"The page is redirected to the login page.",
					"The page is not redirected to the login page.", driver);
			
			ordersPage = footers.navigateToReturnItems();
			Log.message(i++ + ". User navigated to Return Items!", driver);
			
			Log.softAssertThat(ordersPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), ordersPage),
					"The page should be redirected to the login page.",
					"The page is redirected to the login page.",
					"The page is not redirected to the login page.", driver);
			
			footers.navigateToYourAccount();
			Log.message(i++ + ". Clicked On Update information Link!", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyPageElements(Arrays.asList("readyElementAccountNotLoggedIn"), headers),
					"The page should be redirected to the login page.",
					"The page is redirected to the login page.",
					"The page is not redirected to the login page.", driver);
			
			headers.navigateToHome();
			
			//Step-4 Verify Sign in link in Utility bar
			
			Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkMiniCart", "lnkAccountButton", headers),
					"Sign In should display next to the mini cart on the left-hand side.", 
					"Sign In is displayed next to the mini cart on the left-hand side.",
					"Sign In is not displayed next to the mini cart on the left-hand side.", driver);
			
			SignIn signIn= headers.navigateToSignInPage();
			Log.message(i++ + ". Clicked On Sign in Link!", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyPageElements(Arrays.asList("readyElement"), signIn),
					"The page should be redirected to the login page.",
					"The page is redirected to the login page.",
					"The page is not redirected to the login page.", driver);
			
			headers.navigateToHome();
			Log.message(i++ +". Navigated to Home Page", driver);
			
			headers.mouseOverAccountMenu();
			
			Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("flytSignInUnregisteredDesktop"),homePage.headers), 
					"On tap, Fly out should be displayed on the homepage.",
					"On tap, Fly out is displayed on the homepage.",
					"On tap, Fly out is not displayed on the homepage.", driver);
					
			Log.softAssertThat(homePage.headers.elementLayer.verifyElementColor("lnkAccountButton", "rgba(0, 0, 0, 1)", homePage.headers)
							&& homePage.headers.elementLayer.verifyCssPropertyForElement("lnkAccountButton", "background:", "", homePage.headers),
					"The flyout open state of the Sign In link should be displayed the white utility bar and the black Sign In text.", 
					"The flyout open state of the Sign In link is displayed the white utility bar and the black Sign In text.", 
					"The flyout open state of the Sign In link is not displayed the white utility bar and the black Sign In text.", driver);	
						
			headers.clickOnSignInDesktop();
			Log.message(i++ +". Clicked sign in link without any details", driver);
			
			Log.softAssertThat(homePage.headers.elementLayer.verifyElementColor("lblUserNameErrorDesktop", errColorCode, homePage.headers)
							&& homePage.headers.elementLayer.verifyElementColor("lblPasswordInSignInFlyOut", errColorCode, homePage.headers),
					"Email address field and password fields are mandatory & Fields placeholder text should be turned to red color when login user",
					"Email address field and password fields are mandatory & Fields placeholder text is turned to red color when login user",
					"Email address field and password fields are mandatory & Fields placeholder text is not turned to red color when login user", driver);
			
			headers.navigateToHome();
			headers.mouseOverAccountMenu();
			homePage.headers.typeUserNameInFlyout(inValidIP);
			Log.message(i++ + ". Entered Invalid IP Format into Email Field", driver);
			String formatErro = homePage.headers.elementLayer.getElementText("invalidEmailErrorTxt", homePage.headers);
			
			Log.softAssertThat(formatErro.equalsIgnoreCase(emailError),
					"The error message should be displayed as 'Please enter a valid email' above the entered text in the email field", 
					"The error message is displayed as 'Please enter a valid email' above the entered text in the email field",
					"The error message is not displayed as 'Please enter a valid email' above the entered text in the email field", driver);
			
			Log.softAssertThat(headers.verifyPresenceOfShowInPassword(),
					"The SHOW Link should be present at the corner of the password field",
					"The SHOW Link present at the corner of the password field ",
					"The SHOW Link did not present at the corner of the password field ", driver);
			
			headers.typePasswordInFlyout("abcde");
			Log.softAssertThat(headers.verifyPasswordFieldTextMode().toLowerCase().equals("password"),
					"Initially entered password should be displayed as masked",
					"Initially entered password is displayed as masked",
					"Initially entered password is not displayed as masked", driver);
			
			String currentState = headers.clickOnShowHideInPassword();
			Log.message(i++ + ". Show Link Clicked!");

			Log.softAssertThat(currentState.toLowerCase().equals("hide") 
					&& headers.verifyPasswordFieldTextMode().toLowerCase().equals("text"),
					"Values should be visible and the option should be changed to HIDE",
					"Values are visible and the option should be changed to HIDE",
					"Values are not visible and the option should be changed to HIDE", driver);
			
			headers.typeUserNameInFlyout(inValidUsername);
			Log.message(i++ +". typed invalid username: "+ inValidUsername, driver);
			
			headers.typePasswordInFlyout(inValidPassword);
			Log.message(i++ +". typed invalid Password: "+ inValidPassword, driver);
			
			headers.clickOnSignInDesktop();
			Log.message(i++ +". Clicked sign in link without any details", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("readyElement","UnRegisteredEmailError"), signIn),
					"Page should be navigated to sign in page and error message should be displayed above the fields.", 
					"Page is navigated to sign in page and error message should be displayed above the fields.", 
					"Page is not navigated to sign in page and error message should be displayed above the fields.", driver);
			
			headers.navigateToHome();
			headers.mouseOverAccountMenu();
			
			Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElementsWithoutScroll(driver, "lnkForgetPasswordDesktop", "chkRememberMeDesktop", headers)
							&& !headers.verifyRememberMeCheckboxSelectedOrNot(),
					"Remember Me checkbox should display on the left side of the 'sign in' button with the unchecked state.",
					"Remember Me checkbox is displayed on the left side of the 'sign in' button with the unchecked state.",
					"Remember Me checkbox is not displayed on the left side of the 'sign in' button with the unchecked state.", driver);
			
			headers.typeUserNameInFlyout(validUsername);
			headers.typePasswordInFlyout(validPassword);
			headers.checkOnRememberMeDesktop(true);
			headers.clickOnSignInDesktop();
			MyAccountPage myAccount = new MyAccountPage(driver).get();
			Log.message(i++ +". Select the 'Remember Me' checkbox & logged in to an account", driver);
			
			//Step-5 Check the Sign in Authenticated state
			
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("utilityBarDesktop", "lblhlloMyAccDesktop", "btnHeaderSearchIcon", "quickOrederIcon", "lnkCardPLCC", "iconUserDesktop", "lnkOffer", "lnkMiniCart"), headers), 
					"'Utility Bar','Utility Bar - Customer Service','Account - Authenticated State',''Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon'  contents should be displayed in the Header section", 
					"'Utility Bar','Utility Bar - Customer Service','Account - Authenticated State',''Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon'  contents are displayed in the Header section",
					"'Utility Bar','Utility Bar - Customer Service','Account - Authenticated State',''Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon'  contents are not displayed in the Header section", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyPageElements(Arrays.asList("lblhlloMyAccDesktop","iconUserDesktop" ), headers)
							&& headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "iconUserDesktop", "lblMyAccDesktop",headers),
					 "Sign in link will convert to user Icon, Hello (User 1st name) & below that link system should display My account link",
					 "Sign in link will convert to user Icon, Hello (User 1st name) & below that link system is displayed My account link",
					 "Sign in link will convert to user Icon, Hello (User 1st name) & below that link system is not displayed My account link", driver);
		
			Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkMiniCart", "lnkAccountButton",headers),
					"The My account section should be available at left side of the My bag link.",
					"The My account section is available at left side of the My bag link.",
					"The My account section is not available at left side of the My bag link.", driver);
			
			headers.navigateToMyAccount();
			Log.message(i++ +". Double tapped on My Account link from the header", driver);
			
			Log.softAssertThat(myAccount.elementLayer.verifyPageElements(Arrays.asList("readyElement"), signIn),
					"The page should be redirected to the My Account page.",
					"The page is redirected to the My Account page.",
					"The page is not redirected to the My Account page.", driver);
			
			headers.navigateToHome();
			
			homePage.headers.mouseOverAccountMenu();
			Log.message(i++ +". Tapped on My account", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("flytSignInRegisteredDesktop"), headers),
					"Flyout should be displayed.",
					"Flyout is displayed.", 
					"Flyout is not displayed.", driver);
			
			if(!BrandUtils.isBrand(Brand.el)) {
				Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkProfileMyAccFlytDesktop","lnkTrackMyOrderMyAccFlytDesktop","lnkWishListMyAccFlytDesktop","lnkRewardPointsMyAccFlytDesktop","lnkOrderHistoryMyAccFlytDesktop","lnkSavedPaymentMyAccFlytDesktop","lnkAddressbookMyAccFlyt","lnkCatalogPrefsMyAccFlyt","lnkEmailPrefsMyAccFlyt","lnkSignOutMyAccFlytDesktop"), headers),
						"Flyout should consist of following items:PROFILE,TRACK MY ORDER,ORDER,WISHLIST,REWARD POINTS,ORDER HISTOER,SAVED PAYMENTS,ADDRESS BOOK,CATALOG PREFERENCES,EMAIL PREFERENCES,SIGN OUT ",
						"Flyout is consist of following items:PROFILE,TRACK MY ORDER,ORDER,WISHLIST,ORDER HISTOER,SAVED PAYMENTS,ADDRESS BOOK,CATALOG PREFERENCES,EMAIL PREFERENCES,SIGN OUT ",
						"Flyout is not consist of following items:PROFILE,TRACK MY ORDER,ORDER,WISHLIST,ORDER HISTOER,SAVED PAYMENTS,ADDRESS BOOK,CATALOG PREFERENCES,EMAIL PREFERENCES,SIGN OUT ", driver);
			} else {
				Log.softAssertThat(headers.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkProfileMyAccFlytDesktop","lnkTrackMyOrderMyAccFlytDesktop","lnkWishListMyAccFlytDesktop","lnkOrderHistoryMyAccFlytDesktop","lnkSavedPaymentMyAccFlytDesktop","lnkAddressbookMyAccFlyt","lnkCatalogPrefsMyAccFlyt","lnkEmailPrefsMyAccFlyt","lnkSignOutMyAccFlytDesktop"), headers),
						"Flyout should consist of following items:PROFILE,TRACK MY ORDER,ORDER,WISHLIST,ORDER HISTOER,SAVED PAYMENTS,ADDRESS BOOK,CATALOG PREFERENCES,EMAIL PREFERENCES,SIGN OUT ",
						"Flyout is consist of following items:PROFILE,TRACK MY ORDER,ORDER,WISHLIST,ORDER HISTOER,SAVED PAYMENTS,ADDRESS BOOK,CATALOG PREFERENCES,EMAIL PREFERENCES,SIGN OUT ",
						"Flyout is not consist of following items:PROFILE,TRACK MY ORDER,ORDER,WISHLIST,ORDER HISTOER,SAVED PAYMENTS,ADDRESS BOOK,CATALOG PREFERENCES,EMAIL PREFERENCES,SIGN OUT ", driver);
			}
			
			ProfilePage profilePage=headers.clickOnProfileLink();
			Log.message(i++ +". Clicked profile link", driver);
			
			Log.softAssertThat(profilePage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), profilePage),
					"Page should be navigate to Profile page", 
					"Page is navigateed to Profile page",
					"Page is not navigateed to Profile page", driver);

			headers.mouseOverAccountMenu();
			Log.message(i++ +". Mouse hovered on My account", driver);
			
			OrderHistoryPage orderPage= headers.clickOnOrderHistoryLink();
			Log.message(i++ +". Clicked Order history link", driver);
			
			Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage),
					"Page should be navigate to Order History page", 
					"Page is navigateed to Order History page",
					"Page is not navigateed to Order History page", driver);

			WishListPage wishlistPage=headers.clickOnWishList();
			Log.message(i++ +". Clicked Order Wishlist link", driver);
			
			Log.softAssertThat(wishlistPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), wishlistPage),
					"Page should be navigate to Wishlist page", 
					"Page is navigateed to Wishlist page",
					"Page is not navigateed to Wishlist page", driver);

			headers.mouseOverAccountMenu();
			Log.message(i++ +". Mouse hovered on My account", driver);

			QuickOrderPage qcPage=headers.navigateToQuickOrder();
			Log.message(i++ +". Clicked Order Wishlist link", driver);
			
			Log.softAssertThat(qcPage.elementLayer.verifyPageElements(Arrays.asList("containerElement"), qcPage),
					"Page should be navigate to QuickOrder page", 
					"Page is navigateed to QuickOrder page",
					"Page is not navigateed to QuickOrder page", driver);
			
			headers.navigateToHome();
			
			headers.signOut();
			Log.message(i++ +". User is logged out and navigate to sign in page.", driver);
			
			if(headers.elementLayer.verifyElementDisplayed(Arrays.asList("txtSignIn"), headers)) {
				Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("utilityBarDesktop", "lblhlloMyAccDesktop", "btnHeaderSearchIcon", "quickOrederIcon", "lnkCardPLCC", "iconUserDesktop", "lnkOffer", "lnkMiniCart"), headers), 
						"'Utility Bar','Utility Bar - Customer Service','Account - Recognized & Authenticated State', 'Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon'  contents should be displayed in the Header section", 
						"'Utility Bar','Utility Bar - Customer Service','Account - Recognized & Authenticated State', 'Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon' contents are displayed in the Header section",
						"'Utility Bar','Utility Bar - Customer Service','Account - Recognized & Authenticated State', 'Deals','Search icon','Quick order icon', 'Card icon' and 'My Bag icon' contents are not displayed in the Header section", driver);
				
				headers.mouseOverAccountMenu();
				Log.message(i++ +". Tapped on My account", driver);
				
				Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("fldUserNameDesktop"), headers),
						"in the email address field, the system should display the email address",
						"in the email address field, the system should display the email address",
						"in the email address field, the system should display the email address", driver);		
			} else {
				Log.failsoft("Recognized unauthenticated state is not available");
			}
			
			headers.navigateToHome();
			Log.message(i++ + ". Navigated to home.", driver);
			
			//Step-7 Verify My bag link in Utility bar.
			
			Log.softAssertThat(homePage.headers.elementLayer.verifyInsideElementAlligned("lnkMiniCart", "utilityBarDesktop", "right", homePage.headers)
							&& homePage.headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "lnkMiniCart", "lnkAccountButton", homePage.headers),
					"My bag/Mini cart should display all the way to the right-hand side in utility bar.",
					"My bag/Mini cart is displayed all the way to the right-hand side in utility bar.",
					"My bag/Mini cart is not displayed all the way to the right-hand side in utility bar.", driver);
			
			ShoppingBagPage cart = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ +". Navigated to shopping bag page.", driver);
			
			Log.softAssertThat(cart.verifyEmptyCart(),
					"Without adding any product, the page should be navigated to empty cart page.",
					"Without adding any product, the page is navigated to empty cart page.",
					"Without adding any product, the page is not navigated to empty cart page.", driver);
			
			PdpPage pdpPage = headers.navigateToPDP(productKey);
			Log.message(i++ +". Navigated to PDP", driver);
			
			String productName = pdpPage.getProductName().toLowerCase();
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". Added product to bag", driver);
			 
			cart = homePage.headers.clickOnBagLink();
			Log.message(i++ +". Navigated to shopping bag page.", driver);
			
			String myBagCount = headers.getMiniCartCount();
			int cartqty = StringUtils.getNumberInString(myBagCount);
			
			Log.softAssertThat(cart.getQtyInCart() == cartqty &&
							headers.elementLayer.verifyCssPropertyForElement("miniCartQty", "background", "bag-icon-white.svg", headers), 
					"Then the mini cart in the top navigation should displays as an icon with the numeric representation of the number of items in the shopping cart.",
					"Then the mini cart in the top navigation is displays as an icon with the numeric representation of the number of items in the shopping cart.",
					"Then the mini cart in the top navigation is not displays as an icon with the numeric representation of the number of items in the shopping cart.", driver);
			
			headers.navigateToHome();
			
			headers.mouseOverMiniCart();
			Log.message(i++ +". Mouse hovered on bag icon.", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyCssPropertyForElement("lnkMiniCart", "color", "gba(132, 189, 0, 1)", headers)
							&& headers.elementLayer.verifyCssPropertyForElement("lnkMiniCart", "background", "rgb(255, 255, 255)", headers),
					"Flyout should be displayed and my bag link should be displayed the white utility bar and the black my bag text with number icon", 
					"Flyout is displayed and my bag link should be displayed the white utility bar and the black my bag text with number icon", 
					"Flyout is not displayed and my bag link should be displayed the white utility bar and the black my bag text with number icon", driver);	
			
			MiniCartPage miniCart = new MiniCartPage(driver).get();
			String flyoutName = miniCart.getProductName(0).toLowerCase();
			
			headers.mouseOverMiniCart();
			Log.message(i++ +". Mouse hovered on bag icon.", driver);
			
			Log.softAssertThat(flyoutName.contains(productName) &&
							miniCart.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("miniCartfluoytProductName1st","divProductAttributes","lblQuantity","lblPrice","lblTotal" ),miniCart),
					"Mini cart flyout should have all carted products with product information.",
					"Mini cart flyout  have all carted products with product information.",
					"Mini cart flyout not have all carted products with product information.", driver);
			
			miniCart.clickOnProductName();
			Log.message(i++ +". Clicked product name from the flyout", driver);
			
			Log.softAssertThat(pdpPage.getProductName().toLowerCase().contains(productName),
					"The page should be navigated to PDP of the respective product",
					"The page is navigated to PDP of the respective product",
					"The page is not navigated to PDP of the respective product", driver);
			
			homePage.headers.mouseOverMiniCart();
			Log.message(i++ +". Mouse hovered on bag icon.", driver);
			
			Log.softAssertThat(miniCart.elementLayer.verifyInsideElementAlligned("divCheckOutButton", "miniCartOverlay", "bottom", miniCart),
					"Checkout button should be displayed at the end of the flyout",
					"Checkout button is displayed at the end of the flyout",
					"Checkout button is not displayed at the end of the flyout", driver);
			
			miniCart.clickOnCheckOut();
			Log.message(i++ +". Clicked checkout button from the flyout", driver);
			
			Log.softAssertThat(cart.elementLayer.verifyPageElements(Arrays.asList("miniCartContent"), cart),
					"On click, page should be redirected to cart page.",
					"On click, page is redirected to cart page.",
					"On click, page is not redirected to cart page.", driver);
			
			double cartDiscount= cart.getOrderDiscount();
			
			headers.mouseOverMiniCart();
			Log.message(i++ +". Mouse hovered on bag icon.", driver);
			
			Log.softAssertThat(miniCart.elementLayer.verifyElementDisplayed(Arrays.asList("divSubTotal"),miniCart), 
					"Order subtotal should be displayed on the mini cart flyout",
					"Order subtotal is displayed on the mini cart flyout",
					"Order subtotal is not displayed on the mini cart flyout", driver);
			
			Log.softAssertThat(miniCart.getSubTotal() == miniCart.calculateSubTotal(cartDiscount),
					"Subtotal should be calculated as the total amount minus discounts.", 
					"Subtotal is calculated as the total amount minus discounts.",
					"Subtotal is not calculated as the total amount minus discounts.", driver);
			
			if(miniCart.elementLayer.verifyPageElements(Arrays.asList("scrollBar"), miniCart)) {
				Log.softAssertThat(miniCart.elementLayer.verifyPageElements(Arrays.asList("scrollBar"), miniCart),
						"Scroll Bar should be displayed to the right extreme side of the Mini Cart when the number of products are more than the fixed height of the fly-out in the mini cart.",
						"Scroll Bar displayed to the right extreme side of the Mini Cart when the number of products are more than the fixed height of the fly-out in the mini cart.",
						"Scroll Bar not displayed to the right extreme side of the Mini Cart when the number of products are more than the fixed height of the fly-out in the mini cart.", driver);
			} else {
				Log.reference("Only few products are there so scroll bar won't display");
			}
			
			BrowserActions.clickOnEmptySpaceHorizontal(driver, "miniCartOverlay", miniCart);
			Log.softAssertThat(miniCart.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("miniCartOverlay"), miniCart),
					"Mini cart flyout should disappear when the user taps outside of the flyout.",
					"Mini cart flyout disappeares when the user taps outside of the flyout.",
					"Mini cart flyout did not disappear when the user taps outside of the flyout.", driver);
		
			//Step-8 Verify the brand logo
			
			Log.softAssertThat(headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSearch", "brandLogoMobTab",headers )
							&& headers.elementLayer.verifyVerticalAllignmentOfElements(driver, "utilityBarDesktop", "brandLogoMobTab", headers), 
					"Brand logo should display to the left-hand side below the Utility Bar.",
					"Brand logo is displayed to the left-hand side below the Utility Bar.",
					"Brand logo is not display to the left-hand side below the Utility Bar.", driver);
			
			headers.navigateToHome();
			
			Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("readyElement"),homePage), 
					"Page should navigate to brand home page.", 
					"Page is navigateed to brand home page.",
					"Page is not navigateed to brand home page.", driver);
			
			//Step-9 Verify Offers callout link
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("lnkOffer"), headers),
					"Offers callout link should be displayed in the utility bar",
					"Offers callout link is displayed in the utility bar",
					"Offers callout link is not displayed in the utility bar", driver);
			
			OfferPage offersPage = homePage.headers.navigateToOfferPage();
			Log.message(i++ +". Clicked offers link from the headers", driver);
			
			Log.softAssertThat(offersPage.verifyOffersDisplayed(),
					"The contents of offers should be displayed in Offers page", 
					"The contents of offers are displayed in Offers page",
					"The contents of offers are not displayed in Offers page", driver);
			
			Log.softAssertThat(offersPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), offersPage), 
					"Page should be navigate to Offers & coupons landing page", 
					"Page is navigated to Offers & coupons landing page",
					"Page is not navigated to Offers & coupons landing page", driver);
			
			headers.navigateToHome();
			
			//Step-10 Verify catalog quick order link.(Skip this step for Ellos).
			Log.softAssertThat(homePage.headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "quickOrederIcon", "lnkOffer", homePage.headers),
					"CATALOG QUICK ORDER link should display next to the offer link.",
					"CATALOG QUICK ORDER link is displayed next to the offer link.",
					"CATALOG QUICK ORDER link is not displayed next to the offer link.", driver);
			
			qcPage = homePage.headers.navigateToQuickOrder();
			Log.message(i++ +". Tapped on the quick order link");
				
			Log.softAssertThat(qcPage.elementLayer.verifyPageElements(Arrays.asList("containerElement"), qcPage), 
					"Page should be navigate to catalog quick order landing page.", 
					"Page is navigated to catalog quick order landing page.",
					"Page is not navigated to catalog quick order landing page.", driver);
			
			headers.navigateToHome();
			
			//Step-11 Verify hamburger menu.
			
			Log.softAssertThat(homePage.headers.elementLayer.verifyHorizontalAllignmentOfElements(driver, "brandLogoMobTab", "lnkHamburger", homePage.headers),
					"The Hamburger menu icon should be available left side of brand logo.",
					"The Hamburger menu icon is available left side left side of brand logo.",
					"The Hamburger menu icon is be available left side left side of brand logo.", driver);
			
			HamburgerMenu hMenu = (HamburgerMenu) headers.openCloseHamburgerMenu("open");
			Log.message(i++ +". Opened hamburger menu", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyPageElements(Arrays.asList("btnHamburgerClose","navigationCategory1","utilityBar","fldEmailSignUp","customerServiceSection"), hMenu),
						"Hamburger menu expanded state consist following items: 1.-Menu Close Button. 2.Global Navigation. 3.Utility Bar. 4.Email Sign Up. 5Customer Service Links.", 
						"Hamburger menu expanded state consist following items: 1.-Menu Close Button. 2.Global Navigation. 3.Utility Bar. 4.Email Sign Up. 5Customer Service Links.",
						"Hamburger menu expanded state consist following items: 1.-Menu Close Button. 2.Global Navigation. 3.Utility Bar. 4.Email Sign Up. 5Customer Service Links.",driver);
			
			headers.openCloseHamburgerMenu("close");
			Log.message(i++ + ". Hamburger Menu closed.", driver);
			
			Log.softAssertThat(hMenu.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("divHamburgerOverlay"), hMenu), 
					"Closing hamburger menu should reveal the original page.", 
					"Closing hamburger menu reveals the original page.", 
					"Closing hamburger menu does not reveal the original page.", driver);
			
			headers.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Hamburger Menu opened.", driver);
			
			Log.softAssertThat(hMenu.verifyArrowWhenChildCategoriesPresent(),
					"Global navigation should consist of following items:1. Category names and right arrows, 2.Category without child categories.3.Category arrow should not show.",
					"Global navigation is consist of following items:1. Category names and right arrows, 2.Category without child categories.3.Category arrow is not show.",
					"Global navigation is not consist of following items:1. Category names and right arrows, 2.Category without child categories.3.Category arrow is show.", driver);
			
			headers.openCloseHamburgerMenu("close");
			headers.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Re-opened hamburger menu.", driver);
			
			if(hMenu.verifyLevel1CategoryWithoutLevel2Availability()) {
				Log.softAssertThat(hMenu.verifyLevel1CategoryWithoutLevel2Availability(), 
					"Category without child categories.•Category arrow should not show",
					"Category without child categories.•Category arrow is not shown",
					"Category without child categories.•Category arrow is shown", driver);
			
				Log.softAssertThat(hMenu.verifyArrowWhenChildCategoriesNotPresent(),
						"Page should be navigate to category landing page.",
						"Page is navigated to category landing page.",
						"Page is not navigateed to category landing page.", driver);	
			} else {
				Log.reference("Category landing page is not available for this brand");
			}
			
			headers.openCloseHamburgerMenu("open");
			Log.softAssertThat(hMenu.verifyArrowWhenChildCategoriesPresent(),
					"Category arrow should be shown",
					"Category arrow is shown",
					"Category arrow is not shown", driver);
						
			Log.softAssertThat(hMenu.elementLayer.verifyVerticalAllignmentOfElements(driver, "activeCat2Header", "activeCategoryNavLevel2", hMenu),
					"Selected category should be displayed in the hamburger menu and the child categories should be displayed below the Selected category.",
					"Selected category is displayed in the hamburger menu and the child categories are displayed below the Selected category.",
					"Selected category is not displayed in the hamburger menu and the child categories are not displayed below the Selected category.", driver);
			
			headers.openCloseHamburgerMenu("close");
			headers.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Re-opened hamburger menu.", driver);
			
			hMenu.navigateToSubCategory(0);
			Log.message(i++ + ". Navigated to sub-category menu.", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyCssPropertyForElement("lblActiveMenuToggleBack", "background", "icon_", hMenu)
							&& hMenu.elementLayer.verifyCssPropertyForElement("lblActiveMenuToggleBack", "background", ".svg", hMenu)
							&& hMenu.elementLayer.verifyInsideElementAlligned("lblActiveMenuToggleBack", "lblActiveMenuHeading", "left", hMenu),
					"The back arrow should be displayed at the left side of the category header withe symbol like '<' .",
					"The back arrow is displayed at the left side of the category header withe symbol like '<' .",
					"The back arrow is not displayed at the left side of the category header withe symbol like '<' .", driver);
			
			hMenu.clickOnBackArrowInActiveLevel2Category();
			Log.message(i++ + ". Clicked on back button menu button on submenu.", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("navigationCategory1"), hMenu), 
					"On tapping menu back button the page should navigate to previous menu state.", 
					"On tapping menu back button the page navigates to previous menu state.", 
					"On tapping menu back button the page does not navigate to previous menu state.", driver);
			
			hMenu.verifyArrowWhenChildCategoriesPresent();
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkViewAllActive", "activeCategoryNavLevel2" ), hMenu),
					"The View all link should be displayed with the L2 navigation",
					"The View all link should be displayed with the L2 navigation",
					"The View all link should be displayed with the L2 navigation", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("lnkViewAllActive"), hMenu), 
					"The view all link should be available only when user able to tap L2 category.", 
					"The view all link is available only when user able to tap L2 category.", 
					"The view all link is not available only when user able to tap L2 category.", driver);
					
			hMenu.clickOnActiveViewAll();
			Log.message(i++ + ". Clicked on View All button on submenu.", driver);
			
			Log.softAssertThat(headers.elementLayer.verifyElementDisplayed(Arrays.asList("lnkHamburger"),headers ), 
					"On tap the user should be taken to the L2 category and the Hamburger Menu should be closed.", 
					"On tap the user is taken to the L2 category and the Hamburger Menu is closed.", 
					"On tap the user is not taken to the L2 category and the Hamburger Menu is not closed.", driver);
						
			headers.navigateToHome();
			
			homePage.headers.openCloseHamburgerMenu("open");
			Log.message(i++ + ". Opened hamburger menu.", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyVerticalAllignmentOfElements(driver, "navigationCategory1", "utilityBar", hMenu),
					"Utility bar should be displayed below the category pane.",
					"Utility bar is displayed below the category pane.",
					"Utility bar is not displayed below the category pane.", driver);
			
			
			Log.softAssertThat(hMenu.elementLayer.verifyVerticalAllignmentOfElements(driver, "utilityBar", "fldEmailSignUp", hMenu),
					"Email sign up button should be displayed below the Utility bar under hamburger menu.",
					"Email sign up button IS displayed below the Utility bar under hamburger menu.",
					"Email sign up button is not displayed below the Utility bar under hamburger menu.", driver);
			
			hMenu.typeInEmailSignUp(inValidIP);
			Log.message(i++ +". Entered invalid format email address", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayed(Arrays.asList("txtEmailSignUpError"), hMenu),
					"Error message should display when user try to sign up with incorrect  or invalid email addres", 
					"Error message is displayed when user try to sign up with incorrect  or invalid email addres",
					"Error message is not displayed when user try to sign up with incorrect  or invalid email address", driver);
			
			Log.softAssertThat(hMenu.elementLayer.verifyVerticalAllignmentOfElements(driver, "fldEmailSignUp", "customerServiceSection", hMenu),
					"Customer service links should be displayed below the email sign up section.",
					"Customer service links is displayed below the email sign up section.",
					"Customer service links is not displayed below the email sign up section.", driver);
			
			hMenu.typeInEmailSignUp(validUsername);
			hMenu.clickOnEmailSignUp();
			Log.message(i++ +". Entered valid email address", driver);

			Log.softAssertThat(hMenu.elementLayer.verifyElementDisplayed(Arrays.asList("lblEmailSignUpThankYou"), hMenu),
					"User should able to sign up with valid email address.", 
					"User is able to sign up with valid email address.",
					"User is not able to sign up with valid email address.", driver);
			
			boolean navigation = hMenu.verifyCustomerServiceLinksNaviagtion();
			boolean quickLinksNavigation1 = hMenu.verifyquicklinksNaviagtion();
			
			customerServicePage = hMenu.navigateToCustomerService();
			Log.message(i++ + ". Clicked customer service links", driver);
			
			Log.softAssertThat(navigation && quickLinksNavigation1 &&
					customerServicePage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), customerServicePage),
					"Page should be navigated to respective page.",
					"Page is navigated to respective page.",
					"Page is not navigated to respective page.", driver);
		
			Log.testCaseResult();

		}// End of Try
		catch(Exception e){
			Log.exception(e, driver);
		}//End of Catch
		finally{
			Log.endTestCase(driver);
		}//End of finally block
	}//M1_FBB_E2E_C24814

}//TC_FBB_E2E_C24814
