package com.fbb.testscripts.e2e;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24118 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24118(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{
			//Step 1 - Navigate to the website
			
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String checkoutAddressNoTax = "taxless_address";
			String billingAdd = "valid_address1";
			String payment = "card_Visa";
			String productID = TestData.get("prd_expedited-ship-eligible");
			
			{
				HashMap<String, String> accountDetails = new HashMap<>();
				accountDetails.put(AddressesPage.class.getName(), checkoutAddressNoTax + "," + billingAdd);
				accountDetails.put(WishListPage.class.getName(), productID);
				GlobalNavigation.setupTestUserAccount(driver, accountDetails, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - From the header click on Sign in link & navigates to My account Sign in page
			
			SignIn signIn = homePage.headers.navigateToSignInPage();

			//Step 3 - The system navigates the user to My account page
			
			MyAccountPage myAcc = signIn.navigateToMyAccount(AccountUtils.generateEmail(driver), accountData.get("password_global"));
			Log.message(i++ + ". Navigated to My Account page as : " +AccountUtils.generateEmail(driver), driver);
			
			//Step 4a - Navigate to My Wishlist page 
			WishListPage wishListPg = myAcc.navigateToWishListPage();
			Log.message(i++ + ". Navigated To Wishlist page", driver);
			
			Log.softAssertThat(wishListPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "wishlistHeading", "socialIcon", wishListPg), 
					"Share Icons should be displayed below the My Wishlist Heading", 
					"Share Icons is displayed below the My Wishlist Heading", 
					"Share Icons is not displayed below the My Wishlist Heading", driver);
						
			wishListPg.clickMakeWishlistPublicToggle("public");
			Log.message(i++ + ". Made wishlist as public", driver);
			
			if(Utils.isMobile()){
				Log.softAssertThat(wishListPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "chkBoxMakeThisPublic", "btnAddtoCart", wishListPg), 
						"Make public link should be displayed above the add to bag button",
						"Make public link is displayed above the add to bag button", 
						"Make public link is not displayed above the add to bag button", driver);
			}else{			
			Log.softAssertThat(wishListPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "chkBoxMakeThisPublic", "sectionEditRemoveBlock", wishListPg) &&
					wishListPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnAddtoCart", "chkBoxMakeThisPublic", wishListPg), 
					"Make This Public should be displayed between Add to Bag button and Edit & Remove Link",
					"Make This Public is displayed between Add to Bag button and Edit & Remove Link", 
					"Make This Public is not displayed between Add to Bag button and Edit & Remove Link", driver);
			}
			if(Utils.isMobile()) {
				Log.softAssertThat(wishListPg.elementLayer.verifyElementTextContains("btnMakeWishlistpublictoggleMobile", "private", wishListPg), 
						"The link should be updated to 'Make list Private' link",
						"The link is updated to 'Make list Private' link", 
						"The link is not updated to 'Make list Private' link", driver);
			} else {
				Log.softAssertThat(wishListPg.elementLayer.verifyElementTextContains("btnMakeWishlistpublictoggle", "private", wishListPg), 
						"The link should be updated to 'Make list Private' link",
						"The link is updated to 'Make list Private' link", 
						"The link is not updated to 'Make list Private' link", driver);
			}
			
			//Step 5a - Navigate to the cart page
			
			wishListPg.clickAddtoBagByIndex(0);
			Log.message(i++ + ". Product is Added to cart", driver);
			
			ShoppingBagPage shoppingBagPg = wishListPg.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			//Step 4b - Navigate to My Wishlist page 
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementTextContains("txtWishlistItem", "wishlist", shoppingBagPg), 
					"'This is wishlist item' message should display in that product", 
					"'This is wishlist item' message is displaying in that product", 
					"'This is wishlist item' message is not displaying in that product", driver);
			//Step 6 - Navigate to Checkout page - step 1 - ( Account already have a default address )
			
			CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			checkoutPg.selectShippingMethod(ShippingMethod.Express);
			Log.message(i++ + ". Select the express shippment method", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(false);
			Log.message(i++ + ". Use this as a Billing address check box disabled", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			checkoutPg.selectValueFromSavedBillingAddressesDropdownByIndex(1);
			Log.message(i++ + ". Selected saved address in billing", driver);
			
			//Step 7 - Navigate to Checkout page - step 2 - (Account should have multiple addresses saved)
			
			checkoutPg.applyGiftCardByValue(TestGiftCardValue.$20);
			Log.message(i++ + ". Gift Card is applied", driver);
			
			checkoutPg.fillingCardDetails1(payment, true, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			//Step 8 - Navigate to Checkout page - step 3
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			Log.softAssertThat(checkoutPg.comparePaymentBeforeOrderWithEnteredPaymentMethod(payment), 
					"The system should keep the payment details", 
					"Payment method is saved and displayed", 
					"Payment method is not saved correctly", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 9 - Order confirmation page
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);

			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.removeAllPaymentMethods(driver);
			GlobalNavigation.RemoveWishListProducts(driver);
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24118