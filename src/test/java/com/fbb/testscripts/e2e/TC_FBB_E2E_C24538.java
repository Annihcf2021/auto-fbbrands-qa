package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.EnlargeViewPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.SystemProperties;
import com.fbb.support.SystemProperties.Platform;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24538 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "tablet" }, priority = 10, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24538(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		Utils.setDeviceTypeExecutionProperties(SystemProperties.getRunPlatform(), browser);
		WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			//Step 1 - Launch website through Tablet
			
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String prdBadgeID[] = TestData.get("level_with_badge").split("\\|");
			String searchTerm[] = TestData.get("product_search_terms").split("\\|");
			String validAddressLong = "valid_address_long";
			
			{
				GlobalNavigation.registerNewUser(driver, 3, 0, credential);
				GlobalNavigation.emptyCartForUser(username, password, driver);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Browser launched with " + Utils.getWebSite(), driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage), 
					"The system should display the Homepage",
					"The system is displayed the Homepage",
					"The system is not displayed the Homepage", driver);
			
			//Step 2 - Click on Sign in link from the header
			
			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Clicked on SignIn link.", driver);
			
			Log.softAssertThat(signIn.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("divLoginBox"), signIn), 
					"Verify link should be redirected to Sign in page",
					"Link is redirected to Sign in page",
					"Link is not redirected to Sign in page", driver);
			
			MyAccountPage myAcc = signIn.navigateToMyAccount(username, password);
			Log.message(i++ + ". Logged in with user account having multiple addresses and none of them are default", driver);
			
			Log.softAssertThat(myAcc.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAcc), 
					"The page should be redirected to My account page",
					"The page is redirected to My account page",
					"The page is not redirected to My account page", driver);
			
			//Step 3 - Tap on search field in the header.
			SearchResultPage slp = homePage.headers.searchProductKeyword(searchTerm[0]);
						
			//Step 4 - Navigate to search result page and verify.
			
			Log.softAssertThat(slp.getNumberOfProductTilesPerRow() == 3,
					"The number of product tiles displayed per row should be 3!",
					"The number of product tiles displayed per row is 3!",
					"The number of product tiles displayed per row is not 3!", driver);
			
			Log.softAssertThat(slp.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "searchResultCountDeskTab", "inpSecondrySearch", slp), 
					" Search term product count should be shown above the search term in the secondary search box.",
					" Search term product count shown above the search term in the secondary search box.",
					" Search term product count not shown above the search term in the secondary search box.", driver);
			
			Log.softAssertThat(slp.elementLayer.verifyVerticalAllignmentOfElements(driver, "inpSecondrySearch", "refinementSection", slp), 
					"Horizontal refinement bar should be displayed below the secondary search box.",
					"Horizontal refinement bar is displayed below the secondary search box.",
					"Horizontal refinement bar is not displayed below the secondary search box.", driver);
			
			Log.softAssertThat(slp.refinements.elementLayer.verifyElementDisplayed(Arrays.asList("sortByRefinemnet", "drpFilterByCollapsed"), slp.refinements), 
					"'Filter By' menu and 'Sort By' menu should be displayed in Horizontal refinement bar",
					"'Filter By' menu and 'Sort By' menu is displayed in Horizontal refinement bar",
					"'Filter By' menu and 'Sort By' menu is not displayed in Horizontal refinement bar", driver);
			
			slp.refinements.openCloseFilterBy("expanded");			
			Log.message(i++ + ". opened Filter By menu.", driver);
			
			String filteroption= slp.refinements.getFilterByState();
			
			Log.softAssertThat(filteroption.equalsIgnoreCase("expanded"), 
					"Filter By Menu should be Opened",
					"Filter By Menu is Opened",
					"Filter By Menu is not be Opened", driver);
			
			slp.refinements.selectSubRefinementInFilterBy("Color", 1);
			Log.message(i++ + ". Selected value from color dropdown.", driver);
			
			slp.refinements.clickOnBackButtonOnMobileAndTab();
			Log.message(i++ +". Clicked back button from refinement", driver);
			
			if(slp.refinements.getListFilterOptions().contains("size")) {
				slp.refinements.selectSubRefinementInFilterBy("Size", 2);
				Log.message(i++ + ". Selected value from color dropdown.", driver);
			}
			
			slp.refinements.openCloseFilterBy("collapsed");
			Log.message(i++ +". Closed filter by dropdown", driver);
			
			PdpPage pdpPage = slp.navigateToPdp(1);
			
			//Step 5 - Product details page
			
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "sectionPrdDetails", "prodImage_Desk_Tab", pdpPage)
					&& pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "prodBreadCrumb", "prodImage_Desk_Tab", pdpPage), 
					"Product Images should be displayed to the left hand side of the Product detail page below the breadcrumb.",
					"Product Images is displayed to the left hand side of the Product detail page below the breadcrumb.",
					"Product Images is not displayed to the left hand side of the Product detail page below the breadcrumb.", driver);
			
			EnlargeViewPage enlargeViewPage  = pdpPage.clickOnEnlargeButton();
			Log.message(i++ + ". Clicked on Enlarge button on PDP");
	
			Log.softAssertThat(enlargeViewPage.elementLayer.verifyPageElements(Arrays.asList("mdlEnlargeWindow"), enlargeViewPage),					 
					"Enlarge Modal window should be displayed when clicking on enlarge button on PDP", 
					"Enlarge Modal window is displayed", 
					"Enlarge Modal window is not displayed", driver);
			
			Log.softAssertThat(enlargeViewPage.elementLayer.verifyPageElements(Arrays.asList("imgMainProduct", "txtProductName", "colorSwatches", "alternateImages", "btnEnlargeViewClose"), enlargeViewPage),					 
					"'Full Width image', 'Product Name', 'Color Swatch', 'Alternate Images', 'X Button' should be displayed on enlarge view page", 
					"'Full Width image', 'Product Name', 'Color Swatch', 'Alternate Images', 'X Button' is displayed", 
					"'Full Width image', 'Product Name', 'Color Swatch', 'Alternate Images', 'X Button' is not displayed", driver);
			
			enlargeViewPage.closeEnlargeViewModal();
			Log.message(i++ + ". Closed Enlarge view modal", driver);
			
			String prdName = pdpPage.getProductName();
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to Bag", driver);
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			
			//Step 6 - Click on "Checkout Now" button from Add to bag overlay and verify cart page.
			
			Log.softAssertThat(prdName.equals(cartPage.getProductName()), 
					"Make sure added products are properly displaying in the cart page.",
					"Added products are properly displaying in the cart page.",
					"Added products are not properly displaying in the cart page.", driver);
			
			signIn = homePage.headers.signOut();
			Log.message(i++ +". Logged out from site.", driver);
			
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart page.", driver);
			
			Log.softAssertThat(homePage.headers.verifyUserIsLoggedOut(), 
					"Confirm the user logged off from the system", driver);
			
			//Step 7 - Launch the website through Mobile
			
			driver = Utils.changeUADeviceTypeTo(Platform.mobile, browser, driver);
			homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to Home Page in mobile window.", driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage), 
					"The system should display the Homepage", driver);
			
			//Step 8 - Tap on the search icon.
			
			slp = homePage.headers.searchProductKeyword(prdBadgeID[1]);
			
			//Step 9 - verify search result page
			
			Log.softAssertThat(slp.getNumberOfProductTilesPerRow() == 2,
					"The number of product tiles displayed per row should be 2!",
					"The number of product tiles displayed per row is 2!",
					"The number of product tiles displayed per row is not 2!", driver);
			
			Log.softAssertThat(slp.elementLayer.verifyVerticalAllignmentOfElements(driver, "searchResultCountMob", "inpSecondrySearch", slp), 
					" Search term product count should be shown above the search term in the secondary search box.",
					" Search term product count shown above the search term in the secondary search box.",
					" Search term product count not shown above the search term in the secondary search box.", driver);
			
			if(slp.elementLayer.verifyPageElements(Arrays.asList("imgProductBadge"),slp)){
				Log.softAssertThat(slp.verifyBadgeImageLocation(),
						"Product badge should be displayed",
						"Product badge is displayed",
						"Product badge is not displayed", driver);
			}else{
				Log.reference("Product badge is not available in this page");
			}
			
			slp.refinements.openFilterBy();
			Log.message(i++ + ". opened Filter By menu.", driver);
			filteroption = slp.refinements.getFilterByState();
			
			Log.softAssertThat(filteroption.equalsIgnoreCase("expanded"), 
					"Filter By Menu should be Opened",
					"Filter By Menu is Opened",
					"Filter By Menu is not be Opened", driver);
			
			
			slp.refinements.selectSubRefinementInFilterBy("Color", 1);
			Log.message(i++ + ". Selected value from color dropdown.", driver);
			
			slp.refinements.clickOnBackButtonOnMobileAndTab();
			Log.message(i++ +". Clicked back button from refinement", driver);
			
			if(slp.refinements.getListFilterOptions().contains("size")) {
				slp.refinements.selectSubRefinementInFilterBy("Size", 2);
				Log.message(i++ + ". Selected value from color dropdown.", driver);
			}
			slp.refinements.openCloseFilterBy("collapsed");
			Log.message(i++ +". Closed filter by dropdown", driver);
			
			
			pdpPage = homePage.headers.navigateToPDP(prdBadgeID[2]);
			
			//Step 10 - Navigate to Product detail page.
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "prodBreadCrumb", "productName", pdpPage), 
					"Breadcrumb should be displayed above the product name",
					"Breadcrumb is displayed above the product name",
					"Breadcrumb is not displayed above the product name", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productName", "divReviewAndRatingMobile", pdpPage), 
					"Star rating should be displayed below the product name",
					"Star rating is displayed below the product name",
					"Star rating is not displayed below the product name", driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to bag.", driver);
			
			cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to cart page.", driver);
			
			//Step 11 - Navigate to the checkout sign in page
			
			CheckoutPage checkout = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to Checkout signin page.", driver);
			
			checkout.continueToShipping(credential);
			Log.message(i++ + ". Navigated to Checkout.", driver);
			
			//Step 12 - Click on Edit link from item summary section and navigate to Cart page
			
			cartPage = checkout.clickEditItemSummary();
			Log.message(i++ + ". Clicked Edit link from Items summary.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyPageListElements(Arrays.asList("divCartRow"), cartPage), 
					"The system should navigates the user to cart page.",
					"The system navigates the user to cart page.",
					"The system not navigates the user to cart page.",driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyListElementSize("divCartRow", 2, cartPage), 
					"The products added through Tablet and Mobile should be displayed in the cart page.",
					"The products added through Tablet and Mobile is displayed in the cart page.",
					"The products added through Tablet and Mobile is not displayed in the cart page.", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyTextContains("divQuantityMessage", "YOUR SHOPPING BAG HAS BEEN MERGED", cartPage), 
					"The system should display a merge message on the cart page",
					"The system is displayed a merge message on the cart page",
					"The system is not displayed a merge message on the cart page", driver);
			
			//Step 13 - Navigate to Checkout page step -1
			
			 checkout = cartPage.clickOnCheckoutNowBtn(); 
			Log.message(i++ + ". Navigated to Checkout Shipping Page.", driver);
			
			driver = Utils.openNewTab(driver, Utils.getWebSite() + TestData.redirectData.get("addressBook"));
			AddressesPage addressBook = new AddressesPage(driver).get();
			int noOfAddress = addressBook.getSavedAddressesCount();
			driver = Utils.switchToNewWindow(driver);
			
			checkout.fillingShippingDetailsAsSignedInUser("no", "yes", "yes", ShippingMethod.Standard, validAddressLong);
			Log.message(i++ + ". Shipping details filled successfully.", driver);

			checkout.continueToBilling();
			Log.message(i++ + ". Continued to billing.", driver);
			
			//Step 14 - Click on continue button and verify payment details section.
			
			driver = Utils.openNewTab(driver, Utils.getWebSite() + TestData.redirectData.get("addressBook"));
			addressBook = new AddressesPage(driver).get();
			int noOfAddress1 = addressBook.getSavedAddressesCount();
			driver = Utils.switchToFirstWindow(driver);
			
			Log.softAssertThat(noOfAddress1 == (noOfAddress + 1), 
					"This newly entered address should be saved into the address book", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			checkout.fillingCardDetails1("card_Visa", false);
			Log.message(i++ + ". Selected a saved credit card", driver);
			
			//Step 15 - Navigate to Checkout page - step 3
			
			checkout.continueToReivewOrder();
			Log.message(i++ + ". Credit card details submitted.", driver);
			
			OrderConfirmationPage orderConfirm = checkout.clickOnPlaceOrderButton();
			Log.message(i++ + ". Navigated to order confirmation page.", driver);
			
			Log.softAssertThat(orderConfirm.elementLayer.verifyPageElements(Arrays.asList("lblOrderNumber", "lblOrderDate", "lblOrderTotal", "orderSummary", "orderShippingAddress"), orderConfirm), 
					"The following information should match, with the steps that the user selected previously " + 
					"Total, " + 
					"Delivery Method, " + 
					"Payment Method, " + 
					"Billing Address, " + 
					"Shipping Address, " + 
					"Product Information, " + 
					"Order Summary", driver);
			
			Log.testCaseResult();
		
		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24538
