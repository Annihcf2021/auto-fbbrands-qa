package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CategoryLandingPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24585 extends BaseTest {
	
	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	@Test(groups= { "plp", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24585(String browser) throws Exception{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try{
			
			String lvl_Clearance = TestData.get("level_with_clearence").split("\\|")[0];
			String lvl1_Promomsg = TestData.get("level_with_promomsg").split("\\|")[0];
			String lvl2_Promomsg = TestData.get("level_with_promomsg").split("\\|")[1];
			String lvl1_Prdmsg = TestData.get("level_with_specialprdmsg").split("\\|")[0];
			String lvl2_Prdmsg = TestData.get("level_with_specialprdmsg").split("\\|")[1];
			String lvl1_Prdbadge = TestData.get("level_with_badge").split("\\|")[0];
			String lvl2_Prdbadge = TestData.get("level_with_badge").split("\\|")[1];
			String defaultSortOption = TestData.get("sortOption").toLowerCase();
			String sortOrder = "Highest Price";
			String refinement = "Price";
			String[] rowSize = TestData.get("plp_rowSize").split("\\|");
			int desktopRow = StringUtils.getNumberInString(rowSize[0]);
			
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Navigated to Home Page", driver);
			
			PlpPage plpPage = homePage.headers.navigateTo(lvl_Clearance);
			Log.message(i++ +". Navigated to clearance category", driver);
			
			Log.softAssertThat(plpPage.getCurrentSortMode().toLowerCase().contains(defaultSortOption),
					defaultSortOption + " should be the default sort by option selected.", 
					defaultSortOption + " is the default sort by option selected.", 
					defaultSortOption + " is not the default sort by option selected.", driver);
			
			//Step-1 Verify the display of product tiles per row in Product Listing Page
			Log.softAssertThat(plpPage.getNumberOfProductTilesPerRow() == desktopRow,
					"The number of product tiles displayed per row should be " + desktopRow,
					"The number of product tiles displayed per row is " + desktopRow,
					"The number of product tiles displayed per row is not " + desktopRow, driver);
									
			if(plpPage.getSearchResultCount() > 60){
				Log.softAssertThat(plpPage.getProductTileCount() == 60, 
						"The Product Listing Page should show only up to 60 products on initial load.",
						"The Product Listing Page is shown only up to 60 products on initial load.",
						"The Product Listing Page is not shown up to 60 products on initial load.", driver);
			}else{
				Log.reference(". PLP doesn't have more than 60 products.Total products are: " + plpPage.getProductTileCount());
			}
			
			//Step-2 verify the following elements on PLP.
			
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("txtClearencePromoMessage"), plpPage),
					"Clearance Messaging should be displayed",
					"Clearance Messaging is displayed",
					"Clearance Messaging is not displayed", driver);
			
			// To Verify promotional message
			homePage.headers.navigateTo(lvl1_Promomsg, lvl2_Promomsg);
			Log.message(i++ +". Navigated to PLP containing product with promo.", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("txtProdPromoMessage"), plpPage),
					"Promotional Messaging should be displayed",
					"Promotional Messaging is displayed",
					"Promotional Messaging is not displayed", driver);
			
			// To Verify Product message
			homePage.headers.navigateTo(lvl1_Prdmsg, lvl2_Prdmsg);
			Log.message(i++ +". Navigated to PLP containing product with message.", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("txtSpecialProductMsg"), plpPage),
					"Product Messaging should be displayed",
					"Product Messaging is displayed",
					"Product Messaging is not displayed in PLP", driver);
			
			//To verify Product badge
			homePage.headers.navigateTo(lvl1_Prdbadge, lvl2_Prdbadge);
			Log.message(i++ +". Navigated to PLP containing product with badge.", driver);
			
			if(plpPage.elementLayer.verifyPageElements(Arrays.asList("imgProductBadge"),plpPage)){
				Log.softAssertThat(plpPage.verifyBadgeImageLocation(),
						"Product badge should be displayed",
						"Product badge is displayed",
						"Product badge is not displayed", driver);
			} else {
				Log.failsoft("Product Badge not found in PLP.");
			}
			
			//Step-3 Verify colorized image & back view image on PLP
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtProductImages"), plpPage),
					"Backview images should available in PLP", 
					"Backview images are available in PLP",
					"Backview images are not available in PLP", driver);
			
			Log.softAssertThat(plpPage.VerifyBackViewAvailability(),
					"On mouse hover, The system should display the back view image",
					"On mouse hover, The system is displayed the back view image",
					"On mouse hover, The system is not displayed the back view image", driver);
			
			//Step-4 Verify the display of Breadcrumb in the Product Listing Page
			
			String categoryName = plpPage.getCategoryName().toLowerCase();
			List<String> breadcrumbs = plpPage.getBreadcrumbText();
			
			Log.softAssertThat(breadcrumbs.get(0).toLowerCase().contains("home"),
					"The build of brand category root should be displayed as 'HOME'",
					"The build of brand category root is displayed as 'HOME'", 
					"The build of brand category root is not displayed as 'HOME'", driver);
			
			Log.softAssertThat(homePage.headers.elementLayer.verifyCssPropertyForPsuedoElement("breadcrumbDivider", ":after", "content", "/",homePage.headers)
							|| homePage.headers.elementLayer.verifyCssPropertyForPsuedoElement("breadcrumbDivider", ":after", "content", ">",homePage.headers), 
					"breadcrumb category root should be delimited with the symbol ' / ' or '<'", 
					"breadcrumb category root is delimited with the symbol ' / ' or '<'", 
					"breadcrumb category root is not delimited with the symbol ' / ' or '<'", driver);
			
			
			Log.softAssertThat(plpPage.getBreadcrumbFullText().toLowerCase().contains(categoryName), 
					"Category name should be displayed as the breadcrumb value ", 
					"Category name is displayed as the breadcrumb value ",
					"Category name is not displayed as the breadcrumb value ", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "divBreadcrumb", "divRefinement", plpPage),
					"Breadcrumb should be displayed on top of the horizontal refinement",
					"Breadcrumb is displayed on top of the horizontal refinement",
					"Breadcrumb is not displayed on top of the horizontal refinement", driver);
			
			int breadcrumbSize = plpPage.getBreadcrumbcount();
			if(breadcrumbSize > 2){
				for(int cate = breadcrumbSize-1;cate>1;cate--){
					String category = plpPage.getBreadcrumbValueOf(cate);
					plpPage.clickOnBreadCrumbByIndexValue(cate);
										
					if(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage)){
						Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage),
								"On clicking "+cate+" th breadcrumb '"+category+"', system should navigates the user to the selected category PLP",
								"On clicking "+cate+" th breadcrumb '"+category+"', system is navigates the user to the selected category PLP", 
								"On clicking "+cate+" th breadcrumb '"+category+"', system is not navigates the user to the selected category PLP", driver);
					}else{
						CategoryLandingPage categoryPage= new CategoryLandingPage(driver).get();
						Log.softAssertThat(categoryPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"),categoryPage),
								"On clicking "+cate+" th breadcrumb '"+category+"', system should navigates the user to the selected category landing Page",
								"On clicking "+cate+" th breadcrumb '"+category+"', system is navigates the user to the selected category landing Page", 
								"On clicking "+cate+" th breadcrumb '"+category+"', system is not navigates the user to the selected category landing Page", driver);
					}
					driver.navigate().back();
				}
			}
			
			//Step-5 Verify the functionality of the vertical refinement bar
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayedleft("mainContainer", "categoryRefinement", plpPage),
					"vertical refinement bar should display on the left-hand side", 
					"vertical refinement bar is displayed on the left-hand side",
					"vertical refinement bar is not displayed on the left-hand side", driver);
			
			if(plpPage.verifyIfLevel3Displayed()){
				Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("subCategory"), plpPage),
						"As vertical Refinement value, all the sub-categories & child categories name should display",
						"As vertical Refinement value, all the sub-categories & child categories name are displayed", 
						"As vertical Refinement value, all the sub-categories & child categories name are not displayed", driver);
		
			}else if(plpPage.verifyIfLevel2Displayed()){
				Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("categoryLvl2"), plpPage),
						"As vertical Refinement value, Only the sub-categories names should display",
						"As vertical Refinement value, Only the sub-categories names categories name are displayed", 
						"As vertical Refinement value, Only the sub-categoriesnames categories name are not displayed", driver);
			
			}else{
				Log.failsoft("SubCategories and childCategories are not available in PLP", driver);
			}
			
			if(plpPage.verifyIfLevel4Displayed()){
				Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("lvl3CategActive"), plpPage),
						"On clicking the plus symbol, The system should expand & display all the child category name",
						"On clicking the plus symbol, The system is expand & display all the child category name",
						"On clicking the plus symbol, The system is not expand & display all the child category name", driver);
				
				Log.softAssertThat(plpPage.elementLayer.verifyPageElements(Arrays.asList("lvl3CategActive"), plpPage),
						" '+' link should display as '-'", 
						" '+' link is displayed as '-'", 
						" '+' link is not displayed as '-'", driver);
				
				Log.softAssertThat(plpPage.clickOnPlusOrMinusIcon("minus"),
						"On clicking the minus symbol, the system should collapse all the child category under that category",
						"On clicking the minus symbol, the system should collapse all the child category under that category",
						"On clicking the minus symbol, the system should collapse all the child category under that category", driver);
				
				String category =plpPage.getCategoryNamenxtByPlus().toLowerCase();
				plpPage.clickOncategoryNxtPlusSymbol();
				Log.message(i++ +". Clicked category name");
				
				Log.softAssertThat(plpPage.getCategoryName().toLowerCase().contains(category),
						"the system navigates the user to the selected root level category name",
						"the system navigates the user to the selected root level category name",
						"the system navigates the user to the selected root level category name", driver);
				
				driver.navigate().back();
			
			}else{
				Log.reference("Level 4 categories are not available");
			}
			
			//Step-6 Verify the functionality of the horizontal refinement bar
			
			Log.softAssertThat(plpPage.getRefinementList().isEmpty() == false,
					"Refinement options should displayed.",
					"Refinement options are displayed.",
					"Refinement options are not displayed.", driver);
			
			Log.softAssertThat(plpPage.refinements.Verify11thRefinementsValueDisplayedNxtRow(),
					"under the filter options, Values 1-10 should appear in a first column & value 11 begins the next column to the right. ",
					"under the filter options, Values 1-10 is appeared in a first column & value 11 begins the next column to the right. ",
					"under the filter options, Values 1-10 is not appeared in a first column & value 11 begins the next column to the right. ", driver);
			
			int productCount = plpPage.getSearchResultCount();
			
			if (plpPage.elementLayer.verifyPageElements(Arrays.asList("drpColorRefinement"), plpPage.refinements)) {
				String color1 = plpPage.refinements.selectColorOnRefinementByIndex(1).toUpperCase();
				Log.message(i++ +". Selected "+color1+" From color dropdown");
				
				String color2 = plpPage.refinements.selectColorOnRefinementByIndex(2).toUpperCase();
				Log.message(i++ +". Selected "+color2+" From color dropdown");
			} else if (plpPage.elementLayer.verifyPageElements(Arrays.asList("drpSizeRefinement"), plpPage.refinements)) {
				String size1 = plpPage.refinements.selectSizeOnRefinement().toUpperCase();
				Log.message(i++ +". Selected "+size1+" From size dropdown");
				
				String size2 = plpPage.refinements.selectSizeOnRefinement().toUpperCase();
				Log.message(i++ +". Selected "+size2+" From size dropdown");
			}
			
			plpPage.refinements.selectSubRefinementInFilterBy(refinement, 1);
			Log.message(i++ + ". Filtered by "+ refinement, driver);

			Log.softAssertThat(plpPage.getSearchResultCount() < productCount,
					"Products should be displayed based on the color filter option",
					"Products is displayed based on the color filter option",
					"Products is not displayed based on the filter option", driver);
			
			//Step-7 Verify Sort By bar.
			
			BrowserActions.scrollToTopOfPage(driver);
			Log.softAssertThat(plpPage.refinements.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "refinementFilterOptionsDesktop", "drpSortByCollapsedDesktop", plpPage.refinements)
							&& plpPage.refinements.elementLayer.verifyHorizontalAllignmentOfElements(driver, "drpSortByCollapsedDesktop", "divResultCountDesktop", plpPage.refinements),
					"The Sort By bar should be located below and to the right of the refinement bar",
					"The Sort By bar is located below and to the right of the refinement bar",
					"The Sort By bar is not located below and to the right of the refinement bar", driver);

			String sortOption = plpPage.refinements.selectSortBy(sortOrder);
			Log.message(i++ + ". Sort Option(" + sortOption + ") selected from Sorting menu.", driver);
			
			Log.softAssertThat(plpPage.refinements.verifySortApplied(sortOption),
					"Products should be sorted as per the selected option and listed in PLP",
					"Products sorted as per the selected option",
					"Products not sorted as per the selected sorting option", driver);

			
			BrowserActions.scrollToBottomOfPage(driver);
			Log.message(i++ +". Scrolled to bottom of the page", driver);
			
			//Step-8 Verify the display of 'Top' button
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnBackToTop"),plpPage)
							&& plpPage.elementLayer.verifyInsideElementAlligned("btnBackToTop", "fullPlpPage", "right", plpPage), 
					"'Top' button should display at the right bottom of the browser ",
					"'Top' button is displayed at the right bottom of the browser ", 
					"'Top' button is not displayed at the right bottom of the browser ", driver);
			
			
			plpPage.clickBackToTopButton();
			Log.message(i++ + ". Clicked on Back to Top button.", driver);
			
			Log.softAssertThat(plpPage.elementLayer.getElementCSSValue("btnBackToTop", "display", plpPage).equalsIgnoreCase("none")
							|| !plpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnBackToTop"), plpPage),
					"The user should take back to the top of the page and the button should be removed.",
					"The user should take back to the top of the page and the button is removed.",
					"The user should take back to the top of the page and the button is not removed.", driver);
			
			
			BrowserActions.scrollToBottomOfPage(driver);
			Log.message(i++ +". scroll down a little bit & reach in the middle of the page", driver);
			
			BrowserActions.scrollToTopOfPage(driver);
			Log.message(i++ +". Scrolled to top of the page", driver);
			
			plpPage.waitUntilBackToTopDisappear();
			Log.softAssertThat(plpPage.elementLayer.verifyAttributeForElement("btnBackToTop", "style", "display: none", plpPage),
					"The Top Button should disappears when the user scrolls up passing the trigger point for the top button to display",
					"The Top Button is disappears when the user scrolls up passing the trigger point for the top button to display",
					"The Top Button is not disappears when the user scrolls up passing the trigger point for the top button to display", driver);
			
			plpPage.scrollTogetStickyAndBacktoTopButton();
			Log.message(i++ +". Scroll down again & reach in the middle of the page", driver);
			
			//Step-9 Verify the sticky header
			
			Log.softAssertThat(homePage.headers.checkGlobalHeaderIsStickyWithoutScrollDown(),
					"The sticky header should display when the user scroll down",
					"The sticky header is displayed when the user scroll down",
					"The sticky header is not display when the user scroll down", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("categorySticky","minicartIcon"),plpPage),
					"Verify Under the sticky header root category & mini cart should display",
					"Under the sticky header root category & mini cart is displayed", 
					"Under the sticky header root category & mini cart is not displayed", driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "stickyGlobalNavigation", "divRefinement", plpPage),
					"Verify under the sticky header horizontal refinement bar should display",
					"Verify under the sticky header horizontal refinement bar is displayed",
					"Verify under the sticky header horizontal refinement bar is not displayed", driver);
			
			homePage.headers.navigateTo(lvl_Clearance);
			Log.message(i++ +". Clicked on category from stick header", driver);
			
			//Verify the functionality of the pagination.
			
			Log.softAssertThat(plpPage.verifyPaginationDisplayedBelowProductTiles(), 
					"Pagination numbers should be displayed below the product list page", 
					"Pagination numbers is displayed below the product list page",
					"Pagination numbers is not displayed below the product list page", driver);
			
			if (BrandUtils.isBrand(Brand.ks)) {
				Log.softAssertThat(plpPage.verifyCurrentPageNumberHighlighted()	&& 
							plpPage.elementLayer.verifyCssPropertyForPsuedoElement("txtCurrentPageNo", "after", "box-shadow", "rgba(0, 0, 0, 0.6) 0px 2px 5px 0px", plpPage),
						"The pagination number of the current page should be highlighted!",
						"The pagination number of the current page is highlighted!",
						"The pagination number of the current page is not highlighted", driver);
			} else {
				Log.softAssertThat(plpPage.verifyCurrentPageNumberHighlighted(),
						"The pagination number of the current page should be highlighted in black color!",
						"The pagination number of the current page is highlighted in black color!",
						"The pagination number of the current page is not highlighted in black color!", driver);
			}
			
			Log.softAssertThat(!plpPage.verifycurrentpageisClickable(),
					"On clicking Current page should not be clickable", 
					"Current page is not be clickable",
					"Current page is clickable", driver);
			
		int lstpg = plpPage.getLastPageNumber();	
		String lstPage = Integer.toString(lstpg);
		
		Log.softAssertThat(plpPage.isPageNumberDisplayed("1") &&
				plpPage.isPageNumberDisplayed(lstPage),
				"Always 1st-page number and last page number should be displayed",
				"Always 1st-page number and last page number is displayed",
				"Always 1st-page number and last page number is not displayed", driver);
			
			
		plpPage.clickNextButtonInPagination();
		Log.message(i++ +". Clicked on the 'next' link");
		
		Log.softAssertThat(plpPage.getCurrentPageNumber() == 2, 
				"The system should navigates the user to the 2nd page", 
				"The system is navigates the user to the 2nd page",
				"The system is not navigates the user to the 2nd page", driver);
		
		Log.softAssertThat(plpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "firstPage", "btnPaginationPrev", plpPage),
				"next to the 1st page: 'PREV' link should be displayed in Pagination.",
				"next to the 1st page: 'PREV' link is displayed in Pagination.",
				"next to the 1st page: 'PREV' link is not displayed in Pagination.", driver);
					
		plpPage.clickOnPageNumberInPagination(lstpg);
		Log.message(i++ +". Clicked last page number from Pagination."+lstPage, driver);
		Log.softAssertThat(plpPage.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("btnPaginationNext"), plpPage),
				"'NEXT' link should be removed from Pagination", 
				"'NEXT' link is removed from Pagination",
				"'NEXT' link is not be removed from Pagination", driver);
		
		plpPage.mouseHoverProductByIndex(1);
		Log.message(i++ +". Mouse hovered on the first product.");
		
		//Step-11 Verify the Quick shop functionality
		
		plpPage.clickOnPageNumberInPagination(1);
				
		QuickShop quickShop = plpPage.clickOnQuickShop();
		Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("divQuickShop"), quickShop),
				"Quick shop link should display and the system should displays the quick shop overly",
				"Quick shop link is displayed and the system is displays the quick shop overly",
				"Quick shop link is not displayed and the system is not displays the quick shop overly", driver);
		
		String productName = quickShop.getProductName().toLowerCase();
		String imageName = quickShop.elementLayer.getAttributeForElement("imgProdMainImg", "src", quickShop);
		int alternateImgCount = quickShop.getNumberOfAlternateImages();
		
		if(imageName.contains("noimage") && alternateImgCount==1){
			Log.reference("Zoom image is not displayed because product doesn't have correct image");
		}else{
			if(imageName.contains("noimage") && alternateImgCount>1 ) {
				quickShop.clickAlternateImages(1);
			}
			Log.message(i++ +". Trying to Mouse hover on the main image.");
			quickShop.checkProductMainImageZoom();
			
			Log.softAssertThat(quickShop.verifyZoomedImageDisplayedRightOfPrimaryProductImage(),
					"A zoomed image should be displayed over the product attribute section to the right side of the product main image" , 
					"A zoomed image is displayed over the product attribute section to the right side of the product main image",
					"A zoomed image is not displayed over the product attribute section to the right side of the product main image", driver);
		}		
		
		if(quickShop.getNumberOfAlternateImages()>2) {
			quickShop.clickAlternateImages(1);
			Log.message(i++ + ". Clicked on 1st alternate image!", driver);
			
			Log.softAssertThat(quickShop.verifyIfSelectedThumbnailImageDisplayedAsPrimaryProductImage(),
					"The selected Alternate image should be displayed as primary product image!",
					"The selected Alternate image is displayed as primary product image!",
					"The selected Alternate image is not displayed as primary product image!", driver);
		}else {
			Log.message(i++ +". Alternate images are not available for "+quickShop.getProductName()+".",driver);
		}
		
		String color= quickShop.selectColor();
		Log.message(i++ + ". Selected color swatch: "+ color, driver);
		
		Log.softAssertThat(quickShop.verifyIfSelectedColorDisplayedAsPrimaryProductImage(),
				"The colorized image should populates the main product image space", 
				"The colorized image is populates the main product image space",
				"The colorized image is not populates the main product image space", driver);
		
		Log.softAssertThat(quickShop.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSelectedColorValue", "lblColorSwatch", quickShop),
				"The color name should display next by the color level",
				"The color name is displayed next by the color level",
				"The color name is not displayed next by the color level", driver);
		
		Log.softAssertThat(quickShop.verifySizeswatchesUpdatedOrNot(),
				"Size variation attributes should be updated based on the color swatch.",
				"Size variation attributes are updated based on the color swatch.",
				"Size variation attributes are not updated based on the color swatch.", driver);
		
		String size = quickShop.selectSize();
		Log.message(i++ + ". Selected Size swatch: "+ size, driver);
		
		Log.softAssertThat(quickShop.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSelectedSizeValue", "lblSizeSwatch", quickShop) || 
				quickShop.elementLayer.verifyHorizontalAllignmentOfElements(driver, "txtSelectedShoeSizeValue", "lblShoeSizeSwatch", quickShop),
				"The size name should display next by the size level",
				"The size name is displayed next by the size level",
				"The size name is not displayed next by the size level", driver);
		
		Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("availabilityMsg"),quickShop), 
				"Availability message will displayed based on the size", 
				"Availability message is displayed based on the size",
				"Availability message is not displayed based on the size", driver);
		
		quickShop.addToBag();
		Log.message(i++ + ". Clicked Add to bag button", driver);
		
		Log.softAssertThat(quickShop.elementLayer.verifyPageElements(Arrays.asList("flytMiniCart"), quickShop),
				"Add to bag overly should display", 
				"Add to bag overly is displayed",
				"Add to bag overly is not displayed", driver);
		
		ShoppingBagPage cart = quickShop.clickOnCheckoutInMCOverlay();
		Log.message(i++ + ". Clicked checkout now button from Add to bag overlay", driver);
		
		Log.softAssertThat(cart.elementLayer.verifyPageElements(Arrays.asList("miniCartContent"),cart),
				"The system navigates the user to the cart page",
				"The system is navigates the user to the cart page",
				"The system is not navigates the user to the cart page",driver);
		
		//Step-12 Verify the cart page
		
		Log.softAssertThat(cart.getProductName().toLowerCase().equalsIgnoreCase(productName),
				"In the cart page verify selected product should display", 
				"In the cart page verify selected product is displaying",
				"In the cart page verify selected product is not displaying", driver);
		
		Log.testCaseResult();

		}//End try block
		catch(Exception e){
			Log.exception(e, driver);
		}// end catch block
		finally{
			Log.endTestCase(driver);
		} //end finally block
	}//M1_FBB_E2E_C24585
}//TC_FBB_E2E_C24585