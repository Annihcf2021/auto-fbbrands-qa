package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.CSR.CSR;
import com.fbb.pages.CSR.CustomerSearchResultPage;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C26138 extends BaseTest {
	EnvironmentPropertiesReader environmentPropertiesReader;	

	private static EnvironmentPropertiesReader accountsData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");

	@SuppressWarnings("unchecked")
	@Test(groups = { "oobo", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C26138(String browser) throws Exception {
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try {
			
			String userEmail = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountsData.get("password_global");
			String credentials = userEmail + "|" + password;
			String userNameBM = accountsData.get("credentialBM").split("\\|")[0];
			String passwordBM = accountsData.get("credentialBM").split("\\|")[1];
			String prdVariation1 = TestData.get("prd_variation1");
			String prdVariation2 = TestData.get("prd_variation2");
			String cqoNumber = TestData.get("qc_valid").split("\\|")[0];
			HashMap<String, String> cqoNumberVariations = new HashMap<String, String>();
			String address = "valid_address7";
			String firstName = checkoutData.get("valid_address7").split("\\|")[7];
			String lastName = checkoutData.get("valid_address7").split("\\|")[8];
			String cardType = "cards_2";
			
			{
				GlobalNavigation.registerNewUserWithUserDetail(driver, 0, 0, firstName, lastName, credentials);
				GlobalNavigation.addNewAddressToAccount(driver, address, false, credentials);
				cqoNumberVariations = Utils.getCatalogVariationFromTestData(TestData.get("qc_valid"));
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			homePage.headers.navigateToMyAccount(userEmail, password);
			Log.message(i++ + ". Navigated to My Account page as : " + userEmail, driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation1);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			HashSet<String> lstCartProductNamesUser = shoppingBagPg.getCartItemNameList();
			String basketID = shoppingBagPg.getShoppingBagID();
			Log.message(i++ + ". Shopping bag id is " + basketID, driver);
			
			homePage = BrowserActions.clearCookies(driver);
			Log.message(i++ + ". Cleared cookies and navigated to Homepage", driver);
			
			//Step 1: Log in as a CSR.
			CSR csr = homePage.headers.navigateToCSR();
			Log.message(i++ + ". Navigated to CSR Login Page", driver);
			
			csr.loginCSR(userNameBM, passwordBM);
			Log.message(i++ + ". Navigated to CSR Form Page", driver);
			
			//Step 2: Lookup customer basket
			CustomerSearchResultPage searchresultpage = csr.searchCustomerBasket(basketID);
			Log.message(i++ + ". Navigated to Customer Search result Page", driver);
			
			Log.softAssertThat(searchresultpage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("basketIdLnk"), searchresultpage), 
					"The system should display the Basket ID", 
					"The system displaying the Basket ID:: " + basketID, 
					"The system not displayed the Basket ID", driver);
			
			Log.softAssertThat(searchresultpage.verifyCustomerDetailsRegister(basketID, firstName, lastName, userEmail), 
					"The system should display the CustomerDetails", 
					"The system displaying the CustomerDetail ", 
					"The system not displayed the CustomerDetails ", driver);
			
			//Step 3: Click on the basket ID number
			//Step 4: Verify the cart
			shoppingBagPg = searchresultpage.clickBasketID();
			Log.message(i++ + ". Clicked on basked ID in CSR page", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("miniCartContent"), shoppingBagPg), 
					"The system should navigate the user to the storefront side", 
					"The system is navigated the user to the storefront side", 
					"The system is not navigated the user to the storefront side", driver);
			
			HashSet<String> lstCartProductNamesCSR = shoppingBagPg.getCartItemNameList();
			Set<String> lstCartProductsCSR = new HashSet<String>();
			lstCartProductsCSR.addAll(shoppingBagPg.getProductVariationAndBrandName().keySet());
			Log.printMapContent(shoppingBagPg.getProductVariationAndBrandName());
			
			Log.softAssertThat(shoppingBagPg.elementLayer.compareTwoHashSet(lstCartProductNamesUser, lstCartProductNamesCSR), 
					"All the product should match & it should display products.", 
					"All the product are matched & it displayed products.", 
					"All the product are not matched & it not displayed products.", driver);
			
			//Step 5: Navigate to any PDP
			pdpPage = homePage.headers.navigateToPDP(prdVariation2);
			Log.message(i++ + ". Navigated To PDP page by CSR: "+pdpPage.getProductName(), driver);
			
			pdpPage.addToBagKeepOverlay();
			lstCartProductsCSR.add(pdpPage.getVariationFromCartOverlay());
			pdpPage.closeAddToBagOverlay();
			Log.message(i++ + ". Product added to cart by CSR", driver);
			
			//Step 6: Navigate to CQO page
			QuickOrderPage cqoPage = homePage.headers.navigateToQuickOrder();
			Log.message(i++ + ". Navigated to CQO search page", driver);
			
			cqoPage.searchItemInQuickOrder(cqoNumber);
			Log.message(i++ + ". Searched for catalog product", driver);
			
			cqoPage.selectVariation(0, cqoNumberVariations);
			Log.message(i++ +". Selected variation for CQO product", driver);
			
			cqoPage.clickAddProductToBag(0);
			Log.message(i++ + ". Added product to cart", driver);
			
			lstCartProductsCSR.add(cqoPage.getVariationFromCartOverlay());
			cqoPage.closeAddToBagOverlay();
			Log.message(i++ + ". Added product to cart", driver);
			
			//Step 7: Navigate to the cart page
			shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			Set<String> lstCartProductsFinal = shoppingBagPg.getProductVariationAndBrandName().keySet();
			Log.event("CSR:: " + lstCartProductsCSR.toString());
			Log.event("Final cart:: " + lstCartProductsFinal.toString());
			
			Log.softAssertThat(lstCartProductsFinal.equals(lstCartProductsCSR), 
					"All the product should match & it should display products.", 
					"All the product are matched & it displayed products.", 
					"All the product are not matched & it not displayed products.", driver);

			//Step 8: Navigate to the checkout
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			CheckoutPage checkout = shoppingBagPg.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to Checkout Page", driver);
			
			checkout.continueToPayment();
			Log.message(i++ +". Clicked continue button from shipping address section", driver);
			
			//Step 9: Enter Shipping and Billing information.
			checkout.fillingCardDetails1(cardType, false, false);
			Log.message(i++ +". Entered credit card details", driver);
			
			checkout.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Clicked continue in Payment details page", driver);
			
			HashSet<String> ProductId = checkout.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkout.getOrderSummaryTotal();
			
			//Step 10: Payment information
			OrderConfirmationPage orderConfirmation = checkout.clickOnPlaceOrderButton();
			Log.message(i++ + ". Clicked on palce order button", driver);
			
			if(Utils.isMobile()) {
				orderConfirmation.clickOnViewDetailsMobile();
				Log.message(i++ + ". Clicked on view details button", driver);
			}
			
			//Step 10: - Order confirmation page
			Log.softAssertThat(!(orderConfirmation.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			Log.softAssertThat(orderConfirmation.checkEnteredShippingAddressReflectedInOrderReceipt(address), 
					"Same shipping address should display in the receipt which is entered in checkout shipping detail", 
					"Same shipping address is displaying", 
					"Different shipping address is displaying", driver);
			
			Log.softAssertThat(orderConfirmation.checkEnteredBillingAddressReflectedInOrderReceipt(address), 
					"Same billing address should display in the receipt which is entered in checkout billing detail", 
					"Same billing address is displaying", 
					"Different billing address is displaying", driver);
			
			Log.softAssertThat(orderConfirmation.comparePaymentAfterOrderWithEnteredPaymentMethod(cardType), 
					"Same payment method should display in the receipt which is entered in checkout payment section", 
					"Same payment method is displaying", 
					"Different payment method is displaying", driver);
			
			Log.softAssertThat(orderConfirmation.elementLayer.verifyTextContains("lblOrderReceipt", userEmail, orderConfirmation), 
					"The mail ID entered should be reflected in receipt", 
					"The mail ID entered is reflected in receipt", 
					"The mail ID entered is not reflected in receipt", driver);
			
			HashSet<String> ProductIdReceipt = orderConfirmation.getOrderedPrdListNumber();
			
			Log.softAssertThat(orderConfirmation.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = orderConfirmation.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);

			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.removeAllItemsFromQuickOrder(driver);
			Log.endTestCase(driver);
		}// Ending finally
	} // M1_FBB_E2E_C26138

}// TC_FBB_E2E_C26138
