package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24022 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	

	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24022(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{

			String searchText = TestData.get("product_search_terms").split("\\|")[0];
			String userEMailId = AccountUtils.generateEmail(driver);
			String checkoutAdd = "taxless_address";
			String payment = "card_Visa";
			int cartUpdatedQty = 2;
			
			
			//step 1 - Navigate to the website
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//step 2 - Click on search field in the header
			
			homePage.headers.typeTextInSearchField(searchText);
			Log.message(i++ + ". Typed in the Search Field:: " + searchText, driver);
			
			Log.softAssertThat(homePage.headers.getEnteredTextFromSearchTextBox().trim().equals(searchText.trim()), 
					"Text should be entered in the Search text box", 
					"Text is entered in the Search text box", 
					"Text is not entered in the Search text box", driver);
			
			SearchResultPage searchResultPg = homePage.headers.searchProductKeyword(searchText);
			Log.message(i++ + ". Navigated to Search Result Page!", driver);
			
			Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayed(Arrays.asList("sectionSearchResult"), searchResultPg), 
					"Search result page should be displayed", 
					"Search result page is getting displayed", 
					"Search result page is not getting displayed", driver);
			
			Log.softAssertThat(searchResultPg.getSearchResultCount() > 0, 
					"Search result page product count should displayed", 
					"Search result page product count is displayed", 
					"Search result page product count is not displayed", driver);
			
			//Step 3 - Navigate to Product listing page
			String elemResultCount = "";
			
			if(Utils.isMobile()) {
				elemResultCount = "lblResultCountMobile";
			} else {
				elemResultCount = "lblResultCountDesktop";
			}
			
			Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayedleft("lblResultCountContainer", elemResultCount, searchResultPg), 
					"Search result count should be displayed at left side of the page", 
					"Search result count is displayed at left side of the page", 
					"Search result count is not displayed at left side of the page", driver);
			
			Log.softAssertThat(searchResultPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("leftNavigation"), searchResultPg), 
					"Left navigation pane should not be displayed in product list page", 
					"Left navigation pane is not displayed in product list page", 
					"Left navigation pane is displayed in product list page", driver);
			
			//Step 4 - Navigate to Product detail page
			Object[] obj = searchResultPg.navigateToPDPforFirstSwatchWithImage();
			PdpPage pdpPage = (PdpPage) obj[0];
			String prdName = (String) obj[1];
			String prdPrice = (String) obj[2];
			Log.message(i++ + ". Navigated To PDP page : " + pdpPage.getProductName(), driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage), 
					"PDP page should be displayed", 
					"PDP page is getting displayed", 
					"PDP page is not getting displayed", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementTextContains("productName", prdName, pdpPage), 
					"PDP page product name should be correct", 
					"PDP page product name is correct", 
					"PDP page product name is not correct", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementTextContains("txtProdPrice", prdPrice, pdpPage), 
					"PDP page product price should be correct", 
					"PDP page product price is correct", 
					"PDP page product price is not correct", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblPrice"), pdpPage), 
					"System should display the price", 
					"System is displaying the price", 
					"System is not displaying the price", driver);
			//?
			searchResultPg = homePage.headers.searchProductKeyword(searchText);
			Log.message(i++ + ". Typed in the Search Field!", driver);
			
			pdpPage = searchResultPg.navigateToPDPAllSwatch();
			Log.message(i++ + ". Navigated To PDP page : " + pdpPage.getProductName(), driver);
			
			Log.softAssertThat(pdpPage.verifyInitalImageMapping(),
					"Clicking on alternate image should populate in the main image space", 
					"Alternate image is populated in the main image space", 
					"Alternate image is not populated in the main image space", driver);
			
			pdpPage.selectMinimumQuantityVariation(cartUpdatedQty);
			Log.message(i++ + ". All swatches in PDP are selected", driver);
			
			String salesPrice = pdpPage.getSalePrice();
			
			Log.softAssertThat(pdpPage.verifyDisplayImageMatchesSwatchColor(),
					"Clicking on color should populate in the main image space", 
					"Color is populated in the main image space", 
					"Color is not populated in the main image space", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyTxtElementsNotEmpty(Arrays.asList("selectedColorVariant"), pdpPage),
					"Clicking on color should display the color name", 
					"Selected color name is displaying", 
					"Selected color name is not displaying", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("selectedSizeLabel"), pdpPage)
							|| pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("selectedShoeSize"), pdpPage),
					"Clicking on size should display the color name", 
					"Selected size name is displaying", 
					"Selected size name is not displaying", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Product Added to cart", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("mdlMiniCartOverLay"), pdpPage), 
					"Add to Bag Overlay should be displayed", 
					"Add to Bag Overlay is getting displayed", 
					"Add to Bag Overlay is not getting displayed", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementTextContains("lblProductPriceInMCOverLay", salesPrice, pdpPage), 
					"Product price should be displayed in ATB overlay", 
					"Product price is displayed in ATB overlay", 
					"Product price is not displayed in ATB overlay", driver);
			
			//step 5 - Navigate to the cart page
			ShoppingBagPage shoppingBagPg = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			double price = shoppingBagPg.getUpdatedSubtotalPrice();
			double orderPrice = shoppingBagPg.getOrderSubTotal();
			
			shoppingBagPg.updateQuantityByPrdIndex(0, cartUpdatedQty+"");
			
			double UpdatedPrice = shoppingBagPg.getUpdatedSubtotalPrice();
			double orderUpdatedPrice = shoppingBagPg.getOrderSubTotal();
			
			Log.softAssertThat(UpdatedPrice > price, 
					"Subtotal should be increased when the user updates the QTY", 
					"Subtotal is increased when the user updates the QTY", 
					"Subtotal is not increased when the user updates the QTY", driver);
			
			Log.softAssertThat(orderUpdatedPrice > orderPrice, 
					"Subtotal in order sumary should be increased when the user updates the QTY", 
					"Subtotal in order sumary increased when the user updates the QTY", 
					"Subtotal in order sumary not increased when the user updates the QTY", driver);
			
			SignIn signin = (SignIn) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			//Step 6 - Navigate to the checkout sign in page
			CheckoutPage checkoutPg = (CheckoutPage) signin.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("errUsername", "errPassword"), checkoutPg), 
					"Username and Password fields are mandatory, so it cannot be left blank", 
					"Error thrown when username/password fields are left blank", 
					"Error is not thrown when username/password fields are left blank", driver);
			
			checkoutPg.continueToShipping(userEMailId);
			Log.message(i++ + ". Navigated to Shipping section", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("lblGuestUserEmail"), checkoutPg), 
					"The system should accept the valid email address", 
					"The system accepts the valid email address", 
					"The system not accepts the valid email address", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("billingEditFirstname"), checkoutPg), 
					"The system should navigate the user to checkout page - Step 1", 
					"The system is navigate the user to checkout page - Step 1", 
					"The system is not navigate the user to checkout page - Step 1", driver);
			
			//Step 7 - Navigate to Checkout page - step 1
			Log.softAssertThat(checkoutPg.verifyShippingRatesDifferent(), 
					"Verify each faster delivery options cost more than the previous shower option.", 
					"Each faster delivery options cost more than the previous shower option.", 
					"Each faster delivery options do not cost more than the previous shower option.", driver);
			
			Log.softAssertThat(checkoutPg.verifyShippingRateInOrderSummary(), 
					"Verify if selecting different delivery option adds the correct cost to the order summary section.", 
					"Different shipping option adds correct cost to order summary.", 
					"Different shipping option does not add correct cost to order summary.", driver);
			
			checkoutPg.enterAddress1("123 address one");
			Log.message(i++ + ". Typed on Address1", driver);
			
			checkoutPg.enterFirstName("John");
			Log.message(i++ + ". Typed on First name", driver);
			
			checkoutPg.mouseHoverShippingContinue();
			Log.message(i++ + ". Mouse hovered on Continue button", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("errShippingMissingFields", "errMissingFieldsShippingHeading"), checkoutPg), 
					"The user should enter all the mandatory field, otherwise error message should be displayed.", 
					"Error messages displayed.", 
					"Error messages not displayed", driver);
			
			checkoutPg.enterFirstName("1234");
			Log.message(i++ + ". Typed on First name", driver);
			
			checkoutPg.enterAddress2("west");
			Log.message(i++ + ". Typed on Address2", driver);
			
			checkoutPg.mouseHoverShippingContinue();
			Log.message(i++ + ". Mouse hovered on Continue button", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("errShippingMissingFields", "errMissingFieldsShippingHeading"), checkoutPg), 
					"The user should enter all the mandatory field, otherwise error message should be displayed.", 
					"Error messages displayed.", 
					"Error messages not displayed", driver);
			
			checkoutPg.fillingShippingDetailsAsGuest("NO", checkoutAdd, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
			
			//Step 8 - Navigate to Checkout page - step 2
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			Log.softAssertThat(checkoutPg.compareBillingAddressWithEnteredShippingAddress(checkoutAdd), 
					"The system should keep the billing address same as shipping address", 
					"Billing address same as shipping address", 
					"Billing address not same as shipping address", driver);
			
			checkoutPg.enterCVV("123");
			Log.message(i++ + ". Typed on CVV", driver);
			
			checkoutPg.mouseHoverPaymentContinue();
			Log.message(i++ + ". Mouse hovered on Continue button", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("errMissingFieldAboveSelectPayment"), checkoutPg), 
					"The user should enter all the mandatory field, otherwise error message should be displayed.", 
					"Error messages displayed.", 
					"Error messages not displayed", driver);
			
			checkoutPg.clickCVVToolTip();
			Log.message(i++ + ". Clicked on CVV tool tip", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("cvvToolTipContentClose"), checkoutPg), 
					"The system should display the popup window, with CVV information", 
					"Pop up window is displayed", 
					"Pop up window is not displayed", driver);
			
			checkoutPg.clickCloseOnCVVToolTip();
			Log.message(i++ + ". CVV tool tip closed", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("cvvToolTipContentClose"), checkoutPg), 
					"The system should close the tooltip popup window", 
					"Pop up window is not displayed", 
					"Pop up window is displayed", driver);
			
			checkoutPg.fillingCardDetails1(payment, false, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			Log.softAssertThat(checkoutPg.comparePaymentBeforeOrderWithEnteredPaymentMethod(payment), 
					"The system should keep the payment details", 
					"Payment method is saved and displayed", 
					"Payment method is not saved correctly", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			//step 9 - Navigate to Checkout page - step 3
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 10 - Order confirmation page
			Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			Log.softAssertThat(receipt.checkEnteredShippingAddressReflectedInOrderReceipt(checkoutAdd), 
					"Same shipping address should display in the receipt which is entered in checkout shipping detail", 
					"Same shipping address is displaying", 
					"Different shipping address is displaying", driver);
			
			Log.softAssertThat(receipt.checkEnteredBillingAddressReflectedInOrderReceipt(checkoutAdd), 
					"Same billing address should display in the receipt which is entered in checkout billing detail", 
					"Same billing address is displaying", 
					"Different billing address is displaying", driver);
			
			Log.softAssertThat(receipt.comparePaymentAfterOrderWithEnteredPaymentMethod(payment), 
					"Same payment method should display in the receipt which is entered in checkout payment section", 
					"Same payment method is displaying", 
					"Different payment method is displaying", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblProfileEmail", userEMailId, receipt), 
					"The mail ID entered should reflected in receipt", 
					"The mail ID entered is reflected in receipt", 
					"The mail ID entered is not reflected in receipt", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblOrderReceipt", userEMailId, receipt), 
					"The order receipt should send to the entered mail ID", 
					"The order receipt is send to the entered mail ID", 
					"The order receipt is not send to the entered mail ID", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt), 
					"The user can able to take print out of the order receipt", 
					"The user can able to print the order receipt", 
					"The user cannot able to print the order receipt", driver);
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Object obj1 =receipt.clickOnproductNameByIndex(0);
			Log.message(i++ + ". Clicked on product name link from order confirmation page", driver);
			
			Log.softAssertThat(obj1.getClass().getName().contains("PdpPage"),
					"After clicking the product name link, User should redirects to the PDP page",
					"After clicking the product name link, User is redirected to the PDP page",
					"After clicking the product name link, User not redirected to the PDP page", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24022
