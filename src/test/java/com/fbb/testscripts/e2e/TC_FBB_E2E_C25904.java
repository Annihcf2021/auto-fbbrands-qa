package com.fbb.testscripts.e2e;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlatinumCardApplication;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C25904 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C25904(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
				
		int i=1;
		try
		{

			//Step 1 - Navigate to the website			
			String prdVariation = TestData.get("prd_variation");
			String userEMailId = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountData.get("password_global");
			String checkoutAdd = "taxless_address";
						
			//Always Approve Data
			String ssnDetails = checkoutData.get("SSNdetails");
			String addressDetails1 = checkoutData.get("plcc_always_approve_address");
			HashMap<String, String> userAddressM2 = GlobalNavigation.formatPLCCAddressToMapWithSSN(addressDetails1, ssnDetails, driver, userEMailId);
						
			{
				GlobalNavigation.registerNewUser(driver, 0, 0, userEMailId + "|" + password);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Create a new account or logs into an account.
			
			homePage.headers.navigateToMyAccount(userEMailId, password);
			Log.message(i++ + ". User created and logs into an account that is not pre-approved.",driver);

			//Step - 3
			PlatinumCardApplication plccApplication = homePage.navigateToPLCCApplication();
			Log.message(i++ + ". Navigated to Platinum Card Application page",driver);
			
			//Step - 4
			LocalDate dob = java.time.LocalDate.now().plusDays(1);
			plccApplication.enterTextOnField("txtSsnNO", "666085785", "SSN", plccApplication);
			plccApplication.verifyandSelectDateElements("month", Integer.toString(dob.getMonthValue()));
			plccApplication.verifyandSelectDateElements("day", Integer.toString(dob.getDayOfMonth()));
			plccApplication.verifyandSelectDateElements("year", Integer.toString(dob.getYear()));
			Log.message(i++ + ". Entered ssn & invalid DOB",driver);
			
			Log.softAssertThat(plccApplication.elementLayer.verifyTextContains("invalidDateOfBirthError", "Please select valid date", plccApplication),
					"The system should display an error message. Ex - Please select valid date",
					"The system is displayed an error message. Ex - Please select valid date",
					"The system is not displayed an error message. Ex - Please select valid date", driver);
						
			homePage.navigateToPLCCApplication();
			plccApplication.fillPLCCApplication(userAddressM2);
			Log.message(i++ +". Entered Always, approve address", driver);
			
			plccApplication.clickCheckUncheckConsent(true);
			Log.message(i++ + ". Checked concent.", driver);
			
			plccApplication.clickOnRegisterBtn();
			Log.message(i++ + ". Clicked on SUBMIT button.", driver);
			
			if(plccApplication.elementLayer.verifyElementDisplayed(Arrays.asList("divApprovedModal"), plccApplication)) {
				Log.softAssertThat(plccApplication.elementLayer.verifyElementDisplayed(Arrays.asList("divApprovedModal"), plccApplication), 
						"PlatinumCard approved modal should get display", 
						"PlatinumCard approved modal is getting display", 
						"PlatinumCard approved modal is not getting display", driver);
				
				//Step - 5
				plccApplication.clickApprovedModalContinueShopping();
				Log.message(i++ + ". Clicked on Continue Shopping button.", driver);
				
				Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
						"Home page should be displayed", 
						"Home page is getting displayed", 
						"Home page is not getting displayed", driver);
				
				//Step - 6
				PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation);
				Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
				
				pdpPage.addToBagCloseOverlay();
				Log.message(i++ + ". Product added to cart", driver);
				
				ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
				
				//Step 7 -Navigate to the checkout page
				
				CheckoutPage checkoutPg = (CheckoutPage) cartPage.navigateToCheckout();
				Log.message(i++ + ". Navigated to Checkout page", driver);
				
				//Step 8- Filling shipping and billing address
				checkoutPg.fillingShippingDetailsAsGuest("yes", checkoutAdd, ShippingMethod.Standard);
				
				checkoutPg.continueToPayment();
				Log.message(i++ + ". Continued to Payment Page", driver);
				
				if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("plccCardNoError"), checkoutPg)) {
					Log.reference("PLCC card number is not valid, hence can't proceed further", driver);
				
				} else {			
					if(!Utils.isMobile()) {
						Log.softAssertThat(checkoutPg.elementLayer.verifyElementTextContains("lbl_First_PLCC_Card_Name", "Credit Card", checkoutPg), 
								"PLCC card should be selected  when PLCC card approved", 
								"PLCC card is selected when PLCC card approved", 
								"PLCC card is not selected when PLCC card approved", driver);
					} else {
						Log.softAssertThat(checkoutPg.elementLayer.verifyElementTextContains("lbl_First_PLCC_Card_Name_Mobile", "Credit Card", checkoutPg), 
								"PLCC card should be selected automatically when PLCC card approved", 
								"PLCC card is selected automatically when PLCC card approved", 
								"PLCC card is not selected automatically when PLCC card approved", driver);
					}
								
					if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
						Log.reference("Further verfication steps are not supported in current environment.");
						Log.testCaseResult();
						return;
					}
						
					//Step 9 - Navigate to Step 3 - the "Review and Place Order" section.
					
					checkoutPg.clickOnPaymentDetailsContinueBtn();
					Log.message(i++ + ". Continued to Review & Place Order", driver);
					
					//Step 10 - Place the order.
					checkoutPg.clickOnPlaceOrder();
					Log.message(i++ + ". Order Placed successfully", driver);
					
					OrderConfirmationPage orderPage = null;
					
					if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg)) {
						Log.reference("ALWAYS APPROVE address not returned valid PLCC card", driver);
					} else {
						orderPage = new OrderConfirmationPage(driver).get();
						
						if(Utils.isMobile()) {
							orderPage.clickOnViewDetailsMobile();
						}					
						
						Log.softAssertThat(orderPage.elementLayer.verifyNumberMaskedWithSpecifiedDigits("lblEnteredCardNo", 4, orderPage),
								"The last 4 digits of the PLCC card should be be displayed.",
								"The last 4 digits of the PLCC card is be displayed.",
								"The last 4 digits of the PLCC card is not displayed.", driver);
					}
				}
			} else {
				Log.reference("PLCC Approval modal is not displayed hence can't proceed further", driver);
			}
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally
	}
}//M1_FBB_E2E_C24275
