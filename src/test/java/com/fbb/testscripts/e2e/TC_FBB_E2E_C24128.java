package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24128 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "checkout", "desktop","mobile","tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24128(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			//Step 1 - Navigate to the website
			
			String searchText = TestData.get("prd_multiple_searches").split("\\|")[0];
			String hemmingProductID = TestData.get("prd_hemming");
			String userEmailID = AccountUtils.generateEmail(driver);
			String hemmingMessage = demandwareData.get("hemmingMsg");
			String checkoutShipAdd = "address_withtax";
			String checkoutBillAdd = "valid_address8";
			String discoverCard = "card_Discover";
			String rewardCert = TestData.get(Utils.getRewardCertificate(1));
			String rewardCert1 = TestData.get(Utils.getRewardCertificate(2));
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//step 2 - Navigate to search field in the header
			
			SearchResultPage searchResultPg = homePage.headers.searchProductKeyword(searchText);
			Log.message(i++ + ". Navigated to Search Result Page!", driver);
			
			Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayed(Arrays.asList("sectionSearchResult"), searchResultPg), 
					"Search result page should be displayed", 
					"Search result page is getting displayed", 
					"Search result page is not getting displayed", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayed(Arrays.asList("searchResultCountMob"), searchResultPg)
						&& (searchResultPg.getSearchResultCount() > 0), 
						"Search result page product count should displayed", 
						"Search result page product count is displayed", 
						"Search result page product count is not displayed", driver);
			} else {
				Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayed(Arrays.asList("searchResultCountDeskTab"), searchResultPg)
						&& (searchResultPg.getSearchResultCount() > 0), 
						"Search result page product count should displayed", 
						"Search result page product count is displayed", 
						"Search result page product count is not displayed", driver);
			}
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(hemmingProductID);
			Log.message(i++ + ". Searched with Hemming product ID.", driver);
			
			String hemmingProductName = pdpPage.getProductName();
			
			boolean isHemmingEnabled = pdpPage.elementLayer.verifyElementTextContains("txtHemmingTitle", hemmingMessage, pdpPage);
			
			if (isHemmingEnabled) {
				Log.softAssertThat(pdpPage.elementLayer.verifyElementTextContains("txtHemmingTitle", hemmingMessage, pdpPage), 
						"'PLEASE SELECT COLOR & SIZE TO SEE HEMMING OPTIONS.' should be displayed in PDP page when color and size swatches are not selected.", 
						"'PLEASE SELECT COLOR & SIZE TO SEE HEMMING OPTIONS.' is displayed in PDP page when color and size swatches are not selected.", 
						"'PLEASE SELECT COLOR & SIZE TO SEE HEMMING OPTIONS.' not displayed in PDP page when color and size swatches are not selected.", driver);
				
				pdpPage.selectAllSwatches();
				Log.message(i++ +". Selected all swatches", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("cmbMonogrammingMessage"), pdpPage), 
						"Hemming options should be displayed when selecting color and size swatches.", 
						"Hemming options is displayed when selecting color and size swatches.", 
						"Hemming options not displayed when selecting color and size swatches.", driver);
				
				pdpPage.clickOnHemmingCheckbox("enable");
				Log.message(i++ + ". Enabled Hemming checkbox", driver);
				
				pdpPage.selectHemmingOption(1);
				Log.message(i++ + ". Selected Hemming option", driver);
			} else {
				pdpPage.selectAllSwatches();
				Log.message(i++ +". Selected all swatches", driver);
				
				Log.failsoft("Hemming option not found, check BM and test data. Skipping hemming validations...");
			}
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked on Add to Bag button", driver);
			
			ShoppingBagPage cartPage = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Navigated to Shopping bag Page!", driver);
			
			String hemmingPrdNameInCart = cartPage.getProductName();
						
			Log.softAssertThat(hemmingProductName.equalsIgnoreCase(hemmingPrdNameInCart), 
					"Confirm the selected product displaying on the cart page.", 
					"Confirm the selected product is displaying on the cart page.", 
					"Confirm the selected product displaying on the cart page.", driver);
			
			if (isHemmingEnabled) {
				Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("divSpecialMessageHemming"), cartPage), 
						"Selected Hemming size should be displayed in Cart page .ex: HEMMING: 29 (Additional $5.99)", 
						"Selected Hemming size is displayed in Cart page", 
						"Selected Hemming size not displayed in Cart page.", driver);
				
				Log.softAssertThat(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("divSpecialMessageHemmingValue1"), cartPage), 
						"'Hemmable product' message should be displayed below the product details.", 
						"'Hemmable product' message is displayed below the product details.", 
						"'Hemmable product' message not displayed below the product details.", driver);
			} else {
				Log.warning("Hemming not enabled in build. Skipping hemming validations...");
			}
			
			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to Checkout Page!", driver);

			checkoutPage.enterGuestUserEmail("aspire#qa@gmail.com");
			Log.message(i++ + ". Entered special characters in email address field", driver);
			
			checkoutPage.clickOnContinueInGuestLogin();
			 			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblGuestEmailError"), checkoutPage), 
					"Validation failure message should be displayed", 
					"Validation failure message is displayed", 
					"Validation failure message is not displaying", driver);
			
			checkoutPage.continueToShipping(userEmailID);
			
			String specialMessageHemming;
			if(Utils.isMobile()) {
				specialMessageHemming = "divSpecialMessageHemmingValueMobile";
			} else {
				specialMessageHemming = "divSpecialMessageHemmingValue";
			}
			
			if (isHemmingEnabled) {
				Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList(specialMessageHemming), checkoutPage), 
						"'Hemmable product' message should be displayed below the product details.", 
						"'Hemmable product' message is displayed below the product details.", 
						"'Hemmable product' message not displayed below the product details.", driver);
			} else {
				Log.warning("Hemming not enabled in build. Skipping hemming validations...");
			}
				
			checkoutPage.fillingShippingDetailsAsGuest("No", checkoutShipAdd, ShippingMethod.Standard);
			Log.message(i++ +". Filled Shipping address and deselected \"Use this as a billing address\" checkbox",driver);
			
			checkoutPage.checkUncheckUseThisAsBillingAddress(false);
			
			checkoutPage.continueToBilling();
			Log.message(i++ +". Clicked continue button", driver);
			
			//Step-8 Navigate to Checkout page - step 2
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyPageElements(Arrays.asList("billingAddressFields"),checkoutPage),
					"The system should display the billing address form page",
					"The system should display the billing address form page",
					"The system should display the billing address form page",driver);
			
			checkoutPage.fillingBillingDetailsAsGuest(checkoutBillAdd);
			Log.message(i++ +". Entered biling address", driver);
			
			checkoutPage.continueToPayment();
			Log.message(i++ +". Navigated to payment details section", driver);
			
			checkoutPage.clickOnShippingCostTool();  
			
			Log.softAssertThat(checkoutPage.elementLayer.verifyElementDisplayed(Arrays.asList("shippingCostOverlay"), checkoutPage), 
					"The tooltip should be opened when clicking the icon in Standard Delivery Shipping & Handling", 
					"The tooltip is opened when clicking the icon in Standard Delivery Shipping & Handling", 
					"The tooltip not opened when clicking the icon in Standard Delivery Shipping & Handling", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			checkoutPage.clickOnShippingCostToolClose();
			Log.message(i++ +". Closed shipping tooltip", driver);
			
			checkoutPage.applyRewardCertificate(rewardCert);
			Log.message(i++ + ". Reward Certificate is applied", driver);
			
			checkoutPage.applyRewardCertificate(rewardCert1);
			Log.message(i++ + ". 2nd Reward Certificate is applied", driver);
			
			checkoutPage.removeAppliedReward();
			Log.message(i++ + ". Removed applied Reward Certificate.", driver);
			
			checkoutPage.fillingCardDetails1(discoverCard, false, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPage.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			//step 7 - Navigate to Checkout page - step 3
			
			HashSet<String> ProductId = checkoutPage.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPage.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt = checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 8 - Order confirmation page
						
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.removeAllItemsFromQuickOrder(driver);
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24128