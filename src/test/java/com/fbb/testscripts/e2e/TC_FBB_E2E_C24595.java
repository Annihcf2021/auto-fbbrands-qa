package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24595 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	

	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "myaccount", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24595(String browser) throws Exception
	{
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		
		String userEMailId = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
		String password = accountData.get("password_global");
		String credential = userEMailId + "|" + password;
		String colorPasswordDefault = TestData.get("password_default");
		String colorPasswordError = TestData.get("password_error");
		String colorPasswordSuccess = TestData.get("password_success");
		String newpassword_valid = "newpas@123";
		String newpassword_validAlt = "newpas@456";
		String password_ShortLetterOnly = "newpass";
		String password_NumberOnly = "123456789";
		
		int i=1;
		try {
			
			{
				GlobalNavigation.registerNewUser(driver, 0, 0, credential);
			}

			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Browser launched with " + Utils.getWebSite(), driver);

			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(userEMailId, password);
			Log.message(i++ + ". Navigated to My Account Page.", driver);
			
			//step 1 - Verify the functionality of My Profile heading element in My Account Profile screen

			ProfilePage profile = myAcc.navigateToUpdateProfile();
			Log.message(i++ + ". Navigated to Profile Page.", driver);

			Log.softAssertThat(profile.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionBreadcrumb", "lblProfileHeading", profile), 
					"The My Profile heading element should be displayed below the My Account navigation elements available in My Account page (after a faint line break)", driver);
			
			//step 2 - Verify both sections under the profile tab
			
			Log.softAssertThat(profile.elementLayer.verifyPageElements(Arrays.asList("sectionPersonalInfo", "sectionChangePassword"), profile)
							&& profile.elementLayer.verifyVerticalAllignmentOfElements(driver, "lblProfileHeading", "sectionPersonalInfo", profile), 
					"Under the profile tab, it has two section 1) Personal information section 2) Change password section", driver);

			//Step 3 - Verify the change password section
			Log.softAssertThat(profile.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("statusPWLength", "statusPWLetter", "statusPWNumber", "statusPWMatch"), "color", colorPasswordDefault, profile), 
					"All the fields placeholder & The Password Requirements message should display as default(grey) color.", driver);
			
			profile.clickOnUpdatePassword();
			Log.message(i++ + ". Clicked Updated Passwowrd without entering any new passowrd information.", driver);

			Log.softAssertThat(profile.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("lblCurrentPassword", "lblNewPassword", "lblConfirmPassword"), "color", colorPasswordError, profile), 
					"All the fields placeholder & The Password Requirements message should display as red color.", driver);

			profile.typeNewPassword(password_ShortLetterOnly);
			Log.message(i++ + ". Eneterd letter only short password.", driver);
			
			Log.softAssertThat(profile.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("statusPWLength", "statusPWNumber"), "color", colorPasswordError, profile), 
					"8-16 characters icon & 1 number icon displays in a failed state", driver);
			Log.softAssertThat(profile.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("statusPWLetter"), "color", colorPasswordSuccess, profile), 
					"1 letter icon become the default (grey) to pass (green) state", driver);
			
			profile.typeNewPassword(password_NumberOnly);
			Log.message(i++ + ". Entered number only password", driver);
			
			Log.softAssertThat(profile.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("statusPWLetter"), "color", colorPasswordError, profile), 
					"1 letter icon become green to the failed state", driver);
			Log.softAssertThat(profile.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("statusPWLength", "statusPWNumber"), "color", colorPasswordSuccess, profile), 
					"8-16 characters icon & 1 number icon displays in green status", driver);
			
			profile.typeNewPassword(newpassword_valid);
			Log.message(i++ + ". Entered valid new passowrd", driver);
			
			Log.softAssertThat(profile.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("statusPWLength", "statusPWLetter", "statusPWNumber"), "color", colorPasswordSuccess, profile), 
					"All the password verification icons are displaying as green color", driver);
			
			profile.typeConfirmPassword(newpassword_validAlt);
			Log.message(i++ + ". Eneterd mismatched password in Conformation field", driver);
			
			Log.softAssertThat(profile.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("statusPWMatch"), "color", colorPasswordError, profile), 
					"\"Passwords Match\" display as a failed state", driver);
			
			profile.typeCurrentPassword(password);
			profile.typeNewPassword(newpassword_valid);
			profile.typeConfirmPassword(newpassword_valid);
			Log.message(i++ + ". Eneterd valid new password again (make sure at least 8-10 characters and contain at least 1 letter and 1 number)", driver);
			
			Log.softAssertThat(profile.elementLayer.verifyCssPropertyForListOfElement(Arrays.asList("statusPWLength", "statusPWLetter", "statusPWNumber", "statusPWMatch"), "color", colorPasswordSuccess, profile), 
					"All the fields placeholder & The Password Requirements message should display as green color.", driver);

			profile.clickOnUpdatePassword();
			Log.message(i++ + ". Confirm the new password & click on update password button", driver);
						
			if(!BrandUtils.isBrand(Brand.el)) {
				myAcc = new MyAccountPage(driver).get();
				Log.softAssertThat(myAcc.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAcc), 
						"The system updates the password & navigates the user to the Overview page", driver);
				profile = myAcc.navigateToUpdateProfile();
			} else {
				Log.softAssertThat(profile.elementLayer.verifyPageElements(Arrays.asList("txtCurrentPassword", "txtNewPassword"), profile), 
						"The system will update the password & stay in my account profile page", driver);
			}

			//Step 4 - Verify the new password
			profile = new ProfilePage(driver).get();
			SignIn signIn = homePage.headers.signOut();
			Log.message(i++ + ". Logged out from account.", driver);
			
			myAcc = signIn.navigateToMyAccount(userEMailId, newpassword_valid);
			Log.message(i++ + ". From the header log off & try to log in again by using the new password", driver);

			Log.softAssertThat(myAcc.elementLayer.verifyPageElements(Arrays.asList("readyElement"), myAcc), 
					"The system should redirect the user on my account overview page.", driver);

			profile = myAcc.navigateToUpdateProfile();
			Log.message(i++ + ". Navigated to Profile Page.", driver);
			
			Log.softAssertThat(profile.elementLayer.verifyPageElements(Arrays.asList("readyElement"), profile), 
					"Tab on the profile tab & navigates to the profile section", driver);
			
			//Step 5 - Verify the Personal Information section-
			Log.softAssertThat((!profile.elementLayer.getElementText("txtFirstName", profile).isEmpty()) &&
					(!profile.elementLayer.getElementText("txtLastName", profile).isEmpty()) &&
					(!profile.elementLayer.getElementText("txtEmail", profile).isEmpty()), 
					"The first name, last name, email address will prepopulate \r\n" + 
							"- it should display the same the information when they create the account", driver);

			profile.typeFirstName("First Name");
			profile.typeLasName("Last Name");
			profile.typeEmail("asdf@asdf.com");
			profile.typeConfirmEmail("asdf@asdf.com");
			profile.typePhone("2233322333");
			Log.message(i++ + ". Tried to modify details in textboxes.", driver);
			Log.softAssertThat(profile.elementLayer.verifyTextEquals("txtFirstName", "First Name", profile) &&
					profile.elementLayer.verifyTextEquals("txtLastName", "Last Name", profile) &&
					profile.elementLayer.verifyTextEquals("txtEmail", "asdf@asdf.com", profile) &&
					profile.elementLayer.verifyTextEquals("txtConfirmEmail", "asdf@asdf.com", profile) &&
					profile.elementLayer.verifyTextEquals("txtPhoneNo", "2233322333", profile), 
					"The user will be able to update the personal profile fields", driver);

			profile.typeFirstName("First @!@#$");
			profile.typeLasName("Last @!@#$");
			profile.typeEmail("asdf@asdf.com @!@#$");
			profile.typeConfirmEmail("asdf@asdf.com @!@#$");
			profile.typePhone("2233322333 @!@#$");
			Log.softAssertThat(profile.elementLayer.verifyPageElements(Arrays.asList("errFirstName","errLastName","errEmail","errConfirmMail","errPhone"), profile), 
					"If User enters any incorrect data. ex- if the user enters the special character\r\n" + 
							"-The appropriate error message should be displayed.", driver);

			Log.softAssertThat(profile.elementLayer.verifyPlaceHolderMovesAboveForList(Arrays.asList("txtFirstName","txtLastName","txtPhoneNo"), 
					Arrays.asList("errFirstName","errLastName","errPhone"), 
					Arrays.asList("First","Last","2233322333"), profile), 
					"When the user starts typing.\r\n" + 
							"-The placeholder text should move to the top portion of the field", driver);

			profile.typeFirstName("FirstName");
			profile.typeLasName("LastName");
			profile.typeEmail("");
			profile.typeConfirmEmail("");
			profile.typeOnPassword("");
			profile.typePhone("2233322333 @!@#$");
			
			profile.clickOnUpdateInfoBtn();
			Log.softAssertThat(profile.elementLayer.verifyPageElements(Arrays.asList("emailAddressError", "confirmEmailAddressError", "errPassword"), profile), 
					"If the user wants to update their profile information, they need to enter their email address, confirm email address & current password.", driver);

			profile.typeFirstName("FirstName");
			profile.typeLasName("LastName");
			profile.typeEmail(userEMailId);
			profile.typeConfirmEmail(userEMailId);
			profile.typeOnPassword("asdfa@123");
			profile.typePhone("2233322333");
			Log.message(i++ + ". Update the 1st name & phone number field - Enter the email address & confirm email address", driver);
			Log.message(i++ + ". Enter the wrong password", driver);

			profile.clickOnUpdateInfoBtn();
			Log.softAssertThat(profile.elementLayer.verifyPageElements(Arrays.asList("errWrongPassword"), profile), 
					"the system should display the error message.", driver);

			profile.typeConfirmEmail("asdf@asdf.com");
			profile.typeOnPassword(newpassword_valid);
			Log.message(i++ + ". Enter a new last name", driver);
			Log.message(i++ + ". Enters an invalid email address as confirm email address", driver);

			profile.clickOnUpdateInfoBtn();
			Log.softAssertThat(profile.elementLayer.verifyPageElements(Arrays.asList("errConfirmMail"), profile), 
					"the system should display the error message.", driver);

			profile.typeEmail("asdf@asdf.com");
			profile.typeConfirmEmail(userEMailId);
			profile.typeOnPassword(newpassword_valid);
			Log.message(i++ + ". Enter the confirm email address, but enter a different email address", driver);

			profile.clickOnUpdateInfoBtn();
			Log.softAssertThat(profile.elementLayer.verifyPageElements(Arrays.asList("errConfirmMail"), profile), 
					"The system should display a message that \"emails do not match\"", driver);

			//Birth month related steps cannot be automated due to it can not be edited for every execution.
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24595
