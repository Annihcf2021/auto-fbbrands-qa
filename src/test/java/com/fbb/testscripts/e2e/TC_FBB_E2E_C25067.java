package com.fbb.testscripts.e2e;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.iParcelCheckoutPage;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.CollectionUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C25067 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C25067(String browser) throws Exception
	{		
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		
		String searchText = TestData.get("product_search_terms").split("\\|")[0];
		String intrnAddress = "intrn_shipping_canada";
		String prdDropship = TestData.get("prd_dropship");
		String backOrderPrd = TestData.get("prd_back-order");
		String prdRegular = TestData.get("prd_International_Eligible");
		String payment = "card_InternationalShipp";
		
		int i=1;
		try
		{
			//Step 1 - Navigate to the website
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
						
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			
			//Step 4 - Navigate to Product detail page
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdDropship);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			List<String> prdNames= new ArrayList<String>();
			prdNames.add(pdpPage.getProductName().toLowerCase());			
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("cntPdpContent"), pdpPage), 
					"PDP page should be displayed", 
					"PDP page is getting displayed", 
					"PDP page is not getting displayed", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "productName", "lnkReviewsSection", pdpPage), 
					"The product name should display above the product review", 
					"The product name is displaying above the product review", 
					"The product name is not displaying above the product review", driver);
			
			pdpPage.selectColor();
			Log.message(i++ +". Selected color", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyTxtElementsNotEmpty(Arrays.asList("selectedColorVariant"), pdpPage),
					"Selected Color should displayed",
					"Selected Color is displayed",
					"Selected Color is not displayed", driver);
			
			Log.softAssertThat(pdpPage.verifyDisplayImageMatchesSwatchColor(), 
					"The system should display colorized image in the main product image space",
					"The system is displayed colorized image in the main product image space",
					"The system is not displayed colorized image in the main product image space", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "pdpSelectedColor", "colorlabel", pdpPage),
					"The color name should be displayed next to the color label",
					"The color name is displayed next to the color label",
					"The color name is note displayed next to the color label", driver);
			
			Log.softAssertThat(pdpPage.verifySizeswatchesUpdatedOrNot(),
					"Available state should be displayed for each size",
					"Available state should be displayed for each size", 
					"Available state should be displayed for each size", driver);
			
			pdpPage.selectAllSwatches();
			Log.message(i++ + ". Selected all the swatches in PDP page!", driver);
		
			Log.softAssertThat(pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "selectedSizeLabel", "lblSize", pdpPage)||
					pdpPage.elementLayer.verifyHorizontalAllignmentOfElements(driver, "selectedShoeSizeValue", "lblShoeSize", pdpPage),
					"Selected Size should be displayed next to the size label",
					"Selected Size is displayed next to the size label",
					"Selected Size is not displayed next to the size label", driver);
			
			//Step 7a - Navigate to the cart page
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked Add to cart button!", driver);
			
			pdpPage.clickOnContinueShoppingInMCOverlay();
			Log.message(i++ + ". Clicked on Continue Shopping button!", driver);
			
			//step 4 - Click on search field in the header
			
			homePage.headers.typeTextInSearchField(searchText);
			Log.message(i++ + ". Typed in the Search Field!", driver);
			
			Log.softAssertThat(homePage.headers.getEnteredTextFromSearchTextBox().trim().equals(searchText.trim()), 
					"Text should be entered in the Search text box", 
					"Text is entered in the Search text box", 
					"Text is not entered in the Search text box", driver);
			
			//Step 3 - Navigate to Product listing page (SLP)
			
			SearchResultPage searchResultPg = homePage.headers.searchProductKeyword(searchText);
			Log.message(i++ + ". Navigated to Search Result Page!", driver);
			
			Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayed(Arrays.asList("sectionSearchResult"), searchResultPg), 
					"Search result page should be displayed", 
					"Search result page is getting displayed", 
					"Search result page is not getting displayed", driver);
			
			Log.softAssertThat(searchResultPg.getSearchResultCount() > 0, 
					"Search result page product count should displayed", 
					"Search result page product count is displayed", 
					"Search result page product count is not displayed", driver);
			
			String elem = "";
			
			if(Utils.isMobile()) {
				elem = "lblResultCountMobile";
			} else {
				elem = "lblResultCountDesktop";
			}
			
			Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayedleft("lblResultCountContainer",elem, searchResultPg), 
					"Check the result count displayed left side of the page", 
					"The result count is displayed left side of the page", 
					"The result count is not displayed left side of the page", driver);
			
			Log.softAssertThat(searchResultPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("leftNavigation"), searchResultPg), 
					"Left navigation pane should not be displayed in product list page", 
					"Left navigation pane is not displayed in product list page", 
					"Left navigation pane is displayed in product list page", driver);
			
			homePage.headers.navigateToPDP(prdRegular);
			Log.message(i++ +". Navigated to PDP.", driver);
						
			prdNames.add(pdpPage.getProductName().toLowerCase());	
			
			
			//Step 6 - Navigate to the Product detail page
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to cart!", driver);
			
			homePage.headers.navigateToPDP(backOrderPrd.split("\\|")[0]);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			pdpPage.selectColor(backOrderPrd.split("\\|")[1]);
			Log.message(i++ + ". Color selected", driver);
			
			pdpPage.selectSize(backOrderPrd.split("\\|")[2]);
			Log.message(i++ + ". Size selected", driver);
			
			prdNames.add(pdpPage.getProductName().toLowerCase());	
			
			//Step 7b - Navigate to the cart page
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Clicked Add to cart button!", driver);
			
			ShoppingBagPage cartPage = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Clicked on 'Checkout Now' button!", driver);
			
			//Step 8 - Navigate to the i-parcel checkout page.
			Log.softAssertThat(cartPage.getCartItemNameList().containsAll(prdNames),
					"Selected Products should be displayed in cart page",
					"Selected Products are displayed in cart page",
					"Selected Products are not displayed in cart page", driver);

			Log.softAssertThat(cartPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnCheckoutNowFooter", "lnkInternationalShipping", cartPage), 
					"'International Shipping' icon and link should displaying below the 'Checkout now' button", 
					"'International Shipping' icon and link is displaying below the 'Checkout now' button",
					"'International Shipping' icon and link is not displaying below the 'Checkout now' button", driver);
			
			cartPage.clickOnInternationalShippingLink();
			Log.message(i++ + ". Clicked on 'International Shipping' button!", driver);
			
			if(cartPage.elementLayer.verifyElementDisplayed(Arrays.asList("modalInternationalExclusion"), cartPage)){
				List<String> name=cartPage.getproductNamesInInternationalExclusion();				
				for(String prdName:name){
					prdNames.remove(prdName);
				}
				cartPage.clickOnRemoveItemBtnInternationalExclusion();
			}
			
			cartPage.selectConutryOnInternationalShipping(1);
			Log.message(i++ + ". Selected country!", driver);
			
			iParcelCheckoutPage iParcelChkoutPg = cartPage.clickOnContinueInInternationalShipping();
			Log.message(i++ + ". Clicked on Continue in International shipping modal!", driver);
			
			Log.softAssertThat(iParcelChkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), iParcelChkoutPg), 
					"iParcel Checkout Page should be displayed", 
					"iParcel Checkout Page is displayed", 
					"iParcel Checkout Page is not displayed", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			LinkedHashMap<String, String> shippingDetails1 = new LinkedHashMap<String, String>();
			shippingDetails1= iParcelChkoutPg.fillingShippingDetails(intrnAddress);
			
			String userEmail = null;
			for(String key:shippingDetails1.keySet()){
				if(key.contains("email"))
					userEmail = key.split("\\_")[2];	
			}
			
			Log.message(i++ + ". Shipping address filled as guest!", driver);			
			
			iParcelChkoutPg.fillingCardDetails(payment);
			Log.message(i++ + ". Credit card fields filled as guest!", driver);
			
			iParcelChkoutPg.selectUnSelectTermsAndCondition(true);
			Log.message(i++ + ". Accepted Terms and Condition!", driver);
			
			iParcelChkoutPg.selectUpdateButton();
			Log.message(i++ + ". Clicked on update button!", driver);
			
			OrderConfirmationPage receipt = iParcelChkoutPg.clickOnContinueButtonAfterEnteringShippingAndCardDetails();
			Log.message(i++ + ". Clicked on submit button!", driver);
			
			//Step 10 - Order confirmation page
			
			Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			Log.softAssertThat(receipt.checkEnteredShippingAddressReflectedInInternationalOrderReceipt(intrnAddress), 
					"Same shipping address should display in the receipt which is entered in checkout shipping detail", 
					"Same shipping address is displaying", 
					"Different shipping address is displaying", driver);
			
			Log.softAssertThat(receipt.checkEnteredBillingAddressReflectedInInternationalOrderReceipt(intrnAddress), 
					"Same billing address should display in the receipt which is entered in checkout billing detail", 
					"Same billing address is displaying", 
					"Different billing address is displaying", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblOrderReceipt", userEmail, receipt), 
					"The order receipt should send to the entered mail ID", 
					"The order receipt is send to the entered mail ID", 
					"The order receipt is not send to the entered mail ID", driver);
			
			Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt), 
					"The user can able to take print out of the order receipt", 
					"The user can able to print the order receipt", 
					"The user cannot able to print the order receipt", driver);
			
			List<String> receiptPrdNames=receipt.getOrderedPrdListNames();			
				
			Log.softAssertThat(CollectionUtils.compareTwoList(receiptPrdNames, prdNames), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally
	}
}//M1_FBB_E2E_C24022
