package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24591 extends BaseTest {
	
	EnvironmentPropertiesReader environmentPropertiesReader;
		
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24591(String browser) throws Exception{
		Log.testCaseInfo(); 
		
		final WebDriver driver = WebDriverFactory.get(browser);
					
		String guestEmail = AccountUtils.generateEmail(driver);
		String cardType = "cards_2";
		String searchKey = TestData.get("prd_variation");
		String alwaysApproveAddress = "plcc_always_approve_address";
		
		int i=1;
		try{
			//Step-1 As a guest, navigate to the SFCC site.
			HomePage homePage= new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ +". Navigates to  brand home page. ", driver);
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"User should navigates to the site.", 
					"User is navigates to the site.", 
					"User is not  navigates to the site.", driver);
			
			//Step-2 Add products to the cart and navigate to checkout. When user is on the checkout login page, enter a unique email address in the "Checkout as guest" section.
			PdpPage pdpPage=homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ +". Navigated to PDP", driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ +". Product added to cart", driver);
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			
			CheckoutPage checkout= cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ +". Clicked checkout button from cart page", driver);
			
			checkout.enterGuestUserEmail(guestEmail);
			Log.message(i++ +". Entered guest email address: "+guestEmail, driver);
			
			checkout.continueToShipping();
			Log.message(i++ +". Navigated to shipping address section", driver);
			
			//Step-3
							
			checkout.fillingShippingDetailsAsGuest(alwaysApproveAddress, ShippingMethod.Standard);
			Log.message(i++ +". Entered shipping address", driver);
			
			Log.softAssertThat(checkout.verifyUseThisAsBillingAddressIsChecked(), 
					"The Use this as billing address checkbox should be checked",
					"The Use this as billing address checkbox is checked",
					"The Use this as billing address checkbox is not checked", driver);
			
			checkout.continueToPayment();
			Log.message(i++ +". Clicked continue button from shipping address section", driver);
			
			if(checkout.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkout)){
				
				Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkout),
						"The preapproval modal should display. ",
						"The preapproval modal is display. ",
						"The preapproval modal is not displayed. ", driver);

				//Step-4 
				checkout.continueToPLCCStep2();
				Log.message(i++ +". clicked Get, it today button", driver);
			
				Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApprovalStep2"), checkout),
						"When user selects Get It Today, user will be taken to the Step 2 overlay. ",
						"When user selects Get It Today, user is taken to the Step 2 overlay. ",
						"When user selects Get It Today, user is not taken to the Step 2 overlay. ", driver);
		
				//Step-5 
				checkout.typeTextInSSN("1234");
				Log.message(i++ +". Entered SSN number");
				
				checkout.selectDateMonthYearInPLCC2("01","05","1947");
				Log.message(i++ +". Selected date of birth");
				
				checkout.typeTextInMobileInPLCC("8015844547");
				Log.message(i++ +". Entered phone number", driver);
			
				checkout.checkConsentInPLCC("YES");
				Log.message(i++ +". Selected consonent checbox", driver);
				
				checkout.clickOnAcceptInPLCC();
				Log.message(i++ +". Clicked Yes, I Accept button", driver);
				
				if(checkout.elementLayer.verifyElementDisplayed(Arrays.asList("underReviewModal"), checkout)) {
					Log.softAssertThat(checkout.elementLayer.verifyElementDisplayed(Arrays.asList("underReviewModal"), checkout),
							"The Application Under Review modal will pop up.",
							"The Application Under Review modal is pop up.",
							"The Application Under Review modal is not pop up.", driver);
				} else {
					Log.reference("Under review modal is not displayed with \"Always Approve\" data", driver);
				}
				
				checkout.dismissCongratulationModal();
				Log.message(i++ +". Clicking the Continue to Checkout button", driver);
			
				Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("sectionPaymentCard"), checkout), 
						"user will be taken to the payment method section of checkout.",
						"user will be taken to the payment method section of checkout.",
						"user will be taken to the payment method section of checkout.", driver);
				
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
				if(checkout.elementLayer.verifyPageElements(Arrays.asList("lnkAddNewCredit"), checkout)) {
					checkout.clickAddNewCredit();
				}
				//Step-7 
				checkout.fillingCardDetails1(cardType, false, false);
				Log.message(i++ +". Entered credit card details", driver);
				
				checkout.clickOnPaymentDetailsContinueBtn();
				Log.message(i++ +". Clicked continue button", driver);
				
				OrderConfirmationPage order = checkout.clickOnPlaceOrderButton();
				Log.softAssertThat(order.elementLayer.verifyPageElements(Arrays.asList("readyElement"), order),
						"user should taken to the order confirmation page.",
						"user is taken to the order confirmation page.",
						"user is not taken to the order confirmation page.",driver);
							
			} else{
				Log.fail("Checkout step-1 overlay is not displayed hence can't proceed further", driver);
			}			
			
			Log.testCaseResult();

		}//End of Try
		catch(Exception e){
			Log.exception(e, driver);
		}//End of catch
		finally{
			Log.endTestCase(driver);
		}//End of finally block
	}
}
