package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24110 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24110(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try {
			
			String searchText = TestData.get("product_search_terms").split("\\|")[0];
			String productID = TestData.get("prd_variation");
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String checkoutAdd = "address_withtax";
			String payment = "card_Visa";
			
			{
				HashMap<String, String> accountDetails = new HashMap<>();
				accountDetails.put(AddressesPage.class.getName(), checkoutAdd);
				accountDetails.put(PaymentMethodsPage.class.getName(), "0");
				GlobalNavigation.setupTestUserAccount(driver, accountDetails, credential);
			}
			
			//Step 1 - Navigate to the website
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			SearchResultPage searchResultPg = homePage.headers.searchProductKeyword(searchText);
			Log.message(i++ + ". Navigated to Search Result Page for:: " + searchText, driver);
			
			//Step 4 - Navigate to Product detail page
			Object[] obj = searchResultPg.navigateToPDPforFirstSwatchWithImage();
			PdpPage pdpPage = (PdpPage) obj[0];
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
						
			pdpPage = homePage.headers.navigateToPDP(productID);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
						
			pdpPage.selectAllSwatches();
			Log.message(i++ + ". Variation selected", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Product Added to cart", driver);
								
			//Step 5 - Navigate to the cart page
			
			ShoppingBagPage shoppingBagPg = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			double orderSubTotalBefore = shoppingBagPg.getOrderSubTotal();
			int cartProductCount = shoppingBagPg.getProductCountInCart();
			
			shoppingBagPg.mousehoverOnInstockRecommedationImage();
			Log.message(i++ + "Mouse hovered on the recommendation product", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("btnQickShop"), shoppingBagPg),
					"Quick shop button should be displayed.", 
					"Quick shop button is displayed",
					"Quick shop button is not displayed", driver);
			
			QuickShop quickShop = shoppingBagPg.clickOnQuickShopButton();
			Log.message(i++ +". Clicked quick shop button", driver);
			
			quickShop.selectAllSwatches();
			Log.message(i ++ +". Selected color and size", driver);
			
			quickShop.addToBag();
			Log.message(i++ +". product added to cart", driver);
			
			if(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("flytCart"), quickShop)) {
				quickShop.closeCartOverlay();
				Log.message(i++ +" Closed Add to bag overlay", driver);
			} else {
				Log.ticket("SC-5885");
				homePage.headers.navigateToShoppingBagPage();
			}
			
			Log.softAssertThat(shoppingBagPg.getProductCountInCart() > cartProductCount,
					"The cart page should be reloaded and the newly added product should get updated.", 
					"The cart page is reloaded and the newly added product is get updated.",
					"The cart page is not reloaded and the newly added product is not get updated.", driver);
			
			Log.softAssertThat(shoppingBagPg.getOrderSubTotal() > orderSubTotalBefore,
					"Subtotal should be increased",
					"Subtotal is increased",
					"Subtotal is not increased", driver);
			
			SignIn signin = (SignIn) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			//Step 6 - Navigate to the checkout sign in page
			
			CheckoutPage checkoutPg = (CheckoutPage)signin.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);
								
			// step 7 - Navigate to Checkout page - step 1
			
			checkoutPg.continueToShipping(credential);
			Log.message(i++ + ". Navigated to Shipping section", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
			
			checkoutPg.checkUncheckExpressDelivery(true);
			Log.message(i++ + ". Express Delivery radio button enabled", driver);
			
			if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("divShipMedOverlayCont"), checkoutPg)) {
				checkoutPg.clickOnContinueInShippingMethodExceptionOverlay();
			}
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			//Step 8 - Navigate to Checkout page - step 2
									
			checkoutPg.fillingCardDetails1(payment, true, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			Log.softAssertThat(checkoutPg.comparePaymentBeforeOrderWithEnteredPaymentMethod(payment), 
					"The system should keep the payment details", 
					"Payment method is saved and displayed", 
					"Payment method is not saved correctly", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
								
			//Step 9 - Navigate to Checkout page - step 3
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			String orderNumber = receipt.getOrderNumber();
			
			//Step 10 - Order confirmation page			
			Log.softAssertThat(!(orderNumber.isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			MyAccountPage myAccount = homePage.headers.navigateToMyAccount();
			Log.message(i++ + ". Navigated to 'My Account' Page!", driver);
			
			PaymentMethodsPage paymentMethodPage = myAccount.navigateToPaymentMethods();
			Log.message(i++ + ". Navigated to 'Payment methods' Page!", driver);
			
			int savedCardCount = paymentMethodPage.getNumberOfSavedCards();
			
			Log.softAssertThat((savedCardCount > 0) && paymentMethodPage.comparePaymentAfterOrderWithEnteredPaymentMethod(payment),
					"Card details should get saved",
					"Card details is saved",
					"Card details is not saved", driver);
			
			OrderHistoryPage orderHistory = myAccount.clickOnOrderHistoryLink();
			Log.message(i++ + ". Navigated to 'Order History' Page!", driver);
			
			orderHistory.clickOnViewDetails(orderNumber);
			Log.message(i++ + ". Clicked view details for the order: " + orderNumber, driver);
			
			Object obj1 = orderHistory.clickOnBuyAgainLinkByIndex(0);
			Log.message(i++ + ". Clicked on 'Buy Again' link from order guest order details page", driver);
			
			Log.softAssertThat(obj1.getClass().getName().contains("PdpPage"),
					"After clicking the 'Buy Again' link, User should redirects to the PDP page",
					"After clicking the 'Buy Again' link, User is redirected to the PDP page",
					"After clicking the 'Buy Again' link, User not redirected to the PDP page", driver);
			
			Log.testCaseResult();


		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24110
