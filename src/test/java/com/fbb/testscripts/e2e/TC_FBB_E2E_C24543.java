package com.fbb.testscripts.e2e;
import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24543 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24543(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			//Step 1 - Navigate to the website
			
			String level1 = TestData.get("level-1");
			String searchText = TestData.get("product_search_terms").split("\\|")[0];
			String productID = TestData.get("prd_po-box-restricted");
			String prodHeavyItem = TestData.get("prd_heavy_item");
			String rewardCertificate = TestData.get(Utils.getRewardCertificate());
			String checkoutAdd = "taxless_address";
			String username = AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
						
			{
				GlobalNavigation.registerNewUser(driver, 0, 0, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
						
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Sign in with registered user ex: janarthprabhu@gmail.com
			
			homePage.headers.navigateToMyAccount(AccountUtils.generateEmail(driver), accountData.get("password_global"));
			Log.message(i++ + ". Navigated to My Account page as : " +AccountUtils.generateEmail(driver), driver);
			
			//Step 4 - Navigate to search result page
			SearchResultPage searchResultPg = homePage.headers.searchProductKeyword(searchText);
			Log.message(i++ + ". Navigated to Search Result Page!", driver);
			
			Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayed(Arrays.asList("sectionSearchResult"), searchResultPg), 
					"Search result page should be displayed", 
					"Search result page is getting displayed", 
					"Search result page is not getting displayed", driver);
			
			Log.softAssertThat(searchResultPg.getSearchResultCount() > 0, 
					"Search result page product count should displayed", 
					"Search result page product count is displayed", 
					"Search result page product count is not displayed", driver);
			
			String elem = Utils.isMobile() ? "lblResultCountMobile" : "lblResultCountDesktop";
			Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayedleft("lblResultCountContainer",elem, searchResultPg), 
					"Check the result count displayed left side of the page", 
					"The result count is displayed left side of the page", 
					"The result count is not displayed left side of the page", driver);
			
			Log.softAssertThat(searchResultPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("leftNavigation"), searchResultPg), 
					"Left navigation pane should not be displayed in product list page", 
					"Left navigation pane is not displayed in product list page", 
					"Left navigation pane is displayed in product list page", driver);
			
			//Step 4 - Navigate to Product detail page
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(productID);
			Log.message(i++ + ". Navigated to Pdp!", driver);
			
			Log.softAssertThat(!(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkSizechart"), pdpPage)), 
					"Size chart link should not display for the furniture", 
					"Size chart link is not getting displayed for the furniture", 
					"Size chart link is getting displayed for the furniture", driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart!", driver);
			
			//Step 5 - Navigate to Product PLP
			PlpPage plpPage = homePage.headers.navigateTo(level1);
			Log.message(i++ + ". nvigated to PLP!", driver);
						
			plpPage.scrollTogetStickyAndBacktoTopButton();
			Log.message(i++ + ". Scrolled down to get sticky header!", driver);
			
			if(Utils.isDesktop()){
			Log.softAssertThat(plpPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "stickyGlobalNavigation", "divRefinement", plpPage), 
					"Category should be displayed above refinement", 
					"Category is displaying above refinement", 
					"Category is not displaying above refinement", driver);
			}else{
				Log.softAssertThat(homePage.headers.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "fixedHeaderMobTab", "stckyRefinement", homePage.headers), 
						"Stiky header should be displayed above refinement", 
						"Stiky header is displaying above refinement", 
						"Stiky header is not displaying above refinement", driver);
			}
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayedWithoutScrolling(Arrays.asList("backToTop"), plpPage), 
					"Back to top should be displayed", 
					"Back to top is getting displayed", 
					"Back to top is not getting displayed", driver);
			
			//step 6 - Navigate to Product PDP
			
			pdpPage = homePage.headers.navigateToPDP(prodHeavyItem);
			Log.message(i++ + ". Navigated to Pdp!", driver);
			
			pdpPage.addToBagCloseOverlay();
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("divItemId"), pdpPage), 
					"Item id should be displayed", 
					"Item id is getting displayed", 
					"Item id is not getting displayed", driver);
			
			//Step 7 - Navigate to the cart page
			
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Cart page!", driver);
			
			String[] ItemIds = {productID, prodHeavyItem};
			
			Log.softAssertThat(shoppingBagPg.verifyProductsAddedToCart(ItemIds), 
					"Product should be present in shopping bag page", 
					"Product is present in shopping bag page", 
					"Product is not present in shopping bag page", driver);
			
			
			CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to checkout Page.", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("btnShippingContinue"), checkoutPg), 
					"The system should navigate the user to checkout page - Step 1", 
					"The system is navigate the user to checkout page - Step 1", 
					"The system is not navigate the user to checkout page - Step 1", driver);

			Log.softAssertThat(checkoutPg.getNumberOfShippingMethodsAvailable()==1,
					"Only Standard Delivery option alone should be displayed in Shipping Address page.",
					"Only Standard Delivery option alone should be displayed in Shipping Address page.",
					"Only Standard Delivery option alone should be displayed in Shipping Address page.", driver);
			
			checkoutPg.enterAddress1("Po 1123");
			Log.message(i++ + ". Typed Po Box address on Address1", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("divPoBoxBlockOverlay"), checkoutPg), 
					"Po Box Overlay should not displayed", 
					"Po Box Overlay is not getting displayed", 
					"Po Box Overlay is getting displayed", driver);
			
			checkoutPg.enterAddress1("Po Box 1123");
			Log.message(i++ + ". Typed Po Box address on Address1", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("divPoBoxBlockOverlay"), checkoutPg), 
					"Po Box Overlay should get displayed", 
					"Po Box Overlay is getting displayed", 
					"Po Box Overlay is not getting displayed", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("btnPoBoxBlockOverlayEditShipAdd"), checkoutPg), 
					"Po Box Overlay should have Edit shipping address button", 
					"Po Box Overlay is having Edit shipping address button", 
					"Po Box Overlay is not having Edit shipping address button", driver);
			
			checkoutPg.clickEditInPORestrictedOverlay();
			Log.message(i++ + ". Clicked on PO Box item edit button", driver);
			
			checkoutPg.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", ShippingMethod.Standard, checkoutAdd);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			//Step 10 - Navigate to Checkout page - step 2
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
			
			checkoutPg.continueToPaymentByChoosingOriginalAddress(true);
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			checkoutPg.ApplyRewardCertificate(rewardCertificate);
			Log.message(i++ + ". Applied valid reward certificate", driver);
			
			if(!(checkoutPg.getRemainingOrderAmount() == 00.00)) {
				checkoutPg.applyGiftCardByValue(TestGiftCardValue.$100);
				Log.message(i++ + ". Applied 1st gift card", driver);
			}
			
			if(!(checkoutPg.getRemainingOrderAmount() == 00.00)) {
				checkoutPg.applyGiftCardByValue(TestGiftCardValue.$500);
				Log.message(i++ + ". Applied 2nd gift card", driver);
			}	
			
			if(!(checkoutPg.getRemainingOrderAmount() == 00.00)) {
				Log.fail("Remainaing amount is not zero so can't place order", driver);
			} else {
				checkoutPg.clickOnPaymentDetailsContinueBtn();
				Log.message(i++ + ". Continued to Review & Place Order", driver);
				
				HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
				
				String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
				
				String txtSurchargeMessage = Utils.isMobile() ? "txtSurchargeMob" : "txtSurchargeDeskAndTab";
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList(txtSurchargeMessage),checkoutPg), 
						"Product info should have 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", 
						"Product info is having 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", 
						"Product info is not having 'Additional Shipping & Handling Charges of $25.00 will apply for standard shipping' message", driver);
				
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
				
				//Step 11 - Navigate to Checkout page - step 3
				
				OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
				Log.message(i++ + ". Order Placed successfully", driver);
				
				//Step 12 - Order confirmation page
				
				Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()), 
						"Order number is generated automatically when order placed",
						"Order is placed and order number generated",
						"Order number is not generated", driver);
				
				Log.softAssertThat(receipt.checkEnteredShippingAddressReflectedInOrderReceipt(checkoutAdd), 
						"Same shipping address should display in the receipt which is entered in checkout shipping detail", 
						"Same shipping address is displaying", 
						"Different shipping address is displaying", driver);
				
				Log.softAssertThat(receipt.checkEnteredBillingAddressReflectedInOrderReceipt(checkoutAdd), 
						"Same billing address should display in the receipt which is entered in checkout billing detail", 
						"Same billing address is displaying", 
						"Different billing address is displaying", driver);
				
				Log.softAssertThat(receipt.elementLayer.verifyTextContains("lblOrderReceipt", username, receipt), 
						"The order receipt should send to the entered mail ID", 
						"The order receipt is send to the entered mail ID", 
						"The order receipt is not send to the entered mail ID", driver);
				
				Log.softAssertThat(receipt.elementLayer.verifyElementDisplayed(Arrays.asList("lnkPrint"), receipt), 
						"The user can able to take print out of the order receipt", 
						"The user can able to print the order receipt", 
						"The user cannot able to print the order receipt", driver);
				
				HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
				
				Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
						"The product added should be present in the receipt", 
						"The product added is present in the receipt", 
						"The product added is not present in the receipt", driver);
				
				String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
				
				Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
						"The order total should display correctly", 
						"The order total is displaying correctly", 
						"The order total is not displaying correctly", driver);
			}
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24543

