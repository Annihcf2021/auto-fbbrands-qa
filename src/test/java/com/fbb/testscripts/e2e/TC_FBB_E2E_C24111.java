package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.ordering.GuestOrderStatusLandingPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24111 extends BaseTest{
	
	private static EnvironmentPropertiesReader demandware= EnvironmentPropertiesReader.getInstance("demandware");
	private static EnvironmentPropertiesReader account= EnvironmentPropertiesReader.getInstance("accounts");
		
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24111(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{
			String searchText = TestData.get("product_search_terms").split("\\|")[0];
			String searchSecText = TestData.get("product_search_terms").split("\\|")[2];
			String poRestrictedProductID = TestData.get("prd_po-box-restricted");
			String errorColor= TestData.get("error_color_2");
			String userEMailId = AccountUtils.generateEmail(driver);
			String checkoutAdd = "taxless_address";
			String payment = "card_Visa";
			String poBoxMsg = demandware.get("PoBoxMsg");
			String inValidEmail = account.get("Invalid_format_email");
			
			
			
			//Step 1 - Navigate to the website
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Click on search field in the header
			
			homePage.headers.typeTextInSearchField(searchText);
			Log.message(i++ + ". Typed in the Search Field!", driver);
			
			SearchResultPage searchResultPg = homePage.headers.searchProductKeyword(searchText);
			Log.message(i++ + ". Navigated to Search Result Page!", driver);
			
			//Step 3 - Navigate to search result page
			searchResultPg.secondarySearchProductKeyword(searchSecText);
			Log.message(i++ + ". Search using Secondary Search Field!", driver);
			
			Log.softAssertThat(searchResultPg.elementLayer.verifyElementDisplayed(Arrays.asList("sectionSearchResult"), searchResultPg), 
					"Search result page should be displayed", 
					"Search result page is getting displayed", 
					"Search result page is not getting displayed", driver);
			
			Log.softAssertThat(searchResultPg.elementLayer.verifyAttributeForElement("inpSecondrySearch", "value", searchSecText, searchResultPg)
							&& searchResultPg.getSearchResultCount() > 0, 
					"Search result in secondary search textbox page should be displayed", 
					"Search text in secondary search textbox is returning value", 
					"Search text in secondary search textbox is not returning value", driver);
			
			//Step 4 - Navigate to Product detail page
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(poRestrictedProductID);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementTextContains("lblProductInfo", poBoxMsg, pdpPage), 
					"The product should have PO box excluded message", 
					"The product is having PO box excluded message", 
					"The product is not having PO box excluded message", driver);
			
			pdpPage.selectColor();
			Log.message(i++ + ". Color Selected", driver);

			//Step 4 - Navigate to Product detail page
			pdpPage.selectRegularOrSplitSize();
			Log.message(i++ + ". Size Selected", driver);
			
			if (pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("selectedShoeSizeValue"), pdpPage)) {
				Log.softAssertThat(pdpPage.elementLayer.verifyTxtElementsNotEmpty(Arrays.asList("selectedShoeSizeValue"), pdpPage),
						"Clicking on size should display the color name", 
						"Selected size name is displaying", 
						"Selected size name is not displaying", driver);
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyTxtElementsNotEmpty(Arrays.asList("selectedSizeLabel"), pdpPage),
						"Clicking on size should display the color name", 
						"Selected size name is displaying", 
						"Selected size name is not displaying", driver);
			}
			
			pdpPage.selectAllSwatches();
			Log.message(i++ + ". All swatches are selected", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Product Added to cart", driver);

			//Step 5 -Navigate to the cart page
			
			ShoppingBagPage shoppingBagPg = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);

			SignIn signIn = (SignIn) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			//-------Step 6 - Navigate to the checkout sign in page-----------
			CheckoutPage checkoutPg = (CheckoutPage) signIn.clickSignInButton();
			
			checkoutPg.enterInvalidGuestUserEmail(inValidEmail);
			Log.message(i++ +". Entered invalid email address", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("lblGuestEmailError"), checkoutPg) 
							&& checkoutPg.elementLayer.verifyElementColor("lblGuestEmailError", errorColor, checkoutPg), 
					"Email field placeholder text will become red color and the message should display as 'Please enter a valid email.'", 		
					"Email field placeholder text is red color and the message is displayed as 'Please enter a valid email.'", 
					"Email field placeholder text is not red color and the message not displayed as 'Please enter a valid email.'", driver);
			
			checkoutPg.checkNewsLetterSignUp("Uncheck");
			
			checkoutPg.continueToShipping(userEMailId);
			Log.message(i++ + ". Navigated to Shipping section", driver);

			// Step 9 - Add address without "PO BOX" in Shipping address section
			checkoutPg.enterAddress1("123 address one");
			Log.message(i++ + ". Typed on Address1", driver);
			
			checkoutPg.enterFirstName("John");
			Log.message(i++ + ". Typed on First name", driver);
			
			checkoutPg.mouseHoverShippingContinue();
			Log.message(i++ + ". Mouse hovered on Continue button", driver);

			// Step 7 -Navigate to Checkout page - step 1 , Enter PO BOX in Shipping address line 1 text box.
			checkoutPg.enterAddress1("Po Box 1123");
			Log.message(i++ + ". Typed Po Box address on Address1", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("divPoBoxBlockOverlay"), checkoutPg), 
					"Po Box Overlay should get displayed", 
					"Po Box Overlay is getting displayed", 
					"Po Box Overlay is not getting displayed", driver);
			
			// Step 8 - Click on "Edit Shipping Address" button from PO BOX exception overlay
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("btnPoBoxBlockOverlayEditShipAdd"), checkoutPg), 
					"Po Box Overlay should have Edit shipping address button", 
					"Po Box Overlay is having Edit shipping address button", 
					"Po Box Overlay is not having Edit shipping address button", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnPoBoxBlockOverlayRemoveItems"), checkoutPg), 
					"Po Box Overlay should not have Remove Items button", 
					"Po Box Overlay is not having Remove Items button", 
					"Po Box Overlay is having Remove Items button", driver);
			
			checkoutPg.clickEditInPORestrictedOverlay();
			Log.message(i++ + ". Edit Shipping address button clicked!", driver);
			
			checkoutPg.fillingShippingDetailsAsGuest("NO", checkoutAdd, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			// Step 10 - Navigate to Checkout page - step 2
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);

			//Invalid values to check the error
			checkoutPg.selectCardType("Visa");
			checkoutPg.setCardHolderNameInPayment("User");
			checkoutPg.setCardNumberInPayment("1111111111111111");
			checkoutPg.selectExpiryMonth("December");
			
			checkoutPg.clickOrderSummaryContinue();
			Log.message(i++ +". Clicked continue button", driver);

			Log.softAssertThat(checkoutPg.elementLayer.verifyCssPropertyForElement("fldTxtCvv", "outline", "solid", checkoutPg) &&
					checkoutPg.elementLayer.verifyElementColor("lblTxtCvvNoError", errorColor, checkoutPg)&&
					checkoutPg.elementLayer.verifyCssPropertyForElement("selectExpYear", "outline", "solid", checkoutPg),
					"other fields displaying as red border & placeholder displaying as red color.", 
					"other fields displaying as red border & placeholder displaying as red color.",
					"other fields displaying as red border & placeholder displaying as red color.", driver);
			
			checkoutPg.fillingCardDetails1(payment, false, false);
			Log.message(i++ + ". Card Details filling Successfully", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			// Step 11 - Navigate to Checkout page - step 3
			
			Log.softAssertThat(checkoutPg.comparePaymentBeforeOrderWithEnteredPaymentMethod(payment), 
					"The system should keep the payment details", 
					"Payment method is saved and displayed", 
					"Payment method is not saved correctly", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
								
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			// Step 12 - Order confirmation page
			
			OrderConfirmationPage receipt = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
					
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			String orderNumber = receipt.getOrderNumber();
			String orderZip = receipt.getOrderZipCode();
			
			Object obj = receipt.clickOnRecommendationProductNameLinkByIndex(0);
			Log.message(i++ + ". Clicked on recommendation product name link from order confirmation page", driver);
			
			Log.softAssertThat(obj.getClass().getName().contains("PdpPage"),
					"After clicking the recommendation product name link, User should redirects to the PDP page",
					"After clicking the recommendation product name link, User is redirected to the PDP page",
					"After clicking the recommendation product name link, User not redirected to the PDP page", driver);
			
			homePage.footers.navigateToOrderStatus();
			Log.message(i++ + ". Clicked on order history link from footer section", driver);
			
			signIn = new SignIn(driver);
			Log.message(i++ + ". Navigated to order sign in page", driver);
			
			signIn.fillOrderFields(orderNumber, userEMailId, orderZip);
			Log.message(i++ + ". Filled guest order details", driver);
			
			GuestOrderStatusLandingPage guestOrderStatusLandingPage = signIn.clickOnSubmitButton();
			Log.message(i++ + ". Clicked on order lookup button", driver);
			
			guestOrderStatusLandingPage.clickOnBuyAgainLinkByIndex(0);
			Log.message(i++ + ". Clicked on 'Buy Again' link from order guest order details page", driver);
			
			Log.softAssertThat(obj.getClass().getName().contains("PdpPage"),
					"After clicking the 'Buy Again' link, User should redirects to the PDP page",
					"After clicking the 'Buy Again' link, User is redirected to the PDP page",
					"After clicking the 'Buy Again' link, User not redirected to the PDP page", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24111
