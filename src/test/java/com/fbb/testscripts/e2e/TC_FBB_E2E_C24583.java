package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.EnlargeViewPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OfferPage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24583 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24583(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			//Step 1 - Navigate to the website			
			String shoppersClubEmail= accountData.get("credentialShoppersClub").split("\\|")[0];
			String password = accountData.get("password_global");
			String credential = shoppersClubEmail + "|" + password;
			String checkoutAdd = "address_withtax";
			String billingAdd = "taxless_address";
			String payment = "card_Visa";
			String prdDropship = TestData.get("prd_dropship");
			String level1Cat = TestData.get("level-1");
			String addressDetails = checkoutData.get(checkoutAdd);
			
			{
				GlobalNavigation.registerNewUserAddressPayment(driver, checkoutAdd +"|"+billingAdd, payment, credential);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//step 2 - Click on sign in link from the header section
			
			homePage.headers.navigateToMyAccount(shoppersClubEmail, password);
			Log.message(i++ + ". Navigated to My account!", driver);
			
			if(Utils.isDesktop()) {
				homePage.headers.mouseHoverOnCategoryByCategoryName(level1Cat);
				
				Log.softAssertThat(homePage.headers.elementLayer.verifyElementDisplayed(Arrays.asList("currentLevel2"), homePage.headers), 
						"Category flyout should dispayed", 
						"Category flyout is dispayed", 
						"Category flyout is not dispayed", driver);
			}
			
			//Step 3 - From the Global navigation click on any root category
			
			PlpPage plpPage = homePage.headers.navigateTo(level1Cat);
			
			BrowserActions.scrollToBottomOfPage(driver);
			
			Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), plpPage), 
					"PLP page should display", 
					"PLP page is displayed", 
					"PLP page is not displayed", driver);
			
			if(Utils.isDesktop()) {
				Log.softAssertThat(plpPage.elementLayer.verifyElementDisplayed(Arrays.asList("categoryRefinement"), plpPage), 
						"Category Refinement should display", 
						"Category Refinement is displayed", 
						"Category Refinement is not displayed", driver);
			}
			
			//Step 4 - Navigates to PDP
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdDropship);
			Log.message(i++ + ". Navigated to PDP page!", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lnkShippingExpand"), pdpPage), 
					"Shipping and returns section should be displayed", 
					"Shipping and returns section is displayed", 
					"Shipping and returns section is not displayed", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementTextContains("txtProdInfos", "third party",  pdpPage),
					"'This is a third party item' message should be displayed!",
					"'This is a third party item' message is displayed!",
					"'This is a third party item' message is not displayed!", driver);
	
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart!", driver);
			
			if(Utils.isDesktop() || Utils.isTablet()) {
				EnlargeViewPage enlargeModal = pdpPage.clickOnEnlargeButton();
				Log.message(i++ + ". Clicked on Enlarge button.", driver);
				
				Log.softAssertThat(enlargeModal.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), enlargeModal), 
						"Clicking on Enlarge button, a pop-up window will display.", 
						"Clicking on Enlarge button, a pop-up window will display.", 
						"Clicking on Enlarge button, a pop-up window will display.", driver);
				
				if (enlargeModal.elementLayer.verifyElementDisplayed(Arrays.asList("nextImageArrowEnlarge" ,"prevImageArrowEnlarge"), enlargeModal)) {
					int getSlickSlideIndexBefore = enlargeModal.getAltImgSlickIndex();
					
					enlargeModal.clickOnNextImageArrow();
					Log.message(i++ + ". Clicked on Right Arrow.", driver);
					
					int getSlickSlideIndexAfer = enlargeModal.getAltImgSlickIndex();
					
					Log.softAssertThat(getSlickSlideIndexAfer == (getSlickSlideIndexBefore + 1), 
							"Click on right arrow, should sequece the next image", 
							"Click on right arrow, should sequece the next image", 
							"Click on right arrow, should sequece the next image", driver);
		
					enlargeModal.clickOnPrevImageArrow();
					Log.message(i++ + ". Clicked on left Arrow.", driver);
					
					getSlickSlideIndexAfer = enlargeModal.getAltImgSlickIndex();
					
					Log.softAssertThat(getSlickSlideIndexAfer == getSlickSlideIndexBefore, 
							"Click on left arrow, should sequece the next image", 
							"Click on left arrow, should sequece the next image", 
							"Click on left arrow, should sequece the next image", driver);
				} else {
					Log.reference("Previous and Next arrows in enalrge preview are disabled in SM-6015. Arrow will remain disabled until PDP redesign, SM-5738.");
				}
				enlargeModal.closeEnlargeViewModal();
				Log.message(i++ +". Closed enlarge modal", driver);
			}
			
			if(Utils.isMobile()) {
				if(pdpPage.isSizeChartApplicable()) {
					Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElements(driver, "sectionColorAttribute", "lnkSizechart", pdpPage), 
							"The Size Chart link should be displayed below color attribute!",
							"The Size Chart link is displayed below color attribute",
							"The Size Chart link is not displayed below color attribute", driver);
					
					pdpPage.clickSizeChartLink();
					Log.message(i++ + ". Clicked on Size Chart link!", driver);
	
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("modalSizechart"), pdpPage),
							"The Size Chart modal should be displayed!",
							"The Size Chart modal is displayed!",
							"The Size Chart modal is not displayed!", driver);
					
					pdpPage.closeSizeChartModalPopup();
					Log.message(i++ + ". Closed size chart modal.", driver);
				} else {
					Log.failsoft("Test Data not applicable for Size Chart validations.");
				}
			}
			
			//Step 5 - Navigates to the offer page
			
			OfferPage offerPg = homePage.headers.navigateToOfferPage();
			Log.message(i++ + ". Navigated To Offer Page!", driver);
			
			String prdNam = offerPg.getProductIdByIndex(1);
			
			if(Utils.isDesktop()) { 
				Log.softAssertThat(offerPg.elementLayer.verifyElementDisplayed(Arrays.asList("recommendationSection"), offerPg),
						"Recommendation section should be displayed!",
						"Recommendation section is displayed!",
						"Recommendation section is not displayed!", driver);
				
				QuickShop quickShp = offerPg.clickOnQuickShopByIndex(1);
				Log.message(i++ + ". Quick Shop opened!", driver);
				
				quickShp.selectColor();
				Log.message(i++ + ". Selected color in color swatch!", driver);
				
				quickShp.selectSize();
				Log.message(i++ + ". Size swatch selected", driver);

				quickShp.selectAllSwatches();
				
				quickShp.addToBag();
				Log.message(i++ + ". Product added to cart!", driver);
				
			} else {
				pdpPage = offerPg.navigateToPdpByIndex(1);
				Log.message(i++ + ". Navigated to PDP page!", driver);
				
				pdpPage.addToBagKeepOverlay();
				Log.message(i++ + ". Product added to cart!", driver);
				if(!pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("mdlMiniCartOverLay"), pdpPage)) {
					Log.ticket("SC-5885");
				}
			}
			
			//Step 6 - Navigate to the cart page
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated To Shopping Bag page", driver);
			
			String[] nam = {prdNam , prdDropship};
			
			Log.softAssertThat(cartPage.verifyProductsAddedToCart(nam), 
					"Product should added in the cart page", 
					"Product is added in the cart page", 
					"Product is not added in the cart page", driver);
			
			Log.softAssertThat(cartPage.elementLayer.verifyElementTextContains("divShippingMessage", "Standard", cartPage), 
					"Verify the shipping method is displayed as 'Standard Shipping'", 
					"The shipping method is displayed as 'Standard Shipping'", 
					"The shipping method is not displayed as 'Standard Shipping'", driver);
			
			double shippingPrice = cartPage.getShippingPrice();
			
			if(shippingPrice - cartPage.getOrderShippingDiscount() > 2.99) {
				shippingPrice = cartPage.getShippingMethodPrice();
			}
			
			Log.softAssertThat(shippingPrice - cartPage.getOrderShippingDiscount() == 2.99, 
					"Verify the shipping method price is displayed as '2.99'", 
					"The shipping method price is displayed as '2.99'", 
					"The shipping method price is not displayed as '2.99'", driver);
			
			//Step 7 - Navigate to Checkout page - step 3
			
			CheckoutPage checkoutPg = (CheckoutPage) cartPage.navigateToCheckout();
			
			checkoutPg.clickBillingAddressEdit();
			Log.message(i++ + ". Edit Billing address", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementTextContains("txtBillingFirstNameFld", addressDetails.split("\\|")[7], checkoutPg)
							&& checkoutPg.elementLayer.verifyElementTextContains("txtBillingAddressFld", addressDetails.split("\\|")[0], checkoutPg), 
					"Shipping address 'first name' and 'address line 1' should refect", 
					"Shipping address 'first name' and 'address line 1' is refect", 
					"Shipping address 'first name' and 'address line 1' is not refect", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			//Step 8 - Check out page - step 2
			
			checkoutPg.selectValueFromSavedAddressesDropdownByIndexInBillingDetails(2);
			Log.message(i++ + ". Selected saved address from dropdown.", driver);
			
			checkoutPg.selectSavedCard();
			Log.message(i++ + ". Selected saved payment method.", driver);
			
			checkoutPg.enterCVV("123");
			Log.message(i++ + ". Typed on CVV", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			//step 9 - Checkout page - step 3
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//step 10 - Order confirmation page
			
			Log.softAssertThat(!(receipt.getOrderNumber().isEmpty()), 
					"Order number is generated automatically when order placed",
					"Order is placed and order number generated",
					"Order number is not generated", driver);
			
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24583
