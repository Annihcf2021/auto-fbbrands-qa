package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.headers.HamburgerMenu;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24574 extends BaseTest {
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");

	@Test(groups = { "pdp", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24574(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{
			String emailId = AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global"); 
			String credential = emailId + "|" + password;
			String searchKey = TestData.get("product_search_terms").split("\\|")[0];
			String level1 = TestData.get("level-2").split("\\|")[0];
			String level2 = TestData.get("level-2").split("\\|")[1];
			String[] prdWithNoPromo = TestData.get("prd_with_nopromotion").split("\\|");
			String maxCartQtyError = demandwareData.get("CartMaxQtyReachedMsg");
			int cartTargetQuantity = 38;
			
			{	
				GlobalNavigation.registerNewUser(driver, 0, 0,  credential);	
			}
			
			//Step-1 Navigate to the web site
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			//Step-2 Click on "Sign in" link from the header
			SignIn signIn;
			if(!Utils.isMobile()){
				signIn = homePage.headers.navigateToSignInPage();
				Log.message(i++ +". Clicked sign in link from header", driver);
			}else{
				HamburgerMenu hMenu = new HamburgerMenu(driver) ;
				homePage.headers.openCloseHamburgerMenu("open");
				Log.message(i++ +". Opened hamburger menu", driver);
				signIn = hMenu.navigateToSignIn();
				Log.message(i++ +". Navigated to Sign in page", driver);
			}
			
			signIn.navigateToMyAccount(emailId, password);
			Log.message(i++ +". Logged in for email: " + emailId, driver);
			
			//Step-3 Verify the cart page
			
			String cartCount = homePage.headers.getMiniCartCount();
			 
			if(cartCount.equalsIgnoreCase("0")){
				Log.message(i++ +". Account has no products", driver);
			}else{
				GlobalNavigation.RemoveAllProducts(driver);
				Log.message(i++ +". Removed all carted products & Cart is empty", driver);
			}
			
			//Step-4 Verify the functionality of the search field.
			homePage.headers.navigateToSLP(searchKey);
			Log.message(i++ +". Entered search term and navigated to search results page for: " + searchKey, driver);
			
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdWithNoPromo[0]);
			Log.message(i++ +". Navigated to PDP: " + prdWithNoPromo[0], driver);
			 
			int cartQty = 0, qty = 10;
			boolean cartBelowThreshold = true;
			while (cartBelowThreshold) {
				pdpPage.selectAllSwatches();
				Log.message(i++ +". Selected product swtches.", driver);

				String drpQty = pdpPage.selectQuantity(qty, true);
				Log.message(i++ +". Selected quantity: " + drpQty, driver);

				pdpPage.clickAddProductToBag();
				Log.message(i++ +". Clicked Add to bag button from PDP", driver);
				
				if(pdpPage.elementLayer.isElementsDisplayed(Arrays.asList("mdlMiniCartOverLay"), pdpPage)) {
					pdpPage.clickOnContinueShoppingInMCOverlay();
					Log.message(i++ +". Clicked Continue shopping button from Add to bag overlay", driver);	
				} else {
					Log.ticket("SC-5885");
				}

				pdpPage.clickInStockRecommendationPrdImage();
				Log.message(i++ +". Clicked Recomendation section. Navigate to:: " + pdpPage.getProductName(), driver);

				cartQty = StringUtils.getNumberInString(homePage.headers.getMiniCartCount());
				if (cartQty >= 30) {
					cartBelowThreshold = false;
				} else if ((30 - cartQty) > 10) {
					qty = 10;
				} else {
					qty = 30 - cartQty;
				}
			}
				
			pdpPage.selectAllSwatches();
			pdpPage.selectQuantity(cartTargetQuantity - cartQty);
			pdpPage.addToBagKeepOverlay();
			Log.message(i++ +". Clicked Add to bag button from PDP", driver);
			
			if(pdpPage.elementLayer.isElementsDisplayed(Arrays.asList("mdlMiniCartOverLay"), pdpPage)) {
				pdpPage.clickOnContinueShoppingInMCOverlay();
				Log.message(i++ +". Clicked Continue shopping button from Add to bag overlay", driver);	
			} else {
				Log.ticket("SC-5885");
			}
			
			//Step-5 Verify mini cart			
			Log.softAssertThat(homePage.headers.getMiniCartCount().contains(cartTargetQuantity + ""),
				 "In the header section mini cart should displaying total item number as " + cartTargetQuantity,
				 "In the header section mini cart is displaying total item number as " + cartTargetQuantity,
				 "In the header section mini cart is not displaying total item number as " + cartTargetQuantity, driver);	
				
			ShoppingBagPage cart = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ +". Page navigated to cart page",driver);
			
			int actualCartQuantity = cart.getTotalQtyInCart();
			Log.softAssertThat(actualCartQuantity == cartTargetQuantity,
				cartTargetQuantity + " qty should display on the cart page", 
				cartTargetQuantity + " qty is displaying on the cart page",
				actualCartQuantity + " qty is displaying on the cart page", driver);
			
			cart.clickInStockRecommendationPrdImage();
			Log.message(i++ +". Clicked recommendation product from cart page", driver);
			
			//Step-6 Navigates to the PDP
			int productRetryCount = 2;
			while(productRetryCount != 0) {
				pdpPage.selectMinimumQuantityVariation(3);
				Log.message(i++ +". Selected product swatches.", driver);
				try {
					pdpPage.selectQuantity(3);
					Log.message(i++ +". Selected quantity: 3", driver);
					productRetryCount = 0;
				} catch(Exception e) {
					productRetryCount--;
					pdpPage.clickInStockRecommendationPrdImage();
					Log.message(i++ +". Clicked recommendation product again from pdp page: ", driver);
				}
			}
			
			pdpPage.clickAddToBagForErrorMsg();
			Log.message(i++ +". Clicked add to bag button from PDP", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("lblMaxCartError"), pdpPage) && 
					pdpPage.elementLayer.verifyCssPropertyForElement("lblMaxCartError", "display", "block", pdpPage), 
					"If the user attempts to add more quantity than is 40, Error message should be displayed",
					"Error message is displayed",
					"Error message is not displayed", driver);  
	
			//Step-7 Navigate to PLP only Desktop
			if(Utils.isDesktop()){
				PlpPage plpPage = homePage.headers.navigateTo(level1,level2);;
				Log.message(i++ +". Navigated to SLP", driver);
				
				QuickShop quickShop = plpPage.clickOnQuickShop();
				Log.message(i++ +". Clicked quickshop button from PLP", driver);
				
				quickShop.selectMinimumQuantityVariation(5);
				Log.message(i++ +". Selected product swtches.", driver);

				quickShop.selectQty("5");
				Log.message(i++ +". Selected quantity: 5", driver);
				
				quickShop.addToBag();
				Log.message(i++ +". Clicked add to bag button", driver);
				
				Log.softAssertThat(quickShop.elementLayer.verifyElementDisplayed(Arrays.asList("lblcartError"), quickShop) && 
						quickShop.elementLayer.verifyCssPropertyForElement("lblcartError", "display", "block", quickShop), 
						"If the user attempts to add more quantity than is 40, Error message should be displayed",
						"Error message is displayed",
						"Error message is not displayed", driver);			
				
				quickShop.closeQuickShopOverlay();
				Log.message(i++ +". Closed quick shop overlay", driver);
			}
			cart = homePage.headers.navigateToShoppingBagPage();
			HashSet<String> prdNames = cart.getCartItemNameList();
			
			//Step-8 Log off from the account & navigate to the cart page
			homePage.headers.signOut();
			Log.message(i++ + ". Clicked on sign out!", driver);
			
			//Step-9 Now as a guest user navigate to the PDP
			homePage.headers.navigateToSLP(searchKey);
			Log.message(i++ +". Entered search term and navigated to search results page", driver);
			
			pdpPage = homePage.headers.navigateToPDP(prdWithNoPromo[1]);
			
			pdpPage.selectAllSwatches();
			Log.message(i++ +". Selected product swtches.", driver);

			String selectedQty = pdpPage.selectQuantity(5, true);
			Log.message(i++ +". Selected quantity: " + selectedQty, driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ +" Clicked add to bag button");
			
			pdpPage.clickOnContinueShoppingInMCOverlay();
			Log.message(i++ +". Clicked Continue shopping button from Add to bag overlay", driver);
			prdNames.add(pdpPage.getProductName().trim().toLowerCase());
			
			//Step-10 From the header login
			signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ +". Navigated to Sign in page", driver);
			
			signIn.navigateToMyAccount(emailId, password);
			Log.message(i++ +". Provide a valid user ID & password, Log in as a registered user ", driver);
			
			homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ +". Navigated to cart page", driver);
			
			Log.softAssertThat(cart.verifyMergedCartContainsProductList(cart.getCartItemNameList(), prdNames)
							&& cart.elementLayer.verifyElementDisplayed(Arrays.asList("txtQtyMergeMsg"), cart),
					"The system should merge all the products and cart merge message should displayed",
					"The system is merged all the products and cart merge message is displayed",
					"The system is not merged all the products and cart merge message is not displayed", driver);
			
			Log.softAssertThat(cart.elementLayer.verifyElementTextContains("txtQtyMergeMsg", maxCartQtyError, cart) &&
					cart.elementLayer.verifyVerticalAllignmentOfElements(driver, "headerShoppingBag", "txtQtyMergeMsg", cart),
					"The 'Cart Maximum Quantity Reached' message should be displayed below the Cart Heading section",
					"The 'Cart Maximum Quantity Reached' message is displayed below the Cart Heading section", 
					"The 'Cart Maximum Quantity Reached' message is not displayed below the Cart Heading section", driver);
			
			Log.softAssertThat(cart.elementLayer.verifyAttributeForElement("btnCheckoutNow", "disabled", "true", cart), 
					"Checkout now button should be disabled.",
					"Add to Bag button disabled!",
					"Add to bag Button not disabled.", driver);
			
			if(cart.removeProductstoMatchqty(40)){
				Log.message(i++ +". Some Products are removed to match qty 40", driver);
			}else{
				cart.updateProductstoMatchqty(40);
				Log.message(i++ +". Product qty is updated to match qty 40", driver);
			}
			Log.softAssertThat(!cart.elementLayer.verifyAttributeForElement("btnCheckoutNow", "disabled", "true", cart),
					"checkout now button should become enable",
					"checkout now button is become enable", 
					"checkout now button is not become enable", driver);	
			
			Log.testCaseResult();

		} // try
		catch (Exception e) {
			Log.exception(e, driver);
		} // catch
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		} // finally
	}//M1_FBB_E2E_C24575
}//TC_FBB_E2E_C24575
