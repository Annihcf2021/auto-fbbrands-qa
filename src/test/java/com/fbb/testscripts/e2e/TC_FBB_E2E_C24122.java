package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24122 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandWareProperty = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24122(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String checkoutAdd = "address_withtax";
			String payment = "card_Visa";
			String payment1 = "cards_5";
			String validEmail = accountData.get("valid_registered_email");
			String cartHeading = demandWareProperty.get("cart_heading");
			String emptyCartMessage = demandWareProperty.get("empty_cart_message");
			if(validEmail.contains("|")){
				validEmail = validEmail.split("\\|")[0];
			}
			
			{
				HashMap<String, String> accountDetails = new HashMap<>();
				accountDetails.put(AddressesPage.class.getName(), checkoutAdd);
				accountDetails.put(PaymentMethodsPage.class.getName(), payment1+ "|" +payment);
				GlobalNavigation.setupTestUserAccount(driver, accountDetails, credential);
			}
			
			//Step 1 - Navigate to the website
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - Click on my bag link from the header & navigate to Empty cart page

			ShoppingBagPage shoppingBagPg = homePage.headers.clickOnBagLink();
			
			Log.softAssertThat(shoppingBagPg.verifyEmptyCart(), 
					"Page should be redirected to the empty cart page.", 
					"Page redirected to the empty cart page.", 
					"Page not redirected to the empty cart page.", driver);
			
			Log.message(i++ + ". Navigated to Empty cart Page!", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyTextEquals("emptyShoppingHeader", cartHeading, shoppingBagPg)
							&& shoppingBagPg.elementLayer.verifyTextEquals("emptyCartMsg", emptyCartMessage, shoppingBagPg),
					"'MY SHOPPING BAG' heading and 'Your shopping bag is empty' text should be displayed in the empty shopping bag page",
					"'MY SHOPPING BAG' heading and 'Your shopping bag is empty' text is displayed in the empty shopping bag page",
					"'MY SHOPPING BAG' heading and 'Your shopping bag is empty' text is not displayed in the empty shopping bag page", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "emptyCartMsg", "btnEmptyWhatsNew", shoppingBagPg), 
					"Below 'Your Shopping bag is empty' message 'Shop What's New' button should be shown", 
					"Below 'Your Shopping bag is empty' message 'Shop What's New' button is shown", 
					"Below 'Your Shopping bag is empty' message 'Shop What's New' button not shown", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "btnEmptyWhatsNew", "trendNowSlickContainer", shoppingBagPg), 
					"Below 'My Shopping bag' header, 'Trending Now' section should be shown", 
					"Below 'My Shopping bag' header, 'Trending Now' section is shown", 
					"Below 'My Shopping bag' header, 'Trending Now' section not shown", driver);			
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(
					Arrays.asList("prodImageRecommendationEmptyCart", "prodNameRecommendationEmptyCart", "prodPriceRecommendationEmptyCart"), shoppingBagPg), 
					"Product image and product details should be displayed in the Trending Now section.",
					"Product image and product details is displayed in the Trending Now section.",
					"Product image and product details is not displayed in the Trending Now section.", driver);
			
			//Step 3 - Navigate to Footer section and click on Gift card link & navigates to PDP

			PdpPage pdpPage = homePage.footers.navigateToPhysicalGiftCardPDP();
			Log.message(i++ + ". Navigated to gift card PDP page", driver);
			
			pdpPage.openCloseToolTipOverlay("open");
			Log.message(i++ + ". Terms and Condition tool tip overlay is opened", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("closeToolTipOverLay"), pdpPage), 
					"Terms and Condition overlay should displaying", 
					"Terms and Condition overlay is displaying", 
					"Terms and Condition overlay is not displaying", driver);
			
			String[] itemNames = {pdpPage.getProductName()};
			
			pdpPage.openCloseToolTipOverlay("close");
			Log.message(i++ + ". Terms and Condition tool tip overlay is closed", driver);
			
			pdpPage.selectGCSize(TestData.get("giftcard_Amount"));
			Log.message(i++ + ". Gift card amount selected", driver);
			
			Log.softAssertThat(StringUtils.getNumberInString(pdpPage.getSelectedGCAmount()) > 0, 
					"Amount should be selected", 
					"Amount is selected", 
					"Amount is not selected", driver);
			
			pdpPage.checkGCPersonalizedMessage("check");
			Log.message(i++ + ". 'Add a personalized message' check box checked", driver);
			
			//Step 4 - Navigate to the cart page
			
			pdpPage.clickAddToBagForErrorMsg();
			Log.message(i++ + ". Click add to bag button!", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("errPleaseSelectConfirmMail","errMessageNotFill","errToNotFill","errFromNotFill"), pdpPage), 
					"Error should get displayed when from, to and personel message is not entered", 
					"Error is getting displayed when from, to and personel message is not entered", 
					"Error is not getting displayed when from, to and personel message is not entered", driver);
			
			pdpPage.checkGCPersonalizedMessage("uncheck");
			Log.message(i++ + ". 'Add a personalized message' check box unchecked", driver);
			
			Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("txtAreaPersonalMsg","txtGCPersonalMsgTo","txtGCPersonalMsgFrom"), pdpPage), 
					"From, To and Message fields should not display when 'Add a personalized message' check box is unchecked", 
					"From, To and Message fields is not display when 'Add a personalized message' check box is unchecked", 
					"From, To and Message fields is display when 'Add a personalized message' check box is unchecked", driver);
			
			pdpPage.selectQty("2");
			Log.message(i++ + ". Quantity selected as 2!", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Click add to bag button!", driver);
			
			shoppingBagPg = pdpPage.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Click on checkout in add to bag overlay!", driver);
			
			Log.softAssertThat(shoppingBagPg.verifyProductsAddedToCartByProdName(itemNames), 
					"Product should added to cart", 
					"Product is added to cart", 
					"Product is not added to cart", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("productNameSection","productUnitPrice","txtproductId"), shoppingBagPg), 
					"Gift card name, Product id and Price alone should be displayed", 
					"Gift card name, Product id and Price alone is displayed", 
					"Gift card name, Product id and Price alone is not displayed", driver);
			
			Log.softAssertThat(shoppingBagPg.elementLayer.verifyElementDisplayed(Arrays.asList("divShippingOverlayToolTip"), shoppingBagPg), 
					"'Shipping and handling tool-tip' icon for the gift card should be displayed in cart summary section", 
					"'Shipping and handling tool-tip' icon for the gift card is displayed in cart summary section", 
					"'Shipping and handling tool-tip' icon for the gift card is not displayed in cart summary section", driver);
					
			shoppingBagPg.openShippingOverlayToolTip();
			Log.message(i++ + ". Shopping Overlay tool tip opened", driver);
			
			Log.softAssertThat(shoppingBagPg.verifyGiftCardFeeInToolTip(), 
					"'Gift card fee' cost should be calculated as per the quantity added in the cart",
					"'Gift card fee' cost is calculated as per the quantity added in the cart",
					"'Gift card fee' cost is not calculated as per the quantity added in the cart", driver);
			
			shoppingBagPg.clickOnShippingCostToolClose();
			Log.message(i++ + ". Shopping Overlay tool tip closed", driver);
			
			Object obj = shoppingBagPg.clickOnEditLink(0);
			
			if(Utils.isDesktop()) {
				QuickShop qsModal = (QuickShop) obj;
				
				qsModal.chkOrUnchkAddPersonalMsg(true);
				Log.message(i++ + ". 'Add a personalized message' check box checked", driver);
				
				qsModal.enterPersonalMessage("Personal Message");
				Log.message(i++ + ". Added Personal message!", driver);
				
				qsModal.enterToAddress(validEmail);
				Log.message(i++ + ". Added To address!", driver);
				
				qsModal.enterFromAddress(validEmail);
				Log.message(i++ + ". Added From address!", driver);
				
				qsModal.addToBag();
				Log.message(i++ + ". Updated the carted product!", driver);
			} else {
				pdpPage = (PdpPage) obj;
				pdpPage.checkGCPersonalizedMessage("check");
				Log.message(i++ + ". 'Add a personalized message' check box checked", driver);
				
				pdpPage.enterGCPersonalMessage("Personal Message");
				Log.message(i++ + ". Added Personal message!", driver);
				
				pdpPage.enterGCToAddress(validEmail);
				Log.message(i++ + ". Added To address!", driver);
				
				pdpPage.enterGCFromAddress(validEmail);
				Log.message(i++ + ". Added From address!", driver);
				
				pdpPage.clickOnUpdate();
				Log.message(i++ + ". Updated the carted product!", driver);
			}
			
			SignIn signin = (SignIn) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			//Step 5 - Navigate to the checkout sign in page
			
			CheckoutPage checkoutPg = (CheckoutPage)signin.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);
			
			checkoutPg.continueToShipping("validformat@gmail.com|test123@");
			Log.message(i++ + ". Valid unregistered username", driver);

			//Step 6 - Navigate to Checkout page - step 3
			
			checkoutPg.continueToShipping(credential);
			Log.message(i++ + ". Navigated to Shipping section", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);
			
			if(Utils.isDesktop()) {
				checkoutPg.mouseHoverPlaceOrder();
				Log.message(i++ + ". Mouse hovered on Continue button", driver);
			} else {
				checkoutPg.clickOnPlaceOrder();
				Log.message(i++ + ". Clicked on Continue button", driver);
			}		
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("divIncompleteFormWarning"), checkoutPg), 
					"The user should enter all the mandatory field, otherwise error message should be displayed.", 
					"Error messages displayed.", 
					"Error messages not displayed", driver);
			
			checkoutPg.selectSavedCardDetailsBasedOnIndex(2);
			Log.message(i++ + ". Card Details selected Successfully", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("fldTxtCvvNo"), checkoutPg), 
					"When selecting second card for payment the CVV text field should get displayed .", 
					"When selecting second card for payment the CVV text field is displayed.", 
					"When selecting second card for payment the CVV text field is not displayed", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			checkoutPg.selectSavedCardDetailsBasedOnIndex(1);
			Log.message(i++ + ". Card Details selected Successfully", driver);
			
			checkoutPg.enterTextInCardCvvField("123");
			Log.message(i++ + ". Typed on CVV", driver);
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 7 - Order confirmation page
						
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);

			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24122
