package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24811 extends BaseTest {

	EnvironmentPropertiesReader environmentPropertiesReader;

	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");

	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24811(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
			String userEMailIdAlwaysApprove = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountData.get("password_global");
			String credentialAlwaysApprove = userEMailIdAlwaysApprove +"|"+ password;
			
			String addressAlwaysApprove = "plcc_always_approve_address";
			String firstNameAlwaysApprove = checkoutData.get("plcc_always_approve_address").split("\\|")[7];
			String lastNameAlwaysApprove = checkoutData.get("plcc_always_approve_address").split("\\|")[8];
			
			String prdVariation = TestData.get("prd_variation");
			String prdVariation1 = TestData.get("prd_variation1");
			String cardType = "cards_2";
			
			{
				GlobalNavigation.registerNewUserWithUserDetail(driver, 0, 0, firstNameAlwaysApprove, lastNameAlwaysApprove, credentialAlwaysApprove);
				
				GlobalNavigation.addNewAddressToAccount(driver, addressAlwaysApprove, true, credentialAlwaysApprove);
			}

			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

			MyAccountPage myAccount = homePage.headers.navigateToMyAccount(userEMailIdAlwaysApprove, password);
			Log.message(i++ + ". Navigated to My Account page as : " + userEMailIdAlwaysApprove, driver);

			Log.softAssertThat(myAccount.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), myAccount),
					"User should be successfully created", "User is created", "User is not created", driver);

			// Step-3
			AddressesPage addressPg = myAccount.navigateToAddressPage();
			Log.message(i++ + ". Navigated to Address page!", driver);

			Log.softAssertThat(addressPg.getSavedAddressesCount() > 0, 
					"Address should be successfully created",
					"Address is created", "Address is not created", driver);

			// Step-4
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdVariation);
			Log.message(i++ + ". Navigated To PDP page : " + pdpPage.getProductName(), driver);

			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);

			homePage.headers.navigateToPDP(prdVariation1);
			Log.message(i++ + ". Navigated To PDP page : " + pdpPage.getProductName(), driver);

			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);

			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);

			CheckoutPage checkoutPg = (CheckoutPage) cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to checkout page", driver);

			if (checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkoutPg)) {
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("mdlPLCCApproval"), checkoutPg),
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed",
						"PLCC modal is not getting displayed", driver);

				checkoutPg.closePLCCOfferByNoThanks1();
				Log.message(i++ + ". Clicked No Thanks button from pre-approved modal", driver);
				
				checkoutPg.continueToPayment();
				Log.message(i++ + ". Continued to Payment Page", driver);

				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement", "divPaymentDetailsSection"), checkoutPg),
						"User will be taken to the payment method section of Step 2.",
						"User will be taken to the payment method section of Step 2.",
						"User will be taken to the payment method section of Step 2.", driver);

				Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "mdlPLCCRebuttal","paymentmethodSection", checkoutPg),
						"The acquisition rebuttal modal should be displayed above the Add New Credit Card form.",
						"The acquisition rebuttal modal is displayed above the Add New Credit Card form.",
						"The acquisition rebuttal modal is not displayed above the Add New Credit Card form.", driver);

				checkoutPg.openClosePLCCRebuttal("open");

				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCAcquisitionRebuttal"),checkoutPg),
						"The acquisition rebuttal over lay should opens up.",
						"The acquisition rebuttal over lay is opens up.",
						"The acquisition rebuttal over lay is not opens up.", driver);

				// Step-6
				checkoutPg.continueToPLCCStep2InPLCCACQ();
				Log.message(i++ + ". Clicked get it today button from rebuttal modal", driver);

				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApprovalStep2"), checkoutPg),
						"The acquisition rebuttal step 2 overlay should opens up.",
						"The acquisition rebuttal step 2 overlay is opens up.",
						"The acquisition rebuttal step 2 overlay is not opens up.", driver);

				// Step-7
				checkoutPg.closePLCCOfferByNoThanks();
				Log.message(i++ + ". Clicked no thanks button from PLCC step-2 overlay", driver);

				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("btnPaymentDetailsContinue", "paymentMethodSection"), checkoutPg),
						"User should be taken to the payment method section of Step 2",
						"User should be taken to the payment method section of Step 2",
						"User should be taken to the payment method section of Step 2", driver);

				Log.softAssertThat(checkoutPg.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("mdlPLCCRebuttal"),checkoutPg),
						"On checkout, the acquisition rebuttal slot will no longer display",
						"On checkout, the acquisition rebuttal slot is no longer displayed",
						"On checkout, the acquisition rebuttal slot is still displayed", driver);
				
				checkoutPg.fillingCardDetails1(cardType, false);
				Log.message(i++ + ". Filled credit card details", driver);
	
				checkoutPg.continueToReivewOrder();
				Log.message(i++ + ". Navigated to review and place order screen", driver);
				
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}
	
				OrderConfirmationPage orderPage = checkoutPg.clickOnPlaceOrderButton();
				Log.message(i++ + ". Clicked place order button", driver);
	
				Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage),
						"Page should navigated to Order confirmation page",
						"Page is navigated to Order confirmation page",
						"Page is not navigated to Order confirmation page", driver);
			} else {
				Log.reference("PLCC Step-1 overlay is not displayed with ALWAYS APPROVE address data, Futher verification is not proceeded");
			}
			
			Log.testCaseResult();
		} // Ending try block
		catch (Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		} // Ending finally
	}
}
