package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24812 extends BaseTest {

	EnvironmentPropertiesReader envorionmentPropertiesReader;
	
	
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24812(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
				
		String guestEmail = AccountUtils.generateEmail(driver);
		String productId = TestData.get("prd_variation");
		String cardType = "cards_2";

		// Always Approve Data
		String addressAlwaysApprove = "plcc_always_approve_address";
		
		int i = 1;
		try {

			// Step-1
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to home Page", driver);

			Log.softAssertThat(homePage.elementLayer.verifyPageElements(Arrays.asList("divMain"), homePage),
					"Home page should be displayed", 
					"Home page is displayed", 
					"Home page is not displayed", driver);

			// Step-2
			PdpPage pdpPage = homePage.headers.navigateToPDP(productId);
			Log.message(i++ + ". Navigated to PDP", driver);

			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to bag", driver);

			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to cart page", driver);

			CheckoutPage checkout = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to checkout page", driver);

			checkout.continueToShipping(guestEmail);
			Log.message(i++ + ". navigated to shipping address section", driver);

			// Step-3
			checkout.fillingShippingDetailsAsGuest(addressAlwaysApprove, ShippingMethod.Standard);
			Log.message(i++ + ". Filled shipping address", driver);

			Log.softAssertThat(checkout.verifyUseThisAsBillingAddressCheckBox(),
					"Use this as billing address checkbox should checked. ",
					"Use this as billing address checkbox should checked. ",
					"Use this as billing address checkbox should checked. ", driver);

			checkout.continueToPayment();
			Log.message(i++ + ". Clicked continue button from shipping address section", driver);

			if (checkout.elementLayer.verifyElementDisplayed(Arrays.asList("mdlPLCCApproval"), checkout)) {
								
				Log.softAssertThat(checkout.elementLayer.verifyElementDisplayed(Arrays.asList("mdlPLCCApproval"), checkout),
						"The preapproval modal should pop up.",
						"The preapproval modal is pop up.",
						"The preapproval modal is not pop up.", driver);

				// Step-4
				checkout.closePLCCOfferByNoThanks1();
				Log.message(i++ + ". Clicked No Thanks button from pre-approved modal", driver);

				Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("readyElement", "divPaymentDetailsSection"), checkout),
						"User will be taken to the payment method section of Step 2.",
						"User will be taken to the payment method section of Step 2.",
						"User will be taken to the payment method section of Step 2.", driver);

				Log.softAssertThat(checkout.elementLayer.verifyVerticalAllignmentOfElements(driver, "mdlPLCCRebuttal","paymentmethodSection", checkout),
						"The acquisition rebuttal modal should be displayed above the Add New Credit Card form.",
						"The acquisition rebuttal modal is displayed above the Add New Credit Card form.",
						"The acquisition rebuttal modal is not displayed above the Add New Credit Card form.", driver);
									
				if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
					Log.reference("Further verfication steps are not supported in current environment.");
					Log.testCaseResult();
					return;
				}

				// Step-5
				checkout.fillingCardDetails1(cardType, false);
				Log.message(i++ + ". Filled credit card details", driver);
	
				checkout.continueToReivewOrder();
				Log.message(i++ + ". Navigated to review and place order screen", driver);
	
				OrderConfirmationPage orderPage = checkout.clickOnPlaceOrderButton();
				Log.message(i++ + ". Clicked place order button", driver);
	
				Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage),
						"Page should navigated to Order confirmation page",
						"Page is navigated to Order confirmation page",
						"Page is not navigated to Order confirmation page", driver);
				
			} else {
				
				Log.message("<br>");
				Log.fail("PLCC modal is not displayed hence cant proceed further",driver);
				Log.message("<br>");
			}
									
			Log.testCaseResult();

		} // End of try
		catch (Exception e) {
			Log.exception(e, driver);
		} // End of catch
		finally {
			Log.endTestCase(driver);
		} // End of finally block
	}
}
