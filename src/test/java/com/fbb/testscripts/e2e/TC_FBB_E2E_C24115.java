package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PaypalConfirmationPage;
import com.fbb.pages.PaypalPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24115 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, priority = 0, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24115(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i=1;
		try
		{
			
			String searchText = TestData.get("product_search_terms").split("\\|")[0];
			String userEMailId = AccountUtils.generateEmail(driver);
			String checkoutAdd = "taxless_address";
			String prdDropship = TestData.get("prd_dropship");
			String prdVariation = TestData.get("prd_variation");
			String paypalemail = accountData.get("credential_paypal").split("\\|")[0];
			String paypalpassword = accountData.get("credential_paypal").split("\\|")[1];
			String validEmail = accountData.get("valid_registered_email");
			String giftPersonalizeMsg = demandwareData.get("GiftcardMessage");
			
			//Step 1 - Navigate to the website
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			PdpPage pdpPage = null;
			
			pdpPage = homePage.footers.navigateToPhysicalGiftCardPDP();
			Log.message(i++ + ". Navigated to gift card PDP page", driver);
			
			//Step 2 - Footer gift card navigation
			
			pdpPage.selectGCSize();
			Log.message(i++ + ". Gift card amount selected", driver);
			
			pdpPage.checkGCPersonalizedMessage("check");
			Log.message(i++ + ". 'Add a personalized message' check box checked", driver);
			
			pdpPage.enterGCFromAddress(validEmail);
			pdpPage.enterGCToAddress(validEmail);
			pdpPage.enterGCPersonalMessage(giftPersonalizeMsg);
			Log.message(i++ + ". From, To, Personal Message box filled with data.", driver);
			
			pdpPage.clickAddProductToBag();
			Log.message(i++ + ". Click add to bag button!", driver);
			
			pdpPage.clickOnContinueShoppingInMCOverlay();
			Log.message(i++ + ". Click on Continue Shopping in add to bag overlay!", driver);
			
			//Step 3 - Search field in the header
			
			homePage.headers.typeTextInSearchField(searchText);
			Log.message(i++ + ". Typed in the Search Field!", driver);

			//Covered as part of 24022
			Log.softAssertThat(homePage.headers.getEnteredTextFromSearchTextBox().trim().equals(searchText.trim()), 
					"Text should be entered in the Search text box", 
					"Text is entered in the Search text box", 
					"Text is not entered in the Search text box", driver);
			
			//Step 4 - Dropship and Regular Product detail page
			
			pdpPage = homePage.headers.navigateToPDP(prdVariation);
			Log.message(i++ + ". Navigated to PDP.", driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart!", driver);
			
			pdpPage = homePage.headers.navigateToPDP(prdDropship);
			Log.message(i++ + ". Navigated to Dropship PDP page!", driver);
									
			if(Utils.waitForElement(driver, "imgRatingStars", pdpPage, 5)) {
				Log.softAssertThat(pdpPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "productName", "imgRatingStars", pdpPage), 
						"Product name should display above the product review", 
						"Product name is displaying above the product review", 
						"Product name is not displaying above the product review", driver);
			}
			
			if (pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtSinglePriceAlone"), pdpPage)) {
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtSinglePriceAlone"), pdpPage), 
						"Single price should be displayed in PDP page", 
						"Single price is displayed in PDP page", 
						"Single price is not displayed in PDP page", driver);
				
			} else if (pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtSingleMultiStrikedOutPrice"), pdpPage)) {
				if(pdpPage.elementLayer.verifyElementTextContains("txtSingleMultiStrikedOutPrice", "-", pdpPage)) {
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtSingleMultiStrikedOutPrice"), pdpPage), 
							"Multi striked out price should be displayed in PDP page", 
							"Multi striked out price is displayed in PDP page", 
							"Multi striked out price is not displayed in PDP page", driver);
					
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtMultiPriceSalesRed"), pdpPage), 
							"Multi sales price should be displayed in PDP page", 
							"Multi sales price is displayed in PDP page", 
							"Multi sales price is not displayed in PDP page", driver);
				} else {
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtSingleMultiStrikedOutPrice"), pdpPage), 
							"Single striked out price should be displayed in PDP page", 
							"Single striked out price is displayed in PDP page", 
							"Single striked out price is not displayed in PDP page", driver);
					
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtSinlgePriceSalesRed"), pdpPage), 
							"Single sales price should be displayed in PDP page", 
							"Single sales price is displayed in PDP page", 
							"Single sales price is not displayed in PDP page", driver);
				}
			} else {
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("txtMultiPriceAlone"), pdpPage) &&
						pdpPage.elementLayer.verifyElementTextContains("txtMultiPriceAlone", "-", pdpPage), 
						"Multiple price range should be displayed in PDP page", 
						"Multiple price range is displayed in PDP page", 
						"Multiple price range is not displayed in PDP page", driver);
			}
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart!", driver);
			
			int NoOfThumbProdImg = pdpPage.getNoOfThumbnailProdImages();
			
			if(!Utils.isMobile()) {
				if (NoOfThumbProdImg > 4) {
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnPrevImageDisable","btnNextImageEnable"), pdpPage),
							"To check the arrows displaying for product image thumbnails.",
							"The 'Prev arrow', 'Next arrow' are displaying when the product have 5 or more thumbnail images",
							"The arrow is not displaying even the thumbnails are having 5 or more product images", driver);
	
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnPrevImageDisable"), pdpPage),
							"To check the previous arrow is disabled in the begining of image sequence.",
							"The 'Prev arrow' is displaying and disabled in the begining of image sequence.",
							"The 'Prev arrow' is not disabled", driver);
	
					Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnNextImageDisable"), pdpPage),
							"To check the next arrow is enabled in the begining of image sequence.",
							"The 'Next arrow' is displaying and enabled in the begining of image sequence.",
							"The 'Next arrow' is not enabled", driver);
	
				if (Utils.isDesktop()) {
					pdpPage.clickOnSpecifiedAlternateImage(5);
	
					Log.softAssertThat(pdpPage.verifyAlternateImageDisplayStatus(5, "false"),
							"To check the half hidden alternate image is displayed fully when the image is clicked.",
							"The alternate image is displaying fully.",
							"The alternate image is not displaying fully", driver);
				}
	
					pdpPage.scrollAlternateImageInSpecifiedDirection("Next");
	
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnNextImageDisable"), pdpPage),
							"To check the next arrow is disabled when the alternate image sequence reached the end.",
							"The 'Next arrow' is displaying and disabled in the end of image sequence.",
							"The 'Next arrow' is not disabled", driver);
	
					Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnPrevImageDisable"), pdpPage),
							"To check the Prev arrow is enabled at the end of image sequence.",
							"The 'Prev arrow' is displaying and enabled at the end of image sequence.",
							"The 'Prev arrow' is not enabled", driver);
				} else {
					Log.softAssertThat(pdpPage.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnPrevImageEnable","btnNextImageEnable"), pdpPage),
							"To check the arrows displaying for product image thumbnails.",
							"The product only have 4 or less thumbnail images, thus the arrows are not displayed",
							"The arrow is displaying even the product only have 4 or less thumbnail images", driver);
				}
			}
			else {
				if(pdpPage.getNoOfAlternateImage() > 0) {
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("btnSlickDotSection"), pdpPage),
							"For mobile, only alternate image dots will display",
							"For mobile, only alternate image dots are displayed",
							"For mobile, alternate image dots are not displayed", driver);
					
					pdpPage.clickOnAltImageSlickDot(1);
					Log.message(i++ + ". Clicked on slick dot.", driver);
					
					Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("prodImage_Mobile"), pdpPage),
							"On swipe or tab on the dots Alternate images should display on the main image area.",
							"On swipe or tab on the dots Alternate images displayed on the main image area.",
							"On swipe or tab on the dots Alternate images not displayed on the main image area.", driver);
				} else {
					Log.reference("Slickdots are not displayed. Product may not have alternate images.", driver);
				}
			}
			//Step 5 -Navigate to the cart page
			
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Click on checkout in add to bag overlay!", driver);
			
			double orderPrice = shoppingBagPg.getOrderSubTotal();
			
			Object obj = shoppingBagPg.clickOnEditLink(0);
			Log.message(i++ + ". Clicked on edit link", driver);
			
			if(Utils.isDesktop()) {
				QuickShop quickShop= (QuickShop) obj;
				
				Log.softAssertThat(quickShop.elementLayer.verifyAttributeForElement("txtToAddress", "value", validEmail, quickShop)
						&& quickShop.elementLayer.verifyAttributeForElement("txtFromAddress", "value", validEmail, quickShop),
						"Personalized message details should be displayed properly",
						"Personalized message details are displayed properly",
						"Personalized message details are not displayed properly", driver);
				
				quickShop.scrollQuickShopOverlayToPersonalizedMessage();
				Log.message("Scrolled the quick shop overlay to view personal message", driver);
				
				Log.softAssertThat(quickShop.elementLayer.verifyElementTextContains("txtAreaPersonalMsg", giftPersonalizeMsg, quickShop),
						"Personalized message section should be displayed properly",
						"Personalized message section are displayed properly",
						"Personalized message section are not displayed properly", driver);
				
				quickShop.selectQty("2");
				quickShop.addToBag();
				Log.message(i++ + ". Updated the quantity", driver);
			} else {
				pdpPage = (PdpPage) obj;
				
				Log.softAssertThat(pdpPage.elementLayer.verifyAttributeForElement("txtGCPersonalMsgTo", "value", validEmail, pdpPage)
						&& pdpPage.elementLayer.verifyAttributeForElement("txtGCPersonalMsgFrom", "value", validEmail, pdpPage)
						&& pdpPage.elementLayer.verifyElementTextContains("txtAreaPersonalMsg", giftPersonalizeMsg, pdpPage),
						"Personalized message details should be displayed properly",
						"Personalized message details are displayed properly",
						"Personalized message details are not displayed properly", driver);
				
				pdpPage.selectQty("2");
				pdpPage.clickOnUpdate();
				Log.message(i++ + ". Updated the quantity", driver);
			}
			
			double orderUpdatedPrice = shoppingBagPg.getOrderSubTotal();
			
			Log.softAssertThat(orderUpdatedPrice > orderPrice, 
					"Subtotal in order sumary should be increased when the user updates the QTY", 
					"Subtotal in order sumary increased when the user updates the QTY", 
					"Subtotal in order sumary not increased when the user updates the QTY", driver);
			
			SignIn signin = (SignIn) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			//Step 6a - Navigate to the checkout sign in page
			
			CheckoutPage checkoutPg = (CheckoutPage)signin.clickSignInButton();
			Log.message(i++ + ". Clicked Signin button.", driver);
			
			//Step 7 - Navigate to Checkout page - step 1
			
			checkoutPg.continueToShipping(userEMailId);
			Log.message(i++ + ". Navigated to Shipping section", driver);
			
			checkoutPg.fillingShippingDetailsAsGuest("NO", checkoutAdd, ShippingMethod.Standard);
			Log.message(i++ + ". Shipping Address entered successfully", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(true);
			Log.message(i++ + ". Use this as a Billing address check box enabled", driver);
			
			Log.softAssertThat(checkoutPg.getSelectedShippingMethod().trim().equalsIgnoreCase("Regular"), 
					"The shipping method 'Regular/Standard' should selected", 
					"The shipping method 'Regular/Standard' is selected", 
					"The shipping method 'Regular/Standard' is not selected", driver);
			//Step 6b - Navigate to the checkout sign in page
		
			Log.softAssertThat(checkoutPg.elementLayer.verifyListElementSize("lstShippingMethodExp", 3, checkoutPg), 
					"Shipping exception messages should be displayed for shipping methods", 
					"Shipping exception messages are displayed for shipping methods", 
					"Shipping exception messages are not displayed for shipping methods", driver);
			
			//Step 8 - Verify Shipping Exception overlay
			
			checkoutPg.selectShippingMethodOnly(ShippingMethod.SuperFast);
			Log.message(i++ + ". Try to select superfast shippment method", driver);
			
			//Shipping method exception overlay and PO box overlay have all the button element and overlay element same
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("shippingmethodExceptionOverlayHeading"), checkoutPg), 
					"Shipping Method Exception Overlay should get displayed", 
					"Shipping Method Exception Overlay is displayed", 
					"Shipping Method Exception Overlay is not displayed", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("btnCancelExpeditedOverlay","btnContinueExpeditedOverlay"), checkoutPg), 
					"Continue and Cancel button should be displayed in the shipping method exception overlay", 
					"Continue and Cancel button is displayed in the shipping method exception overlay", 
					"Continue and Cancel button is not displayed in the shipping method exception overlay", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementTextContains("txtUpdatedShipMtdInShpExpOverlay", "standard", checkoutPg), 
					"'Standard Delivery' should be displayed nearer to the UPDATED SHIPPING METHOD", 
					"'Standard Delivery' is displayed nearer to the UPDATED SHIPPING METHOD", 
					"'Standard Delivery' is not displayed nearer to the UPDATED SHIPPING METHOD", driver);
			
			checkoutPg.clickCancelInPOBoxOverlay();
			Log.message(i++ + ". Clicked on 'Cancel' in Shipping method exception overlay", driver);
			
			Log.softAssertThat(checkoutPg.getSelectedShippingMethod().trim().equalsIgnoreCase("Regular"), 
					"The shipping method 'Regular/Standard' should reselected", 
					"The shipping method 'Regular/Standard' is reselected", 
					"The shipping method 'Regular/Standard' is not reselected", driver);
			
			checkoutPg.continueToPayment();
			Log.message(i++ + ". Continued to Payment Page", driver);

			//Step 9 - Navigate to Checkout page - step 2
			
			checkoutPg.selectPaypalPayment();
			Log.message(i++ + ". Selected 'Paypal' as payment method", driver);
			
			PaypalPage paypalPage = checkoutPg.clickOnPaypalButton();
			Log.message(i++ + ". Clicked on Paypal button in shopping bag page", driver);
	
			PaypalConfirmationPage pcp = paypalPage.enterPayapalCredentials(paypalemail,paypalpassword);
			Log.message(i++ + ". Continued with Paypal Credentials.", driver);
			
			checkoutPg = pcp.clickContinueConfirmation();
			Log.message(i++ + ". Clicked on Continue button.", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg), 
					"Checkout page should be displayed!", 
					"Checkout page is displayed",
					"Checkout page is not displayed", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			//Step 10 - Navigate to Checkout page - step 3
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			checkoutPg.clickOnShoppingCartToolTip();
			Log.message(i++ + ". Clicked on Shipping & Handling fee icon", driver);
			
			Log.softAssertThat(checkoutPg.verifyGiftCardFeeInToolTip(), 
					"'Gift card fee' cost should be calculated as per the quantity added in the cart",
					"'Gift card fee' cost is calculated as per the quantity added in the cart",
					"'Gift card fee' cost is not calculated as per the quantity added in the cart", driver);
			
			checkoutPg.clickOnShippingCostToolClose();
			Log.message(i++ + ". Clicked close icon on Shipping & Handling tool-tip overlay", driver);
			
			OrderConfirmationPage receipt = checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);

			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			obj = receipt.clickOnproductImageByIndex(0);
			Log.message(i++ + ". Clicked on product image link from order confirmation page", driver);
			
			Log.softAssertThat(obj.getClass().getName().contains("PdpPage"),
					"After clicking the product image link, User should redirects tothe PDP page",
					"After clicking the product image link, User is redirected to the PDP page",
					"After clicking the product image link, User not redirected to the PDP page", driver);
			
			Log.testCaseResult();
		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		} // Ending finally
	} //M1_FBB_E2E_C24115
} //TC_FBB_E2E_C24115
