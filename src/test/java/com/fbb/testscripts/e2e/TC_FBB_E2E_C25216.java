package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BaseTest;
import com.fbb.support.BrandUtils;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C25216 extends BaseTest{
	
	EnvironmentPropertiesReader environmentPropertiesReader;
	
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C25216(String browser) throws Exception{
		
		Log.testCaseInfo(); 
		
		final WebDriver driver = WebDriverFactory.get(browser);
				
		String alwaysApproveAddress = "plcc_always_approve_address";
		String guestEmail = AccountUtils.generateEmail(driver);
					
		int i=1;
		try{
			//Step-1 & 2
			Object[] obj=GlobalNavigation.addGCProduct_Checkout(driver, false, true, i, guestEmail);
			
			CheckoutPage checkout= (CheckoutPage) obj[0];
			i=(int) obj[1];
			
			Log.softAssertThat(checkout.elementLayer.verifyElementDisplayed(Arrays.asList("divPaymentDetailsSection"), checkout),
					"user should be in the billing information section.",
					"user is in the billing information section.",
					"user is not in the billing information section.", driver);
			
			//Step-3
			checkout.fillingBillingDetailsAsGuest(alwaysApproveAddress);
			Log.message(i++ +". Entered billing address", driver);
			
			checkout.continueToPayment();
			Log.message(i++ +". Clicked continue button from billing address", driver);
			
			
			if(checkout.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkout)) {
				Log.reference("pre approved modal is displayed");
					
				Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApproval"), checkout),
						"The preapproval modal should display. ",
						"The preapproval modal is display. ",
						"The preapproval modal is not displayed. ", driver);
					
				//Step-4 
				checkout.continueToPLCCStep2();
				Log.message(i++ +". clicked Get, it today button", driver);
					
				Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApprovalStep2"), checkout),
						"When user selects Get It Today, user will be taken to the Step 2 overlay. ",
						"When user selects Get It Today, user is taken to the Step 2 overlay. ",
						"When user selects Get It Today, user is not taken to the Step 2 overlay. ", driver);
				
				//Step-5 
				checkout.typeTextInSSN("1234");
				Log.message(i++ +". Entered SSN number");
					
				checkout.selectDateMonthYearInPLCC2("01","05","1947");
				Log.message(i++ +". Selected date of birth");
				
				checkout.typeTextInMobileInPLCC("3334445555");
				Log.message(i++ +". Updated mobile number", driver);
				
				checkout.checkConsentInPLCC("YES");
				Log.message(i++ +". Selected consonent checbox", driver);				
					
				checkout.clickOnAcceptInPLCC();
				Log.message(i++ +". Clicked Yes, I Accept button", driver);
				
				if(checkout.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"), checkout)) {
							
					Log.softAssertThat(checkout.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"), checkout),
							"The Congratulations modal will pop up.",
							"The Congratulations modal is pop up.",
							"The Congratulations modal is not pop up.", driver);
								
					//Step-6 
					checkout.dismissCongratulationModal();
					Log.message(i++ +". Clicking the Continue to Checkout button", driver);
						
					Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("sectionPaymentCard"), checkout), 
							"user will be taken to the payment method section of checkout.",
							"user will be taken to the payment method section of checkout.",
							"user will be taken to the payment method section of checkout.", driver);
					
						
					Log.softAssertThat(checkout.checkPLCCCardSelectedBasedOnBrand(Utils.getCurrentBrandShort().substring(0, 2)) ||
							checkout.checkPLCCCardSelectedBasedOnBrand(BrandUtils.getBrandFullName()), 
							" The PLCC type that the user just signed up for will be selected in the Selected Credit Card Type field.", 
							" The PLCC type that the user just signed up for selected in the Selected Credit Card Type field.",
							" The PLCC type that the user just signed up for will not be selected in the Selected Credit Card Type field.", driver);
						
					Log.softAssertThat(checkout.elementLayer.verifyNumberMaskedWithSpecifiedDigits("fldTxtCardNo",4, checkout),
							"The masked number and last 4 digits of the PLCC card should be displayed in the \"Card Number\" field.",
							"The masked number and last 4 digits of the PLCC card is displayed in the \"Card Number\" field.",
							"The masked number and last 4 digits of the PLCC card is not displayed in the \"Card Number\" field.", driver);
							
					Log.softAssertThat(checkout.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("orderSummaryDiscount"), checkout),
							"The $10 discount should not displayed in the Order Summary section.", 
							"The $10 discount is not displayed in the Order Summary section.",
							"The $10 discount is displayed in the Order Summary section.", driver);
					 			
					//Step-7
					checkout.continueToReivewOrder();
					Log.message(i++ +". Navigated to review and place order screeen", driver);
					
					if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
						Log.reference("Further verfication steps are not supported in current environment.");
						Log.testCaseResult();
						return;
					}
					
					if(checkout.elementLayer.verifyPageElements(Arrays.asList("plccCardNoError"), checkout)) {
						Log.reference("By adding Always Approve data Incorrect card number is added, hence can't proceed further");
					
					} else {
						Log.softAssertThat(checkout.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrder"), checkout),
								"Page should navigated to review and place order screen",
								"Page should navigated to review and place order screen",
								"Page should navigated to review and place order screen", driver);
													
						//Step-8
						checkout.clickOnPlaceOrder();
						Log.message(i++ +". Clicked placed order button", driver);
						
						OrderConfirmationPage orderPage = null;
						
						if(checkout.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkout)) {
							Log.reference("By using Always Approve currently user can't place order");
														
						} else {
							orderPage = new OrderConfirmationPage(driver).get();
							Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage),
									"user should taken to the order confirmation page.",
									"user is taken to the order confirmation page.",
									"user is not taken to the order confirmation page.",driver);
							
							Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("OrderDetailsSection"), orderPage),
									" the order details should be displayed",
									" the order details is displayed",
									" the order details is not displayed",driver);
																	
							if(Utils.isMobile()) {
								orderPage.clickOnViewDetailsMobile();
								Log.message(i++ +". Clicked view details button in mobile view", driver);
							}
								
							Log.softAssertThat(orderPage.elementLayer.verifyNumberMaskedWithSpecifiedDigits("lblEnteredCardNo", 4, orderPage),
									"The last 4 digits of the PLCC card should be be displayed.",
									"The last 4 digits of the PLCC card is be displayed.",
									"The last 4 digits of the PLCC card is not displayed.", driver);
						}
					}				
				
				} else {
						Log.reference("Congrdulation modal is not displayed with Always Approve data", driver);
				}
			} else {
				Log.fail("Checkout step-1 ovelay is not displayed with ALWAYS APPROVE data hence cant proceed further", driver);
			}
			Log.testCaseResult();

		}// End of try
		catch(Exception e){
			Log.exception(e, driver);
		}//End of catch
		finally{
			Log.endTestCase(driver);
		}//End of finally		
	}
}