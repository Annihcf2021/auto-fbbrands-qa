package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24129 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@SuppressWarnings("unchecked")
	@Test(groups = { "checkout", "desktop","mobile","tablet" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24129(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
		int i=1;
		try
		{
			//Step 1 - Navigate to the website
			
			String productCatalog = TestData.get("qc_valid_2").split("\\|")[0];
			HashMap<String, String> productCatalogVariatons = new HashMap<String, String>();
			String invalidCQO[] = {"1234567","1234567","12345567","1234567"};
			String username= AccountUtils.generateEmail(driver);
			String password = accountData.get("password_global");
			String credential = username + "|" + password;
			String initialDefaultAddress = "valid_address1";
			String checkoutShipAdd = "address_withtax";
			String checkoutBillAdd = "valid_address8";
			String rewardCert = TestData.get(Utils.getRewardCertificate());
			String cqoProductIDWithoutHyphen = TestData.get("qc_valid_without_hyphen");
			String cqoProductIDWithHyphen = TestData.get("qc_valid_with_hyphen");
			String cqoProductIDWithHyphen1 = TestData.get("qc_valid_with_1_hyphen");
			String valid[] = {cqoProductIDWithoutHyphen,cqoProductIDWithHyphen,cqoProductIDWithHyphen1, productCatalog};
			
			{
				GlobalNavigation.registerNewUserAddress(driver, initialDefaultAddress + "|" + checkoutShipAdd + "|" + checkoutBillAdd, credential);
				GlobalNavigation.deleteDefaultAddress(driver);
				productCatalogVariatons = Utils.getCatalogVariationFromTestData(TestData.get("qc_valid_2"));
			}
			
			//Step 1 - Navigate to the website
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			
			Log.softAssertThat(homePage.elementLayer.verifyElementDisplayed(Arrays.asList("divMain"), homePage), 
					"Home page should be displayed", 
					"Home page is getting displayed", 
					"Home page is not getting displayed", driver);
			
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//step 2 - Click on Sign in link from the header
			
			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.message(i++ + ". Navigated to Signin page.", driver);
			
			signIn.clickForgotPwdLink();
			Log.message(i++ + ". Cicked on forget password link.", driver);
			
			signIn.enterEmailInForgotPasswordMdl(username);
			Log.message(i++ + ". Typed user email id in forget password model.", driver);
			
			signIn.clickonSendBtnInForgotPassword();
			Log.message(i++ + ". Clicked on Send Button", driver);

			//SC-2416
			if(signIn.getForgetPasswordSuccessMsg().contains("Email Sent")) {
				Log.softAssertThat(true, 
						"User will receive a message stating Email Sent", 
						"Appropriate success message displayed in Sign In page.", 
						"Appropriate success message not displayed in Sign In page.", driver);
			} else if(signIn.getForgetPasswordSuccessMsg().contains("Password Reset Requested!")) {
				Log.softAssertThat(true,
						"User will receive a message stating Password Reset Requested",
						"Appropriate success message displayed in Sign In page.", 
						"Appropriate success message not displayed in Sign In page.", driver);
			} else {
				Log.softAssertThat(signIn.getForgetPasswordSuccessMsg().toLowerCase().contains("currently unavailable"), 
						"User will receive a message stating \"We’ve received your request\"", 
						"<b style='color:chocolate'>User recieved a \"Password Reset Service Unavaialble\" message.</b>", 
						"Appropriate success message not displayed in Sign In page.", driver);
			}
			
			if(!Utils.isMobile()) {
				signIn.closeForgotPasswordDialog();
				Log.message(i++ + ". Closed forget password dialog", driver);
			} else {
				signIn.closeForgotPasswordPageMobile();
				Log.message(i++ + ". Closed forget password page", driver);
			}
			
			//step 3 - Navigate to catalog quick order page.
			
			MyAccountPage myAcc = signIn.navigateToMyAccount(username,password);
			Log.message(i++ + ". Navigated to My Account", driver);
			
			QuickOrderPage quickOrd = myAcc.navigateToQuickOrder();
			Log.message(i++ + ". Navigated to Quick order catalog page.");
			
			quickOrd.searchMultipleProductInQuickOrder(invalidCQO);
			Log.message(i++ + ". Searched with invalid product id.");
			
			Log.softAssertThat(quickOrd.elementLayer.verifyPageElements(Arrays.asList("item1ErrorMsg","item2ErrorMsg","item3ErrorMsg","item4ErrorMsg"), quickOrd), 
					"The system should display an error message - Please enter a valid number", 
					"The system displays an error message - Please enter a valid number", 
					"The system is not displaying error message - Please enter a valid number", driver);
			
			quickOrd.searchItemInQuickOrder(cqoProductIDWithoutHyphen);
			Log.message(i++ + ". Searched with valid CQO number (WITHOUT DASHES) in the \"Item/Catalog #\" field ", driver);
			
			int searchListCount = 0;
			String txtCQONumberOfItems = null;
			if(Utils.isMobile()) {
				txtCQONumberOfItems = "txtCQONumberOfItems_Mobile";
			} else {
				txtCQONumberOfItems = "txtCQONumberOfItems_Des_Tab";
			}
			searchListCount = quickOrd.elementLayer.getListElementSize(txtCQONumberOfItems, quickOrd);
			
			Log.softAssertThat( (searchListCount != 0) && (searchListCount == quickOrd.elementLayer.getListElementSize("productPriceList", quickOrd) ),
					"The system should retrieve product along with its quick order pricing", 
					"The system is retrieved product along with its quick order pricing", 
					"The system is not retrieved product along with its quick order pricing", driver);
			
			quickOrd.searchMultipleProductInQuickOrder(valid);
			Log.message(i++ + ". Searched with valid product id.", driver);
			
			searchListCount = quickOrd.elementLayer.getListElementSize(txtCQONumberOfItems, quickOrd);
			
			Log.softAssertThat( (searchListCount != 0) && (searchListCount == quickOrd.elementLayer.getListElementSize("productPriceList", quickOrd) ),
					"The system should retrieve product along with its quick order pricing", 
					"The system is retrieved product along with its quick order pricing", 
					"The system is not retrieved product along with its quick order pricing", driver);
			
			quickOrd.removeAllProducts();
			Log.message(i++ + ". Removed all items from quick order", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(quickOrd.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnClose_Mobile"), quickOrd), 
						"Click on the 'X' link to remove a CQO product", 
						"Click on the 'X' link removes CQO product", 
						"Click on the 'X' link not removes CQO product", driver);
			} else {
				Log.softAssertThat(quickOrd.elementLayer.verifyPageElementsDoNotExist(Arrays.asList("btnClose"), quickOrd), 
						"Click on the 'X' link to remove a CQO product", 
						"Click on the 'X' link removes CQO product", 
						"Click on the 'X' link not removes CQO product", driver);
			} 
			
			quickOrd.searchItemInQuickOrder(productCatalog);
			Log.message(i++ + ". Searched with valid product id.", driver);
			
			quickOrd.selectVariation(0, productCatalogVariatons);
			Log.message(i++ +". Selected variations for CQO item.", driver);
			
			quickOrd.clickAddProductToBag(0);
			Log.message(i++ + ". Added product to cart.", driver);
			
			quickOrd.closeAddToBagOverlay();
			Log.message(i++ + ". Closed ATB overlay.", driver);
			
			//Step 4 - Navigate to the cart page
			
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyPageElements(Arrays.asList("txtCatalogQuickOrder_Mobile"), shoppingBagPg), 
						"'CATALOG QUICK ORDER' text and icon should be displayed in Cart", 
						"'CATALOG QUICK ORDER' text and icon is displayed in Cart", 
						"'CATALOG QUICK ORDER' text and icon is not displayed in Cart", driver);
			} else {
				Log.softAssertThat(shoppingBagPg.elementLayer.verifyPageElements(Arrays.asList("txtCatalogQuickOrder_Desk_Tab"), shoppingBagPg), 
						"'CATALOG QUICK ORDER' text and icon should be displayed in Cart", 
						"'CATALOG QUICK ORDER' text and icon is displayed in Cart", 
						"'CATALOG QUICK ORDER' text and icon is not displayed in Cart", driver);
			}
			
			//step 5 - Navigate to Checkout page step -1
			
			CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			if(Utils.isMobile()) {
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("txtCatalogQuickOrder_Mobile"), checkoutPg), 
						"'CATALOG QUICK ORDER' text and icon should be displayed in Checkout", 
						"'CATALOG QUICK ORDER' text and icon is displayed in Checkout", 
						"'CATALOG QUICK ORDER' text and icon is not displayed in Checkout", driver);
			} else {
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("txtCatalogQuickOrder_Desk_Tab"), checkoutPg), 
						"'CATALOG QUICK ORDER' text and icon should be displayed in Checkout", 
						"'CATALOG QUICK ORDER' text and icon is displayed in Checkout", 
						"'CATALOG QUICK ORDER' text and icon is not displayed in Checkout", driver);
			}
			
			checkoutPg.selectValueFromSavedAddressesDropdownByIndex(1);
			Log.message(i++ + ". Selected saved address in shipping", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("savedAddressCount"), checkoutPg)
							&& checkoutPg.elementLayer.verifyElementTextContains("savedAddressCount", "2", checkoutPg), 
					"Saved address count should be displayed",
					"Saved address count is correctly displayed", 
					"Saved address count is not displaying correctly", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(false);
			Log.message(i++ + ". Use this as a Billing address check box disabled", driver);
			
			//step 6 - Navigate to Checkout page - step 2
			
			checkoutPg.continueToBilling();
			Log.message(i++ + ". Continued to Billing address", driver);
			
			checkoutPg.selectValueFromSavedBillingAddressesDropdownByIndex(1);
			Log.message(i++ + ". Selected saved address in billing", driver);
			
			double priceBeforeApply = checkoutPg.getRemainingTotal();
			
			checkoutPg.applyRewardCertificate(rewardCert);
			Log.message(i++ + ". Reward Certificate is applied", driver);
			
			checkoutPg.removeAppliedReward();
			Log.message(i++ + ". Applied Reward Certificate is removed", driver);
			
			double priceAfterApply = checkoutPg.getRemainingTotal();
			
			Log.softAssertThat(checkoutPg.elementLayer.VerifyPageElementNotDisplayed(Arrays.asList("btnRewardRemove"), checkoutPg),
					"The applied reward certificate should be removed",
					"The applied reward certificate is removed.",
					"The applied reward certificate not removed.", driver);
			
			if(priceAfterApply == priceBeforeApply) {
				Log.message(i++ + ". The Order Summary is recalculated.",driver);
			} else {
				Log.message(i++ + ". The Order Summary price is not matching.",driver);
			}
			
			checkoutPg.applyRewardCertificate(rewardCert);
			Log.message(i++ + ". Same Reward Certificate is applied", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryOrderTotal", "orderSummaryRewardDiscount", checkoutPg)
							&& checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "orderSummaryRewardDiscount", "orderSummaryRemainingTotal", checkoutPg),
					"Applied RW amount should be displayed between Order total and Remaining total",
					"Applied RW amount is displayed between Order total and Remaining total",
					"Applied RW amount not displayed between Order total and Remaining total", driver);
			
			if(!(checkoutPg.getRemainingOrderAmount() == 0.00)) {
				checkoutPg.applyGiftCardByValue(TestGiftCardValue.$10);
				Log.message(i++ + ". Gift Card is applied", driver);
			
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("lnkGiftCardRemainingBalance"), checkoutPg),
						"Gift card displaying the Remaining balance",
						"Gift card is displaying the Remaining balance",
						"Gift card not displaying the Remaining balance", driver);
			}
			
			if(!(checkoutPg.getRemainingOrderAmount() == 0.00)) {
				checkoutPg.applyGiftCardByValue(TestGiftCardValue.$1000);
				Log.message(i++ + ". Another Gift Card is applied", driver);
			}
			
			if(!(checkoutPg.getRemainingOrderAmount() == 0.00)) {
				Log.fail("Order Summary Total is not 0, hence cannot continue!",driver);
			}
			
			Log.softAssertThat(checkoutPg.getRemainingOrderAmount() == 0.00, 
					"Order summary total should get 0",
					"Order summary total is 0",
					"Order summary total is not 0", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			//step 7 - Navigate to Checkout page - step 3
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 8 - Order confirmation page
						
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);
			
			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.removeAllItemsFromQuickOrder(driver);
			GlobalNavigation.RemoveAllProducts(driver);
			GlobalNavigation.removeAllSavedAddress(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24129
