package com.fbb.testscripts.e2e;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24803 extends BaseTest {

	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	@Test(groups = { "internationalslots", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24803(String browser) throws Exception {
		Log.testCaseInfo();

		final WebDriver driver = WebDriverFactory.get(browser);
		
		int i = 1;
		try {
				
			String username = accountData.get("international_User").split("\\|")[0];
			String password = accountData.get("international_User").split("\\|")[1];
			String searchKey = TestData.get("prd_variation");
			String address = "taxless_address";
			String paymentCard = "card_Visa";

			{
				GlobalNavigation.registerNewUserAddressPayment(driver, address, paymentCard, username + "|" + password);
			}
			
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Home Page launched successfully ", driver);

			MyAccountPage myAccount = homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Navigated to My Account Page.", driver);

			Log.softAssertThat(myAccount.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver, "breadCrumb","internationalBanner", myAccount),
					"International Slot should displayed below the Breadcrumb.",
					"International Slot is displayed below the Breadcrumb.",
					"International Slot not displayed below the Breadcrumb.", driver);

			PdpPage pdpPage = homePage.headers.navigateToPDP(searchKey);
			Log.message(i++ + ". Navigated to PDP", driver);

			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Added product to bag", driver);

			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Clicked on mini bag icon & navigates to the cart page", driver);

			Log.softAssertThat(
					cartPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver,
							"internationalCartBanner", "headerShoppingBag", cartPage),
					"The International Shipping message slot should be displayed above the Cart Heading.",
					"The International Shipping message slot is displayed above the Cart Heading.",
					"The International Shipping message slot not displayed above the Cart Heading.", driver);

			CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
			Log.message(i++ + ". Navigated to checkout page", driver);

			Log.softAssertThat(checkoutPage.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(
								driver, "lblShippingDetailsHeading", "internationalCheckoutBanner", checkoutPage),
					"International slot should be displayed below the Shipping Address Heading.",
					"International slot is displayed below the Shipping Address Heading.",
					"International slot not displayed below the Shipping Address Heading.", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			checkoutPage.selectSavedCard_Non_PLCC(1);
			Log.message(i++ +". Selected non PLCC card from payment section", driver);
			
			checkoutPage.enterCVV("333");

			checkoutPage.clickOnPlaceOrderButton();
			Log.message(i++ + ". Placed order", driver);

			homePage.headers.navigateToMyAccount();

			OrderHistoryPage orderHistory = myAccount.clickOnOrderHistoryLink();
			Log.message(i++ + ". Navigated to Order History Page.", driver);

			orderHistory.clickOrderByIndex(0);
			
			if(!Utils.isMobile()) {
				Log.softAssertThat(orderHistory.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver,"internationalOrderPageBanner", "orderNumber", orderHistory),
						"Order Number should be displayed top left below the My Account Navigation Pane or below the International Slot",
						"Order Number is displayed top left below the My Account Navigation Pane or below the International Slot",
						"Order Number not displayed top left below the My Account Navigation Pane or below the International Slot",
						driver);
			} else {
				Log.softAssertThat(orderHistory.elementLayer.verifyVerticalAllignmentOfElementsWithoutScrolling(driver,"internationalOrderPageBanner", "orderNumber", orderHistory),
						"Order Number should be displayed top left below the My Account Navigation Pane or below the International Slot in mobile",
						"Order Number is displayed top left below the My Account Navigation Pane or below the International Slot in mobile",
						"Order Number not displayed top left below the My Account Navigation Pane or below the International Slot in mobile",
						driver);
			}
			
			Log.testCaseResult();
			
		} // Ending try block
		catch (Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			GlobalNavigation.removeAllPaymentMethods(driver);
			Log.endTestCase(driver);
		} // Ending finally

	}

}// M1_FBB_E2E_C24022