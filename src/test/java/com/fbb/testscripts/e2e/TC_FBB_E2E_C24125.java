package com.fbb.testscripts.e2e;

import java.util.Arrays;
import java.util.HashSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.GiftCardPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C24125 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	@Test(groups = { "checkout", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C24125(String browser) throws Exception
	{
		Log.testCaseInfo();
		
		final WebDriver driver = WebDriverFactory.get(browser);
		
		String userEmail = AccountUtils.generateEmail(driver);
		String userPassword = accountData.get("password_global");
		String userCredential = userEmail + "|" + userPassword;
		String checkoutShipAddressTaxed= "taxless_address";
		String eGiftCardPrd = TestData.get("prd_egift_card");
		String checkoutBillAdd = "valid_address1";
		String productBackOrdered = TestData.get("prd_back-order");
		String breadcrumbCategory = TestData.get("E-giftcard_breadcrumb_mobile");
		String authenticated_egc_url = UrlUtils.getProtectedWebSiteWithYotta(Utils.getWebSite() + UrlUtils.getPath(TestData.get("prd_egift_card_url")));
		
		{
			GlobalNavigation.registerNewUserAddress(driver, checkoutShipAddressTaxed + "|" + checkoutBillAdd, userCredential);
		}
		
		int i=1;
		try {
			//Step 1 - Navigate to the website
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step 2 - From the header click on Sign in link & navigate to my account sign in page
			homePage.headers.navigateToMyAccount(userEmail, userPassword);
			Log.message(i++ + ". Navigated to My Account page as : " + userEmail, driver);
			
			//Step 3 - Navigate to gift card landing page
			
			PdpPage pdpPage = null;
			GiftCardPage giftCardLanding = null;
			boolean isNavigatedToGcPage = false;
			try {
				giftCardLanding = homePage.footers.navigateToGiftCardLandingPage();
				Log.message(i++ + ". Navigated to Gift Card Landing Page", driver);
				
				//Step 4 - Navigate to Gift card PDP
				
				Log.softAssertThat(giftCardLanding.elementLayer.verifyAnyElementDisplayed(Arrays.asList("btnGiftCard", "lnkGiftCard"), giftCardLanding)
								&& giftCardLanding.elementLayer.verifyAnyElementDisplayed(Arrays.asList("btnGiftCert", "lnkGiftCert"), giftCardLanding), 
						"Gift Card Landing Page should display two types of gift cards, SHOP E GIFT CARD and SHOP GIFT CARD.",
						"Gift Card Landing Page should display two types of gift cards, SHOP E GIFT CARD and SHOP GIFT CARD.",
						"Gift Card Landing Page should display two types of gift cards, SHOP E GIFT CARD and SHOP GIFT CARD.", driver);
				
				pdpPage = giftCardLanding.navigateToGiftCertificate();
				Log.message(i++ + ". Navigated to gift card PDP page", driver);
				
				isNavigatedToGcPage = true;
			} catch (Exception e) {
				Log.reference("Gift card landing page doesn't have any product", driver);
				try {
					driver.get(authenticated_egc_url);
				} catch(Exception ex) {
					pdpPage = homePage.headers.navigateToPDP(eGiftCardPrd);
				}
				pdpPage = new PdpPage(driver).get();
				Log.message(i++ + ". Navigated to gift card Pdp Page through URL!", driver);
				
				isNavigatedToGcPage = true;
			}
			
			if (isNavigatedToGcPage) {
				if (Utils.isMobile()) {
					Log.softAssertThat(pdpPage.verifyBreadCrumbMobile(breadcrumbCategory),
							"The breadcrumb should displays the relevant category hierarchy for the product",
							"The breadcrumb is displayed with the relevant category hierarchy for the product",
							"The breadcrumb is not displayed with the relevant category hierarchy for the product", driver);
				} else {
					Log.softAssertThat(pdpPage.verifyBreadCrumb(pdpPage.getProductName()),
							"The breadcrumb should displays the relevant category hierarchy for the product and product name",
							"The breadcrumb is displayed with the relevant category hierarchy for the product  and product name",
							"The breadcrumb is not displayed with the relevant category hierarchy for the product  and product name", driver);
					
					Log.softAssertThat(pdpPage.verifyLastBreadcrumbTextNotClickable() && pdpPage.verifyLastBreadcrumbDifferentTextColor(),
							"The product name should not be clickable in the breadcrumb & it should display in different color.",
							"The product name is not clickable in the breadcrumb & it is displaying in different color.",
							"The product name is clickable in the breadcrumb & it is not displaying in different color.", driver);
				}
				
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("productName"), pdpPage), 
						"Product name should display", 
						"Product name is displayed", 
						"Product name is not displayed", driver);
				
				pdpPage.openCloseToolTipOverlay("open");
				Log.message(i++ + ". Terms and Condition tool tip overlay is opened", driver);
				
				Log.softAssertThat(pdpPage.elementLayer.verifyElementDisplayed(Arrays.asList("closeToolTipOverLay"), pdpPage), 
						"Terms and Condition overlay should displaying", 
						"Terms and Condition overlay is displaying", 
						"Terms and Condition overlay is not displaying", driver);
				
				pdpPage.openCloseToolTipOverlay("close");
				Log.message(i++ + ". Terms and Condition tool tip overlay is closed", driver);
				
				pdpPage.selectGCSize();
				Log.message(i++ + ". Gift card amount selected", driver);
				
				pdpPage.enterRecepientEmailAdd("test@yopmail.com");
				Log.message(i++ + ". Entered valid address in Recepient Email address filled", driver);
				
				pdpPage.enterRecepientEmailConfirmAdd("test@yopmail.com");
				Log.message(i++ + ". Entered mismatch address in Recepient Confirm Email address filled", driver);
				
				pdpPage.clickAddProductToBag();
				Log.message(i++ + ". Click add to bag button!", driver);
				
				pdpPage.clickOnContinueShoppingInMCOverlay();;
				Log.message(i++ + ". Click on continue button in add to bag overlay!", driver);
			}
			
			//Step 6 - Navigate to PDP
			pdpPage =  homePage.headers.navigateToPDP(productBackOrdered);
			Log.message(i++ + ". Navigated to Product", driver);
			
			String prdNameToVerify = pdpPage.getProductName();
			String productID = pdpPage.getProductID();
			
			Object obj = pdpPage.addToWishlist();
			Log.message(i++ + ". Added product to wishlist", driver);
			
			Log.softAssertThat(obj.getClass().getName().contains("PdpPage"),
					"After clicking the add to wishlist link, User should stays on the PDP page",
					"After clicking the add to wishlist link, User is stayed on the PDP page",
					"After clicking the add to wishlist link, User not stayed on the PDP page", driver);
			
			//Step 7 - Navigate to wishlist page
			WishListPage wishListPg = (WishListPage) homePage.footers.navigateToWishLists();
			Log.message(i++ + ". Navigated To Wishlist page", driver);
			
			Log.softAssertThat(wishListPg.verifyProductAddedToWishlist(Arrays.asList(prdNameToVerify)),
					"The added item should be displayed on the Wishlist page.",
					"The added item is displayed on the Wishlist page.",
					"The added item was not displayed on the Wishlist page.", driver);
			
			wishListPg.clickOnproductNameByPrdID(productID);
			
			Log.softAssertThat(obj.getClass().getName().contains("PdpPage"),
					"After clicking the product name link, User should navigated to the PDP page",
					"After clicking the product name link, User is navigated to the PDP page",
					"After clicking the product name link, User not navigated to the PDP page", driver);
			
			wishListPg = (WishListPage) homePage.footers.navigateToWishLists();
			Log.message(i++ + ". Navigated To Wishlist page again", driver);
			
			wishListPg.clickOnproductImageByPrdID(productID);
			
			Log.softAssertThat(obj.getClass().getName().contains("PdpPage"),
					"After clicking the add to wishlist link, User should stays on the PDP page",
					"After clicking the add to wishlist link, User is stayed on the PDP page",
					"After clicking the add to wishlist link, User not stayed on the PDP page", driver);
			
			wishListPg = (WishListPage) homePage.footers.navigateToWishLists();
			Log.message(i++ + ". Navigated To Wishlist page again", driver);
			
			if (!Utils.isMobile()) {
				Log.softAssertThat(wishListPg.elementLayer.verifyHorizontalAllignmentOfElements(driver, "btnAddtoCart", "wishListItemDetails", wishListPg), 
						"Product details should displayed left to the add to cart button", 
						"Product details is displayed left to the add to cart button", 
						"Product details is not displayed left to the add to cart button", driver);
			} else {
				Log.softAssertThat(wishListPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "wishListItemDetails", "btnAddtoCart", wishListPg), 
						"Product details should displayed left to the add to cart button", 
						"Product details is displayed left to the add to cart button", 
						"Product details is not displayed left to the add to cart button", driver);
			}
			
			//Step 8 - Navigate to the cart page
			
			wishListPg.clickAddtoBagByIndex(0);
			Log.message(i++ + ". Product is Added to cart", driver);
			
			Log.softAssertThat(wishListPg.elementLayer.verifyElementDisplayed(Arrays.asList("btnCheckoutInMCOverlay"), wishListPg), 
					"Checkout overlay should be displayed", 
					"Checkout overlay is getting displayed", 
					"Checkout overlay is not getting displayed", driver);
			
			ShoppingBagPage shoppingBagPg = wishListPg.clickOnCheckoutInMCOverlay();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			//Step 9 - Navigate to Checkout page - step 1
			//Step 10 - Navigate to Checkout page - step 2
			CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);
			
			checkoutPg.selectSavedAddressInShippingByIndex(1);
			Log.message(i++ + ". Selected saved address in shipping", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyElementTextContains("savedAddressCount", "2", checkoutPg), 
					"Saved address count should be displayed", 
					"Saved address count is correctly displayed", 
					"Saved address count is not displaying correctly", driver);
			
			Log.softAssertThat(!checkoutPg.elementLayer.verifyPageElements(Arrays.asList("lblExpressShip","lblNextDayShip","lblSuperfastShip"), checkoutPg) 
					&& checkoutPg.elementLayer.verifyPageElements(Arrays.asList("lblStandardShip"), checkoutPg), 
					"Ground shipping method alone should be displayed", 
					"Ground shipping method alone is correctly displayed", 
					"Ground shipping method alone is not displaying correctly", driver);
			
			checkoutPg.checkUncheckUseThisAsBillingAddress(false);
			Log.message(i++ + ". Use this as a Billing address check box disabled", driver);
			
			checkoutPg.continueToBilling();
			Log.message(i++ + ". Continued to Billing address", driver);
			
			if(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("mdlAddressSuggestion"), checkoutPg)){
				
				checkoutPg.clickAvsContinueButton();
				Log.message(i++ +". Clicked continue button from AVS modal", driver);
			}
			
			checkoutPg.selectValueFromSavedBillingAddressesDropdownByIndex(2);
			Log.message(i++ + ". Selected saved address in billing", driver);
		
			checkoutPg.selectCardType("Visa");
			checkoutPg.enterTextInCardNameField("JOHN");
			checkoutPg.enterTextInCardNumberField("6500000000000002");
			checkoutPg.selectExpiryMonth("July");
			checkoutPg.selectExpiryYear("2021");
			checkoutPg.enterCVV("123");
			Log.message(i++ + ". Entered payment details.", driver);
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ +". Clicked continue button", driver);
			
			Log.softAssertThat(checkoutPg.elementLayer.verifyTextContains("lblcreditcardPlaceHolderText", "Invalid Credit Card Number",checkoutPg),
					"Error message should be displayed on the card number field",
					"Error message should be displayed on the card number field",
					"Error message should be displayed on the card number field", driver);
			
			if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
				Log.reference("Further verfication steps are not supported in current environment.");
				Log.testCaseResult();
				return;
			}
			
			checkoutPg.selectCardType("Discover");
			Log.message(i++ +". Selected Discover credit card", driver);
			
			//As per SC-208 number and cvv fields should be cleared
			checkoutPg.enterTextInCardNumberField("6500000000000002");
			checkoutPg.enterCVV("123");
			Log.message(i++ +". Filled credit card info", driver);
			
			
			checkoutPg.clickOnPaymentDetailsContinueBtn();
			Log.message(i++ + ". Continued to Review & Place Order", driver);
			
			HashSet<String> ProductId = checkoutPg.getOrderedPrdListNumber();
			
			String OrderSummaryTotal = checkoutPg.getOrderSummaryTotal();
			
			//step 11 - Navigate to Checkout page - step 3
			
			OrderConfirmationPage receipt= checkoutPg.clickOnPlaceOrderButton();
			Log.message(i++ + ". Order Placed successfully", driver);
			
			//Step 12 - Order confirmation page
					
			HashSet<String> ProductIdReceipt = receipt.getOrderedPrdListNumber();
			
			Log.softAssertThat(receipt.elementLayer.compareTwoHashSet(ProductIdReceipt, ProductId), 
					"The product added should be present in the receipt", 
					"The product added is present in the receipt", 
					"The product added is not present in the receipt", driver);
			
			String OrderSummaryTotalReceipt = receipt.getOrderSummaryTotal();
			
			Log.softAssertThat(OrderSummaryTotalReceipt.equals(OrderSummaryTotal), 
					"The order total should display correctly", 
					"The order total is displaying correctly", 
					"The order total is not displaying correctly", driver);

			Log.testCaseResult();

		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			GlobalNavigation.RemoveAllProducts(driver);
			GlobalNavigation.RemoveWishListProducts(driver);
			Log.endTestCase(driver);
		}// Ending finally

	}

}//M1_FBB_E2E_C24125