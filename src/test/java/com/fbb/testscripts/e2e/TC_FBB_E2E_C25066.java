package com.fbb.testscripts.e2e;

import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.BaseTest;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.DataProviderUtils;
import com.fbb.support.EmailReport;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;
import com.fbb.support.WebDriverFactory;

@Listeners(EmailReport.class)
public class TC_FBB_E2E_C25066 extends BaseTest{

	EnvironmentPropertiesReader environmentPropertiesReader;	
	
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	
	@Test(groups = { "plcc", "desktop", "tablet", "mobile" }, dataProviderClass = DataProviderUtils.class, dataProvider = "parallelTestDataProvider")
	public void M1_FBB_E2E_C25066(String browser) throws Exception
	{
		Log.testCaseInfo(); 

		final WebDriver driver = WebDriverFactory.get(browser);
			
		int i=1;
		try
		{	
			String userEMailIdAlwaysApprove = AccountUtils.generateUniqueEmail(driver, "@gmail.com");
			String password = accountData.get("password_global");
			String credentialAlwaysApprove = userEMailIdAlwaysApprove +"|"+ password;
			
			String addressAlwaysApprove = "plcc_always_approve_address";
			String firstNameAlwaysApprove = checkoutData.get("plcc_always_approve_address").split("\\|")[7];
			String lastNameAlwaysApprove = checkoutData.get("plcc_always_approve_address").split("\\|")[8];
			
			String prdNoPLCCDiscount = TestData.get("prd_noDiscountPLCC");
				
			{
				GlobalNavigation.registerNewUserWithUserDetail(driver, 0, 0, firstNameAlwaysApprove, lastNameAlwaysApprove, credentialAlwaysApprove);
				
				GlobalNavigation.addNewAddressToAccount(driver, addressAlwaysApprove, true, credentialAlwaysApprove);
			}
			
			//Step1
			HomePage homePage = new HomePage(driver, Utils.getWebSite()).get();
			Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
			
			//Step2
			MyAccountPage myAcc = homePage.headers.navigateToMyAccount(userEMailIdAlwaysApprove, password);
			Log.message(i++ + ". Navigated to My Account page as : " +userEMailIdAlwaysApprove, driver);
			
			Log.softAssertThat(myAcc.elementLayer.verifyElementDisplayed(Arrays.asList("readyElement"), myAcc), 
					"User should be successfully created", 
					"User is created", 
					"User is not created", driver);
			
			//Step3
			AddressesPage addrPg = myAcc.navigateToAddressPage();
			Log.message(i++ + ". Navigated to Address page!", driver);
			
			Log.softAssertThat(addrPg.getSavedAddressesCount() > 0, 
					"Address should be successfully created", 
					"Address is created", 
					"Address is not created", driver);
			
			//Step4
			PdpPage pdpPage = homePage.headers.navigateToPDP(prdNoPLCCDiscount);
			Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product added to cart", driver);
			
			ShoppingBagPage shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping bag page", driver);
			
			shoppingBagPg.updateOrderTotalMeetPlccDiscount(prdNoPLCCDiscount);
			Log.message(i++ + ". Updated product quantity to meet PLCC discount.", driver);
			
			CheckoutPage checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout page", driver);

			if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("modalCheckoutPlcc"), checkoutPg)){
						
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("modalCheckoutPlcc"), checkoutPg), 
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed", 
						"PLCC modal is not getting displayed", driver);
				
				//Step5
				checkoutPg.clickNoThanksInPLCC();
				Log.message(i++ + ". Click on 'No Thanks' in PLCC!", driver);
				
				//Step6
				checkoutPg.clickOnBrandLogo();
				Log.message(i++ + ". Click on Brand logo!", driver);
				
				homePage.headers.signOut();
				Log.message(i++ + ". User signed out!", driver);
				
				homePage = BrowserActions.clearCookies(driver);
				Log.message(i++ + ". Cleared cookies and navigated to Homepage", driver);
				
				//Step7
				//Step8
				homePage = homePage.headers.navigateToHome();
				Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
				
				myAcc = homePage.headers.navigateToMyAccount(userEMailIdAlwaysApprove, password);
				Log.message(i++ + ". Navigated back to My Account page as : " +credentialAlwaysApprove, driver);
				
				//Step9
				int cartCount = StringUtils.getNumberInString(homePage.headers.getMiniCartCount());
				if(cartCount == 0) {
					pdpPage = homePage.headers.navigateToPDP(prdNoPLCCDiscount);
					Log.message(i++ + ". Navigated To PDP page : "+pdpPage.getProductName(), driver);
					
					pdpPage.addToBagCloseOverlay();
					Log.message(i++ + ". Product added to cart", driver);
				}
				
				shoppingBagPg = homePage.headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping bag page", driver);
				
				checkoutPg = (CheckoutPage) shoppingBagPg.navigateToCheckout();
				Log.message(i++ + ". Navigated to Checkout page", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("modalCheckoutPlcc"), checkoutPg), 
						"PLCC modal should be displayed", 
						"PLCC modal is getting displayed", 
						"PLCC modal is not getting displayed", driver);
				
				//Step10
				checkoutPg.clickNoThanksInPLCC();
				Log.message(i++ + ". Click on 'No Thanks' in PLCC!", driver);
				
				//Step11
				checkoutPg.continueToPayment(true);
				Log.message(i++ + ". Clicked on 'Continue' button and navigated to Payment!", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyVerticalAllignmentOfElements(driver, "mdlPLCCRebuttal", "paymentmethodSection", checkoutPg),
						"The acquisition rebuttal modal should be displayed above the Add New Credit Card form.",
						"The acquisition rebuttal modal is displayed above the Add New Credit Card form.",
						"The acquisition rebuttal modal is not displayed above the Add New Credit Card form.", driver);
				
				//Step12
				checkoutPg.openClosePLCCRebuttal("open");
				Log.message(i++ +". Clicked learn more link from rebuttal solot", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCAcquisitionRebuttal"),checkoutPg),
						"The acquisition rebuttal over lay should opens up.",
						"The acquisition rebuttal over lay is opens up.",
						"The acquisition rebuttal over lay is not opens up.", driver);
				
				//Step13
				checkoutPg.continueToPLCCStep2InPLCCACQ();
				Log.message(i++ +". Clicked get it today button from rebuttal modal", driver);
				
				Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("mdlPLCCApprovalStep2"),checkoutPg),
						"The acquisition rebuttal step 2 overlay should opens up.",
						"The acquisition rebuttal step 2 overlay is opens up.",
						"The acquisition rebuttal step 2 overlay is not opens up.", driver);
				
				//Step14
				checkoutPg.typeTextInSSN("1234");
				Log.message(i++ +". Entered SSN number", driver);
				
				checkoutPg.selectDateMonthYearInPLCC2("15", "08", "1947");
				Log.message(i++ +". Selected date, month, year", driver);
				
				checkoutPg.typeTextInMobileInPLCC("8015841844");
				Log.message(i++ +". Updated phone number", driver);
				
				checkoutPg.checkConsentInPLCC("YES");
				Log.message(i++ +". Checked Consent checkbox", driver);
				
				checkoutPg.clickOnAcceptInPLCC();
				Log.message(i++ +". Clicked yes, i accept button", driver);
				
				if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("approvedModal"), checkoutPg)) {
														
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("approvedModal"),checkoutPg),
							"The Approval modal should be displayed",
							"The Approval modal is displayed",
							"The Approval modal is not displayed", driver);
						
					//Step15
					checkoutPg.dismissCongratulationModal();
					Log.message(i++ +". Clicking the Continue to Checkout button", driver);
					
					Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"), checkoutPg), 
							"user will be taken to the payment method section of checkout.",
							"user will be taken to the payment method section of checkout.",
							"user will be taken to the payment method section of checkout.", driver);
						
					Log.softAssertThat(checkoutPg.checkPLCCCardSelectedBasedOnBrand(Utils.getCurrentBrandShort().substring(0, 2)) ||
							checkoutPg.checkPLCCCardSelectedBasedOnBrand(BrandUtils.getBrandFullName()), 
							" The PLCC type that the user just signed up for will be selected in the Selected Credit Card Type field.", 
							" The PLCC type that the user just signed up for selected in the Selected Credit Card Type field.",
							" The PLCC type that the user just signed up for will not be selected in the Selected Credit Card Type field.", driver);
					
					if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("fldTxtCardNo"), checkoutPg)) {
						Log.softAssertThat(checkoutPg.elementLayer.verifyNumberMaskedWithSpecifiedDigits("fldTxtCardNo",4, checkoutPg),
								"The masked number and last 4 digits of the PLCC card should be displayed in the \"Card Number\" field.",
								"The masked number and last 4 digits of the PLCC card is displayed in the \"Card Number\" field.",
								"The masked number and last 4 digits of the PLCC card is not displayed in the \"Card Number\" field.", driver);
					
					} else {
						Log.reference("PLCC card is added to account, so verifying last 4 digits are not possible");
					}
					
					Log.softAssertThat(checkoutPg.elementLayer.verifyElementDisplayed(Arrays.asList("orderSummaryDiscount"), checkoutPg),
							"The $10 discount should be displayed in the Order Summary section.", 
							"The $10 discount is displayed in the Order Summary section.",
							"The $10 discount is not displayed in the Order Summary section.", driver);
				
					//Step16
					checkoutPg.continueToReivewOrder();
					Log.message(i++ +". Navigated to review and place order screeen", driver);
					
					if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("plccCardNoError"), checkoutPg)) {
						Log.reference("By using always approve data, PLCC card error message is displayed hence can't proceed", driver);
											
					} else {	
						Log.softAssertThat(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("btnPlaceOrder"), checkoutPg),
								"Page should navigated to review and place order screen",
								"Page should navigated to review and place order screen",
								"Page should navigated to review and place order screen", driver);
				
						if (envProperties.get("skipCheckout").contains(Utils.getCurrentEnv())) {
							Log.reference("Further verfication steps are not supported in current environment.");
							Log.testCaseResult();
							return;
						}
						
						//Step17
						OrderConfirmationPage orderPage = null;
						
						checkoutPg.clickOnPlaceOrder();
						Log.message(i++ +". Clicked place order button", driver);
												
						if(checkoutPg.elementLayer.verifyPageElements(Arrays.asList("readyElement"),checkoutPg)) {
							Log.reference("By Using always approve card user can't place order", driver);
										
						} else {
							orderPage = new OrderConfirmationPage(driver).get();	
						
							Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("readyElement"), orderPage),
									"user should taken to the order confirmation page.",
									"user is taken to the order confirmation page.",
									"user is not taken to the order confirmation page.",driver);
							
							Log.softAssertThat(orderPage.elementLayer.verifyPageElements(Arrays.asList("OrderDetailsSection"), orderPage),
									"the order details should be displayed",
									"the order details is displayed",
									"the order details is not displayed",driver);
			
							if(Utils.isMobile()) {
								orderPage.clickOnViewDetailsMobile();
								Log.message("Clicked on the view details link in mobile view", driver);
							}
							
							Log.softAssertThat(orderPage.elementLayer.verifyNumberMaskedWithSpecifiedDigits("lblEnteredCardNo", 4, orderPage),
									"The last 4 digits of the PLCC card should be be displayed.",
									"The last 4 digits of the PLCC card is be displayed.",
									"The last 4 digits of the PLCC card is not displayed.", driver);
						}
					}	
				} else {
					Log.reference("Approval modal is not displayed hence can't proceed further", driver);
				}
			} else {
				Log.fail("PLCC Step-1 overlay is not displayed with ALWAYS APPROVE address data, Futher verification is not proceeded");
			}
			
			Log.testCaseResult();
		} // Ending try block
		catch(Exception e) {
			Log.exception(e, driver);
		} // Ending catch block
		finally {
			Log.endTestCase(driver);
		}// Ending finally
	}
}