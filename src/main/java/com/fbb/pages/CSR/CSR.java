package com.fbb.pages.CSR;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.support.Brand;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;

public class CSR extends LoadableComponent<CSR> {

	private final WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	String urlCSR;

	//***********************************************************
	//  	WebElement description - common
	//***********************************************************

	@FindBy(css = "#table1")
	WebElement divMain;

	@FindBy(name = "Login")
	WebElement loginUserName;

	@FindBy(name = "Password")
	WebElement loginPassword;

	@FindBy(name = "btnLogin")
	WebElement submit;

	@FindBy(name = "OrderNumber")
	WebElement orderNumber;
	
	@FindBy(name = "BasketLookupID")
	WebElement basketId;
	
	@FindBy(name = "IParcelTrackingID")
	WebElement trackingID;

	@FindBy(css = ".submit_button_search[value='Lookup']")
	WebElement lookUPButton;

	@FindBy(css = ".report_table")
	WebElement tblReport;
	
	@FindBy(css = "label>input[name='brand']")
	List<WebElement> lstBrandRadioSelectors;

	//***********************************************************

	/**
	 * constructor of the class
	 * @param driver : Webdriver
	 * 
	 */
	public CSR(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, divMain))) {
			Log.fail("CSR is not loaded", driver);
		}
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To log in as CSR
	 * @param userName - CSR login/BM email
	 * @param password - CSR login/BM password
	 * @throws Exception
	 */
	public void loginCSR(String userName, String password) throws Exception{
		BrowserActions.typeOnTextField(loginUserName, userName, driver, "User name");
		BrowserActions.typeOnTextField(loginPassword, password, driver, "Password");
		BrowserActions.clickOnElementX(submit, driver, "Login button");
		
		if(!Utils.waitForElement(driver, tblReport))
			Log.fail("Failed to log in as CSR", driver);
	}

	/**
	 * To search customer basket as CSR
	 * @param basketID - basket ID provided by customer
	 * @param brand - (Optional) select brand to search in
	 * <br> Search within current brand if none provided
	 * @throws Exception
	 */
	public CustomerSearchResultPage searchCustomerBasket(String basketID, Brand... brand) throws Exception{
		BrowserActions.typeOnTextField(basketId, basketID, driver, "BasketId");
		WebElement brandRadio = brand.length > 0 ? getBrandSelectionRadio(brand[0]) : getBrandSelectionRadio(Utils.getUCNavigatedBrand());
		BrowserActions.clickOnElementX(brandRadio, driver, "Brand radio selector");
		Log.event("Selected radio button for: " + brandRadio.getAttribute("value"));
		BrowserActions.clickOnElementX(lookUPButton, driver, "Lookup button");
		return new CustomerSearchResultPage(driver).get();
	}
	
	private WebElement getBrandSelectionRadio(Brand brand) throws Exception{
		for (WebElement brandRadio : lstBrandRadioSelectors) {
			String valueCurrentRadio = brandRadio.getAttribute("value");
			Brand brandCurrentRadio = UrlUtils.getBrandFromUrl(valueCurrentRadio);
			if(brand.equals(brandCurrentRadio))
				return brandRadio;
		}
		return lstBrandRadioSelectors.get(0);
	}
}
