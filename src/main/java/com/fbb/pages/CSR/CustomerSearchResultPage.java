package com.fbb.pages.CSR;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.support.Brand;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;

public class CustomerSearchResultPage extends LoadableComponent<CustomerSearchResultPage> {

	private final WebDriver driver;

	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	//***********************************************************
	//  	WebElement description - common
	//***********************************************************

	@FindBy(css = ".MainScreen")
	WebElement divMain;
	
	@FindBy(css = ".report_cell a")
	WebElement basketIdLnk;
	
	@FindBy(css = ".report_cell")
	List<WebElement> customerDetails;
	
	@FindBy(css = "label>input[name='brand']")
	List<WebElement> lstBrandRadioSelectors;

	//***********************************************************
	
	/**
	 * constructor of the class
	 * @param driver : Webdriver
	 */
	public CustomerSearchResultPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, divMain))) {
			Log.fail("CSR is not loaded", driver);
		}
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on basket search result link
	 * @param brand - (Optional) select brand to search in
	 * <br> Search within current brand if none provided
	 * @throws Exception
	 */
	public ShoppingBagPage clickBasketID(Brand... brand) throws Exception{
		WebElement brandRadio = brand.length > 0 ? getBrandSelectionRadio(brand[0]) : getBrandSelectionRadio(Utils.getUCNavigatedBrand());
		BrowserActions.clickOnElementX(brandRadio, driver, "Brand radio selector");
		Log.event("Selected radio button for: " + brandRadio.getAttribute("value"));
		try {
			BrowserActions.clickOnElementX(basketIdLnk, driver, "Basket ID link");
		} catch (Exception e) {
			Log.failsoft("Element not found in page!",driver);
		}	
		return new ShoppingBagPage(driver).get();
	}

	/**
	 * To verify guest customer details
	 * @param basketID
	 */
	public boolean verifyCustomerDetailsGuest(String basketID) {
		String actualelements[] = new String[customerDetails.size()];
		
		for (int i = 0; i < customerDetails.size(); i++) {
			actualelements[i] = customerDetails.get(i).getText();
		}
		
		if (!actualelements[0].equalsIgnoreCase(basketID)) {
			return false;
		}
		if (!actualelements[1].equalsIgnoreCase(" ")) {
			return false;
		}
		if (!actualelements[2].equalsIgnoreCase("Guest")) {
			return false;
		}
		if (!actualelements[3].equalsIgnoreCase("Guest")) {
			return false;
		}
		if (!actualelements[4].equalsIgnoreCase("No Customer Address")) {
			return false;
		}
		if (!actualelements[5].equalsIgnoreCase("")) {
			return false;
		}
		if (!actualelements[6].equalsIgnoreCase("")) {
			return false;
		}
		return true;
	}

	/**
	 * To verify guest customer details
	 * @param basketID
	 * @param firstName
	 * @param lastName
	 * @param userEmail
	 */
	public boolean verifyCustomerDetailsRegister(String basketID, String firstName, String lastName, String userEmail) {
		String actualelements[] = new String[customerDetails.size()];
		
		for (int i = 0; i < customerDetails.size(); i++) {
			actualelements[i] = customerDetails.get(i).getText();
		}
		
		if (!actualelements[0].equalsIgnoreCase(basketID)) {
			return false;
		}
		if (actualelements[1].equalsIgnoreCase(" ")) {
			return false;
		}
		if (!actualelements[2].equalsIgnoreCase(firstName)) {
			return false;
		}
		if (!actualelements[3].equalsIgnoreCase(lastName)) {
			return false;
		}
		if (actualelements[4].equalsIgnoreCase("No Customer Address")) {
			return false;
		}
		if (!actualelements[5].equalsIgnoreCase(userEmail)) {
			return false;
		}
		if (actualelements[6].equalsIgnoreCase("")) {
			return false;
		}
		
		return true;
	}
	
	private WebElement getBrandSelectionRadio(Brand brand) throws Exception{
		for (WebElement brandRadio : lstBrandRadioSelectors) {
			String valueCurrentRadio = brandRadio.getAttribute("value");
			Brand brandCurrentRadio = UrlUtils.getBrandFromUrl(valueCurrentRadio);
			if(brand.equals(brandCurrentRadio))
				return brandRadio;
		}
		return lstBrandRadioSelectors.get(0);
	}
}
