package com.fbb.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.footers.Curalate;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.HamburgerMenu;
import com.fbb.pages.headers.Headers;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.BuildProperties;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;

public class HomePage extends LoadableComponent<HomePage> {

	private String appURL;
	private WebDriver driver;
	private boolean isPageLoaded;
	public Headers headers;
	public Footers footers;
	public MiniCartPage minicart;
	public ElementLayer elementLayer;
	
	/**********************************************************************************************
	 ********************************* WebElements of Home Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = "#header input[name='q']")
	WebElement txtSearch;

	@FindBy(css = "div.pt_storefront")
	WebElement divMain;

	@FindBy(css = ".pt_storefront")
	WebElement readyElement;

	@FindBy(id = "ignore_this_warning")
	WebElement overRideLink;

	@FindBy(css = "div.promo-banner")
	WebElement txtPLCCPromoDesktop;
	
	@FindBy(css = "div.promo-banner")
	WebElement txtPLCCPromoMobile;
	
	@FindBy(css = ".footer-plcc-banner .right-section .text-section p")
	WebElement txtFooterMarketing;
	
	@FindBy(css = ".breadcrumb-element")
	WebElement lnkBreadcrumbHome;
	
	@FindBy(css = ".quick-order-breadcrumb.breadcrumb")
	WebElement lnkBreadcrumbCurrentPage;
	
	@FindBy(css = ".content-slot.slot-grid-header")
	WebElement divGridHeader;
	
	@FindBy(css = "div.you-may-like")
	WebElement certonaRecommendationModule;
	
	@FindBy(css = ".carousel-recommendations .slick-track")
	WebElement certonaRecommendations;
	
	@FindBy(css = ".gallery-link-container.curalate-footer-fix")
	WebElement curalateModule;
	
	@FindBy(xpath = "//div[@data-crl8-container-id='homepage']")
	WebElement curalateModuleAlt;
	
	@FindBy(css = "div#crl8-homepage-carousel")
	WebElement curalateCarousel;
	
	@FindBy(css = ".div-trending-section.product-recommendations")
	WebElement divTrendindNow;
	
	@FindBy(css = ".carousel-recommendations .slick-track .product-tile")
	WebElement certonaRecommendationsProductTile;
	
	@FindBy(css = ".carousel-recommendations .slick-track .product-name")
	WebElement certonaRecommendationsProductName;
	
	@FindBy(css = ".carousel-recommendations .slick-track .product-pricing")
	WebElement certonaRecommendationsProductPrice;
	
	@FindBy(css = ".you-may-like .product-tile .thumb-link")
	List<WebElement> lstTrendingNow;
	
	@FindBy(xpath = ".//div[@class='you-may-like']//span[contains(@class, 'price-product-sales-price')][not(contains(text(), 'N/A'))]")
	List<WebElement> lstTrendingNowPrices;
	
	@FindBy(css = ".slick-next.slick-arrow")
	WebElement divNextArrow;
	
	@FindBy(css = ".slick-next.slick-arrow.slick-disabled")
	WebElement divNextArrowDisable;
	
	@FindBy(css = ".breadcrumb")
	WebElement divBreadCrumb;
	
	@FindBy(css = ".primary-logo")
	WebElement divPrimaryLogo;
	
	@FindBy(css = ".home-content-container.home-content-1")
	WebElement divHomeInnerWrapper;
	
	@FindBy(xpath = "//img[contains(@alt,'Home Banner')]")
	WebElement hompagebanner;
	
	@FindBy(css = ".rm-module.rm-module-01-02-01")
	WebElement hompagebanner1;
	
	@FindBy(css = ".customer-service-info.info")
	WebElement CustomerService;
	
	@FindBy(css = "#main picture>picture")
	List<WebElement> lstFramesInFrame;
	
	@FindBy(css = "#main picture")
	List<WebElement> lstImageFrames;
	
	@FindBy(css = "#main picture>img")
	List<WebElement> lstImages;
	
	@FindBy(css = ".gallery-link-container.curalate-footer-fix")
	WebElement curalatePanel1;
	
	@FindBy(css = ".explicit_consent")
	WebElement mdlCookiesConsent;
	
	@FindBy(css = "#privacy_pref_optin")
	WebElement rdoYesCookiesConsent;
	
	@FindBy(css = "#consent_prompt_submit")
	WebElement btnSubmitCookiesConsent;
	
	/**********************************************************************************************
	 ********************************* WebElements of Home Page - Ends ****************************
	 **********************************************************************************************/

	/**
	 * constructor of the class
	 * @param driver : Webdriver
	 * @param url : UAT URL
	 */
	public HomePage(WebDriver driver, String url) {
		appURL = url;
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}// HomePage
	
	
	/**
	 * @param driver : webdriver
	 */
	public HomePage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);
	}// HomePage

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, divMain))) {
			Log.fail("Home Page did not open up. Site might be down.", driver);
		}
		
		headers = new Headers(driver).get();
		footers = new Footers(driver).get();
		elementLayer = new ElementLayer(driver);
		
		Log.event("Home Page Loaded Successfully.");
	}// isLoaded

	@Override
	protected void load() {
		try {
			if (appURL == null || appURL.equals("")) {
				appURL = UrlUtils.getProtectedWebSite();
			} else {
				appURL = UrlUtils.getProtectedWebSite(appURL);
			}
			appURL = UrlUtils.applyYottaSkip(appURL);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (!Utils.getCurrentEnv().equals("prod") && BuildProperties.getInitialLoadMainBrand()
					&& !Utils.getCurrentBrandShort().contains("rm")) {
			try {
				driver.get(UrlUtils.applyYottaSkip(UrlUtils.getProtectedWebSite(UrlUtils.getWebSite(Utils.getCurrentEnv(), Brand.rm))));
				BuildProperties.setInitialLoadMainBrand(false);
				new Headers(driver).get().chooseBrandFromHeader(Utils.getCurrentBrand());
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			driver.get(appURL);
		}
		isPageLoaded = true;
	}// load

	/**
	 * To get the page load status
	 * @return boolean 'true' or 'false'
	 * @throws Exception - Exception
	 */
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}
	
	/**
	 * To check the primary logo in the home page is loaded with correct brand
	 * @param brandName -
	 * @return true if loaded with correct brand else false
	 * @throws Exception -
	 */
	public boolean checkPrimaryLogoBrandName(Brand brandName) throws Exception {
		String linkFromLogo = divPrimaryLogo.findElement(By.cssSelector("a")).getAttribute("href");
		Brand brandFromUrl = UrlUtils.getBrandFromUrl(linkFromLogo);
		
		if (BrandUtils.matchBrand(brandName, brandFromUrl)) {
			String logoSrcArray[] = divPrimaryLogo.findElement(By.cssSelector("svg")).getAttribute("src").split("\\/");
			String brandFromLogo = logoSrcArray[logoSrcArray.length - 2];
			if(BrandUtils.matchBrand(brandName, Brand.fromConfiguration(brandFromLogo))) {
				return true;
			} else {
				Log.event("Brand: " + brandName.toString() + ", did not match primary logo");
				return false;
			}
		} else {
			Log.event("Brand: " + brandName.toString() + ", did not match the URL"); 
			return false;
		}
	}
	
	/**
	 * To get the PLCC promo text
	 * @return String - Promo Text on screen
	 * @throws Exception - Exception
	 */
	public String getPLCCPromoText()throws Exception{
		if(Utils.isDesktop() || Utils.isTablet())
			return BrowserActions.getText(driver, txtPLCCPromoDesktop, "PLLC Promotion Desktop ");
		else
			return BrowserActions.getText(driver, txtPLCCPromoMobile, "PLLC Promotion Mobile ");
	}

	/**
	 * Navigate to Pdp Page
	 * @param index - No of product to be selected (0 to size-1 values will be taken)
	 * @return Object - Pdp Page object
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToTrendingNowPDPByIndex(int index)throws Exception{
		BrowserActions.scrollToView(divTrendindNow, driver);
		int size = lstTrendingNowPrices.size();
		WebElement elem = null;
		if(size == 0) {
			Log.failsoft("Trending Now section does not have any product", driver);
			return null;
		} else if (index >= size) {
			index = Utils.getRandom(0, size);
		}
		
		elem = lstTrendingNowPrices.get(index);
		BrowserActions.scrollToView(elem, driver);
		
		WebElement prdTile = elem.findElement(By.xpath("../../..")).findElement(By.cssSelector(".product-tile .thumb-link"));
		
		if(!Utils.isMobile()) {
			while(!(Utils.waitForElement(driver, elem)) && !(Utils.waitForElement(driver, divNextArrowDisable))) {
				BrowserActions.clickOnElementX(divNextArrow, driver, "Next arrow");
			}
		}
		BrowserActions.clickOnElementX(prdTile, driver, "Product");
		Utils.waitForPageLoad(driver);
		
		return new PdpPage(driver).get();
	}
	
	/**
	 * To get Footer Marketing PLCC text
	 * @return String - footer PLCC text
	 * @throws Exception - Exception
	 */
	public String getFooterPLCCText()throws Exception{
		return BrowserActions.getText(driver, txtFooterMarketing, "PLLC Promotion Footer");
	}

	/**
	 * Navigate to my account section
	 * @return MyAccountPage
	 * @throws Exception - Exception
	 */
	public MyAccountPage navigateToMyAccount()throws Exception{
		if(Utils.isMobile()){
			try{
				HamburgerMenu hMenu = (HamburgerMenu)headers.openCloseHamburgerMenu("open");
				return hMenu.navigateToMyAccount();
			} catch(ClassCastException e) {
				Log.fail("Cannot Open up Hamburger Menu.", driver);
			}
		} else {
			return headers.navigateToMyAccount();
		}
		
		return new MyAccountPage(driver).get();
	}
	
	/**
	 * To verify if amount in marketing messages displayed in header and are same.
	 * @return boolean - true/false if amount are same/not same
	 * @throws Exception - Exception
	 */
	public boolean verifyHeaderFooterMarketingMessage()throws Exception{
		int headerPromo = StringUtils.getNumberInString(getPLCCPromoText().split("\\$")[1].split("\\s+")[0]);
		int footerMarketing = StringUtils.getNumberInString(getFooterPLCCText().split("\\$")[1].split("\\s+")[0]);
		
		return headerPromo == footerMarketing;
	}
	
	/**
	 * To test all images are loaded in page
	 * @return boolean - true/false if all images are loaded
	 * @throws Exception
	 */
	public boolean verifyAllImagesLoaded() throws Exception {
		int countMissingImage = 0, countGif = 0;
		for (WebElement pageImage : lstImages) {
			if (pageImage.isDisplayed()) {
				if (!(!(pageImage.getAttribute("src") == null) && pageImage.getAttribute("src").contains("gif"))
					&& !(!(pageImage.getAttribute("srcset") == null) && pageImage.getAttribute("srcset").contains("gif"))) {
					Log.event("Image dimention:: " + pageImage.getSize());
					if ((pageImage.getSize().width == 0 || pageImage.getSize().height == 0)) {
						countMissingImage++;
					}
				} else {
					countGif++;
				}
			}
		}
		if (countMissingImage > 0) {
			Log.event(countMissingImage + " image(s) not loaded out of " + (lstImages.size() - countGif));
			return false;
		} else
			return true;
	}
	
	/**
	 * T0 scroll to footer section
	 * @throws InterruptedException - 
	 */
	public Curalate scrollToCuralate() throws Exception {
		BrowserActions.scrollToViewElement(curalatePanel1, driver);
		return new Curalate(driver).get();
	}
	
	/**
	 * To verify Certona is loaded
	 * @return true/false if Certona is loaded
	 * <br> In Production returns true only if module content is loaded, else where returns true if module is loaded
	 * @throws Exception
	 */
	public boolean verifyCertonaLoaded() throws Exception {
		if (Utils.waitForElement(driver, certonaRecommendationModule, 10)) {
			if(Utils.getCurrentEnv().equals("prod")) {
				BrowserActions.scrollInToView(certonaRecommendationModule, driver);
				BrowserActions.scrollUp(driver);
				return Utils.waitForElement(driver, certonaRecommendations, 10);
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * To verify Curalate is loaded
	 * @return true/false if Curalate is loaded
	 * <br> In Production returns true only if module content is loaded, else where returns true if module is loaded
	 * @throws Exception
	 */
	public boolean verifyCuralateLoaded() throws Exception {
		if (Utils.waitForElement(driver, curalateModule, 10))  {
			if(Utils.getCurrentEnv().equals("prod")) {
				BrowserActions.scrollInToView(curalateModule, driver);
				return Utils.waitForElement(driver, curalateCarousel, 10);
			} else {
				return true;
			}
		} else if (Utils.waitForElement(driver, curalateModuleAlt, 10)) {
			if(Utils.getCurrentEnv().equals("prod")) {
				BrowserActions.scrollInToView(curalateModuleAlt, driver);
				return Utils.waitForElement(driver, curalateCarousel, 10);
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * To navigate to PLCC application page
	 * @return PlatinumCardApplication object
	 */
	public PlatinumCardApplication navigateToPLCCApplication() throws Exception {
		PlatinumCardApplication plccApplication;
		plccApplication = headers.clickOnPLCCApplyNow();
		if (plccApplication == null)
			plccApplication = footers.navigateToPlatinumCreditCard();
		return plccApplication;
	}
	
}// HomePage
