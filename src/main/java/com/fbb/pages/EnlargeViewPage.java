package com.fbb.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class EnlargeViewPage extends LoadableComponent<EnlargeViewPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public HomePage homePage;
	public ElementLayer elementLayer;

	/**********************************************************************************************
	 ********************************* WebElements of EnlargeViewPage Page ***********************************
	 **********************************************************************************************/


	@FindBy(css = "div[class='dialog-content ui-dialog-content ui-widget-content']")
	WebElement mdlEnlargeWindow;
	
	@FindBy(css = ".pdp-main.enlarge-container")
	WebElement readyElement;

	@FindBy(css = ".enlarge-container .product-name")
	WebElement txtProductName;

	@FindBy(css = ".ui-button-icon-primary.ui-icon.ui-icon-closethick")
	WebElement btnEnlargeViewClose_tablet;
	
	@FindBy(css = ".ui-button-icon.ui-icon.ui-icon-closethick")
	WebElement btnEnlargeViewClose;

	@FindBy(css = ".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.enlarge-dialog.ui-draggable .swatches.color")
	WebElement colorSwatches;

	@FindBy(css = ".enlarge-thumb-image .productthumbnail")
	WebElement alternateImages;

	@FindBy(css = ".enlarge-container .swatches.color li a img")
	List<WebElement> lstcolorSwatches;
	
	@FindBy(css = ".enlarge-container .swatches.color li")
	List<WebElement> lstcolorSwatchesLi;
	
	@FindBy(css = ".enlarge-container .swatches.color .selectable")
	List<WebElement> lstcolorSwatchesSelectable;

	@FindBy(css = ".enlarge-thumb-image .thumb img")
	List<WebElement> lstAlternateImages;
	
	@FindBy(css = ".enlarge-thumb-image a.thumbnail-link img")
	WebElement imgCurrentAlternateImage;

	@FindBy(css = ".pdp-main.enlarge-container .enlarge-hero-image img.productthumbnail")
	WebElement imgMainProduct;
	
	@FindBy(css = ".enlarge-hero-image .thumb:not(.background-thumb) .productthumbnail")
	WebElement imgMainProduct_Tablet;
	
	@FindBy(css = ".enlarge-thumb-image.slick-initialized.slick-slider .slick-list.draggable div .thumb.slick-slide.slick-current.slick-active")
	WebElement imgSelectedAlternateImage;

	@FindBy(css = ".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close.ui-state-focus")
	WebElement btnClose;

	@FindBy(css = ".enlarge-hero-image .productthumbnail")
	WebElement imgProductPrimaryImage;

	@FindBy(css = ".zoomContainer")
	WebElement zoomedImage;
	
	@FindBy(css = ".enlarge-hero-image .productthumbnail")
	WebElement zoomedImageTab;

	@FindBy(css = ".enlarge-dialog .ui-icon-closethick")
	WebElement enlargeImageCloseIcon;
	
	@FindBy(css = ".enlarge-hero-container.slick-initialized.slick-slider .slick-prev")
	WebElement prevImageArrowEnlarge;
	
	@FindBy(css = ".enlarge-hero-container.slick-initialized.slick-slider .slick-next")
	WebElement nextImageArrowEnlarge;
	
	@FindBy(css = ".enlarge-thumb-image .slick-current")
	WebElement imgEnlargeThumbNail;

	@FindBy(css = ".enlarge-right-sec")
	WebElement attributeSection;
	
	@FindBy(css = ".enlarge-container .float-left .productthumbnail")
	WebElement imgPrimaryImage;
	
	@FindBy(css = ".enlarge-container .float-right .productthumbnail")
	WebElement imgSelectColorSwatch;
	
	@FindBy(css = ".enlarge-thumb-image .thumb a img")
	List<WebElement> imgAlternateImage;
	/**********************************************************************************************
	 ********************************* WebElements of EnlargeViewPage Page - Ends ****************************
	 **********************************************************************************************/

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public EnlargeViewPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Enlarge View Page did not open up. Site might be down.", driver);
		}

		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	/**
	 * To get Y co-ordinate of color swatch
	 * @param index -
	 * @return Integer -
	 * @throws Exception -
	 */
	public int getCurrentLocationOfPdtByIndex(int index)throws Exception{		
		WebElement image = lstcolorSwatches.get(index);
		Point point = image.getLocation();		
		return point.getY();
	}

	/**
	 * To get color swatches count
	 * @throws Exception - Exception
	 * @return Integer - number of clor swatches
	 */
	public int getcolorSwatchesCount()throws Exception{
		return lstcolorSwatches.size();
	}

	/**
	 * To close the Enlarge View
	 * @throws Exception - Exception
	 */
	public void closeTheEnlargeView()throws Exception{
		BrowserActions.clickOnElementX(btnEnlargeViewClose, driver, "Enlarge view close button");
	}
	
	/**
	 * To get Alternate Images count
	 * @throws Exception - Exception
	 * @return Integer - number of alternate images
	 */
	public int getAlternateImagesCount()throws Exception{
		return lstAlternateImages.size();
	}

	/**
	 * To click on Shopping Cart tool tip
	 * @param Integer - index
	 * @throws Exception - Exception
	 * @return String - selected alt image
	 */
	public String getSelectedImageName(int index) throws Exception {
		String[] altImages;
		String img = "";
		if(lstAlternateImages.size()>0){
			BrowserActions.clickOnElementX(lstAlternateImages.get(index - 1), driver, "Alternate image");
			Utils.waitForPageLoad(driver);
			altImages=  lstAlternateImages.get(index - 1).getAttribute("src").split("\\/");
			img = altImages[altImages.length-1].split("\\.")[0];
		}
		Log.event("Alt image selected:: " + img);
		return img;
	}
	
	/**
	 * To click on Shopping Cart tool tip
	 * @throws Exception - Exception
	 * @return String - selected alt image
	 */
	public String getSelectedImageName() throws Exception {

		String[] altImages = imgCurrentAlternateImage.getAttribute("src").split("\\/");
		return altImages[altImages.length-1];
	}
	

	/**
	 * To get main product image source 
	 * @return String - image source
	 * @throws Exception - Exception
	 */
	public String getMainProductImage() throws Exception {
		String src = "";
		if(Utils.isTablet()) {
			src = imgMainProduct_Tablet.getAttribute("src");
		} else {
			src = imgMainProduct.getAttribute("src");
		}
		
		String imgMain = src.split("\\/")[src.split("\\/").length-1].split("\\.")[0];
		Log.event("Product main image:: " + imgMain);
		
		return imgMain;
	}
	
	/**
	 * To click on the previous image arrow
	 * @throws Exception - Exception
	 */
	public void clickOnPrevImageArrow() throws Exception {		
		BrowserActions.clickOnElementX(prevImageArrowEnlarge, driver, "Previous Arrow");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on the next image arrow
	 * @throws Exception - Exception
	 */
	public void clickOnNextImageArrow() throws Exception {		
		BrowserActions.clickOnElementX(nextImageArrowEnlarge, driver, "Next Arrow");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To check the next arrow in the enlarge page changes the image
	 * @return true if the image changes, else false
	 * @throws Exception - Exception
	 */
	public boolean checkThumbnailImageSelectedBasedOnNextButton() throws Exception {		
		String beforeNavigating = BrowserActions.getTextFromAttribute(driver, imgEnlargeThumbNail, "aria-describedby", "");
		clickOnNextImageArrow();
		String afterNavigating = BrowserActions.getTextFromAttribute(driver, imgEnlargeThumbNail, "aria-describedby", "");
		
		return beforeNavigating.equals(afterNavigating);
	}
	
	/**
	 * To check the previous arrow in the enlarge page changes the image
	 * @return true if the image changes, else false
	 * @throws Exception - Exception
	 */
	public boolean checkThumbnailImageSelectedBasedOnPrevButton() throws Exception {		
		String beforeNavigating = BrowserActions.getTextFromAttribute(driver, imgEnlargeThumbNail, "aria-describedby", "");
		clickOnPrevImageArrow();
		String afterNavigating = BrowserActions.getTextFromAttribute(driver, imgEnlargeThumbNail, "aria-describedby", "");
		
		return beforeNavigating.equals(afterNavigating);
	}

	/**
	 * To verify main and alternate image equals
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyMainAndAlternateImage()throws Exception{
		String imgAlternate = getSelectedImageName(1);
		String imgMain = getMainProductImage();

		return imgAlternate.contains(imgMain);
	}

	/**
	 * To click on alternate image 
	 * @param Integer - index
	 * @throws Exception - Exception
	 */
	public void clickOnAlternateImage(int index) throws Exception {
		if(lstAlternateImages.size() >= index) {
			BrowserActions.clickOnElementX(lstAlternateImages.get(index - 1), driver, "Select Color swatches");
		} else {
			Log.message("Give the correct alternate image to select");
		}
	}

	/**
	 * To get color swatch
	 * @param Integer - index
	 * @return String - swatch as string
	 * @throws Exception - Exception
	 */
	public String getSwatchColor(int index)throws Exception {
		String[] imgColor = lstcolorSwatches.get(index).getAttribute("src").split("\\|");
		String[] colorCodes = imgColor[imgColor.length-1].replace(".jpg", "").split("\\_");
		String colorCode = colorCodes[colorCodes.length-1];
		
		Log.event("Color Code :: " + colorCode);
		return colorCode;
	}
	
	/**
	 * To get color swatch
	 * @param Integer - index
	 * @return String - swatch as string
	 * @throws Exception - Exception
	 */
	public String getSelectedSwatchColor()throws Exception {
		WebElement selectedColorSwatch = driver.findElement(By.cssSelector(".enlarge-container .swatches.color li.selected a img"));
		String colorCode = selectedColorSwatch.getAttribute("src").split("swatch/")[1].split("\\.")[0];
		Log.event("Selected clor swatch :: " + colorCode);
		return colorCode;
	}
	
	/**
	 * To select color swatch
	 * @param Integer - index
	 * @throws Exception - Exception
	 */
	public void selectColor(int index)throws Exception {
		WebElement color = lstcolorSwatchesSelectable.get(index);
		if (!(BrowserActions.getTextFromAttribute(driver, color, "class", "").toLowerCase().contains("selected"))) {
			WebElement colorAnchor = color.findElement(By.cssSelector("a"));
			BrowserActions.javascriptClick(colorAnchor, driver, "color");
			Utils.waitForPageLoad(driver);
		}
	}

	/**
	 * To get alternate image color text
	 * @return String - alt image
	 * @throws Exception - Exception
	 */
	public String getAlternateImageColor(int index) throws Exception {
		String[] imgColor = lstAlternateImages.get(index).getAttribute("src").split("\\/");
		String[] colorCodes = imgColor[imgColor.length-1].replace(".jpg", "").split("\\_");
		String colorCode = colorCodes[colorCodes.length-2];
		
		Log.event("Color Code :: " + colorCode);
		return colorCode;
	}
	
	/**
	 * To get selected alternate image color text
	 * @return String - Color code
	 * @throws Exception - Exception
	 */
	public String getSelectedAlternateImageColor() throws Exception {
		String[] imgColor = imgCurrentAlternateImage.getAttribute("src").split("\\/");
		String[] colorCodes = imgColor[imgColor.length-1].replace(".jpg", "").split("\\_");
		String colorCode = colorCodes[colorCodes.length-2];
		
		Log.event("Color Code :: " + colorCode);
		return colorCode;
	}

	/**
	 * To close Enlarge View 
	 * @throws Exception - Exception
	 */
	public void closeEnlargeViewModal() throws Exception{		
		BrowserActions.clickOnElementX(enlargeImageCloseIcon, driver,
				"Enlarge modal close");
	}

	/**
	 * To get the current Y co-ordinate position of Alt image of given index
	 * @param Integer - index
	 * @return Integer - Y Point of Image
	 * @throws Exception - Exception
	 */
	public int getCurrentLocationOfAltImgByIndex(int index)throws Exception{		
		WebElement image = lstAlternateImages.get(index);
		Point point = image.getLocation();		
		return point.getY();
	}

	/**
	 * To mouse hover on primary image
	 * @throws Exception - Exception
	 */
	public void mouseHoverOnPrimaryImage() throws Exception{	
		if(Utils.isDesktop()){
			BrowserActions.mouseHover(driver, imgProductPrimaryImage);
			BrowserActions.mouseHover(driver, imgProductPrimaryImage);
		}
		else{
			BrowserActions.moveToElementJS(driver, imgProductPrimaryImage);
			Actions action = new Actions(driver);
			action.moveToElement(imgProductPrimaryImage).doubleClick().build();
		}
	}

	/**
	 * To get alt image slick index
	 * @return Integer - index
	 * @throws Exception - Exception
	 */
	public int getAltImgSlickIndex()throws Exception{
		int getSlickSlideIndex = Integer.parseInt(imgSelectedAlternateImage.getAttribute("data-slick-index"));
		Log.event("Slick Slider Index :: " + getSlickSlideIndex);
		return getSlickSlideIndex;
	}
	
	public void mouseHoverOnAltImage(int index)throws Exception{
		BrowserActions.mouseHover(driver, lstAlternateImages.get(index-1), index + "th Alt Image");
	}
	
	/**
	 * To get color code of product primary image
	 * @return String - Primary image
	 * @throws Exception - Exception
	 */
	public String getPrimaryImgColorCode() throws Exception {
		String imgPath[] = imgPrimaryImage.getAttribute("src").trim().split("\\/");
		String imgCode = imgPath[imgPath.length - 1].split(".jpg")[0].replace(".jpg", "");
		return imgCode;
	}
	
	/**
	 * To get color code of selected color swatch image
	 * @return String - color swatch
	 * @throws Exception - Exception
	 */
	public String getSelectedColorCode() throws Exception {
		String imgPath[] = imgSelectColorSwatch.getAttribute("src").trim().split("\\/");
		String imgCode = imgPath[imgPath.length - 1].split("\\_")[1].replace(".jpg", "");
		return imgCode;
	}
	
	/** 
	 * To select alternate image and get the color code of selected alternate image
	 * @param index - index of the image to be select
	 * @return String - image color code
	 * @throws Exception - Exception
	 */
	public String selectAltImgAndGetColorCode(int index) throws Exception {
		int size = imgAlternateImage.size();
		String imgCode = "";
		if (size >= index) {
			BrowserActions.clickOnElementX(imgAlternateImage.get(index-1), driver, "Alternate image");
			Utils.waitForPageLoad(driver);
			String imgPath[] = imgAlternateImage.get(index).getAttribute("src").trim().split("\\/");
			imgCode = imgPath[imgPath.length - 1].split("\\_")[1].replace(".jpg", "");
		}
		return imgCode;
	}
	
	/**
	 * To verify zoomed image
	 * @return boolean - true/false if zoom is working
	 * @throws Exception - Exception
	 */
	public boolean verifyZoomedImage() throws Exception{
		List<WebElement> lstZoomContainers = driver.findElements(By.cssSelector(".zoomContainer.js-zoom-container"));
		Log.event("No.of Zoom containers :: " + lstZoomContainers.size());
		for(WebElement element : lstZoomContainers) {
			if(element.getCssValue("display").contains("block")) {
				WebElement zoomedImage = element.findElement(By.cssSelector(".zoomWindow"));
				if(zoomedImage.getCssValue("display").contains("block"))
					return true;
			}
		}
		
		Log.event("There are no zoom containers visible in current page.");
		return false;
	}
	
	/**
	 * To verify initial image mapping
	 * @return boolean - true/false if images are mapped correctly
	 * @throws Exception - Exception
	 */
	public boolean verifyInitalImageMapping() throws Exception{
		String mainImgSrc[] = driver.findElement(By.cssSelector("#enlargecontainer .thumb img")).getAttribute("src").split("\\/");
		String firstAltImgSrc[] = lstAlternateImages.get(0).getAttribute("src").split("\\/");
		
		String mainImgsrc = mainImgSrc[mainImgSrc.length-1].split("\\.")[0];
		String firstAltImgsrc = firstAltImgSrc[firstAltImgSrc.length-1].split("\\.")[0];
		
		Log.event("Main Image Code :: " + mainImgsrc);
		Log.event("First Alt Image Code :: " + firstAltImgsrc);
		
		return mainImgsrc.equals(firstAltImgsrc);
	}
	
	/**
	 * To verify selected color swatch and main image displayed corresponds
	 * @return boolean - true/false if swatch and main image corresponds
	 * @throws Exception - Exception
	 */
	public boolean verifySwatchAndMainImage() throws Exception{
		String imgMain = getMainProductImage();
		String imgSwatchSelected = getSelectedSwatchColor();
		return imgMain.split("_m")[0].equals(imgSwatchSelected.split("_z")[0]);
	}
	
	public List<String> getColorsAsList()throws Exception{
    	List<String> colors = new ArrayList<String>();
    	
    	List<WebElement> colorsEle = driver.findElements(By.cssSelector(".enlarge-container ul.swatches.color a"));
    	for(WebElement ele:colorsEle){
    		colors.add(ele.getAttribute("title").split(":")[1]);
    	}
    	
    	return colors;
    }

}
