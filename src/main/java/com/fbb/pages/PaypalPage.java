package com.fbb.pages;

import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;


public class PaypalPage extends LoadableComponent<PaypalPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(css = "div[id='main']")
	WebElement paypalContent;
	
	@FindBy(css = "#injectedUnifiedLogin")
	WebElement iframeParent;
	
	 @FindBy(css = ".textInput input#email")
	 WebElement emailId;
	 
	 @FindBy(css = "#splitEmail #btnNext")
	 WebElement btnPaypalNext;
	 
	 @FindBy(css = ".textInput input#password")
	 WebElement passwordId;
	 
	 @FindBy(name = "injectedUl")
	 WebElement iframe;

	 @FindBy(id = "btnLogin")
	 WebElement paypalSubmitButton;
	 
	 @FindBy(css = "div[class*='ShipTo_header'] span[class*='AddOption_textContent']")
	 WebElement changeAddress;
	 
	 @FindBy(css = "#login .intentFooter a.cancelLink")
	 WebElement lnkCancelPaypal;
	 
	 
	//================================================================================
	//			WebElements Declaration End
	//================================================================================

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public PaypalPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver, paypalContent, 60);
		if (isPageLoaded && !(Utils.waitForElement(driver, paypalContent))) {
			Log.fail("Paypal Page didn't open", driver);
		}
		
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}
	
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}
	
	/**
	 * To type credentials and Open Paypal Dashboard Page
 	 * @param username -
	 * @param password -
	 * @return PaypalConfirmationPage if the credentials are valid
	 * @throws Exception - Exception
	 */
	public PaypalConfirmationPage enterPayapalCredentials(String username,String password)throws Exception{
		
		BrowserActions.typeOnTextField(emailId, username, driver, "Entering the username");
		if(!Utils.waitForElement(driver, passwordId)){
			BrowserActions.clickOnElementX(btnPaypalNext, driver, "Next Button in Paypal Modal");
			Utils.waitForElement(driver, passwordId);
		}
		BrowserActions.typeOnTextField(passwordId, password, driver, "Entering the Password");
		BrowserActions.clickOnElementX(paypalSubmitButton, driver, "Login Button in Paypal Modal");
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver, changeAddress, 15);
		return new PaypalConfirmationPage(driver).get();
		
	}
	
	/**
	 * To cancel PayPal payment by closing PayPal popup window in desktop
	 * <br> Desktop only
	 * @return instance of CheckoutPage
	 * @throws Exception
	 */
	public CheckoutPage closePaypalWindow() throws Exception{
		String handlePayPalWindow = driver.getWindowHandle();
		Log.event("Base windows handle:: " + handlePayPalWindow);
		Set<String> openWindowsList = driver.getWindowHandles();
		Log.event("Number of open windows:: " + openWindowsList.size());
		if (openWindowsList.size() > 1) {
			Log.event("PayPal opened in separate window/tab.");
			String handleBaseWindow = null;
			for(String windowHandle : openWindowsList) {
				Log.event("Current windows handle:: " + windowHandle);
				if (!windowHandle.equals(handlePayPalWindow)) {
					handleBaseWindow = windowHandle;
				}
			}
			driver.close();
			driver.switchTo().window(handleBaseWindow);
			Log.event("Popup window handle:: " + handleBaseWindow);
		} else {
			Log.event("PayPal opened in same window/tab.");
			BrowserActions.clickOnElementX(lnkCancelPaypal, driver, "Cancel PayPal link");
			Utils.waitForPageLoad(driver, 15);
		}
		return new CheckoutPage(driver).get();
	}
	
	/**
	 * To cancel PayPal by clicking cancel link
	 * @return instance of CheckoutPage
	 * @throws Exception
	 */
	public CheckoutPage cancelPaypalByCancelLink() throws Exception {
		String handlePayPalWindow = driver.getWindowHandle();
		Log.event("Base windows handle:: " + handlePayPalWindow);
		Set<String> openWindowsList = driver.getWindowHandles();
		Utils.waitForElement(driver, lnkCancelPaypal);
		BrowserActions.clickOnElementX(lnkCancelPaypal, driver, "Cancel PayPal link");
		Utils.waitForPageLoad(driver);
		if(Utils.isDesktop()){
			Utils.switchWindows(driver, Utils.getWebSite(), "url", "false");
			
			String handleBaseWindow = null;
			for(String windowHandle : openWindowsList) {
				Log.event("Current windows handle:: " + windowHandle);
				if (!windowHandle.equals(handlePayPalWindow)) {
					handleBaseWindow = windowHandle;
				}
			}
			driver.switchTo().window(handleBaseWindow);
		}
		Utils.waitForPageLoad(driver, 15);
		return new CheckoutPage(driver).get();
	}
	
}
