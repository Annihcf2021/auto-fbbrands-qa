package com.fbb.pages.footers;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.PdpPage;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class Curalate extends LoadableComponent<Curalate> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(css = ".pt_gallery")
	WebElement divCuralateLandingPage;
	
	@FindBy(css = "#curalate-carousel")
	List<WebElement> curalatePanel;
	
	@FindBy(css = ".module-studio-container")
	WebElement curalatePanel1;
	
	@FindBy(css = ".footer-container.footer-top")
	WebElement footerSection;
	
	@FindBy(css = ".homepage-seo")
	WebElement txtSEO;
	
	@FindBy(css = "div#crl8-homepage-carousel")
	WebElement slotCustomerPicture;
	
	@FindBy(css = "#crl8-homepage-carousel-modal>div>div")
	WebElement popupcustomerEnlargeModal;
	
	@FindBy(css = ".homepage-carousel-item img")
	List<WebElement> customerPicture;
	
	@FindBy(css = "button.sc-1007v4b-0")
	WebElement prevArrow;
	
	@FindBy(css = "button.guodei-0")
	WebElement nextArrow;
	
	@FindBy(css = "div.fade-enter-done")
	WebElement popupCustomerEnlargeModal;
	
	@FindBy(css = "div#crl8-homepage-carousel-modal >div >div button[aria-label*='Close']")
	WebElement closeCustomerEnlargeModal;
	
	@FindBy(css = "div.fade-enter-done .sc-6yasvi-0.bAXLug .homepage-carousel-product img")
	List<WebElement> divProductImageInGetThisLook;
	
	@FindBy(css = ".sc-1glg9ch-3")
	WebElement customerNameID;
	
	@FindBy(xpath = ".//div[contains(text(), '@')]")
	WebElement customerInstagramID;
	
	@FindBy(css = "div#crl8-homepage-carousel-modal picture")
	WebElement customerMainImage;
	
	@FindBy(css = ".iGvycc")
	WebElement heartNoOfLikes;
	
	@FindBy(xpath = ".//button[@aria-label='Share on Facebook']/parent::div")
	WebElement divSocialIcon;
	
	@FindBy(css = "button[aria-label*='Share on']")
	List<WebElement> linkSocialIcon;
	
	@FindBy(css = "button[aria-label^='Report']")
	WebElement linkReportMedia;
	
	@FindBy(css = "div[aria-describedby*='crl8-homepage-carousel-report-text']")
	WebElement divReportMediaWindow;
	
	@FindBy(css = "#crl8-homepage-carousel-report-email")
	WebElement emailReportMedia;
	
	@FindBy(css = "#crl8-homepage-carousel-report-reason")
	WebElement reasonReportMedia;
	
	@FindBy(xpath = ".//button[contains(text(), 'Report')]")
	WebElement buttonReportMedia;
	
	@FindBy(xpath = ".//button[contains(text(), 'Cancel')]")
	WebElement buttonReportCancel;
	
	@FindBy(xpath = ".//div[@aria-describedby='crl8-homepage-carousel-report-text']//div[contains(text(), 'Success')]")
	WebElement divThankYouReportMediaWindow;
	
	@FindBy(xpath = ".//div[@aria-describedby='crl8-homepage-carousel-report-text']//button[@aria-label='Close Report Media']")
	WebElement closeThankYouReportMediaWindow;
	
	@FindBy(css = "div.homepage-carousel-product")
	WebElement storefrontImage;
	
	@FindBy(css = ".cta-btn.hurme-bold.fs14.uppercase.letter-spacing-2")
	WebElement buttonViewGallery;
	
	@FindBy(xpath = "//button[contains(text(), 'SUBMIT YOUR PHOTO')]")
	WebElement btnSubmitYourPhoto;
	
	@FindBy(css = ".gallery-dialog-open")
	WebElement termsAndCondition;
	
	@FindBy(css = ".gallery-masonry-item")
	WebElement slotCustomerImage;
	
	@FindBy(xpath = ".//button[contains(text(), 'Load More')]")
	WebElement btnLoadMore;
	
	@FindBy(xpath = "//div[contains(@id, 'gallery-masonry-uploader-modal-header')]/parent::div")
	WebElement popupSelectYourContent;
	
	@FindBy(xpath = ".//label[contains(text(), 'browse files')]//parent::div //input")
	WebElement lnkBroweFiles;
	
	@FindBy(css = "input[placeholder*='Add a caption']")
	WebElement addCaption;
	
	@FindBy(xpath = ".//button[contains(text(), 'Next')]")
	WebElement btnNext;
	
	@FindBy(xpath = "//input[contains(@id,'userForm-displayName')]")
	WebElement fieldDisplayName;
	
	@FindBy(xpath = "//input[contains(@id,'userForm-emailAddress')]")
	WebElement fieldEmail;
	
	@FindBy(xpath = "//input[contains(@id,'userForm-termsAndConditions')]")
	WebElement checkboxTermsAndCondition;
	
	@FindBy(xpath = ".//button[contains(text(), 'Submit')]")
	WebElement btnSubmit;
	
	@FindBy(xpath = ".//div[contains(text(), 'Your content was uploaded successfully.')]/parent::*")
	WebElement divSuccessModal;
	
	@FindBy(css = "button[aria-label*='Close media uploader']")
	WebElement closeSuccessModal;
	
	//================================================================================
	//			WebElements Declaration End
	//================================================================================

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public Curalate(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if(isPageLoaded) {		
			if(curalatePanel.size()>1){
				if (isPageLoaded && !(Utils.waitForElement(driver, curalatePanel.get(0)) || Utils.waitForElement(driver, curalatePanel.get(1)))) {
					Log.fail("Curalate Panel didn't display", driver);
				}
			}
			else{
				if (isPageLoaded && !(Utils.waitForElement(driver, curalatePanel1))){
					Log.fail("Curalate Panel didn't display", driver);
				}
			}
			elementLayer = new ElementLayer(driver);
		}
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
		//Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To verify the mouse hover by index
	 * @param int - index of product to hover mouse on  
	 * @throws Exception -Exception
	 */
	public void mouseHoverOnPictureByIndex(int index) throws Exception {
		WebElement picTile = customerPicture.get(index-1);		
		BrowserActions.mouseHover(driver, picTile);
	}
	
	/**
	 * To click on customer picture by index
	 * @param int - index of product to hover mouse on  
	 * @throws Exception -Exception
	 */
	public void clickOnPictureByIndex(int index) throws Exception {	
		WebElement customerPictureElement = customerPicture.get(index-1);
		BrowserActions.mouseHover(driver, customerPictureElement);
		BrowserActions.clickOnElementX(customerPictureElement, driver, "Customer picture");
	}
	
	/**
	 * To click on Next Arrow
	 * @param  
	 * @throws Exception -Exception
	 */
	public void clickOnNextArrow() throws Exception {
		BrowserActions.clickOnElement(nextArrow, driver, "Next Arrow");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on Next Arrow
	 * @param  
	 * @throws Exception -Exception
	 */
	public void clickOnPreviousArrow() throws Exception {
		BrowserActions.clickOnElement(prevArrow, driver, "Next Arrow");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To navigate to first picture not shown by Instagram
	 * @throws Exception
	 */
	public boolean navigateToNonInstagramUpload() throws Exception {
		if(!Utils.waitForElement(driver, customerInstagramID)) {
			return true;
		} else if(Utils.waitForElement(driver, nextArrow)) {
			while(Utils.waitForElement(driver, customerInstagramID) && Utils.waitForElement(driver, nextArrow)) {
				Log.event("ID:: " + customerInstagramID.getText());
				clickOnNextArrow();
			}
			return !Utils.waitForElement(driver, customerInstagramID);
		} else {
			Log.event("Non instagram picture not found.");
			return false;
		}
		
	}
	
	/**
	 * To navigate to picture shown by Instagram
	 * @throws Exception
	 */
	public boolean navigateToInstagramUpload() throws Exception {
		if(Utils.waitForElement(driver, customerInstagramID)) {
			return true;
		} else if(Utils.waitForElement(driver, btnNext)){
			while(!Utils.waitForElement(driver, customerInstagramID) && Utils.waitForElement(driver, btnNext)) {
				clickOnNextArrow();
			}

			return Utils.waitForElement(driver, customerInstagramID);
		} else {
			Log.event("No Instagram uploaded image found.");
			return false;
		}
	}
	
	/**
	 * To click on Close icon of Customer Enlarge modal
	 * @param  
	 * @throws Exception -Exception
	 */
	public void closeCustomerEnlargeModal() throws Exception {
		if(Utils.waitForElement(driver, popupCustomerEnlargeModal)) {
			BrowserActions.clickOnElement(closeCustomerEnlargeModal, driver, "Close icon");
			Utils.waitForPageLoad(driver);
		}
	}
	
	/**
	 * To click on Heart link
	 * @return Boolean - true - if heart link clickable
	 * <br>
	 * false - if heart link not clickable 
	 * @throws Exception -Exception
	 */
	public Boolean clickOnHeart() throws Exception {
		BrowserActions.clickOnElement(heartNoOfLikes, driver, "Heart icon");
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		Utils.waitForPageLoad(driver);
		if(tabs.size()>1) {
			driver.switchTo().window(tabs.get(0));
			return true;
		} else {
			return false;
		}	
	}
    
	/**
	* To click on product picture by index below Get this look header in curalate enlarge window
	* @param int - index of product to click on
	* @throws Exception -Exception
	*/
	public PdpPage clickOnPrdImageBelowGetThisLookByIndex(int index) throws Exception {
		WebElement picTile = divProductImageInGetThisLook.get(index-1);
		BrowserActions.mouseHover(driver, picTile);
		BrowserActions.clickOnElementX(picTile, driver, "Product image below Get this look heading");
		return new PdpPage(driver).get();
	}
	
    /**
	 * To click on Report media link
	 * @param  
	 * @throws Exception -Exception
	 */
	public void clickReportMediaLink() throws Exception {
		BrowserActions.clickOnElementX(linkReportMedia, driver, "Report Media Link");
		Utils.waitForElement(driver, divReportMediaWindow);
	}
	
	/**
	 * To type the email address into email in Report media window
	 * @param txtToType - 
	 * @throws Exception - Exception
	 */
	public void typeInEmailReportMedia(String txtToType)throws Exception{
		BrowserActions.typeOnTextField(emailReportMedia, txtToType, driver, "Email in Report media window ");
	}
	
	/**
	 * To type the reason in Report media window
	 * @param txtToType - 
	 * @throws Exception - Exception
	 */
	public void typeInReasonReportMedia(String txtToType)throws Exception{
		BrowserActions.typeOnTextField(reasonReportMedia, txtToType, driver, "Reason in Report media window");
	}
	
	/**
	 * To click on Report media link
	 * @param  
	 * @throws Exception -Exception
	 */
	public void clickOnReportButtonInReportMediaWindow() throws Exception {
		BrowserActions.clickOnElement(buttonReportMedia, driver, "Report button");
		Utils.waitForElement(driver, divThankYouReportMediaWindow);
	}
	
	/**
	 * To close Thank you modal of Report modal
	 * @param  
	 * @throws Exception -Exception
	 */
	public void closeThankYouModal() throws Exception {
		BrowserActions.clickOnElement(closeThankYouReportMediaWindow, driver, "Close icon");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on View Gallery
	 * @param  
	 * @throws Exception -Exception
	 */
	public void clickOnViewGallery() throws Exception {
		BrowserActions.scrollToView(buttonViewGallery, driver);
		BrowserActions.clickOnElement(buttonViewGallery, driver, "View Gallery");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on Submit your photo
	 * @param  
	 * @throws Exception -Exception
	 */
	public void clickOnSubmitYourPhoto() throws Exception {
		BrowserActions.clickOnElementX(btnSubmitYourPhoto, driver, "Submit your photo");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To upload a photo
	 * @param  
	 * @throws Exception -Exception
	 */
	public void uploadPhoto(String imagePath) throws Exception {
		String userDir = System.getProperty("user.dir");
		BrowserActions.uploadImage(lnkBroweFiles, userDir + imagePath, driver, "Upload button");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To type the reason in Report media window
	 * @param txtToType - 
	 * @throws Exception - Exception
	 */
	public void typeInAddCaption(String txtToType)throws Exception {
		BrowserActions.typeOnTextField(addCaption, txtToType, driver, "Add caption");
	}
	
	/**
	 * To click on Next button
	 * @param  
	 * @throws Exception -Exception
	 */
	public void clickOnNextInSelectYourContent() throws Exception {
		BrowserActions.clickOnElement(btnNext, driver, "Next button");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To type the email in Select Your Content window
	 * @param txtToType - 
	 * @throws Exception - Exception
	 */
	public void typeInEmailSelectYourContent(String txtToType)throws Exception {
		BrowserActions.typeOnTextField(fieldEmail, txtToType, driver, "Email in Select Your Content");
	}
	
	/**
	 * To type the Display Name in Select Your Content window
	 * @param txtToType - 
	 * @throws Exception - Exception
	 */
	public void typeInNameSelectYourContent(String txtToType)throws Exception {
		BrowserActions.typeOnTextField(fieldDisplayName, txtToType, driver, "Display Name in Select Your Content");
	}
	
	/**
	 * To select/Unselect Terms and Condition checkbox 
	 * @param  
	 * @throws Exception -Exception
	 */
	public void SelectCheckboxTermsAndCondition(String option) throws Exception {
		BrowserActions.selectRadioOrCheckbox(checkboxTermsAndCondition, option, driver);
	}
	
	/**
	 * To click on Submit button
	 * @param  
	 * @throws Exception -Exception
	 */
	public void clickOnSubmitButton() throws Exception {
		BrowserActions.clickOnElement(btnSubmit, driver, "Submit button");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on Close icon of Customer Enlarge modal
	 * @param  
	 * @throws Exception -Exception
	 */
	public void closeSuccessModal() throws Exception {
		BrowserActions.clickOnElement(closeSuccessModal, driver, "Close icon");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on Facebook
	 * @param  
	 * @throws Exception -Exception
	 */
	public void clickOnFacebook() throws Exception {
		String winHandleBefore = driver.getWindowHandle();
		BrowserActions.clickOnElement(linkSocialIcon.get(0), driver, "Facebook");
		Utils.waitForPageLoad(driver);
		driver.switchTo().window(winHandleBefore);
        Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on Twitter
	 * @param  
	 * @throws Exception -Exception
	 */
	public void clickOnTwitter() throws Exception {
		String winHandleBefore = driver.getWindowHandle();
		BrowserActions.clickOnElement(linkSocialIcon.get(1), driver, "Twitter");
		Utils.waitForPageLoad(driver);
		driver.switchTo().window(winHandleBefore);
        Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on Pinterest
	 * @param  
	 * @throws Exception -Exception
	 */
	public void clickOnPinterest() throws Exception {
		String winHandleBefore = driver.getWindowHandle();
		BrowserActions.clickOnElement(linkSocialIcon.get(2), driver, "Pinterest");
		Utils.waitForPageLoad(driver);
		driver.switchTo().window(winHandleBefore);
        Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To get Customer Name
	 * @param  
	 * @throws Exception -Exception
	 */
	public String getCustomerNameID() throws Exception {
		if(Utils.waitForElement(driver, customerNameID)) {
			return BrowserActions.getText(driver, customerNameID, "Customer Name ID");
		} else {
			Log.event("There is no customer name ID.");
			return null;
		}
	}
	
	/**
	 * To get Alt text for displayed customer image
	 * @return alt text
	 * @throws Exception
	 */
	public String getCustomerImageText() throws Exception {
		if(Utils.waitForElement(driver, customerMainImage)) {
			String altText = BrowserActions.getTextFromAttribute(driver, customerMainImage, "alt", "Customer Image");
			return altText;
		} else {
			Log.event("Customer Image not displayed.");
			return null;
		}
	}

}
