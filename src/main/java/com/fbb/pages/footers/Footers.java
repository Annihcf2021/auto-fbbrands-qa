package com.fbb.pages.footers;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.GiftCardPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlatinumCardApplication;
import com.fbb.pages.PlatinumCardLandingPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.WishlistLoginPage;
import com.fbb.pages.account.CatalogPreferencePage;
import com.fbb.pages.account.PreApprovedPlatinumCardPage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.customerservice.ContactUsPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.customerservice.EmailSubscriptionGuest;
import com.fbb.pages.customerservice.TrackOrderPage;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class Footers extends LoadableComponent<Footers> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");

	private static final String FOOTER_SECTION = "footer ";
	private static final String FOOTER_NAV = FOOTER_SECTION + "nav ";
	private static final String FOOTER_HEADING = FOOTER_NAV + ".footer-heading ";
	
	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(css = ".footer-container.footer-top")
	WebElement footerPanel;
	
	@FindBy(css = "body footer")
	WebElement footerReadyElement;
	
	@FindBy(css = ".footer-top .footer-email-signup-message")
	WebElement emailSignupMessage;
	
	@FindBy(css = ".footer-top .footer-email-signup-message p")
	WebElement emailSignupMessageValues;
	
	@FindBy(css = ".pt_account.catalog-preferences")
	WebElement readyElementCatalogPreferences;
	
	@FindBy(css = ".pt_product-details")
    WebElement cntPdpContent;

	@FindBy(css = FOOTER_SECTION + "input[id*='dwfrm_emailsignup_email']")
	WebElement fldEmailSignUpFooter;
	
	@FindBy(css = FOOTER_SECTION + ".email-signup-footer-success-heading")
	WebElement lblEmailSignUpSuccessMsg;
	
	@FindBy(css = FOOTER_SECTION + "button[name='dwfrm_emailsignup_signup']")
	WebElement btnFooterEmailSignUp;
	
	@FindBy(css = FOOTER_SECTION + "span[id^='dwfrm_emailsignup_'][id$='error']")
	WebElement txtEmailSignUpError;
	
	@FindBy(css = FOOTER_HEADING + "a[href*='giftcard-landing']")
	WebElement lnkGiftCardLandingPage;
	
	@FindBy(xpath = "//footer//nav//a[text()='Gift Card']")
	WebElement lnkPhysicalGiftCard;
	
	@FindBy(css = ".product-tile .product-name")
    List<WebElement> prdNames;
	
	@FindBy(xpath = "//footer//nav//a[text()='e-Gift Card']")
	WebElement lnkEGiftCard;
	
	@FindBy(css = ".footer-social-icon .instagram>a")
	WebElement imgInstagram;
	
	@FindBy(css = ".footer-social-icon .pinterest>a")
	WebElement imgPinterest;
	
	@FindBy(css = ".footer-social-icon .facebook>a")
	WebElement imgFacebook;
	
	@FindBy(css = ".footer-social-icon .twitter>a")
	WebElement imgTwitter;
	
	@FindBy(css = ".footer-social-icon .youtube>a")
	WebElement imgYoutube;

	@FindBy(xpath = "//" + FOOTER_SECTION + "//a[contains(text(),'FAQ')]")
	WebElement lnkTopQuestions;
	
	@FindBy(css = "#bc-chat-container")
	WebElement mdlLiveChat;

	@FindBy(xpath = "//footer//a[contains(text(),'Contact Us')]")
	WebElement lnkContactUs;

	@FindBy(xpath = "//" + FOOTER_SECTION + "//a//div[contains(text(),'Live Chat')]")
	WebElement lnkLiveChat;

	@FindBy(xpath = "//" + FOOTER_SECTION + "//a[contains(text(),'Return & Exchanges')]")
	WebElement lnkReturnCenter;

	@FindBy(xpath = "//" + FOOTER_SECTION + "//a[contains(text(),'shipping & handling')]")
	WebElement lnkShippingAndHandling;

	@FindBy(xpath = "//" + FOOTER_SECTION + "//a[contains(text(),'International')]")
	WebElement lnkInternationalShipping;

	@FindBy(xpath = "//" + FOOTER_SECTION + "//a[contains(text(),'easy returns')]")
	WebElement lnkEasyReturns;
	
	@FindBy(css = ".footer_item ul li:nth-child(3) a[href*='/order']")
	WebElement lnkOrderHistory;

	@FindBy(css = FOOTER_HEADING + "a[href*='account']")
	WebElement lnkMyAccount;

	@FindBy(xpath = "//" + FOOTER_SECTION + "//a[contains(text(),'Order History')]")
	WebElement lnkOrderStatusUnderAccount;
	
	@FindBy(xpath = "//" + FOOTER_SECTION + "//a[contains(text(),'Track My Order')]")
	WebElement lnkTrackMyOrder;

	@FindBy(css = FOOTER_SECTION + "a[href*='wishlist']")
	WebElement lnkWishLists;

	@FindBy(xpath = "//" + FOOTER_SECTION + "//a[contains(text(),'Rewards')]")
	WebElement lnkRewardPoints;
	
	@FindBy(css = FOOTER_SECTION + "a[href*='about-us']")
	WebElement lnkAboutUs;

	@FindBy(css = FOOTER_SECTION + "a[href*='careers']")
	WebElement lnkFullBeautyBrandCareer;

	@FindBy(css = FOOTER_SECTION + "a[href*='company-profile']")
	WebElement lnkAboutFullBeautyBrands;

	@FindBy(css = "a>img[alt='fullbeauty']")
	WebElement imgFullbeauty;

	@FindBy(css = "a>img[alt='jessicalondon']")
	WebElement imgJessicaLondon;

	@FindBy(css = "a>img[alt='womanwithin']")
	WebElement imgWomanWithin;
	
	@FindBy(css = "a>img[alt*='ro'][alt*='man']")
	WebElement imgRoamans;

	@FindBy(css = "a>img[alt='ellos']")
	WebElement imgEllos;

	@FindBy(css = "a>img[alt='swimsuits']")
	WebElement imgSwimsuitsForAll;

	@FindBy(css = "a>img[alt='kingsize']")
	WebElement imgKingSize;

	@FindBy(css = "a>img[alt='brylanehome']")
	WebElement imgBrylaneHome;

	@FindBy(css = "a>img[alt='bco']")
	WebElement imgBCOutlet;
	
	@FindBy(css = ".brands-list a")
	List<WebElement> brandLinks;

	@FindBy(xpath = "//" + FOOTER_SECTION + "//a[contains(text(),'Privacy Policy')]")
	List<WebElement> lnkPrivacyPolicy;

	@FindBy(xpath = "//" + FOOTER_SECTION + "//a[contains(text(),'Terms of Use')]")
	List<WebElement> lnkTermsOfUse;

	@FindBy(xpath = "//" + FOOTER_SECTION + "//a[contains(text(),'California Transparency in Supply Chains Act')]")
	List<WebElement> lnkSupplyChainsAct;
	
	@FindBy(css = ".brands-list li>a")
	List<WebElement> lnkBrands;
	
	@FindBy(css = ".product-tile")
	List<WebElement> prdTile;
	
	@FindBy(css = ".footer-plcc .footer-plcc-banner .right-section.section .text-section p a[href*='apply']>span")
	WebElement lnkPlatinumCreditCard;
	
	@FindBy(css = ".footer-plcc .text-section p a[href*='Credit-SignInToPreApprove']>span")
	WebElement lnkPlatinumCreditCardAlt;
	
	@FindBy(css = FOOTER_HEADING + "a[href*='platinum-card']")
	WebElement lnkPlatinumCreditCardLanding;
	
	@FindBy(css = FOOTER_SECTION + ".text-section p a[href*='platinum']")
	WebElement lnkLearnMoreFooter;

	@FindBy(xpath = "//" + FOOTER_SECTION + "//a[contains(text(),'Email Subscription')]")
	WebElement lnkEmailSubscription;

	@FindBy(css = FOOTER_HEADING + "a[href*='help']")
	WebElement lnkCustomerService;

	@FindBy(xpath = "//footer//a[contains(text(),'Gift Card Balance')]")
	WebElement lnkGiftCardPage;
	
	@FindBy(css = ".footer-container.footer-down")
	WebElement footerContainer;
	
	@FindBy(css = ".text-section a[href$='/catalog-preferences']")
	WebElement lnkRequestCatalog;
	
	@FindBy(css = ".image-section a[href$='/catalog-preferences'] img")
	WebElement imgRequestCatalog;
	
	@FindBy(css = ".footer-plcc .footer-plcc-banner .right-section.section .text-section p")
	WebElement lnkRewardsFooter;
	
	@FindBy(css = "a.question")
	List<WebElement> qaSections;
	
	@FindBy(css = ".search-wishlist")
    WebElement divSearchWishlist;
	
	@FindBy(css = ".brand-logos")
    WebElement footerBrandLogo;
	
	@FindBy(css = ".brands-list")
	WebElement footerBrandLogoSection;
	
	@FindBy(css = ".footer-brand-logos")
	WebElement footerBrandSection;
	
	@FindBy(css = ".footer-plcc")
    WebElement footerPlcc;
	
	@FindBy(css = ".footer-down-inner .footer-links")
    WebElement footerLink;
	
	@FindBy(css = ".footer-legal-copyrights")
    WebElement footerCopyRights;
	
	@FindBy(css = ".brand-logos img")
	List<WebElement> brandLogoList;
	
	@FindBy(css = ".brands-list img")
	List<WebElement> lstfooterBrandLogos;
	
	@FindBy(css = ".footer-brand-logos img")
	List<WebElement> footerBrandList;
	
	@FindBy(xpath = "//a[text()='Sign In / Register']")
	WebElement linkSignIn;
	
	@FindBy(css = FOOTER_HEADING + "a[href*='catalog-preferences']")
	WebElement lnkCatalogPrefs;
	
	@FindBy(css = FOOTER_NAV + "a[href*='quick-order']")
	WebElement lnkCatalogOrderPage;

	//================================================================================
	//			WebElements Declaration End
	//================================================================================


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public Footers(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
		if (isPageLoaded && !(Utils.waitForElement(driver, footerReadyElement))){
			Log.fail("Footer Panel didn't display", driver);
		}
		
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		//Utils.waitForPageLoad(driver);
	}
	
	/**
	 * TO scroll to footer page
	 * @throws InterruptedException - 
	 */
	public void scrollToFooter() throws InterruptedException {
		BrowserActions.scrollToViewElement(footerPanel, driver);
	}
	
	/**
	 * To type the email address into email signup textbox
	 * @param txtToType - 
	 * @throws Exception - Exception
	 */
	public void typeInEmailSignUp(String txtToType)throws Exception{
		BrowserActions.typeOnTextField(fldEmailSignUpFooter, txtToType, driver, "Email SignUp in Footer ");
	}
	
	/**
	 * To check the sign up error message is displaying correct
	 * @return true if message is correct, else false
	 * @throws Exception -
	 */
	public boolean getErrorMessage()throws Exception{
		String error = "";
		error = BrowserActions.getText(driver, txtEmailSignUpError, "Signup error");
		
		return (error.toLowerCase().contains("please enter a valid email"));
	}
	
	/**
	 * To click on the Email SignUp button
	 * @throws Exception - Exception
	 */
	public void clickOnEmailSignUp()throws Exception{
		BrowserActions.clickOnElementX(btnFooterEmailSignUp, driver, "Email Sign Up ");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To get the error message from Email signup error text from footer
	 * @return String - Error Message
	 * @throws Exception - Exception
	 */
	public String getEmailSignUpErrMsgFooter()throws Exception{
		return Utils.waitForElement(driver, txtEmailSignUpError) ? 
				BrowserActions.getText(driver, txtEmailSignUpError, "Email SignUp Err ") :
					"Error Message not displayed!";
	}
	
	/**
	 * To navigate to Instagram
	 * @throws Exception - Exception
	 */
	public void navigateToInstagram()throws Exception{
		if(Utils.waitForElement(driver, imgInstagram)) {
			BrowserActions.clickOnElementX(imgInstagram, driver, "Instagram ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "instagram", "url", "");
		} else {
			Log.failsoft("Instagram link is not displayed", driver);
		}
	}

	/**
	 * To navigate to Pinterest
	 * @throws Exception - Exception
	 */
	public void navigateToPinterest()throws Exception{
		if(Utils.waitForElement(driver, imgPinterest)) {
			BrowserActions.clickOnElementX(imgPinterest, driver, "Pinterest ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "pinterest", "url", "");
		} else {
			Log.failsoft("Pinterest link is not displayed", driver);
		}
	}

	/**
	 * To navigate to FaceBook
	 * @throws Exception - Exception
	 */
	public void navigateToFaceBook()throws Exception{
		if(Utils.waitForElement(driver, imgFacebook)) {
			BrowserActions.clickOnElementX(imgFacebook, driver, "FaceBook ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "facebook", "url", "");
		} else {
			Log.failsoft("Facebook link is not displayed", driver);
		}
	}

	/**
	 * To navigate to Twitter
	 * @throws Exception - Exception
	 */
	public void navigateToTwitter()throws Exception{
		if (Utils.waitForElement(driver, imgTwitter)) {
		BrowserActions.clickOnElementX(imgTwitter, driver, "Twitter ");
		Utils.waitForPageLoad(driver);
		Utils.switchWindows(driver, "twitter", "url", "");
		} else {
			Log.failsoft("Twitter link is not displayed", driver);
		}
	}

	/**
	 * To navigate to Youtube
	 * @throws Exception - Exception
	 */
	public void navigateToYoutube()throws Exception{
		if(Utils.waitForElement(driver, imgYoutube)) {
			BrowserActions.clickOnElementX(imgYoutube, driver, "Youtube ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "youtube", "url", "");
		} else {
			Log.failsoft("Youtube link is not displayed", driver);
		}
	}

	/**
	 * To navigate to Top Questions
	 * @throws Exception - Exception
	 */
	public void navigateToTopQuestions()throws Exception{
		if (Utils.waitForElement(driver, lnkTopQuestions)) {
			BrowserActions.clickOnElementX(lnkTopQuestions, driver, "Top Questions ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("Top Questions link is not displayed", driver);
		}
	}

	/**
	 * To navigate to Contact Us
	 * @throws Exception - Exception
	 */
	public ContactUsPage navigateToContactUs()throws Exception{
		if(Utils.waitForElement(driver, lnkContactUs)) {
			BrowserActions.clickOnElementX(lnkContactUs, driver, "Contact Us ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("Contact Us link is not displayed", driver);
		}
		return new ContactUsPage(driver).get();
	}

	/**
	 * To navigate to Live chat
	 * @throws Exception - Exception
	 */
	public void navigateToLiveChat()throws Exception{
		if(Utils.waitForElement(driver, lnkLiveChat)) {
			BrowserActions.clickOnElementX(lnkLiveChat, driver, "Live Chat ");
			Utils.waitForElement(driver, mdlLiveChat);
		} else {
			Log.failsoft("Live chat link is not displayed", driver);
		}
	}
	
	/**
	 * To navigate to Return Center
	 * @throws Exception - Exception
	 */
	public void navigateToReturnCenter()throws Exception{
		if(Utils.waitForElement(driver, lnkReturnCenter)) {
			BrowserActions.clickOnElementX(lnkReturnCenter, driver, "Return Center ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("Return Center link is not displayed", driver);
		}
	}

	/**
	 * To navigate to Shipping and handling
	 * @throws Exception - Exception
	 */
	public void navigateToShippingAndHandling()throws Exception{
		BrowserActions.clickOnElementX(lnkShippingAndHandling, driver, "Shipping and handling ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to International Shipping 
	 * @throws Exception - Exception
	 */
	public void navigateToInternationalShipping()throws Exception{
		BrowserActions.clickOnElementX(lnkInternationalShipping, driver, "International Shipping ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to EasyReturns
	 * @throws Exception - Exception
	 */
	public void navigateToEasyReturns()throws Exception{
		BrowserActions.clickOnElementX(lnkEasyReturns, driver, "EasyReturns ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to "Order history" page
	 * @throws Exception - Exception
	 */
	public void navigateToOrderStatus()throws Exception{
		Utils.waitForElement(driver, lnkOrderHistory);
		BrowserActions.clickOnElementX(lnkOrderHistory, driver, "Order Status ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to Your Account
	 * @throws Exception - Exception
	 */
	public void navigateToYourAccount()throws Exception{
		if(Utils.waitForElement(driver, lnkMyAccount)) {
		BrowserActions.clickOnElementX(lnkMyAccount, driver, "My Account");
		Utils.waitForPageLoad(driver); 
		} else {
			Log.failsoft("Your Account is not displayed in page", driver);
		}
	}

	/**
	 * To navigate to Order Status under Account section
	 * @throws Exception - Exception
	 */
	public void navigateToOrderStatusUnderAccountSection()throws Exception{
		BrowserActions.clickOnElementX(lnkOrderStatusUnderAccount, driver, "Order Status ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to Wish lists
	 * @throws Exception - Exception
	 */
	public Object navigateToWishLists()throws Exception{
		if(Utils.waitForElement(driver, lnkWishLists)) {
			BrowserActions.clickOnElementX(lnkWishLists, driver, "Wish lists ");
		}
		Utils.waitForPageLoad(driver);
		if (Utils.waitForElement(driver, divSearchWishlist)) {
            return new WishlistLoginPage(driver).get();
        } else {
        	return new WishListPage(driver).get();
        }
	}

	/**
	 * To navigate to Reward Points
	 * @throws Exception - Exception
	 */
	public void navigateToRewardPoints()throws Exception{
		if(Utils.waitForElement(driver, lnkRewardPoints)) {
			BrowserActions.clickOnElementX(lnkRewardPoints, driver, "Reward Points ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("Reward Points is not displayed in page", driver);
		}
	}
	
	/**
	 * To navigate to PlatinumCreditCard
	 * @return PlatinumCreditCardPage
	 * @throws Exception - Exception
	 */
	public PlatinumCardApplication navigateToPlatinumCreditCard() throws Exception{
		BrowserActions.clickOnElementX(lnkPlatinumCreditCard, driver, "'Apply Now' link in Footer");
		Utils.waitForPageLoad(driver);
		return new PlatinumCardApplication(driver).get();
	}
	
	
	/**
	 * To navigate to PlatinumCreditCard Landing Page
	 * @return PlatinumCreditCardPage
	 * @throws Exception - Exception
	 */
	public PlatinumCardLandingPage navigateToPlatinumCreditCardLanding()throws Exception{
		try {
			BrowserActions.clickOnElementX(lnkPlatinumCreditCardLanding, driver, "Navigate to Platinum Credit Card Landing Page");
		} catch(Exception e) {
			Log.event(e.getLocalizedMessage());
			Log.event("Learn More link not found on page. Using workaround...");
			driver.navigate().to(Utils.getWebSite() + redirectData.get("platinumLanding"));
		}
		Utils.waitForPageLoad(driver);
		return new PlatinumCardLandingPage(driver).get();
	}
	
	
	/**
	 * To navigate to PlatinumCreditCard Apply page for Pre-Approved users
	 * @return PlatinumCreditCardPage
	 * @throws Exception - Exception
	 */
	public PreApprovedPlatinumCardPage navigateToPreApprovedPlatinumCreditCard()throws Exception{
		BrowserActions.clickOnElementX(lnkPlatinumCreditCard, driver, "Navigate to Platinum Credit Card");
		Utils.waitForPageLoad(driver);
		return new PreApprovedPlatinumCardPage(driver).get();
	}
	/**
	 * To navigate to PlatinumCreditCard
	 * @return PlatinumCreditCardPage
	 * @throws Exception - Exception
	 */
	
		
	public EmailSubscriptionGuest navigateToEmailSubscription()throws Exception{
		BrowserActions.clickOnElementX(lnkEmailSubscription, driver, "Navigate to Email Subscription");
		Utils.waitForPageLoad(driver);
		return new EmailSubscriptionGuest(driver).get();
	}
	
	/**
	 * To navigate to About Brand
	 * @param brandName - Brand name in two character
	 * @throws Exception - Exception
	 */
	public void navigateToAboutBrand() throws Exception {
		if (Utils.waitForElement(driver, lnkAboutUs)) {
			BrowserActions.clickOnElementX(lnkAboutUs, driver, "About Us");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("About Us link is not found.", driver);
		}
	}

	/**
	 * To navigate to FBB brand career
	 * @throws Exception - Exception
	 */
	public void navigateToFullBeautyBrandCareer()throws Exception{
		if(Utils.waitForElement(driver, lnkFullBeautyBrandCareer)) {
			BrowserActions.clickOnElementX(lnkFullBeautyBrandCareer, driver, "FBB brand career ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("Full Beauty Brand career is not displayed", driver);
		}
	}

	/**
	 * To navigate to About FBB brand
	 * @throws Exception - Exception
	 */
	public void navigateToAboutFullBeautyBrands()throws Exception{
		if(Utils.waitForElement(driver, lnkAboutFullBeautyBrands)) {
			BrowserActions.clickOnElementX(lnkAboutFullBeautyBrands, driver, "About FBB brand ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("About Full Beauty Brand is not displayed", driver);
		}
	}

	/**
	 * To navigate to Full Beauty brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToFullbeautyBrand()throws Exception{
		if(Utils.waitForElement(driver, imgFullbeauty)) {
			BrowserActions.clickOnElementX(imgFullbeauty, driver, "Full Beauty ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "fullbeauty", "url", "");
		} else {
			Log.failsoft("The Full Beauty brand is disabled", driver);
		}
	}

	/**
	 * To navigate to Jessica London brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToJessicaLondonBrand()throws Exception{
		if(Utils.waitForElement(driver, imgJessicaLondon)){
			BrowserActions.clickOnElementX(imgJessicaLondon, driver, "Jessica London ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "jessicalondon", "url", "");
		} else {
			Log.failsoft("The Jessica London brand is disabled", driver);
		}
	}
	
	/**
	 * To navigate to Physical gift card PDP
	 * @return PDPpage
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPhysicalGiftCardPDP()throws Exception{
		BrowserActions.clickOnElementX(lnkPhysicalGiftCard, driver, "Physical gift card");
		try {
			if (!Utils.waitForElement(driver, cntPdpContent)) {
				GiftCardPage gcLanding = navigateToGiftCardLandingPage();
				gcLanding.clickGiftCardButton();
			}
		} catch (NoSuchElementException e) {
			Log.failsoft("Physical Gift Card page don't have product!", driver);
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}
	

	 /**
	 * To navigate to E gift card PDP
	 * @return PDPpage
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToEGiftCardPDP()throws Exception{
		BrowserActions.clickOnElementX(lnkEGiftCard, driver, "E gift card");
		if (!(Utils.waitForElement(driver, cntPdpContent))) {
			GiftCardPage gcLanding = navigateToGiftCardLandingPage();
			gcLanding.clickEGiftCardButton();
		}
		
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}
	
	/**
	 * To click the brand image by index
	 * @param index -
	 * @throws Exception -
	 */
	public void navigateToNthBrand(int index)throws Exception{
		BrowserActions.clickOnElementX(lnkBrands.get(index-1), driver, "Jessica London ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to Woman Within brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToWomanWithinBrand()throws Exception{
		if(Utils.waitForElement(driver, imgWomanWithin)) {
		BrowserActions.clickOnElementX(imgWomanWithin, driver, "Woman Within ");
		Utils.waitForPageLoad(driver);
		Utils.switchWindows(driver, "womanwithin", "url", "");
		} else {
			Log.failsoft("The Woman Within brand is disabled", driver);
		}
	}

	/**
	 * To navigate to Roamans brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToRoamansBrand()throws Exception{
		if(Utils.waitForElement(driver, imgRoamans)) {
			BrowserActions.clickOnElementX(imgRoamans, driver, "Roamans ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "roamans", "url", "");
		} else {
			Log.failsoft("The Roamans brand is disabled", driver);
		}
	}

	/**
	 * To navigate to ellos brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToEllosBrand()throws Exception{
		if(Utils.waitForElement(driver, imgEllos)) {
			BrowserActions.clickOnElementX(imgEllos, driver, "Ellos ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "ellos", "url", "");
		} else {
			Log.failsoft("The Ellos brand is disabled", driver);
		}
	}

	/**
	 * To navigate to swimsuits brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToSwimsuitsForAllBrand()throws Exception{
		if(Utils.waitForElement(driver, imgSwimsuitsForAll)) {
			BrowserActions.clickOnElementX(imgSwimsuitsForAll, driver, "Swimsuits brand ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "swimsuitsforall", "url", "");
		} else {
			Log.failsoft("The Swim Suits brand is disabled", driver);
		}
	}

	/**
	 * To navigate to kingsize brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToKingSizeBrand()throws Exception{
		if(Utils.waitForElement(driver, imgKingSize)) {
			BrowserActions.clickOnElementX(imgKingSize, driver, "King Size brand ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "kingsizedirect", "url", "");
		} else {
			Log.failsoft("The King Size brand is disabled", driver);
		}
	}

	/**
	 * To navigate to Brylane Home brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToBrylaneHomeBrand()throws Exception{
		if (Utils.waitForElement(driver, imgBrylaneHome)) {
			BrowserActions.clickOnElementX(imgBrylaneHome, driver, "Brylane Home brand ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "brylanehome", "url", "");
		} else {
			Log.failsoft("The Brylane brand is disabled", driver);
		}
	}

	/**
	 * To navigate to BCO brand under Brands section
	 * @throws Exception - Exception
	 */
	public void navigateToBCOutletBrand()throws Exception{
		if(Utils.waitForElement(driver, imgBCOutlet)) {
			BrowserActions.clickOnElementX(imgBCOutlet, driver, "BCO brand ");
			Utils.waitForPageLoad(driver);
			Utils.switchWindows(driver, "bcoutlet", "url", "");
		} else {
			Log.failsoft("The BCOutlet brand is disabled", driver);
		}
	}

	/**
	 * To navigate to Privacy Policy
	 * @throws Exception - Exception
	 */
	public void navigateToPrivacyPolicy()throws Exception{
		//if(Utils.getRunPlatForm().equals("mobile"))
			//BrowserActions.clickOnElementX(lnkPrivacyPolicy.get(1), driver, "Privacy Policy ");
		//else
		if (Utils.waitForElement(driver, lnkPrivacyPolicy.get(0))) {
			BrowserActions.clickOnElementX(lnkPrivacyPolicy.get(0), driver, "Privacy Policy ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("The Privacy policy is not displayed", driver);
		}
		//Need to add return page statement  after the pages have been configured in the application 
	}

	/**
	 * To navigate to Terms of use
	 * @throws Exception - Exception
	 */
	public void navigateToTermsOfUse()throws Exception{
		//if(Utils.getRunPlatForm().equals("mobile"))
			//BrowserActions.clickOnElementX(lnkTermsOfUse.get(1), driver, "Privacy Policy ");
		//else
		if(Utils.waitForElement(driver, lnkTermsOfUse.get(0))) {
			BrowserActions.clickOnElementX(lnkTermsOfUse.get(0), driver, "Privacy Policy ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("The Terms of use is not displayed", driver);
		}
		//Need to add return page statement  after the pages have been configured in the application 
	}

	/**
	 * To navigate to Supply Chains Act
	 * @throws Exception - Exception
	 */
	public void navigateToSupplyChainsAct()throws Exception{
		//if(Utils.getRunPlatForm().equals("mobile"))
		//	BrowserActions.clickOnElementX(lnkSupplyChainsAct.get(1), driver, "Privacy Policy ");
		//else
		if (Utils.waitForElement(driver, lnkSupplyChainsAct.get(0))) {
			BrowserActions.clickOnElementX(lnkSupplyChainsAct.get(0), driver, "Privacy Policy ");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("The Supply chain link is not displayed", driver);
		}
		//Need to add return page statement  after the pages have been configured in the application 
	}

	/**
	 * To navigate to customer service page 
	 * @return CustomerService
	 * @throws Exception - Exception
	 */
	public CustomerService navigateToCustomerService()throws Exception{
		if (Utils.waitForElement(driver, lnkCustomerService)) {
		BrowserActions.clickOnElementX(lnkCustomerService, driver, "Customer Service");
		Utils.waitForPageLoad(driver);
		return new CustomerService(driver).get();
		} else {
			Log.failsoft("The Customer Service link is not displayed", driver);
			return null;
		}
	}
	
	/**
	 * To navigate to gift card page
	 * @return GiftCardPage- Page object
	 * @throws Exception - Exception
	 */
	public GiftCardPage navigateToGiftCardPage()throws Exception{
		if(Utils.waitForElement(driver, lnkGiftCardPage)) {
			BrowserActions.javascriptClick(lnkGiftCardPage, driver, "Gift Card in Footer");
		} else{
			Log.message("Navigation element not available in footer, using workaround to navigate to Gift Card page.");
			String url = driver.getCurrentUrl()+"giftcard-landing-asset-"+Utils.getCurrentBrand()+".html";
			driver.navigate().to(url);
		}		
		Utils.waitForPageLoad(driver);
		return new GiftCardPage(driver).get();
	}

	/**
	 * To navigate to quick order page
	 * @return QuickOrderPage - Page object
	 * @throws Exception - Exception
	 */
	public QuickOrderPage navigateToQuickOrder()throws Exception{
		BrowserActions.clickOnElementX(lnkCatalogOrderPage, driver, "Order From Catalog Link");
		return new QuickOrderPage(driver).get();
	}
	
	/**
	 * To navigate to Catalog Preferences
	 * @return CatalogPreferencePage - page object for Catalog Preferences
	 * @throws Exception - Exception
	 */
	public CatalogPreferencePage navigateToCatalogPreferences()throws Exception{
		if(Utils.waitForElement(driver, imgRequestCatalog)) {
			BrowserActions.clickOnElementX(imgRequestCatalog, driver, "Footer Catalog Image");
		} else {
			BrowserActions.clickOnElementX(lnkRequestCatalog, driver, "Catalog Preferences");
		}
		
		if(Utils.waitForElement(driver, readyElementCatalogPreferences)) {
			return new CatalogPreferencePage(driver).get();
		}else {
			Log.failsoft("Catalog Preference Page didn't open.");
			return null;
		}
	}
	
	/**
	 * To navigate to physical Gift Card
	 * @return PdpPage - Gift Card PDP object
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPhysicalGiftCard()throws Exception{
		BrowserActions.clickOnElementX(lnkPhysicalGiftCard, driver, "physical Gift Card in Footer");
		if(prdTile.size() > 0) {
			
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}
	
	/**
	 * To navigate to EGift Card
	 * @return PdpPage - EGiftcard page object 
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToEGiftCard() throws Exception{
		if(Utils.waitForElement(driver, lnkEGiftCard)){
			BrowserActions.javascriptClick(lnkEGiftCard, driver, "EGift Card link in Footer");
		}else{
			Log.message("Navigation element not available in footer.");
		}
		Utils.waitForPageLoad(driver);
		List<WebElement> prdList = driver.findElements(By.cssSelector("#search-result-items .product-name-image-container > a"));
		if(prdList.size() == 0) {
			return new PdpPage(driver).get();
		}else {
			BrowserActions.clickOnElementX(prdList.get(0), driver, "EGift Card");
			Utils.waitForPageLoad(driver);
			return new PdpPage(driver).get();
		}
	}
	
	/**
	 * To Verify Brand images on Footer
	 * @param Footers - footer page object
	 * @throws Exception - Exception
	 */
	public boolean verifyBrandImages(Footers footer)throws Exception{
		for(Brand brand : Brand.values()) {
			if(!String.valueOf(brand).contains("x") && !BrandUtils.isBrand(brand)) {
				Log.event("Trying to verify " + brand.getConfiguration());
				if(!elementLayer.verifyElementDisplayed(Arrays.asList("img" + brand.getConfiguration()), footer)) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * To verify brand image links in footer
	 * @param WebDriver - driver
	 * @param Footers - footer page object
	 * @throws Exception - Exception
	 */
	public boolean verifyFooterBrandImagesNavigation(WebDriver driver, Footers footer) throws Exception{
		for(Brand brand : Brand.values()) {
			if(!BrandUtils.isBrand(brand)) {
				Method method = footer.getClass().getDeclaredMethod("navigateTo"+brand.getConfiguration()+"Brand");
				method.invoke(footer);
				Log.message("Clicked on "+brand.getConfiguration()+" Brand.", driver);

				Log.softAssertThat(driver.getCurrentUrl().contains(brand.getConfiguration().toLowerCase()), "Page should be redirected to "+brand.getConfiguration()+" Page", driver);

				driver = Utils.switchWindows(driver, Utils.getWebSite(), "url", "NO");
				Log.message("Switched To FullBeauty Website.", driver);
			}
		}
		return true;
	}
	
	/**
	 * To verify brand image links in footer
	 * @param WebDriver - driver
	 * @param Footers - footer page object
	 * @throws Exception - Exception
	 */
	public boolean verifyFooterBrandNavigation() throws Exception{
		for(int i=0; i < brandLinks.size(); i++) {
			WebElement elem = brandLinks.get(i).findElement(By.cssSelector(" img"));
			String brand = BrowserActions.getTextFromAttribute(driver, elem, "title", "Brand name");
			String brandLink = BrowserActions.getTextFromAttribute(driver, brandLinks.get(i), "href", "Brand link");
			if(!(brandLink.contains(brand) && brandLink.contains("www.") && brandLink.contains("http"))) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To navigate to Gift/eGift Card landing page
	 * @throws Exception - Exception
	 */
	public GiftCardPage navigateToGiftCardLandingPage()throws Exception{
		BrowserActions.clickOnElementX(lnkGiftCardLandingPage, driver, "Gift cards section title in footer");
		Utils.waitForPageLoad(driver);
		return new GiftCardPage(driver).get();
	}
	
	/**
	 * To verify a given Q&A section on a page
	 * @param String - Section to verify
	 * @throws Exception - Exception
	 */
	public boolean verifyQASection(String sectionName) throws Exception{
		for(WebElement section : qaSections) {
			if(section.getText().toUpperCase().contains(sectionName.toUpperCase()))
				return true;
		}
		return false;
	}
	
	/**
	 * To navigate to my account page
	 * @return SignIn
	 * @throws Exception - Exception
	 */
	public SignIn clickOnSignInLink()throws Exception{
		BrowserActions.clickOnElement(linkSignIn, driver, "Sign in link");
		Utils.waitForPageLoad(driver);
		return new SignIn(driver).get();
	}
	
	/**
	 * To navigate to Track My Order page
	 * @return TrackMyOrderPage
	 * @throws Exception - Exception
	 */
	public TrackOrderPage navigateToTrackMyOrder() throws Exception {
		BrowserActions.clickOnElementX(lnkTrackMyOrder, driver, "Track Order ");
		Utils.waitForPageLoad(driver);
		return new TrackOrderPage(driver).get();
	}
	
	/**
	 * To navigate to Return Items page
	 * @throws Exception - Exception
	 */
	public TrackOrderPage navigateToReturnItems() throws Exception {
		BrowserActions.clickOnElementX(lnkTrackMyOrder, driver, "Return items ");
		Utils.waitForPageLoad(driver);
		return new TrackOrderPage(driver).get();
	}
	
}
