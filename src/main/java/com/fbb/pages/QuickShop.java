package com.fbb.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.fbb.pages.account.WishListPage;
import com.fbb.reusablecomponents.Enumerations.Direction;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class QuickShop extends LoadableComponent<QuickShop> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	String runBrowser;

	private static final String MINI_CART_OVERLAY = ".minicartpopup ";
	private static final String SIZE_SELECTED = ".swatches.size .selected";

	@FindBy(css = ".product-set-item")
	List<WebElement> childProducts;
	
	@FindBy(css = ".quick-view")
	WebElement divQuickShop;

	@FindBy(css = ".product-variation-content")
	WebElement divproductVariant;

	@FindBy(id = "product-content")
	WebElement divProductContent;

	@FindBy(className = "attribute_label")
	WebElement divAttributeLabel;

	@FindBy(css = "button.ui-dialog-titlebar-close")
	WebElement btnClose;

	@FindBy(css = ".product-primary-image ")
	WebElement imgProductPrimaryImage;

	@FindBy(css = "#thumbnails .background-thumb")
	List<WebElement> lstThumbNailImages;
	
	@FindBy(css = "#thumbnails div.slick-slide")
	List<WebElement> lstAlternateImages;
	
	@FindBy(css = ".attribute.shoeSize .label")
	WebElement lblShoeSizeSwatch;
	
	@FindBy(css = ".product-col-1 #thumbnails .thumb.selected img")
	WebElement selectedThumbImg;

	@FindBy(css = ".product-col-1 #thumbnails .thumb.selected a")
	WebElement selectedThumbImgWW;

	@FindBy(css = "#pdpMain > div > div.product-col-1 #thumbnails .thumb")
	List<WebElement> lstSpecialProductSetThumbNailImages;

	@FindBy(css = ".slimScrollDiv")
	WebElement scrollSection;
	
	@FindBy(css = ".pdpForm .slimScrollBar")
	WebElement scrollBar;
	
	@FindBy(css = ".quick-view .product-name")
	WebElement txtProductTitleDesktop;
	
	@FindBy(css = ".loader-bg")
	WebElement waitLoader;

	@FindBy(css = "h1[class='product-name']")
	WebElement txtSpecialProductTitleDesktop;

	@FindBy(css = ".product-price")
	WebElement txtProductPrice;
	
	@FindBy(css = ".price-sales")
	WebElement txtProductSalesPrice;

	@FindBy(css = ".main-product-set-details .product-price")
	WebElement txtSpecialProductSetPrice;

	@FindBy(css = ".promotion .callout-message")
	WebElement txtPromotionMessage;

	@FindBy(css = ".savingmessage .value")
	WebElement txtSavingsMessage;

	@FindBy(css = "#LiveclickerPlayer")
	WebElement videoPlayer;

	@FindBy(css = ".enlarge-video-sec.hide-mobile .enlarge-video.video-link")
	WebElement lnkVideoMain;

	@FindBy(css = ".mobile-enlarge-video-sec.hide-desktop.hide-tablet .enlarge-video.video-link")
	WebElement lnkVideoMain_Mobile;
	
	@FindBy(css = ".personalizedMessage .input-checkbox")
	WebElement ckbAddPersonalMsg;
	
	@FindBy(css = ".character-count")
	WebElement txtPersonalMsgCounter;
	
	@FindBy(css = "#dwfrm_product_giftcard_purchase_message")
	WebElement txtAreaPersonalMsg;
	
	@FindBy(css = "#dwfrm_product_giftcard_purchase_recipient")
	WebElement txtToAddress;
	
	@FindBy(css = "#dwfrm_product_giftcard_purchase_from")
	WebElement txtFromAddress;

	@FindBy(css = ".swatches.size")
	WebElement tblSizeSwatch;
	
	@FindBy(css = ".swatches.color")
	WebElement tblColorSwatch;

	@FindBy(css = ".product-variations .swatches.size>li ")
	List<WebElement> lstSizeSwatches;
	
	@FindBy(css = ".product-variations .swatches.shoewidth>li.selectable a")
	List<WebElement> lstShoeWidthSwatches;
	
	@FindBy(css = ".product-variations .swatches.shoesize")
	WebElement divShoeSizeSwatches;
	
	@FindBy(css = ".product-variations .swatches.shoesize>li.selectable a")
	List<WebElement> lstShoeSizeSwatches;
	
	@FindBy(css = ".swatches.size .selected a")
	WebElement txtSelectedSize;
	
	@FindBy(css = ".attribute.size .selected-value")
	WebElement txtSelectedSizeValue;
	
	@FindBy(css = ".attribute.shoeWidth .selected-value")
	WebElement txtSelectedShoeWidthValue;
	
	@FindBy(css = ".attribute.shoeSize .selected-value")
	WebElement txtSelectedShoeSizeValue;
	
	@FindBy(css = ".braBandSize .selected-value")
	WebElement txtSelectedBraBandSizeValue;
	
	@FindBy(css = ".braCupSize .selected-value")
	WebElement txtSelectedBraCupSizeValue;
	
	@FindBy(css = ".attribute.color .selected-value")
	WebElement txtSelectedColorValue;
	
	@FindBy(css = ".attribute.color .label")
	WebElement lblColorSwatch;
	
	@FindBy(css = ".attribute.size .label")
	WebElement lblSizeSwatch;
	
	@FindBy(css = ".product-variations .swatches.color>li a")
	List<WebElement> lstColorSwatches;
	
	@FindBy(css = ".swatches.color>li.selected a img")
	WebElement selectedColorSwatchImg;

	@FindBy(css = "li[class='attribute color '] .selected-value")
	WebElement txtSelectedColor;
	
	@FindBy(css = ".product-add-to-cart .availability-msg")
	WebElement availabilityMsg;
	
	@FindBy(id = "add-to-cart")
	WebElement btnAddToBag;
	
	@FindBy(css = ".max-cart-qty")
	WebElement lblcartError;
	
	@FindBy(css = ".button-fancy-large.add-to-cart")
	WebElement btnUpdateCartEditOverlay;

	@FindBy(css = ".swatches.color li.selectable:not([class*='selected']) a")
	List<WebElement> lstColorSelectable;
	
	@FindBy(css = "button[value='ADD TO BAG']")
	WebElement btnAddToBagSpecialProductSet;

	@FindBy(css = ".cart-overlay")
	WebElement flytCart;

	@FindBy(css = ".cart-overlay .ui-dialog-titlebar-close")
	WebElement btnCloseCartOverLay;

	@FindBy(css = ".enlarge-video.video-link")
	WebElement lnkVideo;

	@FindBy(css = ".zoomLens")
	WebElement divZoomLens;

	@FindBy(css = ".zoomWindowContainer > div")
	WebElement divZoomWindowContainer;
	
	@FindBy(css = ".product-image.main-image.hide-mobile img")
	WebElement imgProdMainImg;
	
	@FindBy(css = ".ui-dialog-content.ui-widget-content")
	WebElement quickShopContainer;

	@FindBy(css = ".product-variation-content")
	WebElement divProductVariationContent;

	@FindBy(id = "product-set-list")
	WebElement divSpecialProductVariationContent;

	@FindBy(css = ".product-add-to-cart")
	WebElement divProductAddToCart;

	@FindBy(css = ".view-details")
	WebElement divViewDetails;
	
	@FindBy(css = ".view-details-link")
	WebElement lnkViewDetails;

	@FindBy(css = "button[title*='Select']")
	WebElement txtSelectSizeError;

	@FindBy(css = ".color-not-selected.show-error")
	WebElement txtSelectSizeErrorFireFox;

	@FindBy(css = ".quantity .selected-option.selected")
	WebElement txtSelectedQuantity;

	@FindBy(css = ".quantity .selection-list>li")
	List<WebElement> lstQuantitySelectionList;

	@FindBy(css = "a[title='Add this product to wishlist']")
	WebElement lnkAddToWishList;

	@FindBy(css = "div[class='view-details hide-mobile hide-tablet'] > a")
	WebElement lnkViewFullDetail;

	@FindBy(css = "div[class='view-details'] > a")
	WebElement lnkViewFullDetailSpecialProductSet;

	@FindBy(css = ".search-wishlist")
	WebElement divSearchWishlist;

	@FindBy(css = "button[aria-label='Previous']")
	WebElement lnkPrevious;
	
	@FindBy(css = ".product-primary-image.not-a-device")
	WebElement lnkPlayVideo;

	@FindBy(css = "button.slick-next")
	WebElement lnkNext;

	@FindBy(css = "input.monogramming-enabled-class")
	WebElement chkEnableMonogramming;

	@FindBy(css = "select#monogram-font")
	WebElement drpFontSelect;

	@FindBy(css = "select#monogram-location")
	WebElement drpLocationSelect;
	
	@FindBy(css = ".monogramming-location .single-selection")
	WebElement txtLocationSingleSelect;
	
	@FindBy(css = ".monogramming-location .single-selection .value")
	WebElement txtLocationSingleSelectValue;
	
	@FindBy(css = "input.monogramming-textfield")
	WebElement txtMonogramTextArea;
	
	@FindBy(css = ".monogramming-color .single-selection")
	WebElement txtColorSingleSelect;
	
	@FindBy(css = ".monogramming-color .single-selection .value")
	WebElement txtColorSingleSelectValue;
	
	@FindBy(css = ".monogramming-font .single-selection")
	WebElement txtFontSingleSelect;
	
	@FindBy(css = ".monogramming-font .single-selection .value")
	WebElement txtFontSingleSelectValue;

	@FindBy(css = "select#monogram-color")
	WebElement drpColorSelect;

	@FindBy(css = ".monogramming-preview>textarea")
	WebElement txtMonogramPreview;

	@FindBy(css = ".presonalised-msg-norefund .warning-msg")
	WebElement txtMonogramDisclaimer;

	@FindBy(css = ".input-fields-set .character-left:nth-of-type(1)")
	WebElement textField;

	@FindBy(css = ".product-monogramming .title")
	WebElement txtMonogrammingTitle;

	@FindBy(css = ".form-row.product-monogramming")
	WebElement divMonogrammingOptions;

	@FindBy(css = ".monogramming-enabled.form>div>div>label>span")
	WebElement txtMonogramMessaging;

	@FindBy(css = "a.tips-link")
	WebElement lnkMonogrammingTips;

	@FindBy(css = ".ui-dialog .ui-dialog-titlebar.ui-draggable-handle .ui-dialog-titlebar-close ")
	WebElement btnCloseTipsFlyout;
	
	@FindBy(css = ".hemming-drawer>ul>li.selected")
	WebElement selectedHemmingOption;
	
	@FindBy(css = ".product-hemmable")
	WebElement divHemmingOptions;

	@FindBy(css = ".product-hemming .title")
	WebElement txtHemmingTitle;

	@FindBy(css = ".product-hemming-checkbox")
	WebElement chkProductHemming;
	
	@FindBy(css = ".product-hemming .checkbox")
	WebElement checkBoxHemmingSection;

	@FindBy(css = ".checkbox>label>span")
	WebElement txtHemmingMessaging;

	@FindBy(css = ".hemming-tips-link")
	WebElement lnkHemmingTips;

	@FindBy(css = "div.hemming-drawer")
	WebElement divHemmingDrawer;

	@FindBy(css = ".ui-dialog.product-hemming-tips")
	WebElement flytHemmingTips;
	
	@FindBy(css = ".ui-dialog.ui-front.tips.ui-draggable")
	WebElement hemmingtipsFlyout;

	@FindBy(css = ".hemming-drawer>ul>li")
	List<WebElement> lstHemmingOptions;
	
	@FindBy(css = ".hemming-drawer>ul>li")
	WebElement lstHemmingOption;

	@FindBy(css = ".product-hemmable .hemming-error-msg.hide")
	WebElement txtHemmingErrorMessage;

	@FindBy(css = ".hemming-drawer .presonalised-msg-norefund")
	WebElement txtHemmingDisclaimer;

	@FindBy(css = ".hemming-drawer>ul>li:last-child")
	WebElement btnLastHemmingOption;

	@FindBy(css = ".ui-dialog .content-asset ")
	WebElement hemmingFlyoutInfo;
	
	@FindBy(css = ".input-fields-set .character-left:nth-of-type(1)>div>span:nth-of-type(1)")
	WebElement txtActualCharacterCount;

	@FindBy(css = ".input-fields-set .character-left:nth-of-type(1)>div>span:nth-of-type(2)")
	WebElement txtMaximumCharacterCount;

	@FindBy(css = ".minicartpopup.cart-overlay")
	WebElement flytMiniCart;
	
	@FindBy(css = ".mini-cart-attributes div[data-attribute = 'hemming']")
	WebElement selectedHemmingValueInMCOverlay;
	
	@FindBy(css = ".cart-overlay .mini-cart-pricing .total-price span.value")
	WebElement miniCartTotal;
	
	@FindBy(css = MINI_CART_OVERLAY + ".ui-dialog-titlebar-close")
	WebElement btnCloseMiniCartOverLay;
	
	 @FindBy(css = ".addtocartoverlay-content button.checkout-now")
    WebElement btnCheckoutInMCOverlay;

	@FindBy(css = ".ui-dialog.tips")
	WebElement flytTips;

	@FindBy(css = ".size-chart-link > a span.size_label")
	WebElement lnkSizeChart;
	
	@FindBy(css = ".size-chart-link > a")
	WebElement lnkSizeChart1;

	@FindBy(css = ".size-chart-link .size_icon")
	WebElement imgSizeChartIcon;

	@FindBy(css = ".size-chart-link .size_label")
	WebElement txtSizeChart;

	@FindBy(css = ".size-guide-container")
	WebElement divSizechartContainer;

	@FindBy(css = ".sizechart-dialog .ui-dialog-titlebar-close")
	WebElement btnCloseSizeChartModal;

	@FindBy(css = ".mini-cart-totals > div.mini-cart-subtotals")
	WebElement txtNumberOfItemsInBag;

	@FindBy(css = ".slick-active .special-productset-child-wrapper .special-productset-child")
	List<WebElement> lstSpecialProductChildInCart;

	@FindBy(css = SIZE_SELECTED + " a")
	WebElement selectedSize;
	
	@FindBy(css = ".swatches.shoesize .selectable.selected a")
	WebElement selectedShoeSize;
	
	@FindBy(css = "ul.shoewidth li.selectable a")
	List<WebElement> lstShoeWidth;
	
	@FindBy(css = ".swatches.shoewidth")
    WebElement divWidthGrid_shoe;
	
	@FindBy(css = "ul.shoewidth li.selectable a")
	WebElement shoeWidth;
	
	@FindBy(css = ".swatches.shoewidth .selectable.selected a")
	WebElement selectedshoeWidth;
	
	@FindBy(css = "ul.shoesize")
	WebElement shoeSize;
	
	@FindBy(css = ".attribute .size")
	WebElement selectedSizeQuickShop;

	@FindBy(css = "select[@id='Quantity']")
	WebElement drpQty;

	@FindBy(css = ".quantity div[class='selected-option selected']")
	WebElement selectedQty1;

	@FindBy(xpath = "//select[@id='va-size']//following-sibling::div[contains(@class,'selected-option')]")
	WebElement drpGiftCardSize;
	
	@FindBy(css = ".gift-card .selection-list li:not([class*='selected'])")
	List<WebElement> drpGiftCardSizeOptionSelectable;
	
	@FindBy(css = ".gift-card .selection-list li")
	List<WebElement> drpGiftCardSizeOptions;

	@FindBy(css = "#va-size")
	WebElement drpGiftCardSizeSelect;

	@FindBy(xpath = "//select[@id='Quantity']//following-sibling::div[@class='selected-option selected']")
	WebElement selectedQty;

	@FindBy(css = ".quantity .selection-list li:not([class='selected'])")
	List<WebElement> lstQtySelectable;

	@FindBy(css = ".attribute.size")
	WebElement divRegularSize;
	
	@FindBy(css = ".swatches.size li.selectable a")
	List<WebElement> lstSizeSwatchesSelectable;

	@FindBy(css = ".swatches.size li.unselectable a")
	List<WebElement> lstSizeSwatchesUnSelectable;
	
	@FindBy(css = ".swatches.size li.unselectable a")
	WebElement sizeSwatchesUnSelectable;

	@FindBy(css = ".loader[style*='block']")
	WebElement QuickShopspinner;

	@FindBy(css = ".swatches.color li.selected a")
	WebElement selectedColor;

	@FindBy(css = ".swatches.sizefamily li.selectable a")
	List<WebElement> lstSizeFamilySelectable;
	
	@FindBy(css = ".swatches.sizefamily li")
	List<WebElement> sizeFamily;
	
	@FindBy(css = ".attribute.sizeFamily")
	WebElement divSizeFamily;
	
    @FindBy(css = ".swatches.sizefamily li.selected")
    WebElement selectedSizeFamily;
	
    @FindBy(css = ".sizeFamily .selected-value")
    WebElement selectedSizeFamilyValue;

	@FindBy(css = ".swatches.sizefamily .selectable")
    List<WebElement> SizeFamSelectable;
	
	@FindBy(css = ".swatches.brabandsize li.selectable a")
    List<WebElement> lstBraSizeSwatches;
    
	@FindBy(css = ".swatches.bracupsize")
    WebElement divBraCupSwatches;
	
    @FindBy(css = ".swatches.bracupsize li.selectable a")
    List<WebElement> lstBraCupSizeSwatches;
    	
	@FindBy(css = ".slick-arrow")
	List<WebElement> slickArrows;
	
	@FindBy(css = "#product-content")
	WebElement sectionProductDetails;
	
	@FindBy(css = ".price-sales")
	WebElement currentPrice;
	
	@FindBy(css = ".size .selected-value")
	WebElement lblSelectedSizeName;
	
	@FindBy(css = "a.thumbnail-link")
	List<WebElement> thumbAlternateImages;
	
	@FindBy(css = "li.braBandSize")
	WebElement divBraSize;

	@FindBy(css = ".swatches.color li.selected")
	List<WebElement> lstSelectedColorSwatch;

	@FindBy(css = ".swatches.size li.selected")
	List<WebElement> lstSelectedSizeSwatch;
		
	@FindBy(css = "#Quantity > option")
	List<WebElement> lstQty;
	
	@FindBy(css = ".noOfVariationAttributes")
	WebElement noOfVariationAttributes;
	
	@FindBy(css = ".size_border .attribute_label .label")
	WebElement shoeOrBraAttributesLabel;
	
	/**
	 * constructor of the class
	 * @param driver - Webdriver
	 */
	public QuickShop(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
		runBrowser  = Utils.getRunBrowser(driver);
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, divQuickShop))) {
			Log.fail("Quick Shop overlay did not open up.", driver);
		}
	}

	/**
	 * To get the Load status of page
	 * @return boolean - 'true'(Page loaded) / 'false'(Page not loaded)
	 * @throws Exception - Exception
	 */
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}

	/**
	 * To click on close button on the Quick Shop overlay
	 * @throws Exception - Exception
	 */
	public void closeQuickShopOverlay()throws Exception{
		BrowserActions.clickOnElementX(btnClose, driver, "Close button");
	}

	/**
	 * To zoom the product image in the Quick Shop overlay
	 * @throws Exception - Exception
	 */
	public void zoomProductImage()throws Exception{
		BrowserActions.scrollInToView(imgProductPrimaryImage, driver);
		BrowserActions.mouseHover(driver, imgProductPrimaryImage.findElement(By.cssSelector("img")), "Product Image");
		Actions action = new Actions(driver);
		action.moveToElement(imgProductPrimaryImage.findElement(By.cssSelector("img"))).build().perform();
		Utils.waitForElement(driver, divZoomWindowContainer);
	}

	/**
	 * To verify Xoomlens and zoom window are displayed
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyZoomLensAndZoomWindowAreDisplayed()throws Exception{
		Point lens = divZoomLens.getLocation();
		Point window = divZoomWindowContainer.getLocation();

		if(lens.x != 0 && lens.y != 0 && window.x != 0 && window.y != 0)
			return true;
		else
			return false;

	}
	
	/**
	 * To mouse hover on product primary image
	 * @throws Exception
	 */
	public void checkProductMainImageZoom() throws Exception {
		BrowserActions.mouseHover(driver, imgProdMainImg, "Main Product Image");
		Utils.waitForElement(driver, divZoomWindowContainer, 5);
	}
	
	/**
	 * to verify that the zoomed image is to the right of product image
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyZoomedImageDisplayedRightOfPrimaryProductImage()throws Exception{
		if(!Utils.waitForElement(driver, divZoomWindowContainer, 2)) {
			checkProductMainImageZoom();
		}
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, divZoomWindowContainer, imgProductPrimaryImage);
	}

	/**
	 * to verify that the top of zoomed image alligns with the top of product image
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyTopOfZoomedImageAllignsWithTopOfProductImage()throws Exception{
		return BrowserActions.verifyElementsAreInSameRow(driver, divZoomWindowContainer, imgProductPrimaryImage);
	}

	/**
	 * To click on the video link
	 * @throws Exception - Exception
	 */
	public void clickVideoLink()throws Exception{
		BrowserActions.clickOnElementX(lnkVideo, driver, "Video Link ");
	}

	/**
	 * to verify is selected thumbnail image displayed as primary product image
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfSelectedThumbnailImageDisplayedAsPrimaryProductImage()throws Exception{
		String[] primaryImageCodes = driver.findElement(By.cssSelector(".product-image.main-image .primary-image")).getAttribute("src").split("\\/");

		String[] thumbImageCodes = driver.findElement(By.cssSelector(".thumb.selected img")).getAttribute("src").split("\\/");
		
		String primaryImageCode = primaryImageCodes[primaryImageCodes.length-1].split("\\.j")[0];
		primaryImageCode = primaryImageCode.split("\\_")[primaryImageCode.split("\\_").length-1];
		
		String thumbImageCode = thumbImageCodes[thumbImageCodes.length-1].split("\\.j")[0];
		thumbImageCode = thumbImageCode.split("\\_")[thumbImageCode.split("\\_").length-1];
		
		Log.event("Primary Image Code :: " + primaryImageCode);
		Log.event("Thumbnail Image Code :: " + thumbImageCode);
		
		if(primaryImageCode.equals(thumbImageCode))
			return true;
		else if(primaryImageCode.contains("noimagefound") && thumbImageCode.contains("noimagefound")) {
			Log.warning("Unconfigured image or data feed.");
			return true;
		} else
			return false;
	}

	/**
	 * to verify is selected thumbnail image displayed as primary product image
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfSelectedThumbnailImageDisplayedAsPrimaryProductImageSpecialProductSet()throws Exception{
		/*String primaryImage = imgProductPrimaryImage.findElement(By.cssSelector("img")).getAttribute("src");
		for (WebElement image : lstSpecialProductSetThumbNailImages) {
			if(image.getAttribute("class").contains("selected"))
			{
				String[] selectedImage = image.findElement(By.cssSelector("div>div>a>img")).getAttribute("data-lgimg").split("/");
				selectedImage[selectedImage.length-1] = selectedImage[selectedImage.length-1].replace("\"}", "");
				if(primaryImage.contains(selectedImage[selectedImage.length-1])) {
					return true;
				}
			}
		}
		return false;*/
		
		String primaryImageURL = imgProductPrimaryImage.findElement(By.cssSelector("img")).getAttribute("src");
		String selectedThumbURL = selectedThumbImg.getAttribute("src");
		
		primaryImageURL = primaryImageURL.split("\\/")[(primaryImageURL.split("\\/").length-1)].split("\\?")[0];
		selectedThumbURL = selectedThumbURL.split("\\/")[(selectedThumbURL.split("\\/").length-1)].split("\\?")[0];
		
		Log.event("Primary Image Code :: " + primaryImageURL);
		Log.event("Thumbnail Image Code :: " + selectedThumbURL);
		
		if(primaryImageURL.equals(selectedThumbURL))
			return true;
		else
			return false;
		
	}
	
	/**
	 * To select alternate image of open quickshop overlay
	 * @param index - index of alternate image to be selected
	 * @throws Exception
	 */
	public void selectProductAlternateImage(int index) throws Exception{
		BrowserActions.clickOnElementX(thumbAlternateImages.get(index-1), driver, "Alternate Image");
	}

	/**
	 * to verify is selected color swatch displayed as primary product image
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfSelectedColorDisplayedAsPrimaryProductImage()throws Exception{
		String primaryImageAlt = imgProductPrimaryImage.findElement(By.cssSelector("img")).getAttribute("alt");
		String primaryImageSrc = imgProductPrimaryImage.findElement(By.cssSelector("img")).getAttribute("src");
		primaryImageSrc = primaryImageSrc.split("\\/")[primaryImageSrc.split("\\/").length-1];
		String selectedColor = selectedColorSwatchImg.getAttribute("alt");
		
		Log.event("Primary image src: " + primaryImageSrc);
		Log.event("Primary image alt: " + primaryImageAlt.trim().toLowerCase());
		Log.event("Selected color swatch: " + selectedColor.trim().toLowerCase());
		if(primaryImageSrc.contains("mm") || primaryImageSrc.contains("ma") || primaryImageSrc.contains("mc") 
				|| primaryImageSrc.contains(".jpg")	|| primaryImageAlt.trim().toLowerCase().contains(selectedColor.trim().toLowerCase()))
			return true; 
		else 
			return false;
	}

	/**
	 * To click on alternate images
	 * @param index -
	 * @throws Exception - Exception
	 */
	public void clickAlternateImages(int index)throws Exception{
		BrowserActions.clickOnElementX(lstThumbNailImages.get(index).findElement(By.cssSelector("img")), driver, "Thumbnail image ");
		waitForSpinner();
	}

	/**
	 * Clicking on size chart is opening in new tab?
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean isNewTab()throws Exception{
		if(lnkSizeChart1.getAttribute("target").equals("_blank"))
			return true;
		else 
			return false;
	}

	/**
	 * to verify that selected thumbnail image is underlined
	 * @param index -
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySelectedThumnailImageIsUnderlined(int index)throws Exception{
		return Utils.verifyCssPropertyForElement(lstThumbNailImages.get(index), "text-decoration", "underline");
	}

	/**
	 * to verify the product price
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyProductPriceDisplayed()throws Exception{
		if(Utils.getRunBrowser(driver).toLowerCase().contains("edge") || Utils.getRunBrowser(driver).toLowerCase().contains("firefox"))
		{
			if(txtProductPrice.getLocation().x != 0 && txtProductPrice.getLocation().y != 0)
				return true;
			else
				return false;
		}
		if(txtProductPrice.findElement(By.cssSelector("span[class='price-standard']")).isDisplayed())
		{
			if(txtProductPrice.findElement(By.cssSelector("span[class='price-standard']")).getCssValue("text-decoration").equals("line-through"))
			{
				if(txtProductPrice.findElement(By.cssSelector("span[class='price-sales price-standard-exist']")).isDisplayed())
					return true;
				else
					return false;
			}
			else
			{
				return true;
			}
		}
		else
			return false;


	}
	
	/**
	 * To Select the gift card count or size
	 * 
	 * @param size -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String selectGCSize(String... size) throws Exception {
		if (Utils.waitForElement(driver, drpGiftCardSizeSelect)) {
			Select drpGCSizeSelect = new Select(drpGiftCardSizeSelect);

			if (size.length > 0){
				Log.event("Given Size :: " + size[0]);
				drpGCSizeSelect.selectByVisibleText(size[0]);
			}else {
				if (drpGCSizeSelect.getOptions().size() > 1){
					Log.event("Amount In GC :: " + drpGCSizeSelect.getOptions().get(1).getText());
					if(drpGCSizeSelect.getOptions().get(0).getText().equalsIgnoreCase("select amount")) 
						drpGCSizeSelect.selectByIndex(1);
					Log.event("Amount In GC :: " + drpGCSizeSelect.getOptions().get(1).getText());
					Log.event("Size selection failed");
				}else
					Log.fail("No Gift card amount size available....", driver);
			}

			Log.event("Returning value from Select tag.");
			String value = null;
			try{
			value = drpGCSizeSelect.getAllSelectedOptions().get(0).getText();
			}catch(StaleElementReferenceException e){
				return drpGiftCardSize.getText();
			}
			Log.event(" Value from Select Tag :: " + value);
				return value;
		}

		String dataToReturn = drpGiftCardSize.getText();
		if (dataToReturn.equalsIgnoreCase("select amount")) {
			BrowserActions.clickOnElementX(drpGiftCardSize, driver, "Gift card amount ");
			if (drpGiftCardSizeOptions.size() > 0) {
				if (size.length > 0) {
					for (int i = 0; i < drpGiftCardSizeOptionSelectable.size(); i++) {
						if (drpGiftCardSizeOptionSelectable.get(i).getAttribute("label").trim().replace("\\n", "")
								.equals(size[0])) {
							BrowserActions.clickOnElementX(drpGiftCardSizeOptionSelectable.get(i), driver,
									(i + 1) + "nth Item ");
						}
					}
				} else {
					int rand = Utils.getRandom(0, drpGiftCardSizeOptionSelectable.size()-1);
					JavascriptExecutor executor = (JavascriptExecutor) driver;
					executor.executeScript("arguments[0].style.opacity=1", drpGiftCardSizeOptionSelectable.get(rand));
					try{
						BrowserActions.clickOnElementX(drpGiftCardSizeOptionSelectable.get(rand), driver, rand + "th Item ");
					}catch(Exception e){
						BrowserActions.selectDropdownByIndex(drpGiftCardSizeSelect, rand);
					}
				}
			}
			Utils.waitForPageLoad(driver);
		}
		dataToReturn = drpGiftCardSize.getAttribute("innerHTML");
		return dataToReturn;
	}

	/**
	 * to verify the special product set price
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySpecialProductSetPriceDisplayed()throws Exception{
		if(Utils.waitForElement(driver, txtProductPrice)) {
			if(txtProductPrice.getAttribute("innerHTML").contains("$"))
				return true;
			else
				return false;
		}else
			return false;
	}

	/**
	 * to verify is selected color displayed as primary product image
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyNoSizeSwatchSelectedByDefault()throws Exception{
		for (WebElement size : lstSizeSwatches) {
			if(size.getAttribute("class").contains("selected"))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * To verify color swatch is selected by default
	 * @return Boolean -
	 * @throws Exception -
	 */
	public Boolean verifyColorSwatchSelectedByDefault()throws Exception{
		for (WebElement size : lstColorSwatches) {
			if(size.getAttribute("class").contains("selected"))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * To click on video link
	 * @throws Exception -
	 */
	public void clickMainVideoLink() throws Exception {
		if(!(Utils.isMobile())) {
			BrowserActions.javascriptClick(lnkVideoMain, driver, "Main Video link ");
		} else {
			BrowserActions.javascriptClick(lnkVideoMain_Mobile, driver, "Main Video link ");
		}
		Utils.waitForElement(driver, videoPlayer);
	}

	/**
	 * to verify is selected color displayed as primary product image
	 * @param index -
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyNoSizeSwatchSelectedByDefaultForSpecialProductSet(int index)throws Exception{
		List<WebElement> sizeSwatches = driver.findElements(By.cssSelector(".product-set-item:nth-of-type("+ index +") .attribute.size > div  > ul > li"));
		for (WebElement size : sizeSwatches) {
			if(size.getAttribute("class").contains("selected"))
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To check and unchecked Add personal msg 
	 * @param boolean - checkPersonalMsg
	 * @throws Exception - Exception
	 */
	public void chkOrUnchkAddPersonalMsg(boolean checkPersonalMsg) throws Exception {
		if(Utils.waitForElement(driver, scrollBar)) {
			while(!Utils.waitForElement(driver, ckbAddPersonalMsg)) {
				BrowserActions.dragElement(scrollBar, driver, "Quickshop Scroll Bar", Direction.Down);
			}
		}
		if (Utils.waitForElement(driver, ckbAddPersonalMsg)) {
			if (checkPersonalMsg && !(ckbAddPersonalMsg.isSelected())) {
				BrowserActions.clickOnElementX(ckbAddPersonalMsg, driver, "Add Personalized Message");
			} else if (!checkPersonalMsg && ckbAddPersonalMsg.isSelected()) {
				BrowserActions.clickOnElementX(ckbAddPersonalMsg, driver, "Add Personalized Message");
			}
		}
	}
	
	/**
	 * To enter text in the personalise message text area 
	 * @throws Exception - Exception 
	 */
	public void enterPersonalMessage(String text) throws Exception {
		BrowserActions.scrollInToView(txtAreaPersonalMsg, driver);
		BrowserActions.typeOnTextField(txtAreaPersonalMsg, text, driver, "Personalise Message");
	}
	
	/**
	 * To enter text in the personalise message text area 
	 * @throws Exception - Exception 
	 */
	public void enterToAddress(String text) throws Exception {
		BrowserActions.typeOnTextField(txtToAddress, text, driver, "To Address");
	}
	
	/**
	 * To enter text in the personalise message text area 
	 * @throws Exception - Exception 
	 */
	public void enterFromAddress(String text) throws Exception {
		BrowserActions.typeOnTextField(txtFromAddress, text, driver, "From Address");
	}
	
	/**
	 * To get the product name in the page countTextToEnter
	 * @param countTextToEnter -
	 * @param toCheckBackSpace -
	 * @param textToCheck -
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean verifyTextCounterIncreaseOrDecrease(int countTextToEnter, boolean toCheckBackSpace,
			String... textToCheck) throws Exception {
		char[] charArray = { 'a', ' ', 'e' };
		int changeCount = 0, intPersonalTextCount = 0;
		String randomString = RandomStringUtils.random(countTextToEnter, charArray);
		if (Utils.waitForElement(driver, txtAreaPersonalMsg)) {
			txtAreaPersonalMsg.clear();
			txtAreaPersonalMsg.sendKeys(randomString);
			if (toCheckBackSpace) {
				for (int negCount = 1; negCount <= 3; negCount++) {
					txtAreaPersonalMsg.sendKeys(Keys.BACK_SPACE);
					intPersonalTextCount = getCharacterCount();
					changeCount = intPersonalTextCount + countTextToEnter - negCount;
					if (changeCount != 330) {
						return false;
					}
				}
			} else {
				if (countTextToEnter < 330) {
					intPersonalTextCount = getCharacterCount();
					changeCount = intPersonalTextCount + countTextToEnter;
					if (changeCount != 330) {
						return false;
					}
				} else {
					String textToVerify = getPersonalCounterText();
					if (!textToVerify.equals(textToCheck[0])) {
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * To get personal counter text
	 * @return String - personal counter text
	 * @throws Exception - Exception
	 */
	public String getPersonalCounterText() throws Exception {
		String txtCounter = "";
		if (Utils.waitForElement(driver, txtPersonalMsgCounter)) {
			txtCounter = txtPersonalMsgCounter.getText();
		}
		return txtCounter;
	}
	
	/**
	 * to verify Initial Values of text counter
	 * @param initalCount -
	 * @param initialText -
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean verifyInitialValueOfTextCounter(String initalCount, String initialText) throws Exception {
		int intInitalCount = Integer.parseInt(initalCount);
		if (!(intInitalCount == getCharacterCount())) {
			return false;
		}
		if (!getPersonalCounterText().equals(initialText)) {
			return false;
		}
		return true;
	}
	
	/**
	 * To click on Shopping Cart tool tip
	 * @throws Exception - Exception
	 * @return int
	 * 
	 */
	public int getCharacterCount() throws Exception {
		int intPersonalTextCount = 0;
		if (Utils.waitForElement(driver, txtPersonalMsgCounter)) {
			String txtCounter = txtPersonalMsgCounter.getText().substring(1,
					Math.min(txtPersonalMsgCounter.getText().length(), 4));
			intPersonalTextCount = Integer.parseInt(txtCounter);
		}
		return intPersonalTextCount;
	}

	/**
	 * to verify if color is preselected 
	 * @param index -
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyColorSwatchSelectedByDefaultForSpecialProductSet(int index)throws Exception{
		List<WebElement> products = driver.findElements(By.cssSelector(".product-set-item"));
		List<WebElement> colorSwatches = products.get(index-1).findElements(By.cssSelector(
				".attribute.color > div  > ul > li"));
		for (WebElement color : colorSwatches) {
			if (color.getAttribute("class").contains("selected")) {
				return true;
			}
		}
		return false;


	}

	/**
	 * To select size family swach by index for product
	 * @param product -
	 * @param index -
	 * @return String -
	 * @throws Exception -
	 */
	public String selectSizeFamilyForSpecialProductSet(int product, int index) throws Exception {
		List<WebElement> sizeFamilySwatches = driver.findElements(By.cssSelector(".product-set-item:nth-of-type("+ product +") .attribute.Size.Family > div  > ul > li"));
		String size = sizeFamilySwatches.get(index).findElement(By.cssSelector("a")).getText().trim();
		if(sizeFamilySwatches.get(index).getAttribute("class").contains("selected") == false)
			BrowserActions.clickOnElementX(sizeFamilySwatches.get(index).findElement(By.cssSelector("a")), driver, "Size Swatch");
		return size;
	}

	/**
	 * to select a size swatch
	 * @param index -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String selectSizeSwatch(int index)throws Exception{
		String size = null;
		if(lstSizeSwatches.size()>0){
		 size = lstSizeSwatches.get(index).findElement(By.cssSelector("a")).getText();
		 	if(lstSizeSwatches.get(index).getAttribute("class").contains("selected") == false)
		 		BrowserActions.clickOnElementX(lstSizeSwatches.get(index).findElement(By.cssSelector("a")), driver, "Size Swatch");
						
		}else{
			if(lstShoeSizeSwatches.size()>0){
				size = lstShoeSizeSwatches.get(index).findElement(By.cssSelector("a")).getText();
			if(lstShoeSizeSwatches.get(index).getAttribute("class").contains("selected") == false)
				BrowserActions.clickOnElementX(lstShoeSizeSwatches.get(index).findElement(By.cssSelector("a")), driver, "Size Swatch");
				}
			
			}
		Utils.waitForPageLoad(driver);
		return size;
		}

	/**
	 * to select a color swatch
	 * @param index -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String selectColorSwatch(int index)throws Exception{
		String color;
		if(lstColorSwatches.get(index).findElements(By.cssSelector(" img")).size()>0)
			color = (lstColorSwatches.get(index).findElement(By.cssSelector(" img"))).getAttribute("alt").trim();
		else
			color = (lstColorSwatches.get(index).getText().trim());
		if(lstColorSwatches.get(index).getAttribute("class").contains("selected") == false)
			BrowserActions.javascriptClick(lstColorSwatches.get(index), driver, "Size Swatch");
		Utils.waitForPageLoad(driver);
		return color;
	}
	
	/**
	 * To get selected size
	 * @return String - selected color
	 * @throws Exception - Exception
	 */
	public String getSelectedSizeValue()throws Exception{
		return BrowserActions.getText(driver, txtSelectedSize, "Selected Size").trim();
	}

	/**
	 * to select a size swatch
	 * @param index -
	 * @param product -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String selectSizeSwatchSpecialProductSet(int product, int index)throws Exception{
		List<WebElement> sizeSwatches = driver.findElements(By.cssSelector(".product-set-item"));

		String size = sizeSwatches.get(product-1).findElements(By.cssSelector(" .attribute.size > div  > ul > li span.show-attribute-name")).get(index-1).getText().trim();
		if(sizeSwatches.get(product-1).findElements(By.cssSelector(" .attribute.size > div  > ul > li")).get(index-1).getAttribute("class").contains("selected") == false)
			BrowserActions.clickOnElementX(sizeSwatches.get(product-1).findElements(By.cssSelector(" .attribute.size > div  > ul > li > a")).get(index-1), driver, "Size Swatch");
		Utils.waitForPageLoad(driver);
		return size;
	}
	
	/**
	 * To select size swatch of product set
	 * @param product -
	 * @param index -
	 * @return String -
	 * @throws Exception -
	 */
	public String selectSizeSwatchProductSet(int product, int... index)throws Exception {
		List<WebElement> sizeSwatchesSelectable = childProducts.get(product-1).findElements(By.cssSelector("ul.swatches.size li.selectable input.swatchanchor"));
		String value = new String();
		if(index.length > 0) {
			value = sizeSwatchesSelectable.get(index[0]-1).getText();
			Log.event("Selected Size : "+value);
			BrowserActions.clickOnElementX(sizeSwatchesSelectable.get(index[0]-1), driver, (index[0]-1) + "th Size");
		}else {
			int random = Utils.getRandom(0, sizeSwatchesSelectable.size()-1);
			value = sizeSwatchesSelectable.get(random).getText();
			BrowserActions.clickOnElementX(sizeSwatchesSelectable.get(random), driver, (random) + "th Size");
		}
		Utils.waitForPageLoad(driver);
		return value;
	}

	/**
	 * to select a size swatch
	 * @param productIndex - 
	 * @param index -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String selectColorSwatchSpecialProductSet(int productIndex, int index) throws Exception {
		WebElement colorSwatches = driver.findElement(By.cssSelector(".product-set-item:nth-of-type(" + productIndex
				+ ") .attribute.color > div  > ul > li:nth-of-type("+ index +")"));
		if (colorSwatches.getAttribute("class").contains("selected") == false) {
			BrowserActions.javascriptClick(colorSwatches.findElement(By.cssSelector("a")), driver, "Color Swatch");
		}
		colorSwatches = driver.findElement(By.cssSelector(".product-set-item:nth-of-type(" + productIndex
				+ ") .attribute.color > div  > ul > li:nth-of-type("+ index +")"));
		return colorSwatches.findElement(By.cssSelector("img")).getAttribute("alt").trim();
	}
	/**
	 * to get selected color
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSelectedColorValue()throws Exception{
		return BrowserActions.getText(driver, txtSelectedColor, "Selected Color").trim();

	}
	
	 /**
     * to verify size Swatches are updated based on color selection
     *
     * @return boolean
     * @throws Exception - Exception
     */
    public boolean verifySizeswatchesUpdatedOrNot()throws Exception{
    	int selectable=lstSizeSwatchesSelectable.size();
    	int unselectable=lstSizeSwatchesUnSelectable.size();
    	int size=lstSizeSwatches.size();
    	Log.message("Total available size swatches are: "+ size+". Selectable Sizes are: "+selectable +" and Unselectable sizes are: "+unselectable);	
     	if(selectable+unselectable==size){
     		return true;
     		}
    return false;
    }
    


	/**
	 * to get selected color for special product set
	 * @param index -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSelectedColorValueSpecialProductSet(int index)throws Exception{
		WebElement element = driver.findElement(By.cssSelector(".product-set-item:nth-of-type("+ index +") .attribute:nth-of-type(2) > div > span"));
		return BrowserActions.getText(driver, element, "Selected Color").trim();
	}

	/**
	 * to verify if selected size is displayed
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySelectedSizeIsDisplayed()throws Exception{
		for (WebElement size : lstSizeSwatches) {
			Utils.waitForElement(driver, size);
			if(size.getAttribute("class").contains("selected"))
			{
				if(txtSelectedSize.getText().trim().equals(size.findElement(By.cssSelector("input")).getText().trim()))
					return true;
				else
					return false;
			}
		}
		return false;
	}

	/**
	 * to verify if selected size is displayed Special Product Set
	 * @param index -
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySelectedSizeIsDisplayed(int index)throws Exception{
		try {
			WebElement selectedSize = driver.findElement(By.cssSelector(".size .selected"));
			BrowserActions.scrollToView(selectedSize, driver);
			if(Utils.waitForElement(driver, selectedSize))
				return true;
			else
				return false;
		}catch(NoSuchElementException e) {
			return false;
		}
		
	}

	/**
	 * to verify if size is out of stock
	 * @param index -
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySizeIsOutOfStock(int index)throws Exception{
		if(lstSizeSwatches.get(index).getAttribute("class").contains("unselectable"))
			return true;
		else
			return false;
	}

	/**
	 * to verify if color is out of stock
	 * @param index -
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyColorIsOutOfStock(int index)throws Exception{
		if(lstColorSwatches.get(index).getAttribute("class").contains("unselectable"))
			return true;
		else
			return false;
	}

	/**
	 * to get the selected size value
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSelectedSize()throws Exception{
		return BrowserActions.getText(driver, txtSelectedSizeValue, "Selected Size Value");
	}

	/**
	 * to get the selected size value
	 * @param index -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSelectedSizeSpecialProductSet(int index)throws Exception{
		WebElement sizeSwatches = driver.findElements(By.cssSelector(".product-set-item")).get(index-1).findElement(By.cssSelector(" .attribute.size > div  > span.selected-value"));
		return sizeSwatches.getText().trim();
	}

	/**
	 * to get the selected color value
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSelectedColor()throws Exception{
		return BrowserActions.getText(driver, txtSelectedColorValue, "Selected Color Value");
	}

	/**
	 * to click Add To Bag
	 * @throws Exception - Exception
	 */
	public void addToBag()throws Exception{
		BrowserActions.clickOnElementX(btnAddToBag, driver, "Add to bag");
		Utils.waitForPageLoad(driver);
	}

	public void clickOnUpdateCartSplProdEditOverlay() throws Exception{
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, btnUpdateCartEditOverlay)) {
			Actions action = new Actions(driver);
			action.moveToElement(btnUpdateCartEditOverlay);
			BrowserActions.clickOnElementX(btnUpdateCartEditOverlay, driver, "Update Information");
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * to click Add To Bag in special product set
	 * @throws Exception - Exception
	 */
	public void addToBagSpecialProductSet()throws Exception{
		BrowserActions.clickOnElementX(btnAddToBagSpecialProductSet, driver, "Add to bag");
	}
	
	/**
	 * to get the flyout information
	 * @throws Exception - Exception
	 */
	public String gettextFromHemmingFlyout()throws Exception{
		String Text= null;
		if(Utils.waitForElement(driver, hemmingtipsFlyout)){
		Text= BrowserActions.getText(driver, hemmingFlyoutInfo, "Hemming flyout information");
		}
	return Text;
	}


	/**
	 * to verify the Add To Bag error msg for special product set
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySelectSizeErrorForSpecialProductSet()throws Exception{
		BrowserActions.clickOnElementX(btnAddToBagSpecialProductSet, driver, "Add to bag error");
		String error =  BrowserActions.getText(driver, btnAddToBagSpecialProductSet, "Add to bag error");
		if(error.trim().equalsIgnoreCase("Please Select Size"))
			return true;
		else
			return false;
	}

	/**
	 * To verify vertical alignment of child products in quickshop
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyVerticalAllignmentOfChildProductsInCart()throws Exception {
		for(int i = 1; i < lstSpecialProductChildInCart.size(); i++){
			if(BrowserActions.verifyVerticalAllignmentOfElements(driver, lstSpecialProductChildInCart.get(i-1), lstSpecialProductChildInCart.get(i))==false)
				return false;
		}
		return true;
	}

	/**
	 * to verify if the product image zoomed
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPrimaryProductImageZoomed()throws Exception{	
		if(divZoomWindowContainer.isDisplayed())
			return true;
		else
			return false;
	}	


	/**
	 * to close Add To Bag overlay
	 * @throws Exception - Exception
	 */
	public void closeCartOverlay()throws Exception{
		BrowserActions.clickOnElementX(btnCloseCartOverLay, driver, "Close Add to bag Overlay");
	}

	/**
	 * to verify if height of Quick shop modal
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyQuickShopModalHeight()throws Exception{
		int height = divQuickShop.getSize().height;
		Log.message("The Quick Shop modal height is : "+ height +"px");
		//As per test case, the max height should be 700px. But we ignore a negligible 5px
		if(height >= 650 && height<= 705)
			return true;
		else
			return true;
	}

	/**
	 * to verify if height of product-variation-content
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyproductVariationContentHeight()throws Exception{
		boolean scrollBarPresent = false;
		int height_Product_Content = divProductContent.getSize().height;
		int height_Product_Variant = divproductVariant.getSize().height;
		if(height_Product_Content >= height_Product_Variant)			
			scrollBarPresent = (boolean) ((JavascriptExecutor)driver).executeScript("return arguments[0].scrollHeight > arguments[0].clientHeight;", divproductVariant);
		return scrollBarPresent;
	}
	
	/**
	 * To close quickshop overlay by clicking outside the overlay window
	 * @throws Exception
	 */
	public void closeQuickShopOverlayByClickingOutSide() throws Exception {
        if (Utils.getRunBrowser(driver).equals("safari")) {
               int x = divQuickShop.getLocation().getX() - 10;
               int y = divQuickShop.getLocation().getY();
               String code = "document.elementFromPoint(" + x + "," + y + ").click();";
               ((JavascriptExecutor) driver).executeScript(code);
        } else {
               Actions action = new Actions(driver);
               action.moveToElement(btnClose, 0, 0).click().build().perform();
        }
	}

	/**
	 * to verify if the product Attribute
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyProductContentAttributes()throws Exception{
		Utils.waitForElement(driver, divproductVariant.findElement(By.className("product-price")), 10);
		Utils.waitForElement(driver, divproductVariant.findElement(By.className("promotion")), 10);
		Utils.waitForElement(driver, divproductVariant.findElement(By.className("product-variations")), 10);

		String dataAttributes = divproductVariant.findElement(By.className("product-variations")).getAttribute("data-attributes");		
		if(dataAttributes.contains("size") && dataAttributes.contains("color"))
			return true;
		else
			return false;
	}

	/**
	 * to verify if the product Attribute
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyQuickViewScroll()throws Exception{		
		boolean scrollBarPresent = false;
		int height_Product_Content = divProductContent.getSize().height;
		int height_Product_Variant = divproductVariant.getSize().height;
		if(height_Product_Content >= height_Product_Variant){			
			scrollBarPresent = (boolean) ((JavascriptExecutor)driver).executeScript("return arguments[0].scrollHeight > arguments[0].clientHeight;", divproductVariant);
			BrowserActions.scrollToView(divAttributeLabel, driver);			
		}
		return scrollBarPresent;
	}	

	/**
	 * to get the selected quantity value
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSelectedQuantityValue()throws Exception{
		return BrowserActions.getText(driver, txtSelectedQuantity, "Selected Quantity").trim();
	}

	/**
	 * to get the quantity dropdown values
	 * @return List
	 * @throws Exception - Exception
	 */
	public List<String> getQuantityDropdownValues()throws Exception{
		List<String> values =  new ArrayList<String>();
		for (int i = 0; i < lstQuantitySelectionList.size(); i++) {
			values.add(lstQuantitySelectionList.get(i).getAttribute("label").trim());
		}
		return values;
	}

	/**
	 * to click Add To Wishlist
	 * @return object
	 * @throws Exception - Exception
	 */
	public Object addToWishlist()throws Exception{
		BrowserActions.clickOnElementX(lnkAddToWishList, driver, "Add to wishlist");
		if(Utils.waitForElement(driver, divSearchWishlist))
			return new WishlistLoginPage(driver).get();
		else
			return new WishListPage(driver).get();
	}

	/**
	 * to click view full detail
	 * @return PdpPage
	 * @throws Exception - Exception
	 */
	public PdpPage viewFullDetail()throws Exception{
		BrowserActions.clickOnElementX(lnkViewFullDetail, driver, "view full detail");
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}

	/**
	 * to click view full detail
	 * @return PdpPage
	 * @throws Exception - Exception
	 */
	public PdpPage viewFullDetailForSpecialProductSet()throws Exception{
		BrowserActions.clickOnElementX(lnkViewFullDetailSpecialProductSet, driver, "view full detail");
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}

	/**
	 * To get number of alternate images
	 * @return int
	 * @throws Exception - Exception
	 */
	public int getNumberOfAlternateImages()throws Exception{
		return lstThumbNailImages.size();
	}

	/**
	 * if number of alternate images greater than 4. Then check if the 5th and above alternate images are not active.
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyAlternateImagesNotActive()throws Exception{
		if(getNumberOfAlternateImages() > 4)
		{
			for(int i = 4; i < getNumberOfAlternateImages(); i++)
			{
				if(lstThumbNailImages.get(i).getAttribute("class").contains("slick-active"))
					return false;
			}
		}
		return true;
	}

	/**
	 * To get number of alternate images
	 * @return int
	 * @throws Exception - Exception
	 */
	public int getNumberOfAlternateImagesForSpecialProductSet()throws Exception{
		return lstSpecialProductSetThumbNailImages.size();
	}

	/**
	 * if number of alternate images greater than 4. Then check if the 5th and above alternate images are not active.
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyAlternateImagesNotActiveForSpecialProductSet()throws Exception{
		if(getNumberOfAlternateImagesForSpecialProductSet() > 4)
		{
			for(int i = 4; i < getNumberOfAlternateImagesForSpecialProductSet(); i++)
			{
				if(lstSpecialProductSetThumbNailImages.get(i).getAttribute("class").contains("selected"))
					return false;
			}
		}
		return true;
	}

	/**
	 * To verify if next link enabled for alternate images
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyNextLinkEnabled()throws Exception{
		if(lnkNext.getAttribute("class").contains("slick-disabled"))
			return false;
		else 
			return true;
	}

	/**
	 * To verify if previous link enabled for alternate images
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPreviousLinkEnabled()throws Exception{
		if(lnkPrevious.getAttribute("class").contains("slick-disabled"))
			return false;
		else 
			return true;
	}

	/**
	 * To click next link for alternate images
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyNextLinkGreyedOut()throws Exception{
		while(verifyNextLinkEnabled())
		{
			BrowserActions.javascriptClick(lnkNext, driver, "Next");
		}
		Log.event("Slick Button Next disabled.");
		/*int images = getNumberOfAlternateImages();
		if(lstThumbNailImages.get(images - 1).getAttribute("class").contains("slick-active"))
			return true;
		else 
			return false;*/
		
		if(lnkNext.getAttribute("class").contains("slick-disabled"))
			return true;
		else
			return false;
	}

	/**
	 * To click previous link for alternate images
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPreviousLinkGreyedOut()throws Exception{
		while(verifyPreviousLinkEnabled())
		{
			BrowserActions.javascriptClick(lnkPrevious, driver, "Previous");
		}
		if(lstAlternateImages.get(0).getAttribute("class").contains("slick-active"))
			return true;
		else 
			return false;
	}


	/**
	 * to verify if the monogramming checkbox selected or not
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyMonogrammingCheckboxSelected()throws Exception{
		if(chkEnableMonogramming.isSelected())
			return true;
		else
			return false;
	}

	/**
	 * To check/uncheck monogramming checkbox
	 * @param state - To Open or close --- 'enable', 'disable'
	 * @throws Exception - Exception
	 */
	public void clickOnMonogrammingCheckbox(String state)throws Exception{
		if(Utils.waitForElement(driver, scrollBar)) {
			if(scrollBar.getCssValue("top").split("px")[0].equalsIgnoreCase("0")) {
				BrowserActions.dragElement(scrollBar, driver, "Quickshop Scroll Bar", Direction.Down);
				Log.message("Scrolled down.", driver);
			}
		}
		if(state.equalsIgnoreCase("enable"))
		{
			if(chkEnableMonogramming.isSelected() == false)
			{
				BrowserActions.clickOnElementX(chkEnableMonogramming, driver, "Enable Monogramming Checkbox");
			}
		}
		else if(state.equalsIgnoreCase("disable"))
		{
			if(chkEnableMonogramming.isSelected() == true)
			{
				BrowserActions.clickOnElementX(chkEnableMonogramming, driver, "Disable Monogramming Checkbox");
			}
		}
	}

	/**
	 * to verify if the monogramming options drawer displayed or not
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyMonogrammingOptionsDrawerDisplayed()throws Exception{
		if(divMonogrammingOptions.findElement(By.cssSelector("div.monogramming-fields.hide")).getAttribute("style").contains("block"))
			return true;
		else
			return false;
	}

	/**
	 * to verify that the checkbox is displayed to the left of messaging
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyMessagingDisplayedRightOfMonogrammingCheckbox()throws Exception{
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, txtMonogramMessaging, chkEnableMonogramming);
	}

	/**
	 * to verify that the tips link is displayed to the right of monogramming title
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyTipsDisplayedRightOfMonogrammingTitle()throws Exception{
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, lnkMonogrammingTips, txtMonogrammingTitle);
	}

	/**
	 * To click on tips link
	 * @throws Exception - Exception
	 */
	public void clickOnTipsInMonogrammingSection()throws Exception{
		BrowserActions.clickOnElementX(lnkMonogrammingTips, driver, "Tips link");
	}

	/**
	 * To click on close button on the tips flyout
	 * @throws Exception - Exception
	 */
	public void clickOnTipsFlyoutCloseButton()throws Exception{
		BrowserActions.clickOnElementX(btnCloseTipsFlyout, driver, "Close button");
	}

	/**
	 * to verify the Hemming title
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyHemmingTitle()throws Exception{
		if(txtHemmingTitle.getText().equalsIgnoreCase("Hemming:"))
			return true;
		else
			return false;
	}
	/**
	 * to verify the Hemming title
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public String gethemmingMessage()throws Exception{
		BrowserActions.scrollInToView(txtHemmingTitle, driver);
		if(Utils.waitForElement(driver, txtHemmingTitle)){
			return BrowserActions.getText(driver, txtHemmingTitle, "Hemming message");
		}else
			return null;
	}
	
	public boolean verifyElementIsDisplayed(WebElement element) throws Exception{
		BrowserActions.scrollInToView(element, driver);
		if(element.isDisplayed()){
			return true;
		}
		return false;
	}
	
	/**
	 * to verify if the hemming checkbox selected or not
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyHemmingCheckboxSelected()throws Exception{
		if(chkProductHemming.isSelected())
			return true;
		else
			return false;
	}

	/**
	 * to verify that the hemming checkbox is displayed to the left of hemming tips
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyHemmingCheckboxDisplayedToTheLeftOfHemmingTips()throws Exception{
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, lnkHemmingTips, chkProductHemming);
	}

	/**
	 * To check/uncheck hemming checkbox
	 * @param state - To Open or close --- 'enable', 'disable'
	 * @throws Exception - Exception
	 */
	public void clickOnHemmingCheckbox(String state)throws Exception{
		BrowserActions.scrollInToView(chkProductHemming, driver);
		if(state.equalsIgnoreCase("enable"))
		{
			if(chkProductHemming.isSelected() == false)
			{
				BrowserActions.clickOnElementX(chkProductHemming, driver, "Enable Hemming Checkbox");
			}
		}
		else if(state.equalsIgnoreCase("disable"))
		{
			if(chkProductHemming.isSelected() == true)
			{
				BrowserActions.clickOnElementX(chkProductHemming, driver, "Disable Hemming Checkbox");
			}
		}
	}

	/**
	 * to verify that the hemming checkbox is displayed to the left of hemming tips
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyMessagingDisplayedRightOfHemmingCheckbox()throws Exception{
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, txtHemmingMessaging, chkProductHemming);
	}

	/**
	 * to verify that the hemming title is displayed to the left of hemming tips
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyHemmingTipsDisplayedRightOfHemmingTitle()throws Exception{
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, lnkHemmingTips, txtHemmingTitle);
	}

	/**
	 * To click on tips link
	 * @throws Exception - Exception
	 */
	public void clickOnTipsInHemmingSection()throws Exception{
		BrowserActions.clickOnElementX(lnkHemmingTips, driver, "Tips link");
		Utils.waitForElement(driver, hemmingtipsFlyout);
	}

	/**
	 * to verify if the error message is displayed for the hemming options drawer
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyErrorMessageInHemmingOptions()throws Exception{
		BrowserActions.scrollInToView(txtHemmingErrorMessage, driver);
		String text= BrowserActions.getTextFromAttribute(driver, txtHemmingErrorMessage, "style", "Style is");
		String text1= BrowserActions.getText(driver, txtHemmingErrorMessage, "Error message");
		Log.message(text+"......"+text1);
		if(txtHemmingErrorMessage.getAttribute("style").contains("block") 
				&& txtHemmingErrorMessage.getText().equals("Please select hemming option."))
			return true;
		else
			return false;
	}

	/**
	 * to verify that the no hemming options selected by default
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyNoHemmingOptionsSelectedByDefault()throws Exception{
		for (WebElement element : lstHemmingOptions) {
			if(element.getAttribute("class").equals("hemming-option selected"))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * To click on hemming options
	 * @param index -
	 * @throws Exception - Exception
	 */
	public void selectHemmingOption(int index)throws Exception{
		BrowserActions.clickOnElementX(lstHemmingOptions.get(index), driver, "hemming options");
	}
	/**
	 * to get the selected hemming option
	 *  
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public String getSelectedHemmingValue()throws Exception{
		String hemmingValue= BrowserActions.getText(driver, selectedHemmingOption, "Selected Hemming value");
			return hemmingValue;
	}


	/**
	 * to verify if selected hemming option is highlighted
	 * @param index -
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySelectedHemmingOptionIsHighlighted(int index)throws Exception{
		if(lstHemmingOptions.get(index).getAttribute("class").trim().contains("selected"))
			return true;
		else
			return false;
	}

	/**
	 * to verify if the error message is displayed for the monogramming options drawer
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyErrorMessageInMonogrammingOptions()throws Exception{
		if(drpFontSelect.getAttribute("class").contains("mono-error")
				&&drpLocationSelect.getAttribute("class").contains("mono-error")
				&&drpColorSelect.getAttribute("class").contains("mono-error")
				&&(textField.findElement(By.cssSelector("input")).getAttribute("class")).contains("mono-error"))
			return true;
		else
			return false;
	}

	/**
	 * to select value in Font dropdown
	 * @param index -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String selectMonogrammingFontValue(int index)throws Exception{
		String dataToBeReturned = "";
		if (Utils.waitForElement(driver, txtFontSingleSelect)) {
			dataToBeReturned = BrowserActions.getText(driver,txtFontSingleSelectValue,"Font");
		} else if (Utils.waitForElement(driver, drpFontSelect)) {
			BrowserActions.javascriptClick(drpFontSelect, driver, "Font dropdown");
			WebElement option = drpFontSelect.findElements(By.cssSelector("option")).get(index);
			dataToBeReturned = BrowserActions.getText(driver,option,"Font"); 
			BrowserActions.javascriptClick(option, driver, "option to be selected");
		} else {
			Log.message("Font option not available", driver);
		}
		return dataToBeReturned.trim();
	}

	/**
	 * to select value in location dropdown
	 * @param index -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String selectMonogrammingLocationValue(int index)throws Exception{
		String dataToBeReturned = "";
		if (Utils.waitForElement(driver, txtLocationSingleSelect)) {
			dataToBeReturned = BrowserActions.getText(driver,txtLocationSingleSelectValue,"Location/Position");
		} else if (Utils.waitForElement(driver, drpLocationSelect)) {
			BrowserActions.clickOnElementX(drpLocationSelect, driver, "Location/Position dropdown");
			WebElement option = drpLocationSelect.findElements(By.cssSelector("option")).get(index);
			dataToBeReturned = BrowserActions.getText(driver,option,"Location/Position"); 
			BrowserActions.javascriptClick(option, driver, "option to be selected");
		} else {
			Log.message("Location/Position option not available", driver);
		}
		return dataToBeReturned.trim();
	}

	/**
	 * to select value in Color dropdown
	 * @param index -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String selectMonogrammingColorValue(int index)throws Exception{
		String dataToBeReturned = "";
		if (Utils.waitForElement(driver, txtColorSingleSelect)) {
			dataToBeReturned = BrowserActions.getText(driver,txtColorSingleSelectValue,"Color");
		} else if (Utils.waitForElement(driver, drpLocationSelect)) {
			BrowserActions.javascriptClick(drpColorSelect, driver, "Color dropdown");
			WebElement option = drpColorSelect.findElements(By.cssSelector("option")).get(index);
			dataToBeReturned = BrowserActions.getText(driver,option,"Color"); 
			BrowserActions.javascriptClick(option, driver, "option to be selected");
		} else {
			Log.message("Color option not available", driver);
		}
		return dataToBeReturned;
	}
	
	/**
	 * To type in the Monogram Text field
	 * @param value - Text to type
	 * @throws Exception - Exception
	 */
	public void typeInMonogrammingTextField(String value)throws Exception{
		BrowserActions.scrollInToView(txtMonogramTextArea, driver);
		BrowserActions.typeOnTextField(txtMonogramTextArea, value, driver, "Monogram Text");
	}

	/**
	 * to type in the Text field
	 * @param characterCount -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String typeInTextFieldFormonogramming(int characterCount)throws Exception{
		String maxCharacterCount = txtMaximumCharacterCount.getText().split(" ")[0];
		maxCharacterCount = maxCharacterCount.replace("/", "");
		String randomString = RandomStringUtils.randomAlphanumeric(characterCount);
		textField.findElement(By.cssSelector("input")).sendKeys(randomString);
		return randomString;
	}

	/**
	 * to get number of characters typed in the Text field
	 * @return int
	 * @throws Exception - Exception
	 */
	public int getNumberOfCharactersInTextField()throws Exception{
		return textField.findElement(By.cssSelector("input")).getAttribute("value").trim().length();
	}

	/**
	 * to get the actual character count
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getActualCharacterCount()throws Exception{
		String count = BrowserActions.getText(driver, txtActualCharacterCount, "Actual Character Count").trim();
		return count;
	}

	/**
	 * to get the max character count
	 * @return int
	 * @throws Exception - Exception
	 */
	public int getMaximumCharacterCount()throws Exception{
		String maxCharacterCount = txtMaximumCharacterCount.getText().split(" ")[0];
		maxCharacterCount = maxCharacterCount.replace("/", "");
		int count = Integer.parseInt(maxCharacterCount);
		return count;
	}

	/**
	 * to get the text in Preview field
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getPreviewText()throws Exception{
		return txtMonogramPreview.getAttribute("value").trim();
	}

	/**
	 * To click on close button on the tips flyout
	 * @throws Exception - Exception
	 */
	public void clickOnMiniCartFlyoutCloseButton()throws Exception{
		if(Utils.waitForElement(driver, btnCloseMiniCartOverLay))
			BrowserActions.clickOnElementX(btnCloseMiniCartOverLay, driver, "Close button");
	}
	
	/**
     * To click on checkout button in ATB overlay
     *
     * @return ShoppingBagPage -
     * @throws Exception -
     */
    public ShoppingBagPage clickOnCheckoutInMCOverlay() throws Exception {
        Utils.waitForElement(driver, btnCheckoutInMCOverlay);
        BrowserActions.clickOnElementX(btnCheckoutInMCOverlay, driver, "Checkout Button in Mini Cart Overlay ");
        Utils.waitForPageLoad(driver);

        return new ShoppingBagPage(driver).get();
    }

	/**
	 * to verify that the Size Chart Label is to the right of Size Chart icon
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySizeChartLabelDisplayedRightOfSizeChartIcon()throws Exception{
		if(Utils.waitForElement(driver, txtSizeChart)) 
			return BrowserActions.verifyHorizontalAllignmentOfElements(driver, txtSizeChart, imgSizeChartIcon);
		else 
			Log.failsoft("Test data not applicable for Size Chart.");
		return false;
	}

	/**
	 * to get the text in Sizechart Label
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySizeChartLabel()throws Exception{
		if(Utils.waitForElement(driver, txtSizeChart)) { 
			if(BrowserActions.getText(driver, txtSizeChart, "Size Chart Label").trim().equalsIgnoreCase("Size Chart"))
				return true;
			else 
				return false;
		}else 
			Log.failsoft("Test data not applicable for Size Chart.");
		return false;
		
	}

	/**
	 * To verify the product is having size chart link
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean isSizeChartApplicable()throws Exception{
		BrowserActions.scrollInToView(txtSizeChart, driver);
		if(Utils.waitForElement(driver, txtSizeChart))
			return true;
		else
			return false;
	}
	
	/**
	 * To click on size chart link
	 * @throws Exception - Exception
	 */
	public void clickOnSizeChartLink()throws Exception{
		BrowserActions.clickOnElementX(lnkSizeChart, driver, "size chart link");
	}

	/**
	 * To close the size chart modal popup
	 * @return QuickShop
	 * @throws Exception - Exception
	 */
	public QuickShop closeSizeChartModalPopup()throws Exception{
		BrowserActions.javascriptClick(btnCloseSizeChartModal, driver, "Size Chart");
		//Clicking close button, the mouse focus falls on the Sign In link on headers and flyout opens.
		//Hence changing the mouse focus
		Actions action = new Actions(driver);
		action.moveByOffset(10, 10).build().perform();
		return new QuickShop(driver).get();
	}

	/**
	 * To select quanity
	 * @param qty -
	 * @return String - 
	 * @throws Exception -
	 */
	public String selectQty(String... qty) throws Exception {
		if (Utils.waitForElement(driver, drpQty)) {
			BrowserActions.scrollToViewElement(drpQty, driver);
			Select qtySelect = new Select(drpQty);
			if (qty.length > 0) {
				if (qtySelect.getAllSelectedOptions().get(0).getText().equals(qty[0]) || drpQty.getText().equals(qty[0]))
					return qty[0];
			}
			if (qty.length > 0)
				qtySelect.selectByVisibleText(qty[0].trim());
			else
				qtySelect.selectByIndex(4);

			return drpQty.getAttribute("value") == null ? drpQty.getText() : drpQty.getAttribute("value");
		}

		if (qty.length > 0)
			if (selectedQty1.getText().trim().equals(qty[0]))
				return selectedQty1.getText().trim();

		if (!(Utils.waitForElement(driver, drpGiftCardSize) || Utils.waitForElement(driver, drpGiftCardSizeSelect))) {
			((JavascriptExecutor) driver).executeScript("arguments[0].style.opacity=1", selectedQty1);
			BrowserActions.clickOnElementX(selectedQty1, driver, "Quantity dropdown");
			Log.event("Clicked on Quantity Dropdown");
		}else{
			BrowserActions.scrollToViewElement(selectedQty1, driver);
			BrowserActions.clickOnElementX(selectedQty1, driver, "Quantity dropdown");
		}


		if (qty.length > 0) {
			if (Utils.waitForElement(driver,
					driver.findElement(
							By.xpath("//div[@class='quantity']//ul[@class='selection-list']//li[contains(text(),'"
									+ qty[0] + "')]")))) {
				WebElement qtyToSelect = driver.findElement(
						By.xpath("//div[@class='quantity']//ul[@class='selection-list']//li[contains(text(),'" + qty[0]
								+ "')]"));
				BrowserActions.clickOnElementX(qtyToSelect, driver, "Quantity ");
				Log.event("Clicked on Expected Quantity...");
				BrowserActions.scrollToViewElement(selectedQty1, driver);
				return qty[0];
			} else {
				Log.failsoft("--->>>Given Quantity Not Available...", driver);
				return selectedQty.getText();
			}
		} else {
			int rand = Utils.getRandom(0, lstQtySelectable.size());
			BrowserActions.clickOnElementX(lstQtySelectable.get(rand), driver, "Quantity menu ");
			Log.message("Clicked on random Quantity...");
			BrowserActions.scrollToViewElement(selectedQty1, driver);
			return selectedQty.getText();
		}
	}

	/**
	 * To select product size
	 * @param size - 
	 * @return String -
	 * @throws Exception -
	 */
	public String selectSize(String... size) throws Exception {
		scrollQuickViewForElement(divRegularSize);
		if (!(size.length > 0) || (size.length > 0 && size[0].equals("random"))) {
			if (Utils.waitForElement(driver, selectedSize)) {
				Log.event("Size is already selected.");
				return BrowserActions.getText(driver, txtSelectedSizeValue, "Selected size");
			}
		}

		if (size.length > 0 && (!size[0].equals("random"))) {
			for (int i = 0; i < lstSizeSwatchesSelectable.size(); i++) {
				if (lstSizeSwatchesSelectable.get(i).getText().trim().equals(size[0])) {
					BrowserActions.scrollToViewElement(lstSizeSwatchesSelectable.get(i), driver);
					BrowserActions.clickOnElementX(lstSizeSwatchesSelectable.get(i), driver, size[0]+"Size Swatch ");
					Log.event("Clicked On "+size[0]+" Size...");
					waitForSpinner();
					return BrowserActions.getText(driver, txtSelectedSizeValue, "Selected size");
				}
			}
			for (int i = 0; i < lstSizeSwatchesUnSelectable.size(); i++) {
				if (lstSizeSwatchesUnSelectable.get(i).getText().trim().equals(size[0])) {
					if(size.length == 2) {
						if(size[1].equals("true")) {
							BrowserActions.clickOnElementX(lstSizeSwatchesUnSelectable.get(i), driver, "Size Swatches ");
							Utils.waitUntilElementDisappear(driver, waitLoader);
							return BrowserActions.getText(driver, txtSelectedSizeValue, "Selected size");
						}
					}else {
						Log.message("--->>> Given Size(" + size[0] + ") <b>Not Available</b> in Size list. Selecting Random Size...");
						break;
					}
				}
			}
			Log.fail("Something went wrong. Please check the input parameters...", driver);
			return null;
		} else {
			List<WebElement> lstSizeSwatchesSelectable;
			try {
				BrowserActions.scrollInToView(shoeSize, driver);
			} catch (Exception e) {
				Log.message("Shoe size is not available.", driver);
			}
			if(Utils.waitForElement(driver, shoeSize)) {
				lstSizeSwatchesSelectable = driver.findElements(By.cssSelector("ul.shoesize> li"));
				WebElement shoeWidth = driver.findElement(By.cssSelector("ul.shoewidth li.selectable a"));
				BrowserActions.clickOnSwatchElement(shoeWidth, driver, "Shoe Width");
				Utils.waitForPageLoad(driver);
				if(Utils.waitForElement(driver, scrollBar)) {
					if(scrollBar.getCssValue("top").split("px")[0].equalsIgnoreCase("0")) {
						BrowserActions.dragElement(scrollBar, driver, "Quickshop Scroll Bar", Direction.Down);
						Log.message("Scrolled down.", driver);
					}
				}
				lstSizeSwatchesSelectable = driver.findElements(By.cssSelector("ul.shoesize> li"));
				BrowserActions.clickOnSwatchElement(lstSizeSwatchesSelectable.get(0), driver, "Size Swatches ");
				Utils.waitUntilElementDisappear(driver, waitLoader);
				return BrowserActions.getText(driver, txtSelectedShoeSizeValue, "Selected Shoe sizee");
			}else {
				lstSizeSwatchesSelectable = driver.findElements(By.cssSelector("ul.size> li.selectable:not([class*='selected']) >a"));
				BrowserActions.scrollInToView(lstSizeSwatchesSelectable.get(0), driver);
				BrowserActions.clickOnSwatchElement(lstSizeSwatchesSelectable.get(0), driver, "Size Swatches ");
				Utils.waitUntilElementDisappear(driver, waitLoader);
				Log.event("Clicked On First Size...");
				return BrowserActions.getText(driver, txtSelectedSizeValue, "Selected size");
			}
		}
	}
	
	public String selectBraBandSize(String... braBandSize) throws Exception {
		if (braBandSize.length > 0) {
			for (WebElement braBandSwatch : lstBraSizeSwatches) {
				if (braBandSwatch.getText().equalsIgnoreCase(braBandSize[0])) {
					BrowserActions.scrollInToView(braBandSwatch, driver);
					BrowserActions.clickOnSwatchElement(braBandSwatch, driver, "bra band size swatch");
				}
			}
		} else {
			BrowserActions.scrollInToView(lstBraSizeSwatches.get(0), driver);
			BrowserActions.clickOnSwatchElement(lstBraSizeSwatches.get(0), driver, "bra band size swatch");
		}
		Utils.waitUntilElementDisappear(driver, waitLoader);
		return BrowserActions.getText(driver, txtSelectedBraBandSizeValue, "Selected bra band size");
	}
	
	public String selectBraCupSize(String... braCup) throws Exception {
		scrollQuickViewForElement(divBraCupSwatches);
		if (braCup.length > 0) {
			for (WebElement braCupSwatch : lstBraCupSizeSwatches) {
				if (braCupSwatch.getText().equalsIgnoreCase(braCup[0])) {
					BrowserActions.scrollInToView(braCupSwatch, driver);
					BrowserActions.clickOnSwatchElement(braCupSwatch, driver, "bra cup size swatch");
				}
			}
		} else {
			BrowserActions.scrollInToView(lstBraCupSizeSwatches.get(0), driver);
			BrowserActions.clickOnSwatchElement(lstBraCupSizeSwatches.get(0), driver, "bra cup size swatch");
		}
		Utils.waitUntilElementDisappear(driver, waitLoader);
		return BrowserActions.getText(driver, txtSelectedBraCupSizeValue, "Selected bra cup size");
	}
	
	public String selectShoeWidth(String... shoeWidth) throws Exception {
		if (shoeWidth.length > 0) {
			for (WebElement shoeWidthSwatch : lstShoeWidthSwatches) {
				if (shoeWidthSwatch.getText().equalsIgnoreCase(shoeWidth[0])) {
					BrowserActions.scrollInToView(shoeWidthSwatch, driver);
					BrowserActions.clickOnSwatchElement(shoeWidthSwatch, driver, "shoe width swatch");
				}
			}
		} else {
			BrowserActions.scrollInToView(lstShoeWidthSwatches.get(0), driver);
			BrowserActions.clickOnSwatchElement(lstShoeWidthSwatches.get(0), driver, "shoe width swatch");
		}
		Utils.waitUntilElementDisappear(driver, waitLoader);
		return BrowserActions.getText(driver, txtSelectedShoeWidthValue, "Selected show width size");
	}
	
	public String selectShoeSize(String... shoeSize) throws Exception {
		scrollQuickViewForElement(divShoeSizeSwatches);
		if (shoeSize.length > 0) {
			for (WebElement shoeSizeSwatch : lstShoeSizeSwatches) {
				if (shoeSizeSwatch.getText().equalsIgnoreCase(shoeSize[0])) {
					BrowserActions.scrollInToView(shoeSizeSwatch, driver);
					BrowserActions.clickOnSwatchElement(shoeSizeSwatch, driver, "shoe size swatch");
				}
			}
		} else {
			BrowserActions.scrollInToView(lstShoeSizeSwatches.get(0), driver);
			BrowserActions.clickOnSwatchElement(lstShoeSizeSwatches.get(0), driver, "shoe size swatch");
		}
		Utils.waitUntilElementDisappear(driver, waitLoader);
		return BrowserActions.getText(driver, txtSelectedShoeSizeValue, "Selected shoe size");
	}

	/**
	 * to select color in pdp page
	 * @param color -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String selectColor(String... color) throws Exception {
		String selectedColorTxt;
		if(Utils.waitForElement(driver, scrollBar)) {
			if(scrollBar.getCssValue("top").split("px")[0].equalsIgnoreCase("0")) {
				BrowserActions.dragElement(scrollBar, driver, "Quickshop Scroll Bar", Direction.Down);
				Log.message("Scrolled down.", driver);
			}
		}
		
		if (!(color.length > 0)) {
			if (Utils.waitForElement(driver, selectedColor)) {
				selectedColorTxt = BrowserActions.getText(driver, txtSelectedColorValue, "Selected color name");
				return selectedColorTxt;
			}
		}
		
		if (color.length > 0 && color[0].equals("random")) {
			Log.event("Scrolled To Random Color...");
			BrowserActions.clickOnSwatchElement(lstColorSelectable.get(0), driver, "Color");
			//Log.event("Clicked on "+ lstColorSelectable.get(0).getAttribute("innerHTML") +" Color...");
			waitForSpinner();
			selectedColorTxt = BrowserActions.getText(driver, txtSelectedColorValue, "Selected color name");
			return selectedColorTxt;
		}
		
		if (color.length > 0) {
			String xpathForColor = "//ul[@class='swatches color']//li";
			if (Utils.waitForElement(driver, driver.findElement(By.xpath(xpathForColor)).findElement(By.cssSelector(".swatches.color img[alt='"+color[0].toUpperCase()+"']")))) {
				WebElement colorToSelect = driver.findElement(By.xpath(xpathForColor)).findElement(By.cssSelector(".swatches.color img[alt='"+color[0].toUpperCase()+"']"));
				BrowserActions.clickOnSwatchElement(colorToSelect, driver, "Quantity ");
				Log.event("Clicked on Expected Color...");
				waitForSpinner();
				return color[0];
			} else {
				Log.failsoft("--->>>Given Color Not Available...", driver);
				if (!Utils.waitForElement(driver, selectedColor)) {
					int rand = Utils.getRandom(0, lstColorSelectable.size());
					BrowserActions.clickOnSwatchElement(lstColorSelectable.get(rand), driver, "Quantity ");
					waitForSpinner();
					selectedColorTxt = BrowserActions.getText(driver, txtSelectedColorValue, "Selected color name");
					return selectedColorTxt;
				} else {
					selectedColorTxt = BrowserActions.getText(driver, txtSelectedColorValue, "Selected color name");
					return selectedColorTxt;
				}	
			}
		} else {
			int rand = Utils.getRandom(0, lstColorSelectable.size());
			BrowserActions.scrollInToView(lstColorSelectable.get(rand), driver);
			Log.event("Scrolled To Random Color...");
			BrowserActions.clickOnSwatchElement(lstColorSelectable.get(rand), driver, "Color");
			Log.event("Clicked on Random Color...");
			waitForSpinner();
			selectedColorTxt = BrowserActions.getText(driver, txtSelectedColorValue, "Selected color name");
			return selectedColorTxt;
		}
	}
	
	/**
     * to select size family and return the random size
     *
     * @param sizeF -
     * @return String
     * @throws Exception - Exception
     */
    public String selectSizeFamily(String... sizeF) throws Exception {
        if (!Utils.waitForElement(driver, divSizeFamily)) {
            return "No Size Family";
        }
        if (sizeF.length > 0) {
            String xpathForSizeFamily = "//ul[@class='swatches sizefamily']//li//a[contains(text(),'" + StringUtils.capitalize(sizeF[0]) + "')]";
            if (Utils.waitForElement(driver, driver.findElement(By.xpath(xpathForSizeFamily)))) {
                WebElement sizeFamilyToSelect = driver.findElement(By.xpath(xpathForSizeFamily));
                BrowserActions.clickOnSwatchElement(sizeFamilyToSelect, driver, "Size Family");
                Log.event("Clicked on Expected Size Family...");
            } else {
                Log.failsoft("--->>>Given Size Family Not Available...", driver);
                if (!Utils.waitForElement(driver, selectedSizeFamily)) {
                    int rand = Utils.getRandom(0, lstSizeFamilySelectable.size());
                    BrowserActions.clickOnSwatchElement(lstSizeFamilySelectable.get(rand), driver, "Size Family");
                }
            }
            return getSelectedSizeFamily();
        } else {
        	if(Utils.waitForElement(driver, selectedSizeFamily)) {
        		return BrowserActions.getText(driver, selectedSizeFamilyValue, "Size Family value").trim();
        	} else {
	            int SFSize = SizeFamSelectable.size();
	            if (SFSize > 0) {
	                int rand = Utils.getRandom(0, lstSizeFamilySelectable.size());
	                BrowserActions.clickOnSwatchElement(lstSizeFamilySelectable.get(rand), driver, "Size Faimly");
	                Log.event("Clicked on Random Size Family...");
	                Utils.waitForPageLoad(driver);
	                return lstSizeFamilySelectable.get(rand).getAttribute("title").split("\\:")[1].trim();
	            } else {
	                return getSelectedSizeFamily();
	            }
        	}
        }
    }
    
    public String getSelectedSizeFamily() throws Exception {
        if (Utils.waitForElement(driver, divSizeFamily)) {
            return BrowserActions.getText(driver, selectedSizeFamilyValue, "Size family selected value");
        } else
            return "No Size Family";
    }

	/**
	 * To verify is a QV has bra size option
	 * @return boolean - true/false is bra size is available
	 * @throws Exception - Exception
	 */
	public boolean isBraSizeAvailable() throws Exception {
		if (Utils.waitForElement(driver, divBraSize))
			return true;
		else
			return false;
	}
	
	/**
	 * To verify qty matches for sizefamily and shoewidth $ Bra type products
	 * @return boolean
	 * @throws Exception
	 */
	public boolean selectQtyWithColorAndSize(int qty) throws Exception {
		int type = 0;
		List<WebElement> element = null;
		if (sizeFamily.size() > 0) {
			type = sizeFamily.size();
			element = sizeFamily;
		} else if (lstShoeWidth.size() > 0) {
			type = lstShoeWidth.size();
			element = lstShoeWidth;
		}
		if (Utils.waitForElement(driver, divSizeFamily) || Utils.waitForElement(driver, divWidthGrid_shoe)) {
			boolean b = false;
			int i;
			do {
				i = 0;
				BrowserActions.clickOnElementX(element.get(i), driver, "Selected size");
				b = SelectQuantityMatches(qty);
				if (type > i)
					i++;
				if (b == true)
					return true;
			} while (b == false && i < type);
			return false;

		} else if (isBraSizeAvailable() == true) {
			int colors = lstColorSelectable.size();
			int bandSizes = lstBraSizeSwatches.size();
			int cupSizes = lstBraCupSizeSwatches.size();
			for (int color = 0; color < colors; color++) {
				for (int bandSize = 0; bandSize < bandSizes; bandSize++) {
					for (int cupSize = 0; cupSize < cupSizes; cupSize++) {

						selectColorSwatch(color);
						BrowserActions.clickOnElementX(lstBraSizeSwatches.get(bandSize), driver, "Bra Band size");
						BrowserActions.clickOnElementX(lstBraCupSizeSwatches.get(cupSize), driver, "Bra Band size");

						if (qty <= lstQty.size()) {
							String Qty = Integer.toString(qty);
							selectQty(Qty);
							Log.message(". Selected " + qty + " from dropdown");
							return true;
						}
					}
				}
			}
		} else {
			return SelectQuantityMatches(qty);
		}
		return false;
	}
	
	/**
	 * To select specific size with color and size
	 * @parameter qty value 
	 * @return boolean
	 */
	public boolean SelectQuantityMatches(int qty) throws Exception {
		int colors = lstColorSelectable.size(), sizes = 0;
		WebElement sizeSelected, selectableSize;
		if (lstSizeSwatches.size() > 0) {
			sizes = lstSizeSwatches.size();
			sizeSelected = selectedSize;
			selectableSize = lstSizeSwatchesSelectable.get(0);
		} else {
			sizes = lstShoeSizeSwatches.size();
			sizeSelected = selectedShoeSize;
			selectableSize = lstShoeSizeSwatches.get(0);
		}
		if (Utils.waitForElement(driver, selectedColor) && Utils.waitForElement(driver, sizeSelected)) {
			if (qty <= lstQty.size()) {
				String Qty = Integer.toString(qty);
				selectQty(Qty);
				Log.message(". Selected " + qty + " from dropdown");
				return true;
			}
		}
		if (Utils.waitForElement(driver, selectedColor) && Utils.waitForElement(driver, selectableSize)) {
			for (int s = 0; s < sizes; s++) {
				selectSizeSwatch(s);
				Log.event("Variations selected.");
				if (qty <= lstQty.size()) {
					String Qty = Integer.toString(qty);
					selectQty(Qty);
					Log.message(". Selected " + qty + " from dropdown");
					return true;
				}
			}
		} else if (Utils.waitForElement(driver, sizeSelected)
				&& Utils.waitForElement(driver, lstColorSelectable.get(0))) {
			for (int c = 0; c < colors; c++) {
				selectColorSwatch(c);
				if (qty <= lstQty.size()) {
					String Qty = Integer.toString(qty);
					selectQty(Qty);
					Log.message(". Selected " + qty + " from dropdown");
					return true;
				}
			}
		} else {
			if (Utils.waitForElement(driver, lstColorSelectable.get(0))
					&& Utils.waitForElement(driver, selectableSize)) {
				for (int c = 0; c < colors; c++) {
					for (int s = 0; s < sizes; s++) {
						selectColorSwatch(c);
						selectSizeSwatch(s);
						Log.event("Variations selected.");
						if (qty <= lstQty.size()) {
							String Qty = Integer.toString(qty);
							selectQty(Qty);
							Log.message(". Selected " + qty + " from dropdown");
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * To get product name
	 * @return String -
	 * @throws Exception -
	 */
	public String getProductName()throws Exception{
		BrowserActions.scrollToView(txtProductTitleDesktop, driver);
		return txtProductTitleDesktop.getText().trim();
	}
	
	/**
	 * To wait until pdp spinner disappear
	 */
	public void waitForSpinner() {
		//Utils.waitUntilElementDisappear(driver, PDPspinner);
		if(Utils.waitForElement(driver, waitLoader)) {
			Utils.waitUntilElementDisappear(driver, waitLoader);
		}
	}
	
	/**
	 * To verify quantity dropdown
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyQtyDropdown() throws Exception {
		/*int count = lstQty.size();
		boolean temp = true;
		for (int i = 0; i < count; i++) {
			String text = lstQty.get(i).getText();
			if (!(text.equals(Integer.toString(i + 1)))) {
				temp = false;
				break;
			}
		}
		return temp;*/
		
		boolean state = true;
		if (Utils.waitForElement(driver, drpQty)) {
			Select qty = new Select(drpQty);

			for(int x=1; x <= 10; x++)
				if (!(Integer.parseInt(qty.getOptions().get(x-1).getText().trim()) == (x)))
					return false;				

			return true;
		}
		try {
			if(lstQty.size() != 10) 
				Log.reference("Quantity Avaliable for Selected Variation :: " + lstQty.size());

			for (int i = 0; i < lstQty.size(); i++)
				if (!lstQty.get(i).getText().trim().equals(Integer.toString(i+1))){
					state = false;				
				}
		} catch (StaleElementReferenceException e) {
			return false;
		}

		return state;
	}
	
	/**
	 * To get number of products in product set
	 * @return Integer -
	 * @throws Exception -
	 */
	public int getNumberOfProductsInProductSet()throws Exception{
		Log.event("Number of Child Product Available :: " + childProducts.size());
		return childProducts.size();
	}
	
	/**
	 * To get number of products with size swatch selected
	 * @return Integer -
	 * @throws Exception -
	 */
	public int getNumberOfProductsSizeSwatchesSelected()throws Exception{
		Log.event("Number of Child Product Available with Size Selected:: " + lstSelectedSizeSwatch.size());
		return lstSelectedSizeSwatch.size();
	}
	
	/**
	 * To get number of products without size swatch selected
	 * @return Integer -
	 * @throws Exception -
	 */
	public int getNumberOfProductsSizeSwatchesNotSelected()throws Exception{
		int x = lstSelectedSizeSwatch.size();
		int y = childProducts.size();
		
		Log.event("Number of Products without Size Selected ::" + (y-x));
		return (y-x);
	}
	

	/**
	 * To get number of products with color swatch selected
	 * @return Integer -
	 * @throws Exception -
	 */
	public int getNumberOfProductsColorSwatchesSelected()throws Exception{
		Log.event("Number of Child Product Available with Color Selected:: " + lstSelectedColorSwatch.size());
		return lstSelectedColorSwatch.size();
	}
	
	/**
	 * To verify Zoom image fit with Product detail section
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyZoomImageFitWithProductDetailSection()throws Exception{
		int zoomImg_x = divZoomWindowContainer.getLocation().x;
		int zoomImg_width = divZoomWindowContainer.getSize().width;
		int prdSection_x = sectionProductDetails.getLocation().x;
		int prdSection_width = sectionProductDetails.getSize().width;
		
		if((zoomImg_x <= prdSection_x) && ((zoomImg_x + zoomImg_width) >= (prdSection_x + prdSection_width)))
			return true;
		else
			return false;
	}
	
	/**
	 * To return Current price / sale price of the product
	 * @return String - price
	 * @throws Exception -
	 */
	public String getSalePrice()throws Exception{
		BrowserActions.scrollToView(currentPrice, driver);
		String price = new String();
		price = currentPrice.getText().trim();
		return price;
	}
	
	/**
	 * To return Total price in add to bag overlay
	 * @return String - price
	 * @throws Exception -
	 */
	public String getTotalInMcOverlay()throws Exception{
		String price = new String();
		price = miniCartTotal.getText().trim();
		return price;
	}
	
	/**
	 * To verify given variation size swatch is grayed out
	 * @param sizeAvail Size -
	 * @param colorUnAvail Color -
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyColorSwatchGreyout(String sizeAvail, String colorUnAvail) throws Exception {
		selectSize(sizeAvail);
		String colorUnAvail1 = "//a[contains(translate(@title,'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'"
				+ colorUnAvail.toLowerCase() + "')]//parent::li";

		Log.event("Given Size :: " + sizeAvail);
		Log.event("Given Color ::" + colorUnAvail);
		Log.event("Formed xpath :: " + colorUnAvail1);

		WebElement colorUnAvailEle = driver.findElement(By.xpath(colorUnAvail1));

		if (colorUnAvailEle.getAttribute("class").equals("unselectable"))
			return true;
		else
			return false;

	}
	
	/**
	 * To mouse hover on Add to bag button on Desktop To tab on AddToBag button
	 * on Mobile/Tablet
	 * 
	 * @throws Exception - Exception
	 */
	public void mouseHoverAddToBag() throws Exception {
		if (Utils.isDesktop())
			BrowserActions.moveToElementJS(driver, btnAddToBag);
		else
			btnAddToBag.click();
	}
	
	/**
	 * To click on view details link
	 * @throws Exception -
	 */
	public PdpPage clickOnViewDetails()throws Exception{
		BrowserActions.clickOnElementX(lnkViewDetails, driver, "View Details");
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}
	
	/**
	 * to select all swatches on QuickShop
	 * @throws Exception - Exception
	 */
	public void selectAllSwatches() throws Exception {
		String sizeFamily = selectSizeFamily();
		Log.message("Selected Size Family :: " + sizeFamily, driver);
		String color = selectColor();
		Log.message("Selected Color :: " + color, driver);
		if (lstSizeSwatchesSelectable.size() > 0) {
			String size = selectSize();
			Log.message("Selected Size :: " + size, driver);
		} else if (lstBraSizeSwatches.size() > 0) {
			String braBandSize = selectBraBandSize();
			String braCupSize = selectBraCupSize();
			Log.message("Selected Bra Size :: " + braBandSize + " " + braCupSize, driver);
		} else if (lstShoeSizeSwatches.size() > 0) {
			String shoeWidth = selectShoeWidth();
			String shoeSize = selectShoeSize();
			Log.message("Selected Shoe Size :: " + shoeSize + " " + shoeWidth, driver);
		}
	}
	
	/**
	 * To select product variation that has a given minimum available quantity
	 * @param minQty - minimum needed available quantity for selected variation
	 * @throws Exception
	 */
	public void selectMinimumQuantityVariation(int minQty) throws Exception {
		int sizeCount = lstSizeSwatches.size() > 0 ? lstSizeSwatches.size()
				: (lstShoeSizeSwatches.size() > 0 ? lstShoeSizeSwatches.size() : lstBraCupSizeSwatches.size());
		int maxRetryCount = lstColorSelectable.size() > sizeCount ? lstColorSelectable.size() : sizeCount;
		Log.event("Will try maximum " + maxRetryCount + " times to get variation with minimum quantity of " + minQty);
		for (int i = 0; i < maxRetryCount; i++) {
			BrowserActions.refreshPage(driver);
			selectAllSwatches();
			if(getVariationAvailableQuantity() >= minQty)
				return;
		}
	}
	
	/**
	 * To get maximum selectable quantity of product variation
	 * @return Selectable quantity of current variation
	 * @throws Exception
	 */
	public int getVariationAvailableQuantity() throws Exception {
		if (!Utils.waitForElement(driver, availabilityMsg)) {
			Log.event("Product variation not selected. Selecting variation...");
			selectAllSwatches();
		}
		String qtyMessage = BrowserActions.getText(driver, availabilityMsg, "variation availabiliy message");
		int qtyAvailable;
		if(qtyMessage.contains("Out")) {
			return 0;
		} else if (qtyMessage.contains("Only")) {
			qtyAvailable = com.fbb.support.StringUtils.getNumberInString(qtyMessage);
		} else {
			qtyAvailable = lstQtySelectable.size()+1;
		}
		
		Log.event("Available quantity for selected variation:: " + qtyAvailable);
		return qtyAvailable;
	}
	
	private boolean scrollQuickViewForElement(WebElement element) throws Exception {
		if (Utils.waitForElement(driver, element)) {
			return true;
		}
		BrowserActions.scrollInToView(btnClose, driver);
		if (Utils.waitForElement(driver, scrollBar)) {
			if (scrollBar.getCssValue("top").split("px")[0].equalsIgnoreCase("0")) {
				BrowserActions.dragElement(scrollBar, driver, "Quickshop Scroll Bar", Direction.Down);
				Log.event("Scrolled down.");
			}
		}
		return Utils.waitForElement(driver, element);
	}

	/**
	 * To different color other than given color.
	 * @param color - Color to be omitted
	 * @return selected color name
	 * @throws Exception - Exception
	 */
	public String selectDifferentColor(String... color) throws Exception {
		if(lstColorSelectable.size() == 0) {
			Log.failsoft("There are no color to select!",driver);
		} else {
			for (int i = 0; i < lstColorSelectable.size(); i++) {
				WebElement colorSwatch = lstColorSelectable.get(i);
	            Log.event("Available colors is: " + colorSwatch.getAttribute("aria-label"));
	            if (!(colorSwatch.getAttribute("aria-label").trim().toLowerCase().contains(color[0].trim().toLowerCase()))) {
	                BrowserActions.clickOnSwatchElement(colorSwatch, driver, color[0] + " Color Swatch ");
	                Log.event("Clicked On " + color[0] + " Color...");
	                return BrowserActions.getText(driver, txtSelectedColorValue, "Selected color variant name");
	            }
	        }
		}
		waitForSpinner();
        return null;
	}

	/**
     * To select the different size of the variation product, even size is already selected
     * @param size
     * @return selected color name
     * @throws Exception
     */
	public String selectDifferentSize(String... size) throws Exception{
		if (size.length > 0 && (!size[0].equals("random"))) {
			for (int i = 0; i < lstSizeSwatchesSelectable.size(); i++) {
				if (lstSizeSwatchesSelectable.get(i).getText().trim().equals(size[0])) {
					BrowserActions.scrollToViewElement(lstSizeSwatchesSelectable.get(i), driver);
					BrowserActions.clickOnElementX(lstSizeSwatchesSelectable.get(i), driver, size[0]+"Size Swatch ");
					Log.event("Clicked On "+size[0]+" Size...");
					waitForSpinner();
					return BrowserActions.getText(driver, txtSelectedSizeValue, "Selected size");
				}
			}
			for (int i = 0; i < lstSizeSwatchesUnSelectable.size(); i++) {
				if (lstSizeSwatchesUnSelectable.get(i).getText().trim().equals(size[0])) {
					if(size.length == 2) {
						if(size[1].equals("true")) {
							BrowserActions.clickOnElementX(lstSizeSwatchesUnSelectable.get(i), driver, "Size Swatches ");
							Utils.waitUntilElementDisappear(driver, waitLoader);
							return BrowserActions.getText(driver, txtSelectedSizeValue, "Selected size");
						}
					}else {
						Log.message("--->>> Given Size(" + size[0] + ") <b>Not Available</b> in Size list. Selecting Random Size...");
						break;
					}
				}
			}
			Log.fail("Something went wrong. Please check the input parameters...", driver);
			return null;
		} else {
			List<WebElement> lstSizeSwatchesSelectable;
			try {
				BrowserActions.scrollInToView(shoeSize, driver);
			} catch (Exception e) {
				Log.message("Shoe size is not available.", driver);
			}
			if(Utils.waitForElement(driver, shoeSize)) {
				lstSizeSwatchesSelectable = driver.findElements(By.cssSelector("ul.shoesize> li"));
				WebElement shoeWidth = driver.findElement(By.cssSelector("ul.shoewidth li.selectable a"));
				BrowserActions.clickOnSwatchElement(shoeWidth, driver, "Shoe Width");
				Utils.waitForPageLoad(driver);
				if(Utils.waitForElement(driver, scrollBar)) {
					if(scrollBar.getCssValue("top").split("px")[0].equalsIgnoreCase("0")) {
						BrowserActions.dragElement(scrollBar, driver, "Quickshop Scroll Bar", Direction.Down);
						Log.message("Scrolled down.", driver);
					}
				}
				lstSizeSwatchesSelectable = driver.findElements(By.cssSelector("ul.shoesize> li"));
				BrowserActions.clickOnSwatchElement(lstSizeSwatchesSelectable.get(0), driver, "Size Swatches ");
				Utils.waitUntilElementDisappear(driver, waitLoader);
				return BrowserActions.getText(driver, txtSelectedShoeSizeValue, "Selected Shoe sizee");
			}else {
				lstSizeSwatchesSelectable = driver.findElements(By.cssSelector("ul.size> li.selectable:not([class*='selected']) >a"));
				BrowserActions.scrollInToView(lstSizeSwatchesSelectable.get(0), driver);
				BrowserActions.clickOnSwatchElement(lstSizeSwatchesSelectable.get(0), driver, "Size Swatches ");
				Utils.waitUntilElementDisappear(driver, waitLoader);
				Log.event("Clicked On First Size...");
				return BrowserActions.getText(driver, txtSelectedSizeValue, "Selected size");
			}
		}
	}

	/**
	 * To scroll the quick shop overlay into personalized message
	 * @throws Exception
	 */
	public void scrollQuickShopOverlayToPersonalizedMessage() throws Exception {
		BrowserActions.scrollInToView(ckbAddPersonalMsg, driver);
	}
}
