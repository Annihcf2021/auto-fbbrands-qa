package com.fbb.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ordering.GuestOrderStatusLandingPage;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class GuestOrderTrack extends LoadableComponent<GuestOrderTrack> {
	private final WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	//***********************************************************
	//  	WebElements
	//***********************************************************
	
	@FindBy(css = ".pt_account")
	WebElement readyElement;
	
	@FindBy(css = ".login-box.login-order-track")
	WebElement divOrderStatusBox;
	
	@FindBy(css = ".login-box.login-order-track h2")
	WebElement lblCheckAnOrder;

	@FindBy(css = ".login-box-content.clearfix.make-label-absolute p")
	WebElement subHeadTxt;

	@FindBy(name = "dwfrm_ordertrack_findorder")
	WebElement btnCheckInOrder;

	@FindBy(id = "dwfrm_ordertrack_orderNumber")
	WebElement txtFldOrderNumber;

	@FindBy(id = "dwfrm_ordertrack_orderEmail")
	WebElement txtFldOrderEmail;

	@FindBy(id = "dwfrm_ordertrack_postalCode")
	WebElement txtFldBillingZipcode;

	@FindBy(css = "button[name='dwfrm_ordertrack_findorder']")
	WebElement checkorderstatusbtn;
	
	@FindBy(css = ".orderdetails")                  
	WebElement lnkguestOrderStatusLandingPage;
	
	@FindBy(css = ".order-number")
	WebElement divOrderNumberPrint;

	@FindBy(css = ".pt_account .create-login .inner-block .login-box .error-form")                  
	WebElement nullOrderNoErrormsg;
	
	@FindBy(css = ".error-form")                  
	WebElement orderNoErrorMsg;

	@FindBy(id = "dwfrm_ordertrack_orderNumber-error")                  
	WebElement orderNoInvalidErrorMsg;

	@FindBy(id = "dwfrm_ordertrack_orderEmail-error")                  
	WebElement orderEmailErrorMsg;

	@FindBy(id = "dwfrm_ordertrack_postalCode-error")                  
	WebElement orderPostalZipCodeErrorMsg;
	
	@FindBy(css = ".form-row.postalCode.required.error-handle .input-focus .error")                  
	WebElement orderInvalidZipCodeErrorMsg;
	
	@FindBy(css = "label[for='dwfrm_ordertrack_orderNumber'] .label-text")
	WebElement placeHolderOrderNumber;

	@FindBy(css = "label[for='dwfrm_ordertrack_orderEmail'] .label-text")
	WebElement placeHolderOrderEmail;

	@FindBy(css = "label[for='dwfrm_ordertrack_postalCode'] .label-text")
	WebElement placeHolderZipCode;

	@FindBy(css = ".error-form.ordererror")
	WebElement orderErrorMsg;

	@FindBy(css = ".pt_order")
	WebElement lnkGuestorderStatusPage;
	
	@FindBy(css = ".mini-address-name")
	WebElement txtMiniAddressName;
	
	@FindBy(css = ".address .default")
	WebElement txtShippingAddressName;
	
	@FindBy(css = ".mail-box")
	WebElement icoEmailBox;
	
	@FindBy(css = ".receipt-message")
	WebElement txtReceiptMessage;
	
	@FindBy(css = ".order-detail-section")
	WebElement divOrderDetails;
	
	@FindBy(css = ".view-more-link")
	WebElement lnkViewDetails;
	
	@FindBy(css = ".view-more-link.opened")
	WebElement lnkViewLess;
	
	@FindBy(css = ".order-shipments")
	WebElement divOrderShipment;
	
	private static final String SHIPMENT = ".order-shipment-table ";
	
	@FindBy(css = SHIPMENT)
	WebElement divShipment;
	
	@FindBy(css = SHIPMENT + ".shipment-address .heading")
	WebElement divShipmentHeading;
	
	@FindBy(css = SHIPMENT + ".shipment-address .address")
	WebElement divShipmentAddress;
	
	@FindBy(css = SHIPMENT + ".order-shipping-status .value")
	WebElement divShipmentStatus;
	
	@FindBy(css = SHIPMENT + ".item-details")
	WebElement divShipmentDetails;
	
	@FindBy(css = SHIPMENT + ".item-details .line-item-quantity .qty-value")
	WebElement divShipmentQuantity;
	
	@FindBy(css = SHIPMENT + ".line-item-quantity.hide-desktop .qty-value")
	WebElement divShipmentQuantityMobile;
	
	@FindBy(css = SHIPMENT + ".hide-mobile .specialmessaging")
	WebElement divShipmentSpecialMessage;
	
	@FindBy(css = SHIPMENT + ".details-review a")
	WebElement lnkWriteReview;
	
	@FindBy(css = SHIPMENT + ".buy-again a")
	WebElement lnkBuyAgain;
	
	@FindBy(css = ".order-detail-summary")
	WebElement divOrderSummery;
	
	@FindBy(css = ".tax-disclaimer")
	WebElement divTaxDisclaimer;
	
	//***********************************************************
	//  	Methods
	//***********************************************************
	
	/**
	 * Constructor for Guest Order Track
	 * 
	 */
	public GuestOrderTrack(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, divOrderStatusBox))) {
			Log.fail("GuestOrderTrck page didn't open up", driver);
		}

		elementLayer = new ElementLayer(driver);
	}
	
	/**
	 * To get page load status
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}
	
	/**
	 * To click on check in order button
	 * @throws Exception - Exception
	 */
	public void clickOnCheckInOrder()throws Exception{
		BrowserActions.clickOnElementX(btnCheckInOrder, driver, "Check button in Check Order");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To clear all check order fields 
	 * @throws Exception - Exception
	 */
	public void clearCheckOrderFields()throws Exception{
		txtFldOrderNumber.clear();
		txtFldOrderNumber.sendKeys(Keys.TAB);
		txtFldOrderEmail.clear();
		txtFldOrderEmail.sendKeys(Keys.TAB);
		txtFldBillingZipcode.clear();
		txtFldBillingZipcode.sendKeys(Keys.TAB);
	}
	
	/**
	 * To fill order fields 
	 * @param orderNumber - String
	 * @param orderMail - String
	 * @param orderZip - String
	 * @throws Exception - Exception
	 */
	public void fillOrderFields(String orderNumber, String orderMail, String orderZip)throws Exception{
		BrowserActions.typeOnTextField(txtFldOrderNumber, orderNumber, driver, "Order Number");
		BrowserActions.typeOnTextField(txtFldOrderEmail, orderMail, driver, "Order Mail");
		BrowserActions.typeOnTextField(txtFldBillingZipcode, orderZip, driver, "Order Zipcode");
	}
	
	/**
	 * To navigate to guest order status page
	 * @param String - orderNumber
	 * @param String - orderEmail 
	 * @param String - billingZipCode
	 * @return GuestOrderStatusLandingPage - Page object
	 * @throws Exception - Exception
	 */
	public GuestOrderStatusLandingPage navigateToGuestOrderStatusPage(String orderNumber,String orderEmail,String billingZipCode)throws Exception{
		fillOrderFields(orderNumber, orderEmail, billingZipCode);
		BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Guest order status  page Link");
		
		Utils.waitForPageLoad(driver);
		return new GuestOrderStatusLandingPage(driver).get();
	}
	
	/**
	 * To verify order email is mandatory
	 * @param String - orderNumber
	 * @param String - billingZipCode
	 * @return boolean - true/false if email is mandatory
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderEmailISMandatory(String orderNumber,String billingZipCode) throws Exception{
		{
			txtFldOrderNumber.clear();
			BrowserActions.typeOnTextField(txtFldOrderNumber,orderNumber, driver, "order Number Field");
			txtFldOrderEmail.clear();
			txtFldOrderEmail.sendKeys("");
			txtFldBillingZipcode.clear();
			BrowserActions.typeOnTextField(txtFldBillingZipcode,billingZipCode, driver, "order Billing zip code Field");
			String urlBeforeClick= driver.getCurrentUrl();
			BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Clicked on submit");
			String urlAfterClick= driver.getCurrentUrl();
			if(urlBeforeClick.equals(urlAfterClick)){
					return true;
			}
			else
				return false;
		}
	}
	
	/**
	 * To verify all 3 fields are mandatory in check an order section
	 * @param String - orderNumber
	 * @param String - orderEmail
	 * @return boolean - true/false if ZIP is mandatory 
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderPostalZipcCodeMandatory(String orderNumber,String orderEmail) throws Exception
	{
		txtFldOrderNumber.clear();
		BrowserActions.typeOnTextField(txtFldOrderNumber,orderNumber, driver, "order Number Field");
		txtFldOrderEmail.clear();
		BrowserActions.typeOnTextField(txtFldOrderEmail,orderEmail, driver, "order Email Field");
		txtFldBillingZipcode.clear();
		txtFldBillingZipcode.sendKeys("");
		String urlBeforeClick= driver.getCurrentUrl();
		BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Clicked on submit");
		String urlAfterClick= driver.getCurrentUrl();
		if(urlAfterClick.equals(urlBeforeClick)) {
			return true;
		}
		if(orderPostalZipCodeErrorMsg.getText().equals("0 orders found or please confirm ZipCode")){
				return true;
		}
	return false;
	}
	
	/**
	 * To verify order number is mandatory in Check an order section
	 * @param String - orderEmail
	 * @param String - billingZipCode
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderNoISMandatory(String orderEmail,String billingZipCode) throws Exception{
		{
			txtFldOrderNumber.clear();
			txtFldOrderNumber.sendKeys("");
			txtFldOrderEmail.clear();
			BrowserActions.typeOnTextField(txtFldOrderEmail,orderEmail, driver, "order Email Field");
			txtFldBillingZipcode.clear();
			BrowserActions.typeOnTextField(txtFldBillingZipcode,billingZipCode, driver, "order zip billing code Field");
			String urlBeforeClick= driver.getCurrentUrl();
			BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Clicked on submit");
			String urlAfterClick= driver.getCurrentUrl();
			
			if(urlAfterClick.equals(urlBeforeClick)) {
				return true;
			}
			
			if(orderNoErrorMsg.getText().equals("0 orders found or please confirm Order Number"))
			{
					return true;
			}
		return false;
		}
	}
	
	/**
	 * To validate order number mismatch
	 * @param String - invalidOrderNumber
	 * @param String - orderEmail
	 * @param String - billingZipCode
	 * @return boolean - Status
	 * @throws Exception - Exception
	 */
	public boolean orderNumberMissmatch(String invalidOrderNumber,String orderEmail,String billingZipCode) throws Exception{

		BrowserActions.typeOnTextField(txtFldOrderNumber,invalidOrderNumber, driver, "order Number Field");
		BrowserActions.typeOnTextField(txtFldOrderEmail,orderEmail, driver, "order Email Field");
		BrowserActions.typeOnTextField(txtFldBillingZipcode,billingZipCode, driver, "order Billing zip code Field");
		BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Guest order status  page Link");
		String urlBeforeClick= driver.getCurrentUrl();
		Utils.waitForElement(driver, orderNoErrorMsg);
		String urlAfterClick= driver.getCurrentUrl();
		if(orderNoErrorMsg.getText().contains("We’re having trouble finding your order.")){
			return true;
		}
		if(urlAfterClick.equals(urlBeforeClick)) {
			return true;
		}
		return false;
	}

	/**
	 * To validate the order email mismatch
	 * @param String - orderNumber
	 * @param String - invalidOrderEmail
	 * @param String - billingZipCode
	 * @return boolean - Status
	 * @throws Exception - Exception
	 */
	public boolean orderEmailMissmatch(String orderNumber,String invalidOrderEmail,String billingZipCode) throws Exception{
		BrowserActions.typeOnTextField(txtFldOrderNumber,orderNumber, driver, "order Number Field");
		BrowserActions.typeOnTextField(txtFldOrderEmail,invalidOrderEmail, driver, "order Email Field");
		BrowserActions.typeOnTextField(txtFldBillingZipcode,billingZipCode, driver, "order Billing zip code Field");
		BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Guest order status  page Link");
		Utils.waitForElement(driver, orderEmailErrorMsg);
		if(orderEmailErrorMsg.getText().contains("Please enter a valid email.") || orderEmailErrorMsg.getAttribute("class").equals("error"))
		{
			return true;
		}
		return false;

	}

	/**
	 * To check the order billing mismatch
	 * @param String - orderNumber
	 * @param String - orderEmail
	 * @param String  - invalidBillingZipCode
	 * @return boolean - Status
	 * @throws Exception - Exception
	 */
	public boolean orderBillingZipCodeMissmatch(String orderNumber,String orderEmail,String invalidBillingZipCode) throws Exception{
		txtFldOrderNumber.clear();
	    BrowserActions.typeOnTextField(txtFldOrderNumber,orderNumber, driver, "order Number Field");
		txtFldOrderEmail.clear();
		BrowserActions.typeOnTextField(txtFldOrderEmail,orderEmail, driver, "order Email Field");
		txtFldBillingZipcode.clear();
		BrowserActions.typeOnTextField(txtFldBillingZipcode,invalidBillingZipCode, driver, "order Billing zip code Field");
		BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Guest order status  page Link");
		Utils.waitForElement(driver, orderInvalidZipCodeErrorMsg);
		if(orderInvalidZipCodeErrorMsg.getText().equalsIgnoreCase("Invalid Zip"))
		{
			return true;
		}
		else
			return false;
	}

}
