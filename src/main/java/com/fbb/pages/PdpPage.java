package com.fbb.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.Enumerations.Direction;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.SystemProperties;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;

public class PdpPage extends LoadableComponent<PdpPage> {

    private WebDriver driver;
    private boolean isPageLoaded;
    public HomePage homePage;
    public ElementLayer elementLayer;
    String runBrowser;

    /**********************************************************************************************
     ********************************* WebElements of PDP Page ***********************************
     **********************************************************************************************/

    private static EnvironmentPropertiesReader demandWareProperty = EnvironmentPropertiesReader.getInstance("demandware");
    
    private static final String SIZE_SELECTED = "ul[class*='size']:not(.sizefamily) .selected a";

    private static final String Recently_Viewed_Section = ".product-listing.last-visited";

    private static final String Product_Set_Details_Section = ".main-product-set-details ";

    private static final String Product_Set_Details_Section_Tablet_Mobile = ".product-col-1-productdetail .hide-desktop.hide-tablet ";

    private static final String RecommendationDesktop = ".you-may-like.recommendation.hide-mobile.hide-tablet ";

    private static final String RecommendationMobileTablet = ".you-may-like.recommendation.hide-desktop ";
    
    private static final String Product_Review_Teaser = "#product-teasers ";
    
    private static final String RecommendationDesktop_BH = ".you-may-like.bh-recommendations ";
    
    @FindBy(id = "pdpMain")
    WebElement pdpMain;
    
    @FindBy(css = ".pt_product-search-result.product-list-page")
    WebElement plpReadyElement;
    
    @FindBy(css = ".prod-detail-page")
    WebElement pagePrdDetails;

    @FindBy(css = "#pdpMain .info .product-detail")
    WebElement sectionPrdDetails;

    @FindBy(css = ".loader-bg")
    WebElement waitLoader;

    @FindBy(css = ".minicartpopup.cart-overlay")
    WebElement flytMiniCart;
    
    @FindBy(css = ".details-mainframeid")
	WebElement divItemId;

    @FindBy(css = ".pt_product-details")
    WebElement cntPdpContent;

    @FindBy(css = ".cart-overlay .ui-icon-closethick")
    WebElement btnCloseAddToBagOverlay;
    
    @FindBy(css = ".hide-mobile .enlarge-video.video-link")
    WebElement lnkVideoIcon_Des_Tab;
    
    @FindBy(css = ".hide-tablet.hide-desktop .enlarge-video.video-link")
    WebElement lnkVideoIcon_Mob;
    
    @FindBy(css = "#lc_player_box")
    WebElement videoBoxPlayer;
    
    @FindBy(css = ".loader[style*='block']")
    WebElement PDPspinner;

    @FindBy(css = ".swatches.size")
    WebElement sizeSwatches;
    
    @FindBy(css = ".TTGalleryContent img")
    List<WebElement> lstImgCustGal;
    
    @FindBy(css = "#TT-vc-modal-container")
    WebElement custGalleryContainer;
    
    @FindBy(css = "#TT-vc-modal-container .TT-close-icon")
    WebElement custGalleryClose;
    
    @FindBy(css = ".ratings")
    WebElement productRatings;

    @FindBy(css = "ul[class*='size']> li.selectable>a")
    List<WebElement> lstSizeSwatchesSelectable;
    
    @FindBy(css = ".swatches.shoesize li.selectable a")
    List<WebElement> lstShoeSizeSwatchesSelectable;

    @FindBy(css = ".swatches.brabandsize li.selectable a")
    List<WebElement> lstBraSizeSwatchesSelectable;
    
    @FindBy(css = ".swatches.bracupsize li.selectable a")
    List<WebElement> lstBraCupSizeSwatchesSelectable;
    
    @FindBy(css = ".braBandSize")
    WebElement sectionBraBrandSize;
    
    @FindBy(css = ".braCupSize")
    WebElement sectionBraCupSize;

    @FindBy(css = ".swatches.shoesize")
    WebElement shoeSizeSwatches;
    
    @FindBy(css = ".swatches.shoesize li")
    List<WebElement> lstShoeSizeSwatches;
    
    @FindBy(css = ".product-variation-content .price-sales:not(.price-standard-exist)")
    WebElement txtSinglePriceAlone;
    
    @FindBy(css = ".product-variation-content .price-sales.sdfs")
    WebElement txtMultiPriceSalesRed;
    
    @FindBy(css = ".product-variation-content .price-sales.price-standard-exist:not(.sdfs)")
    WebElement txtSinlgePriceSalesRed;
    
    @FindBy(css = ".product-variation-content .price-standard")
    WebElement txtSingleMultiStrikedOutPrice;
    
    @FindBy(css = ".product-variation-content .product-price span")
    WebElement txtMultiPriceAlone;

    @FindBy(css = ".swatches.size li.unselectable a")
    List<WebElement> lstSizeSwatchesUnSelectable;

    @FindBy(css = ".swatches.size li.unselectable a")
    WebElement SizeSwatchesUnSelectable;

    @FindBy(css = ".swatches.sizefamily li.selectable.selected")
    WebElement SizeFamilySelectable;

    @FindBy(css = ".swatches.shoesize li.unselectable a")
    List<WebElement> lstShoeSizeSwatchesUnSelectable;

    @FindBy(css = ".swatches.brabandsize li.unselectable a")
    List<WebElement> lstBraSizeSwatchesUnSelectable;
   
    @FindBy(css = ".swatches.bracupsize li.unselectable a")
    List<WebElement> lstBraCupSizeSwatchesUnselectable;    
    
    @FindBy(css = ".swatches.size li a")
    List<WebElement> lstSizeSwatches;
    
    @FindBy(css = ".swatches.size li:not(.unselectable) a")
    List<WebElement> lstInStockSizeSwatches;
    
    @FindBy(css = ".swatches.brabandsize li a")
    List<WebElement> lstBraSizeSwatches;
    
    @FindBy(css = ".swatches.bracupsize li a")
    List<WebElement> lstBraCupSizeSwatches;
    
    @FindBy(css = ".swatches.size li>a")
    WebElement SizeSwatches;
    
    @FindBy(css = "#current_color .label")
    WebElement colorLabel;
    
    @FindBy(css = ".product-special-messages")
	WebElement txtProdFeatureMsg;

    @FindBy(css = ".swatches.sizefamily .selectable .swatchanchor")
    List<WebElement> SizeFamSelectable;
    
    @FindBy(css = "#main .thumb.background-thumb .thumbnail-link img")
    List<WebElement> alternateImages;
    
    @FindBy(css = "#main .thumb.background-thumb .thumbnail-link img")
    WebElement alternateImagesProductSet;
    
    @FindBy(css = ".you-may-like.special-recommendation .recommendations")
    WebElement recommendationSectioonPdpProductSet;
    
    @FindBy(css = ".swatches.sizefamily .selected")
    WebElement SizeFamSelected;
    
    @FindBy(css = ".sizeFamily .selected-value")
    WebElement SizeFamSelectedVal;

    @FindBy(css = SIZE_SELECTED)
    WebElement selectedSize;
    
    @FindBy(css = ".product-set-details .swatches.size li.selectable.selected")
    WebElement sizeSwatchesSelected;

    @FindBy(css = ".swatches.shoesize .selectable.selected")
    WebElement selectedshoeSize;
    
    @FindBy(css = ".product-image.main-image.hide-mobile img")
    WebElement prodImage_Desk_Tab;
    
    @FindBy(css = ".thumb.background-thumb.selected img")
    WebElement prodImage_Mobile;

    @FindBy(css = ".thumb.background-thumb.selected img")
    WebElement prodSelectedAltImageDeskAndTab;
    
    @FindBy(css = ".slick-current.slick-active .background-thumb a img")
    WebElement prodSelectedAltImageMob;
    
    @FindBy(css = "li.shoeSize .selected-value")
    WebElement selectedShoeSizeValue;

    @FindBy(css = "li.braBandSize .selectable.selected a")
    WebElement selectedBraSize;

    @FindBy(css = "li.braBandSize .selected-value")
    WebElement selectedBraSizeValue;

    @FindBy(css = ".swatches.width .selected")
    WebElement selectedWidth;

    @FindBy(xpath = "//select[@id='Quantity']//following-sibling::div[@class='selected-option selected']")
    WebElement selectedQty;

    @FindBy(css = "select[id='Quantity']")
    WebElement drpQty;

    @FindBy(css = ".inventory .selected-option.selected")
    WebElement divQty;

    @FindBy(css = ".TTGallery img")
    List<WebElement> customerGalleryImages;

    @FindBy(css = "#TT-vc-modal-container")
    WebElement customerGalleryModal;

    @FindBy(css = ".inventory")
    WebElement divQtySection;

    @FindBy(css = "button[value='ADD TO BAG']")
    WebElement btnAddToCartSplProdset;

    @FindBy(css = ".quantity div[class='selected-option selected']")
    WebElement selectedQty1;

    @FindBy(css = ".product-video-container")
    WebElement prdVideoContainer;

    @FindBy(css = "#Quantity > option")
    List<WebElement> lstQty;
    
    @FindBy(css = "#main .swatches.color li")
    List<WebElement> lstAllColor;

    @FindBy(css = ".attribute_label .selected-value")
    List<WebElement> lstSelectedSizeOrColor;

    @FindBy(css = ".quantity .selection-list li:not([class='selected'])")
    List<WebElement> lstQtySelectable;

    @FindBy(css = "#main .swatches.color li:not([class*='selected']):not([class*='unselectable']) a img")
    List<WebElement> lstColorSelectable;
    
    @FindBy(css = "#main .swatches.color li:not([class*='selected']):not([class*='unselectable']) a")
    List<WebElement> lstLinkColorSelectable;
    
    @FindBy(css = "#main .swatches.color li:not([class*='selected']) img:not([src*='NO-SWATCH-TAB.svg'])")
    List<WebElement> imgLstColorConfiguredSelectable;

    @FindBy(css = "#main .swatches.color li a img:not([src*='NO-SWATCH-TAB.svg'])")
    List<WebElement> lstColorSwatchConfigured;
    
    @FindBy(css = "#main ul.swatches.color li:not([class*='selected']) a img:not([src*='NO-SWATCH-TAB.svg'])")
    List<WebElement> lstColorConfiguredSelectable;
    
    @FindBy(css = "#main ul.swatches.color li:not([class*='selected']):not([class*='unselectable']) a img:not([src*='NO-SWATCH-TAB.svg'])")
    List<WebElement> lstColorConfiguredSelectableInStock;

    @FindBy(css = ".swatches.color li:not([class*='selected']) a")
    WebElement colorSelectable;
    
    @FindBy(css = ".swatches.size li:not([class*='selected']) a")
    WebElement sizeSelectable;     
        
    @FindBy(css = "#main .swatches.color li.unselectable a")
    List<WebElement> lstColorUnSelectable;

    @FindBy(css = ".swatches.sizefamily li a")
    List<WebElement> lstSizeFamily;
    
    @FindBy(css = ".swatches.sizefamily li:not([class*='selected']) a")
    List<WebElement> lstSizeFamilySelectable;

    @FindBy(css = ".sizeFamily .selected-value")
    WebElement SizeFamSelectedValue;

    @FindBy(css = ".product-set-product-image")
    List<WebElement> lstProdSetIndvProdImages;

    @FindBy(css = ".product-set-product-image")
    WebElement ProdSetIndvProdImages;

    @FindBy(css = ".attribute.color .selected-value")
    WebElement selectedColorVariant;
    
    @FindBy(css = ".swatches.color li.selected a")
    WebElement selectedColor;

    @FindBy(css = "li.attribute.sizeFamily .selected")
    WebElement selectedSizeFamily;

    @FindBy(css = "li.attribute.sizeFamily .selected")
    WebElement selectedSizeFamily_KS;

    @FindBy(css = ".attribute.size .selected-value")
    List<WebElement> selectedSizeValue;

    @FindBy(css = ".attribute.size .selected-value")
    WebElement selectedSizeLabel;
    
    @FindBy(xpath = "//div[@class='attribute_label'] //h3[@class='label'] [contains(text(),'Size:')]")
    WebElement selectedSizeLabel1;
    
    @FindBy(css = ".attribute.size .label")
    WebElement lblSize;
    
    @FindBy(css = ".attribute.shoeSize .label")
    WebElement lblShoeSize;
    
    @FindBy(css = ".minicart-quantity.total-qty")
    WebElement lblQtyAdded;

    @FindBy(css = ".attribute.sizeFamily")
    WebElement divSizeFamily;

    @FindBy(css = ".attribute.sizeFamily .selected-value")
    List<WebElement> selectedSizefamily;

    @FindBy(css = ".overlay-product-slick .slick-active")
    WebElement overlayProductInfo;

    @FindBy(css = ".product-thumbnails .slick-active img")
    WebElement mainProdImageActive_Mobile;

    @FindBy(css = ".mini-cart-products .slick-next.slick-arrow")
    WebElement nextArrowOverlay;

    @FindBy(css = ".product-col-1-productdetail .product-shortdescription")
    WebElement lblShortDescPrdSet_Tablet_Mobile;

    @FindBy(css = ".character-count")
    WebElement txtPersonalMsgCounter;

    @FindBy(css = "#dwfrm_product_giftcard_purchase_message")
    WebElement txtAreaPersonalMsg;

    @FindBy(xpath = "//textarea[@id='dwfrm_product_giftcard_purchase_message']//ancestor::div[contains(@class,'form-row')]")
    WebElement txtAreaPersonalMsgError;

    @FindBy(css = "#dwfrm_product_giftcard_purchase_recipient")
    WebElement txtGCPersonalMsgTo;
    
    @FindBy(css = ".personal-message .addto-message")
    WebElement labelGCPersonalizedMessage;

    @FindBy(xpath = "//input[@id='dwfrm_product_giftcard_purchase_recipient']//ancestor::div[contains(@class,'form-row')]")
    WebElement txtGCPersonalMsgToError;

    @FindBy(css = "#dwfrm_product_giftcard_purchase_from")
    WebElement txtGCPersonalMsgFrom;

    @FindBy(xpath = "//input[@id='dwfrm_product_giftcard_purchase_from']//ancestor::div[contains(@class,'form-row')]")
    WebElement txtGCPersonalMsgFromError;

    @FindBy(css = ".LiveclickerVideo")
    WebElement videoPlayer;

    @FindBy(css = ".centeredActionLink.playVideo.show")
    WebElement btnPlayVideo;

    @FindBy(css = ".thumbnail-link")
    List<WebElement> lstProductThumbnaillink;

    @FindBy(css = ".thumbnail-link")
    WebElement productThumbnaillink;

    @FindBy(css = ".slick-current  .thumbnail-link img")
    WebElement productThumbnailSelected;
    
    @FindBy(css = ".pdp-max-width .product-name")
    WebElement productName;
    
    @FindBy(css = ".thumbnail-link img")
    List<WebElement> lstProductAltThumbnail;
    
    @FindBy(css = ".product-image-container .slick-list.draggable .slick-slide")
    List<WebElement> lstAlternateImgs;
    
    @FindBy(css = ".recipient.error")
    WebElement errToNotFill;
    
    @FindBy(css = ".from.error")
    WebElement errFromNotFill;
    
    @FindBy(css = ".message.error")
    WebElement errMessageNotFill;
    
    @FindBy(css = ".confirmemail.error")
    WebElement errCnfEmailNotFill;
    
    @FindBy(css = ".email.error")
    WebElement errEmailNotFill;

    @FindBy(css = Recently_Viewed_Section)
    WebElement recentlyViewedSection;
    
    @FindBy(css = ".last-visited-section")
    WebElement lastVisitedSection;
    
    @FindBy(css=Recently_Viewed_Section + ".slick-list.draggable .grid-tile")
    List<WebElement> recentlyViewedproductTiles;

    @FindBy(css = Recently_Viewed_Section + " .p-image")
    List<WebElement> recentlyViewedProdImage;

    @FindBy(css = Recently_Viewed_Section + " .product-name")
    List<WebElement> recentlyViewedProdName;

    @FindBy(css = ".last-visited .slick-list.draggable .slick-active")
    List<WebElement> recentlyViewedCurrentProd;

    @FindBy(css = Recently_Viewed_Section + " .grid-tile.slick-slide.slick-active")
    List<WebElement> recentlyViewedActiveProd;

    @FindBy(css = Recently_Viewed_Section + " .product-pricing")
    List<WebElement> recentlyViewedProdPricing;
    
    @FindBy(css = Recently_Viewed_Section + ".product-standard-price")
    List<WebElement> recentlyViewedStdPrice;
    
    @FindBy(css = Recently_Viewed_Section + ".product-standard-price")
    WebElement recentlyViewed1stStdPrice;
    
    @FindBy(css = Recently_Viewed_Section + ".product-sales-price")
    List<WebElement> recentlyViewedSalePrice;       

    @FindBy(css = Recently_Viewed_Section + ".product-sales-price")
    WebElement recentlyViewed1stSalePrice;       

    @FindBy(css = Recently_Viewed_Section + " .slick-prev.slick-arrow")
    WebElement recentlyViewSectionPrevArrow;

    @FindBy(css = Recently_Viewed_Section + " .slick-next.slick-arrow")
    WebElement recentlyViewSectionNextArrow;

    @FindBy(css = ".product-info")
    WebElement detailsReviewSection;
    
    @FindBy(css = "#thumbnails.slick-dotted")
    WebElement divSlickWindowMobile;

    @FindBy(css = "#thumbnails .slick-track>div")
    List<WebElement> lstProductThumbnail;
    
    @FindBy(css = ".prod-detail-page .info #product-content img")
    WebElement giftCardDesign;

    @FindBy(css = ".product-image.main-image img")
    WebElement mainProdImage;

    @FindBy(css = ".product-thumbnails img")
    WebElement mainProdImage_Mobile;

    @FindBy(css = ".enlarge-video-sec.hide-mobile .enlarge-video.video-link")
    WebElement lnkVideoMain;

    @FindBy(css = ".mobile-enlarge-video-sec.hide-desktop.hide-tablet .enlarge-video.video-link")
    WebElement lnkVideoMain_Mobile;

    @FindBy(css = ".product-set-details .product-price")
    List<WebElement> lstProductSetItemPrices;

    @FindBy(css = ".price-sales.price-standard-exist")
    List<WebElement> lstProductSetPrices;
    
    @FindBy(css = ".product-set-item.hemmingmonograming.selected")
    WebElement firstProductInProductSet;

    @FindBy(css = ".product-set-item.hemmingmonograming.selected")
    List<WebElement> lstOfHemmSplProductSetProds;

    @FindBy(css = ".product-set-item")
    List<WebElement> lstSplProductSetProds;

    @FindBy(css = "#main .breadcrumb")
    WebElement prodBreadCrumb;

    @FindBy(css = ".zoomLens")
    WebElement zoomLensProdImg;

    @FindBy(css = ".zoomWindow")
    WebElement zoomedProdImg;

    @FindBy(css = ".product-set-item")
    WebElement prodContentSplProductSet;

    @FindBy(css = ".zoomWindowContainer div")
    WebElement zoomedProdImgOutside;

    @FindBy(css = ".product-primary-image .b_product_badge img")
    WebElement prodBadge;
    
    @FindBy(css = ".b_product_badge.hide-desktop.hide-tablet img")
    WebElement prodBadgeMobile;

    @FindBy(css = ".slick-list.draggable")
    WebElement alternateImagesList;

    @FindBy(css = "#thumbnails .slick-prev.slick-arrow.slick-disabled")
    WebElement btnPrevImageDisable;

    @FindBy(css = "#thumbnails .slick-prev.slick-arrow")
    WebElement btnPrevImageEnable;

    @FindBy(css = "#thumbnails .slick-next.slick-arrow")
    WebElement btnNextImageEnable;

    @FindBy(css = "#thumbnails .slick-next.slick-arrow.slick-disabled")
    WebElement btnNextImageDisable;

    @FindBy(css = ".product-special-messages")
    WebElement lblProductSpecialMessage;

    @FindBy(css = ".terms-conditions-link")
    WebElement lnkTermsAndConditions;

    @FindBy(css = ".ui-dialog.terms-and-conditions")
    WebElement toolTipOverLay;

    @FindBy(css = ".product-hemming-tips")
    WebElement hemmingToolTiplnk;

    @FindBy(css = ".ui-button.ui-dialog-titlebar-close")
    WebElement closeToolTipOverLay;
    
    @FindBy(css = " .ui-dialog.ui-corner-all.ui-widget")
    WebElement hemmingToolTipOverlay;

    @FindBy(css = ".attribute.variant-dropdown")
    WebElement drpGiftCardSize;
    
    @FindBy(css = ".attribute.variant-dropdown .selected-option.selected")
    WebElement drpGiftCardSizeValue;

    @FindBy(css = ".variation-select")
    WebElement drpGiftCardSizeSelect;
    
    @FindBy(css = ".product-variation-content")
    WebElement drpGiftCardAmountSelected;

    @FindBy(css = ".gift-card .selection-list li")
    List<WebElement> drpGiftCardSizeOptions;

    @FindBy(css = ".gift-card .selection-list li:not([class*='selected'])")
    List<WebElement> drpGiftCardSizeOptionSelectable;

    @FindBy(css = ".value.custom-select.current_item")
    WebElement drpGiftCardSizeOptionContainer;

    @FindBy(css = ".product-set-item .product-name")
    List<WebElement> lblProductSetInvProdName;

    @FindBy(css = ".product-set-item")
    List<WebElement> lblSplProductSetComponent;

    @FindBy(css = ".product-set-item .product-set-details")
    List<WebElement> lblProductSetInvProdDetail;

    @FindBy(css = ".product-set-item .product-set-details")
    WebElement productSetDetails;

    @FindBy(css = ".product-set-details .product-name")
    WebElement productSetproductName;

    @FindBy(css = ".product-variations")
    WebElement productSetProductvariation;

    @FindBy(css = ".product-set-item .product-add-to-cart.add-sub-product")
    List<WebElement> lblProductSetInvProdQuantity;

    @FindBy(css = "#dwfrm_product_giftcard_purchase_personalizedMessage")
    WebElement chkGCPersonalizedMessage;

    @FindBy(css = ".fields-address.make-label-absolute")
    WebElement personalizedMethodUchecked;

    @FindBy(css = ".please-select")
    WebElement errMsgGCAmount;

    @FindBy(css = ".quantity-ats-message")
    WebElement errMsgQuantityATS;

    @FindBy(css = ".continue-shopping")
    WebElement btnContinueShoppingInMCOverlay;

    @FindBy(xpath = "//div[contains(@class,'TTreviewDimsSingleSelectSummary')]//div[contains(text(),'Pros')]")
    WebElement sectionProsInReview;

    @FindBy(xpath = "//div[contains(@class,'TTreviewDimsSingleSelectSummary')]//div[contains(text(),'Cons')]")
    WebElement sectionConsInReview;

    @FindBy(xpath = "//label[contains(text(),'The Details')]")
    WebElement sectionDetailsTab;

    @FindBy(css = ".tab-label.tab-header:not(.current)")
    WebElement sectionReviewTab;

    @FindBy(css = "#TTwriteReviewBtn")
    WebElement btnWriteAReview;
    
    @FindBy(css = ".TTwriteReview a")
    WebElement lnkWriteReview;
    
    @FindBy(css = "#TTwriteReviewBtn-portrait")
    WebElement btnWriteAReview_mobile;
    
    @FindBy(css = ".TTwriteRevRow #TTcustDimLbl-1 ")
    WebElement txtFitInReviewmodal;

    @FindBy(css = ".TTcustDimRngCont")
    List<WebElement> radioFitWriteAReview;
    
    @FindBy(css = ".TTcustDimRngCont")
    WebElement radioFitInWriteAReview;

    @FindBy(css = "#TT2myNetworkSection #TTwriteReviewScreen")
    WebElement sectionWriteAReviewScreen;
    
    @FindBy(css = "#TurnToMobileContent")
    WebElement sectionWriteAReviewScreenMobile;

    @FindBy(css = ".TTratingBreakdownBox")
    WebElement sectionReviewChart;

    @FindBy(css = ".TTwriteRevRow #TTreviewTitle")
    WebElement inpWriteReviewTitle;
    
    @FindBy(css = "textarea[name='title']")
    WebElement inpWriteReviewTitleMobile;
    
    @FindBy(css = ".TTwriteRevRow #TTreviewText")
    WebElement inpWriteReviewDesc;
    
    @FindBy(css = "textarea[name='text']")
    WebElement inpWriteReviewDescMobile;

    @FindBy(css = "#TTwriteRevGuideLn")
    WebElement lnkReadReviewGuideLine;
    
    @FindBy(css = "#TTReviewGuide")
    WebElement divReviewGuideLine;

    @FindBy(css = "#TTSubWindowClose")
    WebElement btnCloseGuideLine;
    
    @FindBy(css = ".TTmobileHideWriteReviewGuideLines a")
    WebElement lnkCloseGuideLineMobile;

    @FindBy(css = "#header")
    WebElement divHeaderBanner;

    @FindBy(css = "#TTsubmitReview")
    WebElement btnSubmitReview;
    
    @FindBy(css = "#TTrateItSubmitBtn")
    WebElement btnSubmitReviewMobile;
    
    @FindBy(css = "#TTSubScreen")
    WebElement divEmailConfirmationModal;
    
    @FindBy(css = "#TTgenSubWinClose")
    WebElement continueShoppingEmailConfirmationModal;

    @FindBy(css = "#TTwriteRevForm input.TThiddenForADA")
    List<WebElement> inpReviewStar;
    
    @FindBy(css = "#TToverallRatingStars .TTvc-star-lg-empty")
    List<WebElement> inpReviewStarMobile;

    @FindBy(css = "#TTcity")
    WebElement txtLocation;

    @FindBy(css = ".TTwriteRevRightCol .TTratingBox.TTrating-0-0")
    WebElement reviewRatingStar;
    
    @FindBy(css = "#TToverallRatingStars")
    WebElement reviewRatingStarMobile;
    
    @FindBy(css = "#TTtraSubWindow")
    WebElement modalFindUrProfile;
    
    @FindBy(css = ".TTgmailDelAuth.TTdelAuth")
    WebElement lnkEmailFindUrProfile;
    
    @FindBy(css = ".M8HEDc.ibdqA.cd29Sd.bxPAYd.W7Aapd.znIWoc")
    WebElement lnkEmailGooglePopUp;
    
    @FindBy(css = "input[type='email']")
    WebElement fieldEmailGooglePopUp;
    
    @FindBy(css = ".qhFLie .ZFr60d.CeoRYc")
    WebElement nextGooglePopUp;
    
    @FindBy(css = "input[type='password']")
    WebElement fieldPasswordGooglePopUp;

    @FindBy(css = "#TTregEmail")
    WebElement txtFindUrProfileEmail;

    @FindBy(css = "#TTregFirstName")
    WebElement txtFindUrProfileFirstName;

    @FindBy(css = "#TTregLastName")
    WebElement txtFindUrProfileLastName;

    @FindBy(css = "#TTregSubmit")
    WebElement btnSubmitProfile;

    @FindBy(xpath = "//p[contains(text(),' Thanks for your submission')]")
    WebElement txtSubmittedReview;

    @FindBy(css = "#TTSubWindowClose")
    WebElement btnCloseProfile;

    @FindBy(css = "#TTcustDim-4 input")
    List<WebElement> radioWidth;

    @FindBy(css = ".TTMultiSelectDimensionTable .TTMultiSelectDimensionColumnLabel input")
    List<WebElement> checkBoxProsAndCons;
    
    @FindBy(css = ".TTmdCheckbox input")
    List<WebElement> checkBoxProsAndConsMobile;
    
    @FindBy(css = "input[name='TTcustDim-2']")
    List<WebElement> checkBoxPros;
    
    @FindBy(css = "input[name='TTcustDim-2']")
    WebElement checkBoxPro;
    
    @FindBy(css = "#TTcustDim-8 .TTMultiSelectDimensionColumnLabel input")
    List<WebElement> checkBoxProsBH;
    
    @FindBy(css = "input[name='TTcustDim-3']")
    List<WebElement> checkBoxCons;
    
    @FindBy(css = "input[name='TTcustDim-3']")
    WebElement checkBoxCon;
    
    @FindBy(css = "#TTcustDim-9 .TTMultiSelectDimensionColumnLabel input")
    List<WebElement> checkBoxConsBH;
    
    @FindBy(css = ".TTreviewSummary")
    WebElement sectionReviewSummary;
    
    @FindBy(css = "#TurnToGalleryContent .TTGallery ")
    WebElement customerGallerysection;    

    @FindBy(css = ".TTGalleryTitleText")
    WebElement txtCustomerGalleryHeading;

    @FindBy(css = "div.TTreviewCount")
    WebElement txtReviewCount;
    
    @FindBy(css = ".product-teasers a#readReviews") 
    WebElement txtReviewPdp;

    @FindBy(css = "#TT3rShowMore .TT3ShowMoreText")
    WebElement lnkShowMoreReview;

    @FindBy(css = "div.TTreview:not([style='display:none'])")
    List<WebElement> displayingReviewSection;

    @FindBy(css = "#TT3RightLinks")
    WebElement lnkReviewRight;
    
    @FindBy(css = "#TT3settingsLink")
    WebElement lnkMySettings;
    
    @FindBy(css = "#TT2MainSection .nameRF b")
    WebElement textBasicInformation;
    
    @FindBy(css = "#TT3firstName")
    WebElement firstNameMySettings;
    
    @FindBy(css = "#TT3lastName")
    WebElement lstNameMySettings;
    
    @FindBy(css = "#TT4userSettingErrors")
    WebElement textErrorMySettings;
    
    @FindBy(css = "input[name='newEmail0']")
    WebElement fieldAddMail;
    
    @FindBy(css = "#contactInfo .prop a img")
    WebElement closeAddedMail;
    
    @FindBy(css = "#showPhotoEditorLink2")
    WebElement addPhotoMySettings;
    
    @FindBy(css = "#showPhotoEditorLink2")
    WebElement chooseFileMySettings;
    
    @FindBy(css = "#pwd_change")
    WebElement changePasswordMySettings;
    
    @FindBy(css = "#TT3currpasswd")
    WebElement currentPasswordMySettings;
    
    @FindBy(css = "input[name='TT3passwd']")
    WebElement newPasswordMySettings;
    
    @FindBy(css = "#TT3repasswd")
    WebElement conifrmPasswordMySettings;
    
    @FindBy(css = "#TT3updateButton")
    WebElement updateMySettings;  

    @FindBy(css = "a#TT3MyQALink")
    WebElement lnkReviewMyPost;
    
    @FindBy(css = ".TTdialog.TTtra-ui-dialog-content")
    WebElement modalReviewMyPost;
    
    @FindBy(css = "#TT4UPPostsHeader")
    WebElement textYourPost;
    
    @FindBy(css = "#TT4poweredByTT")
    WebElement textPoweredByTurnTo;
    
    @FindBy(css = "#TT4UPBreakdown")
    WebElement sectionStatistics;
    
    @FindBy(css = ".TT4UPType")
    List<WebElement> textStatistics;
    
    @FindBy(css = ".TT4UPType:not(.TT4metaGray)")
    List<WebElement> textGreyStatistics;
    
    @FindBy(css = ".TT4UPType.TT4metaGray")
    List<WebElement> textBlackStatistics;
    
    @FindBy(css = ".TTtab.TTtitle")
    List<WebElement> listMyPostBottomTabs;
    
    @FindBy(css = "#TTreviewsTab")
    WebElement reviewsBottomTabsMyPost;
    
    @FindBy(css = "#TTquestionsTab")
    WebElement questionsBottomTabsMyPost;
    
    @FindBy(css = "#TTanswersTab")
    WebElement answersBottomTabsMyPost;
    
    @FindBy(css = "#TTchatterTab")
    WebElement commentsBottomTabsMyPost;
    
    @FindBy(css = "#TT3toplinks")
    WebElement topLinksMyPost;
    
    @FindBy(css = "#TT3toplinks #TT3loLink")
    WebElement lnkNotFirstNameReview;

    @FindBy(css = "a#TT3AmqLink")
    WebElement lnkReviewMorePurchase;

    @FindBy(css = "#TTsearchTermGrp")
    WebElement txtSearchReview;
    
    @FindBy(css = "#TT4inlineSqvR .TTrevCol2")
    WebElement txtSearchedReviewDetails;
    
    @FindBy(css = ".TT4instAnsUL")
    WebElement lblReviewSectionCmts;

    @FindBy(css = "#TTreviewSort")
    WebElement cmbSortReview;

    @FindBy(css = ".TTreview .TTratingBox")
    WebElement txtReviewCustomerStar;

    @FindBy(css = "span[itemprop='reviewer']")
    WebElement txtReviewReviewer;

    @FindBy(css = "span[itemprop='author']")
    WebElement txtReviewAuthor;
    
    @FindBy(css = ".TTmediaSmallDesc")
    WebElement txtReviewProductName;
    
    @FindBy(css = "#TTrevCatItemImg")
    WebElement reviewProductImage;
    
    @FindBy(css = "#TTwriteRevGreet>p:nth-child(1)")
    WebElement reviewTxtShareExperience;
    
    @FindBy(css = ".TTvc-bar-media-lg-photo.TTmediaBtn")
    WebElement lnkReviewAddPhoto;
    
    @FindBy(css = ".TTwriteRevRightCol .TTvc-bar-media-lg-video.TTmediaBtn")
    WebElement lnkReviewAddVideo;
    
    @FindBy(css = "#TT2myNetworkSection #TTshareMediaScreen")
    WebElement modalAddPhotoVideo;
    
    @FindBy(css = ".TTmediaSubmitBtns .TTskipLink")
    WebElement btnCancelAddPhotoVideo;
    
    @FindBy(css = "#TTtraWindowClose")
    WebElement iconCloseAddPhotoVideo;
    
    @FindBy(css = "#TTSubWindowClose")
    WebElement iconCloseFindYourProfile;

    @FindBy(css = ".TTreviewTitle")
    WebElement txtReviewTitle;
    
    @FindBy(css = ".TTreviewBody")
    WebElement txtReviewSearchBody;

    @FindBy(css = ".TTmoreFeedbackPrompt")
    WebElement lblReviewThankYou;

    @FindBy(css = ".TT2left #TTreviewSummaryAverageRating")
    WebElement txtAvgRatingsReview;
    
    @FindBy(css = ".b_product_badge ")
	WebElement imgProductBadge_Desktop_Tablet;
    
    @FindBy(css = ".b_product_badge.hide-desktop.hide-tablet")
	WebElement imgProductBadge_Mobile;

    @FindBy(css = ".TT2left div.TTratingBox")
    WebElement txtAvgRatingBox;
    
    @FindBy(css = ".product-special-messages")
	WebElement txtSpecialProductMsg;

    @FindBy(css = "#TTreviews .TTreview")
    List<WebElement> sectionReviewsList;

    @FindBy(css = "#TTreviews .TTreview")
    WebElement firstReview;
    
    @FindBy(css = "#TT4instantAnswersR .TT3itemBox .TT3qText")
    WebElement firstSearchedReview;
    
    @FindBy(css = ".TT3itemBox")
    List<WebElement> lstSearchedReview;
    
    @FindBy(css = ".tab-label.tab-header")
    List<WebElement> sectionDetailsAndReview;

    @FindBy(css = ".TTGalleryContent")
    WebElement sectionGalleryContent;

    @FindBy(css = ".tab-content.review-section.current")
    WebElement sectionReviewVisible;

    @FindBy(css = ".product-primary-image img.primary-image")
    WebElement imgPrimaryImage;

    @FindBy(css = ".product-name")
    WebElement productSetName;

    @FindBy(css = ".minicartpopup .mini-cart-image a>img")
    WebElement imgPrimaryImageInMCOverLay;

    @FindBy(css = ".minicartpopup .slick-active .mini-cart-image :not(.brand-logo) img")
    WebElement lnkProductImageActiveProdNameMCOverLay;

    @FindBy(css = ".mini-cart-product.slick-active .mini-cart-name a")
    WebElement lnkProductImageActiveMCOverLay;

    @FindBy(css = ".minicartpopup .slick-active .mini-cart-product-info .mini-cart-name a")
    WebElement lnkProductNameActiveInMCOverLay;

    @FindBy(css = ".minicartpopup .slick-active .mini-cart-product-info .mini-cart-attributes")
    WebElement lnkProductColorSizeInMCOverlay;

    @FindBy(css = ".minicartpopup .slick-active .mini-cart-product-info .mini-cart-pricing")
    WebElement lnkProductQuantityInMCOverlay;

    @FindBy(css = ".minicartpopup .slick-active .mini-cart-product-info .product-availability-list .is-in-stock")
    WebElement lnkProductAvailabilityInMCOverlay;

    @FindBy(css = ".minicartpopup .slick-active .price-sales ")
    WebElement lnkProductPriceInMCOverlay;

    @FindBy(css = ".minicartpopup .mini-cart-product-info .mini-cart-name a")
    WebElement lnkProductNameInMCOverLay;

    @FindBy(css = ".mini-cart-product.slick-slide.slick-current.slick-active .mini-cart-subtotals")
    WebElement subTotalMCOverlay;

    @FindBy(css = ".mini-cart-product.slick-slide.slick-current.slick-active .mini-cart-name a")
    WebElement lnkProductNameOverLay;

    @FindBy(css = ".cart-recommendations.hide-mobile")
    WebElement recommenationMCOverlay;

    @FindBy(css = ".cart-recommendations.hide-mobile .product-tile")
    WebElement recommenationMCOverlayProduct;
    
    @FindBy(css = ".cart-recommendations.hide-mobile .product-tile")
    List<WebElement> recommenationMCOverlayProducts;

    @FindBy(css = ".mini-cart-product.slick-slide.slick-current.slick-active .product-price")
    WebElement lblProductOverLayPrice;

    @FindBy(css = ".mini-cart-product.slick-slide.slick-current.slick-active")
    WebElement lblActiveProductOverlay;

    @FindBy(css = ".minicartpopup .mini-cart-product-info div[data-attribute='size'] .value")
    WebElement lblProductSizeInMCOverLay;
    
    @FindBy(css = ".minicartpopup .mini-cart-product-info div[data-attribute='shoeSize'] .value")
    WebElement lblProductShoeSizeInMCOverLay;
    
    @FindBy(css = ".minicartpopup .mini-cart-product-info div[data-attribute='shoeWidth'] .value")
    WebElement lblProductShoeWidthInMCOverLay;
    
    @FindBy(css = ".minicartpopup .mini-cart-product-info div[data-attribute='braCupSize'] .value")
    WebElement lblProductBraCupInMCOverLay;
    
    @FindBy(css = ".minicartpopup .mini-cart-product-info div[data-attribute='braBandSize'] .value")
    WebElement lblProductBraBandInMCOverLay;

    @FindBy(css = ".minicartpopup .mini-cart-product-info div[data-attribute='color'] .value")
    WebElement lblProductColorInMCOverLay;

    @FindBy(css = ".minicartpopup .slick-current.slick-active .mini-cart-product-info div[data-attribute='size'] .value")
    WebElement lblProductSizeActiveInMCOverLay;

    @FindBy(css = ".minicartpopup .slick-current.slick-active .mini-cart-product-info div[data-attribute='color'] .value")
    WebElement lblProductColorActiveInMCOverLay;

    @FindBy(css = ".minicartpopup .slick-current.slick-active .mini-cart-product-info .product-availability-list")
    WebElement lblProductStockActiveInMCOverLay;

    @FindBy(css = ".mini-cart-product.slick-slide.slick-current.slick-active .mini-cart-subtotals .value")
    WebElement lblProductSubTotalAndNoOfItemMCOverlay;

    @FindBy(css = ".minicartpopup .slick-current.slick-active .mini-cart-product-info .mini-cart-pricing .value")
    List<WebElement> lblProductQtyAndPriceActiveInMCOverLay;

    @FindBy(css = ".minicartpopup .slick-current.slick-active .mini-cart-pricing .price-sales.price-standard-exist")
    WebElement lblProductSalesPriceActiveInMCOverLay;

    @FindBy(xpath = "//div[contains(@class,'minicartpopup')]//span[contains(text(),'Qty:')]//following-sibling::*[1][self::span]")
    WebElement lblProductQtyInMCOverLay;

    @FindBy(css = ".minicartpopup .product-availability-list")
    WebElement lblProductAvailInMCOverLay;

    @FindBy(css = ".minicartpopup .product-price")
    WebElement lblProductPriceInMCOverLay;

    @FindBy(css = ".minicartpopup .price-sales")
    WebElement lblGiftCardPriceInMCOverLay;
    
    @FindBy(css = ".minicartpopup .total-price .value")
    WebElement lblProductTotalInMCOverLay;

    @FindBy(xpath = "//div[contains(@class,'minicartpopup')]//span[contains(text(),'price:')]//following-sibling::span")
    List<WebElement> lstProductPriceInMCOverLay;

    @FindBy(xpath = "//div[contains(@class,'minicartpopup')]//span[contains(text(),'Qty:')]//following-sibling::*[1][self::span]")
    List<WebElement> lstProductQtyInMCOverLay;

    @FindBy(css = ".pdp-main.standardproduct")
    WebElement divStandardProduct;

    @FindBy(css = ".pdp-main.specialproductset")
    WebElement divSpecialProductSet;

    @FindBy(css = ".special-product-set-overlay.slick-active")
    WebElement divSpecialProductSetInMCOverLay;

    @FindBy(css = ".overlay-product-slick .slick-next.slick-arrow")
    WebElement nextArrowInMCOverlay;

    @FindBy(css = ".pdp-main.standardproductset")
    WebElement divProductSet;

    @FindBy(css = ".minicartpopup .hide-mobile .mini-cart-subtotals.hide")
    WebElement lblItemInUrBagMCOverlay;

    @FindBy(css = ".only-for-mobile .mini-cart-subtotals.hide")
    WebElement lblItemInUrBagMCOverlayMobile;
    
    @FindBy(css = "#dialog-container .addtocartoverlay-content")
    WebElement divATBOverlayContent;

    @FindBy(css = ".addtocartoverlay-content button.checkout-now")
    WebElement btnCheckoutInMCOverlay;

    @FindBy(css = "div[id='thumbnails'] div[class$=slick-active]")
    List<WebElement> altrnateImg;

    @FindBy(css = ".view-details>a")
    List<WebElement> viewfullDetail;

    @FindBy(css = ".view-details a")
    WebElement viewfullDetailProduct;

    @FindBy(css = ".product-set-details .product-price")
    WebElement txtProductPrice;

    @FindBy(css = ".product-set-details .price-sales")
    WebElement productSalePrice;

    @FindBy(css = ".sps-ps-total-price")
    WebElement lblSPSPriceSection;

    @FindBy(css = ".swatchanchor>img")
    List<WebElement> lstcolorSwatches;

    @FindBy(css = ".swatchanchor")
    List<WebElement> lstGiftColorSwatches;

    @FindBy(css = ".swatches.color")
    WebElement divColorSwatches;

    @FindBy(css = "li[class='selectable selected']>input>img")
    WebElement selectedSwatchImg;
    
    @FindBy(css = ".product-image.main-image")
    WebElement mainImageProductSet;
    
    @FindBy(css = ".hide-tablet .bh-choose-item-below")
    WebElement chooseItemsBelowProductSetBHDeskMobile;
    
    @FindBy(css = ".hide-desktop.hide-mobile .bh-choose-item-below")
    WebElement chooseItemsBelowProductSetBHTablet;

    @FindBy(css = ".swatches.size .selectable.selected")
    List<WebElement> lstSelectedProdSetSizeSwatch;

    @FindBy(css = ".swatches.color .selectable.selected img")
    List<WebElement> lstSelectedProdSetColorSwatch;

    @FindBy(css = ".swatches.width .selectable.selected input")
    List<WebElement> lstSelectedProdSetWidthSwatch;

    @FindBy(css = ".product-primary-image img[class='primary-image']")
    WebElement primaryImg;

    @FindBy(css = "ul[class='swatches size'] li[class='selectable selected']")
    WebElement btnsizeSelected;

    @FindBy(css = ".unselectable .swatchanchor")
    WebElement sizeOutofStock;

    @FindBy(css = "#ui-id-1")
    WebElement itemAddedtoBagLabel;

    //
    @FindBy(css = "button[title='Close']")
    WebElement btnCloseAddedtoBag;

    @FindBy(css = ".selected-value")
    List<WebElement> selectedValue;

    @FindBy(css = "button[title='Select size and color']")
    WebElement btnPleaseSelectSize;

    @FindBy(css = ".product-add-to-cart .availability-msg")
    WebElement inventoryState;

    @FindBy(css = ".quantity-ats-message")
    WebElement outofStockErrMsg;

    @FindBy(css = ".wishlist>a")
    WebElement wishlist;

    @FindBy(css = "div[class='product-col-1 product-set'] .productthumbnail")
    List<WebElement> lstAlternateImgProductset;

    @FindBy(css = ".color .selected a>img")
    WebElement imgSelectColorSwatch;
    
    @FindBy(css = ".color .selectable")
    List<WebElement> lstcolorSwatchImg;

    @FindBy(css = "select[class='monogram-font-class mono-error']")
    WebElement monogramErrFont;

    @FindBy(css = ".product-variations .swatches.color>li")
    List<WebElement> lstColorSwatches;

    @FindBy(css = "div[class='product-set-details'] span[class='price-standard']")
    List<WebElement> OriginalPriceComponentProd;

    @FindBy(css = "div[class='product-set-details'] span[class$='price-standard-exist']")
    List<WebElement> DiscountPriceComponentProd;

    @FindBy(css = "li[class='selectable selected'] img")
    WebElement colorSwatchselected;

    @FindBy(css = ".clearance-discount-msg.savingmessage span")
    WebElement productPromotion;

    // =====================================================================//
    // ------------------Desktop Elements----------------------------- //
    // =====================================================================//
    @FindBy(css = "h1[class='product-name']")
    WebElement txtSpecialProductSetNameDesktop;

    @FindBy(css = "h1.product-name")
    WebElement txtProductNameDesktop;

    @FindBy(css = ".product-col-2-productdetail .product-name")
    WebElement txtProductSetNameDesktop;

    @FindBy(css = ".product-col-1-productdetail .product-name")
    WebElement txtProductSetNameTablet;

    @FindBy(css = "h1.product-name")
    WebElement txtProductNameSplProdSet;

    @FindBy(css = ".size-chart-link>a")
    WebElement lnkSizechart;

    @FindBy(css = ".product-add-to-cart .availability-msg span")
    WebElement backOrderInventoryState;
    
    @FindBy(css = ".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable")
    WebElement modalSizechart;
    
    @FindBy(css = ".size-guide-container .title")
    WebElement txtSizechartTitle;

    @FindBy(css = ".ui-button-icon-only.ui-dialog-titlebar-close")
    WebElement btnCloseSizeChartModal;

    @FindBy(css = ".size-guide-navigation>a")
    List<WebElement> lstSizeGuideNavigation;

    @FindBy(css = ".size-guide-container .title")
    WebElement lblSizeChartTitle;

    @FindBy(css = ".brand-dropdown>div")
    WebElement drpBrandMenu;

    @FindBy(css = ".dropdown-options>div")
    List<WebElement> lstBrandOptions;

    @FindBy(css = ".content-asset.size-chart-data > img")
    WebElement imgSizeGuideChart;

    @FindBy(css = ".container .infoRow")
    List<WebElement> lstSizeGuideRows;

    @FindBy(css = ".product-monogramming .title")
    WebElement txtMonogrammingTitle;

    @FindBy(css = ".monogramming-enabled-class")
    WebElement chkEnableMonogramming;

    @FindBy(css = ".monogramming-enabled.form>div>div>label>span")
    WebElement txtMonogramMessaging;

    @FindBy(css = "a.tips-link")
    WebElement lnkMonogrammingTips;

    @FindBy(css = ".ui-dialog.tips")
    WebElement flytTips;

    @FindBy(css = "button[title='Close']")
    WebElement btnCloseTipsFlyout;

    @FindBy(css = "select#monogram-font")
    WebElement drpFontSelect;

    @FindBy(css = ".monogramming-color .single-selection")
    WebElement drpColorSingleSelect;

    @FindBy(css = ".monogramming-location .single-selection")
    WebElement drpLocationSingleSelect;

    @FindBy(css = ".monogramming-font .single-selection")
    WebElement drpFontSingleSelect;

    @FindBy(css = ".monogramming-font .selected .value")
    WebElement drpFontValue;

    @FindBy(css = ".monogramming-location .selected .value")
    WebElement drpLocationValue;

    @FindBy(css = ".monogramming-color .selected .value")
    WebElement drpColorValue;

    @FindBy(css = "select#monogram-location")
    WebElement drpLocationSelect;

    @FindBy(css = "div[class^='custom-select monogram-location-class']>select")
    WebElement divMonogramLocation;

    @FindBy(css = "div[class^='custom-select monogram-location-class'] select#monogram-location>option")
    List<WebElement> monogramLocationoptions;

    @FindBy(css = "div[class^='font custom-select monogram-font-class']>select")
    WebElement divMonogramFont;

    @FindBy(css = "div[class^='font custom-select monogram-font-class'] select#monogram-font>option")
    List<WebElement> monogramFontoptions;

    @FindBy(css = "div[class^='custom-select monogram-color-class']>select")
    WebElement divMonogramColor;

    @FindBy(css = "div[class^='custom-select monogram-color-class'] select#monogram-color>option")
    List<WebElement> monogramColoroptions;

    @FindBy(css = "select#monogram-color")
    WebElement drpColorSelect;

    @FindBy(css = ".refresh-link")
    WebElement lnkRefreshMonogramPreview;

    @FindBy(css = ".input-fields-set input[name='monogramminglines-1']")
    WebElement txtMonogramFirstTextField;

    @FindBy(css = ".input-fields-set input[name='monogramminglines-2']")
    WebElement txtMonogramSecondTextField;

    @FindBy(css = ".monogramming-preview>textarea")
    WebElement txtMonogramPreview;

    @FindBy(css = ".presonalised-msg-norefund .warning-msg")
    WebElement txtMonogramDisclaimer;

    @FindBy(css = ".form-row.product-monogramming")
    WebElement divMonogrammingOptions;

    @FindBy(css = ".product-variation-content")
    WebElement sectionProductVariation;

    @FindBy(css = ".hide-desktop .recommendations-heading")
    WebElement txtRecommendationHeadingTabMobile;

    @FindBy(css = ".hide-mobile.hide-tablet .recommendations-heading")
    WebElement txtRecommendationHeadingDesktop;
    
    @FindBy(css=RecommendationDesktop_BH +".recommendations-heading")
    WebElement txtRecommendationHeadingBH;

    @FindBy(css = ".input-fields.monogramming-textfield")
    List<WebElement> monogramingTextField;

    @FindBy(css = ".input-fields-set .character-left:nth-of-type(1)>div>span:nth-of-type(1)")
    WebElement txtActualCharacterCount;

    @FindBy(css = ".input-fields-set .character-left:nth-of-type(1)>div>span:nth-of-type(2)")
    WebElement txtMaximumCharacterCount;

    @FindBy(css = ".input-fields-set .character-left:nth-of-type(1)")
    WebElement textField;

    @FindBy(css = ".input-fields-set .character-left:nth-of-type(1) input")
    WebElement monogrammingTextField;

    @FindBy(css = ".swatches.size")
    WebElement tblSizeSwatch;

    // Hemming
    @FindBy(css = ".product-hemmable")
    WebElement divHemmingOptions;

    @FindBy(css = ".product-hemming .title")
    WebElement txtHemmingTitle;

    @FindBy(css = ".hemming-option.selected")
    WebElement liSelectedHemming;

    @FindBy(css = ".ui-dialog.tips")
    WebElement msgTooltip;

    @FindBy(css = ".swatches.size")
    List<WebElement> lstSizeGrid;

    @FindBy(css = ".swatches.size")
    WebElement divSizeGrid;
    
    @FindBy(css = "#TTreviewSearchTerm")
    WebElement inpReviewSearch;

    @FindBy(css = ".swatches.shoesize")
    WebElement divSizeGrid_shoe;

    @FindBy(css = ".swatches.color")
    List<WebElement> lstColorGrid;

    @FindBy(css = ".attribute.color .selected-value")
    List<WebElement> lstColorValue;

    @FindBy(css = ".swatches.color")
    WebElement divColorGrid;

    @FindBy(css = ".swatches.Width")
    WebElement divWidthGrid;

    @FindBy(css = ".swatches.bracupsize")
    WebElement divCupGrid;
    
    @FindBy(css = ".swatches.shoewidth")
    WebElement divWidthGrid_shoe;
    
    @FindBy(css = ".swatches.shoewidth li a")
    List<WebElement> lstshoeWidth;
    
    @FindBy(css = ".shoeWidth .selected-value")
    WebElement selectedShoeWidth;
    
    @FindBy(css = ".shoeSize .selected-value")
    WebElement selectedShoeSize;

    @FindBy(css = ".swatches.width")
    List<WebElement> lstWidthGrid;

    @FindBy(css = ".swatches.sizefamily")
    List<WebElement> lstSizeFamilyGrid;

    @FindBy(css = ".overlay-product-slick .slick-dots")
    WebElement overlaySlickDotsTab;

    @FindBy(css = ".slick-dots li button")
    List<WebElement> overlaySlickDot;

    @FindBy(css = ".product-hemming-checkbox")
    WebElement chkProductHemming;

    @FindBy(css = ".product-add-to-cart.top-add-all .button-fancy-large")
    WebElement btnAddAllToBag;

    @FindBy(css = ".ui-dialog-title")
    WebElement titleShoppingBagPopUp;

    @FindBy(css = ".checkbox>label>span")
    WebElement txtHemmingMessaging;

    @FindBy(css = ".hemming-tips-link")
    WebElement lnkHemmingTips;

    @FindBy(css = "div.hemming-drawer ul")
    WebElement divHemmingDrawer;

    @FindBy(css = ".ui-dialog.product-hemming-tips")
    WebElement flytHemmingTips;

    @FindBy(css = ".hemming-drawer>ul>li")
    List<WebElement> lstHemmingOptions;

    @FindBy(css = ".hemming-drawer>ul>li:last-child")
    WebElement btnLastHemmingOption;

    @FindBy(css = ".hemming-error-msg")
    WebElement txtHemmingErrorMessage;
    
    @FindBy(css = ".product-variation-content .product-price")
    WebElement txtProdPrice;

    @FindBy(css = ".hemming-drawer .presonalised-msg-norefund")
    WebElement txtHemmingDisclaimer;

    @FindBy(css = ".top-menu-utility .minicart-quantity")
    WebElement txtBagCountDesktop;

    @FindBy(css = "#dialog-container .mini-cart-subtotals.hide")
    WebElement txtItemsAddedInBag;

    @FindBy(css = Product_Review_Teaser)
    WebElement divReviewAndRating;
    
    @FindBy(css = Product_Review_Teaser + "#readReviews")
    WebElement divReview;
    
    @FindBy(css = Product_Review_Teaser + ".TTratingBox")
    WebElement divRating;

    @FindBy(css = Product_Set_Details_Section + " #product-teasers .rating-text")
    WebElement divReviewAndRatingPrdSet;

    @FindBy(css = Product_Set_Details_Section_Tablet_Mobile + " #product-teasers .rating-text")
    WebElement divReviewAndRatingPrdSet_Tab_Mob;

    @FindBy(css = "#product-teasers .TurnToReviewsTeaser")
    WebElement divReviewAndRatingMobile;

    @FindBy(css = "#product-teasers a#readReviews ")
    WebElement lnkReviews;
    
    @FindBy(css = "#product-teasers")
    WebElement lnkReviewsSection;
    
    @FindBy(css = "#product-teasers")
    WebElement lnkReviews_mobile;

    @FindBy(css = Product_Set_Details_Section + " #product-teasers a.rating-text ")
    WebElement lnkReviewsPrdSet;

    @FindBy(css = Product_Set_Details_Section_Tablet_Mobile + " #product-teasers a.rating-text")
    WebElement lnkReviewsPrdSet_Tab_Mob;

    @FindBy(css = Product_Set_Details_Section + " #product-teasers .TTratingBox")
    WebElement imgRatingStarsPrdSet;

    @FindBy(css = Product_Set_Details_Section_Tablet_Mobile + " #product-teasers .TTratingBox")
    WebElement imgRatingStarsPrdSet_Tab_Mob;

    @FindBy(css = Product_Set_Details_Section + " .product-shortdescription")
    WebElement lblShortDescPrdSet;

    @FindBy(css = Product_Set_Details_Section + " .product-price.sps-ps-total-price")
    WebElement lblMainPriceDetailsPrdSet;

    @FindBy(css = Product_Set_Details_Section_Tablet_Mobile + " .product-price.sps-ps-total-price")
    WebElement lblMainPriceDetailsPrdSet_Tab_Mob;

    @FindBy(css = Product_Set_Details_Section_Tablet_Mobile + " .product-longdescription")
    WebElement lblLongDescPrdSet_Tab_Mob;

    @FindBy(css = Product_Set_Details_Section + " .product-set-details .price-sales")
    WebElement lblSalePricePrdSet;

    @FindBy(css = Product_Set_Details_Section + " .sps-ps-total-price .price-sales.price-standard-exist")
    WebElement lblMainPricePrdSet;

    @FindBy(css = Product_Set_Details_Section_Tablet_Mobile + " .sps-ps-total-price .price-sales.price-standard-exist")
    WebElement lblMainPricePrdSet_Mob_Tab;

    @FindBy(css = Product_Set_Details_Section_Tablet_Mobile + " .product-set-details .price-sales")
    WebElement lblSalePricePrdSet_Mob_Tab;

    @FindBy(css = Product_Set_Details_Section + " .product-set-item")
    WebElement sectionProductSetComponent;

    @FindBy(css = Product_Set_Details_Section + " .top-add-all .button-fancy-large")
    WebElement btnTopAddAllToBagPrdSet;

    @FindBy(css = Product_Set_Details_Section + " div.product-add-to-cart:not(.top-add-all) .button-fancy-large")
    WebElement btnBottomAddAllToBagPrdSet;

    @FindBy(css = Product_Set_Details_Section + " .product-longdescription")
    WebElement lblLongDescPrdSet;

    @FindBy(css = Product_Set_Details_Section + " .share-icon-container")
    WebElement sectionShareIconsPrdSet;

    @FindBy(css = Product_Set_Details_Section + " .shop-the-look")
    WebElement lblShopTheLookPrdSet;

    @FindBy(css = Product_Set_Details_Section + " .share-icon.share-twitter")
    WebElement lnkShareTwitterIcon;

    @FindBy(css = Product_Set_Details_Section + " .share-icon.share-facebook")
    WebElement lnkShareFaceBookIcon;

    @FindBy(css = Product_Set_Details_Section + " a[title='Share on Pinterest']")
    WebElement lnkSharePinterestIcon;

    @FindBy(css = ".tabs .tab-label.review-heading")
    WebElement tabReviews;

    @FindBy(css = ".TTreviewSummary .TTreviewCount")
    WebElement lblReviewCount;

    @FindBy(css = "#product-teasers .TTratingBox")
    WebElement imgRatingStars;

    @FindBy(css = ".product-add-to-cart button[value='Add All to Cart']")
    List<WebElement> lstAddAllToBagBtn;

    @FindBy(css = ".tab-content.current")
    WebElement sectionCurrentTab;

    @FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop")
    List<WebElement> shopNowWindow_ProductSet;

    @FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop.open")
    List<WebElement> shopNowWindow_ProductSet_Opened;

    // breadcrumb
    @FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
    WebElement bcLastCategoryNameMobile;

    @FindBy(css = ".breadcrumb a.breadcrumb-element")
    List<WebElement> lstTxtProductInBreadcrumb;

    @FindBy(css = "a[class='breadcrumb-element hide-mobile']")
    List<WebElement> lstTxtProductInBreadcrumbDesktop;

    @FindBy(css = ".breadcrumb a.breadcrumb-element")
    List<WebElement> lstTxtProductInBreadcrumbMobile;

    @FindBy(css = ".breadcrumb-element.current-element")
    WebElement bcCurrentCategoryNameDesktop;

    @FindBy(css = "a.breadcrumb-element.hide-mobile")
    List<WebElement> lstOfBreadcrumbWithCurrentDesktop;

    //
    @FindBy(css = ".product-primary-image a")
    WebElement imgProductPrimaryImage;
    
    @FindBy(css = ".product-primary-image a img")
    WebElement imgPrimaryProductImage;

    @FindBy(css = ".product-thumbnails .slick-active a img")
    WebElement imgProductPrimaryImageMobile;
    
    @FindBy(css = ".zoomWindowContainer > div")
    List<WebElement> lstDivZoomWindowContainer;

    @FindBy(css = ".zoomWindowContainer > div")
    WebElement divZoomWindowContainer;

    @FindBy(css = "#product-content")
    WebElement sectionProductDetails;

    @FindBy(css = ".zoomLens")
    WebElement divZoomLens;
    @FindBy(css = ".size-guide-container>div.title")
    WebElement sizeguideTitle;

    @FindBy(css = "button[value='ADD TO BAG']")
    WebElement btnAddToBagSpecialProductSet;

    @FindBy(css = "button[disabled='disabled']")
    WebElement btnAddToBagSpecialProductSet_Disabled;

    @FindBy(css = ".minicartpopup .mini-cart-product .special-productset-child")
    List<WebElement> lblMiniCartNameSplProdSet;

    @FindBy(css = ".promotion")
    WebElement txtPromoSpecialProductSet;

    @FindBy(css = ".swatches.size .unselectable")
    WebElement swatchSizeOutOfStock;

    @FindBy(id = "add-to-cart")
    WebElement btnAddToBagSpecialProductSetFireFox;

    @FindBy(css = RecommendationDesktop)
    WebElement sectionRecommendationDesktop;
    
    @FindBy(css = ".you-may-like.recommendation.recommendations-h.hide-mobile.hide-tablet")
    WebElement sectionRecommendationDesktopHorizantal;
    
    @FindBy(css = ".you-may-like.recommendation.recommendations-v.hide-mobile.hide-tablet")
    WebElement sectionRecommendationDesktopVertical;
    
    @FindBy(css = RecommendationDesktop_BH)
    WebElement sectionRecommendation_BH;

    @FindBy(css = RecommendationMobileTablet)
    WebElement sectionRecommendationMobileTablet;
    
    @FindBy(css = ".you-may-like.recommendation.bh-recommendations")
    WebElement sectionRecommendationMobileTabletBH;
    
    @FindBy(css = RecommendationMobileTablet +".recommendation-tiles.slick-initialized.slick-slider")
    WebElement sectionRecommendationHorizantal;
    
    @FindBy(css = RecommendationDesktop_BH+".slick-initialized.slick-slider")
    WebElement sectionRecommendationHorizantalBH;
    
    @FindBy(css = RecommendationDesktop_BH +" div[class='b_product_badge'] img")
    WebElement productBadgeInRecommendationDesktop_BH;
    
    @FindBy(css = RecommendationDesktop +" div[class='b_product_badge'] img")
    WebElement productBadgeInRecommendationDesktop;
    
    @FindBy(css = RecommendationMobileTablet +" div[class='b_product_badge'] img")
    WebElement productBadgeInRecommendationMobileTablet;
    
    @FindBy(css = RecommendationDesktop + " img")
    WebElement prodImageRecommendationDesktop;
    
    @FindBy(css = RecommendationDesktop_BH + " img")
    WebElement prodImageRecommendation_BH;

    @FindBy(css = RecommendationMobileTablet + " img")
    WebElement prodImageRecommendationMobileTablet;

    @FindBy(css = RecommendationDesktop + " .product-name")
    WebElement prodNameRecommendationDesktop;
    
    @FindBy(css = RecommendationDesktop_BH + " .product-name")
    WebElement prodNameRecommendation_BH;

    @FindBy(css = RecommendationMobileTablet + " .product-name")
    WebElement prodNameRecommendationMobileTablet;

    @FindBy(css = RecommendationDesktop_BH + " .product-pricing")
    WebElement prodPriceRecommendation_BH;
    
    @FindBy(css = RecommendationDesktop_BH + " .product-pricing")
    List<WebElement> lstProdPriceRecommendation_BH;
    
    @FindBy(css = RecommendationDesktop + " .product-pricing")
    WebElement prodPriceRecommendationDesktop;
    
    @FindBy(css = RecommendationDesktop + " .product-pricing")
    List<WebElement> lstProdPriceRecommendationDesktop;
    
    @FindBy(css = RecommendationMobileTablet + " .product-pricing")
    WebElement prodPriceRecommendationMobileTablet;
    
    @FindBy(css = RecommendationMobileTablet + " .product-pricing")
    List<WebElement> lstProdPriceRecommendationMobileTablet;
    
    @FindBy(css = RecommendationDesktop_BH +".product-standard-price")
    WebElement recommendationlblOriginalPriceInSalePrice_BH;
        
    @FindBy(css = RecommendationDesktop +".product-standard-price")
    WebElement recommendationlblOriginalPriceInSalePrice;
    
    @FindBy(css = RecommendationMobileTablet +".product-standard-price")
    WebElement recommendationlblOriginalPriceInSalePriceMobile;
    
    @FindBy(css = RecommendationDesktop_BH +" .product-tile")
    WebElement tileRecommendationProd_BH;
    
    @FindBy(css = RecommendationDesktop_BH +" .product-tile")
    List<WebElement> tileRecommendationProds_BH;

    @FindBy(css = RecommendationDesktop +" .product-tile")
    WebElement tileRecommendationProd;
    
    @FindBy(css = RecommendationDesktop +" .product-tile")
    List<WebElement> tileRecommendationProdDesktop;
    
    @FindBy(css = RecommendationMobileTablet +" .product-tile")
    WebElement prodTileRecommendationProdTabletMobile;

    @FindBy(css = RecommendationMobileTablet +" .product-tile")
    List<WebElement> tileRecommendationProdTabletMobile;
    
    @FindBy(css = RecommendationMobileTablet +" .slick-active .product-tile")
    WebElement tileActtiveRecommendationProdTabletMobile;
    
    @FindBy(css = RecommendationDesktop_BH +" .product-actual-price:not(.product-sales-price)")
    WebElement prodStdPriceRecommendation_BH;

    @FindBy(css = RecommendationDesktop +" .product-actual-price:not(.product-sales-price)")
    WebElement prodStdPriceRecommendationDesktop;

    @FindBy(css = RecommendationMobileTablet + " .product-actual-price:not(.product-sales-price)")
    WebElement prodStdPriceRecommendationMobileTablet;

    @FindBy(css = RecommendationDesktop_BH + " .product-sales-price:not(.product-actual-price)")
    WebElement prodSalePriceRecommendation_BH;
    
    @FindBy(css = RecommendationDesktop + " .product-sales-price:not(.product-actual-price)")
    WebElement prodSalePriceRecommendationDesktop;

    @FindBy(css = RecommendationMobileTablet + " .product-sales-price:not(.product-actual-price)")
    WebElement prodSalePriceRecommendationMobileTablet;
    
    @FindBy(css = RecommendationDesktop_BH + " img")
    List<WebElement> prodImageListRecommendation_BH;

    @FindBy(css = RecommendationDesktop + " img")
    List<WebElement> prodImageListRecommendationDesktop;

    @FindBy(css = RecommendationMobileTablet + " img")
    List<WebElement> prodImageListRecommendationMobileTablet;
    
    @FindBy(css = RecommendationDesktop_BH + " .product-name")
    List<WebElement> prodNameListRecommendationD_BH;

    @FindBy(css = RecommendationDesktop + " .product-name")
    List<WebElement> prodNameListRecommendationDesktop;

    @FindBy(css = RecommendationMobileTablet + " .product-name")
    List<WebElement> prodNameListRecommendationMobileTablet;
    
    //********Recommendation Next/Prev arrow***********************
    
    @FindBy(css = RecommendationDesktop + ".slick-next:not(.slick-disabled)")
    WebElement nextArrowRecommendationDesktop;

    @FindBy(css = RecommendationMobileTablet + ".slick-next:not(.slick-disabled)")
    WebElement nextArrowRecommendationMobileTablet;
    
    @FindBy(css = RecommendationDesktop + ".slick-prev:not(.slick-disabled)")
    WebElement prevArrowRecommendationDesktop;

    @FindBy(css = RecommendationMobileTablet + ".slick-prev:not(.slick-disabled)")
    WebElement prevArrowRecommendationMobileTablet;
    
    @FindBy(css = RecommendationDesktop + ".slick-next.slick-disabled")
    WebElement nextArrowDisableRecommendationDesktop;

    @FindBy(css = RecommendationMobileTablet + ".slick-next.slick-disabled")
    WebElement nextArrowDisableRecommendationMobileTablet;
    
    @FindBy(css = RecommendationDesktop + ".slick-prev.slick-disabled")
    WebElement prevArrowDisableRecommendationDesktop;

    @FindBy(css = RecommendationMobileTablet + ".slick-prev.slick-disabled")
    WebElement prevArrowDisableRecommendationMobileTablet;
    
    @FindBy(css = RecommendationDesktop_BH + ".slick-next:not(.slick-disabled)")
    WebElement nextArrowRecommendationBH;
    
    @FindBy(css = RecommendationDesktop_BH +".slick-prev:not(.slick-disabled)")
    WebElement prevArrowRecommendationBH;
    
    @FindBy(css = RecommendationDesktop_BH + ".slick-prev.slick-disabled")
    WebElement prevArrowDisableRecommendationBH;
    
    @FindBy(css = RecommendationDesktop_BH + ".slick-next.slick-disabled")
    WebElement nextArrowDisableRecommendationBH;

    @FindBy(css = ".button-with-error-msg > button")
    WebElement txtSelectSizeError;

    @FindBy(css = ".button-fancy-large.all-set-button.add-all-to-cart-disabled")
    WebElement btnAddToBagDisabled;

    @FindBy(css = ".quantity .selected-option.selected")
    WebElement txtSelectedQuantity;

    @FindBy(css = ".quantity .selection-list > li")
    List<WebElement> lstQuantityValues;

    @FindBy(css = ".share-icon-container")
    WebElement divSocialIcons;

    @FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
    WebElement bcLastCategoryNameDesktopTablet;

    @FindBy(css = ".facebook")
    WebElement lnkFaceBook;

    @FindBy(css = ".pinterest")
    WebElement lnkPinterest;

    @FindBy(css = ".twitter")
    WebElement lnkTwitter;

    @FindBy(css = ".share-mail")
    WebElement lnkMail;
    
    @FindBy(css = ".instagram")
    WebElement lnkInstagram;

    @FindBy(css = ".turntofit")
    WebElement turnToFitSection;
    
    @FindBy(css = ".fo-social")
    WebElement sectionSocialIcons;

    @FindBy(css = ".availability-web")
    WebElement txtSpecialProductSetInventory;

    @FindBy(css = ".product-teasers .TTratingBox")
    WebElement productReviewStars;

    // =====================================================================//
    // -------------------Mobile Elements----------------------------- //
    // =====================================================================//
    @FindBy(css = "h1.product-name")
    WebElement txtProductNameMobile;

    @FindBy(css = ".product-col-2-productdetail h1.product-name")
    WebElement txtProductNamePrdSet;
    
    @FindBy(css = ".search-result-items .product-name")
    WebElement txtProductNameRecentlyView;

    @FindBy(css = ".product-primary-image img")
    WebElement imgProductImageDesktop;
    
    @FindBy(css = ".product-image-container a[tabindex='0'] img")
    WebElement imgProductImageMobile;

    @FindBy(css = "li.attribute.size .value")
    WebElement sectionSizeAttribute;

    @FindBy(css = "li.attribute.size .turntofit")
    WebElement sectionTurnToFit;

    @FindBy(css = "li.attribute.color")
    WebElement sectionColorAttribute;

    @FindBy(css = ".you-may-like.recommendation.hide-desktop")
    WebElement sectionRecommendationMobile;

    @FindBy(css = "h1.product-name")
    WebElement txtProductNameMobileTablet;

    @FindBy(css = ".slick-dots li")
    List<WebElement> btnSlickDots;

    @FindBy(css = ".slick-dots")
    WebElement btnSlickDotSection;
    
    @FindBy(css = ".hide-desktop.hide-tablet .product-price.sps-ps-total-price")
    WebElement txtProductSetPriceMob;
    
    @FindBy(css = ".slick-dots .slick-active")
    WebElement btnSlickDotActive;

    @FindBy(css = ".pdp-main :nth-child(1).product-set .product-name:not(.hide-desktop):not(.hide-tablet)")
    WebElement txtProductSetNameMobileTablet;

    // --------For Both Normal Price & Price Range-----------
    @FindBy(css = ".product-variation-content .product-price .price-sales ")
    WebElement lblRegularPrice;

    @FindBy(css = ".product-variation-content .product-price span")
    WebElement lblRegularPriceRange;

    @FindBy(css = ".product-price")
    WebElement lblPrice;
    
    @FindBy(css = "h1.product-name")
    WebElement txtProductName;

    @FindBy(css = ".product-variation-content .product-price .price-sales ")
    List<WebElement> lstRegularPriceProductSet;

    @FindBy(css = ".product-variation-content .product-price .price-sales")
    WebElement lblSalePrice;
    
    @FindBy(css = ".product-variation-content .product-price .price-sales.price-standard-exist")
    WebElement lblDiscountedSalePrice;
    
    @FindBy(css = ".product-price .price-sales.price-standard-exist")
    List<WebElement> lstSalePriceProductset;

    @FindBy(css = ".product-variation-content .product-price .price-standard")
    WebElement lblOriginalPriceInSalePrice;

    @FindBy(css = ".product-price")
    WebElement lblOriginalPrice;

    @FindBy(css = ".product-price .price-standard")
    List<WebElement> lstOriginalPriceInSalePriceProductset;

    @FindBy(css = ".minicart-quantity.total-qty")
    WebElement txtBagCountMobile;
    
    @FindBy(css = ".bh-choose-item-below")
    WebElement btnChooseItemsBelowPrdSet;
    
    @FindBy(css = ".product-set-list .product-set-item")
    WebElement fistItemInProductSet;
    
    @FindBy(css = ".product-set-list .product-set-item")
    List<WebElement> lstItemsInProductSet;
    
    @FindBy(css = ".product-set-list .product-variations")
    WebElement firstProductSetDetails;
    
    @FindBy(css = ".add-to-cart")
    WebElement btnFirstProductSetAddToBag;
    
    @FindBy(css = "#current_size .selected-value")
    WebElement lblSelectedSize;
    
    @FindBy(css = "#current_color .selected-value")
    WebElement lblSelectedColor;
    // --------------------------------------------------------

    // =====================================================================//
    // =====================================================================//
    // =====================================================================//

    @FindBy(css = ".swatches.size li.selectable")
    List<WebElement> lstSize;

    @FindBy(css = ".swatches.size li:not([class='selectable selected']) input")
    WebElement sizeUnselected;

    @FindBy(css = ".swatches.size li:not([class='selectable selected']) input")
    List<WebElement> sizesUnselected;

    @FindBy(css = "#add-to-cart")
    WebElement btnAddToCart;
    
    @FindBy(css = ".product-variations")
    WebElement prdVariation;

    @FindBy(css = "button[value='ADD TO BAG']")
    List<WebElement> btnAddToCartProdset;

    @FindBy(css = ".personalizedMessage .input-checkbox")
    WebElement ckbAddPersonalMsg;

    @FindBy(css = ".button-fancy-large.add-to-cart")
    WebElement btnAddToBag;

    @FindBy(css = ".title")
    WebElement monogrammingTitle;

    @FindBy(css = ".monogram-font-class")
    WebElement cmbFontCmbMonogram;

    @FindBy(css = ".monogram-color-class")
    WebElement cmbColorCmbMonogram;

    @FindBy(css = ".monogram-location-class")
    WebElement cmbLocationCmbMonogram;

    @FindBy(css = ".checkbox")
    WebElement cmbMonogrammingMessage;

    @FindBy(css = ".input-fields-set .character-left:nth-of-type(1)")
    WebElement cmbSixCharCount;

    @FindBy(css = ".input-fields-set .character-left:nth-of-type(2)")
    WebElement cmbEightCharCount;

    @FindBy(css = ".clickhere")
    WebElement lnkClickHere;
    
    @FindBy(css = ".shippingdetails")
    WebElement divShippingInfo;

    @FindBy(css = ".shippingdetails .heading.toggle")
    WebElement lnkShippingExpand;

    @FindBy(css = ".shippingdetails .productinfo")
    WebElement lnkShippingSectionPrdInfo;

    @FindBy(css = ".clickhere.opened")
    WebElement lnkShippingClose;

    @FindBy(css = ".availability-novariation")
    WebElement divNoVariationSelected;
    
    @FindBy(css = ".product-add-to-cart .availability-msg")
    WebElement txtProdAvailStatus;

    @FindBy(css = ".shippinginfo.hide :nth-child(1)")
    WebElement lnkShippingReturn;

    @FindBy(css = ".productinfodate :nth-child(1)")
    WebElement txtInterShipping;

    @FindBy(css = ".shippinginfo p")
    WebElement txtShippingInfo;
    
    @FindBy(css = ".productinfodate")
    WebElement txtProdInfos;

    @FindBy(css = ".heading.toggle.expanded")
    WebElement txtShippingExpand;

    @FindBy(css = ".productinfodate :nth-child(1)")
    WebElement txtPoAddExcluded;

    @FindBy(css = ".productinfodate :nth-child(2)")
    WebElement txtMonogramProduct;

    @FindBy(css = ".productinfodate :nth-child(3)")
    WebElement txtHemmableProduct;

    @FindBy(css = ".productinfodate :nth-child(1)")
    WebElement txtPlasticGiftCard;

    @FindBy(css = ".shippingdetails .heading.toggle")
    WebElement txtShippingAndReturnInfo;

    @FindBy(css = ".moreinfo")
    WebElement lnkMoreInfo;

    @FindBy(css = ".minicartpopup.cart-overlay")
    WebElement mdlMiniCartOverLay;
    
    @FindBy(css = "#product-content > span.visually-hidden")
    WebElement lnkPrdVariationProperty;

    @FindBy(css = ".minicartpopup .ui-dialog-titlebar-close")
    WebElement btnCloseMiniCartOverLay;

    @FindBy(css = ".ui-dialog-titlebar .ui-dialog-title")
    WebElement lblMiniCartOverlayTitle;

    @FindBy(css = ".slick-active .special-productset-child-wrapper .special-productset-child")
    List<WebElement> lstSpecialProductChildInCart;

    @FindBy(css = ".mini-cart-totals > div.mini-cart-subtotals")
    WebElement txtNumberOfItemsInBag;

    @FindBy(css = ".enlarge-video-sec a.enlarge-video.enlarge")
    WebElement btnEnlarge;
    
    @FindBy(css = ".enlarge-video-sec a.enlarge-video.enlarge")
    List<WebElement> lstBtnEnlargeProductSet;

    @FindBy(css = ".enlarge-container")
    WebElement mdlEnlargeWindow;

    @FindBy(css = ".fa.fa-angle-down")
    WebElement drpSizeGuideNavigation;
    
    @FindBy(css = ".productinfodate")
    WebElement lblProductInfo;

    @FindBy(css = ".size-guide-navigation")
    WebElement drpSizeGuide;

    @FindBy(css = ".content-asset.size-chart-data>img")
    WebElement mdlSizeGuidebody;

    @FindBy(css = ".selected-category")
    WebElement selectedNavigationCategoryMobile;

    @FindBy(css = ".size-chart-navigation.selected")
    WebElement selectedNavigationCategoryDesktop;

    @FindBy(css = ".title")
    WebElement txtSizeGuideTitle;

    @FindBy(css = ".brand-dropdown_JSMenu.is-active")
    WebElement drpBrandSubNavigation;

    @FindBy(css = ".size-guide-container")
    WebElement popUpSizeGuide;
    
    @FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
    WebElement breadcrumbElement;

    @FindBy(css = ".breadcrumb-element")
    List<WebElement> listbreadcrum;

    @FindBy(css = ".adjust-inner-content .breadcrumb")
    WebElement divBreadcrumb;

    @FindBy(css = ".product-col-1.product-set")
    WebElement divPrimaryimges;

    @FindBy(css = ".product-thumbnails.hide-tablet.hide-mobile.slick-initialized.slick-slider.remove-ht-ovr-hidden")
    WebElement divAlternativeimages;

    @FindBy(css = ".slick-list.draggable")
    WebElement divProductSetAlternativeimages;

    @FindBy(css = ".product-thumbnails.hide-desktop.remove-ht-ovr-hidden.slick-arrow-bar")
    WebElement divProductSetAlternativeimagesTablet;

    @FindBy(css = ".breadcrumb-element.hide-mobile:not(.current-element)")
    List<WebElement> lstBreadCrumbLinksDesktopTablet;
    
    @FindBy(css = ".breadcrumb-element.hide-desktop.hide-tablet:not(.current-element)")
    List<WebElement> lstBreadCrumbLinksMobile;

    @FindBy(css = ".product-name")
    WebElement txtProductname;

    @FindBy(css = ".product-col-2-productdetail .product-shortdescription")
    WebElement divProductdescription;

    @FindBy(css = ".product-col-1-productdetail .product-shortdescription")
    WebElement divProductdescriptionTablet;

    @FindBy(css = ".product-price")
    WebElement divPricing;

    @FindBy(css = ".product-col-2-productdetail .product-longdescription")
    WebElement divLongdescription;

    @FindBy(css = ".product-col-1-productdetail .product-longdescription")
    WebElement divLongdescriptionTablet;

    @FindBy(css = ".shop-the-look")
    WebElement txtshopthelook;

    @FindBy(css = ".product-add-to-cart.top-add-all .button-fancy-large")
    WebElement btnAddalltobag;

    @FindBy(css = "button[value='ADD TO BAG']")
    WebElement btnAddToBagSplPrdSet;
    
    @FindBy(css = ".monogramming-textfield.mono-error")
    WebElement errMonogTextField;

    @FindBy(css = ".product-add-to-cart.top-add-all .button-fancy-large.all-set-button.add-all-to-cart-disabled")
    WebElement btnAddalltobagFirst;

    @FindBy(css = ".button-fancy-large.all-set-button.add-all-to-cart")
    WebElement btnAddToBagPrdSet;

    @FindBy(css = ".product-set-list")
    WebElement divproductsetlist;
    
    @FindBy(css = "#TTstateUS")
    WebElement selectReviewState;
    
    @FindBy(css = "#TTstate")
    WebElement selectReviewStateMobile;

    @FindBy(css = ".header-promo-bottom")
    WebElement divL1Navigation;


    @FindBy(css = ".zoomContainer")
    WebElement divProductImage;

    @FindBy(css = ".b_product_badge")
    WebElement divProductBadge;

    @FindBy(css = ".zoomLens")
    WebElement divZoomImage;

    @FindBy(css = "#zoom-image-container")
    WebElement divImageOverlay;

    @FindBy(css = "#thumbnails .slick-track")
    WebElement divAlternateImages;

    @FindBy(css = ".slick-next")
    WebElement btnControls;

    @FindBy(css = ".product-col-2-productdetail .share-icon-container .share-icon-label")
    WebElement spanShareIcon;

    @FindBy(css = ".product-col-2-productdetail .share-icon-container span")
    List<WebElement> spanShareIconList;

    @FindBy(css = ".product-col-2-productdetail .share-icon-container")
    WebElement shareIcons;

    @FindBy(css = ".product-col-1-productdetail .share-icon-container span")
    List<WebElement> spanShareIconListTablet;

    @FindBy(css = ".product-review")
    WebElement divRatings;

    @FindBy(css = ".tab:nth-of-type(2) .tab-label.tab-header")
    WebElement lnkReviewsTab;

    @FindBy(css = ".breadcrumb-element.hide-mobile")
    List<WebElement> lstBreadCrumbElementsDesktop;

    @FindBy(css = ".product-set-list .product-set-details")
    List<WebElement> lstProductSetDetails;

    @FindBy(css = ".product-shortdescription")
    List<WebElement> divProductShortDescription;

    @FindBy(css = "#tab-3 .tab-content-details")
    WebElement divDetailsTabContent;

    @FindBy(css = ".hide-desktop.hide-tablet.back-arrow")
    WebElement backArrowIcon;

    @FindBy(css = ".tooltip")
    WebElement txtSeeDetails;

    @FindBy(css = "a[aria-describedby*='ui-id-']")
    WebElement seeDetailsToolTip;

    @FindBy(css = ".tab-label.tab-header.current")
    WebElement lnkDetailsTab;
    
    @FindBy(css = ".current .tab-content-details")
    WebElement lblCurrentSectionContent;

    @FindBy(css = ".tab-content.current > div")
    WebElement divDetailsContent;

    @FindBy(css = ".tooltip .tooltip-content")
    List<WebElement> txtToolTip;

    @FindBy(css = "#dwfrm_product_giftcard_purchase_email")
    WebElement txtRecepientEmail;

    @FindBy(css = "#dwfrm_product_giftcard_purchase_email-error")
    WebElement txtRecepientEmailError;

    @FindBy(css = "#dwfrm_product_giftcard_purchase_confirmemail")
    WebElement txtRecepientConfirmEmail;

    @FindBy(css = "#dwfrm_product_giftcard_purchase_confirmemail-error")
    WebElement errRecepientConfirmMail;

    @FindBy(css = "#dwfrm_product_giftcard_purchase_from")
    WebElement txtRecepientName;
    
    @FindBy(css = ".please-select.error")
    WebElement errPleaseSelectConfirmMail;
    
    @FindBy(css = ".confirmemail.required.error")
    WebElement errConfirmMailEmpty;

    @FindBy(css = "#dwfrm_product_giftcard_purchase_confirmemail-error")
    WebElement txtEmailAddressError;

    @FindBy(css = ".please-select.error")
    WebElement txtError;

    @FindBy(css = ".enlarge-video-sec.mobile-enlarge-video-sec.hide-desktop.hide-tablet a")
    WebElement videoIcon;

    @FindBy(css = ".product-col-1.product-image-container")
    WebElement videoOverlay;

    @FindBy(xpath = "//div[contains(text(),'color:')]//following-sibling::span")
    WebElement lblSelectedColorName;

    @FindBy(css = "ul.swatches.color li.selected")
    WebElement defaultSwatchColor;

    @FindBy(css = "button[class$='add-all-to-cart-disabled']")
    List<WebElement> btnAddAlltoCart;

    @FindBy(css = ".size .selected-value")
    WebElement lblSelectedSizeName;

    @FindBy(css = "a[title='Add this product to wishlist']")
    WebElement lnkAddToWishList;

    @FindBy(css = ".search-wishlist")
    WebElement divSearchWishlist;

    @FindBy(css = "div[class^='product-set-item']")
    List<WebElement> itemsinProdSet;

    @FindBy(css = ".product-price.sps-ps-total-price")
    WebElement txtSpecialProductSetPrice;

    @FindBy(css = ".product-set-product-image")
    List<WebElement> splProductSetComponentImage;

    @FindBy(css = ".attribute.color")
    List<WebElement> splProductSetColorSwatches;

    @FindBy(css = ".product-variations #color-variants-container")
    WebElement productColorSwatches;

    @FindBy(css = ".attribute.size")
    List<WebElement> splProductSetSizeSwatches;

    @FindBy(css = ".attribute.size")
    WebElement productSizeSwatches;

    @FindBy(css = ".attribute.color .selected-value")
    List<WebElement> selectedColorInSplProdSet;

    @FindBy(css = ".attribute.color .unselectable")
    List<WebElement> disableColorOutOfStock;

    @FindBy(css = ".max-cart-qty")
    WebElement lblMaxCartError;

    @FindBy(css = ".savingmessage .value")
    WebElement txtSavingStory;

    @FindBy(css = ".promotion-callout .callout-message")
    WebElement txtPromotionCalloutMsg;

    @FindBy(css = ".breadcrumb .current-element.hide-mobile")
    WebElement lblPrimaryBreadCrumbDesktop;

    @FindBy(css = ".promotion-callout > span:nth-child(8)")
    WebElement txtPromotionCalloutMessage;

    @FindBy(css = ".breadcrumb-element.current-element.hide-desktop")
    WebElement lblCurrentBreadCrumbMobile;

    @FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
    WebElement lblCurrentBreadCrumbDesktop;

    @FindBy(css = ".productinfodate>p")
    WebElement splProductMessaging;

    @FindBy(css = ".terms-scrollbar-height")
    WebElement toolTipInnerElement;

    @FindBy(id = "dialog-container")
    WebElement toolTipOuterElement;

    @FindBy(css = ".zoomContainer")
    WebElement toolTipEmptySpace;
    
    @FindBy(css = ".slimScrollBar")
    WebElement toolTipScroller;
    
    @FindBy(css = ".attribute.color .label")
    WebElement colorlabel;

    @FindBy(css = ".attribute.color .attribute_label span")
    WebElement pdpSelectedColor;

    @FindBy(css = ".swatchanchor.Size.Family")
    WebElement width;

    // =====================================================================//
    // social icon //
    // =====================================================================//
    @FindBy(xpath = "//div[@class='product-col-2-productdetail']/div[@class='share-icon-container']/a[contains(@title,'Pinterest')]")
    WebElement socialIconPinterest;

    @FindBy(css = "button[class^='button-fancy-large all-set-button add-all-to-cart']")
    WebElement addalltobag;

    @FindBy(xpath = "//div[@class='product-col-2-productdetail']/div[@class='share-icon-container']/a[contains(@title,'Facebook')]")
    WebElement socialIconFacebook;

    @FindBy(xpath = "//div[@class='product-col-2-productdetail']/div[@class='share-icon-container']/a[contains(@title,'Twitter')]")
    WebElement socialIconTwitter;

    @FindBy(xpath = "//div[@class='product-col-2-productdetail']/div[@class='share-icon-container']/a[contains(@title,'Email')]")
    WebElement socialIconEmail;

    @FindBy(xpath = "//div[@class='product-col-1-productdetail']/div[@class='share-icon-container']/a[contains(@title,'Pinterest')]")
    WebElement socialIconPinterestTablet;

    @FindBy(css = "button[class^='button-fancy-large all-set-button add-all-to-cart']")
    WebElement addalltobagTablet;

    @FindBy(xpath = "//div[@class='product-col-1-productdetail']/div[@class='share-icon-container']/a[contains(@title,'Facebook')]")
    WebElement socialIconFacebookTablet;

    @FindBy(xpath = "//div[@class='product-col-1-productdetail']/div[@class='share-icon-container']/a[contains(@title,'Twitter')]")
    WebElement socialIconTwitterTablet;

    @FindBy(xpath = "//div[@class='product-col-1-productdetail']/div[@class='share-icon-container']/a[contains(@title,'Email')]")
    WebElement socialIconEmailTablet;

    @FindBy(xpath = "//a[contains(@class,'breadcrumb-element hide-mobile')][text()='Home']")
    WebElement lnkBCHome;

    @FindBy(css = ".breadcrumb-element.current-element.hide-desktop")
    WebElement lnkBCHomeMobile;

    @FindBy(css = ".e-gift-email")
    WebElement sectionEGiftEmail;

    @FindBy(css = ".minicartpopup .checkout-now")
    WebElement btnCheckoutInATBMdl;

    @FindBy(css = ".product-set-item")
    List<WebElement> childProducts;

    @FindBy(css = ".swatches.size li.selected")
    List<WebElement> lstSelectedSizeSwatch;

    @FindBy(css = ".swatches.color li.selected")
    List<WebElement> lstSelectedColorSwatch;

    @FindBy(css = "li.shoeWidth")
    WebElement divWidth;

    @FindBy(css = "li.shoeSize")
    WebElement divShoeSize;

    @FindBy(css = "li.braBandSize")
    WebElement divBraSize;

    @FindBy(css = "li.braCupSize")
    WebElement divCupSize;

    @FindBy(css = "li.size")
    WebElement divRegularSize;
    
    @FindBy(css = ".product-name-image-container .name-link")
    WebElement productNameImage;
    
    @FindBy(css = ".product-name-image-container .name-link")
    List<WebElement> productNameImageLst;

    /**********************************************************************************************
     ********************************* WebElements of PDP Page - Ends ****************************
     **********************************************************************************************/

    /**
     * constructor of the class
     *
     * @param driver : Webdriver
     */
    public PdpPage(WebDriver driver) {
        this.driver = driver;
        ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
        PageFactory.initElements(finder, this);
    }

    @Override
    protected void isLoaded() {
        if (!isPageLoaded) {
            Assert.fail();
        }

        if (isPageLoaded && !(Utils.waitForElement(driver, cntPdpContent))) {
            Log.fail("PDP Page did not open up. Site might be down.", driver);
        }
        try {
	        GlobalNavigation.closePopup(driver);
        } catch (Exception ex) {
        	ex.printStackTrace();
        }
        elementLayer = new ElementLayer(driver);
        runBrowser = Utils.getRunBrowser(driver);
    }// isLoaded

    @Override
    protected void load() {
        Utils.waitForPageLoad(driver);
        isPageLoaded = true;
    }

    /**
     * To get the Load status of page
     *
     * @return boolean - 'true'(Page loaded) / 'false'(Page not loaded)
     * @throws Exception - Exception
     */
    public boolean getPageLoadStatus() throws Exception {
        return isPageLoaded;
    }

    /**
     * To wait for spinner in the PDP
     */
    public void waitForSpinner() {
        //Utils.waitUntilElementDisappear(driver, PDPspinner);
        if (Utils.waitForElement(driver, waitLoader)) {
            Utils.waitUntilElementDisappear(driver, waitLoader);
        }
    }

    /**
     * To get the product name in the page
     *
     * @return String - product name
     * @throws Exception - Exception
     */
    public String getProductName() throws Exception {
        String dataToReturn = new String();
        if (Utils.waitForElement(driver, txtProductNameMobile)) {
            dataToReturn = BrowserActions.getText(driver, txtProductNameMobile, "Product Name");
        } else if (Utils.waitForElement(driver, txtProductNamePrdSet)) {
            dataToReturn = BrowserActions.getText(driver, txtProductNamePrdSet, "Product Name");
        } else {
            Log.reference("Product name not found in page!!");
            return "";
        }
        return dataToReturn.trim().replace("amp;", "");
    }

    /**
     * @return String
     * @throws Exception -
     */
    public String getProductSetName() throws Exception {
        String dataToReturn = new String();
        if (Utils.isDesktop()) {
            dataToReturn = productSetName.getText();
        } else if (Utils.isTablet()) {
            dataToReturn = txtProductSetNameMobileTablet.getText();
        }
        return dataToReturn;
    }


    /**
     * To get Spect product Set Name
     *
     * @return -Name
     * @throws Exception - Exception
     */
    public String getSplProductSetName() throws Exception {
        String dataToReturn = new String();
        if (Utils.isDesktop()) {
            dataToReturn = BrowserActions.getText(driver, txtProductNameSplProdSet, "Special Product set name");
        } else if (Utils.isTablet()) {
            dataToReturn = BrowserActions.getText(driver, txtProductNameSplProdSet, "Special Product set name");
        } else if (Utils.isMobile()) {
            dataToReturn = BrowserActions.getText(driver, txtProductNameMobile, "Special Product set name");
        }
        return dataToReturn;
    }

    /**
     * To click on Shopping Cart tool tip
     * @return int
     * @throws Exception - Exception
     */
    public int getCharacterCount() throws Exception {
        int intPersonalTextCount = 0;
        if (Utils.waitForElement(driver, txtPersonalMsgCounter)) {
            String txtCounter = txtPersonalMsgCounter.getText().substring(1,
                    Math.min(txtPersonalMsgCounter.getText().length(), 4));
            intPersonalTextCount = Integer.parseInt(txtCounter);
        }
        return intPersonalTextCount;
    }

    /**
     * To enter text in the personalise message text area
     *
     * @throws Exception - Exception
     */
    public void enterPersonalMessage(String text) throws Exception {
        BrowserActions.typeOnTextField(txtAreaPersonalMsg, text, driver, "Personalise Message");
    }

    /**
     * To get personal counter text
     *
     * @return String - personal counter text
     * @throws Exception - Exception
     */
    public String getPersonalCounterText() throws Exception {
        String txtCounter = "";
        if (Utils.waitForElement(driver, txtPersonalMsgCounter)) {
            txtCounter = txtPersonalMsgCounter.getText();
        }
        return txtCounter;
    }

    /**
     * to verify Initial Values of text counter
     *
     * @param initalCount -
     * @param initialText -
     * @return boolean
     * @throws Exception -
     */
    public boolean verifyInitialValueOfTextCounter(String initalCount, String initialText) throws Exception {
        int intInitalCount = Integer.parseInt(initalCount);
        if (!(intInitalCount == getCharacterCount())) {
            return false;
        }
        if (!getPersonalCounterText().equals(initialText)) {
            return false;
        }
        return true;
    }


    /**
     * To get the navigation category selected in the size chart container
     *
     * @return NavigationText - selected navigation container menu name
     * @throws Exception - Exception
     */
    public String getSelectedGuideInSizeChartContainer() throws Exception {
        if (Utils.waitForElement(driver, selectedNavigationCategoryDesktop)) {
            return BrowserActions.getText(driver, selectedNavigationCategoryDesktop, "Navigation category");
        }
        return "";
    }

    /**
     * To get the product name in the page countTextToEnter
     *
     * @param countTextToEnter -
     * @param toCheckBackSpace -
     * @param textToCheck      -
     * @return boolean
     * @throws Exception -
     */
    public boolean verifyTextCounterIncreaseOrDecrease(int countTextToEnter, boolean toCheckBackSpace,
                                                       String... textToCheck) throws Exception {
        char[] charArray = {'a', ' ', 'e'};
        int changeCount = 0, intPersonalTextCount = 0;
        String randomString = RandomStringUtils.random(countTextToEnter, charArray);
        if (Utils.waitForElement(driver, txtAreaPersonalMsg)) {
            txtAreaPersonalMsg.clear();
            txtAreaPersonalMsg.sendKeys(randomString);
            if (toCheckBackSpace) {
                for (int negCount = 1; negCount <= 3; negCount++) {
                    txtAreaPersonalMsg.sendKeys(Keys.BACK_SPACE);
                    intPersonalTextCount = getCharacterCount();
                    changeCount = intPersonalTextCount + countTextToEnter - negCount;
                    if (changeCount != 330) {
                        return false;
                    }
                }
            } else {
                if (countTextToEnter < 330) {
                    intPersonalTextCount = getCharacterCount();
                    changeCount = intPersonalTextCount + countTextToEnter;
                    if (changeCount != 330) {
                        return false;
                    }
                } else {
                    String textToVerify = getPersonalCounterText();
                    if (!textToVerify.equals(textToCheck[0])) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * @throws Exception -
     */
    public void selectClickHereInShippingSection() throws Exception {
        if (Utils.waitForElement(driver, lnkClickHere)) {
            BrowserActions.javascriptClick(lnkClickHere, driver, "Click Here");
        }
    }

    /**
     * select Shipping Section expanded
     *
     * @throws Exception -
     */
    public void selectShippingSectionExpanded() throws Exception {
        if (Utils.waitForElement(driver, lnkShippingExpand)) {
            lnkShippingExpand.click();
        }
    }

    /**
     * To click on click here link in shipping and returns section
     *
     * @throws Exception - Exception
     */
    public void clickCloseInShippingSection() throws Exception {
        if (Utils.waitForElement(driver, lnkShippingClose)) {
            BrowserActions.javascriptClick(lnkShippingClose, driver, "Shipping Close");
        }
    }

    /**
     * To add product to bag
     * <br> Will select variation if previously not selected
     * <br> Use for overlay verifications
     * @throws Exception - Exception
     */
    public void addToBagKeepOverlay() throws Exception {
    	selectAllSwatches();
    	clickAddProductToBag();

        if (Utils.waitForElement(driver, outofStockErrMsg)) {
            Log.failsoft("Product Out of Stock!", driver);
            return;
        }
    }
    
    /**
     * To click on checkout button in ATB overlay
     * @return instance of ShoppingBagPage
     * @throws Exception
     */
    public ShoppingBagPage clickOnCheckoutInMCOverlay() throws Exception {
        Utils.waitForElement(driver, btnCheckoutInMCOverlay, 5);
        BrowserActions.clickOnElementX(btnCheckoutInMCOverlay, driver, "Checkout Button in Mini Cart Overlay ");
        Utils.waitForPageLoad(driver);

        return new ShoppingBagPage(driver).get();
    }
    
    /**
     * To add product to bag and navigate to cart page
     * @return ShoppingBagPage
     * @throws Exception
     */
    public ShoppingBagPage addToBagAndNavigate() throws Exception{
    	addToBagKeepOverlay();
    	return clickOnCheckoutInMCOverlay();
    }

    /**
     * To click on update button in edit product details
     * @throws Exception - Exception
     */
    public void clickOnUpdate() throws Exception {
    	Utils.waitForElement(driver, btnAddToCart);
    	BrowserActions.scrollInToView(btnAddToCart, driver);
        BrowserActions.clickOnElementX(btnAddToCart, driver, "Update");
        Utils.waitForPageLoad(driver);
    }

    /**
     * To click on Bread Crumb By Index
     *
     * @param index - type integer, index of the bread crumb need to click
     * @return PlpPage - PlpPage page object
     * @throws Exception - Exception
     */
    public PlpPage clickOnBreadCrumbByIndex(int index) throws Exception {
        BrowserActions.clickOnElementX(lstTxtProductInBreadcrumbDesktop.get(index - 1), driver, "Breadcrumb");
        Utils.waitForPageLoad(driver);
        return new PlpPage(driver).get();
    }
    

    /**
     * To click on Bread Crumb By Index
     *
     * @param index - type integer, index of the bread crumb need to click
     * @return PlpPage - PlpPage page object
     * @throws Exception - Exception
     */
    public void clickOnBreadCrumbByIndexValue(int index) throws Exception {
        BrowserActions.clickOnElementX(lstTxtProductInBreadcrumbDesktop.get(index - 1), driver, "Breadcrumb");
        Utils.waitForPageLoad(driver);
    }
    
    /**
     * To click on the size chart link
     *
     * @throws Exception - Exception
     */
    public void clickSizeChartLink() throws Exception {
        load();
        BrowserActions.clickOnElementX(lnkSizechart, driver, "Size Chart");
        Utils.waitForElement(driver, modalSizechart);
    }

    /**
     * To click on the size chart link
     *
     * @param Productname -
     * @throws Exception -
     */
    public void clickSizeChartLinkProdset(String Productname) throws Exception {
        load();

        int index = 0;
        List<WebElement> componentprods = driver.findElements(By.cssSelector(".item-name"));
        for (int i = 0; i < componentprods.size(); i++) {
            if (componentprods.get(i).getAttribute("title").contains(Productname)) {
                index = i;
                break;
            }
        }

        List<WebElement> product_list = driver.findElements(By.cssSelector(".product-set-item"));
        WebElement Sizechart = product_list.get(index).findElement(By.cssSelector(".size-chart-link>a"));

        BrowserActions.clickOnElementX(Sizechart, driver, "Size Chart");
        Utils.waitForElement(driver, modalSizechart);
    }

    /**
     * To close the size chart modal popup
     *
     * @throws Exception - Exception
     */
    public void closeSizeChartModalPopup() throws Exception {
        BrowserActions.clickOnElementX(btnCloseSizeChartModal, driver, "Close Size Chart");
        if (!(Utils.isTablet() || (runBrowser.equals("chrome") || runBrowser.equals("safari")))) {
            Actions action = new Actions(driver);
            action.moveByOffset(10, 10).build().perform();
        }

    }

    /**
     * To click on Size guide navigation dropdown to sub category
     *
     * @throws Exception -
     */
    public void clickOnSizeGuideNavigationDrpDown() throws Exception {
        BrowserActions.clickOnElementX(drpSizeGuideNavigation, driver, "Size guide Navigation dropdown");
        Utils.waitForElement(driver, drpSizeGuide);
    }

    /**
     * To choose a size guide in the modal popup
     *
     * @param guideToBeSelected -
     * @throws Exception -
     */
    public void clickOnSizeGuide(String guideToBeSelected) throws Exception {
		/*for (WebElement ele : lstSizeGuideNavigation) {
			if (ele.getText().trim().equalsIgnoreCase(guideToBeSelected)) {
				BrowserActions.clickOnElementX(ele, driver, "Size Guide : " + guideToBeSelected);
				break;
			}
		}*/
        WebElement element = driver.findElement(By.xpath("//div[@class='size-guide-navigation']//a[contains(text(),'" + guideToBeSelected + "')]"));
        BrowserActions.clickOnElementX(element, driver, guideToBeSelected + " Size Guide Option");
    }

    /**
     * To verify if the selected size guide is highlighted
     *
     * @param sizeGuide -
     * @return boolean
     * @throws Exception - Exception
     */
    public Boolean verifyIfSelectedSizeGuideHighlighted(String sizeGuide) throws Exception {
        String selectedNavigation;
        if (Utils.isMobile()) {
            selectedNavigation = BrowserActions.getText(driver, selectedNavigationCategoryMobile, "Size Guide");
            if (selectedNavigation.trim().equalsIgnoreCase(sizeGuide)) {
                if (runBrowser.equals("MicrosoftEdge"))
                    return Utils.verifyCssPropertyForElement(selectedNavigationCategoryMobile, "color",
                            "rgb(236, 1, 140)");
                else
                    return Utils.verifyCssPropertyForElement(selectedNavigationCategoryMobile, "color",
                            "rgba(236, 1, 139, 1)");
            }
        } else {
            selectedNavigation = BrowserActions.getText(driver, selectedNavigationCategoryDesktop, "Size Guide");
            if (selectedNavigation.trim().equalsIgnoreCase(sizeGuide)) {
                if (runBrowser.equals("MicrosoftEdge"))
                    return Utils.verifyCssPropertyForElement(selectedNavigationCategoryDesktop, "color",
                            "rgb(236, 1, 140)");
                else
                    return elementLayer.verifyCssPropertyForPsuedoElement("selectedNavigationCategoryDesktop", "after", "color", "", new PdpPage(driver).get());
                //"rgba(236, 1, 140, 1)");
            }
        }
        // }
        return false;
    }

    /**
     * To verify if the size guide link is highlighted on hover
     *
     * @param sizeGuideToHover -
     * @return boolean
     * @throws Exception - Exception
     */

    public String verifyIfSizeGuideLinkHighlightedOnHover(String sizeGuideToHover) throws Exception {
        WebElement element = driver.findElement(By.xpath("//div[@class='size-guide-navigation']//a[contains(text(),'" + sizeGuideToHover + "')]"));
        BrowserActions.mouseHover(driver, element, "size guide");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        String actualValue = (String) executor.executeScript("return window.getComputedStyle(arguments[0], ':after').getPropertyValue('border')", element);
        Log.event("----"+actualValue);
        return actualValue;
    }

    /**
     * To Expand or Collapse the Brand Menu Toggle
     *
     * @param state - To Open or close --- 'expand'(To Open), 'collapse'(To Close)
     * @throws Exception - Exception
     */
    public void clickOnBrandMenuToggle(String state) throws Exception {
        if (state.equalsIgnoreCase("expand")) {
            if (drpBrandMenu.getAttribute("class").equals("brand-dropdown_JSMenu")) {
                WebElement element = drpBrandMenu.findElement(By.cssSelector("i[class='fa fa-angle-down']"));
                BrowserActions.clickOnElementX(element, driver, "Toggle down");
            }
        } else if (state.equalsIgnoreCase("collapse")) {
            if (drpBrandMenu.getAttribute("class").equals("brand-dropdown_JSMenu is-active")) {
                WebElement element = drpBrandMenu.findElement(By.cssSelector("i[class='fa fa-angle-up']"));
                BrowserActions.clickOnElementX(element, driver, "Toggle Up");
            }
        }
    }

    /**
     * to verify if the monogramming checkbox selected or not
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyMonogrammingCheckboxSelected() throws Exception {
        if (chkEnableMonogramming.isSelected())
            return true;
        else
            return false;
    }

    /**
     * To check/uncheck monogramming checkbox
     *
     * @param state - To Open or close --- 'enable', 'disable'
     * @throws Exception - Exception
     */
    public void clickOnMonogrammingCheckbox(String state) throws Exception {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].style.opacity=1", chkEnableMonogramming);
        BrowserActions.scrollInToView(chkEnableMonogramming, driver);
        if (state.equalsIgnoreCase("enable")) {
            if (chkEnableMonogramming.isSelected() == false) {
                BrowserActions.javascriptClick(chkEnableMonogramming, driver, "Enable Monogramming Checkbox");
            }
        } else if (state.equalsIgnoreCase("disable")) {
            if (chkEnableMonogramming.isSelected() == true) {
                BrowserActions.javascriptClick(chkEnableMonogramming, driver, "Disable Monogramming Checkbox");
            }
        }
    }

    /**
     * To check/uncheck monogramming checkbox in productset
     *
     * @param ProductID -
     * @param state     -
     * @throws Exception -
     */
    public void clickOnMonogrammingCheckboxProdset(String ProductID, String state) throws Exception {
        WebElement monogramcb = driver.findElement(
                By.cssSelector("div[id$='" + ProductID + "'] div[class^='monogramming-enabled'] .checkbox-content"));
        BrowserActions.scrollInToView(monogramcb, driver);
        if (state.equalsIgnoreCase("enable")) {
            if (monogramcb.isSelected() == false) {
                BrowserActions.clickOnElementX(monogramcb, driver, "Enable Monogramming Checkbox");
            }
        } else if (state.equalsIgnoreCase("disable")) {
            if (monogramcb.isSelected() == true) {
                BrowserActions.clickOnElementX(monogramcb, driver, "Disable Monogramming Checkbox");
            }
        }
    }

    /**
     * to verify that the checkbox is displayed
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyMonogrammingCheckboxDisplayed() throws Exception {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].style.opacity=1", chkEnableMonogramming);
        return Utils.waitForElement(driver, chkEnableMonogramming, 2);

    }

    /**
     * to verify that the hemming checkbox is displayed
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyHemmingCheckboxDisplayed() throws Exception {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].style.opacity=1", chkProductHemming);
        return Utils.waitForElement(driver, chkProductHemming, 2);

    }

    /**
     * to verify that the checkbox is displayed to the left of messaging
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyMessagingDisplayedRightOfMonogrammingCheckbox() throws Exception {
        return BrowserActions.verifyHorizontalAllignmentOfElements(driver, txtMonogramMessaging, chkEnableMonogramming);
    }

    /**
     * to verify that the tips link is displayed to the right of monogramming
     * title
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyTipsDisplayedRightOfMonogrammingTitle() throws Exception {
        return BrowserActions.verifyHorizontalAllignmentOfElements(driver, lnkMonogrammingTips, txtMonogrammingTitle);
    }

    /**
     * to verify if the monogramming options drawer displayed or not
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyMonogrammingOptionsDrawerDisplayed() throws Exception {
        if (divMonogrammingOptions.findElement(By.cssSelector("div.monogramming-fields.hide")).getAttribute("style")
                .contains("block"))
            return true;
        else
            return false;
    }

    /**
     * To click on tips link
     *
     * @throws Exception - Exception
     */
    public void clickOnTipsInMonogrammingSection() throws Exception {
        BrowserActions.clickOnElementX(lnkMonogrammingTips, driver, "Tips link");
        Utils.waitForElement(driver, flytTips);
    }

    /**
     * To click on close button on the tips flyout
     *
     * @throws Exception - Exception
     */
    public void clickOnTipsFlyoutCloseButton() throws Exception {
        BrowserActions.clickOnElementX(btnCloseTipsFlyout, driver, "Close button");
    }

    /**
     * To click on Add to bag button in PDP
     * <br> To be used as a dumb clicking method
     * <br> Use in script or call from other add to bag methods
     * @throws Exception
     */
    public void clickAddProductToBag() throws Exception {
        String productType = getProductType();
        WebElement elem = null;
        Log.event("Product type:: " + productType);
        
        if (productType.equals("standardproductset")) {
            elem = btnAddAllToBag;
        } else if (productType.equals("specialproductset")) {
        	elem = btnAddToBagPrdSet;
        } else {
        	elem = btnAddToBag;
        }

        BrowserActions.scrollToViewElement(elem, driver);
        Utils.waitForElement(driver, elem);
        BrowserActions.clickOnElementX(elem, driver, "Add To Cart Button ");
        
        waitForSpinner();
        if(Utils.waitForElement(driver, mdlMiniCartOverLay) && !Utils.waitForElement(driver, divATBOverlayContent)) {
        	Log.message("ATB overlay content not loaded properly.", driver);
        }
    }

    /**
     * To click add product to bag button
     * @throws Exception
     */
    public int getAddedQtyInCart() throws Exception {
        String value = BrowserActions.getText(driver, lblQtyAdded, "Qty Added");
        return Integer.parseInt(value);
    }

    /**
     * To add product set to bag
     * @throws Exception
     */
    public void clickAddProductToBagInProductSet() throws Exception {
        if(Utils.waitForElement(driver, btnAddToBagSplPrdSet)) {
        	BrowserActions.scrollToViewElement(btnAddToBagSplPrdSet, driver);
            BrowserActions.clickOnElementX(btnAddToBagSplPrdSet, driver, "Add To Cart Button ");
            Utils.waitForPageLoad(driver);
        }
    }
    
    /**
     * To add below product in product set
     * @throws Exception
     */
    public void clickChooseItemsBelowInProductSet() throws Exception {
        if(Utils.waitForElement(driver, chooseItemsBelowProductSetBHDeskMobile)) {
        	BrowserActions.scrollToViewElement(chooseItemsBelowProductSetBHDeskMobile, driver);
            BrowserActions.clickOnElementX(chooseItemsBelowProductSetBHDeskMobile, driver, "Choose Items Below ");
            Utils.waitForPageLoad(driver);
        }
    }

     /**
     * to select value in Font dropdown
     *
     * @param index -
     * @return String
     * @throws Exception - Exception
     */
    public String selectMonogrammingFontValue(int index) throws Exception {
        WebElement option = drpFontSelect.findElements(By.cssSelector("option")).get(index);
        String dataToBeReturned = option.getText().trim();
        BrowserActions.selectDropdownByIndex(drpFontSelect, index);
        return dataToBeReturned;
    }

    /**
     * to select value in location dropdown
     *
     * @param index -
     * @return String
     * @throws Exception - Exception
     */
    public String selectMonogrammingLocationValue(int index) throws Exception {
        WebElement option = drpLocationSelect.findElements(By.cssSelector("option")).get(index);
        String dataToBeReturned = option.getText().trim();
        BrowserActions.selectDropdownByIndex(drpLocationSelect, index);
        return dataToBeReturned;
    }

    /**
     * to select value in Color dropdown
     *
     * @param index -
     * @return String
     * @throws Exception - Exception
     */
    public String selectMonogrammingColorValue(int index) throws Exception {
        WebElement option = drpColorSelect.findElements(By.cssSelector("option")).get(index);
        String dataToBeReturned = option.getText().trim();
        BrowserActions.selectDropdownByIndex(drpColorSelect, index);
        return dataToBeReturned;
    }

    /**
     * to type in the Text field
     *
     * @param txtToType -
     * @throws Exception -
     */
    public void typeInTextFieldFormonogramming(String txtToType) throws Exception {
        BrowserActions.typeOnTextField(cmbSixCharCount, txtToType, driver, "Text field");
    }

    /**
     * to type in the Text field
     *
     * @param characterCount -
     * @return String
     * @throws Exception - Exception
     */
    public String typeInTextFieldFormonogramming(int characterCount) throws Exception {
        String maxCharacterCount = txtMaximumCharacterCount.getText().split(" ")[0];
        maxCharacterCount = maxCharacterCount.replace("/", "");
        String randomString = RandomStringUtils.randomAlphabetic(characterCount);
        textField.findElement(By.cssSelector("input")).sendKeys(randomString);
        return randomString;
    }

    /**
     * to get number of characters typed in the Text field
     *
     * @return number of character in text field
     * @throws Exception - Exception
     */
    public int getNumberOfCharactersInTextField() throws Exception {
        String value = textField.findElement(By.cssSelector("input")).getAttribute("maxlength").replace(".0", "");
        int characterCount = Integer.parseInt(value);
        return characterCount;
    }

    /**
     * to get the actual character count
     *
     * @return String
     * @throws Exception - Exception
     */
    public String getActualCharacterCount() throws Exception {
        return BrowserActions.getText(driver, txtActualCharacterCount, "Actual Character Count").trim();
    }

    /**
     * to get the max character count
     *
     * @return String
     * @throws Exception - Exception
     */
    public String getMaximumCharacterCount() throws Exception {
        String maxCharacterCount = txtMaximumCharacterCount.getText().split(" ")[0];
        maxCharacterCount = maxCharacterCount.replace("/", "");
        return maxCharacterCount.trim();
    }
    
    /**
     * To get window size to show alt image thumbnails 
     * @return slick windows size
     * @throws Exception
     */
    public int getSlickWindowWidth() throws Exception{
    	BrowserActions.scrollInToView(divSlickWindowMobile, driver);
    	int width = divSlickWindowMobile.getSize().width;
    	return width;
    }

    /**
     * To check the selected image is populated in place of main product image.
     * @param imgToClick -
     * @return boolean -
     * @throws Exception -
     */
    public boolean verifyNewProdImageLoaded(int imgToClick) throws Exception {
    	clickOnSpecifiedAlternateImage(imgToClick);
    	if(Utils.isMobile()) {
    		int slickWidth = getSlickWindowWidth();
    		String[] slickValues = divAlternateImages.getCssValue("transform").split("\\(")[1].split("\\)")[0].split("\\,");
    		for(String slickValue : slickValues) {
    			int value = com.fbb.support.StringUtils.getNumberInString(slickValue.replace("-", ""));
    			if(value == (slickWidth * (imgToClick-1))) {
    				return true;
    			}
    		}
    		return false;
    	}else {
    		String[] mainImgSourceSplit = mainProdImage.getAttribute("src").trim().split("\\/");
            String mainImgSource = mainImgSourceSplit[mainImgSourceSplit.length - 1];
        	
            String[] selectImgSourceSplit = lstProductThumbnaillink.get(imgToClick - 1).findElement(By.cssSelector("img")).getAttribute("data-lgimg").split("\\/");
            String selectImgSource = selectImgSourceSplit[selectImgSourceSplit.length - 1].replace("\"}", "");
            
            if (mainImgSource.contains(selectImgSource)) {
                return true;
            }
            return false;
    	}
    }

    /**
     * To click on video link
     *
     * @throws Exception -
     */
    public void clickMainVideoLink() throws Exception {
        if (!Utils.isMobile()) {
            BrowserActions.javascriptClick(lnkVideoIcon_Des_Tab, driver, "Main Video link ");
        } else {
            BrowserActions.javascriptClick(lnkVideoIcon_Mob, driver, "Main Video link ");
        }
    }

    /**
     * to get the text in Preview field
     *
     * @return String -
     * @throws Exception - Exception
     */
    public String getPreviewText() throws Exception {
        return txtMonogramPreview.getAttribute("value").replace("\n", "").trim();
    }

    /**
     * To click on the refresh button in the monogram preview
     *
     * @throws Exception -
     */
    public void clickOnRefreshMonogramLink() throws Exception {
        BrowserActions.javascriptClick(lnkRefreshMonogramPreview, driver, "Refresh Monogram Link");
        Utils.waitForPageLoad(driver);
    }

    /**
     * To check the font is changing in the preview as we select a different font
     *
     * @return boolean -true if font changes, else false
     * @throws Exception -
     */
    public boolean checkFontChangingInMonogramPreview() throws Exception {
        clickOnMonogrammingCheckbox("disable");
        clickOnMonogrammingCheckbox("enable");
        selectMonogrammingFontValue(1);
        clickOnRefreshMonogramLink();
        String fontValueBefore = BrowserActions.getTextFromAttribute(driver, txtMonogramPreview, "style", "Monogram preview");
        System.out.println("Font- --- --- --- - " + fontValueBefore);
        if (fontValueBefore.isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * To check the color is changing in the preview as we select a different font
     *
     * @return boolean - true if color changes, else false
     * @throws Exception -
     */
    public boolean checkColorChangingInMonogramPreview() throws Exception {
        clickOnMonogrammingCheckbox("disable");
        clickOnMonogrammingCheckbox("enable");
        selectMonogrammingColorValue(1);
        clickOnRefreshMonogramLink();
        String colorValueBefore = BrowserActions.getTextFromAttribute(driver, txtMonogramPreview, "style", "Monogram preview");
        System.out.println("Font- --- --- --- - " + colorValueBefore);
        if (colorValueBefore.isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * To check the location alignment is changing in the preview as we select a different font
     *
     * @return boolean true if location changes, else false
     * @throws Exception -
     */
    public boolean checkLocationChangingInMonogramPreview() throws Exception {
        clickOnMonogrammingCheckbox("disable");
        clickOnMonogrammingCheckbox("enable");
        selectMonogrammingLocationValue(1);
        clickOnRefreshMonogramLink();
        String locationValueBefore = BrowserActions.getTextFromAttribute(driver, txtMonogramPreview, "style", "Monogram preview");
        if (locationValueBefore.isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * to get the text in Preview field
     *
     * @return String
     * @throws Exception - Exception
     */
    public String getMonogrammingTextFieldValue() throws Exception {

        return textField.findElement(By.cssSelector("input")).getAttribute("value");
    }

    /**
     * To get number of alternate images available in slick slide
     *
     * @return Integer -
     * @throws Exception -
     */
    public int getNoOfThumbnailProdImages() throws Exception {
        try {
            int NoOfThumbnails = lstProductThumbnaillink.size();
            return NoOfThumbnails;
        } catch (Exception e) {
            return 0;
        }
    }
    
    /**
     * To get number of alternate images available in slick slide
     *
     * @return Integer -
     * @throws Exception -
     */
    public String getProdImageName() throws Exception {
    	WebElement elem = null;
    	if(Utils.isMobile()) {
    		elem = prodImage_Mobile;
    	} else {
    		elem = prodImage_Desk_Tab;
    	}
    	String mainImgProd = BrowserActions.getTextFromAttribute(driver, elem, "src", "Product Image");
		String[] imgs = mainImgProd.split("\\?")[0].split("\\/");
		Log.event("String from element :" +imgs[imgs.length - 1]);
		return imgs[imgs.length - 1];
    }
    
    
    /**
     *To get the recently viewed product image 
     * @return Product image name
     * @throws Exception
     */
	public String getRecentlyViewedSectionProdImageName() throws Exception {
		BrowserActions.scrollInToView(lastVisitedSection, driver);
		for (WebElement element : recentlyViewedProdImage) {
			String mainImgProd = BrowserActions.getTextFromAttribute(driver, element, "src", "Product Image");
			String[] imgs = mainImgProd.split("\\?")[0].split("\\/");
			return imgs[imgs.length - 1];
		}
		return "";
	}
    
    /**
     *To get the recenlty viewed section all product image name 
     * @return List of products in Recently Viewed
     * @throws Exception
     */
	public List<String> getRecentlyViewedsectionAllProdImageName() throws Exception {
		List<String> prdNames = new ArrayList<String>();
		for (int i = 0; i < recentlyViewedProdName.size(); i++) {
			prdNames.add(BrowserActions.getText(driver, recentlyViewedProdName.get(i), "Product name"));
		}
		return prdNames;
	}
    
    /**
     * To verify main and alt image initial mapping
     * @return true/false if main and first alt-images are mapped correctly
     */
    public boolean verifyInitalImageMapping() throws Exception{
		String firstAltImgSrc[] = alternateImages.get(0).getAttribute("src").split("\\/");
		
		String mainImgsrc = getProdImageName().split("\\.")[0].split("_m")[0];
		String firstAltImgsrc = firstAltImgSrc[firstAltImgSrc.length-1].split("\\.")[0].split("_m")[0];
		
		Log.event("Main Image Code :: " + mainImgsrc);
		Log.event("First Alt Image Code :: " + firstAltImgsrc);
		if(mainImgsrc.contains("noimagefound") && firstAltImgsrc.contains("noimagefound")) {
			Log.warning("Image or data feed not configured.");
			return true;
		}
		
		return mainImgsrc.equals(firstAltImgsrc);
	}

    /**
     * To check the alternate image is displayed fully/not when the image is clicked in slick slider.
     *
     * @param imgToVerify    -
     * @param statusToVerify -
     * @return boolean -
     * @throws Exception -
     */
    public boolean verifyAlternateImageDisplayStatus(int imgToVerify, String statusToVerify) throws Exception {
        String displayStatus = lstAlternateImgs.get(imgToVerify - 1).getAttribute("aria-hidden");
        if (displayStatus.equals(statusToVerify)) {
            return true;
        }
        return false;
    }
    
    /**
     * To verify the alternate image is displayed in main image area.
     *
     * @param altImageIndex - Index of alternate images
     * @return boolean - True if alternate image is displayed in main image area, else false.
     * @throws Exception
     */
    public boolean verifySelectedAlternateImageDisplayed(int altImageIndex) throws Exception {
        Boolean status = false;
        WebElement displayAltImage;
        if(!Utils.isMobile()) {
            displayAltImage = prodSelectedAltImageDeskAndTab;
        } else {
            displayAltImage = prodSelectedAltImageMob;
        }
        String displayImgPath[] = BrowserActions.getTextFromAttribute(driver, displayAltImage, "src", "Alternate image").trim().split("\\/");
        String displayImgCode = displayImgPath[displayImgPath.length - 1].split(".jpg")[0].replace(".jpg", "");
        
        List<WebElement> alternateImage = alternateImages;
        String altImgPath[] = BrowserActions.getTextFromAttribute(driver, alternateImage.get(altImageIndex), "src", "Alternate image").trim().split("\\/");
        String altImgCode = altImgPath[altImgPath.length - 1].split(".jpg")[0].replace(".jpg", "");
        
        if(displayImgCode.equalsIgnoreCase(altImgCode)) {
            status = true;
        }
        return status;
    }
    
    /**
     * To get number of alternate images in the Pdp Page
     * @return intValue - no of alternate image
     * @throws Exception - Exception
     */
    public int getNoOfAlternateImage() throws Exception {
    	if(!Utils.isMobile()) {
    		return lstProductAltThumbnail.size();
    	} else {
    		return btnSlickDots.size();
    	}
    }
    
    /**
     * To get number of alternate images in the Pdp Page
     * @return intValue - no of alternate image
     * @throws Exception - Exception
     */
    public int getNoOfRecommendationProduct() throws Exception {

    	if(BrandUtils.isBrand(Brand.bh)) {
    		return prodNameListRecommendationD_BH.size();
    	} else {
    		if(Utils.isDesktop()) {
    			return prodNameListRecommendationDesktop.size();
    		} else {
    			return prodNameListRecommendationMobileTablet.size();
    		}
    	}
    }

    /**
     * To click on slick slide left/right arrows
     * @param direction - 'prev' / 'next'
     * @throws Exception -
     */
    public void scrollAlternateImageInSpecifiedDirection(String direction) throws Exception {
        if (direction.trim().equalsIgnoreCase("Next")) {
            while (!(Utils.waitForElement(driver, btnNextImageDisable))) {
                BrowserActions.clickOnElementX(btnNextImageEnable, driver, "Next Arrow in Alternate product image ");
            }
        } else if (direction.trim().equalsIgnoreCase("Prev")) {
            while (!(Utils.waitForElement(driver, btnPrevImageDisable))) {
                BrowserActions.clickOnElementX(btnPrevImageEnable, driver, "Prev Arrow in Alternate product image ");
            }
        }
    }
    
    /**
     * To click on slick slide left/right arrows
     * @param direction - 'top' to click above arrow/ 'next' to click below arrow
     * @throws Exception -
     */
    public void scrollRecommendationImageInSpecifiedDirection(String direction) throws Exception {
    	WebElement nextEnable = null;
    	WebElement nextDisable = null;
    	WebElement prevEnable = null;
    	WebElement prevDisable = null;
    	
    	if(BrandUtils.isBrand(Brand.bh)) {
    		nextEnable = nextArrowRecommendationBH;
    		nextDisable = nextArrowDisableRecommendationBH;
    		prevEnable = prevArrowRecommendationBH;
    		prevDisable = prevArrowDisableRecommendationBH;
    	} else {
    		if (Utils.isDesktop()) {
    			nextEnable = nextArrowRecommendationDesktop;
        		nextDisable = nextArrowDisableRecommendationDesktop;
        		prevEnable = prevArrowRecommendationDesktop;
        		prevDisable = prevArrowDisableRecommendationDesktop;
    		} else {
    			nextEnable = nextArrowRecommendationMobileTablet;
        		nextDisable = nextArrowDisableRecommendationMobileTablet;
        		prevEnable = prevArrowRecommendationMobileTablet;
        		prevDisable = prevArrowDisableRecommendationMobileTablet;
    		}
    	}
    	
        if (direction.trim().equalsIgnoreCase("Next")) {
        	while (!(Utils.waitForElement(driver, nextDisable))) {
        		BrowserActions.clickOnElementX(nextEnable, driver, "Next Arrow in Alternate product");
        	}
        } else if (direction.trim().equalsIgnoreCase("Prev")) {
        	while (!(Utils.waitForElement(driver, prevDisable))) {
        		BrowserActions.clickOnElementX(prevEnable, driver, "Prev Arrow in Recommendation product");
        	}
        }
    }

    /**
     * To click on alternate image based on index
     *
     * @param int - # of alt image to select
     * @throws Exception - Exception
     */
    public void clickOnSpecifiedAlternateImage(int imgToClick) throws Exception {
    	try {
	    	if(!Utils.isMobile()) {
	    		WebElement clickImg = lstProductThumbnaillink.get(imgToClick - 1).findElement(By.cssSelector("img"));
	    		BrowserActions.javascriptClick(clickImg, driver, "Alternate product image ");
	    	}else {
	    		WebElement clickImg = btnSlickDots.get(imgToClick-1).findElement(By.cssSelector("button"));
	    		BrowserActions.clickOnElementX(clickImg, driver, "Alternate product image ");
	    	}
    	} catch(IndexOutOfBoundsException | NoSuchElementException ex) {
        	Log.failsoft("Alternate images may not be available or specified number of alternate images are not available", driver);
        }
    }

    /**
     * To get regular price of the product
     * @return String - Regular price
     * @throws Exception - Exception
     */
    public String getRegularPrice() throws Exception {
        return BrowserActions.getText(driver, lblRegularPrice, "Regular price").trim();
    }

    /**
     * To get sale price of the product
     * @return String - Sales price
     * @throws Exception - Exception
     */
    public String getSalePrice() throws Exception {
    	String salesPrice = BrowserActions.getText(driver, lblSalePrice, "Sale price").trim();
        Log.event("Sale Price :: " + salesPrice);
        return salesPrice;
    }

    /**
     * To get original price(Single price) when mark down price present
     *
     * @return String - price
     * @throws Exception -
     */
    public String getOriginalPrice() throws Exception {
        Log.event("lblOriginalPriceInSalePrice.getText().trim()==" + lblOriginalPriceInSalePrice.getText().trim());
        String priceToReturn = lblOriginalPriceInSalePrice.getText().trim();
        if (priceToReturn.contains("-"))
            priceToReturn = priceToReturn.split("\\-")[0];

        return priceToReturn.trim();
    }

    /**
     * To get original price(price range) when mark down price present
     *
     * @return String - price range
     * @throws Exception -
     */
    public String getOriginalPrice1() throws Exception {
        Log.event("lblOriginalPriceInSalePrice.getText().trim()==" + lblOriginalPriceInSalePrice.getText().trim());
        String priceToReturn = lblOriginalPriceInSalePrice.getText().trim();

        return priceToReturn.trim();
    }

    /**
     * To get original price range
     *
     * @return String - price range
     * @throws Exception -
     */
    public String getOriginalPriceRange() throws Exception {
        String priceToReturn = lblOriginalPriceInSalePrice.getText().trim();
        Log.event("Original Price Range :: " + priceToReturn);
        return priceToReturn.trim();
    }

    /**
     * To get regular price range of the product
     *
     * @return String - price range
     * @throws Exception -
     */
    public String getRegularPriceRange() throws Exception {
        String priceToReturn = lblRegularPriceRange.getText().trim();
        Log.event("Regular Price Range :: " + priceToReturn);
        return lblRegularPriceRange.getText().trim();
    }

    /**
     * To get product availability status
     *
     * @return String -
     * @throws Exception -
     */
    public String getProdAvailStatus() throws Exception {
        return txtProdAvailStatus.getText().trim();
    }

    /**
     * To get selected color variant name
     *
     * @return String - color
     * @throws Exception -
     */
    public String getSelectedColorVariant() throws Exception {
    	return BrowserActions.getText(driver, selectedColorVariant, "Selected color");
    }

    /**
     * to verify size swatches are updated based on color selection
     * @return boolean
     * @throws Exception - Exception
     */
	public boolean verifySizeswatchesUpdatedOrNot() throws Exception {
		int selectable = 0, unselectable = 0;
		if (lstSize.size() > 0)
			selectable = lstSize.size();
		else if (lstShoeSizeSwatchesSelectable.size() > 0)
			selectable = lstShoeSizeSwatchesSelectable.size();
		if (lstSizeSwatchesUnSelectable.size() > 0)
			unselectable = lstSizeSwatchesUnSelectable.size();
		else if (lstShoeSizeSwatchesUnSelectable.size() > 0)
			unselectable = lstShoeSizeSwatchesUnSelectable.size();
		if ((selectable > 0) || (unselectable > 0)) {
			int size = 0;
			if (lstSizeSwatches.size() > 0)
				size = lstSizeSwatches.size();
			else
				size = lstShoeSizeSwatches.size();
			Log.message("Total available size swatches are: " + size + ". Selectable Sizes are: " + selectable
					+ " and Unselectable sizes are: " + unselectable);
			if (selectable + unselectable == size) {
				return true;
			}
		} else {
			int selectableBandSize = lstBraSizeSwatchesSelectable.size();
			int unselectableBandSize = lstBraSizeSwatchesUnSelectable.size();
			int noOfbandSizes = lstBraSizeSwatches.size();

			int selectableCupSizes = lstBraCupSizeSwatchesSelectable.size();
			int unselectableCupSize = lstBraCupSizeSwatchesUnselectable.size();
			int noOfCupSizes = lstBraCupSizeSwatches.size();

			Log.message("Total available size swatches are: " + noOfbandSizes + ":" + noOfCupSizes
					+ ". Selectable Sizes are: " + selectableBandSize + ":" + selectableCupSizes
					+ " and Unselectable sizes are: " + unselectableBandSize + ":" + unselectableCupSize);

			if ((selectableBandSize + unselectableBandSize == noOfbandSizes)
					&& (selectableCupSizes + unselectableCupSize == noOfCupSizes)) {
				return true;
			}
		}
		return false;
    }
    
    /**
     * To verify color swatches are updated or not
     * @return boolean
     * @throws Exception
     */
    
    public boolean verifyColorSwatchesUpdation() throws Exception{
	    int totalswatches=lstColorSwatches.size();
	    int colorsAvailable= lstColorSelectable.size();
	    int colorsUnavailable= lstColorUnSelectable.size();
	    	
	    Log.message("Total colorSwacthes are: "+totalswatches +" Available Color swatches are: "+colorsAvailable 
	    		+" Unavailable color swatches are: " +colorsUnavailable);
	    if((colorsAvailable+colorsUnavailable) == totalswatches){
	    	return true;
	    }
	    if(Utils.waitForElement(driver, selectedColor))	{
	    	int colorSwatches = colorsAvailable+colorsUnavailable+1;
	    	if(colorSwatches == totalswatches) {
	    		return true;
	    	}
	    }
	    return false;
    }
    
    /**
     * To verify active sizes are availeble in PDP
     * @return boolean
     * @throws Exception - Exception
     */
	public boolean verifyActiveSizeSwatchesAvailability() throws Exception {
		try {
			int size = lstSizeSwatchesUnSelectable.size();
			if (size > 0)
				return true;
		} catch (NoSuchElementException e) {
			Log.message("Active size swatches are not available");
		}
		return false;
	}
    
    /**
     * To verify greyed out sizes are availeble in PDP
     * @return boolean
     * @throws Exception - Exception
     */
	public boolean verifygryedOutSizeSwatchesAvailability() throws Exception {
		try {
			int size = lstSizeSwatchesUnSelectable.size();
			if (size > 0)
				return true;
		} catch (NoSuchElementException e) {
			Log.message("Out of stock size swatches are not available");
		}
		return false;
	}
    
    /**
     * To verify greyed out sizes are availeble in PDP
     * @return boolean
     * @throws Exception - Exception
     */
	public boolean verifygryedOutColorSwatchesAvailability() throws Exception {
		try {
			int size = lstColorUnSelectable.size();
			if (size > 0)
				return true;
		} catch (NoSuchElementException e) {
			Log.message("Out of stock size swatches are not available");
		}
		return false;
	}
    
    /**
     * to verify Out of stock size
     *
     * @param textToVerify -
     * @return String
     * @throws Exception - Exception
     */
    public boolean verifyOutOfStockSize(String textToVerify) throws Exception {
        int size = lstSizeSwatchesUnSelectable.size();
        String elmSizeTxt = new String();
        WebElement elm;
        for (int i = 0; i < size; i++) {
            elm = lstSizeSwatchesUnSelectable.get(i);
            elmSizeTxt = BrowserActions.getText(driver, elm, "Size");
            if (elmSizeTxt.equals(textToVerify)) {
                return true;
            }
        }
        return false;
    }

    /**
     * To get product availability status in Add to bag overlay
     *
     * @return String -
     * @throws Exception -
     */
    public String getProdAvailStatusInMCOverlay() throws Exception {
        return lblProductAvailInMCOverLay.findElements(By.tagName("span")).get(1).getText().trim();
    }

    /**
     * to verify product Availabilty status
     *
     * @param prodStatus      -
     * @param prodStatusInput -
     * @return boolean
     * @throws Exception -
     */
    public boolean verifyProdAvailabilityStatus(String prodStatus, String prodStatusInput) throws Exception {
        prodStatus = prodStatus.trim();
        int endWord = prodStatus.indexOf("left");
        if (prodStatus.startsWith("Only")) {
            String strReplace = prodStatus.substring(5, endWord - 1);
            prodStatus = prodStatus.replaceAll(strReplace, "X");
        }
        int strLen = prodStatus.length();
        if (prodStatus.contains("Expected")) {
            String strReplace = prodStatus.substring(strLen - 15, strLen);
            prodStatus = prodStatus.replaceAll(strReplace, "X");
        }

        if (prodStatus.trim().equalsIgnoreCase(prodStatusInput.trim())) {
            return true;
        } else
            return false;
    }

    /**
     * to verify the selected color Image for loaded special product set
     *
     * @param productIndex  -
     * @param colorToVerify -
     * @return boolean
     * @throws Exception - Exception
     */
    public boolean verifyTheSelectedColorImageIsLoadedSplProductSet(
    		String colorToVerify, int productIndex) throws Exception {
        WebElement product = lstSplProductSetProds.get(productIndex);
        String imageDetails = product.findElement(
        			By.cssSelector(".product-set-image:nth-of-type(" + productIndex + ") .product-set-product-image img"))
                	.getAttribute("alt");
        if (imageDetails.contains(colorToVerify)) {
            return true;
        }
        return false;
    }

    /**
     * to verify total price of product set
     *
     * @return String
     * @throws Exception - Exception
     */
    public boolean verifyTotalPriceOfProductSet() throws Exception {
        int NoOfProduct = lstProductSetItemPrices.size();
        int intCount;
        float minPrice = 0, maxPrice = 0, minProductSetPrice, maxProductSetPrice, totalPrice = 0,
                totalProductSetPrice = 0;
        String[] productItemPrice;
        String productSalesPrice;
        String productSetSalesPrice;
        for (intCount = 0; intCount < NoOfProduct; intCount++) {
            productSalesPrice = lstProductSetItemPrices.get(intCount).findElement(By.cssSelector("span.price-sales"))
                    .getText();
            if (productSalesPrice.contains("-")) {
                productItemPrice = productSalesPrice.split("\\-");
                minPrice = minPrice + Float.parseFloat(productItemPrice[0].trim().substring(1));
                maxPrice = maxPrice + Float.parseFloat(productItemPrice[1].trim().substring(1));
            } else {
                totalPrice = totalPrice + Float.parseFloat(productSalesPrice.trim().substring(1));
            }
        }
        productSetSalesPrice = lstProductSetPrices.get(0).findElement(By.cssSelector("span.price-sales")).getText();
        if (productSetSalesPrice.contains("-")) {
            productItemPrice = productSetSalesPrice.split("\\-");
            minProductSetPrice = Float.parseFloat(productItemPrice[0].trim().substring(1));
            maxProductSetPrice = Float.parseFloat(productItemPrice[1].trim().substring(1));
            if (maxProductSetPrice == maxPrice && minProductSetPrice == minPrice) {
                return true;
            }
        } else {
            totalProductSetPrice = Float.parseFloat(productSetSalesPrice.trim().substring(1));
            if (totalPrice == totalProductSetPrice) {
                return true;
            }
        }
        return false;
    }

    /**
     * to check size for un selectable
     *
     * @param size -
     * @return String
     * @throws Exception - Exception
     */
    public boolean checkSizeUnSelectable(String size) throws Exception {
        for (int i = 0; i < lstSizeSwatchesUnSelectable.size(); i++) {
            if (lstSizeSwatchesUnSelectable.get(i).getText().trim().equals(size)) {
                return true;
            } else {
                for (i = 0; i < lstSizeSwatchesSelectable.size(); i++) {
                    if (lstSizeSwatchesSelectable.get(i).getText().trim().equals(size)) {
                        BrowserActions.clickOnSwatchElement(lstSizeSwatchesSelectable.get(i), driver, "Size Swatches ");
                    }
                }
            }
        }
        return false;
    }

    /**
     * To select all type of swatches
     * @throws Exception - Exception
     */
    public void selectAllSwatchesForVariationProduct() throws Exception {
        String color = selectColor();
        Log.event("Color Selected : " + color);

        if (Utils.waitForElement(driver, shoeSizeSwatches)) {
            selectshoeSizeWidth();
        }
        String size = null;
        if (isShoeSizeAvailable()) {
            size = selectshoeSizeWidth();
        } else {
            size = selectSize();
        }
        Log.event("Size Selected : " + size);

        String sizeFamily = selectSizeFamily();
        Log.event("Size Family Selected : " + sizeFamily);

        if (isWidthAvailable()) {
            selectWidthBasedOnIndex(0);
        }
    }
    
    /**
     * To select a random size for different types of size options 
     * @return Selected color variant
     * @exception Exception
     */
    public String selectRegularOrSplitSize() throws Exception{
    	String size = "";
    	if (isRegularSizeAvailable()) {
    		size = selectSize();
    		Log.event("Size selected.");
    	} else if (isShoeSizeAvailable()) {
            size = selectshoeSizeWidth();
            if (isWidthAvailable())
                selectWidthBasedOnIndex(0);
            Log.event("Shoe size selected.");
        } else if (isBraSizeAvailable()) {
            size = selectBraBandSize();
            if (isBraCupAvailable()) {
                selectCupBasedOnIndex(0);
            }
            Log.event("Bra size selected.");
        } else {
        	Log.failsoft("No size option found, or not configured.", driver);
        }
    	return size;
    }

    /**
     * to select size in PDP page
     * @param size - (Optional) Name of size to select.
     * <br>Yes/No - (Optional parameter 2) Yes - to select the out-of-stock size
     * <br> No option will select a random size 
     * @return Selected size
     * @throws Exception
     */
    public String selectSize(String... size) throws Exception {
        if ((size.length == 0) || (size.length > 0 && size[0].equals("random"))) {
            if (Utils.waitForElement(driver, selectedSize)) {
            	Log.event("Size is already selected.");
            	return BrowserActions.getText(driver, selectedSize, "Selected Size value");
            }
            
            int index = 0;
            if (lstInStockSizeSwatches.size() > 0) {
            	index = Utils.getRandom(0, lstInStockSizeSwatches.size() - 1);
            	BrowserActions.clickOnSwatchElement(lstInStockSizeSwatches.get(index), driver, "Random size swatch: " + index);
             } else if (lstSizeSwatches.size() > 0) {
            	index = Utils.getRandom(0, lstSizeSwatches.size() - 1);
            	BrowserActions.clickOnSwatchElement(lstSizeSwatches.get(index), driver, "Random size swatch: " + index);
            }
            Utils.waitForPageLoad(driver);
            BrowserActions.scrollInToView(selectedSizeLabel, driver);
            return BrowserActions.getText(driver, selectedSizeLabel, "Selected Size value");

        } else {
        	if(Utils.waitForElement(driver, selectedSize)) {
        		if (BrowserActions.getText(driver, selectedSize, "Selected Size").contains((size[0])))
        			return size[0];
        		else
        			Log.event("Desired size currently not selected.");
        	} else {
        		Log.event("No size currently selected.");
        	}
        	Log.event("Size to select:: " + size[0]);
        	
        	for (int i = 0; i < lstSizeSwatches.size(); i++) {
            	Log.event(lstSizeSwatches.get(i).getText().trim());
            	if (lstSizeSwatches.get(i).getText().replaceAll("\\s+", "").equalsIgnoreCase(size[0].replaceAll("\\s+", ""))) {
                    BrowserActions.clickOnSwatchElement(lstSizeSwatches.get(i), driver, size[0] + " Size Swatch ");
                    Log.event("Clicked On " + size[0] + " Size...");
                    waitForSpinner();
                    return BrowserActions.getText(driver, selectedSize, "Selected Size value");
                }
            }
            for (int i = 0; i < lstSizeSwatchesUnSelectable.size(); i++) {
                if (lstSizeSwatchesUnSelectable.get(i).getText().replaceAll("\\s+", "").equalsIgnoreCase(size[0].replaceAll("\\s+", ""))) {
                    if (size.length == 2 && size[1].equalsIgnoreCase("Yes")) {
                    	BrowserActions.clickOnSwatchElement(lstSizeSwatchesUnSelectable.get(i), driver, "Size Swatches ");
                        Utils.waitUntilElementDisappear(driver, waitLoader);
                        return BrowserActions.getTextFromAttribute(driver, selectedSize, "data-swatchvalue", "Selected Size value");
                    } else {
                        Log.failsoft("--->>> Given Size(" + size[0] + ") <b>Not Available</b> in Size list.");
                        return null;
                    }
                }
            }
        }
        Utils.waitForPageLoad(driver);
        return null;
    }

    /**
     * @param index - index of color to select
     * @return selected color
     * @throws Exception
     */
    public String selectAvailableColorByIndex(int index) throws Exception {
        BrowserActions.scrollToViewElement(lstColorSelectable.get(index - 1), driver);
        BrowserActions.clickOnSwatchElement(lstColorSelectable.get(index - 1), driver, "Color Swatch ");
        Log.event("Clicked On " + index + "th Color...");
        waitForSpinner();
        return BrowserActions.getText(driver, selectedColor, "Selected Color");
    }

    /**
     * To select color in PDP
     * @param color - (Optional parameter 1) Name of color to select.
     * <br>Yes/No - (Optional parameter 2) Yes - to select the out-of-stock color
     * <br>No option will select random color
     * @return Selected color name
     * @throws Exception
     */
    public String selectColor(String... color) throws Exception {
        if ((color.length == 0) || (color.length > 0 && color[0].equalsIgnoreCase("random"))) {
        	if(Utils.waitForElement(driver, selectedColor)) {
        		Log.event("Color is already selected.");
        		return BrowserActions.getText(driver, selectedColorVariant, "Selected color variant name");
        	}
        	if (lstColorConfiguredSelectableInStock.size() > 0) {
        		Log.event(lstColorConfiguredSelectableInStock.size() + " Color configured swatch found on page. Trying to select...");
        		int randomIndex = Utils.getRandom(0, lstColorConfiguredSelectableInStock.size()-1);
        		WebElement elem = lstColorConfiguredSelectableInStock.get(randomIndex).findElement(By.xpath(".."));
        		BrowserActions.clickOnSwatchElement(elem, driver, "Random configured swatch: " + randomIndex);
        	} else if(lstColorConfiguredSelectable.size() > 0) {
        		Log.event(lstColorConfiguredSelectable.size() + " Color configured swatch found on page. Trying to select...");
        		int randomIndex = Utils.getRandom(0, lstColorConfiguredSelectable.size()-1);
        		WebElement elem = lstColorConfiguredSelectable.get(randomIndex).findElement(By.xpath(".."));
        		BrowserActions.clickOnSwatchElement(elem, driver, "Random configured swatch: " + randomIndex);
        	} else {
        		Log.event("Color configured not found on page. Selecting from unconfigured swatch...");
        		Log.event("Number of selecatble color swatches:: " + lstColorSelectable.size());
        		int randomIndex = Utils.getRandom(0, lstColorSelectable.size()-1);
        		WebElement elem = lstColorSelectable.get(randomIndex).findElement(By.xpath(".."));
        		BrowserActions.clickOnSwatchElement(elem, driver, "Random un-configured swatch: " + randomIndex);        		
        	}
        	Utils.waitForPageLoad(driver);
        	return BrowserActions.getText(driver, selectedColorVariant, "Selected color variant name");
        } else {
        	if(Utils.waitForElement(driver, selectedColorVariant)) {
        		if (BrowserActions.getText(driver, selectedColorVariant, "Selected Color").equalsIgnoreCase(color[0].trim()))
        			return color[0];
        		else
        			Log.event("Desired color currently not selected.");
        	} else {
        		Log.event("No color currently selected.");
        	}
        	Log.event("Color to select:: " + color[0]);

            for (int i = 0; i < lstColorSelectable.size(); i++) {
            	WebElement colorSwatch = lstColorSelectable.get(i).findElement(By.xpath(".."));
            	Log.event("Available colors is: " + colorSwatch.getAttribute("aria-label"));
                if (colorSwatch.getAttribute("aria-label").trim().toLowerCase().contains(color[0].trim().toLowerCase())) {
                    BrowserActions.clickOnSwatchElement(colorSwatch, driver, color[0] + " Color Swatch ");
                    Log.event("Clicked On " + color[0] + " Color...");
                    waitForSpinner();
                    return BrowserActions.getText(driver, selectedColorVariant, "Selected color variant name");
                }
            }
            for (int i = 0; i < lstColorUnSelectable.size(); i++) {
                if (lstColorUnSelectable.get(i).getAttribute("title").trim().toLowerCase().contains(color[0].toLowerCase())) {
                	if (color.length == 2 && color[1].equalsIgnoreCase("yes")) {
                    	BrowserActions.clickOnSwatchElement(lstColorUnSelectable.get(i), driver, "Color Swatches ");
                        Utils.waitUntilElementDisappear(driver, waitLoader);
                        return BrowserActions.getText(driver, selectedColorVariant, "Selected color variant name");
                    } else {
                        Log.message("--->>> Given Size(" + color[0] + ") <b>Not Available</b> in Size list. Selecting Random Color...");
                        break;
                    }
                }
            }
            BrowserActions.clickOnSwatchElement(lstColorSelectable.get(0), driver, "Color Swatches ");
            Utils.waitUntilElementDisappear(driver, waitLoader);
            Log.event("Clicked on 1st Color...");
            return BrowserActions.getText(driver, selectedColorVariant, "Selected color variant name");
        }
    }

    /**
     * to select shoe size width
     * @param size - (Optional) Shoe size and width to select
     * <br> No option will select in random
     * @return Selected shoe size width
     * @throws Exception
     */
    public String selectshoeSizeWidth(String... size) throws Exception {
        boolean flag = true;
        if (!(size.length > 0)) {
            if (Utils.waitForElement(driver, selectedshoeSize)) {
            	return BrowserActions.getText(driver, selectedShoeSizeValue, "Selected shoe size value");
            }
        }
        if (size.length > 0) {
        	Utils.waitForElement(driver, selectedshoeSize);

        	for (int i = 0; i < lstShoeSizeSwatchesSelectable.size(); i++) {
        		if (lstShoeSizeSwatchesSelectable.get(i).getText().trim().equals(size[0])) {
        			BrowserActions.clickOnSwatchElement(lstShoeSizeSwatchesSelectable.get(i), driver, "Size Swatches ");
        			Log.event("Clicked On Expected Size...");
        			waitForSpinner();
        			return BrowserActions.getText(driver, selectedShoeSizeValue, "Selected shoe size value");
        		}
        	}
        	for (int i = 0; i < lstShoeSizeSwatchesUnSelectable.size(); i++) {
        		if (lstShoeSizeSwatchesUnSelectable.get(i).getText().trim().equals(size[0])) {
        			Log.message("--->>> Given Size(" + size[0]
        					+ ") <b>Not Available</b> in Size list. Selecting Random Size...");
        			flag = false;
        			break;
        		}
        	}
        	if (flag)
        		Log.message("--->>> Given Size(" + size[0]
        				+ ") <b>Not Applicable</b> in Size list. Selecting Random Size...");
        	int rand = Utils.getRandom(0, lstShoeSizeSwatchesSelectable.size());
        	BrowserActions.clickOnSwatchElement(lstShoeSizeSwatchesSelectable.get(rand), driver, "Size Swatches ");
        	Utils.waitUntilElementDisappear(driver, waitLoader);
        	Log.event("Clicked on Random Size...");
        	return BrowserActions.getTextNoFail(driver, selectedShoeSizeValue, "Selected shoe size value");
        } else {
        	if(isWidthAvailable()) {
        		Log.event("Shoe Width Available.");
        		int rand = Utils.getRandom(0, lstSizeSwatchesSelectable.size());
        		BrowserActions.clickOnSwatchElement(lstSizeSwatchesSelectable.get(rand), driver, "Size Swatches ");
        		Utils.waitUntilElementDisappear(driver, waitLoader);
        		Log.event("Clicked On Random Size...");
        		return BrowserActions.getText(driver, selectedShoeSizeValue, "Selected shoe size value");
        	}
        	return "";
        }
    }

    /**
     * To select bra band size
     * @param size - (optional) Size to select
     * <br> No option will select random size
     * @return Selected size
     * @throws Exception
     */
    public String selectBraBandSize(String... size) throws Exception {
        boolean flag = true;
        if (size.length != 0) {
            if (Utils.waitForElement(driver, selectedBraSize, 2)) {
            	return BrowserActions.getText(driver, selectedBraSizeValue, "Selected Bra size value");
            }
        }
        if (size.length > 0) {
            Utils.waitForElement(driver, selectedBraSize);

            for (int i = 0; i < lstBraSizeSwatchesSelectable.size(); i++) {
                if (lstBraSizeSwatchesSelectable.get(i).getText().trim().equals(size[0])) {
                    BrowserActions.clickOnSwatchElement(lstBraSizeSwatchesSelectable.get(i), driver, "Bra Size Swatches ");
                    Log.event("Clicked On Expected Size...");
                    waitForSpinner();
                    return BrowserActions.getText(driver, selectedBraSizeValue, "Selected Bra size value");
                }
            }
            for (int i = 0; i < lstBraSizeSwatchesUnSelectable.size(); i++) {
                if (lstBraSizeSwatchesUnSelectable.get(i).getText().trim().equals(size[0])) {
                    Log.message("--->>> Given Size(" + size[0]
                            + ") <b>Not Available</b> in Size list. Selecting Random Size...");
                    flag = false;
                    break;
                }
            }
            if (flag)
                Log.message("--->>> Given Size(" + size[0]
                        + ") <b>Not Applicable</b> in Size list. Selecting Random Size...");
            int rand = Utils.getRandom(0, lstBraSizeSwatchesSelectable.size());
            BrowserActions.clickOnSwatchElement(lstBraSizeSwatchesSelectable.get(rand), driver, " Bra Size Swatches ");
            Utils.waitUntilElementDisappear(driver, waitLoader);
            Log.event("Clicked on Random Size...");
            return BrowserActions.getText(driver, selectedBraSizeValue, "Selected Bra size value");
        } else {
        	if(lstBraSizeSwatchesSelectable.size() != 0) {
        		int rand = Utils.getRandom(0, lstBraSizeSwatchesSelectable.size());
        		BrowserActions.clickOnSwatchElement(lstBraSizeSwatchesSelectable.get(rand), driver, " Bra Size Swatches ");
        		Utils.waitUntilElementDisappear(driver, waitLoader);
        		Log.event("Clicked On Random Size...");
        		return BrowserActions.getText(driver, selectedBraSizeValue, "Selected Bra size value");
        	}else {
        		//Log.fail("Bra Size Swatch list returned as 0. Cannot select any swatch", driver);
        		return null;
        	}
        }
    }

    /**
     * to check and unchecked Add personal msg
     *
     * @param checkPersonalMsg -
     * @throws Exception -
     */
    public void chkOrUnchkAddPersonalMsg(boolean checkPersonalMsg) throws Exception {
        if (Utils.waitForElement(driver, ckbAddPersonalMsg)) {
            if (checkPersonalMsg && !(ckbAddPersonalMsg.isSelected())) {
                BrowserActions.clickOnElementX(ckbAddPersonalMsg, driver, "Add Personalized Message");
            } else if (!checkPersonalMsg && ckbAddPersonalMsg.isSelected()) {
                BrowserActions.clickOnElementX(ckbAddPersonalMsg, driver, "Add Personalized Message");
            }
        }
    }

    /**
     * To Check the add to cart button disabled state
     *
     * @return String - value
     * @throws Exception - Exception
     */
    public String addToCartButtonDisabled() throws Exception {
        BrowserActions.scrollToTopOfPage(driver);
        BrowserActions.scrollInToView(btnAddToBag, driver);
       /* BrowserActions.mouseHover(driver, btnAddToBag);
        BrowserActions.clickOnElementX(btnAddToBag, driver, "button"); */
        Log.event(BrowserActions.getTextFromAttribute(driver, btnAddToBag, "disabled", "Number"));
        return BrowserActions.getTextFromAttribute(driver, btnAddToBag, "disabled", "Number");
    }
    
    /**
     * To Check the add to cart button disabled state
     * boolean boolean - if add to bag button disabled or enabled
     * @throws Exception - Exception
     */
    public boolean verifyToCartButtonDisabled() throws Exception {
        BrowserActions.scrollInToView(btnAddToBag, driver);
        String buttonValue = BrowserActions.getTextFromAttribute(driver, btnAddToBag, "value", "Value");
        Log.event("Value:: " + buttonValue);
        return buttonValue.contains("color and size");
    }

    /**
     * to select quantity in pdp page
     *
     * @param qty -
     * @return String
     * @throws Exception - Exception
     */
    public String selectQty(String... qty) throws Exception {
        if (Utils.waitForElement(driver, drpQty)) {
            BrowserActions.scrollToViewElement(drpQty, driver);
            Select qtySelect = new Select(drpQty);
            try {
	            if (qty.length > 0) {
	                if (qtySelect.getAllSelectedOptions().get(0).getText().equals(qty[0]) || drpQty.getText().equals(qty[0]))
	                    return qty[0];
	            }
	            if (qty.length > 0)
	                qtySelect.selectByVisibleText(qty[0].trim());
	            else
	                qtySelect.selectByIndex(4);
            } catch (NoSuchElementException | ArrayIndexOutOfBoundsException ex) {
            	Log.failsoft("Specified qty is not available");            	
            }

            Utils.waitForPageLoad(driver);
            return drpQty.getAttribute("value") == null ? drpQty.getText() : drpQty.getAttribute("value");
        }

        if (qty.length > 0)
            if (selectedQty1.getText().trim().equals(qty[0]))
                return selectedQty1.getText().trim();

        if (!(Utils.waitForElement(driver, drpGiftCardSize) || Utils.waitForElement(driver, drpGiftCardSizeSelect))) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].style.opacity=1", selectedQty1);
            BrowserActions.clickOnElementX(selectedQty1, driver, "Quantity dropdown");
        } else {
            BrowserActions.scrollToViewElement(selectedQty1, driver);
            BrowserActions.clickOnElementX(selectedQty1, driver, "Quantity dropdown");
        }


        if (qty.length > 0) {
            WebElement qtyToSelect;
            List<WebElement> qtyList = driver.findElements(By.cssSelector(".quantity .selection-list li"));
            if (qtyList.size() >= Integer.parseInt(qty[0]))
                qtyToSelect = driver.findElement(By.cssSelector(".quantity .selection-list li[label*='" + qty[0] + "']"));
            else
                for (int i = 0; i < lstColorSelectable.size(); i++) {
                    selectColor(lstColorSelectable.get(i).findElement(By.cssSelector("img")).getAttribute("alt").trim());
                    for (int j = 0; j < lstSizeSwatchesSelectable.size(); j++) {
                        selectSize(lstSizeSwatchesSelectable.get(j).getText().trim());
                        int maxQty = driver.findElements(By.cssSelector(".quantity .selection-list li")).size();
                        if (maxQty >= Integer.parseInt(qty[0]))
                            qtyToSelect = driver.findElement(By.cssSelector(".quantity .selection-list li[label*='" + qty[0] + "']"));
                    }
                }

            qtyToSelect = driver.findElement(By.cssSelector(".quantity .selection-list li[label*='" + qty[0] + "']"));
            BrowserActions.clickOnElementX(qtyToSelect, driver, "Quantity");
            return selectedQty.getText();
        } else {
            int rand = Utils.getRandom(0, lstQtySelectable.size());
            BrowserActions.clickOnElementX(lstQtySelectable.get(rand), driver, "Quantity menu ");
            Log.event("Clicked on random Quantity...");
            BrowserActions.scrollToViewElement(selectedQty1, driver);
            return selectedQty.getText();
        }
    }

    /**
     * To get selected size family
     *
     * @return String - Selected size family value
     * @throws Exception - Exception
     */
    public String getSelectedSizeFamily() throws Exception {
        if (Utils.waitForElement(driver, divSizeFamily)) {
            return BrowserActions.getText(driver, SizeFamSelectedValue, "Size family selected value");
        } else
            return "No Size Family";
    }
    
    public int getSizeFamilyCount() throws Exception{
    	return SizeFamSelectable.size();
    }

    /**
     * to select size family and return the random size
     * @param sizeF - (Optional) Size family to select
     * <br> No option will select random family
     * @return Selected size family
     * @throws Exception
     */
    public String selectSizeFamily(String... sizeF) throws Exception {
		if (!Utils.waitForElement(driver, divSizeFamily)) {
			return "No Size Family";
		}
        if (sizeF.length > 0) {
        	if(sizeF[0].contains("'")) {
            	sizeF[0] = sizeF[0].split("\\'")[0];
            }
            String xpathForSizeFamily = "//ul[@class='swatches sizefamily']//li//a[contains(text(),'" + StringUtils.capitalize(sizeF[0]) + "')]";
            if (Utils.waitForElement(driver, driver.findElement(By.xpath(xpathForSizeFamily)))) {
                WebElement sizeFamilyToSelect = driver.findElement(By.xpath(xpathForSizeFamily));
                BrowserActions.clickOnSwatchElement(sizeFamilyToSelect, driver, "Size Family");
                Log.event("Clicked on Expected Size Family...");
            } else {
                Log.failsoft("--->>>Given Size Family Not Available...", driver);
                if (!Utils.waitForElement(driver, selectedSizeFamily)) {
                    int rand = Utils.getRandom(0, lstSizeFamilySelectable.size());
                    BrowserActions.clickOnSwatchElement(lstSizeFamilySelectable.get(rand), driver, "Size Family");
                }
            }
            return getSelectedSizeFamily();
        } else {
        	if(Utils.waitForElement(driver, SizeFamSelected)) {
        		return BrowserActions.getText(driver, SizeFamSelectedVal, "Size Family value").trim();
        	} else {
	            int SFSize = SizeFamSelectable.size();
	            if (SFSize > 0) {
	                int rand = Utils.getRandom(0, lstSizeFamilySelectable.size());
	                BrowserActions.clickOnSwatchElement(lstSizeFamilySelectable.get(rand), driver, "Size Faimly");
	                Log.event("Clicked on Random Size Family...");
	                Utils.waitForPageLoad(driver);
	                return lstSizeFamilySelectable.get(rand).getAttribute("title").split("\\:")[1].trim();
	            } else {
	                return getSelectedSizeFamily();
	            }
        	}
        }
    }
    
    /**
     * to select size family if already selected sizefamily available and return the random size
     * @param sizeF -
     * @return String
     * @throws Exception - Exception
     */
	public String selectUnSelectedSizeFamily(String... sizeF) throws Exception {
		if (!Utils.waitForElement(driver, divSizeFamily)) {
			return "No Size Family";
		}
		if (sizeF.length > 0) {
			if(sizeF[0].contains("'")) {
				sizeF[0] = sizeF[0].split("\\'")[0];
			}
			String xpathForSizeFamily = "//ul[@class='swatches sizefamily']//li//a[contains(translate(@title,'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'"
					+ sizeF[0].toLowerCase() + "')]";
			if (Utils.waitForElement(driver, driver.findElement(By.xpath(xpathForSizeFamily)))) {
				WebElement sizeFamilyToSelect = driver.findElement(By.xpath(xpathForSizeFamily));
				BrowserActions.clickOnSwatchElement(sizeFamilyToSelect, driver, "Size Family");
				Log.event("Clicked on Expected Size Family...");
			} else {
				Log.failsoft("--->>>Given Size Family Not Available...", driver);
				if (!Utils.waitForElement(driver, selectedSizeFamily)) {
					int rand = Utils.getRandom(0, lstSizeFamilySelectable.size());
					BrowserActions.clickOnSwatchElement(lstSizeFamilySelectable.get(rand), driver, "Size Family");
				}
			}
			return getSelectedSizeFamily();
		} else {
			int SFSize = SizeFamSelectable.size();
			if (SFSize > 0) {
				int rand = Utils.getRandom(0, lstSizeFamilySelectable.size());
				Log.event("Scrolled To Random Size Family...");
				BrowserActions.clickOnSwatchElement(lstSizeFamilySelectable.get(rand), driver, "Size Faimly");
				Log.event("Clicked on Random Size Family...");
				Utils.waitForPageLoad(driver);
				return lstSizeFamilySelectable.get(rand).getAttribute("title").split("\\:")[1].trim();
			} else {
				return getSelectedSizeFamily();
			}
		}
	}
    
    /**
     * to verify the Hemming title
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyHemmingTitle() throws Exception {
        if (BrowserActions.getText(driver, txtHemmingTitle, "Hemming titile").toLowerCase().equalsIgnoreCase("hemming"))
        	return true;
        else
            return false;
    }

    /**
     * to verify the Monogramming title
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyMonogrammingTitle() throws Exception {
        Log.event("Monogramming Title extracted as : " + txtMonogrammingTitle.getText().toLowerCase() + "|| Comparing Text to : monogramming");
        if (txtMonogrammingTitle.getText().toLowerCase().contains("monogramming"))
            return true;
        else
            return false;
    }

    /**
     * to verify if the hemming checkbox selected or not
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyHemmingCheckboxSelected() throws Exception {
        if (chkProductHemming.isSelected())
            return true;
        else
            return false;
    }

    /**
     * to verify that the hemming checkbox is displayed to the left of hemming
     * tips
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyHemmingCheckboxDisplayedToTheLeftOfHemmingTips() throws Exception {
        return BrowserActions.verifyHorizontalAllignmentOfElements(driver, lnkHemmingTips, chkProductHemming);
    }

    /**
     * To check/uncheck hemming checkbox
     *
     * @param state - To Open or close --- 'enable', 'disable'
     * @throws Exception - Exception
     */
    public void clickOnHemmingCheckbox(String state) throws Exception {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].style.opacity=1", chkProductHemming);
        BrowserActions.scrollInToView(chkProductHemming, driver);
        if (state.equalsIgnoreCase("enable")) {
            if (chkProductHemming.isSelected() == false) {
                BrowserActions.clickOnElementX(chkProductHemming, driver, "Enable Hemming Checkbox");
            }
        } else if (state.equalsIgnoreCase("disable")) {
            if (chkProductHemming.isSelected() == true) {
                BrowserActions.clickOnElementX(chkProductHemming, driver, "Disable Hemming Checkbox");
            }
        }
    }

    /**
     * to verify that the hemming checkbox is displayed to the left of hemming tips
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyMessagingDisplayedRightOfHemmingCheckbox() throws Exception {
        return BrowserActions.verifyHorizontalAllignmentOfElements(driver, txtHemmingMessaging, chkProductHemming);
    }

    /**
     * to verify that the hemming title is displayed to the left of hemming tips
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyHemmingTipsDisplayedRightOfHemmingTitle() throws Exception {
        return BrowserActions.verifyHorizontalAllignmentOfElements(driver, lnkHemmingTips, txtHemmingTitle);
    }

    /**
     * To click on tips link
     *
     * @throws Exception - Exception
     */
    public void clickOnTipsInHemmingSection() throws Exception {
        BrowserActions.clickOnElementX(lnkHemmingTips, driver, "Tips link");
        Utils.waitForPageLoad(driver);
    }

    /**
     * To click on hemming options
     *
     * @param index -
     * @throws Exception -
     */
    public void selectHemmingOption(int index) throws Exception {
        BrowserActions.clickOnElementX(lstHemmingOptions.get(index), driver, "hemming options");
    }

    /**
     * to verify that the no hemming options selected by default
     *
     * @return Boolean -
     * @throws Exception - Exception
     */
    public Boolean verifyNoHemmingOptionsSelectedByDefault() throws Exception {
        for (WebElement element : lstHemmingOptions) {
            if (element.getAttribute("class").equals("hemming-option selected")) {
                return false;
            }
        }
        return true;
    }

    /**
     * To enter value in the monogram first text box
     *
     * @param textToEnter -
     * @throws Exception -
     */
    public void enterTextInMonogramFirstTextBox(String textToEnter) throws Exception {
        BrowserActions.typeOnTextField(txtMonogramFirstTextField, textToEnter, driver, "Monogram first text box");
    }

    /**
     * To clear the monogram text fields
     *
     * @throws Exception -
     */
    public void clearMonogramTextField() throws Exception {
        BrowserActions.typeOnTextField(txtMonogramFirstTextField, "", driver, "");
        //BrowserActions.typeOnTextField(txtMonogramSecondTextField, "", driver, "");
    }

    /**
     * To enter value in the monogram second text box
     *
     * @param textToEnter -
     * @throws Exception -
     */
    public void enterTextInMonogramSecondTextBox(String textToEnter) throws Exception {
        BrowserActions.typeOnTextField(txtMonogramSecondTextField, textToEnter, driver, "Monogram second text box");
    }

    /**
     * to verify if selected hemming option is highlighted
     *
     * @param index -
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifySelectedHemmingOptionIsHighlighted(int index) throws Exception {
        if (lstHemmingOptions.get(index).getAttribute("class").trim().contains("selected"))
            return true;
        else
            return false;
    }

    /**
     * to verify if the error message is displayed for the hemming options
     * drawer
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyErrorMessageInHemmingOptions() throws Exception {
        if (txtHemmingErrorMessage.getAttribute("style").contains("block")
                && txtHemmingErrorMessage.getText().trim().equals("Please select hemming option."))
            return true;
        else
            return false;
    }

    /**
     * To close PDP add to bag overlay
     * @throws Exception
     */
    public void closeAddToBagOverlay() throws Exception {
    	Utils.waitForElement(driver, mdlMiniCartOverLay, 5);
        try {
            BrowserActions.clickOnElementX(btnCloseMiniCartOverLay, driver, "AddToBag OverLay Close Button");
        } catch (NoSuchElementException e) {
            if (Utils.waitForElement(driver, mdlMiniCartOverLay)) {
                Log.failsoft("Close Button in minicart overlay not found", driver);
                mdlMiniCartOverLay.sendKeys(Keys.ESCAPE);
                Log.event("Sent Escape Keypress");
            } else {
                if (Utils.waitForElement(driver, errMsgQuantityATS))
                    Log.fail("Inventory Problem occured :: " + errMsgQuantityATS.getText(), driver);
                else
                    Log.fail("Minicart Overlay not opened", driver);
            }

        }
        Utils.waitForPageLoad(driver);

    }

    /**
     * To click on enlarge view button
     *
     * @return EnlargeView page
     * @throws Exception -
     */
    public EnlargeViewPage clickOnEnlargeButton() throws Exception {
        BrowserActions.javascriptClick(btnEnlarge, driver, "Enlarge Button on PDP");
        return new EnlargeViewPage(driver).get();
    }
    
    /**
     * To click on enlarge view button in Product set
     *
     * @return EnlargeView page
     * @throws Exception -
     */
    public EnlargeViewPage clickOnEnlargeButtonProductSet(String prdName) throws Exception {
    	List<WebElement> componentprods = driver.findElements(By.cssSelector(".item-name"));
        for (int i = 0; i < componentprods.size(); i++) {
            if (componentprods.get(i).getAttribute("title").contains(prdName)) {
            	BrowserActions.javascriptClick(lstBtnEnlargeProductSet.get(i), driver, "Enlarge Button on PDP");
                break;
            }
        }    
        return new EnlargeViewPage(driver).get();
    }
    
    /**
     * To click on enlarge view button in Product set
     *
     * @return EnlargeView page
     * @throws Exception -
     */
    public EnlargeViewPage clickOnEnlargeButtonProductSetBasedOnIndex(int productIndex) throws Exception {
    	try {
	    	BrowserActions.scrollToView(lstBtnEnlargeProductSet.get(productIndex), driver);
	    	BrowserActions.clickOnElementX(lstBtnEnlargeProductSet.get(productIndex), driver, "Enlarge Button on PDP");
	        Utils.waitForPageLoad(driver);
    	
    	} catch(ArrayIndexOutOfBoundsException ex) {
    		Log.message("Available products are less than the given products");	
    	}
    	
    	if (Utils.waitForElement(driver, mdlEnlargeWindow)) {
    		return new EnlargeViewPage(driver).get();
    	} else
    		return null;
    }
    

    /**
     * To get number of alternate images
     *
     * @return boolean -
     * @throws Exception -
     */
    public int getNumberOfThumbnails() throws Exception {
        Log.event("--->>>No.oF Tumbnails :: " + lstProductThumbnail.size());
        return lstProductThumbnail.size();
    }
    
    /**
     * verify Gc main images
     * @return true if there is no gc images
     */
    
    public boolean verifyGcImages() throws Exception{
    if(Utils.waitForElement(driver, giftCardDesign)){
    	return false;
    }
    return true;
    }
    
    /**
     * To get number of alternate images
     *
     * @return boolean -
     * @throws Exception -
     */
    public boolean verifyAllThumbnailsDisplayInPage() throws Exception {
        int size = getNumberOfThumbnails();
        if (size >= 4) {
            for (int i = 0; i < 4; i++) {
                if (!lstProductThumbnail.get(i).isDisplayed()) {
                    return false;
                }
            }
            int size1 = 5;
            while (!(Utils.waitForElement(driver, btnNextImageDisable)) && (size1 <= size)) {
            	BrowserActions.clickOnElementX(btnNextImageEnable, driver, "Next Arrow in Alternate product image ");
                if (!lstProductThumbnail.get(size1 - 1).isDisplayed()) {
                    return false;
                }
                size1 = size1 + 1;
            }

        } else {
            for (int i = 0; i < size; i++) {
                if (!lstProductThumbnail.get(i).isDisplayed()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * To verify only one alternate image is present for gift card
     *
     * @return boolean -
     * @throws Exception -
     */
    public boolean verifyThumbnailNos() throws Exception {
        boolean status = false;
        if (lstProductThumbnail.size() > 1) {
            status = false;
        } else {
            status = true;
        }
        return status;
    }

    /**
     * To get number of color swatch available
     *
     * @return Integer -
     * @throws Exception -
     */
    public int getNumberOfColorSwatches() throws Exception {
        return lstColorSwatches.size();
    }
    /**
     * To get number of Size swatch available
     *
     * @return Integer -
     * @throws Exception -
     */
    public int getNumberOfSizeSwatches() throws Exception {
        return lstSize.size();
    }
    
    /**
     * to open or close tool tipoverlay
     *
     * @param state -
     * @throws Exception -
     */
    public void openCloseToolTipOverlay(String state) throws Exception {
        if (state.equals("open")) {
            if (Utils.waitForElement(driver, toolTipOverLay)) {
                if (toolTipOverLay.getAttribute("style").contains("display: block"))
                    Log.event("Tool Tip overlay opened.");
                else
                    BrowserActions.clickOnElementX(lnkTermsAndConditions, driver, "Terms and Conditions ");
                Utils.waitForElement(driver, toolTipOverLay);
            } else
            	BrowserActions.scrollInToView(lnkTermsAndConditions, driver);
                BrowserActions.clickOnElementX(lnkTermsAndConditions, driver, "Terms and Conditions ");
            Utils.waitForElement(driver, toolTipOverLay);
        } else {
            BrowserActions.clickOnElementX(closeToolTipOverLay, driver, "Close Button in Tool tip overlay ");
        }
        if (Utils.waitForElement(driver, toolTipOverLay)) {
            if (toolTipOverLay.getAttribute("style").contains("display: block"))
                Log.event("Tool Tip overlay opened.");
        }
    }

    public int getToolTipDiementsion(String state) throws Exception {
        if (state.equals("height"))
            return toolTipOverLay.getSize().height;
        else
            return toolTipOverLay.getSize().width;
    }
    
    /**
     * to click out side of tool tip overlay
     *
     * @param WebElement-
     * @throws Exception -
     */
    
    public void clickOutSideOfHemmingToolTipOverlay() throws Exception{
    	if (Utils.waitForElement(driver, hemmingToolTipOverlay)){
    		BrowserActions.clickOnEmptySpaceVertical(driver, hemmingToolTipOverlay);	
    		Log.event("Clicked out side of the tool tip window.");
    	}    	
    }
    
    

    /**
     * to Click on Gift card size in pdp page
     *
     * @throws Exception - Exception
     */
    public void clickOnGiftCardSize() throws Exception {
        if (Utils.waitForElement(driver, drpGiftCardSizeSelect))
            if (Utils.isMobile())
                BrowserActions.javascriptClick(drpGiftCardSizeSelect, driver, "GC Size(Div)");
            else
                BrowserActions.javascriptClick(drpGiftCardSizeSelect, driver, "GC Size(Div)");
        else
            BrowserActions.javascriptClick(drpGiftCardSize, driver, "GC Size(Div)");
    }

    /**
     * to select gift card size in pdp page
     *
     * @param size -
     * @return String
     * @throws Exception - Exception
     */
    public String selectGiftCardSize(String... size) throws Exception {
        BrowserActions.mouseHover(driver, driver.findElement(By.cssSelector("footer")));
        if (drpGiftCardSizeOptions.size() == 0)
            Log.fail("Gift card Size not present in page...");

        BrowserActions.clickOnElementX(drpGiftCardSize, driver, "Open dropdown");
        if (size.length > 0) {
            for (int i = 0; i < drpGiftCardSizeOptions.size(); i++) {
                if (drpGiftCardSizeOptions.get(i).getAttribute("label").trim().equals(size[0])) {
                    String selected = drpGiftCardSizeOptions.get(i).getText();
                    BrowserActions.clickOnElementX(drpGiftCardSizeOptionSelectable.get(i), driver, "Gift Card Size ");
                    return selected;
                }
            }
        } else {
            BrowserActions.clickOnElementX(drpGiftCardSizeOptionSelectable.get(Utils.getRandom(1, drpGiftCardSizeOptionSelectable.size())), driver, "Gift Card Size ");
        }
        Utils.waitForPageLoad(driver);
        return drpGiftCardSize.getAttribute("innerHTML").trim();
    }

    /**
     * To check the personalized message check box in gift card pdp
     *
     * @throws Exception -
     */
    public void checkGCPersonalizedMessage() throws Exception {
        BrowserActions.clickOnElementX(chkGCPersonalizedMessage, driver, "chkGCPersonalizedMessage");
        Log.event("Gift card Personalized Message checkbox Clicked!");
    }

    /**
     * to check Gift card personalized message
     *
     * @param state -
     * @throws Exception -
     */
    public void checkGCPersonalizedMessage(String state) throws Exception {
        if (state.equals("check")) {
            if (Utils.waitForElement(driver, txtGCPersonalMsgFrom)) {
                Log.event("Check Box already Checked.");
            } else {
                BrowserActions.javascriptClick(chkGCPersonalizedMessage, driver, "chkGCPersonalizedMessage");
            }
        } else {
            if (!Utils.waitForElement(driver, txtGCPersonalMsgFrom)) {
                Log.event("Checkbox already Unchecked.");
            } else {
                BrowserActions.javascriptClick(chkGCPersonalizedMessage, driver, "chkGCPersonalizedMessage");
            }
        }
        Log.event("Gift card Personalized Message checkbox checked!");
    }

    /**
     * To get amount value selected in gift card pdp
     *
     * @return String -
     * @throws Exception -
     */
    public String getSelectedGCAmount() throws Exception {
    	
    	return drpGiftCardSizeValue.getAttribute("innerHTML");
    }

    /**
     * To get quantity value
     *
     * @return Integer -
     * @throws Exception -
     */
    public int getQuantity() throws Exception {
    	String innerText = BrowserActions.getTextFromAttribute(driver, selectedQty1, "innerText", "QTY text");
        return com.fbb.support.StringUtils.getNumberInString(innerText.replace(".00", "").replace(".0", "").trim());
    }

    /**
     * to get the selected size value
     *
     * @return String
     * @throws Exception - Exception
     */
    public String getSelectedSize() throws Exception {
		if(Utils.waitForElement(driver, selectedShoeSize)) {
			String productSize= BrowserActions.getText(driver, selectedShoeSize, "Shoe Size");
			return productSize;
		} else if(Utils.waitForElement(driver, selectedSize)) {
			String size = BrowserActions.getText(driver, selectedSize, "Selected size");
			Log.event("-->>>Selected Size :: " + size);
			return size;
		} else {
			return "";
		}
    }
    
    /**
     * to get the selected Width value
     *
     * @return String
     * @throws Exception - Exception
     */
    
    public String getSelectedShoeWidth() throws Exception {
	    if(Utils.waitForElement(driver, selectedShoeWidth)){
		    String productWidth= BrowserActions.getText(driver, selectedShoeWidth, "Shoe width");
		    return productWidth;
	    }else{
	    	return "";
	    }
    }
    
    /**
     * to get the selected ShoeSize value
     *
     * @return String
     * @throws Exception - Exception
     */
    public String getSelectedShoeSizeValue() throws Exception {
        Log.event("-->>>Selected Width :: " + divWidthGrid.findElement(By.cssSelector("input")).getText().trim());
        return divWidthGrid.findElement(By.cssSelector("input")).getText().trim();
    }
     
    

    /**
     * to get the selected color value
     * @return Selected color
     * @throws Exception
     */
    public String getSelectedColor() throws Exception {
    	if(Utils.waitForElement(driver, selectedColorVariant)) {
    		return BrowserActions.getText(driver, selectedColorVariant, "Selected color");
    	} else {
    		return "";
    	}
    }

    /**
     * to get the selected size value
     *
     * @param index -
     * @return String
     * @throws Exception - Exception
     */
    public String getSelectedSizeSpecialProductSet(int index) throws Exception {
        WebElement size = driver.findElements(By.cssSelector(".size .selected-value")).get(index);
        return size.getText().trim();
    }

    /**
     * To navigate to Pinterest
     *
     * @throws Exception - Exception
     */
    public void navigateToPinterest() throws Exception {
        BrowserActions.clickOnElementX(lnkSharePinterestIcon, driver, "Pinterest ");
        Utils.waitForPageLoad(driver);
        Utils.switchWindows(driver, "pinterest", "url", "");
    }

    /**
     * To navigate to FaceBook
     *
     * @throws Exception - Exception
     */
    public void navigateToFaceBook() throws Exception {
        BrowserActions.clickOnElementX(lnkShareFaceBookIcon, driver, "FaceBook ");
        Utils.waitForPageLoad(driver);
        Utils.switchWindows(driver, "facebook", "url", "");
    }

    /**
     * To navigate to Twitter
     *
     * @throws Exception - Exception
     */
    public void navigateToTwitter() throws Exception {
        BrowserActions.clickOnElementX(lnkShareTwitterIcon, driver, "Twitter ");
        Utils.waitForPageLoad(driver);
        Utils.switchWindows(driver, "twitter", "url", "");
    }


    /**
     * to get the breadcrumb value
     *
     * @return List of String
     * @throws Exception - Exception
     */
    public List<String> getBreadcrumbText() throws Exception {
        return BrowserActions.getText(lstTxtProductInBreadcrumb, "Breadcrumb value", driver);
    }

    /**
     * to click the breadcrumb value
     *
     * @param breadcrumbValue -
     * @return PdpPage
     * @throws Exception - Exception
     */
    public Object clickCategoryInBreadcrumb(String breadcrumbValue) throws Exception {
        for (WebElement ele : lstTxtProductInBreadcrumb) {
            if (ele.getText().trim().equalsIgnoreCase(breadcrumbValue)) {
                if (breadcrumbValue.trim().equalsIgnoreCase("Home")) {
                    BrowserActions.clickOnElementX(ele, driver, "Breadcrumb Category");
                    Utils.waitForPageLoad(driver);
                    return new HomePage(driver).get();
                } else {
                    BrowserActions.clickOnElementX(ele, driver, "Breadcrumb Category");
                    Utils.waitForPageLoad(driver);
                    return this;
                }
            }
        }
        return null;
    }

    /**
     * to get the last breadcrumb value
     *
     * @return String
     * @throws Exception - Exception
     */
    public String getLastBreadcrumbText() throws Exception {
    	WebElement bcLastCategoryName = Utils.isMobile() ? bcLastCategoryNameMobile : bcLastCategoryNameDesktopTablet;
        return BrowserActions.getText(driver, bcLastCategoryName, "Last Breadcrumb text");
    }

    /**
     * to verify the properties in pdp page
     *
     * @param property -
     * @return String
     * @throws Exception - Exception
     */
    public boolean verifyProperties(String property) throws Exception {
        String propertyText;
        String textToVerify;
        switch (property) {
            case "DropShip":
                textToVerify = lnkMoreInfo.getText();
                propertyText = demandWareProperty.getProperty("DropShip");
                Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
                if (textToVerify.equals(propertyText)) {
                    return true;
                }
                break;
            case "ExtraPostage":
                textToVerify = lnkMoreInfo.getText();
                propertyText = demandWareProperty.getProperty("ExtraPostage");
                Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
                if (textToVerify.equals(propertyText)) {
                    return true;
                }
                break;
            case "InternationalShipping":
                textToVerify = txtInterShipping.getText();
                propertyText = demandWareProperty.getProperty("InternationalShipping");
                Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
                if (textToVerify.equals(propertyText)) {
                    return true;
                }
                break;
            case "POBoxExcluded":
                textToVerify = txtPoAddExcluded.getText();
                propertyText = demandWareProperty.getProperty("POBoxExcluded");
                Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
                if (textToVerify.equals(propertyText)) {
                    return true;
                }
                break;
            case "Monogrammable":
                textToVerify = txtMonogramProduct.getText();
                propertyText = demandWareProperty.getProperty("Monogrammable");
                Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
                if (textToVerify.equals(propertyText)) {
                    return true;
                }
                break;
            case "Hemmable":
                textToVerify = txtHemmableProduct.getText();
                propertyText = demandWareProperty.getProperty("Hemmable");
                Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
                if (textToVerify.equals(propertyText)) {
                    return true;
                }
                break;
            case "GiftCardElectronic":
                textToVerify = lnkMoreInfo.getText();
                propertyText = demandWareProperty.getProperty("GiftCardElectronic");
                Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
                if (textToVerify.equals(propertyText)) {
                    return true;
                }
                break;
            case "GiftCardPlastic":
                textToVerify = txtPlasticGiftCard.getText();
                propertyText = demandWareProperty.getProperty("GiftCardPlastic");
                Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
                if (textToVerify.equals(propertyText)) {
                    return true;
                }
                break;
            case "FreeExchange":
                textToVerify = lnkMoreInfo.getText();
                propertyText = demandWareProperty.getProperty("FreeExchange");
                Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
                if (textToVerify.equals(propertyText)) {
                    return true;
                }
                break;
            case "ExpeditedShipping":
                textToVerify = lnkMoreInfo.getText();
                propertyText = demandWareProperty.getProperty("ExpeditedShipping");
                Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
                if (textToVerify.equals(propertyText)) {
                    return true;
                }
                break;
            case "NeedInfo":
                textToVerify = lnkMoreInfo.getText();
                propertyText = demandWareProperty.getProperty("NeedInfo");
                Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
                if (textToVerify.equals(propertyText)) {
                    return true;
                }
                break;
            case "ClickHere":
                textToVerify = lnkClickHere.getText();
                propertyText = demandWareProperty.getProperty("ClickHere");
                Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
                if (textToVerify.equals(propertyText)) {
                    return true;
                }
                break;
            case "ShippingInfo":
                textToVerify = txtShippingAndReturnInfo.getText();
                propertyText = demandWareProperty.getProperty("ShippingInfo");
                Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
                if (textToVerify.equals(propertyText)) {
                    return true;
                }
                break;
        }
        return false;
    }

    /**
     * to verify the last breadcrumb value is not clickable
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyLastBreadcrumbTextNotClickable() throws Exception {
    	WebElement bcLastCategoryName = Utils.isMobile() ? bcLastCategoryNameMobile : bcLastCategoryNameDesktopTablet;
        if (bcLastCategoryName.getAttribute("innerHTML").contains("href"))
            return false;
        else
            return true;
    }

    /**
     * to verify the last breadcrumb text color is different
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyLastBreadcrumbDifferentTextColor() throws Exception {
    	WebElement bcLastCategoryName = Utils.isMobile() ? bcLastCategoryNameMobile : bcLastCategoryNameDesktopTablet;
    	List<WebElement> lstBreadCrumbLinks = Utils.isMobile() ? lstBreadCrumbLinksMobile : lstBreadCrumbLinksDesktopTablet;
    	String bcLastTextColor = bcLastCategoryName.getCssValue("color");
    	String bcPrevTextColor = lstBreadCrumbLinks.get(0).getCssValue("color");
    	for(int i = 1 ; i < lstBreadCrumbLinks.size() ; i++) {
    		if(!bcPrevTextColor.equalsIgnoreCase(lstBreadCrumbLinks.get(i).getCssValue("color"))) {
    			return false;
    		}
    	}
        return !bcPrevTextColor.equalsIgnoreCase(bcLastTextColor);
    }

    /**
     * To zoom the product image in the Quick Shop overlay
     *
     * @throws Exception - Exception
     */
    public void zoomProductImage() throws Exception {
        BrowserActions.mouseHover(driver, imgProductPrimaryImage.findElement(By.cssSelector("img")), "");
        BrowserActions.moveToElementJS(driver, imgProductPrimaryImage.findElement(By.cssSelector("img")));
    }

    /**
     * to verify that the zoomed image is to the right of product image
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyZoomedImageDisplayedRightOfPrimaryProductImage() throws Exception {
		WebElement zoomContainer = divZoomWindowContainer;
		if (lstDivZoomWindowContainer.size() > 1) {
			zoomContainer = lstDivZoomWindowContainer.get(lstDivZoomWindowContainer.size() - 1);
		}
    	
		boolean verifyZoomedImageDisplayedRightOfPrimaryProductImage = BrowserActions
				.verifyHorizontalAllignmentOfElements(driver, zoomContainer, imgProductPrimaryImage);
        return verifyZoomedImageDisplayedRightOfPrimaryProductImage;
    }

    /**
     * to verify that the top of zoomed image alligns with the top of product
     * image
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyTopOfZoomedImageAllignsWithTopOfProductImage() throws Exception {
    	WebElement zoomContainer = divZoomWindowContainer;
		if (lstDivZoomWindowContainer.size() > 1) {
			zoomContainer = lstDivZoomWindowContainer.get(lstDivZoomWindowContainer.size() - 1);
		}
		Boolean verifyTopOfZoomedImageAllignsWithTopOfProductImage = BrowserActions.verifyElementsAreInSameRow(driver,
				zoomContainer, imgProductPrimaryImage.findElement(By.cssSelector("img")));
    	return verifyTopOfZoomedImageAllignsWithTopOfProductImage;
    }

    /**
     * To verify Quantity Dropdown have 1-10 only
     *
     * @return boolean
     * @throws Exception - Exception
     */
    public boolean verifyQtyDrp() throws Exception {
        boolean state = true;
        if (Utils.waitForElement(driver, drpQty)) {
            Select qty = new Select(drpQty);

            for (int x = 1; x <= 10; x++)
                if (!(Integer.parseInt(qty.getOptions().get(x - 1).getText().trim()) == (x)))
                    return false;

            return true;
        }
        try {
            if (lstQty.size() != 10)
                Log.reference("Quantity Avaliable for Selected Variation :: " + lstQty.size());

            for (int i = 0; i < lstQty.size(); i++)
                if (!lstQty.get(i).getText().trim().equals(Integer.toString(i + 1))) {
                    state = false;
                }
        } catch (StaleElementReferenceException e) {
            return false;
        }

        return state;
    }

    /**
     * To get maximum quantity available in quantity dropdown
     *
     * @return Integer -
     * @throws Exception -
     */
    public int getQuantityListSize() throws Exception {
        Log.event("Available Quantity Size :: " + lstQty.size());
        return lstQty.size();
    }
    
    /**
     * To verify qty matches for sizefamily and shoewidth $ Bra type products
     * @return boolean
     * @throws Exception
     */
	public boolean selectQtyWithColorAndSize(int qty) throws Exception {
		int type = 0;
		List<WebElement> element = null;
		if (lstSizeFamily.size() > 0) {
			type = lstSizeFamily.size();
			element = lstSizeFamily;
		} else if (lstshoeWidth.size() > 0) {
			type = lstshoeWidth.size();
			element = lstshoeWidth;
		}
		if (Utils.waitForElement(driver, divSizeFamily) || Utils.waitForElement(driver, divWidthGrid_shoe)) {
			boolean b = false;
			int i;
			do {
				i = 0;
				BrowserActions.clickOnElementX(element.get(i), driver, "Selected size");
				b = verifyQuantityMatches(qty);
				if (type > i)
					i++;
				if (b == true)
					return true;
			} while (b == false && i < type);
			return false;
		} else if (isBraSizeAvailable() == true) {
			int colors = lstColorSelectable.size();
			int bandSizes = lstBraSizeSwatches.size();
			int cupSizes = lstBraCupSizeSwatches.size();
			for (int color = 0; color < colors; color++) {
				for (int bandSize = 0; bandSize < bandSizes; bandSize++) {
					for (int cupSize = 0; cupSize < cupSizes; cupSize++) {

						selectColorBasedOnIndex(color);
						String bandsize = BrowserActions.getText(driver, lstBraSizeSwatches.get(bandSize), "");
						selectBraBandSize(bandsize);
						selectCupBasedOnIndex(cupSize);
						if (qty <= getQuantityListSize()) {
							String selectqty = Integer.toString(qty);
							selectQty(selectqty);
							Log.message(". Selected " + qty + " from dropdown");
							return true;
						}
					}
				}
			}
		} else {
			return verifyQuantityMatches(qty);
		}
		return false;
	}
    
    
    /**
     * To verify quantity available in quantity drop down
     *
     * @return True - if given Quantity matches with drop down Quantity.
     * @throws Exception -
     */
    public boolean verifyQuantityMatches(int qty) throws Exception {
    	int colors = lstColorSelectable.size(), sizes=0;
    	WebElement sizeSelected, selectableSize;
    	if(lstSizeSwatches.size()>0){
    	sizes = lstSizeSwatches.size();
    	sizeSelected=selectedSize;
    	selectableSize=sizeSelectable;    	
    	}else{
    	sizes=lstShoeSizeSwatches.size();
    	sizeSelected=selectedShoeSizeValue;
    	selectableSize=lstShoeSizeSwatchesSelectable.get(0);
    	}
    	
    	if(Utils.waitForElement(driver, selectedColor) && Utils.waitForElement(driver, sizeSelected)){
    		if(qty<=getQuantityListSize()){
    			String selectqty= Integer.toString(qty);
    			selectQty(selectqty);
    			Log.message(". Selected "+qty+" from dropdown");
    			return true;
    		}
    	}    		
    	if(Utils.waitForElement(driver, selectedColor)&& Utils.waitForElement(driver, selectableSize)){    		
    		for(int s=0 ; s<sizes ; s++){
    		selectSizeBasedOnIndex(s);
    		Log.event("Variations selected.");
    		if(qty <= getQuantityListSize()){
    			String selectqty = Integer.toString(qty);
    			selectQty(selectqty);
    			Log.message(". Selected "+qty+" from dropdown");
    			return true;
    		}  
    		}
    	}
    	else if(Utils.waitForElement(driver, sizeSelected)&& Utils.waitForElement(driver, colorSelectable)){    		
    		  for(int c = 0 ; c<colors ; c++){
    			  selectColorBasedOnIndex(c);
    			  if(qty <= getQuantityListSize()){
		    			String selectqty = Integer.toString(qty);
		    			selectQty(selectqty);
		    			Log.message(". Selected "+qty+" from dropdown");
		    			return true;
		    		}    			  
    		  }    		
    	}else{
	    	if(Utils.waitForElement(driver, colorSelectable) && Utils.waitForElement(driver, selectableSize)){
		    	    for(int c = 0 ; c<colors ; c++){
		        	for(int s=0 ; s<sizes ; s++){
			    		selectColorBasedOnIndex(c);
			    		selectSizeBasedOnIndex(s);
			    		Log.event("Variations selected.");
			    		if(qty <= getQuantityListSize()){
			    			String selectqty = Integer.toString(qty);
			    			selectQty(selectqty);
			    			Log.message(". Selected "+qty+" from dropdown");
			    			return true;
			    		}
		        	}
		        }
	    	}
    	}
    	return false;
	}
    
    /**
     * To Enter Email Address in Gift card To field
     *
     * @param obj - String or Integer "String" - Exact Email Address "Integer" -
     *            Random number of characters
     * @throws Exception - Exception
     */
    public void enterGCToAddress(Object obj) throws Exception {
        String str = obj.toString();
        if (!(str.matches("[0-9]") || str.matches("[0-9][0-9]") || str.matches("[0-9][0-9][0-9]")))
            BrowserActions.typeOnTextField(txtGCPersonalMsgTo, obj.toString(), driver,
                    "GC Personal Message To Address ");
        else
            BrowserActions.typeOnTextField(txtGCPersonalMsgTo,
                    RandomStringUtils.randomAlphanumeric(Integer.parseInt(obj.toString())).toLowerCase(), driver,
                    "GC Personal Message To Address ");
    }

    /**
     * To Enter Email Address in Gift card From field
     *
     * @param obj - String or Integer "String" - Exact Email Address "Integer" -
     *            Random number of characters
     * @throws Exception - Exception
     */
    public void enterGCFromAddress(Object obj) throws Exception {
        String str = obj.toString();
        if (!(str.matches("[0-9]") || str.matches("[0-9][0-9]") || str.matches("[0-9][0-9][0-9]")))
            BrowserActions.typeOnTextField(txtGCPersonalMsgFrom, obj.toString(), driver,
                    "GC Personal Message From Address ");
        else
            BrowserActions.typeOnTextField(txtGCPersonalMsgFrom,
                    RandomStringUtils.randomAlphanumeric(Integer.parseInt(obj.toString())).toLowerCase(), driver,
                    "GC Personal Message From Address ");
    }

    public void closeFlyTipsByClickingOutSide() throws Exception {
        if (SystemProperties.getUserAgentDeviceTest() && Utils.getRunBrowser(driver).equals("safari") ||
                (!Utils.getRunBrowser(driver).equals("safari"))) {
            Actions action = new Actions(driver);
            action.moveToElement(btnCloseTipsFlyout, 0, 0).click().build().perform();
        } else {
            int x = flytTips.getLocation().getX() - 10;
            int y = flytTips.getLocation().getY();
            String code = "document.elementFromPoint(" + x + "," + y + ").click();";
            ((JavascriptExecutor) driver).executeScript(code);

        }
    }

    /**
     * To Enter Email Address in Gift card Personal message field
     *
     * @param obj - String or Integer "String" - Exact Email Address "Integer" -
     *            Random number of characters
     * @throws Exception - Exception
     */
    public void enterGCPersonalMessage(Object obj) throws Exception {
        String str = obj.toString();
        if (!(str.matches("[0-9]") || str.matches("[0-9][0-9]") || str.matches("[0-9][0-9][0-9]")))
            BrowserActions.typeOnTextField(txtAreaPersonalMsg, obj.toString(), driver, "GC Personal Message ");
        else
            BrowserActions.typeOnTextField(txtAreaPersonalMsg,
                    RandomStringUtils.randomAlphanumeric(Integer.parseInt(obj.toString())).toLowerCase(), driver,
                    "GC Personal Message ");
    }

    /**
     * To remove/clear values from Personalization message textarea in gift card pdp
     *
     * @throws Exception -
     */
    public void clearGCPersonalMessage() throws Exception {
        BrowserActions.javascriptClick(txtAreaPersonalMsg, driver, "Area Personal Msg");
        txtAreaPersonalMsg.clear();
    }

    /**
     * To Select the gift card count or size
     *
     * @param size -
     * @return String
     * @throws Exception - Exception
     */
    public String selectGCSize(String... size) throws Exception {
        if (Utils.waitForElement(driver, drpGiftCardSizeSelect)) {
            Select drpGCSizeSelect = new Select(drpGiftCardSizeSelect);

            if (size.length > 0) {
                Log.event("Given Size :: " + size[0]);
                drpGCSizeSelect.selectByVisibleText(size[0]);
            } else {
                if (drpGCSizeSelect.getOptions().size() > 1) {
                    Log.event("Amount In GC :: " + drpGCSizeSelect.getOptions().get(1).getText());
                    if (drpGCSizeSelect.getOptions().get(0).getText().equalsIgnoreCase("select amount"))
                        drpGCSizeSelect.selectByIndex(1);
                    
                    Utils.waitForPageLoad(driver);
                    Log.event("Amount In GC :: " + drpGCSizeSelect.getOptions().get(1).getText());
                    Log.event("Size selection failed");
                } else
                    Log.fail("No Gift card amount size available....", driver);
            }

            Log.event("Returning value from Select tag.");
            String value = null;
            try {
                value = drpGCSizeSelect.getAllSelectedOptions().get(0).getText();
            } catch (StaleElementReferenceException e) {
                return drpGiftCardSize.getText();
            }
            Log.event(" Value from Select Tag :: " + value);
            return value;
        }

        String dataToReturn = drpGiftCardSize.getText();
        if (dataToReturn.toLowerCase().contains("select amount")) {
            BrowserActions.clickOnElementX(drpGiftCardSize, driver, "Gift card amount ");
            if (drpGiftCardSizeOptions.size() > 0) {
                if (size.length > 0) {
                    for (int i = 0; i < drpGiftCardSizeOptionSelectable.size(); i++) {
                        if (drpGiftCardSizeOptionSelectable.get(i).getAttribute("label").trim().replace("\\n", "").equals(size[0])) {
                            BrowserActions.clickOnElementX(drpGiftCardSizeOptionSelectable.get(i), driver, (i + 1) + "nth Item ");
                            break;
                        }
                    }
                } else {
                    int rand = Utils.getRandom(1, drpGiftCardSizeOptionSelectable.size() - 1);
                    JavascriptExecutor executor = (JavascriptExecutor) driver;
                    executor.executeScript("arguments[0].style.opacity=1", drpGiftCardSizeOptionSelectable.get(rand));
                    try {
                        BrowserActions.clickOnElementX(drpGiftCardSizeOptionSelectable.get(rand), driver, rand + "th Item ");
                    } catch (Exception e) {
                        BrowserActions.selectDropdownByIndex(drpGiftCardSizeSelect, rand);
                    }
                }
            }
            Utils.waitForPageLoad(driver);
        }
        dataToReturn = drpGiftCardSizeValue.getAttribute("innerHTML");
        return dataToReturn;
    }

    /**
     * to select All Product set size
     *
     * @param size -
     * @return String
     * @throws Exception - Exception
     */

    public String[] selectAllProductSetSize(String[] size) throws Exception {
        if (lstSizeGrid.size() > 0) {
            int intNoOfProd;
            String[] productText = new String[10];
            List<WebElement> prdGrid = driver.findElements(By.cssSelector(".product-set-item"));
            int NoOfProd = prdGrid.size();
            int NoOfSizeGrid = lstSizeGrid.size();
            for (intNoOfProd = 0; intNoOfProd < NoOfProd; intNoOfProd++) {
                productText[intNoOfProd] = prdGrid.get(intNoOfProd).findElement(By.cssSelector(".product-name"))
                        .getText();
            }
            for (intNoOfProd = 0; intNoOfProd < NoOfSizeGrid; intNoOfProd++) {
                WebElement sizeToChoose = lstSizeGrid.get(intNoOfProd)
                        .findElement(By.cssSelector(".selectable a[title='Select size: " + size[intNoOfProd] + "']"));
                BrowserActions.javascriptClick(sizeToChoose, driver, "Size Swatch");
                if (Utils.waitForElement(driver, waitLoader)) {
                    Utils.waitUntilElementDisappear(driver, waitLoader);
                }
            }
            return productText;
        }
        return null;
    }

    public String[] getAllSelectedSize() throws Exception {
        String[] selectedSize = new String[lstSizeGrid.size()];
        for (int i = 0; i < lstSizeGrid.size(); i++) {
            WebElement sizeSelected = lstSizeGrid.get(i).findElement(By.cssSelector(".selected span"));
            selectedSize[i] = sizeSelected.getAttribute("innerHTML").trim();
        }
        return selectedSize;
    }


    /**
     * to Select All product set size based on index
     *
     * @param index           -
     * @param NeedProductName -
     * @return String
     * @throws Exception - Exception
     */
    public String[] selectAllProductSetSizeBasedOnIndex(int index, boolean... NeedProductName) throws Exception {

        if (lstSizeGrid.size() > 0) {
            int intNoOfProd;
            String[] productText = new String[lstSizeGrid.size()];
            List<WebElement> prdGrid = driver.findElements(By.cssSelector(".product-set-item"));
            int NoOfProd = prdGrid.size();
            int NoOfSizeGrid = lstSizeGrid.size();
            if (NeedProductName.length > 0) {
                for (intNoOfProd = 0; intNoOfProd < NoOfProd; intNoOfProd++) {
                    productText[intNoOfProd] = prdGrid.get(intNoOfProd).findElement(By.cssSelector(".product-name")).getText();
                }
            }
            for (intNoOfProd = 0; intNoOfProd < NoOfSizeGrid; intNoOfProd++) {
                List<WebElement> lstOfSizes = lstSizeGrid.get(intNoOfProd).findElements(By.cssSelector(" .selectable input"));
                BrowserActions.javascriptClick(lstOfSizes.get(index), driver, "Size Swatch");
                Utils.waitForPageLoad(driver);
            }
            return productText;
        }
        return null;
    }

    /**
     * To open or close all the shop now section in product set
     *
     * @param isOpen - true, if needed to open all else false
     * @throws Exception - Exception
     */
    public void openOrCloseAllShopNowSectionInPrdSet(boolean isOpen) throws Exception {
        if (Utils.isMobile()) {
            int size = shopNowWindow_ProductSet.size();
            if (isOpen) {
                for (int i = 0; i < size; i++) {
                    String opened = BrowserActions.getTextFromAttribute(driver, shopNowWindow_ProductSet.get(i), "class", "Shop Now");
                    if (!(opened.toLowerCase().contains("open"))) {
                        BrowserActions.javascriptClick(shopNowWindow_ProductSet.get(i), driver, "Shop Now");
                    }
                }
            } else {
                for (int i = 0; i < size; i++) {
                    String opened = BrowserActions.getTextFromAttribute(driver, shopNowWindow_ProductSet.get(i), "class", "Shop Now");
                    if (opened.toLowerCase().contains("open")) {
                        BrowserActions.javascriptClick(shopNowWindow_ProductSet.get(i), driver, "Shop Now");
                    }
                }
            }
        }
        Utils.waitForPageLoad(driver);
    }


    /**
     * To select size based on index
     *
     * @param index of the size swatch
     * @throws Exception - Exception
     */
    public String selectSizeBasedOnIndex(int index) throws Exception {
        Utils.waitForPageLoad(driver);
        List<WebElement> lstOfSizes;
        if (Utils.waitForElement(driver, divSizeGrid)) {
            lstOfSizes = divSizeGrid.findElements(By.cssSelector(".selectable a"));
        } else {
            lstOfSizes = divSizeGrid_shoe.findElements(By.cssSelector(".selectable a"));
        }
        WebElement elem = lstOfSizes.get(index);
        BrowserActions.scrollInToView(elem, driver);
        BrowserActions.javascriptClick(elem, driver, "Size Swatch");
        Utils.waitForPageLoad(driver);
        return selectedSize.getText().trim();
    }

    /**
     * To click on Customer gallery images based on index
     *
     * @param index - index of the image to be clicked
     * @throws Exception - Exception
     */
    public CustomerGalleryModalPage clickOnCustomerGalleryImages(int index) throws Exception {
        BrowserActions.clickOnElementX(customerGalleryImages.get(index), driver, "Customer Gallery");
        return new CustomerGalleryModalPage(driver).get();
    }


    /**
     * To select color based on index
     *
     * @param index of the size swatch
     * @throws Exception - Exception
     */
    public void selectColorBasedOnIndex(int index) throws Exception {
        Utils.waitForPageLoad(driver);
        List<WebElement> lstOfColor = divColorGrid.findElements(By.cssSelector(".selectable a"));
        WebElement elem = lstOfColor.get(index);
        BrowserActions.scrollInToView(elem, driver);
        BrowserActions.clickOnSwatchElement(elem, driver, "Color Swatch");
        Utils.waitForPageLoad(driver);
    }

    /**
     * To select width based on index
     * @param index of the size swatch
     * @throws Exception - Exception
     */
    public void selectWidthBasedOnIndex(int index) throws Exception {
        Utils.waitForPageLoad(driver);
        List<WebElement> lstOfWidth = null;
        if (Utils.waitForElement(driver, divWidthGrid_shoe)) {
            lstOfWidth = divWidthGrid_shoe.findElements(By.cssSelector(".selectable a"));
        } else if (Utils.waitForElement(driver, divWidthGrid)) {
            lstOfWidth = divWidthGrid.findElements(By.cssSelector(".selectable a"));
        }
        
        if(lstOfWidth.size() > 0) {
        	WebElement elem = lstOfWidth.get(index);
        	BrowserActions.scrollInToView(elem, driver);
        	BrowserActions.clickOnSwatchElement(elem, driver, "swatch index " + index);
        	Utils.waitForPageLoad(driver);
        }
    }

    /**
     * To select width based on index
     *
     * @param index - index of the size swatch
     * @throws Exception - Exception
     */
    public void selectCupBasedOnIndex(int ...index) throws Exception {
    	if (Utils.waitForElement(driver, divCupGrid)) {
    		//Utils.waitForPageLoad(driver);
            List<WebElement> lstOfCup = null;
            
            lstOfCup = divCupGrid.findElements(By.cssSelector(" .selectable a"));
            
            WebElement elem = null;
            if (index.length > 0) {
            	elem = lstOfCup.get(index[0]);
            } else {
            	elem = lstOfCup.get(Utils.getRandom(0, lstOfCup.size()-1));
            }
            
            BrowserActions.scrollInToView(elem, driver);
            BrowserActions.clickOnSwatchElement(elem, driver, "Width Swatch");
            
            waitForSpinner();
        } else {
        	Log.event("Product does not have cup size option.");
        }

    }

    /**
     * To select product set width based on index
     *
     * @param index of the size swatch
     * @throws Exception - Exception
     */
    public void selectAllProductSetWidthBasedOnIndex(int index) throws Exception {
        if (lstWidthGrid.size() > 0) {
            int intNoOfProd;
            int NoOfColorGrid = lstWidthGrid.size();
            Utils.waitForPageLoad(driver);
            for (intNoOfProd = 0; intNoOfProd < NoOfColorGrid; intNoOfProd++) {
                BrowserActions.javascriptClick(lstWidthGrid.get(index).findElement(By.cssSelector("a")), driver, "Width Swatch");
                Thread.sleep(4000);
            }
        }
    }


    /**
     * To select spl product set size family based on index
     *
     * @param index of the size swatch
     * @throws Exception - Exception
     */
    public void selectAllSplProductSetSizeFamilyBasedOnIndex(int index) throws Exception {
        if (lstSizeFamilyGrid.size() > 0) {
            int intNoOfProd;
            int NoOfSizeGrid = lstSizeFamilyGrid.size();
            Utils.waitForPageLoad(driver);
            for (intNoOfProd = 0; intNoOfProd < NoOfSizeGrid; intNoOfProd++) {
                List<WebElement> lstOfSizes = lstSizeFamilyGrid.get(intNoOfProd).findElements(By.cssSelector(".selectable input"));
                BrowserActions.javascriptClick(lstOfSizes.get(index), driver, "Size Family Swatch");
                //There is no specific wait for the reload of the page. Hence including Thread.sleep
                Thread.sleep(4000);
            }
        }
    }

    /**
     * To select spl product set size family based on index
     *
     * @param index of the size swatch
     * @throws Exception - Exception
     */
    public void selectAllSplProductSetSizeBasedOnIndex(int index) throws Exception {
        if (lstSizeGrid.size() > 0) {
            int intNoOfProd;
            int NoOfSizeGrid = lstSizeGrid.size();
            for (intNoOfProd = 0; intNoOfProd < NoOfSizeGrid; intNoOfProd++) {
                List<WebElement> lstOfSizes = lstSizeGrid.get(intNoOfProd).findElements(By.cssSelector(".selectable input"));
                BrowserActions.scrollInToView(lstOfSizes.get(index), driver);
                BrowserActions.javascriptClick(lstOfSizes.get(index), driver, "Size Swatch");
                Utils.waitForPageLoad(driver);
            }
        }

    }

    /**
     * To select spl product set color family based on index
     *
     * @param index of the size swatch
     * @throws Exception - Exception
     */
    public void selectAllSplProductSetColorBasedOnIndex(int index) throws Exception {
        if (lstColorGrid.size() > 0) {
            int intNoOfProd;
            int NoOfColorGrid = lstColorGrid.size();
            for (intNoOfProd = 0; intNoOfProd < NoOfColorGrid; intNoOfProd++) {
                List<WebElement> lstOfColors = lstColorGrid.get(intNoOfProd).findElements(By.cssSelector(".selectable input"));
                BrowserActions.scrollInToView(lstOfColors.get(index), driver);
                BrowserActions.javascriptClick(lstOfColors.get(index), driver, "Color Swatch");
                Utils.waitForPageLoad(driver);
            }
        }
    }

    /**
     * To verify the product are in correct order in the overlay for a product
     * set
     *
     * @param productName - Name of all the products in which order it need to be
     *                    checked
     * @return True if its in correct order else false
     * @throws Exception - Exception
     */
    public boolean verifyOverlayProductName(String[] productName) throws Exception {
        int intNoOfProd;
        int NoOfProd = productName.length;
        String verifyText;
        if (Utils.isMobile()) {
            int index;
            for (index = 0; index < overlaySlickDot.size(); index++) {
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    overlaySlickDot.get(index + 1).click();
                } else {
                    overlaySlickDot.get(index).click();
                    if (!(productName[index].equals(lnkProductNameOverLay.getText()))) {
                        return false;
                    }
                }
            }
            return true;
        } else {
            for (intNoOfProd = 0; intNoOfProd < NoOfProd; intNoOfProd++) {
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    intNoOfProd = intNoOfProd - 1;
                    BrowserActions.javascriptClick(nextArrowOverlay, driver, "Next Arrow Overlay");
                } else {
                    verifyText = lnkProductNameActiveInMCOverLay.getText().toLowerCase().trim().replace("amp;", "");
                    if (!verifyText.equals(productName[intNoOfProd].toLowerCase().trim().replace("amp;", ""))) {
                        return false;
                    }
                    BrowserActions.javascriptClick(nextArrowOverlay, driver, "Next Arrow Overlay");
                }
            }
        }
        return true;
    }

    /**
     * To get number of products in product set
     *
     * @return Integer -
     * @throws Exception -
     */
    public int getNoOfProductInProductSet() throws Exception {
        return lblProductSetInvProdName.size();
    }

    /**
     * To get number of review
     *
     * @return Integer - No of review
     * @throws Exception - Exception
     */
    public int getNoOfReviews() throws Exception {
        String text = BrowserActions.getText(driver, txtReviewCount, "Review Count");
        int reviewCount = com.fbb.support.StringUtils.getNumberInString(text);
        return reviewCount;
    }

    /**
     * To click on number of review
     *
     * @throws Exception - Exception
     */
    public void clickOnNoOfReviews() throws Exception {
    	Utils.waitForElement(driver, txtReviewPdp);
        BrowserActions.clickOnElementX(txtReviewPdp, driver, "Review Count");
        Utils.waitForPageLoad(driver);
    }
    
    public void clickOnSearchedReviewBasedOnIndex(int index) throws Exception{
    	int size = lstSearchedReview.size();
    	if(index < size) {
	    	Utils.waitForElement(driver, lstSearchedReview.get(index));
	        BrowserActions.clickOnElementX(lstSearchedReview.get(index), driver, "Searched Review");
	        Utils.waitForPageLoad(driver);
    	} else {
    		Log.reference("There are only "+size+" review available!");
    	}
    }

    /**
     * To get number of displaying review
     *
     * @return Integer - No of review currently displaying
     * @throws Exception - Exception
     */
    public int getNoOfDisplayingReviews() throws Exception {
        return displayingReviewSection.size();
    }


    /**
     * To click Show more review link
     *
     * @throws Exception - Exception
     */
    public void clickShowMoreReviews() throws Exception {
        BrowserActions.clickOnElementX(lnkShowMoreReview, driver, "Show more review link");
        Utils.waitForPageLoad(driver);
    }

    /**
     * To get number products in special product set
     *
     * @return Integer -
     * @throws Exception -
     */
    public int getNoOfProductInSplProductSet() throws Exception {
        return lblSplProductSetComponent.size();
    }

    /**
     * to verify All product Added In overlay
     * input number of product
     *
     * @param NoOfProd -
     * @return String
     * @throws Exception - Exception
     */
    public boolean verifyAllProductsAddedInOverlay(int NoOfProd) throws Exception {
        int intNoOfProd;
        String verifyText;
        HashSet<String> productName = new HashSet<String>();
        if (Utils.isMobile()) {
            int index = 0;
            int NoOfBubbles = overlaySlickDot.size();
            overlaySlickDot.get(index).click();
            for (index = 0; index < NoOfBubbles; index++) {
                BrowserActions.scrollToBottomOfPage(driver);
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    overlaySlickDot.get(index + 1).click();
                } else {
                    overlaySlickDot.get(index).click();
                    verifyText = lnkProductNameOverLay.getText();
                    productName.add(verifyText);
                }
            }
            if (productName.size() < NoOfProd) {
                return false;
            }
        } else {
            for (intNoOfProd = 0; intNoOfProd < NoOfProd; intNoOfProd++) {
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    intNoOfProd = intNoOfProd - 1;
                    BrowserActions.javascriptClick(nextArrowOverlay, driver, "Next Arrow Overlay");
                } else {
                    verifyText = lnkProductNameOverLay.getText();
                    productName.add(verifyText);
                    BrowserActions.javascriptClick(nextArrowOverlay, driver, "Next Arrow Overlay");
                }
            }
            if (productName.size() < NoOfProd) {
                return false;
            }
        }
        return true;
    }

    /**
     * to select All product set color
     *
     * @param color -
     * @return String
     * @throws Exception - Exception
     */
    public String[] selectAllProductSetColor(String[] color) throws Exception {
        if (lstColorGrid.size() > 0) {
            int intNoOfProd;
            String[] productText = new String[10];
            List<WebElement> prdGrid = driver.findElements(By.cssSelector(".product-set-item"));
            int NoOfProd = prdGrid.size();
            int NoOfColorGrid = lstColorGrid.size();
            for (intNoOfProd = 0; intNoOfProd < NoOfProd; intNoOfProd++) {
                productText[intNoOfProd] = prdGrid.get(intNoOfProd).findElement(By.cssSelector(".product-name"))
                        .getText();
            }
            for (intNoOfProd = 0; intNoOfProd < NoOfColorGrid; intNoOfProd++) {
                WebElement colorToChoose = lstColorGrid.get(intNoOfProd)
                        .findElement(By.cssSelector(".selectable a img[alt='" + color[intNoOfProd] + "']"));
                BrowserActions.javascriptClick(colorToChoose, driver, "Color Swatch");
            }
            return productText;
        }
        return null;
    }

    /**
     * to select All product set color Based on Index
     *
     * @param index -
     * @throws Exception - Exception
     */
    public void selectAllProductSetColorBasedOnIndex(int index) throws Exception {
        int intNoOfProd;
        int NoOfColorGrid = lstColorGrid.size();
        for (intNoOfProd = 0; intNoOfProd < NoOfColorGrid; intNoOfProd++) {
            List<WebElement> lstOfColors = lstColorGrid.get(intNoOfProd).findElements(By.cssSelector(".selectable a img"));
            BrowserActions.javascriptClick(lstOfColors.get(index), driver, "Color Swatch");
            if (Utils.waitForElement(driver, waitLoader)) {
                Utils.waitUntilElementDisappear(driver, waitLoader);
            }
        }
    }

    /**
     * To get the number of 'Add to All Button' in the product set PDP page
     *
     * @return number of add to all button
     * @throws Exception -
     */
    public int getNumberOfAddToAllBtnInPrdSetPdpPage() throws Exception {
        return lstAddAllToBagBtn.size();
    }

    /**
     * to select single product component color
     *
     * @param productindex -
     * @param index        -
     * @return String
     * @throws Exception - Exception
     */
    public String selectSingleProductComponentColorBasedOnIndex(int productindex, int index) throws Exception {
        List<WebElement> lstOfColors = lstColorGrid.get(productindex).findElements(By.cssSelector(".selectable a img"));
        WebElement elem = lstOfColors.get(index);
        BrowserActions.javascriptClick(elem, driver, "Color Swatch");
        Utils.waitForPageLoad(driver);
        WebElement color = lstColorValue.get(productindex);
        return BrowserActions.getText(driver, color, "Color Selected");
    }

    /**
     * to select single product component size based on Index
     *
     * @param prodIndex -
     * @param index     -
     * @return String
     * @throws Exception - Exception
     */
    public String selectSingleProductComponentSizeBasedOnIndex(int index, int prodIndex) throws Exception {
        List<WebElement> lstOfSizes = lstSizeGrid.get(prodIndex).findElements(By.cssSelector(".selectable input"));
        WebElement elem = lstOfSizes.get(index);
        BrowserActions.javascriptClick(elem, driver, "Size Swatch");
        return BrowserActions.getText(driver, elem, "Size Selected");
    }
    
    /**
     * to click Add All to bag
     *
     * @param NoOfProd -
     * @throws Exception -
     */
    public void clickAddAllToBag(int... NoOfProd) throws Exception {
        BrowserActions.scrollInToView(btnAddAllToBag, driver);
        BrowserActions.clickOnElementX(btnAddAllToBag, driver, "Add All To Bag Button");
        if (Utils.waitForElement(driver, waitLoader)) {
            Utils.waitUntilElementDisappear(driver, waitLoader);
        }
        Utils.waitForElement(driver, lblActiveProductOverlay);
    }

    /**
     * to close prod set add to bag overlay by clicking empty space
     *
     * @throws Exception - Exception
     */
    public void closeProdSetAddToBagOverlayByClickingEmptySpace() throws Exception {
        int xScrollPosition = btnCloseMiniCartOverLay.getLocation().x + 30; //enter your x co-ordinate
        int yScrollPosition = btnCloseMiniCartOverLay.getLocation().y + 30; //enter your y co-ordinate

        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("window.scroll(" + xScrollPosition + ", " + yScrollPosition + ");");
        executor.executeScript("arguments[0].click();", btnCloseMiniCartOverLay);
        if (Utils.waitForElement(driver, waitLoader)) {
            Utils.waitUntilElementDisappear(driver, waitLoader);
        }
    }

    /**
     * to Check product main Image zoom content in pdp page
     *
     * @throws Exception - Exception
     */
    public void checkProductMainImageZoom() throws Exception {
        BrowserActions.mouseHover(driver, mainProdImage, "Main Product Image");
        Utils.waitForElement(driver, zoomLensProdImg);
    }
    
    /**
     * to get prodBadgeType in pdp page
     *
     * @return String
     * @throws Exception - Exception
     */
    public String getProdBadgeType() throws Exception {
        String imgSourceProdBadge;
        String[] imgSourceProdBadgeSplit;
        imgSourceProdBadge = prodBadge.findElement(By.cssSelector("img")).getAttribute("src");
        imgSourceProdBadgeSplit = imgSourceProdBadge.replaceAll("[^A-Za-z0-9]", "/").split("/");
        String prodBadgeType = imgSourceProdBadgeSplit[imgSourceProdBadgeSplit.length - 2];
        return prodBadgeType;
    }

    /**
     * to verify badge displayed to left of primary Image
     *
     * @return String
     * @throws Exception - Exception
     */
    public Boolean verifyBadgeDisplayedTopLeftOfPrimaryImage() throws Exception {
    	WebElement badgeElem = null;
    	if(Utils.isMobile()) {
    		badgeElem = imgProductBadge_Mobile;
    	} else {
    		badgeElem = imgProductBadge_Desktop_Tablet;
    	}
    	Point badge = badgeElem.findElement(By.cssSelector("img")).getLocation();
        Point image = imgProductPrimaryImage.findElement(By.cssSelector("img")).getLocation();
        Log.event(image+"||"+badge);
        if ( (badge == image) || (badge.getX() - image.getX() < 15 ))
            return true;
        else
            return false;
    }


    /**
     * To verify the Sale Price Has Price Range In Product Set
     *
     * @return boolean
     * @throws Exception - Exception
     */
    public boolean verifySalePriceHasPriceRangeInPrdSet() throws Exception {
        String price = null;
        if (Utils.isDesktop()) {
            price = BrowserActions.getText(driver, lblMainPricePrdSet, "Main Sale price Of the special product set");
        } else {
            price = BrowserActions.getText(driver, lblMainPricePrdSet_Mob_Tab, "Main Sale price Of the special product set");
        }
        if (price.contains(" - ")) {
            return true;
        }
        return false;
    }
    
     /**
     * To verify product badge type matches given type value
     *
     * @param type -
     * @return boolean -
     * @throws Exception -
     */
    public boolean verifyBadgeType(String type) throws Exception {
        if (prodBadge.getAttribute("src").replace("", "").toLowerCase().contains(type))
            return true;
        else
            return false;
    }

    /**
     * To click on nth breadcrumb link
     *
     * @param elementPos -
     * @return PlpPage -
     * @throws Exception -
     */
    public PlpPage clickOnBreadCrumb(int elementPos) throws Exception {
        if (Utils.isMobile()) {
            BrowserActions.clickOnElementX(backArrowIcon, driver, "Breadcrumb on mobile");
        } else {
            List<WebElement> prodBreadCrumbText = prodBreadCrumb
                    .findElements(By.cssSelector("a.breadcrumb-element.hide-mobile"));
            BrowserActions.javascriptClick(prodBreadCrumbText.get(elementPos - 1), driver, "Bread Crumb to PLP");
        }
        Utils.waitForPageLoad(driver);
        return new PlpPage(driver).get();
    }

    /**
     * To get total number of links available in breadcrumb
     *
     * @return Integer -
     * @throws Exception -
     */
    public int getTotalNoOfBreadCrumb() throws Exception {
        return lstOfBreadcrumbWithCurrentDesktop.size();
    }

    /**
     * To Get the count present in Bag Icon
     *
     * @return int
     * @throws Exception - Exception
     */
    public int getBagCount() throws Exception {
        int countToReturn;
        if (Utils.isMobile())
            countToReturn = Integer.parseInt(txtBagCountMobile.getText().trim().toString());
        else
            countToReturn = Integer.parseInt(txtBagCountDesktop.getText().trim().toString());

        Log.event("Count From Bag :: " + countToReturn);
        return countToReturn;
    }

    /**
     * To mouse hover on Add to bag button on Desktop To tab on AddToBag button
     * on Mobile/Tablet
     *
     * @throws Exception - Exception
     */
    public void mouseHoverAddToBag() throws Exception {
        if (Utils.isDesktop()) {
            BrowserActions.moveToElementJS(driver, btnAddToCart);
        }else{
        	BrowserActions.scrollToViewElement(btnAddToCart, driver);
        	btnAddToCart.click();
           //BrowserActions.clickOnElementX(btnAddToCart, driver, "Add To Bag");
        }
        Utils.waitForPageLoad(driver);
    }
    
    /**
     * To click on tablet slick slider dot's based on index
     *
     * @param index -
     * @throws Exception -
     */
    public void selectOverlaySlickDotsBasedOnIndex_tablet(int index) throws Exception {
        if (Utils.isTablet())
            overlaySlickDot.get(index).click();
    }

    /**
     * To get the button text of Add2Bag
     *
     * @return String -
     * @throws Exception - Exception
     */
    public String getAdd2BagBtnText() throws Exception {
        Log.event("BtnAddToCart Text :: " + BrowserActions.getText(driver, btnAddToCart, "Add To Bag Button "));
        return BrowserActions.getText(driver, btnAddToCart, "Add To Bag Button ");
    }

    /**
     * To click on continue to shopping bag button in ATB overlay
     *
     * @throws Exception -
     */
    public void clickOnContinueShoppingInMCOverlay() throws Exception {
        BrowserActions.scrollInToView(btnContinueShoppingInMCOverlay, driver);
    	BrowserActions.clickOnElementX(btnContinueShoppingInMCOverlay, driver, "Continue Shopping In Mini Cart Overlay ");
        if (Utils.waitForElement(driver, waitLoader)) {
            Utils.waitUntilElementDisappear(driver, waitLoader);
        }
    }

    /**
     * To get image source link of product primary image
     *
     * @return String -
     * @throws Exception -
     */
    public String getPrimaryImgSource() throws Exception {
        return imgPrimaryImage.getAttribute("src").trim();
    }

    /**
     * To get color code of product primary image
     *
     * @return String -
     * @throws Exception -
     */
    public String getPrimaryImgColorCode() throws Exception {
        String imgPath[] = imgPrimaryImage.getAttribute("src").trim().split("\\/");
        String imgCode = imgPath[imgPath.length - 1].split(".jpg")[0].replace(".jpg", "");
        return imgCode;
    }

    /**
     * To get color code of selected color swatch image
     *
     * @return String -
     * @throws Exception -
     */
    public String getSelectedColorCode(boolean... check) throws Exception {
        String imageCodeRef = imgSelectColorSwatch.getAttribute("src").trim();
        if (check.length > 0 && check[0]) {
            int count = 1;
            int noOfAvailableColorSwatch = driver.findElements(By.cssSelector(".swatches.color li.selectable")).size();
            while (imageCodeRef.contains("noimage") && count <= noOfAvailableColorSwatch) {
                selectAvailableColorByIndex(count);
                imageCodeRef = imgSelectColorSwatch.getAttribute("src").trim();
                count++;
            }
            if (count > noOfAvailableColorSwatch)
                Log.fail("Cannot Proceed due to there are no color swatch configured with appropriate color.");
        }
        String imgPath[] = imageCodeRef.split("\\/");
        String imgCode = imgPath[imgPath.length - 1].split("\\_")[3].replace(".jpg", "");
        return imgCode;
    }

    /**
     * To get color code of nth color swatch image
     *
     * @param index -
     * @return String -
     * @throws Exception -
     */
    public String getColorSwatchColorCodeByIndex(int index) throws Exception {
        String colorSrc[] = lstcolorSwatches.get(index - 1).getAttribute("src").trim().split("\\/");
        String imgCode = colorSrc[colorSrc.length - 1].split("\\_")[3].replace(".jpg", "");
        return imgCode;
    }

    /**
     * To get image source of primary image in minicart overlay
     *
     * @return String -
     * @throws Exception -
     */
    public String getMiniCartOverLayImgSource() throws Exception {
        return imgPrimaryImageInMCOverLay.getAttribute("src").trim();
    }
    
    
    /**
     * To get image source of primary image in minicart overlay
     *
     * @return String -
     * @throws Exception -
     */
    public void enterReviewSearchTerm(String term) throws Exception {
    	BrowserActions.typeOnTextField(inpReviewSearch, term, driver, "Review Search");
    }
    
    

    /**
     * To get image source of Active primary image in minicart overlay in product set
     *
     * @return String -
     * @throws Exception -
     */
    public String getMiniCartOverLayActiveImgSourceProductSet() throws Exception {
        return lnkProductImageActiveProdNameMCOverLay.getAttribute("src").trim();
    }

    /**
     * to get MiniCart Ovelay All Image Source product set
     *
     * @param NoOfProd -
     * @return String -
     * @throws Exception - Exception
     */
    public String[] getMiniCartOverLayAllImgSourceProductSet(int NoOfProd) throws Exception {
        String[] prodImgSrc = new String[NoOfProd + 1];

        if (Utils.isMobile()) {
            int prodCnt = 0;
            int index;
            for (index = 0; index < overlaySlickDot.size(); index++) {
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    if (index + 1 > overlaySlickDot.size()) {
                        overlaySlickDot.get(0).click();
                    } else {
                        overlaySlickDot.get(index + 1).click();
                    }
                } else {
                    overlaySlickDot.get(index).click();
                    prodImgSrc[prodCnt] = getMiniCartOverLayActiveImgSourceProductSet();
                    prodCnt++;
                }
            }
        } else {
            int prodCnt = 0;
            for (int intNoOfProd = 0; intNoOfProd < NoOfProd; intNoOfProd++) {
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    intNoOfProd = intNoOfProd - 1;
                    BrowserActions.javascriptClick(nextArrowOverlay, driver, "Next Arrow Overlay");
                } else {
                    prodImgSrc[prodCnt] = getMiniCartOverLayActiveImgSourceProductSet();
                    prodCnt++;
                    BrowserActions.javascriptClick(nextArrowOverlay, driver, "Next Arrow Overlay");
                }
            }
        }
        return prodImgSrc;
    }

    /**
     * To get product name from ATB overlay
     *
     * @return String -
     * @throws Exception -
     */
    public String getProductNameFromMCOverlay() throws Exception {
        return lnkProductNameInMCOverLay.getText();
    }

    /**
     * to Get product set individual product name from MC overlay
     *
     * @param NoOfProd -
     * @return String
     * @throws Exception - Exception
     */
    public String[] getProdSetIndvProductNameFromMCOverlay(int NoOfProd) throws Exception {
        String[] prodNameInProdSetOverLay = new String[NoOfProd];
        if (Utils.isMobile()) {
            int prodCnt = 0;
            int index;
            for (index = 0; index < overlaySlickDot.size(); index++) {
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    if (index + 1 > overlaySlickDot.size()) {
                        overlaySlickDot.get(0).click();
                    } else {
                        overlaySlickDot.get(index + 1).click();
                    }
                } else {
                    overlaySlickDot.get(index).click();
                    prodNameInProdSetOverLay[prodCnt] = lnkProductImageActiveMCOverLay.getText();
                    prodCnt++;
                }
            }
        } else {
            int prodCnt = 0;
            for (int intNoOfProd = 0; intNoOfProd < NoOfProd; intNoOfProd++) {
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    intNoOfProd = intNoOfProd - 1;
                    nextArrowOverlay.click();
                } else {
                    prodNameInProdSetOverLay[prodCnt] = lnkProductImageActiveProdNameMCOverLay.getText();
                    prodCnt++;
                    nextArrowOverlay.click();
                }
            }
        }
        return prodNameInProdSetOverLay;
    }

    /**
     * To get product primary image source in product set pdp
     *
     * @param prodIndex -
     * @return String -
     * @throws Exception -
     */
    public String getProdSetIndvProductImgSource(int prodIndex) throws Exception {
        return lstProdSetIndvProdImages.get(prodIndex).findElement(By.cssSelector("img")).getAttribute("src").trim();
    }

    /**
     * To get the component product details from product as link list
     *
     * @return LinkedList -
     * @throws Exception - Exception
     */
    public LinkedList<LinkedHashMap<String, String>> getProductDetailsFromProdSet() throws Exception {
        LinkedList<LinkedHashMap<String, String>> prdIndvData = new LinkedList<LinkedHashMap<String, String>>();
        LinkedHashMap<String, String> prdData = new LinkedHashMap<String, String>();
        int NoOfProd = getNoOfProductInProductSet();
        for (int intProd = 0; intProd < NoOfProd; intProd++) {
            prdData.put("Name", lblProductSetInvProdDetail.get(intProd).findElement(By.cssSelector(".product-name a")).getText());
            WebElement selectSize = lblProductSetInvProdDetail.get(intProd)
                    .findElement(By.cssSelector(".swatches.size .selectable.selected input"));
            if (Utils.waitForElement(driver, selectSize)) {
                prdData.put("Size", selectSize.getText().trim());
            } else {
                prdData.put("Size", "NA");
            }

            WebElement selectColor = lblProductSetInvProdDetail.get(intProd)
                    .findElement(By.cssSelector(".product-variations :nth-child(2).attribute .selected-value"));
            if (Utils.waitForElement(driver, selectColor)) {
                prdData.put("Color", selectColor.getText().trim());
            } else {
                prdData.put("Color", "NA");
            }

            WebElement selectQty = lblProductSetInvProdQuantity.get(intProd)
                    .findElement(By.cssSelector(".selected-option.selected"));

            if (Utils.waitForElement(driver, selectQty)) {
                prdData.put("Qty", selectQty.getText().trim());
            } else {
                prdData.put("Qty", "NA");
            }

            WebElement selectAvail = lblProductSetInvProdQuantity.get(intProd)
                    .findElement(By.cssSelector(".availability-msg"));
            if (Utils.waitForElement(driver, selectAvail)) {
                prdData.put("Avail", selectAvail.getText());
            } else {
                prdData.put("Avail", "NA");
            }

            WebElement selectPrice = lblProductSetInvProdDetail.get(intProd)
                    .findElement(By.cssSelector(".price-sales "));

            if (Utils.waitForElement(driver, selectPrice)) {
                prdData.put("Price", selectPrice.getText().trim());
            } else {
                prdData.put("Price", "NA");
            }

            prdIndvData.add(prdData);

        }

        return prdIndvData;
    }

    /**
     * To get component product details from product set as hash map
     *
     * @return LinkedHashMap -
     * @throws Exception -
     */
    public LinkedHashMap<String, LinkedHashMap<String, String>> getProductDetailsFromProdSetHashMap() throws Exception {
        LinkedHashMap<String, LinkedHashMap<String, String>> prdList = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        int NoOfProd = getNoOfProductInProductSet();
        int i = 0;
        for (int intProd = 0; intProd < NoOfProd; intProd++) {
            LinkedHashMap<String, String> prdData = new LinkedHashMap<String, String>();
            String HashKey = lblProductSetInvProdDetail.get(intProd).findElement(By.cssSelector(".product-name a")).getText().toLowerCase();//"Product" + i;
            prdData.put(new String("Name"), HashKey);
            WebElement selectSize = lblProductSetInvProdDetail.get(intProd).findElement(By.cssSelector(".swatches.size .selectable.selected input"));
            prdData.put(new String("Size"), new String(selectSize.getText().trim().toLowerCase()));
            WebElement selectColor = lblProductSetInvProdDetail.get(intProd).findElement(By.cssSelector(".product-variations :nth-child(1).attribute .selected-value"));
            prdData.put(new String("Color"), new String(selectColor.getText().trim().toLowerCase()));
            WebElement selectQty = lblProductSetInvProdQuantity.get(intProd).findElement(By.cssSelector(" .quantityinput option[selected]"));
            prdData.put(new String("Qty"), new String(BrowserActions.getTextFromAttribute(driver, selectQty, "value", "Quantity").trim().toLowerCase()));

            WebElement selectAvail = lblProductSetInvProdQuantity.get(intProd).findElement(By.cssSelector(".availability-msg"));
            String status = selectAvail.getText().toLowerCase();
            status = status.contains(",") ? status.split(",")[1] : status;
            prdData.put(new String("Avail"), status);
            WebElement selectPrice = lblProductSetInvProdDetail.get(intProd).findElement(By.cssSelector(".price-sales "));
            prdData.put(new String("Price"), new String(selectPrice.getText().trim().toLowerCase()));
            prdList.put(HashKey, prdData);
            i = i + 1;
        }

        return prdList;
    }
    
    
    /**
     * To get Rating From First Review
     * @return String - rating
     * @throws Exception - Exception
     */
    public String getRatingFromFirstReview() throws Exception {
    	return BrowserActions.getTextFromAttribute(driver, firstReview, "rating", "Review");
    }
    
    

    /**
     * To get product details from ATB Overlay
     *
     * @return LinkedHashMap -
     * @throws Exception -
     */
    public LinkedHashMap<String, LinkedHashMap<String, String>> getPrdDetailFromATBOverlay() throws Exception {
        LinkedHashMap<String, LinkedHashMap<String, String>> prdDetails = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        System.out.println("Number of product set" + getNoOfProductInProductSet());
        for (int i = 0; i < getNoOfProductInProductSet(); i++) {
            LinkedHashMap<String, String> prdDetail = new LinkedHashMap<String, String>();

            WebElement prdSection = driver.findElement(By.cssSelector("div.mini-cart-product[data-slick-index='" + i + "']"));
            prdDetail.put("Name", prdSection.findElement(By.cssSelector(".mini-cart-name a")).getAttribute("innerHTML").replace("amp;", "").toLowerCase());
            prdDetail.put("Size", prdSection.findElement(By.cssSelector("[data-attribute='size'] .value")).getAttribute("innerHTML").trim().toLowerCase());
            prdDetail.put("Color", prdSection.findElement(By.cssSelector("[data-attribute='color'] .value")).getAttribute("innerHTML").trim().toLowerCase());
            prdDetail.put("Qty", prdSection.findElement(By.cssSelector(".mini-cart-pricing > .value")).getAttribute("innerHTML").trim().toLowerCase());
            prdDetail.put("Avail", prdSection.findElement(By.cssSelector(".product-availability-list .is-in-stock")).getAttribute("innerHTML").trim().toLowerCase());
            prdDetail.put("Price", prdSection.findElement(By.cssSelector(".product-price .price-sales")).getAttribute("innerHTML").trim().toLowerCase());

            prdDetails.put(prdDetail.get("Name"), prdDetail);
        }
        return prdDetails;
    }

    /**
     * to get product set Details form MC Overlay for Product set Hash Map
     *
     * @param NoOfProd -
     * @return String
     * @throws Exception - Exception
     */
    public HashMap<String, HashMap<String, String>> getProductDetailsFromMCOverLayProdSetHashMap(int NoOfProd) throws Exception {
        HashMap<String, HashMap<String, String>> prdList = new HashMap<String, HashMap<String, String>>();
        int i = 0;
        if (!(Utils.isMobile())) {
            for (int intProd = 0; intProd < NoOfProd; intProd++) {
                HashMap<String, String> prdData = new HashMap<String, String>();
                String HashKey = "Product" + i;
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    intProd = intProd - 1;
                    nextArrowOverlay.click();
                } else {
                    prdData.put(new String("Name"), new String(lnkProductNameActiveInMCOverLay.getText()));

                    if (Utils.waitForElement(driver, lblProductSizeActiveInMCOverLay)) {
                        prdData.put("Size", lblProductSizeActiveInMCOverLay.getText().trim());
                    } else {
                        prdData.put("Size", "NA");
                    }

                    if (Utils.waitForElement(driver, lblProductColorActiveInMCOverLay)) {
                        prdData.put("Color", lblProductColorActiveInMCOverLay.getText().trim());
                    } else {
                        prdData.put("Color", "NA");
                    }

                    WebElement selectQty = lblProductQtyAndPriceActiveInMCOverLay.get(0);

                    if (Utils.waitForElement(driver, selectQty)) {
                        prdData.put("Qty", selectQty.getText().trim());
                    } else {
                        prdData.put("Qty", "NA");
                    }

                    if (Utils.waitForElement(driver, lblProductStockActiveInMCOverLay)) {
                        String availtext = lblProductStockActiveInMCOverLay.getText().trim();
                        if (availtext.contains(":")) {
                            String AvlText = availtext.split(":")[1].trim();
                            prdData.put("Avail", AvlText);
                        } else {
                            prdData.put("Avail", "NA");
                        }
                    } else {
                        prdData.put("Avail", "NA");
                    }

                    WebElement selectPrice = lblProductSalesPriceActiveInMCOverLay;

                    if (Utils.waitForElement(driver, selectPrice)) {
                        prdData.put("Price", selectPrice.getText().trim());
                    } else {
                        prdData.put("Price", "NA");
                    }

                    prdList.put(HashKey, prdData);
                    i = i + 1;
                    nextArrowOverlay.click();
                }
            }
        } else {
            int index;
            for (index = 0; index < overlaySlickDot.size(); index++) {
                HashMap<String, String> prdData = new HashMap<String, String>();
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    if (index + 1 > overlaySlickDot.size()) {
                        overlaySlickDot.get(0).click();
                    } else {
                        overlaySlickDot.get(index + 1).click();
                    }
                } else {
                    overlaySlickDot.get(index).click();
                    prdData.put("Name" + i, lnkProductNameActiveInMCOverLay.getText());

                    if (Utils.waitForElement(driver, lblProductSizeActiveInMCOverLay)) {
                        prdData.put("Size" + i, lblProductSizeActiveInMCOverLay.getText().trim());
                    } else {
                        prdData.put("Size" + i, "NA");
                    }

                    if (Utils.waitForElement(driver, lblProductColorActiveInMCOverLay)) {
                        prdData.put("Color" + i, lblProductColorActiveInMCOverLay.getText().trim());
                    } else {
                        prdData.put("Color" + i, "NA");
                    }

                    WebElement selectQty = lblProductQtyAndPriceActiveInMCOverLay.get(0);

                    if (Utils.waitForElement(driver, selectQty)) {
                        prdData.put("Qty" + i, selectQty.getText().trim());
                    } else {
                        prdData.put("Qty" + i, "NA");
                    }

                    if (Utils.waitForElement(driver, lblProductStockActiveInMCOverLay)) {
                        String availtext = lblProductStockActiveInMCOverLay.getText().trim();
                        if (availtext.contains(":")) {
                            availtext.split(":")[1].trim();
                        }
                        prdData.put("Avail" + i, availtext);
                    } else {
                        prdData.put("Avail" + i, "NA");
                    }

                    WebElement selectPrice = lblProductQtyAndPriceActiveInMCOverLay.get(1);

                    if (Utils.waitForElement(driver, selectPrice)) {
                        prdData.put("Price" + i, selectPrice.getText().trim());
                    } else {
                        prdData.put("Price" + i, "NA");
                    }

                    prdList.put("Product" + i, prdData);
                    i++;
                }
            }
        }
        return prdList;
    }

    /**
     * to get product Details form MC overlay product set
     *
     * @param NoOfProd -
     * @return String
     * @throws Exception - Exception
     */
    public LinkedList<LinkedHashMap<String, String>> getProductDetailsFromMCOverLayProdSet(int NoOfProd)
            throws Exception {
        LinkedList<LinkedHashMap<String, String>> prdIndvData = new LinkedList<LinkedHashMap<String, String>>();
        LinkedHashMap<String, String> prdData = new LinkedHashMap<String, String>();
        if (Utils.isDesktop()) {
            for (int intProd = 0; intProd < NoOfProd; intProd++) {
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    intProd = intProd - 1;
                    nextArrowOverlay.click();
                } else {
                    prdData.put("Name", lnkProductNameActiveInMCOverLay.getText());

                    if (Utils.waitForElement(driver, lblProductSizeActiveInMCOverLay)) {
                        prdData.put("Size", lblProductSizeActiveInMCOverLay.getText().trim());
                    } else {
                        prdData.put("Size", "NA");
                    }

                    if (Utils.waitForElement(driver, lblProductColorActiveInMCOverLay)) {
                        prdData.put("Color", lblProductColorActiveInMCOverLay.getText().trim());
                    } else {
                        prdData.put("Color", "NA");
                    }

                    WebElement selectQty = lblProductQtyAndPriceActiveInMCOverLay.get(0);

                    if (Utils.waitForElement(driver, selectQty)) {
                        prdData.put("Qty", selectQty.getText().trim());
                    } else {
                        prdData.put("Qty", "NA");
                    }

                    if (Utils.waitForElement(driver, lblProductStockActiveInMCOverLay)) {
                        String availtext = lblProductStockActiveInMCOverLay.getText().trim();
                        if (availtext.contains(":")) {
                            String AvlText = availtext.split(":")[1].trim();
                            prdData.put("Avail", AvlText);
                        } else {
                            prdData.put("Avail", "NA");
                        }
                    } else {
                        prdData.put("Avail", "NA");
                    }

                    WebElement selectPrice = lblProductQtyAndPriceActiveInMCOverLay.get(1);

                    if (Utils.waitForElement(driver, selectPrice)) {
                        prdData.put("Price", selectPrice.getText().trim());
                    } else {
                        prdData.put("Price", "NA");
                    }

                    prdIndvData.add(prdData);
                    nextArrowOverlay.click();
                }
            }
        } else if (Utils.isTablet()) {
            int index;
            for (index = 0; index < overlaySlickDot.size(); index++) {
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    if (index + 1 > overlaySlickDot.size()) {
                        overlaySlickDot.get(0).click();
                    } else {
                        overlaySlickDot.get(index + 1).click();
                    }
                } else {
                    overlaySlickDot.get(index).click();
                    //Thread.sleep(1500);
                    prdData.put("Name", lnkProductNameActiveInMCOverLay.getText());

                    if (Utils.waitForElement(driver, lblProductSizeActiveInMCOverLay)) {
                        prdData.put("Size", lblProductSizeActiveInMCOverLay.getText().trim());
                    } else {
                        prdData.put("Size", "NA");
                    }

                    if (Utils.waitForElement(driver, lblProductColorActiveInMCOverLay)) {
                        prdData.put("Color", lblProductColorActiveInMCOverLay.getText().trim());
                    } else {
                        prdData.put("Color", "NA");
                    }

                    WebElement selectQty = lblProductQtyAndPriceActiveInMCOverLay.get(0);

                    if (Utils.waitForElement(driver, selectQty)) {
                        prdData.put("Qty", selectQty.getText().trim());
                    } else {
                        prdData.put("Qty", "NA");
                    }

                    if (Utils.waitForElement(driver, lblProductStockActiveInMCOverLay)) {
                        String availtext = lblProductStockActiveInMCOverLay.getText().trim();
                        if (availtext.contains(":")) {
                            availtext.split(":")[1].trim();
                        }
                        prdData.put("Avail", availtext);
                    } else {
                        prdData.put("Avail", "NA");
                    }

                    WebElement selectPrice = lblProductQtyAndPriceActiveInMCOverLay.get(1);

                    if (Utils.waitForElement(driver, selectPrice)) {
                        prdData.put("Price", selectPrice.getText().trim());
                    } else {
                        prdData.put("Price", "NA");
                    }

                    prdIndvData.add(prdData);
                }
            }
        }
        return prdIndvData;
    }

    /**
     * get procut details form mc overlay
     *
     * @return String
     * @throws Exception - Exception
     */
    public HashMap<String, String> getProductDetailsFromMCOverLay() throws Exception {
        HashMap<String, String> prdData = new HashMap<String, String>();

        prdData.put("Name", getProductNameFromMCOverlay());
        if (Utils.waitForElement(driver, lblProductColorInMCOverLay)) {
            prdData.put("Color", lblProductColorInMCOverLay.getText().trim());
        }

        if (Utils.waitForElement(driver, lblProductSizeInMCOverLay)) {
            prdData.put("Size", lblProductSizeInMCOverLay.getText().trim());
        }

        if (Utils.waitForElement(driver, lblProductQtyInMCOverLay)) {
            prdData.put("Qty", lblProductQtyInMCOverLay.getText().trim());
        }

        if (Utils.waitForElement(driver, lblProductAvailInMCOverLay)) {
            prdData.put("Avail", lblProductAvailInMCOverLay.getText().split(":")[1].trim());
        }

        if (Utils.waitForElement(driver, lblProductPriceInMCOverLay)) {
            prdData.put("Price", lblProductPriceInMCOverLay.getText().trim());
        }

        if (Utils.waitForElement(driver, lblProductTotalInMCOverLay)) {
            prdData.put("Total", lblProductTotalInMCOverLay.getText().trim());
        }

        return prdData;
    }

    /**
     * Verify product details displayed in mc overlay
     * @return Boolean - True if "product name, color name, size, QTY, Availability, Price" is available else false
     * @throws Exception
     */
    public Boolean verifyProductDetailsDisplayedMCOverLay() throws Exception {
        Boolean status = false;
        
        if(Utils.waitForElement(driver, lblProductSizeInMCOverLay)) {
            status = BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(lnkProductNameInMCOverLay, lblProductColorInMCOverLay, lblProductSizeInMCOverLay, lblProductQtyInMCOverLay, lblProductAvailInMCOverLay, lblProductPriceInMCOverLay, lblProductTotalInMCOverLay));
        } else if(Utils.waitForElement(driver, lblProductShoeSizeInMCOverLay)) {
            status = BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(lnkProductNameInMCOverLay, lblProductColorInMCOverLay, lblProductShoeSizeInMCOverLay, lblProductShoeWidthInMCOverLay, lblProductQtyInMCOverLay, lblProductAvailInMCOverLay, lblProductPriceInMCOverLay, lblProductTotalInMCOverLay));
        } else if(Utils.waitForElement(driver, lblProductBraCupInMCOverLay)) {
            status = BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(lnkProductNameInMCOverLay, lblProductColorInMCOverLay, lblProductBraCupInMCOverLay, lblProductBraBandInMCOverLay, lblProductQtyInMCOverLay, lblProductAvailInMCOverLay, lblProductPriceInMCOverLay, lblProductTotalInMCOverLay));
        }
        return status;
    }
    
    /**
     * To get total product quantity of all products in ATB overlay
     *
     * @return String -
     * @throws Exception -
     */
    public String getTotalProductQty() throws Exception {
        int sum = 0;
        for (int i = 0; i < lstProductQtyInMCOverLay.size(); i++)
            sum += Integer.parseInt(lstProductQtyInMCOverLay.get(i).getText().trim());

        Log.event("Item Quantity :: " + Integer.toString(sum));
        return Integer.toString(sum);
    }

    /**
     * to verify item added message from property
     * @param boolean - isSingle
     * @return boolean - true/false if matches property
     * @throws Exception - Exception
     */
    public boolean verifyItemAddedMessageComingFromProperty(boolean isSingle) throws Exception {
        String text = BrowserActions.getText(driver, lblMiniCartOverlayTitle, "Add to bag overlay 'Item added message'");
        String propertyText;
        if(isSingle) {
        	propertyText = demandWareProperty.getProperty("SingleAddedMsg" + Utils.getCurrentBrandShort().toUpperCase());
        }else {
        	propertyText = demandWareProperty.getProperty("MultiAddedMsg" + Utils.getCurrentBrandShort().toUpperCase());
        }
        return propertyText.equals(text);
    }

    /**
     * To get product type
     * @return String - Product type
     * @throws Exception - Exception
     */
    public String getProductType() throws Exception {
        String className = pdpMain.getAttribute("class");
        String productDetail = pagePrdDetails.getAttribute("class");
        if (className.contains("standardproductset"))
            return "standardproductset";
        else if (className.contains("standardproduct")) {
            String size;
            try {
                size = driver.findElement(By.xpath("//div[@class='attribute_label']//h3[@class='label'][contains(text(),'Size')]//following-sibling::span")).getText().trim();
            } catch (NoSuchElementException e) {
                size = "";
            }

            if (productDetail.contains("e-giftcard-detail") || pdpMain.getAttribute("data-productinfo").contains("E-Gift Card"))
                return "electronic-gc";
            else if (productDetail.contains("giftcard-detail") || pdpMain.getAttribute("data-productinfo").contains("Gift Card"))
                return "physical-gc";
            else if (size.length() == 0 || size.equals("") || size.isEmpty())
                return "master";
            else if (size.length() > 0 || Utils.waitForElement(driver, selectedSize))
                return "variation";
            else
                return "master";
        }//standard products
        else if (className.contains("specialproductset")) {
            return "specialproductset";
        }//special product set
        else
            return "unidentified";
    }

    /**
     * To select random variation in PDP and return variation details
     *
     * @return HashMap -
     * @throws Exception -
     */
    public HashMap<String, String> selectVariation() throws Exception {
        HashMap<String, String> prdData = new HashMap<String, String>();

        String size = selectSize();
        String color = selectColor();
        String qty = selectQty();
        String sizeFamily = selectSizeFamily();
        //String width = selectshoeSizeWidth();
        if (isShoeSizeAvailable()) {
            selectshoeSizeWidth();
            if (isWidthAvailable())
                selectWidthBasedOnIndex(0);
            Log.event("Shoe size selected.");
        } else {
            if (isBraSizeAvailable()) {
                selectBraBandSize();
                if (isBraCupAvailable()) {
                    selectCupBasedOnIndex(0);
                }
                Log.event("Bra size selected.");
            }
        }

        prdData.put("Name", getProductName());
        prdData.put("Color", color);
        prdData.put("Size", size);
        prdData.put("Qty", qty);
        prdData.put("SizeFamily", sizeFamily);
        prdData.put("Price", getProductPrice());
        //prdData.put("Width", width);
        
        Log.event("Product Details :: " + prdData.toString());

        return prdData;
    }

    /**
     * To get product details of currently selected variation
     *
     * @return HashMap -
     * @throws Exception -
     */
    public HashMap<String, String> getProductInfo() throws Exception {
        selectSize();
        selectColor();
        HashMap<String, String> prdData = new HashMap<String, String>();

        prdData.put("Name", getProductName());
        prdData.put("Color", selectedColorVariant.getText().trim());
        prdData.put("Size", selectedSize.getText().trim());
        prdData.put("Qty", Integer.toString(getQuantity()));
        prdData.put("Price", getProductPrice());

        return prdData;
    }

    /**
     * To get product price from PDP
     *
     * @return String -
     * @throws Exception -
     */
    public String getProductPrice() throws Exception {
        if (Utils.waitForElement(driver, lblRegularPrice))
            return lblRegularPrice.getText().trim();
        else if (Utils.waitForElement(driver, lblSalePrice))
            return lblSalePrice.getText().trim();
        else
            return driver.findElement(By.cssSelector(".product-price")).getText();

    }

    /**
     * To get number of items in ATB overlay
     *
     * @return String -
     * @throws Exception -
     */
    public String getTotalItemCountInMCOverlay() throws Exception {
        if (Utils.isMobile()) {
            Log.event("Item Qty IN MCO :: " + lblItemInUrBagMCOverlayMobile.getAttribute("innerHTML").split("<span")[0].trim());
            return lblItemInUrBagMCOverlayMobile.getAttribute("innerHTML").split("<span")[0].trim();
        } else {
            Log.event("Item Qty IN MCO :: " + lblItemInUrBagMCOverlay.getAttribute("innerHTML").split("<span")[0].trim());
            return lblItemInUrBagMCOverlay.getAttribute("innerHTML").split("<span")[0].trim();
        }
    }

    /**
     * To get number of items in ATB overlay
     *
     * @return String -
     * @throws Exception -
     */
    public String getItemInYouBagMCOverlay() throws Exception {
        if (Utils.isMobile()) {
            Log.event("Item Qty IN MCO :: " + lblItemInUrBagMCOverlayMobile.getAttribute("innerHTML").trim());
            return lblItemInUrBagMCOverlayMobile.getAttribute("innerHTML").trim();
        } else {
            Log.event("Item Qty IN MCO :: " + lblItemInUrBagMCOverlay.getAttribute("innerHTML").trim());
            return lblItemInUrBagMCOverlay.getAttribute("innerHTML").trim();
        }
    }

    /**
     * To get number of items in ATB overlay
     *
     * @return String -
     * @throws Exception -
     */
    public String getTotalCostInMCOverlay() throws Exception {
        Log.event("Total Cost in ABT OVerlay :: " + lblProductTotalInMCOverLay.getAttribute("innerHTML"));
        return lblProductTotalInMCOverLay.getAttribute("innerHTML").trim().split("\\$")[1];
    }

    /**
     * To get number of items in ATB overlay
     *
     * @return String -
     * @throws Exception -
     */
    public String getSumOfAllProductPriceInMCOverlay() throws Exception {
        int sum = 0;

        for (int i = 0; i < lstProductPriceInMCOverLay.size(); i++)
            sum += Integer.parseInt(lstProductPriceInMCOverLay.get(i).getText().replace("$", "").trim());

        return Integer.toString(sum);
    }

    /**
     * To get number of items in ATB overlay
     *
     * @return String -
     * @throws Exception -
     */
    public String getSumOfAllProductQtyInMCOverlay() throws Exception {
        int sum = 0;

        for (int i = 0; i < lstProductQtyInMCOverLay.size(); i++)
            sum += Integer.parseInt(lstProductQtyInMCOverLay.get(i).getAttribute("innerHTML").trim());

        return Integer.toString(sum);
    }

    
    /**
     * To check The Number of items and total prices should be displayed in the overlay.
     *
     * @param NoOfProd          -
     * @param productTotalPrice -
     * @return boolean -
     * @throws Exception -
     */
    public boolean checkNoOfItemAndTotalPriceInMCOverlay(int NoOfProd, float productTotalPrice) throws Exception {
        String NoOfProdAndSubTotal = lblProductSubTotalAndNoOfItemMCOverlay.getText();
        int verifyNoOfProd = NoOfProd;
        if (Utils.isMobile()) {
            for (int index = 0; index < overlaySlickDot.size(); index++) {
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    if (index + 1 > overlaySlickDot.size()) {
                        overlaySlickDot.get(0).click();
                    } else {
                        overlaySlickDot.get(index + 1).click();
                    }
                    verifyNoOfProd = verifyNoOfProd + 1;
                } else {
                    overlaySlickDot.get(index).click();
                }
            }

            String textToVerify = Integer.toString(verifyNoOfProd) + " ITEM";
            if (!(NoOfProdAndSubTotal.toLowerCase().contains(textToVerify.toLowerCase()))) {
                return false;
            }
            if (!(NoOfProdAndSubTotal.contains(Float.toString(productTotalPrice)))) {
                return false;
            }

        } else {
            for (int intProd = 0; intProd < NoOfProd; intProd++) {
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    intProd = intProd - 1;
                    nextArrowOverlay.click();
                    verifyNoOfProd = verifyNoOfProd + 1;
                } else {
                    nextArrowOverlay.click();
                }
            }
            
            String textToVerify = Integer.toString(verifyNoOfProd) + " ITEMS";
            if (!(NoOfProdAndSubTotal.contains(textToVerify))) {
                return false;
            }
            if (!(NoOfProdAndSubTotal.contains(Float.toString(productTotalPrice)))) {
                return false;
            }
        }
        
        return true;
    }

    /**
     * To click on view full details link under product image in child product in product set by index
     *
     * @param index -
     * @throws Exception -
     */
    public void clickViewFullDetailsLinkInComponentProduct(int index) throws Exception {
        BrowserActions.javascriptClick(viewfullDetail.get(index), driver, "View Details");
    }
    
    /**
     * To click on view full details link under product image in child product in product set by index
     *
     * @param index -
     * @throws Exception -
     */
    public String getProductID() throws Exception {
    	String productInfo = BrowserActions.getTextFromAttribute(driver, pdpMain, "data-productinfo", "Product ID");
    	productInfo = productInfo.split(",")[1].split("product_id")[1];
    	productInfo = productInfo.split("]")[0].replaceAll("[^a-zA-Z0-9]", "");
    	if (productInfo.length() == 8) {
    		productInfo = productInfo.substring(0, productInfo.length() - 1) + "-" + productInfo.substring(productInfo.length()-1);
    	}
    	return productInfo;
    }
    

    /**
     * to verify Bredcrum is displayed or clickable
     *
     * @param str        -
     * @param validation -
     * @return Boolean
     * @throws Exception - Exception
     */
    public boolean validateBreadcrum(String str, String validation) throws Exception {
        boolean temp = false, flag = false;
        ;
        for (int i = 0; i < listbreadcrum.size(); i++) {
            if (validation == "clickable") {
                if (listbreadcrum.get(i).getText().equals(str)) {
                    String tag = listbreadcrum.get(i).getTagName();
                    temp = listbreadcrum.get(i).isEnabled();
                    List<String> clickableTags = Arrays.asList("input", "button", "a");

                    String stringValue = listbreadcrum.get(i).getAttribute("href");
                    if ((clickableTags.contains(tag) && stringValue != null) && temp) {
                        flag = true;
                        break;
                    }
                }
            } else if ((validation == "displayed")) {
                if (listbreadcrum.get(i).getText().equals(str)) {
                    flag = listbreadcrum.get(i).isDisplayed();
                    break;
                }
            }
        }
        return flag;
    }

    /**
     * to verify the product price
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyProductPriceDisplayed() throws Exception {
    	if (txtProdPrice.findElement(By.cssSelector("span[class='price-standard']")).isDisplayed()) {
    		if (txtProdPrice.findElement(By.cssSelector("span[class='price-standard']")).getCssValue("text-decoration").equals("line-through")) {
    			if (txtProdPrice.findElement(By.cssSelector("span[class='price-sales price-standard-exist']")).isDisplayed()) {
    				return true;
    			} else {
    				return false;
    			}     
            } else {
                return true;
            }
        } else {
        	return false;
        }
    }

    /**
     * to verify CallOut Message
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyCalloutMsgDisplayed() throws Exception {
        if (txtProductPrice.findElement(By.cssSelector("span[class='callout-message']")).isDisplayed()) {
            return true;
        }
        return false;
    }

    /**
     * To click on product review count
     *
     * @throws Exception -
     */
    public void clickOnProductReviewCount() throws Exception {
        BrowserActions.clickOnElementX(lblReviewCount, driver, "Product Review Count");
        Utils.waitForPageLoad(driver);
    }

    /**
     * To click on the alternative image slick dot
     *
     * @param index -
     * @throws Exception -
     */
    public void clickOnAltImageSlickDot(int index) throws Exception {
        if (Utils.isMobile()) {
        	if(btnSlickDots.size() > 0) {
        		BrowserActions.clickOnElementX(btnSlickDots.get(index - 1), driver, "Alternative image slickdot");
        	} else {
        		Log.reference("Slickdots are not displayed. Product may not have alternate images.", driver);
        	}
        }
        Utils.waitForPageLoad(driver);
    }
    
    /**
     * To click on the alternative image
     * @param index - index
     * @throws Exception - Exception
     */
    public boolean clickOnAlternateImages(int index) throws Exception {
    	List<WebElement> alternateImage ;
    	if (!Utils.isMobile()) {
    		alternateImage = alternateImages;
    	} else {
    		alternateImage = overlaySlickDot;
    	}
    	if(alternateImage.size() > index) {
    		try {
                BrowserActions.clickOnElementX(alternateImage.get(index), driver, "Alternative image");
            } catch(NoSuchElementException ex) {
            	Log.failsoft("Expected number of alternate imgaes are not available.", driver);
            }
            Utils.waitForPageLoad(driver);
            return true;
    	} else {
    		Log.reference(index+1 + " images are not available.", driver);
    		return false;
    	}
    }
    
    /**
     * to verify Color Swatch border is displayed for selected color swatch
     *
     * @param color -
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyColorSwatchBorder(String color) throws Exception {
        String borderswatch = BrowserActions.getTextFromAttribute(driver, colorSwatchselected, "alt", "color swatch");
        return (color.equalsIgnoreCase(borderswatch));
    }

    /**
     * To return color swatch count
     *
     * @return Integer -
     * @throws Exception -
     */
    public int colorSwatchCount() throws Exception {
        int count = lstcolorSwatches.size();
        return count;
    }

    /**
     * To get color swatch Y co-ordinate by index
     *
     * @param index -
     * @return Integer -
     * @throws Exception -
     */
    public int getCurrentLocationOfColorSwatchesByIndex(int index) throws Exception {
        WebElement image = lstcolorSwatches.get(index);
        Point point = image.getLocation();
        return point.getY();
    }

    /**
     * To get Current Location Of Color Swatches By Index for Prodset
     *
     * @param swatchlist -
     * @param index      -
     * @return Integer -
     * @throws Exception -
     */
    public int getCurrentLocationOfColorSwatchesByIndexforProdset(List<WebElement> swatchlist, int index)
            throws Exception {
        WebElement image = swatchlist.get(index);
        Point point = image.getLocation();
        return point.getY();
    }

    /**
     * To verify Breadcrumb is displayed at the top left corner in the window
     *
     * @return boolean true if Breadcrumb is displayed at the top left corner in
     * the window else false
     * @throws Exception - Exception
     */
    public boolean verifyBreadcrumbSymbol() throws Exception {
        boolean flag = false;
        BrowserActions.scrollToTopOfPage(driver);
        String txtBreadcrumb = divBreadcrumb.getText();

        if (txtBreadcrumb.contains("")) {
            flag = true;
        }
        return flag;
    }

    /**
     * To verify Quantity Dropdown
     *
     * @return boolean -
     * @throws Exception -
     */
    public boolean verifyQtyDropdown() throws Exception {
        int count = lstQty.size();
        boolean temp = true;
        for (int i = 0; i < count; i++) {
            String text = lstQty.get(i).getText();
            if (!(text.equals(Integer.toString(i + 1)))) {
                temp = false;
                break;
            }
        }
        return temp;
    }

    /**
     * To verify quantity dropdown in product set
     *
     * @param ProductID -
     * @return boolean -
     * @throws Exception -
     */
    public boolean verifyQtyDropdownProdSet(String ProductID) throws Exception {
        List<WebElement> qty = driver
                .findElements(By.cssSelector("div[id$='" + ProductID + "'] .quantity .selection-list li"));
        int count = qty.size();
        boolean temp = true;
        for (int i = 0; i < count; i++) {
            String text = qty.get(i).getAttribute("innerHTML").trim();
            int dropdownvalue = Integer.parseInt(text);
            if (!(dropdownvalue == (i + 1))) {
                temp = false;
                break;
            }
        }
        return temp;
    }

    /**
     * To get breadcrumb text
     *
     * @return String -
     * @throws Exception -
     */
    public String getBreadcrumbValue() throws Exception {
        String breadcrumbValue = BrowserActions.getText(driver, prodBreadCrumb, "Breadcrumb Value");
        return breadcrumbValue;
    }

    /**
     * To get index th string value of breadcrumb
     *
     * @param index -
     * @return String -
     * @throws Exception -
     */
    public String getBreadcrumbValueOf(int index) throws Exception {
        List<WebElement> bcEles = driver.findElements(By.cssSelector(".breadcrumb-element.hide-mobile"));
        String breadcrumbValue = bcEles.get(index - 1).getText();
        Log.event("Breadcrumb Value at #" + index + " :: " + breadcrumbValue);
        return breadcrumbValue;
    }

    /**
     * To move mouse cursor over See Details in promotion call outs
     *
     * @throws Exception -
     */
    public void mouseHoverOnSeeDetails() throws Exception {
        BrowserActions.mouseHover(driver, txtSeeDetails, "See Details Mouse hover");
        Actions action = new Actions(driver);
        action.moveToElement(txtSeeDetails).build().perform();
    }
    
    /**
     * To type email id in receipent fields
     *
     * @param recepientAddress -
     * @throws Exception -
     */
    public void enterRecepientEmailAdd(String recepientAddress) throws Exception {

        Utils.waitForElement(driver, txtRecepientEmail);
        BrowserActions.typeOnTextField(txtRecepientEmail, recepientAddress, driver, "Recepient Email address");
    }

    /**
     * To type email in confirm recipient email
     *
     * @param recepientAddress -
     * @throws Exception -
     */
    public void enterRecepientEmailConfirmAdd(String recepientAddress) throws Exception {
        BrowserActions.typeOnTextField(txtRecepientConfirmEmail, recepientAddress, driver,
                "Recepient confirm Email address");
    }

    /**
     * To type text in recipient name field
     *
     * @throws Exception -
     */
    public void enterRecepientName() throws Exception {

        String recepientName = RandomStringUtils.randomAlphabetic(5);
        BrowserActions.typeOnTextField(txtRecepientName, recepientName, driver,
                "Recepient Name");
    }

    /**
     * To get all child product price in product set as float array
     *
     * @return float[] -
     * @throws Exception -
     */
    public float[] getProductPricesOfIndvProductInProductSet() throws Exception {
        int NoOfProd = getNoOfProductInProductSet();
        float[] productPrice = new float[NoOfProd + 1];
        for (int intProd = 0; intProd < NoOfProd; intProd++) {
            WebElement selectPrice = lblProductSetInvProdDetail.get(intProd).findElement(By.cssSelector(".price-sales "));

            if (Utils.waitForElement(driver, selectPrice)) {
                productPrice[intProd] = Float.parseFloat(selectPrice.getText().substring(1));
            }
        }
        return productPrice;
    }

    /**
     * To get total price of products in product set
     *
     * @return float -
     * @throws Exception -
     */
    public float getSumOfProductPricesInProductSet() throws Exception {
        float productTotalPrice = 0;
        float productPrice[] = getProductPricesOfIndvProductInProductSet();
        for (int intProd = 0; intProd < productPrice.length; intProd++) {
            productTotalPrice = productPrice[intProd] + productTotalPrice;
        }
        return productTotalPrice;
    }

    /**
     * To validate email match between recipient email field vs confirm email field
     *
     * @return boolean -
     * @throws Exception -
     */
    public boolean validateRecepientEmailAddresses() throws Exception {
        boolean status = false;
        String recepientEmail = BrowserActions.getText(driver, txtRecepientEmail, "Recepient Email address");
        String recepientConfirmEmail = BrowserActions.getText(driver, txtRecepientConfirmEmail,
                "Recepient Email address");

        if (!recepientEmail.equals(recepientConfirmEmail)) {
            String recepientEmailError = BrowserActions.getText(driver, txtEmailAddressError,
                    "Recepient Email address");
            if (recepientEmailError.equals("Emails do not match.")) {
                status = true;
            }
        }
        return status;
    }

    /**
     * To clear text from recipient email addresses
     *
     * @throws Exception -
     */
    public void clearRecepientEmailAddress() throws Exception {
        BrowserActions.clearTextField(txtRecepientEmail, "Recepient Email address");
        BrowserActions.clearTextField(txtRecepientConfirmEmail, "Recepient confirm Email address");
        BrowserActions.scrollInToView(txtRecepientEmail, driver);
        BrowserActions.clickOnElementX(txtRecepientEmail, driver, "Recepient Email address");
    }

    /**
     * To validate error message below personalization message box
     *
     * @return boolean -
     * @throws Exception -
     */
    public boolean validateErrorMessage() throws Exception {
        boolean status = false;
        String errorString = BrowserActions.getText(driver, txtError, "Recepient Email address");

        if (errorString.equals("Please Select Recipient's Email Address , Confirm Email Address , Your name and message")) {
            status = true;
        }
        return status;
    }

    /**
     * To click on view icon in PDP
     *
     * @throws Exception -
     */
    public void clickVideoIcon() throws Exception {
        BrowserActions.clickOnElementX(videoIcon, driver, "Video Icon on PDP");
    }

    /**
     * To verify un available color swatch is grayed out for given size swatch
     *
     * @param sizeAvail    -
     * @param colorUnAvail -
     * @return boolean -
     * @throws Exception -
     */
    public boolean verifyColorSwatchGreyout(String sizeAvail, String colorUnAvail) throws Exception {
        selectSize(sizeAvail);
        String colorUnAvail1 = "//a[contains(translate(@title,'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'"
                + colorUnAvail.toLowerCase() + "')]//parent::li";

        Log.event("Given Size :: " + sizeAvail);
        Log.event("Given Color ::" + colorUnAvail);
        Log.event("Formed xpath :: " + colorUnAvail1);

        WebElement colorUnAvailEle = driver.findElement(By.xpath(colorUnAvail1));

        if (colorUnAvailEle.getAttribute("class").equals("unselectable"))
            return true;
        else
            return false;

    }
    
    /**
     * to click the add to bag button with select variations
     *
     * @throws Exception - Exception
     */
    public Object addToWishlist() throws Exception {
        selectAllSwatches();
        BrowserActions.clickOnElementX(lnkAddToWishList, driver, "Add to wishlist");
        if (Utils.waitForElement(driver, divSearchWishlist)) {
            return new WishlistLoginPage(driver).get();
        } else {
        	return this;
        }
    }

     /**
     * To verify if next link enabled for alternate images
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyNextLinkEnabled() throws Exception {
        if (btnNextImageEnable.getAttribute("class").contains("slick-disabled"))
            return false;
        else
            return true;
    }

    /**
     * To verify if previous link enabled for alternate images
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyPreviousLinkEnabled() throws Exception {
        if (btnPrevImageEnable.getAttribute("class").contains("slick-disabled"))
            return false;
        else
            return true;
    }

    /**
     * To click next link for alternate images
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyNextLinkGreyedOut() throws Exception {
        while (verifyNextLinkEnabled()) {
            BrowserActions.javascriptClick(btnNextImageEnable, driver, "Next");
        }
        int images = lstProductThumbnail.size();
        if (lstProductThumbnail.get(images - 1).getAttribute("class").contains("slick-active"))
            return true;
        else
            return false;
    }

    /**
     * To click previous link for alternate images
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyPreviousLinkGreyedOut() throws Exception {
        while (verifyPreviousLinkEnabled()) {
            BrowserActions.javascriptClick(btnPrevImageEnable, driver, "Previous");
        }
        if (lstProductThumbnail.get(0).getAttribute("class").contains("slick-active"))
            return true;
        else
            return false;
    }

    /**
     * if number of alternate images greater than 4. Then check if the 5th and above
     * alternate images are not active.
     *
     * @return boolean
     * @throws Exception - Exception
     */
    public Boolean verifyAlternateImagesNotActive() throws Exception {
        if (lstProductThumbnail.size() > 4) {
            for (int i = 4; i < lstProductThumbnail.size(); i++) {
                if (lstProductThumbnail.get(i).getAttribute("class").contains("slick-active"))
                    return false;
            }
        }
        return true;
    }

    /**
     * To verify size chart link availablity in page.
     *
     * @return boolean -
     * @throws Exception -
     */
    public boolean isSizeChartApplicable() throws Exception {
        if (Utils.waitForElement(driver, lnkSizechart))
            return true;
        else
            return false;
    }

    /**
     * to verify that the size chart link is displayed to the right of size
     * attribute
     *
     * @param productIndex -
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifySizeChartLinkRightOfSizeAttribute(int productIndex) throws Exception {
        if (isSizeChartApplicable()) {
            WebElement element = driver.findElement(By.cssSelector(".product-set-item:nth-of-type(" + productIndex + ") .attribute.size .label"));
            return BrowserActions.verifyHorizontalAllignmentOfElements(driver, lnkSizechart, element);
        } else
            Log.fail("Test Data not applicable for Size Chart.");
        return false;
    }

    /**
     * to select a size swatch
     *
     * @param productIndex -
     * @param index        -
     * @return String
     * @throws Exception - Exception
     */
    public String selectSizeSwatchSpecialProductSet(int productIndex, int index) throws Exception {
        List<WebElement> sizeSwatches = driver.findElements(By.cssSelector(
                ".product-set-item:nth-of-type(" + productIndex + ") .attribute.size > div  > ul > li"));
        if (Utils.getRunBrowser(driver).toLowerCase().contains("edge")) {
            BrowserActions.scrollInToView(sizeSwatches.get(index), driver);
        }
        String size = sizeSwatches.get(index).findElement(By.cssSelector("input")).getText().trim();
        if (sizeSwatches.get(index).getAttribute("class").contains("selected") == false)
            BrowserActions.javascriptClick(sizeSwatches.get(index).findElement(By.cssSelector("input")), driver,
                    "Size Swatch");
        return size;
    }
    /**
     * To get the child product names of product set
     * @return list of product set names
     * @throws Exception
     */
    public List<String> getChildProductNamesOfProductSet() throws Exception{
    	List<String> prdNames = new ArrayList<String>();
    	for(WebElement elem: lblProductSetInvProdName){
    		String name= BrowserActions.getText(driver, elem, "product set Name").toLowerCase();
    		prdNames.add(name);
    	}
    	
    	return prdNames;
    }
    
    /**
     * Select product set color swatche based on the index
     * @return string
     * @throws Exception
     */
    public String selectProductSetSizeSwatchBasedOnIndex(int productIndex, int... index) throws Exception {
    	if(Utils.isMobile()) {
    		OpenOrCloseShopNowDrawerBasedOnIndex(productIndex,"opened");
    	}
    	if(index.length>0) {
    		WebElement elem=lstItemsInProductSet.get(productIndex).findElements(By.cssSelector(".attribute.size .swatches.size li")).get(index[0]);
	    	try {
		    	if(Utils.waitForElement(driver, elem.findElement(By.cssSelector(".selected")))) {
		    		WebElement element=lstItemsInProductSet.get(productIndex).findElement(By.cssSelector(".attribute.size .selected-value"));
		    		return BrowserActions.getText(driver, element, "Selected size value");
		    	}
	    	}catch(NoSuchElementException ex) {
	    		Log.message( "Respective size is not selected");
	    	}try {
	    		if(Utils.waitForElement(driver,elem.findElement(By.cssSelector(".unselectable")))){
	    			return "size is not selectable";
	    		}
	    	}catch (NoSuchElementException e) {
	       		Log.message( "Respective size is not selectable");
	   			WebElement element=elem.findElement(By.cssSelector(".selectable a"));
	    		BrowserActions.clickOnElementX(element, driver, "selected size");
	    		}
    		}else {
    			List<WebElement> elem=lstItemsInProductSet.get(productIndex).findElements(By.cssSelector(".attribute.size .swatches.size li"));
    			for(WebElement element: elem) {
	        		try {
	        			if((Utils.waitForElement(driver, element.findElement(By.cssSelector(".selected"))))) {
			        		WebElement element1=lstItemsInProductSet.get(productIndex).findElement(By.cssSelector(".attribute.size .selected-value"));
			        		return BrowserActions.getText(driver, element1, "Selected size value");
		        		}
		        	}catch(NoSuchElementException ex) {
		        		Log.message("Size is not selected");
		        	}
	        		
		    		try{
		    			WebElement element2 = element.findElement(By.cssSelector(".selectable a"));
		        		BrowserActions.clickOnElementX(element2, driver, "size");
		        		Utils.waitUntilElementDisappear(driver, waitLoader);
			   	    	Utils.waitForPageLoad(driver);
		        		WebElement element1=lstItemsInProductSet.get(productIndex).findElement(By.cssSelector(".attribute.size .selected-value"));
		        		return BrowserActions.getText(driver, element1, "Selected size value");
		        	}catch(NoSuchElementException ep) {
		    			Log.message("Size is not selected");
		    		} 
    			}
    		}   	
    	return null;    	
    }
    
    /**
     * To Select color swatch based on the product index
     * @return String
     * @throws Exception
     */
    public String selectProductSetColorSwatchBasedOnIndex(int productIndex, int... index) throws Exception{
    	if(Utils.isMobile()) {
    		OpenOrCloseShopNowDrawerBasedOnIndex(productIndex,"opened");
    	}
    	if(index.length>0) {
    		WebElement elem = lstItemsInProductSet.get(productIndex).findElements(By.cssSelector(".attribute.color .swatches.color li")).get(index[0]);
    		try {
	    	 	if(Utils.waitForElement(driver, elem.findElement(By.cssSelector(".selected")))) {
		    		return BrowserActions.getText(driver, lstColorValue.get(productIndex), "color");
	    	 	}
    		}catch(NoSuchElementException ex) {
    			Log.message( "Respective color is not selected");
    		}
	    	try{
	    		if(Utils.waitForElement(driver, elem.findElement(By.cssSelector(".unselectable")))) {
	        		return "Color is not selectable";
	    		}
	    	}catch(NoSuchElementException e) {
	    		Log.message( "Respective color is not selectable");    	 
	    	}   	
	    	WebElement element=elem.findElement(By.cssSelector(".selectable a"));
	    	BrowserActions.clickOnElementX(element, driver, "selected color");
	    	Utils.waitUntilElementDisappear(driver, waitLoader);
   	    	Utils.waitForPageLoad(driver);
	    	return BrowserActions.getText(driver, lstColorValue.get(productIndex), "color");
	    
    	}else{
    		List<WebElement> elem = lstItemsInProductSet.get(productIndex).findElements(By.cssSelector(".attribute.color .swatches.color li"));
    		for(WebElement element : elem){
    			try{
    				if(Utils.waitForElement(driver, element.findElement(By.cssSelector(".selected")))) {
    					return BrowserActions.getText(driver, lstColorValue.get(productIndex), "color");
    				}
    			}catch(NoSuchElementException ex) {
    				Log.message("Color is not selected");
    			}
	   			WebElement element2 = element.findElement(By.cssSelector(".selectable a"));
	   	    	BrowserActions.clickOnElementX(element2, driver, "select color");
	   	    	Utils.waitUntilElementDisappear(driver, waitLoader);
	   	    	Utils.waitForPageLoad(driver);
	   	    	return BrowserActions.getText(driver, lstColorValue.get(productIndex), "color");    			
    		}
    	}
    return null;    	
    }
    
    /**
     * To click product set add to bag button based on index
     *@throws Exception 
     */
    
    public void clickProductSetAddToBagBasedOnIndex(int productIndex) throws Exception{
    	if(Utils.isMobile()) {
    		OpenOrCloseShopNowDrawerBasedOnIndex(productIndex,"opened");    		
    	}
    	try{
    		WebElement elem= lstItemsInProductSet.get(productIndex).findElement(By.cssSelector(" .add-to-cart"));
	    	if(Utils.waitForElement(driver, elem)) {
		    	BrowserActions.scrollToView(elem, driver);
		    	BrowserActions.clickOnElementX(elem, driver, "add to bag button");
	    	}
    	}catch(NoSuchElementException e) {
    		Log.message("add to bag button is not available for the product");
    	}
    	Utils.waitForPageLoad(driver);
    }
    /**
     * Add all products to cart from the product set
     * @throws Exception
     */
    
    public void addAllProductsToCartFromPrdSet() throws Exception{
    	for(int i=0;i<lstItemsInProductSet.size();i++) {    		
    		String color = selectProductSetColorSwatchBasedOnIndex(i);
    		String size = selectProductSetSizeSwatchBasedOnIndex(i);
    		clickProductSetAddToBagBasedOnIndex(i);
    		clickOnContinueShoppingInMCOverlay(); 
    		Log.message("Color is: "+color + ".. Size is: "+size);
    	} 	
    }
    /**
     * to select a size swatch
     *
     * @param index        -
     * @param productIndex -
     * @return String
     * @throws Exception - Exception
     */
    public String selectColorSwatchSpecialProductSet(int productIndex, int index) throws Exception {
        WebElement colorSwatches = driver.findElement(By.cssSelector(".product-set-item:nth-of-type(" + productIndex
                + ") .attribute.color > div  > ul > li:nth-of-type(1)"));
        if (colorSwatches.getAttribute("class").contains("selected") == false) {
            BrowserActions.clickOnSwatchElement(colorSwatches.findElement(By.cssSelector("input")), driver, "Color Swatch");
        }
        colorSwatches = driver.findElement(By.cssSelector(".product-set-item:nth-of-type(" + productIndex
                + ") .attribute.color > div  > ul > li:nth-of-type(1)"));
        return colorSwatches.findElement(By.cssSelector("img")).getAttribute("alt").trim();
    }

    public int getNoOfWidthSwatchesInSplProdSet() throws Exception {
        return lstWidthGrid.size();
    }

    /**
     * to select a width swatch
     *
     * @param index        -
     * @param productIndex -
     * @return String
     * @throws Exception - Exception
     */
    public String selectWidthSwatchSpecialProductSet(int productIndex, int index) throws Exception {
        if (lstWidthGrid.size() > 0) {
            List<WebElement> widthSwatches = driver.findElements(By.cssSelector(
                    ".product-set-item:nth-of-type(" + productIndex + ") .attribute:nth-of-type(3) > div  > ul > li"));
            if (widthSwatches.get(index).getAttribute("class").contains("selected") == false)
                BrowserActions.clickOnSwatchElement(widthSwatches.get(index).findElement(By.cssSelector("input")), driver,
                        "Color Swatch");
            return widthSwatches.get(index).findElement(By.cssSelector("input")).getText();
        }
        return null;
    }

    /**
     * to select a width swatch
     *
     * @param productIndex -
     * @return String
     * @throws Exception - Exception
     */
    public String getChildImageSpecialProductSet(int productIndex) throws Exception {
        WebElement image = driver.findElement(
                By.cssSelector(".product-set-item:nth-of-type(" + productIndex + ") .product-set-product-image img"));
        return image.getAttribute("src").trim();
    }
    
    /**
     * to select a width swatch
     *
     * @param productIndex -
     * @return String
     * @throws Exception - Exception
     */
    public void clickCustomerGalleryImageByIndex(int index) throws Exception {
    	BrowserActions.clickOnElementX(lstImgCustGal.get(index-1), driver, "Cust Gallery");
    	Utils.waitForElement(driver, custGalleryContainer);
    }
    
    
    /**
     * to select a width swatch
     *
     * @param productIndex -
     * @return String
     * @throws Exception - Exception
     */
    public void clickCloseCustomerGallery() throws Exception {
    	BrowserActions.clickOnElementX(custGalleryClose, driver, "Cust Gallery close");
    	Utils.waitUntilElementDisappear(driver, custGalleryContainer);
    }

    /**
     * to verify is selected color swatch displayed as primary product image
     *
     * @param productIndex -
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyIfSelectedColorDisplayedAsPrimaryProductImage(int productIndex) throws Exception {
        String primaryImage = imgProductPrimaryImage.findElement(By.cssSelector("img")).getAttribute("src");
        List<WebElement> colorSwatches = driver.findElements(By.cssSelector(
                ".product-set-item:nth-of-type(" + productIndex + ") .attribute:nth-of-type(2) > div  > ul > li"));
        for (WebElement color : colorSwatches) {
            if (color.getAttribute("class").contains("selected")) {
                String[] selectedImage = color.findElement(By.cssSelector("a")).getAttribute("data-lgimg").split("\\/");
                String image = selectedImage[selectedImage.length - 1].split("\\.")[0];
                if (primaryImage.contains(image))
                    return true;
            }
        }
        return false;
    }
    
    /**
     * Verify display image corresponds to color-swatch selected
     * @return boolean - true/false display image is showing correctly
     * @throws Exception - Exception
     */
    public boolean verifyDisplayImageMatchesSwatchColor() throws Exception{
    	String SwatchColor = null;
    	String primaryImage = imgProductPrimaryImage.findElement(By.cssSelector("img")).getAttribute("data-replaceimage");
    	if(primaryImage.contains("noimagefound") || primaryImage.contains("image-coming-soon")) {
    		Log.event("Product display images not fully configured.");
    		return true;
    	}else {
    		primaryImage = primaryImage.split("hi-res")[1].split("\\.")[0].replaceAll("mc", "");
    	}
    	Log.event("primaryImage:: " + primaryImage);
    	
    	String primaryImageColorized = imgProductPrimaryImage.findElement(By.cssSelector("img")).getAttribute("src");
    	if(!primaryImageColorized.contains("mc")) {
    		selectColor();
        	primaryImageColorized = primaryImageColorized.split("hi-res")[1].split("\\.")[0].replaceAll("mc", "");
        	Log.event("primaryImageColorized:: " + primaryImageColorized);
    	}
    	
    	try {
    		SwatchColor = selectedColor.findElement(By.cssSelector("img")).getAttribute("src");
    		if(SwatchColor.contains("NO-SWATCH-TAB")) {
    			Log.event("Product color swatches not fully configured.");
    			return true;
    		}
    		SwatchColor = SwatchColor.split("swatch")[1].split("\\.")[0].replaceAll("zs", "");
    		Log.event("SwatchColor:: " + SwatchColor);
    	}catch(Exception e) {
    		Log.event("No color is currently selected");
    		return false;
    	}
    	
    	if(primaryImage.equals(SwatchColor) || primaryImageColorized.equals(SwatchColor)) {
    		return true;
    	} else if(primaryImageColorized.equals(primaryImage)) {
    		Log.event("Product's colorized imaged are not configured. Master image will display for colorized image.");
    	}
    	return false;
    }

    /**
     * To verify If Selected Color Displayed As Primary Product Image Special Product set
     *
     * @param productIndex -
     * @return boolean -
     * @throws Exception -
     */
    public boolean verifyIfSelectedColorDisplayedAsPrimaryProductImageSplPrdSet(int productIndex) throws Exception {
        String primaryImage = imgProductPrimaryImage.findElement(By.cssSelector("img")).getAttribute("title");
        WebElement colorSwatches = driver.findElement(By.cssSelector(".product-set-item:nth-of-type(" + productIndex + ") .attribute.color .selected-value"));
        String image = colorSwatches.getText();
        System.out.println(" -- -- -- - In image -- " + primaryImage + " -- - --- - --- In swatch --  " + image);
        if (primaryImage.toLowerCase().trim().contains(image.toLowerCase().trim())) {
            return true;
        }
        return false;
    }
    
    /**
     * To verify If Selected Color Displayed As Primary Product Image Product set
     *
     * @param productIndex -
     * @return boolean -
     * @throws Exception -
     */
    public boolean verifyIfSelectedColorDisplayedAsPrimaryProductImagePrdSet(int productIndex) throws Exception {
        String primaryImage = lstItemsInProductSet.get(productIndex-1).findElement(By.cssSelector(".product-set-image img")).getAttribute("title");
        WebElement colorSwatches = driver.findElement(By.cssSelector(".product-set-item:nth-of-type(" + productIndex + ") .attribute.color .selected-value"));
        String image = colorSwatches.getText();
        System.out.println(" -- -- -- - In image -- " + primaryImage + " -- - --- - --- In swatch --  " + image);
        if (primaryImage.toLowerCase().trim().contains(image.toLowerCase().trim())) {
            return true;
        }
        return false;
    }
    

    /**
     * to verify is selected color swatch displayed as primary product image
     *
     * @param productIndex -
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyIfSelectedColorHasBorder(int productIndex) throws Exception {
        List<WebElement> colorSwatches = driver.findElements(By.cssSelector(
                ".product-set-item:nth-of-type(" + productIndex + ") .attribute.color > div  > ul > li"));
        for (WebElement color : colorSwatches) {
            if (color.getAttribute("class").contains("selected")) {
                if (color.findElement(By.cssSelector("input")).getCssValue("border").equals("1px solid"))
                    return true;
            }
        }
        return false;
    }

    /**
     * To verify If Selected Color Has Border In Special Product Set
     *
     * @param productIndex -
     * @return -
     * @throws Exception -
     */
    public Boolean verifyIfSelectedColorHasBorderInSplPrdSet(int productIndex) throws Exception {
        WebElement color = driver.findElement(By.cssSelector(".product-set-item:nth-of-type(" + productIndex + ") .attribute.color .selected"));
        BrowserActions.scrollToViewElement(color, driver);
        if (color.getAttribute("class").contains("selected")) {
            if (!(color.findElement(By.cssSelector("input")).getCssValue("border").isEmpty()))
                return true;
        }
        return false;
    }


    /**
     * to verify is selected color swatch displayed as primary product image
     *
     * @param NoOfProd -
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyTheProductNameRightToProductImage(int NoOfProd) throws Exception {
        if (Utils.isMobile()) {
            int index;
            for (index = 0; index < overlaySlickDot.size(); index++) {
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    if (index + 1 > overlaySlickDot.size()) {
                        overlaySlickDot.get(0).click();
                        if (!(BrowserActions.verifyHorizontalAllignmentOfElements(driver,
                                lnkProductNameActiveInMCOverLay, lnkProductImageActiveProdNameMCOverLay))) {
                            return false;
                        }
                    } else {
                        overlaySlickDot.get(index + 1).click();
                        if (!(BrowserActions.verifyHorizontalAllignmentOfElements(driver,
                                lnkProductNameActiveInMCOverLay, lnkProductImageActiveProdNameMCOverLay))) {
                            return false;
                        }
                    }
                } else {
                    overlaySlickDot.get(index).click();
                    if (!(BrowserActions.verifyHorizontalAllignmentOfElements(driver, lnkProductNameActiveInMCOverLay,
                            lnkProductImageActiveProdNameMCOverLay))) {
                        return false;
                    }
                }
            }
        } else {
            for (int intNoOfProd = 0; intNoOfProd < NoOfProd; intNoOfProd++) {
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    intNoOfProd = intNoOfProd - 1;
                    if (!(BrowserActions.verifyHorizontalAllignmentOfElements(driver, lnkProductNameActiveInMCOverLay,
                            lnkProductImageActiveProdNameMCOverLay))) {
                        return false;
                    }
                    BrowserActions.javascriptClick(nextArrowOverlay, driver, "Next Arrow Overlay");
                } else {
                    if (!(BrowserActions.verifyHorizontalAllignmentOfElements(driver, lnkProductNameActiveInMCOverLay,
                            lnkProductImageActiveProdNameMCOverLay))) {
                        return false;
                    }
                    BrowserActions.javascriptClick(nextArrowOverlay, driver, "Next Arrow Overlay");
                }
            }
        }
        return true;
    }

    /**
     * To verify The Position of Product quantity should be in the below the product size.
     *
     * @param NoOfProd -
     * @return boolean -
     * @throws Exception -
     */
    public Boolean verifyTheProductQtyBelowToProductSize(int NoOfProd) throws Exception {
        if (Utils.isMobile()) {
            int index;
            for (index = 0; index < overlaySlickDot.size(); index++) {
                if (!(Utils.waitForElement(driver, lblProductOverLayPrice))) {
                    if (index + 1 > overlaySlickDot.size()) {
                        overlaySlickDot.get(0).click();
                    } else {
                        overlaySlickDot.get(index + 1).click();
                    }
                } else {
                    overlaySlickDot.get(index).click();
                    if (!(BrowserActions.verifyVerticalAllignmentOfElements(driver, lblProductSizeActiveInMCOverLay,
                            lblProductQtyAndPriceActiveInMCOverLay.get(0)))) {
                        return false;
                    }
                }
            }
        } else {
            for (int intNoOfProd = 0; intNoOfProd < NoOfProd; intNoOfProd++) {
                BrowserActions.scrollToViewElement(lblProductSizeActiveInMCOverLay, driver);
                if (!(BrowserActions.verifyVerticalAllignmentOfElements(driver, lblProductSizeActiveInMCOverLay,
                        lblProductQtyAndPriceActiveInMCOverLay.get(0)))) {
                    return false;
                }
                BrowserActions.javascriptClick(nextArrowOverlay, driver, "Next Arrow Overlay");
            }
        }
        return true;
    }

    /**
     * to verify if color is out of stock
     *
     * @param sizeSwtchId -
     * @param prdID       -
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyColorIsOutOfStock(String prdID, String sizeSwtchId) throws Exception {
        WebElement elem = driver.findElement(By.cssSelector("div.product-set-item[id*='" + prdID + "']"));
        WebElement color = elem.findElement(By.cssSelector(".attribute.color .selectable:not(.selected)"));

        BrowserActions.scrollInToView(color, driver);
        BrowserActions.javascriptClick(color, driver, "color");
        Utils.waitForPageLoad(driver);

        WebElement size = elem.findElement(By.cssSelector(".swatches.size li a[title='Select size: " + sizeSwtchId.toUpperCase() + "']"));

        BrowserActions.scrollInToView(size, driver);
        BrowserActions.javascriptClick(size, driver, "Size");
        Utils.waitForPageLoad(driver);

        color = elem.findElement(By.cssSelector(".attribute.color .unselectable"));
        if (Utils.waitForElement(driver, color)) {
            return true;
        }
        return false;
    }

    /**
     * to verify if size is out of stock
     *
     * @param prdID       -
     * @param coloSwtchId -
     * @return Boolean -
     * @throws Exception -
     */
    public Boolean verifySizeIsOutOfStock(String prdID, String coloSwtchId) throws Exception {
        try {
            WebElement elem = driver.findElement(By.cssSelector("div.product-set-item[id*='" + prdID + "']"));
            WebElement size = elem.findElement(By.cssSelector(".swatches.size .selectable:not(.selected)"));

            BrowserActions.scrollInToView(size, driver);
            BrowserActions.javascriptClick(size, driver, "Size");
            Utils.waitForPageLoad(driver);

            WebElement color = elem.findElement(By.cssSelector(".attribute.color li a[title*='" + coloSwtchId.toUpperCase() + "']"));

            BrowserActions.scrollInToView(color, driver);
            BrowserActions.javascriptClick(color, driver, "color");
            Utils.waitForPageLoad(driver);

            size = elem.findElement(By.cssSelector(".swatches.size .unselectable"));
            if (Utils.waitForElement(driver, size)) {
                return true;
            }
        } catch (NoSuchElementException e) {
            return false;
        }
        return false;
    }

    /**
     * to click Add To Bag in special product set
     *
     * @throws Exception - Exception
     */
    public void addToBagSpecialProductSet() throws Exception {
        BrowserActions.javascriptClick(btnAddToBagSpecialProductSet, driver, "Add to bag");
        Utils.waitForPageLoad(driver);
    }
    /**
     * To click choose items below button
     * @Return void
     */
    public void clickChooseitemsBelowProductSet() throws Exception{    	
    	BrowserActions.scrollToView(btnChooseItemsBelowPrdSet, driver);
    	BrowserActions.clickOnElement(btnChooseItemsBelowPrdSet, driver, "Choose items below link");
    }
    
    /**To verify product image name and price should be above the shop now button
     *@return boolean
     *@throws Exception 
     */
    public boolean verifyPrdImgAndPriceDisplayedAboveShopNowButtonOrNot() throws Exception{
    	
    	if(Utils.waitForElement(driver, fistItemInProductSet)){
    		for(WebElement elem: lstItemsInProductSet){
    			
    			WebElement prdImage=elem.findElement(By.cssSelector(" .product-set-image"));
    			WebElement prdPrice = elem.findElement(By.cssSelector(" .product-price"));
    			WebElement btnShopNow = elem.findElement(By.cssSelector(" .product-variations-toggle.hide-tablet.hide-desktop"));  			
    		
    			if(!(BrowserActions.verifyVerticalAllignmentOfElements(driver, prdPrice, btnShopNow)&& 
    					BrowserActions.verifyVerticalAllignmentOfElements(driver, prdImage, btnShopNow))){
    				return false;
    			}    			
    		}       		
    	}    	
    	return true;	
    }
    
    /**
     * To click on shop now button of product set
     * @throws Exception 
     * 
     */
    public void OpenOrCloseShopNowDrawerBasedOnIndex(int index, String state) throws Exception{
    	if(Utils.waitForElement(driver, fistItemInProductSet)){
    		String CurrentState = getShopNowDrawerState(index);
    		if(CurrentState.equalsIgnoreCase(state)){
    			Log.message("Already " + state+" state");
    		}else{
				WebElement btnShopNow = lstItemsInProductSet.get(index).findElement(By.cssSelector(" .product-variations-toggle.hide-tablet.hide-desktop"));
				BrowserActions.scrollToView(btnShopNow, driver);
				BrowserActions.clickOnElementX(btnShopNow, driver, "Product Details button");
    		}
    	}
    	Utils.waitForElement(driver, lstItemsInProductSet.get(index).findElement(By.cssSelector(" .product-variations")));
    }
    
    public String getShopNowDrawerState(int index ) throws Exception{
    	
    	WebElement btnShopNow = lstItemsInProductSet.get(index).findElement(By.cssSelector(" .product-variations-toggle.hide-tablet.hide-desktop"));	
    	String CurrentState = BrowserActions.getText(driver, btnShopNow, "Product Details button state");
    	if(CurrentState.equalsIgnoreCase("Product Details")){
			return "closed";
		}else{
			return "opened";
		}    	
    }
    
    /**
     * Compare SPS with Minicart Flyout
     *
     * @return boolean
     * @throws Exception -
     */
    public boolean checkSplProdSetCompAddedInSamePageOfMiniCart() throws Exception {
        int nameSize = lblMiniCartNameSplProdSet.size();
        int compSize = lstColorGrid.size();

        return (nameSize == compSize);
    }

    /**
     * to verify the Add To Bag error msg for special product set
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifySelectSizeErrorForSpecialProductSet() throws Exception {
        String error = null;
        BrowserActions.javascriptClick(btnAddToBagSpecialProductSet, driver, "Add to bag error");
        Thread.sleep(2000);
        error = BrowserActions.getText(driver, btnAddToBagSpecialProductSet, "Add to bag error");
        if (error.trim().equalsIgnoreCase("Please Select Size"))
            return true;
        else
            return false;
    }

    /**
     * to get the selected quantity value
     *
     * @return String
     * @throws Exception - Exception
     */
    public String getSelectedQuantityValue() throws Exception {
        return BrowserActions.getText(driver, txtSelectedQuantity, "Selected Quantity").trim();
    }

    /**
     * to get the selected quantity value
     *
     * @return String
     * @throws Exception - Exception
     */
    public String selectQuantityDropdownValue() throws Exception {
        BrowserActions.clickOnElementX(txtSelectedQuantity, driver, "Quantity drpdwn");
        BrowserActions.clickOnElementX(
                driver.findElement(By.cssSelector(".quantity .selection-list > li:nth-of-type(1)")), driver, "");
        return BrowserActions.getText(driver, txtSelectedQuantity, "Selected Quantity");
    }

    /**
     * to verify that the size chart link is displayed to the right of size
     * attribute
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyAddToBagButtonIsRightOfQuantityDropdown() throws Exception {
        return BrowserActions.verifyHorizontalAllignmentOfElements(driver, btnAddToBagSpecialProductSet,
                txtSelectedQuantity);
    }

    /**
     * to select a color swatch
     *
     * @param index -
     * @return String
     * @throws Exception - Exception
     */
    public String selectColorSwatch(int index) throws Exception {
        String color = (lstColorSwatches.get(index).findElement(By.cssSelector("a>img"))).getAttribute("alt").trim();
        if (lstColorSwatches.get(index).getAttribute("class").contains("selected") == false)
            BrowserActions.javascriptClick(lstColorSwatches.get(index).findElement(By.cssSelector("input")), driver,
                    "Size Swatch");
        return color;
    }
    
    /**
     * Mouse hover on the color swatch
     * return color swatch code
     * @param index
     * @throws Exception - Exception
     */
    public String mouseHoverOnColorswatch(int index)throws Exception {
    	WebElement elem=lstAllColor.get(index);
    	
    	BrowserActions.mouseHover(driver,elem, "Mouse hovered on "+index+" color swatch");
    	Utils.waitForElement(driver, pdpSelectedColor);
    	
    	WebElement element = elem.findElements(By.xpath("//ancestor::*[contains(@class,'swatches color')]//li//a//img")).get(index);
    	
    	String[] selectedImage = BrowserActions.getTextFromAttribute(driver, element, "src", "color").split("\\/");
    	return selectedImage[selectedImage.length - 1].split("\\.")[0];
    }
    
    /**
     * Mouse hover on the random color swatch
     * return color swatch code
     * @param index
     * @throws Exception - Exception
     */
	public String mouseHoverOnColorSwatch() throws Exception {
		if (!Utils.waitForElement(driver, selectedColor)) {
			for (int i = 0; i < lstColorSelectable.size(); i++) {
				String colorSwatch = BrowserActions.getTextFromAttribute(driver, lstColorSelectable.get(i), "src", "color swatch having image or not");
				if (!colorSwatch.toLowerCase().contains("no-swatch-tab")) {
					BrowserActions.mouseHover(driver, lstColorSelectable.get(i));
					String[] selectedImage = lstColorSelectable.get(i).findElement(By.cssSelector("input")).getAttribute("data-lgimg").split("/");
					String image = selectedImage[selectedImage.length - 1].split(".")[0];
					return image;
				}
			}
		} else {
			Log.reference("colorized image is not available for this color swatch");

			for (int i = 0; i < lstColorSelectable.size(); i++) {
				String colorSwatch = BrowserActions.getTextFromAttribute(driver, lstColorSelectable.get(i), "src", "color swatch having image or not");
				if (colorSwatch.toLowerCase().contains("no-swatch-tab")) {
					BrowserActions.mouseHover(driver, lstColorSelectable.get(i));
					String[] selectedImage = lstColorSelectable.get(i).findElement(By.cssSelector("input")).getAttribute("data-lgimg").split("/");
					String image = selectedImage[selectedImage.length - 1].split(".")[0];
					return image;
				}
			}
		}
		return null;
	}


    /**
     * To check review tab is selected
     * @return boolean - true if review tab is selected, else false
     * @throws Exception - Exception
     */
    public boolean checkReviewTabIsSelected() throws Exception {
        String classAttr = BrowserActions.getTextFromAttribute(driver, tabReviews, "class", "Review Tab");
        return classAttr.toLowerCase().contains("current");
    }

    /**
     * to select a size swatch
     * @param index -
     * @return String
     * @throws Exception - Exception
     */
    public String selectSizeSwatch(int index) throws Exception {
        String size = (lstSizeSwatches.get(index).getText().trim());
        if (lstSizeSwatches.get(index).getAttribute("class").contains("selected") == false)
            BrowserActions.clickOnElementX(lstSizeSwatches.get(index), driver,
                    "Size Swatch");
        Utils.waitForPageLoad(driver);
        return size;
    }

    /**
     * to select a value in Quantity drpdwn
     * @param index -
     * @return String
     * @throws Exception - Exception
     */
    public String selectQuantity(int index, boolean... substituteMax) throws Exception {
    	BrowserActions.scrollToViewElement(drpQty, driver);
		if ((getVariationAvailableQuantity() < index) && substituteMax.length > 0 && substituteMax[0]) {
			Log.event("Quantity unavailable for selected variation. Selecting available maximum quantity:: " + getVariationAvailableQuantity());
			BrowserActions.selectDropdownByIndex(drpQty, getVariationAvailableQuantity() - 1);
		} else {
			BrowserActions.selectDropdownByIndex(drpQty, index - 1);
		}
    	return BrowserActions.getSelectedOption(drpQty).trim();
    }

    /**
     * to get Special Product Set name in pdp
     * @return String
     * @throws Exception - Exception
     */
    public String getSpecialProductSetName() throws Exception {
        return txtProductname.getText().trim();
    }

    /**
     * to get Special Product Set name in cart overlay
     *
     * @return String
     * @throws Exception - Exception
     */
    public String getSpecialProductSetNameInMCOverlay() throws Exception {
        return divSpecialProductSetInMCOverLay.findElement(By.cssSelector(" .mini-cart-name a")).getText().trim();
    }

    /**
     * to get Special Product Set child size in cart overlay
     *
     * @param productIndex -
     * @return String
     * @throws Exception - Exception
     */
    public String getSpecialProductSetChildSizeInMCOverlay(int productIndex) throws Exception {
        return divSpecialProductSetInMCOverLay.findElement(By.cssSelector(
                " .special-productset-child:nth-of-type(" + productIndex + ") div[data-attribute='size'] .value"))
                .getText().trim();
    }

    /**
     * to get Special Product Set child color in cart overlay
     *
     * @param productIndex -
     * @return String
     * @throws Exception - Exception
     */
    public String getSpecialProductSetChildColorInMCOverlay(int productIndex) throws Exception {
        return divSpecialProductSetInMCOverLay.findElement(By.cssSelector(
                " .special-productset-child:nth-of-type(" + productIndex + ") div[data-attribute='color'] .value"))
                .getText().trim();
    }

    /**
     * to get Special Product Set child width in cart overlay
     *
     * @param productIndex -
     * @return String
     * @throws Exception - Exception
     */
    public String getSpecialProductSetChildWidthInMCOverlay(int productIndex) throws Exception {
        return divSpecialProductSetInMCOverLay.findElement(By.cssSelector(
                " .special-productset-child:nth-of-type(" + productIndex + ") div[data-attribute*='width'] .value"))
                .getText().trim();
    }

    /**
     * To verify special product set color is preselected
     *
     * @return boolean -
     * @throws Exception -
     */
    public boolean verifySpecialProductSetColorPreSelected() throws Exception {
        int size = selectedColorInSplProdSet.size();
        WebElement elem;
        String text = new String();
        for (int i = 0; i < size; i++) {
            elem = selectedColorInSplProdSet.get(i);
            text = BrowserActions.getText(driver, elem, "Color Selected");
            if (text.isEmpty()) {
                return false;
            }
        }
        return true;
    }


    /**
     * to get Special Product Set child image in cart overlay
     *
     * @param productIndex -
     * @return String
     * @throws Exception - Exception
     */
    public String getSpecialProductSetChildImageInMCOverlay(int productIndex) throws Exception {
        return divSpecialProductSetInMCOverLay.findElement(
        		By.cssSelector(" .special-productset-child:nth-of-type(" + productIndex + ") img")).getText().trim();
    }

    /**
     * to click next arrow in cart overlay
     *
     * @throws Exception - Exception
     */
    public void clickNextArrowInMCOverlay() throws Exception {
        BrowserActions.clickOnElementX(nextArrowInMCOverlay, driver, "Next Arrow");
    }

    /**
     * to verify no size is selected by default
     *
     * @param index -
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyNoSizeSwatchSelectedByDefaultForSpecialProductSet(int index) throws Exception {
        List<WebElement> sizeSwatches = driver.findElements(By.cssSelector(
                ".product-set-item:nth-of-type(" + index + ") .attribute.size > div  > ul > li"));
        for (WebElement size : sizeSwatches) {
            if (size.getAttribute("class").contains("selected")) {
                return false;
            }
        }
        return true;
    }

    /**
     * to verify the special product set price
     *
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifySpecialProductSetPriceDisplayed() throws Exception {
        if (txtSpecialProductSetPrice.findElement(By.cssSelector("span[class='price-standard']")).isDisplayed()) {
            if (txtSpecialProductSetPrice.findElement(By.cssSelector("span[class='price-standard']"))
                    .getCssValue("text-decoration").equals("line-through")) {
                if (txtSpecialProductSetPrice
                        .findElement(By.cssSelector("span[class='price-sales price-standard-exist']")).isDisplayed())
                    return true;
                else
                    return false;
            } else {
                return true;
            }
        } else
            return false;
    }

    /**
     * to click Add all to cart button in product set page
     *
     * @throws Exception - Exception
     */
    public void clickAddAlltoCart() throws Exception {
        if (Utils.waitForElement(driver, btnAddAlltoCart.get(0))) {
            btnAddAlltoCart.get(0).click();
        }

    }

    /**
     * To get number of add all to cart button
     *
     * @return Integer -
     * @throws Exception -
     */
    public int countAddAlltoCartbtn() throws Exception {
        if (Utils.waitForElement(driver, btnAddAlltoCart.get(0))) {
            return btnAddAlltoCart.size();
        } else
            return 0;
    }

    /**
     * to validate Add all to cart button in product set page
     *
     * @return boolean
     * @throws Exception - Exception
     *                   itemsinProdSet
     */
    public boolean verifyAddAlltoCartbtnposition() throws Exception {
        boolean temp = false;
        if (Utils.waitForElement(driver, btnAddAlltoCart.get(0))
                && Utils.waitForElement(driver, itemsinProdSet.get(0))) {
            int firstItem = btnAddAlltoCart.get(0).getLocation().getY();
            int AddAlltobagBtn1 = itemsinProdSet.get(0).getLocation().getY();
            int secondItem = btnAddAlltoCart.get(1).getLocation().getY();
            int AddAlltobagBtn2 = itemsinProdSet.get(1).getLocation().getY();

            if ((AddAlltobagBtn1 < firstItem) && (AddAlltobagBtn2 > secondItem)) {
                temp = true;
            } else
                temp = false;

        }
        return temp;
    }

    /**
     * To get quantity ats error message
     *
     * @return String - error message
     * @throws Exception -
     */
    public String getQuantityATSErroMsg() throws Exception {
        Utils.waitForElement(driver, errMsgQuantityATS);
        return errMsgQuantityATS.getAttribute("innerText");
    }

    /**
     * To click on the review tab
     *
     * @throws Exception -
     */
    public void clickOnReviewTab() throws Exception {
        Utils.waitForElement(driver, tabReviews);
        BrowserActions.scrollToView(tabReviews, driver);
        BrowserActions.clickOnElementX(tabReviews, driver, "Review star link");
        Utils.waitForPageLoad(driver);
    }
    
    /**
     * To click on the review stars below product name
     *
     * @throws Exception -
     */
    public void clickOnReviewStarLink() throws Exception {
        Utils.waitForElement(driver, lnkReviewsSection);
        BrowserActions.scrollInToView(lnkReviewsSection, driver);
        BrowserActions.javascriptClick(divReview, driver, "Review star link");
        Utils.waitForPageLoad(driver);
    }   
    
    /**
     * To click on the review tab
     *
     * @throws Exception -
     */
    public void selectSortBasedOnIndex(int index) throws Exception {
        BrowserActions.selectDropdownByIndex(cmbSortReview, index-1);
        Utils.waitForPageLoad(driver);
    }

    /**
     * To click on the write a review button
     * @throws Exception
     */
    public void clickOnWriteAReview() throws Exception {
    	if(!Utils.isMobile()) {
            if (Utils.waitForElement(driver, btnWriteAReview)) {
                BrowserActions.scrollInToView(btnWriteAReview, driver);
                if (BrandUtils.isBrand(Brand.ww))
                    BrowserActions.javascriptClick(btnWriteAReview, driver, "Write a review button");
                else
                    BrowserActions.clickOnElementX(btnWriteAReview, driver, "Write a review button");
            } else {
                if (Utils.waitForElement(driver, lnkWriteReview)) {
                    BrowserActions.scrollInToView(lnkWriteReview, driver);
                    BrowserActions.javascriptClick(lnkWriteReview, driver, "Write a review link");
                }
            }
        }else {
            if (Utils.waitForElement(driver, btnWriteAReview_mobile)) {
                BrowserActions.scrollInToView(btnWriteAReview_mobile, driver);
                if (BrandUtils.isBrand(Brand.ww))
                    BrowserActions.javascriptClick(btnWriteAReview_mobile, driver, "Write a review button");
                else
                    BrowserActions.clickOnElementX(btnWriteAReview_mobile, driver, "Write a review button");
            } else {
                if (Utils.waitForElement(driver, lnkWriteReview)) {
                    BrowserActions.scrollInToView(lnkWriteReview, driver);
                    BrowserActions.javascriptClick(lnkWriteReview, driver, "Write a review link");
                }
            }
        }
		String baseWindowHandle = driver.getWindowHandle();
		Log.event("Base windows handle:: " + baseWindowHandle);
		Set<String> openWindowsList = driver.getWindowHandles();
		Log.event("Number of open windows:: " + openWindowsList.size());
		if(openWindowsList.size() > 1) {
			Log.event("Review section opened in new tab for mobile.");
			String popUpWindowHandle = null;
			for(String windowHandle : openWindowsList) {
				Log.event("Current windows handle:: " + windowHandle);
				if (!windowHandle.equals(baseWindowHandle)) {
					popUpWindowHandle = windowHandle;
				}
			}
			driver.switchTo().window(popUpWindowHandle);
			Log.event("New window handle:: " + popUpWindowHandle);
		}
        Utils.waitForPageLoad(driver);
    }

    /**
     * to Click on fit Radio Button in pdp page
     *
     * @param index -
     * @throws Exception - Exception
     */
    public void clickOnFitRadioButton(int index) throws Exception {
    	if(Utils.waitForElement(driver, txtFitInReviewmodal)) {
	    	WebElement elem = radioFitWriteAReview.get(index - 1);
	        Utils.waitForElement(driver, elem);
	        BrowserActions.scrollInToView(elem, driver);
	        BrowserActions.javascriptClick(elem, driver, "Fit Radio button");
	        Utils.waitForPageLoad(driver);
    	}
    }

    /**
     * To tick Pros and Cons by index
     * @param pros - index of Pro to select
     * @param cons - index of Con to select
     * @throws Exception - Exception
     */
    public void clickOnProsAndConsCheckBox(int pros, int cons) throws Exception {
    	if(checkBoxPros.size() > 0 && checkBoxCons.size() > 0) {
    		try {
        		WebElement checkboxPro = checkBoxPros.get(pros - 1);
        		WebElement checkboxCon = checkBoxCons.get(cons - 1);
        		
    	        BrowserActions.scrollInToView(checkboxPro, driver);
    	        BrowserActions.javascriptClick(checkboxPro, driver, "Pros checkbox");
    	
    	        BrowserActions.scrollInToView(checkboxCon, driver);
    	        BrowserActions.javascriptClick(checkboxCon, driver, "Cons checkbox");
        	}catch(ArrayIndexOutOfBoundsException indexE) {
        		Log.failsoft("Provided index for Pros/Cons is out of bound of available checkboxes. Please check parameters.", driver);
        	}catch(Exception e) {
        		Log.failsoft("Something unexpected happened.\n" + e.getMessage(), driver);
        	}
    	} else {
    		Log.event("Product does not have Pros/Cons in it's review.");
    	}
    	
        Utils.waitForPageLoad(driver);
    }

    /**
     * To check pro's and cons checkboxes
     *
     * @param noOfSelectedCheckBoxes -
     * @return boolean -
     * @throws Exception -
     */
    public boolean checkOnProsAndConsCheckBoxSelected(int noOfSelectedCheckBoxes) throws Exception {
        int cnt = 0;
        for (int i = 0; i < checkBoxProsAndCons.size(); i++) {

            WebElement elem = checkBoxProsAndCons.get(i);
            Utils.waitForElement(driver, elem);
            if (elem.isSelected()) {
                cnt = cnt + 1;
            }
            if (cnt == noOfSelectedCheckBoxes) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * To click on Add Photo button
     *
     * @throws Exception -
     */
    public void clickAddPhoto() throws Exception{
    	if(Utils.waitForElement(driver, lnkReviewAddPhoto)) {
    		BrowserActions.clickOnElementX(lnkReviewAddPhoto, driver, "Add Photo button");
    	}
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To click on Add Video button
     *
     * @throws Exception -
     */
    public void clickAddVideo() throws Exception{
    	if(Utils.waitForElement(driver, lnkReviewAddVideo)) {
    		BrowserActions.clickOnElementX(lnkReviewAddVideo, driver, "Add Photo button");
    	}
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To close Add Photo/ Video modal
     *
     * @throws Exception -
     */
    public void closeAddPhotoVideoModal() throws Exception{
    	if(Utils.waitForElement(driver, btnCancelAddPhotoVideo)) 
    		BrowserActions.clickOnElementX(btnCancelAddPhotoVideo, driver, "cancel button");
    	else 
    		BrowserActions.clickOnElementX(iconCloseAddPhotoVideo, driver, "close icon");
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To close Review Thanks modal
     *
     * @throws Exception -
     */
    public void closeReviewThanksModal() throws Exception{
    	if(Utils.waitForElement(driver, iconCloseAddPhotoVideo)) {
    		BrowserActions.clickOnElementX(iconCloseAddPhotoVideo, driver, "close icon");
    	}
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To close Find Your Profile
     *
     * @throws Exception -
     */
    public void closeFindYourProfileModal() throws Exception{
    	if(Utils.waitForElement(driver, iconCloseFindYourProfile)) {
    		BrowserActions.clickOnElementX(iconCloseFindYourProfile, driver, "close icon");
    	}
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To close Email confirmation Thank you message
     *
     * @throws Exception -
     */
    public void clickContinueShoppingEmailConfirmationModal() throws Exception{
    	if(Utils.waitForElement(driver, continueShoppingEmailConfirmationModal)) {
    		BrowserActions.clickOnElementX(continueShoppingEmailConfirmationModal, driver, "Continue shopping");
    	}
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To click on Review My Post link
     *
     * @throws Exception -
     */
    public void clickReviewMyPost() throws Exception {
    	if(Utils.waitForElement(driver, lnkReviewMyPost)) {
    		BrowserActions.clickOnElementX(lnkReviewMyPost, driver, "Review My Post link");
    	}
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To click on Bottom tabs from My Post
     *
     * @throws Exception -
     */
    public void clickMyPostBottomTabBasedOnIndex(int index, String description) throws Exception {
    	WebElement elem=listMyPostBottomTabs.get(index-1);
    	if(Utils.waitForElement(driver, elem)) {
    		BrowserActions.clickOnElementX(elem, driver, description+" tabs");
    	}
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To click on My Settings
     *
     * @throws Exception -
     */
    public void clickMySettings() throws Exception {
    	if(Utils.waitForElement(driver, lnkMySettings)) {
    		BrowserActions.clickOnElementX(lnkMySettings, driver, " My Settings link");
    	}
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To add FirstName and LastName
     *
     * @throws Exception -
     */
    public void addFirstNameAndLastName(String firstName, String lastName) throws Exception { 
    		BrowserActions.typeOnTextField(firstNameMySettings, firstName, driver, "First Name");
    		BrowserActions.typeOnTextField(lstNameMySettings, lastName, driver, "last Name");
    }
    
    /**
     * To click on Update button in My Settings
     *
     * @throws Exception -
     */
    public void clickUpdateMySettings() throws Exception {
    	if(Utils.waitForElement(driver, updateMySettings)) {
    		BrowserActions.clickOnElementX(updateMySettings, driver, " Update button");
    	}
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To Add email in My Settings
     *
     * @throws Exception -
     */
    public void addEmailInMySettings(String email) throws Exception {
    	if(Utils.waitForElement(driver, fieldAddMail)) {
    		BrowserActions.typeOnTextField(fieldAddMail, email, driver, "Add new email");
    	}
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To remove added email in My settings
     *
     * @throws Exception -
     */
    public void removeEmailInMySettings() throws Exception {
    	if(Utils.waitForElement(driver, closeAddedMail)) {
    		BrowserActions.clickOnElementX(closeAddedMail, driver, "close icon");
    	}
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To click on Add Photo link in My settings
     *
     * @throws Exception -
     */
    public void clickAddPhotoMySettings() throws Exception {
    	if(Utils.waitForElement(driver, addPhotoMySettings)) {
    		BrowserActions.clickOnElementX(addPhotoMySettings, driver, "Add Photo");
    	}
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To click on Change Password link in My settings
     *
     * @throws Exception -
     */
    public void clickChangePasswordMySettings() throws Exception {
    	if(Utils.waitForElement(driver, changePasswordMySettings)) {
    		BrowserActions.clickOnElementX(changePasswordMySettings, driver, "Change Password");
    	}
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To click on "Not FirstName?" link in My Post modal
     *
     * @throws Exception -
     */
    public void clickNotFirstName() throws Exception {
    	if(Utils.waitForElement(driver, lnkNotFirstNameReview)) {
    		BrowserActions.clickOnElementX(lnkNotFirstNameReview, driver, "Not FirstName? link");
    	}
    	Utils.waitForPageLoad(driver);
    }

    /**
     * To type text in review title fields
     *
     * @param inputText -
     * @throws Exception -
     */
    public void enterWriteReviewTitle(String inputText) throws Exception {
    	WebElement titleField;
    	if(!Utils.isMobile()) {
    		titleField = inpWriteReviewTitle;
    	} else {
    		titleField = inpWriteReviewTitleMobile;
    	}
    	Utils.waitForElement(driver, titleField);
    	BrowserActions.scrollInToView(titleField, driver);
    	BrowserActions.typeOnTextField(titleField, inputText, driver, "Review title");
    	Utils.waitForPageLoad(driver);
    }
    
    /**
     * To type text in Review descryption feld
     *
     * @throws Exception -
     */
    public void enterWriteReviewDesc() throws Exception {
    	WebElement reviewField;
    	if(!Utils.isMobile()) {
    		reviewField = inpWriteReviewDesc;
    	} else {
    		reviewField = inpWriteReviewDescMobile;
    	}
    	Utils.waitForElement(driver, reviewField);
    	BrowserActions.scrollInToView(reviewField, driver);
    	char[] charArray = {'a', ' ', 'e', 'i', 'o', 'u', 'b', 'c', ' '};
    	String randomString = RandomStringUtils.random(60, charArray);
    	BrowserActions.typeOnTextField(reviewField, randomString, driver, "Review description");
    	Utils.waitForPageLoad(driver);
    }

    /**
     * To click on read review guide line link
     *
     * @throws Exception -
     */
    public void clickReadReviewGuideLine() throws Exception {
        Utils.waitForElement(driver, lnkReadReviewGuideLine);
        BrowserActions.scrollInToView(lnkReadReviewGuideLine, driver);
        BrowserActions.javascriptClick(lnkReadReviewGuideLine, driver, "Read Review Guidelines");
        Utils.waitForPageLoad(driver);
    }

    /**
     * To click on close in reivew guide line
     *
     * @throws Exception -
     */
    public void clickCloseGuideLine() throws Exception {
    	WebElement closeGuideLine;
    	if(!Utils.isMobile()) {
    		closeGuideLine = btnCloseGuideLine;
    	} else {
    		closeGuideLine = lnkCloseGuideLineMobile;
    	}
    	Utils.waitForElement(driver, closeGuideLine);
    	BrowserActions.scrollInToView(closeGuideLine, driver);
    	BrowserActions.clickOnElementX(closeGuideLine, driver, "Read Review Guidelines close button/link");
    	Utils.waitForPageLoad(driver);
    }

    /**
     * To verify review title and description fields are empty
     *
     * @return boolean -
     * @throws Exception -
     */
    public boolean checkWriteReviewTitleAndDescAreNotEmpty() throws Exception {
        WebElement titleField, reviewField;
		if (!Utils.isMobile()) {
			titleField = inpWriteReviewTitle;
			reviewField = inpWriteReviewDesc;
		} else {
			titleField = inpWriteReviewTitleMobile;
			reviewField = inpWriteReviewDescMobile;
		}
    	
    	Utils.waitForElement(driver, reviewField);

        BrowserActions.scrollInToView(titleField, driver);
        String val1 = BrowserActions.getTextFromAttribute(driver, titleField, "value", "Review Title");

        BrowserActions.scrollInToView(reviewField, driver);
        String val2 = BrowserActions.getTextFromAttribute(driver, reviewField, "value", "Review Description");

        if (val1.isEmpty() || val2.isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * to Click On submit review
     *
     * @throws Exception - Exception
     */

    public void clickOnSubmitReview(String emailId, String password) throws Exception {
        BrowserActions.clickOnElementX(btnSubmitReview, driver, "Details section tab");
        Utils.waitForPageLoad(driver);
        
        loginToReviewGoogleAccount(emailId, password);
   }
    
    /**
     * to Click On submit review
     *
     * @throws Exception - Exception
     */

    public void clickOnSubmitReview() throws Exception {
    	String baseWindowHandle = driver.getWindowHandle();
    	Log.event("Base windows handle:: " + baseWindowHandle);
    	Set<String> openWindowsList = driver.getWindowHandles();
    	Log.event("Number of open windows:: " + openWindowsList.size());
    	
		if (!Utils.isMobile()) {
			BrowserActions.clickOnElementX(btnSubmitReview, driver, "Submit review button");
		} else {
			BrowserActions.clickOnElementX(btnSubmitReviewMobile, driver, "Submit review button");
		}
		Utils.waitForPageLoad(driver);
        
        //Need to confirm email, so skipped
		if (Utils.waitForElement(driver, txtFindUrProfileEmail)) {
		//	BrowserActions.typeOnTextField(txtFindUrProfileEmail, "gopan.damodaran@aspiresys.com", driver, "Profile email");
			BrowserActions.typeOnTextField(txtFindUrProfileEmail, "may30sfcc@mailinator.com", driver, "Profile email");
			BrowserActions.typeOnTextField(txtFindUrProfileFirstName, "gopan", driver, "FirstName");
			BrowserActions.typeOnTextField(txtFindUrProfileLastName, "damodaran", driver, "LastName");
			BrowserActions.clickOnElementX(btnSubmitProfile, driver, "Submit profile");
			Utils.waitForPageLoad(driver);
		}
		if (openWindowsList.size() > 1) {
			Log.event("Review section opened in new tab for mobile.");
			String popUpWindowHandle = null;
			for(String windowHandle : openWindowsList) {
				Log.event("Current windows handle:: " + windowHandle);
				if (!windowHandle.equals(baseWindowHandle)) {
					popUpWindowHandle = windowHandle;
				}
			}
			driver.switchTo().window(popUpWindowHandle);
			Log.event("New window handle:: " + popUpWindowHandle);
    	}
        
   }
    
    public void loginToReviewGoogleAccount(String email, String password) throws Exception{
    	if (Utils.waitForElement(driver, modalFindUrProfile)) {
    		String winHandleBefore = driver.getWindowHandle();
    		BrowserActions.clickOnElementX(lnkEmailFindUrProfile, driver, "Email link in Find your profile");
    		
    		for(String winHandle : driver.getWindowHandles()){
    		    driver.switchTo().window(winHandle);
    		}
    		
    		if(Utils.waitForElement(driver, lnkEmailGooglePopUp)) {
    			BrowserActions.clickOnElementX(lnkEmailGooglePopUp, driver, "Email link");
    			Utils.waitForPageLoad(driver);
        		
        		driver.switchTo().window(winHandleBefore);
                Utils.waitForPageLoad(driver);
    		}
    		else {
    			Utils.waitForElement(driver, fieldEmailGooglePopUp);
        		BrowserActions.typeOnTextField(fieldEmailGooglePopUp, email, driver, "Email Id");
        		BrowserActions.clickOnElementX(nextGooglePopUp, driver, "Next button");
        		
        		Utils.waitForElement(driver, fieldPasswordGooglePopUp);
        		BrowserActions.typeOnTextField(fieldPasswordGooglePopUp, password, driver, "Password");
        		BrowserActions.clickOnElementX(nextGooglePopUp, driver, "Next button");
        		Utils.waitForPageLoad(driver);
        		
        		driver.switchTo().window(winHandleBefore);
                Utils.waitForPageLoad(driver);
    		}	
         }
    }


    /**
     * To click on Details tab
     * @throws Exception -
     */
    public void clickOnDetailsTab() throws Exception {
        Utils.waitForElement(driver, sectionDetailsTab);
        BrowserActions.scrollInToView(sectionDetailsTab, driver);
        BrowserActions.clickOnElementX(sectionDetailsTab, driver, "Details section tab");
        Utils.waitForPageLoad(driver);
    }

    /**
     * To click on review start's based on index -
     * @param index -
     * @throws Exception -
     */
    public void clickOnReviewStar(int index) throws Exception {
    	WebElement elem;
    	if(!Utils.isMobile()) {
    		elem = inpReviewStar.get(index - 1);
    	} else {
    		elem = inpReviewStarMobile.get(index - 1);
    	}
    	Utils.waitForElement(driver, elem);
    	BrowserActions.scrollInToView(elem, driver);
    	BrowserActions.clickOnElementX(elem, driver, "Review star");
    	Utils.waitForPageLoad(driver);
    }

    /**
     * To type text in location field in review
     *
     * @param location -
     * @throws Exception -
     */
    public void enterLocationInReview(String location) throws Exception {
	    if(Utils.waitForElement(driver, txtLocation)) {
		    BrowserActions.scrollInToView(txtLocation, driver);
		    BrowserActions.typeOnTextField(txtLocation, location, driver, "City");
		    Utils.waitForPageLoad(driver);
	    }
    }

    /**
     * To check / select Fit Radio is selected / not
     *
     * @return boolean -
     * @throws Exception -
     */
    public boolean checkReviewFitRadioButtonIsSelected() throws Exception {
        int NoOfelem = radioFitWriteAReview.size();
        for (int i = 0; i < NoOfelem; i++) {
	            WebElement elem = radioFitWriteAReview.get(i);
	            Utils.waitForElement(driver, elem);
	            BrowserActions.scrollInToView(elem, driver);
	            if (elem.isSelected()) {
	                return true;
	            }
        }

        return false;
    }


    /**
     * To validate regular price in a productset
     *
     * @param Productname -
     * @return boolean
     * @throws Exception - Exception
     */
    public boolean validateRegularPriceInProductset(String Productname) throws Exception {

        boolean bool = false;
        int index = 0;
        List<WebElement> componentprods = driver.findElements(By.cssSelector(".item-name"));
        for (int i = 0; i < componentprods.size(); i++) {
            if (componentprods.get(i).getAttribute("title").contains(Productname)) {
                index = i;
                break;
            }
        }

        List<WebElement> product_list = driver.findElements(By.cssSelector(".product-set-item"));
        WebElement product_size = product_list.get(index).findElement(By.cssSelector(".product-price"));
        int size = product_size.findElements(By.cssSelector("span")).size();
        if (size == 2)
            bool = true;

        return bool;

    }

    /**
     * to validate Price range prodIn productset
     *
     * @param Productname -
     * @return String
     * @throws Exception - Exception
     */

    public boolean validatePriceRangeProdInProductset(String Productname) throws Exception {

        boolean bool = false;
        int index = 0;
        List<WebElement> componentprods = driver.findElements(By.cssSelector(".item-name"));
        for (int i = 0; i < componentprods.size(); i++) {
            if (componentprods.get(i).getAttribute("title").contains(Productname)) {
                index = i;
                break;
            }
        }

        List<WebElement> product_list = driver.findElements(By.cssSelector(".product-set-item"));
        WebElement product_size = product_list.get(index).findElement(By.cssSelector(".product-price"));
        WebElement price = product_size.findElement(By.cssSelector("span[class='price-standard']"));

        String string = price.getText();
        if (string.contains("-"))
            bool = true;

        return bool;

    }

    /**
     * to VALIDATE OFFER PRICE iN Product set
     *
     * @param Productname -
     * @return String
     * @throws Exception - Exception
     */
    public boolean validateOfferPriceInProductset(String Productname) throws Exception {

        boolean bool = false;
        int index = 0;
        List<WebElement> componentprods = driver.findElements(By.cssSelector(".item-name"));
        for (int i = 0; i < componentprods.size(); i++) {
            if (componentprods.get(i).getAttribute("title").contains(Productname)) {
                index = i;
                break;
            }
        }

        List<WebElement> product_list = driver.findElements(By.cssSelector(".product-set-item"));
        WebElement product_size = product_list.get(index).findElement(By.cssSelector(".product-price"));
        if (product_size.findElement(By.cssSelector("span[class$='price-standard-exist']")).isDisplayed())
            bool = true;

        return bool;

    }

    /**
     * to validate Call out message product set
     *
     * @param Productname -
     * @return String
     * @throws Exception - Exception
     */
    public boolean validateCallOutMsgProductset(String Productname) throws Exception {
        boolean bool = false;
        WebElement calloutmsg = driver.findElement(By.cssSelector("span[class='callout-message']"));
        if (!(calloutmsg.getText().equals(null))) {
        	bool = true;
        }
        return bool;
    }

    /**
     * to select size product set
     *
     * @param Productname -
     * @param size        -
     * @return String
     * @throws Exception - Exception
     */
    public String selectSizeProductset(String Productname, String size) throws Exception {
        int index = 0;
        String sizeselected = null;
        List<WebElement> componentprods = driver.findElements(By.cssSelector(".item-name"));
        for (int i = 0; i < componentprods.size(); i++) {
            if (componentprods.get(i).getAttribute("title").contains(Productname)) {
                index = i;
                break;
            }
        }

        List<WebElement> product_list = driver.findElements(By.cssSelector(".product-set-item"));
        List<WebElement> swatches = product_list.get(index).findElements(By.cssSelector(".swatches.size li.selectable a"));
        if (size == null || size.length() == 0) {
            int rand = Utils.getRandom(0, swatches.size());
            sizeselected = swatches.get(rand).getAttribute("title").split(":")[1].trim();
            BrowserActions.javascriptClick(swatches.get(rand), driver, "Size Swatches ");
        } else {
            for (int i = 0; i < swatches.size(); i++)

                if (swatches.get(i).getText().equalsIgnoreCase(size)) {
                    sizeselected = swatches.get(i).getText();
                    swatches.get(i).click();
                    System.out.println("Size" + swatches.get(i).getAttribute("title").split(":")[1].trim());
                    Utils.waitForPageLoad(driver);
                    return sizeselected;
                }
        }
        return sizeselected;
    }

    /**
     * to verify default size selected in prodset
     *
     * @return String
     * @throws Exception - Exception
     */
    public boolean verifydefaultSizeSelectedinProdSet() throws Exception {
        boolean temp = false;
        List<WebElement> swatches = driver
                .findElements(By.cssSelector("ul[class='swatches size'] li[class='selectable selected']"));
        if (swatches.size() == 0) {
            temp = true;
        }
        return temp;
    }

    /**
     * to select color in product set
     *
     * @param Productname -
     * @param color       -
     * @return String
     * @throws Exception - Exception
     */
    public String selectColorinProdset(String Productname, String color) throws Exception {
        int index = 0;
        String selectedcolor = null;
        List<WebElement> componentprods = driver.findElements(By.cssSelector(".item-name"));
        for (int i = 0; i < componentprods.size(); i++) {
            if (componentprods.get(i).getAttribute("title").contains(Productname)) {
                index = i;
                break;
            }
        }
        Thread.sleep(3000);
        List<WebElement> product_list = driver.findElements(By.cssSelector(".product-set-item"));
        List<WebElement> swatches = product_list.get(index).findElements(By.cssSelector("li[class^='selectable'] img"));
        if (color == null || color.length() == 0) {
            selectedcolor = swatches.get(0).getAttribute("alt");
            BrowserActions.javascriptClick(swatches.get(0), driver, "Color Swatches ");
        } else {
            for (int i = 0; i < swatches.size(); i++)

                if (swatches.get(i).getAttribute("alt").equalsIgnoreCase(color)) {
                    selectedcolor = swatches.get(i).getAttribute("alt");
                    swatches.get(i).click();
                    return selectedcolor;
                }
        }
        return selectedcolor;
    }

    /**
     * to select color i product set using product Id
     *
     * @param Prodid -
     * @param color  -
     * @return String
     * @throws Exception - Exception
     */

    public String selectColorinProdsetUsingProdID(String Prodid, String color) throws Exception {
        String selectedcolor = null;
        List<WebElement> swatches = driver.findElements(By.cssSelector("div[id$='" + Prodid + "'] li[class^='selectable'] img"));
        if (color == null || color.length() == 0) {
            selectedcolor = swatches.get(0).getAttribute("alt");
            BrowserActions.javascriptClick(swatches.get(0), driver, "Color Swatches ");
        } else {
            for (int i = 0; i < swatches.size(); i++) {
            	if (swatches.get(i).getAttribute("alt").equals(color)) {
                    selectedcolor = swatches.get(i).getAttribute("alt");
                    swatches.get(i).click();
                }
            }
        }
        return selectedcolor;
    }

    /**
     * to verify Color Swatch border is displayed for selected color swatch
     *
     * @param Productname -
     * @param color       -
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyColorSwatchBorderinProdset(String Productname, String color) throws Exception {
        int index = 0;
        List<WebElement> componentprods = driver.findElements(By.cssSelector(".item-name"));
        for (int i = 0; i < componentprods.size(); i++) {
            if (componentprods.get(i).getAttribute("title").contains(Productname)) {
                index = i;
                break;
            }
        }

        List<WebElement> product_list = driver.findElements(By.cssSelector(".product-set-item"));
        WebElement swatches = product_list.get(index)
                .findElement(By.cssSelector("li[class='selectable selected'] img"));

        String borderswatch = swatches.getAttribute("alt");
        if (color.equals(borderswatch)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * to verify color swatches in a row product set
     *
     * @param Productname -
     * @param color       -
     * @return String
     * @throws Exception - Exception
     */
    public Boolean verifyColorSwatchesInaRowProdset(String Productname, String color) throws Exception {
        List<WebElement> swatches = driver.findElements(By.cssSelector("li[class^='selectable'] img"));
        int count = swatches.size();
        Boolean swatchesInRow = false;
        for (int n = 0; n < count; n++) {

            if (n % 5 == 0) {
                int yLocationOfFifthSwatch = getCurrentLocationOfColorSwatchesByIndexforProdset(swatches, n);
                int yLocationOfSixthSwatch = getCurrentLocationOfColorSwatchesByIndexforProdset(swatches, n + 1);
                if (yLocationOfFifthSwatch != yLocationOfSixthSwatch) {
                    swatchesInRow = true;
                } else {
                    swatchesInRow = false;
                    break;
                }
            } else {
                int yLocationOfNthSwatch = getCurrentLocationOfColorSwatchesByIndexforProdset(swatches, n);
                int yLocationOfNplus1Swatch = getCurrentLocationOfColorSwatchesByIndexforProdset(swatches, n + 1);
                if (yLocationOfNthSwatch == yLocationOfNplus1Swatch) {
                    swatchesInRow = true;
                } else {
                    swatchesInRow = false;
                    break;
                }
            }
        }
        return swatchesInRow;
    }
    
    /**	
     * To click Arrow in recently view section	
     *	
     * @return Void -	
     * @throws Exception -
     */	
    public void clickRecentlyviewedArrow() throws Exception {	
        BrowserActions.clickOnElement(recentlyViewSectionNextArrow, driver, "Recently viewed Arrow");    	
    }	

    /**
     * To verify scroll bar in tool tip
     *
     * @param innerElement -
     * @param outerElement -
     * @param obj          -
     * @return boolean -
     * @throws Exception -
     */
    public boolean verifyScrollBarInTooltip(String innerElement, String outerElement, Object obj) throws Exception {

        ElementLayer ele = new ElementLayer(driver);
        return ele.verifyVerticalScrollBar(innerElement, outerElement, obj);
    }

    /**
     * To number of products displayed in recently view section
     *
     * @return Integer -
     * @throws Exception -
     */
    public int getNoOfProdDisplayInRecentView() throws Exception {
    	int countRecentlyViewedPrd = 0;
    	try {
    		BrowserActions.scrollInToView(recentlyViewedSection, driver);
    		countRecentlyViewedPrd = recentlyViewedProdName.size();
    		Log.event("Number of Products Recently Viewed :: " + countRecentlyViewedPrd);
    	} catch(NoSuchElementException e) {
    		Log.event("Recently viewed section is not displayed");
    	}
    	return countRecentlyViewedPrd;
    }

    /**
     * To get active product's present curently in recently view section
     *
     * @return Integer -
     * @throws Exception -
     */
    public int getNoOfProdDisplayingInFrontRecentView() throws Exception {
        BrowserActions.scrollToBottomOfPage(driver);
        return recentlyViewedActiveProd.size();
    }

    /**
     * To verify given product name displayed in recently viewed section
     *
     * @param prodName -
     * @return boolean -
     * @throws Exception -
     */
    public boolean verifyProdDisplayRecentlyViewedSection(String prodName) throws Exception {
        int size = getNoOfProdDisplayInRecentView();
        Log.event("Total Number of Products Displayed :: " + size);
        Log.event("Products :: " + BrowserActions.getText(driver, recentlyViewedProdName, "Recently Viewed Product Names"));
        for (int i = 0; i < size; i++) {
            WebElement prodNameSingle = recentlyViewedProdName.get(i);
            String prodNameRecent = BrowserActions.getText(driver, prodNameSingle, "Recently Viewed Product Name");
            if (prodNameRecent.contains(prodName)) {
                return true;
            }
        }
        return false;
    }
    
    /**
	 * To get the number of products have sale prices
	 * @returns true
	 * @throws Exception - Exception
	 */
	public int getNoOfSalePricesInRecenltyViewed()throws Exception {
		int salePriceProduct= recentlyViewedSalePrice.size();
		return salePriceProduct;
	}
	
	/**
	 * To get the number of products have standard prices
	 * @returns true
	 * @throws Exception - Exception
	 */
	public int getNoOfStdPricesInRecenltyViewed()throws Exception {
		int salePriceProduct= recentlyViewedStdPrice.size();
		return salePriceProduct;
	}
	
	 /**
     * To verify Recently viewed in PDP have product name, image, and price
     * @return true/false if all Recently viewed product tiles are verified to have correct content
     * @throws Exception 
     */
	public boolean verifyRecentlyViewedTileContent() throws Exception {
		List<WebElement> recentlyviewedTiles = null;
		recentlyviewedTiles = recentlyViewedproductTiles;

		for (WebElement recTile : recentlyviewedTiles) {
			WebElement tilePrdName, tilePrdImage, tilePrdPrice;
			tilePrdImage = recTile.findElement(By.cssSelector(".p-image"));
			tilePrdName = recTile.findElement(By.cssSelector(".product-name"));
			tilePrdPrice = recTile.findElement(By.cssSelector(".product-pricing"));

			if (!BrowserActions.VerifyElementDisplayed(driver,
					Arrays.asList(tilePrdImage, tilePrdName, tilePrdPrice))) {
				return false;
			}
		}
		return true;
	}
    
	
    /**
     * To click add product to bag button in productset
     *
     * @param Productname -
     * @throws Exception - Exception
     */
    public void clickAddProductToBagProdset(String Productname) throws Exception {
        int index = 0;
        List<WebElement> componentprods = driver.findElements(By.cssSelector(".item-name"));
        for (int i = 0; i < componentprods.size(); i++) {
            if (componentprods.get(i).getAttribute("title").contains(Productname)) {
                index = i;
                break;
            }
        }

        BrowserActions.scrollToViewElement(btnAddToCartProdset.get(index), driver);
        BrowserActions.javascriptClick(btnAddToCartProdset.get(index), driver, "Add To Cart Button ");
    }

    /**
     * to verify error msg "please select size"
     *
     * @param Productname -
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyErrorMsginAddtoBagProdset(String Productname) throws Exception {
        int index = 0;
        List<WebElement> componentprods = driver.findElements(By.cssSelector(".item-name"));
        for (int i = 0; i < componentprods.size(); i++) {
            if (componentprods.get(i).getAttribute("title").contains(Productname)) {
                index = i;
                break;
            }
        }

        List<WebElement> product_list = driver.findElements(By.cssSelector(".product-set-item"));
        try {
            WebElement swatches = product_list.get(index).findElement(By.cssSelector(".quantity-ats-message padding-eight"));//button[value='ADD TO BAG']

            if (swatches.getText().equals("Please Select Size")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * to verify error msg in selecting any variation
     *
     * @param variation -to be validated (size/color)
     * @return Boolean
     * @throws Exception - Exception
     */
    public Boolean verifyVariationSelectErrMsg(String variation) throws Exception {

        WebElement element = driver.findElement(By.cssSelector("span[class^='variation-selection-error-msg'][style='display: inline;'] span[class='" + variation.toLowerCase() + "']"));

        if (element.getText().toLowerCase().contains(" : please select a size")) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * to click on add to wishlist in productset pdp
     *
     * @param ProductID -
     * @return Boolean
     * @throws Exception - Exception
     */
    public Object clickAddtoWishlistProdset(String ProductID) throws Exception {

        WebElement wishlist = driver.findElement(By.cssSelector("div[id$='" + ProductID + "'] .wishlist>a"));
        BrowserActions.javascriptClick(wishlist, driver, "Add to Wishlist ");
        if (Utils.waitForElement(driver, divSearchWishlist))
            return new WishlistLoginPage(driver).get();
        else
            return null;
    }


    /**
     * to select width in product set
     *
     * @param ProductID -
     * @param width     -
     * @return String
     * @throws Exception - Exception
     */
    public String selectWidthinProdSet(String ProductID, String width) throws Exception {
        List<WebElement> widthvalues = driver.findElements(By.cssSelector("div[id$='" + ProductID + "'] .swatches.width>li>a"));
        String selectedwidth = null;
        String selectedwidth1 = null;
        if (width == null || width.length() == 0) {
            selectedwidth = widthvalues.get(0).getAttribute("title");
            selectedwidth1 = selectedwidth.split(":")[1].trim();
            BrowserActions.javascriptClick(widthvalues.get(0), driver, "Select Width ");
        } else {
            for (int i = 0; i < widthvalues.size(); i++) {
                if (widthvalues.get(i).getAttribute("title").contains(width)) {
                    selectedwidth = widthvalues.get(i).getAttribute("title").split(":")[1].trim();
                    BrowserActions.javascriptClick(widthvalues.get(i), driver, "Select Width ");
                }
            }
        }
        return selectedwidth1;

    }

    /**
     * verify id quantity is in the left of add to bag in productset pdp
     *
     * @param ProductID -
     * @return Boolean
     * @throws Exception - Exception
     */
    public boolean verifyQtyIsleftToAddtobagProdset(String ProductID) throws Exception {

        WebElement qty = driver
                .findElement(By.cssSelector("div[id$='" + ProductID + "'] div[class='selected-option selected']"));
        WebElement addtobag = driver
                .findElement(By.cssSelector("div[id$='" + ProductID + "'] button[value='ADD TO BAG']"));
        Point point1 = qty.getLocation();
        Point point2 = addtobag.getLocation();
        if (point1.getX() < point2.getX()) {
            return true;
        } else
            return false;

    }

    /**
     * verify default quantity selected in productset pdp
     *
     * @param Quantity  -
     * @param ProductID -
     * @return Boolean
     * @throws Exception - Exception
     */
    public boolean verifyDefaultQtyProdset(String ProductID, String Quantity) throws Exception {

        WebElement qty = driver
                .findElement(By.cssSelector("div[id$='" + ProductID + "'] .quantity div[class='selected-option selected']"));
        if (qty.getAttribute("innerHTML").trim().equals(Quantity)) {
            return true;
        } else
            return false;

    }

    /**
     * To click Quantity dropdown in productset pdp
     *
     * @param ProductID -
     * @throws Exception - Exception
     */
    public void clickQtyDropdownProdset(String ProductID) throws Exception {

        WebElement qtyDropdown = driver.findElement(By.cssSelector("div[id$='" + ProductID + "'] .inventory "));

        if (Utils.waitForElement(driver, qtyDropdown)) {
            BrowserActions.clickOnElementX(qtyDropdown, driver, "Qty Dropdown");
        }

    }

    /**
     * To select Quantity in productset pdp
     *
     * @param Quantity  -
     * @param ProductID -
     * @param ProductID -
     * @throws Exception - Exception
     */
    public void selectQtyProdSet(String ProductID, String Quantity) throws Exception {

        Select dropdown = new Select(driver.findElement(By.cssSelector("div[id$='" + ProductID + "'] #Quantity ")));
        if (Quantity != null) {
            dropdown.selectByValue(Quantity);
        } else {
            dropdown.selectByIndex(0);
        }
    }
    
    /**
     * To select Quantity in productset pdp
     *
     * @param Quantity  -
     * @param ProductID -
     * @param ProductID -
     * @throws Exception - Exception
     */
    public void selectStateInReviewModal(String valueOrIndex) throws Exception {
    	if(!Utils.isMobile()) {
    		Select dropdown = new Select(selectReviewState);
        	try {
        		int integer = Integer.parseInt(valueOrIndex);
        		dropdown.selectByIndex(integer);
        	} catch (NumberFormatException E){
        		dropdown.selectByValue(valueOrIndex);
        	}
    	} else {
    		BrowserActions.typeOnTextField(selectReviewStateMobile, valueOrIndex, driver, "Name of state");
    	}
    }
    

    /**
     * verify id quantity is in the left of add to bag in product-set PDP
     *
     * @param ProductID -
     * @return Boolean
     * @throws Exception - Exception
     */
    public boolean verifyQtyBelowToInventoryProdset(String ProductID) throws Exception {

        WebElement qty = driver
                .findElement(By.cssSelector("div[id$='" + ProductID + "'] div[class='selected-option selected']"));
        Point point1 = qty.getLocation();
        Point point2 = inventoryState.getLocation();
        if (point1.getY() < point2.getY()) {
            return true;
        } else
            return false;
    }
    
    /**
     * To navigate to cart from PDP
     * <br>Uses cart overlay button if available. Otherwise uses Headers navigation method.
     * @throws Exception
     */
    public ShoppingBagPage navigateToShoppingBag() throws Exception{
    	if (Utils.waitForElement(driver, mdlMiniCartOverLay)) {
    		return clickOnCheckoutInMCOverlay();
        } else {
        	return new Headers(driver).get().navigateToShoppingBagPage();
        }
    }
    
    /**
     * To select all swatches in the PDP page
     * @throws Exception - Exception
     */
    public void selectAllSwatches() throws Exception {
        String productType = getProductType();
        
        Log.event("Loaded Product Type :: " + productType);
       	String sizeFamily = selectSizeFamily();

        if (productType.equals("master") || productType.equals("variation")) {
            String size = selectRegularOrSplitSize();
            String color = selectColor();
            String productName = getProductName();
            if (isShoeSizeAvailable()) {
                size = selectshoeSizeWidth();
                if (isWidthAvailable()) {
                	Log.event("Shoe Width available.");
                	selectWidthBasedOnIndex(0);
                }
                Log.event("Shoe size selected.");
            } else if (isBraSizeAvailable()) {
                size = selectBraBandSize();
                if (isBraCupAvailable()) {
                	Log.event("Cup size available.");
                    selectCupBasedOnIndex(0);
                }
                Log.event("Bra size selected.");
            }
            
            Log.message("--->>> Product Name :: " + productName);
            Log.message("--->>> Selected Size Family :: " + sizeFamily);
            Log.message("--->>> Selected Color :: " + color);
            Log.message("--->>> Selected Size :: " + size);
        } else if (productType.equals("physical-gc")) {
            //Log.message("Selected Card Design :: " + selectColor());
            Log.message("Selected Amount is :: " + selectGCSize(), driver);
            checkGCPersonalizedMessage("check");
            enterGCToAddress("automation@yopmail.com");
            enterGCFromAddress("automation@yopmail.com");
            enterGCPersonalMessage("Test Personal Message");
        } else if (productType.equals("electronic-gc")) {
            //Log.message("Selected Card Design :: " + selectColor(), driver);
            Log.message("Selected Amount is :: " + selectGCSize(), driver);
            enterRecepientEmailAdd("automation@yopmail.com");
            enterRecepientEmailConfirmAdd("automation@yopmail.com");
            checkGCPersonalizedMessage("check");
            enterGCFromAddress("automation@yopmail.com");
            enterGCPersonalMessage("Test Personal Message");
        } else if (productType.equals("standardproductset")) {
            //Product Set being handled in different methods
        } else if (productType.equals("specialproductset")) {
            for (WebElement childPrd : childProducts) {
                WebElement firstSize = childPrd.findElements(By.cssSelector(".product-variations .attribute.size .swatches.size li.selectable")).get(0);
                BrowserActions.clickOnElementX(firstSize, driver, "First Size in Child Product");
                Utils.waitUntilElementDisappear(driver, waitLoader);
            }
        } else {
            Log.fail("New Product Type is identified. Please add handling steps for new product type.");
        }
    }

    /**
     * To add product to bag and close overlay
     * <br> Will select variation if not previously selected
     * @throws Exception
     */
    public void addToBagCloseOverlay() throws Exception {
    	selectAllSwatches();
    	clickAddProductToBag();

        if (Utils.waitForElement(driver, outofStockErrMsg)) {
            Log.failsoft("Product Out of Stock!", driver);
            return;
        }

        closeAddToBagOverlay();
    }
  
	 /**
	 * To click on product name in ATB overlay
	 * @return void
	 * @throws Exception - Exception
	 */
	 public void clickProductNameInMCOverlay() throws Exception {
		 Utils.waitForElement(driver, lnkProductNameInMCOverLay);
		 BrowserActions.clickOnElement(lnkProductNameInMCOverLay, driver, "Product name in MCOverlay");
		 Utils.waitForPageLoad(driver);
	 }
    
    /**
     * To check personalization checkbox in Gift card PDP
     * @param enable - true if gift card needed to be checked, else false
     * @throws Exception - Exception
     */
    public void clickGiftCardCheckbox(Boolean enable) throws Exception {
        if (enable) {
            if (chkGCPersonalizedMessage.isSelected() == false) {
                BrowserActions.javascriptClick(chkGCPersonalizedMessage, driver, " check box");
            }
        } else {
            if (chkGCPersonalizedMessage.isSelected() == true) {
                BrowserActions.javascriptClick(chkGCPersonalizedMessage, driver, "check box");
            }
        }
    }

    /**
     * To click on reviews tab
     * @throws Exception
     */
    public void clickReviewsTab() throws Exception {
        BrowserActions.clickOnElementX(lnkReviewsTab, driver, "Reviews Tab");
    }

    /**
     * To verify review tab is currently displayed
     * @return boolean
     * @throws Exception
     */
    public boolean verifyReviewsTabOpened() throws Exception {
        if (lnkReviewsTab.getAttribute("class").contains("current"))
            return true;
        else
            return false;
    }

    /**
     * To verify the breadcrumb names
     * @param productName - String
     * @param category - String(Optional)
     * @return true - if given names are equal.
     * @throws Exception
     */
    public boolean verifyBreadCrumb(String productName, String... category) throws Exception {
        List<String> actualValue = new ArrayList<String>();
        List<String> expectedValue = new ArrayList<String>();
        for (WebElement breadcrumb : lstBreadCrumbElementsDesktop) {
            actualValue.add(breadcrumb.getText().trim().toUpperCase());
        }
        
        expectedValue.add("HOME");
        if(category.length > 0) {
        	for(String itr : category) {
        		expectedValue.add(itr.toUpperCase());
        	}
        }
        expectedValue.add(productName.toUpperCase());

        Log.event("Actual Values from WebElement :: " + actualValue.toString().toLowerCase());
        Log.event("Expected Values from WebElement :: " + expectedValue.toString().toLowerCase());
        
        if (actualValue.containsAll((expectedValue)))
            return true;
        else
            return false;
    }
    
    /**
     * To verify the breadcrumb names for Mobile
     * @param category - String(Optional)
     * @return true - if given names are equal.
     * @throws Exception
     */
    public boolean verifyBreadCrumbMobile(String... category) throws Exception {
        String actualValue = category.length !=0 ? category[0] : "HOME";
        String expectedValue = lblCurrentBreadCrumbMobile.getText().trim();

        Log.event("Actual Values from WebElement :: " + actualValue.toString());
        Log.event("Expected Values from WebElement :: " + expectedValue.toString());
        
        if (actualValue.equalsIgnoreCase(expectedValue)) {
        	return true;
        }
        else {
        	return false;
        }
    }

    /**
     * to click on Breadcrumb
     * @param crumb -
     * @return String
     * @throws Exception - Exception
     */
    public Object clickBreadCrumb(String crumb) throws Exception {
        if (crumb.equalsIgnoreCase("home")) {
            Utils.waitUntilElementClickable(driver, lstBreadCrumbElementsDesktop.get(0), 5);
            BrowserActions.clickOnElementX(lstBreadCrumbElementsDesktop.get(0), driver, "Home in breadcrumb");
            Utils.waitForPageLoad(driver);
            return new HomePage(driver).get();
        } else {
            Utils.waitUntilElementClickable(driver, lstBreadCrumbElementsDesktop.get(1), 5);
            BrowserActions.clickOnElementX(lstBreadCrumbElementsDesktop.get(1), driver, "category in breadcrumb");
            Utils.waitForPageLoad(driver);
            return new PlpPage(driver).get();
        }
    }
    
    /**
     * to click on Bread crumb
     * @param crumb -
     * @return String
     * @throws Exception - Exception
     */
    public PlpPage clickLastBreadCrumbLink() throws Exception {
    	int size = lstBreadCrumbElementsDesktop.size();
	    BrowserActions.clickOnElementX(lstBreadCrumbElementsDesktop.get(size-2), driver, "Last breadcrumb category");
	    Utils.waitForPageLoad(driver);
	    return new PlpPage(driver).get();
    }

    /**
     * to verify Facebook share
     * @return String
     * @throws Exception - Exception
     */
    public Boolean verifyFacebookShare() throws Exception {
        Boolean flag = false;
        BrowserActions.clickOnElementX(lnkFaceBook, driver, "facebook");
        ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        if (driver.getTitle().toLowerCase().contains("facebook"))
            flag = true;
        driver.close();
        driver.switchTo().window(tabs2.get(0));
        return flag;
    }

    /**
     * to verify pinteres share
     * @return String
     * @throws Exception - Exception
     */
    public Boolean verifyPinterestShare() throws Exception {
        Boolean flag = false;
        BrowserActions.clickOnElementX(lnkPinterest, driver, "pinterest");
        ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        if (driver.getTitle().equalsIgnoreCase("pinterest"))
            flag = true;
        driver.close();
        driver.switchTo().window(tabs2.get(0));
        return flag;
    }

    /**
     * to verify TwitterShare
     * @return String
     * @throws Exception - Exception
     */
    public Boolean verifyTwitterShare() throws Exception {
        Boolean flag = false;
        BrowserActions.clickOnElementX(lnkTwitter, driver, "twitter");
        ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        if (driver.getTitle().contains("Twitter"))
            flag = true;
        driver.close();
        driver.switchTo().window(tabs2.get(0));
        return flag;
    }

    /**
     * to Verify Mailshare
     * @return String
     * @throws Exception - Exception
     */
    public Boolean verifyMailShare() throws Exception {
        Boolean flag = false;
        BrowserActions.clickOnElementX(lnkMail, driver, "mail");
        ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        if (driver.getTitle().equalsIgnoreCase("mail"))
            flag = true;
        driver.close();
        driver.switchTo().window(tabs2.get(0));
        return flag;
    }

    /**
     * to get Text In BreadCrumb
     * @param runMode -
     * @return String
     * @throws Exception 
     */
	public List<String> getTextInBreadcrumb(String... runMode) throws InterruptedException {
    	List<String> breadcrumbText = new ArrayList<>();
    	try {
	        BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(0), driver);
	        if (Utils.isDesktop() || (runMode.length > 0)) {
	            for (WebElement element : lstTxtProductInBreadcrumb) {
	                if (!element.getText().equals(""))
	                    breadcrumbText.add(element.getText());
	            }
	        }
        } catch (Exception e) {
			e.printStackTrace();
		}
        return breadcrumbText;
    }

    /**
     * To click on index-th breadcrumb category link
     * @param index -
     * @throws Exception -
     */
    public void clickOnBreadCrumbValue(int index) throws Exception {
        WebElement ele = driver.findElements(By.cssSelector(".breadcrumb-element.hide-mobile")).get(index - 1);
        BrowserActions.clickOnElement(ele, driver, "Home in Breadcrumb");
    }

    /**
     * to get Savings story Details in pdp page
     * @return String
     * @throws Exception - Exception
     */
    public boolean getSavingsStoryDetails() throws Exception {

        boolean flag = false;
        String savingStory = BrowserActions.getText(driver, txtSavingStory, "Savings story");

        savingStory = savingStory.toLowerCase();

        if (savingStory.contains("%") && savingStory.contains("off") && savingStory.contains("up to")) {
            savingStory = savingStory.replaceAll("[\\D]", "");
            if ((Integer.parseInt(savingStory) > 10) && !savingStory.contains(".")) {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * To verify clearance message contains "clearance:"
     * @return boolean -
     * @throws Exception -
     */
    public boolean getClearanceMessage() throws Exception {

        boolean flag = false;
        String clearanceMessage = BrowserActions.getText(driver, txtSavingStory, "Clearance Message");
        clearanceMessage = clearanceMessage.toLowerCase();
        if (clearanceMessage.contains("clearance")) {
            flag = true;
        }
        return flag;
    }

    /**
     * to validate placeholder text for monogramming text field
     * @return String
     * @throws Exception - Exception
     */
    public boolean validatePlaceholderTextforMonogrammingTextfield() throws Exception {
        for (int i = 0; i < monogramingTextField.size(); i++) {

            JavascriptExecutor executor = (JavascriptExecutor) driver;
            String retrivedValue = (String) executor.executeScript(" return document.querySelector('input[name=monogramminglines-" + (i + 1) + "]').getAttribute('placeholder')");

            if (!retrivedValue.contains("Enter Text")) {
                return false;
            }
        }
        return true;
    }

    /**
     * to validate place holder text color for monogramming text field
     * @return String
     * @throws Exception - Exception
     */
    public boolean validatePlaceholderTextColorforMonogrammingTextfield() throws Exception {
        for (int i = 0; i < monogramingTextField.size(); i++) {

            JavascriptExecutor executor = (JavascriptExecutor) driver;
            String retrivedValue = (String) executor.executeScript(" return document.querySelector('input[name=monogramminglines-" + (i + 1) + "]').getAttribute('class')");

            if (!retrivedValue.equals("input-fields monogramming-textfield mono-error")) {
                return false;
            }
        }
        return true;
    }

    /**
     * To verify the colors are displaying in red for the error in monogram fields
     * @return cssColor - Color to verify
     * @throws Exception - Exception
     */
    public boolean errorMsgVerificationInMonogramFields(String cssColor) throws Exception {
        boolean status = false;
        if (Utils.waitForElement(driver, drpColorSingleSelect)) {
            Log.reference("Only one color is available, So color error message cannot be verified");
            status = true;
        } else {
            String value = divMonogramColor.getCssValue("color");
            if (value.trim().equals(cssColor)) {
                status = true;
            } else {
                return false;
            }
        }
        if (Utils.waitForElement(driver, drpLocationSingleSelect)) {
            Log.reference("Only one location is available, So location error message cannot be verified");
            status = true;
        } else {
            String value = divMonogramLocation.getCssValue("color");
            if (value.trim().equals(cssColor)) {
                status = true;
            } else {
                return false;
            }
        }
        if (Utils.waitForElement(driver, drpFontSingleSelect)) {
            Log.reference("Only one font is available, So font error message cannot be verified");
            status = true;
        } else {
            String value = divMonogramFont.getCssValue("color");
            if (value.trim().equals(cssColor)) {
                status = true;
            } else {
                return false;
            }
        }
        return status;
    }

    /**
     * To verify vertical alignment of child products
     *
     * @return boolean -
     * @throws Exception -
     */
    public boolean verifyVerticalAllignmentOfChildProductsInCart() throws Exception {
        for (int i = 1; i < lstSpecialProductChildInCart.size(); i++) {
            if (BrowserActions.verifyVerticalAllignmentOfElements(driver, lstSpecialProductChildInCart.get(i - 1), lstSpecialProductChildInCart.get(i)) == false)
                return false;

        }
        return true;
    }

    /**
     * to verfiy Amount Dropdown Error
     *
     * @param obj -
     * @return String
     * @throws Exception - Exception
     */
    public boolean verifyAmountDropdownError(Object obj) throws Exception {
        if (Utils.waitForElement(driver, drpGiftCardSizeSelect)) {
            BrowserActions.scrollToViewElement(drpGiftCardSizeSelect, driver);
            Log.event("Select Box displayed. Classname :: " + drpGiftCardSizeSelect.getAttribute("class"));
            if (drpGiftCardSizeSelect.getAttribute("class").contains("error"))
                return true;
        } else {
            BrowserActions.scrollToViewElement(drpGiftCardSize, driver);
            String errorColorGiftCard = TestData.get("dropdown_error");
            return elementLayer.verifyElementColor("drpGiftCardSize", errorColorGiftCard, obj);
        }

        return false;
    }

    /**
     * to Click on Empty space
     * @throws Exception - Exception
     */
    public void clickOnEmptySpace() throws Exception {

        int xScrollPosition = 10; //enter your x co-ordinate
        int yScrollPosition = 10; //enter your y co-ordinate

        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("window.scroll(" + xScrollPosition + ", " + yScrollPosition + ");");
        executor.executeScript("arguments[0].click();", closeToolTipOverLay);
    }

    /**
     * To scroll window to add to bag button
     * @throws Exception -
     */
    public void moveToAddToBagButton() throws Exception {
        BrowserActions.scrollToViewElement(btnAddToCart, driver);
    }

    /**
     * To refresh page
     * @throws Exception -
     */
    public void refreshPage() throws Exception {
        driver.navigate().refresh();
        Utils.waitForPageLoad(driver);
    }

    /**
     * to verify share icon position
     * @return String
     * @throws Exception - Exception
     */
    public boolean verifyShareIconPosition() throws Exception {
        boolean flag = false;

        if (Utils.isDesktop()) {

            String txtShareIcon = spanShareIconList.get(0).getText();
            Log.message(txtShareIcon);
            if (txtShareIcon.toUpperCase().contains("SHARE")) {

                flag = true;
            }

        } else if (Utils.isTablet()) {

            String txtShareIcon = spanShareIconListTablet.get(0).getText();
            Log.message(txtShareIcon);
            if (txtShareIcon.toUpperCase().contains("SHARE")) {

                flag = true;
            }
        }

        return flag;
    }

    /**
     * to click on Empty space In ABT overlay
     * @throws Exception - Exception
     */

    public void clickOnEmptySpaceInABTOverlay() throws Exception {
        try {
            WebElement abtOverlay = driver.findElement(By.cssSelector(".ui-widget-overlay.ui-front"));
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].click();", abtOverlay);
        } catch (NoSuchElementException e) {
            Log.event("No ABT Overlay");
        }
    }

    /**
     * To click on Home breadcrumb link
     * @return HomePage -
     * @throws Exception -
     */
    public HomePage clickOnHomeBC() throws Exception {
        if (Utils.isMobile()) {
            while (Utils.waitForElement(driver, lnkBCHomeMobile)) {
                BrowserActions.clickOnElementX(lnkBCHomeMobile, driver, "Home Link in BC");
            }
        } else {
            BrowserActions.clickOnElementX(lnkBCHome, driver, "Home Link in BC");
        }
        Utils.waitForPageLoad(driver);
        return new HomePage(driver).get();
    }

    /**
     * To Click on Bread crumb level
     * @param level - the breadcrumb level to click
     * @return PlpPage
     * @throws Exception - Exception
     */
    public PlpPage clickOnBreadCrumbLevel(int level) throws Exception {
        if (Utils.isMobile()) {
            BrowserActions.clickOnElementX(lnkBCHomeMobile, driver, "Level Element");
        } else {
            WebElement element = driver.findElements(By.cssSelector(".breadcrumb-element.hide-mobile")).get(level);
            BrowserActions.clickOnElementX(element, driver, "Level Element");
        }
        return new PlpPage(driver).get();
    }

    /**
     * To get bread crumb level text
     * @param level - the breadcrumb level for which text need to be taken
     * @return String returning the text
     * @throws Exception - Exception
     */
    public String getBreadCrumbLevelText(int level) throws Exception {
        WebElement element = null;
        if (Utils.isMobile()) {
            element = lnkBCHomeMobile;
        } else {
            element = driver.findElements(By.cssSelector(".breadcrumb-element.hide-mobile")).get(level);
        }
        return BrowserActions.getText(driver, element, "Category element");
    }
    
    /**
     * Get number of breadcrumbs
     * @return total number of breadcrumb on current PDP. Mobile returns 1.
     * @throws Exception
     */
    public int getBreadcrumbElementNumber() throws Exception{
    	if(!Utils.isMobile()) {
    		return driver.findElements(By.cssSelector(".breadcrumb-element.hide-mobile")).size();
    	}else
    		return 1;
    }

    /**
     * To get X co-ordinate of Product Badge
     * @return Integer -
     * @throws Exception -
     */
    public int getXLocOfBadge() throws Exception {
        return prodBadge.getLocation().getX();
    }

    /**
     * To get X co-ordinate of primary image
     *
     * @return Integer -
     * @throws Exception -
     */
    public int getXLocOfPrimaryImg() throws Exception {
        return mainProdImage.getLocation().getX();
    }

    /**
     * to get procut price Type in PS
     * @param prdNo -
     * @return String
     * @throws Exception - Exception
     */
    public String getProductPriceTypeInPS(int prdNo) throws Exception {

        WebElement price = lstSplProductSetProds.get(prdNo - 1).findElement(By.cssSelector(".product-price"));
        String html = price.getAttribute("innerText");
        String priceType = new String();
        if (html.contains("class=\"price-sales \""))
            priceType = "regular";
        else if (html.contains("price-sales sdfs price-standard-exist") && price.findElement(By.cssSelector(".price-sales.sdfs.price-standard-exist")).getText().contains("-"))
            priceType = "salePriceRange";
        else if (html.contains("<span>") && price.findElement(By.cssSelector("span")).getText().contains("-"))
            priceType = "regularRange";
        else
            priceType = "sale";

        return priceType.toLowerCase();
    }


    /**
     * to get the text in Preview field
     *
     * @param prdNo -
     * @return String
     * @throws Exception - Exception
     */
    public String getProductPriceInPS(int prdNo) throws Exception {
        WebElement price = lstSplProductSetProds.get(prdNo - 1).findElement(By.cssSelector("product-price"));
        String html = price.getAttribute("innerText");
        String priceType = new String();
        if (html.contains("class=\"price-sales \""))
            priceType = price.findElement(By.cssSelector(".price-sales")).getText();
        else if (html.contains("price-sales sdfs price-standard-exist") && price.findElement(By.cssSelector(".price-sales.sdfs.price-standard-exist")).getText().contains("-"))
            priceType = price.findElement(By.cssSelector(".price-sales.sdfs.price-standard-exist")).getText();
        else if (html.contains("<span>") && price.findElement(By.cssSelector("span")).getText().contains("-"))
            priceType = price.findElement(By.cssSelector("span")).getText();
        else
            priceType = price.findElement(By.cssSelector(".price-sales")).getText();

        return priceType.toLowerCase();
    }


    /**
     * To get number of child products availabe in product set
     *
     * @return Integer -
     * @throws Exception -
     */
    public int getNumberOfProductsInProductSet() throws Exception {
        Log.event("Number of Child Product Available :: " + childProducts.size());
        return childProducts.size();
    }


    /**
     * To get number of products with size swatch selected
     *
     * @return Integer -
     * @throws Exception -
     */
    public int getNumberOfProductsSizeSwatchesSelected() throws Exception {
        Log.event("Number of Child Product Available with Size Selected:: " + lstSelectedSizeSwatch.size());
        return lstSelectedSizeSwatch.size();
    }

    /**
     * To get number of products without size swatch selected
     *
     * @return Integer -
     * @throws Exception -
     */
    public int getNumberOfProductsSizeSwatchesNotSelected() throws Exception {
        int x = lstSelectedSizeSwatch.size();
        int y = childProducts.size();

        Log.event("Number of Products without Size Selected ::" + (y - x));
        return (y - x);
    }

    /**
     * To get number of products with color swatch selected
     *
     * @return Integer - Color swatch size
     * @throws Exception - Exception
     */
    public int getNumberOfProductsColorSwatchesSelected() throws Exception {
        Log.event("Number of Child Product Available with Color Selected:: " + lstSelectedColorSwatch.size());
        return lstSelectedColorSwatch.size();
    }

    /**
     * To get stock value(only number part) for inventory message
     *
     * @return Integer - Stock
     * @throws Exception - Exception
     */
    public int getStockValuefrmInventoryMsg() throws Exception {
        String msg = BrowserActions.getText(driver, inventoryState, "product availability message");
        Log.event("Text Extracted from Inventory message :: " + msg);

        if(!msg.contains("Out of Stock")) {
        	msg = msg.replaceAll("[^0-9]", "");
        	Log.event("Number Extracted from Inventory message :: " + msg);
        } else {
        	Log.warning("Product inventory \"Out of Stock\"");
        }

        int stock = 0;
        try {
            stock = Integer.parseInt(msg);
        } catch (NumberFormatException e) {
            Log.warning("Give product have stock less than 5.");
        }
        return stock;
    }

    /**
     * To verify width section available in PDP
     *
     * @return boolean - true/false if width is available
     * @throws Exception - Exception
     */
    public boolean isWidthAvailable() throws Exception {
        if (Utils.waitForElement(driver, divWidth))
            return true;
        else
            return false;
    }

    /**
     * To verify is a PDP has shoe size option
     * @return boolean - true/false is show size is available
     * @throws Exception - Exception
     */
    public boolean isShoeSizeAvailable() throws Exception {
        if (Utils.waitForElement(driver, divShoeSize))
            return true;
        else
            return false;
    }

    /**
     * To verify is a PDP has bra size option
     * @return boolean - true/false is bra size is available
     * @throws Exception - Exception
     */
    public boolean isBraSizeAvailable() throws Exception {
        if (Utils.waitForElement(driver, divBraSize))
            return true;
        else
            return false;
    }

    /**
     * To verify is a PDP has bra size option
     * @return boolean - true/false is bra size is available
     * @throws Exception - Exception
     */
    public boolean isBraCupAvailable() throws Exception {
        if (Utils.waitForElement(driver, divCupSize))
            return true;
        else
            return false;
    }

    /**
     * To verify if regular size is available on PDP
     * @return boolean - true/false is regular size is available
     * @throws Exception - Exception
     */
    public boolean isRegularSizeAvailable() throws Exception {
        if (Utils.waitForElement(driver, divRegularSize))
            return true;
        else
            return false;
    }

    /**
     * To verify special product message is displayed
     * @return boolean - true/falseif special message is displayed
     * @throws Exception - Exception
     */
    public boolean isSpecialProductMessageDisplayed() throws Exception {
        return Utils.waitForElement(driver, lblProductSpecialMessage);
    }

    /**
     * To get the star ratings for the product set
     * @return String[] - review stars
     * @throws Exception - Exception
     */
    public String[] getProductReviewStarInPrdSet() throws Exception {
        String starvalue = imgRatingStarsPrdSet.getAttribute("class");
        String star[] = new String[2];
        star[0] = starvalue.split("\\-")[1];
        star[1] = starvalue.split("\\-")[2];
        return star;
    }

    /**
     * To verify the product review.
     * @param valueToBeVerified - valueToBeVerified
     * @return boolean - value
     * @throws Exception - Exception
     */
    public boolean verifyProductReview(String valueToBeVerified) throws Exception {
        WebElement reviewText = null;
        if (productReviewStars.getAttribute("class").contains("0-0"))
            reviewText = driver.findElement(By.cssSelector(".product-teasers a"));
        String value = BrowserActions.getText(driver, reviewText, "Veriyfing review text");
        if (value.equalsIgnoreCase(valueToBeVerified))
            return true;
        else
            return false;
    }

    /**
     * To verify the product review star
     * @return String - star count
     * @throws Exception - Exception
     */
    public String getProductReviewStar() throws Exception {
        String starvalue = productReviewStars.getAttribute("class");
        return starvalue.split("\\-")[1];
    }

    /**
     * To verify the product review half star count
     * @return String - half star count
     * @throws Exception - Exception
     */
    public String getProductReviewHalfStar() throws Exception {
        String starvalue = productReviewStars.getAttribute("class");
        return starvalue.split("\\-")[2];

    }

    /**
     * To get the star ratings for the product
     * @return String[]
     * @throws Exception -
     */
    public String[] getProductReviewStarHalfStar() throws Exception {
        String starvalue = imgRatingStars.getAttribute("class");
        String star[] = new String[2];
        star[0] = starvalue.split("\\-")[1];
        star[1] = starvalue.split("\\-")[2];
        return star;
    }

    /**
     * To verify the product set current product
     * @param productset -
     * @return boolean value -
     * @throws Exception -
     */
    public boolean verifyCurrentProduct(String productset) throws Exception {
        String psName = driver.findElements(By.cssSelector(".breadcrumb-element.hide-mobile")).get(1).getAttribute("innerHTML").toLowerCase();
        Log.event("Value From BreadCrumb :: " + psName);
        Log.event("Value From User :: " + productset.toLowerCase());
        if (psName.toLowerCase().trim().contains(productset.toLowerCase().trim()))
            return true;
        else
            return false;
    }

    /**
     * To verify the default selected color in the product set
     * @return int - size
     * @throws Exception - Exception
     */
    public int verifyDefaultColorSelectedInproductSet() throws Exception {
        int size = lstColorValue.size();
        return size;
    }

    /**
     * To move the cursor to add bag button
     *
     * @throws Exception - Exception
     */
    public void moveToAddBag() throws Exception {
        BrowserActions.mouseHover(driver, btnAddToBagSpecialProductSet);
    }

    /**
     * To verify the add bag error message
     * @param index - index
     * @param errorMessage - errorMessage
     * @return boolean - true/false if bag error message is correct
     * @throws Exception - Exception
     */
    public boolean verifyAddtoBagErrorMessage(int index, String errorMessage) throws Exception {
        if (btnAddToCartProdset.get(index - 1).getText().equalsIgnoreCase(errorMessage)) {
            return true;
        } else
            return false;
    }

    /**
     * To verify the label text for selected size
     * @param index - index
     * @param size  - size
     * @return boolean - value
     * @throws Exception - Exception
     */
    public boolean verifySelectedSizevalueLabel(int index, String size) throws Exception {
        String selectedSize = selectedSizeValue.get(index - 1).getText();
        Log.event("Selected Size :: " + size);
        Log.event("Displayed Size :: " + selectedSize);

        if (selectedSize.equalsIgnoreCase(size)) {
            return true;
        } else
            return false;
    }

    /**
     * To verify the label text for selected size
     * @param index      - index
     * @param sizeFamily - sizeFamily
     * @return boolean - value
     * @throws Exception - Exception
     */
    public boolean verifySelectedSizeFamilyLabel(int index, String sizeFamily) throws Exception {
        if (selectedSizefamily.get(index - 1).getText().equalsIgnoreCase(sizeFamily)) {
            return true;
        } else
            return false;
    }

    public boolean verifyMCOverlayStockAvailability(String avail) throws Exception {
        System.out.println("Avail" + avail);
        System.out.println("Avail1" + lnkProductAvailabilityInMCOverlay.getText());
        if (lnkProductAvailabilityInMCOverlay.getText().equalsIgnoreCase(avail)) {
            return true;
        } else
            return false;
    }

    /**
     * To verify total amount
     * @return boolean - true/false if total is correct
     * @throws Exception - Exception
     */
    public boolean verifyTotalAmount() throws Exception {
        List<WebElement> prcingWeb = driver.findElements(By.cssSelector(".minicartpopup .slick-active .mini-cart-product-info .mini-cart-pricing .value"));
        WebElement totalWeb = driver.findElement(By.cssSelector(".minicartpopup .slick-active .mini-cart-product-info .mini-cart-pricing .total-price .value"));
        String unit = prcingWeb.get(0).getText();
        String price = lnkProductPriceInMCOverlay.getText();
        String total = totalWeb.getText();
        float unit1 = Float.parseFloat(unit);
        System.out.println("unit1" + unit1);
        float amount = Float.parseFloat(price.split("\\$")[1]);
        float totalamount = unit1 * amount;

        System.out.println("amount" + amount);
        float total1 = Float.parseFloat(total.split("\\$")[1]);
        System.out.println("total1" + total1);

        if (totalamount == total1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * To Check the add to cart button disabled state
     * @return String - value
     * @throws Exception - Exception
     */
    public String addToCartButtonDisabledState() throws Exception {
        BrowserActions.mouseHover(driver, btnAddToCartSplProdset);
        btnAddToCartSplProdset.click();
        Actions actions = new Actions(driver);
        actions.moveToElement(btnAddToCartSplProdset).contextClick(btnAddToCartSplProdset);
        //BrowserActions.actionClick(btnAddToCartSplProdset, driver, "button");
        return BrowserActions.getTextFromAttribute(driver, btnAddToCartSplProdset, "disabled", "Number");
    }
    
    /**
     * To click on the In-stock product image in the recommendation section
     * @return String - Product name of selected recommendation product
     * @throws Exception - Exception
     */
    public String clickInStockRecommendationPrdImage() throws Exception {
    	String prdID = null, price, prdName = null;
		List<String> prdNameList = null;
    	List<WebElement> eleImg, elePrice, eleTile;
    	WebElement slickPrev = null;
    	
    	if(BrandUtils.isBrand(Brand.bh)) {
    		eleTile = tileRecommendationProds_BH;
    		eleImg = prodImageListRecommendation_BH;
    		elePrice = lstProdPriceRecommendation_BH;
    		slickPrev = prevArrowRecommendationBH;
    	} else {
    		if (Utils.isDesktop()) {
    			eleTile = tileRecommendationProdDesktop;
    			eleImg = prodImageListRecommendationDesktop;
    			elePrice = lstProdPriceRecommendationDesktop;
    			slickPrev = prevArrowRecommendationDesktop;
    		} else {
    			eleTile = tileRecommendationProdTabletMobile;
    			eleImg = prodImageListRecommendationMobileTablet;
    			elePrice = lstProdPriceRecommendationMobileTablet;
    			slickPrev = prevArrowRecommendationMobileTablet;
    		}
    	}
    	
    	while(Utils.waitForElement(driver, slickPrev)) {
    		scrollRecommendationImageInSpecifiedDirection("Prev");
    	}
    	
    	for(int index = 0; index < getNoOfRecommendationProduct(); index++) {
    		BrowserActions.scrollInToView(eleImg.get(index), driver);
    		price = BrowserActions.getText(driver, elePrice.get(index), "Recommendation Prod price");
    		prdNameList = getRecentlyViewedsectionAllProdImageName();
    		prdName = eleTile.get(index).findElement(By.cssSelector(".product-name")).getText();
    		if(price.contains("N/A")) {
    			Log.event("Recommendation product is out of stock");
    			continue;
    		} else if(prdNameList.contains(prdName)) {
    			Log.event("This Product already visited.., Trying to select next recommended product");
    			continue;
    		} else {
    			BrowserActions.scrollInToView(eleTile.get(index), driver);
    			prdID = BrowserActions.getTextFromAttribute(driver, eleTile.get(index), "data-itemid", "Product Id");
    			if (Utils.isDesktop()) {
    				BrowserActions.clickOnElementX(eleImg.get(index), driver, "Instock Recommedation product image");
    			} else {
    				BrowserActions.javascriptClick(eleImg.get(index), driver, "Instock Recommedation product image");
    			}
    			break;
    		}
    	}
    	
    	Utils.waitForPageLoad(driver);
    	return prdID.trim();
    }
    
    /**
     * To click on the product image in the Recently viewed section
     * @param index - index of the product
     * @throws Exception - Exception
     */
    public String clickRecentlyViewedPrdImageByIndex(int index) throws Exception {
    	String name = BrowserActions.getText(driver, recentlyViewedProdName.get(index), "Recently viewed Product");
        BrowserActions.javascriptClick(recentlyViewedProdImage.get(index), driver, "Recently viewed Prod image");
        Utils.waitForPageLoad(driver);
        return name.trim().replace("amp;", "");
    }
    
    /**
     * To click on the product image in the recommendation section
     * @param index - index of the product
     * @throws Exception - Exception
     */
    public String clickRecommendationPrdImageByIndex(int... recommPrdIndex) throws Exception {
        String name;
        List<WebElement> eleName, eleImg;
        WebElement slickNext = null;
        
        if(BrandUtils.isBrand(Brand.bh)) {
        	eleName = prodNameListRecommendationD_BH;
        	eleImg = prodImageListRecommendation_BH;
        	slickNext = nextArrowRecommendationBH;
        } else {
        	if (Utils.isDesktop()) {
        		eleName = prodNameListRecommendationDesktop;
	        	eleImg = prodImageListRecommendationDesktop;
	        	slickNext = nextArrowRecommendationDesktop;
        	} else {
        		BrowserActions.scrollInToView(sectionRecommendationMobileTablet, driver);
        		eleName = prodNameListRecommendationMobileTablet;
              	eleImg = prodImageListRecommendationMobileTablet;
              	slickNext = nextArrowRecommendationMobileTablet;
        	}
        }
        
        int index = recommPrdIndex.length > 0 ? recommPrdIndex[0] : Utils.getRandom(0, eleName.size());
        if (!Utils.waitForElement(driver, eleName.get(index))) {
			if (!Utils.waitForElement(driver, eleName.get(0))) {
				scrollRecommendationImageInSpecifiedDirection("Prev");
			}
			while (!Utils.waitForElement(driver, eleName.get(index))) {
				BrowserActions.clickOnElementX(slickNext, driver, "Next button on recommendation");
			}
		}
		
        if (Utils.isDesktop()) {
            name = BrowserActions.getText(driver, eleName.get(index), "Recommendation Prod Name");
            BrowserActions.clickOnElementX(eleImg.get(index), driver, "Recommedation prduct image");
        } else {
            name = BrowserActions.getText(driver, eleName.get(index), "Recommendation Prod Name");
            BrowserActions.javascriptClick(eleImg.get(index), driver, "Recommedation prduct image");
        }
        Utils.waitForPageLoad(driver);
        return name.trim().replace("amp;", "");
    }

	/**
     * To check the current product is displayed in the recommendation section
     *
     * @param currentPrdId - Current product ID
     * @return boolean - true when current product is not present, else false
     * @throws Exception - Exception
     */
    public boolean verifyRecommendationHaveCurrentProduct(String currentPrdId) throws Exception {
    	List<WebElement> elemList = null;
    	if(BrandUtils.isBrand(Brand.bh)) {
    		elemList = tileRecommendationProds_BH;
    	} else {
    		if (Utils.isDesktop()) {
                elemList = tileRecommendationProdDesktop;
            } else {
                elemList = tileRecommendationProdTabletMobile;
            }
    	}

        int count = elemList.size();
        for (int k = 0; k < count; k++) {
            WebElement elem = elemList.get(k);
            String prdId = BrowserActions.getTextFromAttribute(driver, elem, "data-itemid", "Product Tile");
            if (prdId.equalsIgnoreCase(currentPrdId)) {
                return false;
            }
        }
        return true;
    }
    
    
    /**
     * To verify recommendations in PDP have product name, image, and price
     * @return true/false if all recommendation product tiles are verified to have correct content
     * @throws Exception 
     */
    public boolean verifyRecommendationTileContent()throws Exception{
    	List<WebElement> recommendationTiles = null;
    	WebElement slickNext = null;
    	if (BrandUtils.isBrand(Brand.bh)) {
    		recommendationTiles = tileRecommendationProds_BH;
        	slickNext = nextArrowRecommendationBH;
    	} else {
    		if(Utils.isDesktop()) {
    			recommendationTiles = tileRecommendationProdDesktop;
            	slickNext = nextArrowRecommendationDesktop;
    		} else {
    			recommendationTiles = tileRecommendationProdTabletMobile;
            	slickNext = nextArrowRecommendationMobileTablet;
    		}
    	}
    	
        for(WebElement recTile : recommendationTiles) {
        	WebElement tilePrdName, tilePrdImage, tilePrdPrice;
        	tilePrdImage = recTile.findElement(By.cssSelector(".p-image"));
        	tilePrdName = recTile.findElement(By.cssSelector(".product-name"));
        	tilePrdPrice = recTile.findElement(By.cssSelector(".product-pricing"));
        	
        	if(!BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(tilePrdImage, tilePrdName, tilePrdPrice))) {
				if(Utils.isMobile()) {
					Utils.waitForElement(driver, recTile);
					BrowserActions.dragElement(tilePrdImage, driver, "Tile image", Direction.Left);
					Log.message("Dragged recommendation slider.", driver);
				} else {
					BrowserActions.clickOnElementX(slickNext, driver, "Next on recommendation");
					Log.message("Clicked next on recommendation.", driver);
				}
				
				if(!BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(tilePrdImage, tilePrdName, tilePrdPrice))) {
					return false;
				}
        	}
        }
        return true;
    }
    
    
     /**
     * To select maximum quantity available
     *
     * @return String - Maximum Quantity selected
     * @throws Exception - Exception
     */
    public String selectMaxQty() throws Exception {
        if (Utils.waitForElement(driver, drpQty)) {
            BrowserActions.scrollToViewElement(drpQty, driver);
            Select qtySelect = new Select(drpQty);
            qtySelect.selectByIndex(qtySelect.getOptions().size() - 1);
            Utils.waitForPageLoad(driver);
            return drpQty.getAttribute("value") == null ? drpQty.getText() : drpQty.getAttribute("value");
        } else {
            BrowserActions.scrollToViewElement(selectedQty1, driver);
            BrowserActions.clickOnElementX(selectedQty1, driver, "Quantity dropdown");
            int maxQty = lstQuantityValues.size();
            WebElement qtyToSelect = driver.findElement(By.cssSelector(".quantity .selection-list li[label*='" + maxQty + "']"));
            BrowserActions.javascriptClick(qtyToSelect, driver, "Maximum Quantity");
            Utils.waitForPageLoad(driver);
            return selectedQty1.getText();
        }
    }

    /**
     * To verify Zoom image fit with Product detail section
     *
     * @return boolean - true/false if zoomed image fits detail section
     * @throws Exception - Exception
     */
    public boolean verifyZoomImageFitWithProductDetailSection() throws Exception {
        int zoomImg_x = divZoomWindowContainer.getLocation().x;
        int prdSection_x = sectionProductDetails.getLocation().x;

        return (zoomImg_x <= prdSection_x);
    }


    /**
     * To hover mouse over image
     *
     * @throws Exception - Exception
     */
    public void mouseHoverPrimaryImage() throws Exception {
        BrowserActions.mouseHover(driver, imgPrimaryImage, "Primary Image");
        BrowserActions.mouseHover(driver, imgPrimaryImage, "Primary Image");
        BrowserActions.javaScriptMouseHover(driver, imgPrimaryImage);
        Utils.waitForElement(driver, zoomLensProdImg, 10);
    }

    /**
     * To click and select a different size
     * @throws Exception - Exception
     */
    public void clickSelectUnselectedSize() throws Exception {
        BrowserActions.scrollInToView(sizeSwatches, driver);
        BrowserActions.clickOnElementX(sizesUnselected.get(Utils.getRandom(0, sizesUnselected.size())), driver, "Unselected Size");
        if(Utils.waitForElement(driver, txtProdAvailStatus) && txtProdAvailStatus.getText().contains("Out of Stock")) {
        	Log.event("Selected size out of stock. Trying different size.");
        	clickSelectUnselectedSize();
        }
    }

    /**
     * To hover on Add To Bag in Special Product Set
     * @throws Exception - Exception
     */
    public void mouseHoverAddToBagSPS() throws Exception {
        if (Utils.isDesktop()) {
            BrowserActions.moveToElementJS(driver, btnAddToBagPrdSet);
        } else {
            BrowserActions.scrollToViewElement(btnAddToBagPrdSet, driver);
            btnAddToBagPrdSet.click();
        }
    }

    /**
     * To check quickshop is displayed in recommendation section
     *
     * @return boolean - true if Quickshop is displayed, false if not
     * @throws Exception - Exception
     */
    public boolean checkQuickShopIsDisplayedInRecommendation() throws Exception {
	    BrowserActions.scrollInToView(tileRecommendationProd, driver);
	    BrowserActions.mouseHover(driver, tileRecommendationProd, "Product Tile");
		try{
		    WebElement qcButton = tileRecommendationProd.findElement(By.cssSelector(".quickview"));
		    if (Utils.waitForElement(driver, qcButton)) {
		    	return true;
		    }
		} catch(NoSuchElementException e){
		   	Log.message("Quick shop button is not displayed");
		}
    	return false;
    }

    /**
     * To verify range of hemming options
     *
     * @param rangeStart - start of range
     * @param rangeEnd   - end of range
     * @return boolean - if given the range matches
     * @throws Exception - Exception
     */
    public boolean verifyHemmingLengthRange(String rangeStart, String rangeEnd) throws Exception {
        if (Utils.waitForElement(driver, divHemmingDrawer)) {
            return lstHemmingOptions.get(0).getAttribute("data-hemming-option").equals(rangeStart)
                    && lstHemmingOptions.get(lstHemmingOptions.size() - 1).getAttribute("data-hemming-option").equals(rangeEnd);
        } else {
            Log.event("Hemming options are not dispalyed.");
            return false;
        }
    }

    /**
     * To get value of selected hemming option
     *
     * @return String - selected hemming
     * @throws Exception - Exception
     */
    public String getSelectedHemmingOption() throws Exception {
        for (WebElement hem : lstHemmingOptions) {
            if (hem.getAttribute("class").contains("selected")) {
                return hem.getAttribute("data-hemming-option");
            }
        }
        return "none";
    }

    /**
     * To select given hemming option
     *
     * @param hem - hemming option to select
     * @throws Exception - Exception
     */
    public void clickSelectHemOption(String hem) throws Exception {
        for (WebElement hemElement : lstHemmingOptions) {
            if (hemElement.getAttribute("data-hemming-option").equals(hem)) {
                BrowserActions.clickOnElementX(hemElement, driver, "Hemming Option");
            }
        }
    }

    /**
     * To verify if given hem option is selected
     *
     * @param hem - hem option to verify
     * @throws Exception - Exception
     */
    public boolean verifyHemOptionSelected(String hem) throws Exception {
        for (WebElement hemElement : lstHemmingOptions) {
            if (hemElement.getAttribute("data-hemming-option").equals(hem)) {
                if (hemElement.getAttribute("class").contains("selected")) {
                    return true;
                } else
                    return false;
            }
        }
        return false;
    }
    
    public List<String> getColorsAsList()throws Exception{
    	List<String> colors = new ArrayList<String>();
    	
    	List<WebElement> colorsEle = driver.findElements(By.cssSelector("#product-content ul.swatches.color a"));
    	for(WebElement ele:colorsEle){
    		String colorName = ele.getAttribute("title").split(":")[1];
    		if(colorName.contains(".")){
                colorName = colorName.split("\\.")[0];
            }
    		colors.add(colorName);
    	}
    	
    	return colors;
    }

    /**
     * To select the different size of the variation product, even size is already selected
     * @param size
     * @return selected color name
     * @throws Exception
     */
	public String selectDifferentSize(String... size) throws Exception {
		if ((size.length == 0) || (size.length > 0 && size[0].equals("random"))) {
            int index = 0;
            if (lstInStockSizeSwatches.size() > 0) {
            	index = Utils.getRandom(0, lstInStockSizeSwatches.size() - 1);
            	BrowserActions.clickOnSwatchElement(lstInStockSizeSwatches.get(index), driver, "Random size swatch: " + index);
             } else if (lstSizeSwatches.size() > 0) {
            	index = Utils.getRandom(0, lstSizeSwatches.size() - 1);
            	BrowserActions.clickOnSwatchElement(lstSizeSwatches.get(index), driver, "Random size swatch: " + index);
            } else if(lstShoeSizeSwatchesSelectable.size() >0) {
            	index =  Utils.getRandom(0, lstShoeSizeSwatchesSelectable.size() - 1);
            	BrowserActions.clickOnSwatchElement(lstShoeSizeSwatchesSelectable.get(index), driver, "Random size swatch: " + index);
            }
            Utils.waitForPageLoad(driver);
            return BrowserActions.getTextNoFail(driver, selectedSizeLabel, "Selected Size value");

        } else {
        	Log.event("Size to select: " + size[0]);
        	for (int i = 0; i < lstSizeSwatches.size(); i++) {
            	Log.event(lstSizeSwatches.get(i).getText().trim());
            	if (lstSizeSwatches.get(i).getText().trim().equals(size[0])) {
                    BrowserActions.clickOnSwatchElement(lstSizeSwatches.get(i), driver, size[0] + " Size Swatch ");
                    Log.event("Clicked On " + size[0] + " Size...");
                    waitForSpinner();
                    return BrowserActions.getText(driver, selectedSize, "Selected Size value");
                }
            }
            for (int i = 0; i < lstSizeSwatchesUnSelectable.size(); i++) {
                if (lstSizeSwatchesUnSelectable.get(i).getText().trim().equals(size[0])) {
                    if (size.length == 2) {
                        if (size[1].equals("true")) {
                        	BrowserActions.clickOnSwatchElement(lstSizeSwatchesUnSelectable.get(i), driver, "Size Swatches ");
                            Utils.waitUntilElementDisappear(driver, waitLoader);
                            return BrowserActions.getTextFromAttribute(driver, selectedSize, "data-swatchvalue", "Selected Size value");
                        }
                    } else {
                        Log.failsoft("--->>> Given Size(" + size[0] + ") <b>Not Available</b> in Size list.");
                        return null;
                    }
                }
            }
        }
        Utils.waitForPageLoad(driver);
        return null;
	}

    /**
     * To different color other than given color.
     * @param color - Color to be omitted
     * @return selected color name
     * @throws Exception - Exception
     */
	public String selectDifferentColor(String... color) throws Exception {
		WebElement colorSwatchCurrent = null, colorSwatch = null;
		String swatchColor = null;
		if (lstColorSelectable.size() == 0) {
			Log.failsoft("There are no color to select!", driver);
		} else {
			if (color.length == 0) {
				colorSwatch = lstLinkColorSelectable.get(Utils.getRandom(0, lstLinkColorSelectable.size()-1));
				swatchColor = colorSwatch.getAttribute("aria-label").trim().toLowerCase();
			} else {
				for (int i = 0; i < lstLinkColorSelectable.size(); i++) {
					colorSwatchCurrent = lstLinkColorSelectable.get(i);
					swatchColor = colorSwatchCurrent.getAttribute("aria-label").trim().toLowerCase();
					Log.event("Available colors is: " + swatchColor);
					if (!(swatchColor.contains(color[0].trim().toLowerCase()))) {
						colorSwatch = colorSwatchCurrent;
						break;
					}
				}
			}
			BrowserActions.clickOnSwatchElement(colorSwatch, driver, swatchColor + " Color Swatch ");
			Log.event("Selected color:: " + swatchColor);
			waitForSpinner();
			return BrowserActions.getText(driver, selectedColorVariant, "Selected color variant name");
		}
        return swatchColor;
	}
	
	/**
     * To get color code of given index alternate image
     *
     * @return String - color code
     * @throws Exception - Exception
     */
    public String getAlternateImageCode(int index) throws Exception {
        List<WebElement> alternateImage ;
        if (!Utils.isMobile()) {
                alternateImage = alternateImages;
        } else {
                alternateImage = overlaySlickDot;
        }
        String imgPath[] = BrowserActions.getTextFromAttribute(driver, alternateImage.get(index), "src", "Alternate image").trim().split("\\/");
        String imgCode = imgPath[imgPath.length - 1].split(".jpg")[0].replace(".jpg", "");
        return imgCode;
    }
    /**
     * To verify the given list of names available in the another list
     * @param productList - list of product names in the recently viewed section
     * @param prdNames - list of product names visited
     * @return - true if visited products available in the list
     */
	public boolean verifyProductNames(List<String> productList, List<String> prdNames) throws Exception {
		int count = 0;
		for(int i = 0; i < productList.size(); i++) {
			boolean check = false;
			for(int j =0; j < prdNames.size(); j++) {
				if(productList.get(i).equalsIgnoreCase(prdNames.get(j))) {
					check = true;
					break;
				}
			}
			if(check) {
				count++;
			}
		}
		if(count == productList.size()) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * To get product Brand from PDP page based on given product ID
	 * @param prdId - Product ID
	 * @return String of Current Brand short name
	 * @throws Exception - Exception
	 */
	public Brand getProductBrand() throws Exception {
		String currentURL = driver.getCurrentUrl();
		return UrlUtils.getBrandFromUrl(currentURL);
	}
	
	/**
	 * To get product variation ID from add to cart overlay
	 * <br>Variation ID is available only when a variation is added to cart.
	 * 
	 */
	public String getVariationFromCartOverlay() throws Exception {
		String variationURL = "", prdVariation = "";
		if (Utils.waitForElement(driver, mdlMiniCartOverLay)) {
			WebElement prdOverlayNameLink = mdlMiniCartOverLay.findElement(By.cssSelector(".mini-cart-name>a"));
			variationURL = prdOverlayNameLink.getAttribute("href");
		} else if (Utils.waitForElement(driver, lnkPrdVariationProperty)) {
			selectAllSwatches();
			variationURL = lnkPrdVariationProperty.getAttribute("innerHTML");
		}
		prdVariation = UrlUtils.getProductIDFromURL(variationURL);
		return prdVariation;
	}
	
	/**
	 * To get product Brand from PDP page based on given product ID
	 * @param prdId - Product ID
	 * @return String of Current Brand short name
	 * @throws Exception - Exception
	 */
	public String getProductIdFromUrl() throws Exception {
		String currentURL = driver.getCurrentUrl();
		return UrlUtils.getProductIDFromURL(currentURL);
	}
	
	/**
	 * To get recently viewed by given product ID
	 * @param prodID - Needed Product ID 
	 * @return - <a> href URL Tag of the recently viewed product
	 * @throws Exception - Exception
	 */
	public String getATagFromRecentlyViewedByProductID(String prodID) throws Exception{
		if(Utils.waitForElement(driver, recentlyViewedSection)) {
			WebElement suggestionElem = recentlyViewedSection.findElement(By.xpath("//a[contains(@href,'"+prodID+"')]"));
			return BrowserActions.getTextFromAttribute(driver, suggestionElem, "href", "A Tag of Search suggestion").trim();
		}
		return null;
	}
	
	/**
	 * To get maximum selectable quantity of product variation
	 * @return Selectable quantity of current variation
	 * @throws Exception
	 */
	public int getVariationAvailableQuantity() throws Exception {
		if (!Utils.waitForElement(driver, inventoryState)) {
			Log.event("Product variation not selected. Selecting variation...");
			selectAllSwatches();
		}
		String qtyMessage = BrowserActions.getText(driver, txtProdAvailStatus, "variation availabiliy message");
		int qtyAvailable;
		if(qtyMessage.contains("Out")) {
			return 0;
		} else if (qtyMessage.contains("Only")) {
			qtyAvailable = com.fbb.support.StringUtils.getNumberInString(qtyMessage);
		} else {
			qtyAvailable = lstQtySelectable.size()+1;
		}
		
		Log.event("Available quantity for selected variation:: " + qtyAvailable);
		return qtyAvailable;
	}
	
	/**
	 * To select product variation that has a given minimum available quantity
	 * @param minQty - minimum needed available quantity for selected variation
	 * @throws Exception
	 */
	public void selectMinimumQuantityVariation(int minQty) throws Exception {
		int sizeCount = lstSizeSwatches.size() > 0 ? lstSizeSwatches.size()
				: (lstShoeSizeSwatches.size() > 0 ? lstShoeSizeSwatches.size() : lstBraCupSizeSwatches.size());
		int maxRetryCount = lstColorSelectable.size() > sizeCount ? lstColorSelectable.size() : sizeCount;
		Log.event("Will try maximum " + maxRetryCount + " times to get variation with minimum quantity of " + minQty);
		for (int i = 0; i < maxRetryCount; i++) {
			BrowserActions.refreshPage(driver);
			selectAllSwatches();
			if(getVariationAvailableQuantity() >= minQty)
				return;
		}
	}

	/**
	 * To click on Add to bag button in the PDP.
	 * @throws Exception
	 */
	public void clickAddToBagForErrorMsg() throws Exception {
		String productType = getProductType();
		WebElement elem = null;
		Log.event("Product type:: " + productType);
		
		if (productType.equals("standardproductset")) {
			elem = btnAddAllToBag;
		} else if (productType.equals("specialproductset")) {
			elem = btnAddToBagPrdSet;
		} else {
			elem = btnAddToBag;
		}
        
		BrowserActions.scrollToViewElement(elem, driver);
		Utils.waitForElement(driver, elem);
		BrowserActions.clickOnElementX(elem, driver, "Add To Cart Button");
		Utils.waitForElement(driver, lblMaxCartError);
	}

	/**
	 * To verify the given size is existed in the pdp page
	 * @return boolean - true if present, else false
	 * @throws Exception - Exception
	 */
	public boolean isSizeExist(String size) throws Exception {
		try {
			WebElement sizeElem = driver.findElement(By.cssSelector("a[aria-label*='" + size.toUpperCase() + "']"));
			if(Utils.waitForElement(driver, sizeElem)) {
				return true;
			}
		} catch (NoSuchElementException e) {
			try {
				WebElement sizeElem = driver.findElement(By.cssSelector("a[aria-label*='" + size + "']"));
				if(Utils.waitForElement(driver, sizeElem)) {
					return true;
				}
			} catch (NoSuchElementException e2) {
				return false;
			}
			return false;
		}
		return false;
	}

	/**
	 * To verify the given color is existed in the pdp page
	 * @return boolean - true if present, else false
	 * @throws Exception - Exception
	 */
	public boolean isColorExist(String color) throws Exception {
		try {
			WebElement colorElem = driver.findElement(By.cssSelector("a[aria-label*='" + color.toUpperCase() + "']"));
			if(Utils.waitForElement(driver, colorElem)) {
				return true;
			}
		} catch (NoSuchElementException e) {
			try {
				WebElement colorElem = driver.findElement(By.cssSelector("a[aria-label*='" + color + "']"));
				if(Utils.waitForElement(driver, colorElem)) {
					return true;
				}
			} catch (NoSuchElementException e2) {
				return false;
			}
		}
		return false;
	}
	
	public boolean isFamilyExist(String family) throws Exception {
		try {
			WebElement familyElem = driver.findElement(By.cssSelector("a[aria-label*='" + family + "']"));
			if(Utils.waitForElement(driver, familyElem)) {
				return true;
			}
		} catch (NoSuchElementException e) {
			try {
				WebElement familyElem = driver.findElement(By.cssSelector("a[aria-label*='" + family.toUpperCase() + "']"));
				if(Utils.waitForElement(driver, familyElem)) {
					return true;
				}
			} catch (NoSuchElementException e2) {
				return false;
			}
		}
		return false;
	}
	
	/**
	 * To verify the given size, color, and badge is existed in the pdp page
	 * @return boolean - true if present, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyBadgeAndSwatchesDisplay(String Product) throws Exception {
		if (Product.split("\\|").length == 2) {
			return verifyBadgeType(Product.split("\\|")[1]);
		} else if (Product.split("\\|").length == 3) {
			return isColorExist(Product.split("\\|")[1]) && isSizeExist(Product.split("\\|")[2]);
		} else if (Product.split("\\|").length == 4) {
			return isColorExist(Product.split("\\|")[1]) && isSizeExist(Product.split("\\|")[2]) && isFamilyExist(Product.split("\\|")[3]);
		}
		return false;
	}
}
