package com.fbb.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.CollectionUtils;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;

public class PlpPage extends LoadableComponent<PlpPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Refinements refinements;
	String runBrowser;

	/**********************************************************************************************
	 ********************************* WebElements of PLP Page ****************************
	 **********************************************************************************************/

	private static final String SIZE = "div.refinementSize";
	private static final String BRAND = "div[class='refinement brand ']";
	private static final String LNK_VIEW_MORE_LESS = ".view-more.hide-desktop";
	private static final String COLOR = "div.refinementColor";
	private static final String MONETATE_BANNER = "div#monetate_lightbox_content_container ";


	// --------------- Header Section ---------------------- //


	//---------WebElements Mobile------------//

	@FindBy(css = LNK_VIEW_MORE_LESS)
	WebElement divViewMore;
	
	@FindBy(css = LNK_VIEW_MORE_LESS + " > a")
	List<WebElement> btnViewMore;

	@FindBy(css = LNK_VIEW_MORE_LESS + " > a")
	WebElement btnViewMoreButton;

	@FindBy(css = ".result-count.hide-desktop>span:nth-child(1)")
	WebElement lblResultCountMobile;
	
	@FindBy(css = ".result-count.result-count-mobile.hide-desktop")
	WebElement lblResultCountTablet;
	
	@FindBy(css = ".searchresult-hide-mobile")
	WebElement lblResultCountTablet1;
	//---------------------------------------//

	//----------WebElements Desktop----------//
	@FindBy(css = ".js.wf-lato-n1-active")
	WebElement fullPlpPage;
	
	@FindBy(css = "div[class='result-count']>span:nth-child(1)")
	WebElement lblResultCountDesktop;

	@FindBy(css = "div.hide-tablet.hide-mobile div.selected-option.selected")
	WebElement sortbydropdown;
	
	@FindBy(css = "div.hide-desktop div.selected-option.selected")
	WebElement sortbydropdown_Mob_Tab;

	@FindBy(css = ".current_item .selection-list li")
	List<WebElement> listSortbyOptions;

	@FindBy(css = ".hide-mobile .sort-by .selection-list")
	WebElement sortBySelectionListDesktop;

	@FindBy(css = "div[class$='current_item']")
	static WebElement sortbyForm;

	@FindBy(css = ".current_item .selection-list [label='recommended']")
	static WebElement sortbyRecommended;

	@FindBy(css = ".current_item .selection-list [label='lowest priced']")
	static WebElement sortbyLowestPriced;

	@FindBy(css = ".current_item .selection-list [label='highest priced']")
	static WebElement sortbyHighestPriced;

	@FindBy(xpath = "//div[contains(@class,'current_item')]//li[contains(text(),'Top Rated')]")
	static WebElement sortbyTopRated;

	@FindBy(xpath = "//div[contains(@class,'current_item')]//li[contains(text(),'Best Matches')]")
	static WebElement sortbyBestMatches;

	@FindBy(css = ".sort-by.custom-select.current_item")
	static WebElement currentSortOption;

	@FindBy(css = ".hide-mobile .sort-by.custom-select .selected-option.selected")
	static WebElement selectedSortOption;

	@FindBy(css = "div[class='search-result-options hide-tablet hide-mobile']")
	static WebElement searchResultHeader;
	
	@FindBy(css = ".category-search .simpleSearch")
	static WebElement secondarySearchbox;
	
	@FindBy(css = "#quickviewbutton")
	List<WebElement> lstQuickshopButtons;
	
	@FindBy(css = ".product-swatches .swatch-list")
	List<WebElement> lstProductSwatchList;
	
	@FindBy(css = ".swatches-six .product-swatches-all")
	WebElement colorSwatchShowMoreLess;
	
	@FindBy(css = ".swatches-six .product-swatches-all")
	List<WebElement> lstcolorSwatchShowMoreLess;
	
	@FindBy(css = ".swatches-four .product-swatches-all")
	WebElement colorSwatchShowMore;
	
	@FindBy(css = ".swatches-four .product-swatches-all")
	List<WebElement> lstColorSwatchShowMore;
	
	@FindBy(css = ".swatches-four .product-swatches-all-showless")
	WebElement colorSwatchShowLess;
	
	@FindBy(css = ".swatches-four .product-swatches-all-showless")
	List<WebElement> lstColorSwatchShowLess;
	
	@FindBy(css = ".product-swatches.show-color ")
	WebElement colorSwatchShownMore;
	
	@FindBy(css = ".hide-mobile .sort-by.custom-select .selected-option.selected")
	WebElement defaultSortOption;

	@FindBy(css = ".toggle")
	List<WebElement> listtoggle;

	@FindBy(css = "#main")
	WebElement divmain;
	
	@FindBy(css = "//ul[contains(@class,'swatch-list')]//ancestor::div[contains(@class,'product-tile')]")
	List<WebElement> productWithColorSwatch;

	@FindBy(css = "#search-result-items")
	WebElement ProductTiles;
	
	@FindBy(css = ".product-swatches")
	WebElement ProductSwatches;
	
	@FindBy(css = "div[id='main']")
	WebElement mainContainer;

	@FindBy(css = ".category-banner-text .category")
	WebElement categoryBannerText;
	
	@FindBy(css = ".content-banner-breadcrumb .content-slot")
	WebElement categorySlotBanner;
	
	@FindBy(css = ".hide-desktop .sort-by.custom-select")
	WebElement drpSortByCollapsedMobile;

	@FindBy(css = ".sort-by.custom-select.current_item .selection-list")
	WebElement sortListDropDownExpand;

	//---------------------------------------//
	
	@FindBy(css = ".primary-content")
	WebElement divProductTileContainer;
	
	
	@FindBy(css = ".product-swatches img[src*='images/swatch']")
	List<WebElement> imgColorSwatchWithImage;

	@FindBy(css = ".breadcrumb-element.last-element")
	WebElement bcLastCategoryName;

	@FindBy(css = ".refinement-link.bold>span")
	WebElement toogleActiveForPrice;

	@FindBy(css = "div[id='f684e1823420731b4c27d51fa0']>div[class='promotional-message']>a")
	WebElement promoMessageShirt;
	
	@FindBy(css = ".product-promo .promotional-message")
	WebElement txtProdPromoMessage;
	
	@FindBy(css = ".clearance.promotional-message")
	WebElement txtClearencePromoMessage;
	
	@FindBy(css = ".product-feature-messages")
	WebElement txtProdFeatureMsg;
	
	@FindBy(css = ".product-special-messages")
	WebElement txtSpecialProductMsg;
	
	@FindBy(css = ".hideswatch .swatch")
	WebElement lnkSwatchColor;
	
	@FindBy(css = ".dot-page")
	WebElement dotPages;
	
	@FindBy(css = ".price-product-standard-price")
	WebElement txtProductStdPrice;
	
	@FindBy(xpath = "//span[@class='price-product-standard-price' and contains(text(),' - ')]")
	List<WebElement> lstProductStdPriceRange;
	
	@FindBy(css = ".price-product-sales-price")
	WebElement txtProductSalesPrice;
	
	@FindBy(css = "div.product-tile:not(.product-set) .price-product-sales-price")
	List<WebElement> lstProductSalesPrice;
	
	@FindBy(xpath = "//span[@class='price-product-sales-price' and contains(text(),' - ')]")
	List<WebElement> lstProductSalesPriceRange;
	
	@FindBy(xpath = "//span[@class='price-product-sales-price' and contains(text(),' - ')]")
	WebElement txtProductSalesPriceRange;
	
	@FindBy(xpath = "//span[@class='price-product-standard-price' and contains(text(),' - ')]//ancestor::*[contains(@class, 'product-tile')]//div[contains(@class,'product-name ')]")
	WebElement lnkProductstandardPriceRangeName;
	
	@FindBy(xpath = "//span[contains(text(),' - $')]")
	WebElement txtPriceRange;
	
	@FindBy(css = "div.product-tile:not(.product-set) .price-product-sales-price.product-actual-price ")
	WebElement txtProductActualSalesPrice;
	
	@FindBy(css = "div.product-tile:not(.product-set) .price-product-sales-price.product-actual-price ")
	List<WebElement> lstProductActualSalesPrice;
	
	@FindBy(css = ".swatch-list img")
	WebElement txtSwatchList;

	@FindBy(css = ".breadcrumb-refinement")
	WebElement lblBreadcrumbRefinement;
	
	@FindBy(css = "a[title='Go to Home'].hide-mobile")
	WebElement lblHomeBreadCrumb_Des_Tab;
	
	@FindBy(css = "a[title='Go to Home'].hide-desktop.hide-tablet")
	WebElement lblHomeBreadCrumb_Mob;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
	WebElement lblCurBreadCrumb_Des_Tab;

	@FindBy(css = "div[class='promotional-message']")
	List<WebElement> lstPromoProductMsg;

	@FindBy(css = "div[class='backtorefine']")
	WebElement btnBackarrowLeftNav;

	@FindBy(css = ".pt_product-search-result.product-list-page")
	WebElement readyElement;
	
	@FindBy(css = "label[for='q'] span[class='hide-mobile']")
	WebElement lblSearchText;

	@FindBy(css = "label[for='q'] span[class='hide-desktop']")
	WebElement lblSearchTextMobile;

	@FindBy(css = "h3.refinement-header+div.category-refinement>ul>li>h4>a")
	public List<WebElement> lstSubCategoryRefinement;

	@FindBy(css = ".refinement-header.toggle.expanded")
	public List<WebElement> subCategoryHeader;
	
	@FindBy(css = "#category-level-2 .active  ")
	WebElement categoryLvl2;
	
	@FindBy(css = "#category-level-2 :not([class*='cr-section']) .refinement-link")
	List<WebElement> subCategories;
	
	@FindBy(css = "#category-level-2 :not([class*='cr-section']) .refinement-link")
	WebElement subCategory;
	
	@FindBy(css = "#category-level-3 :not([class*='cr-section']) .refinement-link")
	List<WebElement> lvl3subCategories;
	
	@FindBy(css = "#category-level-3 .expandable.non-active .cr-section .refinement-link")
	WebElement categoryNemeNxtByPlus;
	
	@FindBy(css = "#category-level-3 .cr-icon ")
	WebElement CategoryPlusSymbol;
	
	@FindBy(css = "#category-level-4 .refinement-link")
	List<WebElement> lvl4ChildCategories;	

	@FindBy(css = "div.product-swatches>ul>li")
	List<WebElement> listColor;
	
	@FindBy(css = "div.product-swatches>ul>li")
	WebElement SwatchColor;
	
	@FindBy(css = "div.product-swatches>ul>li:not([class*='hide'])>a:not([class*='un'])")
	List<WebElement> lstColor;
	
	@FindBy(css = "li:not(.two-space-tile):not(.one-space-tile) .product-tile:not([class*='set'])")
	List<WebElement> productTile;
	
	@FindBy(css = "li:not(.two-space-tile):not(.one-space-tile) .product-tile.product-set")
	List<WebElement> productTileSet;
	
	@FindBy(css = "li:not(.two-space-tile):not(.one-space-tile) .product-tile:not([class*='set']) a.name-link")
	List<WebElement> productTileLink;

	@FindBy(css = ".product-tile .name-link")
	List<WebElement> lstProductName;
	
	@FindBy(css = ".product-tile .name-link .product-name")
	WebElement txtProductName;

	@FindBy(css = "a.page-next")
	WebElement lnkNextPage;
	// --------------- Breadcrumb Section ---------------------- //

	@FindBy(className = "breadcrumb-refinement-value")
	List<WebElement> txtBreadCrumbValue;

	@FindBy(css = "span[class='breadcrumb-element breadcrumb-result-text']")
	WebElement txtSearchResultInBreadcrumb;

	@FindBy(css = ".breadcrumb .breadcrumb-element.hide-mobile ")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;

	@FindBy(css = ".content-banner-breadcrumb .breadcrumb")
	WebElement divBreadcrumb;

	@FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
	WebElement divCurrentBreadCrumbDesktopTablet;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop")
	WebElement divCurrentBreadCrumbMobile;
	
	@FindBy(css = ".category-banner-text .category")
	WebElement lblCategoryNameMobile;

	// --------------- Left Navigation Section ----------------- //

	@FindBy(css = "div[class='refineby-attribute']")
	WebElement sectionFilterBy;

	@FindBy(css = ".device-clear-all")
	WebElement lnkClearAll;

	// ----------- Refinement - Refinement Size ------------ //

	@FindBy(css = SIZE)
	WebElement btnsize;

	@FindBy(css = SIZE + " ul[style*='none']")
	public WebElement btnSizeExpand;

	@FindBy(css = SIZE + " i[class='toggle-plus-minus']")
	WebElement btnSizeCollapse;

	@FindBy(css = SIZE + " ul li a")
	List<WebElement> listSelectSize;

	@FindBy(css = SIZE + " ul li a i[class='toggle-check toggle-check-active']")
	List<WebElement> lstSelectedFilter;

	// ----------- Refinement ------------ //

	@FindBy(css = BRAND)
	WebElement lblBrand;
	
	@FindBy(css = ".refinement.category-refinement")
	WebElement categoryRefinement;

	@FindBy(css = BRAND + " ul li ")
	List<WebElement> lstBrand;

	@FindBy(css = BRAND + " ul li ul li a")
	List<WebElement> lstSubBrand;

	@FindBy(css = ".refinement")
	List<WebElement> lstRefinementFilterOptions;

	@FindBy(css = ".refinement h3")
	List<WebElement> lstRefinementFilterOptionsInMobile;

	@FindBy(css = ".refinement-heading-category .toggle-heading")
	List<WebElement> lstRefinementFilterOptionsTitle;

	@FindBy(css = BRAND + " span[class='viewmore-refinement']")
	WebElement btnBrandViewMore;

	@FindBy(css = ".refinement-heading-category.expanded.toggle")
	WebElement drpExpandedRefinement;

	@FindBy(css = "div[class='expended'][style*='block'] li")
	List<WebElement> lstExpandedRefinementAllSubList;

	@FindBy(css = ".refinement.active div[class='expended']")
	WebElement divExpandedRefinementSubList;
	
	@FindBy(css = ".refinements.ws-sticky")
	WebElement divRefinement;
	
	@FindBy(css = "ul.menu-category.level-1")
	WebElement categorySticky;
	
	@FindBy(css = "#navigation")
	WebElement stickyGlobalNavigation;
	
	@FindBy(css = ".shopping-bag-sm")
	WebElement minicartIcon; 
    
	@FindBy(css = ".back-to-top")
	WebElement backToTop;
	
	@FindBy(css = "div[class='expended'][style*='block'] li > div.available")
	List<WebElement> lstExpandedRefinementAvailableSubList;

	@FindBy(css = ".refinement i.fa.fa-angle-down.fa-2x")
	List<WebElement> lstRefinementDownArrow;

	@FindBy(css = ".search-result-options.hide-tablet.hide-mobile .result-count")
	List<WebElement> lblRefinementCountDesktop;

	// -------------- Result product section ------------------ //

	@FindBy(css = "#results-products")
	WebElement txtProductFound;

	// ---------------- Search Result Filter Section ------------- //

	@FindBy(css = "div[class='search-result-options']")
	WebElement sectionSearchResult;

	@FindBy(css = ".sort-by .custom-select")
	WebElement drpSortBy;

	@FindBy(css = ".sort-by .selection-list li")
	List<WebElement> lstSortByOptions;

	@FindBy(css = "i[data-option='column']")
	WebElement toggleGrid;

	@FindBy(css = "div[class='search-result-options'] .items-per-page .custom-select")
	WebElement drpItemsPerPage;

	@FindBy(css = "div[class='custom-select current_item'] select option")
	List<WebElement> optionsInItemsPerPge;

	@FindBy(css = "div[class='search-result-options last'] .items-per-page .custom-select")
	WebElement drpItemsPerPageBottom;

	@FindBy(css = "div[class='search-result-options'] .items-per-page .selection-list li:not([class*='selected'])")
	List<WebElement> lstItemsPerPageOptions;

	@FindBy(css = "div[class='search-result-options last'] .items-per-page .selection-list li:not([class*='selected'])")
	List<WebElement> lstItemsPerPageOptionsBottom;

	// ---------------- Result Hit Section ------------- //

	@FindBy(css = ".results-hits")
	WebElement txtResultHits;

	// --------------- Pagination Section ------------- //

	@FindBy(css = ".pagination li")
	List<WebElement> lstPageNos;
	
	@FindBy(css = ".pagination li a[class*='page']:not([class*='next'])")
	List<WebElement> lstPages;
	
	@FindBy(css = ".pagination li.current-page")
	WebElement txtCurrentPageNo;

	@FindBy(css = ".page-next")
	WebElement iconNextPage;

	@FindBy(css = ".page-last i")
	WebElement iconLastPage;

	@FindBy(css = ".page-previous")
	WebElement iconPreviousPage;

	@FindBy(css = ".page-first i")
	WebElement iconFirstPage;
	
	@FindBy(css = "a.page-1")
	WebElement firstPage;
		
	@FindBy(css = ".breadcrumb-element.last-element")
	WebElement Style;

	// -------------- Product List Section ----------- //

	@FindBy(id = "search-result-items")
	WebElement sectionSearchResults;

	@FindBy(css = "ul[id='search-result-items']>li")
	List<WebElement> lstProducts;

	@FindBy(css = ".product-swatches")
	List<WebElement> lstProductsWithColorSwatch;

	@FindBy(className = "name-link")
	List<WebElement> lstProductNames;

	@FindBy(css = ".thumb-link>picture>img")
	List<WebElement> lstProductImages;
	
	@FindBy(css = ".product-image .thumb-link img[class='p-image']")
	List<WebElement> lstPrdImages;
	
	@FindBy(css = ".product-image img")
	WebElement txtProductImages;
	
	@FindBy(css = ".product-image ")
	WebElement productImages;
	
	@FindBy(css = ".product-image .thumb-link .p-image-rollover")
	WebElement lstbackviewImg;
	
	@FindBy(css = ".product-image .thumb-link .p-image-rollover")
	WebElement backviewImg;
	
	@FindBy(css = ".product-swatches.mobile-top-buffer")
	WebElement lnkMoreColors_Mobile;
	
	@FindBy(css = ".product-swatches.mobile-top-buffer")
	List<WebElement> lstMoreColors_Mobile;
	
	@FindBy(css = ".product-swatches.desktop-top-buffer.tablet-top-buffer.mobile-top-buffer")
	WebElement lnkMoreColors_Desk_Tab;
	
	@FindBy(css = ".product-swatches.desktop-top-buffer.tablet-top-buffer.mobile-top-buffer")
	List<WebElement> lstMoreColors_Desk_Tab;

	@FindBy(id = "quickviewbutton")
	WebElement btnQuickView;

	// ---------- Spinner ------------ //

	@FindBy(css = ".loader[style*='block']")
	WebElement plpSpinner;

	@FindBy(css = ".filterby-refinement.hide-desktop>a")
	WebElement btnFilterByMobile;

	@FindBy(css = "#category-level-1 li a")
	List<WebElement> lstCatagory;

	@FindBy(css = "div[id='secondary'] div[class='close_menu hide-desktop'] i")
	WebElement iconCloseMenu;

	@FindBy(css = "div[id='secondary'] div[class*='refinement refinementSize'] span")
	WebElement txtSize;

	@FindBy(css = "div.size>h3>i")
	WebElement btnSizetoggle;

	@FindBy(css = ".selected>a>span:nth-child(2)")
	WebElement getBrandName;

	@FindBy(css = ".refinement-header")
	List<WebElement> lstCategoryRefinement;

	@FindBy(css = ".refinement-header")
	WebElement textCatagoryrefinement;

	@FindBy(css = ".breadcrum-device.hide-desktop>a")
	WebElement lnkBreadcrumbInmobile;

	@FindBy(css = "div[class='search-result-options last']>div[class='backtotop']>a")
	WebElement txtBacktoTop;

	@FindBy(css = "div[class*='last']>span i[data-option='column']")
	WebElement toggleListBottom;

	@FindBy(css = "div[class*='last']>span i[data-option='column']")
	WebElement toggleGridBottom;

	@FindBy(css = "ul[id='search-result-items']>li")
	public List<WebElement> totalProducts;

	@FindBy(css = ".name-link")
	List<WebElement> lstBrandName;

	@FindBy(css = "div[id='wrapper']>div[id='main']>div[class='refinements']")
	WebElement leftNavigationMobile;	

	@FindBy(css = COLOR)
	WebElement btnColor;

	@FindBy(css = COLOR + " i[class='toggle-plus-minus']")
	WebElement btnColorCollapse;

	@FindBy(css = "div.refinementColor> ul li a>i")
	List<WebElement> listSelectColor;

	@FindBy(css = COLOR + " ul li a i[class='toggle-check toggle-check-active']")
	List<WebElement> lstSelectedFilterColor;

	@FindBy(css = "div.refinementColor> ul>.unselectable.hide")
	WebElement UnSelectable;

	@FindBy(css = ".refinementColor >ul>.view-more-less>.viewmore-refinement")
	WebElement viewMoreColors;

	@FindBy(css = ".product-name>a[title='Go to Product: Petite Juliana Rail Straight Jeans']")
	WebElement productBrand1;

	@FindBy(css = ".product-name>a[title='Go to Product: Petite Size Colored Denim Jeans']")
	WebElement productbrand2;

	@FindBy(css = "#secondary > div.refinement.brand > ul > li:nth-child(3)")
	WebElement Brand2;

	@FindBy(css = "#secondary > div.refinement.brand > ul > li:nth-child(4) > ul > li > a > i")
	WebElement Brand2Product;

	@FindBy(css = BRAND + " i[class='toggle-plus-minus']")
	WebElement btnBrandCollapse;

	@FindBy(css = "#secondary > div.refinement.brand > h3 > i")
	WebElement brandToggleMobile;

	@FindBy(css = ".page-3")
	WebElement page3;

	@FindBy(css = ".toggle-grid.hide-mobile >i[data-option='wide']")
	WebElement listView;

	@FindBy(css = SIZE + " ul li a i[class='toggle-check toggle-check-active']")
	WebElement FirstSelectedFilter;

	@FindBy(css = "div[id='secondary']")
	WebElement leftNavPanel;

	@FindBy(css = ".toggle-grid>i")
	WebElement fldToggleView;

	@FindBy(css = "#secondary .refinement,div[class='filterby-refinement hide-desktop']")
	List<WebElement> lstLeftNavRefinement;

	@FindBy(css = "a.thumb-link>picture>img")
	List<WebElement> lstImgPrimary;

	@FindBy(css = ".breadcrumb-refinement-value>span")
	WebElement filteredValue;

	@FindBy(css = "div.breadcrumb.hide-mobile > span.breadcrumb-refinement > span:nth-child(3) > span")
	WebElement filteredSecondValue;

	@FindBy(css = "#category-level-1>li:nth-child(1)>h4>a")
	WebElement pajamaSets;

	@FindBy(css = "#category-level-1>li:nth-child(1)>h4>a>i:nth-child(1)")
	WebElement arrowInPajamaSets;

	@FindBy(css = "#category-level-1>li[class='expandable toggle-down']")
	WebElement expandanbleToggle;

	@FindBy(css = ".refinement-header.toggle ")
	List<WebElement> lstCategoryRefinementToggle;

	@FindBy(css = "div ul li a i[class='toggle-check toggle-check-active']")
	List<WebElement> chkRefinMentActive;

	@FindBy(css = "div[id='secondary'] div[class='close_menu hide-desktop'] i")
	WebElement IconCloseMenu;

	@FindBy(css = ".loader[style*='block']")
	WebElement searchResultsSpinner;

	@FindBy(css = ".loader[style*='block']")
	WebElement Spinner;

	@FindBy(css = "#main")
	WebElement pageSource;

	@FindBy(css = ".grid-clm>p>a>img")
	WebElement ImgBanner;

	@FindBy(css = "#secondary > div.refinement.brand > ul > li:nth-child(1)")
	WebElement Brand1;

	@FindBy(css = "#secondary > div.refinement.brand > ul > li:nth-child(2) > ul > li:nth-child(1) > a > i")
	WebElement Brand1Product;

	@FindBy(css = ".toggle-grid.hide-mobile >i[data-option='wide']")
	WebElement toggleList;

	@FindBy(css = "span.toggle-grid:nth-child(6) > i[data-option='column']")
	WebElement girdView;

	@FindBy(css = ".shipping-promotion")
	WebElement txtFreeShipping;

	@FindBy(css = "div[id='0045a0589b561f73b470378f9e'] div[class='product-promo']")
	WebElement promoMessage;

	@FindBy(css = ".toggle-grid")
	WebElement toggleViewForMobile;

	@FindBy(css = ".toggle-grid.hide-mobile")
	WebElement toggleViewForDesktop;
	
	@FindBy(css = "div[class='b_product_badge'] img")
	WebElement imgProductBadge;

	@FindBy(css = ".product-swatches-all.product-swatches-all-desktop.product-swatches-all-mobile")
	List<WebElement> lnkViewAllcolors;

	@FindBy(css = ".swatch-list")
	List<WebElement> swatchRowColors;

	@FindBy(css = ".product-standard-price")
	List<WebElement> lstOriginalPrice_Standard_Price;

	@FindBy(css = ".product-sales-price")
	List<WebElement> lstNowPrice_Sale_Price;
	
	@FindBy(css = ".product-pricing")
	List<WebElement> txtProdPrice;
	
	@FindBy(css = ".product-pricing")
	WebElement txtProdPrice1;
	
	@FindBy(css = ".product-pricing")
	WebElement txtSingleProdPrice;

	@FindBy(css = ".search-result-options .filterby-refinement.hide-desktop .toggle-grid>i[data-option='column']")
	WebElement gridViewMobile;

	@FindBy(css = ".search-result-options .filterby-refinement.hide-desktop .toggle-grid>i[data-option='wide']")
	WebElement listViewMobile;

	@FindBy(css = ".current-page")
	WebElement lblCurrentPage;

	@FindBy(css = "div[class='product-badge']")
	List<WebElement> txtProductBadge;
	
	@FindBy(css = "#category-level-3 .cr-icon.active")
	WebElement lvl3CategActive;

	@FindBy(css = ".brand>ul>li")
	List<WebElement> btnBrandAlphabetToggle;

	@FindBy(css = ".refinement.brand>ul>li>ul>li>a>.toggle-check")
	List<WebElement> chkBrandAlphabet;

	// --------------- Pagination Section ------------- //
	@FindBy(css = ".toggle-grid.hide-mobile+div>ul>li.first-last>a[class='page-next']>i")
	WebElement iconNextPageHeader;

	@FindBy(css = ".toggle-grid.hide-mobile+div>ul>li.first-last>a[class='page-last']>i")
	WebElement iconLastPageHeader;

	@FindBy(css = ".toggle-grid.hide-mobile+div>ul>li.first-last>a[class='page-previous']>i")
	WebElement iconPreviousPageHeader;

	@FindBy(css = ".toggle-grid.hide-mobile+div>ul>li.first-last>a[class='page-first']>i")
	WebElement iconFirstPageHeader;

	@FindBy(css = ".category-seo-text")
	WebElement divCategorySEOtext;

	@FindBy(css = "div.content-slot.slot-grid-header")
	WebElement divContentSlotHeader;

	@FindBy(css = ".refinements.ws-sticky")
	WebElement divHorizontalRefinement;

	@FindBy(css = ".search-result-options.hide-tablet.hide-mobile .sort-by .selected-option.selected")
	WebElement drpSortBySelectedDesktop;
	
	@FindBy(css = ".hide-desktop .sort-by .selected-option.selected")
	WebElement drpSortBySelectedMobTab;

	@FindBy(css = ".search-result-options.hide-tablet.hide-mobile .result-count")
	WebElement txtResultCount;

	@FindBy(id = "category-level-1")
	WebElement divVerticalRefinement;

	@FindBy(css = ".product-promo-tile")
	WebElement divCategoryBody1;

	@FindBy(css = ".offer-promo-tile")
	WebElement divCategoryBody2;

	@FindBy(css = ".onespace-tile-content")
	WebElement divCategoryBody3;

	@FindBy(css = ".footer-container .back-to-top")
	WebElement btnBackToTop;

	@FindBy(css = "#search-result-items>li.grid-tile ")
	List<WebElement> lstProductTile;
	
	@FindBy(css = ".pagination ul")
	WebElement paginationSection;

	@FindBy(css = "a.page-next")
	WebElement btnPaginationNext;
	
	@FindBy(css = "a.page-previous")
	WebElement btnPaginationPrev;
	
	@FindBy(css = ".loader-bg")
	WebElement waitLoader;
	
	@FindBy(css = ".footer-container.footer-top")
	WebElement divFooterTop;
	
	@FindBy(css = ".search-result-options .sort-by .selection-list li")
	List<WebElement> sortByOptions;
	
	@FindBy(css = MONETATE_BANNER)
	WebElement divMonetateBanner;
	
	@FindBy(css = "a.name-link[href*='PS_15_']")
	List<WebElement> lstProductSet;

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public PlpPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
		try {
			GlobalNavigation.closePopup(driver);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("PLP Page did not open up. Site might be down.", driver);
		}

		elementLayer = new ElementLayer(driver);
		refinements = new Refinements(driver).get();
		runBrowser  = Utils.getRunBrowser(driver);
	}

	// --------------- Header Section ---------------------- //


	/**
	 * To Scroll into ViewMore button
	 * @throws Exception - Exception
	 */
	public void scrollToViewMore()throws Exception{
		BrowserActions.scrollToViewElement(btnViewMore.get(btnViewMore.size()-1), driver);
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the count of product tiles present in this page
	 * @return list of product size
	 * @throws Exception - Exception
	 */
	public int getProductTileCount()throws Exception{
		int tileCount = lstProducts.size();
		Log.event("Tile count in PLP:: " + tileCount);
		return tileCount;
	}
	
	/**
	 * To get the count of product tiles present in this page
	 * @return list of product size
	 * @throws Exception - Exception
	 */
	public int getSwatchConfiguredTileCount()throws Exception{
		int swatchConfiguredTileCount = lstProductsWithColorSwatch.size();
		Log.event("Swatch configured tile count in PLP:: " + swatchConfiguredTileCount);
		return swatchConfiguredTileCount;
	}
	
	/**
	 * To get list of product id
	 * @param noOfIdNeeded - No of product needed
	 * @return String[] - Product id array
	 * @throws Exception - Exception
	 */
	public String[] getListOfProductId(int noOfIdNeeded)throws Exception{
		HashSet<String> ProductId = new HashSet<String> ();
		int i=0;
		while (ProductId.size() != noOfIdNeeded) {
			WebElement prdId = productTile.get(i);
			ProductId.add(BrowserActions.getTextFromAttribute(driver, prdId, "data-itemid", "product"));
			i++;
		}
		return ProductId.toArray(new String[0]);
	}
	
	/**
	 * To get product id by index
	 * @param noOfIdNeeded - No of product needed
	 * @return String[] - Product id array
	 * @throws Exception - Exception
	 */
	public String getProductIdByIndex(int index)throws Exception{
		WebElement prdId = productTile.get(index-1);
		return BrowserActions.getTextFromAttribute(driver, prdId, "data-itemid", "product");
	}
	
	/**
	 * To get product id by index
	 * @param noOfIdNeeded - No of product needed
	 * @return String[] - Product id array
	 * @throws Exception - Exception
	 */
	public String getOriginalPriceProductIdByIndex(int index)throws Exception{
		WebElement prd = lstProductActualSalesPrice.get(index-1);		
		WebElement prdTile = prd.findElement(By.xpath("ancestor::*[contains(@class, 'product-tile')]"));
		return BrowserActions.getTextFromAttribute(driver, prdTile, "data-itemid", "product");
	}
	
	/**
	 * To get product id by index
	 * @param noOfIdNeeded - No of product needed
	 * @return String[] - Product id array
	 * @throws Exception - Exception
	 */
	public String getOriginalPriceProductNameByIndex(int index)throws Exception{
		WebElement prd = lstProductActualSalesPrice.get(index-1);		
		WebElement prdTile = prd.findElement(By.xpath("ancestor::*[contains(@class, 'product-tile')] //div[contains(@class,'product-name ')]"));
		return BrowserActions.getText(driver, prdTile, "Product");
	}
	
	/**
	 * To mouse hover on given leve1 category
	 * @param String - level1
	 * @Throws Exception - Exception
	 * @return WebElement - level1
	 */
	public void mouseHoverOnShowColors() throws Exception{
		if (Utils.waitForElement(driver, lnkMoreColors_Desk_Tab)) {
			WebElement elem = lstMoreColors_Desk_Tab.get(0).findElement(By.cssSelector(" .swatches-six>a.product-swatches-all"));
			BrowserActions.mouseHover(driver, elem, "Clicked view more color");
		} else {
			BrowserActions.mouseHover(driver, colorSwatchShowMoreLess, "Level-1 category ");			 
		}
		Utils.waitForElement(driver, colorSwatchShownMore);
	}
	
	/**
	 * To verify loaded products have-Product image, name & price
	 * @return true
	 * @throws Exception
	 */
	public int verifyLoadedProductsHaveImgNameAndprice() throws Exception{
		if(lstPrdImages.size()==lstProductName.size() && txtProdPrice.size() == lstProductName.size()){
			return lstPrdImages.size();
		}
		return 0;
	}
	
	/**
	 * To get product URL
	 * @param noOfLinkNeeded - No of product URLs needed
	 * @return String[] - array of product URLs
	 * @throws Exception - Exception
	 */
	public String[] getProductUrl(int noOfLinkNeeded)throws Exception{
		HashSet<String> ProductLink = new HashSet<String> ();
		int i=0;
		while (ProductLink.size() != noOfLinkNeeded) {
			WebElement prdLink = productTileLink.get(i);
			ProductLink.add(BrowserActions.getTextFromAttribute(driver, prdLink, "href", "product").split("\\#")[0]);
			i++;
		}
		return ProductLink.toArray(new String[0]);
	}
	
	/**
	 * To get the integer value of search count found for the navigated category
	 * @return Number of result from search
	 * @throws Exception - Exception
	 */
	public int getSearchResultCount()throws Exception{
		Utils.waitForPageLoad(driver);
		int resultCount = 0; 
		WebElement elem;
		if(Utils.isDesktop()) {
			elem = lblResultCountDesktop;
		}else {
			elem = lblResultCountMobile;
		}
		BrowserActions.scrollInToView(elem, driver);
		resultCount = Integer.parseInt(BrowserActions.getText(driver, elem, "count").replace(",", ""));
		
		Log.event("Result count :: " + resultCount);
		return resultCount;
	}
	
	/**
	 * To get the integer value of last page number of current category
	 * @return int
	 * @throws Exception - Exception
	 */
	public int getLastPageNumber()throws Exception{
		try {
			WebElement lastPg = lstPageNos.get(lstPageNos.size() - 2);
			return Integer.parseInt(BrowserActions.getText(driver, lastPg, "Last Page").trim());
		} catch (Exception ex) {
			return 0;
		}
	}
	
	/**
	 * To verify if page number is displayed
	 * @return Boolean
	 * @throws Exception
	 */
	public Boolean isPageNumberDisplayed(String pageNum)throws Exception{
		//Check if the specified page number is in the list of non-current page numbers
		BrowserActions.scrollInToView(paginationSection, driver);
		
		for (WebElement pageNumber : lstPageNos) {
			if(pageNumber.getText().contains(pageNum))
				return true;
		}
		//Else check if the specified page number is the current page
		if(txtCurrentPageNo.getText().trim().contains(pageNum))
			return true;
		else
			return false;
	}
	
	/**
	 * To get the integer value of last page number of current category
	 * @return int - Current page number
	 * @throws Exception - Exception
	 */
	public int getCurrentPageNumber()throws Exception{
		if(Utils.isDesktop()) {
			return StringUtils.getNumberInString(BrowserActions.getText(driver, txtCurrentPageNo, "Last Page").trim());
		}else {
			if(getProductTileCount() == getSearchResultCount()) {
				return btnViewMore.size()+1;
			}else {
				return btnViewMore.size();
			}
		}
	}
	
	/**
	 * To check current page is clickable or not
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifycurrentpageisClickable()throws Exception{
		boolean temp = false, flag = false;
		if(Utils.waitForElement(driver, txtCurrentPageNo)){
			String tag = txtCurrentPageNo.getTagName();
		    temp = txtCurrentPageNo.isEnabled();
		    BrowserActions.clickOnElementX(txtCurrentPageNo, driver, "Clicked on current Page");
		    List<String> clickableTags = Arrays.asList("input", "button", "a");
            String stringValue = txtCurrentPageNo.getAttribute("href");
            if ((clickableTags.contains(tag) && stringValue != null) && temp) {
            	flag = true;
		    }
		}
		return flag;          		
	}
	
	
	/**
	 * to verify the pagination number of the current page is highlighted in black color
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyCurrentPageNumberHighlighted()throws Exception{
		Brand brand = Utils.getCurrentBrand();
		switch(brand) {
		case rm:
		case rmx:
			return(Utils.verifyCssPropertyForElement(txtCurrentPageNo, "background-color", "51, 30, 83") ||
						Utils.verifyCssPropertyForElement(txtCurrentPageNo, "background-color", "51, 30, 84"));
		case jl:
		case jlx:
		case ww:
		case wwx:
			return Utils.verifyCssPropertyForElement(txtCurrentPageNo, "background-color", "0, 0, 0");
		case ks:
		case ksx:
		case fb:
		case fbx:
			return Utils.verifyCssPropertyForElement(txtCurrentPageNo, "background-color", "0, 0, 0");
		case bh:
		case bhx:
			return Utils.verifyCssPropertyForElement(txtCurrentPageNo, "background-color", "0, 0, 0");
		case el:
		case elx:
			return Utils.verifyCssPropertyForElement(txtCurrentPageNo, "background-color", "255, 255, 255");
		default:
			return false;
		}
	}
	
	/**
	 * to verify the pagination 2nd number onwards the  pages are highlighted in black color
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfAllPageNumberIsHighlihtedOrNot() throws Exception {
		if(Utils.isDesktop()) {
			String colorHighlight = TestData.get("highlight_color");
			int size = lstPages.size() - 1;
			for (int p = 0; p < size; p++) {
				if (!Utils.verifyCssPropertyForElement(lstPages.get(p), "background-color", colorHighlight)) {
					return false;
				}
			}
		} else {
			Log.event("Tablet and Mobile does not use page number.");
		}
		return true;
	}
	
	/**
	 * to click on the pagination number 
	 * @param pageNumberToBeClicked - Page number
	 * @throws Exception - Exception
	 */
	public void clickOnPageNumberInPagination(int pageNumberToBeClicked)throws Exception{
		String pgNoInput = Integer.toString(pageNumberToBeClicked);
		String pgNo = null;
		BrowserActions.scrollInToView(txtCurrentPageNo, driver);
		if(Utils.waitForElement(driver, txtCurrentPageNo)) {
			 pgNo = BrowserActions.getText(driver, txtCurrentPageNo, "Selected Page");
		}
		
		if (pgNo.trim().equals(pgNoInput)) {
			Log.event("The page number '"+pageNumberToBeClicked+"' is already selected");
		} else {
			WebElement pageNumber = null;
			try {
				pageNumber = driver.findElement(By.xpath("//div[@class='pagination']//a[contains(text(),'"+pageNumberToBeClicked+"')]"));
				
				BrowserActions.scrollInToView(pageNumber, driver);
				BrowserActions.clickOnElementX(pageNumber, driver, "Click On The Page Number");
				Utils.waitUntilElementDisappear(driver, waitLoader);
				Utils.waitForElement(driver, paginationSection);
			} catch (NoSuchElementException ex) {
				Log.failsoft("Given Page number "+pageNumberToBeClicked+" is not exist");
			}
		}
	}


	/**
	 * To get refinement list options
	 * @return list
	 * @throws Exception - Exception
	 */
	public List<String> getRefinementList() throws Exception {
		List<String> refinement = new ArrayList<>();

		for (int i = 0; i < lstRefinementFilterOptionsTitle.size(); i++) {
			refinement.add(lstRefinementFilterOptionsTitle.get(i).getText().trim());
		}
		return refinement;
	}

	/**
	 * To click the view more/less link
	 * @throws Exception - Exception
	 */
	public void clickViewMoreOrLessLnk() throws Exception {
		if(Utils.waitForElement(driver, btnViewMore.get(btnViewMore.size()-1))){
			Log.event("clicking on view more button");
			List<WebElement> viewMoreBtn = driver.findElements(By.cssSelector(LNK_VIEW_MORE_LESS));
			BrowserActions.javascriptClick(viewMoreBtn.get(viewMoreBtn.size()-1), driver, "click on the Tab");
			waitUntilPlpSpinnerDisappear();	
			Utils.waitForPageLoad(driver);
		}else{
			Log.failsoft("No More Products to View!");
		}

	}
	
	/**
	 * To click on View more color or show less
	 * @throws Exception 
	 */
	public void clickOnViewMoreORShowLessColorsLink(int index, String state) throws Exception {
		WebElement lnkMoreLessColors = null;
		List<WebElement> lstMoreLessColors = null;
		
		if(Utils.isMobile()) {
			lnkMoreLessColors = lnkMoreColors_Mobile;
			lstMoreLessColors = lstMoreColors_Mobile;
		} else if(Utils.isTablet()) {
			lnkMoreLessColors = lnkMoreColors_Desk_Tab;
			lstMoreLessColors = lstMoreColors_Desk_Tab;
		} else if(Utils.isDesktop()) {
			lnkMoreLessColors = lnkMoreColors_Desk_Tab;
			lstMoreLessColors = lstMoreColors_Desk_Tab;
		}
		
		if (state.equals("less colours")) {
			if (Utils.waitForElement(driver, lnkMoreLessColors)) {
				WebElement elem = null;
				if(!Utils.isMobile()) {
					elem = lstMoreLessColors.get(index).findElement(By.cssSelector(".swatches-six .product-swatches-all-showless"));
				} else {
					elem = lstMoreLessColors.get(index).findElement(By.cssSelector(".swatches-four .product-swatches-all-showless"));
				}
				BrowserActions.clickOnElementX(elem, driver, "Clicked show less color");
			}
		} 
		if (state.equals("more colours")) {
			if (Utils.waitForElement(driver, lnkMoreLessColors)) {
				WebElement elem = null;
				if(!Utils.isMobile()) {
					elem = lstMoreLessColors.get(index).findElement(By.cssSelector(".swatches-six .product-swatches-all"));
				} else {
					elem = lstMoreLessColors.get(index).findElement(By.cssSelector(".swatches-four .product-swatches-all"));
				}
				BrowserActions.clickOnElementX(elem, driver, "Clicked view more color");
			}
		}
	}
	
	/**
	 * To get the coordinates of the product tile
	 * @throws Exception 
	 */
	public int getWidthOfProductTile(int index, String... viewMoreProduct) throws Exception{
		int x=0;
		if(viewMoreProduct.length>0){
			if(Utils.waitForElement(driver, lnkMoreColors_Desk_Tab)){
				WebElement prdTile=lstMoreColors_Desk_Tab.get(index).findElement(By.xpath("//ancestor::*[contains(@class, 'product-tile')]"));
				x=prdTile.getSize().getWidth();
			}
		}else{
			x=lstProducts.get(index).getSize().getWidth();
		}
		return x;
	}
	
	/**
	 * To get the coordinates of the product tile
	 * @throws Exception 
	 */
	public int getHeightOfProductTile(int index, String... viewMoreProduct) throws Exception{
		int x=0;
		if(viewMoreProduct.length>0) {
			if(Utils.waitForElement(driver, lnkMoreColors_Desk_Tab)) {
				WebElement prdTile=lstMoreColors_Desk_Tab.get(index).findElement(By.xpath("//ancestor::*[contains(@class, 'product-tile')]"));
				x=prdTile.getSize().getHeight();
			}
		} else {
			x=lstProducts.get(index).getSize().getHeight();
		}
		return x;
	}
	
	
	/**
	 * To verify Available colors are more than 6 when +more colors link is available
	 * @return boolean
	 * @throws Exception 
	 */
	public boolean verifyMoreLessColorsAvailability() throws Exception {
		int swatchCount = 0, checkStatus = 0;
		WebElement lnkMoreColors = null;
		List<WebElement> lstMoreColors = null;
		
		if(Utils.isMobile()) {
			lnkMoreColors = lnkMoreColors_Mobile;
			lstMoreColors = lstMoreColors_Mobile;
			if(BrandUtils.isBrand(Brand.ks) || BrandUtils.isBrand(Brand.el)) {
				swatchCount = 3;
			} else if(BrandUtils.isBrand(Brand.fb) || BrandUtils.isBrand(Brand.ww)) {
				swatchCount = 5;
			} else {
				swatchCount = 4;
			}
		} else if(Utils.isTablet()) {
			lnkMoreColors = lnkMoreColors_Desk_Tab;
			lstMoreColors = lstMoreColors_Desk_Tab;
			if(BrandUtils.isBrand(Brand.ks) ) {
				swatchCount = 3;
			} else if(BrandUtils.isBrand(Brand.bh)) {
				swatchCount = 5;
			} else {
				swatchCount = 6;
			}
		} else if(Utils.isDesktop()) {
			lnkMoreColors = lnkMoreColors_Desk_Tab;
			lstMoreColors = lstMoreColors_Desk_Tab;
			if(BrandUtils.isBrand(Brand.ks)) {
				swatchCount = 4;
			} else if(BrandUtils.isBrand(Brand.fb) || BrandUtils.isBrand(Brand.ww) ) {
				swatchCount = 7;
			} else {
				swatchCount = 6;
			}
		}
		if(Utils.waitForElement(driver, lnkMoreColors)) {
			for (int i=0 ; i < lstMoreColors.size() ; i++) {
				int colorSwatchSize = lstMoreColors.get(i).findElements(By.cssSelector(".swatch-list a")).size();
				if((swatchCount < colorSwatchSize)) {
					checkStatus++;
				} else {
					Log.event("More option is displaying even there is less color swatches");
					return false;
				}
			}
		}
		if(checkStatus == lstMoreColors.size()) {
			return true;
		} else {
			return false;
		}
	}
	
	
	/**
	 * To wait for Search results spinner
	 */
	private void waitUntilPlpSpinnerDisappear() {
		Utils.waitUntilElementDisappear(driver, plpSpinner);
	}

	/**
	 * To get list of options available in sort by 
	 * @return List<String> - List of sort options 
	 * @throws Exception - Exception
	 */
	public List<String> getSortbylist() throws Exception {
		List<String> options = new ArrayList<>();
		refinements.openCloseSortBy("expanded");
		for (int i = 0; i < listSortbyOptions.size(); i++) {
			options.add(listSortbyOptions.get(i).getAttribute("innerHTML").trim().toUpperCase());
		}
		return options;
	}

	/**
	 * To get the name for all products in product tile or search results
	 * @return List of String - List of names of all products
	 * @throws Exception - Exception
	 */
	public List<String> getProductNames()throws Exception{
		List<String> lstPrice = new ArrayList<String>();
		for(int i=0; i < lstProductName.size(); i++)
			lstPrice.add(lstProductName.get(i).getText().trim());

		return lstPrice;
	}

	/**
	 * To get all product details as list of hashmap
	 * @return List -
	 * @throws Exception -
	 */
	public List<HashMap<String, String>> getAllProductDetails()throws Exception{
		List<HashMap<String, String>> prdDetails = new ArrayList<HashMap<String, String>>();

		String productName = new String();
		String price = new String();

		int x = lstProducts.size() > 10 ? 10 : lstProducts.size();
		for(int i = 0; i < x; i++){
			HashMap<String, String> product = new HashMap<String, String>();
			try{
				productName = lstProducts.get(i).findElement(By.cssSelector("a.name-link")).getText();
			}			
			catch(NoSuchElementException | StaleElementReferenceException e) {productName=null;}
			try {
				price = lstProducts.get(i).findElement(By.cssSelector("span.price-product-sales-price")).getText();
			}
			catch(NoSuchElementException | StaleElementReferenceException e) {price=null;}
			product.put("Name", productName);
			product.put("Price", price);
			prdDetails.add(product);
		}
		return prdDetails;
	}

	/**
	 * to verify that the vertical refinement is displayed to the left of search results
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyVerticalRefinementDisplayedToTheLeftOfSearchResults()throws Exception{
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, sectionSearchResults, divVerticalRefinement);
	}


	/**
	 * to get the number of product tiles displayed per row in the search results  
	 * @return Number of tile per row
	 * @throws Exception - Exception
	 */
	public int getNumberOfProductTilesPerRow()throws Exception{
		List<WebElement> productTiles = driver.findElements(By.cssSelector("#search-result-items>li"));
		int rowCount = 1;
		for (int i = 0; i < productTiles.size(); i++) {
			if(BrowserActions.verifyElementsAreInSameRow(driver, productTiles.get(i+1), productTiles.get(i))) {
				rowCount++;
			} else {
				break;
			}
		}
		
		Log.event("Row Count :: " + rowCount);
		return rowCount;
	}
	
	/**
	 * to get the breadcrumb value
	 * @return List of String 
	 * @throws Exception - Exception
	 */
	public List<String>  getBreadcrumbText()throws Exception{
		return BrowserActions.getText(lstTxtProductInBreadcrumb, "Breadcrumb value", driver);
	}
	
	
	/**
	 * To get breadcrumb count
	 * @return int
	 * throws Exception
	 */
	public int getBreadcrumbcount() throws Exception{
		return lstTxtProductInBreadcrumb.size();
	}
	
	 /**
     * To get index th string value of breadcrumb
     * @param index -
     * @return String -
     * @throws Exception -
     */
    public String getBreadcrumbValueOf(int index) throws Exception {
        List<WebElement> bcEles = driver.findElements(By.cssSelector(".breadcrumb .breadcrumb-element.hide-mobile"));
        String breadcrumbValue = bcEles.get(index-1).getText();
        Log.event("Breadcrumb Value at #" + index + " :: " + breadcrumbValue);
        return breadcrumbValue;
    }
    
    /**
     * To click on Bread Crumb By Index
     * @param index - type integer, index of the bread crumb need to click
     * @return PlpPage - PlpPage page object
     * @throws Exception - Exception
     */
    public PlpPage clickOnBreadCrumbByIndex(int index) throws Exception {
        BrowserActions.clickOnElementX(lstTxtProductInBreadcrumb.get(index - 1), driver, "Breadcrumb");
        Utils.waitForPageLoad(driver);
        return new PlpPage(driver).get();
    }
    
    /**
     * To click on Bread Crumb By Index
     *
     * @param index - type integer, index of the bread crumb need to click
     * @return PlpPage - PlpPage page object
     * @throws Exception - Exception
     */
    public void clickOnBreadCrumbByIndexValue(int index) throws Exception {
        BrowserActions.clickOnElementX(lstTxtProductInBreadcrumb.get(index - 1), driver, "Breadcrumb");
        Utils.waitForPageLoad(driver);
       
    }
    
    
    /**
     * to verify Bredcrum is displayed or clickable
     * @param str        -
     * @param validation -
     * @return Boolean
     * @throws Exception - Exception
     */
    public boolean validateBreadcrumDisplayedOrClickable(String str, String validation) throws Exception {
        boolean temp = false, flag = false;
        ;
        for (int i = 0; i < lstTxtProductInBreadcrumb.size(); i++) {
            if (validation == "clickable") {
                if (lstTxtProductInBreadcrumb.get(i).getText().equals(str)) {
                    String tag = lstTxtProductInBreadcrumb.get(i).getTagName();
                    temp = lstTxtProductInBreadcrumb.get(i).isEnabled();
                    List<String> clickableTags = Arrays.asList("input", "button", "a");

                    String stringValue = lstTxtProductInBreadcrumb.get(i).getAttribute("href");
                    if ((clickableTags.contains(tag) && stringValue != null) && temp) {
                        flag = true;
                        break;
                    }
                }
            } else if ((validation == "displayed")) {
                if (lstTxtProductInBreadcrumb.get(i).getText().equals(str)) {
                    flag = lstTxtProductInBreadcrumb.get(i).isDisplayed();
                    break;
                }
            }
        }
        return flag;
    }
	
	/**
	 * to get the clearance message of a product
	 * @return List of String 
	 * @throws Exception - Exception
	 */
	public String  getClearanceMessageOfAProduct(String prod)throws Exception{
		String value = "";
		try {
			WebElement prd = driver.findElement(By.cssSelector("div[data-monetate-pid='"+prod+"'] .clearance.promotional-message"));
			value = BrowserActions.getText(driver, prd, "Clearence text");
		} catch (NoSuchElementException e) {
			
			if(Utils.waitForElement(driver, btnPaginationNext)) {
				BrowserActions.clickOnElementX(btnPaginationNext, driver, "Next Page ");
				Utils.waitUntilElementDisappear(driver, waitLoader);
				return getClearanceMessageOfAProduct(prod);
				}
			else{
			    Log.failsoft("Given product "+prod+"is not available or doesnt have clearence message", driver);
		        }
		}	
		return value;
	}
	
	/**
	 * To check the product image is correctly displayed according to the color swatch selected
	 * @return true/false - true when displaying correct image 
	 * @throws Exception - Exception
	 */
	public Object[] checkProdImgIsEqualToColorSwatchImage()throws Exception{
		Object[] obj = new Object[3];
		WebElement tileElement = getProductTile();
		BrowserActions.scrollInToView(tileElement, driver);
		
		List<WebElement> prdSwatches = tileElement.findElements(By.cssSelector(".swatch:not(.selected)"));
		WebElement swatchToSelect = prdSwatches.get(Utils.getRandom(0, prdSwatches.size()-1)).findElement(By.cssSelector(".swatch-image"));
		WebElement prdSwatchBlock = tileElement.findElement(By.cssSelector(".product-swatches"));
		BrowserActions.mouseHover(driver, prdSwatchBlock);
		BrowserActions.clickOnElementX(swatchToSelect, driver, "color swatch on prd tile", false);
		String[] swatchSRC = BrowserActions.getTextFromAttribute(driver, swatchToSelect, "src", "Product tile image").split("/");
		String swatchID = swatchSRC[swatchSRC.length-1].split("\\.")[0].replaceAll("[^0-9|\\-]", "");
		
		WebElement prdImage = tileElement.findElement(By.cssSelector(".product-image .p-image"));
		String[] thumbSRC = BrowserActions.getTextFromAttribute(driver, prdImage, "src", "Product tile image").split("/");
		String thumbID = thumbSRC[thumbSRC.length-1].split("\\.")[0].replaceAll("[^0-9|\\-]", "");
		
		String prodID = BrowserActions.getTextFromAttribute(driver, tileElement, "data-itemid", "Product").trim();
		String colorSelected = BrowserActions.getTextFromAttribute(driver, swatchToSelect, "alt", "Product swatch").split("\\,")[1].trim();
		
		obj[0] = thumbID.equals(swatchID);
		obj[1] = prodID;
		obj[2] = colorSelected;
		return obj;
	}
	
	/**
	 * To check the back view is available or not in PLP
	 * 
	 * @throws Excepton
	 */
	public boolean VerifyBackViewAvailability() throws Exception {
		if (Utils.waitForElement(driver, productImages)) {
			for (int prd = 1; prd < lstProducts.size(); prd++) {
				List<WebElement> prodImg = productTile.get(prd - 1).findElements(By.cssSelector(" .product-image img"));
				if (prodImg.size() > 1) {
					return true;
				}
			}
		} else {
			Log.event("Back view is not available in the page");
		}
		return false;
	}
	
	/**
	 * To check the back view is available or not in PLP
	 * 
	 * @throws Excepton
	 */
	public boolean VerifyBackViewDisplayedOrNot() throws Exception{
		if (Utils.waitForElement(driver, productImages)) {
			for (int prd = 1; prd < lstProducts.size(); prd++) {
				String prdImg = getProdImgInPLPOfAProduct(prd);
				BrowserActions.mouseHover(driver, lstPrdImages.get(prd), "Mouse hovered on producttile");
				if (Utils.waitForElement(driver, lstbackviewImg)) {
					String[] mouseHoverImg = lstPrdImages.get(prd).findElement(By.cssSelector("p-image-rollover"))
							.getAttribute("src").split("\\/");
					String hoveredImg = mouseHoverImg[mouseHoverImg.length - 1].split("\\.")[0];
					if (!prdImg.contains(hoveredImg)) {
						Log.event("Back view image is displayed on mouse hover");
						return true;
					}
				}
			}
		} else {
			Log.event("Back view is not available in the page");
		}
		return false;
	}
	
	
	/**
	 * To get the product image name in PLP of a Product
	 * @return true/false - true when displaying correct image 
	 * @throws Exception - Exception
	 */
	public String getProdImgInPLPOfAProduct(int prod)throws Exception{
		int size = productTile.size();
		String imgName = "";
		if (prod <= size) {
			WebElement prodImg = productTile.get(prod - 1).findElement(By.cssSelector(" .product-image img"));
			String mainImgProd = BrowserActions.getTextFromAttribute(driver, prodImg, "src", "Product Image");
			String[] imgs = mainImgProd.split("\\?")[0].split("\\/");
			imgName = imgs[imgs.length - 1];
			Log.event(mainImgProd+"--- -- "+mainImgProd.split("\\?")[0]+"-- --- --"+imgName);
		}
		return imgName;
	}

	/**
	 * to get the breadcrumb Full text
	 * 
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getBreadcrumbFullText() throws Exception {
		String appendString = "";
		List<String> breadCrumbList = getBreadcrumbText();
		for (int index = 0; index < breadCrumbList.size(); index++) {
			if (index != (breadCrumbList.size() - 1)) {
				String temp = breadCrumbList.get(index).replace("\\", "\\\\").trim();
				appendString = appendString
						+ temp.split("\\\\")[0] + "/";
			} else {
				appendString = appendString + breadCrumbList.get(index).trim();
			}
		}
		return appendString;
	}
	/**
	 * To click on Back To Top Button
	 * @throws Exception - Exception
	 */
	public void clickBackToTopButton() throws Exception{
		if (Utils.waitForElement(driver, btnBackToTop)) {
			BrowserActions.clickOnElementX(btnBackToTop, driver, "Back to Top");
			Utils.waitUntilElementDisappear(driver, btnBackToTop);
		} else {
			Log.reference("The Back to top icon is not displaying");
		}
	}
	/**
	 * Scroll down the page to get Sticky refinments and back to top icon
	 * @throws Exception 
	 * 
	 */
	public void scrollTogetStickyAndBacktoTopButton() throws Exception {
		if(productTile.size() > 4) {
			BrowserActions.scrollToView(productTile.get((productTile.size()/2) + 1), driver);
		} else {
			BrowserActions.scrollToView(productTile.get(productTile.size() - 1), driver);
		}
		Utils.waitForElement(driver, btnBackToTop);
	}
	
	/**
	 * To click on quickshop button
	 * @param prdID -
	 * @return QuickShop -
	 * @throws Exception -
	 */
	public QuickShop clickOnQuickShop(String... prdID)throws Exception{
		WebElement prdTile = null;
		if(prdID.length > 0) {
			boolean found = false;
			while(!found) {
				try {
					prdTile = driver.findElement(By.cssSelector("div[data-itemid='"+prdID[0]+"']"));
					found = true;
				} catch (NoSuchElementException e) {
					if(Utils.waitForElement(driver, btnPaginationNext))
						clickNextButtonInPagination();
					else
						Log.fail("Product Not found.", driver);
				}
			}
		} else {
			prdTile = productTile.get(0);
		}
		BrowserActions.scrollInToView(prdTile, driver);
		BrowserActions.mouseHover(driver, prdTile, "Product Tile");
		
		WebElement qcButton;
		try {
			qcButton = prdTile.findElement(By.cssSelector(".quickview"));
		}catch(Exception e) {
			BrowserActions.refreshPage(driver);
			BrowserActions.mouseHover(driver, productTile.get(0), "Product Tile");
			qcButton = productTile.get(0).findElement(By.cssSelector(".quickview"));
		}
		
		if (Utils.waitForElement(driver, qcButton)) {
			BrowserActions.javascriptClick(qcButton, driver, "Quick Shop Button for First Product");
		}
		return new QuickShop(driver).get();
	}
	
	/**
	 * To click on the normal product (Not product set)
	 * @return 
	 * @return product PDP.
	 */
	public PdpPage clickOnNormalProduct(int...index) throws Exception{
		if(index.length>0){
			BrowserActions.clickOnElementX(productTileLink.get(index[0]), driver, "Clicked specific product");
		}else{
			int number = Utils.getRandom(0, productTileLink.size());
			BrowserActions.scrollInToView(productTileLink.get(number), driver);
			BrowserActions.clickOnElementX(productTileLink.get(number), driver, number+"product");
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}
	
	/**
	 * to click the NEXT link in the pagination section
	 * @throws Exception - 
	 */
	public void clickNextButtonInPagination() throws Exception{
		Log.event("Navigating to next page...");
		if(Utils.isDesktop()) {
			BrowserActions.scrollInToView(paginationSection, driver);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btnPaginationNext);
			Utils.waitForElement(driver, paginationSection);
			Utils.waitUntilElementDisappear(driver, waitLoader);
		} else {
			BrowserActions.scrollInToView(divViewMore, driver);
			BrowserActions.clickOnElementX(btnViewMore.get(btnViewMore.size()-1), driver, "View More");
			Utils.waitUntilElementDisappear(driver, waitLoader);
		}
	}
	/**
	 * to click the Previous link in the pagination section
	 * @throws Exception - 
	 */
	public void clickPrevButtonInPagination()throws Exception{
		Log.event("Navigating to previous page...");
		BrowserActions.scrollInToView(paginationSection, driver);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", btnPaginationPrev);
		Utils.waitForElement(driver, paginationSection);
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}
	
	/**
	 * to verify that the pagination numbers are displayed below the product tiles
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPaginationDisplayedBelowProductTiles()throws Exception{
		BrowserActions.scrollInToView(paginationSection, driver);
		return BrowserActions.verifyVerticalAllignmentOfElements(driver,sectionSearchResults , paginationSection);
	}
	

	/**
	 * To click on Sort by arrow
	 * @param arrow -
	 * @throws Exception -
	 */
	public void clickSortbyArrow() throws Exception {
		BrowserActions.clickOnElementX(sortbydropdown, driver, "click on the dropdown");
	}

	/**
	 * To navigate to PDP Page.
	 * @param index - Optional
	 * @return PDP Page
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPdp(int... index) throws Exception {
		if (lstQuickshopButtons.size() > 0) {
			int prdIndex = index.length > 0 ? (index[0] - 1) : Utils.getRandom(0, lstQuickshopButtons.size());
			BrowserActions.scrollInToView(lstQuickshopButtons.get(prdIndex), driver);
			WebElement productTile = lstQuickshopButtons.get(prdIndex).findElement(By.xpath("../.."));
			BrowserActions.clickOnElementX(productTile, driver, "Product Tile");
		} else if (lstProductSwatchList.size() > 0) {
			int prdIndex = index.length > 0 ? (index[0] - 1) : Utils.getRandom(0, lstProductSwatchList.size());
			BrowserActions.scrollInToView(lstProductSwatchList.get(prdIndex), driver);
			WebElement productTile = lstProductSwatchList.get(prdIndex).findElement(By.xpath("../..")).findElement(By.cssSelector("a.name-link"));
			BrowserActions.clickOnElementX(productTile, driver, "Product Tile");
		} else {
			int prdIndex = index.length > 0 ? (index[0] - 1) : Utils.getRandom(0, lstProductName.size());
			BrowserActions.clickOnElementX(lstProductName.get(prdIndex), driver, "Product Tile");
		}

		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}


	/**
	 * To Navigate To PDP Page of the product ID
	 * @param productID - 
	 * @return PdpPage of given Product ID
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPdp(String prdID)throws Exception{
		Log.event("Trying to Click product name link for product id :: " + prdID);
		
		boolean productExists = paginateToProduct(prdID);
		
		if(productExists) {
			WebElement prdLink = driver.findElement(By.cssSelector("div[data-itemid='"+ prdID.trim() +"'] .name-link .product-name"));
			BrowserActions.clickOnElementX(prdLink, driver, "Name Link for Product " + prdID + " ");
			Utils.waitForPageLoad(driver);
		}else {
			Log.fail("Product ID("+prdID+") not found.");
		}
		return new PdpPage(driver).get();
	}

		
	/**
	 * To click view more button in the PLP page
	 * @throws Exception -
	 */
	public void clickOnViewMore()throws Exception{
		if (Utils.waitForElement(driver, btnViewMore.get(btnViewMore.size()-1))) {
			int size = getProductTileCount()+1;
			BrowserActions.scrollToViewElement(btnViewMore.get(btnViewMore.size()-1), driver);
			BrowserActions.clickOnElementX(btnViewMore.get(btnViewMore.size()-1), driver, "View more button");
			Utils.waitUntilElementDisappear(driver, waitLoader);
			Utils.waitForPageLoad(driver);
			List<WebElement> elem = driver.findElements(By.cssSelector(".product-tile"));
			Utils.waitForElement(driver, elem.get(size-1));
		} else {
			Log.fail("The view more button is not found in the page!");
		}
	}

	/**
	 * To select product color in plp page
	 * @param String - productID
	 * @param String - selectColor
	 * @return String - color details and product name
	 * @throws Exception - Exception
	 */
	public String selectProductColorInPLP(String productID, String ...selectColor)throws Exception{
		WebElement prdTile = null;
		paginateToProduct(productID);
		while (prdTile == null) {
			try {
				prdTile = driver.findElement(By.cssSelector("div[data-itemid='"+ productID.trim() +"']"));
			} catch (Exception ea) {
				if (Utils.waitForElement(driver, btnViewMoreButton) || Utils.waitForElement(driver, lnkNextPage)) {
				      clickNextButtonInPagination();
				} else {
					Log.fail("The product "+productID+" is not found in page!");
				}
			}
		}
		
		WebElement prdColorLink = null;
		if (selectColor.length > 0) {
			try {
				prdColorLink = prdTile.findElement(By.cssSelector(".swatch-list a[title*='" + selectColor[0].trim() + "']"));
			} catch (Exception et) {
				List<WebElement> prdColorLinks = prdTile.findElements(By.cssSelector(".swatch-list a"));
				prdColorLink = prdColorLinks.get(Utils.getRandom(0, prdColorLinks.size() - 1));
			}
		} else {
			List<WebElement> prdColorLinks = prdTile.findElements(By.cssSelector(".swatch-list a"));
			prdColorLink = prdColorLinks.get(Utils.getRandom(0, prdColorLinks.size() - 1));
		}
		BrowserActions.clickOnElementX(prdColorLink, driver, "Product color");
		Utils.waitForPageLoad(driver);
		WebElement colorImg = prdColorLink.findElement(By.cssSelector(" img"));
		String[] colorHref = BrowserActions.getTextFromAttribute(driver, colorImg, "data-thumb", "Product Color").split("\\?")[0].split("\\/");
		return colorHref[colorHref.length-1].split("\\.")[0];
	}

	/**
	 * To get the banner text for the current category
	 * @return String Banner text -
	 * @throws Exception -
	 */
	public String getCategoryName() throws Exception {
		BrowserActions.scrollInToView(categoryBannerText, driver);
		return BrowserActions.getText(driver, categoryBannerText, "Category Banner");	
	}
	

	/**
	 * To click on category plus symbol
	 * @return String Banner text -
	 * @throws Exception -
	 */
	public boolean clickOnPlusOrMinusIcon(String symbol) throws Exception{
		if(symbol.equalsIgnoreCase("plus")){
			if(Utils.waitForElement(driver, CategoryPlusSymbol)){
				BrowserActions.clickOnElementX(CategoryPlusSymbol, driver, "Clicked plus symbol");
				if(Utils.waitForElement(driver, lvl4ChildCategories.get(0))){
					return true;
				}
			}
		}
		if(symbol.equalsIgnoreCase("minus")){
			if(Utils.waitForElement(driver, CategoryPlusSymbol) && Utils.waitForElement(driver, lvl4ChildCategories.get(0))){
				BrowserActions.clickOnElementX(CategoryPlusSymbol, driver, "Clicked plus symbol");
				if(!Utils.waitForElement(driver, lvl4ChildCategories.get(0))){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Click on Category name next to plus symbol
	 * @return new PLP
	 *@throws Exceptiom
	 */
	public PlpPage clickOncategoryNxtPlusSymbol() throws Exception{
		if(Utils.waitForElement(driver, CategoryPlusSymbol)){
			BrowserActions.clickOnElementX(categoryNemeNxtByPlus, driver, "Click on Category name next to plus symbol");
			}
		return new PlpPage(driver);
	}
	
	/**
	 * Click on Sub Categories
	 * @return new PLP
	 *@throws Exceptiom
	 */
	public PlpPage clickOnSubCategory(int index) throws Exception{
		if(verifyIfLevel3Displayed()){
			BrowserActions.clickOnElementX(subCategories.get(index), driver, "Click on Category name next to plus symbol");
			}
		return new PlpPage(driver);
	}
	/**
	 * To get the category name by index
	 * @return String
	 *@throws Exceptiom
	 */
	public String getSubCategoryName(int index) throws Exception{
		if(verifyIfLevel2Displayed()){		
			return	BrowserActions.getText(driver,subCategories.get(index), "Click on Category name next to plus symbol");
		}
		return null;	
	}
	
	/**
	 * To get the category name
	 * @return String
	 *@throws Exceptiom
	 */
	public String getCategoryNamenxtByPlus() throws Exception{
		if(Utils.waitForElement(driver, CategoryPlusSymbol)){		
			return	BrowserActions.getText(driver,categoryNemeNxtByPlus, "Click on Category name next to plus symbol");
		}
		return null;	
	}
	
	/**
	 * To verify Product Badge image in Particular Product
	 * @param productID - 
	 * 
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyProdBadgeImgInParticularProd(String productID, String badgeType)throws Exception{
		Utils.waitForPageLoad(driver);
		WebElement prdLink = null;
		boolean flag = true;
		while (flag) {
			try {
				prdLink = driver.findElement(By.cssSelector("div[data-itemid='"+ productID.trim() +"']"));
				flag = false;
			} catch (Exception ee) {
				if(ee.toString().contains("NoSuchElementException")) {
					try {
						BrowserActions.clickOnElementX(lnkNextPage, driver, "Next page");
						Utils.waitForPageLoad(driver);
					} catch (Exception ex) {
						if(ex.toString().contains("NoSuchElementException")) {
							return false;
						}
					}
				}
			}
		}
		
		if(badgeType.toLowerCase().contains("any")) {
			return true;
		} else {
			WebElement productBadge = prdLink.findElement(By.cssSelector(".b_product_badge img"));
			
			String[] badgeTypeSrc = BrowserActions.getTextFromAttribute(driver, productBadge, "src", "Product badge").split("\\/");
			Log.event("-- -- -- --- -"+badgeType+"-- -- -- --- -- --"+badgeTypeSrc[badgeTypeSrc.length - 1]);
			
			if(badgeTypeSrc[badgeTypeSrc.length - 1].toLowerCase().contains(badgeType.toLowerCase()))
				return true;
			else
				return false;
		}
	}
	
	/**
	 * to verify product message in the plp
	 * @return boolean
	 * @throws Exception
	 */
		
	public boolean verifyProdMsgInPLP(String messageType)throws Exception {
		Utils.waitForPageLoad(driver);
		int pageCount = 1 + (getSearchResultCount() / getPaginationSize());
		int currentPage = 1;
		boolean flag = true;
		while (flag && (currentPage <= pageCount)) {
			try {
				if(messageType.contains("badge")) {
					if(Utils.waitForElement(driver, imgProductBadge)){
						return true;
					}
				}else if(messageType.contains("feature")){
					if(Utils.waitForElement(driver, txtProdFeatureMsg)) {
						return true;
					}
				}else if(messageType.contains("promotion")) {
					if(Utils.waitForElement(driver, txtProdPromoMessage))
						return true;
				}else if(messageType.contains("product")) {
					if(Utils.waitForElement(driver,txtSpecialProductMsg)){
						return true;
					}
				}
				Log.event(messageType + " message not found in current page.");
				clickNextButtonInPagination();
				currentPage = currentPage + 1;
			} catch (Exception ex) {
				if(ex.toString().contains("NoSuchElementException")) {
					return false;
				}
			}
		}
		return false;
	} 
		
	/**
	 * To click anywhere in PLP Page
	 * @throws Exception - Exception
	 */
	public void clickAnywhereInPlpPage() throws Exception {
		Utils.waitForElement(driver, divmain);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", divmain);	
	}

	/**
	 * To verify Level-4 Is diplayed
	 * @return true - if displayed
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfLevel4Displayed() throws Exception {
		try {
			WebElement ele = driver.findElement(By.cssSelector("#category-level-2 > li.expandable.active #category-level-3 #category-level-4"));
			if (ele.isDisplayed())
				return true;
		} catch (NoSuchElementException ex) {
			Log.event("Level 4 is not available");
		}
		return false;
	}

	/**
	 * To verify Level-3 Is diplayed
	 * @return true - if displayed
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfLevel3Displayed() throws Exception {
		try {
			WebElement ele = driver.findElement(By.cssSelector("#category-level-2 > li.expandable.active #category-level-3"));
			if (ele.isDisplayed())
				return true;
		} catch (NoSuchElementException ex) {
			Log.event("Level 3 is not available");
		}
		return false;
	}

	/**
	 * To verify Level-2 Is diplayed
	 * @return true - if displayed
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfLevel2Displayed() throws Exception {
		try{
		WebElement ele = driver.findElement(By.cssSelector("#category-level-1 > li.expandable.active #category-level-2"));
		if(ele.isDisplayed())
			return true;
		}catch(NoSuchElementException ex){
			Log.event("Level 2 is not available");
		}
		return false;
	}
	
	/**
	 * To get original price product by index
	 * @param int - index of product to be clicked on
	 * @return PdpPage - pdp Page object
	 * @throws Exception - Exception
	 */
	public String getOriginalPriceProduct(int index) throws Exception {
		return BrowserActions.getText(driver, lstProductActualSalesPrice.get(index-1), "Original price");
	}
	
	/**
	 * To get sale price product by index
	 * @param index - index of product to get sale price
	 * @return String - sale price
	 * @throws Exception - Exception
	 */
	public String getSalePriceProduct(int index) throws Exception {
		return BrowserActions.getText(driver, lstProductSalesPrice.get(index-1), "Sale price");
	}
	
	/**
	 * To verify was and now price
	 * @return boolean - true if 
	 * @throws Exception - Exception
	 */
	public boolean verifyStandaredProductPrice() throws Exception {	
		boolean stdPriceFound = false ;
		do {
			try {	
				
				WebElement prd = lstProductActualSalesPrice.get(0);
				String prdStndPrice = BrowserActions.getText(driver, prd, "Standard price").replace("$", "").trim();
				
				if(!prdStndPrice.contains("\\-")) {
						return true;
				} else {
						return false;
				}
				
			} catch (NoSuchElementException e) {
				if(Utils.waitForElement(driver, btnPaginationNext) || Utils.waitForElement(driver, btnViewMoreButton)) {
					clickNextButtonInPagination();
				}else {
					stdPriceFound = false;
				}
			}
			
		} while(stdPriceFound);
		Log.failsoft("Sale and standard Price range product is not available in this category PLP");
		
		return false;
	}
	
	/**
	 * To click On Product Having Sale And standard Price Range
	 * @return PdpPage - PdpPage 
	 * @throws Exception - Exception
	 */
	public PdpPage clickOnProductHaveSaleAndStndPriceRange() throws Exception {		
		try {
			WebElement element = txtProductSalesPriceRange.findElement(By.xpath("//span[@class='price-product-standard-price' and contains(text(),' - ')]//ancestor::*[contains(@class, 'product-tile')]//div[contains(@class,'product-name ')]"));
			BrowserActions.clickOnElementX(element, driver, "Product name");
			Utils.waitForPageLoad(driver);
		} catch (NoSuchElementException e1) {
			refinements.selectSortBy("Lowest Priced");
			Log.event("Sorted by Lowest Priced.");
			
			try {
				WebElement element = txtProductSalesPriceRange.findElement(By.xpath("//span[@class='price-product-standard-price' and contains(text(),' - ')]//ancestor::*[contains(@class, 'product-tile')]//div[contains(@class,'product-name ')]"));
				BrowserActions.clickOnElementX(element, driver, "Product name");
				Utils.waitForPageLoad(driver);
			} catch(NoSuchElementException e2) {
				Log.failsoft("No Product with Sales price range and Standard Price range");
				return null;
			}
		}
		return new PdpPage(driver).get();
	}
	
	/**
	 * To get first Product id Having Sale And standard Price both in Range
	 * @return String - Product id 
	 * @throws Exception - Exception
	 */
	public String getProductIdHaveSaleAndStndPriceRange() throws Exception {		
		try {
			WebElement prdTile = txtProductSalesPriceRange.findElement(By.xpath("//span[@class='price-product-standard-price' and contains(text(),' - ')]//ancestor::*[contains(@class, 'product-tile')]"));
			return BrowserActions.getTextFromAttribute(driver, prdTile, "data-itemid", "product");
		} catch (NoSuchElementException e) {
			Log.failsoft("No Product with Sales price range and Standard Price range");
		}
		return "";
	}
	
	/**
	 * To verify the click the original price product by index
	 * @param int - index of product to be clicked on
	 * @return PdpPage - pdp Page object
	 * @throws Exception - Exception
	 */
	public PdpPage clickAnOriginalPriceProduct(int index) throws Exception {
		WebElement prd = lstProductActualSalesPrice.get(index-1);		
		WebElement prdTile = prd.findElement(By.xpath("ancestor::*[contains(@class, 'product-tile')]//a[contains(@class, 'name-link')]"));
		BrowserActions.clickOnElementX(prdTile, driver, "click on the tiles");
		return new PdpPage(driver).get();
	}


	/**
	 * To verify the mouse hover by index
	 * @param int - index of product to hover mouse on  
	 * @throws Exception -Exception
	 */
	public void mouseHoverProductByIndex(int index) throws Exception 
	{
		WebElement prdTile = productTile.get(index-1);		
		BrowserActions.mouseHover(driver, prdTile);
		Log.event("Mouse hover to element");
	}	
	
	/**
	 * To verify the mouse hover by index
	 * @param int - index of product to hover mouse on  
	 * @throws Exception -Exception
	 */
	public void mouseHoverProductSetByIndex(int index) throws Exception {
		BrowserActions.scrollInToView(productTileSet.get(index-1), driver);
		WebElement prdTile = productTileSet.get(index-1);	
		BrowserActions.mouseHover(driver, prdTile);
		Log.event("Mouse hover to element");
	}	
	
	/**
	 * To verify the mouse hover by product
	 * @param int - index of product to hover mouse on  
	 * @throws Exception -Exception
	 */
	public void mouseHoveredOnProduct(String productId) throws Exception{
		Log.event("Given Item ID :: " + productId.trim());
		paginateToProduct(productId.trim());
		try {
			WebElement prdTile = driver.findElement(By.xpath("//div[@data-itemid='"+productId+"']//div[@class='product-image']"));
			BrowserActions.scrollInToView(prdTile, driver);
			BrowserActions.mouseHover(driver, prdTile);
			Log.event("Mouse hover to element");
		}catch(NoSuchElementException e) {
			if(Utils.waitForElement(driver, btnPaginationNext)) {
				clickNextButtonInPagination();
			}
		}
	}
	
	/**
	 * To verify the quick shop button displayed in tiles
	 * @param index - index of product tile to verify quick shop button on
	 * @return true - if displayed
	 * @throws Exception
	 */
	public boolean verifyQuickShopButtonDisplayed(int index) throws Exception {
		WebElement productTileByIndex = productTile.get(index-1);
		BrowserActions.scrollInToView(productTileByIndex, driver);
		try {
			WebElement quickOrderButton = productTileByIndex.findElement(By.cssSelector(".quickview"));
			return (quickOrderButton.isDisplayed());
		} catch(Exception e) {
			Log.event(e.getMessage());
			return false;
		}
	}

	/**
	 * To verify sort by list is hiding or not.
	 * @return true -if Hiding element is displayed. 
	 * @throws Exception -Exception
	 */
	public boolean verifySortByListIsHiding() throws Exception {
		WebElement hideSrtList = driver.findElements(By.cssSelector(".sort-by.custom-select .selection-list")).get(1);
		if((hideSrtList.isDisplayed()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * To verify sort by list is hiding or not.
	 * @return true -if Hiding element is displayed. 
	 * @throws Exception -Exception
	 */
	public String getCurrentSortMode() throws Exception{
		Utils.waitForPageLoad(driver);
		try {
			Utils.waitForElement(driver, defaultSortOption);
			BrowserActions.scrollToViewElement(sortbydropdown, driver);
			Log.event("----> Attribute:: "+ BrowserActions.getTextFromAttribute(driver, defaultSortOption, "innerHTML", "Sort option"));
			String sortOption = BrowserActions.getText(driver, defaultSortOption, "Sort option");
			Log.event("----> Current Sort option:: "+ sortOption);
			return sortOption;
		}catch(NoSuchElementException e) {
			Log.event("Sort Option element can not be located. Please check locator.");
			Log.event(e.getLocalizedMessage());
			return null;
		}
	}

	/**
	 * To get the price for given product in search result page
	 * @param String - Product ID
	 * @return String - Price of Product with provided ID
	 * @throws Exception - Exception
	 */
	public String getPriceOf(String productID)throws Exception{
		paginateToProduct(productID);
		String lstPrice = new String();
		lstPrice = driver.findElement(By.cssSelector("div[data-itemid='" + productID + "'] .product-sales-price")).getText().trim();
		return lstPrice;
	}

	/**
	 * To open quick shop modal for give product id.
	 * @param String - Product ID
	 * @return QuickShop - QuickShop page object
	 * @throws Exception - Exception
	 */
	public QuickShop clickQuickShop(String productID)throws Exception{
		Log.event("Given Item ID :: " + productID.trim());
		paginateToProduct(productID.trim());
		boolean flag = true;
		do {
			try {
				WebElement prdTile = driver.findElement(By.xpath("//div[@data-itemid='"+productID+"']//div[@class='product-image']"));
				BrowserActions.scrollInToView(prdTile, driver);
				BrowserActions.mouseHover(driver, prdTile);
				WebElement quickShopButton = driver.findElement(By.xpath("//div[@data-itemid='"+productID.trim()+"']//a[@id='quickviewbutton']"));

				if(quickShopButton.isDisplayed()) {
					BrowserActions.clickOnElementX(quickShopButton, driver, "Quick Shop Button");
				}else{
					BrowserActions.javascriptClick(quickShopButton, driver, "Quick Shop Button");
				}

				return new QuickShop(driver).get();
			}catch(NoSuchElementException e) {
				if(Utils.waitForElement(driver, btnPaginationNext)) {
					clickNextButtonInPagination();
				}else {
					flag = false;
				}
			}
		}while(flag);
		Log.fail("Given Product ID not present in current category...");
		return null;
	}
	
	/**
	 * To open quick shop modal for give product id.
	 * @param String - Product ID
	 * @return QuickShop - QuickShop page object
	 * @throws Exception - Exception
	 */
	public QuickShop clickOnQuickShopButton() throws Exception{
		if(Utils.waitForElement(driver, btnQuickView)){
			BrowserActions.clickOnElementX(btnQuickView, driver, "Quick Shop Button");
		}
		Utils.waitForPageLoad(driver);
		return new QuickShop(driver).get();	
	}
	
	/**
	 * To verify all products tile images are loaded
	 * @return boolean
	 * @throws Exception 
	 */
	public boolean verifyProductTileImages() throws Exception{
		for(WebElement prdTile : productTile) {
			if(!prdTile.findElement(By.cssSelector(".p-image")).isDisplayed()) {
				return false;
			}
		}
		for(WebElement setTile : productTileSet) {
			if(!setTile.findElement(By.cssSelector(".p-image")).isDisplayed()) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify contents of a product tile
	 * @param int - Index of product of verify tile(Optional)
	 * @return boolean - if tile conatins all element - true if tile has missing element - false
	 * @throws Exception - Exception
	 */
	public boolean verifyProductTileContent(int ...prodNumber) throws Exception{
		int index;
		if(prodNumber.length == 1) {
			index = prodNumber[0];
		}else {		
			Random rand = new Random();
			index = rand.nextInt(getSwatchConfiguredTileCount());
		}
		WebElement swatchElement = lstProductsWithColorSwatch.get(index);
		BrowserActions.scrollInToView(swatchElement, driver);
		WebElement tileElement = swatchElement.findElement(By.xpath(".."));
		
		if(tileElement.findElement(By.cssSelector(".p-image")).isDisplayed()) {
			Log.event("Product image displayed for tile number: " + index);
		}else {
			Log.event("Product image not displayed for tile number: " + index);
			return false;
		}
		if(tileElement.findElement(By.cssSelector(".product-name")).isDisplayed()) {
			Log.event("Product name available for tile number: " + index);
		}else {
			Log.event("Product name not available for tile number: " + index);
			return false;
		}
		if(tileElement.findElement(By.cssSelector(".product-pricing")).isDisplayed()) {
			Log.event("Product price available for tile number: " + index);
		}else {
			Log.event("Product price not available for tile number: " + index);
			return false;
		}
		if(tileElement.findElement(By.cssSelector(".product-swatches")).isDisplayed()) {
			Log.event("Color swatch not available for tile number: " + index);
		}else {
			Log.event("Color swatch not available for tile number: " + index);
			return false;
		}

		return true;
	}
	
	/**
	 * To get a product tile element to use in other methods.
	 * @param prdIndex - (Optional) index of product
	 * @return WebElement
	 * @throws Exception
	 */
	private WebElement getProductTile(int ...prdIndex) throws Exception{
		int index;
		Random rand = new Random();
		if(prdIndex.length == 1) {
			index = prdIndex[0];
		}else {		
			index = rand.nextInt(getSwatchConfiguredTileCount());
		}
		WebElement swatchElement = lstProductsWithColorSwatch.get(index);
		BrowserActions.scrollInToView(swatchElement, driver);
		WebElement tileElement = swatchElement.findElement(By.xpath(".."));
		Log.event("Product Tile:: " + tileElement.getAttribute("data-monetate-producturl"));
		return tileElement;
	}
	
	/**
	 * To get a product set tile element to use in other methods.
	 * @param prdsetIndex - (Optional) index of product
	 * @return WebElement
	 * @throws Exception
	 */
	private WebElement getProductSetTile(int ...prdsetIndex) throws Exception{
		int index;
		Random rand = new Random();
		if(prdsetIndex.length == 1) {
			index = prdsetIndex[0];
		}else {		
			index = rand.nextInt(lstProductSet.size());
		}
		WebElement prdsetLink = lstProductSet.get(index);
		BrowserActions.scrollInToView(prdsetLink, driver);
		WebElement tileElement = prdsetLink.findElement(By.xpath("../.."));
		Log.event("ProductSet Tile:: " + tileElement.getAttribute("data-monetate-producturl"));
		return tileElement;
	}
	
	/**
	 * To verify image change by color swatch
	 * @param int - product number(Optional)
	 * @return boolean - image update verification
	 * @throws Exception - Exception
	 */
	public boolean verifyProductImageUpdate(int ...prodNumber) throws Exception{
		int index;
		Random rand = new Random();
		if(prodNumber.length == 1) {
			index = prodNumber[0];
		}else {		
			index = rand.nextInt(getSwatchConfiguredTileCount());
		}
		WebElement tileElement = getProductTile(index);

		WebElement elementImage = null;
		String beforeURL = null;
		boolean imageFound = false;

		while (index < lstProductsWithColorSwatch.size() && !imageFound) {
			elementImage = tileElement.findElement(By.cssSelector(".p-image"));
			beforeURL = elementImage.getAttribute("src");
			Log.event("beforeURL:: "+beforeURL);

			if(beforeURL.toLowerCase().contains("noimage")) {
				index = index + 1;
			} else {
				imageFound = true;
			}
			tileElement = lstProductsWithColorSwatch.get(index).findElement(By.xpath(".."));
		}

		if(beforeURL.toLowerCase().contains("noimage")) {
			Log.reference("All swatch configured product in this category are without main image.");
			return false;
		}

		List<WebElement> colorSwatches = tileElement.findElements(By.cssSelector(".swatch-list .swatch:not(.selected)"));
		WebElement unselectedColor = colorSwatches.get(rand.nextInt(colorSwatches.size()));
		BrowserActions.clickOnElementX(unselectedColor, driver, "Unselected color option");

		elementImage = tileElement.findElement(By.cssSelector(".p-image"));
		String afterURL = elementImage.getAttribute("src");
		Log.event("afterURL:: "+afterURL);

		return (!beforeURL.equals(afterURL));
	}
	
	/**
	 * To verify that hovering over product set does not show quickshop button
	 * @param prdsetIndex (Optional)
	 * 
	 */
	public boolean verifyProductSetNoQuickshop(int... prdsetIndex) throws Exception {
		if (lstProductSet.size() == 0) {
			Log.failsoft("No Product Set found on current page...");
			return false;
		}
		int index;
		if (prdsetIndex.length == 1) {
			index = prdsetIndex[0] < lstProductSet.size() ? prdsetIndex[0] : Utils.getRandom(0, lstProductSet.size());
		} else {		
			index = Utils.getRandom(0, lstProductSet.size());
		}
		WebElement tileElement = getProductSetTile(index);
		BrowserActions.mouseHover(driver, tileElement);
		
		try {
			WebElement prdSetQuickshop = tileElement.findElement(By.cssSelector("#quickviewbutton"));
			Log.event(prdSetQuickshop.toString() + "is found by hovering on " + tileElement.toString());
			return false;
		} catch (NoSuchElementException nsle) {
			return true;
		}
	}
	
	/**
	 * To paginate to product in PLP
	 * @param String - Product ID
	 * @throws Exception - Exception
	 * @return boolean - if product is found
	 */
	public boolean paginateToProduct(String productID)throws Exception{
		boolean prdFound = false;
		int pageCount = 1 + (getSearchResultCount() / getPaginationSize());
		if(pageCount > 1 && Utils.isDesktop()) {
			clickOnPageNumberInPagination(1);
		}
		int currentPage = 1;
		while(!prdFound && (currentPage <= pageCount)) {
			try {
				WebElement prdTile = driver.findElement(By.xpath("//div[@data-itemid='"+productID+"']//div[@class='product-name-image-container']"));
				BrowserActions.scrollInToView(prdTile, driver);
				BrowserActions.mouseHover(driver, prdTile);
				return prdFound = true;
			} catch (NoSuchElementException e) {
				Log.event("Product not found in page: " + currentPage);
				clickNextButtonInPagination();
				currentPage = currentPage + 1;
			}
		}
		
		Log.fail("Given Product ID not present in current category...");
		return false;
	}
	
	/**
	 * To verify sort by options in PLP page
	 * @throws Exeption
	 * @return boolean - if expected options are present
	 */
	public boolean verifySortByOptions() throws Exception {
		BrowserActions.refreshPage(driver);
		List<String> expectedSortOptions = Arrays.asList(TestData.get("plp_sort-order").toUpperCase().split("\\|"));
		List<String> actualSortOptions = getSortbylist();
		return CollectionUtils.listContainsList(expectedSortOptions, actualSortOptions);
	}
	
	/**
	 * To apply sort
	 * @param option - Sort option to apply
	 * @throws Exception
	 */
	public void sortBy(String option)throws Exception{
		if(Utils.isDesktop()) {
			BrowserActions.clickOnElement(drpSortBySelectedDesktop, driver, "Sort By Option");
		} else {
			BrowserActions.clickOnElement(drpSortBySelectedMobTab, driver, "Sort By Option");
		}
		
		for(WebElement opt : sortByOptions)
			if(opt.getText().equalsIgnoreCase(option))
				BrowserActions.clickOnElement(opt, driver, option);
		
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}
	
	/**
	 * To verify product badge location
	 * @throws Exception
	 */
	public boolean verifyBadgeImageLocation()throws Exception{
		WebElement prdImg = driver.findElement(By.xpath("//div[@class='b_product_badge']//img//ancestor::div[contains(@class,'product-image')]"));
		WebElement badge = driver.findElement(By.xpath("//div[@class='b_product_badge']//img"));
		
		int x = prdImg.getLocation().getX() + (prdImg.getSize().width/2);
		int x1 = badge.getLocation().getX();
		return (x1 < x);
	}
	
	/**
	 * To get current pagination size 
	 * @return pagination size
	 * @throws Exception
	 */
	public int getPaginationSize() throws Exception{
		int paginationSize = 60;
		try {
			if((getProductTileCount() < getSearchResultCount())) {
				WebElement nextPageButton;
				if(Utils.isDesktop()) {
					nextPageButton = lnkNextPage;
				}else {
					nextPageButton = btnViewMoreButton;
				}
				String nextURL = nextPageButton.getAttribute("href");
				nextURL = nextURL.split("sz=")[1].split("&")[0];
				paginationSize = StringUtils.getNumberInString(nextURL);
			}else {
				Log.event("Returning default value.");
			}
		}catch(NoSuchElementException e) {
			Log.event("Returning default value.");
		}
		return paginationSize;
	}
	
	/**
	 * To verify that user is in expected PLP category
	 * @param Expected category name
	 * @throws Exception  
	 */
	public boolean verifyCategoryHeader(String level) throws Exception{
		return getCategoryName().equalsIgnoreCase(level);
	} 
	
	/**
	 * To wait for Back-To-Top button to dispppear
	 * @throws Exception
	 */
	public void waitUntilBackToTopDisappear()throws Exception{
		Utils.waitUntilElementDisappear(driver, btnBackToTop);
	}
	
	/**
	 * To get product Id of product with color based on index
	 * @param index - index of the product 
	 * @return String - Product ID 
	 * @throws Exception - Exception
	 */
	public String getProductWithColorSwatch(int index) throws Exception {
		int size = productWithColorSwatch.size();
		if(size >= (index-1)) {
			WebElement productElem = productWithColorSwatch.get(index-1);
			return BrowserActions.getTextFromAttribute(driver, productElem, "data-itemid", "Product ID");
		}
		return null;
	}
	
	
	/**
	 * To select product color in plp page
	 * @param String - productID
	 * @param String - selectColor
	 * @return String - color details and product name
	 * @throws Exception - Exception
	 */
	public String selectProductColorInPLP(String productID, int index)throws Exception{
		paginateToProduct(productID);
		WebElement prdTile = driver.findElement(By.cssSelector("div[data-itemid='"+ productID.trim() +"']"));
		List<WebElement> prdColorLinks = prdTile.findElements(By.cssSelector(".swatch-list a"));
		WebElement prdColorLink = prdColorLinks.get(index-1);
		BrowserActions.clickOnElementX(prdColorLink, driver, "Product color");
		Utils.waitForPageLoad(driver);
		WebElement colorImg = prdColorLink.findElement(By.cssSelector(" img"));
		String[] colorHref = BrowserActions.getTextFromAttribute(driver, colorImg, "data-thumb", "Product Color").split("\\?")[0].split("\\/");
		return colorHref[colorHref.length-1].split("\\.")[0];
	}

	/**
	 * To verify the position of the "Back to top" button
	 * @return true - if displayed properly.
	 * @throws Exception
	 */
	public boolean verifyBackToTopButton() throws Exception {
		driver.manage().window().fullscreen();
		scrollTogetStickyAndBacktoTopButton();
		Utils.waitForElement(driver, btnBackToTop);
		if(elementLayer.verifyInsideElementAlligned("btnBackToTop", "sectionSearchResults", "right", this)) {
			driver.manage().window().maximize();
			return true;
		}
		driver.manage().window().maximize();
		return false;
	}
}
