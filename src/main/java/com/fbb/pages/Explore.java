package com.fbb.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class Explore extends LoadableComponent<Explore>{


	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	String runBrowser;

	/**********************************************************************************************
	 ********************************* WebElements of Explore Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = ".pt_lookbooklanding")
	WebElement readyElement;

	@FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
	WebElement breadcrumExplore;
	
	@FindBy(css = ".breadcrumb-element.hide-desktop.current-element.hide-tablet")
	WebElement breadcrumExploreMobile;

	@FindBy(css = ".breadcrumb-element.hide-mobile")
	WebElement breadcrumHomeHyperLink;
	
	@FindBy(css = ".cat-landing-content")
	WebElement contentSlot;

	@FindBy(css = ".promo-banner")
	WebElement promotionalContent;

	@FindBy(css = ".hotspot-1.hotspot.hide-mobile.hide-tablet")
	WebElement btnhotspot1;
	
	@FindBy(css = ".banner.banner-slot-1 .text-align .heading")
	WebElement contentSlotHeading;
	/**********************************************************************************************
	 ********************************* WebElements of Explore Page - Ends ****************************
	 **********************************************************************************************/



	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public Explore(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Explore Page cannot be open.", driver);
		}

		elementLayer = new ElementLayer(driver);
	}// isLoaded

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To Navigate to HomePage
	 * @return HomePage
	 * @throws Exception - Exception
	 */
	public HomePage navigateToHomePage()throws Exception{
		BrowserActions.clickOnElementX(breadcrumHomeHyperLink, driver, "Explore Breadcrum Hyperlink");
		Utils.waitForPageLoad(driver);
		return new HomePage(driver).get();
	}
	
	/**
	 * To Navigate to HotSpotPage
	 * @throws Exception - Exception
	 */
	public void navigateToHotspotPage()throws Exception{
		BrowserActions.clickOnElementX(btnhotspot1, driver, "Hotspot 1");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * Returns string value of breadcrumb found in page
	 * @throws Exception - Exception
	 * @return string - actualValue
	 */
	public String getBreadCrumbText()throws Exception{
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		String actualValue = (String) executor.executeScript
				("var x = document.querySelectorAll('.breadcrumb .hide-mobile'); var i; var st='';for (i = 0; i < x.length; i++) {st += x[i].textContent;} return st;");
		System.out.println("breadcrumbtext: " + actualValue);
		return actualValue;
	}
}
