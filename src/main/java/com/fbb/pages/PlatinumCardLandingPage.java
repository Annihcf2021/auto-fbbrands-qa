package com.fbb.pages;

import java.util.ArrayList;
import java.util.Arrays;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.account.PreApprovedPlatinumCardPage;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;


public class PlatinumCardLandingPage extends LoadableComponent<PlatinumCardLandingPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	/**********************************************************************************************
	 ********************************* WebElements of Platinum Cards Page ****************************
	 **********************************************************************************************/

	@FindBy(css = ".pt_specials")
	WebElement readyElement;

	@FindBy(css = "div[class*='pt_specials']" )
	WebElement lnkNavPlatinumCards;

	@FindBy(css = ".plcc-top-content" )
	WebElement topContentSlot;

	@FindBy(css = ".plcc-landing-perks .content-asset .perks-content" )
	WebElement platinumPerksLogos;

	@FindBy(css = ".plccbenefits-n-email .plcc-platinumcard-benefits" )
	WebElement exclusiveBenefitsCopy;
	
	@FindBy(css = "#navigation")
	WebElement divGlobalNavigation;

	@FindBy(css = ".plccbenefits-n-email .footer-email-signup .email-signup.make-label-absolute" )
	WebElement emailSignup;

	@FindBy(css = ".perks-offer.clearboth" )
	WebElement freeShippingBlock;

	@FindBy(css = ".bottom-content" )
	WebElement bottomContentSlot;

	@FindBy(css = ".header-promo-bottom .promo-banner" )
	WebElement promotionalContent;

	@FindBy(css = ".plccbenefits-n-email .form-row.emailsignup.email.required input" )
	WebElement emailTextField;

	@FindBy(css = ".plccbenefits-n-email .form-row.emailsignup.email.required .field-wrapper" )
	WebElement emailField;

	@FindBy(css = ".plccbenefits-n-email .label-text" )
	WebElement emailTextFieldPlaceHolder;

	@FindBy(css = ".plccbenefits-n-email .input-focus .error" )
	WebElement emailPlaceHolderIncorrectErrorText;

	@FindBy(css = ".plccbenefits-n-email .error" )
	WebElement emailPlaceHolderEmptyErrorText;

	@FindBy(css = ".plccbenefits-n-email .form-row.form-row-button button" )
	WebElement signupButton;

	@FindBy(css = ".module-studio a[name*='Apply']")
	WebElement applyButton;
	
	@FindBy(css = ".hide-mobile a[href$='/Credit-SignInToPreApprove']")
	WebElement lnkSignInCompleteDesktop;
	
	@FindBy(css = ".hide-desktop a[href$='/Credit-SignInToPreApprove']")
	WebElement lnkSignInCompleteMobile;
	
	@FindBy(css = ".cardstatus a")
	WebElement lnkManageAccount;
	
	@FindBy(css = ".hide-desktop .preapproved a")
	WebElement lnkSignInToPreApproveMobTab;
	
	@FindBy(css = ".hide-mobile.hide-tablet .preapproved a")
	WebElement lnkSignInToPreApproveDesktop;

	@FindBy(css = ".plccbenefits-n-email .form-row.form-row-button" )
	WebElement signup;

	@FindBy(css = ".plccbenefits-n-email .email-signup.make-label-absolute .email-signup-footer-success .content-asset .email-signup-footer-success-heading" )
	WebElement thankYouMessage;

	@FindBy(css = ".plccbenefits-n-email .plcc-platinumcard-benefits .plcc-benefits-head" )
	WebElement exclusiveBenefitsContentHeading ;

	@FindBy(css = ".plccbenefits-n-email .plcc-platinumcard-benefits .plcc-benefit-msg" )
	WebElement exclusiveBenefitsContentMessage ;
	
	@FindBy(css = ".plcc-top-banner")
	WebElement divBannerElementAlt;
	
	@FindBy(css = ".hide-mobile.hide-tablet")
	WebElement divBannerDesktop;
	
	@FindBy(css = ".hide-tablet.hide-desktop")
	WebElement divBannerMobile;
	
	@FindBy(css = ".hide-mobile.hide-desktop")
	WebElement divBannerTablet;

	@FindBy(css = ".plcc-form-section")
	WebElement divPlccFormSection;
	/*********************************************************
	 * **************** WebElements of Mandatory Error********
	 *********************************************************/
	@FindBy(xpath = "//div[@class='form-row  email required error-handle']")
	WebElement txtEmailMandatoryError;
	
	@FindBy (css = ".make-label-absolute .form-row.email.error-handle .input-focus .error")
	WebElement txtInvalidEmailError;

	@FindBy(css = ".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable.plcc-apply-error-model")
	WebElement plccApprovedOverlay;

	@FindBy(css = ".plcc-landing-content")
	WebElement divplccLandingPage;

	@FindBy(css = ".form-row.phone-number-msg")
	WebElement phoneContactDisclaimer;

	@FindBy(css = ".see-benefits")
	WebElement linkSeeBenefits;

	@FindBy(css = ".landing-page>button")
	WebElement btnCancel;
	
	@FindBy(css = ".review-form-error-msg")
	WebElement errorFormReview;
	
	@FindBy(css = ".plcc-bottom-section .content-asset")
	WebElement divCreditCardDisclaimer;
	
	@FindBy(css = ".preapproved-msg")
	WebElement divPreapprovedMessage;
	
	@FindBy(css = ".text-content .msg")
	WebElement divSavedPLCCMessage;
	
	@FindBy(css = ".card-img")
	WebElement bannercardImage;
	
	@FindBy(css = ".logo")
	WebElement bannerBrandLogo;
	
	@FindBy(css = ".benefits-tab")
	WebElement headingBenefits;
	
	@FindBy(css = ".rewards-tab")
	WebElement headingRewards;
	
	@FindBy(css = ".faqs-tab")
	WebElement headingFAQS;
	
	@FindBy(css = ".platinumperk-tab")
	WebElement headingPlatinumPerk;
	
	@FindBy(css = ".rewards")
	WebElement sectionRewards;
	
	@FindBy(css = ".qa-section")
	WebElement sectionFAQS;
	
	/**********************************************************************************************
	 ********************************* WebElements of Platinum Cards Page ****************************
	 **********************************************************************************************/	

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public PlatinumCardLandingPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Platinum Page did not open up.", driver);
		}
		elementLayer = new ElementLayer(driver);
	}

	/**
	 * To verify Top content is displayed below promotional content
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyTopContentSlotBelowPromotionalContent() throws Exception {
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, promotionalContent, topContentSlot);
	}

	/**
	 * To verify Platinum perks logo displayed below Top content slot
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPlatinumPerksLogosBelowTopContentSlot() throws Exception {
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, topContentSlot, platinumPerksLogos);
	}

	/**
	 * To verify Platinum Perks Logo displayed Right side of execlusive benefits
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyplatinumPerksLogosRightOfOrBelowexclusiveBenefits()throws Exception{
		if(Utils.isMobile()) {
			return BrowserActions.verifyVerticalAllignmentOfElements(driver, platinumPerksLogos,exclusiveBenefitsCopy);
		} else {
			return BrowserActions.verifyHorizontalAllignmentOfElements(driver, exclusiveBenefitsCopy,platinumPerksLogos);
		}
	}

	/**
	 * To verify Submit button is displayed right side to Email Field
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifySubmitRightOfEmailTextField()throws Exception{
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, signupButton, emailField);
	}

	/**
	 * To verify Email Address textbox is displayed below Benefits
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailAddressTextBoxBelowExclusiveBenefits() throws Exception {
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, exclusiveBenefitsCopy, emailSignup);
	}

	/**
	 * To verify Content slot displayed below Platinum logo
	 * @return boolean value
	 * @throws Exception - 
	 */
	public Boolean verifyBottomContentSlotBelowPlatinumLogo() throws Exception {

		Boolean flag=true;
		flag = BrowserActions.verifyVerticalAllignmentOfElements(driver, platinumPerksLogos, freeShippingBlock);
		flag = BrowserActions.verifyVerticalAllignmentOfElements(driver, freeShippingBlock, bottomContentSlot);

		return flag;
	}

	/**
	 * To Type text into Email Field
	 * @param email - 
	 * @throws Exception - 
	 */
	public void typeOnFields(String email)throws Exception{
		emailTextField.clear();
		BrowserActions.typeOnTextField(emailTextField, email, driver, "Email Address ");
	}

	/**
	 * To type default email address in email address
	 * @return string value
	 * @throws Exception - Exception
	 */
	public String EnterEmailAddressInTextBox() throws Exception {
		emailTextField.clear();
		emailTextField.sendKeys("automation@yopmail.com");
		return BrowserActions.getText(driver, emailTextField, "Entered Email Field Text");
	}

	/**
	 * To type click on signup button
	 * @throws Exception - Exception
	 */
	public void clickOnSignUp()throws Exception{
		BrowserActions.clickOnElementX(signupButton, driver, "Clicked on signup");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on apply button
	 * @return PlatinumCardApplication - page object
	 * @throws Exception - Exception
	 */
	public PlatinumCardApplication clickOnApplyButton()throws Exception{
		Utils.waitForElement(driver, applyButton);
		BrowserActions.scrollToView(applyButton, driver);
		BrowserActions.clickOnElementX(applyButton, driver, "Apply Button");
		Utils.waitForPageLoad(driver);
		return new PlatinumCardApplication(driver);
	}
	
	/**
	 * To click on Accept this offer
	 * @return PreApprovedPlatinumCardPage - page object
	 * @throws Exception - Exception
	 */
	public PreApprovedPlatinumCardPage clickAcceptThisOffer()throws Exception{
		Utils.waitForElement(driver, applyButton);
		BrowserActions.scrollToView(applyButton, driver);
		BrowserActions.clickOnElementX(applyButton, driver, "Apply Button");
		Utils.waitForPageLoad(driver);
		return new PreApprovedPlatinumCardPage(driver);
	}

	/**
	 * To verify email place holder
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailPlaceholderMovestotheTop()throws Exception {
		Boolean flag = true;
		emailTextField.clear();
		float y1=emailTextFieldPlaceHolder.getLocation().getY();
		typeOnFields("email@gmail.com");
		float y2=emailTextFieldPlaceHolder.getLocation().getY();
		if(y2>y1)
			flag=true;
		return flag;
	}

	/**
	 * To get displayed error message
	 * @return string - error message
	 * @throws Exception - Exception
	 */
	public String getDisplayedEmailErrorMessage() throws Exception{
		typeOnFields("emailgmail");
		BrowserActions.clickOnElementX(signupButton, driver, "Entered incorrect email address and Clicked on signup");
		Utils.waitForPageLoad(driver);
		return BrowserActions.getText(driver, emailPlaceHolderIncorrectErrorText, "Error Message");

	}

	/**
	 * To verify email field error displayed
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailFieldBlankError(String errorColorCode)throws Exception {
		emailTextField.clear();
		BrowserActions.clickOnElementX(signupButton, driver, "Blank email address and Clicked on signup");
		return Utils.verifyCssPropertyForElement(emailPlaceHolderEmptyErrorText, "color", errorColorCode);
	}
	
	/**
	 * To Click on sign in to complete link 
	 * @return SignIn - page object for log in
	 * @throws Exception - Exception
	 */
	public SignIn clickSignInToComplete() throws Exception{
		if(!Utils.isDesktop()) {
			BrowserActions.clickOnElementX(lnkSignInCompleteMobile, driver, "Sign In to complete");
		}else {
			BrowserActions.clickOnElementX(lnkSignInCompleteDesktop, driver, "Sign In to complete");
		}
		return new SignIn(driver).get();
	}
	
	/**
	 * To check when clicking 'manage your card' link is navigating or not 
	 * @return boolean - true if navigating to the link, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyManageYourAccount(String expectedURL) throws Exception{
		String currentWindowHandle=driver.getWindowHandle();
		if(Utils.waitForElement(driver, lnkManageAccount)) {
			BrowserActions.clickOnElementX(lnkManageAccount, driver, "Manage Your Account");
		}else {
			BrowserActions.clickOnElementX(applyButton, driver, "Manage Your Account");
		}
		Utils.waitForPageLoad(driver);

		
		ArrayList<String> lstOpenTabs = new ArrayList<String> (driver.getWindowHandles());
		String popUpTabHandle=null;
		for(String tabHandle : lstOpenTabs)
		{
			if (!tabHandle.equals(currentWindowHandle))
				popUpTabHandle=tabHandle;
		}
		driver.switchTo().window(popUpTabHandle);

		String popUpURL = driver.getCurrentUrl();
		driver.switchTo().window(currentWindowHandle);
		return popUpURL.contains(expectedURL);
	}
	
	/**
	 * To get expand status
	 * @param index - 
	 * @return string  - expanded/collapsed
	 * @throws Exception - Exception
	 */
	public String getExpandStatusOfQuestion(int index)throws Exception{
		Utils.waitForElement(driver, divPlccFormSection);
		BrowserActions.scrollToView(divPlccFormSection, driver);
		return (divPlccFormSection.getAttribute("class").contains("active"))? "expanded":"collapsed";
	}

	/**
	 * To verify Top Banner Slot Contents in the PLCC Landing Page
	 * @return true - if PLCC banner content contains expected elements
	 * @throws Exception - Exception
	 */
	public boolean verifyTopBannerSlotContents() throws Exception {
		boolean flag=true;
		String brand = UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).toString();
		if(brand.contains("x")) {
			brand = brand.replace("x", "");
		}
		
		if(Utils.isDesktop()) {
			if(Utils.waitForElement(driver, bannercardImage)) {
				String imageNameLink = bannercardImage.findElement(By.cssSelector("img")).getAttribute("src");
				Log.event("Image name link: " + imageNameLink);
				if(! (imageNameLink.contains("banner-card-" + brand)) ) {
					Log.event("PLCC banner card Image is not displayed");
					flag= false;
				}
			}
		}
		if(Utils.waitForElement(driver, bannerBrandLogo)) {
			String imageNameLink = bannerBrandLogo.findElement(By.cssSelector("img")).getAttribute("src");
			Log.event("Logo Name Link: " + imageNameLink);
			if(! (imageNameLink.contains(brand)) ) {
				Log.event("PLCC banner Brand Logo is not displayed");
				flag= false;
			}
		}
		if(Utils.isDesktop()) {
			if(!Utils.waitForElement(driver, lnkSignInToPreApproveDesktop)) {
				Log.event("PLCC Sign InTo PreApprove? link is not displayed");
				flag= false;
			}
		} else {
			if(!Utils.waitForElement(driver, lnkSignInToPreApproveMobTab)) {
				Log.event("PLCC Sign InTo PreApprove? link is not displayed");
				flag= false;
			}
		}
		
		if(!elementLayer.verifyElementDisplayed(Arrays.asList("divBannerElementAlt", "applyButton", "lnkManageAccount"), this)) {
			flag= false;
		}
		if(flag) {
			Log.event("Expected PLCC Top Banner Slot Contents is displayed");
			return true;
		} else 
			return false;
	}

}