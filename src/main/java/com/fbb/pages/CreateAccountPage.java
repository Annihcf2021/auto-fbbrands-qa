package com.fbb.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;

public class CreateAccountPage extends LoadableComponent<CreateAccountPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	@FindBy(id = "RegistrationForm")
	WebElement frmCreateAccountForm;
	
	@FindBy(id = "dwfrm_profile_customer_firstname")
	WebElement fldFirstName;
	
	@FindBy(id = "dwfrm_profile_customer_lastname")
	WebElement fldLastName;
	
	@FindBy(id = "dwfrm_profile_customer_email")
	WebElement fldEmailAddress;
	
	@FindBy(id = "dwfrm_profile_customer_emailconfirm")
	WebElement fldConfirmEmailAddress;
	
	@FindBy(css = "input[id*='dwfrm_profile_login_password_']")
	WebElement fldPassword;
	
	@FindBy(css = "input[id*='dwfrm_profile_login_passwordconfirm_']")
	WebElement fldConfirmPassword;
	
	@FindBy(css = "button[name='dwfrm_login_register']")
	WebElement btnCreateAccount;
	
	@FindBy(css = ".server-error")
	WebElement emailExistError;
	
	@FindBy(css = ".pt_account:not(.null)")
	WebElement myAccountReadyElement;
	
	@FindBy(css = "#dwfrm_profile_customer_birthMonth")
	WebElement drpBirthMonth;
	
	@FindBy(css = "div[class='selected-option selected']")
	WebElement selectedMonth;
	
	@FindBy(css = ".form-row.birthMonth.show-text.field-valid .selection-list li:not([class='selected'])")
	List<WebElement> lstMonthSelectable;
	
	@FindBy(xpath = "//select[@id='dwfrm_profile_customer_birthMonth']//following-sibling::div[@class='selected-option selected']")
	WebElement selectedMonth1;

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public CreateAccountPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, frmCreateAccountForm))) {
			Log.fail("Header Panel didn't display", driver);
		}
		
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To fill first name
	 * @param firstName
	 * @return void
	 * @throws Exception - Exception
	 */
	public void fillFirstName(String firstName) throws Exception {
		Utils.waitForElement(driver, fldFirstName);
		fldFirstName.clear();
		fldFirstName.sendKeys(firstName);
	}
	
	/**
	 * To fill last name
	 * @param lastName
	 * @return void
	 * @throws Exception - Exception
	 */
	public void fillLastName(String lastName) throws Exception {
		Utils.waitForElement(driver, fldLastName);
		fldLastName.clear();
		fldLastName.sendKeys(lastName);
	}
	
	/**
	 * To fill email address
	 * @param userEmail
	 * @return void
	 * @throws Exception - Exception
	 */
	public void fillEmailAddress(String userEmail) throws Exception {
		Utils.waitForElement(driver, fldEmailAddress);
		fldEmailAddress.clear();
		fldEmailAddress.sendKeys(userEmail);
	}
	
	/**
	 * To fill confirm email address
	 * @param confirmEmail
	 * @return void
	 * @throws Exception - Exception
	 */
	public void fillConfirmEmailAddress(String confirmEmail) throws Exception {
		Utils.waitForElement(driver, fldConfirmEmailAddress);
		fldConfirmEmailAddress.clear();
		fldConfirmEmailAddress.sendKeys(confirmEmail);
	}
	
	/**
	 * To fill password
	 * @param password
	 * @return void
	 * @throws Exception - Exception
	 */
	public void fillPassword(String password) throws Exception {
		Utils.waitForElement(driver, fldPassword);
		fldPassword.clear();
		fldPassword.sendKeys(password);
	}
	
	/**
	 * To fill confirm password
	 * @param confirmPassword
	 * @return void
	 * @throws Exception - Exception
	 */
	public void fillConfirmPassword(String confirmPassword) throws Exception {
		Utils.waitForElement(driver, fldConfirmPassword);
		fldConfirmPassword.clear();
		fldConfirmPassword.sendKeys(confirmPassword);
	}
	
	/**
	 * To click on create account
	 * @return void
	 * @throws Exception - Exception
	 */
	public Object clickOnCreateAccout() throws Exception {
    	Utils.waitForElement(driver, btnCreateAccount);
    	BrowserActions.clickOnElement(btnCreateAccount, driver, "Create account");
    	if(Utils.waitForElement(driver, myAccountReadyElement)) {
    		if(UrlUtils.getBrandFromUrl(driver.getCurrentUrl()).toString().contains("el")) {
    			return new ProfilePage(driver).get();
    		} else {
    			return new MyAccountPage(driver).get();
    		}
    	} else {
    		return this;
    	}
    }
}
