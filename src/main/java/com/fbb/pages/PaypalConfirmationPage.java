package com.fbb.pages;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.fbb.reusablecomponents.StateUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;


public class PaypalConfirmationPage extends LoadableComponent<PaypalConfirmationPage> {
	
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(css = ".clearfix")
	WebElement readyElement;
	
	@FindBy(css = ".preloader.spinner .loader")
	WebElement loadingSpinner;
	
	@FindBy(css = "#root div[class*='components_pageWrapper']")
	WebElement paypalConfimationContent;
	
	@FindBy(css = "#payment-submit-btn")
	WebElement paypalContinueButton;
	
	@FindBy(css = ".buttons.reviewButton button")
	WebElement paypalContinueFromPayments;
	
	@FindBy(css = "div[class*='ShipTo_header'] span[class*='AddOption_textContent']")
	WebElement changeAddress;
	
	@FindBy(xpath = "//div[@id='shippingAddress'] /a[contains(text(),'Change')] /span")
	WebElement changeAddress1;
	 
	 @FindBy(css = "section[class*='ShipTo_container'] [class*='AddOption_container'] [class*=Link]")
	 WebElement addAddress;
	 
	 @FindBy(css = "#ccFirstName")
	 WebElement shippingFirstName;
	 
	 @FindBy(css = "#ccLastName")
	 WebElement shippingLastName;
	 
	 @FindBy(css = "#line1")
	 WebElement shippingLine1;
	 
	 @FindBy(css = "#city")
	 WebElement shippingCity;
	 
	 @FindBy(css = "#state")
	 WebElement  shippingState;
	 
	 @FindBy(css = "#postalCode")
	 WebElement shippingPostalCode;
	 
	 @FindBy(css = "div[class*=Interstitial] button[class='ppvx_btn']")
	 WebElement shippingAddressSubmit;
	 
	 @FindBy(css = ".loader-bg")
	 WebElement waitLoader;

	 
	//================================================================================
	//			WebElements Declaration End
	//================================================================================

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public PaypalConfirmationPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, paypalConfimationContent))) {
			Log.fail("PaypalConfirmation page is not loaded", driver);
		}
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}
	
	public boolean getPageLoadStatus()throws Exception{
		
		return isPageLoaded;
	}
	
	/**
	 * To click continue confirmation button, popup handle and get CheckoutPage
	 * @return CheckoutPage
	 * @throws Exception - Exception 
	 */
	public CheckoutPage clickContinueConfirmation()throws Exception{
		if(Utils.waitForElement(driver, paypalContinueFromPayments)){
			BrowserActions.scrollInToView(paypalContinueFromPayments, driver);
			BrowserActions.clickOnElementX(paypalContinueFromPayments, driver, "Paypal Continue Buttons from select payment method");
		}
		Utils.waitForElement(driver, paypalContinueButton, 10);
		BrowserActions.scrollInToView(paypalContinueButton, driver);
		BrowserActions.clickOnElementX(paypalContinueButton, driver, "Paypal Continue Buttons");
		if(driver.getWindowHandles().size() > 1){
			driver = Utils.switchWindows(driver, Utils.getWebSite(), "url", "false");
		}
		Utils.waitForPageLoad(driver);
		return new CheckoutPage(driver).get();
	}

	public void addNewPaypalAddress(String addressPaypal) {
		try {
			Utils.waitForElement(driver, changeAddress);
			BrowserActions.clickOnElementX(changeAddress, driver, "Change address button");
			Utils.waitUntilElementDisappear(driver, waitLoader) ;
			Utils.waitForElement(driver, addAddress);
			BrowserActions.clickOnElementX(addAddress, driver, "Add button");
			
			String address = checkoutProperty.getProperty(addressPaypal);
			
			String firstName = address.split("\\|")[7];
			String lastName = address.split("\\|")[8];
			String zipcode = address.split("\\|")[4];
			String city = address.split("\\|")[2];
			String state = address.split("\\|")[3];
			String streetAddress= address.split("\\|")[0];

			shippingFirstName.sendKeys(firstName);
			shippingLastName.sendKeys(lastName);
			shippingLine1.sendKeys(streetAddress);
			shippingCity.sendKeys(city);
			shippingPostalCode.sendKeys(zipcode);
			
			Select selectState = new Select(shippingState);
			selectState.selectByVisibleText(state);
			Log.message("Entered given address", driver);
			
			BrowserActions.clickOnElementX(shippingAddressSubmit, driver, "Save address button");
		} catch(Exception e) {
			Log.event("Element not found");
		}
	}

	public void addNewPaypalAddressADS(HashMap<String, String> addressData) {
		try {
			Utils.waitForElement(driver, changeAddress);
			BrowserActions.clickOnElementX(changeAddress, driver, "Change address button");
			Utils.waitUntilElementDisappear(driver, waitLoader);
			Utils.waitForElement(driver, addAddress);
			BrowserActions.clickOnElementX(addAddress, driver, "Add button");

			shippingFirstName.sendKeys(addressData.get("firstname"));
			shippingLastName.sendKeys(addressData.get("lastname"));
			shippingLine1.sendKeys(addressData.get("streetnumber") + " " 
						+ addressData.get("streetname") + " "
						+ addressData.get("streettype"));
			shippingCity.sendKeys(addressData.get("city"));
			shippingPostalCode.sendKeys(addressData.get("zipcode"));

			Select selectState = new Select(shippingState);
			selectState.selectByVisibleText(StateUtils.getStateName(addressData.get("state")));
			Log.message("Entered given address", driver);

			BrowserActions.clickOnElementX(shippingAddressSubmit, driver, "Save address button");

		} catch (Exception e) {
			Log.event("Element not found");
		}
	}
	
}
