package com.fbb.pages;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.reusablecomponents.BillingPageUtils;
import com.fbb.reusablecomponents.ShippingPageUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StopWatch;
import com.fbb.support.Utils;

public class iParcelCheckoutPage extends LoadableComponent<iParcelCheckoutPage> {
	private final WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	
	//***********************************************************
	//  	WebElements
	//***********************************************************
	
	private static final String txtShippingFirstName = "#shipping_first_name";
	private static final String txtShippingLastName = "#shipping_last_name";
	private static final String txtShippingAddress = "#shipping_address1";
	private static final String txtShippingAddress2 = "#shipping_address2";
	private static final String txtShippingCity = "#shipping_city";
	private static final String txtShippingZipcode = "#shipping_zip";
	private static final String txtShippingPhoneNo = "#shipping_phone";
	private static final String txtShippingState = "#shipping_state";
	private static final String txtShippingControlNo = "#control_number";
	private static final String txtEmailAddr = "#shipping_email";
	
	private static final String txtCreditCardNumber = "#CreditCardNumber";
	private static final String txtCreditCardName = "#Name";
	private static final String selectCreditCardExpMonth = "#ExpMonth";
	private static final String selectCreditCardExpYear = "#ExpYear";
	private static final String txtCreditCardCvv = "#CVV";
	
	
	@FindBy(css = ".container")
	WebElement readyElement;
	
	@FindBy(css = txtShippingFirstName)
	WebElement inpFirstName;
	
	@FindBy(css = txtShippingLastName)
	WebElement inpLastName;
	
	@FindBy(css = txtShippingControlNo)
	WebElement inpCntrlNumber;
	
	@FindBy(css = txtEmailAddr)
	WebElement inpEmail;
	
	@FindBy(css = txtShippingAddress)
	WebElement inpAddress1;
	
	@FindBy(css = txtShippingAddress2)
	WebElement inpAddress2;
	
	@FindBy(css = txtShippingCity)
	WebElement inpCity;
	
	@FindBy(css = txtShippingState)
	WebElement inpState;
	
	@FindBy(css = txtShippingZipcode)
	WebElement inpZip;
	
	@FindBy(css = txtShippingPhoneNo)
	WebElement inpPhone;
	
	@FindBy(css = txtCreditCardNumber)
	WebElement inpCCNumber;
	
	@FindBy(css = txtCreditCardName)
	WebElement inpCCName;
	
	@FindBy(css = selectCreditCardExpMonth)
	WebElement inpCCExpMonth;
	
	@FindBy(css = selectCreditCardExpYear)
	WebElement inpCCExpYear;
	
	@FindBy(css = txtCreditCardCvv)
	WebElement inpCCCvv;
	
	@FindBy(css = "#SubmitButton")
	WebElement btnSubmitButton;
	
	@FindBy(css = "#UpdateDataBottom")
	WebElement btnUpdateButton;
	
	@FindBy(css = "#AcceptTerms")
	WebElement radioTermsAndCond;
	
	@FindBy(css = ".products.section .item  tbody td.half")
	List<WebElement> productNames;
	
	//***********************************************************
	//  	Methods
	//***********************************************************
	
	/**
	 * Constructor for Guest Order Track
	 */
	public iParcelCheckoutPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("iParcelCheckout page didn't open up", driver);
		}

		elementLayer = new ElementLayer(driver);
	}
	
	/**
	 * To get page load status
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}
	
	/**
	 * To enter shipping address as guest user
	 * @param useAddressForShipping -
	 * @param shippingmethod -
	 * @return HashMapvalues
	 * @throws Exception -
	 */
	public LinkedHashMap<String, String> fillingShippingDetails(String useAddressForShipping) throws Exception {
		
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		String randomFirstName = RandomStringUtils.randomAlphabetic(5).toLowerCase();
		shippingDetails.put("type_firstname_First" + randomFirstName, txtShippingFirstName);
		String randomLastName = RandomStringUtils.randomAlphabetic(5) .toLowerCase();
		shippingDetails.put("type_lastname_Last" + randomLastName, txtShippingLastName);
		
		String randomEmail = RandomStringUtils.randomAlphabetic(5).toLowerCase() + RandomStringUtils.randomAlphabetic(5).toLowerCase()+"@yopmail.com";
		shippingDetails.put("type_lastname_Last" + randomLastName, txtShippingLastName);
		
		String address = checkoutProperty.getProperty(useAddressForShipping);
		String address2 = address.split("\\|")[1];
		String zipcode = address.split("\\|")[4];
		String city = address.split("\\|")[2];
		String state = address.split("\\|")[3];
		String phoneNo = address.split("\\|")[5];
		String address1 = address.split("\\|")[0];
		
		if(address.split("\\|").length > 7) {
			String guestFirstName = address.split("\\|")[7];
			String guestLastName = address.split("\\|")[8];
			shippingDetails.put("type_firstname_" + guestFirstName, txtShippingFirstName);
			shippingDetails.put("type_lastname_" + guestLastName, txtShippingLastName);
		}

		if (useAddressForShipping.contains("_1")) {
			address2 = address.split("\\|")[1];
			zipcode = address.split("\\|")[4];
			city = address.split("\\|")[2];
			state = address.split("\\|")[3];			
			phoneNo = address.split("\\|")[5];
		}
		String taxId = address.split("\\|")[9];
		
		shippingDetails.put("type_zipcode_" + zipcode, txtShippingZipcode);
		shippingDetails.put("type_phoneno_" + phoneNo, txtShippingPhoneNo);
		shippingDetails.put("type_state_" + state, txtShippingState);	
		shippingDetails.put("type_city_" + city, txtShippingCity);			
		shippingDetails.put("type_email_" + randomEmail, txtEmailAddr);
		shippingDetails.put("type_controlno_" + taxId, txtShippingControlNo);
		shippingDetails.put("type_address_" + address1, txtShippingAddress);
		shippingDetails.put("type_address1_" + address2, txtShippingAddress2);
		
		ShippingPageUtils.enterShippingDetails(shippingDetails, driver);
		Utils.waitForPageLoad(driver);

		return shippingDetails;
	}
	
	/**
	 * To enter shipping address as guest user
	 * @param useAddressForShipping -
	 * @param shippingmethod -
	 * @return HashMapvalues
	 * @throws Exception -
	 */
	public LinkedHashMap<String, String> fillingCardDetails(String customerDetails) throws Exception {

		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> cardDetails = new LinkedHashMap<String, String>();
		String cardExpireMonth = "";
		String cardExpireYear = "";
		String cardCVN = "";

		String cutomercardDetails = checkoutProperty.getProperty(customerDetails);
		
		String cardOwner = cutomercardDetails.split("\\|")[0];
		String cardNo = cutomercardDetails.split("\\|")[1];
		cardExpireMonth = cutomercardDetails.split("\\|")[2];
		cardExpireYear = cutomercardDetails.split("\\|")[3];
		cardCVN = cutomercardDetails.split("\\|")[4];
		
		cardDetails.put("type_cardno_" + cardNo, txtCreditCardNumber);
		cardDetails.put("type_cardname_" + cardOwner, txtCreditCardName);
		cardDetails.put("select2_expmonth_" + cardExpireMonth, selectCreditCardExpMonth);
		cardDetails.put("select2_expyr_" + cardExpireYear, selectCreditCardExpYear);
		cardDetails.put("type_cvnno_" + cardCVN, txtCreditCardCvv);
		
		BillingPageUtils.enterBillingDetails(cardDetails, driver);
		Log.event("Filled Card Details", StopWatch.elapsedTime(startTime));
		return cardDetails;
	}
	
	/**
	 * To click on the submit button in the i-Parcel checkout page
	 * @return
	 * @throws Exception
	 */
	public OrderConfirmationPage clickOnContinueButtonAfterEnteringShippingAndCardDetails() throws Exception {
		Utils.waitForElement(driver, btnSubmitButton);
		BrowserActions.clickOnElementX(btnSubmitButton, driver, "Click the Shipping Overlay Tool Tip");
		Utils.waitForPageLoad(driver);
		return new OrderConfirmationPage(driver).get();
	}
	
	/**
	 * To check/uncheck the Terms And Condition checkbox in the i-Parcel checkout page
	 * @param Boolean check - Radio button selected if true
	 * @throws Exception
	 */
	public void selectUnSelectTermsAndCondition(boolean check)throws Exception{
		if(radioTermsAndCond.isSelected()) {
			if (!check) {
				BrowserActions.clickOnElementX(radioTermsAndCond, driver, "Click the Shipping Overlay Tool Tip");
			}
		} else {
			if (check) {
				BrowserActions.clickOnElementX(radioTermsAndCond, driver, "Click the Shipping Overlay Tool Tip");
			}
		}
		
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get product name in Checkout page
	 * @return List<String> Product Name=Product IDs 
	 * @throws Exception - Exception
	 */
	public List<String> getPrdListNames() throws Exception{
		List<String> ProductId = new ArrayList<String> ();		
		for(int i=0; i < productNames.size(); i++) {
			WebElement prdId = productNames.get(i);
			ProductId.add(BrowserActions.getText(driver, prdId, "Product name").toLowerCase().split("\\~")[0]);
		}		
		return ProductId;
	}

	/**
	 * To click on the update button in i-Parcel checkout page
	 * @throws Exception
	 */
	public void selectUpdateButton() throws Exception {
		Utils.waitForElement(driver, btnUpdateButton);
		BrowserActions.clickOnElementX(btnUpdateButton, driver, "Click the update button");
		Utils.waitForPageLoad(driver);
	}
	
}
