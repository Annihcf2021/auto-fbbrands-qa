package com.fbb.pages.customerservice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class ShoppingPage extends LoadableComponent<ShoppingPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public CSNavigation csNav;
	
	/**********************************************************************************************
	 ********************************* WebElements of ShoppingPage ****************************
	 **********************************************************************************************/
	
	@FindBy(css = ".pt_article-page")
	WebElement readyElement;
	
	@FindBy(css = "div[class*='pt_article-page account-shopping-']" )
	WebElement lnkNavShopping;
	
	@FindBy(css = ".secondary-left-section.active")
	WebElement leftNavigationContentMobile;
	
	@FindBy(css = ".secondary-aticle-content .secondary-article-heading.mobile-nav")
	WebElement MenuMobile;
	
	@FindBy(css = ".secondary-article-heading.mobile-nav.active")
	WebElement drpDownLeftNavActiveMobile;
	
	@FindBy(css = ".article-heading")
	WebElement csCurrentHeading;
	
	@FindBy(css = ".inner-service-landing-contacts")
	WebElement contactUsSection;

	@FindBy(css = ".primary-content")
	WebElement primarycontent;

	@FindBy(css = ".currentpage")
	WebElement currentPage;

	@FindBy(css = ".article-heading")
	WebElement articleHeading;

	@FindBy(css = ".article-body")
	WebElement articleBody;
	
	
	/**********************************************************************************************
	 ********************************* WebElements of ShoppingPage ****************************
	 **********************************************************************************************/	
	
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public ShoppingPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
		csNav = new CSNavigation(driver).get();
	}
	
	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Contact Us Page did not open up.", driver);
		}
	}
	
	/**
	 * To expand/collapse the left navigation dropdown
	 * @param isExpand - True if menu needed to expand else false
	 * @throws Exception - Exception
	 */
	public void expandOrCollapseLeftNavigationMenu(boolean isExpand)throws Exception{
		if(!Utils.isDesktop()) {
			if(isExpand) {
				if (!(Utils.waitForElement(driver, drpDownLeftNavActiveMobile))) { 
					BrowserActions.clickOnElementX(MenuMobile, driver, "Expanded status of the Elements");
					Utils.waitForElement(driver, drpDownLeftNavActiveMobile);
				}
			} else {
				if (Utils.waitForElement(driver, drpDownLeftNavActiveMobile)) { 
					BrowserActions.clickOnElementX(MenuMobile, driver, "Expanded status of the Elements");
					Utils.waitUntilElementDisappear(driver, drpDownLeftNavActiveMobile);
				}
			}
		}
	}
}
