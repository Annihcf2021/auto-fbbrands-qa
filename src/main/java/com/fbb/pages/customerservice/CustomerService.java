package com.fbb.pages.customerservice;


import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.pages.account.PasswordResetPage;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class CustomerService extends LoadableComponent<CustomerService> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	String runBrowser;


	/**********************************************************************************************
	 ********************************* WebElements of Customer Service page ****************************
	 **********************************************************************************************/

	@FindBy(css = ".page-content.clearfix")
	WebElement readyElement;

	@FindBy(css = ".customerservice-name")
	WebElement lblCustomerSupport;

	@FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
	WebElement breadcrumCustomerService;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop span")
	WebElement breadcrumMobile;

	@FindBy(css = ".breadcrumb-element.hide-mobile")
	List<WebElement> breadcrumHomeHyperLink;
	
	@FindBy(css = ".breadcrumb")
	WebElement sectionBreadcrum;

	@FindBy(xpath = ".//span[contains(text(),'Order status')]//ancestor::a ")
	WebElement tileOrderStatus;

	@FindBy(css = ".article-heading")
	WebElement txtOrderStatus;
	
	@FindBy(css = ".promo-banner")
	WebElement txtPromoBanner;
	
	@FindBy(css = ".customerservice-name")
	WebElement lblCustomerServiceName;

	@FindBy(css = ".page-content clearfix")
	WebElement articlePage;

	@FindBy(css = ".customer-service")
	WebElement readyElement1;

	@FindBy(css = ".customerservice-assest-link .title")
	List<WebElement> lstTitleOfTiles;
	
	@FindBy(css = ".hovered-text ul")
	WebElement lnkHoverTile;

	@FindBy(css = ".inner-asset-info")
	WebElement TilesElements;
	
	@FindBy(xpath = "//div[@class='content-asset-info'][1]")
	WebElement tile1;
	
	@FindBy(xpath = "//div[@class='content-asset-info'][2]")
	WebElement tile2;
	
	@FindBy(xpath = "//div[@class='content-asset-info'][4]")
	WebElement tile4;

	@FindBy(xpath = "//span[contains(text(),'Order status')]//ancestor::a")
	WebElement OrderStatus; 

	@FindBy(css = ".media-contacts")
	WebElement lnkCallUs;
	
	@FindBy(css = ".chat-contact")
	WebElement lnkChatWithUs;
	
	@FindBy(css = ".chat-text-available")
	WebElement liveChatAvailableLnk;
	
	@FindBy(css = ".chat-text-unavailable")
	WebElement liveChatNotAvailableLnk;
	
	@FindBy(css = "#bc-chat-container.bc-draggable-mobile-chat-windows")
	WebElement mdlLiveChat;
	
	@FindBy(css = ".mail-contact a")
	WebElement lnkMailWithUs;
	
	@FindBy(css = ".customer-service-faq div.view-all-faq>a")
	WebElement btnViewAllFaq;

	@FindBy(css = ".question")
	WebElement faqQuestion;
	
	@FindBy(css = ".qa-section")
	WebElement faqSectionHeading;

	@FindBy(css = ".question")
	List<WebElement> faqQuestionList;
	
	@FindBy(xpath = "//a[contains(text(),'I FORGOT MY PASSWORD')]")
	WebElement forgetPasswordQues;

	@FindBy(css = ".question.active")
	WebElement faqQuestionActive;
	
	@FindBy(css = ".inner-service-landing-contacts")
	WebElement contactLandingSection;

	@FindBy(xpath = "//a[contains(text(),'Reset password')]")
	WebElement forgotPwdLnk;

	@FindBy(css = ".breadcrumb")
	WebElement mobileHome;
	
	@FindBy(css = ".tiles-section")
	WebElement tileSection;
	
	@FindBy(xpath = "//div[@class='bc-input-container']//input[@placeholder='First Name ']")
	WebElement liveChatFirstName;
	
	@FindBy(xpath = "//div[@class='bc-input-container']//input[@placeholder='Last Name ']")
	WebElement liveChatLastName;
	
	@FindBy(xpath = "//div[@class='bc-input-container']//input[@placeholder='Email *']")
	WebElement liveChatEmail;
	
	@FindBy(css = ".bc-input-container #rdid")
	WebElement liveChatCategory;
	
	@FindBy(xpath = "//div[@class='bc-input-container']//input[@placeholder='Confirmation/Order # ( if available) ']")
	WebElement liveChatOrderNumber;
	
	@FindBy(xpath = "//div[@class='bc-input-container']//textarea[@placeholder='Initial Question *']")
	WebElement liveChatInitialQuestion;
	
	@FindBy(css = "#bc-prechat-form-container .bc-input-container.bc-action-input-container")
	WebElement liveChatStart;
	
	@FindBy(css = ".bc-bottom-wrap #bc-message-input-text")
	WebElement liveChatMessageBox;
	
	@FindBy(css = ".bc-bottom-wrap .bc-sendbutton-wrap")
	WebElement liveChatSendButton;
	
	@FindBy(css = "#bc-chat-container .bc-headbtn-menulist")
	WebElement btnLiveChatMenu;
	
	@FindBy(css = "#bc-chat-container .bc-headmenu")
	WebElement divLiveChatMenubar;
	
	@FindBy(css = "#bc-chat-container .bc-headbtn-close-icon")
	WebElement btnLiveChatClose;
	
	@FindBy(css = ".bc-close-chatwindow")
	WebElement liveChatClose;
	
	@FindBy(name = "bc-window")
	WebElement liveChatFrameElement;
	/**********************************************************************************************
	 ********************************* WebElements of Customer Service page ****************************
	 **********************************************************************************************/	


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public CustomerService(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
		runBrowser  = Utils.getRunBrowser(driver);
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Customer Service Page did not open up.", driver);
		}
	}

	/**
	 * To navigate To Home Page
	 * @return HomePage
	 * @throws Exception - Exception
	 */
	public HomePage navigateToHomePage()throws Exception{
		BrowserActions.clickOnElementX(breadcrumHomeHyperLink.get(0), driver, "Customer Serivice Page");
		Utils.waitForPageLoad(driver);
		return new HomePage(driver).get();

	}

	/**
	 * To get the List of article header values in Cusomter Service page 
	 * @return List value
	 * @throws Exception - 
	 */
	public List<String> getArticleTileHeaders()throws Exception{

		List<String> dataToReturn = new ArrayList<String>();
		for(int i =0;i<lstTitleOfTiles.size();i++)
			dataToReturn.add(lstTitleOfTiles.get(i).getText().toLowerCase());

		return dataToReturn;
	}

	/**
	 * To Navigate to Order Status Page
	 * @return OrderStatusPage
	 * @throws Exception - Exception
	 */
	public OrderStatusPage navigateToOrderStatusPage()throws Exception{
		BrowserActions.clickOnElementX(OrderStatus, driver, "Order Status Tile link");
		Utils.waitForPageLoad(driver);
		return new OrderStatusPage(driver).get();

	}

	/**
	 * To navigate to Article Page
	 * @return CustomerServicePage
	 * @throws Exception - Exception
	 */
	public CustomerService navigateToArticlePage()throws Exception{

		BrowserActions.clickOnElementX(TilesElements, driver, "Action Page");
		return new CustomerService(driver).get();

	}
	
	
	
	/**
	 * To click on View All Faq
	 * @return FaqPage - Faq Page object
	 * @throws Exception - Exception
	 */
	public FaqPage clickOnViewAllFaq()throws Exception{

		BrowserActions.clickOnElementX(btnViewAllFaq, driver, "Action Page");
		return new FaqPage(driver).get();

	}

	/**
	 * To navigate to Order Status Page
	 * @return OrderStatusPage
	 * @throws Exception - Exception
	 */
	public OrderStatusPage navigateToOrderStatus()throws Exception{
		BrowserActions.clickOnElementX(tileOrderStatus, driver, "order status block ");
		Utils.waitForPageLoad(driver);
		return new OrderStatusPage(driver).get();
	} 

	/**
	 * To get count of articles tiles in a row
	 * @return int
	 * @throws Exception - Exception
	 */
	public int getNumberOfArticleTilesPerRow()throws Exception{
		List<WebElement> productTiles = driver.findElements(By.cssSelector(".inner-asset-info"));
		int rowCount = 1;	
		for (int i = 0; i <= productTiles.size(); i++) {
			if(BrowserActions.verifyElementsAreInSameRow(driver, productTiles.get(i), productTiles.get(i+1))) {
				rowCount++;
			}
			else
				break;
		}
		return rowCount;
	}

	/**
	 * To click on FAQ Question header
	 * @throws Exception - Exception
	 */
	public void clickOnFaqQuestion()throws Exception{		
		BrowserActions.clickOnElementX(faqQuestion, driver, "Action Page");
		Utils.waitForPageLoad(driver);		
	}
	
	/**
	 * To collapse the active FAQ Quesions section
	 * @throws Exception - Exception
	 */
	public void clickOnFaqQuestionActive()throws Exception{	
		BrowserActions.clickOnElementX(faqQuestionActive, driver, "Action Page");
		Utils.waitForPageLoad(driver);	
	}

	/**
	 * To verify Carat Symbol in FQA Question Heading
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public boolean verifyCartsymbol()throws Exception
	{	
		System.out.println(faqQuestion.getCssValue("background"));
		if(faqQuestion.getCssValue("background-image").contains("/images/arrow-down.png"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * To click on Forgot Password link from FAQ Questions list
	 * @param index - 
	 * @throws Exception - Exception
	 */
	public void clickOnForgotMyPassword()throws Exception{	
		BrowserActions.clickOnElementX(forgetPasswordQues, driver, "Forgot my Password");
	}
	
	/**
	 * To click on MailWithUs link
	 * @return ContactUsPage - Contact us page object
	 * @throws Exception - Exception
	 */
	public ContactUsPage clickOnMailWithUs()throws Exception{	
		BrowserActions.clickOnElementX(lnkMailWithUs, driver, "Mail With Us");
		Utils.waitForPageLoad(driver);
		return new ContactUsPage(driver).get();
	}
	

	/**
	 * To navigate to Password Reset page by clicking on Forgot Password Link
	 * @return PasswordResetPage
	 * @throws Exception - Exception
	 */
	public PasswordResetPage clickOnForgotPwdLnk()throws Exception{	
		BrowserActions.clickOnElementX(forgotPwdLnk, driver, "Forgot my Password");
		return new PasswordResetPage(driver).get();
	}

	/**
	 * To verify breadcrumn
	 * @param checkString -
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyCSBreadCrumb(String checkString)throws Exception{
		String bcString = new String();
		
		for(WebElement ele : breadcrumHomeHyperLink) {
			bcString += ele.getAttribute("innerHTML").trim();
		}
		
		Log.event("Text From user :: " + checkString);
		Log.event("Text From WebElement :: " + bcString);
		
		return (checkString.equals(bcString)) ? true : false;
	}
	
	/**
	 * To click on Home in breadcrumb 
	 * @return HomePage - Home page object
	 * @throws Exception - Exception
	 */
	public HomePage clickOnHomeInBC()throws Exception{
		if(Utils.isMobile()) {
			BrowserActions.clickOnElementX(breadcrumMobile, driver, "Home link in BC");
		} else {
			BrowserActions.clickOnElementX(breadcrumHomeHyperLink.get(0), driver, "Home link in BC");
		}
		return new HomePage(driver).get();
	}
	
	/**
	 * To mouse hover on index-th tile
	 * @param index -
	 * @throws Exception -
	 */
	public void mouseHoverOnTile(int index)throws Exception{
		if((lstTitleOfTiles.size() > index - 1) && !(index < 1)) {
			WebElement ele = lstTitleOfTiles.get(index - 1);
			BrowserActions.mouseHover(driver, ele);
		} else {
			Log.failsoft("The size of the tiles is "+ lstTitleOfTiles.size() + " give input from [1 to " + lstTitleOfTiles.size() +"]");
		}
	}

	/**
	 * To click on the Live chat link from customer service page.
	 * @throws Exception
	 */
	public void clickOnLiveChatLink() throws Exception {
			try {
				Utils.waitForElement(driver, liveChatAvailableLnk);
				BrowserActions.clickOnElementX(liveChatAvailableLnk, driver, "Live Chat ");
			} catch (Exception e) {
				if(Utils.waitForElement(driver, liveChatNotAvailableLnk)){
					Log.failsoft("Live chat link is not displayed", driver);
				} else {
					Log.failsoft("Live chat section is not displayed", driver);
				}
			}
			if(Utils.waitForElement(driver, mdlLiveChat))
				Log.event("live chat model displayed");
	}

	/**
	 * TO register user in LiveChat window to initiate chat.
	 * @throws Exception - Exception
	 */
	public void registerInLiveChat() throws Exception {
		
		List<WebElement> iFrames = driver.findElements(By.tagName("iframe"));
		int size = iFrames.size();
		for(int i=0;i<size;i++) {
			try {
				if(iFrames.get(i).getAttribute("name").equalsIgnoreCase("bc-window")) {
					driver.switchTo().frame(i);
					break;
				}
			} catch(NoSuchElementException e) {
				Log.event("Expected element not available in this iframe ");
			}
		}
			BrowserActions.typeOnTextField(liveChatFirstName, "Hello", driver, "FirstName");
			Log.event("FirstName clicked");
			
			BrowserActions.typeOnTextField(liveChatLastName, "welcome", driver, "Last Name ");
			Log.event("Last Name  clicked");
			
			BrowserActions.typeOnTextField(liveChatEmail, "newuser@mail.com", driver, "Chat Email");
			Log.event("Chat Email clicked");
			
			BrowserActions.selectDropdownByIndex(liveChatCategory, 1);
			Log.event("Category clicked");
			
			BrowserActions.typeOnTextField(liveChatOrderNumber, "4600001234", driver, "order number ");
			Log.event("order number clicked");
			
			BrowserActions.typeOnTextField(liveChatInitialQuestion, "Hi", driver, "Initiate message");
			Log.event("Initiate message clicked");
			
			BrowserActions.clickOnElement(liveChatStart, driver, "Start chat");	
			Log.event("Clicked on start chat button");
			
			if(Utils.waitForElement(driver, liveChatClose)) {
				Log.message("No operators are available at the moment", driver);
			} else {
				Log.message("operators are available at the moment", driver);
			}	
	}

	/**
	 * TO Close live chat modal
	 * @throws Exception - Exception
	 */
	public void closeLiveChat() throws Exception {
		if(Utils.waitForElement(driver, mdlLiveChat)) {
			BrowserActions.clickOnElementX(btnLiveChatMenu, driver, "Chat Menu Icon");
			Utils.waitForElement(driver, divLiveChatMenubar, 5);
			Log.message("Clicked on Menu button", driver);
			BrowserActions.javascriptClick(btnLiveChatClose, driver, "Chat Close Icon");
			Log.message("Clicked on Close button", driver);
			
			if(Utils.waitForElement(driver, btnLiveChatClose)) {
				BrowserActions.clickOnElement(btnLiveChatClose, driver, "chat modal close button");
			}
			
			Utils.waitUntilElementDisappear(driver, mdlLiveChat, 10);
		}else {
			Log.event("Chat Not Present");
		}
	}
	
	/**
	 * To get live chat status modal status as
	 * @return true - opened, false - closed
	 * @throws Exception - 
	 */
	public boolean getLiveChatStatus()throws Exception {
		if(Utils.waitForElement(driver, liveChatAvailableLnk)) {
			return true;
		} else {
			return false;
		}
	}
}



