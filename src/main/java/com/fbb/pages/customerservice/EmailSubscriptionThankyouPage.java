package com.fbb.pages.customerservice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class EmailSubscriptionThankyouPage extends LoadableComponent <EmailSubscriptionThankyouPage>{
	
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	/**********************************************************************************************
	 ********************************* WebElements of EmailSubscriptionThankyouPage Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = ".pt_account.null")
	WebElement readyElement;
	
	@FindBy(css = "div[id='primary']")
	WebElement YoutubeContent;
	
	@FindBy(css = ".email-thankyou .html-slot-container h1.offer ")
	WebElement thankyouMessage;
	
	/**********************************************************************************************
	 ********************************* WebElements of PDP EmailSubscriptionThankyouPage - Ends ****************************
	 **********************************************************************************************/

	

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public EmailSubscriptionThankyouPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, thankyouMessage))) {
			Log.fail("Email subscription thankyou page did not open up. Site might be down.", driver);
		}

	}// isLoaded
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver,YoutubeContent );
		
	}



}
