package com.fbb.pages.customerservice;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class ContactPriority extends LoadableComponent<ContactPriority> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	String runBrowser;


	/**********************************************************************************************
	 ********************************* WebElements of Customer Service page ****************************
	 **********************************************************************************************/

	@FindBy(css = "#main")
	WebElement readyElement;
	
	@FindBy(css = ".customerservice-name.hide-tablet.hide-mobile")
	WebElement lblCustomerService_Desktop;
	
	@FindBy(css = ".customerservice-name.hide-desktop")
	WebElement lblCustomerService_MobileTablet;
	
	@FindBy(css = ".contact-us-page>h1")
	WebElement lblContactPriority;
	
	@FindBy(css = ".form-wrapper .text-content")
	WebElement lblSubHeadingRM;
	
	@FindBy(css = ".contact-us-page>.text-content")
	WebElement lblSubHeading;
	
	@FindBy(css = ".form-horizontal fieldset")
	WebElement sectionContactPriorityForm;
	
	@FindBy(css = ".form-horizontal .form-row.email .input-text.email")
	WebElement txtEmailId;
	
	@FindBy(css = ".form-horizontal .form-row.message .input-textarea")
	WebElement txtMessage;
	
	@FindBy(css = ".form-horizontal .form-row.message .error")
	WebElement txtMessageError;
	
	@FindBy(css = ".form-horizontal .form-row.email .label-text")
	WebElement lblEmailIdPlaceHolder;
	
	@FindBy(css = ".form-horizontal .form-row.message .label-text")
	WebElement lblMessagePlaceHolder;
	
	@FindBy(css = ".form-horizontal .email .error")
	WebElement txtEmailIdError;
	
	@FindBy(css = "button#contactUsPrioritySubmit")
	WebElement btnContactPrioritySubmit;
	
	@FindBy(css = ".form-wrapper .form-row-button")
	WebElement sectionContactPrioritySubmitBtn;
	
	@FindBy(css = ".contactpage-chat-module")
	WebElement btnContactPageModule;
	
	@FindBy(css = ".contact-us-priority .form-wrapper")
	WebElement btnContactForm;
	
	/**********************************************************************************************
	 ********************************* WebElements of Customer Service page ****************************
	 **********************************************************************************************/	


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public ContactPriority(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
		runBrowser  = Utils.getRunBrowser(driver);
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Contact Priority Page did not open up.", driver);
		}
	}

	
	/**
	 * To type email Id in the email address field
	 * @param email
	 * @throws Exception
	 */
	public void enterEmailAddress(String email) throws Exception {
		BrowserActions.clearTextField(txtEmailId, "Email Id");
		BrowserActions.typeOnTextField(txtEmailId, email, driver, "Email Id");
	}
	
	/**
	 * To type message in the message text area
	 * @param message
	 * @throws Exception
	 */
	public void enterMessage(String message) throws Exception {
		BrowserActions.clearTextField(txtMessage, "Message");
		BrowserActions.typeOnTextField(txtMessage, message, driver, "Message");
	}
	
	/**
	 * To click the submit button in contact priority form
	 * @throws Exception
	 */
	public void clickOnSubmitButton() throws Exception {
		BrowserActions.clickOnElementX(btnContactPrioritySubmit, driver, "Contact Priority Submit");
		Utils.waitForPageLoad(driver);
	}
	
}



