package com.fbb.pages.customerservice;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class EmailSubscriptionGuest extends LoadableComponent <EmailSubscriptionGuest>{


	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	/**********************************************************************************************
	 ********************************* WebElements of EmailSubscriptionGuest Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = ".pt_account.null")
	WebElement readyElement;

	@FindBy(css = ".breadcrumb .breadcrumb-element.hide-mobile")
	WebElement breadcrumHome;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop")
	WebElement breadcrumbHomeMobile;

	@FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
	WebElement breadcrumEmailSignup;

	@FindBy(css = ".breadcrumb")
	WebElement breadcrumText;

	@FindBy(css = ".email-banner .email-preference-banner")
	WebElement bannerElement;

	@FindBy(css = ".subcription-content .content-asset")
	WebElement emailBenefitsCopy;

	@FindBy(css = ".preference-options.clearboth")
	WebElement pageElement;

	@FindBy(css = ".preference-options.clearboth label .input-checkbox")
	List<WebElement> preferenceOption;
	
	@FindBy (css = ".custom-slider-checkbox .slider.round")
	List<WebElement> preferenceSlider;

	@FindBy(css = ".preference-options.clearboth .email-ligal.clearboth")
	WebElement legalCopy;

	@FindBy(css = ".row-2.subscribe-emailid input.email-box")
	WebElement emailTextBox1;

	@FindBy(css = ".form-row.form-row-button button.emailsubscribeguest.solid-wine-berry.remove-left-top-border")
	WebElement subscribeButton;

	@FindBy(css = ".pt_account .make-label-absolute .form-row.error-handle label span.error")
	WebElement emptyEmailErrorMessage;

	@FindBy(css = "//input[@placeholder='Enter your email address']")
	WebElement emailPlaceholder;

	@FindBy(css = ".qa-section .qa-desc .qa-content>a")
	List<WebElement> questionContent;

	@FindBy(css = ".form-row.email-box.email.required .field-wrapper .input-text.email.email.required.email")
	WebElement emailTextBox;

	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement txtBreadCrumbElementMobile;

	@FindBy(css = ".hide-desktop.hide-tablet.back-arrow")
	WebElement imgBreadCrumbBackArrowMobile;
	
	@FindBy (css = ".subscribe-emailid .email-box")
	WebElement fldEmail;
	
	@FindBy (css = ".solid-wine-berry")
	WebElement btnSubmit;
	
	@FindBy(css = ".unsubscribe-confirmation-email-thankyou h1.mainheader")
	WebElement headerText;
	
	@FindBy(css = ".unsubscribe-confirmation-email-thankyou .email-unauthenticatedunsubscribe-subhead")
	WebElement subheadText;
	
	@FindBy(css = ".field-wrapper .form-row.email.required input")
	WebElement emailAddressTxtField;
	
	@FindBy(css = "button.emailsubscribeguest")
	WebElement submitButton;
	
	@FindBy(css = ".field-wrapper .form-row.email.required .label-text")
	WebElement emailAddressPlaceHolder;

	@FindBy(css = ".form-row.email.required.error-handle .input-focus")
	WebElement emailAddressErrorPlaceHolder;
	/**********************************************************************************************
	 ********************************* WebElements of EmailSubscriptionGuest - Ends ****************************
	 **********************************************************************************************/



	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public EmailSubscriptionGuest(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}



	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Email Subscription Page did not open up. Site might be down.", driver);
		}

	}// isLoaded

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to HomePage by click on breadcrumb
	 * @return HomePage
	 * @throws Exception - Exception
	 */
	public HomePage clickBreadcrumbHome()throws Exception{
		if(Utils.isMobile()) {
			BrowserActions.clickOnElementX(breadcrumbHomeMobile, driver, "Home on Breadcrumb");
		}
		else {
			BrowserActions.clickOnElementX(breadcrumHome, driver, "Home on Breadcrumb");
		}
		
		Utils.waitForPageLoad(driver);
		return new HomePage(driver).get();
	}

	/**
	 * To verify Preference Option selected By Default
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyNoPreferenceOptionsSelectedByDefault() throws Exception {
		for (WebElement element : preferenceOption) {
			if (element.getAttribute("class").equals("hemming-option selected")) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify No Preference Slider Selected By Default
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyNoPreferenceSliderSelectedByDefault() throws Exception {
		for (WebElement element : preferenceSlider) {
			if (element.getAttribute("background-color").contains("#331e53")) {
				return false;
			}
		}
		return true;
	}

	/**
	 * To click preference checkbox
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean clickOnPreferenceCheckbox() throws Exception {
		BrowserActions.clickOnElementX(preferenceOption.get(1), driver, "Select preference Checkbox");
		BrowserActions.clickOnElementX(preferenceOption.get(0), driver, "Select preference Checkbox");
		if (preferenceOption.get(0).isSelected() == false) {
			Log.message("User not able to select any preference independent of each other");
			return false;
		}
		return true;
	}

	/**
	 * To verify the checkbox functionality of Preference checkbox
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean uncheckPreferenceCheckbox() throws Exception {
		BrowserActions.clickOnElementX(preferenceOption.get(0), driver, "Select preference Checkbox");
		BrowserActions.clickOnElementX(preferenceOption.get(3), driver, "Select preference Checkbox");
		if (preferenceOption.get(0).isSelected() == true)
		{
			Log.message("Not able to uncheck preferences");
			return false;
		}

		return true;
	}

	/**
	 * To verify Email field is editable
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailFieldEditable()throws Exception {
		emailTextBox.clear();
		emailTextBox.sendKeys("test@yopmail.com");
		String text = BrowserActions.getText(driver, emailTextBox, "Get field value");
		if(text.isEmpty())
		{
			Log.message("Email field is not editable");
			return false;
		}
		return true;

	}

	/**
	 * To get Errow message while Email fields left empty
	 * @return String
	 * @throws Exception - Exception
	 */
	public String verifyEmailFieldBlankError()throws Exception {
		emailTextBox.clear();
		BrowserActions.clickOnElementX(subscribeButton, driver, "Blank email address and Clicked on signup");
		return BrowserActions.getText(driver, emptyEmailErrorMessage, "Error Message when email field is left blank");

	}
	
	/**
	 * To type text in email field
	 * @param email -
	 * @throws Exception -
	 */
	public void typeOnEmail(String email)throws Exception{
		emailTextBox.clear();
		BrowserActions.typeOnTextField(emailTextBox, email, driver, "Email Address ");
	}
	
	/**
	 * To click on submit button
	 */
	public void clickSubmit() {
		subscribeButton.click();
	}

	/**
	 * To get the Error message in Email Field
	 * @return String 
	 * @throws Exception - Exception
	 */
	public String verifyEmailFieldErrorIncorrectValue() throws Exception{
		emailAddressTxtField.clear();
		emailAddressTxtField.sendKeys("emailgmail@com");
		BrowserActions.clickOnElementX(submitButton, driver, "Entered incorrect email address and Clicked on signup");
		Utils.waitForPageLoad(driver);
		return BrowserActions.getText(driver, emailAddressErrorPlaceHolder, "Error Message");

	}
	
	/**
	 * To Navigate to Thankyou page after email subscription
	 * @return EmailSubscriptionThankyouPage
	 * @throws Exception - Exception
	 */
	public EmailSubscriptionThankyouPage navigateToEmailSubscriptionThankyouPage()throws Exception{
		emailTextBox.clear();
		emailTextBox.sendKeys("test@yopmail.com");
		BrowserActions.clickOnElementX(subscribeButton, driver, "Subscribe button click");
		Utils.waitForPageLoad(driver);
		return new EmailSubscriptionThankyouPage(driver).get();
	}

	/**
	 * To get the breadcrumb text
	 * @return string value
	 * @throws Exception - Exception
	 */
	public String verifyBreadcrumText()throws Exception {

		String text = BrowserActions.getText(driver, breadcrumText, "Breadcrum text");
		Log.messageT("Breadcrum text is displayed as: "+text+"");
		return text;

	}

	/**
	 * To verify Expanded state of Question AND Answers
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyExpandedState()throws Exception{
		Boolean flag=false;
		for(int i=0;i<questionContent.size();i++)
		{
			BrowserActions.clickOnElementX(questionContent.get(i), driver, "Expanded status of the Elements");
			if(questionContent.get(i).getAttribute("class").contains("active"))
			{
				Log.message("Question " + (i+1) +" is expandable");
				flag = true;
			}
			else
			{
				Log.message("Question " + (i+1) +" is not expandable");
				i=questionContent.size();
				flag = false;
			}

		}

		return flag;
	}

	/**
	 * To verify the collapsed state of all Questions
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyCollapsedState()throws Exception{
		Boolean flag=false;
		for(int i=0;i<questionContent.size();i++)
		{
			BrowserActions.clickOnElementX(questionContent.get(i), driver, "Expanded status of the Elements");
			BrowserActions.clickOnElementX(questionContent.get(i), driver, "Collapsed status of the Elements");
			if(questionContent.get(i).getAttribute("class").contains("active"))
			{
				Log.message("Question " + (i+1) +" is not collapsed");
				i=questionContent.size();
				flag = false;
			}
			else
			{
				Log.message("Question " + (i+1) +" is collapsed");
				flag = true;
			}

		}

		return flag;
	}

	/**
	 * To verify Place holder in Email Fields moves above when user starts typing
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailPlaceholderMovestotheTop()throws Exception {
		Boolean flag = true;
		emailAddressTxtField.clear();
		float y1=emailAddressPlaceHolder.getLocation().getY();
		emailAddressTxtField.sendKeys("email@gmail.com");
		float y2=emailAddressPlaceHolder.getLocation().getY();
		if(y2>y1)
			flag=true;
		return flag;
	}

	/**
	 * To get current Breadcrumb text in mobile view 
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getMobileBreadCrumb() throws Exception{
		return BrowserActions.getText(driver, txtBreadCrumbElementMobile, "Mobile Breadcrumb").trim();
	}

	/**
	 * To click on Back arrow in Mobile Breadcrumb
 	 * @return HomePage value
	 * @throws Exception - Exception
	 */
	public HomePage clickBackArrowInBreadcrumb() throws Exception{
		BrowserActions.clickOnElementX(imgBreadCrumbBackArrowMobile, driver, "Back Arrow");
		return new HomePage(driver).get();
	}
	
	/**
	 * Verify clear email fields
	 * @throws Exception - Exception
	 */
	public void clearEmailField()throws Exception {
		emailAddressTxtField.clear();
		BrowserActions.clickOnElementX(submitButton, driver, "Blank email address and Clicked on signup");
		Log.message("clear");
		}

}
