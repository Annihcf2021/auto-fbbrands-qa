package com.fbb.pages.customerservice;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class ContactUsPage extends LoadableComponent<ContactUsPage> {

	private WebDriver driver;
	public boolean isPageLoaded;
	public ElementLayer elementLayer;
	public CSNavigation csNav;
	String runBrowser;


	/**********************************************************************************************
	 ********************************* WebElements of contact us page ****************************
	 **********************************************************************************************/
	@FindBy(css = ".pt_article-page")
	WebElement readyElement;

	@FindBy(css = ".contactpage-right-content.make-label-absolute.contact-us-page>h1")
	WebElement formHeading;

	@FindBy(css = ".customerservice-name.hide-mobile")
	WebElement csPageHeadinglabel;
	
	@FindBy(css = ".secondary-article-heading.mobile-nav")
	WebElement csPageHeadinglabelMobile;
	
	@FindBy(css = ".contactpage-right-content.make-label-absolute.contact-us-page>h1")
	WebElement csCurrentHeading;
	
	@FindBy(css = ".pt_article-page.null" )
	WebElement lnkNavContactUS;

	@FindBy(css = ".text-content .content-asset")
	WebElement formSubText;
	
	@FindBy(css = ".form-wrapper .content-asset")
	WebElement formSubTextRM;

	@FindBy(css = "#ContactUsForm")
	WebElement formContactUS;

	@FindBy(css = "#contactUsSubmit")
	WebElement submitButton; 

	@FindBy(css = ".form-row.email.required label[for='dwfrm_contactus_email'] .label-text")
	WebElement emailAddressPlaceHolder;

	@FindBy(css = ".form-row.email.required label[for='dwfrm_contactus_email'] .error")
	WebElement emailAddressErrorMsgPlaceHolder;

	@FindBy(css = "label[for='dwfrm_contactus_firstname'] .label-text")
	WebElement firstNamePlaceHolder;

	@FindBy(css = ".form-row.lastname.required label[for='dwfrm_contactus_lastname'] .label-text")
	WebElement lastNamePlaceHolder;

	@FindBy(css = ".form-row.zipcode.required label[for='dwfrm_contactus_zipcode'] .label-text")
	WebElement zipCodePlaceHolder;

	@FindBy(css = "#dwfrm_contactus_zipcode-error")
	WebElement zipCodeErrorMsgPlaceHolder;

	@FindBy(css = ".form-row.category.required label[for='dwfrm_contactus_category'] .label-text")
	WebElement categoryPlaceHolder;

	@FindBy(css = ".form-row.subcategory.required label[for='dwfrm_contactus_subcategory'] .label-text")
	WebElement subcategoryPlaceHolder;
	
	@FindBy(css = ".selectBox.category .custom-select")
	WebElement selectTopic;
	
	@FindBy(css = ".selectBox.subcategory .custom-select")
	WebElement selectSubTopic;

	@FindBy(css = ".form-row.message.required label[for='dwfrm_contactus_message'] .label-text")
	WebElement messagePlaceHolder;

	@FindBy(css = "#dwfrm_contactus_email")
	WebElement emailval;

	@FindBy(css = "#dwfrm_contactus_zipcode")
	WebElement zipCodeValue;
	
	@FindBy(css = "#dwfrm_contactus_firstname")
	WebElement firstName;
	
	@FindBy(css = "#dwfrm_contactus_lastname")
	WebElement lastName;
	
	@FindBy(css = "#dwfrm_contactus_category")
	WebElement contactusTopic;
	
	@FindBy(css = "#dwfrm_contactus_subcategory")
	WebElement contactusSubTopic;
	
	@FindBy(css = "#dwfrm_contactus_message")
	WebElement contactusMessage;
	
	
	@FindBy(css = "a[title='Shopping']")
	WebElement lnkShoppingPage;
	
	@FindBy(css = ".secondary-aticle-content .secondary-article-heading.mobile-nav")
	WebElement MenuMobile;
	
	@FindBy(css = ".secondary-article-heading.mobile-nav.active")
	WebElement drpDownLeftNavActiveMobile;

	@FindBy(css = ".form-wrapper")
	WebElement emptySpace;

	@FindBy(css = ".inner-service-landing-contacts h2")
	WebElement tileContactModule;

	@FindBy(css = ".inner-service-landing-contacts h2")
	WebElement dwfrm_contactus_category;

	@FindBy(css = ".solid-wine-berry")
	WebElement btnsubmit;

	@FindBy(css = ".contactpage-right-content make-label-absolute contact-us-page")
	WebElement contactconfirmation;

	@FindBy(css = "div[class*='pt_article-page cs-faq-']" )
	WebElement lnknavFaq;

	@FindBy(css = "a[title='FAQs']")
	WebElement tileLeftNavFaqww;

	@FindBy(css = "a[title='FAQ']")
	WebElement tileLeftNavFaq;
	
	@FindBy(css = ".secondary-left-section")
	WebElement navLeftNavigation;
	
	@FindBy(css = ".customerservice-name.hide-tablet.hide-mobile")
	WebElement csLabelDesktop;
	
	@FindBy(css = ".customerservice-name.hide-desktop")
	WebElement csLabelTablet;
	
	@FindBy(css = ".secondary-left-section")
	WebElement leftNavigationSection;
	
	@FindBy(css = ".inner-service-landing-contacts")
	WebElement contactUsSection;
	
	@FindBy(css = ".contact-us-page")
	WebElement divContactUsBody;

	/**********************************************************************************************
	 ********************************* WebElements of contact us page ****************************
	 **********************************************************************************************/	


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public ContactUsPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
		runBrowser  = Utils.getRunBrowser(driver);
		csNav = new CSNavigation(driver);
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver,readyElement))) {
			Log.fail("Contact Us Page did not open up.", driver);
		}
	}

	/**
	 * To type Email and Zipcode in respective text fields
	 * @param email - 
	 * @param zipCode - 
	 * @throws Exception - 
	 */
	public void typeOnFields(String email, String zipCode)throws Exception{

		BrowserActions.typeOnTextField(emailval, email, driver, "Email Address ");
		BrowserActions.typeOnTextField(zipCodeValue, zipCode, driver, "Zip code ");

	}

	/**
	 * To verify Fields are editable in Contact Us page
	 * @throws Exception - Exception
	 */
	public void verifyFieldsAreEditable()throws Exception {

		//WebElement message = driver.findElement(By.id("dwfrm_contactus_message"));
		//message.sendKeys("contact message for order");

		List<WebElement> ListEditablefields=driver.findElements(By.cssSelector("input[id^='dwfrm_contactus_']:not([readonly='readonly'])")); 
		for (int i = 0; i < ListEditablefields.size(); i++) {
			if(ListEditablefields.get(i).getAttribute("data-required-text").contains("Email Address")) {
				ListEditablefields.get(i).clear();
				ListEditablefields.get(i).sendKeys("automation@yopmail.com");	
			} 
			if(ListEditablefields.get(i).getAttribute("data-required-text").contains("First Name")) {		
				ListEditablefields.get(i).clear();
				ListEditablefields.get(i).sendKeys("AutoFt Name");	
			}
			if(ListEditablefields.get(i).getAttribute("data-required-text").contains("Last Name")) {	
				ListEditablefields.get(i).clear();
				ListEditablefields.get(i).sendKeys("AutoLt Name");	
			}
			if(ListEditablefields.get(i).getAttribute("data-required-text").contains("Zip Code")) {				
				ListEditablefields.get(i).clear();
				ListEditablefields.get(i).sendKeys("55555");	
			}
			if(ListEditablefields.get(i).getAttribute("data-required-text").contains("Zip Code")) {				
				ListEditablefields.get(i).clear();
				ListEditablefields.get(i).sendKeys("55555");	
			}
		}
		Select topic = new Select(driver.findElement(By.id("dwfrm_contactus_category")));
		try{
			topic.selectByIndex(3);
		}catch(Exception e) {
			Log.failsoft("Topic entry not available in menu items. BM configuration missing.");
		}

		Select subTopic = new Select(driver.findElement(By.id("dwfrm_contactus_subcategory")));
		try{
			subTopic.selectByIndex(3);
			
		}catch(Exception e) {
			Log.failsoft("Sub-Topic entry not available in menu items. BM configuration missing.");
		}
		WebElement message = driver.findElement(By.id("dwfrm_contactus_message"));
		message.sendKeys("contact message for order");
		
	}
	
	/**
	 * To select topic and subtopic from dropdown menu
	 * @param String - Topic
	 * @param String - SubTopic
	 * @exception - Exception
	 */
	public void selectTopicSubtopic(String topic, String subTopic)throws Exception{
		Select topicElement = new Select(driver.findElement(By.id("dwfrm_contactus_category")));
		try{
			topicElement.selectByValue(topic);
			Log.event(topic + " selected.");
		}catch(Exception e) {
			Log.failsoft("Topic entry not available in menu items. BM configuration may be needed.");
		}

		Select subTopicElement = new Select(driver.findElement(By.id("dwfrm_contactus_subcategory")));
		try{
			subTopicElement.selectByValue(subTopic);
			Log.event(subTopic + " selected.");
		}catch(Exception e) {
			Log.failsoft("Sub-Topic entry not available. Please check if (correct) topic is selected. BM configuration may be needed.");
		}
	}
	
	/**
	 * To fill contact US form with provided value
	 * @param String - email
	 * @param String - fName
	 * @param String - lName
	 * @param String - ZIP
	 * @param String - topic
	 * @param String - subTopic
	 * @param String - message
	 * @throws Exception - Exception
	 */
	public void fillContactUSForm(String email, String fName, String lName, String ZIP, String topic, String subTopic, String message)throws Exception{
		BrowserActions.refreshPage(driver);
		BrowserActions.typeOnTextField(emailval, email, driver, "Email");
		BrowserActions.typeOnTextField(firstName, fName, driver, "First Name");
		BrowserActions.typeOnTextField(lastName, lName, driver, "Last Name");
		BrowserActions.typeOnTextField(zipCodeValue, ZIP, driver, "ZIP");
		selectTopicSubtopic(topic, subTopic);
		BrowserActions.typeOnTextField(contactusMessage, message, driver, "Message");
	}

	/**
	 * To verify Contact Us form fields are editable
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyFieldsEditable()throws Exception {
		Boolean flag=true;
		verifyFieldsAreEditable();
		List<WebElement> ListEditablefields=driver.findElements(By.cssSelector("input[id^='dwfrm_contactus_']:not([readonly='readonly'])"));
		for (int i = 0; i < ListEditablefields.size(); i++) {
			String text = BrowserActions.getText(driver, ListEditablefields.get(i), "Get field value");
			if(text.isEmpty()) {
				//Log.message("field is not editable");
				i = ListEditablefields.size();
				flag = false;
			}
		}
		return flag;

	}

	/**
	 * To verify Fields in Contact Us form are filled
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyFieldsAreFilled()throws Exception {
		Boolean flag=true;
		List<WebElement> ListEditablefields=driver.findElements(By.cssSelector("input[id^='dwfrm_contactus_']:not([readonly='readonly'])")); 
		//int temp=0;
		for (int i = 0; i < ListEditablefields.size(); i++) {
			if(ListEditablefields.get(i).getText().equals("")) {
				Log.message(ListEditablefields.get(i).getText()+" field is not edited");
				flag = false;
			} else {
				flag = true;
			}

		}
		return flag;
	}

	/**
	 * To verify Fields in Contact Us are mandatory
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyContactUsFieldsMandatory()throws Exception {
		Boolean flag = true;
		BrowserActions.clickOnElementX(submitButton, driver, "Clicked on submit");
		String[] fields= {"Email Address","First Name","Last Name","Zip Code","Topic","Sub-Topic","Required field."};
		List<WebElement> ListEditableTextfields=driver.findElements(By.cssSelector("span[id^=dwfrm_contactus].error")); 

		for (int i = 0; i < ListEditableTextfields.size(); i++) {
			if (ListEditableTextfields.get(i).getAttribute("innerHTML").equals(fields[i])) {
				flag=Utils.verifyCssPropertyForElement(ListEditableTextfields.get(i), "color", "rgba(230, 0, 60, 1)");
				if(flag) {
					Log.message(fields[i]+" is mandatory field");
				} else {
					Log.message(fields[i]+" is not mandatory field");
				}
			}
		}
		return flag;
	}

	/**
	 * To verify Error message with Invalid Email Message entered
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyIncorrectEmailErrorMsg()throws Exception {
		Boolean flag = true;

		typeOnFields("Automationyopmail@com","5555");
		BrowserActions.clickOnElementX(emptySpace, driver, "Clicked on emptyspace");
		flag = Utils.verifyCssPropertyForElement(emailAddressErrorMsgPlaceHolder, "color", "rgba(230, 0, 60, 1)");

		if(flag) {
			Log.message("emailval and zipcode are correct");
		} else {
			Log.message("emailval and zipcode are incorrect");
		}

		return flag;		
	}
	
	/**
	 * To verify Error message with incorrect Zipcode entered
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyIncorrectZipCodeErrorMsg()throws Exception {
		Boolean flag = true;

		flag=Utils.verifyCssPropertyForElement(emailAddressErrorMsgPlaceHolder, "color", "rgba(230, 0, 60, 1)");

		if(flag)
		{
			Log.message("zipcode is correct");
		}
		else
		{
			Log.message("zipcode is incorrect");
		}

		return flag;		
	}

	/**
	 * To Get error message displayed in Email Box
	 * @return String - Error Message from Email Box
	 * @throws Exception - Exception
	 */
	public String getDisplayedEmailErrorMessage() throws Exception{
		typeOnFields("emailgmail@com","5555");
		BrowserActions.clickOnElementX(btnsubmit, driver, "click submit button");
		Utils.waitForPageLoad(driver);
		return BrowserActions.getText(driver, emailAddressErrorMsgPlaceHolder, "Error Message");

	}

	/**
	 * To get Zipcode error message
	 * @return Zipcode Error Message
	 * @throws Exception - Exception
	 */
	public String getDisplayedZipCodeErrorMessage() throws Exception{
		return BrowserActions.getText(driver, zipCodeErrorMsgPlaceHolder, "Error Message");

	}

	/**
	 * To verify by default , fields are empty for unauthenticated user
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyFieldsDefaultEmpty()throws Exception {

		List<WebElement> ListEditablefields=driver.findElements(By.cssSelector("input[id^='dwfrm_contactus_']:not([readonly='readonly'])")); 
		Boolean flag=false;
		for (int i = 0; i < ListEditablefields.size(); i++){

			flag = BrowserActions.getText(driver, ListEditablefields.get(i), "Get field value").isEmpty();	
			if(flag==false)
			{
				i=ListEditablefields.size();
				Log.message(ListEditablefields.get(i)+" is not empty");
			}

		}
		return flag;
	}

	/**
	 * To navigate to contact confirmation page
	 * @return ContactUsPage
	 * @throws Exception - Exception
	 */
	public ContactUsPage navigateToContactConfirmationPage()throws Exception{
		BrowserActions.clickOnElementX(btnsubmit, driver, "ContactUS Page Link");
		Utils.waitForPageLoad(driver);
		return new ContactUsPage(driver).get();
	}

	/**
	 * To navigate to FAQ Page
	 * @return FaqPage
	 * @throws Exception - Exception
	 */
	public FaqPage navigateToFaqPage()throws Exception{
		if (BrandUtils.isBrand(Brand.ww)){
			BrowserActions.clickOnElementX(tileLeftNavFaqww, driver, "FAQ Page Link");
		} else {
			BrowserActions.clickOnElementX(tileLeftNavFaq, driver, "FAQ Page Link");	
		}
		Utils.waitForPageLoad(driver);
		return new FaqPage(driver).get();
	}
	
	
	/**
	 * To navigate to Shopping Page
	 * @return ShoppingPage - Shopping page object
	 * @throws Exception - Exception
	 */
	public ShoppingPage navigateToShoppingPage()throws Exception{
		if(!Utils.isDesktop()) {
			if(!(Utils.waitForElement(driver, drpDownLeftNavActiveMobile))) {
				BrowserActions.clickOnElementX(MenuMobile, driver, "Expanded status of the Elements");
			}
		}
		BrowserActions.clickOnElementX(lnkShoppingPage, driver, "Shopping Page");
		Utils.waitForPageLoad(driver);
		return new ShoppingPage(driver).get();
	}
	
	/**
	 * To verify default entered value not saved by default
	 * @param boolean - if account has address saved
	 * @return boolean - true/false if fields are correctly pre-populated
	 * @throws Exception - Exception
	 */
	public Boolean verifyFieldsDefaultEnteredValues(boolean savedAddress)throws Exception {
		Boolean flag=true;
		List<WebElement> ListEditablefields=driver.findElements(By.cssSelector("input[id^='dwfrm_contactus_']:not([readonly='readonly'])")); 
		for (int i = 0; i < 3; i++){
			String fieldDefaultValue = BrowserActions.getText(driver, ListEditablefields.get(i), "Get field value");		
			if(fieldDefaultValue.isEmpty() || fieldDefaultValue.equals("") || fieldDefaultValue.equals(null)) {
				Log.event("Missing iteration :: " + (i+1));
				return false;
			}
		}
		String fieldDefaultValue = BrowserActions.getText(driver, ListEditablefields.get(3), "Get field value");
		if(savedAddress) {
			if(fieldDefaultValue.isEmpty() || fieldDefaultValue.equals("") || fieldDefaultValue.equals(null)) {
				Log.event("ZIP is not prepopulated");
				return false;
			}
		}else {
			if(fieldDefaultValue.length()!=0 || !fieldDefaultValue.isEmpty()) {
				Log.event("ZIP is prepopulated");
				return false;
			}
		}
		return flag;
	}

	/**
	 * To verify Default values in Address not by default
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyFieldsDefaultEnteredValuesNotSaved()throws Exception {
		Boolean flag=true;
		List<WebElement> ListEditablefields=driver.findElements(By.cssSelector("input[id^='dwfrm_contactus_']:not([readonly='readonly'])")); 

		for (int i = 0; i < 3; i++){
			String fieldDefaultValue = BrowserActions.getText(driver, ListEditablefields.get(i), "Get field value");	
			if(fieldDefaultValue.isEmpty() || fieldDefaultValue.equals("") || fieldDefaultValue.equals(null)) {
				Log.event("Missing iteration :: " + (i+1));
				return false;
			}
		}
		return flag;
	}

	/**
	 * To click on Submit button and navigate to Contact Confirmation page
	 * @return ContactConfirmationPage value
	 * @throws Exception - Exception
	 */
	public ContactConfirmationPage clickOnSubmitButton()throws Exception{

		BrowserActions.clickOnElementX(submitButton, driver, "Clicked on submit");
		Utils.waitForPageLoad(driver);
		return new ContactConfirmationPage(driver).get();
	}

}
