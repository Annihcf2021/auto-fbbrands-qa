package com.fbb.pages.customerservice;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class CSNavigation extends LoadableComponent<CSNavigation> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;


	/**********************************************************************************************
	 ********************************* WebElements of Customer Service page ****************************
	 **********************************************************************************************/

	@FindBy(xpath = "//span[contains(text(),'Order status')]//ancestor::a")
	WebElement tileOrderStatus;

	@FindBy(css = "a[title='order status']")
	WebElement tileLeftNavorderstatus1;

	@FindBy(css = "a[title*='Shipping info' i]")
	WebElement tileLeftNavShippinginformation;

	@FindBy(css = "#ContactUsForm")
	WebElement readyElementContactUs;

	@FindBy(css = ".secondary-left-section")
	WebElement readyElementCustomerService;

	@FindBy(css = ".pt_specials")
	WebElement readyElementPlatinumCard;

	@FindBy(css = "a[title^='Return & Exchanges' i]")
	WebElement tileLeftNavreturnandexchanges;

	@FindBy(css = "a[title$='Platinum Card']")
	WebElement tileLeftNavPlantinumcards;

	@FindBy(css = "a[title='Shopping']")
	WebElement tileLeftNavShopping;

	@FindBy(css = "a[title='Gift Cards' i]")
	WebElement tileLeftNavGiftCards;

	@FindBy(css = "a[title^='Size']")
	WebElement tileLeftNavSizeChart;

	@FindBy(css = "a[title*='FAQ']")
	WebElement tileLeftNavFaq;
/*
	@FindBy(css = "a[title='FAQs']")
	WebElement tileLeftNavFaqww;*/		

	@FindBy(css = "a[title='Contact US']")
	WebElement tileLeftNavContactUS;

	@FindBy(css = ".secondary-left-section")
	WebElement readyElement;

	@FindBy(css = "div[class*='pt_article-page account-clock-']")
	WebElement lnkNavOrderStatus;


	@FindBy(css = "div[class*='pt_article-page account-shipping-']" )
	WebElement lnkNavShippingInformation;

	@FindBy(css = "div[class*='pt_article-page account-return-information-']" )
	WebElement lnkNavReturnAndExchanges;


	@FindBy(css = "div[class*='pt_article-page account-platinum-card-']" )
	WebElement lnkNavPlatinumCards;

	@FindBy(css = "div[class*='pt_article-page account-shopping-']" )
	WebElement lnkNavShopping;

	@FindBy(css = "div[class*='pt_article-page account-gift-']" )
	WebElement lnkNavGiftCards;

	@FindBy(css = "div[class*='pt_article-page account-size-']" )
	WebElement lnkNavSizeChart;

	@FindBy(css = "div[class*='pt_article-page cs-faq-']" )
	WebElement lnknavFaq;

	@FindBy(css = ".pt_article-page" )
	WebElement lnkNavContactUS;

	@FindBy(css = ".faq-content h1")
	WebElement tileFAQ;

	@FindBy(css = ".secondary-article-heading.mobile-nav")
	WebElement readyElementMobile;
	
	@FindBy(css = ".secondary-left-section li a")
	List<WebElement> leftNavigationArticleList;
	
	@FindBy(css = ".secondary-aticle-content .mobile-nav")
	WebElement navLeftNavigationMobile;
	
	@FindBy(css = ".secondary-left-section ul li a")
	List<WebElement> dropdownMenuList;

	/**********************************************************************************************
	 ********************************* WebElements of Customer Service page ****************************
	 **********************************************************************************************/	


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public CSNavigation(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {
		try {
			if (!isPageLoaded) {
				Assert.fail();
			}
			if(Utils.isDesktop()) {
				if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) 
					Log.fail("Customer Service Navigation did not open up.", driver);
			}else {
				if (isPageLoaded && !(Utils.waitForElement(driver, readyElementMobile))) 
					Log.fail("Customer Service Navigation did not open up.", driver);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * To Navigate To Order Status Page
	 * @return OrderStatusPage
	 * @throws Exception - Exception
	 */
	public OrderStatusPage navigateToOrderStatusPage()throws Exception{
		BrowserActions.clickOnElementX(tileLeftNavorderstatus1, driver, "Order Status Link");
		Utils.waitForPageLoad(driver);
		return new OrderStatusPage(driver).get();
	}

	/**
	 * To Navigate To Shipping Information Page
	 * @return ShippingInformationPage
	 * @throws Exception - Exception
	 */
	public ShippingInformationPage navigateToShippingInformationPage()throws Exception{
		openNavigationDropdown();
		BrowserActions.clickOnElementX(tileLeftNavShippinginformation, driver, "shipping information link");
		Utils.waitForPageLoad(driver);
		return new ShippingInformationPage(driver).get();
	}

	/**
	 * To Navigate To Return and Exchange Page
	 * @return ReturnAndExchangePage
	 * @throws Exception - Exception
	 */
	public ReturnAndExchangePage navigateToReturnAndExchangePage()throws Exception{
		openNavigationDropdown();
		BrowserActions.clickOnElementX(tileLeftNavreturnandexchanges, driver, "Return and Exchanges Link");
		Utils.waitForPageLoad(driver);
		return new ReturnAndExchangePage(driver).get();
	}

	/**
	 * To Navigate To PlatinumCardPage
	 * @return PlatinumCardsPage
	 * @throws Exception - Exception
	 */
	public PlatinumCardsPage navigateToPlatinumCardsPage()throws Exception{
		openNavigationDropdown();
		BrowserActions.clickOnElementX(tileLeftNavPlantinumcards, driver, "platinum cards Link");
		Utils.waitForPageLoad(driver);
		return new PlatinumCardsPage(driver).get();
	}

	/**
	 * To Navigate to Shopping Page
	 * @return ShoppingPage
	 * @throws Exception - Exception
	 */
	public ShoppingPage navigateToShoppingPage()throws Exception{
		openNavigationDropdown();
		BrowserActions.clickOnElementX(tileLeftNavShopping, driver, "Shopping Page Link");
		Utils.waitForPageLoad(driver);
		return new ShoppingPage(driver).get();
	}
	
	/**
	 * To Navigate to Gift cards lookup page
	 * @return GiftCardsPage - Gift card lookup page
	 * @throws Exception - Exception
	 */
	public GiftCardsPage navigateToGiftCardsPage()throws Exception{
		openNavigationDropdown();
		BrowserActions.clickOnElementX(tileLeftNavGiftCards, driver, "Gift Cards Link");
		Utils.waitForPageLoad(driver);
		return new GiftCardsPage(driver).get();

	}
	
	/**
	 * To Navigate to Size chart content page
	 * @return SizeChartPage
	 * @throws Exception - Exception
	 */
	public SizeChartPage navigateToSizeChartPage()throws Exception{
		openNavigationDropdown();
		BrowserActions.clickOnElementX(tileLeftNavSizeChart, driver, "SizeChart Link");
		Utils.waitForPageLoad(driver);
		return new SizeChartPage(driver).get();
	}
	
	/**
	 * To Navigate to Frequently Asked Questions Page
	 * @return FaqPage
	 * @throws Exception - Exception
	 */
	public FaqPage navigateToFaqPage()throws Exception{
		openNavigationDropdown();
		BrowserActions.clickOnElementX(tileLeftNavFaq, driver, "FAQ Page Link");
		Utils.waitForPageLoad(driver);
		return new FaqPage(driver).get();


	}
	
	/**
	 * To Navigate to contact us page
	 * @return ContactUsPage
	 * @throws Exception - Exception
	 */
	public ContactUsPage navigateToContactUsPage()throws Exception{
		openNavigationDropdown();
		BrowserActions.clickOnElementX(tileLeftNavContactUS, driver, "Contact us page Link");
		Utils.waitForPageLoad(driver);
		return new ContactUsPage(driver).get();
	}

	/**
	 * To Navigate to Shipping information Page
	 * @throws Exception - Exception
	 */
	public void navigateToShippingInformationPage1()throws Exception{
		BrowserActions.clickOnElementX(tileLeftNavShippinginformation, driver, "shipping information link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to Returns and Exchange Page
	 * @throws Exception - Exception
	 */
	public void navigateToReturnAndExchangePage1()throws Exception{
		BrowserActions.clickOnElementX(tileLeftNavreturnandexchanges, driver, "Return and Exchanges Link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To Navigate to PlatinumCardsPage
	 * @throws Exception - Exception
	 */
	public void navigateToPlatinumCardsPage1()throws Exception{
		BrowserActions.clickOnElementX(tileLeftNavPlantinumcards, driver, "platinum cards Link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To navigate to Shopping Page
	 * @throws Exception - Exception
	 */
	public void navigateToShoppingPage1()throws Exception{
		BrowserActions.clickOnElementX(tileLeftNavShopping, driver, "Shopping Page Link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To navigate to Gift cards Page
	 * @throws Exception - Exception
	 */
	public void navigateToGiftCardsPage1()throws Exception{
		BrowserActions.clickOnElementX(tileLeftNavGiftCards, driver, "Gift Cards Link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To Navigate to Size chart page
	 * @throws Exception - Exception
	 */
	public void navigateToSizeChartPage1()throws Exception{
		BrowserActions.clickOnElementX(tileLeftNavSizeChart, driver, "SizeChart Link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To navigate to Frequently Asked Questions Page
	 * @throws Exception - Exception
	 */
	public void navigateToFaqPage1()throws Exception{
		BrowserActions.clickOnElementX(tileLeftNavFaq, driver, "FAQ Page Link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To navigate to contact us page
	 * @throws Exception - Exception
	 */
	public void navigateToContactUsPage1()throws Exception{
		BrowserActions.clickOnElementX(tileLeftNavContactUS, driver, "Contact us page Link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To open CS navigation dropdown menu on non-desktop platform
	 * @throws Exception - Exception
	 */
	public void openNavigationDropdown()throws Exception{
		if(!Utils.isDesktop()) {
			BrowserActions.clickOnElementX(readyElementMobile, driver, "Dropdown menu");
		}
	}
	
	/**
	 * To get List of Left Navigation menus in Customer Service Page
	 * @return list value
	 * @throws Exception - Exception
	 */
	public List<String> LeftNavigationArticleList()throws Exception{
		List<String> navList = new ArrayList<String>();
		for(int i =0;i<leftNavigationArticleList.size();i++)
		{
			navList.add(leftNavigationArticleList.get(i).getText().toLowerCase());
		}
		System.out.println("navList:: " + navList);
		return navList;
	}
	
	/**
	 * To get List of DropDown menu items in Customer Service Page
	 * @return list value
	 * @throws Exception - Exception
	 */
	public List<String> DropDownArticleList()throws Exception{
		BrowserActions.clickOnElementX(navLeftNavigationMobile, driver, "Dropdown menu");
		List<String> menuList = new ArrayList<String>();
		for(int i =0;i<dropdownMenuList.size();i++)
		{
			menuList.add(dropdownMenuList.get(i).getText().toLowerCase());
		}
		BrowserActions.clickOnElementX(navLeftNavigationMobile, driver, "Dropdown menu");
		System.out.println("Dropdown menuList:: " + menuList);
		return menuList;
	}

}

