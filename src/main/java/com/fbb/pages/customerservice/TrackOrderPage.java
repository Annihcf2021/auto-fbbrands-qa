package com.fbb.pages.customerservice;


import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.ordering.GuestOrderStatusLandingPage;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class TrackOrderPage extends LoadableComponent<TrackOrderPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	String runBrowser;

	
	/**********************************************************************************************
	 ********************************* WebElements of Track My Order page ****************************
	 **********************************************************************************************/
	
	@FindBy(css = ".pt_account")
	WebElement readyElement;
	
	@FindBy(css = ".pt_order")
	WebElement orderHistoryReadyElement;
	
	@FindBy(xpath = "//span[contains(text(),'Order status')]//ancestor::a")
	WebElement tileOrderStatus;

	@FindBy(css = ".article-heading")
	WebElement orderHeading;
	
	@FindBy(css = ".secondary-left-section>ul>li>a")
	List<WebElement> navigationOptions;
	
	@FindBy(css = "a.orderLookupToggle.lookUpButton")
	WebElement btnLookUpBeforeToggle;
	
	@FindBy(id = "dwfrm_ordertrack_orderNumber")
	WebElement txtFldOrderNumber;
	
	@FindBy(id = "dwfrm_ordertrack_orderEmail")
	WebElement txtFldOrderEmail;
	
	@FindBy(id = "dwfrm_ordertrack_postalCode")
	WebElement txtFldBillingZipcode;
	
	@FindBy(css = "button[name='dwfrm_ordertrack_findorder']")
	WebElement checkorderstatusbtn;

	
	/**********************************************************************************************
	 ********************************* WebElements of Track My Order page ****************************
	 **********************************************************************************************/	
	
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public TrackOrderPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
		runBrowser  = Utils.getRunBrowser(driver);
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			if(isPageLoaded && !(Utils.waitForElement(driver, orderHistoryReadyElement))) {
				Log.fail("Track My Order Page did not open up.", driver);
			}
		}
		try {
			GlobalNavigation.closePopup(driver);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * To click on order status 
	 * @throws Exception - Exception
	 */
	public void navigateToOrderStausPage() throws Exception{
		BrowserActions.clickOnElementX(tileOrderStatus, driver, "Order Status Tile link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To navigate to contact us page
	 * @return ContactUsPage
	 * @throws Exception - Exception
	 */
	
	public ContactUsPage navigateToContactUSPage() throws Exception{		
		BrowserActions.clickOnElementX(navigationOptions.get(8), driver, "Contact us link");
		Utils.waitForPageLoad(driver);
		return new ContactUsPage(driver).get();
	}
	
	/**
	 * To click on LookUP button Before Toggle
	 * @throws Exception - Exception
	 */
	public void clickOnLookUpToggle() throws Exception{
		BrowserActions.clickOnElementX(btnLookUpBeforeToggle, driver, "Lookup button Before Toggle");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To enter order track values
	 * @param orderNumber - order number
	 * @param orderEmail - order email id
	 * @param billingZipCode - order billing zip code
	 * @throws Exception - Exception
	 */
	public void typeOnOrderTrackFormFields(String orderNumber,String orderEmail,String billingZipCode) throws Exception{
		
		if (Utils.waitForElement(driver, btnLookUpBeforeToggle)) {
			clickOnLookUpToggle();
		}
		
		BrowserActions.typeOnTextField(txtFldOrderNumber, orderNumber, driver, "Order Number");
		BrowserActions.typeOnTextField(txtFldOrderEmail, orderEmail, driver, "Order Email");
		BrowserActions.typeOnTextField(txtFldBillingZipcode, billingZipCode, driver, "Order Zipcode");
	}
	
	/**
	 * Click on submit button page 
	 * @return GuestOrderStatusLandingPage Page Object
	 * @throws Exception - Exception
	 */
	public GuestOrderStatusLandingPage clickOnSubmitButton() throws Exception{
		BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Clicked on submit");
		Utils.waitForPageLoad(driver);
		return new GuestOrderStatusLandingPage(driver).get();
	}

}

