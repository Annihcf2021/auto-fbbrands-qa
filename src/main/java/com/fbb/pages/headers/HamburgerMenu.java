package com.fbb.pages.headers;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.PlpPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.customerservice.CustomerService;
import com.fbb.pages.customerservice.TrackOrderPage;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class HamburgerMenu extends LoadableComponent<HamburgerMenu> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	private static final String FOOTER_MOBILE = ".menu-footer ";
	private static final String SLIDER_MENU_MOBILE = ".slider-menu ";
	private static final String SLIDER_MENU_UTILITY_MOBILE = SLIDER_MENU_MOBILE + ".menu-utility ";
	private static final String ACCOUNT_MENU_MOBILE = ".right-panel-inner .registered-user ";
	private static final String SECONDARY_HEADER = ".secondary-header ";
	
	//=====================================================================//
	//	-------------------Mobile Elements-----------------------------    //
	//=====================================================================//

	@FindBy(css = ".brand-menu-tab .menu-toggle")
	WebElement lnkHamburger;

	@FindBy(css = "#navigation .hamburger-toggle .menu-toggle")
	WebElement lnkHamburgerOpened;
	
	@FindBy(css = "body:not(.header-mobile-menu-open)")
	WebElement lnkHamburgerClosed;

	@FindBy(css = "button.menu-toggle")
	WebElement btnHamburgerClose;
	
	@FindBy(css = SECONDARY_HEADER + ".deals-xs a")
	WebElement lnkOffers;

	@FindBy(css = FOOTER_MOBILE + "input[id*='dwfrm_emailsignup_email']")
	WebElement fldEmailSignUp;
	
	@FindBy(css = FOOTER_MOBILE + "button[name='dwfrm_emailsignup_signup']")
	WebElement btnEmailSignUp;
	
	@FindBy(css = FOOTER_MOBILE + "span[id*='dwfrm_emailsignup_email_']")
	WebElement txtEmailSignUpError;
	
	@FindBy(css = ".footer-links.hide-mobile")
	WebElement customerServiceSection;
	
	@FindBy(css = FOOTER_MOBILE + ".footer-heading a[href*='help']")
	WebElement divFooterCustomerService;
	
	@FindBy(css = "div.primary-links.hide-desktop.hide-mobile")
	WebElement customerService_Tablet;
	
	@FindBy(css = "li.footer_item :nth-child(1) a[href*='help']")
	WebElement customerserviceHMenu;
	
	@FindBy(css = ".slider-menu .quick-links")
	WebElement hMenuQuickLinks;
	
	@FindBy(css = "a.user-account")
	WebElement lnkSignIn;
	
	@FindBy(css = ".mobile-menu-utilities .ucp2-account-icon")
	WebElement lnkAccountButton;
	
	@FindBy(css = ".mobile-sign-in .user-info.info a")
	WebElement lnkSignInMobile;

	@FindBy(css = ".mobile-menu-utilities .account-btn  span.ucp2-account-icon")
	WebElement imgUserProfile;

	@FindBy(css = ".mobile-menu-utilities .account-btn  span:not(.ucp2-account-icon)")
	WebElement lblUserName;

	@FindBy(css = ".mobile-menu-utilities .ucp2-account-icon")
	WebElement lnkMyAccount;
	
	@FindBy(xpath = "//div[@class='account-links']/a[1]")
	WebElement lnkAccOverview;
	
	@FindBy(css = ".pt_categorylanding")
	WebElement categoryLandingReadyElement;
	
	@FindBy(css = ".pt_product-search-result.product-list-page")
	WebElement plpPagereadyElement;
	
	@FindBy(css = ".pt_product-search-noresult")
	WebElement plpNoResultPageReadyElement;
	
	@FindBy(css = ".pt_lookbooklanding")
	WebElement explorePageReadyElement;

	@FindBy(css = ".slider-menu .menu-item-toggle")
	WebElement lnkMenuItemToggle;
	
	@FindBy(css = ".slider-menu  .menu-category.level-1>li>a")
	WebElement lnkCategoryName;

	@FindBy(css = ".slider-menu div.header-brand-selector")
	WebElement divUtilityBar;
	
	@FindBy(css=SLIDER_MENU_UTILITY_MOBILE)
	WebElement utilityBar;
	
	@FindBy(css = ".slider-menu .woman-within")
	WebElement lnkSliderMenuWomansWithin;
	
	@FindBy(css = ".slider-menu .jessica")
	WebElement lnkSliderMenuJessica;
	
	@FindBy(css = ".slider-menu .roman")
	WebElement lnkSliderMenuRoman;
	
	@FindBy(css = SLIDER_MENU_MOBILE + ".offer-flyout-main .offer-flyout")
	WebElement lnkSliderMenuOfferFlyOut;
	
	@FindBy(css = ".l2-activate .menu-item-toggle")
	WebElement lnkBackArrow;
	
	@FindBy(css = SLIDER_MENU_MOBILE + ".sub-cat-nav")
	WebElement divSubCategories;

	@FindBy(css = "ul.level-1>li")
	List<WebElement> lnkMenuItems;
	
	@FindBy(css = "ul.level-1>li >a:not(.has-sub-menu)")
	List<WebElement> lnkMenuItemsNoSubMenu;
	
	@FindBy(css = ".menu-category .has-sub-menu")
	List<WebElement> mainCategories;
	
	@FindBy(css = "#navigation .active.hoverIntent .has-sub-menu-level2-heading")
	WebElement childCategories;
	
	@FindBy(css = "#navigation .active.hoverIntent .has-sub-menu-level2-heading")
	List<WebElement> lstchlldCategories;	
	
	@FindBy(css = ".l2-activate .has-sub-menu")
	WebElement lblActiveMenuHeading;
	
	@FindBy(css = ".l2-activate .menu-item-toggle")
	WebElement lblActiveMenuToggleBack;
	
	@FindBy(css = ".l2-activate div>ul>li.nav-viewAll")
	WebElement lnkLevel2ViewAll;
	
	@FindBy(css = ".l2-activate .has-sub-menu")
	WebElement lnkSelectedCategory;
	
	@FindBy(css = ".slider-menu .menu-item-toggle")
	List<WebElement> lstCategoryArrow;
	
	@FindBy(css = ".slider-menu .menu-item-toggle")
	WebElement categoryArrow;
	
	@FindBy(css = ".slider-menu .level-1 > li > a")
	WebElement categoryName;
	
	@FindBy(css = ".slider-menu .level-1 > li > a")
	List<WebElement> lstCategoryName;

	@FindBy(css = SLIDER_MENU_MOBILE + "#navigation .menu-category.level-1")
	WebElement navigationCategory1;
	
	@FindBy(css = ".slider-menu .menu-category.level-1")
	List<WebElement> activeCategoryNav2;
	
	@FindBy(css = ".slider-menu .menu-category.level-1")
	WebElement activeCategoryNav;
	
	@FindBy(css = ".slider-menu .menu-category.level-1 .l2-activate .level-2")
	WebElement activeCategoryNavLevel2;
	
	@FindBy(css = ".slider-menu li.l2-activate > a")
	WebElement activeCat2Header;
	
	@FindBy(css = ".l2-activate")
	WebElement activeCat2Content;
	
	@FindBy(css = SLIDER_MENU_UTILITY_MOBILE + ".header-brand-selector .brand-logos a")
	List<WebElement> lnkOtherBrandLinksUnderNav;
	
	@FindBy(css = SLIDER_MENU_UTILITY_MOBILE + ".header-brand-selector")
	WebElement divOtherBrandLinksUnderNav;
	
	@FindBy(css = ".primary-links.hide-desktop.hide-tablet a[href*='help']")
	WebElement lnkCustomerService_Mobile;
	
	@FindBy(css = ".primary-links.hide-desktop.hide-tablet ")
	WebElement customerService_Mobile;
	
	@FindBy(css = ".slider-menu .hamburger-toggle .menu-toggle")
	WebElement btnCloseHamburger;
	
	@FindBy(css = ".brand-menu-tab .hamburger-toggle .menu-toggle")
	WebElement btnOpenHamburger;
	
	@FindBy(css = ACCOUNT_MENU_MOBILE)
	WebElement divAccountMenu;
	
	@FindBy(css = ACCOUNT_MENU_MOBILE + ".my-account-heading-mobile")
	WebElement lblMyAccountHeader;
	
	@FindBy(css = ACCOUNT_MENU_MOBILE + "a.signout")
	WebElement lnkSignOut;
	
	@FindBy(css = ACCOUNT_MENU_MOBILE + "a.Profile")
	WebElement lnkProfile;
	
	@FindBy(css = ACCOUNT_MENU_MOBILE + "a.Wishlist")
	WebElement lnkWishList;
	
	@FindBy(css = ACCOUNT_MENU_MOBILE + "a.Orderhistory")
	WebElement lnkOrderHistory;
	
	@FindBy(css = ACCOUNT_MENU_MOBILE + "a.wallet")
	WebElement lnkSavedPayment;
	
	@FindBy(css = ACCOUNT_MENU_MOBILE + "a.addressbook")
	WebElement lnkAddressbook;
	
	@FindBy(css = ACCOUNT_MENU_MOBILE + "a.catalog-preferences")
	WebElement lnkCatalogPrefs;
	
	@FindBy(css = ACCOUNT_MENU_MOBILE + "a.email-preferences")
	WebElement lnkEmailPrefs;
	
	@FindBy(css = ACCOUNT_MENU_MOBILE + "a.Rewards")
	WebElement lnkRewardPoints;
	
	@FindBy(css = ".account-links > a[href*='quick-order']")
	WebElement lnkQuickOrder; 	
	
	@FindBy(css = SECONDARY_HEADER + ".secondary-catalog a")
	WebElement iconQuickOrderMobile;	
	
	@FindBy(css = ".primary-links")
	WebElement divCustomerService;
	
	@FindBy(css = ".slider-menu li.l2-activate .nav-viewAll a")
	WebElement lnkViewAllActive;
	
	@FindBy(css = ".slider-menu li.l2-activate a[data-catid*='_All_']")
	WebElement lnkAllActiveCategory;
	
	@FindBy(css = ".email-signup-footer-success")
	WebElement lblEmailSignUpThankYou;
	
	@FindBy(css = ".slider-menu .email.icon")
	WebElement lnkEmailUs;
	
	@FindBy(css = ".slider-menu .livechat-icon a div")
	WebElement lnkChat;
	
	@FindBy(css = "#bc-chat-container")
	WebElement mdlLiveChat;
	
	@FindBy(css = ".slider-menu .quick-links a.Track-order")
	WebElement lnkTrackOrder;
	
	@FindBy(css = ".slider-menu .quick-links a.Return-items")
	WebElement lnkReturnItems;
	
	@FindBy(css = ".slider-menu .quick-links a.Update-information")
	WebElement lnkUpdateInfo;
	
	@FindBy(css = FOOTER_MOBILE + ".footer-nav .primary-links.hide-desktop.hide-tablet h2 a")
	WebElement btnCustomerService;
	
	@FindBy(css = ".top-banner .menuUnderLay")
	WebElement divHamburgerOverlay; 
	
	@FindBy(css = ".hamburger-help a")
	WebElement iconHelp;

	//================================================================================
	//			WebElements Declaration End
	//================================================================================


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public HamburgerMenu(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, lnkHamburgerOpened))) {
			Log.fail("Hamburger menu didn't display", driver);
		}
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the element which have exact variable name
	 * @param expectedEle - String "Expected Element"
	 * @return WebElement - Actual WebElement with expected Element variable name
	 * @throws Exception - Exception
	 */
	public static WebElement getElement(String expectedEle)throws Exception{
		WebElement element = null;
		try{
			Field elementField = HamburgerMenu.class.getClass().getDeclaredField(expectedEle);
			elementField.setAccessible(true);
			element = ((WebElement) elementField.get(HamburgerMenu.class.getClass()));
		}//try
		catch(Exception e){
			e.printStackTrace();

		}//catch
		return element;
	}

	/**
	 * To open/close the hamburger menu in mobile
	 * @param state - 
	 * @return object of pages
	 * @throws Exception - Exception
	 */
	public Object openCloseHamburgerMenu(String state) throws Exception {
		if (Utils.waitForElement(driver, lnkHamburgerClosed, 2) && (state.equals("open"))) {
			BrowserActions.javascriptClick(lnkHamburger, driver, "Hamburger menu ");
			Log.event("Hamburger menu opened");
			return new HamburgerMenu(driver).get();
		} else if(Utils.waitForElement(driver, lnkHamburgerOpened, 2) && (state.equals("close"))){
			BrowserActions.javascriptClick(lnkHamburger, driver, "Hamburger menu ");
			Log.event("Hamburger menu closed");
			return new Headers(driver).get();
		} else if (Utils.isTablet()){
			Log.event("The tablet orientation is landscape");
			return null;
		} else {
			return new Headers(driver).get();
		}
	}
	
	/**
	 * TO scroll to footer page
	 * @throws InterruptedException - 
	 */
	public void scrollToDown() throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", divFooterCustomerService);
		Log.event("Scrolled down to Customer Service in Hambuger menu!");
	}
	
	/**
	 * To click on the Email SignUp button
	 * @throws Exception - Exception
	 */
	public void clickOnEmailSignUp()throws Exception{
		BrowserActions.scrollToView(btnEmailSignUp, driver);
		BrowserActions.clickOnElementX(btnEmailSignUp, driver, "Email Sign Up ");
		
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To get the error message from Email signup error text
	 * @return String - Error Message
	 * @throws Exception - Exception
	 */
	public String getEmailSignUpErrMsg()throws Exception{
			return Utils.waitForElement(driver, txtEmailSignUpError) == true ?
					BrowserActions.getText(driver, txtEmailSignUpError, "Email SignUp Error Message ") :
						"No Error Message displayed on the page.";
	}
	
	/**
	 * To type the email address into email signup textbox
	 * @param txtToType - 
	 * @throws Exception - Exception
	 */
	public void typeInEmailSignUp(String txtToType)throws Exception{
		BrowserActions.scrollInToView(fldEmailSignUp, driver);
		BrowserActions.typeOnTextField(fldEmailSignUp, txtToType, driver, "Email SignUp in Footer ");
	}
	
	/**
	 * To click on sign in
	 * @return SignIn - 
	 * @throws Exception - Exception
	 */
	public SignIn navigateToSignIn()throws Exception{
		Log.event("Clicking on SignIn button in Hamburger menu.");
		BrowserActions.clickOnElementX(lnkMyAccount, driver, "Account Link ");
		return new SignIn(driver).get(); 
	}

	/**
	 * To navigate to Offers page
 	 * @return MyAccountPage object
	 * @throws Exception - Exception
	 */
	public MyAccountPage navigateToMyAccount()throws Exception{
		//Have to click on Overview after clicking on Account to navigate to My Account Overview page.
		BrowserActions.clickOnElementX(lnkMyAccount, driver, "My Account Link ");
		BrowserActions.clickOnElementX(lnkProfile, driver, "Profile ");
		Utils.waitForPageLoad(driver);
		return new MyAccountPage(driver).get();
	}
	
	/**
	 * Verify level 1 category is available or not without level 2
	 * @return true
	 * @throws exception
	 */
	public boolean verifyLevel1CategoryWithoutLevel2Availability() throws Exception {
		int ele1st;
		if(lnkMenuItemsNoSubMenu.size() > 0) {
			for (ele1st = 0; ele1st < lnkMenuItemsNoSubMenu.size(); ele1st++) {
				if (!lnkMenuItemsNoSubMenu.get(ele1st).getText().contains("CATALOG")) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * To verifyrightArrowDisplyedOrNot for the categories and 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyArrowWhenChildCategoriesPresent()throws Exception{
		int ele1st;
		for(ele1st=0 ; ele1st<lnkMenuItems.size() ; ele1st++){
			try{
				WebElement ele = lnkMenuItems.get(ele1st).findElement(By.cssSelector(".slider-menu .menu-item-toggle"));
				Log.event("Arrow found for category menu item :: " + ele1st);
				BrowserActions.clickOnElementX(ele, driver, "on Category");
				if(Utils.waitForElement(driver, lblActiveMenuHeading)){
					return true;
				}
			}catch(NoSuchElementException ex){
				Log.event("Element not found.");
			}
		}
		return false;
	}
	
	/**
	 * To verifyrightArrowDisplyedOrNot for the categories and 
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyArrowWhenChildCategoriesNotPresent() throws Exception {
		int ele1st;
		if(lnkMenuItemsNoSubMenu.size() > 0) {
			for (ele1st = 0; ele1st < lnkMenuItemsNoSubMenu.size(); ele1st++) {
				if (!lnkMenuItemsNoSubMenu.get(ele1st).getText().contains("CATALOG")) {
					BrowserActions.scrollInToView(lnkMenuItemsNoSubMenu.get(ele1st), driver);
					BrowserActions.clickOnElementX(lnkMenuItemsNoSubMenu.get(ele1st), driver, "clicked on category");
					Utils.waitForPageLoad(driver);
					if (!( Utils.waitForElement(driver, categoryLandingReadyElement)
						|| Utils.waitForElement(driver, plpPagereadyElement)
						|| Utils.waitForElement(driver, explorePageReadyElement)
						|| Utils.waitForElement(driver, plpNoResultPageReadyElement))) {
						return false;
					} else {
						openCloseHamburgerMenu("open");
					}
				}
			}
		}
		return true;
	}
		
	/**
	 * To navigate to L2 Categories
	 * @param categoryName - 
 	 * @return HamburgerMenu object
	 * @throws Exception - Exception
	 */
	public HamburgerMenu navigateToSubCategory(String categoryName)throws Exception{
		for (WebElement ele : lnkMenuItems) {
			WebElement element = ele.findElement(By.cssSelector("a"));
			if(element.getText().trim().equalsIgnoreCase(categoryName)) {
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);
				break;
			}
		}
		Utils.waitForPageLoad(driver);
		return new HamburgerMenu(driver).get();
	}
	
	/**
	 * To check the category name in the hamburger menu is displaying
	 * @param categoryName - category name to navigate
 	 * @return true if displayed, else false
	 * @throws Exception - Exception
	 */
	public boolean checkNavigatedCategoryName(String categoryName)throws Exception{
		return BrowserActions.getTextFromAttribute(driver, lblActiveMenuHeading, "title", "Active menu").trim().equalsIgnoreCase(categoryName);
	}
	
	/**
	 * To navigate to L2 Categories
 	 * @return HamburgerMenu object
	 * @throws Exception - Exception
	 */
	public HamburgerMenu clickOnBackArrowInActiveLevel2Category()throws Exception{
		BrowserActions.javascriptClick(lblActiveMenuToggleBack, driver, "Back Arrow in Level 2");
		Utils.waitForPageLoad(driver);
		return new HamburgerMenu(driver).get();
	}
	
	/**
	 * To navigate to L2 Categories
	 * @param index - 
 	 * @return HamburgerMenu object
	 * @throws Exception - Exception
	 */
	public HamburgerMenu navigateToSubCategory(int index)throws Exception{
		WebElement element = lstCategoryArrow.get(index);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Utils.waitForPageLoad(driver);
		return new HamburgerMenu(driver).get();
	}

	/**
	 * To navigate back
 	 * @return HamburgerMenu object
	 * @throws Exception - Exception
	 */
	public HamburgerMenu clickOnBackArrow()throws Exception{
		try{
			BrowserActions.clickOnElementX(lblMyAccountHeader, driver, "Back arrow ");
			Utils.waitForPageLoad(driver);
		} catch(Exception e) {
			if(e.toString().contains("is not clickable at point"))
				Log.fail("MyAccount Header Not displayed.");
		}
		return new HamburgerMenu(driver).get();
	}
	
	/**
	 * To click on the SignIn button in Hamburger Menu
	 * @return SignIn Page
	 * @throws Exception - Exception
	 */
	public SignIn clickOnSignIn()throws Exception{
		BrowserActions.clickOnElementX(lnkMyAccount, driver, "Sign In Link ");
		Utils.waitForPageLoad(driver);
		return new SignIn(driver).get();
	}
	
	/**
	 * To click on 'my account'
	 * @throws Exception - Exception
	 */
	public void ClickOnMyAccount()throws Exception{
		BrowserActions.clickOnElementX(lnkMyAccount, driver, "Account menu button");
	}
	
	/**
	 * To verify backarrow exists
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyBackArrow()throws Exception{
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		String imageURL = (String) executor.executeScript("return window.getComputedStyle(document.querySelector('.my-account.heading'), ':before').getPropertyValue('background-image')");
		Log.event("--->>> Image URL :: " + imageURL);
		if(imageURL.contains(".svg"))
			return true;
		else
			return false;
	}
	
	/**
	 * To click on sign out
	 * @throws Exception - Exception
	 */
	public void clickOnSignOut()throws Exception{
		ClickOnMyAccount();
		BrowserActions.clickOnElementX(lnkSignOut, driver, "Sign Out link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on profile element
	 * @return ProfilePage
	 * @throws Exception - Exception
	 */
	public ProfilePage clickOnProfile()throws Exception{
		BrowserActions.clickOnElementX(lnkProfile, driver, "Profile Link");
		Utils.waitForPageLoad(driver);
		return new ProfilePage(driver).get();
	}
	
	/**
	 * To click on wish list element
	 * @return WishListPage
	 * @throws Exception - Exception
	 */
	public WishListPage clickOnWishList()throws Exception{
		BrowserActions.clickOnElementX(lnkWishList, driver, "WishListPage Link");
		Utils.waitForPageLoad(driver);
		return new WishListPage(driver).get();
	}
	
	/**
	 * To click on order history element
	 * @return TrackOrderPage
	 * @throws Exception - Exception
	 */
	public TrackOrderPage clickOnOrderHistory()throws Exception{
		BrowserActions.clickOnElementX(lnkOrderHistory, driver, "OrderPage Link");
		Utils.waitForPageLoad(driver);
		return new TrackOrderPage(driver).get();
	}

	/**
	 * To click on quick order 
	 *  @return quick Order Page
	 * @throws Exception - Exception
	 */
	public QuickOrderPage clickOnquickOrder()throws Exception{
		BrowserActions.clickOnElementX(iconQuickOrderMobile, driver, "Quick Order Link");
		Utils.waitForPageLoad(driver);
		return new QuickOrderPage(driver).get();
	}
	
	/**
	 * To click on Active category header
	 * @throws Exception - Exception
	 */
	public void clickOnActiveCat2Header()throws Exception{
		BrowserActions.clickOnElementX(activeCat2Header, driver, "Header of current Categegory 2 Nav");
	}
	
	/**
	 * To click on nth sub category element based on the brand
	 * @param int - index 
	 * @return PlpPage - PLP page object
	 * @throws Exception - Exception
	 */
	public PlpPage clickOnNthSubCategory(int index)throws Exception{
		List<WebElement> cat2 = new ArrayList<WebElement>();
		cat2 = driver.findElements(By.cssSelector(".slider-menu li.l2-activate .nav-column .nonBoldCat a"));
		BrowserActions.clickOnElementX(cat2.get(index), driver, "Nth Sub Category");
		Utils.waitForPageLoad(driver);
		return new PlpPage(driver).get();
	}
	
	/**
	 * To click on active view all element
	 * @throws Exception - Exception
	 */
	public void clickOnActiveViewAll()throws Exception{
		BrowserActions.clickOnElementX(lnkViewAllActive, driver, "Active View All Link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on email us link
	 * @throws Exception - Exception
	 */
	public void clickOnEmailUs()throws Exception{
		BrowserActions.clickOnElementX(lnkEmailUs, driver, "Email Us Link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on chat us link
	 * @throws Exception - Exception
	 */
	public void clickOnChatUs()throws Exception{
		BrowserActions.scrollToViewElement(lnkEmailUs, driver);
		if(Utils.waitForElement(driver, lnkChat)){
			BrowserActions.javascriptClick(lnkChat, driver, "Email Us Link");
			Utils.waitForElement(driver, mdlLiveChat);	
		}else{
			Log.fail("Live Chat Link not found in Hamburger Menu", driver);
		}
		
	}
	
	/**
	 * To click on track order element
	 * @return TrackOrderPage
	 * @throws Exception - Exception
	 */
	public TrackOrderPage clickOnTrackOrder()throws Exception{
		BrowserActions.clickOnElementX(lnkTrackOrder, driver, "Track Order Link");
		Utils.waitForPageLoad(driver);
		return new TrackOrderPage(driver).get();
	}
	
	/**
	 * To click on return order items
	 * @return TrackOrderPage
	 * @throws Exception - Exception
	 */
	public TrackOrderPage clickOnReturnItems()throws Exception{
		BrowserActions.clickOnElementX(lnkReturnItems, driver, "Return Items Link");
		Utils.waitForPageLoad(driver);
		return new TrackOrderPage(driver).get();
	}
	
	/**
	 * To click on update info link
	 * @return ProfilePage
	 * @throws Exception - Exception
	 */
	public ProfilePage clickOnUdateInfo()throws Exception{
		BrowserActions.clickOnElementX(lnkUpdateInfo, driver, "Update Info Link");
		Utils.waitForPageLoad(driver);
		return new ProfilePage(driver).get();
	}
	
	/**
	 * Navigate to customer service page on tablet
	 * @return
	 * @throws Exception
	 */
	public CustomerService navigateToCustomerService() throws Exception{
		if(Utils.waitForElement(driver, iconHelp)){
			BrowserActions.clickOnElementX(iconHelp, driver, "Help icon in Hamburger");
			Utils.waitForPageLoad(driver);
		}		
		return new CustomerService(driver).get();
	}
	
	/**
	 * To Verify navigationOf customer service links
	 * @return
	 * @throws Exception
	 */
	public boolean verifyCustomerServiceLinksNaviagtion() throws Exception{
		List<WebElement> ele;
		if(Utils.isMobile()){
			ele=customerService_Mobile.findElements(By.cssSelector("a"));
		}else{	
			ele=customerService_Tablet.findElements(By.cssSelector("a"));
		}
		
		for(WebElement element:ele){
			String text=BrowserActions.getTextFromAttribute(driver, element, "href", "Navigation link");
			if(!text.contains("https://")){
				return false;
				}
		}		
		return true;	
	}
	
	/**
	 * To Verify navigation Of quick links
	 * @return
	 * @throws Exception
	 */
	public boolean verifyquicklinksNaviagtion() throws Exception{
		List<WebElement> ele = driver
				.findElements(By.cssSelector(".cs-flyout-quick-links.hide-desktop.hide-tablet .quick-links a"));
		for(WebElement element:ele){
			String text=BrowserActions.getTextFromAttribute(driver, element, "href", "Navigation link");
			if(!text.contains("https://")){
				return false;
			}			
		}		
		return true;	
	}
	
	/**
	 * To get account menu header text for logged in user
	 * @return account header text
	 * <br> Returns empty string for guest user
	 * @throws Exception
	 */
	public String getAccountMenuHeader() throws Exception {
		if (!Utils.waitForElement(driver, divAccountMenu)) {
			BrowserActions.clickOnElementX(lnkAccountButton, driver, "account button");
		}
		return BrowserActions.getTextNoFail(driver, lblMyAccountHeader, "hamburger account menu header for logged in user");
	}
	
	/**
	 * To verify the right arrow for category hamburger category
	 * @return
	 * @throws Exception
	 */
	public boolean verifyCategoryRightArrowDisplayed() throws Exception {
		for(WebElement ele : mainCategories) {
			String imgURL = elementLayer.getCssPropertyForPsuedoElement(ele, ":after", "background");
			if(!imgURL.contains("icon_rightarrow.svg")) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify the back arrow for category header
	 * @return backArrowDisplayed - True if element displayed else false
	 * @throws Exception
	 */
	public boolean verifyCategoryBackArrowDisplayed() throws Exception {
		boolean backArrowDisplayed = false;
		String imgURL = elementLayer.getCssPropertyForPsuedoElement(lblActiveMenuHeading, ":after", "background");
		if(imgURL.contains("icon_leftarrownavsec.svg")) {
			backArrowDisplayed = true;
		}
		return backArrowDisplayed;
	}
	
}