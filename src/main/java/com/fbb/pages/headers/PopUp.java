package com.fbb.pages.headers;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

/**
 * Class dedicated to handle Pop-ups
 * <br>
 * Expand this class as necessary to handle Pop-ups
 */
public class PopUp {
	
	private WebDriver driver;
	
	/*************Pop-up modals**************/
	
	@FindBy(css = "form > input[name*='dwfrm_emailsignup_email_']")
	WebElement txtMonetateSignup;
	
	@FindBy(css = "div#monetate_lightbox_content_container")
	WebElement mdlMonetate_LightBox;
	
	@FindBy(css = "#email-signup-modal")
	WebElement mdlEmailSignup;
	
	@FindBy(css = "div#mnt-mobile-email-signup")
	WebElement mdlEmailSignupMobile;
	
	@FindBy(css = "#content-popup")
	WebElement mdlContentPopup;

	/*************Close buttons**************/
	
	@FindBy(css = ".mt_clickzone.close")
	WebElement btnMonetateClose;
	
	@FindBy(css = "area[title='close']")
	WebElement btnMonetateCloseAlt;
	
	@FindBy(css = "button[class*='close']")
	WebElement btnPopupClose;

	@FindBy(css = "div#monetate_lightbox_content_container .close")
	WebElement btnExitStartupModel;
	
	@FindBy(css = "#mnt-mobile-email-signup > .mnt-mobile-email-close")
	WebElement mdlEmailSignupCloseMobile;
	
	/*************WebElement End********************/
	
	public PopUp(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}
	
	/**
	 * To detect if Pop-up is being displayed, and close it.
	 * @throws Exception
	 */
	public void closeLightboxPopup() throws Exception{
		if (Utils.isElementAvailable(mdlMonetate_LightBox)) {
			Log.event("Monetate popup displayed.");
			try {
				if (Utils.isElementAvailable(btnMonetateCloseAlt)) {
					BrowserActions.clickOnElementX(btnMonetateCloseAlt, driver, "btnMonetateCloseAlt button");
				} else if (Utils.isElementAvailable(btnMonetateClose)) {
					BrowserActions.clickOnElementX(btnMonetateClose, driver, "btnMonetateClose button");
				} else if (Utils.isElementAvailable(btnExitStartupModel)) {
					BrowserActions.clickOnElementX(btnExitStartupModel, driver, "btnExitStartupModel button");
				} else {
					Actions keyAction = new Actions(driver);
					keyAction.keyDown(Keys.ESCAPE).build().perform();
				}
				Utils.waitUntilElementDisappear(driver, mdlMonetate_LightBox, 5);
				Log.event("Monetate popup closed.");
			} catch(Exception e) {
				Log.message("Failed to close Monetate popup.", driver);
				Log.event(e.getLocalizedMessage());
			}
		} else if (Utils.isElementAvailable(mdlContentPopup)) {
			Log.event("Pop-up displayed");
			try {
				if (Utils.isElementAvailable(btnPopupClose)) {
					BrowserActions.clickOnElementX(btnPopupClose, driver, "Pop-up close button");
				} else {
					Actions keyAction = new Actions(driver);
					keyAction.keyDown(Keys.ESCAPE).build().perform();
				}
				Utils.waitUntilElementDisappear(driver, mdlContentPopup, 5);
				Log.event("Pop-up closed.");
			} catch(Exception e) {
				Log.message("Failed to close Pop-up modal.", driver);
				Log.event(e.getLocalizedMessage());
			}
		} else if (Utils.isMobile() && Utils.isElementAvailable(mdlEmailSignupMobile)) {
			Log.event("Email sign-up mobile pop-up displayed");
			try {
				if(Utils.isElementAvailable(mdlEmailSignupCloseMobile)) {
					BrowserActions.clickOnElementX(mdlEmailSignupCloseMobile, driver, "Pop-up close button");
				}
				Utils.waitUntilElementDisappear(driver, mdlEmailSignupMobile, 5);
				Log.event("Pop-up closed.");
			} catch (Exception e) {
				Log.message("Failed to close Mobile Pop-up modal.", driver);
				Log.event(e.getLocalizedMessage());
			}
		}
	}

}
