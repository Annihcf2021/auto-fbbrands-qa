package com.fbb.pages.headers;

import java.awt.Robot;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.CreateAccountPage;
import com.fbb.pages.ElementLayer;
import com.fbb.pages.Explore;
import com.fbb.pages.HomePage;
import com.fbb.pages.MiniCartPage;
import com.fbb.pages.OfferPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlatinumCardApplication;
import com.fbb.pages.PlatinumCardLandingPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.SearchResultPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.SignIn.UserAccount;
import com.fbb.pages.CSR.CSR;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.Enumerations.Toggle;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.BuildProperties;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StopWatch;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;

public class Headers extends LoadableComponent<Headers> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public HamburgerMenu hamburgerMenu;
	
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	private static EnvironmentPropertiesReader envProperty = EnvironmentPropertiesReader.getInstance("env");
	
	String plccHeaderMessage = demandwareData.get("plcc_header_message_short");
	String plccHeaderPromoMessage = demandwareData.get("plcc_header_promo_message");
	
	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	//--------------Header Brand Navigation ELements-------------------

	private static final String WRAPPER = "#wrapper ";
	private static final String HEADER = WRAPPER + "#header ";
	private static final String SLIDER_MENU_MOBILE = ".slider-menu ";
	private static final String HEADER_NAV_MOBILE = SLIDER_MENU_MOBILE + "#navigation-bar ";
	private static final String lISTOFSEARCHSUGGESTIONS = ".product-suggestions";

	private static final String MENU_UTIL = HEADER + ".top-menu-utility ";
	private static final String MENU_UTIL_BRAND_CONTENT_MOBILE = MENU_UTIL + ".header-brand-selector .content-asset .brand-logos.hide-desktop ";

	private static final String MENU_UTIL_USER_DESKTOP = ".right-panel-inner .right-panel-pane-login ";
	private static final String MENU_UTIL_REG_USER_DESKTOP = ".right-panel-inner .right-panel-pane-login ";
	private static final String MENU_UTIL_USER_MOBILE = HEADER_NAV_MOBILE + ".user-info ";

	private static final String MENU_UTIL_MINICART_DESKTOP = ".right-panel-inner .right-panel-pane-cart ";

	private static final String HEADER_NAV_LEVEL1 = "#navigation .menu-category.level-1 li ";

	private static final String HEADER_SUB_CATEGORY_l2 = ".hover~.level-2 .hide-mobile>ul>li>ul>li>a";
	private static final String HEADER_SUB_CATEGORY_l2_HAS_L3 = ".hover~.level-2 .hide-mobile ul>li>a~.level-3";
	private static final String HEADER_SUB_CATEGORY_l3 = "//following-sibling::ul[@class='level-3']/li/a";
	
	private static final String MONETATE_BANNER = "div#monetate_lightbox_content_container ";
	
	private static final String SECONDARY_HEADER = ".secondary-header ";

	//-----------------------------------------------------------------

	//=====================================================================//
	//	------------------Desktop Elements-----------------------------    //
	//=====================================================================//

	@FindBy(css = HEADER)
	WebElement headerPanel;

	@FindBy(css = WRAPPER)
	WebElement wrapper;
	
	@FindBy(css = ".pt_product-details")
    WebElement cntPdpContent;

	@FindBy(css = ".pt_cart")
	WebElement readyElementShoppingBagPage;
	
	@FindBy(css = MENU_UTIL)
	WebElement utilityBarDesktop;

	@FindBy(css = MENU_UTIL + ".menu-utility-user span.customer-service")
	WebElement lnkCustomerServiceDesktop;

	@FindBy(css = MENU_UTIL + "span.email.icon")
	WebElement lnkEmailUsDesktop;
	
	@FindBy(css = "div#monetate_lightbox_content_container")
	WebElement topPromotionalBanner;
	
	@FindBy(css = ".catalog-quick-order")
	WebElement readyElementQuickOrder;
	
	@FindBy(css = ".customer-service-flyout .cs-flyout-phone .content-asset")
	WebElement callUsCustomerFlyoutSection;
	
	@FindBy(css = "#navigation .menu-category.level-1")
	WebElement stickyCategory;
		
	@FindBy(css = ".menu-item-toggle.fa-chevron-right")
	WebElement lnkHamburgerArrow;
	
	@FindBy(css = MENU_UTIL + ".email-us-icon a img")
	WebElement imgEmailUsDesktop;

	@FindBy(css = "#wrapper")
	WebElement fullPage;
	
	@FindBy(css = "#profilemenu")
	WebElement lblProfileMenu;

	@FindBy(css = MENU_UTIL + ".email-us-icon")
	WebElement divEmailUsDesktop;

	@FindBy(css = MENU_UTIL + ".livechat-icon a>div")
	WebElement lnkLiveChatDesktop;

	@FindBy(css = MENU_UTIL + ".livechat-icon a img")
	WebElement imgLiveChatDesktop;

	@FindBy(css = MENU_UTIL + "a.Track-order")
	WebElement lnkTrackOrderDesktop;

	@FindBy(css = MENU_UTIL + "a.Return-items")
	WebElement lnkReturnItemsDesktop;
	
	@FindBy(css = MENU_UTIL + "a.returnLink")
	WebElement lnkReturnItemsDesktopRM;
	
	@FindBy(css = ".login-box-content .error-form")
	WebElement errNoPassword;
	
	@FindBy(css = ".login-box-content .create-account")
	WebElement btnCreateAccountMobile;

	@FindBy(css = MENU_UTIL + "a.Update-information")
	WebElement lnkUpdateMyInfoDesktop;

	@FindBy(css = ".primary-logo a[href*='ww']")
	WebElement lnkWomanWithin_desktop;

	@FindBy(css = ".primary-logo a[href*='jl']")
	WebElement lnkJessicaLondon_desktop;

	@FindBy(css = ".primary-logo a[href*='rm']")
	WebElement lnkRomans_desktop;
	
	@FindBy(css = ".primary-logo a[href*='ks']")
	WebElement lnkKingSize_desktop;
	
	@FindBy(css = ".product-suggestion")
	WebElement productSuggestion;
	
	@FindBy(css = ".product-suggestion a")
	WebElement productSuggestionATag;
	
	@FindBy(css = ".primary-logo a[href*='el']")
	WebElement lnkEllos_desktop;

	@FindBy(css = ".customer-service-flyout")
	WebElement CustomerSerivceFlyOut;
	
	@FindBy(css = ".menu-utility-user .customer-service")
	WebElement CustomerSerivceFlyOut1;

	@FindBy(css = ".brands-header-active img.selected")
	WebElement brandLogo;
	
	@FindBy(css = ".brands-header-active-mobile img.selected")
	WebElement brandLogoMobTab;
	
	@FindBy(css = ".active img[class='selected']")
	WebElement brandLogoImg;
	
	@FindBy(css = ".primary-logo a")
	WebElement lnkPrimaryLogo;

	@FindBy(css = "a.user-account")
	WebElement lnkSignInDesktop;
	
	@FindBy(css = "a.user-account span")
	WebElement lnkSignInIconDesktop;
	
	@FindBy(xpath = "//a[contains(@class,'user-account')] //span[contains(text(),'SIGN IN')]")
	WebElement txtSignIn;
	
	@FindBy(css = SECONDARY_HEADER + ".secondary-account.active")
	WebElement btnAccountButtonActive;
	
	@FindBy(css = ".secondary-header .ucp2-account-icon")
	WebElement lnkAccountButton;
	
	@FindBy(css = ".mobile-menu-utilities .ucp2-account-icon")
	WebElement lnkAccountButtonMobile;
	
	@FindBy(css = SECONDARY_HEADER + ".account-btn  span:not(.ucp2-account-icon)")
	WebElement spanAccountButton;
	
	@FindBy(css = SECONDARY_HEADER + ".secondary-card a")
	WebElement lnkCardPLCC;
	
	@FindBy(css = ".secondary-header-overlay-container.active")
	WebElement divAccountMenu;

	@FindBy(css = ".top-menu-utility span.sign-in")
	WebElement lnkSignInDesktop1;

	@FindBy(css = ".slider-menu a.user-account.login")
	WebElement lnkMyAccount_mobile;

	@FindBy(css = ".slider-menu .account-links a")
	WebElement lnkProfile_mobile;
	
	@FindBy(css = SECONDARY_HEADER + ".deals-xs  a")
	WebElement lnkOffer;
	
	@FindBy(css = "a.offer-flyout span.offers")
	WebElement textOffers;
	
	@FindBy(css = "a.offer-flyout span.offer-flyout")
	WebElement offersBrandLogo;
	
	@FindBy(css = ".header-offers .offers-flyout-banner")
	WebElement offersFlyout;
	
	@FindBy(css = ".offers-flyout-banner .oc-offer-content")
	WebElement promotionalBannerOffersFlyout;
	
	@FindBy(css = ".offers-flyout-banner a img")
	WebElement promoBannerImageInOffersFlyout;
	
	@FindBy(css = ".offers-flyout-banner section a")
	WebElement promoBannerNoImageOfferInFlyout;
	
	@FindBy(css = ".offers-flyout-banner .viewall-offers a")
	WebElement viewAllOffersAndCouponsButton;	
		
	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".account-links a")
	List<WebElement> lnkSignedInMenuOptions;

	@FindBy(css = ".user-panel.registered-user.user-display .account-links .Profile")
	WebElement lnkMyAccountProfile_Mobile;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + "input[id*='dwfrm_login_username_']")
	WebElement fldUserNameDesktop;

	@FindBy(css = ".input-text")
	List<WebElement> txtfldUserNameDesktop;
	
	@FindBy(css = ".error-form")
	WebElement txtSignInError;
	
	@FindBy(css = "[id^='dwfrm_login_username_'][id$='error']")
	WebElement invalidEmailError;
	
	@FindBy(css = ".username.error-handle+.reset-error-caption+.emailValid ")
	WebElement invalidEmailErrorTxt;
	
	@FindBy(css = ".username.error-handle+.reset-error-caption")
	WebElement requiredEmailErrorTxt;

	@FindBy(css = ".input-text.required.error")
	WebElement txtfldPasswordDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + "input[id*='dwfrm_login_password_']")
	WebElement fldPasswordDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + "button.pwd-show")
	WebElement lnkShowHidePassword;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + "#loginFlyout")
	WebElement btnFlyoutSignIn;
	
	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".create-account-section a")
	WebElement btnFlyoutCreateAccount;

	@FindBy(css = ".top-menu-utility .user-info.info>a")
	WebElement btnSignInDesktopClick;

	@FindBy(css = ".user-account")
	List<WebElement> btnSignInDesktopNew;

	@FindBy(css = "//input[contains(@id,'dwfrm_login_username')]/following-sibling::span[contains(@id,'error')]")
	WebElement txtUserNameErrorDesktop;

	@FindBy(css = "span[id*='dwfrm_login_username'][class='error']")
	WebElement lblUserNameErrorDesktop;

	@FindBy(xpath = "//input[contains(@id,'dwfrm_login_password')]/following-sibling::span[contains(@id,'error')]")
	WebElement txtPasswordErrorDesktop;
	
	@FindBy(css = "label[for*='dwfrm_login_password'] span")
	WebElement lblPasswordInSignInFlyOut;

	@FindBy(css = "show-text label[for*='dwfrm_login_password'] .label-text")
	WebElement lblPasswordErrorDesktop;
	
	@FindBy(css = "#dwfrm_login .error-form")
	WebElement lblEmptyPasswordError;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + "#password-reset")
	WebElement lnkForgetPasswordDesktop;

	@FindBy(css = "#main .login-box.login-account")
	WebElement divLoginBoxDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.unregistered-user")
	WebElement flytSignInUnregisteredDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + "#dwfrm_login_rememberme")
	WebElement chkRememberMeDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.registered-user")
	WebElement flytSignInRegisteredDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.unregistered-user .login-data .login-email")
	WebElement lblOtherBrandLoginMsgDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.unregistered-user .login-data .login-brands")
	WebElement lblOtherBrandLoginBrandsDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + "#loggedInUserName")
	WebElement lblUserNameDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".my-account")
	WebElement lblMyAccDesktop;

	@FindBy(css = SECONDARY_HEADER + ".secondary-account .account-btn span:nth-child(2)")
	WebElement lblhlloMyAccDesktop;
	
	@FindBy(css = MENU_UTIL_USER_DESKTOP)
	WebElement lblUserAccDesktop;

	@FindBy(css = ".login-user .my-account")
	WebElement lblUserAccTablet;

	@FindBy(css = ".mobile-sign-in.hide-tablet .user-account >span:nth-child(2)")
	WebElement lblUserAccMobile;	

	@FindBy(css = ".user-info .user-account.login")
	WebElement lnkUserAccMobile;

	@FindBy(xpath = "//div[@class='account-links']/a[1]")
	WebElement lnkHamburgerOverview;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.unregistered-user #Facebook")
	WebElement btnFaceBookInSignInFlyoutDesktop;

	@FindBy(css = SECONDARY_HEADER + ".secondary-account .account-btn .ucp2-account-icon")
	WebElement iconUserDesktop;

	@FindBy(css = "#primary>h1 .account-logout")
	WebElement lnkSignOutDesktop;

	@FindBy(css = ".hide-desktop.hide-tablet.back-arrow")
	WebElement lnkArrowBreadCrumb_mobile;

	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement txtBreadCrumb_mobile;

	@FindBy(css = "div.breadcrumb")
	WebElement breadCrumb;
	
	@FindBy(css = ".breadcrumb .back-arrow")
	WebElement breadcrumbMobileArrow;
	
	@FindBy(css = ".breadcrumb .breadcrumb-slash")
	WebElement breadcrumbDivider;

	@FindBy(css = ".breadcrumb-element")
	List<WebElement> breadcrumbElements;
	
	@FindBy(css = ".breadcrumb-element")
	WebElement breadcrumbElement;

	@FindBy(css = ".promo-banner")
	WebElement divPromoContent;

	@FindBy(css = MENU_UTIL_MINICART_DESKTOP + ".mini-cart-link")
	WebElement lnkViewBagDesktop;
	
	@FindBy(css = MENU_UTIL_MINICART_DESKTOP + ".mini-cart-link span")
	WebElement lnkViewBagIconDesktop;
	
	@FindBy(xpath = "//a[contains(@class,'mini-cart-link minicart-qty mini-cart-empty')]//span[contains(@class,'my-bag')]")
	WebElement textMyBag;
	
	@FindBy(css = SECONDARY_HEADER + ".shopping-bag-sm a")
	WebElement lnkMiniCart;
	
	@FindBy(css = SECONDARY_HEADER + ".shopping-bag-sm .minicart-quantity")
	WebElement miniCartQty;
	
	@FindBy(css = MENU_UTIL_MINICART_DESKTOP + ".mini-cart-total a.mini-cart-link")
	WebElement lnkMiniCartTablet;
	
	@FindBy(css = MENU_UTIL_MINICART_DESKTOP + ".mini-cart-total .minicart-quantity")
	WebElement lblMiniCartCountDesktop;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.unregistered-user .create-account-section a span")
	WebElement lnkCreateNewAccount;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.unregistered-user .create-account-section .new-customer")
	WebElement lblCreateNewAccount;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".user-panel.unregistered-user .create-account-section .new-customer")
	WebElement lblNewCustomer;

	@FindBy(css = ".create-message")
	WebElement lblNewCustomerCreateMessage;

	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a.Profile")
	WebElement lnkProfileMyAccFlytDesktop;

	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a.Wishlist")
	WebElement lnkWishListMyAccFlytDesktop;
	
	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a.Rewards")
	WebElement lnkQuickOrderMyAccFlytDesktop;

	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a.Orderhistory")
	WebElement lnkOrderHistoryMyAccFlytDesktop;
	
	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a.wallet")
	WebElement lnkSavedPaymentMyAccFlytDesktop;

	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a.Rewards")
	WebElement lnkRewardPointsMyAccFlytDesktop;

	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a.signout")
	WebElement lnkSignOutMyAccFlytDesktop;
	
	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a.addressbook")
	WebElement lnkAddressbookMyAccFlyt;
	
	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a.catalog-preferences")
	WebElement lnkCatalogPrefsMyAccFlyt;
	
	@FindBy(css = MENU_UTIL_REG_USER_DESKTOP + "a.email-preferences")
	WebElement lnkEmailPrefsMyAccFlyt;

	@FindBy(css = ".login-user .my-account")
	WebElement lnkMyAccMobile;

	@FindBy(css = ".slider-menu a.Wishlist")
	WebElement lnkWishListMobile;

	@FindBy(css = MENU_UTIL_MINICART_DESKTOP + ".mini-cart-content")
	WebElement flytMyBag;

	@FindBy(css = MENU_UTIL_USER_DESKTOP + ".serverSide-errorMessage.hide")
	WebElement lblServerSideLoginError;

	@FindBy(css = ".right-section #mini-cart .button.mini-cart-link-cart")
	WebElement btnCheckOut;

	@FindBy(css = ".customer-service-info.info span.customer-service")
	WebElement custService;

	@FindBy(css = "pt_account null")
	WebElement lnksigninpage;
	
	@FindBy(css = ".menu-category.level-1 a.has-sub-menu:not([href*='/quick-order'])")
	List<WebElement> lnkGlobalNavCategories;
	
	@FindBy(css = ".menu-category.level-1 a.nonBoldCat")
	WebElement lnkGlobalNavNonBildCategories;
	
	@FindBy(css = "li.l2-activate .col-1>.nav-viewAll")
	WebElement btnActiveViewAll;
	
	@FindBy(css = "#category-level-2 :not([class*='cr-section']) .refinement-link")
	List<WebElement> subCategories;
	
	@FindBy(xpath = "//ul[@class='menu-category level-1']// a[contains(@class,'has-sub-menu boldCat')]")
	List<WebElement> lnkGlobalCategories;
	
	@FindBy(xpath = "//ul[@class='menu-category level-1']// a[contains(@class,'has-sub-menu boldCat')]")
	WebElement lnkGlobalboldCategories;	
	
	@FindBy(css = ".selected .has-sub-menu")
	WebElement navGlobalNavigationSelected;
	
	@FindBy(css = "#mini-cart .my-bag")
	WebElement lnkMyBag_Tablet;
	
	@FindBy(css = "span.cqo_text")
	WebElement spanCatalogQuickOrder;
	
	@FindBy(css = ".fixed-header .quick-order img")
	WebElement stickyCqoIcon;
	
	@FindBy(css = ".quick-order div.iconHolder>a")
	WebElement lnkCatalogQuickOrder;
	
	@FindBy(xpath = "//*[@class=\"promo-banner\"] //a[@href=contains(text(),\"Apply Now\")]")
	WebElement applyNow;	

	//=====================================================================//
	//	-------------------Mobile Elements-----------------------------    //
	//=====================================================================//

	@FindBy(css = MENU_UTIL_BRAND_CONTENT_MOBILE + ".woman-within")
	WebElement lnkBrand1_mobile;

	@FindBy(css = MENU_UTIL_BRAND_CONTENT_MOBILE + ".jessica")
	WebElement lnkBrand2_mobile;

	@FindBy(css = "button.menu-toggle")
	WebElement lnkHamburger;

	@FindBy(css = "body.header-mobile-menu-open")
	WebElement lnkHamburgerOpened;
	
	@FindBy(css = "body:not(.header-mobile-menu-open)")
	WebElement lnkHamburgerClosed;
	
	@FindBy(css= "li.l2-activate .sub-cat-nav")
	WebElement lvl2Navigation;

	@FindBy(css = MENU_UTIL_BRAND_CONTENT_MOBILE + ".roman")
	WebElement lnkBrand3_mobile;

	@FindBy(css = ".user-info  a.user-account .sign-in")
	WebElement lnkSignInMobile;

	@FindBy(css = MENU_UTIL_USER_MOBILE + "a.user-account.login")
	WebElement lnkMyAccountMobile;

	@FindBy(css = ".user-info #loggedInUserName")
	WebElement lblUserNameMobile;

	@FindBy(css = ".login-user .my-account")
	WebElement lblMyAccMobile;

	@FindBy(css = ".account-logout a")
	WebElement lnkMyAccSignOutMobile;

	@FindBy(css = MENU_UTIL_USER_MOBILE + ".log-in")
	WebElement iconUserMobile;

	@FindBy(css = MENU_UTIL + ".mini-cart-link")
	WebElement lnkViewBagMobile;

	@FindBy(css = SECONDARY_HEADER + "[class*='search'] a")
	WebElement btnHeaderSearchIcon;

	@FindBy(css = ".shopping-bag-sm span.ucp2-cart-icon")
	WebElement iconMyBag;

	@FindBy(css = ".cart-bag-qty")			
	WebElement iconMyBagQty;
	
	@FindBy(css = ".cart-bag")			
	WebElement iconMyBagWoQty;
	
	@FindBy(css = "#mini-cart .my-bag")			
	WebElement minicartMyBag;

	@FindBy(xpath = "//a[contains(text(),'QUICK ORDER')]")
	WebElement iconQuickOrderMobile;
	
	@FindBy(css = SECONDARY_HEADER + ".secondary-catalog a")
	WebElement quickOrederIcon;
	
	@FindBy(css = "#rm-catalog-icon-mobile a")
	WebElement quickOrederIcon_rm;
	
	@FindBy(css = ".header-promo-bottom .promo-banner")
	WebElement bottomPromotionalBanner;

	@FindBy(css = ".mini-cart-link .minicart-quantity")
	WebElement lblMiniCartCountMobile;

	@FindBy(css = ".top-banner")
	WebElement stickyHeader;

	@FindBy(css = ".menu-category > li:nth-child(n)")
	List<WebElement> rootCategories;

	@FindBy(css = "#header input[name='q']")
	WebElement txtSearch;

	@FindBy(css = ".hitgroup .hit")
	List<WebElement> lstBrandNameInSearchSuggestion;

	@FindBy(css = ".search-suggestion-wrapper")
	WebElement autoSuggestPopup;

	@FindBy(css = ".phrase-suggestions h4")
	WebElement lstPhraseTitleInSearchSuggestion;

	@FindBy(css = ".phrase-suggestions a")
	WebElement lnksearch;

	@FindBy(css = ".search-phrase a span.original")
	WebElement lnkSearchOriginal;

	@FindBy(css = ".phrase-suggestions a.hit")
	List<WebElement> lstCategoriesInSearchSuggestion;

	@FindBy(css = ".phrase-suggestions .header")
	List<WebElement> lstPhraseSuggestionSubHeaders;

	@FindBy(css = ".product-suggestions .product-suggestion .product-link .product-details .product-name")
	List<WebElement> lstProductSuggestions;

	@FindBy(css = ".product-suggestion")
	List<WebElement> lstProductSuggestion;

	@FindBy(css = ".product-suggestion .product-image > img")
	WebElement lstProductImageInSuggestion;

	@FindBy(css = ".product-suggestion .product-name")
	List<WebElement> lstProductNameInSearchSuggestion;

	@FindBy(css = ".product-suggestion .product-name")
	List<WebElement> lstProductPriceInSuggestion;

	@FindBy(css = ".product-suggestion .product-image > img")
	List<WebElement> lstProductImage;

	@FindBy(css = ".product-suggestion .product-name")
	List<WebElement> lstProductName;

	@FindBy(css = ".product-suggestion .product-price")
	List<WebElement> lstProductPrice;

	@FindBy(css = ".simplesearch fieldset > button[type='submit']")
	WebElement btnFlyoutSearch;
	
	@FindBy(css = ".search-phrase>a")
	WebElement lnkSearchPhrase;

	@FindBy(css = ".phrase-suggestions .hitgroup")
	WebElement lstPhraseSuggestionGroups;

	@FindBy(css = ".product-suggestion:nth-child(2)")
	WebElement lstProductSuggestionSingle;

	@FindBy(css = ".phrase-suggestions .hitgroup:nth-child(1)")
	WebElement lstPhraseSuggestionGroupsSingle;

	@FindBy(css = lISTOFSEARCHSUGGESTIONS)
	WebElement divProductSuggestions;

	@FindBy(css = ".phrase-suggestions")
	WebElement divPhraseSuggestions;

	@FindBy(css = ".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable")
	WebElement mdlResetPassword;

	@FindBy(css = "#dialog-container #PasswordResetForm #dwfrm_requestpassword_resetemail")
	WebElement txtEmailInResetPasswordMdl;

	@FindBy(css = "#dialog-container #PasswordResetForm #dwfrm_requestpassword_resetemail-error")
	WebElement lblEmailErrorInResetPassword;

	@FindBy(css = ".top-menu-utility .woman-within img")
	WebElement lnkWomanWithinBrandImage;

	@FindBy(css = ".top-menu-utility .jessica img")
	WebElement lnkJesiccaBrandImage;

	@FindBy(css = ".scroll-wrapper .brand-logos .woman-within img")
	WebElement lnkWomanWithinBrandImage_mobile;

	@FindBy(css = ".scroll-wrapper .brand-logos .jessica img")
	WebElement lnkJesiccaBrandImage_mobile;

	@FindBy(css = ".scroll-wrapper .brand-logos .roman img")
	WebElement lnkRoamansBrandImage_mobile;

	@FindBy(css = ".top-menu-utility .roman img")
	WebElement lnkRoamansBrandImage;

	@FindBy(css = ".product-suggestion .product-image img")
	List<WebElement> lstProductImageInSearchSuggestion;

	@FindBy(css = "input#q")
	WebElement txtSearchMobile;

	@FindBy(css = ".account-logout a")
	WebElement lnkSignOutMobile;
	
	@FindBy(css = ".menu-utility-user")
	WebElement divUtilityBar;
	
	@FindBy(css = "div.offer-flyout-main")
	WebElement divOfferFlyout;
	
	@FindBy(css = ".header-search")
	WebElement divSearchBox;
	
	@FindBy(css = ".header-promo-banner")
	WebElement divTopPromoBanner;
	
	@FindBy(css = ".menu-category.level-1")
	WebElement divGlobalNavigation;
	
	@FindBy(css = ".menu-category.level-1 li a[title='Explore']")
	WebElement lnkExplore;
	
	@FindBy(css = "//div[@class='slider-menu']//ul[@class='menu-category level-1']//li/child::a[contains(text(), 'Explore')]")
	WebElement lnkExploreMobile;

	//================================================================================
	//			Other Page Ready Elements for Verification
	//================================================================================

	@FindBy(css = ".pt_account:not(.null)")
	WebElement readyElementMyAccountPage;
	
	@FindBy(css = ".pt_account.null")
	WebElement readyElementAccountNotLoggedIn;
	
	@FindBy(css = "div[class='pt_account overview']")
	WebElement readyElementMyAccountPageDiv;

	@FindBy(css = "#primary .login-box.login-account")
	WebElement readyElementSignInPage;

	@FindBy(css = ".pt_product-search-result")
	WebElement readyElementSLPPage;
	
	@FindBy(css = ".pt_product-search-noresult")
	WebElement readyElementSLPPageNoResult;
	
	@FindBy(css = ".pt_product-search-result.product-list-page")
	WebElement readyElementPLP;
	
	//================================================================================
	//			WebElements Declaration End
	//================================================================================
	
	@FindBy(css = ".category-search input[name='q']")
	WebElement txtSeacondarySearch;

	@FindBy(xpath = ".//*[@id='navigation-bar']/ul/li[4]/a")
	WebElement lnkNavbarDresses;

	@FindBy(css = ".secondary-catalog a")
	WebElement lnkQuickOrder;
	
	@FindBy(css = ".header-secondary .catalogOrderIcon a img")
	WebElement lnkQuickorder_tablet;
	
	@FindBy(css = ".header-secondary .cqo_text")
	WebElement lnkQuickorder_tabletRM;

	@FindBy(css = ".button.mini-cart-link-cart")
	WebElement lnkCheckOut;

	@FindBy(css = "div[class='mini-cart-content']")
	WebElement divMiniCartOverlay;

	@FindBy(xpath = ".//*[@id='profilemenu']/div/div/a[3]")
	WebElement lnkOrderStatus;

	@FindBy(xpath = ".//*[@id='profilesmenu']/div/div/a[3]")
	WebElement txtLoginErrorMsg;

	@FindBy(css = "#bc-chat-container.ui-draggable")
	WebElement mdlLiveChat;
	
	@FindBy(css = "#bc-chat-container .bc-headbtn-menulist")
	WebElement btnLiveChatMenu;
	
	@FindBy(css = "#bc-chat-container .bc-headmenu")
	WebElement divLiveChatMenubar;
	
	@FindBy(css = "#bc-chat-container .bc-headbtn-close-icon")
	WebElement btnLiveChatClose;
	
	@FindBy(css = "#bc-chat-container .bc-headbtn-minimize-icon")
	WebElement btnLiveChatMinimize;
	
	@FindBy(css = ".bc-close-chatwindow ")
	WebElement closeLiveChatModal;
	
	@FindBy(css = ".x-panel-bwrap")
	WebElement btnCloseStorefrontKit;

	@FindBy(css = ".header-mian-banner")
	WebElement fixedHeader;
	
	@FindBy(css = ".fixed-header .fixed-header-wrapper")
	WebElement fixedHeaderMobTab;
	
	@FindBy(css = ".refinements.ws-sticky")
	WebElement stckyRefinement;
	
	@FindBy(css = ".refinements .filters")
	WebElement stckyRefinementFilters;
	
	@FindBy(css = ".footer-down")
	WebElement footerDown;

	//GiftcardPage

	@FindBy(xpath = "//footer//a[contains(text(),'Gift Card Balance')]")
	WebElement lnkGiftCardPage;

	@FindBy(xpath = "//a[contains(@class,'breadcrumb-element')][not(contains(@class,'current'))][last()]")
	WebElement lnkSubCategoryBC;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement lnkSubCategoryBC_Mobile;

	@FindBy(css = ".top-menu-utility")
	WebElement topMenuUtilityBar;

	@FindBy(css = "li.hoverIntent div.level-2")
	WebElement currentLevel2;
	
	@FindBy(css = "li.hoverIntent div.level-2 > .sub-cat-wrapper > .banner")
	WebElement currentLevel2Banner;
	
	@FindBy(css = "li.hoverIntent>a.has-sub-menu")
	WebElement hoveredGlobalNavigationLevel1;
	
	@FindBy(css = "div.promo-banner a[href*='/platinum-card']")
	WebElement lnkRewards;
	
	@FindBy(css = ".promo-banner>a")
	WebElement lnkRewardsAlt;
	
	@FindBy(css = ".promo-banner .promo-text")
	WebElement textPromoRewards;
	
	@FindBy(css = "div.promo-banner a[href*='/Credit-PreApproved']")
	WebElement lnkLearnMoreAlt;
	
	@FindBy(css = "div.promo-banner a[href*='/platinum']")
	WebElement lnkLearnMore;
	
	@FindBy(css = "div.promo-banner a[href*='/apply']")
	WebElement lnkApplyNow;
	
	@FindBy(css = "div.promo-banner a[href*='/apply']")
	WebElement lnkAcceptNow;
	
	@FindBy(css = "div.promo-banner a[href*='/Credit-SignInToPreApprove']")
	WebElement lnkAcceptNowAlt;
	
	@FindBy(css = "div.promo-banner a")
	WebElement lnkRewardsHeader;
	
	@FindBy(css = MONETATE_BANNER)
	WebElement divMonetateBanner;
	
	@FindBy(css = ".level-1 > li")
	List<WebElement> catL1s;
	
	@FindBy(css = ".DelayHover")
	WebElement divHoverMenu;
	
	@FindBy(css=".expand-menu div[class='icon-more']")
	WebElement moreBrandIcon;
	
	//================================================================================
	//			Universal Cart Navigation
	//================================================================================
	
	@FindBy(css = ".brands-header")
	WebElement divBrandSelector;
	
	@FindBy(css = "li[class*='brands-header']")
	List<WebElement> lstBrandSelectors;
	
	@FindBy(css = "li[class*='brands-header']:not(.brands-header-active)")
	List<WebElement> lstInactiveBrandSelectors;
	
	@FindBy(css = "li[class*='brands-header']:hover .oss-brand-title")
	WebElement brandHoverMessage;
	
	@FindBy(css = ".brands-header-active")
	WebElement divActiveBrand;
	
	@FindBy(css = ".brands-header-active-mobile")
	WebElement divActiveBrandMobTab; 
	
	@FindBy(css = "li[class*='brands-header'] .arrow-up")
	WebElement cursorHoverMessage;
	
	@FindBy(css = ".header-search.focus")
    WebElement divSearchBoxActive;
	
    @FindBy(css = ".brand-selectors-oss img[alt='roamans-logo-uc']")
    WebElement brandLogoRM;
    
    @FindBy(css = ".brand-selectors-oss img[alt='jessicalondon-logo-uc']")
    WebElement brandLogoJL;
    
    @FindBy(css = ".brand-selectors-oss img[alt='ellos-logo-uc']")
    WebElement brandLogoEL;

	/**
	 * constructor of the class
	 * @param driver - WebDriver instance
	 */
	public Headers(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, headerPanel))) {
			Log.fail("Header Panel didn't display", driver);
		}
		try {
			GlobalNavigation.closePopup(driver);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(!BuildProperties.isUCValueSet()) {
			BuildProperties.setUniversalCart(Utils.waitForElement(driver, divBrandSelector));
		}
		elementLayer = new ElementLayer(driver);
		hamburgerMenu = new HamburgerMenu(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
	}

	/**
	 * Get name of brand from header logo
	 * @return Brand name
	 * @throws Exception
	 */
	public String getBrandNameFromLogo() throws Exception {
		WebElement tileActiveBrand = Utils.isDesktop() ? divActiveBrand : divActiveBrandMobTab;
		String src = tileActiveBrand.findElement(By.cssSelector("img.selected")).getAttribute("src");
		String[] srcArray = src.split("\\/");
		String title = srcArray[srcArray.length-1];
		title = title.split("\\_")[0];
		return title;
	}
	
	private WebElement getSharedCartBrandTile(Brand brand) throws Exception{
		WebElement brandTile = null;
		if (BrandUtils.matchBrand(brand, UrlUtils.getBrandFromUrl(driver.getCurrentUrl()))) {
			brandTile = Utils.isDesktop() ? divActiveBrand : divActiveBrandMobTab;
		} else {
			try {
				BrowserActions.scrollToTopOfPage(driver);
				String locator;
				String currentURL = driver.getCurrentUrl();
				if (currentURL.contains("sfcc")) {
					locator = "a[href*='" + brand.toString() + "']";
				} else {
					if(currentURL.contains("adobe")) {
						locator = "a[href*='adobe." + brand.getConfiguration().substring(0, 4).toLowerCase() + "']";
					} else {
						locator = "a[href*='www." + brand.getConfiguration().substring(0, 4).toLowerCase() + "']";
					}
				}
				Log.event("Generated locator:: " + locator);
				brandTile = divBrandSelector.findElement(By.cssSelector(locator));
			} catch (NoSuchElementException e) {
				Log.reference(getBrandNameFromLogo() + " does not have " + brand.getConfiguration() + " configured as a Shared Cart brand");
			}
		}
		return brandTile;
	}
	
	/**
	 * To choose between brands in header
	 * @param brandToNavigate - Which brand home page to navigate
	 * @return HomePage - Home page of related brand
	 * @throws Exception - Exception
	 */
	public void chooseBrandFromHeader(Brand brandToNavigate) throws Exception {
		if (BrandUtils.compareBrandName(getActiveSharedCartBrandName().getConfiguration(), brandToNavigate.getConfiguration())) {
			Log.reference("Already on desired brand.");
			navigateToHome();
		} else if (!envProperty.get("enabledSharedCart").contains(brandToNavigate.toString())) {
			Log.failsoft(brandToNavigate.getConfiguration() + " is not yet included in Shared Cart.");
		} else {
			WebElement lnkBrandToNavigate = getSharedCartBrandTile(brandToNavigate);
			if(!Utils.isDesktop()){
				BrowserActions.clickOnElement(moreBrandIcon, driver, "+ more icon in header");
			}
			BrowserActions.clickOnElementX(lnkBrandToNavigate, driver, "Header UC " + brandToNavigate.getConfiguration());
			Utils.waitForPageLoad(driver);
		}
		Utils.setUCNavigatedBrand(driver);
	}

	/**
	 * To click on brand logo
	 * @return HomePage
	 * @throws Exception - Exception
	 */
	public HomePage clickOnBrandFromHeader() throws Exception {
		if(Utils.isDesktop()) {
			BrowserActions.clickOnElementX(brandLogo, driver, "Brand In Header Brand");
		} else {
			BrowserActions.clickOnElementX(brandLogoMobTab, driver, "Brand In Header Brand");
		}
		Utils.waitForPageLoad(driver);
		return new HomePage(driver).get();
	}
	
	/**
	 * To check Global Header Is Sticky When Scroll Down
	 * @return HomePage
	 * @throws Exception - Exception
	 */
	public boolean checkGlobalHeaderIsStickyWhenScrollDown() throws Exception {
		boolean isSticky = false;
		BrowserActions.scrollInToView(footerDown, driver);
		if(Utils.isDesktop()){
			isSticky =  Utils.verifyCssPropertyForElement(fixedHeader, "position", "fixed");
		} else {
			isSticky =  Utils.verifyCssPropertyForElement(fixedHeaderMobTab, "position", "fixed");
		}
		return isSticky;
	}
	
	/**
	 * To check Global Header Is Sticky Without Scroll Down
	 * @return HomePage
	 * @throws Exception - Exception
	 */
	public boolean checkGlobalHeaderIsStickyWithoutScrollDown() throws Exception {
		boolean isSticky = false;
		if(Utils.isDesktop()) {
			isSticky =  Utils.verifyCssPropertyForElement(fixedHeader, "position", "fixed");
		} else {
			isSticky =  Utils.verifyCssPropertyForElement(fixedHeaderMobTab, "position", "fixed");
		}
		return isSticky;
	}
	
	/**
	 * To search the provided keyword
	 * @param textToSearch - Search term
	 * @return SearchResultPage page object
	 * @throws Exception
	 */
	public SearchResultPage searchProductKeyword(String textToSearch)throws Exception {
		final long startTime = StopWatch.startTime();
		BrowserActions.scrollToTopOfPage(driver);
		toggleSearchPanel(Toggle.Open);
		BrowserActions.typeOnTextField(txtSearch, textToSearch, driver,"Search field");
		BrowserActions.clickOnElementX(btnFlyoutSearch, driver, "Serach icon");
		try{
			Utils.waitUntilElementDisappear(driver, btnFlyoutSearch);
		} catch(Exception e) {
			if(!textToSearch.isEmpty()) {
				Log.ticket("SC-6029");
				BrowserActions.clickOnElementX(btnFlyoutSearch, driver, "Search Icon ");
			}
		}
		BrowserActions.scrollToTopOfPage(driver);
		Log.event("Searched the provided product!", StopWatch.elapsedTime(startTime));
		return new SearchResultPage(driver).get();
	}

	/**
	 * To get the text from primary search input box
	 * @return Text from primary search box 
	 * @throws Exception
	 */
	public String getTextFromPrimarySearchBox()throws Exception{
		return BrowserActions.getText(driver, txtSearch, "Primary Search Textbox");
	}

	/**
	 * To type given string into search input box
	 * @param txtToType - 
	 * @return string 
	 * @throws Exception - Exception
	 */
	public String typeTextInSearchField(String txtToType)throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		toggleSearchPanel(Toggle.Open);
		Log.event("Typing text("+ txtToType +") into primary search box!");
		String txtToReturn = BrowserActions.typeOnTextField(txtSearch, txtToType, driver, "Primary Searchbox");
		Utils.waitForElement(driver, lstPhraseTitleInSearchSuggestion);
		Utils.waitForElement(driver, autoSuggestPopup);
		return txtToReturn;
	}
	
	/**
	 * To type given string into search input box
	 * @param txtToType - 
	 * @return string 
	 * @throws Exception - Exception
	 */
	public String typeTextOnSearchField(String txtToType)throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		Log.event("Typing text("+ txtToType +") into primary search box!");
		String txtToReturn = BrowserActions.typeOnTextField(txtSearch, txtToType, driver, "Primary Searchbox");
		return txtToReturn;
	}
	
	/**
	 * to click snd button from keyboard
	 * @throws Exception
	 */
	public void clickGoButton() throws Exception{
		txtSearch.submit();		
	}
	
	/**
	 * to get the text from search input box
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getEnteredTextFromSearchTextBox() throws Exception {		
		String	txtSearchTextBox = txtSearch.getAttribute("value");
		return txtSearchTextBox;
	}

	/**
	 * To click on the brand name in search suggestion with the given index
	 * @param index - 
	 * @return SearchResultPage
	 * @throws Exception - Exception
	 */
	public SearchResultPage clickOnBrandNameInSearchSuggestionByIndex(int index) throws Exception{
		BrowserActions.clickOnElementX(lstBrandNameInSearchSuggestion.get(index-1), driver, "brand name");
		return new SearchResultPage(driver).get();
	}

	/**
	 * to get the text from search suggestion
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSearchTextFromSearchSuggestion()throws Exception{
		return BrowserActions.getText(driver, lnksearch, "Seach Suggestion");
	}

	/**
	 * to verify the text from search suggestion is violet color
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySearchTexColortFromSearchSuggestion()throws Exception{
		return Utils.verifyCssPropertyForElement(lnksearch, "color", "rgba(101, 30, 64, 1)");
	}

	/**
	 * to verify the flyout on the sticky header
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyRootCategoryHoverFlyout()throws Exception{
		WebElement hasSubMenu = driver.findElements(By.cssSelector(".level-1 .has-sub-menu")).get(0);
		BrowserActions.mouseHover(driver, hasSubMenu);
		Utils.waitForPageLoad(driver);
		WebElement flyOut = hasSubMenu.findElement(By.xpath("//following-sibling::div"));
		Log.event("Hovered Category Visibility :: " + flyOut.getCssValue("visibility"));
		if(flyOut.getCssValue("visibility").equals("visible"))
			return true;
		else
			return false;
	}

	/**
	 * To Mouse hover on category based on index
	 * @param index - index number of the category
	 * @throws Exception - Exception
	 */
	public void mouseHoverOnCategoryByIndex(int index)throws Exception{
		BrowserActions.scrollToBottomOfPage(driver);
		BrowserActions.scrollToTopOfPage(driver);
		BrowserActions.mouseHover(driver, rootCategories.get(index));
		BrowserActions.mouseHover(driver, rootCategories.get(index));
		Log.event("Mouse hovered on " + index + "th Category.");
		Utils.waitForElement(driver, currentLevel2);
	}

	/**
	 * To Mouse hover on category based on category name
	 * @param CategoryName - Category name to hover 
	 * @throws Exception - Exception
	 */
	public void mouseHoverOnCategoryByCategoryName(String CategoryName)throws Exception{
		if(CategoryName.contains("|")) {
			CategoryName = CategoryName.split("\\|")[0];
		}
		BrowserActions.scrollToBottomOfPage(driver);
		BrowserActions.scrollToTopOfPage(driver);

		WebElement category = null;
		try {
			category = getLevel1Category(CategoryName);
		} catch (NoSuchElementException ex) {
			Log.failsoft("The given Category "+CategoryName+" is not present");
		} catch (Exception e) {
			Log.failsoft("Unexpected error");
		}

		BrowserActions.mouseHover(driver, category);
		BrowserActions.mouseHover(driver, category);

		Log.event("Mouse hovered on " + CategoryName + " Category.");
		Utils.waitForElement(driver, currentLevel2);
	}
	
	/**
	 * To verify quick order page is navigated
	 * @return boolean - true when display, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyNavigatedToQuickOrderBySearchingKeyword()throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		toggleSearchPanel(Toggle.Open);
		BrowserActions.typeOnTextField(txtSearch, "Quick Order", driver,"Search field");
		BrowserActions.clickOnElementX(btnFlyoutSearch, driver, "Serach icon");
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, readyElementQuickOrder)) {
			return true;
		}
		return false;
	}
	
	/**
	 * To verify pdp page is navigated
	 * @return boolean - true when display, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyNavigatedToPdp(String prodIdToSearch)throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		toggleSearchPanel(Toggle.Open);
		String product = prodIdToSearch.split("\\|")[0];
		BrowserActions.typeOnTextField(txtSearch, product, driver,"Produc ID");
		BrowserActions.clickOnElementX(btnFlyoutSearch, driver, "Serach icon");
		Utils.waitForPageLoad(driver);
		if (Utils.waitForElement(driver, readyElementSLPPage)){
			try {
				WebElement proElem = driver.findElement(By.cssSelector("div[data-itemid='"+product+"']"));
				BrowserActions.clickOnElementX(proElem, driver, "Product");
				Utils.waitForPageLoad(driver);
			} catch (NoSuchElementException e){
				return false;
			}
		}
		if (Utils.waitForElement(driver, cntPdpContent)) {
			PdpPage pdpPage = new PdpPage(driver).get();
			if (prodIdToSearch.split("\\|").length == 1) {
				return true;
			} else if (pdpPage.verifyBadgeAndSwatchesDisplay(prodIdToSearch)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * To verify pdp page is navigated using url
	 * @return boolean - true when display, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyNavigateToPDPUsingUrl(String url)throws Exception {
		try {
			driver.get(url);
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, cntPdpContent)) {
			return true;
		}
		return false;
	}

	/**
	 * to click the text from search suggestion and navigate to Search Results page
	 * @return SearchResultPage
	 * @throws Exception - Exception
	 */
	public SearchResultPage clickSearchTextFromSearchSuggestion()throws Exception{
		BrowserActions.javascriptClick(lnksearch, driver, "Search Suggestion");
		return new SearchResultPage(driver).get();
	}

	/**
	 * To hover the mouse pointer on the sign in button
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean mouseOverAccountMenu() throws Exception {
		if (!Utils.waitForElement(driver, lblUserAccDesktop, 1)) {
			if (!Utils.isMobile()) {
				BrowserActions.scrollToTopOfPage(driver);
				BrowserActions.clickOnElementX(lnkAccountButton, driver, "Sign In Menu Link");
			} else {
				return false;
			}
			if (!Utils.waitForElement(driver, lblUserAccDesktop, 10)) {
				BrowserActions.clickOnElementX(lnkAccountButton, driver, "Sign In Menu Link");
			} 

			if (Utils.waitForElement(driver, lblUserAccDesktop, 10)) {
				Log.event("SignIn Flyout displayed");
				return true;
			} else {
				Log.event("SignIn Flyout not displayed.");
				return false;
			}
		} else {
			Log.event("SignIn Flyout displayed");
			return true;
		}
	}

	/**
	 * To get the value entered in the email field
	 * @return Value entered in the Email field
	 * @throws Exception -
	 */
	public String getValueEnteredInEmailAddress()throws Exception{
		return BrowserActions.getTextFromAttribute(driver, fldUserNameDesktop, "value", "Email Address");
	}


	/**
	 * To hover the mouse pointer on the sign in button
	 * @throws Exception - Exception
	 */
	public void mouseOverMyBag() throws Exception {
		if(!Utils.waitForElement(driver, divMiniCartOverlay)) {
			BrowserActions.scrollToTopOfPage(driver);
			if (Utils.isDesktop()) {
				BrowserActions.mouseHover(driver, lnkMiniCart, "Cart page Link ");
			} else if (Utils.isTablet()) {
				clickOnMyBagForTablet();
			}
		}
	}
	
	/**
	 * To click on My bag for tablet
	 * @throws Exception - Exception
	 */
	private void clickOnMyBagForTablet() throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		if (Utils.isTablet()) {
			try {
				BrowserActions.scrollToView(lnkMiniCart, driver);
				BrowserActions.actionClick(lnkMiniCart, driver, "Sign In Menu Link ");
			} catch (WebDriverException x) {
				BrowserActions.javascriptClick(lnkMiniCart, driver, "Sign In Menu Link ");
			}
		}
	}
	
	/**
	 * to navigate Approved PLCC landing page 
	 * @return PlatinumCreditCardPage
	 * @throws Exception - Exception
	 */
	public PlatinumCardLandingPage navigateToApprovedPlatinumCreditCardLanding()throws Exception{
		try {
			BrowserActions.clickOnElementX(lnkCardPLCC, driver, "Navigate to Platinum Credit Card Landing Page");
		}catch(Exception e) {
			Log.event(e.getLocalizedMessage());
			Log.event("Rewards link not found on page. Using workaround...");
			driver.navigate().to(Utils.getWebSite() + redirectData.get("platinumLanding"));
		}
		Utils.waitForPageLoad(driver);
		return new PlatinumCardLandingPage(driver).get();
	}

	/**
	 * To open/close the hamburger menu in mobile
	 * @param String - open or close state
	 * @return Object - page object
	 * @throws Exception - Exception
	 */
	public Object openCloseHamburgerMenu(String state) throws Exception {
		BrowserActions.scrollInToView(lnkHamburger, driver);
		if (Utils.waitForElement(driver, lnkHamburgerClosed, 2) && (state.equals("open"))) {
			BrowserActions.javascriptClick(lnkHamburger, driver, "Hamburger menu ");
			Log.event("Hamburger menu opened");
			return new HamburgerMenu(driver).get();
		} else if(Utils.waitForElement(driver, lnkHamburgerOpened, 2) && (state.equals("close"))){
			BrowserActions.javascriptClick(lnkHamburger, driver, "Hamburger menu ");
			Log.event("Hamburger menu closed");
			return new Headers(driver).get();
		} else if (Utils.isTablet()){
			Log.event("The tablet orientation is landscape");
			return null;
		} else {
			return new HamburgerMenu(driver).get();
		}
	}
	
	/**
	 * To type the username in username field in Sign in flyout
	 * @param String - username to type 
	 * @throws Exception - Exception
	 */
	public void typeUserNameInFlyout(String username)throws Exception{
		BrowserActions.typeOnTextField(fldUserNameDesktop, username, driver, "Email field");
		Log.event("--->>>Typed User name :: " + username);
	}

	/**
	 * To type the password in password field in Sign in flyout
	 * @param String - password to type 
	 * @throws Exception - Exception
	 */
	public void typePasswordInFlyout(String password)throws Exception{
		BrowserActions.typeOnTextField(fldPasswordDesktop, password, driver, "Password field");
		Log.event("--->>>Typed Password :: " + password);
	}

	/**
	 * To click on the sign in button in the Sign in flyout
	 * @return Object - page object
	 * @throws Exception - Exception
	 */
	public Object clickOnSignInDesktop()throws Exception{
		mouseOverAccountMenu();
		BrowserActions.clickOnElementX(btnFlyoutSignIn, driver, "Sign In Button");
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, readyElementMyAccountPage)){
			Log.event("Logged in successfully with given account.");
			return new MyAccountPage(driver).get();
		}else if(Utils.waitForElement(driver, readyElementMyAccountPage, 2)){
			Log.event("Logged in successfully with given account.");
			return new MyAccountPage(driver).get();
		}else if(Utils.waitForElement(driver, divLoginBoxDesktop, 2)){
			Log.event("Cannot login with given credentials.");
			return new SignIn(driver).get();
		}else 
			return new Headers(driver).get();
	}

	/**
	 * To Check/UnCheck the 'Remember Me' Checkbox 
	 * @param needCheck - True to check the Remember me Checkbox else false
	 * @throws Exception - Exception
	 */
	public void checkOnRememberMeDesktop(boolean needCheck)throws Exception{
		boolean isChecked = chkRememberMeDesktop.isSelected();
		if (isChecked != needCheck) {
			BrowserActions.clickOnElementX(chkRememberMeDesktop, driver, "Remember Me checkbox");
		}
	}

	/**
	 * To Check/UnCheck the 'Remember Me' Checkbox 
	 * @param needCheck - True to check the Remember me Checkbox else false
	 * @throws Exception - Exception
	 */
	public boolean verifyRememberMeCheckboxSelectedOrNot()throws Exception{
		return chkRememberMeDesktop.isSelected();	
	}

	/**
	 * To click on the sign in button in the Sign in flyout
	 * @return sign in
	 * @throws Exception - Exception
	 */
	public SignIn clickOnSignInMobile()throws Exception{
		BrowserActions.clickOnElementX(lnkAccountButtonMobile, driver, "Sign In link in Mobile");
		Utils.waitForPageLoad(driver);
		return new SignIn(driver).get();
	}
	
	/**
	 * To login to account by given credentials
	 * @param email
	 * @param password
	 * @param rememberMe
	 */
	public Object loginAccount(String email, String password, boolean ...rememberMe) throws Exception {
		mouseOverAccountMenu();
		typeUserNameInFlyout(email);
		typePasswordInFlyout(password);
		if (rememberMe.length > 0) {
			checkOnRememberMeDesktop(rememberMe[0]);
		}
		Object obj = clickOnSignInDesktop();
		GlobalNavigation.saveCredentialInDataSheet(driver, email, readyElementMyAccountPage);
		return obj;
	}

	/**
	 * To navigate to my account with valid credentials
	 * @param String - username 
	 * @param String - password 
	 * @param boolean - (Optional) createNewUser
	 * @return MyAccountPage - MyAccountPage page object
	 * @throws Exception - Exception
	 */
	public MyAccountPage navigateToMyAccount(String username, String password, boolean... createNewUser)throws Exception{
		Object obj = null;
		HashMap<UserAccount, String> userDetails = new HashMap<>();
		userDetails.put(UserAccount.Email, username);
		userDetails.put(UserAccount.Password, password);
		userDetails.put(UserAccount.EmailSubscription, "checked");
		
		if (!Utils.isMobile()) {
			obj = loginAccount(username, password);
		} else {
			openCloseHamburgerMenu("open");
			SignIn signIn = clickOnSignInMobile();
			obj = signIn.loginAccount(username, password);
		}
		
		if (obj.getClass().getSimpleName().equals(MyAccountPage.class.getSimpleName())) {
			Log.event("Successfully logged in.");
			return (MyAccountPage)obj;
		} else if (obj.getClass().getSimpleName().equals(SignIn.class.getSimpleName())) {
			Log.event("Retrying logging in.");
			SignIn signIn = (SignIn) obj;
			obj = signIn.loginAccount(username, password);
			
			if (obj.getClass().getSimpleName().equals(MyAccountPage.class.getSimpleName())) {
				return (MyAccountPage)obj;
			} else if(createNewUser.length > 0 && (createNewUser.length > 0 ? createNewUser[0] : false)) {
				obj = signIn.createAccount(userDetails);
				try {
					return (MyAccountPage) obj;
				} catch (ClassCastException e) {
					Log.fail("Class Cast Exception Occured. Please verify the method logic.", driver);
					return null;
				}
			} else {
				Log.fail("Invalid User credentials. Please check the spelling.", driver);
				return null;
			}
		} else {
			Log.fail("Account sign in page not displayed", driver);
			return null;
		}
	}

	/**
	 * To navigate to my account with valid credentials
	 * @param username - 
	 * @param password - 
	 * @param chkRemMe -
	 * @return MyAccountPage
	 * @throws Exception - Exception
	 */
	public MyAccountPage navigateToMyAccountWithRememberChkBox(String username, String password, boolean chkRemMe, boolean... createNewUser)throws Exception{
		Object obj = null;
		HashMap<UserAccount, String> userDetails = new HashMap<>();
		userDetails.put(UserAccount.Email, username);
		userDetails.put(UserAccount.Password, password);
		userDetails.put(UserAccount.EmailSubscription, "checked");
		
		if (!Utils.isMobile()) {
			obj = loginAccount(username, password, chkRemMe);
		} else {
			openCloseHamburgerMenu("open");
			SignIn signIn = clickOnSignInMobile();
			obj = signIn.loginAccount(username, password, chkRemMe);
		}
		
		if (obj.getClass().getSimpleName().equals(MyAccountPage.class.getSimpleName())) {
			Log.event("Successfully logged in.");
			GlobalNavigation.saveCredentialInDataSheet(driver, username, readyElementMyAccountPage);
			return (MyAccountPage)obj;
		} else if (obj.getClass().getSimpleName().equals(SignIn.class.getSimpleName())) {
			Log.event("Retrying logging in.");
			SignIn signIn = (SignIn) obj;
			obj = signIn.loginAccount(username, password);
			
			if (obj.getClass().getSimpleName().equals(MyAccountPage.class.getSimpleName())) {
				GlobalNavigation.saveCredentialInDataSheet(driver, username, readyElementMyAccountPage);
				return (MyAccountPage)obj;
			} else if(createNewUser.length > 0 && (createNewUser.length > 0 ? createNewUser[0] : false)) {
				if(!(TestData.verifyEmailIDExist(username, Utils.getCurrentBrand()))) {
					obj = signIn.createAccount(userDetails);
					try {
						return (MyAccountPage) obj;
					} catch (ClassCastException e) {
						Log.fail("Class Cast Exception Occured. Please verify the method logic.", driver);
						return null;
					}
				} else {
					Log.fail("User already exists. Please verify the method logic.", driver);
					return null;
				}
			} else {
				Log.fail("Invalid User credentials. Please check the spelling.", driver);
				return null;
			}
		} else {
			Log.fail("Account sign in page not displayed", driver);
			return null;
		}
	}

	/**
	 * To navigate to my account with valid credentials
	 * @param username - 
	 * @param password - 
	 * @param chkRemMe -
	 * @param userDetail - first name|last name should be the format
	 * @return MyAccountPage
	 * @throws Exception - Exception
	 */
	public MyAccountPage navigateToMyAccountWithSpecifiedUser(String username, String password, String fName, String lName, boolean chkRemMe, boolean... createNewUser)throws Exception{
		HashMap<UserAccount, String> userDetails = new HashMap<>();
		userDetails.put(UserAccount.Email, username);
		userDetails.put(UserAccount.Password, password);
		userDetails.put(UserAccount.FirstName, fName);
		userDetails.put(UserAccount.LastName, lName);
		userDetails.put(UserAccount.EmailSubscription, "checked");
		
		SignIn signinPg = navigateToSignInPage();
		Object obj = null;
		if(!(TestData.verifyEmailIDExist(username, Utils.getCurrentBrand()))) {
			obj = signinPg.createAccount(userDetails);
		} else {
			obj = signinPg.loginAccount(username, password, chkRemMe);
		}
		
		if (obj.getClass().getSimpleName().equals(MyAccountPage.class.getSimpleName())) {
			GlobalNavigation.saveCredentialInDataSheet(driver, username);
			return (MyAccountPage)obj;
		} else {
			obj = signinPg.loginAccount(username, password, chkRemMe);
			if (obj.getClass().getSimpleName().equals(MyAccountPage.class.getSimpleName())) {
				ProfilePage profilePg = navigateToProfilePage();
				if (!profilePg.getFirstName().equalsIgnoreCase(fName)) {
					profilePg.typeFirstName(fName);
				}
				if (!profilePg.getFirstName().equalsIgnoreCase(fName)) {
					profilePg.typeLasName(lName);
				}
				profilePg.clickUpdateInfoBtn();
				return navigateToMyAccount();
			}
			try {
				return (MyAccountPage) obj;
			} catch (ClassCastException e) {
				Log.fail("Class Cast Exception Occured. Please verify the user account is active.", driver);
				return null;
			}
		}
	}

	/**
	 * To navigate to my accoutn page
	 * @return MyAccountPage
	 * @throws Exception - Exception
	 */
	public MyAccountPage navigateToMyAccount() throws Exception{
		Utils.waitForPageLoad(driver);
		BrowserActions.scrollToTopOfPage(driver);
		if (!Utils.isMobile()) {
			if (Utils.waitForElement(driver, btnAccountButtonActive)) {
	    		Log.ticket("SC-5383");
	    		clickOnSearchIcon();
	    	}
			BrowserActions.clickOnElementX(lnkAccountButton, driver, "Account link in header");
			Utils.waitForPageLoad(driver);
			BrowserActions.clickOnElementX(lnkProfileMyAccFlytDesktop, driver, "Profile link in flyout");
		} else {
			openCloseHamburgerMenu("open");
			BrowserActions.javascriptClick(lnkAccountButtonMobile, driver, "My Account link in hamburger");
			BrowserActions.javascriptClick(lnkProfileMyAccFlytDesktop, driver, "Overview link");
		}
		Utils.waitForPageLoad(driver);
		return new MyAccountPage(driver).get();
	}

	/**
	 * To navigate to profile page
	 * @return ProfilePage
	 * @throws Exception - Exception
	 */
	public ProfilePage navigateToProfilePage()throws Exception{
		Utils.waitForPageLoad(driver);
		if (Utils.isMobile()) {
			openCloseHamburgerMenu("open");
			BrowserActions.javascriptClick(lnkAccountButtonMobile, driver, "My Account link in hamburger");
		} else {
			BrowserActions.clickOnElementX(lnkAccountButton, driver, "My Account link in header");			
		}
		BrowserActions.clickOnElementX(lnkProfileMyAccFlytDesktop, driver, "Profile link");
		Utils.waitForPageLoad(driver);
		return new ProfilePage(driver).get();
	}

	/**
	 * To get the account menu header text
	 * @return account menu header
	 * <br> Returns empty string for guest user
	 * @throws Exception
	 */
	public String getAccountMenuHeader() throws Exception {
		String accountMenuHeader = "";
		if (verifyUserIsLoggedIn()) {
			if (Utils.isMobile()) {
				HamburgerMenu hMenu = (HamburgerMenu) openCloseHamburgerMenu("open");
				accountMenuHeader = hMenu.getAccountMenuHeader();
				openCloseHamburgerMenu("close");
			} else {
				mouseOverAccountMenu();
				accountMenuHeader = BrowserActions.getText(driver, lblMyAccDesktop, "account menu header for logged in user");
			}
			return accountMenuHeader;
		} else {
			Log.failsoft("User is not logged in");
			return accountMenuHeader;
		}
	}
	
	/**
	 * To get user name from header for logged in user
	 * @return name from account menu
	 */
	public String getUserName() throws Exception {
		String accountHeader = getAccountMenuHeader();
		return accountHeader.replace("Hi", "").replace(",", "").trim();
	}

	/**
	 * To select a product form search suggestion and navigate to PDP Page 
	 * @param product(Optional) - An Integers represents nth item in list, or a string represents product name in list
	 * <br> If empty, a random item will be chosen 
	 * @return PdpPage instance of product selected from search suggestions
	 * @throws Exception
	 */
	public PdpPage selectProductFromSearchSuggestion(String... product)throws Exception{
		if(product.length > 0){
			try{
				int num = Integer.parseInt(product[0]) - 1;
				BrowserActions.clickOnElementX(lstProductNameInSearchSuggestion.get(num-1), driver, "nth Product in Product Suggestion ");
			}catch(NumberFormatException e){
				for (WebElement prdInSuggestion : lstProductSuggestion) {
					WebElement singleProduct = prdInSuggestion.findElement(By.cssSelector(".product-name"));
					if(singleProduct.getText().equalsIgnoreCase(product[0])){
						BrowserActions.clickOnElementX(singleProduct, driver, "Product(" + product[0] + ") in Product suggestion ");
						break;
					}
				}
			}
		}else{
			int rand = Utils.getRandom(0, lstProductSuggestion.size() - 1);
			WebElement singleProduct = lstProductSuggestion.get(rand).findElement(By.cssSelector(".product-name"));
			BrowserActions.clickOnElementX(singleProduct, driver, "Random product in product suggesstion ");
		}
		
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}

	/**
	 * To navigate to search result page for given search term
	 * @param searchKey - Search term
	 * @return SearchResultPage
	 * @throws Exception - Exception
	 */
	public SearchResultPage navigateToSLP(String searchKey)throws Exception{
		toggleSearchPanel(Toggle.Open);
		BrowserActions.typeOnTextField(txtSearch, searchKey, driver, "Search field");
		BrowserActions.javascriptClick(btnFlyoutSearch, driver, "Search Icon ");
		try{
			Utils.waitUntilElementDisappear(driver, btnFlyoutSearch);
		} catch(Exception e) {
			Log.ticket("SC-6029");
			BrowserActions.javascriptClick(btnFlyoutSearch, driver, "Search Icon ");
		}
		Utils.waitForPageLoad(driver);
		return new SearchResultPage(driver).get();
	}

	/**
	 * To verify the search phrase presence in left side enhanced search suggestion
	 * in auto suggest pop up
	 * @param String - searchKey 
	 * @return boolean - 'true', if displayed, 'false', if not displayed
	 * @throws Exception - Exception
	 */
	public boolean verifySearchPhraseInEnhancedSearch(String searchKey)throws Exception{
		List<WebElement> lstCategorySuggestions = lstPhraseSuggestionGroups.findElements(By.cssSelector("a.hit"));
		for(int i = 0; i < lstCategorySuggestions.size(); i++){
			if(!lstCategorySuggestions.get(i).getAttribute("innerHTML").toLowerCase().contains(searchKey.toLowerCase()))
				return false;
		}
		return true;
	}

	/**
	 * To get product name from search suggestions
	 * @return string - list of product name
	 * @throws Exception - Exception
	 */
	public List<String> getProductNameFromSearchSuggestion()throws Exception{
		List<String> prdName = new ArrayList<String>();
		prdName = BrowserActions.getText(lstProductNameInSearchSuggestion, "Product name in search suggestion", driver);
		return prdName;
	}

	/**
	 * To hover the mouse pointer on the sign in button
	 * @throws Exception - Exception
	 */
	public void mouseOverCustomerService() throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		if(!Utils.waitForElement(driver, lnkEmailUsDesktop)){
			if(Utils.isTablet()){
				BrowserActions.clickOnElementX(lnkCustomerServiceDesktop, driver, "Customer Service link");
			}else {
				BrowserActions.mouseHover(driver, lnkCustomerServiceDesktop, "Customer Service link");
				if(Utils.waitForElement(driver, lnkEmailUsDesktop, 3)){
					Log.event("Customer service Flyout displayed");
				} else{
					BrowserActions.scrollToBottomOfPage(driver);
					BrowserActions.mouseHover(driver, lnkCustomerServiceDesktop, "Customer Service link");
					if(!(Utils.waitForElement(driver, lnkEmailUsDesktop, 3))){
						Log.fail("Customer service Flyout not opened", driver);
					}
				}
			}
		} else {
			Log.event("Customer service flyout is already opened");
		}
	}

	/**
	 * To hover the mouse pointer on the My Bag Icon
	 * @return Mini cart page
	 * @throws Exception - Exception
	 */
	public MiniCartPage mouseOverMiniCart() throws Exception {
		if (Utils.isDesktop()) {
			if (Utils.getRunBrowser(driver).equals("safari") || Utils.getRunBrowser(driver).equals("internet explorer")) {
				Log.fail("Browser Limitations : Mouse over functionality is not working", driver);
			} else {
				BrowserActions.scrollToTopOfPage(driver);
				BrowserActions.mouseHover(driver, lnkMiniCart, "My Bag Link ");
				if (Utils.waitForElement(driver, divMiniCartOverlay)) {
					Log.event("Mini Cart Flyout displayed");
				}
			}
		} else if (Utils.isTablet()) {
			BrowserActions.refreshPage(driver);
			try {
				BrowserActions.scrollToView(lnkMiniCart, driver);
				BrowserActions.actionClick(lnkMiniCart, driver, "Sign In Menu Link ");
			} catch (WebDriverException x) {
				BrowserActions.javascriptClick(lnkMiniCart, driver, "Sign In Menu Link ");
			}
		}

		return new MiniCartPage(driver).get();
	}

	/**
	 * To navigate to Live Chat
	 * @throws Exception - Exception
	 */
	public void navigateToLiveChat()throws Exception{
		BrowserActions.clickOnElementX(imgLiveChatDesktop, driver, "Live Chat Us ");
		Utils.waitForElement(driver, mdlLiveChat, 30);		// Live chat modal takes longer than usual time to load.
	}
	
	/**
	 * To navigate to Update My Info page throug My Account flyout
	 * @return UpdateMyInformationPage
	 * @throws Exception - Exception
	 */
	public ProfilePage clickOnProfileLink()throws Exception{
		if (!Utils.isMobile()) {
			if(!Utils.waitForElement(driver, flytSignInRegisteredDesktop)) {
				BrowserActions.scrollToTopOfPage(driver);
				BrowserActions.clickOnElementX(lnkAccountButton, driver, "Account link in header");
				Utils.waitForPageLoad(driver);
			}
			BrowserActions.clickOnElementX(lnkProfileMyAccFlytDesktop, driver, "Profile link in flyout");
		} else {
			if(!Utils.waitForElement(driver, lnkProfileMyAccFlytDesktop)) {
				openCloseHamburgerMenu("open");
				BrowserActions.javascriptClick(lnkAccountButtonMobile, driver, "My Account link in hamburger");
			}
			BrowserActions.javascriptClick(lnkProfileMyAccFlytDesktop, driver, "Overview link");
		}
		Utils.waitForPageLoad(driver);
		return new ProfilePage(driver).get();
	}
	
	/**
	 * To navigate to Order history through My Account flyout
	 * @return OrderHistoryPage object
	 * @throws Exception - Exception
	 */
	public OrderHistoryPage clickOnOrderHistoryLink()throws Exception{
		BrowserActions.clickOnElementX(lnkOrderHistoryMyAccFlytDesktop, driver, "Update My Info ");
		Utils.waitForPageLoad(driver);
		return new OrderHistoryPage(driver).get();
	}
	
	/**
	 * To get global navigation level 1 menu element
	 * @param level1 - name of level1 category element to get
	 * @throws Exception
	 */
	protected WebElement getLevel1Category(String level1) throws Exception{
		WebElement level1Menu = null;
		try {
			if(level1.contains("'")) {
				level1 = level1.split("\\'")[0];
			}
			String navEle = "#navigation a[title*='"+level1+"']";
			Log.event("Constructed Locator :: " + navEle);
			level1Menu = driver.findElement(By.cssSelector(navEle));
		} catch(NoSuchElementException e) {
			Log.event("Element not found by constructed locator. Trying loop method...");
			for(WebElement level1Element : lnkGlobalNavCategories) {
				if(BrowserActions.getText(driver, level1Element, "Level 1 element").equalsIgnoreCase(level1)) {
					level1Menu = level1Element;
					break;
				}
			}
		}
		return level1Menu;
	}
	
	/**
	 * To get global navigation level 2 menu element
	 * @param level1CatElement - parent level1 menu element
	 * @param level2 - name of level2 category element to get
	 * @throws Exception
	 */
	protected WebElement getLevel2Category(WebElement level1CatElement, String level2) throws Exception{
		WebElement level2Menu = null;
		BrowserActions.mouseHover(driver, level1CatElement, "Level-1 menu");
		List<WebElement> level2Categories = level1CatElement.findElement(By.xpath("..")).findElements(By.cssSelector(".nav-column>li>div>a"));
		if (level2.equals("random")) {
			int rand = ThreadLocalRandom.current().nextInt(1,level2Categories.size());
			level2Menu = level2Categories.get(rand - 1);
			Utils.waitForElement(driver, level2Menu);
		} else {
			BrowserActions.mouseHover(driver, level1CatElement);
			for(WebElement level2Item : level2Categories) {
				if(BrowserActions.getText(driver, level2Item, "level2 option").equalsIgnoreCase(level2)) {
					level2Menu = level2Item;
					return level2Menu;
				}
			}
			
			if(level2.contains("'")) {
				level2 = level2.split("\\'")[0];
			}
			try {
				String level2XPath = "./../div[@class='level-2']//ul[contains(@class,'nav-column')]//li//a[contains(text(),'"+ level2 +"')]";
				Log.event("Generated XPath:: " + level2XPath);
				level2Menu = level1CatElement.findElement(By.xpath(level2XPath));
			} catch(Exception e) {
				String level2XPath = "//ul[contains(@class,'level-1')]//a[contains(@class,'has-sub-menu')]/../div[@class='level-2']//ul[contains(@class,'nav-column')]//li//a[contains(text(),'"+ level2 +"')]";
				Log.event("Generated XPath:: " + level2XPath);
				level2Menu = driver.findElement(By.xpath(level2XPath));
				for(WebElement category : level2Categories) {
					Log.event("category:: " + category.getText());
					if(category.getText().equalsIgnoreCase(level2)) {
						level2Menu = category;
						break;
					}
				}
			}
			Utils.waitForElement(driver, level2Menu);
		}
		return level2Menu;
	}

	/**
	 * To navigate to a random level 1 category
	 * @return PlpPage instance
	 * @throws Exception
	 */
	public PlpPage navigateToRandomCategory() throws Exception {
		if (Utils.isDesktop()) {
			WebElement level1Element = lnkGlobalNavCategories.get(Utils.getRandom(0, lnkGlobalNavCategories.size()-1));
			BrowserActions.clickOnElementX(level1Element, driver, "Header Nav Bar category link");
			if(!Utils.waitForElement(driver, readyElementPLP)) {
				WebElement l2SubMenu = subCategories.get(Utils.getRandom(0, subCategories.size()-1));
				BrowserActions.clickOnElementX(l2SubMenu, driver, "level2 sub category");
			}
		} else {
			openCloseHamburgerMenu("open");
			WebElement level1Element = lnkGlobalNavCategories.get(Utils.getRandom(0, lnkGlobalNavCategories.size()-1));
			BrowserActions.clickOnElementX(level1Element, driver, "Header Nav Bar category link");
			try {
				WebElement elementViewAll = level1Element.findElement(By.xpath("..")).findElement(By.cssSelector(".l2-activate .nav-viewAll a:not(.level-3)"));
				BrowserActions.clickOnElementX(elementViewAll, driver, "Level1 view all");
			} catch (NoSuchElementException nse) {
				if(Utils.waitForElement(driver, btnActiveViewAll)) {
					BrowserActions.clickOnElementX(btnActiveViewAll, driver, "Level1 view all");
				} else {
					Log.event("This category is not configured with a view all button.");
				}
			}
		}
		
		return new PlpPage(driver).get();
	}
	
	/**
	 * To navigate to category level-1
	 * @param level1 -
	 * @return PlpPage -
	 * @throws Exception -
	 */
	public PlpPage navigateTo(String level1) throws Exception {
		long startTime = StopWatch.startTime();
		if(level1.contains("|")) {
			String newLevel1 = level1.split("\\|")[0].trim();
			String level2 = level1.replace(newLevel1+"|", "").trim();
			if(!level2.equalsIgnoreCase("View All"))
				return navigateTo(newLevel1, level2);
			else
				level1 = newLevel1;
		}
		if (Utils.isDesktop()) {
			WebElement element = getLevel1Category(level1);
			if(element != null) {
				BrowserActions.clickOnElementX(element, driver, "Category 1 ");
			} else {
				Log.fail(level1 + " is not available in this Site. Please check with Business Manager.", driver);
			}

		} else {
			//Hamburger navigation: Tablet and Mobile
			openCloseHamburgerMenu("open");
			try{
				String hMenuLeve1Locator = ".slider-menu .menu-category.level-1>li>a[title=\""+level1+"\"]";
				Log.event("Generated hamburger level1 locator:: " + hMenuLeve1Locator);
				WebElement element = BrowserActions.checkLocator(driver, hMenuLeve1Locator);
				BrowserActions.clickOnElementX(element, driver, "Level 1 Header Menu Toogle(+)");
				if(Utils.waitForElement(driver, lnkHamburgerOpened)|| Utils.waitForElement(driver, lvl2Navigation) ) {
					try {
						WebElement level1ViewAll = driver.findElement(By.cssSelector("ul.level-1 li.l2-activate li.nav-viewAll a"));
						BrowserActions.clickOnElementX(level1ViewAll, driver, "View All Button");
					}catch(NoSuchElementException e) {
						Log.fail("View All button not found. Check locator.");
					}
				}
			}catch(NoSuchElementException e){
				Log.fail(level1 + " is not available in this Site. Please check with Business Manager.", driver);
			}
		}
		Log.event(" -----> Clicked '" + level1 + "'");
		Utils.waitForPageLoad(driver);
		GlobalNavigation.closePopup(driver);
		Actions action = new Actions(driver);
		action.moveByOffset(1, 1).build().perform();
		Log.event("Elapsed time for level1 navigation:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
		return new PlpPage(driver).get();
	}

	/**
	 * To navigate to level2 category item Ex: Mens - Men's Clothing
	 * Parameterized level1's level2 category will be clicked
	 * @param level1 - L1 category item
	 * @param level2 - L2 category item
	 * @return PLP page
	 * @throws Exception - Exception
	 */
	public PlpPage navigateTo(String level1, String level2) throws Exception {
		long startTime = StopWatch.startTime();
		navigateToHome();
		if(level2.equalsIgnoreCase("View All")) {
			return navigateTo(level1);
		} else if(level2.contains("|")) {
			String newlevel2 = level2.split("\\|")[0].trim();
			String level3 = level2.replace(newlevel2+"|", "").trim();
			return navigateTo(level1, newlevel2, level3);
		}
		Log.event("Given Level :: " + level1 + " >> " + level2);
		if (Utils.isDesktop()) {
			WebElement level1Parent = getLevel1Category(level1);
			BrowserActions.mouseHover(driver, level1Parent, "Level-1 category:: " + level1);
			WebElement level2Menu = getLevel2Category(level1Parent, level2);
			Utils.waitForElement(driver, level2Menu);
			BrowserActions.clickOnElementX(level2Menu, driver, "Level 2 category");
			Log.event("Clicked on Level2 category:: " + level2);
		} else{
			//Hamburger menu - Tablet and Mobile
			openCloseHamburgerMenu("open");
			Utils.waitForPageLoad(driver);
			WebElement level1Parent = driver.findElement(By.xpath("//ul[contains(@class,'level-1')]//a[contains(@title,'"+ level1 +"')]"));
			Utils.waitForElement(driver, level1Parent);
			BrowserActions.clickOnElementX(level1Parent, driver, "Level 1 Header Menu Toogle(+)");
			Log.event("Clicked on: " + level1);
			if(level2.equalsIgnoreCase("View All")){
				WebElement lvl1ViewAll = driver.findElement(By.cssSelector("ul.level-1 li.l2-activate .col-1> li.nav-viewAll>a"));
				Utils.waitForElement(driver, lvl1ViewAll);
				BrowserActions.javascriptClick(lvl1ViewAll, driver, "Level-2 category ");
				Log.event("Clicked on View All");
			}else {
				List<WebElement> elements = driver.findElements(By.cssSelector(".level-1 .l2-activate .col-1 .parent-level3>a"));
				WebElement level2ShopByCategory;
				boolean hasByCategory = false;
				for(int index=0 ; index < elements.size() ; index++) {
					if(elements.get(index).getText().equalsIgnoreCase("Shop By Category")) {
						level2ShopByCategory = elements.get(index);
						Utils.waitForElement(driver, level2ShopByCategory);
						BrowserActions.clickOnElementX(level2ShopByCategory, driver, "shop by category");
						Log.event("Clicked on shop by category for " + level1);
						hasByCategory = true;
						break;
					}
				}
				if(hasByCategory) {
					Log.event("Shop by category is enabled");
					WebElement level2Menu = driver.findElement(By.cssSelector("ul.level-1 .l2-active .level-2 .sub-cat-nav"));
					WebElement level2MenuCat = null;
					List<WebElement> level2Cats = level2Menu.findElements(By.cssSelector("li>div.nonBoldCat>a"));
					for(WebElement menuOption : level2Cats) {
						if (menuOption.getText().equalsIgnoreCase(level2)) {
							level2MenuCat = menuOption;
							break;
						}
					}
					boolean hasSubMenu = level2MenuCat.getAttribute("class").contains("has-sub-menu");
					Utils.waitForElement(driver, level2MenuCat);
					BrowserActions.clickOnElementX(level2MenuCat, driver, "Menu category");
					Log.event("Clicked on: " + level2);
					if(hasSubMenu){
						Log.event("Sub menu is present.");
						WebElement lvl2ViewAll = level2MenuCat.findElement(By.xpath("../..")).findElement(By.cssSelector(".nav-viewAll>a"));
						Utils.waitForElement(driver, lvl2ViewAll);
						BrowserActions.clickOnElementX(lvl2ViewAll, driver, " level 2 View All");
						Log.event("Clicked on view all for " + level2);
					}
				}else {
					Log.event("Shop By Category is disabled.");
					WebElement level2Menu = driver.findElement(By.cssSelector("ul.level-1 li.l2-activate .level-2"));
					WebElement level2MenuCat = null;
					List<WebElement> level2MenuOptions = level2Menu.findElements(By.cssSelector(".parent-level3>a"));
					for(WebElement menuOption : level2MenuOptions) {
						Utils.waitForElement(driver, menuOption);
						if(menuOption.getText().equalsIgnoreCase(level2)) {
							level2MenuCat = menuOption;
							break;
						} 
					}
					boolean hasSubMenu = level2MenuCat.getAttribute("class").contains("has-sub-menu");
					Utils.waitForElement(driver, level2MenuCat);
					BrowserActions.clickOnElementX(level2MenuCat, driver, "level2 menu");
					Log.event("Clicked on: " + level2);
					Utils.waitForPageLoad(driver);
					if(hasSubMenu) {
						WebElement catHeading = driver.findElement(By.cssSelector(".level-1 .l3-active>.parent-level3>a"));
						Utils.waitForElement(driver, catHeading);
						if(catHeading.getText().equalsIgnoreCase(level2)) {
							WebElement lvl2ViewAll = driver.findElement(By.cssSelector(".col-1 .l3-active .nav-column .nav-viewAll>a"));
							Utils.waitForElement(driver, lvl2ViewAll);
							BrowserActions.clickOnElementX(lvl2ViewAll, driver, " level 2 View All");
							Log.event("Clicked on view all for " + level2);
						} else {
							Log.failsoft("On incorrect category");
						}
					}
				}
			}
		}
		Utils.waitForPageLoad(driver);
		Log.event("Elapsed time for level2 navigation:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
		return new PlpPage(driver).get();
	}

	/**
	 * To get Level 2 category
	 * @param level2 - 
	 * @return WebElement
	 * @throws Exception - 
	 */
	public WebElement getLevel2Category(String level2)throws Exception{
		List<WebElement> subCat = driver.findElements(By.cssSelector("ul.level-1 li.l2-activate div.nonBoldCat a"));
		for(int i = 0; i < subCat.size(); i++){
			if(subCat.get(i).getText().trim().toLowerCase().equals(level2.toLowerCase()))
				return subCat.get(i);
		}
		return null;
	}
	
	/**
	 * To navigate to level3 category item Ex: Men - Men's Clothing - Jeans
	 * <br>
	 * Parameterized level1 and level2 will be navigated then level3 will be clicked
	 * @param level1 - L1 category item
	 * @param level2 - L2 category item
	 * @param level3 - L3 category item
	 * @return PlpPage - Plp Page
	 * @throws Exception - Exception
	 */
	public PlpPage navigateTo(String level1, String level2, String level3) throws Exception {
		long startTime = StopWatch.startTime();
		if(level3.contains("|")) {
			String newlevel3 = level3.split("\\|")[0].trim();
			String level4 = level3.replace(newlevel3+"|", "").trim();
			return navigateTo(level1, level2, newlevel3, level4);
		}
		Log.event("Given Level :: " + level1 + " >> " + level2 + " >> " + level3);
		if (Utils.isDesktop()) {
			WebElement element = BrowserActions.checkLocator(driver, HEADER_NAV_LEVEL1 + "a[title='" + level1 + "']");
			BrowserActions.mouseHover(driver, element, "Level-1 category ");
			if (level3.equals("random")) {

				List<WebElement> level2Parent = BrowserActions.checkLocators(driver, HEADER_SUB_CATEGORY_l2_HAS_L3);
				int rand = ThreadLocalRandom.current().nextInt(1,
						level2Parent.size());
				WebElement level2Text = level2Parent.get(rand - 1).findElement(By.xpath("../a"));
				level2 = BrowserActions.getText(driver, level2Text, "L2 category text");
				Log.event(" Clicked random L2 category: '" + level2 + "'");
				List<WebElement> level3Category = level2Text.findElement(By.xpath("..")).findElements(By.cssSelector("ul[class='level-3']>li>a"));
				if (level3Category.size() > 0) {
					rand = ThreadLocalRandom.current().nextInt(1, level3Category.size());
					WebElement randomLevel3 = level3Category.get(rand - 1);
					level3 = BrowserActions.getText(driver, randomLevel3, "L3 category text");
					BrowserActions.clickOnElementX(randomLevel3, driver, "Level 3 category");

				} else {
					throw new Exception("There is no L3 category for L1 - " + level1);
				}
				Log.event(" Clicked random L3 category: '" + level3 + "'");
			} else {
				WebElement level3catagory = driver.findElement(By.xpath("//ul[contains(@class,'level-1')]//a[contains(@title,'"+level1+"')][contains(@class,'has-sub-menu')]/../div[@class='level-2']//ul[contains(@class,'nav-column')]//li//a[contains(text(),'"+level2+"')]//ancestor::div[@class='sub-cat-nav']//div[contains(@class,'level4')]//a[contains(text(),'"+level3+"')]"));
				BrowserActions.clickOnElementX(level3catagory, driver, "sub category(L3): " + level3);
			}
		} else {
			openCloseHamburgerMenu("open");
			WebElement element = driver.findElement(By.xpath("//ul[contains(@class,'level-1')]//a[contains(@title,'"+ level1 +"')]"));
			BrowserActions.clickOnElementX(element, driver, "Level 1 Header Menu Toogle(+)");
			WebElement level2Parent = driver.findElement(By.cssSelector(".level-1 .l2-navigate ul.nav-column.col-1"));
			WebElement level2Parent1 = level2Parent.findElement(By.xpath("//a[contains(text(), '"+level2+"')]"));
			BrowserActions.javascriptClick(level2Parent1, driver, "Level-2 category ");
			Log.event("Clicked on Level2Parent...");
			WebElement level3catagory = null;
			if(level3.equalsIgnoreCase("View All")) {
				level3catagory = level2Parent.findElement(By.cssSelector(".l3-active .level-3 .nav-viewAll a"));
			} else {
				level3catagory = level2Parent.findElement(By.xpath("//a[contains(text(), '"+level3+"')]"));
			}
			BrowserActions.javascriptClick(level3catagory, driver, "sub category(L3): " + level3);
		}
		Utils.waitForPageLoad(driver);
		Log.event("Elapsed time for level3 navigation:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
		return new PlpPage(driver).get();
	}

	/**
	 * To navigate to level3 category item Ex: Men - Men's Clothing - Jeans
	 * <br>
	 * Parameterized level1 and level2 will be navigated then level3 will be clicked
	 * @param level1 - L1 category item
	 * @param level2 - L2 category item
	 * @param level3 - L3 category item
	 * @param level4 - L4 category item
	 * @throws Exception - Exception
	 */
	public PlpPage navigateTo(
			String level1, String level2, String level3, String level4) throws Exception {
		
		long startTime = StopWatch.startTime();
		if (Utils.isDesktop() || Utils.isTablet()) {
			WebElement element = BrowserActions.checkLocator(driver, HEADER_NAV_LEVEL1 + "a[data-displaytext*='" + level1 + "']");
			BrowserActions.mouseHover(driver, element, "Level-1 category ");
			if (level3.equals("random")) {

				List<WebElement> level2Parent = BrowserActions.checkLocators(driver, HEADER_SUB_CATEGORY_l2_HAS_L3);
				int rand = ThreadLocalRandom.current().nextInt(1, level2Parent.size());
				WebElement level2Text = level2Parent.get(rand - 1).findElement(By.xpath("../a"));
				level2 = BrowserActions.getText(driver, level2Text, "L2 category text");
				Log.event(" Clicked random L2 category: '" + level2 + "'");
				List<WebElement> level3Category = level2Text.findElement(By.xpath("..")).findElements(By.cssSelector("ul[class='level-3']>li>a"));
				if (level3Category.size() > 0) {
					rand = ThreadLocalRandom.current().nextInt(1, level3Category.size());
					WebElement randomLevel3 = level3Category.get(rand - 1);
					level3 = BrowserActions.getText(driver, randomLevel3, "L3 category text");
					BrowserActions.clickOnElementX(randomLevel3, driver, "Level 3 category");
				} else {
					throw new Exception("There is no L3 category for L1 - " + level1);
				}
				Log.event(" Clicked random L3 category: '" + level3 + "'");
			} else {
				WebElement level2Parent = BrowserActions.getMatchingTextElementFromList(
						BrowserActions.checkLocators(driver, HEADER_SUB_CATEGORY_l2), level2, "equals");
				WebElement level3catagory = BrowserActions.getMatchingTextElementFromList(
						level2Parent.findElements(By.xpath(HEADER_SUB_CATEGORY_l3)), level3, "equals");
				BrowserActions.clickOnElementX(level3catagory, driver, "sub category(L3): " + level3);
			}
		} else if ((Utils.isMobile())) {
			openCloseHamburgerMenu("open");
			WebElement element = BrowserActions.checkLocator(driver, "//a[@data-displaytext='" + level1 + "']/following-sibling::i");
			BrowserActions.javascriptClick(element, driver, "Level 1 Header Menu Toogle(+)");
			if (level3.equals("random")) {
				List<WebElement> level2Parent = element.findElement(By.xpath("..")).findElements(By.cssSelector("div[class='level-2'] .hide-desktop ul>li>i"));
				int rand = ThreadLocalRandom.current().nextInt(1, level2Parent.size());

				WebElement level2Text = level2Parent.get(rand - 1);
				level2 = BrowserActions.getText(driver, level2Text, "L2 category text");
				BrowserActions.javascriptClick(level2Text, driver, "Level 2 category");

				List<WebElement> level3Category = level2Text.findElement(
						By.xpath("..")).findElements(By.cssSelector("ul[class='level-3']>li>a"));
				if (level3Category.size() > 0) {
					rand = ThreadLocalRandom.current().nextInt(1, level3Category.size());
					WebElement randomLevel3 = level3Category.get(rand - 1);
					level3 = BrowserActions.getText(driver, randomLevel3, "L3 category text");
					BrowserActions.javascriptClick(randomLevel3, driver, "Level 3 category");
				} else {
					throw new Exception("There is no L3 category for L1 - " + level1);
				}
				Log.event(" Clicked random L3 category: '" + level3 + "'");
			} else {
				WebElement level2Parent = BrowserActions.getMatchingTextElementFromList(
								BrowserActions.checkLocators(driver, "li[class*='l2-activate'] .hide-desktop>ul>li"), level2, "equals");
				WebElement level2ParentExpand = level2Parent.findElement(By.cssSelector("i"));
				BrowserActions.javascriptClick(level2ParentExpand, driver, "Level 1 Header Menu");
				WebElement level3catagory = BrowserActions.getMatchingTextElementFromList(
								level2Parent.findElements(By.xpath(HEADER_SUB_CATEGORY_l3)), level3, "equals");
				BrowserActions.javascriptClick(level3catagory, driver, "sub category(L3): " + level3);
			}
		}
		Utils.waitForPageLoad(driver);
		Log.event("Elapsed time for level4 navigation:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
		return new PlpPage(driver).get();
	}

	/**
	 * To click on Forget Password Link in SignIn Flyout
	 * @param email - user email id
	 * @return - Headers(if email is given), Password Reset Page(if no email is given)
	 * @throws Exception - Exception
	 */
	public Headers clickOnForgetPassword(String... email)throws Exception{
		mouseOverAccountMenu();
		if(email.length > 0) {
			BrowserActions.typeOnTextField(fldUserNameDesktop, email[0], driver, "Email Address in SignIn Flyout ");	
		}
		BrowserActions.javascriptClick(lnkForgetPasswordDesktop, driver, "Forget password Link in Desktop");
		Utils.waitForElement(driver, mdlResetPassword);
		return new Headers(driver).get();
	}

	/**
	 * To get the error message shown in Password Reset
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getPasswordResetError()throws Exception{
		return BrowserActions.getText(driver, lblEmailErrorInResetPassword, "Email Error in Password Reset ");
	}

	/**
	 * To type user's email id in email textbox in Password Reset model
	 * @param email - user's email id
	 * @throws Exception - Exception
	 */
	public void typeOnEmailInResetPasswordModel(String email)throws Exception{
		BrowserActions.typeOnTextField(txtEmailInResetPasswordMdl, email, driver, "Email Field in Reset Password Model ");
	}

	/**
	 * To verify the text mode of Password field
	 * @return 'text' - if text is visible. 'password' - if text is being protected
	 * @throws Exception - Exception
	 */
	public String verifyPasswordFieldTextMode()throws Exception{
		return fldPasswordDesktop.getAttribute("type");
	}

	/**
	 * To verify the password field and show link's parent are same
	 * @return - true(if both have same parent), false(if both doesn't have same parent)
	 * @throws Exception - Exception
	 */
	public boolean verifyPresenceOfShowInPassword()throws Exception{
		String class1 = fldPasswordDesktop.findElement(By.xpath("..")).getAttribute("class");
		String class2 = lnkShowHidePassword.findElement(By.xpath("..")).getAttribute("class");
		if(class1.equals(class2))
			return true;
		else
			return false;
	}

	/**
	 * To click on Show/Hide in Password field
	 * @return State of Password field - show(Not protected), hide(Protected)
	 * @throws Exception - Exception
	 */
	public String clickOnShowHideInPassword()throws Exception {
		BrowserActions.javascriptClickWithoutAction(lnkShowHidePassword, driver, "Show/Hide Link ");
		String passwordType = fldPasswordDesktop.getAttribute("type").equals("text")? "hide" : "show";
		return passwordType;
	}

	/**
	 * To navigate to profile page
	 * @return ProfilePage
	 * @throws Exception - Exception
	 */
	public OfferPage navigateToOfferPage()throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		BrowserActions.scrollToView(lnkOffer, driver);
		BrowserActions.clickOnElement(lnkOffer, driver, "Offers link in header");
		Utils.waitForPageLoad(driver);
		return new OfferPage(driver).get();
	}
	
	/**
	 * To tap on the offers link
	 * @throws Exception - Exception
	 */
	public void clickOnOffersLink()throws Exception {
		BrowserActions.scrollToView(lnkOffer, driver);
		BrowserActions.clickOnElement(lnkOffer, driver, "Offers link in header");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To get the other brands Loin message in SignIn Flyout
	 * @return string 
	 * @throws Exception - Exception
	 */
	public String getLoginMessage()throws Exception {
		String loginMessage = BrowserActions.getText(driver, lblOtherBrandLoginMsgDesktop, "Login Message ") + " " +
				BrowserActions.getText(driver, lblOtherBrandLoginBrandsDesktop, "Login Brand ");
		return loginMessage;
	}

	/**
	 * To click on back arrow in mobile breadcrumb
	 * @throws Exception - Exception
	 */
	public void clickBackArrowOnBreadCrumbMobile()throws Exception {
		BrowserActions.clickOnElementX(lnkArrowBreadCrumb_mobile, driver, "Bread crumb arrow ");
	}

	/**
	 * To get bread crumb text
	 * @return the bread crumb text
	 * @throws Exception - Exception
	 */
	public String getBreadCrumbText()throws Exception {
		if(Utils.isMobile()) {
			return BrowserActions.getText(driver, txtBreadCrumb_mobile, "Mobile BreadCrumb");
		} else {
			return BrowserActions.getText(driver, breadCrumb, "BreadCrumb");
		}
	}

	/**
	 * To navigate to Account Creation page by clicking on Create account in SignIn Flyout
	 * @return CreateAccountPage
	 * @throws Exception - Exception
	 */
	public CreateAccountPage navigateToCreateAccount()throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		if (Utils.isMobile()) {
			openCloseHamburgerMenu("open");
			BrowserActions.clickOnElementX(lnkAccountButtonMobile, driver, "Sign In Menu Link");
			Utils.waitForPageLoad(driver);
			BrowserActions.clickOnElement(btnCreateAccountMobile, driver, "Create Account button");
		} else {
			BrowserActions.clickOnElementX(lnkAccountButton, driver, "Sign In Menu Link");
			Utils.waitForElement(driver, flytSignInUnregisteredDesktop);
			BrowserActions.clickOnElement(btnFlyoutCreateAccount, driver, "Create Account button");
		}
		Utils.waitForPageLoad(driver);
		return new CreateAccountPage(driver).get();
	}
	
	/**
	 * To click on Create account in SignIn Flyout
	 * @throws Exception - Exception
	 */
	public void clickOnCreateAccount()throws Exception {
		BrowserActions.javascriptClick(btnFlyoutCreateAccount, driver, "Create a New Account Link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on SignOut link in MyAccount Flyout to signout
	 * @return SignIn page
	 * @throws Exception - Exception
	 */
	public SignIn clickOnSignOut()throws Exception {
		BrowserActions.clickOnElementX(lnkSignOutMyAccFlytDesktop, driver, "SignOut Link in My Acc Flyout", false);
		Utils.waitForPageLoad(driver);
		return new SignIn(driver).get();
	}
	
	/**
	 * To click on WishList link
	 * @return WishListPage - Wishlist page object
	 * @throws Exception - Exception
	 */
	public WishListPage clickOnWishList()throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		if(!(Utils.isMobile())) {
			mouseOverAccountMenu();
			BrowserActions.clickOnElementX(lnkWishListMyAccFlytDesktop, driver, "WishList Link in My Acc Flyout");
		}else {
			hamburgerMenu.openCloseHamburgerMenu("open");
			BrowserActions.clickOnElementX(lnkAccountButtonMobile, driver, "My account");
			BrowserActions.clickOnElementX(lnkWishListMyAccFlytDesktop, driver, "WishList Link");
		}
		Utils.waitForPageLoad(driver);
		return new WishListPage(driver).get();
	}

	/**
	 * To get signout url
	 * @return string - url
	 * @throws Exception - Exception
	 */
	public String getSignOutUrl()throws Exception {
		String url = BrowserActions.getTextFromAttribute(driver, lnkSignOutMyAccFlytDesktop, "href", "sign out");
		Utils.waitForPageLoad(driver);
		return url;
	}


	/**
	 * To clear the text in user name and password field in SignIn Flyout
	 * @throws Exception - Exception
	 */
	public void clearUserNamePassword()throws Exception {
		fldUserNameDesktop.clear();
		fldPasswordDesktop.clear();
	}

	/**
	 * To Navigate to other Brand's Home page
	 * @return Brand's Homepage
	 * @throws Exception - Exception
	 */
	public HomePage navigateToHome() throws Exception {
		WebElement lnkPrimaryLogo;
		try {
			if (Utils.isDesktop()) {
				lnkPrimaryLogo = driver.findElement(By.cssSelector("#brandHeader .active:not(.active-first) a img"));
			} else {
				lnkPrimaryLogo = driver.findElement(By.cssSelector("#brandHeader .active-first a img"));
			}
			BrowserActions.clickOnElementX(lnkPrimaryLogo, driver, "brand primary logo");
		} catch (NoSuchElementException nsle) {
			Log.event("Brand primary logo constructed WebElement not found.");
			if (Utils.isDesktop()) {
				lnkPrimaryLogo = brandLogo;
			} else {
				lnkPrimaryLogo = brandLogoMobTab;
			}
			BrowserActions.clickOnElementX(lnkPrimaryLogo, driver, "brand primary logo");
		}
		Utils.waitForPageLoad(driver);
		return new HomePage(driver).get();
	}

	/**
	 * To click on the Search Icon in the mobile viewport
	 * @throws Exception - Exception
	 */
	public void clickOnSearchIcon() throws Exception {
		BrowserActions.clickOnElementX(btnHeaderSearchIcon, driver, "Search Icon ");
	}
	
	/**
	 * To toggle open/close search panel
	 * @param toggle - search panel desired state
	 * @throws Exception
	 */
	public void toggleSearchPanel(Toggle toggle) throws Exception {
		boolean isPanelOpen = BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(txtSearch));
		
		if((isPanelOpen && toggle.equals(Toggle.Close)) || (!isPanelOpen && toggle.equals(Toggle.Open))) {
			BrowserActions.clickOnElementX(btnHeaderSearchIcon, driver, "Serach icon");
		}
	}

	/**
	 * To click on the Search Icon in the Desktop viewport
	 * @throws Exception - Exception
	 */
	public void clickFlyoutSearchButton()throws Exception {
		BrowserActions.clickOnElementX(btnFlyoutSearch, driver, "Search Icon ");
	}
	
	/**
	 * To click on mini-cart icon
	 * @throws Exception 
	 */
	public void clickMiniCartTablet() throws Exception {
		BrowserActions.refreshPage(driver);
		BrowserActions.scrollToTopOfPage(driver);		
		try{
			BrowserActions.scrollToView(lnkMiniCart, driver);
			BrowserActions.actionClick(lnkMiniCart, driver, "My Bag link ");
		}catch(WebDriverException x) {
			BrowserActions.javascriptClick(miniCartQty, driver, "My Bag link ");
		}
	}

	/**
	 * To click on the Mini cart
	 * @throws Exception - Exception
	 */
	public void clickAndHoldOnBag()throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		if(Utils.waitForElement(driver, lnkMiniCart)) {
			BrowserActions.clickAndHoldOnElement(lnkMiniCart, driver, "My Bag");
		}
		if(!driver.findElement(By.cssSelector(".mini-cart-total")).isDisplayed()) {
			Log.event("Mini Cart hover menu did not open up. Using workaround...");
			forceOpenMiniCartHover(driver);
		}
	}

	/**
	 * To click on the Mini cart
	 * @throws Exception - Exception
	 */
	public void tabMiniCartIcon()throws Exception {
		BrowserActions.clickAndHoldOnElement(lnkMiniCart, driver, "My Bag");
	}

	/**
	 * To click on the Mini cart to Navigate to Shopping bag page
	 * @return ShoppingBagPage - ShoppingBag Page object 
	 * @throws Exception - Exception
	 */
	public ShoppingBagPage clickOnBagLink()throws Exception {
		BrowserActions.clickOnElementX(lnkMiniCart, driver, "View Bag Link");
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	/**
	 * To click on the Mini cart a tag to Navigate to Shopping bag page
	 * @return ShoppingBagPage
	 * @throws Exception - Exception
	 */
	public ShoppingBagPage navigateToShoppingBagPage()throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		BrowserActions.clickOnElementX(lnkMiniCart, driver, "View Bag Link ");

		Utils.waitForPageLoad(driver);
		if(!Utils.waitForElement(driver, readyElementShoppingBagPage)){
			Log.reference("Failed to Navigate Shopping Bag via Element Click. Using work around...");
			driver.get(Utils.getWebSite() + "/cart");
			Utils.waitForPageLoad(driver);
		}
		return new ShoppingBagPage(driver).get();
	}

	/**
	 * To click outside of the page
	 * @param elem - Locator of an object to take coordinates
	 * @param obj - obj where locator is defined
	 * @throws Exception - Exception
	 */
	public void clickOutSide(String elem, Object obj)throws Exception {
		Field f = obj.getClass().getDeclaredField(elem);
		f.setAccessible(true);
		WebElement element = ((WebElement) f.get(obj));

		Point coordinates = element.getLocation();
		Robot robot = new Robot();
		robot.mouseMove(coordinates.getX(), coordinates.getY());
		Actions action = new Actions(driver);
		action.doubleClick();
		action.click().build().perform();
		action.moveByOffset(100, 100).build().perform();		
	}

	/**
	 * To click outside of the page
	 * @throws Exception - Exception
	 */
	public void clickOutSide()throws Exception {
		if(Utils.getRunBrowser(driver).equals("safari") || Utils.getRunBrowser(driver).equals("internet explorer")) {
			Log.fail("***Feature Not working in " + Utils.getRunBrowser(driver) + "***");
		}
		Actions action = new Actions(driver);
		action.moveByOffset(1, 1).click().build().perform();
		action.click().build().perform();
	}

	/**
	 * To navigate To PDP Page of Given product ID
	 * @param productID - 
	 * @return PDP Page
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPDP(String productID)throws Exception {
		final long startTime = StopWatch.startTime();
		Log.event("Navigating to:: " + productID);
		boolean hasVariation = false;
		boolean foundInSuggestion = false;
		String[] prdVariation = null;
		String color = null, size = null, family = null;
		if(productID.contains("|")) {
			hasVariation = true;
			prdVariation = productID.split("\\|");
			productID = prdVariation[0];
			if(prdVariation.length >= 3) {
				color = prdVariation[1];
				size = prdVariation[2];
			}
			if(prdVariation.length == 4) {
				family = prdVariation[3];
			}
		}
		
		try {
			BrowserActions.scrollToTopOfPage(driver);
			toggleSearchPanel(Toggle.Open);
			BrowserActions.typeOnTextField(txtSearch, productID, driver,"Search field");
			if (Utils.waitForElement(driver, autoSuggestPopup, 10)) {
				Log.event("Suggestion displayed.");
				try {
					WebElement suggestionTile = autoSuggestPopup.findElement(By.cssSelector("a[href*='" + productID + ".'] .product-image img"));
					BrowserActions.clickOnElementX(suggestionTile, driver, "Prd suggestion tile");
					foundInSuggestion = true;
				} catch (Exception e) {
					Log.event(e.getLocalizedMessage());
					BrowserActions.javascriptClick(btnFlyoutSearch, driver, "Search icon");
				}
			} else {
				Log.event("Suggestion not displayed.");
				BrowserActions.javascriptClick(btnFlyoutSearch, driver, "Search icon");
			}
		} catch(InvalidElementStateException e) {
			redirectToPDP(productID, driver);
		}
		Utils.waitForPageLoad(driver);

		if(!foundInSuggestion && Utils.waitForElement(driver, readyElementSLPPage)){
			Log.event("Navigated to Search Result Page.");
			SearchResultPage slp = new SearchResultPage(driver);
			if (slp.isProductPresentInPage(productID)) {
				slp.navigateToPdpByPrdID(productID);
			} else {
				String prdURL = Utils.getWebSite() + "/products/" + productID + ".html";
				driver.get(prdURL);
				if(driver.getCurrentUrl().contains("/404")) {
					Log.fail("Given Product not found...", driver);
				}
			}
		}
		
		PdpPage pdp = new PdpPage(driver).get();
		if(hasVariation) {
			Log.event("Product has variation. Trying to selct...");
			if(family != null) {
				pdp.selectSizeFamily(family);
			}
			try {
				pdp.selectColor(color);
				pdp.selectSize(size);
			}catch(Exception e) {
				Log.event("Color and/or Size variations not correctly configured in data.");
			}
		}
		Log.event("Elapsed time to navigateToPDP:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
		return pdp;
	}
	
	/**
	 * To navigate To PDP Page of Given product ID
	 * @param productName - give product name exactly same in the application
	 * @return Object - PDP Page
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPDPByProdName(String productName)throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		toggleSearchPanel(Toggle.Open);
		BrowserActions.clickOnElementX(txtSearch, driver, "Text field");
		BrowserActions.typeOnTextField(txtSearch, productName, driver,"Entering category in the search field");
		Log.event("Entered " + productName + " in search box.");
		BrowserActions.javascriptClick(btnFlyoutSearch, driver, "Search icon");
		try{
			Utils.waitUntilElementDisappear(driver, btnFlyoutSearch);
		} catch(Exception e) {
			Log.ticket("SC-6029");
			BrowserActions.javascriptClick(btnFlyoutSearch, driver, "Search Icon ");
		}
		Utils.waitForPageLoad(driver);
		
		if(Utils.waitForElement(driver, readyElementSLPPage)){
			Log.event("Navigated to Search Result Page.");
			SearchResultPage slp = new SearchResultPage(driver);
			if(slp.isProductPresentInPageBasedOnName(productName)) {
				PdpPage pdpPage = slp.navigateToPdpByPrdName(productName);
				return pdpPage;
			}
		}
		
		return new PdpPage(driver).get();
	}

	/**
	 * To navigate to pdp with direct hit
	 * @param prdID - 
	 * @param driver -
	 * @return PdpPage -
	 * @throws Exception -
	 */
	public PdpPage redirectToPDP(String prdID, WebDriver driver)throws Exception{
		String url = Utils.getWebSite() + "/products/" + prdID + ".html";
		Log.event("Current Url is: " + url);
		driver.get(url);
		return new PdpPage(driver).get();
	}

	/**
	 * TO get mini cart count
	 * @return string - mini cart count
	 * @throws Exception - Exception
	 */
	public String getMiniCartCount()throws Exception {
		String qty = "0";
		try{
			qty = miniCartQty.getText().trim();
			Log.event("Mini cart quantity is "+ qty);
			return qty;
		}catch(NoSuchElementException e){
			return qty;
		}
	}

	/**
	 * To navigate to Quick Order Landing Page
	 * @return QuickOrderPage - page object
	 * @throws Exception - Exception
	 */
	public QuickOrderPage navigateToQuickOrder()throws Exception {
		
		BrowserActions.scrollToTopOfPage(driver);
		BrowserActions.clickOnElementX(lnkQuickOrder, driver, "Quick Order Icon");
		return new QuickOrderPage(driver).get();
	}

	/**
	 * TO Close live chat modal
	 * @throws Exception - Exception
	 */
	public void closeLiveChat()throws Exception {
		if(Utils.waitForElement(driver, mdlLiveChat)) {
			BrowserActions.clickOnElementX(btnLiveChatMenu, driver, "Chat Menu Icon");
			Utils.waitForElement(driver, divLiveChatMenubar, 5);
			Log.message("Clicked on Menu button", driver);
			BrowserActions.javascriptClick(btnLiveChatClose, driver, "Chat Close Icon");
			Log.message("Clicked on Close button", driver);
			if(Utils.waitForElement(driver, btnLiveChatClose)) {
				BrowserActions.clickOnElement(btnLiveChatClose, driver, "chat modal close button");
			}
			Utils.waitUntilElementDisappear(driver, mdlLiveChat, 10);
		}else {
			Log.event("Chat Not Present");
		}
	}

	/**
	 * To navigate TO explore page
	 * @param String - level1 category name 
	 * @return Explore - explore pageobject
	 * @throws Exception - Exception
	 */
	public Explore navigateToExplore() throws Exception {
		try {
			String exploreURL = redirectData.get("explore_url_"+Utils.getCurrentBrandShort());
			if (Utils.isDesktop() || Utils.isTablet()) {
				if(BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(lnkExplore))) {
					BrowserActions.clickOnElementX(lnkExplore, driver, "Category 1 ");
				} else {
					Log.event("Navigation element not available, using workaround to navigate to Explore page.");
					driver.navigate().to(driver.getCurrentUrl()+exploreURL);
				}
			} else {
				openCloseHamburgerMenu("open");
				if(BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(lnkExploreMobile))) {
					BrowserActions.clickOnElementX(lnkExploreMobile, driver,
							"Level 1 Header Menu Toogle(+)");
				} else {
					Log.event("Navigation element not available, using workaround to navigate to Explore page.");
					driver.navigate().to(driver.getCurrentUrl()+exploreURL);
				}
			}
			
			Log.event(" -----> Clicked 'Explore'");
			Utils.waitForPageLoad(driver);
		}catch(Exception e) {
			Log.fail("Given Category navigation( Explore ) Not available.", driver);
		}
		return new Explore(driver).get();
	}

	/**
	 * To navigate to Sign In Page
	 * @return SignIn - SignIn Page object
	 * @throws Exception - Exception
	 */
	public SignIn navigateToSignInPage() throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		if (Utils.isMobile()) {
			openCloseHamburgerMenu("open");
			BrowserActions.clickOnElementX(lnkAccountButtonMobile, driver, "Sign In Menu Link");
		} else {
			mouseOverAccountMenu();
			Utils.waitForElement(driver, flytSignInUnregisteredDesktop, 10);
			BrowserActions.clickOnElementX(btnFlyoutCreateAccount, driver, "Create Account button", false);
		}

		Utils.waitForPageLoad(driver);
		if (Utils.waitForElement(driver, readyElementSignInPage)) {
			Log.event("Navigate to SignIn page");
			return new SignIn(driver).get();
		} else
			return null;
	}

	/**
	 * To navigate to PLP from Last category in Breadcrumb
	 * @return PlpPage - PlpPage page object
	 * @throws Exception - Exception
	 */
	public PlpPage navigateToLastSubCategory()throws Exception {
		if(Utils.isMobile()) {
			if(lnkSubCategoryBC_Mobile.getText().equalsIgnoreCase("Home")) {
				Log.fail("Product does not have proper category configuration. Least Sub category will navigate to Home.", driver);
			}
			BrowserActions.clickOnElementX(lnkSubCategoryBC_Mobile, driver, "Last Category in BC");
		} else {
			if(lnkSubCategoryBC.getText().equalsIgnoreCase("Home")) {
				Log.fail("Product does not have proper category configuration. Least Sub category will navigate to Home.", driver);
			}
			BrowserActions.clickOnElementX(lnkSubCategoryBC, driver, "Last Category in BC");
		}
		return new PlpPage(driver).get();
	}

	/**
	 * To navigate to PLP page for given product id.
	 * If searching the product navigates to SLP, then we will go into PDP of the product and then we will click the least breadcrumb link
	 * and navigated to PLP.
	 * 
	 * If searching the product navigates to PDP, then we will click the least breadcrumb link and navigate to PDP.
	 * @param String - product ID to open
	 * @return PlpPage - PlpPage page object
	 * @throws Exception - Exception
	 */
	public PlpPage navigateToPLP(String prdID)throws Exception {
		typeTextInSearchField(prdID);
		clickFlyoutSearchButton();
		WebElement wrapperElement = driver.findElement(By.id("wrapper"));
		if(wrapperElement.getAttribute("class").contains("product-search-page")) {
			new SearchResultPage(driver).get().navigateToPdpByPrdID(prdID);
		}else if(wrapperElement.getAttribute("class").contains("pt_error")) {
			Log.fail("Product configuration failure occured. cannot load PDP Page.", driver);
		}

		return new Headers(driver).get().navigateToLastSubCategory();
	}

	/**
	 * To log out from sign in account
	 * @return SignIn page object
	 * @throws Exception
	 */
	public SignIn signOut()throws Exception {
		if (Utils.isMobile()) {
			HamburgerMenu hMenu = (HamburgerMenu)openCloseHamburgerMenu("open");
			hMenu.clickOnSignOut();
		} else {
			mouseOverAccountMenu();
			clickOnSignOut();
		}
		return new SignIn(driver).get();
	}

	/**
	 * To click n-th breadcrumb
	 * @int breadcrumbIndex - index of breadcrumb to be clicked
	 * @throws Exception - Exception
	 */
	public void clickNthBreadcrumb(int breadcrumbIndex)throws Exception {
		BrowserActions.clickOnElementX(breadcrumbElements.get(breadcrumbIndex), driver, "n-th Breadcrumb");
	}
	
	/**
	 * To get the last Element of the breadcumb
	 * @return String - Breadcrumb text of last element
	 * @throws Exception - Exception
	 */
	public String getLastBreadcrumb() throws Exception {
		if(breadcrumbElements.size() == 1)
			return breadcrumbElements.get(0).getText();
		else
			return breadcrumbElements.get(breadcrumbElements.size()-2).getText();
	}

	/**
	 * To navigate to Wishlist Page
	 * @return WishListPage - WishListPage page object
	 * @throws Exception - Exception
	 */
	public WishListPage navigateToWishListPage()throws Exception {
		mouseOverAccountMenu();
		if (Utils.waitForElement(driver, lblUserAccDesktop)) {
			BrowserActions.clickOnElementX(lnkWishListMyAccFlytDesktop, driver, "WishList Link");
		} else {
			Log.event("Wishlist didn't open. Using workaround...");
			String wishlist = redirectData.get("wishlist");
			String nav_URL = Utils.getWebSite() + wishlist;
			driver.get(nav_URL);
		}
		return new WishListPage(driver).get();
	}

	/**
	 * To forcefully open mini cart hover menu in Tablet
	 * @throws Exception  - Exception
	 */
	public void forceOpenMiniCartHover(WebDriver driver) throws Exception {
		String classAttribute = lnkMiniCart.getAttribute("class");
		classAttribute = classAttribute.trim() + " DelayHover";
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.querySelector('.mini-cart-total').setAttribute('class', '"+classAttribute+"')");
	}
	
	/**
	 * TO get list of Global Navigation categories on site
	 * @throws Exception - Exception
	 * @return List<String> - Global Navigation Categories
	 */
	public List<String> getGlobalNavCategoriesList() throws Exception {
		List<String> navGlobalList = new ArrayList<String>();
		if(Utils.waitForElement(driver, lnkGlobalNavNonBildCategories)) {
			for(int i=0;i<lnkGlobalNavCategories.size();i++) {
				navGlobalList.add(lnkGlobalNavCategories.get(i).getText().toLowerCase());
			}
		}
		if(Utils.waitForElement(driver, lnkGlobalboldCategories)) {
				for(int i=0;i<lnkGlobalCategories.size();i++) {
				navGlobalList.add(lnkGlobalCategories.get(i).getText().toLowerCase());
			}
		}
		Log.event("Global navigation list: " + navGlobalList.toString());
		return navGlobalList;
	}
	
	/**
	 * To mouse hover on given leve1 category
	 * @param String - level1
	 * @Throws Exception - Exception
	 * @return WebElement - level1
	 */
	public WebElement mouseHoverOnCategory(String level1) throws Exception {
		WebElement level1Parent = null;
		if (level1.equals("random")) {
			int rand = ThreadLocalRandom.current().nextInt(0, lnkGlobalNavCategories.size() - 1);
			level1Parent = lnkGlobalNavCategories.get(rand);
		} else {
			level1Parent = getLevel1Category(level1);
		}
		BrowserActions.mouseHover(driver, level1Parent, "Level-1 category ");
		BrowserActions.mouseHover(driver, level1Parent, "Level-1 category ");
		Log.event("Mouse hovered on " + level1Parent.getText());
		return level1Parent;
	}
	 
	 /**
	  * To get number of column1 items in submenu
	  * @param String - level1
	  * @return boolean - column item verification
	  * @throws Exception - Exception
	  */
	 public boolean verifySubMenuColumnSize(String level1) throws Exception {
		 WebElement level1Parent = mouseHoverOnCategory(level1);
		 List<WebElement> level2ColumnElements = level1Parent.findElement(By.xpath("..")).findElements(By.cssSelector(".level-2 > .sub-cat-wrapper > .sub-cat-nav ul.col-1>li>div"));
		 List<WebElement> level2ColumnElements2 = level1Parent.findElement(By.xpath("..")).findElements(By.cssSelector(".level-2 > .sub-cat-wrapper > .sub-cat-nav ul.col-1 ul.col-2>li>div"));
		 return level2ColumnElements2.size()<=(8-level2ColumnElements.size());
	 }
	 
	 /**
	  * To get number of column1 items in submenu
	  * @param String - level1
	  * @return boolean - column item verification
	  * @throws Exception - Exception
	  */
	 public boolean verifyCategoryFlyOutDisplays(String level1) throws Exception {
		 WebElement level1Parent = mouseHoverOnCategory(level1);
		 WebElement level2ColumnElements = level1Parent.findElement(By.xpath("..")).findElement(By.cssSelector(".level-2 > .sub-cat-wrapper > .sub-cat-nav"));
		 return Utils.waitForElement(driver, level2ColumnElements);
	 }
	 
	 /**
	  * To Verify banner image shown is correct
	  * @param String - level1 category name
	  * @return boolean - true if banner image if from correct week, else false 
	  * @throws Exceptio - Exception
	  */
	 public boolean verifyBannerImageDate(String level1) throws Exception {
		 mouseHoverOnCategory(level1);

		 if(currentLevel2Banner.isDisplayed()) {
			 String src = currentLevel2Banner.findElement(By.cssSelector("section img")).getAttribute("src");
			 Calendar now = Calendar.getInstance();
			 String yearCurrent = ""+now.get(Calendar.YEAR);
			 String weekCurrent = "wk"+now.get(Calendar.WEEK_OF_YEAR);
			 Log.event("Current Year: "+ yearCurrent+ ", Current Week: "+ weekCurrent);
			 return src.contains(yearCurrent) && src.contains(weekCurrent) && src.contains(Utils.getCurrentBrandShort()) && src.contains(level1);
		 } else
			 return false;
	 }
	 
	 /**
	  * To verify search suggestions are shown with all components
	  * @return boolean - true/false for if all components are shown
	  * @throws Exception - Exception
	  */
	 public boolean verifySearchSuggestions() throws Exception {
		 Log.event("Number of search suggestions:: " + lstProductSuggestion.size());
		 int maxProductSuggestion;
		 if(Utils.isMobile()) {
			 maxProductSuggestion = 2;
		 } else {
			 maxProductSuggestion = 3;
		 }
		 for(int i=0; i<maxProductSuggestion && i< lstProductSuggestion.size(); i++) {
			 WebElement suggestion = lstProductSuggestion.get(i);
			 
			 if(Utils.waitForElement(driver, suggestion.findElement(By.cssSelector(".product-image")))) {
				 Log.event("Search suggestion image is displayed");
			 }else {
				 Log.event("Search suggestion image is not displayed");
				 return false;
			 }
			 
			 if(Utils.waitForElement(driver, suggestion.findElement(By.cssSelector(".product-name")))) {
				 Log.event("Search suggestion name is displayed");
			 }else {
				 Log.event("Search suggestion name is not displayed");
				 return false;
			 }
			 
			 if(Utils.waitForElement(driver, suggestion.findElement(By.cssSelector(".product-price")))) {
				 Log.event("Search suggestion price is displayed");
			 }else {
				 Log.event("Search suggestion price is not displayed");
				 return false;
			 }
		 }
		 return true;
	 }
	 
	/**
	 * To verify that correct account options for loggen in user are displayed 
	 * @param String - List of expected options
	 * @throws Exception - Exception
	 */
	public boolean verifySignedInMenuOptions(String expectedOptions) throws Exception {
		List<String> expectedOptionsList = Arrays.asList(expectedOptions.toUpperCase().split("\\|"));
		List<String> actualOptionsList = new ArrayList<String>();
		for(WebElement menuItem : lnkSignedInMenuOptions) {
			actualOptionsList.add(menuItem.getText().toUpperCase());
		}
		if(expectedOptionsList.equals(actualOptionsList)) {
			return true;
		}else {
			List<String> intersection = new ArrayList<String>(expectedOptionsList);
			intersection.retainAll(actualOptionsList);
			Log.reference("Missing Options: " + intersection.toString());
			return false;
		}
	}

	/**
	 * To click on PLCC application link on header
	 * @return PlatinumCardApplication page object
	 */
	public PlatinumCardApplication clickOnPLCCApplyNow() throws Exception {
		if (Utils.waitForElement(driver, lnkApplyNow)) {
			BrowserActions.clickOnElementX(lnkApplyNow, driver, "'Apply Now' link in Header");
			Utils.waitForPageLoad(driver);
			return new PlatinumCardApplication(driver).get();
		} else {
			Log.warning("PLCC apply button not found in header.");
			return null;
		}
	}
	
	/**
	 * To navigate to CSR page
	 * @throws Exception
	 */
	public CSR navigateToCSR() throws Exception {
		String urlCSR = UrlUtils.getWebSite(Utils.getCurrentEnv(), Brand.rmx) + redirectData.get("login_page_CSR");
		urlCSR = UrlUtils.applyYottaSkip(urlCSR);
		Log.event("CSR url:: " + urlCSR);
		driver.navigate().to(urlCSR);
		return new CSR(driver).get();
	}

	/**
	 * To verify the promo banner message is displayed.
	 * @return true - if message is displayed as expected
	 * @throws Exception 
	 */
	public boolean checkPromoBannerMessage() throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		String promoBannerText = divPromoContent.getText().split("!")[0].toLowerCase();
		String currentBrandName = BrandUtils.getBrandFullName();
		return (promoBannerText.contains(plccHeaderPromoMessage) && BrandUtils.containBrandName(promoBannerText, currentBrandName) && promoBannerText.contains(plccHeaderMessage));
	}
	
	/**
     * To verify the user is logged in to site
     * @return loggedInStatus - True if user is logged in else false
     * @throws Exception 
     */
	public boolean verifyUserIsLoggedIn() throws Exception {
		if (Utils.waitForElement(driver, btnAccountButtonActive)) {
			Log.ticket("SC-5383");
			clickOnSearchIcon();
		}
		boolean loggedInStatus = false;
		if (Utils.isMobile()) {
			openCloseHamburgerMenu("open");
			BrowserActions.clickOnElementX(lnkAccountButtonMobile, driver, "Sign In Menu Link");
			loggedInStatus = Utils.waitForElement(driver, readyElementMyAccountPage);
			openCloseHamburgerMenu("close");
		} else {
			BrowserActions.clickOnElementX(lnkAccountButton, driver, "Sign In Menu Link");
			loggedInStatus = Utils.waitForElement(driver, flytSignInRegisteredDesktop);
		}
		return loggedInStatus;
	}
    
    /**
     * To verify the user is logged in to site
     * @return loggedInStatus - True if user is logged in else false
     * @throws Exception 
     */
	public boolean verifyUserIsLoggedOut() throws Exception {
		boolean isLoggedOut = false;
		if (Utils.isMobile()) {
			openCloseHamburgerMenu("open");
			BrowserActions.clickOnElementX(lnkAccountButtonMobile, driver, "Sign In Menu Link");
			isLoggedOut = Utils.waitForElement(driver, readyElementSignInPage);
		} else {
			BrowserActions.clickOnElementX(lnkAccountButton, driver, "Sign In Menu Link");
			isLoggedOut = Utils.waitForElement(driver, flytSignInUnregisteredDesktop, 10);
		}
		return isLoggedOut;
	}
    
    /**
     * To get hover message from brand sc tile.
     * @param brandToHover -  which brand to hover on
     * @return hoverMessage - SC hover message.
     * @throws Exception 
     */
    public String getHoverMessageFromBrandScTile(Brand brandToHover) throws Exception {
    	String hoverMessage = "";
    	hoverOnBrandScTile(brandToHover);
    	if(Utils.waitForElement(driver, brandHoverMessage)) {
    		hoverMessage = BrowserActions.getText(driver, brandHoverMessage, "Brand title");
    	} else {
    		Log.failsoft("SC Hover message is not displayed", driver);
    	}
        return hoverMessage;
    }
    
    /**
     * To hover on brand SC tile.
     * @param brandToHover - which brand to hover on
     * @return void
     * @throws Exception 
     */
    public void hoverOnBrandScTile(Brand brandToHover) throws Exception {
        if (!envProperty.get("enabledSharedCart").contains(brandToHover.toString())) {
    		Log.failsoft(brandToHover.getConfiguration() + " is not yet included in Shared Cart.");
    	} else {
    		WebElement brandTile = getSharedCartBrandTile(brandToHover);
    		BrowserActions.mouseHover(driver, brandTile, "Brand title");
    	}
    }
    
    /**
     * To get mid point location of brand sc tile.
     * @param brandToGetLocation - which brand to get location
     * @return midLocation - mid point(x,y)
     * @throws Exception 
     */
    public Point getMidPointLocationOfBrandScTile(Brand brandToGetLocation) throws Exception {
    	Point midLocation = new Point(0,0);
    	WebElement brandImgLocator;
    	String brandName;
    	int xPosition, yPosition;
    	if(!envProperty.get("enabledSharedCart").contains(brandToGetLocation.toString())) {
    		Log.failsoft(brandToGetLocation.getConfiguration() + " is not yet included in Shared Cart.");
    	} else {
    		for(WebElement brandLocator : lstBrandSelectors) {
    			Point value = brandLocator.getLocation();
    			brandImgLocator = brandLocator.findElement(By.cssSelector(" img"));
	        	brandName = BrowserActions.getTextFromAttribute(driver, brandImgLocator, "alt", "Brand image").split("-")[0];
	        	if(brandName.equalsIgnoreCase(BrandUtils.getBrandFullName(brandToGetLocation))) {
	        		xPosition = brandLocator.getSize().getWidth();
	        		int xValue = value.x + (xPosition/2);
	        		yPosition = brandLocator.getSize().getHeight();
	        		int yValue = value.y + (yPosition/2);
	        		midLocation.move(xValue, yValue);
	    			break;
	        	}
    		}
    	}
    	return midLocation;
    }
    
    /**
	 * To get Search suggestion by given product ID
	 * @param prodID - Product ID to enter in the search box
	 * @return - <a> href URL of the search suggestion product
	 * @throws Exception - Exception
	 */
	public String getATagFromSearchSuggestionByProductID(String prodID)throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		if(Utils.isMobile()){
			if(!(BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(txtSearch)))) {
				BrowserActions.clickOnElementX(btnHeaderSearchIcon, driver, "Serach icon");
			}
			BrowserActions.typeOnTextField(txtSearch, "", driver,"Search field");
			BrowserActions.typeOnTextField(txtSearch, prodID, driver,"Search field");
		} else {
			BrowserActions.typeOnTextField(txtSearch, "", driver,"Search field");
			BrowserActions.typeOnTextField(txtSearch, prodID, driver,"Search field");
		}
		WebElement suggestionElem = null;
		Utils.WaitForAjax(driver);
		if(Utils.waitForElement(driver, productSuggestion)) {
			suggestionElem = driver.findElement(By.xpath("//a[contains(@href,'"+prodID+"')]"));
			String value = BrowserActions.getTextFromAttribute(driver, suggestionElem, "href", "A Tag of Search suggestion").trim();
			return value;
		}
		return null;
	}
	
	/**
	 * To verify Search suggestion is displaying a product only once
	 * @param prodID - Product ID to enter in the search box
	 * @return - true if displaying once, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyInSearchSuggestionProductIsDisplayingOnce(String prodID)throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		if(Utils.isMobile()){
			if(!(BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(txtSearch)))) {
				BrowserActions.clickOnElementX(btnHeaderSearchIcon, driver, "Serach icon");
			}
			BrowserActions.typeOnTextField(txtSearch, "", driver,"Search field");
			BrowserActions.typeOnTextField(txtSearch, prodID, driver,"Search field");
		} else {
			BrowserActions.typeOnTextField(txtSearch, "", driver,"Search field");
			BrowserActions.typeOnTextField(txtSearch, prodID, driver,"Search field");
		}
		Utils.WaitForAjax(driver);
		if(Utils.waitForElement(driver, productSuggestion)) {
			String value = BrowserActions.getTextFromAttribute(driver, productSuggestionATag, "href", "A Tag of Search suggestion").trim();
			if(value.contains(prodID) && (lstProductSuggestions.size() == 1)) {
				return true;
			}
		}
		return false;
	}
	
	/**
     * To get mid point location of oss-brand arrow.
     * @param brandToGetLocation - which brand to get location
     * @return midLocation - mid point(x,y)
     * @throws Exception 
     */
    public Point getMidPointLocationOfBrandArrow(Brand brandToGetArrowLocation) throws Exception {
        String brandName;
        Point midLocation = new Point(0,0);
        int xPosition, yPosition;
        if (!envProperty.get("enabledSharedCart").contains(brandToGetArrowLocation.toString())) {
            Log.failsoft(brandToGetArrowLocation.getConfiguration() + " is not yet included in Shared Cart.");
        } else {
            for(int i = 0; i < lstBrandSelectors.size() ; i++) {
                WebElement brandLocator = lstBrandSelectors.get(i).findElement(By.cssSelector(" img"));
                brandName = BrowserActions.getTextFromAttribute(driver, brandLocator, "alt", "Brand image").split("-")[0];
                if(brandName.equalsIgnoreCase(BrandUtils.getBrandFullName(brandToGetArrowLocation))) {
                    WebElement ele = lstBrandSelectors.get(i).findElement(By.cssSelector("span.arrow-up"));
                    Point value = ele.getLocation();
                    xPosition = ele.getSize().getWidth();
                    int xValue = value.x + (xPosition/2);
                    yPosition = ele.getSize().getHeight();
                    int yValue = value.y + (yPosition/2);
                    midLocation.move(xValue, yValue);
                    break;
                } 
            }
        }
        return midLocation;
    }
    
    /**
     * To verify the hover message of the all the brands
     * @return true - if hovered brand displays the message with specific brand name
     */
    public boolean verifyHoverMessageAllBrands() throws Exception {
    	String hoverMessage = "";
    	List<Brand> brandsEnv = BrandUtils.getEnvPropertyBrands("enabledSharedCart");
    	for(Brand targetBrand : brandsEnv) {
    		hoverMessage = getHoverMessageFromBrandScTile(targetBrand);
    		if(! BrandUtils.containBrandName( hoverMessage , BrandUtils.getBrandFullName(targetBrand) ) ) {
    			return false;
    		}
    	}
        return true;
    }

    /**
     * To verify the current active brand favicon is not clickable
     * @return True - if the current active brand favicon is not clickable
     */
    public boolean verifyCurrentBrandNotClickable() throws Exception {
        if(divActiveBrand.findElements(By.cssSelector("a")).isEmpty()) {
            return true;
        }
        return false;
    }
    
    /**
     * To verify the current active brand favicon is not clickable
     * @return True - if the current active brand favicon is not clickable
     */
    public Brand getActiveSharedCartBrandName() throws Exception {
        String url = null;
        Brand brand = null;
        WebElement activeBrand;
        if(Utils.isDesktop()) {
               activeBrand = divActiveBrand;
        } else {
               activeBrand = divActiveBrandMobTab;
        }
        BrowserActions.scrollToTopOfPage(driver);
        if (Utils.waitForElement(driver, activeBrand)) {
               url = activeBrand.findElement(By.cssSelector("img.selected")).getAttribute("src");
               brand = UrlUtils.getBrandFromUrl(url);
        }
        return brand;
    }


    /**
     * To verify the active brand and location is displayed in first
     * @return 'true' - given brand is active and displayed in first
     *         'false' - given brand not available or not displayed in first
     * @throws Exception - Exception
     */
    public boolean verifyActiveScBrandLocation(Brand name) throws Exception {
    	Brand brandCheck = getActiveSharedCartBrandName();
        if(!brandCheck.equals(name)) {
            Log.failsoft("Given brand name is not available in the site");
            return false;
        } else {
	        Point rmx = getMidPointLocationOfBrandScTile(Brand.rmx);
	        Point jlx = getMidPointLocationOfBrandScTile(Brand.jlx);
	        Point elx = getMidPointLocationOfBrandScTile(Brand.elx);
	        if( ((rmx.x < jlx.x) && (jlx.x < elx.x)) || ((jlx.x < rmx.x) && (rmx.x < elx.x)) || ((elx.x < rmx.x) && (rmx.x < jlx.x)) ) {
	                return true;
	        }
        }
        return false;
    }
    
    /**
     * To verify the cursor is in primary search box
     * @return 'true' - cursor is in primary search box
     *         'false' - cursor not placed in primary search box
     * @throws Exception - Exception
     */
    public boolean verifyPrimarySearchCursor()throws Exception{
        boolean status = false;
        if(Utils.isMobile()){
            if(!(BrowserActions.VerifyElementDisplayed(driver, Arrays.asList(txtSearch))))
                BrowserActions.clickOnElementX(btnHeaderSearchIcon, driver, "Serach icon");
            BrowserActions.typeOnTextField(txtSearch, "", driver,"Search field");
        } else {
            BrowserActions.scrollToTopOfPage(driver);
            BrowserActions.clickOnElementX(txtSearch, driver, "Serach field");
        }
        if(txtSearch.equals(driver.switchTo().activeElement()))
            status = true;
        
        return status;
    }
    
    /**
     * To click on Learn more link PLCC banner from header
     * @return PlatinumCardLandingPage
     * @throws Exception - Exception
     */
    public PlatinumCardLandingPage clickOnLearnmorelinkPLCC() throws Exception {
    	Utils.waitForElement(driver, lnkLearnMore);
    	BrowserActions.clickOnElementX(lnkLearnMore, driver, "Learn More Link");
    	return new PlatinumCardLandingPage(driver).get();
    }
    
    /**
     * TO log out user
     * @throws Exception
     */
    public void logoutUser() throws Exception {
    	if (Utils.isDesktop() || Utils.isTablet()) {
    		//Tablet logout UI/UX will have to be updated when site is updated.
    		mouseOverAccountMenu();
    		BrowserActions.clickOnElementX(lnkSignOutMyAccFlytDesktop, driver, "Signout button", false);
    	} else if (Utils.isMobile()) {
    		HamburgerMenu hMenu = (HamburgerMenu) openCloseHamburgerMenu("open");
    		hMenu.clickOnSignOut();
    	}
    	BuildProperties.setInitialLoadMainBrand(true);
    	Log.event("Clicked on log out from menu.");
    }
    
    /**
     * To open account menu
     * @throws Exception
     */
    public void openAccountMenu() throws Exception{
    	if(!Utils.waitForElement(driver, divAccountMenu)) {
    		if (Utils.isMobile()) {
    			openCloseHamburgerMenu("open");
    			BrowserActions.clickOnElement(lnkAccountButtonMobile, driver, "My Account link in header");
    		} else {
    			BrowserActions.clickOnElement(lnkAccountButton, driver, "My Account link in header");
    		}
    		Utils.waitForElement(driver, divAccountMenu, 10);
    		Log.event("Account menu opened");
    	}
    }
}
