package com.fbb.pages;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.headers.Headers;
import com.fbb.reusablecomponents.BillingPageUtils;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.reusablecomponents.Enumerations.TestGiftCardValue;
import com.fbb.reusablecomponents.ShippingPageUtils;
import com.fbb.reusablecomponents.TestData;
import com.fbb.support.Brand;
import com.fbb.support.BrowserActions;
import com.fbb.support.DateTimeUtility;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StopWatch;
import com.fbb.support.StringUtils;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;

public class CheckoutPage extends LoadableComponent<CheckoutPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	private static EnvironmentPropertiesReader demandWareProperty = EnvironmentPropertiesReader.getInstance("demandware");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");

	
	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	//Shipping Address Fields
	private static final String txtShippingNickName = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_nickName']";
	private static final String txtShippingFirstName = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_firstName']";
	private static final String txtShippingLastName = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_lastName']";
	private static final String txtShippingAddress = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_address1']";
	private static final String txtShippingAddress2 = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_address2']";
	private static final String txtShippingCity = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_city']";
	private static final String txtShippingZipcode = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_postal']";
	private static final String txtShippingPhoneNo = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_phone']";
	private static final String btnShippingStateDiv = "#dwfrm_singleshipping_shippingAddress_addressFields_states_state + .selected-option";
	private static final String btnShippingState = "#dwfrm_singleshipping_shippingAddress_addressFields_states_state";
	//private static final String btnShippingCountryDiv = "#dwfrm_singleshipping_shippingAddress_addressFields_country + .selected-option";
	private static final String btnShippingCountry = "#dwfrm_singleshipping_shippingAddress_addressFields_country";

	//Shipping Checkboxes
	private static final String chkShippingMakeItDefault = "#dwfrm_singleshipping_shippingAddress_makeDefaultAddress";
	private static final String chkShippingSaveThisAddress = "#dwfrm_singleshipping_shippingAddress_addToAddressBook";
	private static final String chkUseAddressForBilling = "#dwfrm_singleshipping_shippingAddress_useAsBillingAddress";
	//Free Gift Message Fields
	private static final String txtFreeGiftMessage1 = "#dwfrm_singleshipping_shippingAddress_giftMessage1";
	private static final String txtFreeGiftMessage2 = "#dwfrm_singleshipping_shippingAddress_giftMessage2";
	private static final String txtFreeGiftMessage3 = "#dwfrm_singleshipping_shippingAddress_giftMessage3";

	//Billing Address Fields
	private static final String txtBillingNicName = "input[id='dwfrm_billing_billingAddress_addressFields_nickName']";
	private static final String txtBillingFirstName = "input[id='dwfrm_billing_billingAddress_addressFields_firstName']";
	private static final String txtBillingLastName = "input[id='dwfrm_billing_billingAddress_addressFields_lastName']";
	private static final String txtBillingAddress = "input[id='dwfrm_billing_billingAddress_addressFields_address1']";
	private static final String txtBillingAddress2 = "input[id='dwfrm_billing_billingAddress_addressFields_address2']";
	private static final String txtBillingCity = "input[id='dwfrm_billing_billingAddress_addressFields_city']";
	private static final String txtBillingZipcode = "input[id='dwfrm_billing_billingAddress_addressFields_postal']";
	private static final String txtBillingPhoneNo = "input[id='dwfrm_billing_billingAddress_addressFields_phone']";
	private static final String btnBillingStateDiv = "#dwfrm_billing_billingAddress_addressFields_states_state + .selected-option";
	private static final String btnBillingState = "#dwfrm_billing_billingAddress_addressFields_states_state";
	private static final String btnBillingCountryDiv = "#dwfrm_billing_billingAddress_addressFields_country + .selected-option";
	//private static final String btnBillingCountry = "#dwfrm_billing_billingAddress_addressFields_country";
	
	//AVS Modal
	private static final String divAddressValidationModal = ".address-validation-dialog[style*='display: block'] ";
	private static final String flytAddressSuggestion = divAddressValidationModal + "#address-validation-dialog";

	//Billing
	private static final String txtbillingFirstName = "input[id='dwfrm_billing_billingAddress_addressFields_firstName']";
	private static final String txtbillingLastName = "input[id='dwfrm_billing_billingAddress_addressFields_lastName']";

	@FindBy(css = txtBillingNicName)
	WebElement txtBillingNickNameFld;

	@FindBy(css = txtBillingFirstName)
	WebElement txtBillingFirstNameFld;

	@FindBy(css = txtBillingLastName)
	WebElement txtBillingLastNameFld;

	@FindBy(css = txtBillingAddress)
	WebElement txtBillingAddressFld;

	@FindBy(css = txtBillingCity)
	WebElement txtBillingAddress1Fld;
	
	@FindBy(css = btnBillingStateDiv)
	WebElement drpBillingAddressState;

	@FindBy(css = txtBillingZipcode)
	WebElement txtBillingZipcodeFld;

	@FindBy(css = txtBillingPhoneNo)
	WebElement txtBillingPhoneFld;
	
	@FindBy(css = btnBillingState)
	WebElement txtBillingstate;
	
	@FindBy(css = ".help-click.hide-desktop.hide-tablet")
	WebElement headerHelpSection;

	//Billing Checkboxes
	private static final String chkBillingMakeItDefault = "input[id='dwfrm_billing_billingAddress_makeDefault']"; 
	private static final String chkBillingSaveThisAddress = "input[id='dwfrm_billing_billingAddress_addToAddressBook']";

	//Shippment Methods
	private static final String rdoStandard = "input[id*='shipping-method-Regular']";
	private static final String rdoExpress = "input[id*='shipping-method-Express']";
	private static final String rdoSuperFast = "input[id*='shipping-method-SuperFast']";
	private static final String rdoNextDay = "input[id*='shipping-method-NextDay']";
	private static final String rdoHeathers = "input[id*='shipping-method-heathertestingfixedpriceship']";
	
	@FindBy(css = rdoStandard)
	WebElement radioStandardDelivery;

	@FindBy(css = rdoExpress)
	WebElement radioExpressDelivery;

	@FindBy(css = rdoSuperFast)
	WebElement radioSuperFastDelivery;

	@FindBy(css = rdoNextDay)
	WebElement radioNextDayDelivery;

	@FindBy(css = rdoHeathers)
	WebElement radioHeathersDelivery;
	
	@FindBy(css = "input[id*='shipping-method']")
	List<WebElement> rdoShippingMethods;
	
	@FindBy(css = "label[for*='Regular']")
	WebElement lblStandardShip;
	
	@FindBy(css = "label[for*='Express']")
	WebElement lblExpressShip;
	
	@FindBy(css = "label[for*='SuperFast']")
	WebElement lblSuperfastShip;
	
	@FindBy(css = "label[for*='NextDay']")
	WebElement lblNextDayShip;
	
	@FindBy(css = "label[for*= 'Regular'] .base-shipping")
	WebElement spanStandardDeliveryCost;
	
	@FindBy(css = "label[for*= 'Express'] .base-shipping")
	WebElement spanExpressDeliveryCost;
	
	@FindBy(css = "label[for*= 'SuperFast'] .base-shipping")
	WebElement spanSuperFastDeliveryCost;
	
	@FindBy(css = "label[for*= 'NextDay'] .base-shipping")
	WebElement spanNextDayDeliveryCost;
	
	@FindBy(css = "label[for*= 'shipping-method'] .base-shipping")
	List<WebElement> lstDeliveryCosts;

	//Payment Fields
	private static final String btnCardTypeDiv = "#dwfrm_billing_paymentMethods_creditCard_type + .selected-option";
	private static final String btnCardType = "select#dwfrm_billing_paymentMethods_creditCard_type";
	private static final String txtNameOnCard = "input[id='dwfrm_billing_paymentMethods_creditCard_owner']";
	private static final String txtCardNo = "input[id*='dwfrm_billing_paymentMethods_creditCard_number']";
	private static final String txtCvnNo = "input[id*='dwfrm_billing_paymentMethods_creditCard_cvn']";
	private static final String txtCvnNoForSavedCard = "input[id*='dwfrm_billing_paymentMethods_creditCard_cvn'][class*='valid']";
	private static final String btnCardExpMonth = "#dwfrm_billing_paymentMethods_creditCard_expiration_month";
	private static final String btnCardExpYear = "#dwfrm_billing_paymentMethods_creditCard_expiration_year";

	//Continue Buttons
	private static final String btnContinueToPlaceOrderTxt = ".checkout-order-summary ";

	@FindBy(id = "dwfrm_singleshipping_addressList")
	WebElement drpShippingAddress;

	@FindBy(xpath = "//select[id='dwfrm_singleshipping_addressList']//following-sibling::div")
	WebElement drpShippingAddressDiv;

	@FindBy(css = txtShippingFirstName)
	WebElement txtShippingFirstNameFld;

	@FindBy(css = txtShippingLastName)
	WebElement txtShippingLastNameFld;

	@FindBy(css = txtShippingCity)
	WebElement txtShippingCityFld;

	@FindBy(css = btnContinueToPlaceOrderTxt + "button")
	WebElement btnContinueToPlaceOrder;

	@FindBy(css = "#wrapper")
	WebElement divWrapper;
	
	@FindBy(css = ".mobile-isgift input")
	WebElement giftMessageDesktop;

	@FindBy(css = ".desktop-isgift input")
	WebElement giftMessageMobile;
	
	@FindBy(css = "label[for='dwfrm_billing_paymentMethods_creditCard_owner'] .label-text")
	WebElement lblNameOnCard;

	@FindBy(css = "input[id='dwfrm_billing_paymentMethods_creditCard_owner']")
	WebElement fldTxtNameOnCard;

	@FindBy(css = "label[for*='dwfrm_billing_paymentMethods_creditCard_owner'] .error")
	WebElement lblNameOnCardError;

	@FindBy(css = "label[for*='dwfrm_billing_paymentMethods_creditCard_number'] .label-text")
	WebElement lblTxtCardNo;
	
	@FindBy(css = ".international-cart-message")
	WebElement internationalCheckoutBanner;
	
	@FindBy(css = "label[for*='dwfrm_billing_paymentMethods_creditCard_number'] .error")
	WebElement lblTxtCardNoError;
	
	@FindBy(css = ".form-row.creditcardnumber.number.required .server-error")
	WebElement lblTxtCreditCardNoError;

	@FindBy(css = "input[id*='dwfrm_billing_paymentMethods_creditCard_number']")
	WebElement fldTxtCardNo;
	
	@FindBy(css = ".required.creditcardnumber.error")
	WebElement plccCardNoError;
	
	@FindBy(css = ".items-in-bag .address-actions a")
	WebElement lnkEditItemSummary;
	
	@FindBy(css = ".cart-columns .quick-order-badge.hide-desktop .quick-order-badge-link .heading")
	WebElement txtCatalogQuickOrder_Mobile;

	@FindBy(css = ".cart-columns .quick-order-badge.hide-mobile .quick-order-badge-link .heading")
	WebElement txtCatalogQuickOrder_Desk_Tab;

	@FindBy(css = "button[name='dwfrm_profile_confirm']")		
	WebElement btnCreateAccountOrderReceipt;
	
	@FindBy(css = ".country.required")		
	WebElement selectShippingCountry;
	
	@FindBy(css = ".item-details .specialmessaging")		
	WebElement txtProductInfo; 
	
	@FindBy(css = ".specialmessaging-Mobile .specialmessaging")		
	WebElement txtProductInfoMobile;
	
	@FindBy(css = ".product-list-item .name a")
    List<WebElement> prodNames;
	
	@FindBy(xpath = "//div[@class='specialmessag clearboth']//p[contains(text(),'Additional Shipping & Handling')]")
	WebElement txtSurchargeDeskAndTab;
	
	@FindBy(xpath = "//div[@class='specialmessaging-Mobile']//p[contains(text(),'Additional Shipping & Handling')]")
	WebElement txtSurchargeMob;
			
	@FindBy(css = ".selectpayment .disabled-hover-event")		
	WebElement btnPaymentDisable;
	
	@FindBy(css = ".type .selected-option.selected")		
	WebElement selectedCard;
	
	@FindBy(css = ".country.required li.selected")		
	WebElement selectShippingDefCountry;
	
	@FindBy(css = "input[id='shipping-method-Express']")		
	WebElement radioExpressCheckout;
	
	@FindBy(css = ".order-shipping-discount .value")		
	WebElement ordShippingDiscount;
	
	@FindBy(css = ".field-desc .promo")		
	WebElement lblFreeShipping;
	
	@FindBy(id = "dwfrm_singleshipping_shippingAddress_addressFields_firstName-error")		
	WebElement errShippingFirstName;		

	@FindBy(id = "dwfrm_singleshipping_shippingAddress_addressFields_lastName-error")		
	WebElement errShippingLastName;		

	@FindBy(id = "dwfrm_singleshipping_shippingAddress_addressFields_postal-error")		
	WebElement errShippingZipcode;		

	@FindBy(id = "dwfrm_singleshipping_shippingAddress_addressFields_phone-error")		
	WebElement errShippingPhone;
	
	@FindBy(css = ".city.required span.error")		
	WebElement errShippingCity;
	
	@FindBy(css = ".shipping .address-dropdown .selected-option.selected")
	WebElement selectedShippingAddress;
	
	@FindBy(css = ".pt_order-confirmation")		
	WebElement readyElemForOrdCnfPg;
	
	@FindBy(css = ".checkout-tab.billing .editaddress")		
	WebElement lnkEditBilling;
	
	@FindBy(css = ".checkout-billing .head-wrapper legend")		
	WebElement lblBillingHeading;
	
	@FindBy(css = ".address1.required span.error")		
	WebElement errShippingAddress1;
	
	@FindBy(css = ".checkout-shipping .head-wrapper legend")		
	WebElement lblShippingHeading;
	
	@FindBy(css = ".customer-signin-section .edit-link")		
	WebElement lnkEditSigninSection;
	
	@FindBy(css = ".checkout-guest")
	WebElement lblGuestUserEmail;
	
	@FindBy(css = ".checkout-billing")		
	WebElement sectionBillingAddress;
	
	@FindBy(css = ".billing-cards-section")		
	WebElement sectionPaymentCard;

	@FindBy(css = "label[for*='dwfrm_billing_paymentMethods_creditCard_cvn'] .label-text")
	WebElement lblTxtCvvNo;

	@FindBy(css = ".shippingoverlay-link")
	WebElement spanCostToolTip;

	@FindBy(css = "label[for*='dwfrm_billing_paymentMethods_creditCard_cvn'] .error")
	WebElement lblTxtCvvNoError;

	@FindBy(css = ".selected input[id*='dwfrm_billing_paymentMethods_creditCard_cvn']")
	WebElement fldTxtCvvNo;
	
	@FindBy(css = "input[id*='dwfrm_billing_paymentMethods_creditCard_cvn']")
	WebElement fldTxtCvv;

	@FindBy(css = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_firstName']")
	WebElement txtFirstnameShipping;

	@FindBy(css = "label[for='dwfrm_singleshipping_shippingAddress_addressFields_firstName'] [class*='error']")
	WebElement errFirstnameShipping;

	@FindBy(css = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_lastName']")
	WebElement txtLastnameShipping;	

	@FindBy(css = "label[for='dwfrm_singleshipping_shippingAddress_addressFields_lastName'] [class*='error']")
	WebElement errLastnameShipping;

	@FindBy(css = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_address1']")
	WebElement txtAddressline1Shipping;

	@FindBy(css = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_address2']")
	WebElement txtAddressline2Shipping;
	
	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_address2-error")
	WebElement txtAddressline2ShippingError;

	@FindBy(css = ".loader-bg")
	WebElement waitLoader;
	
	@FindBy(css = ".saveCard input")
	WebElement saveCardCheckBox;

	@FindBy(css = ".pt_checkout")
	WebElement readyElement;

	@FindBy(css = "#dwfrm_billing_billingAddress_email_emailAddress")
	WebElement txtGuestEmail;

	@FindBy(css = "label[for='dwfrm_billing_billingAddress_email_emailAddress']")
	WebElement lblGuestEmail;
	
	@FindBy(css = ".checkout-tab .head-wrapper h2")
	WebElement lblGuestHeading;
	
	@FindBy(css = ".checkout-plcc-modal .plcc-section-1")
	WebElement modalCheckoutPlcc;
	
	@FindBy(css = ".checkout-plcc-modal .plcc-nothanks-button")
	WebElement btnPLCCNoThanks;
	
	@FindBy(css = ".checkout-plcc-modal .plcc-get-it-button")
	WebElement btnPLCCGetItToday;
	
	@FindBy(css = ".plcc-profile-summary")
	WebElement plccProfileSummary;
	
	@FindBy(css = ".ssn.required input")
	WebElement plccSocialSecurityNumber;
	
	@FindBy(css = "#dwfrm_billing_billingAddress_email_emailAddress-error")
	WebElement lblGuestEmailError;

	@FindBy(css = "#dwfrm_billing_billingAddress_addToEmailList")
	WebElement chkNewsLetterSignUp;

	@FindBy(css = "input[id*='dwfrm_login_username']")
	WebElement txtRegUserEmail;

	@FindBy(css = "input[id*='dwfrm_login_password']")
	WebElement txtRegUserPwd;

	@FindBy(css = ".spc-guest-btn")
	WebElement btnContinue;
	
	@FindBy(css = ".spc-guest-btn")
	WebElement btnGuestContinue;
	
	@FindBy(css = ".svg.svg-converted")
	WebElement imgBrandImage;
	
	@FindBy(css = ".reset-error-caption.invalid-email")
	WebElement errUnregisteredUser;

	@FindBy(css = "button[name='dwfrm_login_login']")
	WebElement btnSignInAsUser;
	
	@FindBy(css = ".error-form.plcaeordererror")
	WebElement errPlaceOrder;

	@FindBy(css = ".disabled-hover-event")
	WebElement btnDisableCtnToPayment;

	@FindBy(css = ".rw-error.error.hide")
	WebElement promoRwErrorMessage;

	@FindBy(css = ".coupon-error .error.coupon-code-error") 
	WebElement couponErrorMessage;
	
	@FindBy(css = ".left-content .error-messages .error.coupon-code-error")
	WebElement couponErrorMessage_Mobile;
	
	@FindBy(css = ".cartcoupon .remove")
	WebElement lnkRemoveLinkCoupon;

	@FindBy(css = ".couponsection")
	WebElement couponSection;

	@FindBy(css = ".payment-cards")
	WebElement divAcceptedPayments;
	
	@FindBy(css = ".option-tooltip.coupon-tooltip")
	WebElement couponTooltip;

	@FindBy(css = ".couponsection legend")
	WebElement promoHeader;

	@FindBy(css = ".rw-error.error.hide")
	WebElement promoRwErrorMessageHided;

	@FindBy(css = ".form-row.nickName.required.show-text .input-text.required.valid")
	WebElement txtNickNameInput;

	@FindBy(css = ".address-actions .shipping-cancel")
	WebElement lnkCancelShippingAddress;

	@FindBy(css = ".error.coupon-code-error")
	WebElement promoCouponError;

	@FindBy(css = ".couponsection.tab-section .couponheader.row-header .arrow-img.expanded")
	WebElement expandedPromoCodeSection;

	@FindBy(css = ".rewardheader.row-header .arrow-img.expanded")
	WebElement expandedRewardSection;
	
	@FindBy(css = ".selected-shipping-method")
	WebElement txtUpdatedShipMtdInShpExpOverlay;

	@FindBy(css = ".couponsection a")
	WebElement couponToolTipsIcon;

	@FindBy(css = ".content-asset .tips")
	WebElement couponTooltipContent;

	@FindBy(css = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_address1']")
	WebElement txtShippingAdd1;
	
	@FindBy(css = ".username.error-handle")
	WebElement errUsername;
	
	@FindBy(css = ".password.error-handle")
	WebElement errPassword;

	@FindBy(css = "input[id='dwfrm_singleshipping_shippingAddress_addressFields_address2']")
	WebElement txtShippingAdd2;

	@FindBy(css = ".giftReceipt .field-wrapper .input-checkbox")
	WebElement chkGiftReceiptCheckBox;

	@FindBy(css = ".useAsBillingAddress .input-checkbox")
	WebElement chkUseBillingAddress;

	@FindBy(css = ".isGift .input-checkbox")
	WebElement chkGiftMessageCheckBox_Login;
	
	@FindBy(css = ".mobile-isgift .isGift .input-checkbox")
	WebElement chkGiftMessageCheckBox;
	
	@FindBy(css = ".isGift .input-checkbox")
	WebElement chkGiftMessageCheckBox_Mobile_Login;
	
	@FindBy(css = ".desktop-isgift .isGift .input-checkbox")
	WebElement chkGiftMessageCheckBox_Mobile;
	
	@FindBy(css = ".gift-section legend")
	WebElement lblGiftOptionsHeading;

	@FindBy(css = ".input-radio.default-ship-method")
	WebElement rdbDefaulDeliveryOption;

	@FindBy(css = ".form-row.isGift.show-text.label-inline.form-indent .label-text")
	WebElement lblFreeGiftMessage;

	@FindBy(css = ".desktop-isgift label[for='dwfrm_singleshipping_shippingAddress_isGift']")
	WebElement lblFreeGiftMessage_mobile;

	@FindBy(css = ".form-row.giftReceipt.show-text.label-inline.form-indent .label-text")
	WebElement lblFreeGiftReceipt;

	@FindBy(css = ".block-section.shipping-section.gift-section legend")
	WebElement lblGiftOptionHeading;

	@FindBy(css = ".form-row.gift-message-text.giftMessage1 .input-textarea")
	WebElement txtFormGiftMessage1;

	@FindBy(css = ".form-row.gift-message-text.giftMessage2 .input-textarea")
	WebElement txtFormGiftMessage2;

	@FindBy(css = ".form-row.gift-message-text.giftMessage3 .input-textarea")
	WebElement txtFormGiftMessage3;

	@FindBy(css = ".giftmessage.info")
	List<WebElement> txtGiftMessageSectionAfterContinue;

	@FindBy(css = ".giftreceipt.info .value")
	WebElement txtGiftReceiptValueAfterContinue;
	
	@FindBy(css = "div[class='giftinfo'] .giftmessage.info")
	WebElement txtGiftMessageSectionUnderShipping;

	@FindBy(css = ".giftinfo:not(.giftinfo-jl)  .giftmessage.info")
	WebElement txtGiftMessageSectionUnderShipping_JL;

	@FindBy(css = ".form-row.gift-message-text.giftMessage1 .char-remain-count")
	WebElement txtFormGiftMessage1CharCount;

	@FindBy(css = ".form-row.gift-message-text.giftMessage2 .char-remain-count")
	WebElement txtFormGiftMessage2CharCount;

	@FindBy(css = ".form-row.gift-message-text.giftMessage3 .char-remain-count")
	WebElement txtFormGiftMessage3CharCount;

	@FindBy(css = ".form-row.gift-message-text.giftMessage1 .char-count")
	WebElement txtFormGiftMessage1CharCountFull;

	@FindBy(css = ".form-row.gift-message-text.giftMessage2 .char-count")
	WebElement txtFormGiftMessage2CharCountFull;

	@FindBy(css = ".form-row.gift-message-text.giftMessage3 .char-count")
	WebElement txtFormGiftMessage3CharCountFull;

	@FindBy(css = ".step-3.active")
	WebElement step3Active;
	
	@FindBy(css = ".header-mian-banner .step-1.active")
	WebElement step1Active;

	@FindBy(css = ".customer-signin-section")
	WebElement signedInSection;
	
	@FindBy(css = "div.giftinfo:not(.giftinfo-jl) .giftreceipt.info .value")
	WebElement txtGiftReceiptValue;
	
	@FindBy(css = ".giftinfo-jl .giftreceipt.info .value")
	WebElement txtGiftReceiptValue_Jl_Tablet;
	
	@FindBy(css = ".giftinfo .giftreceipt.info.DelayHover .value")
	WebElement txtGiftReceiptValue_Jl;
	
	@FindBy(css = ".giftinfo-jl")
	WebElement txtGiftInfo_Jl;
	
	@FindBy(css = "div.giftinfo:not(.giftinfo-jl)")
	WebElement txtGiftInfo_JL_DeskMob;

	@FindBy(css = ".block-section.saved-address-block .saved-addr-dropdown-section .selected-option.selected")
	WebElement selectedOptionShipping;

	@FindBy(css = ".selected-details.hide-desktop.hide-tablet")
	WebElement selectedShippingFullAddress;
	
	@FindBy(css = ".shipping .custom-select.address-dropdown .selected-option")
	WebElement selectedShippingAddressDropDown;
	
	@FindBy(css = ".billing .custom-select.address-dropdown .selected-option")
	WebElement selectedBillingAddress;
	
	@FindBy(css = ".address-dropdown .selection-list")
	WebElement drpAddressSelectionList;

	@FindBy(css = ".selected-details.hide-desktop.hide-tablet")
	WebElement selectedBillingAddressMobile;

	@FindBy(css = ".form-row.cvn.cvn.required.show-text  .input-text.required.valid")
	WebElement txtCvvField;

	@FindBy(css = ".payment-method.make-label-absolute .card-list .input-text.cvn.valid")
	WebElement txtCvvFieldDefault;

	@FindBy(css = ".giftcardsection.tab-section .couponheader.row-header .arrow-img.expanded")
	WebElement expandedGiftCardSection;

	@FindBy(css = ".giftcardsection .couponheader.row-header legend")
	WebElement giftCardSectionHeading;

	@FindBy(css = ".checkout-tab-content .order-summary-footer .button-fancy-large")
	WebElement btnPlaceOrder;

	@FindBy(css = ".button-fancy-large.rightbutton.final-submit")
	WebElement btnPlaceOrderRight;
	
	@FindBy(css = ".button-fancy-large.rightbutton")
	WebElement btnOrderSummaryContinue;
	
	@FindBy(css = ".right-section .secure-message")
	WebElement secureMsgBelowReviewOrder;
	
	@FindBy(css = ".left-section .review-disclaimer")
	WebElement reviewdisclaimerReviewSection;
	
	@FindBy(css = ".checkout-order-summary .secure-message")
	WebElement secureMsgBelowPlaceOrder;

	@FindBy(css = ".checkout-tab-head.open h2")
	WebElement reviewPlaceOrderHeading;

	@FindBy(css = ".card-list")
	WebElement savedCardsSection;

	@FindBy(css = ".columns-combine.hide-mobile .price-total")
	List<WebElement> listItemsPrice;

	@FindBy(css = ".items-in-bag.saved-address-block .head-wrapper")
	WebElement txtItemsInBagHeader;

	@FindBy(css = ".items-in-bag.saved-address-block .head-wrapper")
	WebElement txtItemsInBagHeading;

	@FindBy(css = ".items-in-bag.saved-address-block .head-wrapper .address-actions a")
	WebElement lnkItemsInBagEdit;

	@FindBy(css = ".button-fancy-large.spc-saveAddr-btn")
	WebElement btnSaveAddr;

	@FindBy(name = "dwfrm_singleshipping_shippingAddress_save")
	WebElement btnShippingContinue;
	
	@FindBy(css = ".button-fancy-large.spc-saveAddr-btn")
	WebElement btnBillingContinue;

	@FindBy(css = "button[class='button-fancy-large spc-billing-btn']")
	WebElement btnPaymentDetailsContinue;

	@FindBy(css = ".couponsection .couponheader.row-header legend")
	WebElement promoHeading;

	@FindBy(css = ".couponsection.tab-section .couponheader.row-header .arrow-img.expanded")
	WebElement btnHvePromoCodeExpandedArrow;

	@FindBy(css = ".giftcardsection.tab-section .couponheader.row-header .arrow-img.expanded")
	WebElement btnHveGiftCardExpandedArrow;	

	@FindBy(css = ".giftcardsection.tab-section .couponheader.row-header .arrow-img")
	WebElement btnHveGiftCardArrow;

	@FindBy(css = ".giftcardsection.tab-section .arrow-img")
	WebElement btnHveGiftCardArrowdown;

	@FindBy(css = ".couponsection.tab-section .couponheader.row-header .arrow-img")
	WebElement btnHvePromoCodeArrow;

	@FindBy(css = ".rewardcertificate .rewardheader .arrow-img")
	WebElement icoRewardCertArrow;

	@FindBy(css = ".giftcardsection .couponheader .arrow-img")
	WebElement icoGiftCardArrow;

	@FindBy(css = ".couponsection .couponheader .arrow-img")
	WebElement icoPromoCodeArrow;

	@FindBy(css = ".rewardcertificate .rewardheader.toggle.row-header.expanded .arrow-img")
	WebElement icoRewardCertArrowUp;

	@FindBy(css = ".couponsection .couponheader.toggle.row-header.expanded .arrow-img")
	WebElement icoPromoCodeArrowUp;

	@FindBy(css = ".giftcardsection .couponheader.toggle.row-header.expanded .arrow-img")
	WebElement icoGiftCardArrowUp;

	@FindBy(css = ".rewardcertificate .rewardheader.toggle.row-header .arrow-img")
	WebElement icoRewardCertArrowDown;

	@FindBy(css = ".couponsection .couponheader.toggle.row-header .arrow-img")
	WebElement icoPromoCodeArrowDown;

	@FindBy(css = ".giftcardsection .couponheader.toggle.row-header .arrow-img")
	WebElement icoGiftCardArrowDown;

	@FindBy(css = ".rewardheader .arrow-img.expanded")
	WebElement btnHveRewardExpandedArrow;

	@FindBy(css = ".rewardheader .arrow-img")
	WebElement btnHveRewardArrow;

	@FindBy(css = ".rewardcertificate input")
	WebElement txtRewardText;

	@FindBy(css = ".giftcardsection.tab-section .form-row.cardnumber .input-text")
	WebElement txtGiftCardNumb;

	@FindBy(css = ".giftcardsection.tab-section .form-row.pin .input-text")
	WebElement txtGiftCardPin;

	@FindBy(css = ".giftcardsection.tab-section .form-row.cardnumber .label-text")
	WebElement txtGiftCardNumbPlaceHolder;

	@FindBy(css = ".giftcardsection.tab-section .form-row.pin .label-text")
	WebElement txtGiftCardPinPlaceHolder;

	@FindBy(css = ".remaining-total.order-detail .value")
	WebElement txtRemainingOrderAmount;

	@FindBy(css = ".gift-card-number.value")
	WebElement txtAppliedGiftCardNumber;

	@FindBy(css = ".giftcardsection.tab-section .option-tooltip")
	WebElement lnkGiftCardToolTip;

	@FindBy(css = ".giftcardsection.tab-section .applied-value.applied-value")
	WebElement lnkGiftCardValueApplied;

	@FindBy(css = ".giftcardsection.tab-section .applied-code-section .balance")
	WebElement lnkGiftCardRemainingBalance;

	@FindBy(css = ".giftcardsection.tab-section span.cursor")
	WebElement lnkGiftCardRemove;

	@FindBy(css = ".remove-applied-giftcard span ")
	WebElement lnkGiftCardRemove2;
	
	@FindBy(css = "a.remove-applied-giftcard")
	WebElement lnkGiftCardRemove3;

	@FindBy(css = ".giftcardsection.tab-section .giftcert-error.error")
	WebElement lnkInvalidGiftCardError;

	@FindBy(css = ".gift-cert-tooltip button[title='Close']")
	WebElement lnkGiftCardToolTipClose;

	@FindBy(css = ".giftcardsection.tab-section .gift-card-number")
	WebElement lnkGiftCardNumber;

	@FindBy(css = ".print-page")
	WebElement lnkPrintPageOrderCnf;
	
	 @FindBy(css = ".pt_order-confirmation")
	 WebElement OrderConfirmationreadyElement;

	@FindBy(css = ".receipt-message")
	WebElement lblReceiptMsgOrderCnf;

	@FindBy(css = ".order-status.section .order-date .value")
	WebElement lblOrderDateInConfPage;

	@FindBy(css = ".order-status.section")
	WebElement lblOrderStatusSectionInConfPage;

	@FindBy(css = ".you-may-like.loaded")
	WebElement sectionRecommendationInConfPage;

	@FindBy(css = ".you-may-like.loaded .product-name")
	List<WebElement> lstRecommendationProdInConfPage;

	@FindBy(css = ".order-status.section .total.inner-block-section .value")
	WebElement lblOrderPriceInConfPage;

	@FindBy(css = ".order-payment-instruments.section")
	WebElement lblPaymentMethodInConfPage;

	@FindBy(css = ".order-payment-instruments.section .cc-number.value")
	WebElement lblCardNumberInConfPage;

	@FindBy(css = ".order-payment-instruments.section .cc-exp")
	WebElement lblCardExpiryInConfPage;

	@FindBy(css = ".order-billing.section")
	WebElement lblBillingAddInConfPage;

	@FindBy(css = ".order-billing.section .mini-address-name")
	WebElement lblAddressNameConfPage;

	@FindBy(css = ".mini-address-location div")
	List<WebElement> lstAddressDetailsConfPage;

	@FindBy(css = ".view-more-link")
	WebElement lnkViewDetailsInOrdConf_Mobile;

	@FindBy(css = ".view-more-link.opened")
	WebElement lnkViewLessInOrdConf_Mobile;

	@FindBy(css = ".hide-desktop.hide-tablet.view-more-section .view-more-link")
	WebElement lnkOrderCnfPgViewMore_mobile;

	@FindBy(css = ".order-payment-instruments.section")
	WebElement lnkOrderCnfPgPayment;

	@FindBy(css = ".hide-desktop.hide-tablet.view-more-section .view-more-link.opened")
	WebElement lnkOrderCnfPgViewLess_mobile;

	@FindBy(css = ".order-billing.section")
	WebElement lnkOrderCnfPgOrderBillingAdd;

	@FindBy(css = ".pobox-exclusions")
	WebElement divPoBoxBlockOverlay;
	
	@FindBy(css = ".pobox-exclusions .ship-method-submit")
	WebElement divShipMedOverlayCont;

	@FindBy(css = ".cart-columns")
	List<WebElement> cartItems;

	@FindBy(css = ".pobox-intro")
	WebElement txtPoBoxBlockOverlayIntro;

	@FindBy(xpath = "//div[@class='footer_item']//li//a[contains(text(),'my account')]")
	WebElement lnkFooterOrderingStatus;

	@FindBy(css = ".step-1.inactive.submitted .progress-text")
	WebElement lnkHeaderShipping;

	@FindBy(css = ".slimScrollBar")
	WebElement scrPoBoxBlockOverlayScrollBar;

	@FindBy(css = ".remove-button button.remove-tems")
	WebElement btnPoBoxBlockOverlayRemoveItems;
	
	@FindBy(css = "#cart-table .cart-row")
	List<WebElement> lstCartedItems;

	@FindBy(css = ".button-fancy-large.edit-shipping")
	WebElement btnPoBoxBlockOverlayEditShipAdd;

	@FindBy(css = ".pobox-items .item-image img:nth-of-type(1)")
	WebElement mainImageInPoBoxOverlay;

	@FindBy(css = ".product-line-item .item-image img[alt='cart brand']")
	WebElement brandImageInPoBoxOverlay;

	@FindBy(css = ".pobox-items .name a")
	WebElement productNameInPoBoxOverlay;

	@FindBy(css = ".pobox-items div[class='attribute'][data-attribute='size'] .value")
	WebElement productSizeInPoBoxOverlay;

	@FindBy(css = ".pobox-items div[class='attribute'][data-attribute*='Size'] .value")
	WebElement productShoeSizeInPoBoxOverlay;

	@FindBy(css = ".pobox-items div[class='attribute'][data-attribute='color'] .value")
	WebElement productColorInPoBoxOverlay;

	@FindBy(css = ".pobox-items .name a")
	List<WebElement> productNamePoBoxOverlay;

	@FindBy(css = ".pobox-items .cart-unit-price.attribute")
	WebElement productPriceInPoBoxOverlay;

	@FindBy(css = ".pobox-items")
	WebElement poBoxBlockOverlayItemsSection;

	@FindBy(css = ".pobox-items .product-line-item")
	List<WebElement> poBoxBlockOverlayProducts;
	
	@FindBy(css = ".pobox-items .product-line-item")
	WebElement poBoxBlockOverlayProduct;
	
	@FindBy(css = ".pobox-heading")
	WebElement txtPoBoxBlockOverlayHeading;

	@FindBy(css = ".giftcardsection.tab-section .apply.cancel")
	WebElement btnGiftCardApply;

	@FindBy(css = ".rewardcertificate .apply.cancel")
	WebElement btnRewardApply;

	@FindBy(css = ".rewardcertificate .option-tooltip")
	WebElement btnRewardToolTip;
	
	@FindBy(css = ".applied-code-section.giftcert .value")
	WebElement appliedGiftCardNumber;

	@FindBy(css = ".applied-code-section.giftcert .value")
	List<WebElement> lstAppliedGiftCardNumber;
	
	@FindBy(css = ".applied-code-section.giftcert .applied-value")
	WebElement appliedGiftCardValue;

	@FindBy(css = ".applied-code-section.giftcert .applied-value")
	List<WebElement> lstAppliedGiftCardValue;

	@FindBy(css = ".rewardcertificate.tab-section .applied-code-section .value")
	WebElement rewardAppliedValue;

	@FindBy(css = ".rewardcertificate.tab-section .applied-code-section .value")
	List<WebElement> lstRewardAppliedValue;

	@FindBy(css = ".rewardcertificate.tab-section .remove-reward-certificate")
	List<WebElement> lstBtnRewardRemove;

	@FindBy(css = ".rewardcertificate.tab-section .remove-reward-certificate")
	WebElement btnRewardRemove;

	@FindBy(css = ".rewardcertificate.tab-section .applied-value.clearboth")
	WebElement txtRewardValueApply;

	@FindBy(css = ".rewardcertificate.tab-section .balance.clearboth")
	WebElement txtRewardRemainingBalance;

	@FindBy(css = ".tooltip-dialog")
	WebElement rewardToolTipPopUp;

	@FindBy(css = ".tooltip-dialog .ui-icon-closethick")
	WebElement btnRewardToolTipPopUpClose;

	@FindBy(css = ".rewardcertificate.tab-section .rewardcert-error.error")
	WebElement txtRewardError;

	@FindBy(css = "a.dialog-tooltip")
	WebElement lnkDeliveryOptionsToolTip;

	@FindBy(css = ".tooltip-dialog")
	WebElement divDeliveryOptionsToolTipDialog;

	@FindBy(css = ".tooltip-dialog button.ui-dialog-titlebar-close")
	WebElement btnCloseToolTipDialog;

	@FindBy(css = ".giftcardsection.tab-section .option-tooltip")
	WebElement btnGiftCardToolTip;

	@FindBy(css = ".form-row.couponCode input")
	WebElement txtCouponCode;

	@FindBy(css = ".form-row.couponCode label[for='dwfrm_billing_couponCode']")
	WebElement txtCouponCode_focus;

	@FindBy(css = ".rewardcertificate label[for='dwfrm_billing_rewardCode']")
	WebElement txtRewardCert_focus;

	@FindBy(css = ".billing-coupon-code .item-details")
	List<WebElement> appliedCoupons;
	
	@FindBy(css = ".redemption.rowcoupons .item-details")
	WebElement appiledCoupon;

	@FindBy(css = ".coupon-apply .apply.cancel")
	WebElement btnCouponApply;

	@FindBy(css = ".couponsection .cartcoupon.clearfix .value")
	WebElement appliedCoupon;
	
	@FindBy(css = ".discount .label span")
	WebElement lblWrappedCouponDetails;
	
	@FindBy(css = ".right-content .code-missing.error")
	WebElement lblCouponEmptyError;
	
	@FindBy(css = ".left-content .code-missing.error ")
	WebElement lblCouponError_Mobile;

	@FindBy(css = ".couponsection .cartcoupon.clearfix .value")
	List<WebElement> lstappliedCoupon;

	@FindBy(css = ".couponsection .remove.textbutton")
	WebElement btnRemoveCoupon;

	@FindBy(css = ".couponsection .remove.textbutton")
	List<WebElement> lstBtnRemovedCoupon;

	@FindBy(css = ".couponsection .see-details")
	WebElement btnSeeDetails;

	@FindBy(css = ".couponsection .discount-details")
	WebElement discountDetails;

	@FindBy(css = ".couponsection .see-details.opened")
	WebElement btnHideDetails;

	@FindBy(css = ".discount-details.hide")
	WebElement txtCouponCodeDetails;

	@FindBy(css = ".left-section button[value='Place Order']")
	WebElement btnPlaceOrderInReview;

	@FindBy(css = "#secondary .order-summary-footer button[value='Place Order']")
	WebElement btnPlaceOrderInOrderSummary;

	@FindBy(css = ".checkout-tab.summary.review .section-heading")
	WebElement headerReview_PlaceOrder;

	@FindBy(css = "div.checkout-tab-head .items-in-bag")
	WebElement headerShoppingCartList;

	@FindBy(css = ".confirmation-message>h1")
	WebElement txtOrderConfirmationMsg;

	@FindBy(css = "h1.order-number .value")
	WebElement lblOrderNumberMsg;

	@FindBy(css = ".item-quantity")
	WebElement 	divItemQuantity;
	
	@FindBy(css = ".addresssection")
	WebElement 	lblBillingAddressSelected;
	
	@FindBy(css = ".selected-option.selected span")
	WebElement txtDropDownSelectedBillingAddress;
	
	@FindBy(css = ".cc-number.value")
	WebElement 	lblEnteredCardNo;
	
	@FindBy(css = ".cc-exp")
	WebElement 	lblEnteredExpiryDate;
	
	@FindBy(css = ".cc-owner.value")
	WebElement 	lblEnteredCardOwnerName;
	
	@FindBy(css = ".selectpayment .disabled-hover-event")
	WebElement 	btnDisabledPaymentContinue;

	@FindBy(name = "dwfrm_cart_shipments_i0_items_i0_quantity")
	WebElement 	txtItemQuantity;

	@FindBy(name = ".qty-increase.qty-arrows")
	WebElement 	arrowQtyIncrease;

	@FindBy(name = ".qty-decrease.qty-arrows")
	WebElement 	arrowQtyDecrease;

	@FindBy(css = ".newcreditcard")
	WebElement btnAddNewCard;

	@FindBy(css = ".shipping-add-new")
	WebElement btnAddNewShippingAddress;	

	@FindBy(css = ".saved-address-count")
	WebElement savedAddressCount;
	
	@FindBy(css = ".express-checkout-billing .saved-address-count")
	WebElement billingAddressSavedCount;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress > fieldset > div.block-section.saved-address-block > div.head-wrapper > div > div.edit-section > a.shipping-add-edit")
	WebElement btnEditShippingAddress;

	@FindBy(css = ".address-actions .shipping-add-edit")
	WebElement btnEditShippingAddressLogIn;

	@FindBy(css = ".payment-method-options .form-row .custom-radio-icon")
	WebElement radioCreditCard;
	
	@FindBy(css = ".creditcardnumber label span")
	WebElement lblcreditcardPlaceHolderText;

	@FindBy(css = "#is-PayPal")
	WebElement radioPaypal;

	@FindBy(css = "div[class='login-box login-create-account clearfix'] .hide-mobile")
	WebElement txtCreateAccount;

	@FindBy(css = "#main > div > div > div.order-confirmation-details > div > div > div.login-box.login-create-account.clearfix > div.create-heading.hide-desktop.hide-tablet")
	WebElement txtCreateAccountMobile;

	@FindBy(css = ".login-box-left-content")
	WebElement txtLoginContent;

	@FindBy(css = ".profile-name .profile-value")
	WebElement txtProfileName;

	@FindBy(css = ".profile-email .profile-value")
	WebElement txtProfileEmail; 

	@FindBy(css = ".edit-profile")
	WebElement lnkEditOrderReceiptEmail;

	@FindBy(css = ".edit-profile")
	WebElement lnkEditEmail;

	@FindBy(css = ".largebutton")
	WebElement btnCreateAccount;

	@FindBy(css = "input[id*='dwfrm_profile_login_password_']")
	WebElement txtPassword;

	@FindBy(css = "input[id*='dwfrm_profile_login_passwordconfirm']")
	WebElement txtConfirmPassword;

	@FindBy(css = "span[id*='dwfrm_profile_login_password_']")
	WebElement txtPasswordError;

	@FindBy(css = "span[id*='dwfrm_profile_login_passwordconfirm_']")
	WebElement txtConfirmPasswordError;

	@FindBy(css = ".login-box-right-content")
	WebElement contentCreateAccountSection;

	@FindBy(id = "dwfrm_singleshipping_shippingAddress_addressFields_nickName")
	WebElement txtNickName;

	@FindBy(css = "div.customer-signin-section")
	WebElement divSignInSection;

	@FindBy(css = ".checkout-tab.shipping")
	WebElement divCheckoutShipping;
	
	@FindBy(css = ".checkout-tab.shipping .tab-content-show")
	WebElement divCheckoutShippingExpand;
	
	@FindBy(css = ".checkout-tab.billing .tab-content-show")
	WebElement divCheckoutBillingExpand;
	
	@FindBy(css = ".customer-signin-section .edit-link")
	WebElement lnkEditSignin;

	@FindBy(css = ".edit-link.edit-Shipping")
	WebElement lnkEditShipping;

	@FindBy(css = ".checkout-tab.billing .addaddress")
	WebElement lnkAddNewBilling;

	@FindBy(css = ".card-count .newcreditcard")
	WebElement lnkAddNewCredit;

	@FindBy(css = ".card-count .newcreditcard")
	WebElement btnSaveCreditCardCancel;
	
	@FindBy(css = ".checkout-tab.billing a")
	WebElement lnkEditBillingAndPayment;
	
	@FindBy(css = "div.checkout-tab-head .address-actions")
	WebElement lnkEditShoppingCart;

	@FindBy(css = "#cart-table")
	WebElement tblShoppingCartItems;

	@FindBy(css = "#cart-table .tbody > .cart-row")
	WebElement firstItemInCartTable;

	@FindBy(css = "#cart-table .tbody .cart-row")
	List<WebElement> lstItemsInCartList;

	@FindBy(css = "#cart-table .tbody .cart-row")
	WebElement ItemsInCartList;

	@FindBy(css = ".checkout-tab.billing .section-heading")
	WebElement headingBilling;

	@FindBy(css = ".checkout-tab.billing")
	WebElement divCheckoutBilling;
	
	@FindBy(css = ".checkout-tab.summary")
	WebElement divCheckoutReviewPlaceOrder; 

	@FindBy(css = ".checkout-tab.summary.review")
	WebElement divCheckoutReview_PlaceOrder;

	@FindBy(css = ".checkout-tab.summary.review .checkout-tab-content .review-disclaimer")
	WebElement lblCheckout_Review_Desclaimer_Message;

	@FindBy(css = ".checkout-order-summary .order-summary-footer .review-disclaimer")
	WebElement lblReviewDesclaimerInSummary;

	@FindBy(css = ".checkout-tab.billing .saved-address-block")
	WebElement divCheckoutBillingBlock;

	@FindBy(css = ".selectpayment")
	WebElement divCheckoutPayment;

	@FindBy(css = ".checkout-tab.billing > .checkout-tab-content")
	WebElement divCheckoutBillingContent;

	@FindBy(id = "dwfrm_singleshipping_shippingAddress_addressFields_firstName")
	WebElement txtFirstName;

	@FindBy(css = ".primary-logo a")
	WebElement lnkPrimaryLogo;

	@FindBy(css = ".chk-out-header-right-section")
	WebElement headerRightSection;

	@FindBy(css = ".firstName .label-text")
	WebElement lblFirstNamePlaceHolder;

	@FindBy(id = "dwfrm_singleshipping_shippingAddress_addressFields_lastName")
	WebElement txtLastName;
	
	@FindBy(css = ".field-wrapper .input-text.lastName.required.error")
	WebElement errorLastName;

	@FindBy(css = ".lastName .label-text")
	WebElement lblLastNamePlaceHolder;

	@FindBy(id = "dwfrm_singleshipping_shippingAddress_addressFields_address1")
	WebElement txtAddress1;

	@FindBy(css = ".address1 .label-text")
	WebElement lblAddress1PlaceHolder;

	@FindBy(id = "dwfrm_singleshipping_shippingAddress_addressFields_address2")
	WebElement txtAddress2;

	@FindBy(css = ".address2 .label-text")
	WebElement lblAddress2PlaceHolder;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress_addressFields_postal")
	WebElement txtZipcode;

	@FindBy(css = "label[for='dwfrm_singleshipping_shippingAddress_addressFields_postal'] [class*='error']")
	WebElement errZipcodeShipping;

	@FindBy(css = ".postal .label-text")
	WebElement lblZipcodePlaceHolder;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress_addressFields_city")
	WebElement txtCity;

	@FindBy(css = ".city .label-text")
	WebElement lblCityPlaceHolder;
	
	@FindBy(css = ".state .label-text")
	WebElement lblStatePlaceHolder;

	@FindBy(css = "select#dwfrm_singleshipping_shippingAddress_addressFields_states_state")
	WebElement drpState;
	
	@FindBy(css = "#dwfrm_billing .state .input-select")
	WebElement billingAddSelectedState_Tablet;
	
	@FindBy(css = "#dwfrm_billing .selected-option.selected")
	WebElement billingAddSelectedState;

	@FindBy(css = "select#dwfrm_singleshipping_shippingAddress_addressFields_country")
	WebElement drpCountry;

	@FindBy(id = "dwfrm_singleshipping_shippingAddress_addressFields_phone")
	WebElement txtPhone;
	
	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_postal")
	WebElement inpPostalAddBiling;

	@FindBy(css = ".phone .label-text")
	WebElement lblPhonePlaceHolder;

	@FindBy(css = ".country .label-text")
	WebElement lblCountryPlaceHolder;

	@FindBy(css = "input#dwfrm_singleshipping_shippingAddress_makeDefaultAddress")
	WebElement chkMakeItDefault;

	@FindBy(css = "input#dwfrm_singleshipping_shippingAddress_addToAddressBook")
	WebElement chkSaveThisAddress;

	@FindBy(css = "input#dwfrm_singleshipping_shippingAddress_useAsBillingAddress")
	WebElement chkUseThisAsBillingAddress;

	@FindBy(css = ".address-dropdown.hide-mobile")
	WebElement drpShippingSelectAddress;
	
	@FindBy(css = ".custom-select.address-dropdown")
	WebElement drpSelectAddressOptions;

	@FindBy(css = "button.button-fancy-large:not(.rightbutton)")
	WebElement btnContinueToPayment;

	@FindBy(css = "#shipping-method-list label[for*='shipping-method']")
	List<WebElement> lblShippingMethodName;

	@FindBy(css = "#shipping-method-list label[for*='shipping-method']>span")
	List<WebElement> lblShippingMethodCost;

	@FindBy(css = "#shipping-method-list .form-caption")
	List<WebElement> lblShippingMethodDetails;

	@FindBy(css = ".form-row .promo .label")
	List<WebElement> txtPromotionCalloutMessage;

	@FindBy(css = "input#dwfrm_singleshipping_shippingAddress_isGift")
	WebElement chkGiftMessage;

	@FindBy(css = "input#dwfrm_singleshipping_shippingAddress_giftReceipt")
	WebElement chkGiftReceipt;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress_giftMessage1")
	WebElement txtGiftMessage;

	@FindBy(css = ".edit-link.edit-Shipping")
	WebElement lnkEditBillingAddress;

	@FindBy(xpath = "//h2[contains(text(),'Shipping Details')]")
	WebElement lblShippingDetailsHeading;

	@FindBy(css = ".mini-shipping .address")
	WebElement divShippingAddress;

	@FindBy(css = ".addresssection.block-section")
	WebElement divBillingAddress;

	@FindBy(css = ".minishipments-method")
	WebElement divDeliveryOptions;

	@FindBy(css = ".delivery-options .minishipments-method .option-heading")
	WebElement txtSavedDeliveryOption;

	@FindBy(css = ".giftmessage.info .value")
	WebElement txtSavedGiftMessage;

	@FindBy(css = "div[class='giftinfo']")
	WebElement divGiftInfo;

	@FindBy(css = ".shipping-add-new")
	WebElement lnkAddNew;

	@FindBy(css = ".jl-address-action .shipping-add-new")
	WebElement lnkAddNewJL;

	@FindBy(css = ".input-select.address-dropdown")
	WebElement drpSelectAddress;
	
	@FindBy(css = ".billing .address-dropdown .selected-option.selected")
	WebElement drpSelectAddressSelectedOption;
	
	@FindBy(xpath = "//select[contains(@class,'address-dropdown')]//following-sibling::div[contains(@class,'selected')]")
	WebElement customDrpShippingAddress;

	@FindBy(css = ".billing .input-select.address-dropdown")
	WebElement drpSelectAddressMobile;
	
	@FindBy(css = ".custom-select.address-dropdown")
	WebElement drpSelectAddressCurrentItem;
	
	@FindBy(css = ".shipping .select-address .field-wrapper")
	WebElement shippingAddSectionMob;
	
	@FindBy(css = ".saved-address-count")
	WebElement savedAddressCountWithText;
	
	@FindBy(css = ".shippingmethod-exception")
	WebElement lblShippingMethodExcep;

	@FindBy(css = ".selected-details.hide-desktop.hide-tablet")
	WebElement txtFullAddressMobile;

	@FindBy(css = ".customer-signin-section .edit-link")
	WebElement lnkEditCustomerSignin;

	private final static String MYACCOUTLOGIN = "#primary .login-box.login-account ";

	@FindBy(css = MYACCOUTLOGIN + "input[name*='dwfrm_login_password']")
	WebElement fldPassword;

	@FindBy(css = ".checkout-guest")
	WebElement txtGuestEmailID;

	@FindBy(css = ".billing>div.checkout-tab-head")
	WebElement lblPaymentDetailsHeading;

	@FindBy(css = ".checkout-tab.billing .head-wrapper>legend")
	WebElement lblBillingAddressHeading;

	@FindBy(css = ".selecteddetails")
	WebElement lblSelectedAddress;

	@FindBy(css = "form#dwfrm_billing")
	WebElement divPaymentDetailsSection;

	@FindBy(css = ".savesection .editsection")
	WebElement lnkSaveBillingDetails;

	@FindBy(css = ".saved-address-block .block-section .head-wrapper")
	WebElement billingAddressHeading;

	@FindBy(css = ".savesection .addresscancel")
	WebElement lnkCancelBillingDetails;

	@FindBy(css = ".select-address.clearboth.hide")
	WebElement hideBillingAddressPanel;

	@FindBy(css = "label[for=dwfrm_billing_addressList] span")
	WebElement savedAddressCountText;

	@FindBy(css = ".welcome-check")
	WebElement lblEmailInSignInBox;

	@FindBy(css = ".checkout-tab.shipping .block-section")
	WebElement divShippingSectionContent;
	
	@FindBy(css = ".shippingmethod-exception")
	List<WebElement> lstShippingMethodExp;

	@FindBy(css = ".mini-shipping .minishipments-method")
	WebElement lblEmailInShippingDetails;

	@FindBy(css = ".delivery-method")
	WebElement shippingType;

	@FindBy(css = ".delivery-options .minishipments-method .option-heading")
	WebElement lblShippingType;
	
	@FindBy(css = ".delivery-options .minishipments-method .delivery-method")
	WebElement txtDeliveryViaEmail;

	@FindBy(css = ".checkout-tab.shipping .mini-shipping .address")
	WebElement lblShippingAddress;

	@FindBy(css = "#dwfrm_billing_rewardCode")
	WebElement txtRewardCode;

	@FindBy(css = "#dwfrm_svsgiftcard_cardnumber")
	WebElement txtGiftCardCode;

	@FindBy(css = ".rewardcertificate.tab-section .right-content")
	WebElement rewardCardRightSection;

	@FindBy(css = ".couponsection.tab-section .right-content")
	WebElement couponRightSection;

	@FindBy(css = ".giftcardsection.tab-section .right-content")
	WebElement giftCardRightSection;

	@FindBy(css = ".mini-payment-instrument .detail")
	WebElement lblMiniPaymentSection;

	@FindBy(css = ".mini-payment-instrument .detail .cc-number.value span.img")
	WebElement logoPaymentType;

	@FindBy(css = ".mini-payment-instrument .detail .cc-number.value")
	WebElement lblCardNumberInMiniPayment;

	@FindBy(css = ".cart-row.gift-Card .name a")
	WebElement lnkGiftCardPrdNameInCartList;

	@FindBy(css = ".cart-row.gift-Card .sku .value")
	WebElement lblGiftCardSkuInCartList;

	@FindBy(css = ".cart-row.gift-Card .giftcard.from .value")
	WebElement lblGCFromEmailInCartList;

	@FindBy(css = ".cart-row.gift-Card .giftcard.to .value")
	WebElement lblGCToEmailInCartList;

	@FindBy(css = ".cart-row.gift-Card .giftmessage")
	WebElement lblGCPersonalMsgInCartList;

	@FindBy(css = "#primary")
	WebElement primaryContentInCheckout;

	@FindBy(css = "#secondary .checkout-order-summary")
	WebElement checkoutOrderSummary;

	@FindBy(css = "#secondary .checkout-order-summary .checkout-order-totals")
	WebElement checkoutOrderTotalTable;

	@FindBy(css = "#secondary .checkout-order-summary .summary-heading")
	WebElement checkoutSummaryHeading;

	@FindBy(css = "#secondary .checkout-order-summary .order-totals-table .order-subtotal .value")
	WebElement lblOrderSubtotalInSummary;

	@FindBy(css = "#secondary .checkout-order-summary .order-totals-table .order-shipping")
	WebElement lblShippingTypeInSummary;

	@FindBy(css = "#secondary .checkout-order-summary .order-totals-table .order-sales-tax")
	WebElement lblSalesTaxInSummary;

	@FindBy(css = ".checkout-order-totals .order-detail.order-total")
	WebElement lblOrderTotalInSummary;
	
	@FindBy(css = ".plcccard.selected")
	WebElement plccSelectedCard;
	
	@FindBy(css = ".checkout-order-totals .remaining-total")
	WebElement lblRemainingTotalInSummary;

	@FindBy(css = "#address-validation-dialog")
	List<WebElement> lstAddressSuggestionModal;
	
	@FindBy(css = divAddressValidationModal + "#address-validation-dialog")
	WebElement mdlAddressSuggestion;
	
	@FindBy(css = divAddressValidationModal + ".original-address .custom-radio-icon")
	WebElement rdoShipToOriginalAddress;

	@FindBy(css = divAddressValidationModal + "button#selected-address-continue")
	WebElement btnContinueInAddressSuggestionModal;
	
	@FindBy(css = divAddressValidationModal + ".original-address input")
	WebElement inpOrigAdrInSuggModal;

	@FindBy(css = divAddressValidationModal + ".corrected-avs-button")
	WebElement lnkCorrectAddress;
	
	@FindBy(css = divAddressValidationModal + "#address-validation-dialog .corrected-avs-button")
	WebElement btnThatsMyRightAddress;
	
	@FindBy(css = divAddressValidationModal + "#address-validation-dialog button.update-avs-button")
	WebElement btnUpdateMyAddress;

	@FindBy(css = divAddressValidationModal + "#address-validation-dialog #cancel-avs-button")
	WebElement btnCancelInAddressSuggestionModal;

	@FindBy(xpath = "//h2[@class='section-heading'][contains(text(),'Payment Details')]")
	WebElement lblPaymentDetail;

	@FindBy(css = ".billing .tab-content-show")
	WebElement divPaymentDetails;	

	@FindBy(css = "#dwfrm_profile_customer_firstname")
	WebElement txtAccountFirstName;

	@FindBy(css = ".selecteddetails .addresssection")
	WebElement lblSavedAddressInBillingDetails;

	@FindBy(css = ".address")
	WebElement lblSavedAddressInShippingDetails;

	@FindBy(css = ".rewardheader span")
	WebElement rewardCertificateArrow;

	@FindBy(css = ".couponsection div>span")
	WebElement promoCodeArrow;

	@FindBy(css = "#dwfrm_profile_customer_lastname")
	WebElement txtAccountLastName;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_addressid")
	WebElement txtNickNameBillingDetails;

	@FindBy(css = "#dwfrm_profile_customer_email")
	WebElement txtAccountEmail;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_firstName")
	WebElement txtFirstNameBillingDetails;

	@FindBy(css = "#dwfrm_profile_customer_emailconfirm")
	WebElement txtAccountConfirmEmail;

	@FindBy(css = ".form-row.firstName.required input")
	WebElement billingEditFirstname;

	@FindBy(css = ".nickname-field input")
	WebElement billingEditNickname;
	
	@FindBy(css = ".checkout-tab.billing .saved-address-block legend")
	WebElement billingHeading;

	@FindBy(css = ".form-row.lastName.required input")
	WebElement billingEditLastname;

	@FindBy(css = "label[for=dwfrm_billing_billingAddress_addressFields_firstName]")
	WebElement billingEDitFirstNamePlaceHolder;

	@FindBy(css = "label[for=dwfrm_billing_billingAddress_addressFields_lastName]")
	WebElement billingEDitLastNamePlaceHolder;

	@FindBy(css = "label[for=dwfrm_billing_billingAddress_addressFields_address1]")
	WebElement billingAddress1PlaceHolder;

	@FindBy(css = "label[for=dwfrm_billing_billingAddress_addressFields_address2]")
	WebElement billingAddress2PlaceHolder;

	@FindBy(css = "label[for=dwfrm_billing_billingAddress_addressFields_postal]")
	WebElement billingZipPlaceHolder;

	@FindBy(css = "label[for=dwfrm_billing_billingAddress_addressFields_city]")
	WebElement billingCityPlaceHolder;

	@FindBy(css = "label[for=dwfrm_billing_billingAddress_addressFields_phone]")
	WebElement billingPhonePlaceHolder;

	@FindBy(css = "input[id*='dwfrm_profile_login_password_']")
	WebElement txtAccountPassword;	

	@FindBy(css = ".giftcardsection div>span")
	WebElement giftCardArrow;	

	@FindBy(css = "#dwfrm_billing_rewardCode")
	WebElement tbRewardCertificate;

	@FindBy(css = "input[id*='dwfrm_profile_login_passwordconfirm_']")
	WebElement txtAccountConfirmPassword;	

	@FindBy(id = "dwfrm_billing_billingAddress_addressFields_lastName")
	WebElement txtLastNameBillingDetails;

	@FindBy(css = ".mini-billing-address .address>div")
	List<WebElement> billingAddressmodules;

	@FindBy(css = ".saved-address-block .addresssection")
	WebElement billingAddressSection;

	@FindBy(xpath = "//div[@class='head-wrapper']/legend[contains(text(),'Billing Address')]")
	List<WebElement> headingbillingAddress;

	@FindBy(css = ".cc-number")
	WebElement ccnumberAfterPaymentDetail;

	@FindBy(css = ".billing .edit-link")
	WebElement editPaymentDetail;

	@FindBy(css = ".editaddress")
	WebElement btneditBillingAddress;

	@FindBy(css = ".addresssection")
	WebElement divbillingAddresssection;
	
	@FindBy(css = ".billing .selected-option.selected .nickname-bold")
	WebElement txtBillingaddressNickName;
	
	@FindBy(css = ".billing .address-nickname-mobile.hide-desktop.hide-tablet")
	WebElement txtBillingaddressNickNameMob;

	@FindBy(css = ".left-cont")
	WebElement divCardDetails;

	@FindBy(css = ".fields-containter .firstName ")
	WebElement billingAddrsFirstName;	

	@FindBy(css = ".fields-containter>div")
	List<WebElement> divbillingAddrsContainer;

	@FindBy(name = "dwfrm_billing_saveAddress")
	WebElement btnselectPaymentMethod;

	@FindBy(css = ".fields-containter .form-row")
	List<WebElement> BillingAddressFields;

	@FindBy(css = ".fields-containter .form-row .label-text")
	List<WebElement> lblBillingAddressFields;

	@FindBy(css = "input[id$='dwfrm_billing_billingAddress_addressFields_postal']:not([readonly='readonly'])[type='text']")
	WebElement txtFieldZipCode;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_firstName")
	WebElement txtFieldFirstname;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_lastName")
	WebElement txtFieldLastname;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_address1")
	WebElement txtFieldAddressline1;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_address2")
	WebElement txtFieldAddressline2;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_city")
	WebElement txtFieldCity;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_phone")
	WebElement txtFieldPhone;

	@FindBy(id = "dwfrm_billing_billingAddress_addressFields_address1")
	WebElement txtAddress1BillingDetails;

	@FindBy(id = "dwfrm_billing_billingAddress_addressFields_address2")
	WebElement txtAddress2BillingDetails;

	@FindBy(id = "dwfrm_billing_billingAddress_addressFields_postal")
	WebElement txtZipcodeBillingDetails;

	@FindBy(id = "dwfrm_billing_billingAddress_addressFields_city")
	WebElement txtCityBillingDetails;

	@FindBy(id = "dwfrm_billing_billingAddress_addressFields_phone")
	WebElement txtPhoneBillingDetails;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_states_state")
	WebElement drpStateBillingDetails;

	@FindBy(css = "#dwfrm_billing_addressList")
	WebElement drpBillingSavedAddr;

	@FindBy(css = ".hide-desktop.hide-tablet #dwfrm_billing_addressList")
	WebElement drpBillingSavedAddrMobile;

	@FindBy(css = "#dwfrm_billing_addressList option")
	List<WebElement> optDrpBillingSavedAddr;

	@FindBy(css = "select#dwfrm_billing_billingAddress_addressFields_country")
	WebElement drpCountryBillingDetails;
	
	@FindBy(css = "div.country.required")
	WebElement drpCountryBilling;

	@FindBy(css = "#dwfrm_billing_billingAddress_addressFields_country")
	WebElement drpCountryBillingEditDetails;

	@FindBy(id = "dwfrm_billing_billingAddress_addressFields_postal-error")
	WebElement txtZipcodeErrorInBilling;

	@FindBy(css = "#dwfrm_billing_billingAddress_addToAddressBook")
	WebElement chkSaveThisAddressInBillingDetails;

	@FindBy(css = "#dwfrm_billing_billingAddress_makeDefault")
	WebElement chkMakeDefaultInBillingDetails;

	@FindBy(name = "dwfrm_billing_saveAddress")
	WebElement btnSelectPaymentMethod;

	@FindBy(css = ".checkout-tab.shipping>div.checkout-tab-head")
	WebElement lblShippingDetailsTitle;

	@FindBy(css = ".shipping .saved-address-block>div>legend")
	WebElement lblShippingAddressTitle;

	@FindBy(css = ".saved-address-count")
	WebElement txtSavedAddressCount;

	@FindBy(xpath = "//select[@id='dwfrm_billing_addressList']//following-sibling::div[@class='selected-option selected']")
	WebElement addressSelectorList;
	
	@FindBy(xpath = "//select[@id='dwfrm_billing_addressList']//following-sibling::div[@class='address-nickname-mobile hide-desktop hide-tablet']")
	WebElement addressSelectorListMobile;

	@FindBy(xpath = "//select[@id='dwfrm_singleshipping_addressList']//following-sibling::div[@class='address-nickname-mobile hide-desktop hide-tablet']")
	WebElement addressSelectorShippingListMobile;
	
	@FindBy(css = "legend.delivery-option-heading")
	WebElement lblDeliveryOptionHeading;
	
	@FindBy(css = ".input-text.nickName")
	WebElement txtNickNameField;
	
	@FindBy(css = ".selected-option.selected .nickname-bold")
	WebElement shippingAddressNickNamebold;
	
	@FindBy(css = ".address-nickname-mobile.hide-desktop.hide-tablet")
	WebElement shippingAddressNickNameboldMob;

	@FindBy(css = ".field-desc .shipping-method-name")
	WebElement shiipingMethodDeliveryOption;

	@FindBy(css = "#shipping-method-list .field-desc")
	WebElement shippingMethods;

	@FindBy(css = ".field-desc span")
	WebElement shiipingMethodCost;

	@FindBy(css = ".field-desc .form-caption")
	WebElement shiipingMethodDeliveryDetail;

	@FindBy(css = btnCardType)
	WebElement selectCardType;
	
	@FindBy(css = "label[for*='dwfrm_billing_paymentMethods_creditCard_type']")
	WebElement selectCardTypeDropdown;
	
	@FindBy(css = "#dwfrm_billing_paymentMethods_creditCard_type + .selected-option + .selection-list")
	WebElement divCardList;
	
	@FindBy(css = "select[id='dwfrm_billing_paymentMethods_creditCard_type'] + .selected-option + .selection-list li")
	List<WebElement> lstPaymentMethods;
	
	@FindBy(css = "select[id='dwfrm_billing_paymentMethods_creditCard_type'] + .selected-option + .selection-list li[label*='Platinum']")
	List<WebElement> lstPaymentMethodsPLCC;
	
	@FindBy(css = "select[id='dwfrm_billing_paymentMethods_creditCard_type'] + .selected-option + .selection-list li:not([label*='Platinum']):not([label*='Credit'])")
	List<WebElement> lstPaymentMethodsRegular;

	@FindBy(css = btnCardTypeDiv)
	WebElement drpCardType;
	
	@FindBy(css = btnCardType)
	WebElement drpCardType1;
	
	@FindBy(css = ".form-row.type.required.show-text")
	WebElement drpCardTypeSection;

	@FindBy(css = ".payment-method .month select")
	WebElement selectExpMonth;
	
	@FindBy(css = ".form-row.month.error-handle label")
	WebElement lblExpiredMonthMsg;
	
	@FindBy(css = ".month div[rel='Expiry Month'].selected-option.selected")
	WebElement selectedExpMonth;

	@FindBy(css = ".payment-order")
	WebElement sectionPaymentOrder;

	@FindBy(css = "select[id='dwfrm_billing_paymentMethods_creditCard_expiration_month'] + .selected-option")
	WebElement drpExpMonth;

	@FindBy(css = "select[id='dwfrm_billing_paymentMethods_creditCard_expiration_month'] + .selected-option + ul li:not([label='Expiry Month'])")
	List<WebElement> lstExpMonth; 

	@FindBy(css = "select[id='dwfrm_billing_paymentMethods_creditCard_expiration_year']")
	WebElement selectExpYear;

	@FindBy(css = "select[id='dwfrm_billing_paymentMethods_creditCard_expiration_year'] + .selected-option")
	WebElement drpExpYear;
	
	@FindBy(css = btnCardType)
	WebElement drpCardTypePaymentSection;

	@FindBy(css = ".enter-card-detials-wrapper .type.required")
	WebElement sectionCardType;

	@FindBy(css = ".month.required .custom-select")
	WebElement sectionExpMonth;
	
	@FindBy(css = ".year.required .custom-select")
	WebElement sectionExpYear;

	@FindBy(css = "select[id='dwfrm_billing_paymentMethods_creditCard_expiration_year'] + .selected-option + ul li:not([label='Expiry Year'])")
	List<WebElement> lstExpYear;

	@FindBy(css = "#dwfrm_billing_paymentMethods_creditCard_makeDefault")
	WebElement chkBoxMakeThisDefaultPayment;

	@FindBy(css = "#dwfrm_billing_paymentMethods_creditCard_saveCard")
	WebElement chkBoxSaveCard;

	@FindBy(css = ".payment-methods-accepted.right-cont")
	WebElement contPaymentMethodsAccepted;
	
	@FindBy(css = ".payment-method ")
	WebElement paymentmethodSection;
		
	@FindBy(css = ".cardinfo")
	List<WebElement> lstSavedCards;

	@FindBy(css = ".cardinfo")
	WebElement savedCards;

	@FindBy(css = ".carddetails.selected")
	WebElement lblSelectedCard;

	@FindBy(css = ".carddetails")
	List<WebElement> lblSavedCardDetails;

	@FindBy(css = ".savecreditcard .editcard")
	WebElement lnkSaveInPayments;

	@FindBy(css = ".savecreditcard .cardcancel")
	WebElement lnkCancelInPayments;

	@FindBy(css = ".billingfields.make-label-absolute")
	WebElement divNewCardSection;

	@FindBy(css = "span.shippingmethod-exception")
	WebElement txtShippingMethodException;
	
	@FindBy(css = ".ship-exception .form-caption")
	WebElement txtShippingMethodDescription;

	@FindBy(css = ".form-row.form-indent.ship-exception.label-inline ")
	List<WebElement> lstShippingMethods;

	@FindBy(css = ".form-row.form-indent.ship-exception.label-inline")
	WebElement shipingMethod;

	@FindBy(css = "span.shippingsurcharge")
	WebElement txtShippingSurcharge;

	@FindBy(css = "div.no-shipping-methods")
	WebElement txtNoShippingException;

	@FindBy(css = "#dwfrm_billing > div.selectpayment > div.savedcards >fieldset .billing-cards-section.block-section .payment-method.make-label-absolute.credit-auth.payment-method-expanded .card-list div:nth-child(3)  .cvninfo.selected")
	WebElement defaultCreditCard;

	@FindBy(css = ".billing-cards-section")
	WebElement billingCardSection;

	@FindBy(css = ".order-sales-tax.order-detail .value")
	WebElement txtSalesTaxValue;

	@FindBy(xpath = "//div[contains(@class,'form-row  address2')]//span[contains(text(),'Address Line2 (optional)')]")
	WebElement txtaddress2optional;

	@FindBy(xpath = "div[class^='form-row  address2']>label>span")
	WebElement txtaddress2edge;

	@FindBy(css = ".saved-addr-dropdown-section .shipping-add-edit")
	WebElement lnkShippingAddressEdit;

	@FindBy(css = ".enter-card-detials-wrapper")
	WebElement paymentMethodSection;

	@FindBy(css = ".rewardheader.toggle.row-header>legend")
	WebElement rewardCertificateSection;

	@FindBy(css = ".giftcardsection.tab-section")
	WebElement giftCardSection;

	@FindBy(css = ".paypal-radio-message")
	WebElement paypalCombinationMessaging;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress .head-wrapper")
	WebElement shippingDetailsHeader;

	@FindBy(css = ".primary-logo>a")
	WebElement brandLogo;

	@FindBy(css = ".pobox-exclusions")
	WebElement editShippingAddressOverlay;

	@FindBy(css = "button[class$='edit-shipping']")
	WebElement btneditShippingAddress;

	@FindBy(css = ".edit-Shipping")
	WebElement lnkEditShippingAddress;

	@FindBy(css = "div.live-chat div.bc_header")
	WebElement lnkLiveChat;

	@FindBy(css = "div#bc-chat-container")
	WebElement mdlLiveChat;

	@FindBy(css = "div.bc-minimize-state-container")
	WebElement icoLiveChat;

	@FindBy(css = "div.bc-headbtn.bc-headbtn-minimize")
	WebElement btnLiveChatMinimize;

	@FindBy(css = "span.help-click.hide-desktop.hide-tablet")
	WebElement lnkNeedHelpMobile;

	@FindBy(css = "div.chk-out-header-top-content div.content")
	WebElement cntNeedHelp;
	
	@FindBy(css = ".state.required")
	WebElement selectStateShipping;

	@FindBy(css = "div.order-totals-table div div")
	List<WebElement> lstOrderElements;

	@FindBy(css = "div.order-totals-table div div")
	WebElement lstOrderElement;

	@FindBy(css = ".order-subtotal.order-detail")
	WebElement divOrderSubTotal;

	@FindBy(css = ".order-discount.discount.order-detail")
	WebElement orderSummaryDiscount;
	
	@FindBy(css = ".order-discount.discount.order-detail .value")
	WebElement orderSummaryDiscountValue;

	@FindBy(css = ".order-option-price.order-detail")
	WebElement orderSummaryOptionCost;
	
	@FindBy(css = ".order-option-price.order-detail .value")
	WebElement orderSummaryOptionCostValue;

	@FindBy (css = ".order-shipping.order-detail")
	WebElement orderSummaryShippingDetail;
	
	@FindBy (css = ".order-shipping .value")
	WebElement orderSummaryShippingCost;
	
	@FindBy(css = ".order-shipping.order-detail .value")
	WebElement divShippingMessagePrice;

	@FindBy(css = ".order-shipping.order-detail .label")
	WebElement divShippingMessage;

	@FindBy (css = ".order-shipping-discount.discount.order-detail")
	WebElement orderSummaryShippingDiscountDetail;
	
	@FindBy (css = ".order-shipping-discount.discount.order-detail .value")
	WebElement orderSummaryShippingDiscountValue;

	@FindBy (css = ".order-sales-tax.order-detail")
	WebElement orderSummarySalesTax;

	@FindBy (css = ".order-total.order-detail.order-total-remaining")
	WebElement orderSummaryOrderTotal;

	@FindBy (css = ".reward-certificate.discount.order-detail")
	WebElement orderSummaryRewardDiscount;
	
	@FindBy (css = ".state select")
	WebElement selectShippingState;

	@FindBy (css = ".gift-card.discount.order-detail")
	WebElement orderSummaryGiftCardDiscount;

	@FindBy (css = ".remaining-total.order-detail")
	WebElement orderSummaryRemainingTotal;
	
	@FindBy (css = ".remaining-total.order-detail .value")
	WebElement orderSummaryRemainingTotalValue;

	@FindBy (css = ".order-saving.discount.order-detail")
	WebElement orderSummaryTotalSavings;

	@FindBy(css = ".order-shipping.order-detail span")
	WebElement divShippingDetail;

	@FindBy(css = ".order-subtotal.order-detail .value")
	WebElement txtSubtotalValue;

	@FindBy(css = "div#bc-chat-container div.bc-frame-title")
	WebElement headerBoldChat;

	@FindBy(css = "#dwfrm_singleshipping_shippingAddress")
	WebElement formShippingDetails;

	@FindBy(css = ".showall")
	WebElement lnkShowAll;

	@FindBy(css = ".cvv-msg")
	WebElement lnkCVVMsg;

	@FindBy(css = ".selected input[id*='dwfrm_billing_paymentMethods_creditCard_cvn']")
	WebElement fldCVV;

	@FindBy(css = ".li.cardholder")
	List<WebElement> txtCardHolder;

	@FindBy(css = ".cvninfo.selected .label-text")
	WebElement txtCVVPlaceHolderText;

	@FindBy(css = ".newcreditcard")	
	WebElement btnAddNewCreditCard;

	@FindBy(css = ".no-of-cards")
	WebElement savedPaymentCount;

	@FindBy(css = ".fixed-header .checkout-progress-indicator")
	WebElement stickyNavigationBar;

	@FindBy(css = ".header-mian-banner .checkout-progress-indicator .step-1")
	WebElement headerChkoutStep1;
	
	@FindBy(css = ".header-mian-banner .checkout-progress-indicator .step-1 .progress-count")
	WebElement headerCountChkoutStep1;
	
	@FindBy(css = ".header-mian-banner .checkout-progress-indicator .step-1 .progress-text")
	WebElement headerTextChkoutStep1;

	@FindBy(css = ".header-mian-banner .checkout-progress-indicator .step-1 a")
	WebElement lnkheaderChkoutStep1;

	@FindBy(css = ".header-mian-banner .checkout-progress-indicator .step-2")
	WebElement headerChkoutStep2;
	
	@FindBy(css = ".header-mian-banner .checkout-progress-indicator .step-2 .progress-count")
	WebElement headerCountChkoutStep2;
	
	@FindBy(css = ".header-mian-banner .checkout-progress-indicator .step-2 .progress-text")
	WebElement headerTextChkoutStep2;

	@FindBy(css = ".header-mian-banner .checkout-progress-indicator .step-2 a")
	WebElement lnkheaderChkoutStep2;

	@FindBy(css = ".header-mian-banner .checkout-progress-indicator .step-3")
	WebElement headerChkoutStep3;
	
	@FindBy(css = ".header-mian-banner .checkout-progress-indicator .step-3 .progress-count")
	WebElement headerCountChkoutStep3;
	
	@FindBy(css = ".header-mian-banner .checkout-progress-indicator .step-3 .progress-text")
	WebElement headerTextChkoutStep3;
	
	@FindBy(css = ".header-mian-banner .checkout-progress-indicator .step-3.inactive")
	WebElement headerInactiveChkoutStep3;
	
	@FindBy(css = ".header-mian-banner .checkout-progress-indicator .step-3.inactive.submitted")
	WebElement headerInactiveSubmittedChkoutStep3;

	@FindBy(css = ".header-mian-banner .checkout-progress-indicator .step-3 a")
	WebElement lnkheaderChkoutStep3;

	@FindBy(css = "div.customer-signin-section h2.align-left")
	WebElement headerCustomerSignInSection;

	@FindBy(css = ".items-in-bag legend")
	WebElement headerProductList;

	@FindBy(css = ".items-in-bag legend")
	WebElement headerProductListMobile;

	@FindBy(css = "div.checkout-tab-head div.address-actions a")
	WebElement lnkEditInShoppingBagList;
	
	@FindBy(css = ".hide-mobile .price-total.discount")	
	WebElement productItemTotalMarkDownDesktop;
	
	@FindBy(css = ".hide-desktop.hide-tablet .price-total.discount")	
	WebElement productItemTotalMarkDownMobile;
	
	@FindBy(css = ".checkout-rebuttal")
	WebElement mdlPLCCRebuttal;

	@FindBy(css = ".acquisition-rebuttal")
	WebElement lnkLearnMoreInPLCCRebuttal;

	@FindBy(css = ".cart-row .sku .value")
	List<WebElement> lblProductSkuInCartList;
	
	@FindBy(css = ".bonus-product .hide-desktop.hide-tablet .item-total .bonus-item")
	WebElement lblSubtotalBonusPrdMobile;
	
	@FindBy(css = ".bonus-product .hide-mobile .item-total .bonus-item")
	WebElement lblSubtotalBonusPrdDesk_Tab;
	
	@FindBy(css = ".bonus-product-calloutMsg")
	WebElement bonusCalloutMsg;
		
	@FindBy(css = ".cart-row [data-attribute='size']")
	List<WebElement> lblSizeInCartList;

	@FindBy(css = ".cart-row [data-attribute='color']")
	List<WebElement> lblcolorInCartList;

	@FindBy(css = ".cart-row .product-availability-list")
	List<WebElement> lblAvailabilityInCartList;

	@FindBy(css = ".cart-row .cart-unit-price")
	List<WebElement> lblPriceInCartList;

	@FindBy(css = ".cart-unit-price.attribute + div[class='attribute'] .value")
	List<WebElement> lblShippingMethodInCartList;

	@FindBy(css = ".col-2 h2.heading")
	WebElement lblCheckoutAsGuest;

	@FindBy(css = ".col-1 .login-box")
	WebElement signInLoginBox;
	
	@FindBy(css = ".checkoutlogin .col-2 .login-box")
	WebElement guestLoginBox;

	@FindBy(css = ".col-2 .login-box-content p")
	WebElement lblSubCopyMsgInCheckoutAsGuest;

	@FindBy(css = ".checkout-tab.shipping h2")
	WebElement lblShippingTab;

	@FindBy(css = ".checkoutlogin-col-wrapper")
	WebElement loginSection;

	@FindBy(css = ".block-section.saved-address-block")
	WebElement shippingSection;

	@FindBy(css = ".items-in-bag.saved-address-block legend")
	WebElement shoppingBagHeading;

	//==============================================
	//		PLCC Approval Modal
	//==============================================
	@FindBy(css = ".ui-dialog.checkout-plcc-modal")
	WebElement mdlPLCCApproval;
	
	@FindBy(css = "#dialog-container div.plcc-pre-approved-name span.plcc-preapproved-name")
	WebElement firstNameInPLCCModal;
	
	@FindBy(css = "#dialog-container div.plcc-pre-approved-name span:not([class*='name'])")
	WebElement txtCongratulationsInPLCCModal;

	@FindBy(css = ".ui-dialog.plcc-acquisition-rebuttal-model")
	WebElement mdlPLCCAcquisitionRebuttal;

	@FindBy(css = ".ui-dialog.checkout-plcc-modal-step2")
	WebElement mdlPLCCApprovalStep2;
	
	@FindBy(css = ".order-totals-table .order-value.value")
	WebElement lblOrderSummaryTableTotal;
	
	@FindBy(css = ".product-list-item .display-inline-block")
	List<WebElement> lblPrdListNumbers;

	@FindBy(css = "div.checkout-plcc-modal div.plcc-banner")
	WebElement cntPLCCOffer;

	@FindBy(css = ".checkout-plcc-modal .plcc-preapproved-credit")
	WebElement lblPLCCCreditLimit;

	@FindBy(css = "#dialog-container .offer-content")
	WebElement cntOfferContent;

	@FindBy(css = ".checkout-plcc-modal .prescreen-message")
	WebElement cntPreScreenNotice;

	@FindBy(css = "div.checkout-plcc-modal div.legal-copy-message")
	WebElement cntLegalCopy;

	@FindBy(css = ".legal-copy-message.rebuttal-in-hide")
	WebElement cntLegalCopyFull;

	@FindBy(css = "div.checkout-plcc-modal button.plcc-get-it-button")
	WebElement btnGetItTodayInPLCC;

	@FindBy(css = "div.plcc-acquisition-rebuttal-model button.plcc-get-it-button")
	WebElement btnGetItTodayInPLCCACQ;

	@FindBy(css = "div[class*='checkout-plcc-modal'] button.plcc-nothanks-button")
	WebElement btnNoThanksInPLCC1;

	@FindBy(css = "div.plcc-acquisition-rebuttal-model button.plcc-nothanks-button")
	WebElement btnNoThanksInPLCCAcquisition;

	@FindBy(css = ".checkout-plcc-modal .form-row-button")
	WebElement divNoThanksAccept1;

	@FindBy(css = ".plcc-lower")
	WebElement divNoThanksAccept2;

	@FindBy(css = "#dialog-container button.plcc-nothanks-button")
	WebElement btnNoThanksInPLCC2;

	@FindBy(css = ".checkout-plcc-modal-step2 button.ui-dialog-titlebar-close")
	WebElement btnCloseInPLCC2;

	@FindBy(css = "#dialog-container .form-row.form-row-button .form-row-button-fixed")
	WebElement lblNoThanksGetItToday;

	@FindBy(css = "#ads-notice")
	WebElement lblAPRHtmlCode;

	@FindBy(css = ".electronic-iframe")
	WebElement iframeElectronicMsg;
	
	//==============================================
	//		PLCC Modal - Page-2
	//==============================================

	@FindBy(css = ".pre-approved-copy")
	WebElement lblPreApprovedCopy;

	@FindBy(css = ".legal-copy")
	WebElement lblLegalCopy;

	@FindBy(css = ".accept-offer-copy")
	WebElement lblAcceptOffCopy;

	@FindBy(css = "input[id*='dwfrm_creditapplication_ssn_']")
	WebElement txtSSNinPLCCStep2;

	@FindBy(css = "label[for*='dwfrm_creditapplication_ssn'] .label-text")
	WebElement placeHolderSSNinPLCCStep2;

	@FindBy(css = "span[id*='dwfrm_creditapplication_ssn_'][id*='-error']")
	WebElement errSSNinPLCCStep2;

	@FindBy(css = "input[id*='dwfrm_creditapplication_phone_']")
	WebElement txtMobileNoInPLCCStep2;

	@FindBy(css = "span[id*='dwfrm_creditapplication_phone_'][id*='-error']")
	WebElement errMobileNoInPLCCStep2;

	@FindBy(css = "label[for*='dwfrm_creditapplication_phone'] .label-text")
	WebElement placeHolderMobileNoInPLCCStep2;

	@FindBy(css = "input[id*='dwfrm_creditapplication_alternativePhone_']")
	WebElement txtAltMobileNoInPLCCStep2;

	@FindBy(css = "span[id*='dwfrm_creditapplication_alternativePhone_'][id*='-error']")
	WebElement errAltMobileNoInPLCCStep2;

	@FindBy(css = "label[for*='dwfrm_creditapplication_alternativePhone_'] .label-text")
	WebElement placeHolderAltMobileNoInPLCCStep2;

	@FindBy(css = ".plcc-primary .day")
	WebElement selectPLCCBirthDateDrp;
	
	@FindBy(css = ".plcc-primary .day select")
	WebElement selectPLCCBirthDate;

	@FindBy(css = "#dwfrm_creditapplication_birthday_day + .selected-option.selected")
	WebElement divPLCCBirthDateDrp;

	@FindBy(css = MYACCOUTLOGIN + "button[name='dwfrm_login_login']")
	WebElement btnSignIn;

	@FindBy(css = ".pt_checkout")
	WebElement readyElementCheckOut;

	@FindBy(css = MYACCOUTLOGIN + "input[name*='dwfrm_login_username']")
	WebElement fldEmail;

	@FindBy(css = ".plcc-primary .month")
	WebElement selectPLCCBirthMonthDrp;
	
	@FindBy(css = ".plcc-primary .month select")
	WebElement selectPLCCBirthMonth;

	@FindBy(css = "#dwfrm_creditapplication_birthday_month + .selected-option.selected")
	WebElement divPLCCBirthMonthDrp;

	@FindBy(css = ".plcc-primary .year")
	WebElement selectPLCCBirthYearDrp;
	
	@FindBy(css = ".plcc-primary .year select")
	WebElement selectPLCCBirthYear;

	@FindBy(css = "#dwfrm_creditapplication_birthday_year + .selected-option.selected")
	WebElement divPLCCBirthYearDrp;

	@FindBy(css = ".plcc-accept-button")
	WebElement btnAcceptInPLCCStep2;
	
	@FindBy(css = ".plcc-modal-error")
	WebElement divPLCCStep2Error;

	@FindBy(css = ".plcc-ssn-label .question-mark-icon.dialog-tooltip")
	WebElement icoSSNToolTip;

	@FindBy(css = ".tooltip-dialog")
	WebElement toolTipForPLCC;

	@FindBy(css = ".plcc-masked-number")
	WebElement lblSSNMasked;

	@FindBy(xpath = "//span[contains(text(),'Birth Date')]")
	WebElement lblBirthDate;

	@FindBy(css = ".plcc-profile-summary")
	WebElement lblAddressInPLCC;

	@FindBy(css = ".plcc-profile-details.plcc-profile-name")
	WebElement lblNameInPLCCPreAddress;

	@FindBy(css = ".plcc-profile-details.plcc-profile-address")
	WebElement lblAddressInPLCCPreAddress;

	@FindBy(css = ".plcc-profile-details.plcc-city")
	WebElement lblCityInPLCCPreAddress;

	@FindBy(css = ".plcc-edit-profile")
	WebElement lnkEditInPLCCAddress;

	@FindBy(css = ".tooltip-dialog .ui-dialog-titlebar-close")
	WebElement iconCloseToolTip;

	@FindBy(css = "input[id*='dwfrm_creditapplication_firstName_']")
	WebElement txtFirstNameInPLCCStep2;

	@FindBy(css = "span[id*='dwfrm_creditapplication_firstName_'][id*='-error']")
	WebElement errFirstNameInPLCCStep2;

	@FindBy(css = "label[for*='dwfrm_creditapplication_firstName'] .label-text")
	WebElement placeHolderFirstNameInPLCCStep2;

	@FindBy(css = "input[id*='dwfrm_creditapplication_lastName_']")
	WebElement txtLastNameInPLCCStep2;

	@FindBy(css = "span[id*='dwfrm_creditapplication_lastName_'][id*='-error']")
	WebElement errLastNameInPLCCStep2;

	@FindBy(css = "label[for*='dwfrm_creditapplication_lastName_'] .label-text")
	WebElement placeHolderLastNameInPLCCStep2;

	@FindBy(css = "input[id*='dwfrm_creditapplication_address1_']")
	WebElement txtAddress1InPLCCStep2;

	@FindBy(css = "span[id*='dwfrm_creditapplication_address1_'][id*='-error']")
	WebElement errAddress1InPLCCStep2;

	@FindBy(css = "label[for*='dwfrm_creditapplication_address1_'] .label-text")
	WebElement placeHolderAddress1InPLCCStep2;

	@FindBy(css = "input[id*='dwfrm_creditapplication_address2_']")
	WebElement txtAddress2InPLCCStep2;

	@FindBy(css = "span[id*='dwfrm_creditapplication_address2_'][id*='-error']")
	WebElement errAddress2InPLCCStep2;

	@FindBy(css = "label[for*='dwfrm_creditapplication_address2_'] .label-text")
	WebElement placeHolderAddress2InPLCCStep2;

	@FindBy(css = "input[id*='dwfrm_creditapplication_city_']")
	WebElement txtCityInPLCCStep2;

	@FindBy(css = "span[id*='dwfrm_creditapplication_city_'][id*='-error']")
	WebElement errCityInPLCCStep2;

	@FindBy(css = "label[for*='dwfrm_creditapplication_city_'] .label-text")
	WebElement placeHolderCityInPLCCStep2;

	@FindBy(css = "input[id*='dwfrm_creditapplication_zipcode_']")
	WebElement txtZipcodeInPLCCStep2;

	@FindBy(css = "span[id*='dwfrm_creditapplication_zipcode_'][id*='-error']")
	WebElement errZipcodeInPLCCStep2;

	@FindBy(css = "label[for*='dwfrm_creditapplication_zipcode_'] .label-text")
	WebElement placeHolderZipcodeInPLCCStep2;

	@FindBy(css = "input[id*='dwfrm_creditapplication_email_']")
	WebElement txtEmailInPLCCStep2;

	@FindBy(css = "span[id*='dwfrm_creditapplication_email_'][id*='-error']")
	WebElement errEmailInPLCCStep2;

	@FindBy(css = "label[for*='dwfrm_creditapplication_email_'] .label-text")
	WebElement placeHolderEmailInPLCCStep2;

	@FindBy(css = "select[id*='dwfrm_creditapplication_states_state']")
	WebElement selectPLCCStateDrp;

	@FindBy(css = "select[id*='dwfrm_creditapplication_states_state'] + .selected-option.selected")
	WebElement divPLCCStateDrp;

	@FindBy(css = ".plcc-model2-form .contact-disclaimer")
	WebElement lblContactDisclaimer;
	
	@FindBy(css = ".contact-disclaimer:nth-child(13)")
	WebElement lblContactDisclaimerAlt;

	@FindBy(css = ".pre-screen-notice")
	WebElement lblPreScreenNotice;

	@FindBy(css = "div[data-trackvalue='CCA - Schumer Box & Financial Terms']")
	WebElement lblCreditCardTerms;

	@FindBy(css = "#dwfrm_creditapplication_consent")
	WebElement chkConsonent;

	@FindBy(css = "label[for='dwfrm_creditapplication_consent']")
	WebElement lblConsonentText;

	@FindBy(css = divAddressValidationModal + "#address-validation-dialog")
	WebElement mdlAddressValidation;

	@FindBy(css = "div[class*='address-validation-dialog'][style*='block'] .correct-address-button button")
	WebElement btnCorrectAddress;

	@FindBy(css = ".field-desc>label")
	List<WebElement> lblShippingMethod;

	@FindBy(css = ".checkout-pobox-smexception")
	WebElement mdlPOBox;

	@FindBy(css = ".button-fancy-large.edit-shipping.full-right.edit-shipping")
	WebElement btnEditPOBoxOverlay;

	@FindBy(css = ".checkout-pobox-smexception button.ship-method-submit")
	WebElement btnContinueInPOBoxModal;

	@FindBy(css = "#dwfrm_profile_customer_firstname")
	WebElement txtProfileFirstName;

	@FindBy(css = "#dwfrm_profile_customer_lastname")
	WebElement txtProfileLastName;

	@FindBy(css = "#dwfrm_profile_customer_email")
	WebElement txtProfileEmailAdd;

	@FindBy(css = "#dwfrm_profile_customer_emailconfirm")
	WebElement txtProfileConfirmEmailAdd;

	@FindBy(css = "input[id*='dwfrm_profile_login_password_']")
	WebElement txtProfilePwd;

	@FindBy(css = "input[id*='dwfrm_profile_login_passwordconfirm_']")
	WebElement txtProfilePwdConfirm;

	@FindBy(css = ".saved-address-block .fields-containter.block-section")
	WebElement sectionSaveAddressWithFields;

	@FindBy(css = ".selected-shipping-method")
	WebElement selectedShippingMethodInOverlay;

	@FindBy(css = ".button-fancy-large.ship-method-submit")
	WebElement btnContinueExpeditedOverlay;

	@FindBy(css = ".cancel-ship-method-except")
	WebElement btnCancelExpeditedOverlay;

	@FindBy(css = "input[id*='shipping-method-'][checked='checked']")
	WebElement rdoShippingMethod;

	@FindBy(css = "#dialog-container > div")
	WebElement poBoxExclusionModal;

	@FindBy(css = ".form-row.form-indent.ship-exception.label-inline.even .field-wrapper")
	List<WebElement> deliveryOption;

	@FindBy(css = ".pobox-exclusions.sm-main-section")
	WebElement shippingmethodExceptionOverlay;

	@FindBy(css = ".delivery-option-heading")
	WebElement deliveryOptionHeading;

	@FindBy(css = ".pobox-heading.shippingexception-heading")
	WebElement shippingmethodExceptionOverlayHeading;

	@FindBy (css = ".pobox-intro.shippingexception-intro")
	WebElement shippingMethodExceptionItemIntroduction;

	@FindBy (css = ".pobox-intro.shippingexception-intro .selected-shipping-method")
	WebElement shippingMethodExceptionItemDeliveryoption;

	@FindBy(css = ".apply-slim-scrollbar")
	WebElement itemIntroductionText;

	@FindBy(css = ".selected-shipping-method")
	WebElement updateShippingMethodName;

	@FindBy(css = "billing-cards-section block-section")
	WebElement billingOptionpage;

	@FindBy(css = ".error-tender-rebuttal")
	WebElement tenderRebuttalMsg;
	
	@FindBy(css = ".card-list .form-row.cvn.cvn.required .input-text.cvn.required")
	WebElement cvvNumber;
	
	@FindBy(css = ".question-mark-icon.cvv-tooltip.option-tooltip")
	WebElement cvvToolTip;

	@FindBy(css = "div[class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front cvv-tooltip tooltip-dialog ui-draggable']")
	WebElement cvvToolTipContent;

	@FindBy(css = ".cvv-tooltip.tooltip-dialog .ui-dialog-titlebar-close")
	WebElement cvvToolTipContentClose;

	@FindBy(css = "div[class='cvninfo selected'] input[id*='dwfrm_billing_paymentMethods_creditCard_cvn_']")
	WebElement txtCVN;

	@FindBy(css = "div[class='form-row cvn cvn required error-handle'] span[id*='dwfrm_billing_paymentMethods_creditCard_cvn_']")
	WebElement txtCVVError;

	@FindBy(css = ".carddetails.selected")
	WebElement cardDetailsSelected;

	@FindBy(css = ".plcc-message")
	WebElement plccMsg;

	@FindBy(css = ".paywith")
	WebElement amtRemaining;

	@FindBy(css = ".paypal-email")
	WebElement paypalEmail;

	@FindBy(css = ".details.block-section")
	WebElement paypalShippinAddressSection;

	@FindBy(css = "button[id*='paypalExpressButton']")
	WebElement btnPaypal;
	
	@FindBy(css = "div[id*='zoid-paypal-checkout'] .paypal-checkout-close")
	WebElement btnClosePayPal;
	
	 @FindBy(css = ".textInput input#email")
	 WebElement emailPayPal;

	@FindBy(css = ".button-fancy-large.ship-method-submit")
	WebElement OverlayContinueButton;

	@FindBy(css = ".cancel-ship-method-except")
	WebElement OverlayCancelButton;

	@FindBy(css = ".shippingoverlay-link")
	WebElement shippingCostToolTip;

	@FindBy(css = ".shipping-overlay .ui-dialog-content")
    WebElement shippingCostOverlay; 
	
	@FindBy(css = ".shipping-overlay .ui-dialog-content .discount .value")		
	WebElement discountPriceToolTip;
	
	@FindBy(css = "#dialog-container .surcharge-product.order-detail")		
	WebElement giftCardPriceInShippingCostOverlay;
	
	@FindBy(css = ".shipping-overlay .shippingtotal.order-detail")
	WebElement totalFeeInTooltip;
	
	@FindBy(css = ".shipping-overlay .shipping-method-cost .value")
	WebElement txtShippingPriceInTooltip;
	
	@FindBy(css = "#dialog-container .surcharge-product")
	List<WebElement> lstSurchargeProductRate;
	
	@FindBy(css =  ".shipping-overlay button.ui-dialog-titlebar-close")
	WebElement shippingCostOverlayClose;

	@FindBy(css =  "div[class='dialog-content ui-dialog-content ui-widget-content']  .promo.discount.order-detail")
	WebElement divShippingPromotionDetail;

	@FindBy(css = "#primary > div.checkout-tabs.spc-summary > div.checkout-tab.summary.review > div.checkout-tab-content.tab-content-show.place-order > div.error-form")
	WebElement error;

	@FindBy(css = ".form-row.firstName.required input")
	WebElement firstName;

	@FindBy(css = ".form-row.lastName.required input")
	WebElement lastName;

	@FindBy(css = ".form-row.address1.required input")
	WebElement address1;

	@FindBy(css = ".form-row.address2  input")
	WebElement address2;

	@FindBy(css = ".form-row.postal.required input")
	WebElement postalCode;

	@FindBy(css = ".form-row.city.required input")
	WebElement city;

	@FindBy(css = ".form-row.phone.required input")
	WebElement Phone;

	@FindBy(css = "label[for='dwfrm_billing_billingAddress_addressFields_firstName'] .error")
	WebElement errorMsgFirstName;

	@FindBy(css = "label[for='dwfrm_billing_billingAddress_addressFields_lastName'] .error")
	WebElement errorMsgLastName;

	@FindBy(css = "label[for='dwfrm_billing_billingAddress_addressFields_address1'] .error")
	WebElement errorMsgAddress1;

	@FindBy(css = "label[for='dwfrm_billing_billingAddress_addressFields_address2'] .error")
	WebElement errorMsgAddress2;

	@FindBy(css = "label[for='dwfrm_billing_billingAddress_addressFields_city'] .error")
	WebElement errorMsgcity;

	@FindBy(css = "label[for='dwfrm_billing_billingAddress_addressFields_postal'] .error")
	WebElement errorMsgPostalcode;

	@FindBy(css = "label[for='dwfrm_billing_billingAddress_addressFields_states_state'] .error")
	WebElement errorMsgState;

	@FindBy(css = "label[for='dwfrm_billing_billingAddress_addressFields_phone'] .error")
	WebElement errorMsgPhone;

	@FindBy(css = ".form-row.firstName.required .label-text")
	WebElement firstNamePlaceholder;

	@FindBy(css = ".form-row.lastName.required .label-text")
	WebElement lastNamePlaceholder;

	@FindBy(css = ".form-row.address1.required .label-text")
	WebElement address1Placeholder;

	@FindBy(css = ".form-row.address2  .label-text")
	WebElement address2Placeholder;

	@FindBy(css = ".form-row.postal.required .label-text")
	WebElement postalPlaceholder;

	@FindBy(css = ".form-row.city.required .label-text")
	WebElement cityPlaceholder;

	@FindBy(css = ".cart-row")
	List<WebElement> divCartRow;
	
	@FindBy(css = ".name a")
	List<WebElement> lstCartProductNames;

	@FindBy(css = "label[for='is-CREDIT_CARD']")
	WebElement rdoCreditCard;

	@FindBy(css = ".fields-containter.block-section")
	WebElement billingAddressFields;

	@FindBy(css = ".billing-page-continue")
	WebElement sectionContinueBilling;

	@FindBy(xpath = "//input[@id='is-CREDIT_CARD']//parent::div")
	WebElement divRdoCreditCard;

	@FindBy(css = "div[for='is-PayPal']")
	WebElement rdoPaypal;

	@FindBy(xpath = "//input[@id='is-PayPal']//parent::div")
	WebElement divRdoPaypal;
	
	@FindBy(css = "div[data-method='CREDIT_CARD']")
	WebElement divCreditCardListSection;
	
	@FindBy(css = ".tax-error-message")
	WebElement taxErrorMessage;
		
	@FindBy(css = ".addresses-section .addr-dialog-heading")
	WebElement mdlAddressSuggestionHeading;
		
	@FindBy(css = ".cardinfo #dwfrm_billing_billingAddress_differedBilling")
	WebElement chkBoxDifferedBilling;
	
	@FindBy(css = ".addresses-section .address-information")
	WebElement mdlAddressSuggestionInformation;
		
	@FindBy(css = ".addresses-section .address-information .custom-radio-icon")
	WebElement addressSugRadioButton;
	 
	@FindBy(css = ".addr-dialog-sub-heading")
	List<WebElement> addressHeading; 
		
	@FindBy(css = ".cancel-avs-suggestion")
	WebElement cancelAvsButton;
	
	//==============================================
	//		PLCC Modal - Congratulations
	//==============================================
	@FindBy(css = ".approved-model")
	WebElement approvedModal;
	
	@FindBy(css = "#dialog-container .review-model:not(.error-model)")
	WebElement underReviewModal;
	
	@FindBy(css = ".plcc-apply-error")
	WebElement applyErrorModalPLCC;
	
	@FindBy(css = ".continue-checkout")
	WebElement btnContinueToCheckout;

	@FindBy(css = ".plcc-message-card")
	WebElement lblPLCCMessage;

	@FindBy(css = ".carddetails.plcccard")
	WebElement section_First_PLCC_Card;

	@FindBy(css = ".carddetails.plcccard .img")
	WebElement logo_First_PLCC_Card;

	@FindBy(css = ".carddetails.plcccard .cardname.hide-mobile")
	WebElement lbl_First_PLCC_Card_Name;
	
	@FindBy(css = ".carddetails.plcccard .cardname.hide-desktop")
	WebElement lbl_First_PLCC_Card_Name_Mobile;
	
	@FindBy(css = ".carddetails.selected .cardholder")
	WebElement lbl_Selected_Card_holder_Name;
	
	@FindBy(css = ".carddetails:not(.selected) .cardholder")
	WebElement lbl_UnSelected_Card_holder_Name;

	@FindBy(css = ".carddetails.plcccard .cardholder")
	WebElement lbl_First_PLCC_Card_holder_Name;

	@FindBy(css = ".carddetails.plcccard .available-credit")
	WebElement lbl_First_PLCC_Available_Credit;

	@FindBy(css = ".carddetails.plcccard.selected")
	WebElement selectedPLCCCard;

	@FindBy(css = ".card-list #dwfrm_billing_billingAddress_differedBilling")
	WebElement rdoDifferedBilling;

	@FindBy(css = ".carddetails.plcccard .rewardPoints")
	WebElement section_First_PLCC_Rewards;

	@FindBy(css = ".carddetails.plcccard .cardnumber-value")
	WebElement lbl_First_PLCC_Available_Credit_Value;

	@FindBy(css = ".carddetails.plcccard div.plcc-tooltip")
	WebElement lbl_First_PLCC_Tooltip;

	@FindBy(css = "div[class='carddetails selected']")
	WebElement section_Selected_Non_PLCC_Card;

	@FindBy(css = "div[class='carddetails selected'] span.img")
	WebElement logo_Selected_Non_PLCC_Card;

	@FindBy(css = "div[class='carddetails selected'] .cardname.hide-mobile")
	WebElement lbl_Selected_Non_PLCC_Card_Name;
	
	@FindBy(css = "div[class='carddetails selected'] .cardname.hide-desktop")
	WebElement lbl_Selected_Non_PLCC_Card_Name_Mobile;

	@FindBy(css = "div[class='carddetails selected'] .cardholder")
	WebElement lbl_Selected_Non_PLCC_Card_holder_Name;

	@FindBy(css = "div[class='carddetails selected'] .cardnumber")
	WebElement lbl_Selected_Non_PLCC_Card_Number;

	@FindBy(css = "div[class='carddetails selected'] .expdate")
	WebElement lbl_Selected_Non_PLCC_Card_Exp_Date;

	@FindBy(css = ".cvninfo label[for*='dwfrm_billing_paymentMethods_creditCard_cvn'] .label-text")
	WebElement lbl_Selected_Non_PLCC_Card_CVV_PlaceHolder;

	@FindBy(css = ".cvv-tooltip.tooltip-dialog")
	WebElement mdlCVVTooltipModal;
	
	@FindBy(css = ".carddetails.expired")
	WebElement expireCard;

	@FindBy(css = ".carddetails.expired .expdate")
	WebElement lbl_Expired_Non_PLCC_ExpDate;

	@FindBy(css = ".hidedetails")
	WebElement divHiddenCards;

	@FindBy(css = ".order-confirmation-details")
	WebElement orderConfirmation;
	
	@FindBy(css = "#RegistrationForm input.password")		
	WebElement fldPasswordOrderReceipt;		

	@FindBy(css = "#RegistrationForm input.confirmpassword")		
	WebElement fldConfirmPasswordOrderReceipt;

	@FindBy(css = "label[for='shipping-method-Regular']")
	WebElement lblRegularShippingMethodName;

	@FindBy(css = "label[for='shipping-method-Regular'] span")
	WebElement lblRegularShippingMethodCost;

	@FindBy(css = "label[for='shipping-method-Regular'] span.standard-shipping")
	WebElement lblRegularShipping_standard_cost;

	@FindBy(css = "label[for='shipping-method-Regular'] span.discount-shipping")
	WebElement lblRegularShipping_discount_cost;

	@FindBy(css = "label[for='shipping-method-Regular'] + .promo")
	WebElement lblRegularShippingPromoMsg;

	@FindBy(css = "label[for='shipping-method-Regular'] + div+ div.form-caption")
	WebElement lblRegularShippingMessage_with_promo;

	@FindBy(css = "label[for='shipping-method-Regular'] .shippingsurcharge")
	WebElement lblRegularShippingSurcharge;
	
	@FindBy(css = "label[for='shipping-method-SuperFast'] .shippingsurcharge")
	WebElement lblSuperFastShippingSurcharge;
	
	@FindBy(css = ".shippingsurcharge")
	WebElement lblShippingSurcharge;
	
	@FindBy(css = "label[for='shipping-method-SuperFast']")
	WebElement lblSuperFastShippingMethodName;

	@FindBy(css = "label[for='shipping-method-Regular'] + div.form-caption")
	WebElement lblRegularShippingMessage;

	@FindBy(css = "label[for='shipping-method-SuperFast'] + div.form-caption")
	WebElement lblSuperFastShippingMessage;

	@FindBy(xpath = "//label[@for='shipping-method-SuperFast']/following-sibling::span[@class='shippingmethod-exception']")
	WebElement lblSuperFast_Mixed_Method_Exception;

	@FindBy(id = "shipping-method-list")
	WebElement section_Shipping_methods;
	
	@FindBy(css = ".basket-details")
	WebElement divCheckoutSummeryContent;
	
	@FindBy(css = ".items-in-bag .head-wrapper legend")
	WebElement legendItemHeading;

	@FindBy(css = ".checkout-tab.billing h2")
    WebElement placeOrderBillingHeading;

	@FindBy(css = ".saved-address-block .block-section legend")
    WebElement billingSectionHeading;
    
    @FindBy(css = ".checkout-tab.billing .mini-billing-address h3")
    WebElement placeorderBillingSectionHeading;
    
    @FindBy(css = ".mini-payment-instrument")
    WebElement placeorderCardSection;
    
    @FindBy(css = ".billing-right-section .reward-certificate")
    WebElement placeorderRewardCertificate;
    
    @FindBy(css = ".billing-right-section .promo-code")
    WebElement placeorderpromoCode;
    
    @FindBy(css = ".billing-right-section .gift-card")
    WebElement placeorderGiftCard;
	
	@FindBy(css = ".checkout-tab.billing a")
    WebElement placeOrderlnkEditBillingAddress;
	
	@FindBy(css = "div.selected-option span")
	WebElement savedAddressNickName;
	
	@FindBy(css = ".address-id-mobile")
	WebElement savedAddressNickNameMobile;
	
	//PLCC Acquisition Rebuttal
	@FindBy(css = "#dialog-container")
	WebElement divAcquistionRebuttal;
	
	@FindBy(css = "#dialog-container .welcome")
	WebElement divAcquistionRebuttalWelcome;
	
	@FindBy(css = "#dialog-container .pre-approved")
	WebElement divAcquistionRebuttalPreApprovedMessage;
	
	@FindBy(css = "#dialog-container .plcc-banner")
	WebElement divAcquistionRebuttalBanner;
	
	@FindBy(css = "#dialog-container .prescreen-message")
	WebElement divPLCCPrescreenMessage;
	
	@FindBy(css = "#dialog-container .legal-copy")
	WebElement divPLCCLegalCopy;
	
	@FindBy(css = "#dialog-container .legal-copy .legal-copy-message")
	WebElement lnkPLCCLegalCopyClickHere;
	
	@FindBy(css = "#dialog-container .legal-copy .legal-copy-message a[href*='agreements']")
	WebElement lnkLegalCopyClick;
	
	@FindBy(css = ".checkout-tab.billing .selected-details")
	WebElement lblBillingAddress;
	
	@FindBy(css = ".order-summary-footer .required-fields-warn")
	WebElement divIncompleteFormWarning;
	
	@FindBy(xpath = "//div[@class='item-details'] //div[@class='specialmessaging'] //p[contains(text(),'Hemmable')]")
	WebElement divSpecialMessageHemmingValue;
	
	@FindBy(xpath = "//div[@class='specialmessaging-Mobile'] //p[contains(text(),'Hemmable')]")
	WebElement divSpecialMessageHemmingValueMobile;
	
	@FindBy(css = ".expdate")
	List<WebElement> cardExpDates;
	
	@FindBy(css = "#dwfrm_singleshipping_shippingAddress .required-fields-warn")
	WebElement errShippingMissingFields;
	
	@FindBy(css = ".required-field")
	WebElement errMissingFieldsShippingHeading;
	
	@FindBy(css = ".selectpayment .required-fields-warn")
	WebElement errMissingFieldAboveSelectPayment;
	
	@FindBy(css = ".saved-address-block .required-field")
	WebElement errMissingFieldBillingHeading;
	
	@FindBy(css = ".payment-method-options .required-field")
	WebElement errMissingFieldCCRdo;
	
	@FindBy(name = "dwfrm_billing_save")
	WebElement btnPaymentContinue;
	
	@FindBy(css = ".checkout-tab.shipping .fieldsection:not(.hide)")
	WebElement divEmptyFormShipping;
	
	//==============================================
	//        Defer Payment
	//==============================================
	
	@FindBy(css = ".defer-show")
	WebElement divDeferPayment;
	    
	@FindBy(css = "#dwfrm_billing_deferCode")
	WebElement txtBoxDeferPaymentCode;
	    
	@FindBy(css = ".apply-defer span")
	WebElement applyDeferPaymentCode;


	//================================================================================
	//			WebElements Declaration End
	//================================================================================

	/**
	 * constructor of the class
	 * 
	 * @param driver : WebDriver
	 */
	public CheckoutPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Checkout Page didn't display", driver);
		}

		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get page load status
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}

	/**
	 * To click on primary logo
	 * @throws Exception - Exception
	 */
	public HomePage clickPrimaryLogo() throws Exception{
		BrowserActions.clickOnElementX(lnkPrimaryLogo, driver, "Update Information");
		Utils.waitForPageLoad(driver);
		return new HomePage(driver).get();

	}
	
	/**
	 * To Click on the continue button in the guest log in page
	 * @return CheckoutPage object
	 * @throws Exception - Exception
	 */
	public void clickOnContinueInGuestLogin()throws Exception{
		BrowserActions.clickOnElementX(btnContinue, driver, "Continue");
		Utils.waitForPageLoad(driver);
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}
	
	/**
	 * To click on edit sign in link
	 * @throws Exception - Exception
	 */
	public void clickEditSignIn() throws Exception{
		BrowserActions.clickOnElementX(lnkEditSigninSection, driver, "Edit Link");
		Utils.waitForElement(driver, txtGuestEmail);
	}

	/**
	 * To enter guest user field
	 * @param emailAddress - to get the email Address
	 * @throws Exception - throws an exception
	 */
	public void enterGuestUserEmail(String emailAddress) throws Exception{
		BrowserActions.typeOnTextField(txtGuestEmail, emailAddress, driver, "Guest User email Address");
	}
	
	/**
	 * To click On Brand Image
	 * @throws Exception - throws an exception
	 */
	public void clickOnBrandImage() throws Exception{
		BrowserActions.clickOnElementX(imgBrandImage, driver, "Brand Image");
	}

	/**
	 * To enter invalid guest user field
	 * @param emailAddress - to get the email Address
	 * @throws Exception - throws exception
	 */
	public void enterInvalidGuestUserEmail(String emailAddress) throws Exception{
		BrowserActions.typeOnTextField(txtGuestEmail, emailAddress, driver, "Guest User email Address");
		WebElement ele = BrowserActions.checkLocator(driver, "[name='dwfrm_login_unregistered']");
		BrowserActions.clickOnElementX(ele, driver, "register btn");
		Utils.waitForElement(driver, lblGuestEmailError);
	}

	/**
	 * To click Continue button
	 * @param credential -
	 * @throws Exception - Exception
	 */
	public void continueToShipping(String... credential) throws Exception{
		if(credential.length > 0){
			if(credential[0].contains("|")){
				txtRegUserEmail.clear();
				txtRegUserPwd.clear();
				txtRegUserEmail.sendKeys(credential[0].split("\\|")[0]);
				txtRegUserPwd.sendKeys(credential[0].split("\\|")[1]);
				BrowserActions.clickOnElementX(btnSignInAsUser, driver, "SignIn Button");
				Log.event("Checkout as Signed In User...");
			}else{
				txtGuestEmail.clear();
				txtGuestEmail.sendKeys(credential[0]);
				BrowserActions.clickOnElementX(btnGuestContinue, driver, "Continue Button");
				Log.event("Checkout as Guest User...");
			}

		}else{
			BrowserActions.clickOnElementX(btnGuestContinue, driver, "Update Information");
			Log.event("Checkout as Guest User...");
		}
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}
	
	
	/**
	 * To enter shipping address
	 * @param shippingAddress1 -
	 * @throws Exception -
	 */
	public void enterShippingAddress1(String shippingAddress1) throws Exception{
		BrowserActions.scrollInToView(txtShippingAdd1, driver);
		BrowserActions.javascriptClick(txtShippingAdd1, driver, "Shipping address1");
		BrowserActions.typeOnTextField(txtShippingAdd1, shippingAddress1, driver, "Shipping Address1");
		if(!(Utils.waitForElement(driver, divPoBoxBlockOverlay))) {
			System.out.println("Entering tab");
			BrowserActions.javascriptClick(txtShippingAdd2, driver, "Shipping address2");
			Utils.waitForElement(driver, divPoBoxBlockOverlay);
		}
	}

	/**
	 * To enter shipping address as guest user
	 * @param useAddressForShipping -
	 * @param shippingmethod -
	 * @return HashMapvalues
	 * @throws Exception -
	 */
	public LinkedHashMap<String, String> fillingShippingDetailsAsGuest(
			String useAddressForShipping, ShippingMethod shippingmethod) throws Exception {
		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		String randomFirstName = RandomStringUtils.randomAlphabetic(5).toLowerCase();
		shippingDetails.put("type_firstname_First" + randomFirstName, txtShippingFirstName);
		String randomLastName = RandomStringUtils.randomAlphabetic(5) .toLowerCase();
		shippingDetails.put("type_lastname_Last" + randomLastName, txtShippingLastName);

		String address = checkoutProperty.getProperty(useAddressForShipping);
		String address2 = new String();
		String zipcode = address.split("\\|")[4];
		String city = address.split("\\|")[2];
		String state = address.split("\\|")[3];
		String phoneNo = address.split("\\|")[5];
		String address1 = address.split("\\|")[0];
		//String country = address.split("\\|")[6];
		
		if(address.split("\\|").length > 7) {
			String guestFirstName = address.split("\\|")[7];
			String guestLastName = address.split("\\|")[8];
			shippingDetails.put("type_firstname_" + guestFirstName, txtShippingFirstName);
			shippingDetails.put("type_lastname_" + guestLastName, txtShippingLastName);
		}

		if (useAddressForShipping.contains("_1")) {
			address2 = address.split("\\|")[1];
			zipcode = address.split("\\|")[4];
			city = address.split("\\|")[2];
			state = address.split("\\|")[3];			
			phoneNo = address.split("\\|")[5];
			//country = address.split("\\|")[6];
		}

		shippingDetails.put("type_address_" + address1, txtShippingAddress);
		if (useAddressForShipping.contains("_1"))
			shippingDetails.put("type_address1_" + address2, txtShippingAddress2);

		shippingDetails.put("type_zipcode_" + zipcode, txtShippingZipcode);

		if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(btnShippingState)))){
			shippingDetails.put("select_state_" + state, btnShippingState);	

		}else
			shippingDetails.put("select1_state_" + state, btnShippingStateDiv);

		shippingDetails.put("type_city_" + city, txtShippingCity);			
		shippingDetails.put("type_phoneno_" + phoneNo, txtShippingPhoneNo);

		WebElement element = null;
		if (shippingmethod != null) {
			switch (shippingmethod) {
			case Standard:
				shippingDetails.put("check_Shipping method_YES", rdoStandard);
				element = radioStandardDelivery;
				break;
			case Express:
				shippingDetails.put("check_Shipping method_YES", rdoExpress);
				element = radioExpressDelivery;				
				break;
			case SuperFast:
				shippingDetails.put("check_Shipping method_YES", rdoSuperFast);
				element = radioSuperFastDelivery;
				break;
			case NextDay:
				shippingDetails.put("check_Shipping method_YES", rdoNextDay);
				element = radioNextDayDelivery;
				break;
			case HeathersFixed:
				shippingDetails.put("check_Shipping method_YES", rdoHeathers);
				element = radioHeathersDelivery;
				break;
			}
		}

		ShippingPageUtils.enterShippingDetails(shippingDetails, driver);
		shippingDetails.remove("check_Shipping method_YES");

		if(element != null) {
			shippingDetails.put("Shipping_method", BrowserActions.getText(driver, element, "Shipping method"));
		}
		
		Log.event("Entered Shipping Details as a Guest user", StopWatch.elapsedTime(startTime));
		Utils.waitForPageLoad(driver);
		Utils.waitForPageLoad(driver);

		return shippingDetails;
	}

	/**
	 * To enter shipping address as guest user
	 * @param useAddressForBilling -
	 * @param shippingInfo -
	 * @param shippingMethod -
	 * @return Hashmap values 
	 * @throws Exception -
	 */
	public LinkedHashMap<String, String> fillingShippingDetailsAsGuest(
			String useAddressForBilling, String shippingInfo, ShippingMethod shippingMethod) throws Exception {
		
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		String address = checkoutProperty.getProperty(shippingInfo);

		if(address.split("\\|").length > 7){
			shippingDetails.put("type_First Name_" + address.split("\\|")[7], txtShippingFirstName);
			shippingDetails.put("type_Last Name_" + address.split("\\|")[8], txtShippingLastName);
		}else{
			shippingDetails.put("type_First Name_User" + RandomStringUtils.randomAlphabetic(5), txtShippingFirstName);
			shippingDetails.put("type_Last Name_" + RandomStringUtils.randomAlphabetic(5), txtShippingLastName);
		}
		shippingDetails.put("type_Address1_" + address.split("\\|")[0], txtShippingAddress);
		shippingDetails.put("type_Address2_" + address.split("\\|")[1], txtShippingAddress2);
		shippingDetails.put("type_Zipcode_" + address.split("\\|")[4], txtShippingZipcode);

		if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(btnShippingState)))){
			shippingDetails.put("select_State_" + address.split("\\|")[3], btnShippingState);	
		} else
			shippingDetails.put("select1_State_" + address.split("\\|")[3], btnShippingStateDiv);

		shippingDetails.put("type_City_" + address.split("\\|")[2], txtShippingCity);

		shippingDetails.put("type_PhoneNo_" + address.split("\\|")[5], txtShippingPhoneNo);

		if(useAddressForBilling.equalsIgnoreCase("yes"))
			shippingDetails.put("check_Use Address For Billing_YES", chkUseAddressForBilling);

		if (shippingMethod != null) {
			switch (shippingMethod) {
			case Standard:
				shippingDetails.put("check_Shipping method_YES", rdoStandard);
				break;
			case Express:
				shippingDetails.put("check_Shipping method_YES", rdoExpress);
				break;
			case SuperFast:
				shippingDetails.put("check_Shipping method_YES", rdoSuperFast);
				break;
			case NextDay:
				shippingDetails.put("check_Shipping method_YES", rdoNextDay);
				break;
			case HeathersFixed:
				shippingDetails.put("check_Shipping method_YES", rdoHeathers);
				break;
			}
		}

		if (Utils.waitForElement(driver, txtShippingFirstNameFld)) {
			ShippingPageUtils.enterShippingDetails(shippingDetails, driver);
			Log.event("Entered shipping details.");
		}
		shippingDetails.clear();
		shippingDetails = getShippingInformation();
		return shippingDetails;

	}

	/**
	 * To enter billing address as guest user
	 * @param billingInfo -
	 * @return -
	 * @throws Exception -
	 */
	public LinkedHashMap<String, String> fillingBillingDetailsAsGuest(String billingInfo) throws Exception {
		LinkedHashMap<String, String> billingDetails = new LinkedHashMap<String, String>();
		String address = checkoutProperty.getProperty(billingInfo);

		if(address.split("\\|").length > 7){
			billingDetails.put("type_First Name_" + address.split("\\|")[7], txtBillingFirstName);
			billingDetails.put("type_Last Name_" + address.split("\\|")[8], txtBillingLastName);
		}else{
			billingDetails.put("type_First Name_User" + RandomStringUtils.randomAlphabetic(5), txtBillingFirstName);
			billingDetails.put("type_Last Name_" + RandomStringUtils.randomAlphabetic(5), txtBillingLastName);
		}
		billingDetails.put("type_Address1_" + address.split("\\|")[0], txtBillingAddress);
		billingDetails.put("type_Address2_" + address.split("\\|")[1], txtBillingAddress2);
		billingDetails.put("type_Zipcode_" + address.split("\\|")[4], txtBillingZipcode);

		if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(btnBillingState)))){
			billingDetails.put("select_State_" + address.split("\\|")[3], btnBillingState);	
		}else
			billingDetails.put("select1_State_" + address.split("\\|")[3], btnBillingStateDiv);

		billingDetails.put("type_City_" + address.split("\\|")[2], txtBillingCity);

		billingDetails.put("type_PhoneNo_" + address.split("\\|")[5], txtBillingPhoneNo);

		BillingPageUtils.enterBillingDetails(billingDetails, driver);

		billingDetails.clear();
		billingDetails = getBillingInformation();
		return billingDetails;
	}

	/**
	 * To enter billing address as signed in user
	 * @param makeItDefault -
	 * @param saveThisAddress -
	 * @param billingInfo -
	 * @return Billing address details -
	 * @throws Exception -
	 */
	public LinkedHashMap<String, String> fillingBillingDetailsAsSignedUser(String makeItDefault, String saveThisAddress, String billingInfo)
			throws Exception {
		LinkedHashMap<String, String> billingDetails = new LinkedHashMap<String, String>();
		String address = checkoutProperty.getProperty(billingInfo);

		if(address.split("\\|").length > 7){
			billingDetails.put("type_First Name_" + address.split("\\|")[7], txtBillingFirstName);
			billingDetails.put("type_Last Name_" + address.split("\\|")[8], txtBillingLastName);
		}else{
			billingDetails.put("type_First Name_User" + RandomStringUtils.randomAlphabetic(5), txtBillingFirstName);
			billingDetails.put("type_Last Name_" + RandomStringUtils.randomAlphabetic(5), txtBillingLastName);
		}
		billingDetails.put("type_Address1_" + address.split("\\|")[0], txtBillingAddress);
		billingDetails.put("type_Address2_" + address.split("\\|")[1], txtBillingAddress2);
		billingDetails.put("type_Zipcode_" + address.split("\\|")[4], txtBillingZipcode);

		if(Utils.waitForElement(driver, drpBillingAddressState)){
			BrowserActions.clickOnElementX(drpBillingAddressState, driver, "State in Billing");
			BrowserActions.clickOnElementX(driver.findElement(By.xpath("//select[@id='dwfrm_billing_billingAddress_addressFields_states_state']//following-sibling::ul//li[contains(translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'"+address.split("\\|")[3].toLowerCase()+"')]")), driver, "State");
			Log.event("Selected State :: " + driver.findElement(By.cssSelector(btnBillingState)).getText());
		}

		if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(btnBillingState)))){
			Select drpState = new Select(driver.findElement(By.cssSelector(btnBillingState)));
			drpState.selectByVisibleText(address.split("\\|")[3]);
			Log.event("Selected State :: " + driver.findElement(By.cssSelector(btnBillingState)).getText());
		}

		billingDetails.put("type_City_" + address.split("\\|")[2], txtBillingCity);

		billingDetails.put("type_PhoneNo_" + address.split("\\|")[5], txtBillingPhoneNo);

		if(makeItDefault.equalsIgnoreCase("yes"))
			billingDetails.put("check_Make It Default_YES", chkBillingMakeItDefault);

		if(saveThisAddress.equalsIgnoreCase("yes"))
			billingDetails.put("check_Save This Address_YES", chkBillingSaveThisAddress);

		ShippingPageUtils.enterShippingDetails(billingDetails, driver);

		billingDetails.clear();
		billingDetails = getBillingInformation();
		return billingDetails;

	}

	/**
	 * To click on contine in POBox modal
	 * @throws Exception - Exception
	 */
	public void continuePOBoxValidation()throws Exception{
		if(Utils.waitForElement(driver, mdlPOBox)){
			Log.event("Clicking on contine in POBox modal");
			BrowserActions.clickOnElementX(btnContinueInPOBoxModal, driver, "Continue Button in POBox Modal");
		}else{
			Log.failsoft("POBox modal not displayed", driver);
		}
		Utils.waitForPageLoad(driver);
	}


	/**
	 * To get shipping information
	 * @return shipping information
	 * @throws Exception - Exception
	 */
	public LinkedHashMap<String, String> getShippingInformation()throws Exception{
		LinkedHashMap<String, String> shippingInfo = new LinkedHashMap<String, String>();

		shippingInfo.put("FirstName", getFirstName());
		shippingInfo.put("LastName", getLastName());
		shippingInfo.put("Address1", getAddressLine1());
		shippingInfo.put("Address2", getAddressLine2());
		shippingInfo.put("Zipcode", getZipcode());
		shippingInfo.put("State", getSelectedState());
		shippingInfo.put("City", getCity());
		shippingInfo.put("Country", getSelectedCountry());
		shippingInfo.put("PhoneNo", getPhoneNumber());

		return shippingInfo;
	}

	/**
	 * To get billing info
	 * @return billing information
	 * @throws Exception - Exception
	 */
	public LinkedHashMap<String, String> getBillingInformation()throws Exception{
		LinkedHashMap<String, String> billingInfo = new LinkedHashMap<String, String>();

		String firstName = BrowserActions.getText(driver, txtBillingFirstName, "First Name In Billing Address");
		String LastName = BrowserActions.getText(driver, txtBillingLastName, "Last Name In Billing Address");
		String address1 = BrowserActions.getText(driver, txtBillingAddress, "Address Line 1 In Billing Address");
		String address2 = BrowserActions.getText(driver, txtBillingAddress2, "Address Line 2 In Billing Address");
		String zipcode = BrowserActions.getText(driver, txtBillingZipcode, "Zip Code In Billing Address");
		String state = new String();
		String city = BrowserActions.getText(driver, txtBillingCity, "City In Billing Address");
		String country = new String();
		String phone = BrowserActions.getText(driver, txtBillingPhoneNo, "Phone Number In Billing Address");

		if(Utils.waitForElement(driver, drpStateBillingDetails))
			state = BrowserActions.getSelectedOption(drpStateBillingDetails);
		else
			state = BrowserActions.getText(driver, btnBillingStateDiv, "State Div"); 	

		if(Utils.waitForElement(driver, drpCountryBillingDetails))
			country = BrowserActions.getSelectedOption(drpCountryBillingDetails);
		else
			country = BrowserActions.getText(driver, btnBillingCountryDiv, "State Div"); 

		billingInfo.put("FirstName", firstName);
		billingInfo.put("LastName", LastName);
		billingInfo.put("Address1", address1);
		billingInfo.put("Address2", address2);
		billingInfo.put("Zipcode", zipcode);
		billingInfo.put("State", state);
		billingInfo.put("City", city);
		billingInfo.put("Country", country);
		billingInfo.put("PhoneNo", phone);

		return billingInfo;
	}

	/**
	 * To click on contine to payment
	 * @param checkuseasbillingadd -
	 * @throws Exception -
	 */
	public void continueToPayment(boolean checkuseasbillingadd) throws Exception{
		Utils.waitForPageLoad(driver);
		Log.event("Trying to continue to Payment Section.");
		boolean flag = false;
		if(checkuseasbillingadd)
			if(Utils.waitForElement(driver, chkUseThisAsBillingAddress)) {
				if(!BrowserActions.isRadioOrCheckBoxSelected(chkUseThisAsBillingAddress)) {
					BrowserActions.selectRadioOrCheckbox(chkUseThisAsBillingAddress, "YES", driver);
				}
			}

		if(Utils.waitForElement(driver, btnShippingContinue) ) {
			if(btnShippingContinue.getAttribute("disabled") == null){
				flag = false;
				BrowserActions.scrollInToView(btnShippingContinue, driver);
				BrowserActions.clickOnElementX(btnShippingContinue, driver, "Continue in Shipping");
			}else{
				flag = true;
			}
		}

		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver,btnselectPaymentMethod)){
			if(btnselectPaymentMethod.getAttribute("disabled") == null){
				flag=false;
				BrowserActions.scrollInToView(btnselectPaymentMethod, driver);
				BrowserActions.clickOnElementX(btnselectPaymentMethod, driver, "Select Payment Method");
			}else{
				flag = true;
			}
		}

		if(flag){
			Log.fail("Continue Button not enabled. Further Validations cannot be done.", driver);
		}
		Utils.waitForPageLoad(driver);
		Utils.waitUntilElementDisappear(driver, waitLoader);

		if(Utils.waitForElement(driver, mdlAddressSuggestion)){
			if(Utils.waitForElement(driver, btnContinueInAddressSuggestionModal))
				BrowserActions.clickOnElementX(btnContinueInAddressSuggestionModal, driver, "Continue Button in AVS");

			if(Utils.waitForElement(driver, btnCorrectAddress))
				BrowserActions.clickOnElementX(btnCorrectAddress, driver, "Continue Button in AVS");
		}
		Utils.waitUntilElementDisappear(driver, waitLoader);

		//Handling PLCC Modal
		while(getPLCCModalStatus())
			closePLCCOfferByNoThanks1();
	}
	
	/**
	* To verify PLCC contents are displayed
	* @return boolean - True if PLCC contents are displayed else false
	* @throws Exception
	*/
	public boolean verifyPLCCStep1ContentDisplayed() throws Exception {
		return elementLayer.verifyElementDisplayed(Arrays.asList("cntPLCCOffer", "lblPLCCCreditLimit", "cntOfferContent", "cntPreScreenNotice", "cntLegalCopy"), this);
	}

	/**
	 * To click on continue to payment
	 * @throws Exception - Exception
	 */
	public void continueToPayment() throws Exception{
		
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, txtFirstnameShipping)) {
			BrowserActions.scrollInToView(txtFirstnameShipping, driver);
			BrowserActions.clickOnElementX(txtFirstnameShipping, driver, "First Name in Shipping");
			txtFirstnameShipping.sendKeys(Keys.TAB);
		}else if(Utils.waitForElement(driver, txtFirstNameBillingDetails)){
			BrowserActions.scrollInToView(txtFirstNameBillingDetails, driver);
			BrowserActions.clickOnElementX(txtFirstNameBillingDetails, driver, "First Name in Billing");
			txtFirstNameBillingDetails.sendKeys(Keys.TAB);
		}

		Utils.waitForPageLoad(driver);
		Log.event("Trying to continue to Payment Section.");
		boolean flagContinueDisabled = false;

		if(Utils.waitForElement(driver, btnShippingContinue) ) {
			if(btnShippingContinue.getAttribute("disabled") == null){
				flagContinueDisabled = false;
				BrowserActions.scrollInToView(btnShippingContinue, driver);
				BrowserActions.clickOnElementX(btnShippingContinue, driver, "Continue in Shipping");
			}else{
				flagContinueDisabled = true;
			}
		}else if(Utils.waitForElement(driver,btnselectPaymentMethod)){
			if(btnselectPaymentMethod.getAttribute("disabled") == null){
				flagContinueDisabled = false;
				BrowserActions.scrollInToView(btnselectPaymentMethod, driver);
				BrowserActions.clickOnElementX(btnselectPaymentMethod, driver, "Select Payment Method");
			}else{
				flagContinueDisabled = true;
			}
		}

		if(flagContinueDisabled){
			Log.fail("Continue Button not enabled. Further Validations cannot be done.", driver);
		}
		Utils.waitForPageLoad(driver);
		Utils.waitUntilElementDisappear(driver, waitLoader);

		if(Utils.waitForElement(driver, mdlAddressSuggestion)){
			if(Utils.waitForElement(driver, rdoShipToOriginalAddress)) {
				BrowserActions.clickOnElementX(inpOrigAdrInSuggModal, driver, "Original Address in AVS");
			}
			if(Utils.waitForElement(driver, btnContinueInAddressSuggestionModal))
				BrowserActions.clickOnElementX(btnContinueInAddressSuggestionModal, driver, "Continue Button in AVS");
			else if(Utils.waitForElement(driver, btnThatsMyRightAddress))
				BrowserActions.clickOnElementX(btnThatsMyRightAddress, driver, "THAT'S THE RIGHT ADDRESS Button in AVS");
			Utils.waitUntilElementDisappear(driver, mdlAddressSuggestion);
		}
		
		if(Utils.waitForElement(driver, shippingmethodExceptionOverlay)){
			BrowserActions.clickOnElementX(divShipMedOverlayCont, driver, "Continue Button in AVS");
			Utils.waitUntilElementDisappear(driver, shippingmethodExceptionOverlay);
		}
		Utils.waitUntilElementDisappear(driver, waitLoader);
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on continue to payment
	 * @throws Exception - Exception
	 */
	public void continueToPaymentByChoosingOriginalAddress(boolean...choseOriginalAdrInSugModal) throws Exception{

		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, txtFirstnameShipping)) {
			BrowserActions.scrollInToView(txtFirstnameShipping, driver);
			BrowserActions.clickOnElementX(txtFirstnameShipping, driver, "First Name in Shipping");
			txtFirstnameShipping.sendKeys(Keys.TAB);
		}else if(Utils.waitForElement(driver, txtFirstNameBillingDetails)){
			BrowserActions.scrollInToView(txtFirstNameBillingDetails, driver);
			BrowserActions.clickOnElementX(txtFirstNameBillingDetails, driver, "First Name in Billing");
			txtFirstNameBillingDetails.sendKeys(Keys.TAB);
		}

		Utils.waitForPageLoad(driver);
		Log.event("Trying to continue to Payment Section.");
		boolean flag = false;

		if(Utils.waitForElement(driver, btnShippingContinue) ) {
			if(btnShippingContinue.getAttribute("disabled") == null){
				flag = false;
				BrowserActions.scrollInToView(btnShippingContinue, driver);
				BrowserActions.clickOnElementX(btnShippingContinue, driver, "Continue in Shipping");
			}else{
				flag = true;
			}
		}else if(Utils.waitForElement(driver,btnselectPaymentMethod)){
			if(btnselectPaymentMethod.getAttribute("disabled") == null){
				flag=false;
				BrowserActions.scrollInToView(btnselectPaymentMethod, driver);
				BrowserActions.clickOnElementX(btnselectPaymentMethod, driver, "Select Payment Method");
			}else{
				flag = true;
			}
		}

		if(flag){
			Log.fail("Continue Button not enabled. Further Validations cannot be done.", driver);
		}
		Utils.waitForPageLoad(driver);
		Utils.waitUntilElementDisappear(driver, waitLoader);

		if(Utils.waitForElement(driver, mdlAddressSuggestion)){
			if(choseOriginalAdrInSugModal.length > 0) {
				if(Utils.waitForElement(driver, btnContinueInAddressSuggestionModal) || Utils.waitForElement(driver, inpOrigAdrInSuggModal)){
					if(choseOriginalAdrInSugModal[0]) {
						BrowserActions.clickOnElementX(inpOrigAdrInSuggModal, driver, "Original address in ASM");
					}
				}
			}
			if(Utils.waitForElement(driver, btnContinueInAddressSuggestionModal))
				BrowserActions.clickOnElementX(btnContinueInAddressSuggestionModal, driver, "Continue Button in AVS");

			if(Utils.waitForElement(driver, btnCorrectAddress))
				BrowserActions.clickOnElementX(btnCorrectAddress, driver, "Continue Button in AVS");
			Utils.waitUntilElementDisappear(driver, mdlAddressSuggestion);
		}
		Utils.waitUntilElementDisappear(driver, waitLoader);
		Utils.waitForPageLoad(driver);


	}

	/**
	 * To click on contine to review order
	 * @throws Exception - Exception
	 */
	public void continueToReivewOrder()throws Exception{
		Log.event("Tying to click on continue in payment section");
		BrowserActions.scrollToViewElement(btnContinueToPlaceOrder, driver);
		BrowserActions.clickOnElementX(btnContinueToPlaceOrder, driver, "Btn Continue to Place Order");
		Utils.waitForPageLoad(driver);
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}

	/**
	 * To verify that the heading is displayed above the intro
	 * @return boolean -
	 * @throws Exception - Exception
	 */
	public Boolean verifyHeadingisAboveTheIntroInPoBoxExcludeOverlay() throws Exception {
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, txtPoBoxBlockOverlayHeading, txtPoBoxBlockOverlayIntro);
	}
	
	/**
	 * To verify that the heading is displayed above the intro
	 * @return boolean -
	 * @throws Exception - Exception
	 */
	public void clickOnContinueInShippingMethodExceptionOverlay() throws Exception {
		if (Utils.waitForElement(driver, divShipMedOverlayCont)){
			BrowserActions.clickOnElementX(divShipMedOverlayCont, driver, "Ship method overlay countinue");
			Utils.waitUntilElementDisappear(driver, divPoBoxBlockOverlay);
		}
	}

	/**
	 * To verify the vertical alignment of applied coupon codes
	 * @return boolean -
	 * @throws Exception - Exception
	 */
	public Boolean verifyAppliedCouponCodeLocations() throws Exception {
		BrowserActions.scrollInToView(appliedCoupon, driver);
		if(appliedCoupons.size() > 1) {
			WebElement elem1 = appliedCoupons.get(0);
			WebElement elem2 = appliedCoupons.get(1);
			return BrowserActions.verifyVerticalAllignmentOfElements(driver, elem1, elem2);
		}else {
			Log.failsoft("No more than one coupon applied for the validation.");
			return false;	
		}
	}

	/**
	 * To verify that the Place Order button is displayed above the Items In Bag
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPlaceOrderBtnisAboveItemsInBag() throws Exception {
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, btnPlaceOrder, txtItemsInBagHeader);
	}

	/**
	 * To verify that the intro is displayed above the items section
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyIntroIsAboveItemsSectionInPoBoxExcludeOverlay() throws Exception {
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, txtPoBoxBlockOverlayIntro, poBoxBlockOverlayItemsSection);
	}
	
	/**
	 * To continue to billing section from shipping
	 * @param chooseOriginalAddress - To choose original address from AVS suggestion modal(Optional) 
	 * @throws Exception - Exception
	 */
	public void continueToBilling(boolean... chooseOriginalAddress) throws Exception {
		BrowserActions.clickOnElementX(btnShippingContinue, driver, "Continue to billing");
		Utils.waitUntilElementDisappear(driver, waitLoader);
	
		if(Utils.waitForElement(driver, mdlAddressSuggestion)){
			Log.event("AVS modal is displayed.");
			
			if(chooseOriginalAddress.length > 0 && chooseOriginalAddress[0]) {
				if(Utils.waitForElement(driver, btnContinueInAddressSuggestionModal) || Utils.waitForElement(driver, inpOrigAdrInSuggModal)) {
					BrowserActions.clickOnElementX(inpOrigAdrInSuggModal, driver, "Original address in ASM");
				}
			}
			
			if(Utils.waitForElement(driver, btnContinueInAddressSuggestionModal)) {
				BrowserActions.clickOnElementX(btnContinueInAddressSuggestionModal, driver, "Continue button in AVS");
			} else if (Utils.waitForElement(driver, btnCorrectAddress)) {
				BrowserActions.clickOnElementX(btnCorrectAddress, driver, "Correct Address button in AVS");
			}
			Utils.waitUntilElementDisappear(driver, mdlAddressSuggestion);
		}
		Utils.waitUntilElementDisappear(driver, waitLoader);
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver, sectionBillingAddress);
	}
	
	/**
	 * To continue to payment section from shipping
	 * @throws Exception - Exception
	 */
	public void continueToPaymentFromBilling() throws Exception {
		BrowserActions.clickOnElementX(btnBillingContinue, driver, "Continue to payment");
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver, sectionPaymentCard);
	}
	
	/**
	 * To click on order summary continue button
	 * @throws Exception
	 */
	public void clickOrderSummaryContinue()throws Exception{
		BrowserActions.scrollToView(btnOrderSummaryContinue, driver);
		BrowserActions.clickOnElementX(btnOrderSummaryContinue, driver, "order summary continue");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on order summary continue button
	 * @throws Exception
	 */
	public void clickBlockSectionContinue()throws Exception{
		BrowserActions.scrollToView(btnContinueToPayment, driver);
		BrowserActions.clickOnElementX(btnContinueToPayment, driver, "order summary continue");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To verify that the items section is displayed above the remove button
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyItemsSectionIsAboveRemoveBtnInPoBoxExcludeOverlay() throws Exception {
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, poBoxBlockOverlayItemsSection, btnPoBoxBlockOverlayRemoveItems);
	}

	/**
	 * To verify the alignment of products in item section
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyItemsSectionProductsAlignmentInPoBoxExcludeOverlay() throws Exception {
		if(Utils.isMobile()){
			return BrowserActions.verifyVerticalAllignmentOfElements(driver, poBoxBlockOverlayProducts.get(0), poBoxBlockOverlayProducts.get(1));
		} else {
			return BrowserActions.verifyHorizontalAllignmentOfElements(driver, poBoxBlockOverlayProducts.get(1), poBoxBlockOverlayProducts.get(0));
		}
	}

	/**
	 * To get the num of products in PO box exclude overlay
	 * @return int
	 * @throws Exception - Exception
	 */
	public int getNoOfProductsInPoBoxExcludeOverlay() throws Exception {
		return poBoxBlockOverlayProducts.size();
	}

	/**
	 * To verify the alignment of remove and edit buttons
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPositionOfRemoveAndEditBtnInPoBoxOverlay() throws Exception {
		if(Utils.isMobile()){
			return BrowserActions.verifyVerticalAllignmentOfElements(driver, btnPoBoxBlockOverlayRemoveItems, btnPoBoxBlockOverlayEditShipAdd);
		} else {
			return BrowserActions.verifyHorizontalAllignmentOfElements(driver, btnPoBoxBlockOverlayEditShipAdd, btnPoBoxBlockOverlayRemoveItems);
		}
	}

	/**
	 * To verify alignment of edit and heading
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPositionOfItemsInBagEditAndHeading() throws Exception {
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, lnkItemsInBagEdit, txtItemsInBagHeading);
	}

	/**
	 * To verify the alignment of order summary and signe in section
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPositionOfOrderSummaryAndSignedInSection() throws Exception {
		if(Utils.isMobile()){
			return BrowserActions.verifyVerticalAllignmentOfElements(driver, signedInSection, checkoutOrderTotalTable);
		} else {
			return BrowserActions.verifyHorizontalAllignmentOfElements(driver, checkoutOrderTotalTable, signedInSection);
		}
	}

	/**
	 * To verify the gift message
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyGiftMessageTextComingFromProperty() throws Exception {
		String textToVerify = new String();
		if(!Utils.isMobile()) {
			textToVerify = lblFreeGiftMessage.getText();
		} else {
			textToVerify = lblFreeGiftMessage_mobile.getText();
		}
		String propertyText = demandWareProperty.getProperty("FreeGiftMsgLabel");
		if (textToVerify.equals(propertyText)) {
			return true;
		}
		return false;
	}

	/**
	 * To verify gift receipt text
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyGiftReceiptTextComingFromProperty() throws Exception {
		String textToVerify = lblFreeGiftReceipt.getText();
		String propertyText = demandWareProperty.getProperty("FreeGiftReceiptLabel");
		if (textToVerify.toLowerCase().trim().equals(propertyText.toLowerCase().trim())) {
			return true;
		}
		return false;
	}

	/**
	 * To verify gift heading text
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyGiftHeadingTextComingFromProperty() throws Exception {
		String textToVerify = lblGiftOptionHeading.getText();
		String propertyText = demandWareProperty.getProperty("IsGift");
		if (textToVerify.toLowerCase().trim().equals(propertyText.toLowerCase().trim())) {
			return true;
		}
		return false;
	}

	/**
	 * To verify gift message text
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyGiftMessage1TextComingFromProperty() throws Exception {
		String textToVerify = txtFormGiftMessage1.getAttribute("placeholder");
		String propertyText = demandWareProperty.getProperty("PlaceHolderMsg1");
		if (textToVerify.toLowerCase().trim().equals(propertyText.toLowerCase().trim())) {
			return true;
		}
		return false;
	}

	/**
	 * To verify gift message 2
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyGiftMessage2TextComingFromProperty() throws Exception {
		String textToVerify = txtFormGiftMessage2.getAttribute("placeholder");
		String propertyText = demandWareProperty.getProperty("PlaceHolderMsg2");
		if (textToVerify.toLowerCase().trim().equals(propertyText.toLowerCase().trim())) {
			return true;
		}
		return false;
	}

	/**
	 * To verify gift message 3
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyGiftMessage3TextComingFromProperty() throws Exception {
		String textToVerify = txtFormGiftMessage3.getAttribute("placeholder");
		String propertyText = demandWareProperty.getProperty("PlaceHolderMsg3");
		if (textToVerify.toLowerCase().trim().equals(propertyText.toLowerCase().trim())) {
			return true;
		}
		return false;
	}

	/**
	 * To verify heading text
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyHeadingTextComingFromPropertyInPoBoxOverlay() throws Exception {
		String textToVerify = txtPoBoxBlockOverlayHeading.getText();
		String propertyText = demandWareProperty.getProperty("PoBoxExcludeException");
		System.out.println("-- -- --- -- propertyText -- --  "+propertyText+" -- -- -- -- -- textToVerify -- -- "+textToVerify);
		if (textToVerify.toLowerCase().trim().equals(propertyText.toLowerCase().trim())) {
			return true;
		}
		return false;
	}

	/**
	 * To verify item intro text
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyItemIntroTextComingFromPropertyInPoBoxOverlay() throws Exception {
		String textToVerify = txtPoBoxBlockOverlayIntro.getText();
		String propertyText = demandWareProperty.getProperty("PoBoxExcludeItemIntro");
		if (textToVerify.toLowerCase().trim().equals(propertyText.toLowerCase().trim())) {
			return true;
		}
		return false;
	}

	/**
	 * To verify promo code expanded or not
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean checkPromoCodeSectionExpadedOrNot() throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		if(Utils.waitForElement(driver, expandedPromoCodeSection)) {
			return true;
		}
		return false;
	}

	/**
	 * To verify rewards section expanded or not
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean checkRewardSectionExpadedOrNot() throws Exception{
		if(Utils.waitForElement(driver, expandedRewardSection)) {
			return true;
		}
		return false;
	}

	/**
	 * To verify gift card section expanded or not
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean checkGiftCardSectionExpadedOrNot() throws Exception{
		if(Utils.waitForElement(driver, expandedGiftCardSection)) {
			return true;
		}
		return false;
	}

	/**
	 * To expanded or collapse promo code
	 * @param needToExpand -
	 * @throws Exception - Exception
	 */
	public void expandOrColapsePromoCodeSection(boolean needToExpand) throws Exception{
		if(needToExpand) {
			if(!(checkPromoCodeSectionExpadedOrNot())) {
				BrowserActions.scrollInToView(btnHvePromoCodeArrow, driver);
				BrowserActions.javascriptClick(btnHvePromoCodeArrow, driver, "Have a Promo Code Expand Arrow");
			}
		} else if(checkPromoCodeSectionExpadedOrNot()) {
			BrowserActions.scrollInToView(btnHvePromoCodeExpandedArrow, driver);
			BrowserActions.javascriptClick(btnHvePromoCodeExpandedArrow, driver, "Have a Promo Code Expand Arrow");
		}
	}

	/**
	 * To expanded or collapse gift card
	 * @param needToExpand - true for expand, false for collapse
	 * @throws Exception - Exception
	 */
	public void expandOrColapseGiftCardSection(boolean needToExpand) throws Exception{
		if(needToExpand) {
			BrowserActions.scrollToViewElement(giftCardSection, driver);
			if(!(checkGiftCardSectionExpadedOrNot())) {
				BrowserActions.javascriptClick(btnHveGiftCardArrow, driver, "Have a Promo Code Expand Arrow");
			}
		} else if(checkGiftCardSectionExpadedOrNot()) {
			BrowserActions.javascriptClick(btnHveGiftCardExpandedArrow, driver, "Have a Promo Code Expand Arrow");
		}
	}

	/**
	 * To Expand Gift Card Section
	 * @throws Exception - Exception
	 */
	public void expandGiftcard() throws Exception{
		if(Utils.waitForElement(driver, btnHveGiftCardArrowdown)) {
			BrowserActions.scrollToView(btnHveGiftCardArrowdown, driver);
			BrowserActions.javascriptClick(btnHveGiftCardArrow, driver, "Have a Promo Code Expand Arrow");
		}
	}

	/**
	 * To click on tool tip in Reward Section
	 * @throws Exception - Exception
	 */
	public void clickRewardSectionToolTip() throws Exception{
		BrowserActions.javascriptClick(btnRewardToolTip, driver, "Reward tooltip");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To remove the applied reward
	 * @throws Exception - Exception
	 */
	public void removeAppliedReward() throws Exception{
		if(Utils.waitForElement(driver, btnRewardRemove)) {
			BrowserActions.javascriptClick(btnRewardRemove, driver, "Remove reward button");
			Utils.waitForPageLoad(driver);
		} else {
			Log.event("No applied reward cetificate found.");
		}
	}

	/**
	 * To remove the applied gift card
	 * @throws Exception - Exception
	 */
	public void removeAppliedGiftCard() throws Exception{
		if(Utils.waitForElement(driver, lnkGiftCardRemove3)) {
			BrowserActions.javascriptClick(lnkGiftCardRemove3, driver, "Gift Card remove");
		} else if(!Utils.waitForElement(driver, lnkGiftCardRemove2)) {
			BrowserActions.javascriptClick(lnkGiftCardRemove, driver, "Gift Card remove");
		} else {
			BrowserActions.javascriptClick(lnkGiftCardRemove2, driver, "Gift Card remove");
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To close tool tip in reward section
	 * @throws Exception - Exception
	 */
	public void closeRewardSectionToolTip() throws Exception{
		BrowserActions.javascriptClick(btnRewardToolTipPopUpClose, driver, "Reward tooltip");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on tool tip in gift card section
	 * @throws Exception - Exception
	 */
	public void clickGiftCardSectionToolTip() throws Exception{
		BrowserActions.scrollInToView(btnGiftCardToolTip, driver);
		BrowserActions.javascriptClick(btnGiftCardToolTip, driver, "Gift card tooltip");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To close Gift card tool tip
	 * @throws Exception - Exception
	 */
	public void closeGiftCardSectionToolTip() throws Exception{
		BrowserActions.scrollInToView(lnkGiftCardToolTipClose, driver);
		BrowserActions.javascriptClick(lnkGiftCardToolTipClose, driver, "Gift card tooltip close");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To Expand/Collapse Reward Section
	 * @param needToExpand -
	 * @throws Exception - Exception
	 */
	public void expandOrColapseRewardSection(boolean needToExpand) throws Exception{
		if(needToExpand) {
			if(!(checkRewardSectionExpadedOrNot())) {
				BrowserActions.clickOnElementX(btnHveRewardArrow, driver, "Have a Reward Code Expand Arrow");
			}
		} else if(checkRewardSectionExpadedOrNot()) {
			BrowserActions.clickOnElementX(btnHveRewardExpandedArrow, driver, "Have a Reward Code Expand Arrow");
		}
	}

	/**
	 * To apply reward certificate in payment
	 * @param rewardCertificate -
	 * @throws Exception -
	 */
	public void applyRewardCertificate(String rewardCertificate) throws Exception{
		expandOrColapseRewardSection(true);
		BrowserActions.scrollInToView(txtRewardText, driver);
		BrowserActions.typeOnTextField(txtRewardText, rewardCertificate, driver, "Reward code");
		BrowserActions.scrollInToView(btnRewardApply, driver);
		BrowserActions.javascriptClick(btnRewardApply, driver, "'Reward' Apply button");
		Utils.waitUntilElementDisappear(driver, waitLoader);
		if(Utils.waitForElement(driver, txtRewardError)) {
			String errorMessage = BrowserActions.getText(driver, txtRewardError, "Reward Certificate error message");
			int retryCount = 0;
			while(errorMessage.toLowerCase().contains("Please renter and Submit".toLowerCase()) && (retryCount < 3)) {
				BrowserActions.javascriptClick(btnRewardApply, driver, "'Reward' Apply button again");
				Utils.waitUntilElementDisappear(driver, waitLoader);
				retryCount++;
				if(!Utils.waitForElement(driver, txtRewardError)) {
					return;
				}
				errorMessage = BrowserActions.getText(driver, txtRewardError, "Reward Certificate error message");
			}
			Log.failsoft("Invalid Reward Certificate: " + errorMessage, driver);
		}
	}
	
	/**
	 * To type reward certificate in payment
	 * @param rewardCertificate -
	 * @throws Exception -
	 */
	public void typeOnRewardTxtbox(String rewardCertificate) throws Exception{
		expandOrColapseRewardSection(true);
		BrowserActions.scrollInToView(txtRewardText, driver);
		BrowserActions.typeOnTextField(txtRewardText, rewardCertificate, driver, "Reward code");
	}
	
	/**
	 * To click reward certificate buttons in payment
	 * @throws Exception -
	 */
	public void clickOnRewardTxtbox() throws Exception{
		BrowserActions.scrollInToView(btnRewardApply, driver);
		BrowserActions.javascriptClick(btnRewardApply, driver, "'Reward' Apply button");
		Utils.waitForElement(driver, txtRewardError);
	}
	
	/**
	 * To try and enter gift card of given value until valid data is found or exhausted
	 * @param giftCard - Value of card data to use 
	 * @throws Exception
	 */
	public void applyGiftCardByValue(TestGiftCardValue giftCard) throws Exception{
		boolean lastCard = false, validData = false;
		int index = 1;
		while(!lastCard && !validData) {
			String giftCardKey = "gift_card_" + giftCard + "_" + index;
			if(TestData.get(giftCardKey) != null) {
				String[] giftCardData = TestData.get(giftCardKey).split("\\|");
				Log.event("Entering card :: " + giftCardKey);
				applyGiftCardNumbAndPin(giftCardData[0], giftCardData[1]);
				if(!Utils.waitForElement(driver, appliedGiftCardNumber)) {
					index ++;
				} else {
					validData = true;
					break;
				}
			} else {
				lastCard = true;
				if(Utils.waitForElement(driver, lnkInvalidGiftCardError)) {
					String errorMessage = BrowserActions.getText(driver, lnkInvalidGiftCardError, "Gift Card error message");
					if(errorMessage.trim().length() > 0) {
						Log.failsoft("Gift Card error displayed :: " + errorMessage, driver);
					}
				}
				break;
			}
		}
	}

	/**
	 * To apply gift card in payment section
	 * @param GiftCardNumb -
	 * @param PinNumb -
	 * @throws Exception -
	 */
	public void applyGiftCardNumbAndPin(String GiftCardNumb, String PinNumb) throws Exception{
		expandOrColapseGiftCardSection(true);
		BrowserActions.scrollInToView(txtGiftCardNumb, driver);
		BrowserActions.typeOnTextField(txtGiftCardNumb, GiftCardNumb, driver, "Gift Card number");

		BrowserActions.scrollInToView(txtGiftCardPin, driver);
		BrowserActions.typeOnTextField(txtGiftCardPin, PinNumb, driver, "Gift Card pin");

		BrowserActions.scrollInToView(btnGiftCardApply, driver);
		BrowserActions.javascriptClick(btnGiftCardApply, driver, "'Gift Card' Apply button");
		
		Utils.waitUntilElementDisappear(driver, waitLoader);
		Utils.waitForPageLoad(driver);		
	}

	/**
	 * To click on Apply Gift card button
	 * @throws Exception - Exception
	 */
	public void clickApplyGiftCard() throws Exception{
		Utils.waitForPageLoad(driver);
		BrowserActions.scrollInToView(btnGiftCardApply, driver);
		BrowserActions.javascriptClick(btnGiftCardApply, driver, "'Gift Card' Apply button");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on Apply Gift card button
	 * @throws Exception - Exception
	 */
	public float getRemainingOrderAmount() throws Exception{
		String value = "0.0";
		if (Utils.waitForElement(driver, txtRemainingOrderAmount)) {
			value = BrowserActions.getText(driver, txtRemainingOrderAmount, "Remaining Order Amount").replace("$","").trim();
		} else {
			value = getOrderSummaryTotal();
		}
		return (float) StringUtils.getPriceFromString(value);
	}


	/**
	 * To get Gift card Error message
	 * @throws Exception - Exception
	 * @return string
	 */
	public String getGiftCardErrorMessage() throws Exception{
		return BrowserActions.getText(driver, lnkInvalidGiftCardError, "GiftCard Error");
	}

	/**
	 * To clear Gift card and Pin number Fields
	 * @throws Exception - Exception
	 */
	public void clearGiftCardNumbAndPin() throws Exception{
		BrowserActions.scrollInToView(txtGiftCardNumb, driver);
		BrowserActions.clearTextField(txtGiftCardNumb, "Gift Card Number");

		BrowserActions.scrollInToView(txtGiftCardPin, driver);
		BrowserActions.clearTextField(txtGiftCardPin, "Gift Card pin");

		Utils.waitForPageLoad(driver);
	}

	/**
	 * To Fill card Details in Payments
	 * @param SaveToPaymentMethod -
	 * @param customerDetails -
	 * @param iteration -
	 * @return LinkedHasMap -
	 * @throws Exception -Exception
	 */
	public LinkedHashMap<String, String> fillingCardDetails(String SaveToPaymentMethod, String customerDetails,String... iteration) throws Exception {

		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> cardDetails = new LinkedHashMap<String, String>();
		String cardExpireMonth = "";
		String cardExpireYear = "";
		String cardCVN = "";
		String cardOwner = "";
		String cutomercardDetails = checkoutProperty.getProperty(customerDetails);
		String cardNo = "";
		String cardType = cutomercardDetails.split("\\|")[0];
		
		if(cardType.contains("Credit Card")) {
			cardNo = cutomercardDetails.split("\\|")[1];
		} else {
			cardOwner = cutomercardDetails.split("\\|")[1];
			cardNo = cutomercardDetails.split("\\|")[2];
			cardExpireMonth = cutomercardDetails.split("\\|")[3];
			cardExpireYear = cutomercardDetails.split("\\|")[4];
			cardCVN = cutomercardDetails.split("\\|")[5];
		}

		if(!Utils.waitForElement(driver, fldTxtNameOnCard)){
			cardDetails.put("type_cvnno_" + cardCVN, txtCvnNoForSavedCard);
		}else{
			if (iteration.length > 0) {
				cardDetails.put("select1_cardtype_" + cardType, btnCardType);

			} else {			
				cardDetails.put("select_cardtype_" + cardType, btnCardType);
			}
			if(cardType.contains("Credit Card")) {
				cardDetails.put("type_cardno_" + cardNo, txtCardNo);
			} else {
				cardDetails.put("type_cardname_" + cardOwner, txtNameOnCard);
				cardDetails.put("type_cardno_" + cardNo, txtCardNo);
				cardDetails.put("select_expmonth_" + cardExpireMonth, btnCardExpMonth);
				cardDetails.put("select_expyr_" + cardExpireYear, btnCardExpYear);
				cardDetails.put("type_cvnno_" + cardCVN, txtCvnNo);
			}
		}

		BillingPageUtils.enterBillingDetails(cardDetails, driver);
		Log.event("Filled Card Details", StopWatch.elapsedTime(startTime));
		Actions action = new Actions(driver);
		action.sendKeys(Keys.TAB).build().perform();
		return cardDetails;
	}

	/**
	 * To click on Save Address Button in Address Section
	 * @throws Exception - Exception
	 */
	public void clickOnSaveAddrBtn() throws Exception {
		if(Utils.waitForElement(driver, btnSaveAddr)) {
			Utils.waitForPageLoad(driver);
			BrowserActions.scrollInToView(btnSaveAddr, driver);
			BrowserActions.clickOnElementX(btnSaveAddr, driver, "Continue");
		}
		
	}

	/**
	 * To Click on continue in Payment Section
	 * @throws Exception - Exception
	 */
	public void clickOnPaymentDetailsContinueBtn() throws Exception {
		if(Utils.waitForElement(driver, btnPaymentDetailsContinue)) {
			BrowserActions.scrollInToView(btnPaymentDetailsContinue, driver);
			BrowserActions.clickOnElementX(btnPaymentDetailsContinue, driver, "Continue");
			Utils.waitForPageLoad(driver);
			Utils.waitUntilElementDisappear(driver, waitLoader);
		}
	}

	/**
	 * To verify Promotion Error message is not clickable
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean checkPromoErrorIsNotClickable() throws Exception{
		WebDriverWait wait = new WebDriverWait(driver, 10);
		if(wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".couponsection.tab-section .error-messages.hide-mobile .error:not(.rw-error)"))) != null) {
			return false;
		}
		return true;
	}

	/**
	 * Enter text in the field card name
	 * @param txtToType
	 * @throws Exception
	 */
	public void enterTextInCardNameField(String txtToType) throws Exception{
		BrowserActions.clearTextField(fldTxtNameOnCard, "Name on card");
		BrowserActions.typeOnTextField(fldTxtNameOnCard, txtToType, driver, "Name on card");
	}

	/**
	 * Enter text in the field card cvv
	 * @param txtToType
	 * @throws Exception
	 */
	public void enterTextInCardCvvField(String txtToType) throws Exception{
		WebElement elem = null;
		if(Utils.waitForElement(driver, fldTxtCvvNo)) {
			elem = fldTxtCvvNo;
		} else {
			elem = fldTxtCvv;
		}
		BrowserActions.clearTextField(elem, "Cvv field");
		BrowserActions.typeOnTextField(elem, txtToType, driver, "Cvv field");
	}

	/**
	 * Enter text in the field card number
	 * @param txtToType
	 * @throws Exception
	 */
	public void enterTextInCardNumberField(String txtToType) throws Exception{
		BrowserActions.clearTextField(fldTxtCardNo, "Card Number");
		BrowserActions.typeOnTextField(fldTxtCardNo, txtToType, driver, "Card Number");
	}
	
	/**
	 * verify expiry month selected is displayed in the payment section
	 * @param month - Month to verify
	 * @throws Exception - Exception
	 */
	public boolean verifySelectedMonthDisplayedInExpiryMonth(String month) throws Exception{
		String text = BrowserActions.getText(driver, selectedExpMonth, "Drop down Exp month");
		
		if (text.toLowerCase().contains(month.toLowerCase())) {
			return true;
		}
		return false;
	}

	/**
	 * To set an expired date 
	 * @param expiredMonth - (Optional) Set value to older month.
	 * @return boolean
	 * @throws Exception
	 */
	public boolean setExpiredDateInPayment(String... expiredMonth) throws Exception{
		Map<String, String> currentDate = Utils.getCurrentDateDetails();
		if(currentDate.get("month").equalsIgnoreCase("0")) {
			Log.event("Cannot set expired date when testing in january.");
			return false;
		}
		selectExpiryYear(currentDate.get("year"));
		if(expiredMonth.length == 0) {
			selectExpiryMonth("January");
		} else if(expiredMonth.length == 1) {
			selectExpiryMonth(expiredMonth[0]);
		}
		return true;
	}

	/**
	 * To verify Error message in reward section is not clickable
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean checkRewardErrorIsNotClickable() throws Exception{
		WebDriverWait wait = new WebDriverWait(driver, 10);
		if(wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".rw-error"))) != null) {
			return false;
		}
		return true;
	}

	/**
	 * To type coupon code in PromoCode Fields
	 * @param couponCode -
	 * @throws Exception - Exception
	 */
	public void enterPromoCouponCode(String couponCode) throws Exception{
		if(Utils.waitForElement(driver, txtCouponCode_focus)) {
			BrowserActions.javascriptClick(txtCouponCode_focus, driver, "'PromoCode' text box focus");
		}
		BrowserActions.typeOnTextField(txtCouponCode, couponCode, driver, "Promo Coupon code");
		if(Utils.waitForElement(driver, couponErrorMessage) || Utils.waitForElement(driver, couponErrorMessage_Mobile)) {
			Log.failsoft("Coupon error message displayed.", driver);
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To clear content in Promo Code Fields
	 * @throws Exception - Exception
	 */
	public void clearPromoCouponCode() throws Exception{
		if(Utils.waitForElement(driver, txtCouponCode_focus)) {
			BrowserActions.javascriptClick(txtCouponCode_focus, driver, "'PromoCode' text box focus");
		}
		BrowserActions.clearTextField(txtCouponCode, "Promo Coupon code");
		if(Utils.waitForElement(driver, txtCouponCode_focus)) {
			BrowserActions.javascriptClick(txtCouponCode_focus, driver, "'PromoCode' text box focus");
		}
	}

	/**
	 * To clear promocode field content by sending backspace keys
	 * @param times -
	 * @throws Exception -
	 */
	public void clearPromoCodeByBackSpace(int times) throws Exception{
		if(Utils.waitForElement(driver, txtCouponCode_focus)) {
			BrowserActions.javascriptClick(txtCouponCode_focus, driver, "'PromoCode' text box focus");
		}
		for(int i=0;i<times;i++) {
			txtCouponCode.sendKeys(Keys.BACK_SPACE);
		}
		if(Utils.waitForElement(driver, txtCouponCode_focus)) {
			BrowserActions.javascriptClick(txtCouponCode_focus, driver, "'PromoCode' text box focus");
		}
	}

	/**
	 * To click on edit link in Shipping Address section
	 * @throws Exception - Exception
	 */
	public void EditShippingAddress()throws Exception{
		if(Utils.waitForElement(driver, lnkEditShippingAddress)) {
			BrowserActions.clickOnElementX(lnkEditShippingAddress, driver, "Edit");
		}
		Utils.waitForElement(driver, chkGiftMessage);
	}

	/**
	 * To click on Apply button in PromoCode Section
	 * @throws Exception - Exception
	 */
	public void clickApplyPromoCouponCode() throws Exception{
		BrowserActions.javascriptClick(btnCouponApply, driver, "'PromoCode' Apply button");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To verify promocode field is empty
	 * @throws Exception - Exception
	 * @return boolean
	 */
	public boolean checkPromoCouponCodeTextFieldIsEmpty() throws Exception{
		String txt = txtCouponCode.getAttribute("value");
		if(txt.isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * To apply promocode in payment section
	 * @param couponCode -
	 * @throws Exception -
	 */
	public void applyPromoCouponCode(String couponCode) throws Exception{
		expandOrColapsePromoCodeSection(true);
		BrowserActions.scrollInToView(txtCouponCode, driver);
		BrowserActions.typeOnTextField(txtCouponCode, couponCode, driver, "Promo Coupon code");
		BrowserActions.scrollInToView(btnCouponApply, driver);

		if(Utils.getRunBrowser(driver).toLowerCase().contains("edge")) {
			BrowserActions.clickOnElementX(btnCouponApply, driver, "'PromoCode' Apply button");
		} else {
			BrowserActions.javascriptClick(btnCouponApply, driver, "'PromoCode' Apply button");
		}

		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on Place Order Button
	 * @return OrderConfirmationPage - Order confirmation page object
	 * @throws Exception - Exception
	 */
	public OrderConfirmationPage clickOnPlaceOrderButton() throws Exception{
		if(Utils.waitForElement(driver, btnPlaceOrder, 20)) {
			BrowserActions.scrollInToView(btnPlaceOrder, driver);
			BrowserActions.clickOnElementX(btnPlaceOrder, driver, "Place Order");
			Utils.waitForPageLoad(driver);

			if (Utils.waitForElement(driver,OrderConfirmationreadyElement)) {
				OrderConfirmationPage orderConfPg = new OrderConfirmationPage(driver).get();
				Log.message("Order number " + orderConfPg.getOrderNumber() + " has been placed.");
				return orderConfPg;
			} else if (Utils.waitForElement(driver, error)) {
				Log.fail("Unable to Place Order due to '" + error.getText() + "'", driver);
			} else {
				Log.fail("Something went wrong.", driver);
			}
		}
		return null;
	}
	
	/**
	 * To click on Place Order Button Under Order Summary Section 
	 * @return OrderConfirmationPage - Order confirmation page object
	 * @throws Exception - Exception
	 */
	public OrderConfirmationPage clickOnOrderSummaryPlaceOrderButton() throws Exception{
		if(Utils.waitForElement(driver, btnPlaceOrderRight, 20)) {
			BrowserActions.scrollInToView(btnPlaceOrderRight, driver);
			BrowserActions.clickOnElementX(btnPlaceOrderRight, driver, "Place Order");
			Utils.waitForPageLoad(driver);
		}
		
		if (Utils.waitForElement(driver,OrderConfirmationreadyElement)) {
			Log.event("Order placed.");
			return new OrderConfirmationPage(driver).get();
		} else {
			if(Utils.waitForElement(driver, error)){
				Log.failsoft("Unable to Place Order due to " + error.getText(), driver);
			} else {
				Log.failsoft("Something went wrong.", driver);
			}
			return null;
		}
	}
	
	/**
	 * To click on Place Order Button
	 * @throws Exception - Exception
	 */
	public void clickOnPlaceOrder() throws Exception{
		WebElement elem = null;
		if(Utils.waitForElement(driver, btnPlaceOrder, 20)) {
			elem = btnPlaceOrder;
		} else {
			elem = btnPlaceOrderInOrderSummary;
		}
			BrowserActions.scrollToView(elem, driver);
			BrowserActions.javascriptClick(elem, driver, "Place Order");
			Utils.waitForPageLoad(driver);
	}
	

	/**
	 * To click on Place Order button in Summary Section
	 * @throws Exception - Exception
	 */
	public OrderConfirmationPage clickOnPlaceOrderButtonInRightSideOrderSummary() throws Exception{
		Utils.waitForPageLoad(driver);
		BrowserActions.scrollInToView(btnPlaceOrderInOrderSummary, driver);
		BrowserActions.clickOnElementX(btnPlaceOrderInOrderSummary, driver, "Place Order");
		Utils.waitForPageLoad(driver);
		return new OrderConfirmationPage(driver).get();
	}

	/**
	 * To get Order Confirmation Message
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getOrderConfirmationMessage() throws Exception{
		return BrowserActions.getText(driver, txtOrderConfirmationMsg, "Order Confirmation message");
	}

	/**
	 * To Fill Shipping Fields with User given Shipping address as Authenticated USer
	 * @param saveToAddressBook -
	 * @param useAddressForBilling -
	 * @param shippingmethod -
	 * @param customerDetails -
	 * @return LinkedHashMap -
	 * @throws Exception -
	 */
	public LinkedHashMap<String, String> fillingShippingDetailsAsSignedInUser(
			String saveToAddressBook, String useAddressForBilling, ShippingMethod shippingMethod, String customerDetails) throws Exception {

		final long startTime = StopWatch.startTime();
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();

		String randomNickName = RandomStringUtils.randomAlphabetic(5).toLowerCase();

		shippingDetails.put("type_Address_Nick" + randomNickName,txtShippingNickName);
		String randomFirstName = RandomStringUtils.randomAlphabetic(5).toLowerCase();
		shippingDetails.put("type_firstname_QaFirst" + randomFirstName,txtShippingFirstName);
		String randomLastName = RandomStringUtils.randomAlphabetic(5).toLowerCase();
		shippingDetails.put("type_lastname_QaLast" + randomLastName, txtShippingLastName);

		String address = checkoutProperty.getProperty(customerDetails);
		String address1 = address.split("\\|")[0];
		String address2 = address.split("\\|")[1];
		String zipcode = address.split("\\|")[4];
		String state = address.split("\\|")[3];
		String city = address.split("\\|")[2];	
		String phoneNo = address.split("\\|")[5];

		if (customerDetails.contains("_1")) {
			address2 = address.split("\\|")[1];
			city = address.split("\\|")[2];
			state = address.split("\\|")[3];
			zipcode = address.split("\\|")[4];
			phoneNo = address.split("\\|")[5];
		}
		
		if(address.split("\\|").length > 7) {
			String firstName = address.split("\\|")[7];
			String lastName = address.split("\\|")[8];
			shippingDetails.put("type_firstname_" + firstName, txtShippingFirstName);
			shippingDetails.put("type_lastname_" + lastName, txtShippingLastName);
		}
		
		shippingDetails.put("type_address_" + address1, txtShippingAddress);
		if (customerDetails.contains("_1"))
			shippingDetails.put("type_address1_" + address2, txtShippingAddress2);
		
		shippingDetails.put("type_address2_" + address2, txtShippingAddress2);

		shippingDetails.put("type_zipcode_" + zipcode, txtShippingZipcode);	

		if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(btnShippingStateDiv)))){
			shippingDetails.put("select1_state_" + state, btnShippingStateDiv);	
		}else
			shippingDetails.put("select_state_" + state, btnShippingState);	

		shippingDetails.put("type_city_" + city, txtShippingCity);		
		shippingDetails.put("type_phoneno_" + phoneNo, txtShippingPhoneNo);

		if (shippingMethod != null) {
			switch (shippingMethod) {
			case Standard:
				shippingDetails.put("check_Shipping method_YES", rdoStandard);
				break;
			case Express:
				shippingDetails.put("check_Shipping method_YES", rdoExpress);
				break;
			case SuperFast:
				shippingDetails.put("check_Shipping method_YES", rdoSuperFast);
				break;
			case NextDay:
				shippingDetails.put("check_Shipping method_YES", rdoNextDay);
				break;
			case HeathersFixed:
				shippingDetails.put("check_Shipping method_YES", rdoHeathers);
				break;
			}
		}
		
		if(saveToAddressBook.equalsIgnoreCase("YES"))
			checkUncheckSaveThisAddress(true);
		else
			checkUncheckSaveThisAddress(false);
		
		if(useAddressForBilling.equalsIgnoreCase("yes")) {
			checkUncheckUseThisAsBillingAddress(true);
		} else {
			checkUncheckUseThisAsBillingAddress(false);
		}
			
		ShippingPageUtils.enterShippingDetails(shippingDetails, driver);
		shippingDetails.remove("check_Shipping method_YES");

		Log.event("Filled Shipping Details", StopWatch.elapsedTime(startTime));

		return shippingDetails;

	}
	/**
	 * To Fill Shipping Fields with User given Shipping address as Authenticated USer
	 * @param makeItDefault -
	 * @param saveThisAddress -
	 * @param useAddressForBilling -
	 * @param shippingMethod -
	 * @param shippingInfo -
	 * @param freeGift -
	 * @return -
	 * @throws Exception -
	 */
	public LinkedHashMap<String, String> fillingShippingDetailsAsSignedInUser(
			String makeItDefault, String saveThisAddress, String useAddressForBilling, ShippingMethod shippingMethod, String shippingInfo, String... freeGift) throws Exception {
		Utils.waitForElement(driver, txtShippingFirstNameFld);
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		String address = checkoutProperty.getProperty(shippingInfo);

		shippingDetails.put("type_NickName_Test", txtShippingNickName);
		shippingDetails.put("type_First Name_" + "JOHN", txtShippingFirstName);
		shippingDetails.put("type_Last Name_" + "GKPNGFF", txtShippingLastName);
		shippingDetails.put("type_Address1_" + address.split("\\|")[0], txtShippingAddress);
		shippingDetails.put("type_Address2_" + address.split("\\|")[1], txtShippingAddress2);
		shippingDetails.put("type_Zipcode_" + address.split("\\|")[4], txtShippingZipcode);

		//Array indexes updated to match with corrsponding pipe separated fields on valid_addresses on checkout.properties
		//This method called by Global Navigation uses one specific valid_address 
		if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(btnShippingState)))){
			shippingDetails.put("select_State_" + address.split("\\|")[3], btnShippingState);	
		}else
			shippingDetails.put("select1_State_" + address.split("\\|")[3], btnShippingStateDiv);

		shippingDetails.put("type_City_" + address.split("\\|")[2], txtShippingCity);

		shippingDetails.put("type_PhoneNo_" + address.split("\\|")[5], txtShippingPhoneNo);

		if(makeItDefault.equalsIgnoreCase("yes"))
			shippingDetails.put("check_Make It Default_YES", chkShippingMakeItDefault);

		if(saveThisAddress.equalsIgnoreCase("yes"))
			shippingDetails.put("check_Save This Address_YES", chkShippingSaveThisAddress);

		if(useAddressForBilling.equalsIgnoreCase("yes"))
			shippingDetails.put("check_Use Address For Billing_YES", chkUseAddressForBilling);

		if(useAddressForBilling.equalsIgnoreCase("no"))
			shippingDetails.put("check_Use Address For Billing_NO", chkUseAddressForBilling);

		if (shippingMethod != null) {
			switch (shippingMethod) {
			case Standard:
				shippingDetails.put("check_Shipping method_YES", rdoStandard);
				break;
			case Express:
				shippingDetails.put("check_Shipping method_YES", rdoExpress);
				break;
			case SuperFast:
				shippingDetails.put("check_Shipping method_YES", rdoSuperFast);
				break;
			case NextDay:
				shippingDetails.put("check_Shipping method_YES", rdoNextDay);
				break;
			case HeathersFixed:
				shippingDetails.put("check_Shipping method_YES", rdoHeathers);
				break;
			}
		}

		if(freeGift.length > 0){
			BrowserActions.javascriptClick(giftMessageDesktop, driver, "Gift Message Check Box");
			if(freeGift[0].contains("|")){
				String[] freeGiftMessage = freeGift[0].split("\\|")[0].split("_");
				if(freeGiftMessage.length > 0)
					shippingDetails.put("type_Free Gift Message1_" + freeGiftMessage[0], txtFreeGiftMessage1);

				if(freeGiftMessage.length > 1)
					shippingDetails.put("type_Free Gift Message2_" + freeGiftMessage[1], txtFreeGiftMessage2);

				if(freeGiftMessage.length > 2)
					shippingDetails.put("type_Free Gift Message3_" + freeGiftMessage[2], txtFreeGiftMessage3);
			}else{
				shippingDetails.put("type_Free Gift Message1_" + freeGift[0], txtFreeGiftMessage1);
			}
		}

		if(!Utils.waitForElement(driver, txtShippingFirstNameFld)){
			BrowserActions.scrollToView(btnAddNewShippingAddress, driver);
			BrowserActions.clickOnElementX(btnAddNewShippingAddress, driver, "Add New Shipping Address");
		}

		while(getPLCCModalStatus())
			closePLCCOfferByNoThanks1();

		ShippingPageUtils.enterShippingDetails(shippingDetails, driver);

		shippingDetails.clear();
		shippingDetails = getShippingInformation();
		return shippingDetails;

	}
	
	/**
	 * To Compare billing address and the shipping address are same
	 * @param shippingInfo - address property
	 * @return boolean - boolean
	 * @throws Exception - Exception
	 */
	public boolean compareBillingAddressWithEnteredShippingAddress(String shippingInfo) throws Exception {
		Utils.waitForElement(driver, txtShippingFirstNameFld);
		String address = checkoutProperty.getProperty(shippingInfo);
		BrowserActions.scrollInToView(lblBillingAddressSelected, driver);
		String billAdd = BrowserActions.getText(driver, lblBillingAddressSelected, "Billing Address");
		if(billAdd.contains("|")) {
			billAdd = billAdd.split("\\|")[1];
		}
		return elementLayer.verifyTwoAddressMatch(address, billAdd);
	}

	/**
	 * To compare payment method before ordering with entered payment method
	 * @param payment - payment property
	 * @return boolean - boolean
	 * @throws Exception - Exception
	 */
	public boolean comparePaymentBeforeOrderWithEnteredPaymentMethod(String payment) throws Exception {
		Utils.waitForElement(driver, txtShippingFirstNameFld);
		String paymentInfos = checkoutProperty.getProperty(payment);
		
		BrowserActions.scrollInToView(lblEnteredCardOwnerName, driver);
		
		String cardNo = BrowserActions.getText(driver, lblEnteredCardNo, "Card No").trim();
		String expiry = BrowserActions.getText(driver, lblEnteredExpiryDate, "Expiry date").trim();
		String cardOwnerName = BrowserActions.getText(driver, lblEnteredCardOwnerName, "Card Owner Name");
		
		String[] paymentInfo = paymentInfos.split("\\|");
		
		if(!(paymentInfo[1].toLowerCase().trim().contains(cardOwnerName.toLowerCase().trim()))) {
			return false;
		}
		
		if(!(cardNo.trim().contains(paymentInfo[2].trim().substring(13)))) {
			return false;
		}
		
		if(!(expiry.trim().contains(paymentInfo[4].trim()))) {
			return false;
		}
		
		return true;
	}

	/**
	 * To change the product Quantity
	 * @param quantity -
	 * @throws Exception - Exception
	 */
	public void changeProductQuantity(int quantity )throws Exception{

		Utils.waitForElement(driver, divItemQuantity);


		while(Integer.parseInt(txtItemQuantity.getAttribute("value"))==quantity){

			if(Integer.parseInt(txtItemQuantity.getAttribute("value"))<quantity){
				BrowserActions.clickOnElementX(arrowQtyDecrease, driver, "Decrease Product Quantity");
			}else{
				BrowserActions.clickOnElementX(arrowQtyIncrease, driver, "Increase Product Quantity");
			}

			Utils.waitForPageLoad(driver);
		}



	}

	/**
	 * To type text in nick name field
	 * @param nickname -
	 * @throws Exception - Exception
	 */
	public void typeTextInNickNameField(String nickname)throws Exception{
		BrowserActions.typeOnTextField(txtNickName, nickname, driver, "Nick name");
	}

	/**
	 * To type text in first name field
	 * @param firstname -
	 * @throws Exception - Exception
	 */
	public void typeTextInFirstNameField(String firstname)throws Exception{
		BrowserActions.typeOnTextField(txtFirstName, firstname, driver, "first name");
	}

	/**
	 * to verify Place holder For First name field
	 * @return -
	 * @throws Exception -
	 */
	public Boolean verifyPlaceHolderForFirstNameField()throws Exception{
		if(lblFirstNamePlaceHolder.getText().trim().equals("First Name"))
			return true;
		else
			return false;
	}

	/**
	 * to verify Place holder For last name field
	 * @param lastname -
	 * @throws Exception -
	 */
	public void typeTextInLastNameField(String lastname)throws Exception{
		BrowserActions.typeOnTextField(txtLastName, lastname, driver, "last name");
	}

	/**To verify Last name place holder label
	 * @return - boolean
	 * @throws Exception -
	 */
	public Boolean verifyPlaceHolderForLastNameField()throws Exception{
		if(lblLastNamePlaceHolder.getText().trim().equals("Last Name"))
			return true;
		else
			return false;
	}
	/**
	 * To verify gift receipt checkbox is checked
	 * @return - boolean
	 * @throws Exception -
	 */
	public Boolean verifyGiftReceiptCheckBoxIsChecked()throws Exception{
		return chkGiftReceiptCheckBox.isSelected();
	}

	/**
	 * To verify Gift Receipt Checkbox in Shipping Address section is clickable
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyGiftReceiptCheckBoxIsClickable()throws Exception{
		if(chkGiftReceiptCheckBox.isSelected()) {
			BrowserActions.clickOnElementX(chkGiftReceiptCheckBox, driver, "Gift receipt checkbox");
			if(!(chkGiftReceiptCheckBox.isSelected())) {
				BrowserActions.clickOnElementX(chkGiftReceiptCheckBox, driver, "Gift receipt checkbox");
				return true;
			}
		} else if(!chkGiftReceiptCheckBox.isSelected()) {
			BrowserActions.clickOnElementX(chkGiftReceiptCheckBox, driver, "Gift receipt checkbox");
			if((chkGiftReceiptCheckBox.isSelected())) {
				BrowserActions.clickOnElementX(chkGiftReceiptCheckBox, driver, "Gift receipt checkbox");
				return true;
			}
		}
		return false;
	}

	/**
	 * To verify Gift Messag echeckbox in Shipping address section is clickable
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyGiftMessageCheckBoxIsClickable()throws Exception{
		if(chkGiftMessageCheckBox.isSelected()) {
			BrowserActions.clickOnElementX(chkGiftMessageCheckBox, driver, "Gift receipt checkbox");
			if(!(chkGiftMessageCheckBox.isSelected())) {
				BrowserActions.clickOnElementX(chkGiftMessageCheckBox, driver, "Gift receipt checkbox");
				return true;
			}
		} else if(!chkGiftMessageCheckBox.isSelected()) {
			BrowserActions.clickOnElementX(chkGiftMessageCheckBox, driver, "Gift receipt checkbox");
			if((chkGiftMessageCheckBox.isSelected())) {
				BrowserActions.clickOnElementX(chkGiftMessageCheckBox, driver, "Gift receipt checkbox");
				return true;
			}
		}
		return false;
	}
	/**
	 * to verify Gift message checkBox
	 * @return -
	 * @throws Exception -
	 */

	public Boolean verifyGiftMessageCheckBoxIsChecked()throws Exception{
		WebElement elem = null;
		if(Utils.isMobile()) {
			if (Utils.waitForElement(driver, chkGiftMessageCheckBox_Mobile_Login)) {
				elem = chkGiftMessageCheckBox_Mobile_Login;
			} else {
				elem = chkGiftMessageCheckBox_Mobile;
			}
		} else {
			if (Utils.waitForElement(driver, chkGiftMessageCheckBox_Login)) {
				elem = chkGiftMessageCheckBox_Login;
			} else {
				elem = chkGiftMessageCheckBox;
			}
		}
		
		return elem.isSelected();
	}

	/**
	 * Click Gift receipt checkbox
	 * @param needToCheck -
	 * @throws Exception -
	 */
	public void clickGiftReceiptCheckBox(boolean needToCheck)throws Exception{
		if(needToCheck) {
			if(!verifyGiftReceiptCheckBoxIsChecked()) {
				BrowserActions.clickOnElementX(chkGiftReceiptCheckBox, driver, "Gift receipt checkbox");
			}
		} else {
			if(verifyGiftReceiptCheckBoxIsChecked()) {
				BrowserActions.clickOnElementX(chkGiftReceiptCheckBox, driver, "Gift receipt checkbox");
			}
		}
	}
	

	/**
	 * Click Gift message checkbox
	 * @param needToCheck - true if check box needs to be selected, false if it need to unselected
	 * @throws Exception - Exception
	 */
	public void clickGiftMessageCheckBox(boolean needToCheck)throws Exception{
		WebElement elem = null;
		if(Utils.isMobile()) {
			if (Utils.waitForElement(driver, chkGiftMessageCheckBox_Mobile_Login)) {
				elem = chkGiftMessageCheckBox_Mobile_Login;
			} else {
				elem = chkGiftMessageCheckBox_Mobile;
			}
		} else {
			if (Utils.waitForElement(driver, chkGiftMessageCheckBox_Login)) {
				elem = chkGiftMessageCheckBox_Login;
			} else {
				elem = chkGiftMessageCheckBox;
			}
		}
		if(needToCheck) {
			if(!verifyGiftMessageCheckBoxIsChecked()) {
				BrowserActions.clickOnElementX(elem, driver, "Gift message checkbox");
			}
		} else {
			if(verifyGiftMessageCheckBoxIsChecked()) {
				BrowserActions.clickOnElementX(elem, driver, "Gift message checkbox");
			}
		}
	}	
	

	/**
	 *  Get selected Option in shipping
	 * @return String
	 * @throws Exception -
	 */
	public String getSelectedOptionShipping()throws Exception{
		WebElement selectedShipment = driver.findElement(By.xpath("//div[@id='shipping-method-list']//input[@checked='checked']//ancestor::div[contains(@class,'form-row')]//label"));
		return BrowserActions.getText(driver, selectedShipment, "Shipping Address default");
	}

	/**
	 * get Selected option for Billing
	 * @return string value
	 * @throws Exception -
	 */
	public String getSelectedOptionBilling()throws Exception{
		return BrowserActions.getText(driver, addressSelectorList, "Billing Address default");
	}

	/**
	 * To click on Billing Address dropdown
	 * @throws Exception - Exception
	 */
	public void clickOnBillingAddDrp()throws Exception{
		if(Utils.waitForElement(driver, drpBillingSavedAddr) || Utils.waitForElement(driver, drpBillingSavedAddrMobile)){
			if(Utils.isMobile())
				BrowserActions.javascriptClick(drpBillingSavedAddrMobile, driver, "Saved Address Dropdown");
			else
				BrowserActions.javascriptClick(drpBillingSavedAddr, driver, "Saved Address Dropdown");
		}else{
			WebElement ele = null;
			if(Utils.isMobile())
				ele = driver.findElement(By.cssSelector(".hide-desktop.hide-tablet #dwfrm_billing_addressList + .selected-option.selected"));
			else
				ele = driver.findElement(By.xpath("//select[@id='dwfrm_billing_addressList']//following-sibling::div[@class='selected-option selected']"));
			BrowserActions.clickOnElementX(ele, driver, "Billing Dropdown");
		}
	}

	/**
	 * To get CVV
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getCvvEntered()throws Exception{
		return txtCvvFieldDefault.getAttribute("value");
	}

	/**
	 * To enter value in CVV field
	 * @param value -
	 * @throws Exception - Exception
	 */
	public void enterCvvEntered(String value)throws Exception{
		if(Utils.waitForElement(driver, txtCvvFieldDefault)) {
			txtCvvFieldDefault.sendKeys(value);
		}
	}

	/**
	 * To verify the state of use this as billing address checkbox
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyUseThisAsBillingAddressCheckBox()throws Exception{
		boolean status = false;
		if(Utils.waitForElement(driver, chkUseBillingAddress)) {
			status = chkUseBillingAddress.isSelected();
		}
		return status;
	}

	/**
	 * To verify the state of Default delivery option checkbox
	 * @return String
	 * @throws Exception - Exception
	 */
	public Boolean verifyDefaultDeliveryOptionChecked()throws Exception{
		boolean flag = false;
		try {
			flag = rdbDefaulDeliveryOption.isSelected();
		}catch(NoSuchElementException e) {
			return false;
		}
		
		return flag;
	}

	/**
	 * To verify the state of Gift Message checkbox
	 * @return String
	 * @throws Exception - Exception
	 */
	public Boolean clickGiftMessageCheckBox()throws Exception{
		BrowserActions.clickOnElementX(chkGiftMessageCheckBox, driver, "Gift message checkbox");
		return verifyGiftMessageCheckBoxIsChecked();
	}

	/**
	 * To verify the max character count in Gift Message
	 * @param maxCount -
	 * @return Boolean -
	 * @throws Exception - Exception
	 */
	public Boolean checkGiftMessage1AllowMoreThanMaxChar(int maxCount)throws Exception{
		String charLimit = txtFormGiftMessage1.getAttribute("data-character-limit");
		if (maxCount == Integer.parseInt(charLimit)) {
			return true;
		}
		return false;
	}

	/**
	 * To verify the character in Gift Message is correct
	 * @param counterText -
	 * @return Boolean 
	 * @throws Exception - Exception
	 */
	public Boolean checkGiftMessage1CounterTextIsCorrect(String counterText)throws Exception{
		String verifyText = BrowserActions.getText(driver, txtFormGiftMessage1CharCountFull, "Message 1 counter text");
		if (counterText.contains(verifyText)) {
			return true;
		}
		return false;
	}

	/**
	 * To verify the character in Gift Message 2 is correct
	 * @param couterText -
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean checkGiftMessage2CounterTextIsCorrect(String couterText)throws Exception{
		String verifyText = BrowserActions.getText(driver, txtFormGiftMessage2CharCountFull, "Message 2 counter text");
		if (couterText.contains(verifyText)) {
			return true;
		}
		return false;
	}

	/**
	 * To verify the character in Gift Message 3 is correct
	 * @param couterText -
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean checkGiftMessage3CounterTextIsCorrect(String couterText)throws Exception{
		String verifyText = BrowserActions.getText(driver, txtFormGiftMessage3CharCountFull, "Message 3 counter text");
		if (couterText.contains(verifyText)) {
			return true;
		}
		return false;
	}

	/**
	 * To verify the Gift Message 2 allows more than max character
	 * @param maxCount -
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean checkGiftMessage2AllowMoreThanMaxChar(int maxCount)throws Exception{
		String charLimit = txtFormGiftMessage2.getAttribute("data-character-limit");
		if (maxCount == Integer.parseInt(charLimit)) {
			return true;
		}
		return false;
	}

	/**
	 * To verify the Gift Message 3 allows more than max character
	 * @param maxCount -
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean checkGiftMessage3AllowMoreThanMaxChar(int maxCount)throws Exception{
		String charLimit = txtFormGiftMessage3.getAttribute("data-character-limit");
		if (maxCount == Integer.parseInt(charLimit)) {
			return true;
		}
		return false;
	}

	/**
	 * To get the Gift Message 1 character count
	 * @return int
	 * @throws Exception - Exception
	 */
	public int getGiftMessage1CharCount()throws Exception{
		return Integer.parseInt(txtFormGiftMessage1CharCount.getText());
	}

	/**
	 * To get the Gift Message 2 character count
	 * @return int
	 * @throws Exception - Exception
	 */
	public int getGiftMessage2CharCount()throws Exception{
		return Integer.parseInt(txtFormGiftMessage2CharCount.getText());
	}

	/**
	 * To get the Gift Message 3 character count
	 * @return int
	 * @throws Exception - Exception
	 */
	public int getGiftMessage3CharCount()throws Exception{
		return Integer.parseInt(txtFormGiftMessage3CharCount.getText());
	}

	/**
	 * To select Express Checkout
	 * @param needToSelect -
	 * @throws Exception - Exception
	 */
	public void selectExpressCheckOut(boolean needToSelect)throws Exception{
		if(!(radioExpressDelivery.isSelected())) {
			if(needToSelect) {
				BrowserActions.clickOnElementX(radioExpressDelivery, driver, "Express checkout radio button");
			}
		} else {
			if(!needToSelect) {
				BrowserActions.clickOnElementX(radioExpressDelivery, driver, "Express checkout radio button");
			}
		}
	}

	/**
	 * To verify Text in Gift message 1 count gets increase / decrease while typing text
	 * @param countTextToEnter -
	 * @param maxCount -
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean verifyTextInGiftMessage1CounterIncreaseAndDecrease(int countTextToEnter, int maxCount)throws Exception{
		BrowserActions.clearTextField(txtFormGiftMessage1, "Message 1 counter text");
		char[] charArray = { 'a', ' ', '@' };
		String randomString = RandomStringUtils.random(countTextToEnter, charArray);
		txtFormGiftMessage1.sendKeys(randomString);
		int verifyCount = countTextToEnter + getGiftMessage1CharCount();
		if(maxCount != verifyCount) {
			return false;
		}
		txtFormGiftMessage1.sendKeys(Keys.BACK_SPACE);
		int intPersonalTextCount = getGiftMessage1CharCount();
		int changeCount = intPersonalTextCount + countTextToEnter - 1;
		if (changeCount != maxCount) {
			return false;
		}

		if(Utils.getRunBrowser(driver).equals("MicrosoftEdge")) {
			int loopCount = maxCount - intPersonalTextCount; 
			for (int i=0;i<loopCount;i++) {
				txtFormGiftMessage1.sendKeys(Keys.BACK_SPACE);
			}
		} else {
			BrowserActions.clearTextField(txtFormGiftMessage1, "Message 1 counter text");
		}
		if(getGiftMessage1CharCount() != maxCount) {
			return false;
		}
		char[] charArray2 = { ' ' };
		randomString = RandomStringUtils.random(2, charArray2);
		txtFormGiftMessage1.sendKeys(randomString);
		changeCount = getGiftMessage1CharCount() + 2;
		if (changeCount != maxCount) {
			return false;
		}
		return true;
	}

	/**
	 * To verify Text in Gift message 2 count gets increase / decrease while typing text
	 * @param countTextToEnter -
	 * @param maxCount -
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean verifyTextInGiftMessage2CounterIncreaseAndDecrease(int countTextToEnter, int maxCount)throws Exception{
		BrowserActions.clearTextField(txtFormGiftMessage2, "Message 2 counter text");
		char[] charArray = { 'a', ' ', '@' };
		String randomString = RandomStringUtils.random(countTextToEnter, charArray);
		txtFormGiftMessage2.sendKeys(randomString);
		int verifyCount = countTextToEnter + getGiftMessage2CharCount();
		if(maxCount != verifyCount) {
			return false;
		}
		txtFormGiftMessage2.sendKeys(Keys.BACK_SPACE);
		int intPersonalTextCount = getGiftMessage2CharCount();
		int changeCount = intPersonalTextCount + countTextToEnter - 1;
		if (changeCount != maxCount) {
			return false;
		}
		if(Utils.getRunBrowser(driver).equals("MicrosoftEdge")) {
			int loopCount = maxCount - intPersonalTextCount; 
			for (int i=0;i<loopCount;i++) {
				txtFormGiftMessage2.sendKeys(Keys.BACK_SPACE);
			}
		} else {
			BrowserActions.clearTextField(txtFormGiftMessage2, "Message 2 counter text");
		}
		if(getGiftMessage2CharCount() != maxCount) {
			return false;
		}
		char[] charArray2 = { ' ' };
		randomString = RandomStringUtils.random(2, charArray2);
		txtFormGiftMessage2.sendKeys(randomString);
		changeCount = getGiftMessage2CharCount() + 2;
		if (changeCount != maxCount) {
			return false;
		}
		return true;
	}

	/**
	 * To verify Text in Gift message 3 count gets increase / decrease while typing text
	 * @param countTextToEnter -
	 * @param maxCount -
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean verifyTextInGiftMessage3CounterIncreaseAndDecrease(int countTextToEnter, int maxCount)throws Exception{
		BrowserActions.clearTextField(txtFormGiftMessage3, "Message 3 counter text");
		char[] charArray = { 'a', ' ', '@' };
		String randomString = RandomStringUtils.random(countTextToEnter, charArray);
		txtFormGiftMessage3.sendKeys(randomString);
		int verifyCount = countTextToEnter + getGiftMessage3CharCount();
		if(maxCount != verifyCount) {
			return false;
		}
		txtFormGiftMessage3.sendKeys(Keys.BACK_SPACE);
		int intPersonalTextCount = getGiftMessage3CharCount();
		int changeCount = intPersonalTextCount + countTextToEnter - 1;
		if (changeCount != maxCount) {
			return false;
		}
		if(Utils.getRunBrowser(driver).equals("MicrosoftEdge")) {
			int loopCount = maxCount - intPersonalTextCount; 
			for (int i=0;i<loopCount;i++) {
				txtFormGiftMessage3.sendKeys(Keys.BACK_SPACE);
			}
		} else {
			BrowserActions.clearTextField(txtFormGiftMessage3, "Message 3 counter text");
		}
		if(getGiftMessage3CharCount() != maxCount) {
			return false;
		}
		char[] charArray2 = { ' ' };
		randomString = RandomStringUtils.random(2, charArray2);
		txtFormGiftMessage3.sendKeys(randomString);
		changeCount = getGiftMessage3CharCount() + 2;
		if (changeCount != maxCount) {
			return false;
		}
		return true;
	}

	/**
	 * To enter value in Gift Message 1 
	 * @param txtToType -
	 * @throws Exception - Exception
	 */
	public void enterTextInGiftMessage1Text(String txtToType) throws Exception{
		BrowserActions.clearTextField(txtFormGiftMessage1, "Message 1 counter text");
		BrowserActions.typeOnTextField(txtFormGiftMessage1, txtToType, driver, "Message 1 text area");
	}

	/**
	 * To enter value in Gift Message 2
	 * @param txtToType -
	 * @throws Exception - Exception
	 */
	public void enterTextInGiftMessage2Text(String txtToType) throws Exception{
		BrowserActions.clearTextField(txtFormGiftMessage2, "Message 2 counter text");
		BrowserActions.typeOnTextField(txtFormGiftMessage2, txtToType, driver, "Message 2 text area");
	}
	
	/**
	 * To enter value in Gift Message 2
	 * @param txtToType -
	 * @throws Exception - Exception
	 */
	public boolean checkPLCCCardSelectedBasedOnBrand(String brand) throws Exception{
		try {
			String text = getCreditCardType();
			Log.event("PLCC card is: " + text);
			Log.event("Expected element is: " + brand);
			
			if (text.toLowerCase().contains(brand.toLowerCase())) {
				return true;
			}
			
			text = text.replaceAll(" ", "");
			brand = brand.replaceAll(" ", "");
			if (brand.length()>5 && text.toLowerCase().contains(brand.toLowerCase().substring(0, 5))) {
				return true;
			}
		} catch (NoSuchElementException ex) {
			return false;
		}
		return false;
	}
	

	/**
	 * To enter value in Gift Message 3
	 * @param txtToType -
	 * @throws Exception - Exception
	 */
	public void enterTextInGiftMessage3Text(String txtToType) throws Exception{
		BrowserActions.clearTextField(txtFormGiftMessage3, "Message 3 counter text");
		BrowserActions.typeOnTextField(txtFormGiftMessage3, txtToType, driver, "Message 3 text area");
	}

	/**
	 * To click Edit shipping link
	 * @throws Exception - Exception
	 */
	public void clickEditShippingLink() throws Exception{
		BrowserActions.scrollToViewElement(lnkEditShipping, driver);
		BrowserActions.clickOnElementX(lnkEditShipping, driver, "Edit shipping Link");
		Utils.waitForElement(driver, chkGiftReceiptCheckBox);
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}

	/**
	 * To get entered text in Gift message box 1
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getEnteredTextInGiftMessage1() throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		if(Utils.waitForElement(driver, txtFormGiftMessage1)) {
			BrowserActions.scrollInToView(txtFormGiftMessage1, driver);
			try {
				return BrowserActions.getTextFromAttribute(driver, txtFormGiftMessage1, "value", "Message 1 text area");
			}catch (Exception e) {
				Log.fail("The edit shipping is not working properly - PXSFCC-4040", driver);
			}
		}
		return null;
	}

	/**
	 * To get text from Gift Message 2
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getEnteredTextInGiftMessage2() throws Exception{
		BrowserActions.scrollInToView(txtFormGiftMessage2, driver);
		return BrowserActions.getTextFromAttribute(driver, txtFormGiftMessage2, "value", "Message 2 text area");
	}

	/**
	 * To get text from Gift Message 3
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getEnteredTextInGiftMessage3() throws Exception{
		BrowserActions.scrollInToView(txtFormGiftMessage3, driver);
		return BrowserActions.getTextFromAttribute(driver, txtFormGiftMessage3, "value", "Message 3 text area");
	}

	/**
	 * To verify Gift message text is present
	 * @param textToVerify -
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean checkGiftMessage1TextApplied(String textToVerify) throws Exception{
		List<WebElement> giftMessages;
		if(txtGiftMessageSectionAfterContinue.size() > 1) {
			giftMessages = txtGiftMessageSectionAfterContinue.get(txtGiftMessageSectionAfterContinue.size() - 1)
					.findElements(By.cssSelector(".value"));
		} else {
			giftMessages = txtGiftMessageSectionAfterContinue.get(0).findElements(By.cssSelector(".value"));
		}
		if (textToVerify.contains(giftMessages.get(0).getText())) {
			return true;
		}
		return false;
	}

	/**
	 * To verify Gift message text is present
	 * @param textToVerify -
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean checkGiftMessage2TextApplied(String textToVerify) throws Exception{
		List<WebElement> giftMessages;
		if(txtGiftMessageSectionAfterContinue.size() > 1) {
			giftMessages = txtGiftMessageSectionAfterContinue.get(txtGiftMessageSectionAfterContinue.size() - 1)
					.findElements(By.cssSelector(".value"));
		} else {
			giftMessages = txtGiftMessageSectionAfterContinue.get(0).findElements(By.cssSelector(".value"));
		}
		if (textToVerify.contains(giftMessages.get(1).getText())) {
			return true;
		}
		return false;
	}

	/**
	 * To verify Gift message text is present
	 * @param textToVerify -
	 * @return true/false 
	 * @throws Exception -
	 */
	public boolean checkGiftMessage3TextApplied(String textToVerify) throws Exception{
		List<WebElement> giftMessages;
		if(txtGiftMessageSectionAfterContinue.size() > 1) {
			giftMessages = txtGiftMessageSectionAfterContinue.get(txtGiftMessageSectionAfterContinue.size() - 1)
					.findElements(By.cssSelector(".value"));
		} else {
			giftMessages = txtGiftMessageSectionAfterContinue.get(0).findElements(By.cssSelector(".value"));
		}
		if (textToVerify.contains(giftMessages.get(2).getText())) {
			return true;
		}
		return false;
	}

	/**
	 * To verify if gift receipt is accepted or not
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean checkGiftReceiptAcceptedOrNot() throws Exception {
		String value = BrowserActions.getText(driver, txtGiftReceiptValueAfterContinue, "Gift Receipt Accepted");
		if(value.toLowerCase().contains("yes")) {
			return true;
		}
		return false;
	}

	/**
	 * To click Add Shipping Address
	 * @throws Exception - Exception
	 */
	public void clickOnAddShippingAddress() throws Exception{
		BrowserActions.clickOnElementX(btnAddNewShippingAddress, driver, "Add new card");
		Utils.waitForPageLoad(driver);
	}


	/**
	 * To select PayPal payment
	 * @throws Exception - Exception
	 */
	public void selectPaypalPayment() throws Exception {
		BrowserActions.selectRadioOrCheckbox(radioPaypal, "YES", driver);
	}
	
	/**
	 * To select credit card payment
	 * @throws Exception
	 */
	public void selectCreditCardPayment() throws Exception {
		BrowserActions.selectRadioOrCheckbox(rdoCreditCard, "YES", driver);
	}

	/**
	 * To type text in Address line 1
	 * @param address1 -
	 * @throws Exception - Exception
	 */
	public void typeTextInAddressLine1Field(String address1)throws Exception{
		BrowserActions.typeOnTextField(txtAddress1, address1, driver, "address line 1");
	}
	
	/**
	* To apply deferred payment code in promo section
    * @param String - deferCode
    * @return void
    * @throws Exception
    */
    public void applyDeferredPaymentCode(String deferCode) throws Exception {
        expandOrColapsePromoCodeSection(true);
        BrowserActions.scrollToView(divDeferPayment, driver);
        BrowserActions.typeOnTextField(txtBoxDeferPaymentCode, deferCode, driver, "Deferred payment code");
        BrowserActions.clickOnElementX(applyDeferPaymentCode, driver, "defer code 'Apply' button");
        Utils.waitForPageLoad(driver);
    }
	
	/**
	 * To verify the shipping address pre-populated in the respective fields in shipping address section
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyShippingAddressPrePopulate() throws Exception{
		String firstNameValue = BrowserActions.getTextFromAttribute(driver, txtFirstnameShipping, "value", "First name");
		String lastNameValue = BrowserActions.getTextFromAttribute(driver, txtLastnameShipping, "value", "Last name");
		String addressLine1Value = BrowserActions.getTextFromAttribute(driver, txtAddressline1Shipping, "value", "Address line 1");
		String zipcodeValue = BrowserActions.getTextFromAttribute(driver, txtZipcode, "value", "Zipcode");
		String PhoneNumberValue = BrowserActions.getTextFromAttribute(driver, txtPhone, "value", "Phone number");
		if(firstNameValue.isEmpty() || lastNameValue.isEmpty() || addressLine1Value.isEmpty() || zipcodeValue.isEmpty() || PhoneNumberValue.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * To click on edit link in account creation section in order reciept section
	 * @throws Exception - Exception
	 */
	public void clickOnEditLinkInOrderReceiptAccountCreation()throws Exception{
		BrowserActions.clickOnElementX(lnkEditOrderReceiptEmail, driver, "Edit Link");
	}

	/**
	 * To click on edit email link
	 * @throws Exception - Exception
	 */
	public void clickOnEditEmailLnk() throws Exception{
		BrowserActions.scrollToViewElement(lnkEditSignin, driver);
		BrowserActions.clickOnElementX(lnkEditSignin, driver, "Edit Email Link");
		Utils.waitForElement(driver, txtGuestEmail);
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}

	/**
	 * To click on create account button
	 * @throws Exception - Exception
	 */
	public void clickOnCreateAccountButtonWithoutEnteringPassword() throws Exception{
		BrowserActions.clickOnElementX(btnCreateAccount, driver, "Create Account Button");
	}

	/**
	 * To click on create account button
	 * @return MyAccountPage
	 * @throws Exception - Exception
	 */
	public MyAccountPage clickOnCreateAccountButton() throws Exception{
		BrowserActions.clickOnElementX(btnCreateAccount, driver, "Create Account Button");
		return new MyAccountPage(driver).get();

	}

	/**
	 * To get entered password
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getEnteredPassword() throws Exception{
		String password = BrowserActions.getText(driver, txtPassword, "Password");
		return password;
	}

	/**
	 * To click product in the recommendation section based on index
	 * @param index -
	 * @return PdpPage -
	 * @throws Exception -
	 */
	public PdpPage clickOnRecommendationProductBasedOnIndex(int index) throws Exception{
		int size = lstRecommendationProdInConfPage.size();
		if (size > index) {
			BrowserActions.clickOnElementX(lstRecommendationProdInConfPage.get(index), driver, "Recommendation product");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("The recommendation section only have "+size+" products");
		}
		return new PdpPage(driver).get();
	}

	/**
	 * To verify the address is displayed correctly in the order confirmation page
	 * @param address , which is used to place order 
	 * @return boolean true if address is correctly displayed, else false
	 * @throws Exception -
	 */
	public boolean verifyAddressCorrectlyDisplayedInOrdConfPage(String address) throws Exception{
		String addressDetail = checkoutProperty.get(address);
		String addressConfirmation = BrowserActions.getText(driver, lnkOrderCnfPgOrderBillingAdd, "Order confirmation address");
		return elementLayer.verifyTwoAddressMatch(addressDetail, addressConfirmation);
	}

	/**
	 * To get entered  confirm password
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getEnteredConfirmPassword() throws Exception{
		String password = BrowserActions.getText(driver, txtConfirmPassword, "Password");
		return password;
	}

	/**
	 * To enter password
	 * @param password -
	 * @throws Exception - Exception
	 */
	public void enterPassword(String password) throws Exception{
		txtPassword.sendKeys(password);
	}

	/**
	 * To enter confirm password
	 * @param password -
	 * @throws Exception - Exception
	 */
	public void enterconfirmPassword(String password) throws Exception{
		txtConfirmPassword.sendKeys(password);
	}

	/**
	 * To clear password
	 * @throws Exception - Exception
	 */
	public void clearPasswordTextFields() throws Exception{
		txtPassword.clear();
		txtConfirmPassword.clear();
	}

	/**
	 * To verify placeholder label string for address line 1
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPlaceHolderForAddressline1Field()throws Exception{
		if(lblAddress1PlaceHolder.getText().trim().equals("Address Line1"))
			return true;
		else
			return false;
	}

	/**
	 * To type text in address line 2
	 * @param address2 -
	 * @throws Exception - Exception
	 */
	public void typeTextInAddressLine2Field(String address2)throws Exception{
		BrowserActions.typeOnTextField(txtAddress2, address2, driver, "address line 2");
	}

	/**
	 * To clear text in address line 2
	 * @throws Exception - Exception
	 */
	public void clearTextInAddressLine2Field()throws Exception{
		BrowserActions.clearTextField(txtAddress2, "address line 2");
	}

	/**
	 * To verify placeholder label string for address line 2
	 * @return boolean 
	 * @throws Exception - Exception
	 */
	public Boolean verifyPlaceHolderForAddressline2Field()throws Exception{
		if(lblAddress2PlaceHolder.getText().trim().equals("Address Line2 (optional)"))
			return true;
		else
			return false;
	}

	/**
	 * To type text in zipcode field
	 * @param address1 -
	 * @throws Exception - Exception
	 */
	public void typeTextInZipcodeField(String address1)throws Exception{
		BrowserActions.typeOnTextField(txtZipcode, address1, driver, "zipcode");
	}

	/**
	 * To verify placeholder label String for zipcode
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPlaceHolderForZipcodeField()throws Exception{
		if(lblZipcodePlaceHolder.getText().trim().equals("Zip Code"))
			return true;
		else
			return false;
	}

	/**
	 * To type in city field
	 * @param address1 -
	 * @throws Exception - Exception
	 */
	public void typeTextInCityField(String address1)throws Exception{
		BrowserActions.typeOnTextField(txtCity, address1, driver, "city");
	}

	/**
	 * To verify placeholder for city field
	 * @return boolean-
	 * @throws Exception - Exception
	 */
	public Boolean verifyPlaceHolderForCityField()throws Exception{
		if(lblCityPlaceHolder.getText().trim().equals("City"))
			return true;
		else
			return false;
	}

	/**
	 * to select value in State dropdown in shipping address section
	 * @param state -
	 * @return String 
	 * @throws Exception - Exception
	 */
	public String selectState(String state) throws Exception {
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(btnShippingState)))){
			shippingDetails.put("select_state_" + state, btnShippingState);	
		}else
			shippingDetails.put("select1_state_" + state, btnShippingStateDiv);
		ShippingPageUtils.enterShippingDetails(shippingDetails, driver);
		return getSelectedState();
	}
	
	/**
	 * To verify placeholder is moving up for the state dropdown
	 * @param state - value to select
	 * @return boolean - true if placeholder moving up, else false 
	 * @throws Exception - Exception
	 */
	public boolean verifyPlaceHolderState(String state) throws Exception {
		int y1 = lblStatePlaceHolder.getLocation().y;
		Log.event(" Initial Position of Place Holder :: " + y1);
		
		LinkedHashMap<String, String> shippingDetails = new LinkedHashMap<String, String>();
		if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(btnShippingState)))){
			shippingDetails.put("select_state_" + state, btnShippingState);	
		}else
			shippingDetails.put("select1_state_" + state, btnShippingStateDiv);
		ShippingPageUtils.enterShippingDetails(shippingDetails, driver);
		
		int y2 = lblStatePlaceHolder.getLocation().y;
		Log.event(" After Text entered, Position of Place Holder :: " + y2);

		if(y1 > y2)
			return true;
		else
			return false;
	}
	
	/**
	 * To select State in Billing Address Section
	 * @param state -
	 * @throws Exception - Exception
	 */
	public void selectStateinBilling(String state) throws Exception {
		LinkedHashMap<String, String> billingDetails = new LinkedHashMap<String, String>();
		if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(btnBillingState)))){
			billingDetails.put("select_state_" + state, btnBillingState);	
		}else
			billingDetails.put("select1_state_" + state, btnBillingStateDiv);
		BillingPageUtils.enterBillingDetails(billingDetails, driver);			
	}

	/**
	 * To get selected state option
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getSelectedState() throws Exception {
		//if(Utils.waitForElement(driver, drpState))
			return BrowserActions.getSelectedOption(drpState);
		//else
		//return BrowserActions.getText(driver, billingAddSelectedState, "State Div"); 
	}
	/**
	 * To get the selected billing address state
	 * return string
	 * Throws Exception
	 */
	public String getBillingAddSelectedState() throws Exception{		
		return BrowserActions.getText(driver, btnBillingStateDiv, "State Div");
	}
	/**
	 * to select value in Country dropdown
	 * @param index -
	 * @return String
	 * @throws Exception -Exception
	 */
	public String selectCountry(int index) throws Exception {
		WebElement option = drpCountry.findElements(By.cssSelector("option")).get(index);
		String dataToBeReturned = option.getText().trim();
		BrowserActions.selectDropdownByValue(drpCountry, "United States");
		return dataToBeReturned;
	}

	/**
	 * To get selected country
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getSelectedCountry() throws Exception {
		System.out.print("Entering ccountry");
		String country;
		if(Utils.waitForElement(driver, drpCountry)) {
			country = BrowserActions.getSelectedOption(drpCountry);
		} else {
			country = BrowserActions.getText(driver, btnShippingCountry, "State Div"); 
		}
		return country;
	}

	/**
	 * To type text in phone number field
	 * @param phoneNum -
	 * @throws Exception - Exception
	 */
	public void typeTextInPhoneNumberField(String phoneNum)throws Exception{
		BrowserActions.scrollInToView(txtPhone, driver);
		BrowserActions.typeOnTextField(txtPhone, phoneNum, driver, "phone");
	}

	/**
	 * To verify placeholder for phone num field
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPlaceHolderForPhoneNumberField()throws Exception{
		if(lblPhonePlaceHolder.getText().trim().equals("Phone Number"))
			return true;
		else
			return false;
	}

	/**
	 * To verify placeholder for country field
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyCountryHolderForPhoneNumberField()throws Exception{
		if(lblCountryPlaceHolder.getText().trim().equals("Country"))
			return true;
		else
			return false;
	}

	/**
	 * To get the number of digits in phone number field
	 * @return int
	 * @throws Exception - Exception
	 */
	public int getNumberOfTextInPhoneField() throws Exception {
		return txtPhone.getText().length();
	}

	/**
	 * To verify make it default checkbox is checked
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyMakeItDefaultIsChecked() throws Exception {
		return chkMakeItDefault.isSelected();
	}

	/**
	 * To check/uncheck make it default
	 * @param enable -
	 * @throws Exception - Exception
	 */
	public void checkUncheckMakeItDefault(Boolean enable) throws Exception {
		if(enable)
		{
			if(chkMakeItDefault.isSelected() == false)
			{
				BrowserActions.clickOnElementX(chkMakeItDefault, driver, " Check Make It Default");
			}
		}
		else
		{
			if(chkMakeItDefault.isSelected() == true)
			{
				BrowserActions.clickOnElementX(chkMakeItDefault, driver, "Uncheck Make It Default");
			}
		}
	}
	/**
	 * To verify save this Address 
	 * @return boolean
	 * @throws Exception -
	 */
	public Boolean verifySaveThisAddressIsChecked() throws Exception {
		return chkSaveThisAddress.isSelected();
	}

	/**
	 * To check/uncheck save this address
	 * @param enable -
	 * @throws Exception - Exception
	 */
	public void checkUncheckSaveThisAddress(Boolean enable) throws Exception {
		if(enable)
		{
			if(chkSaveThisAddress.isSelected() == false)
			{
				BrowserActions.clickOnElementX(chkSaveThisAddress, driver, " Check Save This Address");
			}
		}
		else
		{
			if(chkSaveThisAddress.isSelected() == true)
			{
				BrowserActions.clickOnElementX(chkSaveThisAddress, driver, "Uncheck Save This Address");
			}
		}
	}

	/**
	 * To verify if Save this Address is enabled
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySaveThisAddressIsEnabled() throws Exception {
		if(chkSaveThisAddress.isDisplayed() && chkSaveThisAddress.isEnabled())
			return true;
		else 
			return false;
	}

	/**
	 * To verify if save this address is enabled
	 * @return boolean
	 * @throws Exception -
	 */
	public Boolean verifySaveThisAddressInBillingIsEnabled() throws Exception {
		if(chkSaveThisAddressInBillingDetails.isDisplayed() && chkSaveThisAddressInBillingDetails.isEnabled())
			return true;
		else 
			return false;
	}

	/**
	 * To check/uncheck use this as billing address
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyUseThisAsBillingAddressIsChecked() throws Exception {
		return chkUseThisAsBillingAddress.isSelected();
	}

	/**
	 * To check/uncheck use this as billing address
	 * @param enable -
	 * @throws Exception - Exception
	 */
	public void checkUncheckUseThisAsBillingAddress(Boolean enable) throws Exception {
		if(enable) {
			if(chkUseThisAsBillingAddress.isSelected() == false) {
				BrowserActions.clickOnElementX(chkUseThisAsBillingAddress, driver, " Check Use This As Billing Address");
			}
		} else {
			if(chkUseThisAsBillingAddress.isSelected() == true) {
				BrowserActions.clickOnElementX(chkUseThisAsBillingAddress, driver, "Uncheck Use This As Billing Address");
			}
		}
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To check/uncheck Save Card Check Box
	 * @param enable -
	 * @throws Exception - Exception
	 */
	public void checkUncheckSaveCardCheckBox(Boolean enable) throws Exception {
		if(enable) {
			if(saveCardCheckBox.isSelected() == false) {
				BrowserActions.clickOnElementX(saveCardCheckBox, driver, " Check Save Card Check Box");
			}
		} else {
			if(saveCardCheckBox.isSelected() == true) {
				BrowserActions.clickOnElementX(saveCardCheckBox, driver, "Uncheck Save Card Check Box");
			}
		}
	}

	/**
	 * To click on continue in Shipping Address Section
	 * @throws Exception - Exception
	 */
	public void clickOnContinue()throws Exception {
		if(Utils.isDesktop()) {
			clickOrderSummaryContinue();
		} else {
			clickBlockSectionContinue();
		}
		Utils.waitForPageLoad(driver);
		List<WebElement> mdlSuggestion = driver.findElements(By.cssSelector(flytAddressSuggestion));
		if(mdlSuggestion.size() > 0) {
			for (WebElement ele : mdlSuggestion) {
				if(Utils.waitForElement(driver, ele)){
					if(Utils.waitForElement(driver, btnThatsMyRightAddress)) {
						BrowserActions.clickOnElementX(btnThatsMyRightAddress, driver, "Ship To Original Address alternate");
					} else {
						if(Utils.waitForElement(driver, btnContinueInAddressSuggestionModal)){
							BrowserActions.clickOnElementX(rdoShipToOriginalAddress, driver, "Ship To Original Address");
							BrowserActions.clickOnElementX(btnContinueInAddressSuggestionModal, driver, "Continue button");
						} else{
							Utils.waitUntilElementDisappear(driver, waitLoader);
							BrowserActions.clickOnElementX(driver.findElement(By.cssSelector(".address-information .original-address .input-radio")), driver, "Ship To Original Address");
							BrowserActions.clickOnElementX(btnContinueInAddressSuggestionModal, driver, "Continue button");
						}
					}
				}
			}
		}

		Utils.waitForPageLoad(driver);
	}
	/**
	 * To check/uncheck use this as billing address
	 * @return true/false
	 * @throws Exception -
	 */
	public Boolean verifyContinueButtonEnabled()throws Exception {
		if(btnContinueToPayment.isDisplayed() && btnContinueToPayment.isEnabled())
			return true;
		else
			return false;
	}

	/**
	 * To verify that the shipping method name is not clickable
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyShippingMethodNameNotClickable()throws Exception {
		Boolean flag = false;
		for (WebElement name : lblShippingMethodName) {
			if(name.getAttribute("class").contains("field") || name.getAttribute("innerHTML").contains("field") 
					|| name.getTagName().equals("a") && (!name.getAttribute("href").equals(null)) && 
					(!name.getAttribute("href").isEmpty()))

				flag = true;
			else
				return false;
		}
		return flag;
	}

	/**
	 * To verify if shipping method cost is displayed
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyShippingMethodCostIsDisplayed()throws Exception {
		Boolean flag = false;
		for (WebElement cost : lblShippingMethodCost) {
			if(cost.isDisplayed())
				flag = true;
			else
				return false;
		}
		return flag;
	}

	/**
	 * To verify Shipping method promotion is displayed
	 * @param shippingmethod -
	 * @param discount -
	 * @return true/false
	 * @throws Exception - Exception
	 */
	public Boolean verifyShippingMethodPromotionIsDisplayed(String shippingmethod, String discount)throws Exception {
		List<WebElement> shippingCost = driver.findElements(By.cssSelector("#shipping-method-list label"));
		for (WebElement cost : shippingCost) {
			if(cost.getText().trim().contains(shippingmethod))
			{
				if(cost.findElement(By.cssSelector("span.standard-shipping")).getCssValue("text-decoration").contains("line-through"))
				{
					//&& cost.findElement(By.cssSelector("span.discount-shipping")).getText().trim().equals(discount)
					
					if(cost.findElement(By.cssSelector("span.discount-shipping")).isDisplayed() 
							&& cost.findElement(By.cssSelector("span.discount-shipping")).getCssValue("color").trim().equals("rgba(231, 0, 0, 1)"))
						return true;
				}
			}
		}
		return false;
	}

	/**
	 * To verify that the shipping method details is displayed
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyShippingMethodDetailsIsDisplayed()throws Exception {
		Boolean flag = false;
		for (WebElement details : lblShippingMethodDetails) {
			if(details.isDisplayed())
				flag = true;
			else
				return false;
		}
		return flag;
	}

	/**
	 * To check/uncheck gift message
	 * @param enable -
	 * @throws Exception - Exception
	 */
	public void checkUncheckGiftMessage(Boolean enable) throws Exception {
		if(enable)
		{
			if(chkGiftMessage.isSelected() == false)
			{
				BrowserActions.clickOnElementX(chkGiftMessage, driver, " Check Gift Message");
			}
		}
		else
		{
			if(chkGiftMessage.isSelected() == true)
			{
				BrowserActions.clickOnElementX(chkGiftMessage, driver, "Uncheck Gift Message");
			}
		}
	}

	/**
	 * To check/uncheck gift receipt
	 * @param enable -
	 * @throws Exception - Exception
	 */
	public void checkUncheckGiftReceipt(Boolean enable) throws Exception {
		if(enable)
		{
			if(chkGiftReceipt.isSelected() == false)
			{
				BrowserActions.clickOnElementX(chkGiftReceipt, driver, " Check Gift Receipt");
			}
		}
		else
		{
			if(chkGiftReceipt.isSelected() == true)
			{
				BrowserActions.clickOnElementX(chkGiftReceipt, driver, "Uncheck Gift Receipt");
			}
		}
	}

	/**
	 * To type gift message
	 * @param message -
	 * @throws Exception - Exception
	 */
	public void typeGiftMessage(String message)throws Exception{
		BrowserActions.typeOnTextField(txtGiftMessage, message, driver, "Gift message text");
	}

	/**
	 * To get value in nickname field
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getNickName()throws Exception {
		return txtNickName.getAttribute("value").trim();
	}
	
	/**
	 * To get address nickname of saved address in dropdown
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getNickNameFromDropDown()throws Exception {
		String savedAddressNickname;
		if(Utils.isMobile()) {
			savedAddressNickname = savedAddressNickNameMobile.getText().trim();
		}else {
			savedAddressNickname = savedAddressNickName.getText().trim();
		}
		return savedAddressNickname;
	}

	/**
	 * To get value in firstname field
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getFirstName()throws Exception {
		return txtFirstName.getAttribute("value").trim();
	}

	/**
	 * To get value in lastname field
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getLastName()throws Exception {
		return txtLastName.getAttribute("value").trim();
	}

	/**
	 * To get value in address line 1 field
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getAddressLine1()throws Exception {
		return txtAddress1.getAttribute("value").trim();
	}

	/**
	 * To get value in address line 2 field
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getAddressLine2()throws Exception {
		return txtAddress2.getAttribute("value").trim();
	}

	/**
	 * To get value in zipcode field
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getZipcode()throws Exception {
		return txtZipcode.getAttribute("value").trim();
	}

	/**
	 * To get value in city field
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getCity()throws Exception {
		return txtCity.getAttribute("value").trim();
	}

	/**
	 * To get value in phone number field
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getPhoneNumber()throws Exception {
		return txtPhone.getAttribute("value").trim();
	}

	/**
	 * To get value in gift message field
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getGiftMessage()throws Exception {
		return txtGiftMessage.getAttribute("value").trim();
	}

	/**
	 * To click Add New Address
	 * @throws Exception - Exception
	 */
	public void clickAddNewAddress()throws Exception {
		BrowserActions.clickOnElementX(lnkAddNew, driver, "Add New");
		Utils.waitForPageLoad(driver);
	}


	/**
	 * To get saved nickname
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSavedNickname()throws Exception {
		WebElement element = divShippingAddress.findElement(By.cssSelector("div:nth-of-type(1)"));
		return BrowserActions.getText(driver, element, "Nick Name").trim();
	}

	/**
	 * To get saved city
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSavedCity()throws Exception {
		WebElement element = divShippingAddress.findElement(By.cssSelector("div:nth-of-type(6)"));
		return BrowserActions.getText(driver, element, "City").trim();
	}

	/**
	 * To get saved zipcode
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSavedZipcode()throws Exception {
		WebElement element = divShippingAddress.findElement(By.cssSelector("div:nth-of-type(6)"));
		return BrowserActions.getText(driver, element, "Zipcode").trim();
	}

	/**
	 * To get saved phone number
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSavedPhoneNumber()throws Exception {
		WebElement element = divShippingAddress.findElement(By.cssSelector("div:nth-of-type(3)"));
		return BrowserActions.getText(driver, element, "Phone Number").trim();
	}

	/**
	 * To get email Id from sign in
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getEmailIDFromSignIn()throws Exception{
		return lblEmailInSignInBox.getText().trim();
	}

	/**
	 * To get email ID from shipping section
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getEmailIDFromShippingSection()throws Exception{
		Log.event("--->>>Email Verification :: " + lblEmailInShippingDetails.getText());
		return lblEmailInShippingDetails.getText().trim();
	}

	/**
	 * To get saved delivery option
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSavedDeliveryOption()throws Exception {
		return BrowserActions.getText(driver, txtSavedDeliveryOption, "Saved Delivery Option").trim();
	}

	/**
	 * To get shipping type
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getShippingTypeInShippingDetails()throws Exception{
		Log.event("--->>>Shipping Type :: " + shippingType.getText());
		return shippingType.getText().trim();
	}

	/**
	 * To get mini payment info
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getMiniPaymentInfo()throws Exception{
		String dataToReturn = new String();
		dataToReturn = lblMiniPaymentSection.getText();
		return dataToReturn;
	}

	/**
	 * To valculate Sutotal in Cart List 
	 * @return string
	 * @throws Exception - Exception
	 */
	public String calculateSubtotalInCartList()throws Exception{
		float subTotal = 0;
		for(int i = 0; i < lstItemsInCartList.size(); i++){
			if(Utils.isMobile()){
				if(lstItemsInCartList.get(i).findElements(By.cssSelector(".hide-desktop.hide-tablet .item-total div[class*='price']")).size() > 1){
					WebElement finalPrice = lstItemsInCartList.get(i).findElement(By.cssSelector(".hide-desktop.hide-tablet .item-total div[class='price-adjusted-total']"));
					Log.event("Adding amount to SubTotal :: " + finalPrice.getAttribute("innerHTML").trim().replace("$", ""));
					subTotal += Float.parseFloat(finalPrice.getAttribute("innerHTML").trim().replace("$", ""));
				}else{
					WebElement price = lstItemsInCartList.get(i).findElement(By.cssSelector(".hide-desktop.hide-tablet .item-total span[class*='price']"));
					Log.event("Adding amount to SubTotal :: " + price.getAttribute("innerHTML").trim().replace("$", ""));
					subTotal += Float.parseFloat(price.getAttribute("innerHTML").trim().replace("$", ""));
				}
			}else{
				if(lstItemsInCartList.get(i).findElements(By.cssSelector(".hide-mobile .item-total div[class*='price']")).size() > 1){
					WebElement finalPrice = lstItemsInCartList.get(i).findElement(By.cssSelector(".hide-mobile .item-total div[class='price-adjusted-total']"));
					Log.event("Adding amount to SubTotal :: " + finalPrice.getAttribute("innerHTML").trim().replace("$", ""));
					subTotal += Float.parseFloat(finalPrice.getAttribute("innerHTML").trim().replace("$", ""));
				}else{
					WebElement price = lstItemsInCartList.get(i).findElement(By.cssSelector(".hide-mobile .item-total span[class*='price']"));
					Log.event("Adding amount to SubTotal :: " + price.getAttribute("innerHTML").trim().replace("$", ""));
					subTotal += Float.parseFloat(price.getAttribute("innerHTML").trim().replace("$", ""));
				}
			}
		}
		Log.event("Order Sub-Total :: " + String.format("%.2f", subTotal));
		return String.format("%.2f", subTotal);
	}

	/**
	 * To get subtotal in summary
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSubtotalInSummary()throws Exception{
		String subTotalToReturn = lblOrderSubtotalInSummary.getText().trim().replace("$", ""); 
		Log.event("Order Sub-Total :: " + subTotalToReturn);
		return subTotalToReturn;
	}
	
	/**
	 * To get product ID in Checkout page
	 * @return HashSet<String> ProductId- Product IDs 
	 * @throws Exception - Exception
	 */
	public HashSet<String> getOrderedPrdListNumber() throws Exception{
		HashSet<String> ProductId = new HashSet<String> ();
		
		for(int i=0; i < lblPrdListNumbers.size(); i++) {
			WebElement prdId = lblPrdListNumbers.get(i);
			ProductId.add(BrowserActions.getText(driver, prdId, "Product ID"));
		}
		
		return ProductId;
	}

	/**
	 * To get order total in summary
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getOrderTotalInSummary()throws Exception{

		return lblOrderTotalInSummary.getText().trim().split("\\$")[1];
	}

	/**
	 * Expand / Collapse Reward Certificate Section
	 * @param state - String 
	 * 			To Expand pass parameter as "expand"
	 * 			To Collapse Pass Parameter as "collapse"
	 * @return String
	 * 			If Expanded - "expanded"
	 * 			If Collapsed - "collapsed"
	 * @throws Exception - Exception
	 */
	public String expandCollapseRewardCertSection(String state)throws Exception{

		BrowserActions.scrollToViewElement(icoRewardCertArrow, driver);
		if(state.equals("expand") && Utils.waitForElement(driver, txtRewardCode)){
			Log.event("Reward Certificate Section expanded!");
			return state+"ed";
		}else{
			BrowserActions.clickOnElementX(icoRewardCertArrow, driver, "Reward Certification Arrow Icon");
			Log.event("Reward Certificate Section "+state+"ed!");
			return state+"ed";
		}
	}

	/**
	 * Expand / Collapse Promo Code Section
	 * @param state - String 
	 * 			To Expand pass parameter as "expand"
	 * 			To Collapse Pass Parameter as "collapse"
	 * @return String
	 * 			If Expanded - "expanded"
	 * 			If Collapsed - "collapsed"
	 * @throws Exception - Exception
	 */
	public String expandCollapsePromoCodeSection(String state)throws Exception{

		BrowserActions.scrollToViewElement(icoPromoCodeArrow, driver);
		if(state.equals("expand") && Utils.waitForElement(driver, txtCouponCode)){
			Log.event("Reward Certificate Section expanded!");
			return state+"ed";
		}else{
			BrowserActions.clickOnElementX(icoPromoCodeArrow, driver, "Promocode Arrow Icon");
			Log.event("Reward Certificate Section "+state+"ed!");
			return state+"ed";
		}
	}

	/**
	 * Expand / Collapse GiftCard Section
	 * @param state - String 
	 * 			To Expand pass parameter as "expand"
	 * 			To Collapse Pass Parameter as "collapse"
	 * @return String
	 * 			If Expanded - "expanded"
	 * 			If Collapsed - "collapsed"
	 * @throws Exception - Exception
	 */
	public String expandCollapseGiftCardSection(String state)throws Exception{

		BrowserActions.scrollToViewElement(icoGiftCardArrow, driver);
		if(state.equals("expand") && Utils.waitForElement(driver, txtGiftCardCode)){
			Log.event("Reward Certificate Section expanded!");
			return state+"ed";
		}else{
			BrowserActions.clickOnElementX(icoGiftCardArrow, driver, "Giftcard Arrow Icon");
			Log.event("Reward Certificate Section "+state+"ed!");
			return state+"ed";
		}
	}
	
	/**
	 * To click No Thanks In PLCC
	 * @throws Exception - Exception
	 */
	public void clickNoThanksInPLCC()throws Exception{
		BrowserActions.scrollToViewElement(btnPLCCNoThanks, driver);
		BrowserActions.clickOnElementX(btnPLCCNoThanks, driver, "'No Thanks' button");
		Utils.waitUntilElementDisappear(driver, modalCheckoutPlcc);
	}
	
	/**
	 * To click Get It today In PLCC
	 * @throws Exception - Exception
	 */
	public void clickGetItTodayInPLCC()throws Exception{
		BrowserActions.scrollToViewElement(btnPLCCGetItToday, driver);
		BrowserActions.clickOnElementX(btnPLCCGetItToday, driver, "'No Thanks' button");
		Utils.waitUntilElementDisappear(driver, modalCheckoutPlcc);
	}
	
	
	/**
	 * To type on Social Security Number
	 * @param Social Security Number -
	 * @throws Exception - Exception
	 */
	public void typeSocialSecurityNumberInPlcc(String SocialSecurityNumber)throws Exception{
		try {
			BrowserActions.typeOnTextField(plccSocialSecurityNumber, SocialSecurityNumber, driver, "Social Security Number");
		} catch(NoSuchElementException e) {
			Log.failsoft("Element not found in page!",driver);
		}
	}
	
	
	/**
	 * To get total in order summary
	 * @return TotalToReturn - Order total
	 * @throws Exception - Exception
	 */
	public String getOrderSummaryTotal()throws Exception{
		String TotalToReturn = BrowserActions.getText(driver, lblOrderSummaryTableTotal, "Order Summary Table Total"); 
		Log.event("Order Summary Total :: " + TotalToReturn);
		return TotalToReturn.replace("$", "").trim();
	}
	
	/**
	 * To get Shipping Price form order summary
	 * @return double - value of price
	 * @throws Exception - Exception
	 */
	public double getShippingPrice() throws Exception {
		String value = BrowserActions.getText(driver, divShippingMessagePrice, "Shipping price in Cart"); 
		return Double.valueOf(value.replace("$", "").trim());
	}

	/**
	 * To scroll to billing address
	 * @throws Exception - Exception
	 */
	public void scrollToBillingAddress()throws Exception{
		BrowserActions.scrollToViewElement(lnkEditBilling, driver);
	}

	/**
	 * To get saved gift message
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSavedGiftMessage()throws Exception {
		return BrowserActions.getText(driver, txtSavedGiftMessage, "Saved Gift Message").trim();
	}

	/**
	 * To click edit customer sign in
	 * @return CheckoutPage
	 * @throws Exception - Exception
	 */
	public CheckoutPage clickEditCustomerSignin()throws Exception {
		BrowserActions.clickOnElementX(lnkEditCustomerSignin, driver, "Edit Customer Signin");
		Utils.waitForPageLoad(driver);
		return new CheckoutPage(driver).get();
	}
	
	
	public Map<String, String> getPlaceHolderPosition()throws Exception {
		Map<String, String> placeHolderLoc = new HashMap<String, String>();
		
		placeHolderLoc.put("FName", Integer.toString(lblFirstNamePlaceHolder.getLocation().y));
		placeHolderLoc.put("LName", Integer.toString(lblLastNamePlaceHolder.getLocation().y));
		placeHolderLoc.put("Add1", Integer.toString(lblAddress1PlaceHolder.getLocation().y));
		placeHolderLoc.put("Add2", Integer.toString(lblAddress2PlaceHolder.getLocation().y));
		placeHolderLoc.put("Zip", Integer.toString(lblZipcodePlaceHolder.getLocation().y));
		placeHolderLoc.put("City", Integer.toString(lblCityPlaceHolder.getLocation().y));
		placeHolderLoc.put("Phone", Integer.toString(lblPhonePlaceHolder.getLocation().y));
		
		return placeHolderLoc;
	}
	
	public boolean verifyPlaceHolderPosition(Map<String, String> expectedList, Map<String, String> actualList)throws Exception {
		try {
			for (String k : expectedList.keySet()) {
				String k1 = expectedList.get(k);
				String k2 = actualList.get(k);
				if(!(Integer.parseInt(k1) > Integer.parseInt(k2))) {
					return false;
				}
			}
		} catch (NullPointerException np) {
			return false;
		}
		return true;
		
	}

	/**
	 * To type on password
	 * @param password -
	 * @throws Exception - Exception
	 */
	public void typeOnPassword(String password)throws Exception{
		fldPassword.clear();
		BrowserActions.typeOnTextField(fldPassword, password, driver, "Email Address ");
	}

	/**
	 * To click save in billing details
	 * @throws Exception - Exception
	 */
	public void clickSaveInBillingDetails()throws Exception {
		BrowserActions.clickOnElementX(lnkSaveBillingDetails, driver, "Save link");
		Utils.waitUntilElementDisappear(driver, lnkSaveBillingDetails);
	}

	/**
	 * To click cancel in billing details
	 * @throws Exception - Exception
	 */
	public void clickCancelInBillingDetails()throws Exception {
		if(Utils.waitForElement(driver, lnkCancelBillingDetails)) {
			BrowserActions.scrollToViewElement(lnkCancelBillingDetails, driver);
			BrowserActions.clickOnElementX(lnkCancelBillingDetails, driver, "Cancel link");
			Utils.waitForPageLoad(driver);
		} else {
			Log.failsoft("Cancel button is not displaying in the billing address", driver);
		}
	}

	/**
	 * To verify edit link displayed right of billing address title
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyEditLinkRightOfBillingAddressTitle()throws Exception {
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, btneditBillingAddress, lblBillingAddressHeading);

	}

	/**
	 * To click edit in Billing Address section
	 * @throws Exception - Exception
	 */
	public void clickEditAddressInBillingSection()throws Exception {
		BrowserActions.clickOnElementX(btneditBillingAddress, driver, "Edit link");
		Utils.waitUntilElementDisappear(driver, waitLoader);
		Utils.waitForElement(driver, selectedBillingAddressMobile);
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the saved address from billing details
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSavedAddressFromBillingDetails()throws Exception {
		return BrowserActions.getText(driver, lblSavedAddressInBillingDetails, "Saved Address").trim();
	}

	/**
	 * To get the saved address from shipping details
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSavedAddressFromShippingDetails()throws Exception {
		WebElement selectedAddress = driver.findElement(By.cssSelector(".addresssection"));
		String dataToReturn = selectedAddress.getText();
		return dataToReturn;
	}
	
	/**
	 * To get the saved address from shipping details
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSavedAddressNicknameFromShippingDetails()throws Exception {
		WebElement addressNickName = driver.findElement(By.cssSelector(".addresssection span"));
		String txtNickName = addressNickName.getText();
		return txtNickName;
	}

	/**
	 * To get the value from saved address dropdown
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getValueFromSavedAddressDropdown()throws Exception {
		/*if(Utils.getRunPlatForm().equalsIgnoreCase("mobile"))
		return BrowserActions.getSelectedOption(drpSelectAddressMobile.findElement(By.cssSelector("select#dwfrm_singleshipping_addressList")));
	else
		return BrowserActions.getSelectedOption(drpShippingSelectAddress.findElement(By.cssSelector("select#dwfrm_singleshipping_addressList")));*/
		WebElement addressDropdown = null;
		if(Utils.isMobile()) {
			addressDropdown = driver.findElement(By.xpath("//select[@id='dwfrm_singleshipping_addressList']//following-sibling::div[contains(@class,'address-nickname-mobile hide-desktop hide-tablet')]"));
		} else {
			addressDropdown = driver.findElement(By.xpath("//select[@id='dwfrm_singleshipping_addressList']//following-sibling::div[contains(@class,'selected')]"));
		}
		Log.event("Saved Default Address :: " + addressDropdown.getText());
		return addressDropdown.getText();
	}

	/**
	 * To get full address in mobile
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getFullAddressInMobile()throws Exception {
		return BrowserActions.getText(driver, txtFullAddressMobile, "Full address").trim();
	}

	/**
	 * To get the vale from saved address dropdown in billing details 
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getValueFromSavedAddressDropdownInBillingDetails()throws Exception {
		if(Utils.isMobile())
			return BrowserActions.getSelectedOption(drpSelectAddressMobile.findElement(By.cssSelector("select#dwfrm_billing_addressList")));
		else
			return BrowserActions.getSelectedOption(drpSelectAddress.findElement(By.cssSelector("select#dwfrm_billing_addressList")));

	}

	/**
	 * To select value from saved address dropdown
	 * @param valueToBeSelected -
	 * @throws Exception - Exception
	 */
	public void selectValueFromSavedAddressesDropdown(String valueToBeSelected)throws Exception {
		BrowserActions.selectDropdownByValue(drpSelectAddress.findElement(By.cssSelector("select#dwfrm_singleshipping_addressList")), valueToBeSelected);
	}
	
	/**
	 * Super fast Radio Button Is Selected/Not
	 * @return boolean - true if selected, else false
	 * @throws Exception - Exception
	 */
	public boolean checkShippingMethodSuperfastRadioButtonIsSelected()throws Exception{
		try {
			WebElement selectedShipment = driver.findElement(By.cssSelector("#shipping-method-SuperFast"));
			if(selectedShipment.isSelected()) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * To select a saved address in shipping address by index
	 * @param index - Index of address to be selected
	 * @throws Exception
	 */
	public void selectSavedAddressInShippingByIndex(int index) throws Exception {
		if (Utils.waitForElement(driver, step1Active)) {
			if(Utils.waitForElement(driver, lnkShippingAddressEdit)) {
				BrowserActions.clickOnElementX(lnkShippingAddressEdit, driver, "Shipping address edit button");
			}
			BrowserActions.selectDropdownByIndex(drpShippingAddress, index);
		} else {
			Log.event("User is not on Checkout Step 1/Shipping.");
		}
	}
	
	/**
	 * To Select Value from Save Address from Dropdown By Index
	 * @param index -
	 * @throws Exception - Exception
	 */
	public void selectValueFromSavedAddressesDropdownByIndex(int index)throws Exception {
		Utils.waitForPageLoad(driver);
		if(Utils.isMobile()) {
			BrowserActions.selectDropdownByIndex(drpSelectAddress, index);			
		} else {
			BrowserActions.clickOnElementX(selectedShippingAddress, driver, "Address selection drop down");
			Utils.waitForElement(driver, drpSelectAddressOptions);
			List<WebElement> elementsList = drpSelectAddressOptions.findElements(By.cssSelector(" .selection-list li"));
			WebElement element = elementsList.get(index);
			BrowserActions.clickOnElementX(element, driver, "Address");
			
		}
		Utils.waitForPageLoad(driver);
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}
	/**
	 * To Select value from Saved billing Address By drowpdown by index
	 * @param index -
	 * @throws Exception - Exception
	 */
	public void selectValueFromSavedBillingAddressesDropdownByIndex(int index)throws Exception {
		Utils.waitForPageLoad(driver);
		if(Utils.isMobile()) {
			BrowserActions.clickOnElementX(drpSelectAddressMobile, driver, "clicked on drop down menu");
			List<WebElement> elementsList = driver.findElements(By.xpath("//select[@id='dwfrm_billing_addressList']//following-sibling::ul//li"));
			WebElement element = elementsList.get(index);
			BrowserActions.clickOnElementX(element, driver, "Address");
						
		} else {
			if(Utils.waitForElement(driver, drpBillingSavedAddr)){
				BrowserActions.selectDropdownByIndex(drpBillingSavedAddr, index);
			}else{
				BrowserActions.clickOnElementX(drpSelectAddressSelectedOption, driver, "Address selection drop down");
				List<WebElement> elementsList = driver.findElements(By.xpath("//select[@id='dwfrm_billing_addressList']//following-sibling::ul//li"));
				WebElement element = elementsList.get(index);
				BrowserActions.clickOnElementX(element, driver, "Address");
			}
			
		}
		Utils.waitForElement(driver, lblBillingAddressSelected);
	}

	/**
	 * To select value from saved address dropdown in billing details
	 * @param valueToBeSelected -
	 * @throws Exception -
	 */
	public void selectValueFromSavedAddressesDropdownInBillingDetails(String valueToBeSelected)throws Exception {
		BrowserActions.selectDropdownByValue(drpSelectAddress.findElement(By.cssSelector("select#dwfrm_billing_addressList")), valueToBeSelected);
	}

	/**
	 * To select value from saved address dropdown by index
	 * @param index -
	 * @throws Exception - Exception
	 */
	public void selectValueFromSavedAddressesDropdownByIndexInBillingDetails(int index)throws Exception {
		BrowserActions.selectDropdownByIndex(drpSelectAddressMobile, index);
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To check/uncheck express delivery
	 * @param enable -
	 * @throws Exception - Exception
	 */
	public void checkUncheckExpressDelivery(Boolean enable) throws Exception {
		try {
			WebElement ele = driver.findElement(By.cssSelector(rdoExpress));
			if(enable) {
				if(ele.isSelected() == false) {
					BrowserActions.clickOnElementX(ele, driver, "Check Express Delivery");
				}
			} else {
				if(ele.isSelected() == true) {
					BrowserActions.clickOnElementX(ele, driver, "Uncheck Express Delivery");
				}
			}
		} catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected. Check BM if product has Express Delivery option.", driver);
		}
	}

	/**
	 * To verify express delivery is checked
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyExpressDeliveryIsChecked() throws Exception {
		WebElement ele = driver.findElement(By.cssSelector(rdoExpress));
		return ele.isSelected();
	}

	/**
	 * To wait for payment details not loaded
	 * @throws Exception - Exception
	 */
	public void waitforPaymentDetailstoLoad()throws Exception{
		Utils.waitForElement(driver, divPaymentDetails);
	}

	/**
	 * To apply reward certificate
	 * @param RCcode -
	 * @throws Exception - Exception
	 */
	public void ApplyRewardCertificate(String RCcode)throws Exception{
		BrowserActions.scrollInToView(rewardCertificateArrow, driver);
		BrowserActions.clickOnElementX(rewardCertificateArrow, driver, "RW Certificate expand arrow");
		Utils.waitForElement(driver, tbRewardCertificate);
		BrowserActions.typeOnTextField(tbRewardCertificate, RCcode, driver, "");
		BrowserActions.clickOnElementX(btnRewardApply, driver, "'Reward' Apply button");
	}

	/**
	 * To click on Add New Payment method
	 * @throws Exception - Exception
	 */
	public void clickOnAddNewPaymentMethod()throws Exception{
		BrowserActions.clickOnElementX(lnkAddNewCredit, driver, "Add New Credit Card Link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on Add New Payment method
	 * @throws Exception - Exception
	 */
	public void clickEditBillingAddressPLCCError()throws Exception{
		BrowserActions.clickOnElementX(lnkEditBillingAndPayment, driver, "Edit Billing And Payment");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * Enter details to create Account
	 * @param emailAddress -
	 * @param confirmEmailAddress -
	 * @param password -
	 * @param confirmPassword -
	 * @throws Exception -
	 */
	public void enterDetailsToCreateAccount(String emailAddress, String confirmEmailAddress,
			String password, String confirmPassword) throws Exception{

		txtAccountFirstName.clear();
		txtAccountLastName.clear();
		txtAccountEmail.clear();
		txtAccountConfirmEmail.clear();

		String randomFirstName = RandomStringUtils.randomAlphabetic(5).toLowerCase();
		txtAccountFirstName.sendKeys(randomFirstName);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		txtAccountLastName.sendKeys(randomLastName);

		txtAccountEmail.sendKeys(emailAddress);
		txtAccountConfirmEmail.sendKeys(confirmEmailAddress);

		txtAccountPassword.sendKeys(password);
		txtAccountConfirmPassword.sendKeys(confirmPassword);
	}

	/**
	 * To fill address in Billing details
	 * @param details -
	 * @throws Exception - Exception
	 */
	public void fillAddressInBillingDetails(List<String> details)throws Exception{
		BrowserActions.typeOnTextField(txtNickNameBillingDetails, details.get(0), driver, "Nick Name");
		BrowserActions.typeOnTextField(txtFirstNameBillingDetails, details.get(1), driver, "First Name");
		BrowserActions.typeOnTextField(txtLastNameBillingDetails, details.get(2), driver, "Last Name");
		BrowserActions.typeOnTextField(txtAddress1BillingDetails, details.get(3), driver, "Address 1");
		if(details.size() == 9) //if Address 2 is needed
		{
			BrowserActions.typeOnTextField(txtAddress2BillingDetails, details.get(4), driver, "Address 2");
			BrowserActions.typeOnTextField(txtCityBillingDetails, details.get(5), driver, "City");
			BrowserActions.typeOnTextField(txtZipcodeBillingDetails, details.get(6), driver, "Zipcode");
			BrowserActions.typeOnTextField(txtPhoneBillingDetails, details.get(7), driver, "Phone Number");
		}
		else
		{
			BrowserActions.typeOnTextField(txtCityBillingDetails, details.get(4), driver, "City");
			BrowserActions.typeOnTextField(txtZipcodeBillingDetails, details.get(5), driver, "Zipcode");
			BrowserActions.typeOnTextField(txtPhoneBillingDetails, details.get(6), driver, "Phone Number");
		}

		BrowserActions.selectDropdownByValue(drpStateBillingDetails, details.get(8));
		BrowserActions.selectDropdownByValue(drpCountryBillingDetails, "United States");
	}

	/**
	 * To verify state of make it default CHECKBOX
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyMakeItDefaultIsCheckedInBillingDetails() throws Exception {
		BrowserActions.scrollToViewElement(chkMakeDefaultInBillingDetails, driver);
		return chkMakeDefaultInBillingDetails.isSelected();
	}

	/**
	 * To check/uncheck make it default
	 * @param enable -
	 * @throws Exception - Exception
	 */
	public void checkUncheckMakeItDefaultInBillingDetails(Boolean enable) throws Exception {
		if(enable) {
			if(chkMakeDefaultInBillingDetails.isSelected() == false) {
				BrowserActions.clickOnElementX(chkMakeDefaultInBillingDetails, driver, " Check Make It Default");
			}
		} else {
			if(chkMakeDefaultInBillingDetails.isSelected() == true) {
				BrowserActions.clickOnElementX(chkMakeDefaultInBillingDetails, driver, "Uncheck Make It Default");
			}
		}
	}

	/**
	 * To verify state of Save this address checkbox
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySaveThisAddressIsCheckedInBillingDetails() throws Exception {
		BrowserActions.scrollToViewElement(chkSaveThisAddressInBillingDetails, driver);
		return chkSaveThisAddressInBillingDetails.isSelected();
	}

	/**
	 * To check/uncheck Save this address 
	 * @param enable -
	 * @throws Exception - Exception
	 */
	public void checkUncheckSaveThisAddressInBillingDetails(Boolean enable) throws Exception {
		if(enable) {
			if(chkSaveThisAddressInBillingDetails.isSelected() == false) {
				BrowserActions.clickOnElementX(chkSaveThisAddressInBillingDetails, driver, " Check Save This Address");
			}
		} else {
			if(chkSaveThisAddressInBillingDetails.isSelected() == true) {
				BrowserActions.clickOnElementX(chkSaveThisAddressInBillingDetails, driver, "Uncheck Save This Address");
			}
		}
	}

	/**
	 * To click Select Payment Method
	 * @throws Exception - Exception
	 */
	public void clickSelectPaymentMethod() throws Exception {
		BrowserActions.clickOnElementX(btnSelectPaymentMethod, driver, "Select Payment Method");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To select a card detail by index
	 * @param index -
	 * @throws Exception - Exception
	 */
	public void selectSavedCardDetailsBasedOnIndex(int index) throws Exception {
		WebElement element  = lblSavedCardDetails.get(index-1);
		BrowserActions.clickOnElementX(element, driver, "Select Card Detail ");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click edit Shipping Address
	 * @throws Exception - Exception
	 */
	public void clickEditShippingAddress() throws Exception {
		try {
			BrowserActions.clickOnElementX(btnEditShippingAddressLogIn, driver, "Edit link");
			Utils.waitForPageLoad(driver);
		} catch (NoSuchElementException e) {
			Log.failsoft("The shipping address edit button is not displaying in page" ,driver);
		}
	}

	/**
	 * To click edit billing address
	 * @throws Exception - Exception
	 */
	public void clickBillingAddressEdit() throws Exception {
		BrowserActions.clickOnElementX(lnkEditBilling, driver, "Edit link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click Add New billing address
	 * @throws Exception - Exception
	 */
	public void clickAddNewBillingAddress() throws Exception {
		BrowserActions.scrollToViewElement(lnkAddNewBilling, driver);
		BrowserActions.clickOnElementX(lnkAddNewBilling, driver, "Edit link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click Add New Credit
	 * @throws Exception - Exception
	 */
	public void clickAddNewCredit() throws Exception {
		BrowserActions.clickOnElementX(lnkAddNewCredit, driver, "Edit link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click saved credit card cancel
	 * @throws Exception - Exception
	 */
	public void clickSaveCreditCardCancel() throws Exception {
		BrowserActions.clickOnElementX(btnSaveCreditCardCancel, driver, "Edit link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click Items In Bag edit
	 * @return ShoppingBagPage
	 * @throws Exception - Exception
	 */
	public ShoppingBagPage clickItemsInBagEdit() throws Exception {
		BrowserActions.clickOnElementX(lnkItemsInBagEdit, driver, "Edit link");
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	/**
	 * To click footer ordering status
	 * @throws Exception - Exception
	 */
	public void clickFooterOrderingStatus() throws Exception {
		BrowserActions.scrollInToView(lnkFooterOrderingStatus, driver);
		BrowserActions.clickOnElementX(lnkFooterOrderingStatus, driver, "Footer link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click header shipping
	 * @throws Exception - Exception
	 */
	public void clickHeaderShipping() throws Exception {
		BrowserActions.clickOnElementX(lnkHeaderShipping, driver, "Header link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click cancel edit shipping address
	 * @throws Exception - Exception
	 */
	public void clickCancelEditShippingAddress() throws Exception {
		BrowserActions.clickOnElementX(lnkCancelShippingAddress, driver, "Cancel link");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get saved address count 
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getSavedAddressesCount() throws Exception {
		return BrowserActions.getText(driver, txtSavedAddressCount, "Saved Address Count");
	}

	/**
	 * To verify if the saved address dropdown is expanded
	 * @return boolean
	 * @throws Exception - Exception
	 * 
	 */
	public Boolean isSavedAddressDropdownExpanded() throws Exception {
		if(drpSelectAddressCurrentItem.getAttribute("class").contains("select"))
			return true;
		else
			return false;
	}

	/**
	 * To expand or collapse save Address Dropdown
	 * @param expand -
	 * @throws Exception -
	 */
	public void expandCollapseSavedAddressDropdown(Boolean expand) throws Exception {
		if (expand) {
			if (isSavedAddressDropdownExpanded() == false) {
				if(Utils.isMobile() && Utils.waitForElement(driver, drpSelectAddressMobile)) {
					BrowserActions.clickOnElementX(drpSelectAddressMobile, driver, "Saved Address dropdown expanded");
				} else if (Utils.waitForElement(driver, drpSelectAddress)) {
					BrowserActions.clickOnElementX(drpSelectAddress, driver, "Saved Address dropdown expanded");
				}
			}
		} else {
			if (isSavedAddressDropdownExpanded()==true) {
				if(Utils.isMobile() && Utils.waitForElement(driver, drpSelectAddressMobile)) {
					BrowserActions.clickOnElementX(drpSelectAddressMobile, driver, "Saved Address dropdown collapsed");
				} else if (Utils.waitForElement(driver, drpSelectAddress)) {
					BrowserActions.clickOnElementX(drpSelectAddress, driver, "Saved Address dropdown collapsed");
				}
			}
		}
	}

	/**
	 * To verify billing address module not clickable
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyBillingAddressModuleNotClickable()throws Exception {
		Boolean flag = false;
		for (WebElement addressline : billingAddressmodules) {
			if(!(Utils.waitUntilElementClickable(driver, addressline, 5))) {
				flag=true;
			}
		}
		return flag;
	}

	/**
	 * To click edit payment detail
	 * @throws Exception - Exception
	 */
	public void clickEditPaymentDetail()throws Exception {
		BrowserActions.scrollInToView(editPaymentDetail, driver);
		BrowserActions.clickOnElementX(editPaymentDetail, driver, "Edit");
		Utils.waitForElement(driver, drpCardType);
	}

	/**
	 * To get saved address in edit payment detail
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSavedAddressinEditPaymentDetail()throws Exception {
		String address = divbillingAddresssection.getText();
		return address;
	}

	/**
	 * To verify billing address module
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyBillingAddressModule()throws Exception {
		Boolean flag = false;
		for (WebElement field : divbillingAddrsContainer) {
			if((Utils.waitForElement(driver, field)) && divbillingAddrsContainer.size() == 9) {
				flag=true;
				break;
			}
		}
		return flag;
	}

	/**
	 * To verify Billing Address Text field is Editable
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyIsBillingAddressTextFieldEditable()throws Exception {
		Boolean flag = true;
		String[] fields= {"firstName","lastName","address1","address2","postal","city","phone"};
		List<WebElement> ListEditableTextfields=driver.findElements(By.cssSelector("input[id^='dwfrm_billing_billingAddress']:not([readonly='readonly'])[type='text']")); 
		for (int i = 0; i < ListEditableTextfields.size(); i++) {
			if(ListEditableTextfields.get(i).getAttribute("id").endsWith(fields[i])) {
				if(fields[i].equals("postal")||fields[i].equals("phone") ) {
					String oldvalue = ListEditableTextfields.get(i).getAttribute("value");
					ListEditableTextfields.get(i).sendKeys("612345");
					String newvalue = ListEditableTextfields.get(i).getAttribute("value");
					ListEditableTextfields.get(i).clear();
					if(oldvalue.equals(newvalue)) {
						flag = false;
						break;
					}
				} else {
					String oldvalue = ListEditableTextfields.get(i).getAttribute("value");					
					ListEditableTextfields.get(i).sendKeys(fields[i]);
					String newvalue = ListEditableTextfields.get(i).getAttribute("value");
					ListEditableTextfields.get(i).clear();
					if(oldvalue.equals(newvalue)) {
						flag = false;
						break;
					}
				}
			} else {
				flag = false;
				break;
			}
		}
		return flag;
	}
	
	/**
	 * To click payment method
	 * @throws Exception - Exception
	 */
	public void clickselectPaymentMethod()throws Exception {
		BrowserActions.clickOnElementX(btnselectPaymentMethod, driver, "Select Payment");
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}

	/**
	 * To click on See More link
	 * @throws Exception - Exception
	 */
	public void clickOnSeeMoreLink()throws Exception {
		BrowserActions.scrollInToView(btnSeeDetails, driver);
		BrowserActions.javascriptClick(btnSeeDetails, driver, "See Details Link in Coupon");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on remove link
	 * @throws Exception - Exception
	 */
	public void removeAppliedCoupon()throws Exception {
		expandOrColapsePromoCodeSection(true);
		BrowserActions.javascriptClick(btnRemoveCoupon, driver, "Remove Link in Coupon");
		Utils.waitForPageLoad(driver);
		Utils.WaitForAjax(driver);
	}

	/**
	 * To apply multiple coupon code
	 * @param couponCode -
	 * @throws Exception - Exception
	 */
	public void applyMultiplePromoCouponCode(String[] couponCode)throws Exception {
		for(int i=0;i<couponCode.length;i++) {
			expandOrColapsePromoCodeSection(true);
			BrowserActions.scrollInToView(txtCouponCode, driver);
			BrowserActions.typeOnTextField(txtCouponCode, couponCode[i], driver, "'Promo Coupon' code");
			BrowserActions.javascriptClick(btnCouponApply, driver, "'PromoCode' Apply button");
			Utils.waitForPageLoad(driver);
			Utils.WaitForAjax(driver);
		}
	}

	/**
	 * TO validate billing address fields
	 * @return true/false
	 * @throws Exception - Exception
	 */
	public Boolean validateIncorrectDataFormatInBillingAddressFields()throws Exception {
		Boolean flag = false;
		Boolean flag1, flag2, flag3, flag4, flag5, flag6;
		flag1 = flag2 = flag3 = flag4 = flag5 = flag6 = false;
		String[] fields= {"First Name","Last Name","Address Line1","Zip Code","State","City","Phone Number"};
		List<WebElement> textboxes=driver.findElements(By.cssSelector("input[id^='dwfrm_billing_billingAddress']:not([readonly='readonly'])[type='text']"));

		for (int i = 0; i < textboxes.size(); i++) {
			textboxes.get(i).clear();
			textboxes.get(i).sendKeys("$*@()");
		}
		
		clickselectPaymentMethod();
		List<WebElement> Errorfields=driver.findElements(By.cssSelector("span[id^=dwfrm_billing].error")); 

		for (int i = 0; i < Errorfields.size(); i++) {
			flag = Utils.verifyCssPropertyForElement(Errorfields.get(i), "color", "rgba(230, 0, 60, 1)");
			
			if(flag) {
				if(Errorfields.get(i).getAttribute("id").contains("postal-error")) {
					if(Errorfields.get(i).getText().equals("Invalid ZIP CODE")) {
						flag1 = true;
						Log.message("Error message is displayed for ZIP CODE");
					} else {
						Log.message("Error message is not displayed for ZIP CODE");
					}
				}
				
				if(Errorfields.get(i).getAttribute("id").contains("phone-error")) {
					if(Errorfields.get(i).getText().equals("Please enter a valid phone number.")) {
						flag2 = true;
						Log.message("Error message is displayed for PHONE NUMBER");
					} else {
						Log.message("Error message is not displayed for PHONE NUMBER");
					}
				}

				if(Errorfields.get(i).getAttribute("id").contains("firstName-error")) {
					if(Errorfields.get(i).getText().equals("Please enter valid firstname")) {
						flag3 = true;
						Log.message("Error message is displayed for FIRST NAME");
					} else {
						Log.message("Error message is not displayed for FIRST NAME");
					}
				}
				
				if(Errorfields.get(i).getAttribute("id").contains("lastName-error")) {
					if(Errorfields.get(i).getText().equals("Please enter valid lastname")) {
						flag4 = true;
						Log.message("Error message is displayed for LAST NAME");
					} else {
						Log.message("Error message is not displayed for LAST NAME");
					}
				}
				
				if(Errorfields.get(i).getAttribute("id").contains("city-error")) {
					if(Errorfields.get(i).getText().equals("Please enter valid city ")) {
						flag5 = true;
						Log.message("Error message is displayed for City");
					} else {
						Log.message("Error message is not displayed for city");
					}
				}
			} else {
				flag6 = false;									
				Log.message("Error message is not displayed for "+fields[i]);
			}
		}

		for (int i = 0; i < textboxes.size(); i++) {			
			textboxes.get(i).clear();
		}
		return flag && flag1 && flag2 && flag3 && flag4 && flag5 && flag6;
	}

	/**
	 * To select state in Billing Address
	 * @param State -
	 * @throws Exception - Exception
	 */
	public void selectStateinBillingAddrs(String State)throws Exception {
		Select dropdown = new Select(driver.findElement(By.id("dwfrm_billing_billingAddress_addressFields_states_state")));
		if(!(State.length()==0)) {
			dropdown.selectByValue(State);
		} else
			dropdown.selectByIndex(1);
	}


	/**
	 * To click on the view more link in order confirmation page
	 * @throws Exception -
	 */
	public void clickOnViewDetailsLink()throws Exception {
		if(Utils.isMobile()) {
			if(Utils.waitForElement(driver, lnkViewDetailsInOrdConf_Mobile)) {
				BrowserActions.clickOnElementX(lnkViewDetailsInOrdConf_Mobile, driver, "View more");
			}
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on the view less link in order confirmation page
	 * @throws Exception -
	 */
	public void clickOnViewLessLink()throws Exception {
		if(Utils.isMobile()) {
			if(Utils.waitForElement(driver, lnkViewLessInOrdConf_Mobile)) {
				BrowserActions.clickOnElementX(lnkViewLessInOrdConf_Mobile, driver, "View less");
			}
		}
		Utils.waitForPageLoad(driver);
	}


	/**
	 * To select country in billing address module
	 * @param Country -
	 * @throws Exception - Exception
	 */
	public void selectCountryinBillingAddrs(String Country)throws Exception {
		Select dropdown = new Select(driver.findElement(By.id("dwfrm_billing_billingAddress_addressFields_country")));
		if(Country.length()>0) {
			dropdown.selectByValue(Country);
		} else
			dropdown.selectByIndex(0);
	}


	/** 
	 * To verify place holder disappear Bill in Address
	 * @param fieldname -
	 * @return true/false
	 * @throws Exception -Exception
	 */
	public Boolean verifyPlaceholderDisappearsBillinAddress(String fieldname)throws Exception {
		Boolean flag = false;
		if(fieldname.equals("state")) {
			selectStateinBilling("Florida");
			WebElement labelWithFocus = driver.findElement(By.cssSelector("label[for='dwfrm_billing_billingAddress_addressFields_states_state'][class*='input-focus']"));
			boolean classproperty=Utils.waitForElement(driver, labelWithFocus);			
			if(classproperty) {
				flag=true;
			} else {
				Log.message("Placeholder did not change for State.");
			}
		} else {

			String randomstring = RandomStringUtils.randomAlphabetic(5);			
			WebElement input=driver.findElement(By.cssSelector("input[id='dwfrm_billing_billingAddress_addressFields_"+fieldname+"']"));
			input.sendKeys(randomstring);
			WebElement labelWithFocus = driver.findElement(By.cssSelector("label[for='dwfrm_billing_billingAddress_addressFields_"+fieldname+"'][class*='input-focus']"));
			boolean classproperty=Utils.waitForElement(driver, labelWithFocus);			
			if(classproperty) {
				flag=true;
			} else {
				Log.message("Placeholder did not change for "+fieldname);
			}
			
			input.clear();
		}
		return flag;
	}


	/**
	 * To verify Placeholder disapplease in billin address
	 * @return true/false
	 * @throws Exception - Exception
	 */
	public Boolean verifyPlaceholderDisappearsBillinAddress()throws Exception {
		Boolean flag = false,before=false,after=false;
		Boolean flag1,flag2,flag3,flag4,flag5;
		flag1=flag2=flag3=flag4=flag5 = false;
		int y1,y2;
		y1=y2=0;
		for (int i = 0; i < lblBillingAddressFields.size(); i++) 
		{
			if((lblBillingAddressFields.get(i).getText().equals("Zip Code")))
			{
				WebElement element=BillingAddressFields.get(i).findElement(By.cssSelector("input[id^='dwfrm_billing_billingAddress']"));	
				y1=lblBillingAddressFields.get(i).getLocation().getY();
				before=Utils.verifyCssPropertyForElement(lblBillingAddressFields.get(i), "color", "rgba(102, 102, 102, 1)");
				element.sendKeys("22134");
				y2=lblBillingAddressFields.get(i).getLocation().getY();
				after=Utils.verifyCssPropertyForElement(lblBillingAddressFields.get(i), "color", "rgba(0, 0, 0, 1)");
				element.clear();
				if(y1>y2&&(before&&after))
				{
					flag1=true;
				}
			}
			else if((lblBillingAddressFields.get(i).getText().equals("Phone Number")))
			{
				WebElement element=BillingAddressFields.get(i).findElement(By.cssSelector("input[id^='dwfrm_billing_billingAddress']"));	
				y1=lblBillingAddressFields.get(i).getLocation().getY();
				before=Utils.verifyCssPropertyForElement(lblBillingAddressFields.get(i), "color", "rgba(102, 102, 102, 1)");
				element.sendKeys("2213477766");
				y2=lblBillingAddressFields.get(i).getLocation().getY();
				after=Utils.verifyCssPropertyForElement(lblBillingAddressFields.get(i), "color", "rgba(0, 0, 0, 1)");
				element.clear();
				if(y1>y2&&(before&&after))
				{
					flag2=true;
				}
			}
			else if(lblBillingAddressFields.get(i).getText().equals("State"))
			{
				before=Utils.verifyCssPropertyForElement(lblBillingAddressFields.get(i), "color", "rgba(102, 102, 102, 1)");
				y1=lblBillingAddressFields.get(i).getLocation().getY();
				selectStateinBillingAddrs("");
				y2=lblBillingAddressFields.get(i).getLocation().getY();
				after=Utils.verifyCssPropertyForElement(lblBillingAddressFields.get(i), "color", "rgba(0, 0, 0, 1)");
				if(y1>y2&&(before&&after))
				{
					flag3=true;
				}
			}
			else if(lblBillingAddressFields.get(i).getText().equals("Country"))
			{
				before=Utils.verifyCssPropertyForElement(lblBillingAddressFields.get(i), "color", "rgba(170, 170, 170, 1)");
				y1=lblBillingAddressFields.get(i).getLocation().getY();
				selectCountryinBillingAddrs("");	
				y2=lblBillingAddressFields.get(i).getLocation().getY();
				after=Utils.verifyCssPropertyForElement(lblBillingAddressFields.get(i), "color", "rgba(0, 0, 0, 1)");
				if(y1>y2&&(before&&after))
				{
					flag4=true;
				}
			}
			else
			{
				WebElement element=BillingAddressFields.get(i).findElement(By.cssSelector("input[id^='dwfrm_billing_billingAddress']"));	
				y1=lblBillingAddressFields.get(i).getLocation().getY();
				before=Utils.verifyCssPropertyForElement(lblBillingAddressFields.get(i), "color", "rgba(102, 102, 102, 1)");
				element.sendKeys("abc");
				y2=lblBillingAddressFields.get(i).getLocation().getY();
				after=Utils.verifyCssPropertyForElement(lblBillingAddressFields.get(i), "color", "rgba(0, 0, 0, 1)");
				element.clear();
				if(y1>y2&&(before&&after))
				{
					flag5=true;
				}
			}


		}
		if(flag1&&flag2&&flag3&&flag4&&flag5) {flag=true;}
		return flag;
	}

	/**
	 * To validate zip code 
	 * @param zipcode -
	 * @return true/false 
	 * @throws Exception -
	 */
	public Boolean ValidateZipCode(String[] zipcode)throws Exception {
		Boolean flag=false;
		txtFieldZipCode.clear();
		for(int i=0;i<zipcode.length;i++)
		{
			txtFieldZipCode.sendKeys(zipcode[i]);		
			WebElement errormsg=null;
			try  {
				errormsg=driver.findElement(By.cssSelector("#dwfrm_billing_billingAddress_addressFields_postal-error"));
				if(errormsg.getAttribute("style").contains("display: none;")) {
					flag=true;
				}
			}
			catch (NoSuchElementException| SecurityException e1) {}
			txtFieldZipCode.clear();
		}
		return flag;
	}

	/**
	 * To fill billing address module is editable
	 * @param useAddressForBilling -
	 * @throws Exception - Exception
	 */
	public void fillBillingAddressTextFieldEditable(String useAddressForBilling) throws Exception {
		List<WebElement> textboxes=driver.findElements(By.cssSelector("input[id^='dwfrm_billing_billingAddress']:not([readonly='readonly'])[type='text']"));
		for (int i = 0; i < textboxes.size(); i++) {			
			textboxes.get(i).clear();
		}
		LinkedHashMap<String, String> billingDetails = new LinkedHashMap<String, String>();
		String randomFirstName = RandomStringUtils.randomAlphanumeric(5)
				.toLowerCase();
		billingDetails.put("type_firstname_QaFirst" + randomFirstName,
				txtbillingFirstName);
		String randomLastName = RandomStringUtils.randomAlphanumeric(5)
				.toLowerCase();
		billingDetails.put("type_lastname_QaLast" + randomLastName,
				txtbillingLastName);

		String address = checkoutProperty.getProperty(useAddressForBilling);
		String address1 = address.split("\\|")[0];
		String address2 = new String();
		String zipcode = address.split("\\|")[3];
		String city = address.split("\\|")[1];
		String state = address.split("\\|")[2];		
		String phoneNo = address.split("\\|")[4];	

		txtFieldAddressline1.sendKeys(address1);
		txtFieldAddressline2.sendKeys(address2);
		txtFieldCity.sendKeys(city);
		txtFieldZipCode.sendKeys(zipcode);
		txtFieldPhone.sendKeys(phoneNo);
		selectStateinBillingAddrs(state);
		selectCountryinBillingAddrs("");	

	}
	
	/**
	 * To click edit in billing address module
	 * @throws Exception - Exception
	 */
	public String getPostalCodeFromBillingAdd()throws Exception {
		return BrowserActions.getTextFromAttribute(driver, inpPostalAddBiling, "value", "Postal code");
	}
	
	/**
	 * To click edit in billing address module
	 * @throws Exception - Exception
	 */
	public String getCityFromBillingAdd()throws Exception {
		return BrowserActions.getTextFromAttribute(driver, txtFieldCity, "value", "Billing address city");
	}
	
	/**
	 * To click edit in billing address module
	 * @throws Exception - Exception
	 */
	public void enterPostalCodeInBillingAdd(String zipCode)throws Exception {
		BrowserActions.typeOnTextField(inpPostalAddBiling, zipCode, driver, "Postal code");
		Log.event("Eneterd ZIP:: " + zipCode);
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get shipping details header title
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getShippingDetailsHeaderTitle() throws Exception{
		Log.event("Shipping detail header:: "+BrowserActions.getText(driver, shippingDetailsHeader, "Shipping Details Header"));
		return BrowserActions.getText(driver, shippingDetailsHeader, "Shipping Details Header");
	}

	/**
	 * To click on brand logo
	 * @throws Exception - Exception
	 */
	public void clickOnBrandLogo()throws Exception {
		BrowserActions.clickOnElementX(brandLogo, driver, "Brand Logo");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on credit card dropdown
	 * @throws Exception
	 */
	public void clickOnCreditCardDrp() throws Exception {
		if(Utils.waitForElement(driver, selectCardType)){
			BrowserActions.javascriptClick(selectCardType, driver, "Credit Catd Type");
		}else if(Utils.waitForElement(driver, drpCardType)){
			BrowserActions.clickOnElementX(drpCardType, driver, "Credit Card Dropdown");
		}else{
			Log.fail("Credit Card dropdown not found in Page");
		}
	}

	/**
	 * To get card type
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getCardTypeSelectListState()throws Exception{
		String stateToBeReturned = "closed";
		try{
			if(Utils.waitForElement(driver, selectCardType)){
				if(selectCardType.findElements(By.cssSelector(".select-option")).get(0).isDisplayed()){
					stateToBeReturned = "opened";
				}
			}else if(Utils.waitForElement(driver, drpCardType)){
				if(driver.findElement(By.cssSelector(btnCardTypeDiv + " + .selection-list")).getCssValue("display").equals("block")){
					stateToBeReturned = "opened";
				}
			}else{
				Log.fail("Credit Card dropdown not found in Page");
			}
		}catch(ArrayIndexOutOfBoundsException e){
			stateToBeReturned = "closed";
		}

		return stateToBeReturned;
	}

	/**
	 * To verify card oreder
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyCardOrder()throws Exception{
		boolean stateToReturn = true;

		Select cardDrp = new Select(selectCardType);
		List<WebElement> cardOpt = cardDrp.getOptions();
		boolean tag = true;

		int i = 0;
		cardOpt.remove(0);
		for(WebElement ele : cardOpt){
			tag = ele.getAttribute("plcctype").equals("false") ? true : false;
			i++;
			if(i==0 && tag==false){
				i++;
				return false;
			}
			if(tag){
				if(!ele.getAttribute("plcctype").equals("false"))
					return false;
			}else{
				if(!ele.getAttribute("plcctype").equals("true"))
					return false;
			}
		}

		return stateToReturn;
	}

	/**
	 * To select card type
	 * @param cardType - Payment Method to select by name
	 * @throws Exception
	 */
	public void selectCardType(String cardType)throws Exception{
		Select cardList = new Select(selectCardType);
		if(cardType.equalsIgnoreCase("Credit Card")) {
			cardList.selectByValue(Utils.getCurrentBrandShort().toUpperCase() + "_PLCC");
		} else {
			try {
				cardList.selectByVisibleText(cardType);
			} catch(Exception e) {
				Log.fail(cardType + " is either not avaialble, or not a valid card type.");
			}
		}
		try {
			selectCardType.sendKeys(Keys.TAB);
		} catch (NoSuchElementException e) {
			Log.event("Select credit card drop-down not displayed.");
		}
	}

	/**
	 * To get number of cards in the payment page card type drop down
	 * @return
	 * @throws Exception
	 */
	public int getNoOfCardsInPaymentPgCardTypeDropDown()throws Exception{
		Select cardDrp = new Select(selectCardType);
		List<WebElement> cardOpt = cardDrp.getOptions();
		return cardOpt.size();
	}

	/**
	 * To get credit card type
	 * @return string 
	 * @throws Exception - Exception
	 */
	public String getCreditCardType()throws Exception{
		String cardTypeToReturn = new String();
		WebElement cardType ;
		if (Utils.isMobile()) {
			cardType = lbl_First_PLCC_Card_Name_Mobile;
		} else {
			cardType = lbl_First_PLCC_Card_Name;
		}
		if(Utils.waitForElement(driver, selectCardType)){
			cardTypeToReturn = selectCardType.getAttribute("value");
		}else if(Utils.waitForElement(driver, drpCardType)){
			cardTypeToReturn = drpCardType.getText();
		} else if(Utils.waitForElement(driver, cardType)) {
			cardTypeToReturn = cardType.getText();
			Log.event("Card is in selected state");
		}else{
			Log.fail("Credit Card dropdown not found in Page");
		}

		Log.event("Credit Card Type :: " + cardTypeToReturn);
		return cardTypeToReturn;
	}

	/**
	 * To set card holder name in payment
	 * @param name -
	 * @throws Exception - Exception
	 */
	public void setCardHolderNameInPayment(String name)throws Exception{
		BrowserActions.typeOnTextField(txtNameOnCard, name, driver, "Credit card holder name field");
	}

	/**
	 * To get card holder name in payment
	 * @return string 
	 * @throws Exception - Exception
	 */
	public String getCardHolderNameInPayment()throws Exception{
		return BrowserActions.getText(driver, txtNameOnCard, "Credit card holder name field");
	}

	/**
	 * To set card number in payment
	 * @param number -
	 * @throws Exception - Exception
	 */
	public void setCardNumberInPayment(String number)throws Exception{
		BrowserActions.typeOnTextField(fldTxtCardNo, number, driver, "Credit card Number field");
	}

	/**
	 * To get card number in payment
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getCardNumberInPayment()throws Exception{
		String cardNumber;
		if(Utils.waitForElement(driver, fldTxtCardNo)) {
			cardNumber = BrowserActions.getText(driver, fldTxtCardNo, "Credit card Number field");
			Log.event("Card number in checkout step 2: " + cardNumber);
		} else {
			cardNumber = BrowserActions.getText(driver, lblCardNumberInMiniPayment, "Credit card Number field");
			Log.event("Card number in checkout step 3: " + cardNumber);
		}
		
		return cardNumber;
	}
	
	/**
	 * Verify if dummy PLCC data received based on pre-populated PLCC card number
	 * <br>
	 * Use only for PLCC guest checkout
	 * @return true/false
	 * @throws Exception
	 */
	public boolean dummyDataRecieved()throws Exception{
		boolean dummyDataFromADS = false;
		String cardNumber = "";
		if(Utils.waitForElement(driver, lblCardNumberInMiniPayment)) {
			cardNumber = BrowserActions.getText(driver, lblCardNumberInMiniPayment, "Credit card Number field");
			dummyDataFromADS = cardNumber.length() >= 12;
		} else {
			cardNumber = getCardNumberInPayment();
			dummyDataFromADS = cardNumber.length() >= 10;			
		}
		Log.reference("PLCC card number:: " + cardNumber, driver);
		if (cardNumber.trim().length() == 0)
			Log.failsoft("PLCC card number recieved is empty.");
		else if(dummyDataFromADS) {
			Log.warning("Dummy PLCC card number sent by ADS.");
		}
		return dummyDataFromADS;
	}

	/**
	 * To set CVV in payment
	 * @param cvv -
	 * @throws Exception - Exception
	 */
	public void setCVVInPayment(String cvv)throws Exception{
		if(Utils.waitForElement(driver, fldTxtCvv)) {
			BrowserActions.typeOnTextField(fldTxtCvv, cvv, driver, "CVV field");
		} else {
			BrowserActions.typeOnTextField(fldTxtCvvNo, cvv, driver, "CVV field");
		}
	}

	/**
	 * To get CVV in payment
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getCVVInPayment()throws Exception{
		WebElement elem = null;
		if(Utils.waitForElement(driver, fldTxtCvvNo)) {
			elem = fldTxtCvvNo;
		} else {
			elem = fldTxtCvv;
		}
		Log.event("CVV From txtBox :: " + BrowserActions.getText(driver, elem, "Credit card Number field"));
		return BrowserActions.getText(driver, elem, "Credit card Number field");
	}
	
	/**
	 * To get CVV in payment
	 * @return string
	 * @throws Exception - Exception
	 */
	public ShoppingBagPage clickEditItemSummary()throws Exception{
		BrowserActions.clickOnElementX(lnkEditItemSummary, driver, "ItemSummary");
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	/**
	 * To click on Expiry Month
	 * @throws Exception - Exception
	 */
	public void clickOnExpiryMonthDrp()throws Exception{
		if(Utils.waitForElement(driver, selectExpMonth)) {
			BrowserActions.javascriptClick(selectExpMonth, driver, "Credit Card Dropdown");
		} else if(Utils.waitForElement(driver, drpExpMonth)) {
			BrowserActions.clickOnElementX(drpExpMonth, driver, "Credit Card Dropdown");
		} else {
			Log.fail("Expiry Month dropdown not found in Page", driver);
		}
	}

	/**
	 * To select expiry month
	 * @param expMonth -
	 * @throws Exception - Exception
	 */
	public void selectExpiryMonth(String expMonth)throws Exception{
		if(Utils.waitForElement(driver, selectExpMonth)){
			BrowserActions.selectDropdownByValue(selectExpMonth, expMonth);
		}else if(Utils.waitForElement(driver, drpExpMonth)){
			BrowserActions.clickOnElementX(drpExpMonth, driver, "Exp Month Dropdown");
			WebElement element = driver.findElement(By.cssSelector("select[id='dwfrm_billing_paymentMethods_creditCard_expiration_month'] + .selected-option + .selection-list li[label='"+expMonth+"']"));
			BrowserActions.clickOnElementX(element, driver, "Exp Month");
		}else{
			Log.fail("Expiry Month dropdown not found in Page", driver);
		}
	}

	/**
	 * To get expiry month value
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getExpiryMonth()throws Exception{
		String expMonthToReturn = new String();

		if(Utils.waitForElement(driver, selectExpMonth)){
			expMonthToReturn = driver.findElement(By.cssSelector("select[id='dwfrm_billing_paymentMethods_creditCard_expiration_month'] + .selected-option")).getAttribute("innerHTML");
		}else if(Utils.waitForElement(driver, drpExpMonth)){
			expMonthToReturn = drpExpMonth.getText();
		}else{
			Log.fail("Expiry Month dropdown not found in Page");
		}

		Log.event("Selected EXP Month :: " + expMonthToReturn);
		return expMonthToReturn;
	}

	/**
	 * To verify expiry year
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyExpYearDrp() throws Exception {
		boolean state = true;
		int year = Year.now().getValue();
		if (Utils.waitForElement(driver, selectExpYear)) {
			Select qty = new Select(selectExpYear);

			int i=0;
			for(WebElement element : qty.getOptions()){
				if(i==0){i++; continue;}
				if (Integer.parseInt(element.getText()) != year)
					return false;
				year++;
			}
			return true;
		}
		try {
			for (int i = 0; i <= 10; i++) {
				if (!lstExpYear.get(i).getAttribute("label").trim().equals(Integer.toString(year + i)))
					state = false;
			}
		} catch (StaleElementReferenceException e) {
			return false;
		}

		return state;
	}

	/**
	 * To verify expiry month
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyExpMonthDrp() throws Exception {
		boolean state = true;
		List<String> yearList = Arrays.asList("Expiry Month","January","February","March","April","May","June","July","August","September","October","November","December");
		if (Utils.waitForElement(driver, selectExpMonth)) {
			Log.event("Inside Select Condition Block");
			Select qty = new Select(selectExpMonth);
			int x = 0;
			for(WebElement element : qty.getOptions()){
				if (!element.getText().trim().toLowerCase().equals(yearList.get(x).toLowerCase()))
					return false;
				x++;
			}
			return true;
		}
		try {
			Log.event("Inside Div Condition Block");
			for (int i = 1; i <= 12; i++) {
				Log.event(i + ". Text From Element :: " + lstExpMonth.get(i-1).getText().trim().toLowerCase());
				Log.event(i + ". Text From List :: " + yearList.get(i).toLowerCase());
				if (!lstExpMonth.get(i-1).getText().trim().toLowerCase().equals(yearList.get(i).toLowerCase()))
					return false;
			}
		} catch (StaleElementReferenceException e) {
			return false;
		}

		return state;
	}

	/**
	 * To click expiry year dropdown
	 * @throws Exception - Exception
	 */
	public void clickOnExpiryYearDrp()throws Exception{
		if(Utils.waitForElement(driver, selectExpYear)){
			BrowserActions.clickOnElementX(selectExpYear, driver, "Credit Card Dropdown");
		}else if(Utils.waitForElement(driver, drpExpYear)){
			BrowserActions.clickOnElementX(drpExpYear, driver, "Credit Card Dropdown");
		}else{
			Log.fail("Expiry Month dropdown not found in Page");
		}
	}

	/**
	 * To select Expiry year
	 * @param expYear -
	 * @throws Exception - Exception
	 */
	public void selectExpiryYear(String expYear)throws Exception{
		if(Utils.waitForElement(driver, selectExpYear)){
			BrowserActions.selectDropdownByValue(selectExpYear, expYear);
		}else if(Utils.waitForElement(driver, drpExpYear)){
			BrowserActions.clickOnElementX(drpExpYear, driver, "Exp Month Dropdown");
			WebElement element = driver.findElement(By.cssSelector("select[id='dwfrm_billing_paymentMethods_creditCard_expiration_year'] + .selected-option + .selection-list li[label='"+expYear+"']"));
			BrowserActions.clickOnElementX(element, driver, "Expiry Year");
		}else{
			Log.fail("Expiry Year dropdown not found in Page");
		}
	}

	/**
	 * To get expiry year
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getExpiryYear()throws Exception{
		String expYearToReturn = new String();

		if(Utils.waitForElement(driver, selectExpYear)){
			expYearToReturn = selectExpYear.getAttribute("value");
		}else if(Utils.waitForElement(driver, drpExpYear)){
			expYearToReturn = drpExpYear.getText();
		}else{
			Log.fail("Expiry Year dropdown not found in Page");
		}

		return expYearToReturn;
	}

	/**
	 * To get expiry month selected list state
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getExpMonSelectListState()throws Exception{
		String stateToBeReturned = "closed";
		if(Utils.waitForElement(driver, selectCardType)){
			if(selectCardType.findElements(By.cssSelector("select-option")).get(0).isDisplayed()){
				stateToBeReturned = "opened";
			}
		}else if(Utils.waitForElement(driver, drpCardType)){
			if(driver.findElement(By.cssSelector(btnCardTypeDiv + " + .selection-list")).getAttribute("display").equals("block")){
				stateToBeReturned = "opened";
			}
		}else{
			Log.fail("Credit Card dropdown not found in Page");
		}

		return stateToBeReturned;
	}

	/**
	 * To check Make This Default Payment checkbox
	 * @param state -
	 * @throws Exception - Exception
	 */
	public void checkMakeThisDefaultPaymentChkBox(boolean state)throws Exception{
		if(chkBoxMakeThisDefaultPayment.isSelected() && state){
			Log.event("Make this default check box already selected");
		}else {
			BrowserActions.clickOnElementX(chkBoxMakeThisDefaultPayment, driver, "Make this default check box");
		}
	}

	/**
	 * To check Make Save Card checkbox
	 * @param state -
	 * @throws Exception - Exception
	 */
	public void checkMakeSaveCardChkBox(boolean state)throws Exception{
		if(chkBoxSaveCard.isSelected() && state){
			Log.event("Save Card check box already selected");
		}else {
			BrowserActions.clickOnElementX(chkBoxSaveCard, driver, "Save card check box");
		}
	}

	/**
	 * To get num of saved cards
	 * @return int
	 * @throws Exception - Exception
	 */
	public int getNoOfSavedCards()throws Exception{
		return lstSavedCards.size();
	}

	/**
	 * To click on save link in payments
	 * @throws Exception - Exception
	 */
	public void clickOnSaveLinkInPayments()throws Exception{
		Log.event("Trying to click on save link...");
		BrowserActions.clickOnElementX(lnkSaveInPayments, driver, "Save Link in Add New Payment method section");
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}

	/**
	 * To click on cancel link in payments
	 * @throws Exception - Exception
	 */
	public void clickOnCancelLinkInPayments()throws Exception{
		BrowserActions.clickOnElementX(lnkCancelInPayments, driver, "Save Link in Add New Payment method section");
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}

	/**
	 * To filling card details
	 * @param chkoutCardName -
	 * @param saveCard -
	 * @return HashMap
	 * @throws Exception -Exception
	 */
	public HashMap<String, String> fillingCardDetails1(String chkoutCardName, boolean saveCard)throws Exception{
		HashMap<String, String> cardInfoToReturn = new HashMap<String, String>();

		String cardInfo = checkoutProperty.getProperty(chkoutCardName);
		selectCardType(cardInfo.split("\\|")[0]);
		setCardHolderNameInPayment(cardInfo.split("\\|")[1]);
		setCardNumberInPayment(cardInfo.split("\\|")[2]);
		selectExpiryMonth(cardInfo.split("\\|")[3]);
		selectExpiryYear(cardInfo.split("\\|")[4]);
		setCVVInPayment(cardInfo.split("\\|")[5]);

		cardInfoToReturn.put("CardType", cardInfo.split("\\|")[0]);
		cardInfoToReturn.put("Name", cardInfo.split("\\|")[1]);
		cardInfoToReturn.put("CardNumber", cardInfo.split("\\|")[2]);
		cardInfoToReturn.put("ExpMonth", cardInfo.split("\\|")[3]);
		cardInfoToReturn.put("ExpYear", cardInfo.split("\\|")[4]);

		if(saveCard)
			checkMakeSaveCardChkBox(saveCard);

		return cardInfoToReturn;
	}
	
	
	/**
	 * Returns remaining order total if gift card/reward certificate applied.
	 * Returns original amount if nothing applied
	 * @return double - Remaining order total
	 * @throws Exception
	 */
	public double getRemainingTotal() throws Exception{
		if(Utils.waitForElement(driver, orderSummaryRemainingTotalValue)) {
			return StringUtils.getPriceFromString(orderSummaryRemainingTotalValue.getText());
		}else
			return StringUtils.getPriceFromString(lblOrderSummaryTableTotal.getText());
	}

	/**
	 * To filling card details
	 * @param chkoutCardName -
	 * @param saveCard -
	 * @param makeItDefault -
	 * @return HashMap
	 * @throws Exception -Exception
	 */
	public HashMap<String, String> fillingCardDetails1(String chkoutCardName, boolean saveCard, boolean makeItDefault)throws Exception{
		HashMap<String, String> cardInfoToReturn = new HashMap<String, String>();
		String cardInfo = checkoutProperty.getProperty(chkoutCardName);
		Log.event("Card info:: " + cardInfo);
		
		if(cardInfo.split("\\|")[0].contains("Credit Card")) {
			cardInfoToReturn.put("CardType", cardInfo.split("\\|")[0]);
			cardInfoToReturn.put("CardNumber", cardInfo.split("\\|")[1]);
		} else {
			cardInfoToReturn.put("CardType", cardInfo.split("\\|")[0]);
			cardInfoToReturn.put("Name", cardInfo.split("\\|")[1]);
			cardInfoToReturn.put("CardNumber", cardInfo.split("\\|")[2]);
			cardInfoToReturn.put("ExpMonth", cardInfo.split("\\|")[3]);
			cardInfoToReturn.put("ExpYear", cardInfo.split("\\|")[4]);
			cardInfoToReturn.put("CVV", cardInfo.split("\\|")[5]);
		}
		
		if(getRemainingTotal() == 0.0) {
			Log.reference("There is no remaining balance on current order.");
		} else {
			if(cardInfo.split("\\|")[0].contains("Credit Card")) {
				Log.event("Entering PLCC card info.");
				selectCardType(cardInfo.split("\\|")[0]);
				setCardNumberInPayment(cardInfo.split("\\|")[1]);
			} else {
				Log.event("Entering non-PLCC/regular card info.");
				selectCardType(cardInfo.split("\\|")[0]);
				setCardHolderNameInPayment(cardInfo.split("\\|")[1]);
				setCardNumberInPayment(cardInfo.split("\\|")[2]);
				selectExpiryMonth(cardInfo.split("\\|")[3]);
				selectExpiryYear(cardInfo.split("\\|")[4]);
				setCVVInPayment(cardInfo.split("\\|")[5]);
			}

			if(saveCard)
				checkMakeSaveCardChkBox(saveCard);
			if(makeItDefault)
				checkMakeThisDefaultPaymentChkBox(makeItDefault);
		}
		return cardInfoToReturn;
	}

	/**
	 * To select saved card
	 * @param index - Card index(optional)
	 * @return HashMap<String, String> - card details
	 * @throws Exception - Exception
	 */
	public HashMap<String, String> selectSavedCard(int... index)throws Exception{
		HashMap<String, String> cardInfo = new HashMap<String, String>();

		List<WebElement> lstSaveCard = driver.findElements(By.cssSelector("div.carddetails"));
		if(index.length > 0){
			BrowserActions.clickOnElementX(lstSaveCard.get(index[0] - 1), driver, index + " nth Card in Saved Card List");
			Utils.waitUntilElementDisappear(driver, waitLoader);
		}else{
			if(lstSaveCard.size() == 0)
				lstSaveCard = driver.findElements(By.cssSelector(".carddetails"));
			BrowserActions.clickOnElementX(lstSaveCard.get(0), driver, "1st Card in Saved Card List");
			Utils.waitUntilElementDisappear(driver, waitLoader);
		}

		cardInfo.put("CardType", lblSelectedCard.findElement(By.cssSelector(".cardname.hide-mobile")).getText());
		cardInfo.put("Name", lblSelectedCard.findElement(By.cssSelector(".cardholder")).getText());
		cardInfo.put("CardNumber", lblSelectedCard.findElement(By.cssSelector(".cardnumber")).getText().split(" ")[3]);
		cardInfo.put("ExpMonth", DateTimeUtility.getMonthNameOf(Integer.parseInt(lblSelectedCard.findElement(By.cssSelector(".expdate")).getText().split(" ")[1].split("\\/")[0])));
		String ExpYr = lblSelectedCard.findElement(By.cssSelector(".expdate")).getText();
		Log.event("Before Splitting : "+ExpYr+"|| After Splitting year : " +ExpYr.substring(ExpYr.length() - 2, ExpYr.length()));
		cardInfo.put("ExpYear", "20" + (ExpYr.substring(ExpYr.length() - 2, ExpYr.length())).trim());
		return cardInfo;
	}

	/**
	 * To verify shipping method exception
	 * @return boolean - true if Shipping exception displays, else false
	 * @throws Exception - Exception
	 */
	public Boolean verifyShippingmethodException()throws Exception{
		return (Utils.waitForElement(driver, txtNoShippingException));
	}

	/**
	 * To verify shipping method exception not clickable
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyShippingMethodExceptionNotClickable()throws Exception {
		for (WebElement exception : lstShippingMethods) {
			if(Utils.waitUntilElementClickable(driver, exception, 20))
				return false;
		}
		return true;
	}

	/**
	 * To verify shipping surcharge
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyShippingSurcharge()throws Exception {
		if(Utils.waitForElement(driver, txtShippingSurcharge))
		{
			if(txtShippingSurcharge.getText().contains("$") && txtShippingSurcharge.getText().toLowerCase().contains("surcharge"))
				return true;
			else
				return false;
		}
		else
			return false;

	}

	/**
	 * To get number of shipping methods available
	 * @return int
	 * @throws Exception - Exception
	 */
	public int getNumberOfShippingMethodsAvailable()throws Exception {
		return lblShippingMethodName.size();
	}

	/**
	 * To get shipping method name
	 * @param index -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getShippingMethodsName(int index)throws Exception {
		return lblShippingMethodName.get(index).getText().trim();
	}

	/**
	 * To open/close live chat
	 * @param state -
	 * @throws Exception - Exception
	 */
	public void openCloseLiveChat(String state) throws Exception{
		if(state.equals("open")){
			if(Utils.waitForElement(driver, mdlLiveChat))
				Log.event("Bold Chat - Chat modal opened.");
			else
				BrowserActions.clickOnElementX(lnkLiveChat, driver, "Live chat link");
		}else {
			if(Utils.waitForElement(driver, icoLiveChat)){
				BrowserActions.clickOnElementX(icoLiveChat, driver, "Bold Chat Icon");
			}
			new Headers(driver).get().closeLiveChat();
		}
	}

	/**
	 * To expand/collapse Need Help
	 * @param state -
	 * @throws Exception - Exception
	 */
	public void expandCollapseNeedHelp(String state)throws Exception{
		if(state.equals("open") && cntNeedHelp.isDisplayed()){
			Log.event("Need Help Content opened");
		}else{
			BrowserActions.clickOnElementX(lnkNeedHelpMobile, driver, "Need Help Link in Mobile view");
			Log.event("Need Help content " + state + "ed");
		}
	}


	/**
	 * To click show all cards
	 * @throws Exception - Exception
	 */
	public void clickOnShowAllCards() throws Exception{
		BrowserActions.clickOnElementX(lnkShowAll, driver, "Show All Cards");
	}

	/**
	 * To check subtotal position
	 * @param obj -
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean checkSubTotalPosition(Object obj)throws Exception{
		return elementLayer.verifyAttributeForElement("lstOrderElement", "class", "order-subtotal order-detai", obj);
	}

	/**
	 * To click on card holder name
	 * @param index -
	 * @throws Exception - Exception
	 */
	public void clickOnCardHolderName(int index) throws Exception{
		BrowserActions.clickOnElementX(txtCardHolder.get(index-1), driver, "Card Holder Name");
	}

	/**
	 * To enter CVV
	 * @param CVV -
	 * @throws Exception - Exception
	 */
	public void enterCVV(String CVV) throws Exception{
		WebElement elem = null;
		if(Utils.waitForElement(driver, fldTxtCvvNo)) {
			elem = fldTxtCvvNo;
		} else {
			elem = fldTxtCvv;
		}
		BrowserActions.typeOnTextField(elem, CVV, driver, "CVV field");
	}

	/**
	 * To get entered CVV
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getEnteredCVV() throws Exception{
		String cvv = BrowserActions.getText(driver, fldCVV, "CVV Tool Tip Close");
		Log.event("Entered CVV :: "+cvv);
		return cvv;
	}

	/**
	 * To click on Add New Credit card
	 * @throws Exception - Exception
	 */
	public void clickOnAddNewCreditCard() throws Exception{
		BrowserActions.clickOnElementX(btnAddNewCreditCard, driver, "Add New Credit Card");
	}

	/**
	 * To verify Sales Tax
	 * @param salesTax -
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifySalesTax(int salesTax )throws Exception{
		String sales = txtSalesTaxValue.getText();
		int ExactValue = Integer.parseInt(sales.split("\\$")[1]);

		return(ExactValue == salesTax);
	}
	
	/**
	 * To get discount amount from order summary section
	 * @return value of order discount found in summary
	 * <br> return 0d if no discount found.
	 * @throws Exception
	 */
	public double getOrderSummaryDiscount() {
		if (Utils.waitForElement(driver, orderSummaryDiscountValue)) {
			Double discount = StringUtils.getPriceFromString(orderSummaryDiscountValue.getText());
			Log.event("Order discount:: " + discount);
			return discount;
		}
		Log.event("No order discount available");
		return 0d;
	}

	/**
	 * To order total calculation
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderTotalCalculation()throws Exception{
		boolean flag = false;
		double subtotal = 0.00;
		double shipping = 0.00;
		double discount = 0.00;
		double options = 0.00;
		double tax = 0.00;
		double orderTotal = 0.00;
		DecimalFormat df = new DecimalFormat("#.##");
		
		try {
			if(!txtSubtotalValue.getText().contains(",")) {
				subtotal = Double.parseDouble(txtSubtotalValue.getText().split("\\$")[1]);
			} else {
				subtotal = Double.parseDouble(txtSubtotalValue.getText().split("\\$")[1].replaceAll(",",""));
			}
			shipping = Double.parseDouble(divShippingMessagePrice.getText().split("\\$")[1]);
			
			if(Utils.waitForElement(driver, orderSummaryShippingDiscountValue)) {
				discount = Double.parseDouble(orderSummaryShippingDiscountValue.getText().split("\\$")[1]);
			}
			
			if(Utils.waitForElement(driver, orderSummaryDiscountValue)) {
				discount += Double.parseDouble(orderSummaryDiscountValue.getText().split("\\$")[1]);
			}
			
			if(Utils.waitForElement(driver, orderSummaryOptionCostValue)) {
				options = Double.parseDouble(orderSummaryOptionCostValue.getText().split("\\$")[1]);
			}
			
			if(!(txtSalesTaxValue.getText().contains("-"))) {
				tax = Double.parseDouble(txtSalesTaxValue.getText().split("\\$")[1]);
			}
			
			if(!lblOrderSummaryTableTotal.getText().contains(",")) {
				orderTotal = Double.parseDouble(lblOrderSummaryTableTotal.getText().split("\\$")[1]);
			} else {
				orderTotal = Double.parseDouble(lblOrderSummaryTableTotal.getText().split("\\$")[1].replaceAll(",",""));
			}
			
			if(orderTotal == Double.parseDouble(df.format((subtotal - discount ) + (options + shipping + tax) ))){
				flag = true;
			}
		
		}catch(NoSuchElementException | ArrayIndexOutOfBoundsException e){
			Log.fail("Expected element not available in Order summary section in checkout page.", driver);
		}
		return flag;
	}

	/**
	 * To get header step status
	 * @param stepNumber -
	 * @return String -
	 * @throws Exception - Exception
	 */
	public String getHeaderStepStatus(int stepNumber)throws Exception{
		if(stepNumber == 1){
			if(headerChkoutStep1.getAttribute("class").contains("active"))
				return "active";
			else
				return "inactive";
		}else if(stepNumber == 2){
			if(headerChkoutStep2.getAttribute("class").contains("active"))
				return "active";
			else
				return "inactive";
		}else{
			if(headerChkoutStep3.getAttribute("class").contains("active"))
				return "active";
			else
				return "inactive";
		}
	}

	/**
	 * To click on checkout step
	 * @param stepNo -
	 * @throws Exception - Exception
	 */
	public void clickOnCheckoutStep(int stepNo)throws Exception{
		if(stepNo == 1){
			BrowserActions.clickOnElementX(headerChkoutStep1, driver, "Checkout Step-1");
		}else if(stepNo == 2){
			BrowserActions.clickOnElementX(headerChkoutStep2, driver, "Checkout Step-2");
		}else{
			BrowserActions.clickOnElementX(headerChkoutStep3, driver, "Checkout Step-3");
		}

		Utils.waitForPageLoad(driver);
	}


	/**
	 * To click on edit in shopping bag list
	 * @return ShoppingBagPage
	 * @throws Exception - Exception
	 */
	public ShoppingBagPage clickOnEditInShoppingBagList()throws Exception{
		BrowserActions.clickOnElementX(lnkEditInShoppingBagList, driver, "Edit Link in Shopping Bag List");
		Utils.waitForPageLoad(driver);
		return new ShoppingBagPage(driver).get();
	}

	/**
	 * To fill first name
	 * @param firstname -
	 * @throws Exception - Exception
	 */
	public void fillFirstnameShipping(String firstname)throws Exception{
		if(Utils.waitForElement(driver, txtFirstnameShipping))	{
			BrowserActions.typeOnTextField(txtFirstnameShipping, firstname, driver, "First Name");
			Utils.waitForPageLoad(driver);
		} else {
			Log.fail("Could not find firstname field "+txtFirstnameShipping);
		}
	}	
	
	/**
	 * To Open / Close PLCC Rebuttal Modal By Clicking Learn More link in PLCC Rebuttal Section
	 * @param state -
	 * 			- "open"  - To Open PLCC Rebuttal Modal
	 * 			- "close"  - To Close PLCC Rebuttal Modal
	 * @throws Exception - Exception
	 */
	public void openClosePLCCRebuttal(String state)throws Exception{
		try{
			BrowserActions.scrollToTopOfPage(driver);
			BrowserActions.scrollToViewElement(lnkLearnMoreInPLCCRebuttal, driver);
			if(state.equals("open") && Utils.waitForElement(driver, mdlPLCCAcquisitionRebuttal))
				Log.event("PLCC Rebuttal Modal opened.");
			else{
				if(state.equals("open")){
					BrowserActions.clickOnElementX(lnkLearnMoreInPLCCRebuttal, driver, "Learn More link in PLCC Rebuttal Section");
					Log.event("Clicked on Learn More Button in PLCC Rebuttal");
				} else {
					BrowserActions.clickOnElementX(btnNoThanksInPLCCAcquisition, driver, "No Thanks Button");
				}
				
				Utils.waitUntilElementDisappear(driver, waitLoader);
				Log.event("PLCC Rebuttal Modal "+state+"ed");
			}
		}catch(NoSuchElementException e){
			Log.fail("PLCC Rebuttal not present in this page.", driver);
		}
	}

	/**
	 * To fill last name
	 * @param lastname -
	 * @throws Exception - Exception
	 */
	public void fillLastnameShipping(String lastname)throws Exception{
		if(Utils.waitForElement(driver, txtLastnameShipping)) {
			BrowserActions.typeOnTextField(txtLastnameShipping, lastname, driver, "First Name");
		} else {
			Log.fail("Could not find Lastname field "+txtLastnameShipping);
		}
	}

	/**
	 * To fill address line 1
	 * @param addressline1 -
	 * @throws Exception - Exception
	 */
	public void fillAddressline1Shipping(String addressline1)throws Exception{
		if(Utils.waitForElement(driver,txtAddressline1Shipping )) {
			BrowserActions.typeOnTextField(txtAddressline1Shipping, addressline1, driver, "First Name");
		} else {
			Log.fail("Could not find Lastname field "+txtAddressline1Shipping);
		}
	}

	/**
	 * To fill address line 2
	 * @param addressline2 -
	 * @throws Exception - Exception
	 */
	public void fillAddressline2Shipping(String addressline2)throws Exception{
		if(Utils.waitForElement(driver,txtShippingAdd2 )) {
			BrowserActions.typeOnTextField(txtShippingAdd2, addressline2, driver, "First Name");
		} else {
			Log.fail("Could not find Lastname field "+txtShippingAdd2);
		}
	}

	/**
	 * To fill zipcode
	 * @param zipcode -
	 * @throws Exception - Exception
	 */
	public void fillZipCodeShipping(String zipcode)throws Exception{
		if(Utils.waitForElement(driver,txtZipcode )) {
			BrowserActions.typeOnTextField(txtZipcode, zipcode, driver, "First Name");
		} else {
			Log.fail("Could not find Lastname field "+txtZipcode);
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To fill city
	 * @param city -
	 * @throws Exception - Exception
	 */
	public void fillCityShipping(String city)throws Exception{
		if(Utils.waitForElement(driver,txtCity )) {
			BrowserActions.typeOnTextField(txtCity, city, driver, "First Name");
		} else {
			Log.fail("Could not find Lastname field "+txtCity);
		}
	}

	/**
	 * To fill phone number
	 * @param phone - Phone number to type in
	 * @throws Exception
	 */
	public void fillPhoneShipping(String phone) throws Exception{
		if(Utils.waitForElement(driver,txtPhone )) {
			BrowserActions.typeOnTextField(txtPhone, phone, driver, "First Name");
		} else {
			Log.fail("Could not find Phone number field " + txtPhone);
		}
	}

	/** 
	 * To click on First name field 
	 * @throws Exception - Exception
	 */
	public void clickFirstname()throws Exception{
		if(Utils.waitForElement(driver,txtFirstnameShipping )) {
			txtFirstnameShipping.click();
		}
		else {
			Log.fail("Could not find firstname field" +txtFirstnameShipping);
		}
	}

	/**
	 * To remove items in PO Box overlay
	 * @throws Exception - Exception
	 */
	public CheckoutPage clickRemoveItemsPOBoxOverlay()throws Exception{
		BrowserActions.javascriptClick(btnPoBoxBlockOverlayRemoveItems, driver, "Po Box Overlay Edit Ship Add");
		Utils.waitForPageLoad(driver);
		return new CheckoutPage(driver).get();
	}

	/**
	 * To fill address line 1
	 * @param addressline1 - address line
	 * @throws Exception - Exception
	 */
	public void enterAddressLine1(String addressline1)throws Exception{
		txtAddressline1Shipping.clear();
		txtAddressline1Shipping.sendKeys(Keys.TAB);
		if(Utils.waitForElement(driver,txtAddressline1Shipping )) {
			BrowserActions.typeOnTextField(txtAddressline1Shipping, addressline1, driver, "Address Line1");
			if (!Utils.waitForElement(driver, divPoBoxBlockOverlay)) {
				txtAddressline1Shipping.sendKeys(Keys.TAB);
			}
		}
	}

	/**
	 * To click edit shipping address in PO Box overlay
	 * @throws Exception - Exception
	 */
	public void clickEditShipAddPOBoxOverlay()throws Exception{
		if(Utils.waitForElement(driver,btnPoBoxBlockOverlayEditShipAdd )) {
			BrowserActions.clickOnElementX(btnPoBoxBlockOverlayEditShipAdd, driver, "Po Box Overlay Edit Ship Add");
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To scroll to item list section
	 * @throws Exception - Exception
	 */
	public void scrollToItemsListSection()throws Exception{
		BrowserActions.scrollToViewElement(lstItemsInCartList.get(0), driver);
		Log.event("Scrolled to Items List Section");
	}

	/**
	 * To get the shipping method of SKU
	 * @param sku -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getShippingMethodOfSku(String sku)throws Exception{
		String shippingMethod = new String();

		WebElement methodEle = driver.findElement(By.xpath("//div[contains(@class,'sku')]//span[contains(text(),'"+sku+"')]//ancestor::div[contains(@class,'item-details')]//span[contains(text(),'Delivery:')]//following-sibling::span"));
		shippingMethod = BrowserActions.getText(driver, methodEle, "Shipping Method");
		Log.event("Sku("+ sku +") :: Shipping Method :: " + shippingMethod);

		return shippingMethod;
	}

	/**
	 * To get the shipping method of product
	 * @param prdName -
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getShippingMethodOfProduct(String prdName)throws Exception{
		String shippingMethod = new String();

		WebElement methodEle = driver.findElement(By.xpath("//div[contains(@class,'name')]//a[contains(translate(@title,'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'"+prdName.toLowerCase()+"')]//ancestor::div[contains(@class,'item-details')]//span[contains(text(),'Delivery:')]//following-sibling::span"));
		shippingMethod = BrowserActions.getText(driver, methodEle, "Shipping Method");
		Log.event("Sku("+ prdName +") :: Shipping Method :: " + shippingMethod);

		return shippingMethod;
	}

	/**
	 * To navigate to PDP by prod img of SKU
	 * @param sku -
	 * @return PdpPage
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPDPByProductImgOfSku(String sku)throws Exception{
		try{
			WebElement prdImg = driver.findElement(By.xpath("//span[contains(text(),'"+sku+"')]//ancestor::div[contains(@class,'cart-row')]//div[@class='item-image']//a"));
			BrowserActions.clickOnElementX(prdImg, driver, "Product Image of " + sku);
		}catch(NoSuchElementException e){
			Log.fail("No Such(" + sku + ") Product present in Cart List");
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}

	/**
	 * To navigate to PDP by prod img
	 * @param prdName -
	 * @return PdpPage 
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPDPByProductImg(String prdName)throws Exception{
		try{
			WebElement prdImg = driver.findElement(By.xpath("//div[contains(@class,'name')]//a[contains(translate(@title,'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'"+prdName.toLowerCase()+"')]//ancestor::div[contains(@class,'cart-row')]//div[@class='item-image']//a"));
			BrowserActions.clickOnElementX(prdImg, driver, "Product Image of " + prdName);
		}catch(NoSuchElementException e){
			Log.fail("No Such(" + prdName + ") Product present in Cart List");
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}

	/**
	 * To click Edit shipping address in PO Box overlay
	 * @throws Exception
	 */
	public void clickEditShippingAddressPOBoxOverlay()throws Exception{
		if(Utils.waitForElement(driver,btneditShippingAddress ))
		{
			btneditShippingAddress.click();
		}
		else {
			Log.fail("Could not find Edit shipping address button. "+btneditShippingAddress);
		}

	}

	/**
	 * To get items in cart count
	 * @return Number of products in Cart
	 * @throws Exception - Exception
	 */
	public int itemsinCartCount()throws Exception{
		int count = lstItemsInCartList.size();
		return count;
	}

	/**
	 * To get items price from cart list
	 * @param index -
	 * @return float
	 * @throws Exception - Exception
	 */
	public float getItemsPricefromCartList(int index)throws Exception{

		if(Utils.waitForElement(driver, listItemsPrice.get(index)))
			return Float.parseFloat(listItemsPrice.get(index).getText().trim().replace("$", ""));
		return 0;
	}

	/**
	 * To navigate to PDP by prod name of SKU
	 * @param sku -
	 * @return PdpPage
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPDPByProductNameOfSku(String sku)throws Exception{
		try{
			WebElement prdNameLink = driver.findElement(By.xpath("//span[contains(text(),'"+sku+"')]//ancestor::div[contains(@class,'cart-row')]//div[@class='name']//a"));
			BrowserActions.clickOnElementX(prdNameLink, driver, "Product Image of " + sku);
		}catch(NoSuchElementException e){
			Log.fail("No Such(" + sku + ") Product present in Cart List");
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}

	/**
	 * To navigate to PDP by prod name
	 * @param prdName -
	 * @return PdpPage
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPDPByProductName(String prdName)throws Exception{
		try{
			WebElement prdNameLink = driver.findElement(By.xpath("//div[contains(@class,'name')]//a[contains(translate(@title,'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'"+prdName.toLowerCase()+"')]"));
			BrowserActions.clickOnElementX(prdNameLink, driver, "Product Image of " + prdName);
		}catch(NoSuchElementException e){
			Log.fail("No Such(" + prdName + ") Product present in Cart List");
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}


	/**
	 * To verify error message for invalid address in shipping address
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean errShippingFirstName()throws Exception{
		Boolean flag = false;
		if(txtFirstName.getAttribute("class").contains("error"))
			flag = true;
		else
			return false;

		if(txtLastName.getAttribute("class").contains("error"))
			flag = true;
		else
			return false;

		if(txtAddress1.getAttribute("class").contains("error"))
			flag = true;
		else
			return false;

		if(txtAddress2.getAttribute("class").contains("error"))
			flag = true;
		else
			return false;

		if(txtZipcode.getAttribute("class").contains("error"))
			flag = true;
		else
			return false;

		if(txtPhone.getAttribute("class").contains("error"))
			flag = true;
		else
			return false;

		return flag;
	}


	/**
	 * To verify error message for invalid address in shipping address
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyErrorMessageInBillingAddressInvalidData()throws Exception{
		Boolean flag = false;
		if(txtFirstNameBillingDetails.getAttribute("class").contains("error"))
			flag = true;
		else
			return false;

		if(txtLastNameBillingDetails.getAttribute("class").contains("error"))
			flag = true;
		else
			return false;

		if(txtZipcodeBillingDetails.getAttribute("class").contains("error"))
			flag = true;
		else
			return false;

		if(txtPhoneBillingDetails.getAttribute("class").contains("error"))
			flag = true;
		else
			return false;

		return flag;
	}

	/**
	 * To get guest email error
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getGuestEmailError()throws Exception{
		Log.event("--->>>Guest Email Error :: " + BrowserActions.getText(driver, lblGuestEmailError, "Guest Email Error"));
		return BrowserActions.getText(driver, lblGuestEmailError, "Guest Email Error");
	}

	/**
	 * To check news letter sign up
	 * @param state -
	 * @throws Exception - Exception
	 */
	public void checkNewsLetterSignUp(String state)throws Exception{
		if(state.equals("check") && BrowserActions.isRadioOrCheckBoxSelected(chkNewsLetterSignUp)){
			Log.event("News Letter Signup checkbox checked!");
		}else{
			BrowserActions.selectRadioOrCheckbox(chkNewsLetterSignUp, driver, (state.equals("check")) ? "YES" : "NO");
			Log.event("New Letter Signup checkbox "+state+"ed!");
		}
	}

	/**
	 * To get default payment method
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getDefaultPaymentMethod()throws Exception{
		for (WebElement card : lstSavedCards) {
			if(card.findElement(By.cssSelector("div[class*='carddetails']")).getAttribute("class").contains("selected")) {
				WebElement elem = card.findElement(By.cssSelector(".moile-wrapper span:nth-of-type(1)"));
				String cardName = BrowserActions.getText(driver, elem, "selected payment card");
				return cardName.trim();
			}
		}
		return null;
	}

	/**
	 * To make PLCC visible
	 * @throws Exception - Exception
	 */
	public void makePLCCVisible()throws Exception{
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].style.display='block';", mdlPLCCApprovalStep2);
		Log.event("Executed JavaScript to Make PLCC Modal Visible...");
		if(!Utils.waitForElement(driver, mdlPLCCApprovalStep2))
			Log.fail("Failed to Make PLCC Modal Visible again...");
	}

	/**
	 * To continue to PLCC step 2
	 * @throws Exception - Exception
	 */
	public void continueToPLCCStep2()throws Exception{
		BrowserActions.scrollToViewElement(btnGetItTodayInPLCC, driver);
		BrowserActions.clickOnElementX(btnGetItTodayInPLCC, driver, "Get It Today Button in PLCC Offer Modal");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To fill PLCC Step 2 data with ADS data
	 * @param addressADS - ADS data formatted as Map
	 * @throws Exception
	 */
	public void fillPLCCStep2WithADSData(Map<String, String> addressADS) throws Exception{
		String last4SSN = addressADS.get("ssn").substring(5);
		typeTextInSSN(last4SSN);
		Log.event("Entered SSN number last 4 digit:: " + last4SSN);
		
		String DOB = addressADS.get("dob");
		selectDateMonthYearInPLCC2(DOB.split("/")[1], DOB.split("/")[0], DOB.split("/")[2]);
		Log.event("Entered date of birth:: " + DOB);
	}

	/**
	 * To continue to PLCC step 2 in PLCC ACQ
	 * @throws Exception - Exception
	 */
	public void continueToPLCCStep2InPLCCACQ()throws Exception{
		Log.event("Trying to click on Get Ready Button in PLCC Acquisition modal");
		BrowserActions.scrollToViewElement(btnGetItTodayInPLCCACQ, driver);
		BrowserActions.clickOnElementX(btnGetItTodayInPLCCACQ, driver, "Get It Today Button in PLCC Acquisition Modal");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To close PLCC offer by clicking No Thanks
	 * @throws Exception - Exception
	 */
	public void closePLCCOfferByNoThanks()throws Exception{
		Log.event("Trying to close PLCC Modal");
		BrowserActions.scrollToViewElement(btnNoThanksInPLCC2, driver);
		BrowserActions.clickOnElementX(btnNoThanksInPLCC2, driver, "No Thanks Button in PLCC Offer Modal");
		BrowserActions.scrollToTopOfPage(driver);
	}

	/**
	 * To close PLCC offer by clicking No Thanks
	 * @throws Exception - Exception
	 */
	public void closePLCCOfferByNoThanks1()throws Exception{
		Log.event("Trying to close PLCC Modal");
		BrowserActions.scrollToViewElement(btnNoThanksInPLCC1, driver);
		BrowserActions.clickOnElementX(btnNoThanksInPLCC1, driver, "No Thanks Button in PLCC Offer Modal");
	}

	/**
	 * To close PLCC offer by close button
	 * @throws Exception - Exception
	 */
	public void closePLCCOfferByCloseBtn()throws Exception{
		Log.event("Trying to close PLCC Modal");
		BrowserActions.scrollToViewElement(btnCloseInPLCC2, driver);
		BrowserActions.clickOnElementX(btnCloseInPLCC2, driver, "close Button in PLCC Offer Modal");
	}

	/**
	 * To click Delivery Option tool tip
	 * @throws Exception - Exception
	 */
	public void clickDeliveryOptionToolTip()throws Exception{
		BrowserActions.clickOnElementX(lnkDeliveryOptionsToolTip, driver, "Tool Tip");
		Utils.waitForElement(driver, divDeliveryOptionsToolTipDialog);
	}

	/**
	 * To close PLCC offer by clicking No Thanks
	 * @throws Exception - Exception
	 */
	public void closeDeliveryOptionToolTip()throws Exception{
		BrowserActions.clickOnElementX(btnCloseToolTipDialog, driver, "Close");
	}

	/**
	 * To get shipping method label
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean getShippingMethodLabelsNonExpeditedShipping() throws Exception{
		boolean isStandardShipment = true;
		if(lblShippingMethod.size()>0){
			for(int i=0 ; i<lblShippingMethod.size() ; i++){
				String shippingMethod = BrowserActions.getText(driver, lblShippingMethod.get(i), "Shipping Method");
				if(shippingMethod.equals("Express Delivery") || shippingMethod.equals("Super Fast Delivery") || shippingMethod.equals("Next business Day Delivery")){
					isStandardShipment = false;
				} else{
					isStandardShipment = true;
				}
			}
		}		
		return isStandardShipment;
	}

	/**
	 * To get PLCC modal status
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean getPLCCModalStatus()throws Exception{
		return (Utils.waitForElement(driver, mdlPLCCApproval, 5));
	}
	
	/**
	 * To Verify legal link is displayed on legal copy
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean VerifyLegalLnkInLegalCopy() throws Exception{
		String legalCopy=BrowserActions.getText(driver, lnkPLCCLegalCopyClickHere, "legal copy message");
		String legalcopyLnk= BrowserActions.getText(driver, lnkLegalCopyClick, "Legal copy link");
		if(legalCopy.contains(legalcopyLnk)){
			return true;
		}
		return false;
	}

	/**
	 * To click edit in PO restricted overlay
	 * @throws Exception - Exception
	 */
	public void clickEditInPORestrictedOverlay()throws Exception{
		BrowserActions.clickOnElementX(btnEditPOBoxOverlay, driver, "Close");
		BrowserActions.clickOnElementX(txtShippingFirstNameFld, driver, "First name");
	}

	/**
	 * To get profile firstname
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getProfileFirstName()throws Exception {
		return txtProfileFirstName.getAttribute("value").trim();
	}

	/**
	 * To get profile last name
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getProfileLastName()throws Exception {
		return txtProfileLastName.getAttribute("value").trim();
	}

	/**
	 * To get profile email
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getProfileEmailAdd()throws Exception {
		return txtProfileEmailAdd.getAttribute("value").trim();
	}

	/**
	 * To get profile confirm email
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getProfileConfirmEmailAdd()throws Exception {
		return txtProfileConfirmEmailAdd.getAttribute("value").trim();
	}

	/**
	 * To get profile password
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getProfilePwd()throws Exception {
		return BrowserActions.getText(driver, txtProfilePwd, "Password");
	}

	/**
	 * To get profile confirm password
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getProfileConfirmPwd()throws Exception {
		return BrowserActions.getText(driver, txtProfilePwdConfirm, "Confirm Password");
	}

	/**
	 * To verify payment details title
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPaymentDetailsTitle() throws Exception {
		if(lblPaymentDetailsHeading.findElement(By.cssSelector("h2")).getText().trim().equalsIgnoreCase("2. Payment Details"))
			return true;
		else 
			return false;
	}

	/**
	 * To verify billing address title
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyBillingAddressTitle() throws Exception {
		if(lblBillingAddressHeading.getText().trim().equalsIgnoreCase("Billing Address"))
			return true;
		else 
			return false;
	}

	/**
	 *  To verify if shipping method exception is displayed
	 * @param excludeMethod -
	 * @return boolean
	 * @throws Exception -
	 */
	public Boolean verifyShippingMethodExceptionDisplayed(String excludeMethod) throws Exception {
		List<WebElement> shippingMethods = driver.findElements(By.cssSelector("#shipping-method-list .form-row")); 
		for (WebElement ele : shippingMethods) {
			if(!ele.findElement(By.cssSelector("div > label")).getText().contains(excludeMethod))
			{
				try {
					WebElement element = ele.findElement(By.cssSelector("span.shippingmethod-exception"));
					if(!Utils.waitForElement(driver, element))
								return false;
				}catch(NoSuchElementException e) {
					return false;
				}
			}
			/*else
			{
				if(ele.findElements(By.cssSelector("span.shippingmethod-exception")).size() > 0)
					return false;
			}*/
		}

		return true;
	}

	/**
	 * To get create account details
	 * @return account details
	 * @throws Exception - Exception
	 */
	public LinkedHashMap<String, String> getCreateAccountDetails()throws Exception{
		LinkedHashMap<String, String> profileInfo = new LinkedHashMap<String, String>();
		profileInfo.put("FirstName", getProfileFirstName());
		profileInfo.put("LastName", getProfileLastName());
		profileInfo.put("EmailAdd", getProfileEmailAdd());
		profileInfo.put("ConfirmEmailAddress", getProfileConfirmEmailAdd());
		profileInfo.put("Password", getProfilePwd());
		profileInfo.put("ConfirmPassword", getProfileConfirmPwd());

		return profileInfo;
	}

	/**
	 * To get entered nickname
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getEnteredNickName() throws Exception{
		return BrowserActions.getText(driver, txtNickName, "Nick Name");
	}

	/**
	 * To get selected shipping method
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSelectedShippingMethodInOverlay() throws Exception{
		return BrowserActions.getText(driver, selectedShippingMethodInOverlay, "Seelcted Shipping Method");
	}

	/**
	 * To click on continue in PO box overlay
	 * @throws Exception - Exception
	 */
	public void clickContinueInPOBoxOverlay() throws Exception{
		BrowserActions.clickOnElementX(btnContinueExpeditedOverlay, driver, "Continue button in Overlay");
		Utils.waitUntilElementDisappear(driver, divPoBoxBlockOverlay);
	}

	/**
	 * To click on cancel in PO box overlay
	 * @throws Exception - Exception
	 */
	public void clickCancelInPOBoxOverlay() throws Exception{
		BrowserActions.clickOnElementX(btnCancelExpeditedOverlay, driver, "Cancel button in Overlay");
		Utils.waitUntilElementDisappear(driver, divPoBoxBlockOverlay);
	}

	/**
	 * To get selected shipping method
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getSelectedShippingMethod() throws Exception{
		String shippingMethod = BrowserActions.getTextFromAttribute(driver, rdoShippingMethod, "value", "Selected Shipping method");
		shippingMethod = shippingMethod.replace("Delivery", "").replaceAll(" ", "").trim();

		Log.event("Selected shipping method:: " + shippingMethod);
		return shippingMethod;
	}

	/**
	 * To enter shipping address for restricted product
	 * @param address -
	 * @throws Exception - Exception
	 */
	public void enterShippingAddressForRestrictedProduct(String address) throws Exception{
		String randomFirstName = RandomStringUtils.randomAlphabetic(5)//randomAlphanumeric
				.toLowerCase();		
		txtFirstnameShipping.sendKeys(randomFirstName);
		txtFirstnameShipping.sendKeys(randomFirstName);

		txtAddressline1Shipping.sendKeys(address);
		txtAddressline1Shipping.sendKeys(Keys.TAB);	
	}

	/**
	 * To click on accept in PLCC
	 * @throws Exception - Exception
	 */
	public void clickOnAcceptInPLCC() throws Exception{
		BrowserActions.scrollToViewElement(btnAcceptInPLCCStep2, driver);
		Log.event("Trying to click on Accept in Plcc Step 2");
		BrowserActions.clickOnElementX(btnAcceptInPLCCStep2, driver, "Accept Button in PLCC Step 2");
		
		if(Utils.waitForElement(driver, divPLCCStep2Error)) {
			Log.ticket("SC-1453");
			BrowserActions.scrollToViewElement(txtMobileNoInPLCCStep2, driver);
			String unformattedNumber = txtMobileNoInPLCCStep2.getAttribute("value").replaceAll("^0-9", "");
			typeTextInMobileInPLCC(unformattedNumber);
			
			BrowserActions.scrollToViewElement(btnAcceptInPLCCStep2, driver);
			BrowserActions.clickOnElementX(btnAcceptInPLCCStep2, driver, "Accept Button in PLCC Step 2");
		}
	}

	/**
	 * To type text in SSN
	 * @param ssnNumber -
	 * @throws Exception - Exception
	 */
	public void typeTextInSSN(String ssnNumber) throws Exception{
		BrowserActions.typeOnTextField(txtSSNinPLCCStep2, ssnNumber, driver, "SSN Field");
	}

	/**
	 * To click on SSN tool tip
	 * @throws Exception - Exception
	 */
	public void clickOnSSNToolTip()throws Exception{
		Log.event("Trying to click on Toot tip in PLCC");
		BrowserActions.clickOnElementX(icoSSNToolTip, driver, "SSN Tool Tip");
	}

	/**
	 * To close SSN tool tip
	 * @throws Exception - Exception
	 */
	public void clickOnSSNToolTipClose()throws Exception{
		Log.event("Trying to click on Close button in Tool Tip modal.");
		BrowserActions.clickOnElementX(iconCloseToolTip, driver, "Tool Tip close Icon");
	}

	/**
	 * To click on edit address in PLCC
	 * @throws Exception - Exception
	 */
	public void clickonEditAddressInPLCC()throws Exception{
		Log.event("Trying to click on Edit link in PLCC Address");
		BrowserActions.clickOnElementX(lnkEditInPLCCAddress, driver, "Edit Link in PLCC Address");
	}

	/**
	 * To clear PLCC fields
	 * @throws Exception - Exception
	 */
	public void clearPLCCAccFields()throws Exception{
		BrowserActions.scrollInToView(txtSSNinPLCCStep2, driver);
		txtSSNinPLCCStep2.clear();
		BrowserActions.scrollInToView(txtMobileNoInPLCCStep2, driver);
		txtMobileNoInPLCCStep2.clear();
		BrowserActions.scrollInToView(txtAltMobileNoInPLCCStep2, driver);
		txtAltMobileNoInPLCCStep2.clear();
		selectDateMonthYearInPLCC2("DD","MM","YYYY");
	}

	/**
	 * To select date, month and year in PLCC
	 * @param date -
	 * @param month -
	 * @param year -
	 * @throws Exception - 
	 */
	public void selectDateMonthYearInPLCC2(String date, String month, String year)throws Exception{
		if(selectPLCCBirthDateDrp.isDisplayed() && selectPLCCBirthMonthDrp.isDisplayed() && selectPLCCBirthYearDrp.isDisplayed()){
			BrowserActions.scrollToViewElement(selectPLCCBirthDateDrp, driver);
			Select dateDrp = new Select(selectPLCCBirthDate);
			dateDrp.selectByVisibleText(date);

			Select monthDrp = new Select(selectPLCCBirthMonth);
			monthDrp.selectByVisibleText(month);

			Select yearDrp = new Select(selectPLCCBirthYear);
			yearDrp.selectByVisibleText(year);
		}else{
			BrowserActions.scrollToViewElement(divPLCCBirthDateDrp, driver);
			if(!divPLCCBirthDateDrp.getText().equals(date)){
				BrowserActions.clickOnElementX(divPLCCBirthDateDrp, driver, "PLCC State Field");
				WebElement dateEle = driver.findElement(By.xpath("//select[@id='dwfrm_creditapplication_birthday_day']//following-sibling::ul//li[contains(text(),'"+date+"')]"));
				BrowserActions.clickOnElementX(dateEle, driver, "Given Date in list");
			}
			BrowserActions.scrollToViewElement(divPLCCBirthMonthDrp, driver);
			if(!divPLCCBirthMonthDrp.getText().equals(month)){
				BrowserActions.clickOnElementX(divPLCCBirthMonthDrp, driver, "PLCC State Field");
				WebElement monthEle = driver.findElement(By.xpath("//select[@id='dwfrm_creditapplication_birthday_month']//following-sibling::ul//li[contains(text(),'"+month+"')]"));
				BrowserActions.clickOnElementX(monthEle, driver, "Given Month in list");
			}
			BrowserActions.scrollToViewElement(divPLCCBirthYearDrp, driver);
			if(!divPLCCBirthYearDrp.getText().equals(year)){
				BrowserActions.clickOnElementX(divPLCCBirthYearDrp, driver, "PLCC State Field");
				WebElement yearEle = driver.findElement(By.xpath("//select[@id='dwfrm_creditapplication_birthday_year']//following-sibling::ul//li[contains(text(),'"+year+"')]"));
				BrowserActions.clickOnElementX(yearEle, driver, "Given Year in list");
			}
		}
	}

	/**
	 * To select state in PLCC
	 * @param state -
	 * @throws Exception - Exception
	 */
	public void selectStateInPLCC2(String state)throws Exception{
		if(selectPLCCStateDrp.isDisplayed()){
			Select stateDrp = new Select(selectPLCCStateDrp);
			stateDrp.selectByVisibleText(state);
		}else{
			BrowserActions.scrollToViewElement(divPLCCStateDrp, driver);
			BrowserActions.clickOnElementX(divPLCCStateDrp, driver, "PLCC State Field");
			WebElement stateEle = driver.findElement(By.xpath("//select[contains(@id,'dwfrm_creditapplication_states_state')]//following-sibling::ul//li[contains(translate(text(),'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'"+state.toLowerCase()+"')]"));
			BrowserActions.clickOnElementX(stateEle, driver, "Given State in list");
		}
	}

	/**
	 * To fill address in PLCC
	 * @param plccAddress -
	 * @throws Exception - Exception
	 */
	public void fillingAddressInPLCC(String plccAddress)throws Exception{
		String address = checkoutProperty.getProperty(plccAddress);

		String firstName = address.split("\\|")[0];
		String lastName = address.split("\\|")[1];
		String add1 = address.split("\\|")[2];
		String add2 = address.split("\\|")[3];
		String city = address.split("\\|")[4];
		String state = address.split("\\|")[5];
		String zipcode = address.split("\\|")[6];
		String email = address.split("\\|")[7];

		BrowserActions.typeOnTextField(txtFirstNameInPLCCStep2, firstName, driver, "First Name in PLCC");
		BrowserActions.typeOnTextField(txtLastNameInPLCCStep2, lastName, driver, "Last Name in PLCC");
		BrowserActions.typeOnTextField(txtAddress1InPLCCStep2, add1, driver, "Address1 in PLCC");
		BrowserActions.typeOnTextField(txtAddress2InPLCCStep2, add2, driver, "Address2 in PLCC");
		BrowserActions.typeOnTextField(txtCityInPLCCStep2, city, driver, "City in PLCC");
		selectStateInPLCC2(state);
		BrowserActions.typeOnTextField(txtZipcodeInPLCCStep2, zipcode, driver, "Zipcode in PLCC");
		BrowserActions.typeOnTextField(txtEmailInPLCCStep2, email, driver, "Email in PLCC");
	}
	
	/**
	 * To update address details in PLCC
	 * @param address - raw address or reference to properties
	 * @param fromProperty - true/false if address is from property or raw input
	 * @throws Exception
	 */
	public void updateDetailsInPLCCStep2(String address, boolean fromProperty)throws Exception{
		clickonEditAddressInPLCC();
		String firstName = "Hello", lastName = "Welcome"; 
		if(fromProperty) {
			String addressDetails = checkoutProperty.getProperty(address);
			firstName = addressDetails.split("\\|")[0];
			lastName = addressDetails.split("\\|")[1];
		} else {
			try {
				firstName = address.split("\\|")[0];
				lastName = address.split("\\|")[1];
			} catch(ArrayIndexOutOfBoundsException e){
				Log.event("Incomplete/invalid parameter. When fromProperty is set false, First and Last name should be passed separated by the pipe symbol.");
			}
		}

		BrowserActions.typeOnTextField(txtFirstNameInPLCCStep2, firstName, driver, "First Name in PLCC");
		BrowserActions.typeOnTextField(txtLastNameInPLCCStep2, lastName, driver, "Last Name in PLCC");
	}

	/**
	 * To type email in PLCC
	 * @param email - Email to type
	 * @throws Exception - Exception
	 */
	public void typeTextInEmailInPLCC(String email)throws Exception{
		BrowserActions.typeOnTextField(txtEmailInPLCCStep2, email, driver, "Email Field in PLCC");
	}

	/**
	 * To type text in mobile field in PLCC
	 * @param mobile - Phone number to type
	 * @throws Exception - Exception
	 */
	public void typeTextInMobileInPLCC(String mobile)throws Exception{
		BrowserActions.scrollInToView(txtMobileNoInPLCCStep2, driver);
		BrowserActions.typeOnTextField(txtMobileNoInPLCCStep2, mobile, driver, "Mobile Number field in PLCC step 2");
	}

	/**
	 * To type text in alternate mobile field in PLCC
	 * @param mobile - Phone number to type
	 * @throws Exception
	 */
	public void typeTextInAltMobileInPLCC(String mobile)throws Exception{
		BrowserActions.scrollInToView(txtAltMobileNoInPLCCStep2, driver);
		BrowserActions.typeOnTextField(txtAltMobileNoInPLCCStep2, mobile, driver, "Alternate Mmobile number field in PLCC step 2");
	}

	/**
	 * To get date value from PLCC birthdate
	 * @return Date from PLCC
	 * @throws Exception
	 */
	public String getDateFromPLCCBirthDate()throws Exception{
		String datePLCC = BrowserActions.getText(driver, divPLCCBirthDateDrp, "Birth Date in PLCC");
		Log.event("Date From PLCC Birth Date :: " + datePLCC);
		return datePLCC;
	}

	/**
	 * To get month value from PLCC birthdate
	 * @return Month from PLCC
	 * @throws Exception
	 */
	public String getMonthFromPLCCBirthDate()throws Exception{
		String monthPLCC = BrowserActions.getText(driver, divPLCCBirthMonthDrp, "Birth Date in PLCC");
		Log.event("Month From PLCC Birth Date :: " + monthPLCC);
		return monthPLCC;
	}

	/**
	 * To get year value from PLCC birthdate
	 * @return Year from PLCC
	 * @throws Exception
	 */
	public String getYearFromPLCCBirthDate()throws Exception{
		String yearPLCC = BrowserActions.getText(driver, divPLCCBirthYearDrp, "Birth Date in PLCC");
		Log.event("Year From PLCC Birth Date :: " + yearPLCC);
		return yearPLCC;
	}

	/**
	 * To verify date dropdown
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyDateDropdown()throws Exception{
		boolean state = true;
		if (Utils.waitForElement(driver, selectPLCCBirthDateDrp)) {
			BrowserActions.scrollToViewElement(selectPLCCBirthDateDrp, driver);
			Select date = new Select(selectPLCCBirthDateDrp);

			for(int x=1; x < 32; x++)
				if (!(Integer.parseInt(date.getOptions().get(x-1).getText().trim()) == (x)))
					return false;				

			return true;
		}
		try {
			BrowserActions.scrollToViewElement(divPLCCBirthDateDrp, driver);
			List<WebElement> lstPLCCBirthDateDrp = driver.findElements(By.cssSelector("#dwfrm_creditapplication_birthday_day + .selected-option.selected + .selection-list li"));
			for (int i = 1; i < 32; i++)
				if (!(Integer.parseInt(lstPLCCBirthDateDrp.get(i).getAttribute("innerHTML").trim()) == i)){
					state = false;				
				}
		} catch (StaleElementReferenceException e) {
			return false;
		}

		return state;
	}

	/**
	 * To click sign in button
	 * @return Object
	 * @throws Exception - Exception
	 */
	public Object clickSignInButton()throws Exception{
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].style.opacity=1", btnSignIn); 

		BrowserActions.javascriptClick(btnSignIn, driver, "SignIn Button In Sign in page ");
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, readyElementCheckOut))
			return new CheckoutPage(driver).get();
		else
			return null;
	}

	/**
	 * To type email
	 * @param email -
	 * @throws Exception - Exception
	 */
	public void typeOnEmail(String email)throws Exception{
		fldEmail.clear();
		BrowserActions.typeOnTextField(fldEmail, email, driver, "Email Address ");
	}

	/**
	 * To verify month dropdown
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyMonthDropdown()throws Exception{
		boolean state = true;
		if (Utils.waitForElement(driver, selectPLCCBirthMonthDrp)) {
			BrowserActions.scrollToViewElement(selectPLCCBirthMonthDrp, driver);
			Select month = new Select(selectPLCCBirthMonthDrp);

			for(int x=1; x < 13; x++)
				if (!(Integer.parseInt(month.getOptions().get(x-1).getText().trim()) == (x)))
					return false;				

			return true;
		}
		try {
			List<WebElement> lstPLCCBirthMonthDrp = driver.findElements(By.cssSelector("#dwfrm_creditapplication_birthday_month + .selected-option.selected + .selection-list li"));
			for (int i = 1; i < 13; i++)
				if (!(Integer.parseInt(lstPLCCBirthMonthDrp.get(i).getAttribute("innerHTML").trim()) == i)){
					state = false;				
				}
		} catch (StaleElementReferenceException e) {
			return false;
		}

		return state;
	}

	/**
	 * To verify year dropdown
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyYearDropdown()throws Exception{
		boolean state = true;
		int curYear = Calendar.getInstance().get(Calendar.YEAR);
		if (Utils.waitForElement(driver, selectPLCCBirthYearDrp)) {
			Select year = new Select(selectPLCCBirthYearDrp);
			BrowserActions.scrollToViewElement(selectPLCCBirthYearDrp, driver);
			for(int x=1, j=0; x < 100; x++)
				if (!(Integer.parseInt(year.getOptions().get(x-1).getText().trim()) == (curYear-j)))
					return false;				

			return true;
		}
		try {
			List<WebElement> lstPLCCBirthYearDrp = driver.findElements(By.cssSelector("#dwfrm_creditapplication_birthday_year + .selected-option.selected + .selection-list li"));
			for (int i = 1, j=0; i < 100; i++,j++)
				if (!(Integer.parseInt(lstPLCCBirthYearDrp.get(i).getAttribute("innerHTML").trim()) == (curYear-j))){
					state = false;				
				}
		} catch (StaleElementReferenceException e) {
			return false;
		}

		return state;
	}

	/**
	 * To check consent in PLCC
	 * @param state - check.uncheck consent check-box
	 * @throws Exception
	 */
	public void checkConsentInPLCC(String state)throws Exception{
		BrowserActions.scrollInToView(chkConsonent, driver);
		if(state.equalsIgnoreCase("yes")) {
			if(!chkConsonent.isSelected()) {
				BrowserActions.clickOnElementX(chkConsonent, driver, "Checkbox");
			} else {
				Log.event("Consonent Checkbox already Checked.");
			}
		} else {
			if(chkConsonent.isSelected()) {
				BrowserActions.clickOnElementX(chkConsonent, driver, "Checkbox");
			} else {
				Log.event("Consonent Checkbox already unchecked.");
			}
		}
	}

	/**
	 * To clear PLCC text fields
	 * @throws Exception - Exception
	 */
	public void clearPLCCTextFields()throws Exception{
		txtSSNinPLCCStep2.clear();
		txtMobileNoInPLCCStep2.clear();
		txtAltMobileNoInPLCCStep2.clear();
		txtFirstNameInPLCCStep2.clear();
		txtLastNameInPLCCStep2.clear();
		txtAddress1InPLCCStep2.clear();
		txtAddress2InPLCCStep2.clear();
		txtCityInPLCCStep2.clear();
		txtZipcodeInPLCCStep2.clear();
		txtEmailInPLCCStep2.clear();
	}

	/**
	 * To fill PLCC account details
	 * @param account -
	 * @throws Exception - Exception
	 */
	public void fillPLCCAccountDetails(String account)throws Exception{
		String acc;
		if(account.contains("|")) {
			acc = account;
		} else {
			acc = checkoutProperty.getProperty(account);
		}

		String ssn = acc.split("\\|")[0];
		String date = acc.split("\\|")[1].split("\\-")[0];
		String month = acc.split("\\|")[1].split("\\-")[1];
		String year = acc.split("\\|")[1].split("\\-")[2];
		String phone = acc.split("\\|")[2];

		BrowserActions.typeOnTextField(txtSSNinPLCCStep2, ssn, driver, "SSN Number");
		selectDateMonthYearInPLCC2(date, month, year);
		BrowserActions.typeOnTextField(txtMobileNoInPLCCStep2, phone, driver, "Phone Number");
	}	

	/**
	 * To click on shopping cart tool tip
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean clickOnShoppingCartToolTip()throws Exception{
		BrowserActions.clickOnElementX(spanCostToolTip, driver, "Shopping cost Tool Tip");
		return (Utils.waitForElement(driver, spanCostToolTip));
	}

	/**
	 * To wait until PLCC validation
	 * @throws Exception - Exception
	 */
	public void waitUntilPLCCValidation()throws Exception{
		Utils.waitUntilElementDisappear(driver, txtMobileNoInPLCCStep2);
	}

	/**
	 * To verify overlapping of elements
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyBillingAddressDrpOverLapNickname()throws Exception{
		return txtNickNameBillingDetails.isDisplayed();
	}

	/**
	 * To get guest email ID
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getGuestEmailID()throws Exception{
		return BrowserActions.getText(driver, txtGuestEmailID, "Guest Email").trim();
	}
	
	/**
	 * To click CVV tool tip
	 * @throws Exception - Exception
	 */
	public void clickCVVToolTip()throws Exception{
		BrowserActions.clickOnElementX(cvvToolTip, driver, "CVV Tool Tip");	
		Utils.waitForElement(driver, cvvToolTipContent);
	}

	/**
	 * To close CVV tool tip
	 * @throws Exception - Exception
	 */
	public void clickCloseOnCVVToolTip()throws Exception{
		Utils.waitForElement(driver, cvvToolTipContent);	
		Utils.waitForElement(driver, cvvToolTipContentClose);
		BrowserActions.clickOnElementX(cvvToolTipContentClose, driver, "CVV Tool Tip Close");	
	}

	/**
	 * To get CVV length
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyCVVLength()throws Exception{
		String cvv = BrowserActions.getText(driver, txtCVN, "CVV Tool Tip Close");
		int length = cvv.length();
		return(length == 3 || length == 4);
	}

	/**
	 * To click PayPal button
	 * @return PaypalPage
	 * @throws Exception - Exception
	 */
	public PaypalPage clickOnPaypalButton()throws Exception {
		String baseWindowHandle = driver.getWindowHandle();
		Log.event("Base windows handle:: " + baseWindowHandle);
		BrowserActions.clickOnElementX(btnPaypal, driver, "Paypal Button");
		Set<String> openWindowsList = driver.getWindowHandles();
		Log.event("Number of open windows:: " + openWindowsList.size());
		if(openWindowsList.size() > 1) {
			Log.event("PayPal opened in separate window/tab.");
			String popUpWindowHandle = null;
			for(String windowHandle : openWindowsList) {
				Log.event("Current windows handle:: " + windowHandle);
				if (!windowHandle.equals(baseWindowHandle)) {
					popUpWindowHandle = windowHandle;
				}
			}
			driver.switchTo().window(popUpWindowHandle);
			Log.event("Popup window handle:: " + popUpWindowHandle);
		}else {
			Log.event("PayPal opened in same window/tab.");
		}
		Utils.waitForElement(driver, emailPayPal, 30);	//PayPal window sometimes take too long to load, resulting in SO_TIMEOUT
		return new PaypalPage(driver).get();
	}

	/**
	 * To get continue paypal button text
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getContinuePaypalButtonText()throws Exception{
		return BrowserActions.getText(driver, btnPaypal, "Paypal Button");	
	}

	/**
	 * To fill shipping details as guest for PO restricted items
	 * @throws Exception - Exception
	 */
	public void fillingShippingDetailsAsGuestForPORestrictedItems() throws Exception{

		String  randomFirstName = RandomStringUtils.randomAlphabetic(5).toLowerCase();//randomAlphanumeric

		BrowserActions.typeOnTextField(txtShippingFirstName, randomFirstName, driver, "Shipping First Name");

		String randomLastName = RandomStringUtils.randomAlphabetic(5).toLowerCase();

		BrowserActions.typeOnTextField(txtShippingLastName, randomLastName, driver, "Shipping Last Name");
		BrowserActions.typeOnTextField(txtShippingAddress, "PO BOX 344", driver, "Shipping Address1");
	}

	/**
	 * To click continue button
	 * @return CheckoutPage
	 * @throws Exception - Exception
	 */
	public CheckoutPage getContinueOverlayButtonText()throws Exception{
		BrowserActions.clickOnElementX(OverlayContinueButton, driver, "Checkbox");
		return new CheckoutPage(driver).get();
	}

	/**
	 * To click shipping cost tool
	 * @throws Exception - Exception
	 */
	public void clickOnShippingCostTool() throws Exception{
		BrowserActions.clickOnElementX(shippingCostToolTip, driver, "Shipping cost tool");
		Utils.waitForElement(driver, shippingCostOverlay);
	}

	/**
	 * To verify Z Pattern of Shipping Methods
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean verifyShippingMethodPattern()throws Exception{
		boolean valueToReturn = true;

		List<WebElement> shippingMethods = driver.findElements(By.cssSelector("input[name='dwfrm_singleshipping_shippingAddress_shippingMethodID']"));

		for(int i = 0; i+2 < shippingMethods.size(); i=i+2) {
			WebElement leftEle = shippingMethods.get(i);
			WebElement rightEle = shippingMethods.get(i+1);
			WebElement belowEle = shippingMethods.get(i+2);

			int leftEleY = leftEle.getLocation().y;
			int rightEleY = rightEle.getLocation().y;
			int belowEleY = belowEle.getLocation().y;

			if(!((leftEleY == rightEleY) && (leftEleY < belowEleY)))
				return false;
		}

		return valueToReturn;
	}

	/**
	 * To select shipping method based on given option
	 * Close the exception overlay, if displayed.
	 * @param shippingmethod - "standard", "express", "superfast", "nextday"
	 * @throws Exception - Exception
	 */
	public void selectShippingMethod(ShippingMethod shippingmethod)throws Exception{
		WebElement element = null;
		switch (shippingmethod) {
		case Standard:
			element = radioStandardDelivery;
			break;
		case Express:
			element = radioExpressDelivery;
			break;
		case SuperFast:
			element = radioSuperFastDelivery;
			break;
		case NextDay:
			element = radioNextDayDelivery;
			break;
		case HeathersFixed:
			element = radioHeathersDelivery;
			break;
		}

		BrowserActions.clickOnElementX(element, driver, "Shipping Method");
		if(Utils.waitForElement(driver, shippingmethodExceptionOverlay)){
			BrowserActions.clickOnElementX(divShipMedOverlayCont, driver, "Continue Button in AVS");
			Utils.waitUntilElementDisappear(driver, shippingmethodExceptionOverlay);
		}
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}
	
	/**
	 * To select shipping method based on given option only
	 * @param shippingmethod - "standard", "express", "superfast", "nextday"
	 * @throws Exception - Exception
	 */
	public void selectShippingMethodOnly(ShippingMethod shippingmethod)throws Exception{
		WebElement element = null;
		switch (shippingmethod) {
		case Standard:
			element = radioStandardDelivery;
			break;
		case Express:
			element = radioExpressDelivery;
			break;
		case SuperFast:
			element = radioSuperFastDelivery;
			break;
		case NextDay:
			element = radioNextDayDelivery;
			break;
		case HeathersFixed:
			element = radioHeathersDelivery;
			break;
		}
		BrowserActions.clickOnElementX(element, driver, "Shipping Method");
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}

	/**
	 * To close shipping cost tool
	 * @throws Exception - Exception
	 */
	public void clickOnShippingCostToolClose() throws Exception{
		BrowserActions.clickOnElementX(shippingCostOverlayClose, driver, "Shipping cost tool close");
	}

	/**
	 * To switch to disclaimer frame
	 * @return WebDriver
	 * @throws Exception - Exception
	 */
	public WebDriver switchToDisclaimerIframe()throws Exception{
		driver = driver.switchTo().parentFrame();
		WebElement iframe = driver.findElement(By.cssSelector(".electronic-iframe"));
		return driver.switchTo().frame(iframe);
	}

	/**
	 * To switch to fianancial frame
	 * @return WebDriver
	 * @throws Exception - Exception
	 */
	public WebDriver switchToFinancialIframe()throws Exception{
		driver = driver.switchTo().parentFrame();
		WebElement iframe = driver.findElement(By.cssSelector(".financial-iframe"));
		return driver.switchTo().frame(iframe);
	}

	/**
	 * To switch to card terms frame
	 * @return WebDriver
	 * @throws Exception - Exception
	 */
	public WebDriver switchToCardTermsframe()throws Exception{
		driver = driver.switchTo().parentFrame();
		WebElement iframe = driver.findElement(By.cssSelector(".financial-iframe"));
		return driver.switchTo().frame(iframe);
	}

	/**
	 * To enter first name
	 * @param FirstName -
	 * @throws Exception - Exception
	 */
	public void enterFirstName(String FirstName)throws Exception {
		BrowserActions.typeOnTextField(firstName, FirstName, driver, "FirstName");

	}
	
	/**
	 * To clear first name
	 * @throws Exception - Exception
	 */
	public void clearFirstName()throws Exception {
		BrowserActions.clearTextField(firstName, "ClearFirstname");

	}

	/**
	 * To enter last name
	 * @param lastname -
	 * @throws Exception - Exception
	 */
	public void enterLastName(String lastname)throws Exception {
		BrowserActions.typeOnTextField(lastName, lastname, driver, "LastName");
	}

	/**
	 * To enter address line 1
	 * @param Address1 -
	 * @throws Exception - Exception
	 */
	public void enterAddress1(String Address1)throws Exception {
		BrowserActions.typeOnTextField(address1, Address1, driver, "Address1");
	}

	/**
	 * To enter address 2
	 * @param Address2 -
	 * @throws Exception - Exception
	 */
	public void enterAddress2(String Address2)throws Exception {
		BrowserActions.typeOnTextField(address2, Address2, driver, "Address2");
	}

	/**
	 * To enter city
	 * @param City -
	 * @throws Exception - Exception
	 */
	public void enterCity(String City)throws Exception {
		BrowserActions.typeOnTextField(city, City, driver, "City");
	}

	/**
	 * To enter zipcode
	 * @param pcode -
	 * @throws Exception - Exception
	 */
	public void enterPostalCode(String pcode)throws Exception {
		BrowserActions.typeOnTextField(postalCode, pcode, driver, "PostalCode");

	}

	/**
	 * To enter phone num
	 * @param pcode -
	 * @throws Exception - Exception
	 */
	public void enterPhoneCode(String pcode)throws Exception {
		BrowserActions.typeOnTextField(Phone, pcode, driver, "Phone");

	}

	/**
	 * To get error msg first name
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getErrorMessageFirstname()throws Exception{
		return BrowserActions.getText(driver, errorMsgFirstName, "Error message");
	}

	/**
	 * To get error msg last name
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getErrorMessageLastname()throws Exception{
		return BrowserActions.getText(driver, errorMsgLastName, "Error message");
	}

	/**
	 * To get error msg address 1
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getErrorMessageAddress1()throws Exception{
		return BrowserActions.getText(driver, errorMsgAddress1, "Error message");
	}

	/**
	 * To get error msg city
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getErrorMessageCity()throws Exception{
		return BrowserActions.getText(driver, errorMsgcity, "Error message");
	}

	/**
	 * To get error msg for zipcode
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getErrorMessagePostalcode()throws Exception{
		return BrowserActions.getText(driver, errorMsgPostalcode, "Error message");
	}

	/**
	 * To get error msg for state
	 * @return string
	 * @throws Exception - Exception
	 */
	public String getErrorMessageState()throws Exception{
		return BrowserActions.getText(driver, errorMsgState, "Error message");
	}
	
	/**
	 * To get error msg for phone
	 * @return String string
	 * @throws Exception - Exception
	 */
	public String getErrorMessagePhone()throws Exception{
		return BrowserActions.getText(driver, errorMsgPhone, "Error message");
	}

	/**
	 * To get product color based on index
	 * @param index -
	 * @return String color value
	 * @throws Exception -
	 */
	public String getProductColorVariation(int index)throws Exception{
		WebElement color = driver.findElements(By.cssSelector("#cart-table div.cart-row")).get(index).findElement(By.cssSelector("div[data-attribute='color'] .value"));
		return BrowserActions.getText(driver, color, "Product color variation");
	}

	/**
	 * To get product size based on index
	 * @param index -
	 * @return String size value
	 * @throws Exception -
	 */
	public String getProductSizeVariation(int index)throws Exception{
		WebElement size = null;
        WebElement cartRow = driver.findElements(By.cssSelector("#cart-table div.cart-row")).get(index);
        try{
            size = cartRow.findElement(By.cssSelector("div[data-attribute='size'] .value"));
        } catch (NoSuchElementException ex){
            size = cartRow.findElement(By.cssSelector("div[data-attribute='shoeSize'] .value"));
        }
		return BrowserActions.getText(driver, size, "Product size variation");
	}

	/**
	 * To get product Name based on index
	 * @param index -
	 * @return String product name
	 * @throws Exception -
	 */
	public String getProductName(int index)throws Exception{
		WebElement name = driver.findElements(By.cssSelector("#cart-table div.cart-row")).get(index).findElement(By.cssSelector(".name a"));
		return BrowserActions.getText(driver, name, "Product name variation");
	}

	/**
	 * To get product quantity based on index
	 * @param index -
	 * @return String quantity value
	 * @throws Exception -
	 */
	public String getProductQuantity(int index)throws Exception{
		WebElement qty = driver.findElements(By.cssSelector("#cart-table div.cart-row")).get(index).findElement(By.cssSelector(".hide-"+ (Utils.isMobile() ? "desktop" : "mobile") +" .item-quantity .value"));
		return qty.getAttribute("innerHTML").trim();
	}

	/**
	 * To get product price based on index
	 * @param index -
	 * @return String price value
	 * @throws Exception -
	 */
	public String getProductPrice(int index)throws Exception{
		WebElement qty;
		try {
			qty = driver.findElements(By.cssSelector("#cart-table div.cart-row")).get(index).findElement(By.cssSelector(".hide-"+ (Utils.isMobile() ? "desktop" : "mobile") +" .item-total span[class*='price-total']"));
		}catch(Exception e) {
			qty = driver.findElements(By.cssSelector("#cart-table div.cart-row")).get(index).findElement(By.cssSelector(".hide-"+ (Utils.isMobile() ? "desktop" : "mobile") +" .item-total div[class*='price-adjusted-total']"));
		}
		
		return qty.getAttribute("innerHTML").trim();
	}

	/**
	 * To get product details as linked list of linked hashmap
	 * @return LinkedList of product details
	 * @throws Exception -
	 */
	public LinkedList<LinkedHashMap<String, String>> getProductDetails()throws Exception{
		LinkedList<LinkedHashMap<String, String>> dataToReturn = new LinkedList<LinkedHashMap<String, String>>();

		for(int i = 0; i < divCartRow.size(); i++) {
			LinkedHashMap<String, String> product = new LinkedHashMap<String, String>();
			product.put("Name", getProductName(i));
			product.put("Color", getProductColorVariation(i));
			product.put("Size", getProductSizeVariation(i));
			product.put("Quantity", getProductQuantity(i));
			product.put("Price", getProductPrice(i));

			dataToReturn.add(product);
		}

		return dataToReturn;
	}

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//				Order Page Elements & Methods
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	/**
	 * To get order number
	 * @return String order number
	 * @throws Exception -
	 */
	public String getOrderNumber()throws Exception{
		WebElement orderNumber = driver.findElement(By.cssSelector(".order-number .value"));
		return orderNumber.getText().trim();
	}

	/**
	 * To type password and confirm password in account form in order receipt page
	 * @throws Exception -
	 */
	public void fillPasswordInOrderReceipt()throws Exception{
		BrowserActions.typeOnTextField(fldPasswordOrderReceipt, accountData.get("password_global"), driver, "Password");
		BrowserActions.typeOnTextField(fldConfirmPasswordOrderReceipt, accountData.get("password_global"), driver, "Confirm Password");
	}

	/**
	 * To click on create account button in order receipt page
	 * @throws Exception -
	 */
	public void clickOnCreateAccountOrderReceipt()throws Exception{
		BrowserActions.clickOnElementX(btnCreateAccountOrderReceipt, driver, "Create Account");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To close the congratulations modal after account signup
	 * @throws Exception -
	 */
	public void dismissCongratulationModal() throws Exception {
		Utils.waitForElement(driver, approvedModal);
		BrowserActions.clickOnElementX(btnContinueToCheckout, driver, "Continue to Checkout");
	Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on saved shipping address dropdown
	 * @throws Exception
	 */
	public void clickOnSelectedShipingAddress() throws Exception {
		BrowserActions.clickOnElementX(selectedOptionShipping, driver, "Save Address Dropdown");
	}

	/**
	 * To compare shipping and billing address
	 * @return boolean true both address are equal
	 * @throws Exception
	 */
	public boolean compareShippingBillingAddress() throws Exception {
		String Shipping = divShippingAddress.getText().trim().replace("\n", "").replace(" ", "").replace(",", "").replaceAll("\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d", "");
		String billing =  divBillingAddress.getText().trim().replace("\n", "").replace("|", "").replace(",", "").replace(" ", "");
		Log.event("Shipping Address :: " + Shipping);
		Log.event("Billing Address :: " + billing);
		return Shipping.equalsIgnoreCase(billing)?true:false;
	}

	/**
	 * To click shipping method cancel button
	 * @throws Exception
	 */
	public void clickShippingMethoCancelBtn() throws Exception {
		BrowserActions.clickOnElementX(OverlayCancelButton, driver, "Cancel Button");
	}


	/**
	 * To verify the number of product added to the cart
	 * @return product count as integer
	 * @throws Exception
	 */
	public int sizeOfTheCart() throws Exception {
		int totalItem = cartItems.size();
		Log.event("Item Count :: " + totalItem);
		return totalItem;
	}

	/**
	 * To get amount remaining above credit card section
	 * @return float - amount remaining
	 * @throws Exception - Exception
	 */
	public float getAmountRemaining() throws Exception{
		String value = amtRemaining.getText().replaceAll("[a-zA-Z$]", "").trim();
		Log.event("Value Extracted from Amount Remaining :: " + value);
		return Float.parseFloat(value);
	}

	/**
	 * To select PLCC payment from Saved Card List
	 * @param index - nth Payment
	 * @return HashMap - Selected Payment Information
	 * @throws Exception - Exception
	 */
	public HashMap<String, String> selectSavedCard_PLCC(int... index)throws Exception{
		HashMap<String, String> cardInfo = new HashMap<String, String>();

		List<WebElement> lstSaveCard = driver.findElements(By.cssSelector(".carddetails.plcccard"));
		if(lstSaveCard.size() == 0){
			Log.fail("There are no PLCC cards available...", driver);
		}
		if(index.length > 0){
			if(lstSaveCard.get(index[0]-1).getAttribute("class").contains("selected"))
				Log.event(index + "th card already selected.");
			else{
				BrowserActions.clickOnElementX(lstSaveCard.get(index[0] - 1), driver, index + " nth Card in Saved Card List");
				Utils.waitUntilElementDisappear(driver, waitLoader);
			}
		}else{
			BrowserActions.clickOnElementX(lstSaveCard.get(0), driver, "1st Card in Saved Card List");
			Utils.waitUntilElementDisappear(driver, waitLoader);
		}

		cardInfo.put("CardType", "PLCC: " + lstSaveCard.get(0).findElement(By.cssSelector(".cardname.hide-mobile")).getText());
		cardInfo.put("Name", lstSaveCard.get(0).findElement(By.cssSelector(".cardholder")).getText());
		cardInfo.put("AvailableCredit", lstSaveCard.get(0).findElement(By.cssSelector(".cardnumber-value")).getText().trim());
		cardInfo.put("RewardPoints", lstSaveCard.get(0).findElement(By.cssSelector(".rewardpoints-value")).getText().trim());

		return cardInfo;
	}

	/** To get class names from order total based on index
	 * @param index - nth total
	 * @return String - class name
	 * @throws Exception - Exception
	 */
	public String getClassNamesFromOrderTotalsListByIndex(int index)throws Exception{
		return lstOrderElements.get(index-1).getAttribute("class").trim();
	}

	/**
	 * To clicking the coupon tooltip
	 * @throws Exception
	 */
	public void clickonCouponTooltip() throws Exception {
		BrowserActions.clickOnElementX(couponToolTipsIcon, driver, "Coupon Tooltip");
	}

	/**
	 * To select Non-PLCC payment from Saved Card List
	 * @param index - Card Payment index(Optional)
	 * @return HashMap<String, String> - Selected Payment Information
	 * @throws Exception - Exception
	 */
	public HashMap<String, String> selectSavedCard_Non_PLCC(int... index)throws Exception{
		HashMap<String, String> cardInfo = new HashMap<String, String>();

		List<WebElement> lstSaveCard = driver.findElements(By.cssSelector(".carddetails:not([class*='plcccard']):not([class*='message'])"));
		if(lstSaveCard.size() == 0){
			Log.fail("There are no PLCC cards available...", driver);
		}
		if(index.length > 0){
			if(lstSaveCard.get(index[0]-1).getAttribute("class").contains("selected"))
				Log.event(index + "th card already selected.");
			else{
				BrowserActions.clickOnElementX(lstSaveCard.get(index[0] - 1), driver, index + " nth Card in Saved Card List");
				Utils.waitUntilElementDisappear(driver, waitLoader);
			}
		}else{
			BrowserActions.clickOnElementX(lstSaveCard.get(0), driver, "1st Card in Saved Card List");
			Utils.waitUntilElementDisappear(driver, waitLoader);
		}

		cardInfo.put("CardType", lstSaveCard.get(0).findElement(By.cssSelector(".cardname.hide-mobile")).getText());
		cardInfo.put("Name", lstSaveCard.get(0).findElement(By.cssSelector(".cardholder")).getText());
		cardInfo.put("CardNumber", lstSaveCard.get(0).findElement(By.cssSelector(".cardnumber")).getText().split(" ")[3]);
		cardInfo.put("ExpMonth", DateTimeUtility.getMonthNameOf(Integer.parseInt(lstSaveCard.get(0).findElement(By.cssSelector(".expdate")).getText().split(" ")[1].split("\\/")[0])));
		String ExpYr = lstSaveCard.get(0).findElement(By.cssSelector(".expdate")).getText();
		Log.event("Before Splitting : "+ExpYr+"|| After Splitting year : " +ExpYr.substring(ExpYr.length() - 2, ExpYr.length()));
		cardInfo.put("ExpYear", "20" + (ExpYr.substring(ExpYr.length() - 2, ExpYr.length())).trim());
		return cardInfo;
	}
	
	/**
	 * To parse and return address as a HashMap
	 * @param address - address from page
	 * @return HashMap - Selected Payment Information
	 * @throws Exception - Exception
	 */
	public static HashMap<String, String> formatBillingAddressToMap(String address)throws Exception{
		HashMap<String, String> addressAsMap = new HashMap<String, String>();
		addressAsMap.put("addressLine1", address.split("\\|")[1].trim());
		addressAsMap.put("addressLine2", address.split("\\|")[2].trim());
		addressAsMap.put("city", address.split("\\|")[3].trim());
		addressAsMap.put("state", address.split("\\|")[4].trim().split(" ")[0].trim());
		addressAsMap.put("zipcode", address.split("\\|")[4].trim().split(" ")[1].trim().split("-")[0].trim());
		addressAsMap.put("nickName", address.split("\\|")[0].trim().split(" ")[0].trim());
		addressAsMap.put("country", address.split("\\|")[5].trim());
		addressAsMap.put("firstName", address.split("\\|")[0].trim().split(" ")[1].trim());
		addressAsMap.put("lastName", address.split("\\|")[0].trim().split(" ")[2].trim());
		return addressAsMap;
	}
	
	/**
	 * To compare addreeses read from two different sources
	 * @param addressEntered -  addressEntered
	 * @param addressfromPage - addressfromPage
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean compareTwoAddress(String addressEntered, String addressfromPage) throws Exception{
		if(addressfromPage.contains("|")) {
			addressfromPage = addressfromPage.split("\\|")[1];
		}
		return elementLayer.verifyTwoAddressMatch(addressEntered, addressfromPage);
	}
	
	/**
	 * To get name list of products in checkout summery.	
	 * @return HashSet<String> - Names of products
	 * @throws Exception - Exception
	 */
	public HashSet<String> getCheckoutSummeryNameList() throws Exception{
		HashSet<String> listCartItemNames = new HashSet<String>();
		for(WebElement cartItem: lstCartProductNames) {
			listCartItemNames.add(cartItem.getText().trim().toLowerCase());
		}
		return listCartItemNames;
	}

	/**
	 * To get Number of Shipping methods
	 * @return int - Number of Shipping methods available
	 * @throws Exception - Exception
	 */
	public int getShippingMethodCount()throws Exception{
		int noOfShippingMethods = driver.findElements(By.cssSelector("#shipping-method-list input[type='radio']")).size();
		Log.event("Number of Shipping methods available :: " + noOfShippingMethods);
		return noOfShippingMethods;
	}
	
	public boolean verifyOrderDateFormat(String format) throws Exception{
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		try {
			dateFormat.parse(lblOrderDateInConfPage.getText().trim());
		}catch (ParseException pe) {
			return false;
		}
		return true;
	}
	
	public String getselectedShippingAddress()throws Exception{
		WebElement selectedShippingAddress = driver.findElement(By.cssSelector(".shipping .custom-select.address-dropdown .selected-option"));
		return BrowserActions.getText(driver, selectedShippingAddress, "Selected Shipping Address");
	}
	
	/**
	 * To get selected billing address
	 * @return billing address
	 * @throws Exception
	 */
	public String getselectedBillingAddress() throws Exception {
		WebElement selectedbillingAddress = driver.findElement(By.cssSelector(".billing .custom-select.address-dropdown .selected-option"));
		return BrowserActions.getText(driver, selectedbillingAddress, "Selected Billing Address");
	}
	
	/**
	 * To hover mouse on continue button in shipping address page
	 * @throws Exception
	 */
	public void mouseHoverShippingContinueButton() throws Exception{
		BrowserActions.scrollInToView(btnShippingContinue, driver);
		BrowserActions.mouseHover(driver, btnShippingContinue, "Continue Button");
	}
	
	/**
	 * To hover mouse on continue button on payment page
	 * @throws Exception
	 */
	public void mouseHoverPaymentContinueButton() throws Exception{
		BrowserActions.scrollInToView(btnPaymentDetailsContinue, driver);
		BrowserActions.mouseHover(driver, btnPaymentDetailsContinue, "Continue Button");
	}
	
	/**
	 * To click on contine to payment
	 * @throws Exception - Exception
	 */
	public void continueToPaymentWithoutClickingAVS() throws Exception{
		Utils.waitForPageLoad(driver);
		WebElement ele = driver.findElement(By.cssSelector("#wrapper"));
		BrowserActions.clickOnElementX(ele, driver, "Wrapper Element to Enable continue");
		if(Utils.waitForElement(driver, txtFirstnameShipping)) {
			BrowserActions.scrollInToView(txtFirstnameShipping, driver);
			BrowserActions.clickOnElementX(txtFirstnameShipping, driver, "First Name in Shipping");
			txtFirstnameShipping.sendKeys(Keys.TAB);
		}else if(Utils.waitForElement(driver, txtFirstNameBillingDetails)){
			BrowserActions.scrollInToView(txtFirstNameBillingDetails, driver);
			BrowserActions.clickOnElementX(txtFirstNameBillingDetails, driver, "First Name in Billing");
			txtFirstNameBillingDetails.sendKeys(Keys.TAB);
		}

		Utils.waitForPageLoad(driver);
		Log.event("Trying to continue to Payment Section.");
		boolean isContinueButtonEnabled = true;

		if(Utils.waitForElement(driver, btnShippingContinue) ) {
			if(btnShippingContinue.getAttribute("disabled") == null){
				BrowserActions.scrollInToView(btnShippingContinue, driver);
				BrowserActions.clickOnElementX(btnShippingContinue, driver, "Continue in Shipping");
			} else {
				isContinueButtonEnabled = false;
			}
		}else if(Utils.waitForElement(driver,btnselectPaymentMethod)){
			if(btnselectPaymentMethod.getAttribute("disabled") == null){
				BrowserActions.scrollInToView(btnselectPaymentMethod, driver);
				BrowserActions.clickOnElementX(btnselectPaymentMethod, driver, "Select Payment Method");
			}else{
				isContinueButtonEnabled = false;
			}
		}

		if(!isContinueButtonEnabled) {
			Log.fail("Continue Button not enabled. Further Validations cannot be done.", driver);
		}
		Utils.waitForPageLoad(driver);
		Utils.waitUntilElementDisappear(driver, waitLoader);
	}
	
	public String getAVSHeadingforRadioButton()throws Exception{
		return BrowserActions.getText(driver, addressHeading.get(0), "Radio button Heading");
	}
	
	public void clickCancelAvsButton()throws Exception{
		BrowserActions.clickOnElement(cancelAvsButton, driver, "Cancel Button in Avs Modal");
	}
	
	public String getSaleTax()throws Exception{
		return BrowserActions.getText(driver, txtSalesTaxValue, "Radio button Heading");
	}
	
	public void clickCorrectAddress()throws Exception{
		Utils.waitForElement(driver, lnkCorrectAddress);
		BrowserActions.clickOnElementX(lnkCorrectAddress, driver, "Thats My Address");
		Utils.waitForPageLoad(driver);
	}
	
	public void clickAvsContinueButton()throws Exception{
		
		if(Utils.waitForElement(driver, mdlAddressSuggestion)){
			if(Utils.waitForElement(driver, btnContinueInAddressSuggestionModal))
				BrowserActions.clickOnElementX(btnContinueInAddressSuggestionModal, driver, "Continue Button in AVS");

			if(Utils.waitForElement(driver, btnCorrectAddress))
				BrowserActions.clickOnElementX(btnCorrectAddress, driver, "Continue Button in AVS");
			Utils.waitUntilElementDisappear(driver, mdlAddressSuggestion);
		}
		Utils.waitUntilElementDisappear(driver, waitLoader);
		Utils.waitForPageLoad(driver);

	}
	
	public void clickFirstPlccCard()throws Exception{
		BrowserActions.clickOnElement(section_First_PLCC_Card, driver, "PLCCCard");
		Utils.waitForPageLoad(driver);
	}

	public String getCurrentSection()throws Exception{
	    return "";
    }
	public String getSelectedCardType()throws Exception{
	    return "";
    }
	
	/**
	 * To verify visibility of PLCC Step 2 modal 
	 * @return boolean - true/false if step two modal is fully visible
	 * @throws Exception
	 */
	public boolean verifyStepTwoPLCCVisibleFull()throws Exception{
		if(Utils.waitForElement(driver, mdlPLCCApprovalStep2)) {
			boolean verifyWidth = mdlPLCCApprovalStep2.getSize().width <= driver.manage().window().getSize().width;
			boolean verifyHeight = mdlPLCCApprovalStep2.getSize().height <= driver.manage().window().getSize().height;
			return verifyWidth && verifyHeight;
		}
		else {
			Log.event("PLCC Step 2 modal is not displayed.");
			return false;
		}
	}
	
	/**
	 * To verify card expiration date text color
	 * @return boolean - true/false if date is shown in corrrect clor
	 * @throws Exception
	 */
	public boolean verifyExpirationDateColor() throws Exception {
		String expiredDateColor = demandWareProperty.get("card_date_color_expired");
		String validDateColor = demandWareProperty.get("card_date_color_valid");
		for (WebElement cardDate : cardExpDates) {
			String[] date = cardDate.getText().split("\\s");
			if (date.length != 0) {
				int expMonth = StringUtils.getNumberInString(date[1]);
				int expYear = StringUtils.getNumberInString(date[2]);
				int currentMonth = StringUtils.getCurrentDateDetails("month");
				int currentYear = StringUtils.getCurrentDateDetails("year") % 100;
				if (currentYear < expYear || (currentYear == expYear && currentMonth < expMonth - 1)) {
					if (!cardDate.getCssValue("color").equals(validDateColor))
						return false;
				}
				if (currentYear > expYear || (currentYear == expYear && currentMonth >= expMonth - 1)) {
					if (!cardDate.getCssValue("color").equals(expiredDateColor))
						return false;
				}
			}
		}
		return true;
	}
	
	/**
	 To verify address fields are empty on checkout step 2 
	 * @return - true/false if fields are empty
	 * @throws Exception
	 */
	public boolean verifyAddressFieldsEmpty() throws Exception{
		boolean fNameEmpty = BrowserActions.getText(driver, txtBillingFirstName, "First Name").equals("");
		boolean lNameEmpty = BrowserActions.getText(driver, txtBillingLastName, "Last Name").equals("");
		boolean addressLine1Empty = BrowserActions.getText(driver, txtBillingAddress, "Address Line 1").equals("");
		boolean addressLine2Empty = BrowserActions.getText(driver, txtBillingAddress2, "Address Line 2").equals("");
		boolean zipEmpty = BrowserActions.getText(driver, txtBillingZipcode, "ZIP Code").equals("");
		boolean stateEmpty;
		if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(btnBillingState)))) {
			stateEmpty = BrowserActions.getText(driver, btnBillingState, "State").equals("");
		}else {
			stateEmpty = BrowserActions.getText(driver, btnBillingStateDiv, "State").equals("");
		}
		boolean cityEmpty = BrowserActions.getText(driver, txtBillingCity, "City").equals("");
		boolean phoneNumberEmpty = BrowserActions.getText(driver, txtBillingFirstName, "Phone Number").equals("");
		
		return fNameEmpty && lNameEmpty && addressLine1Empty && addressLine2Empty && zipEmpty && stateEmpty && cityEmpty && phoneNumberEmpty;
	}
	
	/**
	 * To get Shipping rates as a LinkedHashMap
	 * 
	 */
	public LinkedHashMap<ShippingMethod, Double> getShippingRateTable() throws Exception{
		LinkedHashMap<ShippingMethod, Double> shippingTable = new LinkedHashMap<ShippingMethod, Double>();
		double cost;
		if(Utils.waitForElement(driver, spanStandardDeliveryCost)) {
			cost = StringUtils.getPriceFromString(BrowserActions.getText(driver, spanStandardDeliveryCost, "Shipping cost from shipping table"));
			shippingTable.put(ShippingMethod.Standard, cost);
		}
		if(Utils.waitForElement(driver, spanExpressDeliveryCost)) {
			cost = StringUtils.getPriceFromString(BrowserActions.getText(driver, spanExpressDeliveryCost, "Shipping cost from shipping table"));
			shippingTable.put(ShippingMethod.Express, cost);
		}
		if(Utils.waitForElement(driver, spanSuperFastDeliveryCost)) {
			cost = StringUtils.getPriceFromString(BrowserActions.getText(driver, spanSuperFastDeliveryCost, "Shipping cost from shipping table"));
			shippingTable.put(ShippingMethod.SuperFast, cost);
		}
		if(Utils.waitForElement(driver, spanNextDayDeliveryCost)) {
			cost = StringUtils.getPriceFromString(BrowserActions.getText(driver, spanNextDayDeliveryCost, "Shipping cost from shipping table"));
			shippingTable.put(ShippingMethod.NextDay, cost);
		}
		return shippingTable;
	}
	
	/**
	 * To verify that all faster available shipping methods are more expensive than slower slower methods
	 * <br>(Not applicable if shipping coupon is used on a faster method)
	 * @return true/false if delivery options are correctly priced
	 * @throws Exception
	 */
	public boolean verifyShippingRatesDifferent() throws Exception {
		LinkedHashMap<ShippingMethod, Double> shippingTable = getShippingRateTable();
		if(shippingTable.isEmpty()) {
			Log.failsoft("Shipping options are not available or not loaded.");
			return false;
		} else if (shippingTable.size() == 1) {
			Log.event("Only one Shipping option is available.");
			return true;
		} else {
			Collection<Double> shippingRates = shippingTable.values();
			Object[] values = shippingRates.toArray();
			for(int i = 0; i < shippingRates.size()-1; i++) {
				if((double)values[i+1] <= (double)values[i])
					return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify if selected delivery option cost is reflected in order summery section
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyShippingRateInOrderSummary() throws Exception {
		for(WebElement shipOption : rdoShippingMethods) {
			BrowserActions.clickOnElementX(shipOption, driver, "delivery option radio button");
			Utils.waitForElement(driver, shipOption);
			clickOnContinueInShippingMethodExceptionOverlay();
			WebElement txtShipCost = shipOption.findElement(By.xpath("../..")).findElement(By.cssSelector(".shipping-method-name")).findElement(By.xpath("../span"));
			String shipCostTable = BrowserActions.getText(driver, txtShipCost, "Shipping cost from shipping table");
			Float shipCost = Float.parseFloat(shipCostTable.split("\\$")[1].replace(")", ""));
			String totalShipCost;
			try {
				WebElement txtSurchargeCost = shipOption.findElement(By.xpath("../..")).findElement(By.cssSelector(".shippingsurcharge"));
				String surchargeCostTable = BrowserActions.getText(driver, txtSurchargeCost, "Shipping cost from shipping table");
				Float surchargeCost = Float.parseFloat(surchargeCostTable.split("\\$")[1].split("S")[0].trim());
				totalShipCost = Float.toString(shipCost + surchargeCost);
			} catch(Exception e) {
				totalShipCost = Float.toString(shipCost);
			}
			String shipCostOrderSummary = BrowserActions.getText(driver, orderSummaryShippingCost, "Shipping cost in order summery.").replace("$", "");
			if(!shipCostOrderSummary.contains(totalShipCost)) {
				return false;
			}
		}
		return true;
	}
	
	public void mouseHoverShippingContinue()throws Exception{
		BrowserActions.mouseHover(driver, btnShippingContinue, "Shipping Continue");
	}
	
	public void clickShippingContinue()throws Exception{
		BrowserActions.clickOnElementX(btnShippingContinue, driver, "Shipping Continue");
	}
	
	public void mouseHoverPaymentContinue()throws Exception{
		BrowserActions.mouseHover(driver, btnPaymentContinue, "Payment Continue");
	}
	
	public void mouseHoverPlaceOrder()throws Exception{
		BrowserActions.mouseHover(driver, btnPlaceOrder, "Place Order");
	}

	public void fillingShippingAddressADS(HashMap<String, String> addressData, WebDriver driver) {
		
		try {
			BrowserActions.typeOnTextField(txtFirstName, addressData.get("firstname") + " " + addressData.get("middlename"), driver, "First Name");
			BrowserActions.typeOnTextField(txtLastName, addressData.get("lastname"), driver, "Last Name");
			BrowserActions.typeOnTextField(txtAddressline1Shipping, addressData.get("streetnumber") + " " + addressData.get("streetname"), driver, "Address Line 1");
			BrowserActions.typeOnTextField(txtAddressline2Shipping, addressData.get("streettype"), driver, "Address Line 2");
			BrowserActions.typeOnTextField(txtZipcode, addressData.get("zipcode"), driver, "Zip Code");
			BrowserActions.typeOnTextField(txtCity, addressData.get("city"), driver, "City");
			BrowserActions.typeOnTextField(txtPhone, "9879879879", driver, "Phone");
			
			if(Utils.waitForElement(driver, txtAddressline2ShippingError)) {
				Log.event("Address Line 2 error from given PLCC data. Clearing field...");
				BrowserActions.typeOnTextField(txtAddressline2Shipping, "", driver, "Address Line 2");
			}
		} catch (Exception e) {
			Log.event("Element not found in shipping address filling section.");
		}	
	}
	
	public void fillingBillingAddressADS(HashMap<String, String> addressData, WebDriver driver) {
		
		try {
			BrowserActions.typeOnTextField(txtFieldFirstname, addressData.get("firstname") + " " + addressData.get("middlename"), driver, "First Name");
			BrowserActions.typeOnTextField(txtFieldLastname, addressData.get("lastname"), driver, "Last Name");
			BrowserActions.typeOnTextField(txtFieldAddressline1, addressData.get("streetnumber") + " " + addressData.get("streetname"), driver, "Address Line 1");
			BrowserActions.typeOnTextField(txtFieldAddressline2, addressData.get("streettype"), driver, "Address Line 2");
			BrowserActions.typeOnTextField(inpPostalAddBiling, addressData.get("zipcode"), driver, "Zip Code");
			BrowserActions.typeOnTextField(txtFieldCity, addressData.get("city"), driver, "City");
			BrowserActions.typeOnTextField(txtFieldPhone, "9879879879", driver, "Phone");
			
		} catch (Exception e) {
			Log.event("Element not found in billing address filling section.");
		}	
	}
	
	/**
	 * To verify system navigated to shipping section
	 * @return boolean - True if navigated to shipping section else false
	 * @throws Exception
	 */
	public boolean verifyNavigatedToShippingSection() throws Exception {
		if(Utils.waitForElement(driver, divCheckoutShippingExpand)) {
			return true;
		}
		return false;
	}
	
	/**
	 * To verify system navigated to billing section
	 * @return boolean - True if navigated to billing section else false
	 * @throws Exception
	 */
	public boolean verifyNavigatedToBillingSection() throws Exception {
		if(Utils.waitForElement(driver, divCheckoutBillingExpand)) {
			return true;
		}
		return false;
	}
	
	/**
	 * To get product Brand from cart line items based on given product ID
	 * @param prdId - Product ID
	 * @return String of Current Brand short name
	 * @throws Exception - Exception
	 */
	public Brand getProductBrand(String prdId) throws Exception {
		String productVariation = null;
		String productURL = null;
		for(int i=0 ; i < lstCartedItems.size() ; i++) {
			WebElement cartItemImage = lstCartedItems.get(i).findElement(By.cssSelector(".item-image>a"));
			productVariation = UrlUtils.getProductIDFromURL(cartItemImage.getAttribute("href"));
			if(productVariation.equalsIgnoreCase(prdId)) {
				productURL = lstCartedItems.get(i).findElement(By.cssSelector(".item-image>a")).getAttribute("href");
				break;
			}
		}
		productURL = UrlUtils.getSharedCartRedirectURL(productURL);
		return UrlUtils.getBrandFromUrl(productURL);
	}
	
	/**
	 * To get the Product ID and Brand Name from cart line items.
	 * @return HashMap<String, Brand> - HashMap<productId, brandName> of cart line items
	 * @throws Exception
	 */
	public HashMap<String, Brand> getProductVariationAndBrandName() throws Exception {
		HashMap<String, Brand> productDetails = new HashMap<String, Brand>();
		String productVariation = null;
		Brand brandName = null;
		for(int i=0 ; i < lstCartedItems.size() ; i++) {
			WebElement cartItemImage = lstCartedItems.get(i).findElement(By.cssSelector(".item-image>a"));
			productVariation = UrlUtils.getProductIDFromURL(cartItemImage.getAttribute("href"));
			brandName = getProductBrand(productVariation);
			Log.event(productVariation + ":" + brandName.toString());
			productDetails.put(productVariation, brandName);
		}
		return productDetails;
	}

	/**
	 * To verify the gift card fee from shipping tool-tip overlay in the checkout page.
	 * @return true - if gift card fee is displayed as expected
	 * <br>false - if gift card fee is not displayed as expected
	 * @throws Exception
	 */
	public boolean verifyGiftCardFeeInToolTip() throws Exception {
		String giftCardFeeExpected = demandWareProperty.get("gift_Card_Fee");
		DecimalFormat df = new DecimalFormat("#.##");
		Double giftCardPrice, singleGiftCardPrice, giftCardQty, subTotal, shippingTotal, discountPrice;
		giftCardPrice = singleGiftCardPrice = giftCardQty = subTotal = shippingTotal = discountPrice = 0.00;
		for(int i=0 ; i < lstSurchargeProductRate.size() ; i++) {
			String surchargeProductName = lstSurchargeProductRate.get(i).findElement(By.cssSelector(".label")).getText();
			if(surchargeProductName.equalsIgnoreCase("Gift Card Fee:")) {
				singleGiftCardPrice = Double. parseDouble(lstSurchargeProductRate.get(i).findElement(By.cssSelector(".value")).getText().split("\\$")[1]);
				giftCardQty =  Double.parseDouble(lstSurchargeProductRate.get(i).findElement(By.cssSelector(".value")).getText().split("\\$")[0].trim().replaceAll("x", ""));
				giftCardPrice = Double.parseDouble(df.format(giftCardQty * singleGiftCardPrice));
			} else {
				subTotal = Double.parseDouble( df.format((subTotal + Double.parseDouble(lstSurchargeProductRate.get(i).findElement(By.cssSelector(".value")).getText().split("\\$")[1]))) );
			}
		}
		subTotal = Double.parseDouble( df.format((subTotal + Double.parseDouble(txtShippingPriceInTooltip.getText().split("\\$")[1]) + giftCardPrice)) );
		try {
			if(discountPriceToolTip.isDisplayed() && discountPriceToolTip.getText().split("\\$")[0].contains("-")) {
				discountPrice = Double.parseDouble(discountPriceToolTip.getText().split("\\$")[1]);
				subTotal = Double.parseDouble( df.format((subTotal - discountPrice)));
			}
		} catch(NoSuchElementException e) {
			Log.event("Shipping discount is not applied");
		}
		
		shippingTotal = Double.parseDouble(totalFeeInTooltip.findElement(By.cssSelector(".value")).getText().split("\\$")[1]);
		if((subTotal.compareTo(shippingTotal) == 0) && (singleGiftCardPrice == Double.parseDouble(giftCardFeeExpected))) {
			return true;
		} else {
			return false;
		}
	}
}

