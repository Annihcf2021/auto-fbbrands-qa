package com.fbb.pages;

import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

public class PLCC extends LoadableComponent<PLCC> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(tagName = "body")
	WebElement readyElement;

	@FindBy(css = ".plcc-apply-error-model")
    WebElement mdlApplied;

    @FindBy(css = ".plcc-apply-error-model .header")
    WebElement lblModelHeader;

    @FindBy(css = ".plcc-apply-error-model .sub-header")
    WebElement lblModelSubHeader;

    @FindBy(css = ".guest-sign-in")
    WebElement btnSignInToSaveCard;

    @FindBy(css = ".continue-shopping")
    WebElement btnContinueTo;

    @FindBy(css = ".continue-message")
    WebElement lblContinueMessage;



	//================================================================================
	//			WebElements Declaration End
	//================================================================================




	/**
	 * constructor of the class
	 *
	 * @param driver
	 *            : Webdriver
	 *
	 */
	public PLCC(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !Utils.waitForElement(driver, readyElement))
		    Log.fail("Footer Panel didn't display", driver);

		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		//Utils.waitForPageLoad(driver);
	}

	public void clickOnSignInToSaveThisCard()throws Exception{
        BrowserActions.clickOnElementX(btnSignInToSaveCard, driver, "Sign In To Save Card Button");
        Utils.waitForPageLoad(driver);
    }

    public void clickOnContinueTo()throws Exception{
        BrowserActions.clickOnElementX(btnContinueTo, driver, "Sign In To Save Card Button");
        Utils.waitForPageLoad(driver);
    }

}
