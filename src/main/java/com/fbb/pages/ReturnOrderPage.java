package com.fbb.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.account.OrderHistoryPage;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class ReturnOrderPage extends LoadableComponent<ReturnOrderPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	String runBrowser;
	
	@FindBy(css = "item-visibility-container")
	WebElement readyElement;
	
	@FindBy(css = ".return-items")
	WebElement divReturnPage;
	
	@FindBy(css = ".order-details-section")
	WebElement divOrderDetails;
	
	@FindBy(css = ".return-items.return-confirmation")
	WebElement divDownloadReturnLablePage;
	
	@FindBy(xpath = "//a[contains(@class,'continue' )]")
	WebElement btnReturn;

	@FindBy(name = "dwfrm_ordertrack_cancelordert")
	WebElement btnCancel;
	
	@FindBy(name = "dwfrm_ordertrack_returnorder")
	WebElement btnReturnSelectedItems;
	
	@FindBy(css = ".item-error")
	WebElement lblErorMessage;
	
	@FindBy(css = ".returns-check")
	List<WebElement> checkboxReturn;
	
	@FindBy(css = ".returns-check")
	WebElement checkboxReturn1;
	
	@FindBy(css = ".item-details .name")
	WebElement returnProductName;
	
	@FindBy(css = ".item-image")
	WebElement returnProductImage;
	
	@FindBy(css = ".item-details")
	WebElement returnProductDetails;
	
	@FindBy(css = ".qty-value.value")
	List<WebElement> returnProductQty;
	
	@FindBy(css = ".eligible-item.list-tems ")
	List<WebElement> returnEligibleProduct;
	
	@FindBy(css = ".eligible-item.list-tems .quantityinput")
	WebElement returnQty;
	
	@FindBy(css = ".eligible-item.list-tems .quantityinput")
	List<WebElement> litReturnQty;
	
	@FindBy(css = ".eligible-item.list-tems .reasoninput")
	WebElement returnReason;
	
	@FindBy(css = ".eligible-item.list-tems #reason")
	List<WebElement> lstReturnReason;
	
	@FindBy(css = ".return-list-download")
	WebElement btnDownloadReturnLable;
	
	@FindBy(css = ".item-instructions")
	WebElement textInstructionForReturn;
	
	@FindBy(css = ".eligible-list")
	WebElement eligibleForReturn;
	
	@FindBy(css = ".non-eligible-list")
	WebElement nonEligibleForReturn;
	
	@FindBy(css = ".non-eligible-item.list-tems .returns-check")
	WebElement nonEligibleForReturnCheckBox;
	
	@FindBy(css = "#profilemenu .account-links a")
	WebElement lstAccountMenu;
	
	@FindBy(css = "#secondary h1.heading.update-value.hide-desktop.hide-tablet")
	WebElement lblOverView;
	
	@FindBy(css = ".navigation-links-row .order-history a")
	WebElement lnkOrderHistory;
	
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public ReturnOrderPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
		runBrowser  = Utils.getRunBrowser(driver);
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Return Page did not open up.", driver);
		}
	}

	/**
	 * To get the Load status of page
	 * @return boolean - 'true'(Page loaded) / 'false'(Page not loaded)
	 * @throws Exception - Exception
	 */
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}
	
	/**
	 * To click on Cancel button
	 * @return -
	 * @throws Exception - Exception
	 */
	public void clickCancelButton()throws Exception{
		BrowserActions.clickOnElementX(btnCancel, driver, "Cancel button ");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on Return selected item button
	 * @return -
	 * @throws Exception - Exception
	 */
	public void clickReturnSelectedItemButton()throws Exception{
		BrowserActions.clickOnElementX(btnReturnSelectedItems, driver, "Cancel button ");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on Return button
	 * @return void
	 * @throws Exception - Exception
	 */
	public void clickOnReturn()throws Exception{
		BrowserActions.clickOnElementX(btnReturn, driver, "Return button ");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click check box based on index
	 * @return -
	 * @throws Exception - Exception
	 */
	public void clickCheckboxBasedOnIndex(int index)throws Exception{
		BrowserActions.selectRadioOrCheckbox(checkboxReturn.get(index), "yes");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click check box based on index
	 * @return -
	 * @throws Exception - Exception
	 */
	public void uncheckCheckboxBasedOnIndex(int index)throws Exception{
		BrowserActions.selectRadioOrCheckbox(checkboxReturn.get(index), "no");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click check box based on index
	 * @return -
	 * @throws Exception - Exception
	 */
	public String getReturnQtyBasedOnIndex(int index)throws Exception{
		return BrowserActions.getText(driver, returnProductQty.get(index), "Return Qty");
	}
	
	/**
	 * To expand an collapse overview
	 * @param state, value should be open when need to expand, else close
	 * @throws Exception - Exception
	 */
	public void expandCollapseOverview(String state)throws Exception{
		if(Utils.isMobile()) {
			if(state.equals("open") && Utils.waitForElement(driver, lstAccountMenu)){
				Log.event("My Account Overview Menu " + state + "ed");
			} else {
				BrowserActions.clickOnElementX(lblOverView, driver, "My Account Overview Menu Link");
				Log.event("Clicked On Overview Link");
			}
		}
	}
	
	/**
	 * To click on order history link
	 * @return OrderHistoryPage -
	 * @throws Exception -
	 */
	public OrderHistoryPage clickOnOrderHistoryLink() throws Exception{
		if(Utils.isMobile()) {
			expandCollapseOverview("open");
		}
		BrowserActions.clickOnElementX(lnkOrderHistory, driver, "Order History link");
		Utils.waitForPageLoad(driver);
		return new OrderHistoryPage(driver).get();
	}
	
	/**
	 * To select return qty
	 * @return -
	 * @throws Exception - Exception
	 */
	public void slectReturnQtyBasedOnProductIndexAndQtyIndex(int prdIndex, int qtyIndex)throws Exception{
		BrowserActions.selectDropdownByIndex(litReturnQty.get(prdIndex), qtyIndex);
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To select return qty
	 * @return -
	 * @throws Exception - Exception
	 */
	public void slectReturnReasonBasedOnProductIndexAndReasonIndex(int prdIndex, int reasonIndex)throws Exception{
		BrowserActions.selectDropdownByIndex(lstReturnReason.get(prdIndex), reasonIndex);
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on Download Return lable button
	 * @return void
	 * @throws Exception - Exception
	 */
	public void clickOnDownloadReturnLable()throws Exception{
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		BrowserActions.clickOnElementX(btnDownloadReturnLable, driver, "Download Return lable button ");
		Utils.waitForPageLoad(driver);
		driver.switchTo().window(tabs.get(0));
	}
	

}
