package com.fbb.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.footers.Footers;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;


public class GiftCardPage extends LoadableComponent<GiftCardPage> {

	private WebDriver driver;
	public boolean isPageLoaded;
	public ElementLayer elementLayer;
	EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");
	
	@FindBy(xpath = "//div[@id='primary']//a[text()='shop gift cards']")
	WebElement lnkGiftCard;
	
	@FindBy(css = ".button-gift .gift")
	WebElement btnGiftCard;
	
	@FindBy(xpath = "//div[@id='primary']//a[text()='shop e-gift cards']")
	WebElement lnkGiftCert;
	
	@FindBy(css = ".button-gift .e-gift")
	WebElement btnGiftCert;

	@FindBy(css = ".gift-card-message")
	WebElement lblBackgroundContent;
	
	@FindBy(css = ".pt_content")
	WebElement readyElement;
	
	@FindBy(css = "#wrapper")
	WebElement wrapperElement;

	@FindBy(css = "a.js-gift-card-balance")
	List<WebElement> balanceCheckUpButton;

	@FindBy(css = ".js-gc-check-submit")
	WebElement applyButton;

	@FindBy(css = ".giftcard-lookup-error")
	WebElement invalidGiftCardErorMsg;

	@FindBy(css = "[id*='dwfrm_giftcard_cardnumber']")
	WebElement txtfieldCardname;

	@FindBy(css = ".cardnumber input")
	WebElement txtfieldCardnumber;

	@FindBy(css = ".form-row.pin.required input")
	WebElement txtFieldCardPin;
	
	@FindBy(css = ".balance-result-block .svs-card-amount span:not(.svs-card-balance)")
	WebElement txtGiftCardAmountSymbol;
	
	@FindBy(css = ".gift-card-result-section img.hide-tablet.hide-mobile.gift-cart-bg-image")
	WebElement imgGiftCardResultDesktop;
	
	@FindBy(css = ".gift-card-result-section img.hide-desktop.hide-mobile.gift-cart-bg-image")
	WebElement imgGiftCardResultTablet;
	
	@FindBy(css = ".gift-card-result-section img.hide-desktop.hide-tablet.gift-cart-bg-image")
	WebElement imgGiftCardResultMobile;
	
	@FindBy(css = ".balance-result-block .svs-card-amount")
	WebElement txtGiftCardAmount;
	
	@FindBy(css = ".balance-result-block .svs-card-balance")
	WebElement txtGiftCardBalance;
	
	@FindBy(css = ".balance-result-block .available-balance-text")
	WebElement txtAvailableBalance;
	
	@FindBy(css = ".balance-result-block .svs-masked-number")
	WebElement txtMaskedNumber;
	
	@FindBy(css = ".note-section")
	WebElement txtNoteSection;

	@FindBy(css = ".giftcard-lookup-error")
	WebElement txtWrongGiftCardErrorMsg;
	
	@FindBy(xpath = "//span[contains(@id, 'dwfrm_giftcard_cardnumber')][@class='error']")
	WebElement txtInvalidCardNumberErr;
	
	@FindBy(xpath = "//span[contains(@id, 'dwfrm_giftcard_pin')][@class='error']")
	WebElement txtInvalidPinNumberErr;
	
	@FindBy(css = ".cardnumber span.error")
	WebElement errfieldCardnumber;

	@FindBy(css = ".pin span.error")
	WebElement errFieldCardPin;

	@FindBy(css = ".dialog-content,ui-dialog-content.ui-widget-content")
	WebElement backGroundContent;

	@FindBy(css = ".gc-checkbalance")
	WebElement divChkBalanceModelPopup;
	
	@FindBy(css = ".gift-card-lookup-section img.hide-tablet.hide-mobile.gift-cart-bg-image")
	WebElement imgGiftCardBgImageDesktop;
	
	@FindBy(css = ".gift-card-lookup-section img.hide-desktop.hide-tablet.gift-cart-bg-image")
	WebElement imgGiftCardBgImageMobile;
	
	@FindBy(css = ".gift-card-lookup-section img.hide-desktop.hide-mobile.gift-cart-bg-image")
	WebElement imgGiftCardBgImageTablet;

	@FindBy(css = "div.heading")
	WebElement txtPleaseEnterCardnoandPin;

	@FindBy(css = "button[title='Close']")
	WebElement btnclose;

	@FindBy(css = "div.form-row.cardnumber .label-text")
	WebElement placeholderCardNumber;

	@FindBy(css = "div.form-row.pin .label-text")
	WebElement placeholderPin;
	
	@FindBy(css = ".balance-result-section")
	WebElement sectionGiftCardBalance;

	@FindBy(css = ".gc-show-message")
	WebElement invalidcardMsg;

	@FindBy(css = ".gc-show-message>span")
	WebElement totalBalanceMsg;
	
	@FindBy(css = ".gc-show-message")
	WebElement totalBalance;

	@FindBy(css = "label[for ^=dwfrm_giftcard_cardnumber] span.label-text")
	WebElement cardnamePlaceHolder;

	@FindBy(css = "label[for ^=dwfrm_login_password] span.label-text")
	WebElement cardpinPlaceHolder;

	@FindBy(css = "label[for ^='dwfrm_giftcard_cardnumber'] .error")
	WebElement errorMsgCardName;

	@FindBy(css = "label[for ^=dwfrm_giftcard_pin] .error")
	WebElement errorMsgCardPin;

	@FindBy(css = ".giftcard-lookup-error")
	WebElement giftCardError;

	@FindBy(css = ".form-row.pin.required input")
	WebElement txtFiedCardPin;
	
	@FindBy(css = ".another-card")
	WebElement lnkAnotherCard;

	@FindBy(css = ".form-row.cardnumber.required input")
	WebElement txtfieldCardname1;

	@FindBy(css = "div.gift-landing.hide-tablet.hide-desktop div.main-slot button")
	WebElement btnGiftCertMobile;

	@FindBy(css = "div.gift-landing.hide-tablet.hide-desktop div.main-slot-1 button")
	WebElement btnGiftCardMobile;

	@FindBy(css = ".breadcrumb .hide-mobile")
	List<WebElement> lblBreadCrumb;
	
	@FindBy(css = ".breadcrumb .hide-mobile[title*='Home']")
	WebElement lblBreadCrumbHome;
	
	@FindBy(css = ".breadcrumb .current-element.hide-desktop")
	WebElement lblBreadCrumbHomeMobile;
	
	@FindBy(css = ".breadcrumb .current-element.hide-mobile")
	WebElement lblBreadCrumbGiftCardBalance;
	
	@FindBy(css = ".mobile-hide .js-gift-card-balance")
	WebElement btnCheckBalanceDesktopTablet;
	
	@FindBy(css = ".desktop-hide .js-gift-card-balance")
	WebElement btnCheckBalanceMobile;

	// --------------- Header Section ---------------------- //
	/**
	 * Initiate the web driver
	 * @param driver -web driver
	 * 
	 */
	public GiftCardPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail( "Gift card Page didn't open", driver);
		}

		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	/**
	 * To verify Gift card balance lookup fields are mandatory
	 * @return boolean - true/false if fields are mandatory
	 * @throws Exception - Exception
	 */
	public Boolean verifyGiftCardBalanceLookUpFieldsMandatory()throws Exception {
		Boolean flag = true;
		String[] fields= {"Card Number","Pin"};
		String errorColor1 = demandwareData.get("card_date_color_expired");
		String errorColor2 = demandwareData.get("card_date_color_expired_2");
		List<WebElement> ListEditableTextfields=driver.findElements(By.cssSelector("span[id^=dwfrm_giftcard].error"));

		for (int i = 0; i < ListEditableTextfields.size(); i++) {
			if (ListEditableTextfields.get(i).getAttribute("innerHTML").equals(fields[i])){
				flag = Utils.verifyCssPropertyForElement(ListEditableTextfields.get(i), "color", errorColor1)
						|| Utils.verifyCssPropertyForElement(ListEditableTextfields.get(i), "color", errorColor2);
				if(flag){
					Log.message(fields[i]+" is mandatory field");
				}else
				{
					Log.message(fields[i]+" is not mandatory field");
				}
			}
		}
		return flag;
	}
	
	/**
	 * To click on Gift Card button
	 */
	public void clickGiftCardButton() throws Exception {
		if (Utils.waitForElement(driver, btnGiftCard)) {
			BrowserActions.clickOnElementX(btnGiftCard.findElement(By.xpath("./..")), driver, "Gift Card button");
		} else
			BrowserActions.clickOnElementX(lnkGiftCard, driver, "Gift Card button");
	}
	
	/**
	 * To click on Electronic Gift Card button
	 */
	public void clickEGiftCardButton() throws Exception {
		if (Utils.waitForElement(driver, btnGiftCert)) {
			BrowserActions.clickOnElementX(btnGiftCert.findElement(By.xpath("./..")), driver, "e-Gift Card button");
		} else
			BrowserActions.clickOnElementX(lnkGiftCert, driver, "e-Gift Card button");
	}
	
	/**
	 * To click on Apply button in check balance modal
	 * @throws Exception - Exception
	 */
	public void clickApplyInCheckBalanceModal() throws Exception {
		if (Utils.waitForElement(driver, divChkBalanceModelPopup)) {
			Utils.waitForElement(driver, applyButton);
			BrowserActions.clickOnElementX(applyButton, driver, "CheckUpBalance");
			if (Utils.waitForElement(driver, totalBalance) || Utils.waitForElement(driver, txtInvalidCardNumberErr) || Utils.waitForElement(driver, txtInvalidPinNumberErr)) {
				return;
			}
		}
	}

	/**
	 * To enter card number and click on apply button 
	 * @param String - cardNumber
	 * @param String - cardPin
	 * @throws Exception - Exception
	 */
	public void checkGiftCardBalance(String cardNumber, String cardPin) throws Exception{
		if (!Utils.waitForElement(driver, divChkBalanceModelPopup)) {
			clickOnCheckYourBalance();
		}
		BrowserActions.typeOnTextField(txtfieldCardnumber,cardNumber, driver, "order Number Field");
		BrowserActions.typeOnTextField(txtFieldCardPin,cardPin, driver, "order Number Field");
		clickApplyInCheckBalanceModal();
	}

	/**
	 * To Validate the placeholder is displayed
	 * @param String - element to be validated
	 * @param Object - Page object instance
	 * @return boolean - status as boolean
	 * @throws Exception - Exception
	 */
	public boolean vaidateplaceholder(String element, Object obj)throws Exception{
		boolean flag=false;
		WebElement verifyElement=ElementLayer.getElement(element, obj);
		try{
			WebElement placeholder=verifyElement.findElement(By.cssSelector(".input-focus"));
			if(placeholder.isDisplayed()){
				flag=true;
			}else 
			{
				Log.message("Placeholder did not change for "+element);
			}
		}
		catch(Exception e){
			Log.message("Placeholder did not change for "+element);
		}
		return flag;
	}

	/**
	 * To enter card number in field
	 * @param String - cardNumber
	 * @throws Exception - Exception
	 */
	public void enterCardNumber(String cardNumber) throws Exception{
		BrowserActions.typeOnTextField(txtfieldCardnumber,cardNumber, driver, "Card Number Field");
	}
	
	/**
	 * To enter card number and click on apply button 
	 * @param String - cardNumber
	 * @param String - cardPin
	 * @throws Exception - Exception
	 */
	public void ApplyCardDetails(String cardNumber, String cardPin) throws Exception{
		BrowserActions.typeOnTextField(txtfieldCardnumber,cardNumber, driver, "order Number Field");
		BrowserActions.typeOnTextField(txtFieldCardPin,cardPin, driver, "order Number Field");
		clickApplyInCheckBalanceModal();
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on close button
	 * @throws Exception - Exception
	 */
	public void clickClose() throws Exception{
		Utils.waitForElement(driver, btnclose);
		BrowserActions.clickOnElementX(btnclose, driver, "Close button");
	}

	/**
	 * To type card pin in field
	 * @param String - cardpin 
	 * @throws Exception - Exception
	 */
	public void enterCardPin(String cardpin)throws Exception {
		BrowserActions.typeOnTextField(txtFiedCardPin, cardpin, driver, "CardPin");
	}

	/**
	 * To get the error message displayed in card Name field
	 * @return String - error message
	 * @throws Exception - Exception
	 */
	public String getErrorMessageCardname()throws Exception{
		return BrowserActions.getText(driver, errorMsgCardName, "Error message");
	}
	
	
	/**
	 * To verify the Card Number is masked in the Gift card balance section
	 * @return boolean - True If card number is masked, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyCardNumberIsMasked() throws Exception{
		String value = BrowserActions.getText(driver, txtMaskedNumber, "Card number").replace("*", "").trim();
		if(value.length() == 4) {
			return true;
		} 
		return false;
	}

	/**
	 * To get the error message displayed in card pin field
	 * @return String - error message
	 * @throws Exception - Exception
	 */
	public String getErrorMessageCardpin()throws Exception{
		return BrowserActions.getText(driver, errorMsgCardPin, "Error message");
	}
	
	/**
	 * Navigate to gift card - PDP page 
	 * @return PdpPage - page object
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToGiftCards()throws Exception{
		if(Utils.waitForElement(driver, btnGiftCard)) {
			BrowserActions.clickOnElementX(btnGiftCard.findElement(By.xpath("./..")), driver, "Gift Card button");
		}else {
			new Footers(driver).get().navigateToPhysicalGiftCard();
		}
		
		Utils.waitForPageLoad(driver);
		String wrapperClass = wrapperElement.getAttribute("class");
		if(wrapperClass.contains("pt_product-details")) {
			return new PdpPage(driver).get();
		}else if(wrapperClass.contains("product-list-page")) {
			PlpPage plp = new PlpPage(driver);
			PdpPage pdp = plp.navigateToPdp(1);
			return pdp;
		}else
			return null;
	}

	/**
	 * Navigate to gift certificate - PDP page 
	 * @return PdpPage - page object
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToGiftCertificate()throws Exception{
		if(Utils.waitForElement(driver, btnGiftCert)) {
			BrowserActions.clickOnElementX(btnGiftCert.findElement(By.xpath("./..")), driver, "Electonic Gift Card button");
		}else {
			new Footers(driver).get().navigateToEGiftCard();
		}
		Utils.waitForPageLoad(driver);
		String wrapperClass = wrapperElement.getAttribute("class");
		if(wrapperClass.contains("pt_product-details")) {
			return new PdpPage(driver).get();
		}else if(wrapperClass.contains("product-list-page")) {
			PlpPage plp = new PlpPage(driver);
			PdpPage pdp = plp.navigateToPdp(1);
			return pdp;
		}else
			return null;
	}
	
	/**
	 * To click on the Home link in Breadcrumb
	 * @return HomePage - Home page object
	 * @throws Exception - Exception
	 */
	public HomePage clickOnHomeInBC()throws Exception{
		if(Utils.isMobile())
			BrowserActions.clickOnElementX(lblBreadCrumbHomeMobile, driver, "Home Link in BC");
		else
			BrowserActions.clickOnElementX(lblBreadCrumbHome, driver, "Home Link in BC");
		
		return new HomePage(driver).get();
	}
	
	/**
	 * To verify breadcrumb have given items in order
	 * @param String - checkString
	 * @return boolean - true/false if breadcrumb is correctly displayed
	 * @throws Exception - Exception
	 */
	public boolean verifyBreadCrumb(String checkString)throws Exception{
		String compareString = new String();
		for(WebElement ele : lblBreadCrumb) {
			compareString += ele.getAttribute("innerHTML").trim();
		}
		
		Log.event("String from Element :: " + compareString);
		Log.event("Text From User :: " + checkString);
		
		return (compareString.equals(checkString))? true : false;
	}
	
	/**
	 * To click Check Balance button
	 */
	public void clickOnCheckYourBalance()throws Exception{
		if (balanceCheckUpButton.size() == 1) {
			BrowserActions.clickOnElementX(balanceCheckUpButton.get(0), driver, "Check Your balance");
		} else {
			if (Utils.isMobile()) {
				BrowserActions.clickOnElementX(btnCheckBalanceMobile, driver, "Check Your balance");
			} else {
				BrowserActions.clickOnElementX(btnCheckBalanceDesktopTablet, driver, "Check Your balance");
			}
		}
		Utils.waitForElement(driver, divChkBalanceModelPopup);
	}

}


