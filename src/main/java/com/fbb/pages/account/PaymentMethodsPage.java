package com.fbb.pages.account;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;

public class PaymentMethodsPage extends LoadableComponent<PaymentMethodsPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	private static final String MyAccountPageLinksNavigation = ".navigation-links-row";

	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");

	public final LinkedHashMap<String, String> accountDetails = new LinkedHashMap<String, String>();

	// ================================================================================
	// WebElements Declaration Start
	// ================================================================================

	@FindBy(css = ".payment-list")
	WebElement paymentList;

	@FindBy(css = ".cardinfo")
	List<WebElement> savedCardsList;

	@FindBy(css = ".pt_account.paymentinstruments")
	WebElement readyElement;

	@FindBy(css = "div.delete-creditcard-overlay")
	WebElement ovrlayDeleteCard;

	@FindBy(css = ".delete-payment-card")
	WebElement deleteOverlayBox;

	@FindBy(css = ".type.select-handle span.error")
	WebElement selectCardTypeError;

	@FindBy(css = ".owner.required input.error")
	WebElement inpOwnerError;

	@FindBy(css = "#dwfrm_paymentinstruments_creditcards_newcreditcard_owner-error")
	WebElement nameOnCardError;

	@FindBy(css = ".number input.error")
	WebElement inpCardNumberError;

	@FindBy(css = ".left-cont .form-row.creditcardnumber  label span")
	WebElement cardNumberError;

	@FindBy(css = ".month span.error")
	WebElement selectMonthError;

	@FindBy(css = ".year span.error")
	WebElement selectYearError;

	@FindBy(css = ".delete-creditcard-overlay h1")
	WebElement overlayDeleteConfHeading;

	@FindBy(css = ".delete-creditcard-overlay .waring-msg")
	WebElement overlayDeleteConfSubHeading;

	@FindBy(css = ".delete-card-number")
	WebElement spanDeleteCardNumber;

	@FindBy(css = MyAccountPageLinksNavigation + " li.payment-methods a[style*='cursor']")
	WebElement lnkPaymentMethodsAfterNavigate;

	@FindBy(css = ".confirm-delete-creditcard")
	WebElement btnConfirmDelete;

	@FindBy(css = ".cancle-delete-creditcard")
	WebElement btnCancelDelete;

	@FindBy(css = ".li.cardnumber")
	WebElement lblEnteredCardNo;

	@FindBy(css = ".li.cardholder")
	WebElement lblEnteredCardOwnerName;

	@FindBy(css = ".ui-dialog-titlebar-close")
	WebElement btnOverlayClose;

	@FindBy(css = ".default-msg")
	WebElement lblDefaultPaymentIcon;

	@FindBy(css = ".default-msg .is-default")
	WebElement iconDefaultPayment;

	@FindBy(css = ".default-msg span:not(.is-default)")
	WebElement lblDefaultPayment;

	@FindBy(css = ".add-card")
	WebElement lnkAddNewCard;

	@FindBy(css = ".cancel.cancel-button.button-text.hide-mobile")
	WebElement lnkCancelAddCard;

	@FindBy(css = ".payment-methods a")
	WebElement lnkPaymentMethods;

	@FindBy(css = ".catalog-preferences a")
	WebElement lnkCatalogPreferences;

	@FindBy(css = ".wishlists a")
	WebElement lnkWishlist;

	@FindBy(css = ".email-preferences a")
	WebElement lnkEmailPreferences;

	@FindBy(css = ".paymentslist.make-label-absolute h1")
	WebElement lblPaymentMethodsTitle;

	@FindBy(css = ".no-card-msg")
	WebElement lblNoSavedCardMessage;

	@FindBy(css = "ul.selection-list")
	WebElement creditCardDropdown;

	@FindBy(css = ".type .label-text")
	WebElement txtCreditCardTypePlaceholder;

	@FindBy(css = ".owner .label-text")
	WebElement txtNamePlaceholder;

	@FindBy(id = "dwfrm_paymentinstruments_creditcards_newcreditcard_owner-error")
	WebElement txtInvalidNamePlaceholder;

	@FindBy(css = ".creditcardnumber .label-text")
	WebElement txtCardNumberPlaceholder;

	@FindBy(css = ".creditcardnumber > label > span.error")
	WebElement txtInvalidCardNumberPlaceholder;

	@FindBy(css = ".month .label-text")
	WebElement txtExpiryMonthPlaceholder;

	@FindBy(css = ".cardinfo.Discover")
	WebElement crdDiscover;

	@FindBy(css = ".year .label-text")
	WebElement txtExpiryYearPlaceholder;

	@FindBy(css = ".left-cont div.form-row.type .custom-select")
	WebElement selectCardTypeField;

	@FindBy(css = ".left-cont div.form-row.month")
	WebElement selectMonthField;

	@FindBy(css = ".left-cont div.form-row.year .custom-select")
	WebElement selectYearField;

	@FindBy(css = "#dwfrm_paymentinstruments_creditcards_newcreditcard_type > option")
	List<WebElement> lstCardTypes;

	@FindBy(css = "#dwfrm_paymentinstruments_creditcards_newcreditcard_type")
	WebElement drpSelectCardType;

	@FindBy(css = "#dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_month")
	WebElement drpExpiryMonth;

	@FindBy(css = "#dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_year")
	WebElement drpExpiryYear;

	@FindBy(id = "dwfrm_paymentinstruments_creditcards_newcreditcard_owner")
	WebElement txtNameOnCard;

	@FindBy(css = "input[id*='dwfrm_paymentinstruments_creditcards_newcreditcard_number']")
	WebElement txtCardNumber;

	@FindBy(name = "dwfrm_paymentinstruments_creditcards_create")
	WebElement btnSave;

	@FindBy(css = "#dwfrm_paymentinstruments_creditcards_newcreditcard_makeDefault")
	WebElement chkMakeDefaultPayment;

	@FindBy(css = ".payment-methods-accepted")
	WebElement divPaymentMethodsAccepted;

	@FindBy(css = ".breadcrumb-element.hide-mobile")
	List<WebElement> lstBreadCrumbElementsDesktop;

	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement txtBreadCrumbElementMobile;

	@FindBy(css = ".hide-desktop.hide-tablet.back-arrow")
	WebElement imgBreadCrumbBackArrowMobile;

	@FindBy(css = ".carddetails .delete")
	List<WebElement> lstSavedCardDelete;

	@FindBy(css = ".confirm-delete-creditcard")
	WebElement btnConfirm;

	@FindBy(css = ".carddetails")
	List<WebElement> lstSavedCards;

	@FindBy(css = ".plcc-card-message")
	WebElement txtPLCCMessage;

	@FindBy(css = ".cardinfo")
	WebElement rowFirstCard;

	@FindBy(css = ".payment-list .card-li")
	WebElement firstCard;
	
	@FindBy(css = ".plcc-tooltip a")
	WebElement tooltipPLCC;

	@FindBy(css = ".plcc-tooltip.hide-desktop a")
	WebElement tooltipPLCCMobile;

	@FindBy(css = ".plcc-tooltip.hide-mobile a")
	WebElement tooltipPLCCDesktop;

	@FindBy(css = ".showall")
	WebElement btnShowAll;

	@FindBy(css = ".default-card .cardtype .img")
	WebElement cardImage;

	@FindBy(css = ".default-card .cardtype .cardname")
	WebElement cardName;

	@FindBy(css = ".default-card .cardholder")
	WebElement cardHolderName;

	@FindBy(css = ".default-card .cardnumber")
	WebElement cardNumber;

	@FindBy(css = ".default-card .expdate")
	WebElement cardExpDate;

	@FindBy(css = ".default-card .delete")
	WebElement deleteButton;
	
	@FindBy(css = "button.delete ")
	List<WebElement> lstDeleteButtons;

	@FindBy(css = ".expired .expdate")
	WebElement expireExpDate;

	@FindBy(css = ".carddetails.expired")
	WebElement expireCard;

	@FindBy(css = ".expdate")
	List<WebElement> cardExpDates;

	@FindBy(css = ".payment-form.hide")
	WebElement divAddNewCardModule;

	@FindBy(css = ".type .selected-option.selected")
	WebElement selectedCard;

	@FindBy(css = ".month .selected-option.selected")
	WebElement selectedMonth;

	@FindBy(css = ".year .selected-option.selected")
	WebElement selectedYear;

	@FindBy(css = ".cardinfo:not(.Visa):not(.Mastercard):not(.Discover):not(.Amex):not(.Master) .carddetails")
	List<WebElement> savedCardPLCC;

	@FindBy(css = ".year .select-option:not([label='Expiry Year'])")
	List<WebElement> lstYearsAvailable;

	@FindBy(css = ".month .select-option:not([label='Expiry Month'])")
	List<WebElement> lstMonthsAvailable;

	// ================================================================================
	// WebElements Declaration End
	// ================================================================================

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public PaymentMethodsPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Payment method page is not loaded", driver);
		}

		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	public boolean getPageLoadStatus() throws Exception {
		return isPageLoaded;
	}

	/**
	 * To delete the card details.
	 * 
	 * @param cardNumber
	 *            -
	 * @throws Exception
	 *             - Exception
	 */
	public void deleteCard(String cardNumber) throws Exception {
		try {
			if (!(driver.findElements(By.cssSelector(".no-card-msg")).size() > 0)) {
				if (cardNumber.length() > 4)
					cardNumber = cardNumber.substring(cardNumber.length() - 4, cardNumber.length());

				WebElement btnDelete = driver.findElement(By.xpath("//div[contains(text(),'" + cardNumber
						+ "')]//following-sibling::div[contains(@class,'delete')]//button"));
				BrowserActions.clickOnElementX(btnDelete, driver, "Delete Link for Card Number : " + cardNumber);
				if (Utils.waitForElement(driver, ovrlayDeleteCard)) {
					BrowserActions.clickOnElementX(btnConfirmDelete, driver, "Delete Button in Overlay");
					Utils.waitForPageLoad(driver);
				} else
					Log.fail("Confirm Delete Overlay Not opended");
			}
		} catch (NoSuchElementException e) {
			Log.event("Card not available in wallet.");
		}
	}

	/**
	 * To verify the card is present
	 * @param cardNumber
	 * @return Status Boolean
	 * @throws Exception
	 */
	public boolean verifyCardPresent(String cardNumber) throws Exception {
		if (cardNumber.length() > 4)
			cardNumber = cardNumber.substring(cardNumber.length() - 4, cardNumber.length());

		try {
			WebElement card = driver.findElement(By.xpath("//div[contains(text(),'" + cardNumber + "')]"));
			Log.event(cardNumber + " is present at: " + card.toString());
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	/**
	 * To verify payment method title
	 * 
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyPaymentMethodsTitle() throws Exception {
		if (lblPaymentMethodsTitle.getText().trim().equals("Payment Methods"))
			return true;
		else
			return false;
	}

	/**
	 * To verify Save card message
	 * 
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyNoSavedCardsMessage() throws Exception {
		if (Utils.waitForElement(driver, lblNoSavedCardMessage)) {
			if (lblNoSavedCardMessage.getText().trim().equals("No saved card found."))
				return true;
			else
				return false;
		} else
			return false;

	}

	/**
	 * To verify placeholder text
	 * 
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyPlaceHolderText() throws Exception {
		if (!txtCreditCardTypePlaceholder.getText().trim().equalsIgnoreCase("Select Credit Card Type"))
			return false;

		if (!txtNamePlaceholder.getText().trim().equalsIgnoreCase("Name on Card"))
			return false;

		if (!txtCardNumberPlaceholder.getText().trim().equalsIgnoreCase("Card Number"))
			return false;

		if (!txtExpiryMonthPlaceholder.getText().trim().equalsIgnoreCase("Expiry Month"))
			return false;

		if (!txtExpiryYearPlaceholder.getText().trim().equalsIgnoreCase("Expiry Year"))
			return false;

		return true;
	}

	/**
	 * To verify drop down should display PLCC cards first
	 * 
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyOrderOfCardsDisplayed() throws Exception {
		for (int i = 1; i < lstCardTypes.size() - 2; i++) {
			if (lstCardTypes.get(i).getAttribute("plcctype").equals("false")
					&& lstCardTypes.get(i + 1).getAttribute("plcctype").equals("true"))
				return false;
		}
		return true;
	}

	/**
	 * To select the credit card type
	 * 
	 * @param cardType
	 *            -
	 * @throws Exception
	 *             - Exception
	 */
	public void selectCardType(String cardType) throws Exception {
		BrowserActions.selectDropdownByValue(drpSelectCardType, cardType);
	}

	/**
	 * To click save payment method
	 * 
	 * @throws Exception
	 *             - Exception
	 */
	public void savePaymentMethod() throws Exception {
		BrowserActions.clickOnElementX(btnSave, driver, "Save");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To verify error message is displayed when field is empty
	 * 
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyErrorMessageDisplayedWhenFieldAreEmpty() throws Exception {
		Boolean flag = false;
		if (drpSelectCardType.getAttribute("class").contains("error"))
			flag = true;

		if (txtNameOnCard.getAttribute("class").contains("error"))
			flag = true;

		if (txtCardNumber.getAttribute("class").contains("error"))
			flag = true;

		if (drpExpiryMonth.getAttribute("class").contains("error"))
			flag = true;

		if (drpExpiryYear.getAttribute("class").contains("error"))
			flag = true;

		return flag;
	}

	/**
	 * To verify the invalid entry in card name field
	 * 
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyInvalidEntryInNameOnCardField() throws Exception {
		String placeHolder = BrowserActions.getText(driver, txtInvalidNamePlaceholder, "Name placeholder");
		if (placeHolder.trim().equalsIgnoreCase("Please enter the valid card holder name"))
			return true;
		else
			return false;
	}

	/**
	 * To verify the invalid entry in card number field
	 * 
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyInvalidEntryInCardNumberField(String userInput) throws Exception {
		String expectedInput = "" + StringUtils.getNumberInString(userInput);
		return (getCardNumberLength() == expectedInput.length());
	}

	/**
	 * To type the card name
	 * 
	 * @param name
	 *            -
	 * @throws Exception
	 *             - Exception
	 */
	public void typeName(String name) throws Exception {
		BrowserActions.typeOnTextField(txtNameOnCard, name, driver, "Name on card");
	}

	/**
	 * To type the card number
	 * 
	 * @param card
	 *            -
	 * @throws Exception
	 *             - Exception
	 */
	public void typeCardNumber(String card) throws Exception {
		BrowserActions.typeOnTextField(txtCardNumber, card, driver, "Card Number");
	}

	/**
	 * To get credit card number length
	 * 
	 * @return Length as Integer
	 * @throws Exception
	 *             - Exception
	 */
	public int getCardNumberLength() throws Exception {
		return BrowserActions.getText(driver, txtCardNumber, "Card Number").trim().length();
	}

	/**
	 * To Verify month dropdown values
	 * 
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyExpiryMonthDropdownValues() throws Exception {
		List<String> actual = new ArrayList<String>();
		for (WebElement option : drpExpiryMonth.findElements(By.cssSelector("option"))) {
			actual.add(option.getText().trim());
		}
		if (actual.equals(Arrays.asList("Expiry Month", "January", "February", "March", "April", "May", "June", "July",
				"August", "September", "October", "November", "December")))
			return true;
		else
			return false;
	}

	/**
	 * To verify expiry year drop down values
	 * 
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyExpiryYearDropdownValues() throws Exception {
		int startYear = Calendar.getInstance().get(Calendar.YEAR);
		int endYear = startYear + 11;
		WebElement lastOption = drpExpiryYear.findElement(By.cssSelector("option:nth-of-type(13)"));
		int actualEndYear = Integer.parseInt(lastOption.getText().trim());
		if (actualEndYear == endYear)
			return true;
		else
			return false;
	}

	/**
	 * To verify make default payment is checked
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public Boolean isMakeDefaultPaymentChecked() throws Exception {
		if (chkMakeDefaultPayment.isSelected())
			return true;
		else
			return false;
	}

	/**
	 * To click out side of the credit card dropdown list
	 * 
	 * @throws Exception
	 * 
	 */
	public void clickOutSideOfCreditCardListDropdown() throws Exception {
		if (Utils.waitForElement(driver, creditCardDropdown)) {
			BrowserActions.clickOnEmptySpaceVertical(driver, creditCardDropdown);
			Utils.waitUntilElementDisappear(driver, creditCardDropdown);
		}
	}

	/**
	 * To verify breadcrum value
	 * 
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyBreadCrumb() throws Exception {
		List<String> actualValue = new ArrayList<String>();
		for (WebElement breadcrumb : lstBreadCrumbElementsDesktop) {
			actualValue.add(breadcrumb.getText().trim().toUpperCase());
		}

		if (actualValue.equals(Arrays.asList("HOME", "MY ACCOUNT", "PAYMENT METHODS")))
			return true;
		else
			return false;
	}

	/**
	 * To click the breadcrum
	 * 
	 * @param crumb
	 *            -
	 * @return Myaccount or homepage object
	 * @throws Exception
	 *             - Exception
	 */
	public Object clickBreadCrumb(String crumb) throws Exception {
		if (crumb.equalsIgnoreCase("home")) {
			BrowserActions.clickOnElementX(lstBreadCrumbElementsDesktop.get(0), driver, "Home in breadcrumb");
			return new HomePage(driver);
		} else {
			BrowserActions.clickOnElementX(lstBreadCrumbElementsDesktop.get(1), driver, "My Account in breadcrumb");
			return new MyAccountPage(driver);
		}
	}

	/**
	 * To get mobile breadcrum value
	 * 
	 * @return String
	 * @throws Exception
	 *             - Exception
	 */
	public String getMobileBreadCrumb() throws Exception {
		return BrowserActions.getText(driver, txtBreadCrumbElementMobile, "Mobile Breadcrumb").trim();
	}

	/**
	 * To click backarrow in breadcrum
	 * 
	 * @return Myaccount page object
	 * @throws Exception
	 *             - Exception
	 */
	public MyAccountPage clickBackArrowInBreadcrumb() throws Exception {
		BrowserActions.clickOnElementX(imgBreadCrumbBackArrowMobile, driver, "Back Arrow");
		return new MyAccountPage(driver);
	}

	/**
	 * To click new credit card link
	 * 
	 * @throws Exception
	 *             - Exception
	 */
	public void clickAddNewCardLink() throws Exception {
		BrowserActions.clickOnElementX(lnkAddNewCard, driver, "Add New Card");
		Utils.waitForElement(driver, drpSelectCardType);
	}

	/**
	 * To cancel adding new credit card
	 * 
	 * @throws Exception
	 *             - Exception
	 */
	public void clickCancelAddNewCard() throws Exception {
		BrowserActions.clickOnElementX(lnkCancelAddCard, driver, "Cancel Add Card");
	}

	/**
	 * To fill credit crad details
	 * 
	 * @param cardDetails
	 *            -
	 * @throws Exception
	 *             - Exception
	 */
	public void fillCardDetails(HashMap<String, String> cardDetails) throws Exception {
		if (cardDetails.get("IsPLCC").equalsIgnoreCase("Yes")) {
			BrowserActions.selectDropdownByValue(drpSelectCardType, cardDetails.get("CardType"));
			BrowserActions.typeOnTextField(txtCardNumber, cardDetails.get("Number"), driver, "Card Number");
		} else {
			BrowserActions.selectDropdownByValue(drpSelectCardType, cardDetails.get("CardType"));
			BrowserActions.typeOnTextField(txtNameOnCard, cardDetails.get("Name"), driver, "Name on card");
			BrowserActions.typeOnTextField(txtCardNumber, cardDetails.get("Number"), driver, "Card Number");
			BrowserActions.selectDropdownByValue(drpExpiryMonth, cardDetails.get("ExpMonth"));
			BrowserActions.selectDropdownByValue(drpExpiryYear, cardDetails.get("ExpYear"));
		}

		if (cardDetails.get("MakeDefaultPayment").equalsIgnoreCase("Yes")) {
			if (isMakeDefaultPaymentChecked() == false)
				BrowserActions.clickOnElementX(chkMakeDefaultPayment, driver, "Make Default Payment");
		} else {
			if (isMakeDefaultPaymentChecked() == true)
				BrowserActions.clickOnElementX(chkMakeDefaultPayment, driver, "Make Default Payment");
		}
	}

	/**
	 * To fill credit crad details using property
	 * 
	 * @param cardDetails
	 *            -
	 * @throws Exception
	 *             - Exception
	 */
	public void fillCardDetailsUsingProperty(String cardProperty, boolean IsPLCC, boolean makeItDefault)
			throws Exception {
		String cardDetails = checkoutProperty.getProperty(cardProperty);

		if (!Utils.waitForElement(driver, txtNameOnCard)) {
			clickAddNewCardLink();
		}

		BrowserActions.selectDropdownByValue(drpSelectCardType, cardDetails.split("\\|")[0]);

		if (IsPLCC) {
			BrowserActions.typeOnTextField(txtCardNumber, cardDetails.split("\\|")[1], driver, "Card Number");
		} else {
			BrowserActions.typeOnTextField(txtNameOnCard, cardDetails.split("\\|")[1], driver, "Name on card");
			BrowserActions.typeOnTextField(txtCardNumber, cardDetails.split("\\|")[2], driver, "Card Number");
			BrowserActions.selectDropdownByValue(drpExpiryMonth, cardDetails.split("\\|")[3]);
			BrowserActions.selectDropdownByValue(drpExpiryYear, cardDetails.split("\\|")[4]);
		}
		checkOrUnCheckMakeDefaultInCardsSection(makeItDefault);
	}

	/**
	 * To verify expire month drop down is displayed
	 * 
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyExpiryMonthDropdownDisplayed() throws Exception {
		if (driver.findElement(By.cssSelector(".month .custom-select")).isDisplayed())
			return true;
		else
			return false;
	}

	/**
	 * To Verify Expire year drop down is dsiplayed
	 * 
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyExpiryYearDropdownDisplayed() throws Exception {
		if (driver.findElement(By.cssSelector(".year .custom-select")).isDisplayed())
			return true;
		else
			return false;
	}

	/**
	 * To get default card type.
	 * 
	 * @return Card type as String
	 * @throws Exception
	 *             - Exception
	 */
	public String getDefaultCardType() throws Exception {
		for (WebElement savedCard : lstSavedCards) {
			if (savedCard.findElement(By.cssSelector("div > div > span[class^='is-default']")).getAttribute("class")
					.contains("yes"))
				return savedCard.findElement(By.cssSelector("div > div > span[class*='img']")).getAttribute("class")
						.trim();
		}
		return null;
	}

	/**
	 * To get default card number.
	 * 
	 * @return String - Card number
	 * @throws Exception
	 *             - Exception
	 */
	public String getDefaultCardNumber() throws Exception {
		for (WebElement savedCard : lstSavedCards) {
			if (savedCard.findElement(By.cssSelector("div > div > span[class^='is-default']")).getAttribute("class")
					.contains("yes")) {
				String cardNumber = savedCard.findElement(By.cssSelector(".cardnumber")).getAttribute("innerHTML")
						.trim().replaceAll("[^0-9]", "");
				Log.event("Default card number ends with:: " + cardNumber);
				return cardNumber;
			}
		}
		return null;
	}

	/**
	 * To get card stype
	 * 
	 * @param index
	 *            -
	 * @return Card type as String
	 * @throws Exception
	 *             - Exception
	 */
	public String getCardType(int index) throws Exception {
		WebElement card = lstSavedCards.get(index);
		return card.findElement(By.cssSelector("div > div > span[class*='img']")).getAttribute("class").trim();
	}

	/**
	 * To verify the brand plcc message
	 * 
	 * @param cardType
	 *            -
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyBrandInPLCCMessage(String cardType) throws Exception {
		if (cardType.contains("rm")) {
			if (txtPLCCMessage.getText().trim().contains("Roaman's"))
				return true;
			else
				return false;
		} else if (cardType.contains("jl")) {
			if (txtPLCCMessage.getText().trim().contains("Jessica"))
				return true;
			else
				return false;
		} else {
			if (txtPLCCMessage.getText().trim().contains("Woman"))
				return true;
			else
				return false;
		}
	}

	/**
	 * To verify delete button is not available in plcc
	 * 
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyDeleteNotAvailableForPLCC() throws Exception {
		List<WebElement> plccCards = driver.findElements(By.cssSelector(".payment-list>div[class='cardinfo']"));
		for (WebElement cards : plccCards) {
			if (cards.getAttribute("innerHTML").contains("button-text delete close-on-mobile"))
				return false;
		}
		return true;
	}

	/**
	 * To make card type as default.
	 * 
	 * @param cardType
	 *            -
	 * @throws Exception
	 *             - Exception
	 */
	public void makeCardTypeAsDefault(String cardType) throws Exception {
		for (WebElement savedCard : lstSavedCards) {
			if (savedCard.findElement(By.cssSelector("div > div > span[class='cardname']")).getText().trim()
					.equalsIgnoreCase(cardType)) {
				if (savedCard.findElements(By.cssSelector("div > div > span[class='is-default ']")).size() > 0) {
					BrowserActions.clickOnElementX(
							savedCard.findElement(By.cssSelector("div > div > span[class='is-default ']")), driver,
							"Make Default");
					BrowserActions.clickOnElementX(savedCard.findElement(By.cssSelector(".makedefault-payment")),
							driver, "Make Default checkbox");
					Utils.waitForPageLoad(driver);
					break;
				}
			}
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To verify order of save credit card
	 * 
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyOrderOfSavedCards() throws Exception {
		Boolean nonPLCC = false;
		for (int i = 1; i < lstSavedCards.size(); i++) {
			if (lstSavedCards.get(i - 1).findElement(By.cssSelector("div > div > span[class*='img']"))
					.getAttribute("class").trim().contains("plcc")) {
				if (nonPLCC == false)
					BrowserActions.verifyVerticalAllignmentOfElements(driver, lstSavedCards.get(i - 1),
							lstSavedCards.get(i));
				else
					return false;
			} else {
				nonPLCC = true;
				BrowserActions.verifyVerticalAllignmentOfElements(driver, lstSavedCards.get(i - 1),
						lstSavedCards.get(i));
			}
		}
		return true;
	}

	/**
	 * To verify position of default card in save list
	 * 
	 * @param cardType
	 *            -
	 * @return Status Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyPositionOfDefaultCardInTheSavedCardsList(String cardType) throws Exception {
		if (lstSavedCards.get(0).findElement(By.cssSelector("div > div > span[class='cardname']")).getText().trim()
				.equalsIgnoreCase(cardType)) {
			if (lstSavedCards.get(0).findElement(By.cssSelector("div > div > span[class*='img']")).getAttribute("class")
					.trim().contains("plcc")) {
				if (lstSavedCards.get(0).findElement(By.cssSelector("div > div > span[class^='is-default']"))
						.getAttribute("class").contains("yes"))
					return true;
				else
					return false;
			}
			return false;
		} else {
			WebElement ele = driver.findElements(By.cssSelector("div[class*='cardinfo ']")).get(0);
			if (ele.findElement(By.cssSelector("div > div > div > span[class^='is-default']")).getAttribute("class")
					.contains("yes"))
				return true;
			else
				return false;
		}
	}

	/**
	 * To Verify make default link is displayed for mobile
	 * 
	 * @return Boolean - true/false if default link is displayed on mobile
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyMakeDefaultLinkDisplayedForMobile() throws Exception {
		for (WebElement savedCard : lstSavedCards) {
			if (savedCard.findElement(By.cssSelector("div > div > span[class^='is-default']")).getAttribute("class")
					.contains("yes")) {
				if (savedCard.findElements(By.cssSelector(".make-default")).size() > 0)
					return false;
			}
		}
		return true;
	}

	/**
	 * To verify default payment method is not clickable
	 * 
	 * @return Boolean - true/false if default payment method is clickable
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyDefaultPaymentIconNotClickable() throws Exception {
		String defaultCard = null;
		for (WebElement savedCard : lstSavedCards) {
			if (savedCard.findElement(By.cssSelector("div > div > span[class^='is-default']")).getAttribute("class")
					.contains("yes")) {
				defaultCard = savedCard.findElement(By.cssSelector("div > div > span[class='cardname']")).getText()
						.trim();
				BrowserActions.clickOnElementX(savedCard.findElement(By.cssSelector("div > div > span.is-default.yes")),
						driver, "Green indicator for default payment");
				break;
			}
		}

		// Verify the default card is still the same
		if (driver.findElement(By.cssSelector(
				"div[class='cardinfo " + defaultCard + "'] > div.carddetails > div > div > span.is-default.yes"))
				.isDisplayed())
			return true;
		else
			return false;
	}

	/**
	 * To verify payment method link is not clickable
	 * 
	 * @return String Boolean
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyPaymentMethodsLinkNotClickable() throws Exception {
		if (lnkPaymentMethods.getAttribute("style").contains("cursor: text"))
			return true;
		else
			return false;
	}

	/**
	 * To add new credit card
	 * 
	 * @param String
	 *            - cardName
	 * @throws Exception
	 *             - Exception
	 */
	public void addNewCard(String cardName, boolean... makeDefault) throws Exception {
		String cardInfo[] = checkoutProperty.getProperty(cardName).split("\\|");

		String cardType = cardInfo[0];
		String nameOnCard = cardInfo[1];
		String cardNo = cardInfo[2];
		String expMonth = cardInfo[3];
		String expYear = cardInfo[4];

		if (!Utils.waitForElement(driver, txtNameOnCard))
			clickAddNewCardLink();
		Utils.waitForElement(driver, txtNameOnCard);
		BrowserActions.scrollToViewElement(drpSelectCardType, driver);
		BrowserActions.selectDropdownByValue(drpSelectCardType, cardType);
		BrowserActions.typeOnTextField(txtNameOnCard, nameOnCard, driver, "Card Name");
		BrowserActions.typeOnTextField(txtCardNumber, cardNo, driver, "Card Number");
		BrowserActions.scrollToViewElement(drpExpiryMonth, driver);
		BrowserActions.selectDropdownByValue(drpExpiryMonth, expMonth);
		BrowserActions.scrollToViewElement(drpExpiryYear, driver);
		BrowserActions.selectDropdownByValue(drpExpiryYear, expYear);
		if (makeDefault.length > 0) {
			checkOrUnCheckMakeDefaultInCardsSection(makeDefault[0]);
		}
		BrowserActions.clickOnElementX(btnSave, driver, "Save Button");
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver, paymentList);
	}

	/**
	 * To get number of saved cards count
	 * 
	 * @return Integer - Number of saved cards
	 * @throws Exception
	 *             - Exception
	 */
	public int getNumberOfSavedCards() throws Exception {
		if (Utils.waitForElement(driver, paymentList)) {
			Log.event("Number of saved cards:: " + savedCardsList.size());
			return savedCardsList.size();
		}
		else
			return 0;
	}
	
	/**
	 * To get number of PLCC cards in account
	 * @throws Exception
	 */
	public int getNumberOfPlatinumCards() throws Exception {
		if (Utils.waitForElement(driver, paymentList)) {
			Log.event("Number of saved PLCC:: " + savedCardPLCC.size());
			return savedCardPLCC.size();
		}
		else
			return 0;
	}

	/**
	 * To compare payment method after ordering with entered payment method
	 * 
	 * @param payment
	 *            - payment property
	 * @return boolean - boolean
	 * @throws Exception
	 *             - Exception
	 */
	public boolean comparePaymentAfterOrderWithEnteredPaymentMethod(String payment) throws Exception {
		String paymentInfos = checkoutProperty.getProperty(payment);

		String cardNo = BrowserActions.getText(driver, lblEnteredCardNo, "Card No").trim();
		String cardOwnerName = BrowserActions.getText(driver, lblEnteredCardOwnerName, "Card Owner Name");

		String[] paymentInfo = paymentInfos.split("\\|");

		if (!(paymentInfo[1].toLowerCase().trim().contains(cardOwnerName.toLowerCase().trim()))) {
			return false;
		}

		if (!(cardNo.trim().contains(paymentInfo[2].trim().substring(13)))) {
			return false;
		}

		return true;
	}

	/**
	 * To check/uncheck made default Checkbox
	 * 
	 * @param boolean
	 *            - true/false if Checkbox should be checked
	 * @throws Exception
	 *             - Exception
	 */
	public void checkOrUnCheckMakeDefaultInCardsSection(boolean makeDefault) throws Exception {
		if (makeDefault) {
			if (!(chkMakeDefaultPayment.isSelected())) {
				BrowserActions.clickOnElement(chkMakeDefaultPayment, driver, "Make Default Payment");
			}
		} else {
			if (chkMakeDefaultPayment.isSelected()) {
				BrowserActions.clickOnElement(chkMakeDefaultPayment, driver, "Make Default Payment");
			}
		}
	}

	/**
	 * To get default card type.
	 * 
	 * @return String - Card type as String
	 * @throws Exception
	 *             - Exception
	 */
	public boolean verifyDefaultCardGreenIndicator() throws Exception {
		if (Utils.waitForElement(driver, btnShowAll))
			BrowserActions.clickOnElementX(btnShowAll, driver, "Show all button");
		for (WebElement savedCard : lstSavedCards) {
			if (savedCard.findElement(By.cssSelector("div > div > span[class^='is-default']")).getAttribute("class")
					.contains("yes"))
				return (savedCard.findElement(By.cssSelector("div > div > span[class*='img']"))
						.getLocation().x > savedCard
								.findElement(By.cssSelector("div > div > span[class^='is-default']")).getLocation().x);
		}
		return false;
	}

	/**
	 * To delete all saved cards
	 * 
	 * @throws Exception
	 *             - Exception
	 */
	public void deleteAllSavedCards() throws Exception {
		int noCardsSaved = lstDeleteButtons.size();
		try {
			for (int i = 0; i <= noCardsSaved; i++) {
				BrowserActions.clickOnElementX(driver.findElement(By.cssSelector("button.delete")), driver, "DELETE");
				Utils.waitForElement(driver, btnConfirmDelete);
				BrowserActions.clickOnElementX(btnConfirmDelete, driver, "Confirm Delete card");
				Utils.waitUntilElementDisappear(driver, deleteOverlayBox);
				Utils.waitUntilElementDisappear(driver, btnConfirmDelete);
				Utils.waitForPageLoad(driver);			
			}
		} catch(NoSuchElementException ex) {
			if(getNumberOfSavedCards() == 0) {
				Log.event("Removed all cards.");
			} else if (getNumberOfSavedCards() == getNumberOfPlatinumCards()) {
				Log.message("All cards except PLCC removed. PLCC cards not user removable.", driver);
			} else {
				Log.failsoft("Unable to remove cards", driver);
			}
		}
	}

	/**
	 * To select month on add new card module
	 * 
	 * @param String
	 *            - Month
	 * @throws Exception
	 *             - Exception
	 */
	public void selectMonth(String expMonth) throws Exception {
		BrowserActions.scrollToViewElement(drpExpiryMonth, driver);
		BrowserActions.selectDropdownByValue(drpExpiryMonth, expMonth);

	}

	/**
	 * To select year on add new card module
	 * 
	 * @param String
	 *            - Year
	 * @Exception - Exception
	 */
	public void selectYear(String expYear) throws Exception {
		BrowserActions.scrollToViewElement(drpExpiryYear, driver);
		BrowserActions.selectDropdownByValue(drpExpiryYear, expYear);
	}

	/**
	 * To get selected card on add new card module
	 * 
	 * @return String - Card type
	 * @Exception - Exception
	 */
	public String getSelectedCard() throws Exception {
		return BrowserActions.getText(driver, selectedCard, "Selected card");
	}

	/**
	 * To get name entered on add new card module
	 * 
	 * @return String - Card owner name
	 * @Exception - Exception
	 */
	public String getNameText() throws Exception {
		return BrowserActions.getText(driver, txtNameOnCard, "Name typed");
	}

	/**
	 * To get card number entered on add new card module
	 * 
	 * @return String - Card number
	 * @Exception - Exception
	 */
	public String getCardNumber() throws Exception {
		return BrowserActions.getText(driver, txtCardNumber, "Card number typed");
	}

	/**
	 * To get month selected on add new card module
	 * 
	 * @return String - Month selected
	 * @Exception - Exception
	 */
	public String getSelectedMonth() throws Exception {
		return BrowserActions.getText(driver, selectedMonth, "month selected");
	}

	/**
	 * To get year selected on add new card module
	 * 
	 * @return String - Year selected
	 * @Exception - Exception
	 */
	public String getSelectedYear() throws Exception {
		return BrowserActions.getText(driver, selectedYear, "Year selected.");
	}

	/**
	 * To verify position of default card
	 * 
	 * @return Boolean - true/false if default card is placed correctly
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyPositionOfDefaultCard() throws Exception {
		WebElement defaultCard = driver.findElement(By.cssSelector(".cardtype > .is-default.yes"))
				.findElement(By.xpath("../../../.."));
		Log.message(defaultCard.getAttribute("class"));
		if (savedCardPLCC.size() != 0) {
			if (!BrowserActions.verifyVerticalAllignmentOfElements(driver, savedCardPLCC.get(savedCardPLCC.size() - 1),
					defaultCard)) {
				return false;
			}
		}

		List<WebElement> lstNonPLCC = lstSavedCards;
		lstNonPLCC.removeAll(savedCardPLCC);
		if (savedCardPLCC.size() != 0) {
			if (!BrowserActions.verifyVerticalAllignmentOfElements(driver, defaultCard, lstNonPLCC.get(0))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * To verify months in dropdown are in correct order
	 * 
	 * @return Boolean - true/false if months are in correct order
	 * @throws -
	 *             Exception - Exception
	 */
	public Boolean verifyMonthList() throws Exception {
		if (lstMonthsAvailable.size() == 12) {
			for (int i = 0; i < 12; i++) {
				if (!lstMonthsAvailable.get(i).getAttribute("value").trim().equals(i + 1 + "")) {
					return false;
				}
			}
		} else {
			Log.failsoft("12 months are not present.");
			return false;
		}
		return true;
	}

	/**
	 * To verify years are shown as spec
	 * 
	 * @return Boolean - true/false if years are shown as current year+11 years top
	 *         down.
	 * @throws Exception
	 *             - Exception
	 */
	public Boolean verifyYearList() throws Exception {
		int currentYear = StringUtils.getCurrentDateDetails("year");
		if (lstYearsAvailable.size() == 12) {
			for (int i = 0; i < 12; i++) {
				if (!lstYearsAvailable.get(i).getAttribute("value").trim().equals(currentYear + i + "")) {
					return false;
				}
			}
		} else {
			Log.failsoft("Total 12 years are not shown.");
			return false;
		}
		return true;
	}

	/**
	 * To click on delete button on index numbered saved card
	 * 
	 * @param int
	 *            - index of card
	 * @throws Exception
	 *             - Exception
	 */
	public String clickDeleteOnSavedCard(int index) throws Exception {
		WebElement delButton = lstSavedCardDelete.get(index - 1);
		WebElement cardNumber = delButton.findElement(By.xpath("../../../.."))
				.findElement(By.cssSelector(".cardnumber"));
		BrowserActions.clickOnElementX(delButton, driver, "DELETE button on card #" + index);
		Utils.waitForElement(driver, ovrlayDeleteCard);
		return cardNumber.getText();
	}

	/**
	 * To cancel deleting a card
	 * 
	 * @throws Exception
	 *             - Exception
	 */
	public void clickCancelOnDeleteConfirmation() throws Exception {
		BrowserActions.clickOnElementX(btnCancelDelete, driver, "Cancel button.");
	}

	/**
	 * To close delete cnfirmation
	 * 
	 * @throws Exception
	 *             - Exception
	 */
	public void clickCloseConfirmationOverlay() throws Exception {
		BrowserActions.clickOnElementX(btnOverlayClose, driver, "Close button.");
	}

	/**
	 * To click out side to close delete cnfirmation
	 * 
	 * @throws Exception
	 *             - Exception
	 */
	public void clickOutSideOfDeleteOverlay() throws Exception {
		Utils.waitForElement(driver, deleteOverlayBox);
		BrowserActions.clickOnEmptySpaceVertical(driver, deleteOverlayBox);
		Utils.waitUntilElementDisappear(driver, deleteOverlayBox);
	}

	/**
	 * To click on DELETE on Delete Confirmation
	 * 
	 * @throws Exception
	 *             - Exception
	 */
	public void clickDeleteOnDeleteConfirmation() throws Exception {
		BrowserActions.clickOnElementX(btnConfirmDelete, driver, "DELETE button.");
		Utils.waitForPageLoad(driver);
		Utils.waitUntilElementDisappear(driver, btnConfirmDelete);
	}

	/**
	 * To verify details of saved PLCC cards
	 * 
	 * @return boolean - true/false if details have been verified
	 * @throws Exception
	 *             - Exception
	 */
	public boolean verifyDetailsSavedPLCC() throws Exception {
		for (WebElement savedPLCC : savedCardPLCC) {
			WebElement cardName = savedPLCC.findElement(By.cssSelector(".cardname"));
			WebElement cardNameOnCard = savedPLCC.findElement(By.cssSelector(".cardholder"));
			WebElement cardAvailableCredit = savedPLCC.findElement(By.cssSelector(".cardnumber-along-text"));
			WebElement cardRewardPoints = savedPLCC.findElement(By.cssSelector(".rewardpoints-along-text"));
			if (!Utils.isMobile()) {
				WebElement cardToolTip = savedPLCC.findElement(By.cssSelector(".plcc-tooltip.hide-mobile"));
				if (!BrowserActions.verifyHorizontalAllignmentOfElements(driver, cardNameOnCard, cardName)
						|| !BrowserActions.verifyHorizontalAllignmentOfElements(driver, cardAvailableCredit,
								cardNameOnCard)
						|| !BrowserActions.verifyHorizontalAllignmentOfElements(driver, cardRewardPoints,
								cardAvailableCredit)
						|| !BrowserActions.verifyHorizontalAllignmentOfElements(driver, cardToolTip, cardRewardPoints))
					return false;
			} else {
				WebElement cardToolTip = savedPLCC.findElement(By.cssSelector(".plcc-tooltip.hide-desktop"));
				if (BrowserActions.verifyVerticalAllignmentOfElements(driver, cardNameOnCard, cardName)
						|| BrowserActions.verifyVerticalAllignmentOfElements(driver, cardAvailableCredit,
								cardNameOnCard)
						|| BrowserActions.verifyVerticalAllignmentOfElements(driver, cardRewardPoints,
								cardAvailableCredit)
						|| BrowserActions.verifyVerticalAllignmentOfElements(driver, cardNameOnCard, cardToolTip)
						|| BrowserActions.verifyHorizontalAllignmentOfElements(driver, cardName, cardToolTip))
					return false;
			}
		}
		return true;
	}

	/**
	 * To verify saved card logo position
	 * 
	 * @return boolean - true/false if log are positioned correctly
	 * @throws Exception
	 *             - Exception
	 */
	public boolean verifySavedCardLogo() throws Exception {
		for (WebElement savedCard : lstSavedCards) {
			WebElement cardLogo = savedCard.findElement(By.cssSelector(".img"));
			WebElement cardName = savedCard.findElement(By.cssSelector(".cardname"));
			if (!BrowserActions.verifyHorizontalAllignmentOfElements(driver, cardName, cardLogo)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * To verify card expiration date text color
	 * 
	 * @return boolean - true/false if date is shown in corrrect clor
	 * @throws Exception
	 *             - Exception
	 */
	public boolean verifyExpirationDateColor() throws Exception {
		String expiredDateColor = demandwareData.get("card_date_color_expired");
		String validDateColor = demandwareData.get("card_date_color_valid");
		for (WebElement cardDate : cardExpDates) {
			String date = cardDate.getText().split("\\s")[1];
			if ((StringUtils.getNumberInString(date) + "").length() != 0) {
				int expMonth = StringUtils.getNumberInString(date.split("/")[0]);
				int expYear = StringUtils.getNumberInString(date.split("/")[1]);
				int currentMonth = StringUtils.getCurrentDateDetails("month");
				int currentYear = StringUtils.getCurrentDateDetails("year") % 100;
				if (currentYear < expYear || (currentYear == expYear && currentMonth < expMonth - 1)) {
					if (!cardDate.getCssValue("color").equals(validDateColor))
						return false;
				}
				if (currentYear > expYear || (currentYear == expYear && currentMonth >= expMonth - 1)) {
					if (!cardDate.getCssValue("color").equals(expiredDateColor))
						return false;
				}
			}
		}
		return true;
	}

	/**
	 * To verify PLCC cards are above other saved cards
	 * 
	 * @return boolean - true/false if saved PLCC is shown correctly
	 * @throws Exception
	 *             - Exception
	 */
	public boolean verifySavedPLCCLocation() throws Exception {
		WebElement lastPLCC = null, firstNonPLCC = null;
		if (savedCardsList.size() != 0) {
			for (int i = 0; i < savedCardsList.size(); i++) {
				if (savedCardsList.get(i).getAttribute("class").length() > "cardinfo".length()) {
					lastPLCC = savedCardsList.get(i);
					if (i < savedCardsList.size() - 1) {
						firstNonPLCC = savedCardsList.get(i + 1);
					} else {
						Log.event("No non-PLCC card is saved to verify.");
						return true;
					}
				}
			}
			return BrowserActions.verifyVerticalAllignmentOfElements(driver, lastPLCC, firstNonPLCC);
		} else {
			Log.event("No PLCC card is saved to verify.");
			return true;
		}
	}

	/**
	 * To display saved card list fully
	 * 
	 * @throws Exception
	 *             - Exception
	 */
	public void expandSavedCardList() throws Exception {
		if (Utils.waitForElement(driver, btnShowAll)) {
			BrowserActions.clickOnElementX(btnShowAll, driver, "Show All");
			Log.event("Clicked on Show All button");
		}
	}

	/*
	 * To verify the expired card with selected as default available "Make it
	 * default" if expired card is not as default card
	 * 
	 * @throws Exception
	 *             - Exception
	 */
	public void verifyExpiryDefaultCard() throws Exception {
		try {
		if (Utils.waitForElement(driver, expireCard)) {
			BrowserActions.clickOnElementX(expireCard, driver, "Expire Card");
			checkOrUnCheckMakeDefaultInCardsSection(true);
		}else {
			Log.event("No non-PLCC expired card is saved to verify.");
			}
		} catch (NoSuchElementException e) {
			Log.event("No non-PLCC expired card is saved to verify.");
			}
	}
}
