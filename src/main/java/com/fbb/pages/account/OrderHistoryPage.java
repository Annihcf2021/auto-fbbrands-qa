package com.fbb.pages.account;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.ReturnOrderPage;
import com.fbb.pages.customerservice.ReturnAndExchangePage;
import com.fbb.reusablecomponents.Enumerations.Toggle;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class OrderHistoryPage extends LoadableComponent <OrderHistoryPage>{
	
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	/**********************************************************************************************
	 ********************************* WebElements of OrderHistoryPage ***********************************
	 **********************************************************************************************/
	
	private static final String MyAccountPageLinksNavigation = ".navigation-links-row";

	@FindBy(css = ".pt_order")
	WebElement readyElement;
	
	@FindBy(css = "div[class*='ordertrack-form-content']")
	WebElement divInternationalSection;
	
	@FindBy(css = ".order-history .content-wrap h1")
	WebElement lblOrderHistory;
	
	@FindBy(css = MyAccountPageLinksNavigation + " .order-history a[style*='cursor']")
	WebElement lnkOrderHistoryAfterNavigation;

	@FindBy(css = ".secondary-navigation-links .order-history a")
	WebElement lnkOrderHistory;
	
	@FindBy(css = ".order-history h1.account-subpage-heading")
	WebElement lblHistoryHeading;
	
	@FindBy(css = ".order-history h2")
	WebElement lblHistorySubheading;
	
	@FindBy(css = ".order-history-table")
	WebElement divOrderHistoryTable;
	
	private static final String OrderHistoryHeader = ".order-history-table .order-history-header ";
	
	@FindBy(css = OrderHistoryHeader)
	WebElement divOrderHistoryHeader;
	
	@FindBy(css = "#dwfrm_orders")
	WebElement divOrderHistory;
	
	@FindBy(css = OrderHistoryHeader + ".col.col-1")
	WebElement txtHeaderOrderNumber;
	
	@FindBy(css = OrderHistoryHeader + ".col-brand")
	WebElement txtHeaderOrderBrand;
	
	@FindBy(css = OrderHistoryHeader + ".col.col-2")
	WebElement txtHeaderOrderDate;
	
	@FindBy(css = OrderHistoryHeader + ".col.col-3")
	WebElement txtHeaderOrderOrderTotal;
	
	@FindBy(css = OrderHistoryHeader + ".col.col-4")
	WebElement txtHeaderOrderOrderStatus;
	
	@FindBy(css = OrderHistoryHeader + ".col.col-5")
	WebElement txtHeaderOrderOrderAction;
	
	@FindBy(css = ".order-number")
	WebElement orderNumber;
	
	@FindBy(css = ".internation-shipment-method")
	WebElement internationalOrderPageBanner;
	
	@FindBy(css = ".shipping-method a")
	WebElement internationalBannerLink;
	
	@FindBy(css = ".order-list.with-returns .order-status")
	WebElement returnOrderStatus;
	
	@FindBy(css = ".order-list.with-returns .order-history-overlay")
	WebElement returnOrderStatusToolTip;
	
	@FindBy(css = "#dialog-container")
	WebElement returnOrderStatusToolTipPopUp;
	
	@FindBy(css = ".ui-button-icon.ui-icon.ui-icon-closethick")
	WebElement closRreturnOrderStatusToolTip;
	
	@FindBy(css = ".order-list")
	List<WebElement> divOrderListEntries;
	
	@FindBy(css = ".order-list.with-returns")
	List<WebElement> divOrderWithReturnsListEntries;
	
	@FindBy(css = ".order-list .order-number .value")
	List<WebElement> divOrderNumbers;
	
	@FindBy(css = ".order-list .order-date .value")
	List<WebElement> divOrderEntryDates;
	
	@FindBy(css = ".order-list .order-price")
	List<WebElement> divOrderEntryTotals;
	
	@FindBy(css = ".order-list-wrapper.search-result-items li:nth-child(1) .order-history-data .col.col-1 .value")
	WebElement txtOrderNumber;
	
	@FindBy(css = ".order-list-wrapper.search-result-items li:nth-child(1) .order-history-data .col.col-2 .value")
	WebElement txtOrderDate;
	
	@FindBy(css = ".order-list-wrapper.search-result-items li:nth-child(1) .order-history-data .col.col-3 .value")
	WebElement txtOrderTotal;
	
	@FindBy(css = ".order-list-wrapper.search-result-items li:nth-child(1) .order-history-data .col.col-4 .value")
	WebElement txtOrderStatus;
	
	@FindBy(css = ".button-text.view-detials")
	WebElement txtOrderViewDetails;
	
	@FindBy(css = ".order-list-wrapper.search-result-items")
	WebElement orderHistoryTable;
	
	@FindBy(css = "#primary .order-details-section")
	WebElement orderDetailsPage;
	
	@FindBy(css = ".trackingnumber.hide-mobile")
	WebElement lnkTrackMyPackage;
	
	@FindBy(css = ".trackingnumber.hide-desktop")
	WebElement lnkTrackMyPackageMobile;
	
	@FindBy(css = ".order-history .content-asset")
	WebElement orderDetailsContentAsset;
	
	@FindBy(css = ".order-history .content-asset .question-title")
	WebElement orderQuestionTitle;
	
	@FindBy(css = ".order-history .content-asset .question")
	List <WebElement> orderQuestionCaratSymbol;
	
	@FindBy(css = ".qa-content")
	List<WebElement> questionsSection;
	
	@FindBy(css = ".question")
	WebElement questionsExpandIcon;
	
	@FindBy(css = "a[title='Go to Home']")
	WebElement breadCrumbHome;
	
	@FindBy(css = "a[title='Go to My Account']")
	WebElement breadCrumbMyAccount;
	
	@FindBy(xpath = "//div[@class='breadcrumb']/a[contains(@class, 'breadcrumb-element current-element')]")
	WebElement breadCrumbOrderHistory;
	
	@FindBy(css = ".order-history .content-wrap .top-content")
	WebElement lblOrderHistoryTopContent;
	
	@FindBy(css = ".button-text.return")
	WebElement txtReturnItem;
	
	@FindBy(css = ".orderdetails .actions .continue")
	WebElement txtReturnItem1;
	
	@FindBy(css = ".order-list-wrapper.search-result-items .value.processing.order_processed")
	WebElement txtProcessStatus;
	
	@FindBy(css = ".breadcrumb-element.hide-mobile")
	List<WebElement> lstbreadcrumbs;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement lstbreadcrumb;
	
	@FindBy(css = ".order-list.opened")
	List<WebElement> ordersummaryopened;
	
	@FindBy(css = "div[class='col col-1 col-first order-number']")
	List<WebElement> listOrdernumber;
	
	@FindBy(css = "div[class='col col-2 order-date']")
	List<WebElement> listOrderDate;
	
	@FindBy(css = "div[class='col col-3 order-price']")
	List<WebElement> listOrderTotal;
	
	@FindBy(css = ".order-list")
	List<WebElement> orderlist;
	
	@FindBy(css = ".order-list.opened .button-text.view-detials")
	WebElement viewdetails;
	
	@FindBy(css = ".order-list.opened .arr-img.only-for-mobile")
	WebElement closearrow;
	
	@FindBy(css = ".arr-img.only-for-mobile")
	List<WebElement> opencloseOrderSummary;
	
	@FindBy(css = ".hide-desktop.hide-tablet.back-arrow")
	WebElement breadcrumBackArrow;
	
	@FindBy(css = ".order-number .value")
	List<WebElement> orderNumbers;
	
	@FindBy(css = ".order-number .value")
	WebElement orderNumberValue;
	
	@FindBy(css = ".order-number")
	WebElement divOrderNumberPrint;
	
	private static final String SHIPMENT = ".order-shipment-table ";
	
	@FindBy(css = SHIPMENT)
	WebElement divShipment;
	
	@FindBy(css = SHIPMENT + ".shipment-address .heading")
	WebElement divShipmentHeading;
	
	//Gift Card
	private static final String GIFTCARD = ".gift-card";
	
	@FindBy(css = GIFTCARD)
	WebElement divGiftCard;
	
	@FindBy(css = GIFTCARD + " .shipmentnumber")
	WebElement gcShipmentNumber;
	
	@FindBy(css = GIFTCARD + " .method .heading")
	WebElement gcShimpmentHeading;
	
	@FindBy(css = GIFTCARD + " .method .value")
	WebElement gcShipmentMethod;
	
	@FindBy(css = GIFTCARD + " .address")
	WebElement gcShipmentAddress;
	
	@FindBy(css = GIFTCARD + " .address .default")
	WebElement gcShipmentName;
	
	@FindBy(css = GIFTCARD + " .address div:nth-child(2)")
	WebElement gcShipmentAddress1;
	
	@FindBy(css = GIFTCARD + " .address div:nth-child(3)")
	WebElement gcShipmentAddress2;
	
	@FindBy(css = GIFTCARD + " .shipping-status .value")
	WebElement gcShipmentStatus;
	
	@FindBy(css = GIFTCARD + " .name")
	WebElement gcName;
	
	@FindBy(css = GIFTCARD + " .item-image img")
	WebElement gcItemImage;
	
	@FindBy(css = GIFTCARD + " .item-image a")
	WebElement gcItemImageLink;
	
	@FindBy(css = GIFTCARD + " .col-2 .line-item-quantity")
	WebElement gcQtyDesktop;
	
	@FindBy(css = GIFTCARD + " .hide-desktop.line-item-quantity")
	WebElement gcQtyMobile;
	
	@FindBy(css = GIFTCARD + " .line-item-price")
	WebElement gcPrice;
	
	//Electronic Gift Card
	private static final String ELECTRONICGIFTCARD = ".e-gift-card";
	
	@FindBy(css = ELECTRONICGIFTCARD)
	WebElement divElectronicGiftCard;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .shipmentnumber")
	WebElement egcShipmentNumber;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .method .heading")
	WebElement egcShimpmentHeading;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .method .value")
	WebElement egcShipmentMethod;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .shipping-status .value")
	WebElement egcShipmentStatus;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .name")
	WebElement egcName;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .item-image img")
	WebElement egcItemImage;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .item-image a")
	WebElement egcItemImageLink;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .col-2 .line-item-quantity")
	WebElement egcQtyDesktop;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .hide-desktop.line-item-quantity")
	WebElement egcQtyMobile;
	
	@FindBy(css = ELECTRONICGIFTCARD + " .line-item-price")
	WebElement egcPrice;
	
	@FindBy(css = "button[name='dwfrm_ordertrack_findorder']")
	WebElement btnSubmitINTL;
	
	@FindBy(css = "#dwfrm_ordertrack_orderNumber")
	WebElement fldOrderNumberINTL;
	
	@FindBy(css = "#dwfrm_ordertrack_orderEmail")
	WebElement fldOrderEmailINTL;
	
	@FindBy(css = "#dwfrm_ordertrack_orderNumber-error")
	WebElement errorOrderNumberINTL;
	
	@FindBy(css = "#dwfrm_ordertrack_orderEmail-error")
	WebElement errorOrderEmailINTL;
	
	@FindBy(css = ".order-shipment-details .shipping-status .value")
	WebElement statusShipmentINTL;
	
	@FindBy(css = ".mainContainer")
	WebElement divTrackOrderINTL;
	
	@FindBy(css = ".line-items .buy-again a")
	List<WebElement> lnkBuyAgain;
	
	/**********************************************************************************************
	 ********************************* WebElements of OrderHistoryPage - Ends ****************************
	 **********************************************************************************************/

	

	/**
	 * constructor of the class
	 * @param driver : Webdriver
	 */
	public OrderHistoryPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {
		Utils.waitForPageLoad(driver);
		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("OrderHistory Page did not open up. Site might be down.", driver);
		}
		
		elementLayer = new ElementLayer(driver);
	}// isLoaded
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver,readyElement );
	}
	
	public enum OrderAttribute {
		Number,
		Brand,
		Date,
		Total,
		Status;
	}
	
	/**
	 * Verify latest order date is correct
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyLatestOrderDate()throws Exception{
		if(Utils.waitForElement(driver, txtOrderDate)){
			String time1 = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
			String time2 = new SimpleDateFormat("M/d/yy").format(new Date());
			String latestOrderDate=txtOrderDate.getText();
			
			Log.event("Order Date :: " + latestOrderDate);
			Log.event("System Date :: " + time1 + " :: " + time2);
			return(latestOrderDate.contains(time1) || latestOrderDate.contains(time2));
		}
		return false;
	}
	
	/**
	 * Verify order date format for all orders
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderDateFormat()throws Exception{
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
		for(WebElement orderDate: divOrderEntryDates) {
			try {
				dateFormat.parse(orderDate.getText().replace("DATE", "").trim());
			} catch(ParseException e){
				return false;
			}
		}
		return true;
	}
	/**
	 * Verify the order dollar symbol
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderTotalDollarSymbol()throws Exception{
		for(WebElement orderTotal: divOrderEntryTotals) {
			if(!orderTotal.getText().contains("$"))
				return false;
		}
		return true;
	}
	/**
	 * Verify click on view details
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean clickOnViewDetails(String... orderNumber) throws Exception {
		if(orderNumber.length > 0) {
			toggleOrderDetailMobile(orderNumber[0], Toggle.Open);
			WebElement btnViewDetails = getOrderRowElement(orderNumber[0]).findElement(By.cssSelector(".view-detials"));
			BrowserActions.clickOnElementX(btnViewDetails, driver, "view details for " + orderNumber[0]);
		} else {
			BrowserActions.clickOnElementX(txtOrderViewDetails, driver, "View Details Link");
		}
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, orderDetailsPage)){
			return true;
		}
		return false;
	}
	
	/**
	 * Verify click on Return order tooltip
	 * @return -
	 * @throws Exception - Exception
	 */
	public void clickOnReturnStatusToolTip(String... orderNumber)throws Exception{
		if(orderNumber.length == 0) {
			BrowserActions.clickOnElementX(returnOrderStatusToolTip, driver, "Return status Tooltip");
			Utils.waitForElement(driver, returnOrderStatusToolTipPopUp);
		} else {
			WebElement tooltipButton = getOrderRowElement(orderNumber[0]).findElement(By.cssSelector(".order-history-overlay"));
			BrowserActions.clickOnElementX(tooltipButton, driver, "tooltop button for " + orderNumber[0]);
		}
	}
	
	/**
	 * Verify click on close icon in Return order tooltip
	 * @throws Exception - Exception
	 */
	public void clickOnCloseReturnStatusToolTip()throws Exception {
		if(Utils.waitForElement(driver, returnOrderStatusToolTipPopUp)) {
			BrowserActions.clickOnElementX(closRreturnOrderStatusToolTip, driver, "Close icon in Return status Tooltip");
		}
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * Verify get expand status question
	 * @param index - 
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getExpandStatusOfQuestion(int index)throws Exception{
		Utils.waitForElement(driver, questionsSection.get(index));
		BrowserActions.scrollToView(questionsSection.get(index), driver);
		return (questionsSection.get(index).getAttribute("class").contains("active"))? "expanded":"collapsed";
	}
	/**
	 * Verify Expance collapse question
	 * @param index - 
	 * @param state - 
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean expandCollapseQuestionOf(int index, String state)throws Exception{
		boolean flag = false;
			BrowserActions.scrollToView(questionsSection.get(index), driver);
		if (state.equals("expand") && getExpandStatusOfQuestion(index).equals("collapsed")) {
			Log.event("Section collapsed");
			BrowserActions.clickOnElementX(questionsSection.get(index), driver, "Nth Question Section");
			if(!getExpandStatusOfQuestion(index).equals("collapsed")){
				flag = true;
			}
		} else if(state.equals("collapse") && getExpandStatusOfQuestion(index).equals("expanded")){
			Log.event("Section expanded");
			BrowserActions.clickOnElementX(questionsSection.get(index), driver, "Nth Question Section");
			Log.event("Question Setion " + index + " " + state + "ed!!!");
			if(getExpandStatusOfQuestion(index).equals("expanded")){
				flag = true;
			}
		}
		return flag;
	}
	
	/**
	 * To click on Track My Package link
	 * @return vaid
	 * @throws Exception - Exception
	 */
	public boolean clickOnTrackMyPackage() throws Exception {
		if(Utils.isMobile()) {
			BrowserActions.clickOnElementX(lnkTrackMyPackageMobile, driver, "Track My Package mobile link ");
		} else {
			BrowserActions.clickOnElement(lnkTrackMyPackage, driver, "Track My Package link ");
		}
		
		Utils.waitForPageLoad(driver);
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs.get(tabs.size()-1));
		if(Utils.waitForElement(driver, divTrackOrderINTL)) {
			driver.switchTo().window(tabs.get(0));
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Verify the breadcrum text
	 * @param TextToVerify - 
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyBreadcrumbText(String TextToVerify)throws Exception{
		String breadCrumbText=breadCrumbHome.getText()+" / "+ breadCrumbMyAccount.getText()+" / "+breadCrumbOrderHistory.getText();
		if(breadCrumbText.toUpperCase().equals(TextToVerify.toUpperCase())){
			return true;
		}
		return false;
	}
	
	/**
	 * Verify the breadcrum navigation
	 * @param breadcrumb - 
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyBreadcrumbNavigation(String breadcrumb)throws Exception{
		switch (breadcrumb){
		case "Home":
			BrowserActions.clickOnElementX(breadCrumbHome, driver, "BreadCrumbHome");
			Utils.waitForPageLoad(driver);
			return driver.getCurrentUrl().contains("home") 
					|| driver.findElement(By.cssSelector(".pt_storefront")).isDisplayed();
			
		case "My Account":
			BrowserActions.clickOnElementX(breadCrumbMyAccount, driver, "BreadCrumb my account");
			Utils.waitForPageLoad(driver);
			return driver.getCurrentUrl().contains("account") 
					|| driver.getCurrentUrl().contains("register") 
					|| driver.findElement(By.cssSelector(".pt_account")).isDisplayed();
		default:
			return false;
		}
	}
	/**
	 * Verify order status
	 * @return flag - 
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderStatus()throws Exception{
		boolean flag=true;
		
		return flag;
	}
	
	/**
	 * to click the breadcrumb value
	 * @param breadcrumbValue - 
	 * @return HomePage for breadcrumbValue=Home
	 * @return MyAccountPage for breadcrumbValue=others
	 * @throws Exception - Exception
	 */
	public Object clickCategoryInBreadcrumb(String breadcrumbValue) throws Exception {
		for (WebElement ele : lstbreadcrumbs) {
			if (ele.getText().trim().equalsIgnoreCase(breadcrumbValue)) {
				if (breadcrumbValue.trim().equalsIgnoreCase("Home")) {
					BrowserActions.clickOnElementX(ele, driver, "Breadcrumb Category");
					Utils.waitForPageLoad(driver);
					return new HomePage(driver).get();
				} else {
					BrowserActions.clickOnElementX(ele, driver, "Breadcrumb Category");
					Utils.waitForPageLoad(driver);
					return new MyAccountPage(driver).get();
				}
			}
		}
		// if breadcrumb value is not found, return null
		return null;
	}
	
	/**
	 * Verify click on breadcrum mobile 
	 * @throws Exception - Exception
	 */
	public void clickOnBreakCrumbMobile() throws Exception{
		BrowserActions.clickOnElementX(breadcrumBackArrow, driver, "Breadcrumb");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on Return link 
	 * @throws Exception - Exception
	 */
	public ReturnOrderPage clickOnReturn() throws Exception{
		if(Utils.waitForElement(driver, txtReturnItem)) {
			BrowserActions.clickOnElement(txtReturnItem, driver, "Return link");
		}else {
			BrowserActions.clickOnElement(txtReturnItem1, driver, "Return link");
		}
		Utils.waitForPageLoad(driver);
		return new ReturnOrderPage(driver).get();
	}
	
	/**
	 * To click on Return link 
	 * @throws Exception - Exception
	 */
	public ReturnAndExchangePage clickOnReturnINTL() throws Exception{
		if(Utils.waitForElement(driver, txtReturnItem)) {
			BrowserActions.clickOnElement(txtReturnItem, driver, "Return link");
		}else {
			BrowserActions.clickOnElement(txtReturnItem1, driver, "Return link");
		}
		Utils.waitForPageLoad(driver);
		return new ReturnAndExchangePage(driver).get();
	}
	
	/**
	 * Verify get breadcrum count
	 * @return breadcrum count Integer
	 * @throws Exception - Exception
	 */
	public int getbreadcrumbCount() throws Exception{
		Utils.waitForPageLoad(driver);
		return lstbreadcrumbs.size();
	}
	
	/**
	 * to verify Bredcrum is displayed or clickable
	 * @param str -
	 * @param validation - 
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public boolean validateBreadcrum(String str, String validation) throws Exception {
		boolean temp = false,flag=false;
		int count=getbreadcrumbCount();
		for (int i = 0; i < count; i++)  {
			if (validation == "clickable") {
				if (lstbreadcrumbs.get(i).getText().equals(str)) {
					String tag=lstbreadcrumbs.get(i).getTagName();
					temp = lstbreadcrumbs.get(i).isEnabled();
					List<String> clickableTags= Arrays.asList("input","button","a");
					String stringValue = lstbreadcrumbs.get(i).getAttribute("href");
					if ((clickableTags.contains(tag)&& stringValue != null)&&temp) {
						flag=true;
						if(stringValue.contains("void(0)"))
							flag= false;
					}
				}
			} else if ((validation == "displayed")) {	
				flag = false;
				if(Utils.isMobile()) {
					Log.message("Breadcrum Value" +lstbreadcrumb.getText());
					if (lstbreadcrumb.getText().equalsIgnoreCase(str)) {
						flag = true;
					}	
				} else {
					if (lstbreadcrumbs.get(i).getText().equalsIgnoreCase(str)) {
						flag = lstbreadcrumbs.get(i).isDisplayed();
					}
				}
			}
		}
		return flag;
	}
	
	/**
	 * Verify click order by index
	 * @param int - index 
	 * @throws Exception - Exception
	 */
	public void clickOrderByIndex(int index) throws Exception {
		if(orderlist.size() > 0) {
			try {
				if(Utils.isMobile()) {
					WebElement expand = orderlist.get(index).findElement(By.cssSelector(".arr-img.only-for-mobile"));
					BrowserActions.clickOnElementX(expand, driver, "Expand order card");
				}
				WebElement nthViewDetails = orderlist.get(index).findElement(By.cssSelector(".view-detials"));
				BrowserActions.clickOnElementX(nthViewDetails, driver, "Order number " + index+1);
				Utils.waitForPageLoad(driver);
			} catch (Exception e) {
				Log.event(e.getMessage());
			}
		} else {
			Log.event("No order found.");
		}
	}
	
	/**
	 * To click no order by order number
	 * @param orderNumber - Order Number to view detail of
	 * @throws Exception - Exception
	 */
	public void clickOrderByNumber(String orderNumber) throws Exception{
		if (getOrderNumersList().size() == 0) {
			Log.event("No order found.");
		} else if (getOrderNumersList().contains(orderNumber)) {
			toggleOrderDetailMobile(orderNumber, Toggle.Open);
			WebElement btnViewDetails = getOrderRowElement(orderNumber).findElement(By.cssSelector(".view-detials"));
			BrowserActions.clickOnElementX(btnViewDetails, driver, "view details for " + orderNumber);
		} else {
			Log.event("Order number not found.");
		}
	}
	
	/**
	 * Verify close summary
	 * @throws Exception - Exception
	 */
	public void closesummary() throws Exception {
		BrowserActions.clickOnElementX(closearrow, driver, "Close order summary tray");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * Verify open close summary tray
	 * @param index - 
	 * @throws Exception - Exception
	 */
	public void opencloseOrderSummarytray(int index) throws Exception {
		BrowserActions.clickOnElementX(opencloseOrderSummary.get(index), driver, "Order summary Tray/Arrow");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * Verify order of elements in order entry
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderListElementsOrder()throws Exception{
		for(WebElement orderEntry: divOrderListEntries) {
			if(orderEntry.findElement(By.cssSelector(".col.col-1")).getLocation().x > orderEntry.findElement(By.cssSelector(".col.col-2")).getLocation().x
			|| orderEntry.findElement(By.cssSelector(".col.col-2")).getLocation().x > orderEntry.findElement(By.cssSelector(".col.col-3")).getLocation().x
			|| orderEntry.findElement(By.cssSelector(".col.col-3")).getLocation().x > orderEntry.findElement(By.cssSelector(".col.col-4")).getLocation().x
			|| orderEntry.findElement(By.cssSelector(".col.col-4")).getLocation().x > orderEntry.findElement(By.cssSelector(".col.col-5")).getLocation().x) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Verify order of elements in order entry for Mobile
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderListElementsOrderMobile()throws Exception{
		for(WebElement orderEntry: divOrderListEntries) {
			if(orderEntry.findElement(By.cssSelector(".order-date .only-for-mobile")).getLocation().x > orderEntry.findElement(By.cssSelector(".order-price .only-for-mobile")).getLocation().x) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Verify elements in order entry for mobile
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyMobileOrderListElements()throws Exception{
		boolean orderNumberViewable, orderDateViewable, orderTotalViewable;
		for(WebElement orderEntry: divOrderListEntries) {
			orderNumberViewable = Utils.waitForElement(driver, orderEntry.findElement(By.cssSelector(".col.col-1")));
			orderDateViewable = Utils.waitForElement(driver, orderEntry.findElement(By.cssSelector(".col.col-2")));
			orderTotalViewable =Utils.waitForElement(driver, orderEntry.findElement(By.cssSelector(".col.col-3")));
			if(!(orderNumberViewable && orderDateViewable && orderTotalViewable))
				return false;
		}
		return true;
	}
	
	/**
	 * Verify mobile order list is closed
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyMobileOrderListIsClised()throws Exception{
		boolean orderViewDetailsViewable;
		for(WebElement orderEntry: divOrderListEntries) {
			orderViewDetailsViewable = Utils.waitForElement(driver, orderEntry.findElement(By.cssSelector(".col.col-5")));
			if(orderViewDetailsViewable)
				return false;
		}
		return true;
	}
	
	/**
	 * Verify mobile order list is closed
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyMobileOrderListIsOpened()throws Exception{
		boolean orderViewDetailsViewable;
		for(WebElement orderEntry: divOrderListEntries) {
			WebElement viewDetails = orderEntry.findElement(By.cssSelector(".col.col-5"));
			orderViewDetailsViewable = Utils.waitForElement(driver, viewDetails);
			if(!orderViewDetailsViewable) {
				BrowserActions.javascriptClick(orderEntry, driver, "Order entry");
			}
			if(Utils.waitForElement(driver, viewDetails))
				return false;
		}
		return true;
	}

	/**
	 * To sanitize order number extracted from element
	 * @param orderNumber - raw order number text
	 * @return Sanitized order number
	 */
	private String sanitizeOrderNumber(String orderNumber) throws Exception {
		if(orderNumber.contains("\n")) {
			orderNumber = orderNumber.replaceAll("\n", "");
		}
		if(orderNumber.contains("#")) {
			orderNumber = orderNumber.split("#")[1];
		}
		return orderNumber.trim();
	}
	
	/**
	 * To get list of order numbers in array format
	 * @return String[] - order numbers
	 * @throws Exception - Exception
	 */
	public List<String> getOrderNumersList() throws Exception {
		List<String> array = new ArrayList<String>();
		for (WebElement orderNumber : orderNumbers) {
			String valueOrderNumber = orderNumber.getText().trim();
			array.add(sanitizeOrderNumber(valueOrderNumber));
		}
		return array;
	}
	
	/**
	 * To get list of order numbers in array format
	 * @return String[] - order numbers
	 * @throws Exception - Exception
	 */
	public List<String> getReturnableOrderNumersList() throws Exception {
		List<String> array = new ArrayList<String>();
		for (WebElement orderWithReturn : divOrderWithReturnsListEntries) {
			WebElement orderNumber = orderWithReturn.findElement(By.cssSelector(".order-number .value"));
			String valueOrderNumber = orderNumber.getText().trim();
			array.add(sanitizeOrderNumber(valueOrderNumber));
		}
		return array;
	}
	
	/**
	 * To get order date for given order number
	 * @param orderNumber - Order number
	 * @return String - Order Date
	 * @throws Exception - Exception
	 */
	public String getOrderDateForOrderNumber(String orderNumber)throws Exception{
		WebElement orderRow = driver.findElement(By.xpath("//span[contains(text(),'" + orderNumber + "')]//ancestor::li"));
		String orderDate = orderRow.findElement(By.cssSelector(".order-date .value")).getText().replace("Date", "").trim();
		System.out.println("Orderdata:: " + orderDate);
		return orderDate;
	}
	
	/**
	 * To get order date for given order number
	 * @param orderNumber - Order number
	 * @return String - Order Total
	 * @throws Exception - Exception
	 */
	public String getOrderTotalForOrderNumber(String orderNumber)throws Exception{
		WebElement orderRow = driver.findElement(By.xpath("//span[contains(text(),'" + orderNumber + "')]//ancestor::li"));
		String orderTotal = orderRow.findElement(By.cssSelector(".order-price .value")).getText().replace("Total", "").trim();
		return orderTotal;
	}
	
	/**
	 * To verify order numbers are unique
	 * @throws Exception - Exception
	 */
	public boolean verifyOrderNumbersUnique() throws Exception{
		Set<String> lstOrderNumber = new HashSet<String>();
		for(WebElement orderNumberElement: divOrderNumbers) {
			lstOrderNumber.add(orderNumberElement.getText());
		}
		return lstOrderNumber.size() == divOrderNumbers.size();
	}
	
	/**
	 * To get order row by order number
	 * @param orderNumber
	 * @return WebElement for row matching given order number
	 * @throws Exception 
	 */
	private WebElement getOrderRowElement(String orderNumber) throws Exception{
		Log.event("Searching order number:: " + orderNumber);
		for (WebElement entry : divOrderListEntries) {
			WebElement entryNumber = entry.findElement(By.cssSelector(".order-number .value"));
			if(BrowserActions.getText(driver, entryNumber, "Order number").contains(orderNumber)) {
				return entry;
			}
		}
		return null;
	}
	
	/**
	 * To expand order row for mobile
	 * @param orderNumber
	 * @param toggle - desired state
	 */
	public void toggleOrderDetailMobile(String orderNumber, Toggle toggle) throws Exception{
		if(Utils.isMobile()) {
			WebElement orderRow = getOrderRowElement(orderNumber);
			WebElement expandArrow = orderRow.findElement(By.cssSelector(".order-number .arr-img"));
			switch (toggle) {
			case Open:
				if(orderRow.getAttribute("class").contains("opened")) {
					Log.event("Order already expanded");
				} else {
					BrowserActions.clickOnElementX(expandArrow, driver, "order detail expand arrow");
				}
				break;
				
			case Close:
				if(!orderRow.getAttribute("class").contains("opened")) {
					Log.event("Order already collapsed");
				} else {
					BrowserActions.clickOnElementX(expandArrow, driver, "order detail expand arrow");
				}
				break;
			}
		}
	}
	
	/**
	 * To get various order summary attribute from order history page
	 * @param orderNumber
	 * @param attribute
	 */
	public String getOrderAttributeFromSummary(String orderNumber, OrderAttribute attribute) throws Exception{
		WebElement orderRow = getOrderRowElement(orderNumber);
		WebElement orderRowAttribute = null;
		String attributeValue = "";
		switch(attribute) {
		case Number:
			orderRowAttribute = orderRow.findElement(By.cssSelector(".order-number .value"));
			break;
		case Brand:
			orderRowAttribute = orderRow.findElement(By.cssSelector(".col-brand .value"));
			break;
		case Date:
			orderRowAttribute = orderRow.findElement(By.cssSelector(".order-date .value"));
			break;
		case Total:
			orderRowAttribute = orderRow.findElement(By.cssSelector(".order-price .value"));
			break;
		case Status:
			orderRowAttribute = orderRow.findElement(By.cssSelector(".order-status .value"));
			break;
		}
		attributeValue = BrowserActions.getText(driver, orderRowAttribute, orderNumber + " " + attribute.toString());
		Log.event(attribute.toString() + " for order number #" + orderNumber + " is:: " + attributeValue);
		return attributeValue;
	}
	
	/**
	 * To click on international order submit button
	 * @throws Exception - Exception
	 */
	public void clickOnSubmitINTL() throws Exception {
		BrowserActions.clickOnElement(btnSubmitINTL, driver, "Submit button in INTL");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To find an international order with valid order number and email
	 * @param order
	 * @throws Exception - Exception
	 */
	public void findOrderINTL(String orderNumber, String orderEmail) throws Exception {
		BrowserActions.typeOnTextField(fldOrderNumberINTL, orderNumber, driver, "INTL order sumber");
		BrowserActions.typeOnTextField(fldOrderEmailINTL, orderEmail, driver, "INTL order email");
		BrowserActions.clickOnElement(btnSubmitINTL, driver, "Submit button in INTL");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on the "buy again" link based on given index
	 * @param index - integer
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage clickOnBuyAgainLinkByIndex(int index) throws Exception {
		BrowserActions.clickOnElementX(lnkBuyAgain.get(index), driver, "buy again link");
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}
}