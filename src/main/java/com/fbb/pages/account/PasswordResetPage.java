package com.fbb.pages.account;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class PasswordResetPage extends LoadableComponent<PasswordResetPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	

	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(css = ".setnewpassword-layout")
	WebElement frmPasswordReset;

	@FindBy(css = "#dwfrm_requestpassword_email")
	WebElement fldEmailAddress;
	
	@FindBy(css = "button[name='dwfrm_requestpassword_send']")
	WebElement btnSend;
	
	@FindBy(css = "#dwfrm_requestpassword_email-error")
	WebElement lblEmailError;
	
	@FindBy(css = ".wrapper-forgot-password")
	WebElement subHeadRequestPassword;
	
	@FindBy(css = ".breadcrumb .breadcrumb-element.hide-mobile")
	List<WebElement> lstTxtProductInBreadcrumb;
	
	@FindBy(css = ".breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;
	
	@FindBy(css = "div.breadcrumb")
	WebElement breadcrumbMobile;
	
	@FindBy(css = ".promo-banner")
	WebElement promoContent;
	
	@FindBy(css = ".setnewpassword .email .label-text")
	WebElement lblEmailPlaceHolder;
	
	@FindBy(css = ".pt_account:not(.null)")
	WebElement myAccount;
	
	@FindBy(css = "div.pt_storefront")
	WebElement homePage;
	
	@FindBy(css = ".hide-desktop.hide-tablet.back-arrow")
	WebElement breadcrumbArrowMobile;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
	WebElement breadcrumbCurrentElement;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop")
	WebElement breadcrumbCurrentElementMobile;
	
	@FindBy(css = ".setnewpassword")
	WebElement contentSlot;
	
	@FindBy(css = ".heading>h3")
	WebElement pwdResetHeader;
	
	@FindBy(css = ".setnewpassword-layout.resetpassword>p")
	WebElement pwdResetSubHead;
	
	@FindBy(css = "#dwfrm_requestpassword_resetemail")
	WebElement txtEmailId;
	
	@FindBy(css = "#dwfrm_requestpassword_email-error")
	WebElement txtEmailError;
	
	@FindBy(css = "div[class='form-row row-inline email required show-text'] .label-text")
	WebElement txtEmailPlaceHolder;
	
	@FindBy(css = ".wrapper-forgot-password h1")
	WebElement lblSuccessMsg;
	
	@FindBy(css = ".setnewpassword-layout.resetpassword .make-label-absolute>a")
	WebElement btnBackToLogin;
	
	@FindBy(css = ".setnewpassword .heading")
	WebElement resetPasswordPageHeading;
	
	@FindBy(css = "input[id*='dwfrm_resetpassword_password_']")
	WebElement resetPasswordPageNewPassword;
	
	@FindBy(css = ".form-row.password.required .pwd-show")
	WebElement resetPasswordPageNewPasswordShow;
	
	@FindBy(css = ".form-row.password.required.error-handle .form-caption")
	WebElement errorNewPassword;
	
	@FindBy(css = "input[id*='dwfrm_resetpassword_passwordconfirm']")
	WebElement resetPasswordPageConfirmPassword;
	
	@FindBy(css = ".form-row.passwordconfirm.required .pwd-show")
	WebElement resetPasswordPageConfirmPasswordShow;
	
	@FindBy(css = "span[id*='dwfrm_resetpassword_passwordconfirm'][id*='error']")
	WebElement errorConfirmPassword;
	
	@FindBy(css = ".password-status .match-check.fail")
	WebElement errorMismatchConfirmPassword;
	
	@FindBy(css = ".form-row-button.reset-button .apply")
	WebElement resetPasswordPageReset;
	
	@FindBy(css = ".reset-message-notify")
	WebElement msgPasswordUpdated;
	
	//================================================================================
	//			WebElements Declaration End
	//================================================================================




	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public PasswordResetPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, frmPasswordReset))) {
			Log.fail("Password Reset Page is not loaded", driver);
		}
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To Enter the email Address in email field
	 * @param email - The Email Address
	 * @throws Exception - Exception
	 */
	public void enterEmailAddress(String email)throws Exception{
		BrowserActions.typeOnTextField(fldEmailAddress, email, driver, "Email Address ");
	}
	
	/**
	 * To get the Error message under Email Text box
	 * @return String - Error Message related to email address
	 * @throws Exception - Exception
	 */
	public String getErrorMessage()throws Exception{
		return BrowserActions.getText(driver, lblEmailError, "Error Message ");
	}
	
	/**
	 * To click on the Send Button
	 * @throws Exception - Exception
	 */
	public void clickOnSendButton()throws Exception{
		BrowserActions.clickOnElementX(btnSend, driver, "Send Button ");
		Utils.waitForPageLoad(driver);
	}
	/**
	 * To verify the breadcrum text
	 * @param runMode - 
	 * @return breadcrumbText - 
	 * @throws InterruptedException - 
	 */
	public List<String> getTextInBreadcrumb(String... runMode)throws Exception {
		List<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(0),
				driver);
		if (Utils.isDesktop() || Utils.isTablet() || (runMode.length > 0)) {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;
	}
	/**
	 * To Verify the breadcrum value
	 * @param index - 
	 * @throws Exception - Exception
	 */
	public void clickOnBreakCrumbValue(int index) throws Exception{
		BrowserActions.clickOnElementX(lstTxtProductInBreadcrumb.get(index), driver, "Breadcrumb");
	}
	/**
	 * To verify breadcrum text for mobile
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getTextInBreadcrumbMobile()
			throws Exception {
	
		String breadcrumbText = BrowserActions.getText(driver, breadcrumbMobile, "Breadcrumb in mobile");
		breadcrumbText = breadcrumbText.replace("BACK", "").trim();
		return breadcrumbText;
	}
	/**
	 * To verify the click on breadcrum
	 * @throws Exception - Exception
	 */
	public void clickOnBreakCrumbMobile() throws Exception{
		BrowserActions.clickOnElementX(breadcrumbArrowMobile, driver, "Breadcrumb");
	}
	/**
	 * To verify click on send button
	 * @throws Exception - Exception
	 */
	public void clickOnSendBtn() throws Exception{
		BrowserActions.clickOnElementX(btnSend, driver, "Send Button");
	}
	
	public void waitForSuccessMessage()throws Exception{
		Utils.waitUntilElementDisappear(driver, txtEmailId,10);
	}
	/**
	 * To verify the enteing the emial address
	 * @param emailId - 
	 * @throws Exception - Exception
	 */
	public void enterEmailId(String emailId) throws Exception{
		txtEmailId.clear();
		txtEmailId.sendKeys(emailId);
		txtEmailId.sendKeys(Keys.TAB);
		Utils.waitForElement(driver, txtEmailPlaceHolder);
	}
	/**
	 * To verify the get entering the mail id
	 * @return Strings  - 
	 * @throws Exception - Exception
	 */
	public String getEnteredEmailId() throws Exception{
		return BrowserActions.getText(driver, txtEmailId, "Email Id");
	}
	/**
	 * To verify clicking back button
	 * @throws Exception - Exception
	 */
	public void clickOnBackToLoginBtn() throws Exception{
		BrowserActions.clickOnElementX(btnBackToLogin, driver, "Back To Login");
	}
	
	/**
	 * Type password in password field.
	 * @throws Exception - Exception
	 */
	public void enterPasswordInResetPage(String passwordField, String passwordText, Object obj)throws Exception {
		Field f = obj.getClass().getDeclaredField(passwordField);
		f.setAccessible(true);
		WebElement passwordFieldElement = ((WebElement) f.get(obj));
		Utils.waitForElement(driver, passwordFieldElement);
		BrowserActions.typeOnTextField(passwordFieldElement, passwordText, driver, "Password field in Reset Password Page");
	}
	
	/**
	 * Click on Reset button in Reset password page.
	 * @throws Exception - Exception
	 */
	public void clickResetInResetPage()throws Exception {
		Utils.waitForElement(driver, resetPasswordPageReset);
		BrowserActions.clickOnElement(resetPasswordPageReset, driver, "Reset button");
	}
	
	/**
	 * To get Error message of New Password field
	 * @return String - Error message text
	 * @throws Exception - Exception
	 */
	public String getErrorTextNewPasswordInResetPage()throws Exception {
		Utils.waitForElement(driver, errorNewPassword);
		String errorText = BrowserActions.getText(driver, errorNewPassword, "Error message of New Password field");
		return errorText;
	}
	
	/**
	 * To get Error message of confirm Password field
	 * @return String - Error message text
	 * @throws Exception - Exception
	 */
	public String getErrorTextConfirmPasswordInResetPage()throws Exception {
		Utils.waitForElement(driver, errorConfirmPassword);
		String errorText = BrowserActions.getText(driver, errorConfirmPassword, "Error message of confirm Password field");
		return errorText;
	}
	
	/**
	 * To get mismatch Error message of confirm Password field
	 * @return String - Error message text
	 * @throws Exception - Exception
	 */
	public String getMismatchErrorTextInResetPage()throws Exception {
		Utils.waitForElement(driver, errorMismatchConfirmPassword);
		String errorText = BrowserActions.getText(driver, errorMismatchConfirmPassword, "Error message of confirm Password field");
		return errorText;
	}
	
	/**
	 * To click show password element 
	 * @throws Exception - Exception
	 */
	public String clickShowPasswordInResetPage(String passwordField, Object obj)throws Exception {
		Field f = obj.getClass().getDeclaredField(passwordField);
		f.setAccessible(true);
		WebElement passwordShowHide = ((WebElement) f.get(obj));
		Utils.waitForElement(driver, passwordShowHide);
		BrowserActions.javascriptClick(passwordShowHide, driver, "Show password");
		String showHideState = BrowserActions.getText(driver, passwordShowHide, "State of Show/Hide");
		return showHideState;
	}
	
	/**
	 * To check password field is masked or visible
	 * @param passwordField type - mask/unmask
	 * @throws Exception - Exception
	 */
	public Boolean checkPasswordTextIsMaskOrVisibleInResetPage(String passwordField, String type, Object obj)throws Exception {
		Boolean flag = false;
		Field f = obj.getClass().getDeclaredField(passwordField);
		f.setAccessible(true);
		WebElement passwordFieldElement = ((WebElement) f.get(obj));
		if(type.equals("mask")){
			if(passwordFieldElement.getAttribute("type").equals("password"))
				flag=true;
		}
		if(type.equals("unmask")){
			if(passwordFieldElement.getAttribute("type").equals("text"))
				flag=true;
		}
		return flag;
	}
	
}
