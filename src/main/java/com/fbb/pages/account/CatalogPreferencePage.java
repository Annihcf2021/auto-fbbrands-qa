package com.fbb.pages.account;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class CatalogPreferencePage extends LoadableComponent <CatalogPreferencePage>{


	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	private static final String MyAccountPageLinksNavigation = ".navigation-links-row";

	private static EnvironmentPropertiesReader demandWareProperty = EnvironmentPropertiesReader
			.getInstance("demandware");

	/**********************************************************************************************
	 ********************************* WebElements of CatalogPreferencePage ***********************************
	 **********************************************************************************************/

	@FindBy(css = ".pt_account")
	WebElement readyElement;
	
	@FindBy(css = MyAccountPageLinksNavigation)
	WebElement divAccountNavigation;

	@FindBy(css = MyAccountPageLinksNavigation + " .catalog-preferences a[style*='cursor']")
	WebElement lnkCatalogPrefAfterNavigate;

	@FindBy(css = "h1.catalog-request-header")
	WebElement lblCatalogPref;

	@FindBy(css = ".breadcrumb-element")
	List <WebElement> brdcrumMyAccount;

	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop")
	WebElement brdcrumCurrentMobile;

	@FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
	WebElement brdcrumCurrentTablet;

	@FindBy(css = ".catalog-request-header")
	WebElement catalogRequestHeader;

	@FindBy(css = ".catalog-request img")
	WebElement copycatlogimage;

	@FindBy(css = ".catalog-request-step1")
	WebElement lblStep1;

	@FindBy(css = ".catalog-request-step2")
	WebElement lblStep2;
	
	@FindBy(css = ".navigation-links-row .catalog-preferences a")
	WebElement lnkCatalogPreference;

	@FindBy(css = ".form-row.show-text")
	List <WebElement> catlogStep1;

	@FindBy(css = ".form-row.firstname.required input")
	WebElement firstName;
	
	@FindBy(css = ".sister-brand-section .sister-brand-rows ")
	List<WebElement> sisterBrands;
	
	@FindBy(css = ".sister-brand-name.hide-mobile img[alt='Ellos']")
	WebElement sisterBrandEllosDeskTab;
	
	@FindBy(css = ".sister-brand-name h1 img[alt='Full Beauty']")
	WebElement sisterBrandfullbeautyDeskTab;
	
	@FindBy(css = ".sister-brand-name.hide-desktop.hide-tablet img[alt='Full Beauty']")
	WebElement sisterBrandfullbeautyMob;
	
	@FindBy(css = ".sister-brand-name.hide-desktop.hide-tablet img[alt='Ellos']")
	WebElement sisterBrandEllosMob;	
	
	@FindBy(css = ".form-row.lastname.required input")
	WebElement lastName;

	@FindBy(css = ".form-row.address1.required input")
	WebElement address1;

	@FindBy(css = ".form-row.address2 input")
	WebElement address2;

	@FindBy(css = ".form-row.postal.required input")
	WebElement postalCode;

	@FindBy(css = ".form-row.city.required input")
	WebElement city;

	@FindBy(css = "button.catalog-preference")
	WebElement btnSaveCatlogue;

	@FindBy(css = ".form-row.state.required ")
	WebElement stateRequired;

	@FindBy(css = "label[for='dwfrm_printpreferences_firstname'] .error")
	WebElement errorMsgFirstName;

	@FindBy(css = "label[for='dwfrm_printpreferences_lastname'] .error")
	WebElement errorMsgLastName;

	@FindBy(css = "label[for='dwfrm_printpreferences_address1'] .error")
	WebElement errorMsgAddress1;

	@FindBy(css = "label[for='dwfrm_printpreferences_address2'] .error")
	WebElement errorMsgAddress2;

	@FindBy(css = "label[for='dwfrm_printpreferences_city'] .error")
	WebElement errorMsgcity;

	@FindBy(css = "label[for='dwfrm_printpreferences_postal'] .error")
	WebElement errorMsgPostalcode;

	@FindBy(css = "label[for='dwfrm_printpreferences_states_state'] .error")
	WebElement errorMsgState;

	@FindBy(css = "label[for=dwfrm_printpreferences_firstname][class='input-focus']")
	WebElement firstNamePlaceholder;

	@FindBy(css = "label[for=dwfrm_printpreferences_lastname][class='input-focus']")
	WebElement lastNamePlaceholder;

	@FindBy(css = "label[for=dwfrm_printpreferences_address1][class='input-focus']")
	WebElement address1Placeholder;

	@FindBy(css = "label[for=dwfrm_printpreferences_address2][class='input-focus']")
	WebElement address2Placeholder;

	@FindBy(css = "label[for=dwfrm_printpreferences_postal][class='input-focus']")
	WebElement postalPlaceholder;

	@FindBy(css = "label[for=dwfrm_printpreferences_city][class='input-focus']")
	WebElement cityPlaceholder;	

	@FindBy(css = "a.breadcrumb-element:nth-child(1)")
	WebElement breadCrumbHome;

	@FindBy(css = "a.breadcrumb-element:nth-child(2)")
	WebElement breadCrumbMyAccount;
	
	@FindBy (css = "a.breadcrumb-element:nth-child(4)")
	WebElement breadCrumbCatalogPreference;

	@FindBy(css = ".form-row.requestCatalog.show-text ")
	WebElement requestCatluge;
	
	@FindBy(css = ".form-row.receiveLessCatalog.show-text ")	
	WebElement receiveLessCat;
	
	@FindBy(css = ".form-row.stopCatalog.show-text")	
	WebElement stopReceivingCat;	
	
	@FindBy(css = "#dwfrm_printpreferences_catalogPreferences_requestCatalog")
	WebElement radioRequestCatluge;
	
	@FindBy(css = "#dwfrm_printpreferences_catalogPreferences_receiveMoreCatalog")
	WebElement radioReceiveMoreCat;
	
	@FindBy(css = "#dwfrm_printpreferences_catalogPreferences_receiveLessCatalog")
	WebElement radioReceiveLessCat;
	
	@FindBy(css = "#dwfrm_printpreferences_catalogPreferences_stopCatalog")
	WebElement radioStopCat;
	
	@FindBy(css = ".catalog-prefernces")
	WebElement catalogPreference;
	
	@FindBy(css = "div.breadcrumb")
	WebElement divBreadcrumb;
	
	@FindBy(css = ".secondary-navigation-links")
	WebElement divAccountNavLinks;
	
	@FindBy(css = ".catalog-thankyou")
	WebElement divCatalogThankyou;
	/**********************************************************************************************
	 ********************************* WebElements of CatalogPreferencePage - Ends ****************************
	 **********************************************************************************************/



	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public CatalogPreferencePage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Catalog Preference Page did not open up. Site might be down.", driver);
		}

		elementLayer = new ElementLayer(driver);

	}// isLoaded

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver,readyElement );

	}

	/**
	 * To Verify navigate to Account page
	 * @param index - 
	 * @return MyAccountPage
	 * @throws Exception - Exception
	 */
	public MyAccountPage navigateToMyAccountPage(int index) throws Exception{
		BrowserActions.clickOnElementX(brdcrumMyAccount.get(index), driver, "Breadcrum My account");
		Utils.waitForPageLoad(driver);
		return new MyAccountPage(driver).get();
	}

	/**
	 * 	To Verify the navigate to home page
	 * @param index - 
	 * @return Homepage
	 * @throws Exception - Exception
	 */
	public HomePage navigateToMyHometPage(int index) throws Exception{
		BrowserActions.clickOnElementX(brdcrumMyAccount.get(index), driver, "Breadcrum My account");
		Utils.waitForPageLoad(driver);
		return new HomePage(driver).get();
	}

	/**
	 * To verify breadcrum element is displayed
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean breadCrumElementIsDisplayed() throws Exception{

		for(int i=0;i<(brdcrumMyAccount.size())-1;i++)
		{
			System.out.println(brdcrumMyAccount.get(1).getText());
			if(brdcrumMyAccount.get(i).isDisplayed())
			{

				continue;
			}
			else
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * To Verify the Catlogue step list
	 * @return List<String> - List of catalog step strings
	 * @throws Exception - Exception
	 */
	public List<String> catlogueStep1List()throws Exception{
		List<String> xyz = new ArrayList<String>();
		for(int i =0;i<catlogStep1.size();i++)
		{
			Log.event("Catalog item: " + catlogStep1.get(i).getText().toLowerCase());
			xyz.add(catlogStep1.get(i).getText().toLowerCase());
		}
		return xyz;
	}
	/**
	 * To Verify Entering the first name
	 * @param FirstName - 
	 * @throws Exception - Exception
	 */
	public void enterFirstName(String FirstName)throws Exception {
		BrowserActions.typeOnTextField(firstName, FirstName, driver, "FirstName");

	}
	/**
	 * To Verify clearing the first name
	 * @throws Exception - Exception
	 */
	public void clearFirstName()throws Exception {
		BrowserActions.clearTextField(firstName, "ClearFirstname");

	}
	/**
	 * To Verify Entering the last name
	 * @param lastname - 
	 * @throws Exception - Exception
	 */
	public void enterLastName(String lastname)throws Exception {
		BrowserActions.typeOnTextField(lastName, lastname, driver, "LastName");
	}
	
	/**
	 * To Verify Entering the last name
	 * @param lastname - 
	 * @throws Exception - Exception
	 */
	public void selectCatalogPrefRadioButton(String catalog)throws Exception {
		switch (catalog) {
		case "Request":
			BrowserActions.clickOnElementX(radioRequestCatluge, driver, "Request catalog");
			break;
		case "More":
			BrowserActions.clickOnElementX(radioReceiveMoreCat, driver, "More catalog");			
			break;
		case "Less":
			BrowserActions.clickOnElementX(radioReceiveLessCat, driver, "Less catalog");
			break;
		case "Stop":
			BrowserActions.clickOnElementX(radioStopCat, driver, "Stop catalog");
			break;
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To Verify Enter address1
	 * @param Address1 - 
	 * @throws Exception - Exception
	 */
	public void enterAddress1(String Address1)throws Exception {
		BrowserActions.typeOnTextField(address1, Address1, driver, "Address1");
	}
	
	/**
	 * To Verify Enter address1
	 * @param Address1 - 
	 * @throws Exception - Exception
	 */
	public void clickCatalogPreferences()throws Exception {
		BrowserActions.clickOnElement(lnkCatalogPreference, driver, "Catalog Preference link");
		Utils.waitForPageLoad(driver);
	}
	
	
	/**
	 * To Verify Enter address2
	 * @param Address2 - 
	 * @throws Exception - Exception
	 */
	public void enterAddress2(String Address2)throws Exception {
		BrowserActions.typeOnTextField(address2, Address2, driver, "Address2");
	}
	
	/**
	 * To Verify Enter city
	 * @param City - 
	 * @throws Exception - Exception
	 */
	public void enterCity(String City)throws Exception {
		BrowserActions.typeOnTextField(city, City, driver, "City");
	}
	
	/**
	 * To Verify Enter PostalCode
	 * @param pcode - 
	 * @throws Exception - Exception
	 */
	public void enterPostalCode(String pcode)throws Exception {
		BrowserActions.typeOnTextField(postalCode, pcode, driver, "PostalCode");
		postalCode.sendKeys(Keys.TAB);
		postalCode.sendKeys(Keys.TAB);
	}
	
	/**
	 * To verify the clicking the save button
	 * @throws Exception - Exception
	 */
	public void clickSaveCatalogue()throws Exception{
		BrowserActions.clickOnElementX(btnSaveCatlogue, driver, "Save address");
		Utils.waitForElement(driver, divCatalogThankyou);
	}
	
	
	/**
	 * To verify the clicking the save button
	 * @throws Exception - Exception
	 */
	public boolean checkRequestCatalogIsSelected()throws Exception{
		if (radioRequestCatluge.isSelected()) {
			return true;
		}
		return false;
	}

	/**
	 * To verify the firstname error message
	 * @return error message
	 * @throws Exception - Exception
	 */
	public String getErrorMessageFirstname()throws Exception{
		return BrowserActions.getText(driver, errorMsgFirstName, "Error message");
	}
	
	/**
	 * To verify the Lastname error message
	 * @return error message
	 * @throws Exception - Exception
	 */
	public String getErrorMessageLastname()throws Exception{
		return BrowserActions.getText(driver, errorMsgLastName, "Error message");
	}
	
	/**
	 * To verify the Address1 error message
	 * @return error message
	 * @throws Exception - Exception
	 */
	public String getErrorMessageAddress1()throws Exception{
		return BrowserActions.getText(driver, errorMsgAddress1, "Error message");
	}
	
	/**
	 * To verify the city error message
	 * @return error message
	 * @throws Exception - Exception
	 */
	public String getErrorMessageCity()throws Exception{
		return BrowserActions.getText(driver, errorMsgcity, "Error message");
	}
	
	/**
	 * To verify the postal error message
	 * @return error message
	 * @throws Exception - Exception
	 */
	public String getErrorMessagePostalcode()throws Exception{
		Log.messageT(BrowserActions.getText(driver, errorMsgPostalcode, "Error message"));
		return BrowserActions.getText(driver, errorMsgPostalcode, "Error message");
	}
	
	/**
	 * To verify the state error message
	 * @return error message
	 * @throws Exception - Exception
	 */
	public String getErrorMessageState()throws Exception{
		return BrowserActions.getText(driver, errorMsgState, "Error message");
	}

	/**
	 * To verify property of catlogue preference header
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public boolean verifyCatloguePrefHeaderTextIsComingFromProperty()throws Exception{
		String textToVerify = BrowserActions.getText(driver, catalogRequestHeader, "Heading");
		String propText = demandWareProperty.getProperty("catalog.header");

		if(!(textToVerify.toLowerCase().contains(propText.toLowerCase()))) {
			return false;
		}
		return true;
	}
	/**
	 * To verify property of catlogue preference step 1 value
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public boolean verifyCatloguePrefStep1FromProperty()throws Exception{
		String textToVerify = BrowserActions.getText(driver, lblStep1, "Heading");
		Log.event("Catalog Property :: " + textToVerify);
		String propText = demandWareProperty.getProperty("catalog.step1");

		if(!(textToVerify.toLowerCase().contains(propText.toLowerCase()))) {
			return false;
		}
		return true;
	}
	/**
	 * To verify property of catlogue preference step2
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public boolean verifyCatloguePrefStep2FromProperty()throws Exception{
		String textToVerify = BrowserActions.getText(driver, lblStep2, "Heading");
		String propText = demandWareProperty.getProperty("catalog.step2");
		Log.event("Element Text :: " + textToVerify);
		Log.event("Catalog Property :: " + propText);

		if(!(textToVerify.toLowerCase().equals(propText.toLowerCase()))) {
			return false;
		}
		return true;
	}
	/**
	 * To verify breadcrum text
	 * @return boolean value - 
	 * @param TextToVerify - to verify 
	 * @throws Exception - Exception
	 */
	public boolean verifyBreadcrumbText(String TextToVerify)throws Exception{
		boolean flag=false;

		String breadCrumbText=breadCrumbHome.getText()+" / "+ breadCrumbMyAccount.getText()+" / "+breadCrumbCatalogPreference.getText();
		Log.messageT(breadCrumbText);
		Log.event("Bread Crumb Text :: " + breadCrumbText);
		if(breadCrumbText.equals(TextToVerify)){
			flag=true;
		}

		return flag;
	}
	/**
	 * To verify breadcrum text
	 * @return boolean value
	 * @param TextToVerify - 
	 * @throws Exception - Exception
	 */
	public boolean verifyBreadcrumbText1(String TextToVerify)throws Exception{
		boolean flag=false;

		String breadCrumbText=breadCrumbHome.getText()+" / "+breadCrumbCatalogPreference.getText();
		System.out.println(breadCrumbText);
		if(breadCrumbText.equals(TextToVerify)){
			flag=true;
		}

		return flag;
	}


}
