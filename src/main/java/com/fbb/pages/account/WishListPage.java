package com.fbb.pages.account;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.QuickShop;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.support.Brand;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;

public class WishListPage extends LoadableComponent <WishListPage>{
	
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	private static EnvironmentPropertiesReader envProperty = EnvironmentPropertiesReader.getInstance("env");
	
	private static final String MyAccountPageLinksNavigation = ".navigation-links-row";
	
	
	/**********************************************************************************************
	 ********************************* WebElements of WishListPage Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = ".pt_wish-list.wishlist")
	WebElement readyElement;
	
	@FindBy(css = MyAccountPageLinksNavigation + " .wishlists a[style*='cursor']")
	WebElement lnkWishListAfterNavigate;
	
	@FindBy(css = ".wishlist-main h1")
	WebElement lblWishList;
	
	@FindBy(css = "#dwfrm_wishlist_search_firstname")
	WebElement tbSearchWishlistFirstName;
	
	@FindBy(css = "#dwfrm_wishlist_search_lastname")
	WebElement tbSearchWishlistLastName;
	
	@FindBy(css = ".product-add-to-cart button")
	List<WebElement> btnAddtoBag;
	
	@FindBy(css = ".addtocartoverlay-content button.checkout-now")
    WebElement btnCheckoutInMCOverlay;
	
	@FindBy(css = ".product-add-to-cart button")
	WebElement btnAddtoCart;
	
	@FindBy(css = ".wishlist-row")
	WebElement txtProductRow;
	
	@FindBy(css = ".product-list-item .attribute")
	WebElement txtProductAttribute;
	
	@FindBy(css = ".item-availability.hide-desktop.hide-tablet .product-availability-list")
	WebElement txtProductAvailability_Mob;
	
	@FindBy(css = ".item-availability.hide-mobile .product-availability-list")
	WebElement txtProductAvailability_Desk_Tab;
	
	@FindBy(css = "#dwfrm_wishlist_search_email")
	WebElement tbSearchWishlistemail;
	
	@FindBy(css = ".list-title")
	WebElement txtTitleWishlist;
	
	@FindBy(css = "button[name='dwfrm_wishlist_search_search']")
	WebElement btnSearchWishlist;
	
	@FindBy(css = ".option-date-added")
	WebElement txtDateAdded;
	
	@FindBy(css = ".breadcrumb-element.hide-mobile")
	List<WebElement> lstbreadcrumbs;
	
	@FindBy(css = ".list-title")
	WebElement lblWishlistTitle;
	
	@FindBy(css = ".results-found .heading")
	WebElement lblResultFound;
	
	@FindBy(css = ".wishlist-main .heading")
	WebElement wishlistHeading;
	
	@FindBy(css = ".first-name .inner-block")
	WebElement firstnameResultFound;
	
	@FindBy(css = ".fo-social")
	WebElement socialIcon;
	
	@FindBy(css = ".last-name .inner-block")
	WebElement lastnameResultFound;
	
	@FindBy(name = "dwfrm_wishlist_setListPublic")
	WebElement btnMakeWishlistpublic;
	
	@FindBy(css = "button.share-option.hide-mobile")
	WebElement btnMakeWishlistpublictoggle;
	
	@FindBy(css = "button.share-option.hide-desktop.hide-tablet")
	WebElement btnMakeWishlistpublictoggleMobile;
	
	@FindBy(id = "dwfrm_wishlist_items_i0_public")
	WebElement chkBoxMakeThisPublic;
	
	@FindBy(css = ".edit-section")
	WebElement sectionEditRemoveBlock;
	
	@FindBy(css = ".share-link-copy")
	WebElement shareLinkUrl;
	
	@FindBy(css = "#copy-text")
	WebElement copyText;
	
	@FindBy (css = ".share-options")
	WebElement shareOptions;
	
	@FindBy (css = "a.share-icon")
	List<WebElement> shareIcons;
	
	@FindBy(css = ".error-message")
	WebElement wishlistErrorMsg;
	
	@FindBy(css = ".wishlist-content>h2")
	WebElement lblWishListEmpty;

	@FindBy(css = "div[class^='share-link-copy']>a")
	WebElement copylinkurl;
	
	@FindBy(css = ".item-image img[alt='cart brand']")
	WebElement imgItemBrand;
	
	@FindBy(css = ".item-image a img")
	WebElement imgProductItem;
	
	@FindBy(css = ".copy-link")
	WebElement copylink;
	
	@FindBy(css = "div.edit-wishlist-detail.hide-mobile>a")
	List<WebElement> editlink;
	
	@FindBy(css = ".edit-wishlist-detail-mobile a")
	List<WebElement> editLinkMobile;
	
	@FindBy(css = ".quick-view")
	WebElement divQuickShop;
	
	@FindBy(css = ".ui-front.quick-view")
	WebElement quickview;
		
	@FindBy(css = ".button-text.delete-item")
	List<WebElement> removelink;
	
	@FindBy(css = ".button-fancy-small.add-to-cart")
	List<WebElement> addtobag;
	
	@FindBy(css = ".minicartpopup")
	WebElement mdlAddToCartPopup;
	
	@FindBy(css = ".minicartpopup .continue-shopping")
	WebElement mdlContinueShopping;
	
	@FindBy(css = "div[class$='clearfix make-label-absolute'] button[id='Facebook']")
	WebElement btnSigninWithfacebook;
	
	@FindBy(css = "div.wishlist-row")
	List<WebElement> wishListItemList;
	
	@FindBy(css = ".swatches.size .selectable:not(.selected)")
	WebElement sizeUnselected;
	
	@FindBy(css = "div[data-attribute='size'] .value")
	List<WebElement> sizeSelected;
	
	@FindBy(css = "div.wishlist-row div div div a img")
	List<WebElement> wishListItemImageList;
	
	@FindBy(css = ".wishlist-row .product-list-item .name")
	List<WebElement> lstWishListItemName;
	
	@FindBy(css = ".product-list-item .name")
	WebElement wishListItemName;
	
	@FindBy(css = ".product-list-item .name")
	WebElement wishListItemDetails;
	
	@FindBy(css = ".edit-wishlist-detail-mobile.hide-desktop.hide-tablet a")
	WebElement lnkEditMobile;
	
	@FindBy(css = "div.edit-wishlist-detail.hide-mobile>a")
	WebElement lnkEdit;
	
	@FindBy(css = "div.wishlist-row div div div img")
	List<WebElement> wishListItemBrandLogoList;
	
	@FindBy(css = ".slimScrollDiv .product-variation-content .product-name")
	WebElement modalHeader;
	
	@FindBy(css = ".size li.selectable")
	List<WebElement> sizeOptions;
	
	@FindBy(css = ".size li.selectable")
	WebElement sizeOption;
	
	@FindBy(css = "#add-to-cart")
	WebElement modalWishlistUpdate;
	
	@FindBy(css = ".wishlist-logo")
	WebElement modalAddToWishlist;
	
	@FindBy(css = "a.share-wishlist")
	WebElement emailShare;
	
	@FindBy(css = ".wishlist-email")
	WebElement winEmailShare;

	@FindBy(css = ".search-wishilist-box")
	WebElement searchWishlistBox;
	
	@FindBy(css = ".wishlist-columns .col-1")
	WebElement wishListProductDetails;
	
	@FindBy(css = ".ui-dialog-titlebar-close")
	WebElement btnCloseEditModal;
	
	@FindBy(css = ".view a")
	WebElement btnViewFriendWishlist;
	
	@FindBy(css = "#pdpMain")
	WebElement pdpMain;
	/**********************************************************************************************
	 ********************************* WebElements of WishListPage - Ends ****************************
	 **********************************************************************************************/

	

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public WishListPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("WishList Page did not open up. Site might be down.", driver);
		}
		
		elementLayer = new ElementLayer(driver);

	}// isLoaded
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver,readyElement );
		
	}

	/**
	 * To seach a wish list
	 * @param firstname -
	 * @param lastname -
	 * @param email -
	 * @throws Exception -
	 */
	public void searchWishlist(String firstname,String lastname,String email) throws Exception{
		Utils.waitForElement(driver, tbSearchWishlistemail);		
		tbSearchWishlistFirstName.sendKeys(firstname);			
		tbSearchWishlistLastName.sendKeys(lastname);
		tbSearchWishlistemail.sendKeys(email);		
		BrowserActions.clickOnElementX(btnSearchWishlist, driver, "Search Wishlist button");
		Utils.waitForPageLoad(driver);		
	}
	
	/**
	 * To get bread crumb count
	 * @return Integer -
	 * @throws Exception -
	 */
	public int getbreadcrumbCount() throws Exception{
		Utils.waitForPageLoad(driver);
		int count=lstbreadcrumbs.size();
		return count;
	}
	
	/**
	 * to verify Share icons open in site specific page
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public boolean validateShareIcons() throws Exception {
		for(WebElement element : shareIcons) {
			String link = element.getAttribute("href");
			String dataShare = element.getAttribute("data-share");
			String target = element.getAttribute("target");
			if( !(link.contains(dataShare) && target.equals("_blank")) )
				return false;
		}
		return true;
	}
	/**
	 * To verify the return breadcrum index
	 * @param index - 
	 * @return Webelement
	 * @throws Exception - Exception
	 */
	public WebElement returnbreadcrumbbyindex(int index) throws Exception{
		Utils.waitForPageLoad(driver);		
		return lstbreadcrumbs.get(index);
	}

	/**
	 * to click the breadcrumb value
	 * @param breadcrumbValue - bread crumb name 
	 * @return PdpPage - Product detail page object
	 * @throws Exception - Exception
	 */
	public Object clickCategoryInBreadcrumb(String breadcrumbValue) throws Exception {
		if (Utils.isDesktop()) {
			for (WebElement ele : lstbreadcrumbs) {
				if (ele.getText().trim().equalsIgnoreCase(breadcrumbValue)) {
					if (breadcrumbValue.trim().equalsIgnoreCase("Home")) {
						BrowserActions.clickOnElementX(ele, driver, "Breadcrumb Category");
						Utils.waitForPageLoad(driver);
						return new HomePage(driver).get();
					} else {
						BrowserActions.clickOnElementX(ele, driver, "Breadcrumb Category");
						Utils.waitForPageLoad(driver);
						return new MyAccountPage(driver).get();
					}
				}
			}
		}
		// if breadcrumb value is not found, return null
		return null;
	}
	/**
	 * To verify click public toggle in whishlist
	 * @param state - state will take two value - "public" and "private"
	 * @throws Exception - Exception
	 */
	public void clickMakeWishlistPublicToggle(String state) throws Exception{
		WebElement elem = null;
		if(!Utils.isMobile()) {
			elem = btnMakeWishlistpublictoggle;
		}else {
			elem = btnMakeWishlistpublictoggleMobile;
		}
		
		BrowserActions.scrollInToView(elem, driver);
		String wishlistCurrentState = BrowserActions.getText(driver, elem, "Make public/private");
		
		if(wishlistCurrentState.toLowerCase().contains(state.toLowerCase()))	{
			BrowserActions.clickOnElementX(btnMakeWishlistpublictoggle, driver, "MAKE LIST PUBLIC");
			Utils.waitForPageLoad(driver);
		} else {
			Log.event("List is already "+state);
		}	
	}
	
	/**
	 * To verify click in copytext
	 * @return Status boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyCopyText() throws Exception{
		Utils.waitForPageLoad(driver);	
		String urlFromButton, urlFromField;
		Utils.waitForElement(driver, copylink);	
		BrowserActions.clickOnElementX(copylink, driver, "Copy Link");
		tbSearchWishlistemail.sendKeys(Keys.chord(Keys.CONTROL, "v"));
		urlFromButton = tbSearchWishlistemail.getAttribute("value");
		urlFromField = copyText.getAttribute("innerHTML").trim();
		if(urlFromField.equals(urlFromButton))
		{
			return true;
		}
		tbSearchWishlistemail.clear();
		return false;
	
	}
	/**
	 * To verify click in edit link
	 * @param index - 
	 * @return Quickshop object
	 * @throws Exception - Exception
	 */
	public Object clickEditLink(int index) throws Exception{
		Utils.waitForPageLoad(driver);	
		Utils.waitForElement(driver, editlink.get(index));	
		BrowserActions.clickOnElementX(editlink.get(index), driver, "Edit Link");
		return new QuickShop(driver).get();
	}
	
	/**
	 * To verify click in edit link
	 * @param index - 
	 * @return Quickshop object
	 * @throws Exception - Exception
	 */
	public void clickAddtoBagByIndex(int index) throws Exception{
		Utils.waitForElement(driver, btnAddtoBag.get(index));	
		BrowserActions.clickOnElementX(btnAddtoBag.get(index), driver, "Add to Bag");
		Utils.waitForElement(driver, btnCheckoutInMCOverlay, 5);
		Utils.waitForPageLoad(driver);
	}
	
	 /**
     * To click on checkout button in ATB overlay
     *
     * @return ShoppingBagPage -
     * @throws Exception -
     */
    public ShoppingBagPage clickOnCheckoutInMCOverlay() throws Exception {
        Utils.waitForElement(driver, btnCheckoutInMCOverlay);
        BrowserActions.clickOnElementX(btnCheckoutInMCOverlay, driver, "Checkout Button in Mini Cart Overlay ");
        Utils.waitForPageLoad(driver);

        return new ShoppingBagPage(driver).get();
    }
	
	
	/**
	 * To verify click in edit link
	 * @param index - 
	 * @return Quickshop object
	 * @throws Exception - Exception
	 */
	public Object clickEditLinkTabletMobile(int index) throws Exception{
		Utils.waitForPageLoad(driver);	
		Utils.waitForElement(driver, editLinkMobile.get(index));	
		BrowserActions.clickOnElementX(editLinkMobile.get(index), driver, "Edit Link");
		if(!Utils.waitForElement(driver, pdpMain)) {
			Log.failsoft("Page not navigated to PDP. SM-612");
			driver.navigate().back();
			return new WishListPage(driver).get();
		}
		return new PdpPage(driver).get();
	}
	
	/**
	 * To verify click in remove link
	 * @param index - 
	 * @throws Exception - Exception
	 */
	public void clickRemoveLink(int index) throws Exception{
		Utils.waitForPageLoad(driver);	
		Utils.waitForElement(driver, removelink.get(index));	
		BrowserActions.clickOnElementX(removelink.get(index), driver, "Remove Link");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To remove all items from wishlist page
	 * @throws Exception - Exception
	 */
	public void removeAllProduct() throws Exception{
		while (removelink.size() > 0) {
			WebElement removeLink = driver.findElement(By.cssSelector("button[name='dwfrm_wishlist_items_i0_deleteItem']"));
			BrowserActions.clickOnElementX(removeLink, driver, "Remove link");
			Utils.waitForPageLoad(driver);
		}
	}
	
	/**
	 * To verify and validating the number of items
	 * @param itemname - 
	 * @return items count as integer
	 * @throws Exception - Exception
	 */
	public int validateNoOfItem(String itemname) throws Exception{
		Utils.waitForPageLoad(driver);
		List<WebElement> items=driver.findElements(By.cssSelector(".item-image>a[title='"+itemname+"']"));
		
		return items.size();
	}
	
	/**
	 * To select a different size of selected item
	 * @return int - number of items in wishlist
	 * @throws Exception - Exception
	 */
	public int getItemCount() throws Exception{
		return wishListItemList.size();
	}
	
	/**
	 * To select a different size of selected item
	 * @throws Exception - Exception
	 */
	public boolean selectSizeInModal() throws Exception{
		if(Utils.waitForElement(driver, sizeOption)) {
			if(sizeOptions.size() == 1) {
				Log.message("Only one size available.");
			}
			else {
				BrowserActions.clickOnElementX(sizeUnselected, driver, "Unselected Size");
				Log.message("Different size selected.");
				Utils.waitForPageLoad(driver);
			}
			return true;
		}
		else {
			Log.failsoft("Wishlist edit modal shows empty. SM-612.");
			clickCloseProductEditModal();
			return false;
		}
	}
	
	/**
	 * To get selected size of a product
	 * @param index - Product index
	 * @return SelectedSize - Size selected
	 * @throws Exception - Exception
	 */
	public String getSelectedSize(int index) throws Exception {
		return BrowserActions.getText(driver, sizeSelected.get(index), "Unselected Size");
	}
	
	/**
	 * To update wishlist item
	 * @throws Exception - Exception
	 */
	public void clickUpdateOnModal() throws Exception{
		BrowserActions.clickOnElementX(modalWishlistUpdate, driver, "Update current wishlist item");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on Email share
	 * @throws Exception - Exception
	 */
	public void clickOnEmailShare() throws Exception{
		BrowserActions.clickOnElementX(emailShare, driver, "Email share");
		Utils.waitForElement(driver, winEmailShare);
	}
	
	/**
	 * To add updated item to wishlist
	 * @throws Exception - Exception
	 */
	public void clickAddWishlistModal() throws Exception{
		BrowserActions.clickOnElementX(modalAddToWishlist, driver, "Add selected variant to wishlist");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To verify the validate number of items is available
	 * @param itemname - 
	 * @return Status Boolean
	 * @throws Exception - Exception
	 */
	public boolean validateItemNoLongerAvailable(String itemname) throws Exception{
		Utils.waitForPageLoad(driver);
		List<WebElement> items=driver.findElements(By.cssSelector(".item-details"));
		List<WebElement> itemnames=null;
		int index=0;
		boolean flag=false;
		for (int i=0;i<items.size();i++)
		{
			itemnames=items.get(i).findElement(By.cssSelector(".name>a"));
			if(itemnames.get(i).getAttribute("innerHTML").equals(itemname))
			{
				index=i;
			}
		}
		
		WebElement availability=items.get(index).findElement(By.cssSelector(".item-availability.hide-mobile .value.notavailable"));
		if(availability.getText().trim().equals("Out of Stock")&&editlink.size()<items.size()&&addtobag.size()<items.size())
		{
			flag=true;
		}
		
		return flag;
	}
	
	/**
	 * To close wishlist item edit modal
	 * @throws Exception - Exception
	 */
	public void clickCloseProductEditModal() throws Exception{
		BrowserActions.clickOnElementX(btnCloseEditModal, driver, "Close Product Edit Modal.");
		Utils.waitForPageLoad(driver);

	}
	
	/**
	 * To view friend's public wishlist
	 * @throws Exception - Exception
	 */
	public void clickView() throws Exception{
		BrowserActions.clickOnElementX(btnViewFriendWishlist, driver, "Clicked on view button");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To get the list of product id from the wish-list page line items
	 * @return List<String> - List<productId>
	 * @throws Exception
	 */
	public List<String> getProductIdList() throws Exception {
		List<String> productId = new ArrayList<String>();
		String prdId = null;
		for(int i=0 ; i < wishListItemList.size() ; i++) {
			WebElement element = wishListItemList.get(i);
			prdId = BrowserActions.getTextFromAttribute(driver, element, "data-itemid", "product");
			productId.add(prdId);
		}
		return productId;
	}
	
	/**
	 * To get product Brand from wish-list line items based on given product ID
	 * @param prdId - Product ID
	 * @return String of Current Brand short name
	 * @throws Exception - Exception
	 */
	public Brand getProductBrand(String prdId) throws Exception {
		String productId = null;
		String prdURL = null;
		for(int i=0 ; i < wishListItemList.size() ; i++) {
			WebElement element = wishListItemList.get(i);
			productId = BrowserActions.getTextFromAttribute(driver, element, "data-itemid", "product");
			if(productId.equalsIgnoreCase(prdId)) {
				prdURL = element.findElement(By.cssSelector(".item-image>a")).getAttribute("href");
				break;
			}
		}
		prdURL = UrlUtils.getSharedCartRedirectURL(prdURL);
		return UrlUtils.getBrandFromUrl(prdURL);
	}
	
	/**
	 * To get the Product ID and Brand Name from wish-list line items.
	 * @return HashMap<String, Brand> - HashMap<productId, brandName> of wish-list line items
	 * @throws Exception
	 */
	public HashMap<String, Brand> getProductIdAndBrandName() throws Exception {
		HashMap<String, Brand> productDetails = new HashMap<String, Brand>();
		String productId = null;
		Brand brandName = null;
		for(int i=0 ; i < wishListItemList.size() ; i++) {
			WebElement prdId = wishListItemList.get(i);
			productId = BrowserActions.getTextFromAttribute(driver, prdId, "data-itemid", "product");
			brandName = getProductBrand(productId);
			productDetails.put(productId, brandName);
		}
		return productDetails;
	}
	
	/**
	 * To get the Product variation ID and Brand Name from wish-list line items.
	 * @return HashMap<String, Brand> - HashMap<productId, brandName> of wish-list line items
	 * @throws Exception
	 */
	public HashMap<String, Brand> getProductVariationAndBrandName() throws Exception {
		HashMap<String, Brand> productDetails = new HashMap<String, Brand>();
		String productVariation = null;
		Brand brandName = null;
		for(int i=0 ; i < wishListItemList.size() ; i++) {
			WebElement wislistItem = wishListItemList.get(i).findElement(By.cssSelector(".item-image>a"));;
			String prdURL = wislistItem.getAttribute("href");
			prdURL = UrlUtils.getSharedCartRedirectURL(prdURL);
			productVariation = UrlUtils.getProductIDFromURL(prdURL);
			brandName = UrlUtils.getBrandFromUrl(prdURL);
			Log.event(productVariation + ":" + brandName.toString());
			productDetails.put(productVariation, brandName);
		}
		return productDetails;
	}
	
	/**
	 * To add all wishlist items to cart
	 * @throws Exception
	 */
	public void addAllToCart() throws Exception {
		for(int i=0 ; i < wishListItemList.size() ; i++) {
			WebElement itemAddToBag = wishListItemList.get(i).findElement(By.cssSelector(".add-to-cart"));
			BrowserActions.clickOnElementX(itemAddToBag, driver, "add to bag for item #"+i);
			if(Utils.waitForElement(driver, mdlAddToCartPopup)) {
				BrowserActions.clickOnElementX(mdlContinueShopping, driver, "Continue Shopping button");
			}
		}
	}
	
	 /**
     * To verify Edit link is gone
     * @return editLinkStatus - True if Edit link is not displayed in wishlist else false
     * @throws Exception - Exception
     */
    public boolean verifyEditLinkNotDisplayed() throws Exception{
        boolean editLinkStatus = false;
        String currentBrand = Utils.getCurrentBrandShort();
        if(!envProperty.get("enabledSharedCart").contains(currentBrand)) {
            Log.failsoft(currentBrand + " is not yet included in Shared Cart.");
        } else if(!BrowserActions.VerifyElementDisplayed(driver, editlink)) {
            editLinkStatus = true;
        }
        return editLinkStatus;
    }
    
    /**
     * To verify products added to wishlist
     * @param prdNameToVerify - List of product names to verify
     * @return Boolean - true if product added to wislist else false
     * @throws Exception - Exception
     */
    public boolean verifyProductAddedToWishlist(List<String> prdNameToVerify) throws Exception {
    	boolean productDisplayStatus = false;
    	List<String> wishlistProductName = new ArrayList<String>();
    	for(WebElement elem : lstWishListItemName) {
    		String name = BrowserActions.getText(driver, elem, "Wishlist product name");
    		wishlistProductName.add(name.toLowerCase());
    	}
    	for(String prdName : prdNameToVerify) {
    		if(wishlistProductName.contains(prdName.toLowerCase())) {
    			productDisplayStatus = true;
    		} else {
    			productDisplayStatus = false;
    			Log.message(prdName + " is not displayed in mini cart ocerlay");
    			break;
    		}
    	}
    	return productDisplayStatus;
    }
    
    /**
     * To click add-to-bag button in wish-list page based on given Product ID
     * @param productID - product ID to click.
     * @throws Exception - Exception
     */
    public void clickAddtoBagByPrdID(String productID) throws Exception {
    	WebElement element = null;
    	String prdID = null;
    	for(WebElement prdList : wishListItemList) {
    		prdID = BrowserActions.getTextFromAttribute(driver, prdList, "data-itemid", "product");
    		if(prdID.equalsIgnoreCase(productID)) {
    			element = prdList.findElement(By.cssSelector(".product-add-to-cart button"));
    			BrowserActions.clickOnElementX(element, driver, "Add to Bag");
    			break;
    		}
    	}
    	Utils.waitForElement(driver, btnCheckoutInMCOverlay, 5);
    	Utils.waitForPageLoad(driver);
    }

    /**
     * To click on the product name based on given product ID
     * @param productID - String
     * @return PdpPage
     * @throws Exception 
     */
	public PdpPage clickOnproductNameByPrdID(String productID) throws Exception {
		WebElement element = null;
		String prdID = null;
		for(WebElement prdList : wishListItemList) {
			prdID = BrowserActions.getTextFromAttribute(driver, prdList, "data-itemid", "product");
			if(prdID.equalsIgnoreCase(productID)) {
				element = prdList.findElement(By.cssSelector(".name a"));
				BrowserActions.clickOnElementX(element, driver, "wishlist product name");
				break;
			}
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}
	
	/**
	 * To click on the product image based on given product ID
	 * @param productID - String
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage clickOnproductImageByPrdID(String productID) throws Exception {
		WebElement element = null;
		String prdID = null;
		for(WebElement prdList : wishListItemList) {
			prdID = BrowserActions.getTextFromAttribute(driver, prdList, "data-itemid", "product");
			if(prdID.equalsIgnoreCase(productID)) {
				element = prdList.findElement(By.cssSelector(".item-image a img"));
				BrowserActions.clickOnElementX(element, driver, "wishlist product image");
				break;
			}
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}
}