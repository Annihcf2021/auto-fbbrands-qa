package com.fbb.pages.account;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.PdpPage;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class MailDropPage extends  LoadableComponent <MailDropPage> {
	
	private String appURL;
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");
	private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
	
	
	@FindBy(css = "div.page.page-home")
	WebElement readyElement;
	
	@FindBy(css = ".home-top-container .inboxform-input input")
	WebElement dropmailSearch;
	
	@FindBy(css = ".messagelist-container div a.messagelist-row-link")
	WebElement firstdropMail;
	
	@FindBy(css = ".messagelist-container div a.messagelist-row-link")
	List<WebElement> ListdropMail;
	
	@FindBy(css= ".inboxheader-container button span")
	WebElement reLoadDropInbox;
	
	@FindBy(xpath= "//a[contains(text(), 'RESET MY PASSWORD')]")
	WebElement lnkResetPassword;
	
	@FindBy(xpath= "//a[contains(text(),'Click here')]")
	WebElement linkHereResetMyPassword;
	
	@FindBy(css= ".messagedata-iframe")
	WebElement iframeMsgBody;
	
	@FindBy(xpath = "//a[contains(text(),'View in Browser')]")
	WebElement viewInBrowser;
	
	@FindBy(css = "img[src*='Logo']")
	WebElement brandLogoImg;
	
	@FindBy(xpath = "//img[contains(@src,'Logo')] //ancestor:: a")
	WebElement brandLogoLinkAll;
	
	@FindBy(xpath = "//td[contains(text(), 'OFF')]//ancestor::td[@class='padLR15']//tbody")
	WebElement topPromoBanner;
	
	@FindBy(xpath = "//a[contains(text(), 'CODE')]")
	WebElement topPromoBannerLink;
	
	@FindBy(xpath = "//td[contains(text(),'Thank you for your order')]")
	WebElement thankYouHeader;
	
	@FindBy(xpath = "//td //strong[contains(text(),'Hi')]")
	WebElement hiUserName;
	
	@FindBy(xpath = "//strong[contains(text(),'Hi')] //ancestor::td[contains(@class, 'padLR15')]")
	WebElement orderInformationMsg;
	
	@FindBy(xpath = "//td //strong[contains(text(),'Order Number')]")
	WebElement orderNumber;
	
	@FindBy(xpath = "//span[contains(text(),'Shipping Address')]")
	WebElement shippingAddress;
	
	@FindBy(xpath = "//span[contains(text(),'Billing Address')]")
	WebElement billingAddress;
	
	@FindBy(xpath = "//td //strong[contains(text(),'Shipping Method')]")
	WebElement shippingMethod;
	
	@FindBy(xpath = "//td //strong[contains(text(),'Payment')]")
	WebElement paymentMethod;
	
	@FindBy(xpath = "//td //strong[contains(text(),'Gift Card')]")
	WebElement giftCard;
	
	@FindBy(xpath = "//td //strong[contains(text(),'Reward Certificate')]")
	WebElement rewardCertificate;
	
	@FindBy(xpath = "//td //strong[contains(text(),'Promotions')]")
	WebElement promotions;
	
	@FindBy(xpath = "//td[contains(text(),'Merchandise Subtotal')]")
	WebElement merchandiseSubtotalText;
	
	@FindBy(xpath = "//td[contains(text(),'Shipping & Handling')]")
	WebElement shippingAndHandlingText;
	
	@FindBy(xpath = "//td[contains(text(),'Shipping & Handling Surcharge')]")
	WebElement shippingSurchargeText;
	
	@FindBy(xpath = "//td[contains(text(),'Estimated Tax')]")
	WebElement estimatedTaxText;
	
	@FindBy(xpath = "//td[contains(text(),'Your Subtotal')]")
	WebElement subtotalText;
	
	@FindBy(xpath = "//td[contains(text(),'Your Order Total')]")
	WebElement TotalText;
	
	@FindBy(xpath = "//a[contains(text(),'ORDER STATUS')]")
	WebElement orderStatusLink;
	
	@FindBy(xpath = "//a[contains(text(),'BILLING & PAYMENT')]")
	WebElement billingPaymentLink;
	
	@FindBy(xpath = "//a[contains(text(),'RETURNS & EXCHANGES')]")
	WebElement retrunAndExchangeLink;
	
	@FindBy(xpath = "//a[contains(text(),'Click here to enroll')]")
	WebElement enrollLink;
	
	@FindBy(css = ".fxg-header__logo")
	WebElement fedExLogo;
	
	@FindBy(xpath = "//span[contains(text(),'Apply')]//ancestor::a")
	WebElement applyLink;
	
	@FindBy(xpath = "//span[contains(text(),'Apply')]//ancestor::a//ancestor::td[@class='padLR15']")
	WebElement plccBanner;
	
	@FindBy(xpath = "//img[contains(@src,'TopSellerHorizontal')]//ancestor:: a")
	WebElement recommendationProductLink;
	
	@FindBy(xpath = "//th[contains(@class, 'displayBlock')][contains(@class, 'padTB15')]")
	WebElement categoryHeader;
	
	@FindBy(xpath = "//th[contains(@class, 'displayBlock')][contains(@class, 'padTB15')] //a")
	WebElement categoryHeaderLink;
	
	@FindBy(xpath = "//span[contains(text(),'Email Address')]//ancestor::a")
	WebElement linkConfirmEmailAddress;
	
	@FindBy(xpath = "//a[contains(text(),'Email Address')]")   
	WebElement linkAlternateConfirmEmailAddress;
	
	
	
	/**
	 * constructor of the class
	 * @param drive : Webdriver
	 */
	public MailDropPage(WebDriver driver) {
		appURL = redirectData.get("homePage_MailDrop");
		
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);	
	}
	
	@Override
	protected void isLoaded() {
		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("MailDrop Page did not open up. Site might be down.", driver);
		} else {
			Log.event("MailDrop Page loaded successfully");
		}
		elementLayer = new ElementLayer(driver);
	}
	
	@Override
	protected void load() {
		driver.get(appURL);
		
		if(Utils.waitForElement(driver, readyElement)){
			isPageLoaded = true;
		}		
		Utils.waitForPageLoad(driver);
	}
	
	
	/**
	 * Navigate to User mail box in Drop Mail
	 * @param userEmail
	 * @throws Exception
	 */
	public void navigateToDropMailBox(String userEmail) throws Exception {
		try {
			Utils.waitForElement(driver, dropmailSearch);
			if(userEmail.contains("@")) {
				userEmail = userEmail.split("@")[0];
			}
			BrowserActions.typeOnTextField(dropmailSearch, userEmail, driver, "mail search field");
			Log.event("--->>>Typed Usr name :: " + userEmail);
			BrowserActions.clickByPressEnter(dropmailSearch, driver, "tap enter from mail search field");
			Log.event("--->>>Clicked search by enter :: " + userEmail);
			Utils.waitForPageLoad(driver);
		} catch (Exception ex) {
			Log.failsoft("Element not present in the page");
		}
	}
	
	/**
	 * Open respective received method mail 
	 * @param emailType - Type of the email(Eg: Order, Password)
	 * @param oldEmailCount - Count of old email's from the email address
	 * @param description - description of the email type
	 * @throws Exception - Exception
	 */
	public void openReceivedMail(String emailType, int oldEmailCount, String description) throws Exception {
		boolean emailAvailability = false, newEmail = true;
		String currentBrand  = BrandUtils.getBrandFullName().toLowerCase();
		List<WebElement> emailList = ListdropMail;
		
		newEmail = newMailAvailability(oldEmailCount, emailType);
		
		try {
			if(newEmail) {
				for(int i=0; i<emailList.size(); i++) {
					WebElement emailSubject = emailList.get(i).findElement(By.cssSelector(".messagelist-subject"));
					WebElement emailBrand = emailList.get(i).findElement(By.cssSelector(".messagelist-from"));
					
					String emailHeading = BrowserActions.getText(driver, emailSubject, "email subject");
					String emailRecBrand = BrowserActions.getText(driver, emailBrand, "email brand");
					
					if(emailHeading.toLowerCase().contains(emailType) && emailRecBrand.toLowerCase().contains(currentBrand.substring(0,4))) {				
						emailAvailability = true;
						Log.event(description + " is available in the email list");
						BrowserActions.clickOnElementX(emailList.get(i), driver, "respective Email");
						Utils.waitForPageLoad(driver);
						break;
					}
				}
			}		
			
			if(!(emailAvailability && newEmail) ) {
					Log.fail(description + " is not available in the email list, hence can't proceed further");
			}
				
		} catch (Exception ex) {
			Log.failsoft("Element not present in the page");
		}		
	}
	
	/**
	 * To Reload the page to get new Email
	 * @throws Exception
	 */
	public void reLoadDropMail() throws Exception {
		BrowserActions.clickOnElementX(reLoadDropInbox, driver, "Reload drop mail");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To check new mail availability
	 * @param oldEmailCount - Previous inbox count
	 * @param emailType - Type of email to be received
	 * @return boolean - True if email is received else false
	 * @throws Exception
	 */
	public boolean newMailAvailability(int oldEmailCount, String emailType) throws Exception {	
		Long sleepTime = (configProperty.getProperty("sleepTimeForNewEmail") == null) ? 15000 : Long.parseLong(configProperty.getProperty("sleepTimeForNewEmail"));
		int newEmailcount;
		for(int i=0; i<9; i++) {
			reLoadDropMail();
			newEmailcount = mailAvailabilityCount();
			if(newEmailcount > oldEmailCount) {
				for(int j=0; j < (newEmailcount-oldEmailCount); j++) {
					String emailHeading = ListdropMail.get(j).findElement(By.cssSelector(".messagelist-subject")).getText();
					if(emailHeading.toLowerCase().contains(emailType)) {
						return true;
					}
				}
			}
			Thread.sleep(sleepTime);
		}
		return false;
	}
	
	/**
	 * To get count of available email
	 * @return boolean - True if email is received else false
	 * @throws Exception
	 */
	public int mailAvailabilityCount() throws Exception {
		return ListdropMail.size();
	}
	
	/**
	 * To navigate specific frame in the MailDrop
	 * @param element - Element to switch frame
	 * @throws Exception
	 */	
	private void navigateToSpecificFrame(WebElement element) throws Exception {
		try {
			Utils.waitForElement(driver, element);
			driver.switchTo().parentFrame();
			driver.switchTo().frame(element);
		} catch (Exception e) {
			Log.event("IFrame not available");
		}		
	}
	
	/**
	 * verify vertical elements is displayed.
	 * @param expectedElements -  List of element to check
	 * @param obj
	 * @return Boolean - true when element is displayed else false
	 * @throws Exception - Exception
	 */
	public boolean verifyElementDisplayed(List<String> expectedElements, Object obj) throws Exception {
		navigateToSpecificFrame(iframeMsgBody);	
		return elementLayer.verifyElementDisplayed(expectedElements, obj);
	}
	
	/**
	 * verify vertical alignment of elements.
	 * @param aboveElement - Element in the above position
	 * @param belowElement - Element in the below position
	 * @param obj
	 * @return Boolean - true when condition matches, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyVerticalAllignment(WebDriver driver, String aboveElement, String belowElement, Object obj) throws Exception {
		navigateToSpecificFrame(iframeMsgBody);	
		return elementLayer.verifyVerticalAllignmentOfElements(driver, aboveElement, belowElement, obj);
	}
	
	/**
	 * verify horizontal alignment of elements.
	 * @param rightElement - Element in the right position
	 * @param leftElement - Element in the left position
	 * @param obj
	 * @return Boolean - true when condition matches, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyHorizontalAllignment(WebDriver driver, String rightElement, String leftElement, Object obj) throws Exception {
		navigateToSpecificFrame(iframeMsgBody);	
		return elementLayer.verifyHorizontalAllignmentOfElements(driver, rightElement, leftElement, obj);
	}
	
	/**
	 * To click on "Reset My Password" button in Forgot password request mail.
	 * @return PasswordResetPage - Object of PasswordResetPage
	 * @throws Exception - Exception
	 */
	public PasswordResetPage clickResetMyPasswordInEmail() throws Exception {
		navigateToSpecificFrame(iframeMsgBody);
		if(Utils.waitForElement(driver, lnkResetPassword)) {
			BrowserActions.scrollInToView(lnkResetPassword, driver);
			BrowserActions.clickOnElementX(lnkResetPassword, driver, "Reset My Password button");
		} else {
			Log.ticket("SC-1923");
			BrowserActions.scrollInToView(linkHereResetMyPassword, driver);
			BrowserActions.clickOnElementX(linkHereResetMyPassword, driver, "Click Here link");
		}
		List<String> handle = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(handle.get(handle.size() - 1));
        return new PasswordResetPage(driver).get();
	}
	
	/**
	 * To get link and open in new tab
	 * @param link - WebElement to get link
	 * @throws Exception - Exception
	 */
	public void openLinksInNewTab(WebElement link) throws Exception {
		navigateToSpecificFrame(iframeMsgBody);
		try {
			Utils.waitForElement(driver, link);
		} catch (Exception e) {
			Log.failsoft("Element not present in the page");
		}
		
		String redirectLink = BrowserActions.getTextFromAttribute(driver, link, "href", "redirectLink");
		String redirectUrl;
		try {
			redirectUrl = redirectLink.split("//")[2];
		} catch (Exception e) {
			redirectUrl = redirectLink.split("//")[1];
		}
		Log.event("--->>>Clicking on:: " + redirectUrl);
		Utils.openNewTab(driver, "https://" + redirectUrl);
		
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * verify view in browser link in order confirmation mail
	 * @return Boolean - true when Browser link redirected, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyViewBrowserLink() throws Exception {
		openLinksInNewTab(viewInBrowser);
		Utils.waitForPageLoad(driver);
		
		List<String> handle = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(handle.get(handle.size() - 1));
		
		try {
			Utils.waitForElement(driver, thankYouHeader);
			driver.close();
			driver.switchTo().window(handle.get(0));
			return true;
		} catch (Exception e) {
			Log.failsoft("The order confirmation email is not displayed in browser");
		}
		driver.close();
		driver.switchTo().window(handle.get(0));
		return false;
	}
	
	/**
	 * verify selected link in order confirmation mail
	 * @param WebElement - WebElement of the link to verify
	 * @param description - Description of the element
	 * @return Boolean - true when the selected link redirected, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyLinkRedirectedRespectiveBrand(String WebElement, String description) throws Exception {
		boolean OrderStatusPageNavigated = false;
		WebElement element = ElementLayer.getElement(WebElement, this);
		try {
			openLinksInNewTab(element);
			Utils.waitForPageLoad(driver);
			
			List<String> handle = new ArrayList<String> (driver.getWindowHandles());
			driver.switchTo().window(handle.get(handle.size() - 1));
			String title = driver.getTitle().toLowerCase();
			
			if (title.contains(BrandUtils.getBrandFullName().substring(0, 4).toLowerCase())) {
				Log.event("--->>>Navigated to brand site");
				OrderStatusPageNavigated = true;
			}
			driver.close();
			driver.switchTo().window(handle.get(0));
		} catch(Exception e) {
			Log.failsoft(description + " link not found.");
		}
		return OrderStatusPageNavigated;
	}
	
	/**
	 * verify Enroll link in order confirmation mail
	 * @return Boolean - true when Enroll link redirected, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyEnrollLink() throws Exception {
		boolean fedExPageNavigated = false;
		try {
			openLinksInNewTab(enrollLink);
			Utils.waitForPageLoad(driver);
			
			List<String> handle = new ArrayList<String> (driver.getWindowHandles());
			driver.switchTo().window(handle.get(handle.size() - 1));
			if(fedExLogo.isDisplayed()) {
				Log.event("--->>>Navigated to appropriate site");
				fedExPageNavigated = true;
			}
			driver.close();
			driver.switchTo().window(handle.get(0));
		} catch(Exception e) {
			Log.failsoft("Enroll link not found.");
		}
		return fedExPageNavigated;
	}
	
	/**
	 * verify products details are displayed in Order mail properly.
	 * @param prdDetails - Product details to verify
	 * @param obj
	 * @return Boolean - true if all attributes displayed properly, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyProductDetailsDisplayed(WebDriver driver, LinkedList<LinkedHashMap<String, String>> prdDetails, Object obj) throws Exception {
		Boolean result = false;
		navigateToSpecificFrame(iframeMsgBody);
		LinkedHashMap<String, String> productInfo = new LinkedHashMap<String, String>();
		for(int i = 0; i < prdDetails.size(); i++) {
			productInfo = prdDetails.get(i);
			try {
				WebElement prdName = driver.findElement(By.xpath("//span[contains(text(),'"+productInfo.get("Name")+"')]"));
				String prdInfoMail = prdName.findElement(By.xpath(" //ancestor::td[contains(@class, 'pad_L15')] /..")).getText();
			
				if(prdInfoMail.contains(productInfo.get("Name"))) {
					 if(prdInfoMail.contains(productInfo.get("Id").split("#")[1])) {
						 if(prdInfoMail.contains(productInfo.get("Color"))) {
							 if(prdInfoMail.contains(productInfo.get("Size"))) {
								 if(prdInfoMail.contains(productInfo.get("Quantity"))) {
									 if(prdInfoMail.contains(productInfo.get("Price"))) {
										 result = true;
										 Log.event("Product details are displayed properly for '"+ (i+1) +"' product");
									 } else { Log.event("Product price is mismatching for '"+ (i+1) +"' product"); }
								 } else { Log.event("Product quantity is mismatching for '"+ (i+1) +"' product"); }
							 } else { Log.event("Product size is mismatching for '"+ (i+1) +"' product"); }
						 } else { Log.event("Product color is mismatching for '"+ (i+1) +"' product"); }
					 } else { Log.event("Product Id is mismatching for '"+ (i+1) +"' product"); }
				} else { Log.event("Product name is mismatching for '"+ (i+1) +"' product"); }
			} catch(NoSuchElementException e) {
				Log.event("Product details are not displayed for '"+ (i+1) +"' product");
			}
		} 
		return result;
	}
	
	/**
	 * To click on "Confirm email address" button in Review Confirmation mail
	 * @return PdpPage - Object of the PDP page
	 * @throws Exception - Exception
	 */
	public PdpPage clickReviewConfirmEmailAddress() throws Exception {
		navigateToSpecificFrame(iframeMsgBody);
		WebElement confirmEmailAddress;
		if(Utils.waitForElement(driver, linkConfirmEmailAddress)) {
			confirmEmailAddress = linkConfirmEmailAddress;
		} else {
			confirmEmailAddress = linkAlternateConfirmEmailAddress;
		}
        BrowserActions.scrollInToView(confirmEmailAddress, driver);
        BrowserActions.clickOnElementX(confirmEmailAddress, driver, "Confirm Email Address button");
        Utils.waitForPageLoad(driver);
        List<String> handle = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(handle.get(handle.size() - 1));
		return new PdpPage(driver).get();
	}
}
