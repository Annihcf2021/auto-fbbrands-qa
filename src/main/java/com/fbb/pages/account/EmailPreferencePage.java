package com.fbb.pages.account;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.pages.customerservice.EmailSubscriptionThankyouPage;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class EmailPreferencePage extends LoadableComponent <EmailPreferencePage>{
	
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	/**********************************************************************************************
	 ********************************* WebElements of EmailPreferencePage ***********************************
	 **********************************************************************************************/

	
	private static final String MyAccountPageLinksNavigation = ".navigation-links-row";
	
	@FindBy(css = MyAccountPageLinksNavigation + " .email-preferences a[style*='cursor']")
	WebElement lnkEmailPrefAfterNavigate;
	
	@FindBy(css = ".pt_account.email-subscription")
	WebElement readyElement;
	
	@FindBy(css = ".email-thankyou .html-slot-container h1.offer ")
	WebElement thankyouMessage;
	
	@FindBy(css = ".menu-category")
	WebElement globalNavigation;
	
	@FindBy(css = ".promo-banner")
	WebElement promoBanner;
	
	@FindBy(css = ".main")
	WebElement myAccountHeader;
	
	@FindBy(css = ".email-preferences .top-content h1")
	WebElement lblEmailPref;
	
	@FindBy(css = ".email-benefits")
	WebElement lblEmailBenefits;
	
	@FindBy(css = ".email-currentaddress")
	WebElement lblEmailSubPref;
	
	@FindBy(css = ".email-edit")
	WebElement lblCurrentEmail;
	
	@FindBy(css = ".email-currentaddress")
	WebElement lblCurrentEmailMessage;
	
	@FindBy(css = ".email-edit a.edit-click")
	WebElement lnkEditCurrentEmail;
	
	@FindBy(css = "a.edit-click img")
	WebElement lnkEditCurrentEmailIcon;
	
	@FindBy(css = ".interest .edit-click a")
	WebElement lnkEditPreferences;

	@FindBy(css = ".currentsite-brand .brand-catalog-image.hide-mobile .content-asset img")
	WebElement imgBrandDesktop;
	
	@FindBy(css = ".sisterbrands")
	WebElement sisterBrands;
	
	@FindBy(css = ".custom-slider-checkbox .slider-label")
	List<WebElement> lblSisterBrandSubscribe;
	
	@FindBy(css = ".sisterbrands .email-preference-brand.hide-mobile img")
	List<WebElement> imgSisterBrand;
	
	@FindBy(css = ".sisterbrands .only-for-mobile.email-preference-brand img")
	List<WebElement> imgSisterBrandMobile;
	
	@FindBy(css = ".sisterbrands .email-preference-brand.hide-mobile img")
	WebElement imgSisterBrandSingle;
	
	@FindBy(css = ".email-description:not(.email-benefits) p")
	WebElement lblSisterBrandDesc;
	
	@FindBy(css = ".sisterbrands .only-for-mobile.email-preference-brand img")
	WebElement imgSisterBrandSingleMobile;
	
	@FindBy(css = ".otherbrands-divider")
	WebElement sectionBrandDivider;
	
	@FindBy(css = ".email-info")
	WebElement lblEmailInfo;
	
	@FindBy(css = ".custom-slider-checkbox .slider.round")
	List<WebElement> btnSubscribeAllBrands;
	
	@FindBy(css = ".sisterbrands .brand-catalog-image.hide-mobile .content-asset p img")
	List<WebElement> imgAllBrandsDesktop;

	@FindBy(css = ".currentsite-brand .brand-catalog-image:not(.hide-mobile) .content-asset img")
	WebElement imgBrandMobile;
	
	@FindBy(css = "div[class ='sisterbrands'] div[class='brand-catalog-image'] .content-asset p img")
	List<WebElement> imgAllBrandsMobile;

	@FindBy(css = ".currentsite-brand .email-preference-brand.hide-mobile img")
	WebElement imgBrandNameDesktop;
	
	@FindBy(css = ".email-preference-brand.hide-mobile img")
	List<WebElement> imgAllBrandNamesDesktop;
	
	@FindBy(css = ".brand-catalog-image.hide-mobile img")
	List<WebElement> imgAllBrandImageDesktop;
	
	@FindBy(css = ".currentsite-brand .only-for-mobile.email-preference-brand img")
	WebElement imgBrandNameMobile;
	
	@FindBy(css = ".only-for-mobile.email-preference-brand img")
	List<WebElement> imgAllBrandNamesMobile;
	
	@FindBy(css = ".brand-catalog-image:not(.hide-mobile) .content-asset img")
	List<WebElement> imgAllBrandImageMobile;

	@FindBy(css = ".email-description .left-text .content-asset")
	List<WebElement> txtAllBrandsDescripton;
	
	@FindBy(css = ".slider")
	List<WebElement> sliderAllBrand;

	@FindBy(css = ".email-benefits.email-description")
    WebElement txtEmailBenefits;
	
	@FindBy(css = ".preference-options")
	WebElement divPreferenceOptions;
	
	@FindBy(css = ".secondary-navigation-links")
	WebElement divSecondaryOption;
	
	@FindBy(css = ".custom-slider-checkbox .slider.round")
	WebElement btnSubscribe;
	
	@FindBy(css = ".otherbrands-divider")
	WebElement txtOtherBrandsCopy;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
	WebElement txtBreadCrumbCurrentElementDesktop;
	
	@FindBy(css = ".breadcrumb-element.hide-mobile")
	List<WebElement> lstBreadCrumbElementsDesktop;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement txtBreadCrumbElementMobile;
	
	@FindBy(css = ".hide-desktop.hide-tablet.back-arrow")
	WebElement imgBreadCrumbBackArrowMobile;
	
	@FindBy(css = ".email-preferences a")
	WebElement lnkEmailPref;
	
	@FindBy(css = ".switch")
	WebElement chkSubscribeCustomSlider;
	
	@FindBy(css = "div.email-unsubscription-section")
	WebElement emailUnSubscribePopup;
	
	@FindBy(css = ".unsubscribe-email-radio .custom-radio-icon")
	WebElement chkUnsubscribe;
	
	@FindBy(css = ".unsubscription-reasoncode .custom-radio-icon")
	WebElement chkReasonForUnsubscribe;
	
	@FindBy(css = ".unsubscribe-email .subscribe-action > button")
	WebElement btnReasonForUnsubscribeSubmit;
	
	@FindBy(css = ".currentsite-brand .slider")
	WebElement toggleCurrentBrandEmailSubscription;
	
	@FindBy(css = ".currentsite-brand .subscribe-action .emailsubscribe")
	WebElement btnCurrentBrandEmailSubscribe;
	
	@FindBy(css = ".currentsite-brand .custom-slider-checkbox[aria-checked='false'] .slider")
	WebElement toggleCurrentBrandEmailSubscribe;
	
	@FindBy(css = ".currentsite-brand .custom-slider-checkbox[aria-checked='true'] .slider")
	WebElement btnUnSubscribeCurrentbrand;
	
	@FindBy(css = ".email-unsubscription-section")
	WebElement modalEmailFrequency;
	
	@FindBy(css = ".modal-msg")
	WebElement modalHeader;
	
	@FindBy(css = ".emailfrequencyform")
	WebElement divFrequencyOptions;
	
	@FindBy(css = ".fewercontactsrequested-radio .custom-radio-icon")
	WebElement radioFewerContact;
	
	@FindBy(css = ".select-unsubscribe-reason")
	WebElement divUnsubscribeReasons;
	
	@FindBy(css = ".ui-dialog-titlebar-close")
	WebElement btnCloseModal;
	
	@FindBy(css = ".unsubscription-reasoncode .custom-radio-icon")
	List<WebElement> lstUnsubscribeReasons;
	
	@FindBy(css = ".other-reason-for-unsubscription .custom-radio-icon")
	WebElement radioUnsubOtherReason;
	
	@FindBy(css = ".other-reason-textbox textarea")
	WebElement txtOtherReason;
	
	@FindBy(css = ".currentsite-brand")
	WebElement sectionCurrentBrand;
	
	@FindBy(css = ".sisterbrands .email-preference-row")
	List<WebElement> sectionSisbrand;
	
	@FindBy(name = "emailsubscribe-overlay-submit")
	WebElement btnSubmitInUnsubscribeMdlWhenEmailPrefSelected;
	
	@FindBy(css = ".email-frequency-modal .unsubscribe-email  .solid-wine-berry")
	WebElement btnSubmitInUnsubscribeMdl;
	
	@FindBy(name = "subscribed-status")
	WebElement chkSubscribeStatus;
	
	/**********************************************************************************************
	 ********************************* WebElements of EmailPreferencePage - Ends ****************************
	 **********************************************************************************************/

	

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public EmailPreferencePage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Email Preference Page did not open up. Site might be down.", driver);
		}
		
		elementLayer = new ElementLayer(driver);

	}// isLoaded
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver,readyElement );
	}
	
	/**
	 * Verify get current email id
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public String getCurrentEmail() throws Exception{
		return BrowserActions.getText(driver, lblCurrentEmail, "Current Email").trim().split(" ")[0];
	}
	
	/**
	 * Verify current email message
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyCurrentEmailMessage() throws Exception{
		if(lblCurrentEmailMessage.getText().trim().equals("Your current address is set to:"))
			return true;
		else
			return false;
	}
	
	/**
	 * Verify clicking the edit button
	 * @return Profile page object
	 * @throws Exception - Exception
	 */
	public ProfilePage clickEdit() throws Exception{
		BrowserActions.clickOnElementX(lnkEditCurrentEmail, driver, "Edit Link");
		Utils.waitForPageLoad(driver);
		return new ProfilePage(driver).get();
	}
	
	/**
	 * Verify check and uncheck the preference option
	 * @param index - 
	 * @param enable - 
	 * @throws Exception - Exception
	 */
	public void checkUncheckPreferenceOption(int index, Boolean enable) throws Exception{
		if (enable) {
			if(divPreferenceOptions.findElements(By.cssSelector(".options-checkbox .input-checkbox")).get(index).isSelected() == false)
				BrowserActions.clickOnElementX(divPreferenceOptions.findElements(By.cssSelector(".options-checkbox .input-checkbox")).get(index), driver, "Preference Option");
		} else {
			if(divPreferenceOptions.findElements(By.cssSelector(".options-checkbox .input-checkbox")).get(index).isSelected())
				BrowserActions.clickOnElementX(divPreferenceOptions.findElements(By.cssSelector(".options-checkbox .input-checkbox")).get(index), driver, "Preference Option");
		}
	}
	
	/**
	 * Verify the preference option is selected
	 * @param index - 
	 * @return Boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyPreferenceOptionSelected(int index) throws Exception{
		if(divPreferenceOptions.findElements(By.cssSelector(".options-checkbox .input-checkbox")).get(index).isSelected())
			return true;
		else
			return false;
	}
	
	/**
	 * Verify all preference option is selected by default
	 * @return Boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyAllPreferenceOptionsUnselectedByDefault() throws Exception{
		int options = divPreferenceOptions.findElements(By.cssSelector(".options-checkbox .input-checkbox")).size();
		for (int i = 0; i <options; i++) {
			if(verifyPreferenceOptionSelected(i))
				return false;
		}
		return true;
	}
	
	/**
	 * Verify clicking the subscribe
	 * @throws Exception - Exception
	 */
	public void clickSubscribe() throws Exception{
		BrowserActions.clickOnElementX(btnSubscribe, driver, "Subscribe button");
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver, lnkEditPreferences);
	}
	
	/**
	 * Verify breadcrum value
	 * @return Boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyBreadCrumb() throws Exception{
		List<String> actualValue = new ArrayList<String>();
		for (WebElement breadcrumb : lstBreadCrumbElementsDesktop) {
			actualValue.add(breadcrumb.getText().trim().toLowerCase());
		}
		Log.event("Bbreadcrumb actual value:: " + actualValue.toString());
		if(actualValue.equals(Arrays.asList("home", "my account", "email preferences")))
			return true;
		else
			return false;	
	}
	
	/**
	 * VErify clicking the breadcrum
	 * @param crumb - 
	 * @return Object
	 * @throws Exception - Exception
	 */
	public Object clickBreadCrumb(String crumb) throws Exception{
		if (crumb.equalsIgnoreCase("home")) {
			BrowserActions.clickOnElementX(lstBreadCrumbElementsDesktop.get(0), driver, "Home in breadcrumb");
			return new HomePage(driver);
		} else {
			BrowserActions.clickOnElementX(lstBreadCrumbElementsDesktop.get(1), driver, "My Account in breadcrumb");
			return new MyAccountPage(driver);
		}
	}
	
	/**
	 * VErify getting the mobile breadcrum
	 * @return String values
	 * @throws Exception - Exception
	 */
	public String getMobileBreadCrumb() throws Exception{
		return BrowserActions.getText(driver, txtBreadCrumbElementMobile, "Mobile Breadcrumb").trim();
	}
	
	/**
	 * Verify click call arrow in breadcrum
	 * @return Myaccount page
	 * @throws Exception - Exception
	 */
	public MyAccountPage clickBackArrowInBreadcrumb() throws Exception{
		BrowserActions.clickOnElementX(imgBreadCrumbBackArrowMobile, driver, "Back Arrow");
		return new MyAccountPage(driver);
	}
	
	/**
	 * Verify email preference is not clickable
	 * @return Boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailPreferencesLinkNotClickable() throws Exception{
		if(lnkEmailPref.getAttribute("style").contains("cursor: text"))
			return true;
		else
			return false;
	}
	
	/**
	 * VErify the edit preference click
	 * @throws Exception - Exception
	 */
	public void clickEditPreferences() throws Exception{
		BrowserActions.clickOnElementX(lnkEditPreferences, driver, "Edit Link");
		Utils.waitForElement(driver, btnSubscribe);
	}
	
	/**
	 * Verify email preference check subscribe 
	 * @param brandIndex - 
	 * @param enable - 
	 * @return Email preference object
	 * @throws Exception - Exception
	 */
	public EmailPreferencePage checkUncheckSubscribe(int brandIndex, Boolean enable) throws Exception{
		if (enable) {
			if(verifySubscribedStatus(brandIndex))
				Log.message("Already subscribed");
			else
				BrowserActions.clickOnElementX(driver.findElement(By.cssSelector(".email-preference-row:nth-of-type("+ brandIndex +") .switch > div.slider.round")), driver, "Subscribed");
		} else {
			if(!verifySubscribedStatus(brandIndex))
				Log.message("Already unsubscribed");
			else {
				BrowserActions.clickOnElementX(driver.findElement(By.cssSelector(".email-preference-row:nth-of-type("+ brandIndex +") .switch > div.slider.round")), driver, "UnSubscribed");
				Utils.waitForElement(driver, emailUnSubscribePopup);
				BrowserActions.clickOnElementX(chkUnsubscribe, driver, "Unsubscribe");
				BrowserActions.clickOnElementX(lstUnsubscribeReasons.get(0), driver, "Reason");
				BrowserActions.clickOnElementX(btnReasonForUnsubscribeSubmit, driver, "Submit");
				Utils.waitForPageLoad(driver);
			}
		}
		Utils.waitForPageLoad(driver);
		return new EmailPreferencePage(driver).get();
	}
	
	/**
	 * To click unsubscribe on given brand   
	 * @param int - index of brand to click unsubscribe
	 * @throws Exception - Exception
	 */
	public void clickSubscriptionSlider(int brandIndex)throws Exception{
		WebElement subscriptionSlider = driver.findElements(By.cssSelector(".slider")).get(brandIndex-1);
		BrowserActions.clickOnElementX(subscriptionSlider, driver, "Subscription slider");
		Utils.waitForElement(driver, modalEmailFrequency);
			
	}
	
	/**
	 * To click subscribe sister brands   
	 * @param int - index of brand to click subscribe. Starts with 1
	 * @throws Exception - Exception
	 */
	public boolean getSubscriptionStatusSisterBrands(int brandIndex)throws Exception{
		Log.event("Get subscription status for: " + sectionSisbrand.get(brandIndex-1).findElement(By.cssSelector(".subscribed-status")).getAttribute("class").replaceAll("subscribed-status ", ""));
		String subscriptionStatus = sectionSisbrand.get(brandIndex-1).findElement(By.cssSelector(".switch>input")).getAttribute("value");
		return(subscriptionStatus.equalsIgnoreCase("Subscribed"));
	}
	
	/**
	 * To click subscribe sister brands   
	 * @param int - index of brand to click subscribe. Starts with 1
	 * @throws Exception - Exception
	 */
	public void clickSubscriptionSliderForSisterBrands(int brandIndex)throws Exception{
		Log.event("Clicking on subscription slider for: " + sectionSisbrand.get(brandIndex-1).findElement(By.cssSelector(".subscribed-status")).getAttribute("class").replaceAll("subscribed-status ", ""));
		WebElement subscriptionSlider = sectionSisbrand.get(brandIndex-1).findElement(By.cssSelector(".slider"));
		BrowserActions.clickOnElementX(subscriptionSlider, driver, "Subscription slider");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To select email frequency option 
	 * @param String - "less"/"none" to select less frequent/unsubscribe
	 * @throws Exception - Exception
	 */
	public void selectRadioOnUnsubscribeModal(String frequency)throws Exception{
		if (frequency.equalsIgnoreCase("less")) {
			BrowserActions.clickOnElementX(radioFewerContact, driver, "Please email me less frequently");
		} else if (frequency.equalsIgnoreCase("none")) {
			BrowserActions.scrollInToView(chkUnsubscribe, driver);
			BrowserActions.clickOnElementX(chkUnsubscribe, driver, "Unsubscribe");
		} else {
			Log.event("Invalid parameter given");
		}
	}
	
	/**
	 * Verify the Subscribe label for a brand
	 * @param brandIndex - index of the element
	 * @param textToVerify - text to verify with element label
	 * @return true if label is correct else false
	 * @throws Exception - Exception
	 */
	public Boolean verifySubscribeLabelOfTheBrands(int brandIndex, String textToVerify) throws Exception{
		String lblText = BrowserActions.getText(driver, lblSisterBrandSubscribe.get(brandIndex), "Brand Subscribe label");
		if(!textToVerify.equalsIgnoreCase(lblText)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Verify the Subscribe status
	 * @param brandIndex - 
	 * @return Boolean value
	 * @throws Exception - Exception
	 */
	public Boolean verifySubscribedStatus(int brandIndex) throws Exception{
		if (brandIndex == 0) {
			if(driver.findElement(By.cssSelector(".currentsite-brand .slider-label")).getText().trim().equalsIgnoreCase("Subscribed"))
				return true;
			else
				return false;
		} else {
			if(driver.findElement(By.cssSelector(".email-preference-row:nth-of-type("+ brandIndex +") .slider-label")).getText().trim().equalsIgnoreCase("Subscribed"))
				return true;
			else
				return false;
		}
	}
	
	/**
	 * Verify Brand logo's relative location
	 * return boolean - true/false if logo is located correctly
	 * @throws Exception - Exception
	 */
	public boolean verifyBrandImageLogoRelativePosition() throws Exception{
		if (!Utils.isMobile()) {
			for(int i=0 ; i <imgAllBrandNamesDesktop.size() ; i++ ) {
				if(imgAllBrandImageDesktop.get(i).getLocation().x >= imgAllBrandNamesDesktop.get(i).getLocation().x)
					return false;
			}
		} else {
			for(int i=0 ; i <imgAllBrandNamesMobile.size() ; i++ ) {
				if(imgAllBrandImageMobile.get(i).getLocation().x < imgAllBrandNamesMobile.get(i).getLocation().x
				|| imgAllBrandImageMobile.get(i).getLocation().y < imgAllBrandNamesMobile.get(i).getLocation().y)
					return false;
			}
		}
		return true;
	}
	
	/**
	 * Verify Brand logo and description's relative location
	 * return boolean - true/false if logo are located correctly
	 * @throws Exception - Exception
	 */
	public boolean verifyBrandDescriptionLogoRelativePosition() throws Exception{
		if(!Utils.isMobile()) {
			for(int i=0 ; i <imgAllBrandsDesktop.size() ; i++ ) {
				if(imgAllBrandsDesktop.get(i).getLocation().x >= txtAllBrandsDescripton.get(i).getLocation().x)
					return false;
			}
		} else {
			for(int i=0 ; i <imgAllBrandsMobile.size() ; i++ ) {
				if(imgAllBrandsMobile.get(i).getLocation().x < txtAllBrandsDescripton.get(i).getLocation().x)
					return false;
			}
		}
		return true;
	}
	
	/**
	 * Verify brand description and slider's relative location
	 * @return booleam - true/false if elements are located properly
	 * @throws Exception - Exception
	 */
	public boolean verifyDescriptionSliderRelativePosition() throws Exception{
		if(!(Utils.isMobile())) {
			for(int i=0 ; i <txtAllBrandsDescripton.size() ; i++ ) {
				if(txtAllBrandsDescripton.get(i).getLocation().x > sliderAllBrand.get(i).getLocation().x)
					return false;
			}
		} else {
			for(int i=0 ; i <txtAllBrandsDescripton.size() ; i++ ) {
				if(imgAllBrandImageMobile.get(i).getLocation().y > sliderAllBrand.get(i).getLocation().y)
					return false;
			}
		}
		return true;
		
	}
	
	/**
     * to click out side of email prequency modal
     * @throws Exception -
     */    
	public void clickOutSideOfEmailPreferenceModal() throws Exception {
		if (Utils.waitForElement(driver, modalEmailFrequency)) {
			BrowserActions.scrollToView(modalEmailFrequency, driver);
			BrowserActions.clickOnEmptySpaceVertical(driver, modalEmailFrequency);
			Log.event("Clicked out side of the email prequency modal.");
		}
	}
	
	/**
	 * To verify unsubscribe reason radio buttons 
	 * @param int - reason numbers(optional)
	 * @param Object - page object radio buttons are declared in
	 * @return boolean - true/fasle if radio buttons function properly
	 * @throws Exception - Exception
	 */
	public boolean verifyUnsubReasonRadio(Object obj, int ...reasonIndex)throws Exception{
		if(Utils.waitForElement(driver, modalEmailFrequency)) {
			BrowserActions.clickOnElementX(chkReasonForUnsubscribe, driver, "Unsubscribe.");
			WebElement radioReason = null;
			if (reasonIndex.length==1) {
				radioReason = lstUnsubscribeReasons.get(reasonIndex[0]);
				BrowserActions.clickOnElementX(radioReason, driver, "Unsubscribe reason.");
				return radioReason.getCssValue("background-image").contains("circle-selected.svg")
						|| radioReason.getCssValue("background-position").equals("-32px -5px");
			} else if (reasonIndex.length==2) {
				radioReason = lstUnsubscribeReasons.get(reasonIndex[0]);
				BrowserActions.clickOnElementX(radioReason, driver, "Unsubscribe reason.");
				WebElement radioReasonAlt = lstUnsubscribeReasons.get(reasonIndex[1]);
				BrowserActions.clickOnElementX(radioReasonAlt, driver, "Unsubscribe reason.");
				return (radioReasonAlt.getCssValue("background-image").contains("circle-selected.svg") 
								|| radioReasonAlt.getCssValue("background-position").equals("-32px -5px")) 
						&& (radioReason.getCssValue("background-image").contains("circle-outline.svg") 
								|| radioReason.getCssValue("background-position").equals("-5px -5px"));
			} else {
				radioReason = lstUnsubscribeReasons.get(Utils.getRandom(0, lstUnsubscribeReasons.size()-1));
				BrowserActions.clickOnElementX(radioReason, driver, "Unsubscribe reason.");
				return radioReason.getCssValue("background-image").contains("circle-selected.svg")
						|| radioReason.getCssValue("background-position").equals("-32px -5px");
			}
			
		} else {
			Log.event("Email frequency modal is not open.");
			return false;
		}
	}
	
	/**
	 * To reset account subscriptions to brands 
	 * @throws Exceptio - Exception
	 */
	public void resetSubscriptionState()throws Exception{
		if (Utils.waitForElement(driver, divPreferenceOptions)) {
			BrowserActions.clickOnElementX(btnCurrentBrandEmailSubscribe, driver, "Subscribe button for current brand.");
		}
		Utils.waitForPageLoad(driver);
		for (int i=1 ; i< btnSubscribeAllBrands.size() ; i++) {
			if(btnSubscribeAllBrands.get(i).getCssValue("background-color").equals("rgba(51, 30, 83, 1)")) {
				BrowserActions.clickOnElementX(btnSubscribeAllBrands.get(i), driver, "Unsubscribe toggle for sister brand.");
				selectRadioOnUnsubscribeModal("none");
				BrowserActions.clickOnElementX(btnReasonForUnsubscribeSubmit, driver, "Submit");
				Utils.waitForPageLoad(driver);
				Log.message("Clicked on submit", driver);
			}
		}
	}
	
	/**
	 * To verify if text-box for Other is enabled
	 * @return boolean - true/false if text is enabled
	 * @throws Exception - Exception
	 */
	public boolean verifyOtherReasonTextboxEnabled()throws Exception{
		if (Utils.waitForElement(driver, modalEmailFrequency)) {
			if (Utils.waitForElement(driver, divUnsubscribeReasons)) {
				return txtOtherReason.isEnabled();
			} else {
				Log.event("Unsubscribe radio button is not selected.");
				return false;
			}
		} else {
			Log.event("Email frequency modal is not opened.");
			return false;
		}
	}
	
	/**
	 * To click on subscribe button
	 * @return EmailSubscriptionThankyouPage
	 * @throws Exception
	 */
	public EmailSubscriptionThankyouPage clickOnSubscribe()throws Exception{
		BrowserActions.clickOnElementX(btnCurrentBrandEmailSubscribe, driver, "Subscripe");
		Utils.waitForPageLoad(driver);
		return new EmailSubscriptionThankyouPage(driver).get();
	}
	
	/**
	 * To click on subscribe button
	 * @throws Exception
	 */
	public void clickOnSubscribeRegisterUser()throws Exception{
		if (Utils.waitForElement(driver, btnCurrentBrandEmailSubscribe)) {
			BrowserActions.clickOnElementX(btnCurrentBrandEmailSubscribe, driver, "Subscribe");
		} else {
			BrowserActions.clickOnElementX(toggleCurrentBrandEmailSubscribe, driver, "Subscribe");
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To verify Subscribe option is displayed right to product information or not
	 * @return boolean 
	 */
	public boolean verifySubscribePosition()throws Exception{
		for (WebElement sisBrand : sectionSisbrand) {
			
			WebElement content = sisBrand.findElement(By.cssSelector(".sisterbrands .email-description .left-text"));
			WebElement rdoSlick = sisBrand.findElement(By.cssSelector(".subscribed-status"));
			if(Utils.isMobile()) {
				if(!BrowserActions.verifyVerticalAllignmentOfElements(driver, content, rdoSlick)) {
					return false;
				}
			} else {
				if(!BrowserActions.verifyHorizontalAllignmentOfElements(driver, rdoSlick, content))
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To select reason for unsubscribe by index 
	 * @param index - Index to reason to select. Starts from 1 
	 * @throws Exception
	 */
	public void selectOptionInUnSubscribeByIndex(int index)throws Exception{
		BrowserActions.clickOnElementX(lstUnsubscribeReasons.get(index - 1), driver, index + "th Item in unsubscribe list");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on submit button
	 * @throws Exception
	 */
	public void clickOnSubmit()throws Exception{
		if(Utils.waitForElement(driver, btnSubmitInUnsubscribeMdl)){			
			BrowserActions.clickOnElementX(btnSubmitInUnsubscribeMdl, driver, "Submit");
			Utils.waitUntilElementDisappear(driver, btnSubmitInUnsubscribeMdl);
		} else {
			BrowserActions.clickOnElementX(btnSubmitInUnsubscribeMdlWhenEmailPrefSelected, driver, "Submit");
			Utils.waitUntilElementDisappear(driver, btnSubmitInUnsubscribeMdlWhenEmailPrefSelected);
		}
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To unsubscribe current brand emails
	 * @throws Exception
	 */
	public void unSubscribeCurrentbrand()throws Exception{
		BrowserActions.scrollToView(btnUnSubscribeCurrentbrand, driver);
		BrowserActions.clickOnElementX(btnUnSubscribeCurrentbrand, driver, "Unsubscribe");
		Utils.waitForElement(driver, emailUnSubscribePopup);
		selectRadioOnUnsubscribeModal("none");
		selectOptionInUnSubscribeByIndex(1);
		clickOnSubmit();
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To unsubscribe sister brand emails by index
	 * @param index - index of which brand emails to unsubscibe from. Starts with 1
	 * @throws Exception
	 */
	public void unSubscribeSisterbrand(int index)throws Exception{
		Log.event("Unsubscribing from: " + sectionSisbrand.get(index-1).findElement(By.cssSelector(".subscribed-status")).getAttribute("class").replaceAll("subscribed-status ", ""));
		WebElement subscriptionSlider = sectionSisbrand.get(index-1).findElement(By.cssSelector(".slider"));
		BrowserActions.clickOnElementX(subscriptionSlider, driver, "Unsubscribe");
		selectRadioOnUnsubscribeModal("none");
		selectOptionInUnSubscribeByIndex(1);
		clickOnSubmit();
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * Return true if current brand subscribtion is active
	 * @return - true/false
	 * @throws Exception - Exception
	 */
	public boolean currentBrandSubscribtionStatus()throws Exception{
		return (Utils.waitForElement(driver, btnUnSubscribeCurrentbrand)) ? true : false;
	}
	
}
