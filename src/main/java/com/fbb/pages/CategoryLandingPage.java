package com.fbb.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class CategoryLandingPage extends LoadableComponent<CategoryLandingPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	String runBrowser;

	/**********************************************************************************************
	 ********************************* WebElements of Category Landing Page ****************************
	 **********************************************************************************************/

	// --------------- Header Section ---------------------- //


	//---------WebElements Mobile------------//
	
	@FindBy(css = ".pt_categorylanding")
	WebElement readyElement;
	
	@FindBy(css = ".cta-btn")
	WebElement btnShop;

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public CategoryLandingPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);

	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Category Landing Page did not open up. Site might be down.", driver);
		}

		elementLayer = new ElementLayer(driver);
		runBrowser  = Utils.getRunBrowser(driver);
	}

	// --------------- Header Section ---------------------- //
	
	 /**
     * To get the Load status of page
     *
     * @return boolean - 'true'(Page loaded) / 'false'(Page not loaded)
     * @throws Exception - Exception
     */
    public boolean getPageLoadStatus() throws Exception {
        return isPageLoaded;
    }
	
	/**
	 * to click on shop button
	 * @throws Exception - Exception
	 */
	public void clickShopButton()throws Exception{
		BrowserActions.clickOnElementX(btnShop, driver, "Shop");
		Utils.waitForPageLoad(driver);
	}
}
