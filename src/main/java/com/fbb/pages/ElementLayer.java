package com.fbb.pages;

import java.lang.reflect.Field;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.fbb.externalAPI.GoogleMapsAPI;
import com.fbb.support.BrowserActions;
import com.fbb.support.CollectionUtils;
import com.fbb.support.ColorUtils;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;

/**
 * ElementLayer page is used to verify each page elements.
 * 
 * We can declare and initialize this class on each page object classes
 */
public class ElementLayer {

	private final WebDriver driver;
	public static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
	private static EnvironmentPropertiesReader demandWareProperty = EnvironmentPropertiesReader.getInstance("demandware");

	/**
	 * Constructor class for ElementLayer, here we initializing the driver for
	 * page
	 * @param driver -
	 */
	public ElementLayer(WebDriver driver) {
		this.driver = driver;
	}

	/**
	 * Verify if expected page WebElements are present
	 * <p>If expected element is present on the current page, add to list of
	 * value/fields to actualElement list and then compare to expectedElements
	 * @param expectedElements - list of expected WebElement names
	 * @param obj - the page object the elements are on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 4 possibilities
	 */
	public boolean verifyPageElements(List<String> expectedElements, Object obj, boolean... scrollToFind) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
				if(scrollToFind.length > 0 && scrollToFind[0]) {
					BrowserActions.scrollInToView(element, driver);
				}
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if (Utils.waitForElement(driver, element, 2)) {
				actual_elements.add(expEle);
			}
		}
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}
	
	/**
	 * Verify if expected page WebElements are present
	 * <p>If expected element is present on the current page, add to list of
	 * value/fields to actualElement list and then compare to expectedElements
	 * @param expectedElements - expected WebElement values
	 * @param obj - the page object the elements are on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 4 possibilities
	 */
	public boolean isElementAvailable(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if (Utils.waitForElement(driver, element, 2)) {
				actual_elements.add(expEle);
			}
		}
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Verify if expected page WebElements are disabled
	 * <p>If expected element is present on the current page, add to list of
	 * value/fields to actualElement list and then compare to expectedElements
	 * @param expectedElements - expected WebElement values
	 * @param obj - the page object the elements are on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 4 possibilities
	 */
	public boolean verifyPageElementsDisabled(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if (Utils.waitForDisabledElement(driver, element, Utils.maxElementWait)) {
				actual_elements.add(expEle);
			}
		}
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Verify if expected page WebElements are checked/selected 
	 * <p>If expected element checked/selected on the current page, add to list
	 * of value/fields to actualElement list and then compare to expectedElements
	 * @param expectedElements - expected WebElement values
	 * @param obj - the page object the elements are on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 4 possibilities
	 */
	public boolean verifyPageElementsChecked(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if(!Utils.isTablet()) {
				(new WebDriverWait(driver, 5).pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class)
						.withMessage("Creat an Event mobel box did not open")).until(ExpectedConditions.visibilityOf(element));
			}
			if (element.isSelected()) {
				actual_elements.add(expEle);
			}
		}
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}
	
	/**
	 * Verify if expected page WebElements are checked/selected 
	 * <p>If expected element checked/selected on the current page, add to list
	 * of value/fields to actualElement list and then compare to expectedElements
	 * @param expectedElements - expected WebElement values
	 * @param obj - the page object the elements are on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 4 possibilities
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyPageListElementsChecked(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such field present on this page, Please check the expected list values:: " + expEle);
			}
			List<WebElement> element = null;
			try {
				element = ((List<WebElement>) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			
			boolean flag = true;
			for(WebElement ele : element) {
				if (!ele.isSelected()) {
					flag = false;
				}
			}
			if(flag)
				actual_elements.add(expEle);
		}
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Verify if expected page WebElements are unchecked/unselected 
	 * <p>If expected element checked/selected on the current page, add to list
	 * of value/fields to actualElement list and then compare to expectedElements
	 * @param expectedElements - expected WebElement values
	 * @param obj - the page object the elements are on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 4 possibilities
	 */
	public boolean verifyPageElementsUnChecked(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if(!Utils.isTablet()) {
				(new WebDriverWait(driver, 5).pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class)
						.withMessage("Creat an Event mobel box did not open")).until(ExpectedConditions.visibilityOf(element));
			}
			if (!element.isSelected()) {
				actual_elements.add(expEle);
			}
		}
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}


	/**
	 * Verify the lack of presence of page WebElements
	 * <p>If expected element is NOT present on this current page, add to list of
	 * value/fields to actualElement list and then compare to expectedElements
	 * @param expectedNotToSee - expected non-existing WebElement values
	 * @param obj - the page object the elements should not be on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 2 possibilities
	 */
	public boolean verifyPageElementsDoNotExist(List<String> expectedNotToSee, Object obj) throws Exception {
		List<String> nonexisting_elements = new ArrayList<String>();
		for (String expEle : expectedNotToSee) {
			Field f = null;
			WebElement element = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
				element = ((WebElement) f.get(obj));
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such field present on this page, Please check the expected list values:: " + expEle);
			}
			if (!Utils.waitForElement(driver, element, 2)) {
				nonexisting_elements.add(expEle);
			}
		}
		return CollectionUtils.compareTwoList(expectedNotToSee, nonexisting_elements);
	}

	/**
	 * Verify if expected page List WebElements are present
	 * <p>If size of the list element is greater than zero and first element from
	 * expected list elements present on the current page, add to list of
	 * value/fields to actualElement list and then compare to expectedElements
	 * @param expectedElements - expected List WebElement values
	 * @param obj - the page object the elements are on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 5 possibilities
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyPageListElements(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such fields present on this page, Please check the expected list values:: " + expEle);
			}
			List<WebElement> elements = null;
			try {
				elements = ((List<WebElement>) f.get(obj));
			} catch (ClassCastException | IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if (elements.size() > 0 
					&& Utils.waitForElement(driver, elements.get(0))) {
				actual_elements.add(expEle);
			}
		}
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Verify the lack of presence of page WebElements
	 * <p>If expected element is NOT present on this current page, add to list of
	 * value/fields to actualElement list and then compare to expectedElements
	 * @param expectedNotToSee - expected non-existing WebElement values
	 * @param obj - the page object the elements should not be on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 2 possibilities
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyPageListElementsDoNotExist(List<String> expectedNotToSee, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedNotToSee) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such fields present on this page, Please check the expected list values:: " + expEle);
			}
			List<WebElement> elements = null;
			try {
				elements = ((List<WebElement>) f.get(obj));
			} catch (ClassCastException | IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if (elements.size() == 0) {
				actual_elements.add(expEle);
			}
		}
		return CollectionUtils.compareTwoList(expectedNotToSee, actual_elements);
	}

	/**
	 * Verify if expected list of page WebElements are disabled
	 * <p>If expected list element is present on the current page, add to list of
	 * value/fields to actualElement list and then compare to expectedElements
	 * @param expectedElements - expected WebElement values
	 * @param obj - the page object the elements are on
	 * @return true if both the lists are equal, else returns false
	 * @throws Exception - one of 4 possibilities
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyPageListElementsDisabled(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			List<WebElement> elements = null;
			try {
				elements = ((List<WebElement>) f.get(obj));
			} catch (ClassCastException |IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if (elements.size() > 0 
					&& Utils.waitForDisabledElement(driver, elements.get(0), Utils.maxElementWait)) {
				actual_elements.add(expEle);
			}
		}
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Verify contents of a WebElement equals a passed in string variable
	 * @param textToVerify - expected text
	 * @param elementToVerify - element to verify the text of
	 * @param obj - page object
	 * @return true if text on screen matches passed variable contents
	 */
	public boolean verifyTextEquals(String elementToVerify, String textToVerify, Object obj) {
		boolean status = false;
		WebElement element = null;
		try{
			Field elementField = obj.getClass().getDeclaredField(elementToVerify);
			elementField.setAccessible(true);
			element = ((WebElement) elementField.get(obj));
			String eleText=null;
			if(element.getTagName().equals("input")  || element.getTagName().equals("textarea"))
				eleText = element.getAttribute("value").trim().replaceAll("\\s+", " ");
			else
				eleText = element.getText().trim().replaceAll("\\s+", " ");
			
			Log.event("Text from element:: " + eleText);
			return (eleText.equals(textToVerify));
		}//try
		catch(Exception e){
			e.printStackTrace();
		}//catch

		return status;
	}
	
	/**
	 * To verify string is masked or not
	 * @param elementToVerify - Element to verify number of masked digit
	 * @param noOfdigits - number of digits to be masked
	 * @param obj - page object
	 * @return true/false if masked properly
	 * @throws Exception
	 */
	public boolean verifyNumberMaskedWithSpecifiedDigits(String elementToVerify,int noOfdigits, Object obj) throws Exception{
		boolean digits=false; WebElement element = null; String eleText;
		try{
			Field elementField = obj.getClass().getDeclaredField(elementToVerify);
			elementField.setAccessible(true);
			element = ((WebElement) elementField.get(obj));
			if(element.getTagName().equals("input")  || element.getTagName().equals("textarea"))
				eleText = element.getAttribute("value");
			else
				eleText = element.getText();

			boolean maskedDigits=eleText.contains("**")|| eleText.contains("X");
			eleText = eleText.replaceAll("\\D","");
			if(eleText.length() == noOfdigits){
				digits = true;
			}
			if(maskedDigits && digits){
				return true;
			}
		}//try
		catch(Exception e){
			e.printStackTrace();
		}//catch
		
		return false;
	}

	/**
	 * To get the element which have exact variable name
	 * @param expectedEle - String "Expected Element"
	 * @param obj - page object
	 * @return WebElement with expected Element variable name
	 * @throws Exception
	 */
	public static WebElement getElement(String expectedEle, Object obj) throws Exception{
		WebElement element = null;
		try{
			Field elementField = obj.getClass().getDeclaredField(expectedEle);
			elementField.setAccessible(true);
			element = ((WebElement) elementField.get(obj));
		} catch(Exception e) {
			e.printStackTrace();
		}
		return element;
	}

	/**
	 * To get the element which have exact variable name
	 * @param expectedEle - String "Expected Element"
	 * @param obj - page object
	 * @return List of WebElement with expected Element variable name
	 * @throws Exception -
	 */
	@SuppressWarnings("unchecked")
	public static List<WebElement> getListElement(String expectedEle, Object obj) throws Exception{
		List<WebElement> element = null;
		try{
			Field elementField = obj.getClass().getDeclaredField(expectedEle);
			elementField.setAccessible(true);
			element = ((List<WebElement>) elementField.get(obj));
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return element;
	}

	/**
	 * Verify the List element size
	 * @param elementName - element to be checked
	 * @param sizeToVerify - size to be verified
	 * @param obj - driver object
	 * @return if size matches - true if size not match - false
	 * @throws Exception
	 */
	public boolean verifyListElementSize(String elementName, int sizeToVerify, Object obj)throws Exception{
		List<WebElement> listElement = getListElement(elementName, obj);
		Log.event("Expected List element size :: " +sizeToVerify);
		Log.event("Actual List element size :: " +listElement.size());
		return listElement.size() == sizeToVerify;
	}

	/**	
	 * To verify the list of page elements are displayed
	 * @param expectedElements - list of element names to verify
	 * @param obj - page object
	 * @return verification if element is displayed
	 * @throws Exception
	 */
	public boolean verifyElementDisplayed(List<String> expectedElements, Object obj)throws Exception{

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				BrowserActions.scrollToTopOfPage(driver);
				element = ((WebElement) f.get(obj));
				if (element.isDisplayed()) {
					actual_elements.add(expEle);
				} else {
					BrowserActions.scrollInToView(element, driver);
					if (element.isDisplayed()) {
						actual_elements.add(expEle);
					}
				}
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			} catch (NoSuchElementException e){
				Log.event("No Such Element present in Page :: " + expEle);
			}
		}
		
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}
	
	/**	
	 * To verify id any element from a list is available
	 * <br> Use this method when different brands have different locator for same element 
	 * @param expectedElements - list of element names to verify
	 * @param obj - page object
	 * @return verification if element is displayed
	 * @throws Exception
	 */
	public boolean verifyAnyElementDisplayed(List<String> expectedElements, Object obj)throws Exception{
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				BrowserActions.scrollToTopOfPage(driver);
				element = ((WebElement) f.get(obj));
				if (element.isDisplayed()) {
					Log.event("Element found:: [" + expEle + "]");
					return true;
				} else {
					BrowserActions.scrollInToView(element, driver);
					if (element.isDisplayed()) {
						Log.event("Element found:: [" + expEle + "]");
						return true;
					}
				}
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			} catch (NoSuchElementException e){
				Log.event("Element not found:: [" + expEle + "]");
			}
		}
		Log.event("No element from given list found on page.");
		return false;
	}

	/**	
	 * To verify the list of page elements are displayed without scrolling the page
	 * @param expectedElements - list of element names to verify
	 * @param obj - page object
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyElementDisplayedWithoutScrolling(List<String> expectedElements, Object obj)throws Exception{

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
				if (element.isDisplayed()) {
					actual_elements.add(expEle);
				}
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			} catch (NoSuchElementException e){
				Log.event("No Such Element present in Page :: " + expEle);
				return false;
			}
		}
		
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * To verify the list of page elements are displayed
	 * @param expectedElements -
	 * @param obj - page object
	 * @return boolean
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public boolean VerifyPageListElementDisplayed(List<String> expectedElements, Object obj)throws Exception{

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			List<WebElement> element = null;
			try {
				element = ((List<WebElement>) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			boolean flag = true;
			Log.event("Total number of " + expEle + " :: " + element.size());
			for(int i = 0; i < element.size(); i++){
				if (!element.get(i).isDisplayed()) {
					flag = false;
				}
			}
			if(flag)
				actual_elements.add(expEle);
		}
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * to verify the List of page elements are not displayed
	 * @param expectedElements - list of elements
	 * @param obj - page object
	 * @return boolean
	 * @throws Exception
	 */
	public boolean VerifyPageElementNotDisplayed(List<String> expectedElements, Object obj)throws Exception{

		List<String> absentElements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
				if (element.isDisplayed() == false) {
					absentElements.add(expEle);
				} else {
					Log.event(expEle + " found in page.");
					BrowserActions.scrollToViewElement(element, driver);
				}
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
				absentElements.add(expEle);
			} catch (NoSuchElementException err) {
				absentElements.add(expEle);
			}
		}
		
		return CollectionUtils.compareTwoList(expectedElements, absentElements);
	}

	/**
	 * to verify the List of page elements are not displayed
	 * @param expectedElements - list of elements to verify
	 * @param obj - page object
	 * @return true/false if list of elements are not displayed
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public boolean VerifyPageListElementNotDisplayed(List<String> expectedElements, Object obj)throws Exception{

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			List<WebElement> element = null;
			try {
				element = ((List<WebElement>) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			boolean flag = true;
			for(int i = 0; i < element.size(); i++){
				if (element.get(i).isDisplayed() == true) {
					flag = false;
				}
				if(flag)
					actual_elements.add(expEle);
			}
		}
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * To get element's position as hash map
	 * @param elementName - Name of element
	 * @param obj - page object
	 * @return element location as hashmap of co-ordinate
	 * @throws Exception
	 */
	public LinkedHashMap<String, String> getElementXandYAxis(String elementName, Object obj)throws Exception{
		LinkedHashMap<String, String> axisDetails =  new LinkedHashMap<String, String>();
		WebElement element = getElement(elementName, obj);

		Point eleAxis = element.getLocation();

		String AxisX = Integer.toString(eleAxis.x);
		String AxisY = Integer.toString(eleAxis.y);

		axisDetails.put("XAxis", AxisX);
		axisDetails.put("YAxis", AxisY);

		return axisDetails;
	}
	
	/**
	 * To Compare Two hash set string
	 * @param set1 - Set1 hashset
	 * @param set2 - Set2 hashset
	 * @return true/false if two sets are equal 
	 * @throws Exception - Exception
	 */
	public boolean compareTwoHashSet(HashSet<String> set1, HashSet<String> set2)throws Exception{

		if(set1 == null || set2 == null){
			return false;
		}
	
		if(set1.size() != set2.size()){
			return false;
		}
	
		return set1.containsAll(set2);
	}
	

	/**
	 * To verify the first element displayed below the second element
	 * @param firstElement -
	 * @param secondElement -
	 * @param obj -
	 * @return 'true' / 'false'
	 * @throws Exception -
	 */
	public boolean verifyElementDisplayedBelow(String firstElement, String secondElement, Object obj)throws Exception{
		try {
			Field f = obj.getClass().getDeclaredField(firstElement);
			f.setAccessible(true);
			WebElement firstEle = ((WebElement) f.get(obj));

			Field f1 = obj.getClass().getDeclaredField(secondElement);
			f1.setAccessible(true);
			WebElement secondEle = ((WebElement) f1.get(obj));

			int firstPoint = firstEle.getLocation().getY();
			System.out.println("firstElement Y: "+ firstPoint);
			int secondPoint = secondEle.getLocation().getY();
			System.out.println("secondElement Y: "+ secondPoint);
			return (firstPoint < secondPoint);
		} catch(Exception e) {
			Log.event(e.getLocalizedMessage());
			return false;
		}
	}

	/**
	 * To verify the List of String in ascending/descending order
	 * @param strList - List of String
	 * @param order - 'ascending' / 'descending'
	 * @return 'true' - 'false'
	 * @throws Exception -
	 */
	public boolean verifyListInOrder(List<String> strList, String order)throws Exception{
		List<String> lstCopy = new ArrayList<String>(strList);
		Collections.sort(lstCopy);
		if(!order.equalsIgnoreCase("ascending")) {
			Collections.reverse(lstCopy);
		}
		return strList.equals(lstCopy);
	}

	/**
	 * Verify the CSS property for an element
	 * @param element - WebElement for which to verify the CSS property
	 * @param cssProperty - the CSS property name to verify
	 * @param expectedValue - the actual CSS value of the element
	 * @param obj - page object
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean verifyCssPropertyForElement(String element, String cssProperty, String expectedValue, Object obj)throws Exception {
		if (cssProperty.toLowerCase().contains("color")) {
			return verifyElementColor(element, expectedValue, obj);
		}
		
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		BrowserActions.scrollToViewElement(webElement, driver);
		String actualCSSValue = null;
		try {
			actualCSSValue = webElement.getCssValue(cssProperty);
		} catch (NoSuchElementException ex) {
			Log.reference(element + " is not displayed");
			return false;
		}

		Log.event("Expected '" + cssProperty + "' Value :: " + expectedValue);
		Log.event("Actual '" + cssProperty + "' Value :: " + actualCSSValue);
		return actualCSSValue.toLowerCase().contains(expectedValue.toLowerCase());
	}
	
	/**
	 * Verify the CSS property for an element
	 * @param element - WebElement for which to verify the CSS property
	 * @param cssProperty - the CSS property name to verify
	 * @param expectedValue - the actual CSS value of the element
	 * @param obj - page object
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyComputedCssPropertyForElement(String element, String cssProperty, String expectedValue, Object obj)throws Exception {
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		String actualValue = (String) executor.executeScript("return window.getComputedStyle(arguments[0]).getPropertyValue('"+cssProperty+"')",webElement);

		Log.event("Expected '" + cssProperty + "' Value :: " + expectedValue);
		Log.event("Actual '" + cssProperty + "' Value :: " + actualValue);
		return actualValue.contains(expectedValue);
	}
	
	/**
	 * To get CSS property of a pseudo element
	 */
	public String getCssPropertyForPsuedoElement(WebElement element, String psuedoEle, String cssProperty, boolean... scrollToElement) throws Exception{
		if (scrollToElement.length > 0 && scrollToElement[0]) {
			BrowserActions.scrollToView(element, driver);
		}
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		psuedoEle = psuedoEle.startsWith(":") ? psuedoEle : ":"+psuedoEle;
		String jsExecStr = "return window.getComputedStyle(arguments[0], '"+psuedoEle+"').getPropertyValue('"+cssProperty+"')";
		Log.event("JS Execution String:: " + jsExecStr);
		return (String) executor.executeScript(jsExecStr, element);
	}

	/**
	 * Verify the CSS property for an element
	 * @param element - WebElement for which to verify the CSS property
	 * @param cssProperty - the CSS property name to verify
	 * @param expectedValue - the actual CSS value of the element
	 * @param psuedoEle - Pseudo propery name
	 * @param obj - page object
	 * @return boolean
	 * @throws Exception -
	 */
	public boolean verifyCssPropertyForPsuedoElement(String element, String psuedoEle, String cssProperty, String expectedValue, Object obj, boolean... scrollToElement)throws Exception {
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		boolean scroll = scrollToElement.length == 0 ? true : scrollToElement[0];
		String actualValue = getCssPropertyForPsuedoElement(webElement, psuedoEle, cssProperty, scroll);
		
		Log.event("Expected '" + cssProperty + "' Value :: " + expectedValue);
		Log.event("Actual '" + cssProperty + "' Value :: " + actualValue);
		return actualValue.contains(expectedValue);
	}

	/**
	 * Verify the CSS property for an element
	 * 
	 * @param element - WebElement for which to verify the CSS property
	 * @param cssProperty - the CSS property name to verify
	 * @param psuedoEle -
	 * @param expectedValue - the actual CSS value of the element
	 * @param psuedoEle -
	 * @param obj -
	 * @return boolean
	 * @throws Exception -
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyCssPropertyForPsuedoListElement(String element,String psuedoEle, String cssProperty, String expectedValue, Object obj)throws Exception {
		boolean result = false;
		int missingcount = 0;
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		List<WebElement> webElement = ((List<WebElement>) f.get(obj));
		for(WebElement ele : webElement){
			BrowserActions.scrollToViewElement(ele, driver);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			String actualValue = (String) executor.executeScript("return window.getComputedStyle(arguments[0], ':"+psuedoEle+"').getPropertyValue('"+cssProperty+"')",ele);
			
			Log.event("Expected '" + cssProperty + "' Value :: " + expectedValue);
			Log.event("Actual '" + cssProperty + "' Value :: " + actualValue);
			if(actualValue.contains(expectedValue)){
				result = true;
			}else missingcount++;
		}
		return result && (missingcount==0);
	}

	/**
	 * To verify attribute value of element contains given string
	 * @param element - Name of element
	 * @param attributeName - Attribute name of element
	 * @param expectedValue - Expected atribute value of element
	 * @param obj - Page object
	 * @return boolean - True/False if value matched or not.
	 * @throws Exception
	 */
	public boolean verifyAttributeForElement(String element, String attributeName, String expectedValue, Object obj)throws Exception {
		boolean result = false;
		try {
			Field f = obj.getClass().getDeclaredField(element);
			f.setAccessible(true);
			WebElement webElement = ((WebElement) f.get(obj));
			BrowserActions.scrollToViewElement(webElement, driver);
			String actualAttibuteValue = webElement.getAttribute(attributeName);
			
			actualAttibuteValue = actualAttibuteValue.replaceAll("&amp;", "");
			expectedValue = expectedValue.replaceAll("&", "");
			
			Log.event("Expected '" + attributeName + "' Value :: " + expectedValue);
			Log.event("Actual '" + attributeName + "' Value :: " + actualAttibuteValue);
	
			if (actualAttibuteValue.contains(expectedValue)) {
				result = true;
			} 
		} catch (NullPointerException ex) {
			return false;
		}
		return result;
	}
	
	/**
	 * To get an attribute of an element 
	 * @param element - Declared element name
	 * @param attributeName - Name of the Attribute get the value
	 * @param obj - Page object where the element is declared
	 * @return String - The value extracted from attribute
	 * @throws Exception - Exception 
	 */
	public String getAttributeForElement(String element, String attributeName, Object obj) throws Exception{
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		BrowserActions.scrollToViewElement(webElement, driver);
		String attibuteValue = webElement.getAttribute(attributeName);
		Log.event("Attribute from Element:: " + attibuteValue);
		return attibuteValue;
	}

	/**
	 * Verify the CSS property for an element
	 * @param element - WebElement for which to verify the CSS property
	 * @param cssProperty - the CSS property name to verify
	 * @param expectedValue - the actual CSS value of the element
	 * @param obj - page object
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyCssPropertyForParentElement(String element, String cssProperty, String expectedValue, Object obj)throws Exception {
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = (WebElement) f.get(obj);
		WebElement parentEle = webElement.findElement(By.xpath(".."));
		String actualClassProperty = parentEle.getCssValue(cssProperty);
		Log.event("Expected '" + cssProperty + "' Value :: " + expectedValue);
		Log.event("Actual '" + cssProperty + "' Value :: " + actualClassProperty);
		return actualClassProperty.contains(expectedValue);
	}

	/**
	 * To verify attribute value of parent element of given element contains given string
	 * @param element - Element name
	 * @param attributeName - attribute name
	 * @param expectedValue - Expected attribute value
	 * @param obj - Page object
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyAttributeForParentElement(String element, String attributeName, String expectedValue, Object obj)throws Exception {
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = (WebElement) f.get(obj);
		WebElement parentEle = webElement.findElement(By.xpath(".."));
		String actualClassProperty = parentEle.getAttribute(attributeName);
		Log.event("Expected '" + attributeName + "' Value :: " + expectedValue);
		Log.event("Actual '" + attributeName + "' Value :: " + actualClassProperty);
		return actualClassProperty.contains(expectedValue);
	}

	/**
	 * Verify the CSS property for an element
	 * @param element - WebElement for which to verify the CSS property
	 * @param cssProperty - the CSS property name to verify
	 * @param expectedValue - the actual CSS value of the element
	 * @param obj - page object    
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyCssPropertyForListElement(String element, String cssProperty, String expectedValue, Object obj)throws Exception {
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		@SuppressWarnings("unchecked")
		List<WebElement> webElement = ((List<WebElement>) f.get(obj));
		for(int i = 0; i < webElement.size(); i++){
			Log.event("Expected Value:: " + expectedValue);
			Log.event("Actual Value in " + element + "(" + (i+1) + "):: " + webElement.get(i).getCssValue(cssProperty));
			if(!webElement.get(i).getCssValue(cssProperty).contains(expectedValue)){
				return false;
			}
		}
		return true;
	}

	/**
	 * Verify the Attribute value for an element
	 * @param element - WebElement for which to verify the CSS property
	 * @param attributeName - the attributeName to verify
	 * @param expectedValue - the actual CSS value of the element
	 * @param obj - page object    
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyAttibuteForListElement(String element, String attributeName, String expectedValue, Object obj)throws Exception {
		try {
			Log.event("Value to verify :: " + expectedValue);
			List<WebElement> webElement = getListElement(element, obj);
			for(int i = 0; i < webElement.size(); i++){
				Log.event("Value from Element-" + (i+1) + " :: " + webElement.get(i).getAttribute(attributeName));
				if(!webElement.get(i).getAttribute(attributeName).contains(expectedValue)){
					return false;
				}
			}
		} catch (NoSuchElementException ex) {
			Log.event("<<<The element is not displayed in the page>>> " + element);
			return false;
		}
		return true;
	}

	/**
	 * Verify the CSS property for an element
	 * @param expectedElements - WebElement for which to verify the CSS property
	 * @param cssProperty - the CSS property name to verify
	 * @param expectedValue - the actual CSS value of the element
	 * @param obj - page object
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyCssPropertyForListOfElement(List<String> expectedElements, String cssProperty, String expectedValue, Object obj)throws Exception {

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
				String actualValue = element.getCssValue(cssProperty);
				BrowserActions.scrollToViewElement(element, driver);
				if (actualValue.contains(expectedValue)) {
					actual_elements.add(expEle);
				}
				Log.event("Expected CSS values :: " + expectedValue + "\nActual Value :: " + actualValue);
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			} catch (NoSuchElementException e){
				Log.failsoft("No Such Element present in Page :: " + expEle);
			}
		}
		
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Verify the Attribute Value for List of elements
	 * @param expectedElements - List of element to verify
	 * @param attributeName - name of attribute to verify
	 * @param expectedValue - Expected attribute value
	 * @param obj - page object
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyAttributeForListOfElement(List<String> expectedElements, String attributeName, String expectedValue, Object obj)throws Exception {

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
				BrowserActions.scrollToViewElement(element, driver);
				Log.event("Expected Value :: " + expectedValue);
				Log.event("Actual Value :: " + element.getAttribute(attributeName));
				if (element.getAttribute(attributeName).contains(expectedValue)) {
					actual_elements.add(expEle);
				}
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			} catch (NoSuchElementException e){
				Log.reference("No Such Element present in Page :: " + expEle);
			} catch (NullPointerException e){
				Log.reference("Element throws Null Pointer Exception :: " + expEle);
			}
		}
		
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Wrapper to verify if the first element is right to the second element in the UI
	 * @param driver - WebDriver Instance
	 * @param rightElement - WebElement which is present right side in the UI
	 * @param leftElement - WebElement which is present left side in the UI
	 * @param obj - page object
	 * @return Boolean
	 * @throws Exception
	 */
	public Boolean verifyHorizontalAllignmentOfElements(WebDriver driver, String rightElement, String leftElement, Object obj)throws Exception {

		Field f = obj.getClass().getDeclaredField(rightElement);
		f.setAccessible(true);
		WebElement elementRight = ((WebElement) f.get(obj));

		Field f1 = obj.getClass().getDeclaredField(leftElement);
		f1.setAccessible(true);
		WebElement elementLeft = ((WebElement) f1.get(obj));
		
		Point eleRight = null;
		Point eleLeft = null;
		
		try {
			BrowserActions.scrollInToView(elementRight, driver);
			eleRight = elementRight.getLocation();
		}catch(NoSuchElementException x) {
			Log.event("Right element not displaying : "+rightElement);
			return false;
		}

		try {
			BrowserActions.scrollInToView(elementLeft, driver);
			eleLeft = elementLeft.getLocation();
		} catch(NoSuchElementException x) {
			Log.event("Left element not displaying : "+leftElement);
			return false;
		}
		
		Log.event(eleLeft.x + "-->" + eleRight.x);
		return (eleLeft.x < eleRight.x);
	}
	
	/**
	 * Wrapper to verify if the first element is right to the second element in the UI
	 * @param driver - WebDriver Instance
	 * @param rightElement - WebElement which is present right side in the UI
	 * @param leftElement - WebElement which is present left side in the UI
	 * @param obj - page object
	 * @return Boolean
	 * @throws Exception
	 */
	public Boolean verifyHorizontalAllignmentOfElementsWithoutScroll(
			WebDriver driver, String rightElement, String leftElement, Object obj) throws Exception {

		Field f = obj.getClass().getDeclaredField(rightElement);
		f.setAccessible(true);
		WebElement elementRight = ((WebElement) f.get(obj));

		Field f1 = obj.getClass().getDeclaredField(leftElement);
		f1.setAccessible(true);
		WebElement elementLeft = ((WebElement) f1.get(obj));
		
		Point eleRight = null;
		Point eleLeft = null;
		
		if (Utils.waitForElement(driver, elementRight)) {
			eleRight = elementRight.getLocation();
		} else {
			Log.event("Right element not displaying : "+rightElement);
			return false;
		}
		
		if (Utils.waitForElement(driver, elementLeft)) {
			eleLeft = elementLeft.getLocation();
		} else {
			Log.event("Left element not displaying : "+leftElement);
			return false;
		}
		
		Log.event(eleLeft.x + "-->" + eleRight.x);
		return (eleLeft.x < eleRight.x);
	}

	/**
	 * To verify elements in the list are horizontally aligned
	 * @param element - Name of element
	 * @param obj - Page object element declared in
	 * @return boolean - true if aligned correctly else false
	 * @throws Exception - Exception
	 */
	public boolean verifyHorizontalAlignmentOfListElementItem(String element, Object obj) throws Exception{
		boolean status = true;
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		@SuppressWarnings("unchecked")
		List<WebElement> elementList = (List<WebElement>) (f.get(obj));
		int size = elementList.size();
		if(size > 1) {
			for(int k=0; k < size - 1; k++) {

				WebElement elemLeft = elementList.get(k);
				WebElement elemRight = elementList.get(k+1);

				Point eleRight = elemRight.getLocation();
				Point eleLeft = elemLeft.getLocation();

				Log.event(eleLeft.x + "-->" + eleRight.x);

				if(eleLeft.x > eleRight.x) {
					status = false;
				}
			}
		} else {
			Log.message("The size of the list element is "+size, driver);
			return false;
		}
		return status;
	}

	/**
	 * To verify elements in the list are veritically aligned
	 * @param element - Name of element
	 * @param obj - Page object element declared in
	 * @return boolean - true is aligned correctly else false
	 * @throws Exception - Exception
	 */
	public boolean verifyVerticalAlignmentOfListElementItem(String element, Object obj) throws Exception{
		boolean status = true;
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		@SuppressWarnings("unchecked")
		List<WebElement> elementList = (List<WebElement>) (f.get(obj));
		int size = elementList.size();
		if(size > 1) {
			for(int k=0; k < size - 1; k++) {

				WebElement elemAbove = elementList.get(k);
				WebElement elemBelow = elementList.get(k+1);

				Point eleAbove = elemAbove.getLocation();
				Point eleBelow = elemBelow.getLocation();

				Log.event(eleAbove.y + "-->" + eleBelow.y);

				if(eleAbove.y > eleBelow.y) {
					status = false;
				}
			}
		} else {
			Log.message("The size of the list element is "+size, driver);
			return false;
		}
		return status;
	}

	/**
	 * Wrapper to verify if the first element is above the second element in the UI
	 * @param driver - WebDriver Instance
	 * @param aboveElement - WebElement which is present above in the UI
	 * @param belowElement - WebElement which is present below in the UI
	 * @return Boolean
	 * @param obj - page object
	 * @throws Exception -
	 */
	public Boolean verifyVerticalAllignmentOfElements(
			WebDriver driver, String aboveElement, String belowElement, Object obj) throws Exception {

		Field f = obj.getClass().getDeclaredField(aboveElement);
		f.setAccessible(true);
		WebElement elementAbove = ((WebElement) f.get(obj));

		Field f1 = obj.getClass().getDeclaredField(belowElement);
		f1.setAccessible(true);
		WebElement elementBelow = ((WebElement) f1.get(obj));

		try {
			BrowserActions.scrollToView(elementAbove, driver);
		} catch (NoSuchElementException ex) {
			Log.event("***Missing Element :: "+ aboveElement +"***");
			return false;
		}
		if(!Utils.waitForElementFromDOM(driver, elementAbove)){
			Log.event("***Missing Element :: "+ aboveElement +"***");
			return false;
		}

		try {
			BrowserActions.scrollToView(elementBelow, driver);
		} catch(NoSuchElementException exx) {
			Log.event("***Missing Element :: "+ belowElement +"***");
			return false;
		}
		if(!Utils.waitForElementFromDOM(driver, elementBelow)){
			Log.event("***Missing Element :: "+ belowElement +"***");
			return false;
		}

		Point eleAbove = elementAbove.getLocation();
		Point eleBelow = elementBelow.getLocation();
		Log.event(eleAbove.y + "-->" + eleBelow.y);
		return (eleAbove.y < eleBelow.y);
	}
	
	/**
	 * Wrapper to verify if the first element is above the second element in the UI
	 * 
	 * @param driver       - WebDriver Instance
	 * @param aboveElement - WebElement which is present above in the UI
	 * @return Boolean
	 * @param obj          -
	 * @param belowElement - WebElement which is present below in the UI
	 * @throws Exception -
	 */
	public Boolean verifyVerticalAllignmentOfElementsWithoutScrolling(
			WebDriver driver, String aboveElement, String belowElement, Object obj) throws Exception {

		Field f = obj.getClass().getDeclaredField(aboveElement);
		f.setAccessible(true);
		WebElement elementAbove = ((WebElement) f.get(obj));

		Field f1 = obj.getClass().getDeclaredField(belowElement);
		f1.setAccessible(true);
		WebElement elementBelow = ((WebElement) f1.get(obj));

		if(!Utils.waitForElementFromDOM(driver, elementAbove)){
			Log.event("***Missing Element :: "+ aboveElement +"***");
			return false;
		}

		if(!Utils.waitForElementFromDOM(driver, elementBelow)){
			Log.event("***Missing Element :: "+ belowElement +"***");
			return false;
		}

		Point eleAbove = elementAbove.getLocation();
		Point eleBelow = elementBelow.getLocation();
		Log.event(eleAbove.y + "-->" + eleBelow.y);
		return (eleAbove.y < eleBelow.y);
	}

	/**
	 * Wrapper to verify if the first element is above the second element in the UI
	 * @param elementNames - list of element name
	 * @param obj          - page object
	 * @return boolean
	 * @throws Exception
	 */
	public Boolean verifyVerticalAllignmentOfListOfElements(List<String> elementNames, Object obj) throws Exception{
		boolean flag = true;
		List<Integer> elementYPos = new ArrayList<Integer>();
		List<String> missingEle = new ArrayList<String>();
		for(int i = 0; i < elementNames.size(); i++){
			Field f = obj.getClass().getDeclaredField(elementNames.get(i));
			f.setAccessible(true);
			try{
				WebElement element = ((WebElement) f.get(obj));
				elementYPos.add(element.getLocation().y);
				System.out.print("-->>"+element.getLocation().y);
			}catch(NoSuchElementException e){
				missingEle.add(elementNames.get(i));
				flag=false;
			}
		}

		for(int i = 0; i < elementYPos.size()-1; i++){
			for(int j = i+1; j < elementYPos.size(); j++){
				if(elementYPos.get(i) > elementYPos.get(i)){
					flag = false;
				}
			}
		}

		if(missingEle.size() > 0)
			Log.failsoft("Missing element on this page:: " + missingEle);

		return flag;
	}

	/**
	 * Wrapper to verify if the Element order in the UI
	 * @param elementNames - list of elements by expected order
	 * @param obj - page object
	 * @return Boolean
	 * @throws Exception
	 */
	public Boolean verifyHorizondalAllignmentOfListOfElements(List<String> elementNames, Object obj) throws Exception {
		boolean flag = true;
		List<Integer> elementXPos = new ArrayList<Integer>();
		for(int i = 0; i < elementNames.size(); i++){
			Field f = obj.getClass().getDeclaredField(elementNames.get(i));
			f.setAccessible(true);
			WebElement element = ((WebElement) f.get(obj));
			elementXPos.add(element.getLocation().x);
		}

		for(int i = 0; i < elementXPos.size()-1; i++){
			for(int j = i+1; j < elementXPos.size(); j++){
				if(elementXPos.get(i) > elementXPos.get(i)){
					flag = false;
				}
			}
		}
		return flag;
	}

	/**
	 * To verify the Center alignment of an element
	 * @param elementName - list of element name in expected order
	 * @param obj - page object
	 * @return true(if element alignment is expected), false(If element alignment is not as expected
	 * @throws Exception
	 */
	public Boolean verifyCenterAllignmentOfElement(String elementName, Object obj) throws Exception{
		Field f = obj.getClass().getDeclaredField(elementName);
		f.setAccessible(true);
		WebElement element = ((WebElement) f.get(obj));
		int screenWidth = driver.manage().window().getSize().width;
		int xPosOfElement = element.getLocation().x;

		return !(xPosOfElement > (screenWidth/2));
	}

	/**
	 * To scroll to a particular element in the given page object
	 * @param elementName - name of element to scroll to
	 * @param obj - page object
	 * @throws Exception
	 */
	public void scrollToViewElement(String elementName, Object obj)throws Exception{
		Field f = obj.getClass().getDeclaredField(elementName);
		f.setAccessible(true);
		WebElement element = ((WebElement) f.get(obj));
		BrowserActions.scrollToViewElement(element, driver);
	}

	/**
	 * To verify the container have Vertical scroll bar
	 * 
	 * @param innerElementName  - inner element name
	 * @param outterElementName - outer elemnt name
	 * @param obj               - page object
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyHorizondalScrollBar(String innerElementName, String outterElementName, Object obj)throws Exception{

		Field f1 = obj.getClass().getDeclaredField(innerElementName);
		f1.setAccessible(true);
		WebElement innerElement = ((WebElement) f1.get(obj));

		Field f2 = obj.getClass().getDeclaredField(outterElementName);
		f2.setAccessible(true);
		WebElement outterElement = ((WebElement) f2.get(obj));

		Log.event("Inner Element Height :: " + innerElement.getSize().height);
		Log.event("Outter Element Height :: " + outterElement.getSize().height);

		return (innerElement.getSize().height > outterElement.getSize().height);
	}

	/**
	 * To verify container have vertical scroll bar
	 * @param innerElementName - inner element name
	 * @param outterElementName - outer element name
	 * @param obj - page object
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyVerticalScrollBar(String innerElementName, String outterElementName, Object obj)throws Exception{
		Field f1 = obj.getClass().getDeclaredField(innerElementName);
		f1.setAccessible(true);
		WebElement innerElement = ((WebElement) f1.get(obj));

		Field f2 = obj.getClass().getDeclaredField(outterElementName);
		f2.setAccessible(true);
		WebElement outterElement = ((WebElement) f2.get(obj));

		return (innerElement.getSize().width > outterElement.getSize().width);
	}

	/**
	 * To get the page load status of page objects
	 * @param obj - Page Object
	 * @return true/false if page is loaded
	 * @throws Exception
	 */
	public boolean verifyPageLoadStatus(Object obj)throws Exception{
		Field pageLoad = obj.getClass().getDeclaredField("isPageLoaded");
		pageLoad.setAccessible(true);
		boolean status = ((Boolean) pageLoad.get(obj));
		return status;
	}

	/**
	 * To verify the maximum length of the Text field related web elements
	 * @param expectedElements - list of text field elements to verify
	 * @param maxLength - text field length 
	 * @param obj - page object
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyMaximumLengthOfTxtFldsEqualsTo(List<String> expectedElements, int maxLength, Object obj)throws Exception{

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
				BrowserActions.scrollInToView(element, driver);
				if(!Utils.waitForElement(driver, element)){
					Log.reference("No Such Element on Page :: " + expEle);
					continue;
				}
				BrowserActions.scrollToViewElement(element, driver);
				Log.event("--->>>Max Length of " + expEle + " :: " + element.getAttribute("maxlength"));
				if (Integer.parseInt(element.getAttribute("maxlength")) >= maxLength) {
					actual_elements.add(expEle);
				}
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}

		}
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Verify text field allow special characters
	 * @param expectedElements - list of elements to verify
	 * @param specialCharacters - list of special characters to verify against
	 * @param obj - page object
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyTextFieldAllowSpecialCharacters(List<String> expectedElements, List<String> specialCharacters, Object obj)throws Exception{

		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
				BrowserActions.scrollToViewElement(element, driver);
				boolean state = true;
				for(int i=0; i < specialCharacters.size(); i++){
					BrowserActions.typeOnTextField(element, specialCharacters.get(i), driver, "Text Fields");
					String txtInBox = element.getAttribute("value");
					if(!specialCharacters.contains(txtInBox))
						state = false;
				}
				if (state) {
					actual_elements.add(expEle);
				}
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
		}
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * Verify element text count equals to expected
	 * @param element - element name
	 * @param count - text size
	 * @param obj - page object
	 * @return boolean
	 * @throws Exception
	 */
	public boolean VerifyElementTextCountEqualTo(String element, int count, Object obj) throws Exception{
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));

		int cnt = 0;
		if(webElement.getTagName().equals("input")  || webElement.getTagName().equals("textarea"))
			cnt = webElement.getAttribute("value").length();
		else
			cnt = webElement.getText().length();

		if(cnt == 0) cnt = webElement.getText().length();

		Log.event("Expected text count from User :: " + count);
		Log.event("Actual text count from Element :: " + cnt);
		return (cnt == count) ? true : false;
	}

	/**
	 * To verify given inner element is aligned with container element as per given alignment condition
	 * @param innerElement - inside element
	 * @param outerElement - container element
	 * @param alignment - alignment to verify
	 * @param obj - page object
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyInsideElementAlligned(String innerElement, String outerElement, String alignment, Object obj) throws Exception{
		boolean status = false;
		WebElement innerEle = getElement(innerElement, obj);
		WebElement outterEle = getElement(outerElement, obj);
		int innerY, outterY, outterYCenterPoint, innerX, outterX, outterXCenterPoint;

		try {
			Log.event("Inner Element Co-ordinates :: " + innerEle.getLocation());
			Log.event("Outter Element Co-ordinates :: " + outterEle.getLocation());

			switch(alignment){
			case "top":
				innerY = innerEle.getLocation().y;
				outterY = outterEle.getLocation().y;
				outterYCenterPoint = outterEle.getSize().height/2;
				if(innerY >= outterY && innerY <= (outterY+outterYCenterPoint))
					status = true;
				break;

			case "bottom":
				innerY = innerEle.getLocation().y;
				outterY = outterEle.getLocation().y;
				outterYCenterPoint = outterEle.getSize().height/2;
				if(innerY >= (outterY+outterYCenterPoint))
					status = true;
				break;

			case "left":
				innerX = innerEle.getLocation().x;
				outterX = outterEle.getLocation().x;
				outterXCenterPoint = outterEle.getSize().width/2;
				if(innerX >= outterX && innerX <= (outterX+outterXCenterPoint))
					status = true;
				break;

			case "right":
				innerX = innerEle.getLocation().x;
				outterX = outterEle.getLocation().x;
				outterXCenterPoint = outterEle.getSize().width/2;
				if( innerX > (outterX+outterXCenterPoint))
					status = true;
				break;
			}
		}catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.", driver);
		}
		return status;
	}

	/**
	 * Verify element text equals to expected
	 * @param element - element name
	 * @param testString - text to verify
	 * @param obj - page object
	 * @return boolean - true/false if element text equals to expected
	 * @throws Exception
	 */
	public boolean verifyElementTextEqualTo(String element, String testString, Object obj) throws Exception {
		Field f1 = obj.getClass().getDeclaredField(element);
		f1.setAccessible(true);
		WebElement innerElement = ((WebElement) f1.get(obj));

		String text = new String();
		if(innerElement.getTagName().contains("select") || innerElement.getTagName().contains("input"))
			text = innerElement.getAttribute("value");
		else
			text = innerElement.getText().trim();

		Log.event("Expected Text From User :: " + testString);
		Log.event("Actual Text From Element :: " + text);
		return (text.equalsIgnoreCase(testString));
	}
	
	/**
	 * To get element text
	 * @param String - element name
	 * @param Object - page object
	 * @throws Exception - Exception
	 */
	public String getElementText(String element, Object obj) throws Exception {
		Field f1 = obj.getClass().getDeclaredField(element);
		f1.setAccessible(true);
		WebElement innerElement = ((WebElement) f1.get(obj));

		String text = new String();
		if(innerElement.getTagName().contains("select") || innerElement.getTagName().contains("input"))
			text = innerElement.getAttribute("value");
		else
			text = innerElement.getText().trim();
		Log.event("Element text :: " + text);
		return text;
	}

	/**
	 * Verify list element equals text
	 * @param elements -
	 * @param index -
	 * @param testString -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyListElementTextEqualTo(List<String> elements, int index, String testString, Object obj) throws Exception{
		String text = new String();
		for (String expEle : elements) {
			Field f = null;			
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			List<WebElement> innerElement = null;
			try {
				innerElement = ((List<WebElement>) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}

			if(innerElement.get(index).getTagName().contains("select") || innerElement.get(index).getTagName().contains("input"))
				text = innerElement.get(index).getAttribute("value");
			else
				text = innerElement.get(index).getText().trim();
		}
		
		Log.event("Expected text:: " + testString);
		Log.event("Actual Element text:: " + text);
		return (text.equals(testString));
	}

	/**
	 * To verify the email address from an element is in correct format
	 * @param elementString -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyEmailAddress(String elementString, Object obj) throws Exception {
		String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

		Field f1 = obj.getClass().getDeclaredField(elementString);
		f1.setAccessible(true);
		WebElement element = ((WebElement) f1.get(obj));

		if(!Utils.waitForElement(driver, element)){
			Log.reference("No Such Element on the Page :: " + elementString);
			return false;
		}
		BrowserActions.scrollToViewElement(element, driver);
		String text = new String();
		if(element.getTagName().contains("select") || element.getTagName().contains("input"))
			text = element.getAttribute("value");
		else
			text = element.getText().trim();

		Log.event("Extracted Text :: " + text);

		return text.matches(EMAIL_REGEX);
	}

	/**
	 * Verify element contains verification text
	 * 
	 * @param elementString - element to verify
	 * @param testString - text to verify for
	 * @param obj - page object
	 * @return true/false if verification text is present
	 * @throws Exception
	 */
	public boolean verifyElementTextContains(String elementString, String testString, Object obj) throws Exception {
		Field f1 = obj.getClass().getDeclaredField(elementString);
		f1.setAccessible(true);
		WebElement element = ((WebElement) f1.get(obj));

		if(!Utils.waitForElement(driver, element)){
			Log.reference("No Such Element on the Page :: " + elementString);
			return false;
		}
		BrowserActions.scrollToViewElement(element, driver);
		String actualText = new String();
		if(element.getTagName().contains("select") || element.getTagName().contains("input") || element.getTagName().contains("textarea"))
			actualText = element.getAttribute("value");
		else
			actualText = element.getAttribute("innerText").trim();

		Log.event("Expected Text :: " + testString);
		Log.event("Actual Text :: " + actualText);
		return actualText.toLowerCase().contains(testString.toLowerCase().trim());
	}
	
	/**
	 * To verify dropdown are empty or not
	 * @Param String - elementString
	 * @return true if dropdown doesn't contain any value
	 * @throws Exception
	 */
	public boolean verifyElementEmpty(String elementString, Object obj) throws Exception {
		Field f1 = obj.getClass().getDeclaredField(elementString);
		f1.setAccessible(true);
		WebElement element = ((WebElement) f1.get(obj));

		if(!Utils.waitForElement(driver, element)){
			Log.reference("No Such Element on the Page :: " + elementString);
			return false;
		}
		
		try{
			BrowserActions.scrollToViewElement(element, driver);
			String text = new String();
			if(element.getTagName().contains("select") || element.getTagName().contains("input") || element.getTagName().contains("textarea"))
				text = element.getAttribute("value");
			else
				text = element.getText().trim();
			if(text.isEmpty())
				return true;
		}catch(NullPointerException ex){
			return true;
		}
		
		return false;
	}
	
	/**
	 *  To verify element text contains string
	 * @param listElementString - element to verify
	 * @param index - index of list element
	 * @param testString - string to verify for
	 * @param obj - page object
	 * @return true/false if string is found
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public boolean verifyParticularListElementTextContains(String listElementString, int index, String testString, Object obj)throws Exception {
		Field f1 = obj.getClass().getDeclaredField(listElementString);
		f1.setAccessible(true);
		List<WebElement> elements = ((List<WebElement>) f1.get(obj));
		WebElement element = elements.get(index-1);
		
		if(!Utils.waitForElement(driver, element)){
			Log.reference("No Such Element on the Page :: " + listElementString);
			return false;
		}
		BrowserActions.scrollToViewElement(element, driver);
		String text = new String();
		if(element.getTagName().contains("select") || element.getTagName().contains("input") || element.getTagName().contains("textarea"))
			text = element.getAttribute("value");
		else
			text = element.getText().trim();

		Log.event("Expected Text :: " + testString);
		Log.event("Actual Text :: " + text);
		return (text.toLowerCase().contains(testString.toLowerCase()))?true : false;
	}

	/**
	 * To verify selected color swatches product set
	 * @param expectedElements - list of element names
	 * @param obj - page object
	 * @return true/false if swatched match
	 * @throws Exception
	 */
	public boolean verifySelectedcolorSwatchesProdSet(List<String> expectedElements, Object obj) throws Exception {
		List<String> actual_elements = new ArrayList<String>();
		for (String expEle : expectedElements) {
			Field f = null;
			try {
				f = obj.getClass().getDeclaredField(expEle);
				f.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new Exception("No such a field present on this page, Please check the expected list values:: " + expEle);
			}
			WebElement element = null;
			try {
				element = ((WebElement) f.get(obj));
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				Log.exception(e1);
			}
			if (Utils.waitForElement(driver, element, 2)) {
				actual_elements.add(expEle);
			}
		}
		return CollectionUtils.compareTwoList(expectedElements, actual_elements);
	}

	/**
	 * To verify element is displayed left side inside / of container
	 * @param container - name of container element 
	 * @param element - name of inner element
	 * @param obj - page object
	 * @return true/false if element is displayed as expected
	 * @throws Exception
	 */
	public boolean verifyElementDisplayedleft(String container, String element, Object obj)throws Exception{
		Field containerField = obj.getClass().getDeclaredField(container);
		Field elementField = obj.getClass().getDeclaredField(element);
		containerField.setAccessible(true);
		elementField.setAccessible(true);
		WebElement containerEle = ((WebElement) containerField.get(obj));
		WebElement elementEle = ((WebElement) elementField.get(obj));

		int container_x = 0;
		int container_size_width = 0;
		int element_x = 0;
		
		try {
			container_x = containerEle.getLocation().x;
			container_size_width = containerEle.getSize().width;
			element_x = elementEle.getLocation().x;
		} catch (NoSuchElementException ex) {
			return false;
		}
		
		int containerCenterPoint = container_x + (container_size_width/2);
		return (element_x < containerCenterPoint);
	}

	/**
	 * To verify the first element displayed below the second element
	 * @param firstElement -
	 * @param secondElement -
	 * @param obj -
	 * @return 'true' / 'false'
	 * @throws Exception -
	 */
	public boolean verifyElementDisplayedOverlay(String firstElement, String secondElement, Object obj)throws Exception{
		Field f = obj.getClass().getDeclaredField(firstElement);
		f.setAccessible(true);
		WebElement firstEle = ((WebElement) f.get(obj));

		Field f1 = obj.getClass().getDeclaredField(secondElement);
		f1.setAccessible(true);
		WebElement secondEle = ((WebElement) f1.get(obj)); 

		return (firstEle.getLocation().getY() <= secondEle.getLocation().getY());
	}

	/**
	 * To verify text field element allows character type
	 * @param elementString -
	 * @param characterType -
	 * @param obj -
	 * @param elementDesc -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyTxtFldElementAllows(String elementString, String characterType, Object obj, String elementDesc)throws Exception{
		boolean stateToBeReturned = false;
		String dataToChk = new String();

		Field f = obj.getClass().getDeclaredField(elementString);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		if(!Utils.waitForElement(driver, webElement)) {
			Log.reference("No Such Element present in Page :: " + elementString);
			return false;
		}
		BrowserActions.scrollToViewElement(webElement, driver);

		switch (characterType) {
		case "number":
			BrowserActions.typeOnTextField(webElement, "1234567890", driver, elementDesc);
			dataToChk = BrowserActions.getText(driver, webElement, elementDesc);
			stateToBeReturned = ("1234567890").contains(dataToChk) ? true : false;
			Log.message("--->>>Tried to type Numbers", driver);
			break;

		case "alphabet":
			BrowserActions.typeOnTextField(webElement, "AaBbCcDd", driver, elementDesc);
			webElement.sendKeys(Keys.TAB);
			dataToChk = BrowserActions.getText(driver, webElement, elementDesc);
			stateToBeReturned = (dataToChk.equals("AaBbCcDd")) ? true : false;
			Log.message("--->>>Tried to type Alphabets", driver);
			break;

		case "special":
			BrowserActions.typeOnTextField(webElement, "@#$%&*,./", driver, elementDesc);
			dataToChk = BrowserActions.getText(driver, webElement, elementDesc);
			stateToBeReturned = (dataToChk.equals("@#$%&*,./")) ? true : false;
			Log.message("--->>>Tried to type Special Characters", driver);
			break;

		default:
			BrowserActions.typeOnTextField(webElement, characterType, driver, elementString);
			dataToChk = BrowserActions.getText(driver, webElement, elementString);
			stateToBeReturned = (dataToChk.equals(characterType)) ? true : false;
			Log.message("--->>>Tried to type Special Characters", driver);
			break;
		}

		return stateToBeReturned;
	}

	/**
	 * To verify list of field element allows specified type
	 * @param elementString -
	 * @param characterType -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyListOfTxtFldElementAllows(List<String> elementString, List<String> characterType, Object obj)throws Exception{
		List<String> missedEle = new ArrayList<String>();
		for(int i = 0; i < elementString.size(); i++){
			boolean stateToBeReturned = false;
			String dataToChk = new String();

			Field f = obj.getClass().getDeclaredField(elementString.get(i));
			f.setAccessible(true);
			WebElement webElement = ((WebElement) f.get(obj));
			BrowserActions.scrollInToView(webElement, driver);


			switch (characterType.get(i)) {
			case "number":
				BrowserActions.typeOnTextField(webElement, "1234567890", driver, elementString.get(i));
				dataToChk = BrowserActions.getText(driver, webElement, elementString.get(i));
				stateToBeReturned = (dataToChk.equals("1234567890")) ? true : false;
				break;

			case "alphabet":
				BrowserActions.typeOnTextField(webElement, "AaBbCcDd", driver, elementString.get(i));
				dataToChk = BrowserActions.getText(driver, webElement, elementString.get(i));
				stateToBeReturned = (dataToChk.equals("AaBbCcDd")) ? true : false;
				break;

			case "special":
				BrowserActions.typeOnTextField(webElement, "@#$%&*,./", driver, elementString.get(i));
				dataToChk = BrowserActions.getText(driver, webElement, elementString.get(i));
				stateToBeReturned = (dataToChk.equals("@#$%&*,./")) ? true : false;
				break;

			default:
				BrowserActions.typeOnTextField(webElement, characterType.get(i), driver, elementString.get(i));
				dataToChk = BrowserActions.getText(driver, webElement, elementString.get(i));
				stateToBeReturned = (dataToChk.equals(characterType.get(i))) ? true : false;
				break;
			}

			if(!stateToBeReturned)
				missedEle.add(elementString.get(i));

			BrowserActions.scrollInToView(webElement, driver);
			webElement.clear();
		}

		if(missedEle.size() > 0){
			Log.failsoft("Missed Elements :: " + missedEle.toString());
			return false;
		}else{
			Log.event("Verified All Elements :: " + elementString.toString());
			return true;
		}
	}

	/**
	 * To verify element text with demandware property
	 * @param elements -
	 * @param dmwrProp -
	 * @param obj -
	 * @return status as boolean 
	 * @throws Exception - 
	 */
	public boolean verifyElementTextWithDWREProp(List<String> elements, String dmwrProp, Object obj)throws Exception{
		boolean stateToReturn = true;
		for(int i = 0 ; i < elements.size(); i++){
			WebElement element = getElement(elements.get(i), obj);
			if(!(BrowserActions.getText(driver, element, "element").toLowerCase().equals(demandWareProperty.getProperty(dmwrProp).toLowerCase()))){
				stateToReturn = false;
			}
		}
		
		return stateToReturn;
	}

	/**
	 * To verify text with demandware property
	 * @param textToVerify  -
	 * @param dmwrProp -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyTextWithDWREProp(List<String> textToVerify, String dmwrProp, Object obj)throws Exception{
		boolean stateToReturn = true;
		for(int i = 0 ; i < textToVerify.size(); i++){
			if(!(textToVerify.get(i).toLowerCase().contains(demandWareProperty.getProperty(dmwrProp).toLowerCase()))){
				stateToReturn = false;
				Log.failsoft("Text :: " + textToVerify.get(i) + "  ---  DWRE Prop Value :: " + demandWareProperty.getProperty(dmwrProp));
			}
		}

		return stateToReturn;
	}

	/**
	 * To verify text is available in the element 
	 * @param elementToVerify -
	 * @param textToVerify -
	 * @param obj -
	 * @param scroll - whether to scroll to bring element into viewport
	 * @return status as boolean
	 */
	public boolean verifyTextContains(String elementToVerify, String textToVerify, Object obj, boolean... scroll) {
		boolean status = false;
		WebElement element = null;
		try{
			Field elementField = obj.getClass().getDeclaredField(elementToVerify);
			elementField.setAccessible(true);
			element = ((WebElement) elementField.get(obj));
			if (scroll.length == 0 || scroll[0]) {
				BrowserActions.scrollInToView(element, driver);
			}
			String txtFrmEle = element.getText().trim().replaceAll("\\s+", " ");

			Log.event("Expected Text From user :: " + textToVerify);
			Log.event("Actual Text From Element :: " + txtFrmEle);

			if (txtFrmEle.toLowerCase().contains(textToVerify.toLowerCase())) {
				status = true;
			}
		}//try
		catch(Exception e){
			e.printStackTrace();
			Log.event("Element not found.");
		}//catch

		return status;
	}

	/**
	 * To verify text is available in the element 
	 * @param elementToVerify -
	 * @param textToVerify -
	 * @param obj -
	 * @return status as boolean
	 */
	public boolean verifyTextContainsForListElement(String elementToVerify, String textToVerify, Object obj) {
		boolean status = false;
		try{
			List<WebElement> listElement = getListElement(elementToVerify, obj);
			for(WebElement element : listElement) {
				String txtFrmEle = element.getText().trim().replaceAll("\\s+", " ");

				Log.event("Expected Text From user :: " + textToVerify);
				Log.event("Actual Text From Element :: " + txtFrmEle);

				if (txtFrmEle.toLowerCase().contains(textToVerify.toLowerCase())) {
					status = true;
				}
			}
		}//try
		catch(Exception e){
			e.printStackTrace();
		}//catch

		return status;
	}

	/**
	 * To verify placeholder moves above
	 * @param txtBoxElementString -
	 * @param lblElementString -
	 * @param valueToEnter -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyPlaceHolderMovesAbove(String txtBoxElementString, String lblElementString, String valueToEnter, Object obj)throws Exception{
		WebElement txtBox = getElement(txtBoxElementString, obj);
		WebElement lblBox = getElement(lblElementString, obj);

		BrowserActions.scrollInToView(txtBox, driver);
		if(!Utils.waitForElement(driver, txtBox)) {
			Log.reference("No Such Element present in Page :: " + txtBoxElementString);
			return false;
		}
		BrowserActions.typeOnTextField(txtBox, "", driver, txtBoxElementString);
		int y1 = lblBox.getLocation().y;
		Log.event(lblElementString + " Initial Position of Place Holder :: " + y1);
		BrowserActions.typeOnTextField(txtBox, valueToEnter, driver, txtBoxElementString);
		int y2 = lblBox.getLocation().y;
		Log.event(lblElementString + " After Text entered, Position of Place Holder :: " + y2);

		return (y1 > y2);
	}

	/**
	 * To verify placeholder moves above for list of elements
	 * @param txtBoxElement -
	 * @param lblElement -
	 * @param valueToEnter -
	 * @param obj -
	 * @return status as boolean
	 * @throws Exception -
	 */
	public boolean verifyPlaceHolderMovesAboveForList(List<String> txtBoxElement, List<String> lblElement, List<String> valueToEnter, Object obj)throws Exception{
		List<String> txtBoxEleActual = new ArrayList<String>();
		List<String> txtBoxEleMissed = new ArrayList<String>();
		for(int i=0; i < txtBoxElement.size(); i++ ){
			boolean stat = verifyPlaceHolderMovesAbove(txtBoxElement.get(i), lblElement.get(i), valueToEnter.get(i), obj);

			if(stat)
				txtBoxEleActual.add(txtBoxElement.get(i));
			else
				txtBoxEleMissed.add(txtBoxElement.get(i));
		}

		if(CollectionUtils.compareTwoList(txtBoxElement, txtBoxEleActual)){
			Log.event("Checkout All elements in Page :: " + txtBoxElement.toString());
			return true;
		}else{
			Log.failsoft("Missed Elements :: " + txtBoxEleMissed.toString());
			return false;
		}
	}

	/**
	 * To verify text element is not empty
	 * @param elementNames -
	 * @param obj -
	 * @return status as boolean 
	 * @throws Exception -
	 */
	public boolean verifyTxtElementsNotEmpty(List<String> elementNames, Object obj)throws Exception{
		List<String> missedEle = new ArrayList<String>();
		for(String ele : elementNames){
			WebElement txtBox = getElement(ele, obj);
			BrowserActions.scrollToViewElement(txtBox, driver);
			String value = BrowserActions.getText(driver, txtBox, ele);
			if(value.equals("") || value.isEmpty())
				missedEle.add(ele);

		}
		if(missedEle.size() > 0){
			Log.failsoft("Missed Elements :: " + missedEle.toString());
			return false;
		}else{
			Log.event("Verified All Elements :: " + elementNames.toString());
			return true;
		}
	}

	/**
	 * To verify element is empty
	 * @param elementNames -
	 * @param obj -
	 * @return status as boolean 
	 * @throws Exception -
	 */
	public boolean verifyTxtElementsIsEmpty(List<String> elementNames, Object obj)throws Exception{
		List<String> missedEle = new ArrayList<String>();
		for(String ele : elementNames){
			WebElement txtBox = getElement(ele, obj);
			BrowserActions.scrollToViewElement(txtBox, driver);
			String value = BrowserActions.getText(driver, txtBox, ele);
			if(!value.equals("") || !value.isEmpty())
				missedEle.add(ele);

		}
		if(missedEle.size() > 0){
			Log.failsoft("Missed Elements :: " + missedEle.toString());
			return false;
		}else{
			Log.event("Verified All Elements :: " + elementNames.toString());
			return true;
		}
	}

	/**
	 * To verify the element color
	 * @param elementName - Name of element
	 * @param expectedColor - Expected color code
	 * @param obj - page object containing elementName
	 * @return true/false if color elementName color matches with colorCode
	 * @throws Exception
	 */
	public boolean verifyElementColor(String elementName, String expectedColor, Object obj)throws Exception{
		int colorDeltaTolerance = StringUtils.getNumberInString(configProperty.get("colorDeltaTolerance"));
		WebElement element = getElement(elementName, obj);
		BrowserActions.scrollToViewElement(element, driver);
		String elementColor = element.getCssValue("color");
		
		Log.event("Expected Color code:: " + expectedColor);
		Log.event("Actual Element Color code:: " + elementColor);
		return ColorUtils.getColorDeltaE(expectedColor, elementColor) < colorDeltaTolerance;
	}

	/**
	 * To verify list of element colors
	 * @param lstElementNames -
	 * @param colorCode -
	 * @param obj -
	 * @return true/false if all list elements match expected color
	 * @throws Exception -
	 */
	public boolean verifyListOfElementColor(List<String> lstElementNames, String colorCode, Object obj) throws Exception {
		List<String> lstMismatchedElement = new ArrayList<String>();
		for (String element : lstElementNames) {
			if (!verifyElementColor(element, colorCode, obj))
				lstMismatchedElement.add(element);
		}

		if (lstMismatchedElement.size() > 0) {
			Log.failsoft("Elements not matching color:: " + lstMismatchedElement.toString());
			return false;
		}
		return true;
	}

	/**
	 * To clear the specified text fields
	 * @param elements -
	 * @param obj -
	 * @throws Exception -
	 */
	public void clearTextFields(List<String> elements, Object obj)throws Exception{
		for(String ele : elements){
			WebElement element = getElement(ele, obj);
			element.clear();
		}
	}

	/**
	 * To get tagname of the element
	 * @param ele -
	 * @param obj -
	 * @return tagname 
	 * @throws Exception -
	 */
	public String getTagName(String ele, Object obj) throws Exception{
		Field f = null;		
		String tag=null;
		try {
			f = obj.getClass().getDeclaredField(ele);
			f.setAccessible(true);
		} catch (NoSuchFieldException | SecurityException e1) {
			throw new Exception("No such a field present on this page, Please check the value:: " + ele);
		}
		WebElement element = null;
		try {
			element = ((WebElement) f.get(obj));
			element.getAttribute("innerHTML");
			tag=element.getTagName();


		} catch (IllegalArgumentException | IllegalAccessException e1) {
			Log.exception(e1);
		} catch (NoSuchElementException e2){
			Log.failsoft("Required Element["+ele+"] not displayed in Page Test.", driver);

		}
		return tag;
	}

	/**
	 * To verify given list of elements displayed in page
	 * @param elements - 
	 * @param obj - 
	 * @return boolean - 
	 * @throws Exception -
	 */
	public boolean isElementsDisplayed(List<String> elements, Object obj)throws Exception{
		List<String> missedElements = new ArrayList<String>();
		boolean status = true;
		for(String element : elements){
			WebElement ele = getElement(element, obj);
			if(!Utils.waitForElement(driver, ele)){
				missedElements.add(element);
				status = false;
			}
		}

		Log.event("Missed Elements :: " + missedElements.toString());
		return status;
	}

	/**
	 * To verify given elements are same
	 * @param element1 -
	 * @param element2 -
	 * @param obj -
	 * @return Boolean -
	 * @throws Exception -
	 */
	public Boolean verifyElementsAreInSameRow(String element1, String element2, Object obj)throws Exception {
		Point ele1 = getElement(element1, obj).getLocation();
		Point ele2 = getElement(element1, obj).getLocation();

		Log.event("Element-1 Position :: " + ele1.y);
		Log.event("Element-2 Position :: " + ele2.y);

		return (ele1.y == ele2.y);
	}

	/**
	 * To get height of given element
	 * @param element -
	 * @param obj -
	 * @return Integer -
	 * @throws Exception -
	 */
	public int getElementHeight(String element, Object obj)throws Exception{
		return getElement(element, obj).getSize().height;
	}

	/**
	 * To get width of given element
	 * @param element -
	 * @param obj -
	 * @return  Integer -
	 * @throws Exception -
	 */
	public int getElementWidth(String element, Object obj)throws Exception{
		return getElement(element, obj).getSize().width;
	}

	public boolean verifyMaxElementsInRow(String listElement, int maxCountInRow, Object obj)throws Exception{
		List<WebElement> listEle = getListElement(listElement, obj);
		int lstElementNumber = listEle.size();
		int currentFirstElement = 0;
		while(currentFirstElement < lstElementNumber) {
			int rowFirstY = listEle.get(currentFirstElement).getLocation().y;
			for(int i=currentFirstElement+1 ; i<(currentFirstElement+maxCountInRow) && i<(lstElementNumber); i++) {
				int currentElementY = listEle.get(i).getLocation().y;
				Log.event(currentFirstElement+":" + rowFirstY + " :: "+i+":"+currentElementY);
				if(rowFirstY != currentElementY) return false;
			}
			currentFirstElement = currentFirstElement + maxCountInRow;
		}
		return true;
	}
	
	/**
	 * To get element CSS value
	 * @param element - element
	 * @return cssProperty - cssValue
	 * @param obj - obj
	 * @throws Exception - e
	 * */
	public String getElementCSSValue(String element, String cssProperty, Object obj) throws Exception{
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		BrowserActions.scrollToView(webElement, driver);
		String cssValue = webElement.getCssValue(cssProperty);
		Log.event(cssProperty + " of Given Element is :: " + cssValue);
		
		return cssValue;
	}
	
	/**
	 * To get computed CSS value of an element
	 * @param element - element name to get CSS value of
	 * @param cssProperty - CSS property to get value
	 * @param obj - page object the element is declared in
	 * @return String - Computed CSS value of element
	 * @throws Exception - Exception  
	 */
	public String getComputedCssValueForElement(String element, String cssProperty, Object obj)throws Exception {
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		String computedCSSValue = (String) executor.executeScript("return window.getComputedStyle(arguments[0]).getPropertyValue('"+cssProperty+"')",webElement);

		Log.event("Element Computed CSS Value :: " + computedCSSValue);
		return computedCSSValue;
	}
	
	/**
	 * To extract numerical value from element
	 * @param element - name of element
	 * @param obj - page object element declared in
	 * @throws Exception - Exception
	 */
	public int getNumberInElement(String element, Object obj) throws Exception{
		Field f = obj.getClass().getDeclaredField(element);
		f.setAccessible(true);
		WebElement webElement = ((WebElement) f.get(obj));
		String numberAsStr = webElement.getText().replaceAll("[^0-9]", "");
		
		if(numberAsStr.length() == 0) {
			Log.event("No number found in Element");
			return 0;
		}
		
		else {
			int valueFromElement = Integer.parseInt(numberAsStr);
			Log.event("Number found in Element :: " + valueFromElement);
			return valueFromElement;
		}
	}
	
	/**
	 * To verify if the first element is above the second element in the UI
	 * @param driver - WebDriver Instance
	 * @param aboveElement - WebElement which is present above in the UI
	 * @param belowElement - WebElement which is present below in the UI
	 * @param obj - Page object element is declared in
	 * @return Boolean - true/false if first element is above second element
	 * @throws Exception - Exception
	 */
	public Boolean verifyVerticalAllignmentOfElementsWithoutPadding(WebDriver driver, String aboveElement, String belowElement, Object obj) throws Exception {

		Field f = obj.getClass().getDeclaredField(aboveElement);
		f.setAccessible(true);
		WebElement elementAbove = ((WebElement) f.get(obj));

		Field f1 = obj.getClass().getDeclaredField(belowElement);
		f1.setAccessible(true);
		WebElement elementBelow = ((WebElement) f1.get(obj));

		if(!Utils.waitForElementFromDOM(driver, elementAbove)){
			Log.event("***Missing Element :: "+ aboveElement +"***");
			return false;
		}

		if(!Utils.waitForElementFromDOM(driver, elementBelow)){
			Log.event("***Missing Element :: "+ belowElement +"***");
			return false;
		}

		Point eleAbove = elementAbove.getLocation();
		int elementHeightAbove = eleAbove.y;
		if(elementAbove.getCssValue("padding-top").length()!=0) {
			elementHeightAbove = elementHeightAbove + StringUtils.getNumberInString(elementAbove.getCssValue("padding-top"));
		}
		Point eleBelow = elementBelow.getLocation();
		int elementHeightBelow = eleBelow.y;
		if(elementBelow.getCssValue("padding-top").length()!=0) {
			elementHeightBelow = elementHeightBelow + StringUtils.getNumberInString(elementBelow.getCssValue("padding-top"));
		}
		Log.event(elementHeightAbove + "-->" + elementHeightBelow);
		return (elementHeightAbove < elementHeightBelow);
	}
	
	/**
	 * To verify an element is within bound of another element
	 * @param WebDriver - driver
	 * @param String - inner element name
	 * @param String outer element name
	 * @param Object - page object elements declared in
	 * @return boolean - true/false if inner element is within bounds
	 * @throws Exception - Exception 
	 */
	public boolean verifyElementWithinElement(WebDriver driver, String innerElement, String outerElement, Object obj) throws Exception{
		Field f = obj.getClass().getDeclaredField(innerElement);
		f.setAccessible(true);
		WebElement elementInner = ((WebElement) f.get(obj));
		Field f1 = obj.getClass().getDeclaredField(outerElement);
		f1.setAccessible(true);
		WebElement elementOuter = ((WebElement) f1.get(obj));

		if(!Utils.waitForElementFromDOM(driver, elementInner)){
			Log.event("***Missing Element :: "+ innerElement +"***");
			return false;
		}
		if(!Utils.waitForElementFromDOM(driver, elementOuter)){
			Log.event("***Missing Element :: "+ outerElement +"***");
			return false;
		}
		
		if((elementInner.getSize().height > elementOuter.getSize().height) || (elementInner.getSize().width > elementOuter.getSize().width)) {
			return false;
		}
		boolean horizontalBound = (elementInner.getLocation().x >= elementOuter.getLocation().x) &&
				(elementInner.getLocation().x + elementInner.getSize().width <= elementOuter.getLocation().x + elementOuter.getSize().width);
		boolean verticalBound = (elementInner.getLocation().y >= elementOuter.getLocation().y) &&
				(elementInner.getLocation().y + elementInner.getSize().height <= elementOuter.getLocation().y + elementOuter.getSize().height);
		
		return horizontalBound && verticalBound;
	}
	
	/**
	 * To verify is given radio button is selected or not
	 * @param String - name of radio button 
	 * @param boolean - selection state to verify
	 * @param Object - page object
	 * @return boolean - true/false if radio button is in expected state
	 * @throws Exception - Exception
	 */
	public boolean verifyRadioButtonSelection(String element, boolean stateExpected, Object obj)throws Exception{
		if(stateExpected) {
			return verifyCssPropertyForElement(element, "background-image", "circle-selected.svg", obj) 
					|| verifyCssPropertyForElement(element, "background-position", "-32px -5px", obj);
		}
		if(!stateExpected) {
			return verifyCssPropertyForElement(element, "background-image", "circle-outline.svg", obj) 
					|| verifyCssPropertyForElement(element, "background-position", "-5px -5px", obj);
		}
		else 
			return false;
	}

    /**
     * To verify given input box / text area is editable or not
     * @param driver - WebDriver
     * @param element - element to verify
     * @param obj - Page object
     * @return boolean - true/false
     * @throws Exception - Exception
     */
	public boolean verifyElementIsEditable(WebDriver driver, String element, Object obj)throws Exception{
		boolean flag = false;
		try{
			WebElement ele = getElement(element, obj);

			String valueBefore = BrowserActions.getText(driver, ele, "");
			ele.clear();
			ele.clear();
			ele.sendKeys("word");
			String valueAfter = BrowserActions.getText(driver, ele, "");
			if(valueAfter.equals("word"))
				flag = true;

			ele.clear();
			ele.sendKeys(valueBefore);
			return flag;

		}catch(Exception e){
			Log.exception(e);
		}
		return flag;
	}

    /**
     * To verify field validation for error message
     * @param driver - WebDriver
     * @param elementsToValidate - text fields / text area elements
     * @param elementsToVerify - error message values
     * @param submitElement - submit button element / tab
     * @param obj - Page Object
     * @return boolean - true/false
     * @throws Exception - Exception
     */
	public boolean verifyFieldValidation(WebDriver driver, List<String> elementsToValidate, List<String> elementsToVerify, String submitElement, Object obj)throws Exception{
		boolean flag = true;
		try{
			for(String element : elementsToValidate){
				WebElement ele = getElement(element, obj);
				ele.clear();
				if(submitElement.equals("tab"))
					ele.sendKeys(Keys.TAB);
			}

			if(!submitElement.equals("tab")){
				WebElement ele = getElement(submitElement, obj);
				ele.click();
			}

			for(String element : elementsToVerify){
				WebElement ele = getElement(element, obj);
				if(!Utils.waitForElement(driver, ele)){
					flag = false;
					Log.event("Element not found :: " + element);
				}
			}
		}catch(Exception e){
			Log.exception(e);
		}
		return flag;

	}

    /**
     * To verify zip code field validations for multiple type of zip code formats
     *  like "55555" or "55555-4444"
     * @param driver - WebDriver
     * @param elementName - area code field name
     * @param errorEleName - Error element to be validated
     * @param obj - Page Object
     * @return boolean - true/false
     * @throws Exception - Exception
     */
	public boolean verifyZipCodeValidation(WebDriver driver, String elementName, String errorEleName, Object obj)throws Exception{
	    boolean flag = true;
	    try{
	        WebElement element = getElement(elementName, obj);
	        String eleText = BrowserActions.getText(driver, element, "");
	        element.clear();
	        element.sendKeys("28205");
	        element.sendKeys(Keys.TAB);
	        if(Utils.waitForElement(driver, getElement(errorEleName, obj)))
	            flag = false;

	        element.clear();
	        element.sendKeys("48813-8363");
            element.sendKeys(Keys.TAB);
            if(Utils.waitForElement(driver, getElement(errorEleName, obj)))
                flag = false;
            element.clear();
            element.sendKeys(eleText);
        }catch(Exception e){
            Log.exception(e);
        }

        return flag;
    }
	
	/**
	 * To verify if two given addresses match using Google Geocoding API
	 * @param address1
	 * @param address2
	 * @return true/false if addresses match
	 * @throws Exception
	 */
	public boolean verifyTwoAddressMatch(String address1, String address2) throws Exception{
		if(address1.equalsIgnoreCase(address2)) {
			return true;
		}
		
		String formattedAddress1 = GoogleMapsAPI.getFormattedAddress(address1).toLowerCase();
		String formattedAddress2 = GoogleMapsAPI.getFormattedAddress(address2).toLowerCase();
		boolean match = false;
		if(formattedAddress1.equals(formattedAddress2)) {
			return true;
		} else {
			List<String> addressList1 = Arrays.asList(formattedAddress1.split(","));
			List<String> addressList2 = Arrays.asList(formattedAddress2.split(","));
			match = (addressList1.containsAll(addressList2) || addressList2.containsAll(addressList1));
		}
		
		if(!match) {
			match = verifyPOBoxAddresses(address1, address2);
		}
		
		return match;
	}
	
	/**
	 * To verify addresses as PO Box addresses
	 * @param address1
	 * @param address2
	 * @return if two PO Box addresses match.
	 * @throws Exception
	 */
	public boolean verifyPOBoxAddresses(String address1, String address2) throws Exception{
		address1 = address1.toLowerCase();
		address2 = address2.toLowerCase();
		if(address1.equals(address2)) {
			return true;
		}
		
		List<String> addressAsList1 = StringUtils.splitStringToList(address1);
		List<String> addressAsList2 = StringUtils.splitStringToList(address2);
		String addr1line1 = addressAsList1.get(0);
		String addr2line1 = addressAsList2.get(0);
		
		String zip1 = "", zip2 = "";
		for(String addrSegment : addressAsList1) {
			String tempSegment = addrSegment.replaceAll("[^0-9]", "");
			if(tempSegment.length() == 5 || tempSegment.length() == 9) {
				zip1 = addrSegment;
				break;
			}
		}
		for(String addrSegment : addressAsList2) {
			String tempSegment = addrSegment.replaceAll("[^0-9]", "");
			if(tempSegment.length() == 5 || tempSegment.length() == 9) {
				zip2 = addrSegment;
				break;
			}
		}
		
		boolean match = false;
		if(address1.contains(addr2line1) && address1.contains(zip2)) {
			match = true;
		} else if (address2.contains(addr1line1) && address2.contains(zip1)) {
			match = true;
		}
		return match;
	}

	/**
	 * To get the List web element size
	 * @param string
	 * @return size of the List Web element as integer
	 * @throws Exception 
	 */
	public int getListElementSize(String element, Object obj) throws Exception {
		List<WebElement> elementList=null;
		try {
			elementList = getListElement(element, obj);
		} catch (Exception e) {
			Log.message("Throws exception:"+e);
		}
		int size = elementList.size();
		Log.event("Size of list element " + element + ":: " + size);
		return size;
	}
}