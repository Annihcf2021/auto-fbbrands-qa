package com.fbb.pages;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.reusablecomponents.Enumerations.Toggle;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;

public class Refinements extends LoadableComponent<Refinements> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	//=====================================================================//
	//	-------------------Mobile Elements-----------------------------    //
	//=====================================================================//

	@FindBy(css = "#main .refinements")
	WebElement readyElementMobile;

	@FindBy(css = "#main .refinements")
	WebElement readyElementDesktop;
	
	@FindBy(css = ".pt_product-search-result.product-list-page")
	WebElement readyElementPLP;
	
	@FindBy(id = "search-result-items")
	WebElement readyElementSLP;

	@FindBy(css = ".refinements.ws-sticky")
	WebElement refinementSection;

	@FindBy(css = ".refinements .filters")
	WebElement drpFilterByCollapsed;
	
	@FindBy(css = "a.hide-desktop.hide-tablet.view-results")
	WebElement btnFilterApply;
	
	@FindBy(css = ".breadcrumb-refinement .breadcrumb-refinement-value")
	WebElement selectedFilteredValue;
	
	@FindBy(css = ".breadcrumb-refinement .breadcrumb-refinement-value")
	List<WebElement> lstSelectedRefinementValues;
	
	@FindBy(css = ".toggle-heading .count-values span.selected")
	WebElement selectedfiltervalueMobile;
	
	@FindBy(css = ".filter.no-ajaxcall")
	WebElement drpFilterByArrow;

	@FindBy(css = ".filter>.fa-angle-down")
	WebElement btnFilterArrowDown;

	@FindBy(css = ".filters.refinement-mobile")
	WebElement drpFilterByExpanded;

	@FindBy(css = ".search-result-options.hide-tablet.hide-mobile .sort-by.custom-select")
	WebElement drpSortByCollapsedDesktop;

	@FindBy(css = ".refinements .sort-by")
	WebElement drpSortByCollapseMobileTablet;
	
	@FindBy(css = ".refinements .sort-by.custom-select")
	WebElement sortByRefinemnet;
	
	@FindBy(css = ".hide-desktop .sort-by.custom-select")
	WebElement drpSortByCollapsedMobile;

	@FindBy(css = ".hide-desktop .sort-by .selected-option.selected")
	WebElement drpSortSelectedOption;

	@FindBy(css = ".sort-by.custom-select.current_item")
	WebElement drpSortByExpanded;

	@FindBy(css = ".refinement-heading-category")
	List<WebElement> lstRefinementFilterOptionsDesktop;
	
	@FindBy(css = ".refinements.ws-sticky .refinement")
	List<WebElement> lstFilterRefinements;

	@FindBy(css = ".mobile-refinement .refinement")
	List<WebElement> lstRefinementFilterOptionsMobile;

	@FindBy(css = ".mobile-refinement")
	WebElement refinementFilterOptionsDesktop;

	@FindBy(css = ".mobile-refinement .refinement")
	WebElement refinementFilterOptionsMobile;

	@FindBy(css = ".refinement h3")
	List<WebElement> lstRefinementFilterOptionsInMobile;

	@FindBy(css = ".refinement.active div[class='expended']")
	WebElement divExpandedRefinementSubList;

	@FindBy(css = ".refinement.active div.expended")
	WebElement divExpandedRefinementSubListTablet;

	@FindBy(css = ".refinement-heading-category.expanded.toggle")
	WebElement drpExpandedRefinement;

	@FindBy(css = "div[class='expended'][style*='block'] li")
	List<WebElement> lstExpandedRefinementAllSubList;

	@FindBy(css = "div[class*='expended'][style*='block'] li > div.available span[class^='check-square']")
	List<WebElement> lstExpandedRefinementAvailableSubList;
	
	@FindBy(css = "div[class*='expended'][style*='block'] li a")
	List<WebElement> lstExpandedRefinementAvailable;

	@FindBy(css = ".refinement.active div[class='expended-main'] li a")
	List<WebElement> lstExpandedRefinementAvailableSubListTablet;

	@FindBy(css = ".refinement.active div[class='expended-main'] li > div.available span.check-square")
	WebElement expandedRefinementAvailableSubListTablet;

	@FindBy(css = ".refinement.active>.expended .expended-main li > div.available a")
	List<WebElement> lstCollapsedRefinementAvailableSubList;
	
	@FindBy(css = ".refinement.active>.expended .expended-main li > div.available a")
	WebElement collapsedRefinementAvailability;

	@FindBy(css = ".refinement i.fa.fa-angle-down.fa-2x")
	List<WebElement> lstRefinementDownArrow;

	@FindBy(css = ".refinement.active i.fa-angle-down")
	WebElement lnkRefinementMenuBackTablet;

	@FindBy(css = ".backbutton")
	WebElement lnkRefinementMenuBackMobile;

	@FindBy(css = ".search-result-options.hide-tablet.hide-mobile .result-count span:nth-child(1)")
	WebElement lblRefinementCountDesktop;

	@FindBy(css = ".search-result-options.hide-tablet.hide-mobile .result-count")
	WebElement divResultCountDesktop;

	@FindBy(css = "#category-level-2 .expandable.active")
	WebElement level2Section;

	@FindBy(css = ".result-count.hide-desktop span:nth-child(1)")
	WebElement lblRefinementCountTablet;
	
	@FindBy(css = ".result-count.hide-desktop")
	WebElement divResultCountMobileTablet;

	@FindBy(css = ".filters.refinement-mobile .active .toggle .fa.fa-angle-down.fa-2x")
	WebElement iconReturnToMainRefinementInMobile;

	@FindBy(css = ".filters.refinement-mobile .active .toggle-heading .count.hide-mobile")
	WebElement lblSelectedSubRefCount;

	@FindBy(css = ".filters.refinement-mobile .filter-count")
	WebElement lblSelectedSubRefCountMobile;
	
	@FindBy(css = ".refinements .filter-count")
	WebElement lblSelectedFilterCountTab;

	@FindBy(css = ".hide-desktop.hide-tablet.backbutton.no-ajaxcall")
	WebElement refinementBackButtonMobile;
	
	@FindBy(css = ".apply-button-link")
	WebElement refinementApplyResultsMobile;

	@FindBy(css = ".filters.refinement-mobile .active")
	WebElement activeSubRefinement;

	@FindBy(css = ".refinement-breadcrumb a.device-clear-all")
	WebElement btnClearAllInTablet;

	@FindBy(css = ".device-clear-all.hide-desktop")
	WebElement btnClearAllInMobile;

	@FindBy(css = ".refinement-breadcrumb")
	WebElement divRefinementBar;
	
	@FindBy(css = ".refinement-breadcrumb .refine-by-clearall .device-clear-all")
	WebElement btnClearAllInDesktop;

	@FindBy(css = ".refinement-breadcrumb a.device-clear-all")
	WebElement btnClearInActiveRefInTablet;
	
	@FindBy(css = ".breadcrumb-clearall .breadcrumb-refinement-value a")
	WebElement btnClearAllRefinementTablet;
	
	@FindBy(css = ".refinement.active .clear")
	WebElement btnClearInActiveRefInMobile;
	
	@FindBy(css = ".breadcrumb-relax")
	List<WebElement> lstClearRefinement;

	@FindBy(css = ".sort-by.custom-select.current_item .selection-list > li")
	List<WebElement> lstSortingOptions;

	@FindBy(css = ".sort-by.custom-select.current_item .selection-list > li:not([class='selected'])")
	List<WebElement> lstSortOptionSelectable;

	@FindBy(css = ".sort-by.custom-select.current_item .selected-option.selected")
	WebElement selectedSortingOptionNearSortBy;

	@FindBy(css = ".sort-by.custom-select.current_item .selection-list > li.selected")
	WebElement selectedSortingOptionInDrp;

	@FindBy(css = ".sort-breadcrumb")
	WebElement txtSortBreadcrumb;

	@FindBy(id = "search-result-items")
	WebElement sectionSearchResults;
	
	@FindBy(css = "ul[id='search-result-items']>li")
	List<WebElement> lstProducts;
	//=====================================================================//
	//    -------------------Vertical Refinement Elements-----------------------------  //
	//=====================================================================//

	@FindBy(id = "category-level-1")
	WebElement verticalRefinement;

	@FindBy(css = "#category-level-1>li")
	List<WebElement> lstLevel1Category;

	@FindBy(css = ".level-1 .selected>a")
	WebElement activeLevel1;

	@FindBy(css = "#category-level-2>li")
	List<WebElement> lstLevel2Category;

	@FindBy(css = "#category-level-3>li")
	List<WebElement> lstLevel3Category;

	@FindBy(css = "#category-level-4>li")
	List<WebElement> lstLevel4Category;

	@FindBy(css = ".category-level-1.cr-section-pl")
	WebElement Level2;

	@FindBy(id = "category-level-2")
	WebElement Level3;

	@FindBy(css = ".refinements-count .refinement .refinement-heading-category")
	List<WebElement> lstFilterOptions;
	
	@FindBy(css = ".refinements-count ")
	WebElement filterExpandedDropdown;

	//================================================================================
	//			WebElements Declaration End
	//================================================================================

	@FindBy(css = ".loader[style*='block']")
	WebElement plpSpinner;
	
	@FindBy(css = ".loader-bg")
	WebElement plpLoadingOverlay;

	@FindBy(css = ".promo-banner")
	WebElement divPromoBanner;

	@FindBy(css = ".hide-desktop .selection-list")
	WebElement divSortMenu;
	
	@FindBy(css = ".refinements .refinement")
	List<WebElement> lstRefinementsOptions;
	
	@FindBy(css = ".refinements .refinement.price")
	WebElement drpPriceRefinement;
	
	@FindBy(css = ".refinements .refinement.size")
	WebElement drpSizeRefinement;
	
	@FindBy(css = ".refinements .refinement.brand")
	WebElement drpBrandRefinement;
	
	@FindBy(css = ".refinements .refinement.refinementColor")
	WebElement drpColorRefinement;
	
	@FindBy(css = ".price .fa-angle-down")
	WebElement btnDownPriceDesktop;
	
	@FindBy(css = ".price .fa-angle-up")
	WebElement btnUpPriceDesktop;
	
	@FindBy(css = ".size .fa-angle-down")
	WebElement btnDownSizeDesktop;
	
	@FindBy(css = ".size .fa-angle-up")
	WebElement btnUpSizeDesktop;
	
	@FindBy(css = ".brand .fa-angle-down")
	WebElement btnDownBrandDesktop;
	
	@FindBy(css = ".brand .fa-angle-up")
	WebElement btnUpBrandDesktop;
	
	@FindBy(css = ".refinementColor .fa-angle-down")
	WebElement btnDownColorDesktop;
	
	@FindBy(css = ".refinementColor .fa-angle-up")
	WebElement btnUpColorDesktop;
	
	static final String colorOptions = "li.color-refinement-count li a";
	
	@FindBy(css = colorOptions)
	List<WebElement> lstAvailableColors;
	
	@FindBy(css = "li.color-refinement-count li")
	WebElement AvailableColors;
	
	static final String refinementColor = ".breadcrumb-refinement-value>span>a";
	
	@FindBy(css = refinementColor)
	List<WebElement> selectedColors;
	
	@FindBy(css = ".breadcrumb-refined-by")
	WebElement spanFilteredBy;
	
	@FindBy(css = ".price div.available>a")
	List<WebElement> lstAvailablePrices;
	
	@FindBy(css = ".gift-landing")
	WebElement giftCardLandingPage;
	
	@FindBy(css = ".brand div.available>a")
	List<WebElement> lstAvailableBrands;
	
	static final String refinementSize = ".size div.available>a";
	
	@FindBy(css = refinementSize)
	List<WebElement> lstAvailableSizes;
	
	@FindBy(css = ".breadcrumb-refinement-name")
	List<WebElement> lstSelectedRefinements;
	
	@FindBy(css = ".breadcrumb-refinement .breadcrumb-refinement-value")
	List<WebElement> lstAllSelectedRefinements;
	
	@FindBy(css = ".refinementsCombinations-main .refinementsCombinations-header")
	List<WebElement> braSizeHeaders;
	
	@FindBy(css = ".refinementsCombinations-main li a[data-prefn='Bra Band Size']")
	List<WebElement> braBandSizes;
	
	@FindBy(css = ".refinementsCombinations-main li a[data-prefn='Bra Cup Size']")
	List<WebElement> braCupSizes;

	@FindBy(css = ".refinement .count-selected-value")
	List<WebElement> selectedvaluesFilter;
	
	@FindBy(css = ".pt_product-search-noresult")
	WebElement noSearchResultPage;
	
	@FindBy(css = ".loader-bg")
	WebElement loader;


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public Refinements(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void isLoaded() {
		try {
			if (!isPageLoaded) {
				Assert.fail();
			}
			if (isPageLoaded) {
				if(Utils.waitForElement(driver, noSearchResultPage)){
					Log.reference("Search Results not available for given Keywork.");
				} else if (Utils.waitForElement(driver, giftCardLandingPage)){
					Log.reference("Gift Card Landing page will not have Refinement.");
				} else{
					if(Utils.isDesktop()) {
						if(!Utils.waitForElement(driver, readyElementDesktop))
							Log.fail("Refinements menu didn't display", driver);
					}
					else{
						if(!Utils.waitForElement(driver, readyElementMobile))
							Log.fail("Refinements menu didn't display", driver);
					}
				}
			}
			elementLayer = new ElementLayer(driver);
			Log.event("Refinements Loaded successfully.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To wait for Search results spinner
	 */
	private void waitUntilPlpSpinnerDisappear() {
		Utils.waitUntilElementDisappear(driver, plpSpinner);
		Utils.waitUntilElementDisappear(driver, plpLoadingOverlay);
	}

	/**
	 * To get the element which have exact variable name
	 * @param expectedEle - String "Expected Element"
	 * @return WebElement - Actual WebElement with expected Element variable name
	 * @throws Exception - Exception
	 */
	public static WebElement getElement(String expectedEle)throws Exception{
		WebElement element = null;
		try{
			Field elementField = Refinements.class.getClass().getDeclaredField(expectedEle);
			elementField.setAccessible(true);
			element = ((WebElement) elementField.get(Refinements.class.getClass()));
		}//try
		catch(Exception e){
			e.printStackTrace();

		}//catch
		return element;
	}

	/**
	 * to get the name of the active Level1 Category name
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public String getActiveLevel1CategoryName()throws Exception{
		return activeLevel1.getText().trim();
	}
	
	/**
	 * Click on back button from filter by drop down on mobile
	 *@throws Exception 
	 */
	public void clickOnBackButtonOnMobileAndTab() throws Exception {
		if(Utils.isMobile()) {
			clickFilterArrowDown();
			driver.manage().window().fullscreen();
			clickFilterArrowDown();
			if (Utils.waitForElement(driver, divExpandedRefinementSubListTablet)) {
				if (Utils.waitForElement(driver, refinementBackButtonMobile)) {
					BrowserActions.clickOnElementX(refinementBackButtonMobile, driver, "back button");
				}
			}
			driver.manage().window().maximize();
		} else if(Utils.isTablet()) {
			if (Utils.waitForElement(driver, divExpandedRefinementSubListTablet)) {
				Utils.waitForElement(driver, lnkRefinementMenuBackTablet);
				BrowserActions.clickOnElementX(lnkRefinementMenuBackTablet, driver, "back button");
			}
		}
	}	

	/**
	 * Click on view results button from filter by dropdown on mobile
	 *@throws Exception 
	 */
	public void clickApplyResultsOnMobile() throws Exception{		
		if(Utils.waitForElement(driver, refinementApplyResultsMobile)){
			BrowserActions.clickByPressEnter(refinementApplyResultsMobile, driver, "apply button from refinement flyout");
		}
	}
	
	/**
	 * to navigate to given level 2 Category.
	 * @level2 String - Name of level2 menu
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public void navigateLevel2Category(String level2)throws Exception{
		for (WebElement category : lstLevel2Category) {
			if (category.getText().trim().equalsIgnoreCase(level2)) {
				System.out.println("Name of l2 category to be clicked on:: " + category.getText());
				BrowserActions.clickOnElementX(category, driver, "Level 2 category");
				break;
			}
		}
	}

	/**
	 * to get the name of the active Level2 Category name
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public String getActiveLevel2CategoryName()throws Exception{
		for (WebElement category : lstLevel2Category) {
			if(category.getAttribute("class").contains("active")) {
				return category.findElement(By.cssSelector("div>a")).getText().trim();
			}
		}
		return null;
	}

	/**
	 * to get the name of the active Level1 Category name
	 * @param categoryName - 
	 * @param expand -
	 * @throws Exception - Exception
	 */
	public void expandCollapseLevel1Category(String categoryName, Boolean expand)throws Exception{
		WebElement ele;
		for (WebElement category : lstLevel1Category) {
			if(category.findElement(By.cssSelector("div>a")).getText().trim().toLowerCase().contains(categoryName.toLowerCase()))
			{
				if(expand.equals(true))
				{
					if(category.getAttribute("class").equals("expandable active "))
					{
						Log.event("The category is already expanded");
						break;
					}
					else
					{
						ele = category.findElement(By.cssSelector("div>span"));
						BrowserActions.clickOnElementX(ele, driver, "");
						break;
					}
				}
				else
				{
					if(category.getAttribute("class").equals("expandable active  non-active"))
					{
						Log.event("The category is already collapsed");
						break;

					}
					else
					{
						ele = category.findElement(By.cssSelector("div>span"));
						BrowserActions.clickOnElementX(ele, driver, "");
						break;
					}
				}
			}
		}

	}

	/**
	 * to get the name of the active Level1 Category name
	 * @param categoryName - 
	 * @param expand -
	 * @throws Exception - Exception
	 */
	public void expandCollapseLevel2Category(String categoryName, Boolean expand)throws Exception{
		WebElement ele;
		List<WebElement> lstlevel2 = driver.findElements(By.cssSelector("#category-level-1>li>ul>li.expandable"));
		for (WebElement category : lstlevel2) {
			if(category.findElement(By.cssSelector("div>a")).getText().trim().equalsIgnoreCase(categoryName))
			{
				if(expand.equals(true))
				{
					if(category.getAttribute("class").equals("expandable active"))
					{
						Log.event("The category is already expanded");
						break;
					}
					else
					{
						BrowserActions.scrollInToView(category.findElement(By.cssSelector("div>span")), driver);
						ele = category.findElement(By.cssSelector("div>span"));
						BrowserActions.clickOnElementX(ele, driver, "");
						break;
					}
				}
				else
				{
					if(category.getAttribute("class").equals("expandable active non-active") == false)
					{
						BrowserActions.scrollInToView(category.findElement(By.cssSelector("div>span")), driver);
						ele = category.findElement(By.cssSelector("div>span"));
						BrowserActions.clickOnElementX(ele, driver, "");
						break;
					}
					else
					{
						Log.event("The category is already collapsed");
						break;
					}
				}
			}
		}

	}

	/**
	 * To enpand / collapse level3 category
	 * @param categoryName -
	 * @param expand -
	 * @throws Exception -
	 */
	public void expandCollapseLevel3Category(String categoryName, Boolean expand)throws Exception{
		WebElement ele;
		for (WebElement category : lstLevel3Category) {
			if(category.findElement(By.cssSelector("div>a")).getText().trim().equalsIgnoreCase(categoryName))
			{
				if(expand.equals(true))
				{
					if(category.getAttribute("class").contains("expandable active"))
					{
						Log.event("The category is already expanded");
						break;
					}
					else
					{
						BrowserActions.scrollInToView(category.findElement(By.cssSelector("div>span")), driver);
						ele = category.findElement(By.cssSelector("div>span"));
						BrowserActions.clickOnElementX(ele, driver, "");
						break;
					}
				}
				else
				{
					if(category.getAttribute("class").contains("non-active") == false)
					{
						BrowserActions.scrollInToView(category.findElement(By.cssSelector("div>span")), driver);
						ele = category.findElement(By.cssSelector("div>span"));
						BrowserActions.clickOnElementX(ele, driver, "");
						break;
					}
					else
					{
						Log.event("The category is already collapsed");
						break;
					}
				}
			}
		}

	}

	/**
	 * to verify if L3 is displayed below L2
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyLevel3CategoryDisplayedBelowLevel2Category()throws Exception{
		WebElement Level2 = null;
		for (WebElement category : lstLevel2Category) {
			if(category.getAttribute("class").contains("expandable active"))
			{
				Level2 = category.findElement(By.cssSelector("div>a"));
				break;
			}
		}
		WebElement Level3 = driver.findElement(By.cssSelector("#category-level-2 > li.expandable.active #category-level-3"));
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, Level2, Level3);
	}

	/**
	 * to verify if L3 is displayed below L2
	 * @param categoryName - 
	 * @return PlpPage
	 * @throws Exception - Exception
	 */
	public PlpPage clickLevel3Category(String categoryName)throws Exception{
		List<WebElement> lstLevel3Category = driver.findElements(By.cssSelector("#category-level-2 > li.expandable.active >ul > li"));
		for (WebElement category : lstLevel3Category) {
			if(category.findElement(By.cssSelector("a")).getText().trim().equalsIgnoreCase(categoryName))
			{
				BrowserActions.clickOnElementX(category.findElement(By.cssSelector("a")), driver, "");
				break;
			}
		}
		return new PlpPage(driver).get();
	}

	/**
	 * to verify if L4 is displayed below L3
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyLevel4CategoryDisplayedBelowLevel3Category()throws Exception{
		WebElement Level3 = null;
		for (WebElement category : lstLevel3Category) {
			if(category.getAttribute("class").contains("expandable active"))
			{
				Level3 = category.findElement(By.cssSelector("div>a"));
				break;
			}
		}
		WebElement Level4 = driver.findElement(By.cssSelector("#category-level-2 > li.expandable.active #category-level-3 #category-level-4 > li > a"));
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, Level3, Level4);
	}

	/**
	 * to verify if Provided L4 is displayed below provided L3
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyLevel4CategoryDisplayedBelowLevel3Category(String level3, String level4)throws Exception{
		WebElement elementLevel3 = null, elementLevel4= null;
		for (WebElement category : lstLevel3Category) {
			if(category.findElement(By.cssSelector("a")).getText().trim().equalsIgnoreCase(level3)){
				elementLevel3 = category;
				break;
			}
		}

		for (WebElement category : lstLevel4Category) {
			if(category.findElement(By.cssSelector("a")).getText().trim().equalsIgnoreCase(level4)){
				elementLevel4 = category;
				break;
			}
		}
		//WebElement Level4 = driver.findElement(By.cssSelector("#category-level-2 > li.expandable.active #category-level-3 #category-level-4 > li > a"));
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, elementLevel3, elementLevel4);
	}

	/**
	 * click L4 category
	 * @param categoryName - 
	 * @return PlpPage
	 * @throws Exception - Exception
	 */
	public PlpPage clickLevel4Category(String categoryName)throws Exception{
		for (WebElement category : lstLevel4Category) {
			if(category.findElement(By.cssSelector("a")).getText().trim().equalsIgnoreCase(categoryName))
			{
				BrowserActions.clickOnElementX(category.findElement(By.cssSelector("a")), driver, "");
				break;
			}
		}
		return new PlpPage(driver).get();
	}

	/**
	 * to verify if L3 is displayed below L2
	 * @param categoryName - 
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean isTheSelectedLevel3CategoryHighlighted(String categoryName)throws Exception{
		Boolean flag = false;
		List<WebElement> lstLevel3Category = driver.findElements(By.cssSelector("#category-level-2 > li.expandable.active >ul > li"));
		for (WebElement category : lstLevel3Category) {
			if(category.findElement(By.cssSelector("a")).getText().trim().equalsIgnoreCase(categoryName))
			{
				if(category.findElement(By.cssSelector("a")).getAttribute("class").equals("refinement-link  active"))
				{
					flag = true;
					break;
				}
				else
				{
					flag = false;
					break;
				}
			}
		}
		return flag;
	}

	/**
	 * to verify if L4 is highlighted
	 * @param categoryName - 
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean isTheSelectedLevel4CategoryHighlighted(String categoryName)throws Exception{
		for (WebElement category : lstLevel4Category) {
			if(category.findElement(By.cssSelector("a")).getText().trim().equalsIgnoreCase(categoryName)) {
				System.out.println("L4:: "+category.findElement(By.cssSelector("a")).getText());
				System.out.println("class attribute:: "+category.findElement(By.cssSelector("a")).getAttribute("class").toString());
				if(category.findElement(By.cssSelector("a")).getAttribute("class").equals("refinement-link  active"))
					return true;
			}
		}
		return false;
	}

	/**
	 * To get refinement element
	 * @param refinement -
	 * @return WebElement of given / random refinement
	 * @throws Exception -
	 */
	public WebElement getRefinementElement(String... refinement)throws Exception{
		WebElement element = null;
		if(Utils.isDesktop()) {
			if(refinement.length > 0){
				for(int i = 0; i < lstRefinementFilterOptionsDesktop.size(); i++){
					if(lstRefinementFilterOptionsDesktop.get(i).getText().toLowerCase().contains(refinement[0].toLowerCase())){
						element = lstRefinementFilterOptionsDesktop.get(i);
						break;
					}
				}
			} else {
				int rand = Utils.getRandom(0, lstRefinementFilterOptionsDesktop.size());
				element = lstRefinementFilterOptionsDesktop.get(rand);
			}
			return element;
		} else {
			if(refinement.length > 0){
				for(int i = 0; i < lstRefinementFilterOptionsMobile.size(); i++){
					if(lstRefinementFilterOptionsMobile.get(i).getText().toLowerCase().contains(refinement[0].toLowerCase())){
						element = lstRefinementFilterOptionsMobile.get(i).findElement(By.cssSelector(".toggle-heading"));
						break;
					}
				}
			} else {
				int rand = Utils.getRandom(0, lstRefinementFilterOptionsMobile.size());
				element = lstRefinementFilterOptionsMobile.get(rand).findElement(By.cssSelector(".toggle-heading"));
			}
			return element;
		}

	}

	/**
	 * To return the State of Refinement Section weather it is opened or closed
	 * @param refinement - Refinement Section Name
	 * @return String
	 * 			closed - Refinement is Collapsed
	 * 			opened - Refinement is Expanded
	 * @throws Exception - Exception
	 */
	public String verifyRefinementSection(String refinement)throws Exception{
		String state = new String();
		WebElement element = getRefinementElement(refinement).findElement(By.xpath("following-sibling::div"));
		if(element.getCssValue("display").equals("none"))
			state = "closed";
		else
			state = "opened";

		Log.event("Refinement State :: " + state);
		return state;
	}
	
	/**
	 * To verify refinements are displayed swatches up to 10 in one row and 11th from next row
	 * @return boolean
	 * @throws Exception 
	 */
	public boolean Verify11thRefinementsValueDisplayedNxtRow() throws Exception {
		int options = lstRefinementFilterOptionsDesktop.size(), filtersCount = 0;
		for (int option = 0; option < options; option++) {
			String refinement = BrowserActions.getText(driver, lstRefinementFilterOptionsDesktop.get(option), "Refinement");
			clickOnRefinementToggle(refinement, "expanded");
			List<WebElement> elem = lstFilterRefinements.get(option).findElements(By.cssSelector(".color-refinement-count ul li"));

			if (elem.size() > 10) {
				String option1stValue = BrowserActions.getText(driver, elem.get(0), "First Elemnet is");
				String option11thValue = BrowserActions.getText(driver, elem.get(10), "First Elemnet is");
				Log.event(refinement + " Refinemnet have morethan 10 swatches in dropdown. "
						+ "First value is:" + option1stValue + " ,Last value is:" + option11thValue);
				return BrowserActions.verifyHorizontalAllignmentOfElements(driver, elem.get(10), elem.get(0));
			} else{
				filtersCount = filtersCount+1;
			}
		}
		if(filtersCount == options) {
			Log.reference("No filter with more than 10 refinment values. Skipping step.");
			return true;
		}
		return false;
	}

	/**
	 * To get the refinement arrow mark State
	 * @param refinement - Refinement Section name
	 * @return List
	 * 			List[0] - Refinement Element
	 * 			List[1] - State - "upward" / "downward"
	 * @throws Exception - Exception
	 */
	public List<Object> getRefinementArrowState(String refinement)throws Exception{
		String state = new String();
		List<Object> obj = new ArrayList<Object>();
		WebElement element = null;
		for(int i = 0; i < lstRefinementFilterOptionsDesktop.size(); i++){
			if(lstRefinementFilterOptionsDesktop.get(i).findElement(By.cssSelector("span.toggle-heading")).getText().toLowerCase().contains(refinement.toLowerCase())){
				element = lstRefinementFilterOptionsDesktop.get(i);
				obj.add(element);
			}
		}

		if(element.findElement(By.cssSelector("i.fa.fa-angle-down.fa-2x")).isDisplayed())
			state = "downward";
		else if(element.findElement(By.cssSelector("i.fa.fa-angle-up.fa-2x")).isDisplayed())
			state = "upward";
		else
			state = "not-displayed";

		obj.add(state);
		return obj;
	}

	/**
	 * To select the Random sub refinement of given Refinement section name
	 * @param refinement - Refinement Section Name
	 * @return - String - Selected randrom Sub-refinement name
	 * @throws Exception - Exception
	 */
	public String selectRandomSubRefinementOf(String refinement)throws Exception{
		List<Object> obj = getRefinementArrowState(refinement);
		if(obj.get(1).equals("downward")){
			WebElement element = ((WebElement)obj.get(0));
			BrowserActions.clickOnElementX(element, driver, "Downward Toggle Mark of " + refinement + " ");
		}
		int rand = Utils.getRandom(0, lstExpandedRefinementAvailableSubList.size());
		BrowserActions.scrollToViewElement(lstExpandedRefinementAvailableSubList.get(rand), driver);
		BrowserActions.clickOnElementX(lstExpandedRefinementAvailableSubList.get(rand), driver, rand + "th item");
		waitUntilPlpSpinnerDisappear();
		return driver.findElements(By.cssSelector("div[class*='expended'][style*='block'] li > div.available")).get(rand).findElement(By.cssSelector("a")).getText();
	}

	/**
	 * To select the index'th sub refinement of given Refinement section name
	 * @param refinement - Refinement Section Name
	 * @param index - 
	 * @return - String - Selected randrom Sub-refinement name
	 * @throws Exception - Exception
	 */
	public String selectSubRefinementOf(String refinement, int index)throws Exception{
		List<Object> obj = getRefinementArrowState(refinement);
		if(obj.get(1).equals("downward")){
			WebElement element = ((WebElement)obj.get(0));//.findElement(By.xpath("..")).findElement(By.xpath("i.fa.fa-angle-down.fa-2x"));
			BrowserActions.clickOnElementX(element, driver, "Downward Toggle Mark of " + refinement + " ");
		}
		if(driver.findElements(By.cssSelector("div[class='expended right'][style*='block'] li > div.available span.check-square")).size() > 0)
		{
			BrowserActions.scrollToViewElement(lstExpandedRefinementAvailableSubList.get(index-1), driver);
			BrowserActions.clickOnElementX(lstExpandedRefinementAvailableSubList.get(index-1), driver, index-1 + "th item");
			waitUntilPlpSpinnerDisappear();
			return lstExpandedRefinementAvailableSubList.get(index-1).findElement(By.xpath("..")).getText().trim();
		}
		else
		{
			BrowserActions.scrollToViewElement(driver.findElements(By.cssSelector("div[class='expended-main'] li > div.available span.check-square")).get(index-1), driver);
			BrowserActions.clickOnElementX(driver.findElements(By.cssSelector("div[class='expended-main'] li > div.available span.check-square")).get(index-1), driver, index-1 + "th item");
			waitUntilPlpSpinnerDisappear();
			return driver.findElements(By.cssSelector("div[class='expended-main'] li > div.available")).get(index-1).findElement(By.cssSelector("a")).getText();
		}
	}

	/**
	 * To get the Refinement Status
	 * @param refinement - Refinement Section Name
	 * @return - String
	 * 			collapsed - Refinement is closed
	 * 			expanded  - Refinement is opened
	 * @throws Exception - Exception
	 */
	public String getRefinementStatus(String refinement)throws Exception{
		if(Utils.isDesktop()) {
			for(WebElement filter : lstRefinementFilterOptionsDesktop) {
				if(filter.findElement(By.cssSelector(".toggle-heading")).getText().toLowerCase().contains(refinement.toLowerCase()))
					return (filter.getAttribute("class").contains("expanded"))? "expanded" : "collapsed";
			}
			return "collapsed";
		} else {
			for(WebElement filter : lstFilterOptions) {
				if(filter.getText().toLowerCase().contains(refinement.toLowerCase()))
					return (filter.getAttribute("class").contains("expanded"))? "expanded" : "collapsed";
			}
			return "collapsed";
		}
	}

	/**
	 * To Expand or Collapse the Refinement Toggle
	 * @param refinement - Refinement Name
	 * @param state - To Open or close --- 'expand'(To Open), 'collapse'(To Close)
	 * @throws Exception - Exception
	 */
	public void clickOnRefinementToggle(String refinement, String... state)throws Exception{
		WebElement element = getRefinementElement(refinement);
		WebElement toggle = element;
		String presentState = (element.getAttribute("class").contains("expanded"))? "expanded" : "collapsed";
		if(state.length > 0){
			if(presentState.contains(state[0]))
				Log.event("Refinement Already " + presentState);
			else{
				if(presentState.equals("collapsed"))
					toggle = element.findElement(By.cssSelector("i.fa.fa-angle-down.fa-2x"));
				else
					toggle = element.findElement(By.cssSelector("i.fa.fa-angle-up.fa-2x"));	
				BrowserActions.clickOnElementX(toggle, driver, refinement + " toogle icon ");
				presentState = (element.getAttribute("class").equals("toggle"))? "collapsed" : "expanded";
				Log.event("After clicking on toggle now State is :: " + presentState);
			}
		}else{
			Log.event("Present State of Toggle is :: " + presentState);
			if(presentState.equals("collapsed"))
				toggle = element.findElement(By.cssSelector("i.fa.fa-angle-down.fa-2x"));
			else
				toggle = element.findElement(By.cssSelector("i.fa.fa-angle-up.fa-2x"));	
			BrowserActions.clickOnElementX(toggle, driver, refinement + " toogle icon ");
			presentState = (element.getAttribute("class").equals("toggle"))? "collapsed" : "expanded";
			Log.event("After clicking on toggle now State is :: " + presentState);
		}

	}

	/**
	 * To get refinement list options
	 * @return list
	 * @throws Exception - Exception
	 */
	public List<String> getRefinementList() throws Exception {
		List<String> refinement = new ArrayList<>();

		for (int i = 0; i < lstRefinementFilterOptionsInMobile.size(); i++) {
			refinement.add(lstRefinementFilterOptionsInMobile.get(i).getText());
		}
		return refinement;
	}

	/**
	 * To collapse the expanded refinement status
	 * @throws Exception - Exception
	 */
	public void closeOpenedRefinement()throws Exception{
		if(Utils.waitForElement(driver, drpExpandedRefinement)) {
			WebElement element = drpExpandedRefinement.findElement(By.cssSelector("i.fa.fa-angle-up.fa-2x"));
			BrowserActions.clickOnElementX(element, driver, "Toggle mark of currently opened refinement ");
		}else {
			Log.event("There are no active expanded refinements");
		}
	}

	/**
	 * To get the Status of FilterBy menu
	 * @return String
	 * 			collapsed - FilterBy Menu is in its default State
	 * 			expanded - FilterBy Menu is opened
	 * @throws Exception - Exception
	 */
	public String getFilterByState()throws Exception{
		if(drpFilterByCollapsed.getAttribute("class").contains("refinement-mobile"))
			return "expanded";
		else
			return "collapsed";
	}

	/**
	 * To get the Status of FilterBy menu
	 * @return String
	 * 			collapsed - FilterBy Menu is in its default State
	 * 			expanded - FilterBy Menu is opened
	 * @throws Exception - Exception
	 */
	public String getSortByState()throws Exception{

		if(Utils.isDesktop()){
			if(drpSortByCollapsedDesktop.getAttribute("class").contains("current_item"))
				return "expanded";
			else
				return "collapsed";
		}else{
			if(drpSortByCollapsedMobile.getAttribute("class").contains("current_item"))
				return "expanded";
			else
				return "collapsed";
		}
	}

	/**
	 * To open or close the Filter By option
	 * @param state - String (expanded/collapsed)
	 * @return String
	 * 		expanded - Filter By is expanded
	 * 		collapsed - Filter By is collapsed
	 * @throws Exception - Exception
	 */
	public String openCloseFilterBy(String state)throws Exception{
		String cState = new String();

		if(getFilterByState().equals(state))
			cState = state;
		else{
			BrowserActions.clickOnElementX(drpFilterByArrow, driver, "Filter By drp ");
			if(getFilterByState().equals(state))
				cState = state;
			else
				BrowserActions.clickOnElementX(drpFilterByArrow, driver, "Filter By drp ");
			cState = getFilterByState();
		}
		return cState;
	}

	/**
	 * To open filter by option
	 * @throws Exception -
	 */
	public void openFilterBy()throws Exception{
		if(Utils.waitForElement(driver, drpFilterByCollapsed)){
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].setAttribute('class', 'filters refinement-mobile')", drpFilterByCollapsed);
		}

	}

	/**
	 * To Main refinement under Filter By in Table/Mobile
	 * @param refinement - Main refinement
	 * @return String
	 * 			- Selected Main refinement text
	 * @throws Exception - Exception
	 */
	public String selectRefinementInFilterBy(String... refinement)throws Exception{
		String selectedRefinement = new String();
		WebElement element = null;
		if(getFilterByState().equals("collapsed"))
			openCloseFilterBy("expanded");
		if(refinement.length > 0)
			element = getRefinementElement(refinement[0]);
		else
			element = getRefinementElement();
		
		BrowserActions.scrollToTopOfPage(driver);
		BrowserActions.clickOnElementX(element, driver, "Main refinement in Filter by ");
		selectedRefinement = element.getText();
		return selectedRefinement;
	}

	/**
	 * To Main refinement under Filter By in Table/Mobile
	 * @param refinement - Main refinement
	 * @return String
	 * 			- Selected Main refinement text
	 * @throws Exception - Exception
	 */
	public String selectRefinementInFilterByWithoutBackButton(int index, String... refinement)throws Exception{
		String selectedRefinement = new String();
		WebElement element = null;
		if(getFilterByState().equals("collapsed"))
			openCloseFilterBy("expanded");
		if(refinement.length > 0)
			element = getRefinementElement(refinement[0]);
		else
			element = getRefinementElement();

		selectedRefinement = element.getText();
		WebElement elem = driver.findElement(By.cssSelector("a[data-prefn='"+refinement[0]+"']"));
		Utils.waitForElement(driver, elem);
		List<WebElement> options = driver.findElements(By.cssSelector("a[data-prefn='"+refinement[0]+"'] .check-square"));
		BrowserActions.clickOnElementX(options.get(index-1), driver, "Options in refinement");
		return selectedRefinement;
	}
	
	/**
	 * To select sub random or index'th sub refinement
	 * @param refinement - Main refinement
	 * @param index - nth Sub refinement
	 * @return - String Array
	 * 		[0] - Main refinement Selected
	 * 		[1] - Sub refinement selected
	 * @throws Exception - Exception
	 */
	public String[] selectSubRefinementInFilterBy(String refinement, int... index)throws Exception{
		List<WebElement> filterOptions;
		String[] selectedRefinement = new String[2];
		int filterOptionIndex;
		
		if(!Utils.isDesktop()) {
			filterOptions = lstExpandedRefinementAvailableSubListTablet;
		} else {
			filterOptions = lstExpandedRefinementAvailable;
		}
		
		if(!Utils.isDesktop()) {
			if(getFilterByState().equals("collapsed")) {
				openCloseFilterBy("expanded");
			}
		}
		
		Boolean refinementAvailable = toggleRefinementState(refinement, Toggle.Open);
		if(refinementAvailable) {
			selectedRefinement[0] = refinement;
			if(index.length > 0) {
				filterOptionIndex = index[0]-1;
			} else {
				if(refinement.equalsIgnoreCase("Price")) {
					filterOptionIndex = Utils.getRandom(0, filterOptions.size()-1);
				} else {
					filterOptionIndex = Utils.getRandom(0, filterOptions.size());
				}
			}
			
			WebElement filterByValue = filterOptions.get(filterOptionIndex);
			selectedRefinement[1] = filterByValue.getText();
			
			if(selectedRefinement[1].contains("(")) {
				int startIndex = selectedRefinement[1].indexOf("(");
				selectedRefinement[1] = selectedRefinement[1].substring(0, startIndex-1).trim();
			}
			Log.event("Refinement value:: " + selectedRefinement[1]);
			
			BrowserActions.scrollToViewElement(filterByValue, driver);
			BrowserActions.clickOnSwatchElement(filterByValue, driver, filterOptionIndex + "th item");
			waitUntilPlpSpinnerDisappear();
			
			toggleRefinementState(refinement, Toggle.Close);
		} else {
			Log.fail(refinement + "is not available in filter by dropdown", driver);
		}
		return selectedRefinement;
	}
	
	/**
	 * To select refinement by order of index
	 * @param filterByIndex - index of filter to use
	 * @param index - (optional) index of subfilter option
	 * @return - filter name and selected value
	 * @throws Exception
	 */
	public String[] selectSubRefinementInFilterBy(int filterByIndex, int... index) throws Exception {
		String[] selectedRefinement = new String[2];
		List<WebElement> refinementOptions, filterBy;
		int refinemtntIndex;
		if (!Utils.isDesktop()) {
			filterBy = lstRefinementFilterOptionsMobile;
			refinementOptions = lstExpandedRefinementAvailableSubListTablet;
		} else {
			filterBy = lstRefinementFilterOptionsDesktop;
			refinementOptions = lstExpandedRefinementAvailable;
		}
		
		selectedRefinement[0] = BrowserActions.getText(driver, (WebElement)filterBy.get(filterByIndex).findElement(By.cssSelector(".toggle-heading")), "Filter option");
		BrowserActions.clickOnElementX(filterBy.get(filterByIndex), driver, "Filter option");
		
		if(index.length > 0) {
			refinemtntIndex = index[0];
		} else {
			refinemtntIndex = refinementOptions.size()/2;
		}
		selectedRefinement[1] = refinementOptions.get(refinemtntIndex).getText().split(" ")[0];
		Log.event("Refinement value:: " + selectedRefinement[1]);
		
		BrowserActions.scrollToViewElement(refinementOptions.get(refinemtntIndex), driver);
		BrowserActions.clickOnSwatchElement(refinementOptions.get(refinemtntIndex), driver, selectedRefinement[1]);
		waitUntilPlpSpinnerDisappear();
		
		BrowserActions.clickOnElementX(filterBy.get(filterByIndex), driver, "Filter option");
		return selectedRefinement;
	}
	
	/**
	 * To select refinement by value
	 * @param filterByName - refinement to select
	 * @param refinementOption - value of refinement to select
	 *  @return - String Array
	 * 		[0] - Main refinement Selected
	 * 		[1] - Sub refinement selected
	 * @throws Exception
	 */
	public String[] selectSubRefinementInFilterBy(String filterByName, String refinementOption)throws Exception{
		List<WebElement> filterOptions;
		String[] selectedRefinement = new String[2];
		
		if(!Utils.isDesktop()) {
			filterOptions = lstExpandedRefinementAvailableSubListTablet;
		} else {
			filterOptions = lstExpandedRefinementAvailable;
		}
		
		if(!Utils.isDesktop()) {
			if(getFilterByState().equals("collapsed")) {
				openCloseFilterBy("expanded");
			}
		}
		
		Boolean refinementAvailable = toggleRefinementState(filterByName, Toggle.Open);
		if(refinementAvailable) {
			selectedRefinement[0] = filterByName;
			Log.message(selectedRefinement[0] + " filter is opened.", driver);
			
			WebElement filterByValue = filterOptions.get(0);
			for (WebElement filterOption : filterOptions) {
				Log.event("Filter text:: " + filterOption.getText());
				if(filterOption.getText().toUpperCase().contains(refinementOption.toUpperCase())) {
					filterByValue = filterOption;
					break;
				}
			}
			selectedRefinement[1] = filterByValue.getText().split(" ")[0];
			Log.event("filterByName value:: " + selectedRefinement[1]);
			
			BrowserActions.scrollToViewElement(filterByValue, driver);
			BrowserActions.clickOnSwatchElement(filterByValue, driver, selectedRefinement[1]);
			waitUntilPlpSpinnerDisappear();
			
			toggleRefinementState(filterByName, Toggle.Close);
		} else {
			Log.fail(filterByName + "is not available in filter by dropdown", driver);
		}
		return selectedRefinement;
	}
	
	/**
	 * To open/close given refinement menu
	 * @param refinement - name of refinement option
	 * @param toggle - Desired state
	 * @return boolean - true if refinement is available in filter by dropdown else false
	 * @throws Exception
	 */
	public boolean toggleRefinementState(String refinement, Toggle toggle) throws Exception {
		WebElement refinementElement = getRefinementElement(refinement);
		if(refinementElement == null) {
			return false;
		}
		
		if (toggle.equals(Toggle.Close) 
				&& getRefinementStatus(refinement).equals("expanded")) {
			BrowserActions.clickOnElementX(refinementElement, driver, refinement + " refinement in Filter");
		} else if (toggle.equals(Toggle.Open) 
				&& getRefinementStatus(refinement).equals("collapsed")) {
			BrowserActions.clickOnElementX(refinementElement, driver, refinement + " refinement in Filter");
		}
		return true;
	}

	/**
	 * To verify selected refinement is displayed is displayed in refinement bar
	 * @param refinementValue
	 * @return true/false value to verify is found
	 * @throws Exception
	 */
	public boolean verifyRefinementSelection(String refinementValue) throws Exception {
		Log.event("Expected value:: " + refinementValue);
		String selectedRefinements = BrowserActions.getText(driver, divRefinementBar, "Refinemnet bar");
		Log.event("Actual value:: " + selectedRefinements);
		return selectedRefinements.toLowerCase().contains(refinementValue.toLowerCase());
	}

	/**
	 * To return to configured Main refinement list from sub refinement (Table/Mobile)
	 * @throws Exception - Exception
	 */
	public void returnToMainRefineMentInFilterBy()throws Exception{
		if(Utils.waitForElement(driver, iconReturnToMainRefinementInMobile))
			BrowserActions.clickOnElementX(iconReturnToMainRefinementInMobile, driver, "Left arror in Sub Refinement ");
	}

	/**
	 * To click on Clear all button to remove all refinements
	 * @throws Exception - Exception
	 */
	public void clickOnClearAllInRefinement()throws Exception{
		if(Utils.isDesktop()) {
			BrowserActions.clickOnElementX(btnClearAllInDesktop, driver, "Clear All desktop");
		} else if(Utils.isTablet()) {
			BrowserActions.clickOnElement(btnClearAllInTablet, driver, "Clear All tablet");
		} else {
			BrowserActions.scrollToTopOfPage(driver);
			Utils.waitForElement(driver, btnClearAllInMobile);
			BrowserActions.clickByPressEnter(btnClearAllInMobile, driver, "Clear All mobile");
		}	
		waitUntilPlpSpinnerDisappear();
	}
	
	/**
	 * To get the filter applied count in filter by drop box
	 * @return int - product count
	 * @throws Exception - Exception
	 */
	public int getAppliedFilterCount()throws Exception{
		int count=0; String filterCount=null;
		if(Utils.waitForElement(driver, collapsedRefinementAvailability)){
			if(Utils.waitForElement(driver, lblSelectedSubRefCountMobile)){
				for(int i=0; i<selectedvaluesFilter.size();i++){
				String value = BrowserActions.getTextFromAttribute(driver, selectedvaluesFilter.get(i), "value", "Selected count").replace(".","");
				count= count+ Integer.parseInt(value);		
			}
				filterCount= BrowserActions.getText(driver, lblSelectedSubRefCountMobile, "Count of filter by heading").replaceAll("[^\\d]", "");
			
			}
		}else{
			try{
			filterCount = BrowserActions.getText(driver, lblSelectedFilterCountTab, "Filter count").replaceAll("[^\\d.]", "");			
			
			}catch(NullPointerException ex){
				filterCount="0";
			}
			}
		int countfilter=Integer.parseInt(filterCount);
	if(countfilter==count){
		Log.message("Count is: "+filterCount);
	return count;
	}
	return countfilter;
	}
	
	/**
	 * To click apply button from filter by drop down in MObile
	 * @return void
	 * @throws Exception
	 */
	public void clickapplyButtonFromFilterInMobile() throws Exception {
		Utils.waitForElement(driver, btnFilterApply);
		BrowserActions.clickOnSwatchElement(btnFilterApply, driver, "Apply button");
		waitUntilPlpSpinnerDisappear();
		BrowserActions.scrollToTopOfPage(driver);
	}
	
	/**
	 * To get the product count in List page
	 * @return String - product count
	 * @throws Exception - Exception
	 */
	public int getProductCount() throws Exception {
		String prdCount = "";
		if (Utils.isDesktop())
			prdCount = BrowserActions.getText(driver, divResultCountDesktop, "Desktop result count");
		else
			prdCount = BrowserActions.getText(driver, divResultCountMobileTablet, "Mobile/Tablet result count");

		return StringUtils.getNumberInString(prdCount);
	}

	/**
	 * To click on clear in Sub Refinement menu Under Filter By
	 * @throws Exception - Exception
	 */
	public void clickOnClearInSubRefinement()throws Exception{
		if (!Utils.isMobile()) {
			if (Utils.waitForElement(driver, btnClearInActiveRefInTablet)) {
				BrowserActions.clickOnElementX(btnClearInActiveRefInTablet, driver, "Clear button ");
			} else {
				Log.ticket("CO-342");
				clearIndividualRefinements();
				return;
			}
		} else {
			if (getFilterByState().equals("collapsed")) {
				openCloseFilterBy("expanded");
			}
			Utils.waitForElement(driver, btnClearInActiveRefInMobile);
			BrowserActions.clickOnSwatchElement(btnClearInActiveRefInMobile, driver, "clear all");
		}
		waitUntilPlpSpinnerDisappear();
	}
	
	/**
	 * TO clear each individual refinement
	 * <br>Desktop and Tablet only
	 * @throws Exception
	 */
	public void clearIndividualRefinements() throws Exception {
		if(!Utils.isMobile()) {
			while(lstClearRefinement.size() > 0) {
				BrowserActions.clickOnElementX(lstClearRefinement.get(0), driver, "Clear button ");
				waitUntilPlpSpinnerDisappear();
			}
		}
	}

	/**
	 * To open or close the Sory By option
	 * @param state - String (expanded/collapsed)
	 * @return String
	 * 		expanded - Filter By is expanded
	 * 		collapsed - Filter By is collapsed
	 * @throws Exception - Exception
	 */
	public String openCloseSortBy(String state)throws Exception{
		String cState = new String();

		if(getSortByState().equals(state))
			cState = state;
		else{
			if(Utils.isDesktop())
				BrowserActions.clickOnElementX(drpSortByCollapsedDesktop.findElement(By.cssSelector(".selected-option.selected")), driver, "Filter By drp ");
			else {
				BrowserActions.clickOnElementX(drpSortSelectedOption, driver, "Sort By drp ");
			}
			cState = getSortByState();
		}
		return cState;
	}
	
	/**
	 * To get selected refinemnet values
	 * @return string
	 * @throws Exception
	 */
	public String[] getSelectedrefinments() throws Exception{
		String[]  SelectedValues = new String[6];
		if(lstAllSelectedRefinements.size() > 0){
			for(int i=0; i<lstAllSelectedRefinements.size();i++){
				 SelectedValues [i]= lstAllSelectedRefinements.get(i).getText().trim();		
				}
			}
		return SelectedValues;
	}
	
	/**
	 * To get the element for given refinement section
	 * @param sortOption - Refinement name
	 * @return WebElement - WebElement of given Refinement Name
	 * @throws Exception - Exception
	 */
	public WebElement getSortElement(String... sortOption)throws Exception{
		WebElement element = null;
		if(sortOption.length > 0){
			for(int i = 0; i < lstSortOptionSelectable.size(); i++){
				if(lstSortOptionSelectable.get(i).getText().trim().toLowerCase().contains(sortOption[0].toLowerCase())){
					element = lstSortOptionSelectable.get(i);
					break;
				}
			}
		}else{
			int rand = Utils.getRandom(0, lstSortOptionSelectable.size());
			element = lstSortOptionSelectable.get(rand);
		}

		if(sortOption.length > 0 && element == null){
			Log.fail("Requested Sort option["+sortOption[0]+"] not available.");
		}
		return element;
	}

	/**
	 * To choose given sort option or random sort option
	 * @param sortOption - Sort Name
	 * @return - Selected sort Name
	 * @throws Exception - Exception
	 */
	public String selectSortBy(String... sortOption)throws Exception{
		String selectedSortOption = new String();

		openCloseSortBy("expanded");
		if (sortOption.length > 0) {
			WebElement element = getSortElement(sortOption[0]);
			selectedSortOption = element.getAttribute("innerHTML");
			BrowserActions.clickOnElementX(element, driver, "Sort Option ");			
		} else {
			int rand = Utils.getRandom(0, lstSortOptionSelectable.size());
			BrowserActions.clickOnElementX(lstSortOptionSelectable.get(rand), driver, "Sort Option ");
			selectedSortOption = lstSortOptionSelectable.get(rand).getText();
		}
		waitUntilPlpSpinnerDisappear();
		openCloseSortBy("collapsed");
		GlobalNavigation.closePopup(driver);
		return selectedSortOption;
	}

	/**
	 * To verify given sortby option applied
	 * @param sortedOrder -
	 * @param plpPage -
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifySortApplied(String sortedOrder) throws Exception {
		if (!(sortedOrder.toLowerCase().contains("low") || sortedOrder.toLowerCase().contains("high"))) {
			Log.warning("Sort option '" + sortedOrder + "' cannot be tested. Only 'Lowest Priced' and 'Highest Priced' can be tested.");
			return true;
		}
		
		List<HashMap<String, String>> prdList = null;
		if(Utils.waitForElement(driver, readyElementPLP) || Utils.waitForElement(driver, readyElementSLP)) {
			prdList = getAllProductDetails();
		} else {
			Log.failsoft("PLP or SLP is not loaded. Please check script.");
			return false;
		}
		List<Double> lstPrice = new ArrayList<Double>();
		for (HashMap<String, String> prdEntry : prdList) {
			String prdPrice = prdEntry.get("Price");
			if (prdPrice!= null && prdPrice.contains("-")) {
				prdPrice = sortedOrder.toLowerCase().contains("low") ? prdPrice.split("\\-")[0] : prdPrice.split("\\-")[1];
			}
			if (prdPrice!= "" )
				lstPrice.add(StringUtils.getPriceFromString(prdPrice));
		}
		
		List<Double> lstPriceCopy = new ArrayList<Double>(lstPrice);
		Collections.sort(lstPriceCopy);
		if(sortedOrder.toLowerCase().contains("high"))
			Collections.reverse(lstPriceCopy);
		return lstPrice.equals(lstPriceCopy);
	}

	/**
	 * To verify Sort Option in Breadcrumb
	 * @param sortedOrder - 
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifySortOptionInBreadCrumb(String sortedOrder)throws Exception{
		String actualSortedOrder = txtSortBreadcrumb.findElement(By.cssSelector(".breadcrumb-refinement")).getText().trim();
		if(actualSortedOrder.equalsIgnoreCase(sortedOrder))
			return true;
		else
			return false;
	}

	/**
	 * Verify that given level 2 item is on left side of page. 
	 * @param String - level2
	 * @throws Exception
	 */
	public boolean verifyLevel2PlacementOnLeft(String level2) throws Exception{
		for(WebElement ele: lstLevel2Category) {
			ele = ele.findElement(By.className("cr-section"));
			if(ele.getText().trim().equalsIgnoreCase(level2)) {
				return BrowserActions.verifyHorizontalAllignmentOfElements(driver, sectionSearchResults, ele);
			}
		}
		return false;
	}

	/**
	 * Verify that given level 3 item is on left side of page. 
	 * @param String - level3
	 * @throws Exception
	 */
	public boolean verifyLevel3PlacementOnLeft(String level3) throws Exception{
		for(WebElement ele: lstLevel3Category) {
			ele = ele.findElement(By.className("cr-section"));
			if(ele.getText().trim().equalsIgnoreCase(level3)) {
				return BrowserActions.verifyHorizontalAllignmentOfElements(driver, sectionSearchResults, ele);
			}
		}
		return false;
	}

	/**
	 * Verify that given level 3 item is on left side of page. 
	 * @param String - level3
	 * @throws Exception
	 */
	public boolean verifyLevel3Selected(String level3) throws Exception{
		for(WebElement ele: lstLevel3Category) {
			ele = ele.findElement(By.className("refinement-link"));
			if(ele.getText().trim().equalsIgnoreCase(level3)) {
				return ele.getAttribute("class").contains("active");
			}
		}
		return false;
	}

	/**
	 * Verify that given level 4 item is on left side of page. 
	 * @param String - level4
	 * @throws Exception
	 */
	public boolean verifyLevel4PlacementOnLeft(String level4) throws Exception{
		for(WebElement ele: lstLevel4Category) {
			ele = ele.findElement(By.className("refinement-link"));
			if(ele.getText().trim().equalsIgnoreCase(level4)) {
				return BrowserActions.verifyHorizontalAllignmentOfElements(driver, sectionSearchResults, ele);
			}
		}
		return false;
	}

	/**
	 * To click on arrow button on Filter refinement
	 * @throws Exception - Exception
	 */
	public void clickFilterArrowDown() throws Exception{
			Utils.waitForElement(driver, drpFilterByArrow);
			BrowserActions.scrollInToView(drpFilterByArrow, driver);
			BrowserActions.clickOnElementX(drpFilterByArrow, driver, "Filter Arrow Down");
			Utils.waitForPageLoad(driver);
	}

	/**
	 * To get list of available filter options
	 * @return List<String> - list of filter options
	 * @throws Exception - Exception
	 */
	public List<String> getListFilterOptions() throws Exception{
		List<String> filterOptions = new ArrayList<String>();
		for(WebElement filterOption: lstFilterOptions) {
			filterOptions.add(filterOption.getText().trim().toLowerCase());
		}
		return filterOptions;
	}
	
	/**
	 * To get no  of available filter options
	 * @return count of filter options
	 * @throws Exception - Exception
	 */
	public int getListFilterOptionscount() throws Exception{
		return lstFilterOptions.size();
	}
	
	/**
	 * To open or close color refinement options
	 * @param String - "open"/"close" to Open and Close refinement menu
	 * @throws Exception - Exception
	 */
	public void clickOpenCloseColorFilterDesktop(String state) throws Exception{
		if(state.equals("open")) {
			if(btnDownColorDesktop.isDisplayed()) {
				BrowserActions.clickOnElementX(btnDownColorDesktop, driver, "Color open arrow button");
			}else {
				Log.event("Color refinemnts already open.");
			}
		}
		if(state.equals("close")) {
			if(btnUpColorDesktop.isDisplayed()) {
				BrowserActions.clickOnElementX(btnUpColorDesktop, driver, "Color open arrow button");
			}else {
				Log.event("Color refinemnts already closed.");
			}
		}
	}
	
	/**
	 * To select a color on refinement 
	 * @param String - color to select(Optional)
	 * @throws Exception - Exception
	 */
	public String selectColorOnRefinement(String ...color) throws Exception{
		clickOpenCloseColorFilterDesktop("open");
		String selectedColor="";
		if(color.length == 0) {
			WebElement colorToPick = lstAvailableColors.get(Utils.getRandom(0, lstAvailableColors.size()));
			selectedColor = colorToPick.getAttribute("data-prefv");
			BrowserActions.clickOnElement(colorToPick, driver, "Color selected");
		}else {
			WebElement colorToPick = driver.findElement(By.cssSelector(".refinement.refinementColor li a[data-prefv='"+color[0]+"']"));
			try{
				selectedColor = colorToPick.getAttribute("data-prefv");
				BrowserActions.javascriptClick(colorToPick, driver, color + " color selected");
			}catch(Exception e) {
				selectColorOnRefinement();
			}
		}
		Utils.waitForPageLoad(driver);
		clickOpenCloseColorFilterDesktop("close");
		waitUntilPlpSpinnerDisappear();
		return selectedColor;
	}
	
	/**
	 * To select a color on refinement 
	 * @param int -index of color to select
	 * @throws Exception - Exception
	 */
	public String selectColorOnRefinementByIndex(int index) throws Exception{
		clickOpenCloseColorFilterDesktop("open");
		String selectedColor="";
		int size=lstAvailableColors.size();
		if(index<size){
			WebElement elem=lstAvailableColors.get(index);
			selectedColor = BrowserActions.getTextFromAttribute(driver, elem, "data-prefv", "Color");
			BrowserActions.clickOnElementX(elem, driver, "Clicked on "+index+" Color");
			Utils.waitForPageLoad(driver);
		}else{
		Log.message("available colors are lessthan the given number");
		}
		Utils.waitForPageLoad(driver);
		clickOpenCloseColorFilterDesktop("close");
		waitUntilPlpSpinnerDisappear();
		return selectedColor;
	}
	
	/**
	 * To verify selected  color is available in PLP 
	 * @param String - color to select(Optional)
	 * @throws Exception - Exception
	 */
	public boolean verifySelectedColorAvailability(String ColorName)throws Exception{
		
		List<WebElement> elem=driver.findElements(By.cssSelector(".swatch-list li a[title*='"+ColorName+"']"));
		if(elem.size()>1){
			return true;
		}		
		return false;
	}
	
	/**
	 * To open or close size refinement options
	 * @param String - "open"/"close" to Open and Close refinement menu
	 * @throws Exception - Exception
	 */
	public void clickOpenCloseSizeFilterDesktop(String state) throws Exception{
		if(state.equals("open")) {
			if(btnDownSizeDesktop.isDisplayed()) {
				BrowserActions.clickOnElementX(btnDownSizeDesktop, driver, "Size open arrow button");
			}else {
				Log.event("Size refinemnts already open.");
			}
		}
		if(state.equals("close")) {
			if(btnUpSizeDesktop.isDisplayed()) {
				BrowserActions.clickOnElementX(btnUpSizeDesktop, driver, "Size open arrow button");
			}else {
				Log.event("Size refinemnts already closed.");
			}
		}
	}
	
	/**
	 * To select a size on refinement
	 * @param String - size to be selected(Optional)
	 * @throws Exception - Exception 
	 */
	public String selectSizeOnRefinement(String ...size) throws Exception{
		clickOpenCloseSizeFilterDesktop("open");
		String sizeSelected = "";
		if(size.length == 0) {
			WebElement sizeToPick = lstAvailableSizes.get(Utils.getRandom(0, lstAvailableSizes.size()));
			BrowserActions.clickOnElement(sizeToPick, driver, "Random size selected");
			sizeSelected = sizeToPick.getAttribute("data-prefv");
		}else {
			WebElement sizeToPick = driver.findElement(By.cssSelector(refinementSize+"#swatch-"+size[0].replace("/", "_").replace(" ", "_").toLowerCase()));
			try {
				BrowserActions.clickOnElement(sizeToPick, driver, size[0] + " selected");
				sizeSelected = sizeToPick.getAttribute("data-prefv");
			}catch(Exception e) {
				selectSizeOnRefinement();
			}
		}
		Utils.waitForPageLoad(driver);
		clickOpenCloseSizeFilterDesktop("close");
		return sizeSelected;
	}
	
	/**
	 * To verify given color option is selected in refinement 
	 * @param String - color selection to verify
	 * @return boolean - true/false for if color is selected
	 * @throws Exception - Exception
	 */
	public boolean verifyColorRefinementSelected(String color) throws Exception{
		WebElement colorToVerify = driver.findElement(By.cssSelector(refinementColor+"#swatch-"+color.toLowerCase()));
		return colorToVerify.isDisplayed();
	}
	
	/**
	 * To verify given refinement is selected  
	 * @param String - refinement to verify
	 * @return boolean - true/false if given refinement is selected
	 * @throws Exception - Exception 
	 */
	public boolean verifyRefinementSelected(String refinement) throws Exception{
		for(WebElement ref: lstSelectedRefinements) {
			if(ref.getText().equalsIgnoreCase(refinement)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * To verify if given refinement is removed 
	 * @param String - refinement to verify
	 * @return boolean - true/false if given refinement is removed
	 * @throws Exception - Exception
	 */
	public boolean verifyRefinementRemoved(String refinement) throws Exception{
		for(WebElement ref: lstSelectedRefinements) {
			if(ref.getText().equalsIgnoreCase(refinement)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify remove(X) button present in all selected refinement
	 * @return boolean - true/false if X is present in all refinement
	 * @throws Exception - Exception
	 */
	public boolean verifyRefinementXButton() throws Exception{
		for(WebElement ref: lstSelectedRefinements) {
			if(!ref.findElement(By.xpath("..")).findElement(By.cssSelector("a.breadcrumb-relax")).isDisplayed()) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To verify the shape of remove button on refinements 
	 * @return boolean - true/false if remove button is shaped correctly.
	 * @throws Exception - Exception
	 */
	public boolean verifyRemoveButtonShape() throws Exception{
		for(WebElement ref: lstSelectedRefinements) {
			if(!ref.findElement(By.xpath("..")).findElement(By.cssSelector("a.breadcrumb-relax")).getText().trim().equalsIgnoreCase("x")) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * To open refinement in desktop
	 * return true if the flyout displays.
	 * @throws Exception 
	 */
	public boolean openDesktopFilterRefinement(String refinement)throws Exception{
		for(WebElement heading: lstFilterOptions)
			if(heading.getText().equalsIgnoreCase(refinement)){
				if(heading.findElement(By.cssSelector(".fa-angle-down.fa-2x ")).isDisplayed()){
					BrowserActions.clickOnElementX(heading, driver, refinement+"opened");
					return true;
				}else if(heading.findElement(By.cssSelector("i.fa.fa-angle-up.fa-2x")).isDisplayed())
				return true;
					
			}			
		return false;
	}
	
	/**
	 * To remove a given a refinement 
	 * String - refinement to remove
	 * @throws Exception - Exception
	 */
	public void removeRefinement(String refinement) throws Exception{
		for(WebElement ref: lstSelectedRefinementValues) {
			if(ref.getText().toUpperCase().contains(refinement.toUpperCase())) {
				WebElement closeRef = ref.findElement(By.cssSelector("a.breadcrumb-relax"));
				BrowserActions.clickOnElement(closeRef, driver, "close on refinement");
				Utils.waitForPageLoad(driver);
				break;
			}
		}
	}
	
	 /**
     * To wait for spinner in the PDP
     */
    public void waitForSpinner() {
        if (Utils.waitForElement(driver, loader)) {
            Utils.waitUntilElementDisappear(driver, loader);
        }
    }
    
	public boolean selectBraBrandSize(int index)throws Exception{
		if(index<braBandSizes.size()){
			BrowserActions.clickOnElementX(braBandSizes.get(index), driver, "Bra Band Size selected");
			waitForSpinner();
			return true;
		}
		
	return false;
	}
	
	public boolean selectBraCubSize(int index)throws Exception{
		if(index<braCupSizes.size()){
			BrowserActions.clickOnElementX(braCupSizes.get(index), driver, "Bra Cup Size selected");
			waitForSpinner();
			return true;
		}
	return false;
	}
	
	/**
	 * Select bra and cup size in Tablet/Mobile
	 */
	public void selectFirstBraBrand_CubSizeInMobAndTab()throws Exception{
		selectRefinementInFilterBy("BRA SIZE");
		WebElement braBrandSize = driver.findElement(By.cssSelector(".refinementsCombinations-main.expended")).findElements(By.xpath("//ul//div[text()='Bra Band Size']//following-sibling::li")).get(0);
		BrowserActions.clickOnElement(braBrandSize, driver, "1st Size");
		Utils.waitUntilElementDisappear(driver, loader);
		WebElement braCubSize =  driver.findElement(By.cssSelector(".refinementsCombinations-main.expended")).findElements(By.xpath("//ul//div[text()='Bra Cup Size']//following-sibling::li")).get(0);
		BrowserActions.clickOnElement(braCubSize, driver, "1st Size");
		Utils.waitUntilElementDisappear(driver, loader);
		selectRefinementInFilterBy("BRA SIZE");
	}
	
	/**
	 * Select bra and cup size in Desktop
	 */
	public void selectFirstBraBrand_CubSizeInDesk(int bandSizeindex, int cupSizeindex)throws Exception{
		/*for(WebElement elem:lstRefinementFilterOptionsDesktop){
			if(elem.getText().equalsIgnoreCase("bra size")){
				BrowserActions.clickOnElementX(elem, driver, "filter option");
				if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(".refinementsCombinations-main.expended")))){
					selectBraBrandSize(bandSizeindex);
					selectBraCubSize(cupSizeindex);		
				}				
			}			
		}*/	
		
		WebElement refinementBraSize = driver.findElement(By.xpath("//span[@class='toggle-heading'][contains(text(),'Bra Size')]//preceding-sibling::i[contains(@class,'down')]"));
		BrowserActions.javascriptClick(refinementBraSize, driver, "refinement");
		selectBraBrandSize(bandSizeindex);
		selectBraCubSize(cupSizeindex);
	}
	
	/**
	 * To get all product details as list of hashmap
	 * @return List -
	 * @throws Exception -
	 */
	public List<HashMap<String, String>> getAllProductDetails()throws Exception{
		List<HashMap<String, String>> prdDetails = new ArrayList<HashMap<String, String>>();

		int x = lstProducts.size() > 10 ? 10 : lstProducts.size();
		for(int i = 0; i < x; i++){
			BrowserActions.scrollInToView(lstProducts.get(i), driver);
			HashMap<String, String> product = new HashMap<String, String>();
			String productName = "", price = "";
			try {
				productName = lstProducts.get(i).findElement(By.cssSelector(".product-name")).getText();
			} catch (NoSuchElementException | StaleElementReferenceException e) {
				Log.event("Error getting name:: " + e.getClass().getName());
			}
			try {
				price = lstProducts.get(i).findElement(By.cssSelector("span.price-product-sales-price")).getText();
			} catch (NoSuchElementException | StaleElementReferenceException e) {
				Log.event("Error getting price:: " + e.getClass().getName());
			}
			product.put("Name", productName);
			product.put("Price", price);
			prdDetails.add(product);
		}
		return prdDetails;
	}
}
