package com.fbb.pages;

import java.util.LinkedHashMap;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class PreApprovedAppPage extends LoadableComponent<PreApprovedAppPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	public final LinkedHashMap<String, String> accountDetails = new LinkedHashMap<String, String>();

	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(css = ".pt_specials")
	WebElement readyElement;
	
	@FindBy(css = ".tab.rewards-tab a")
	WebElement rewardSection;
	
	
	//================================================================================
	//			WebElements Declaration End
	//================================================================================




	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public PreApprovedAppPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("PLCC application page for pre-approved users did not open.", driver);
		}
		
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To get page load status
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}
	
	/**
	 * To click on reward section
	 * @throws Exception -
	 */
	public void clickOnRewardSection()throws Exception{
		BrowserActions.clickOnElementX(rewardSection, driver, "Reward Section");
	}
}
