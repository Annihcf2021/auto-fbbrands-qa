package com.fbb.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.account.MyAccountPage;
import com.fbb.support.Brand;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;


public class OrderConfirmationPage extends LoadableComponent<OrderConfirmationPage> {

	private WebDriver driver;
	public boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	private static EnvironmentPropertiesReader checkoutProperty = EnvironmentPropertiesReader.getInstance("checkout");

	@FindBy(css = ".pt_order-confirmation")
	WebElement readyElement;
	
	@FindBy(css = ".order-number .value")
	WebElement lblOrderNumber;
	
	@FindBy(css = ".order-details-section .orderdetails")
	WebElement OrderDetailsSection;
	
	@FindBy(css = ".order-details-section .login-box.login-create-account")
	WebElement creatAccountSection;
	
	@FindBy(css = ".order-date .value")
	WebElement lblOrderDate;
	
	@FindBy(css = ".total .value")
	WebElement lblOrderTotal;
	
	@FindBy(css = ".column.col-3 .bonus-item")
	WebElement bonusPrdSubTotalDesk_Tab;
	
	@FindBy(css = ".line-item-price .bonus-item")
	WebElement bonusPrdSubTotalMobile;
	
	@FindBy(css = ".order-shipments")
	WebElement orderDetails;
	
	@FindBy(css = ".order-detail-section .view-more-section .view-more-link")
	WebElement viewDetailsMobile;
	
	@FindBy(css = ".order-payment-summary")
	WebElement orderSummary;
	
	@FindBy(css = ".order-shipment-address")
	WebElement orderShippingAddress;
	
	@FindBy(css = ".order-details-section div.additional-offers ")
	WebElement confrmCotentSlot;
	
	@FindBy(css = "div.additional-offers h2")
	WebElement contentAssetHeader;
	
	@FindBy(css = ".qa-section")
	WebElement orderCnfQASection;
	
	@FindBy(css = ".cc-owner.value")
	WebElement lblEnteredCardOwnerName;
	
	@FindBy(css = ".cc-number.value")
	WebElement 	lblEnteredCardNo;
	
	@FindBy(css = ".cc-exp")
	WebElement 	lblEnteredExpiryDate;
	
	@FindBy(css = ".total .value")
	WebElement 	lblSubtotal;
	
	@FindBy(css = ".profile-email .profile-value")
	WebElement 	lblProfileEmail;
	
	@FindBy(css = ".discount-section.section .inner-block .value")
	WebElement txtPromoCode;
	
	@FindBy(css = ".receipt-message")
	WebElement 	lblOrderReceipt;
	
	@FindBy(css = ".product-list-item .display-inline-block")
	List<WebElement> lblPrdListNumbers;
	
	@FindBy(css = ".order-totals-table .order-value.value")
	WebElement 	lblOrderSummaryTableTotal;
	
	@FindBy(css = ".print-page")
	WebElement 	lnkPrint;
		
	@FindBy(css = ".qa-desc .qa-content .answer.active ")
	WebElement qaAnswerActive;
	
	@FindBy(css = ".qa-desc .qa-content .question")
	WebElement qaQuestion;
	
	@FindBy(css = ".order-total .order-value")
	WebElement orderSummaryTotal;
	
	@FindBy(css = ".address")
	WebElement orderShipAddress;
	
	@FindBy(css = ".mini-address-location")
	WebElement orderBillAddress;
	
	@FindBy(css = ".mini-address-name")
	WebElement orderBillAddressName;
	
	@FindBy(css = "#RegistrationForm .password input")
	WebElement txtPasswordFld;
	
	@FindBy(css = "#RegistrationForm .passwordconfirm input")
	WebElement txtConfirmPasswordFld;
	
	@FindBy(css = ".largebutton")
	WebElement btnCreateAccount;
	
	@FindBy(css = ".pt_account")
	WebElement myAccReadyElement;
	
	@FindBy(css = ".order-totals-table > div > div:not([class*='order-total']):not([class='hide']):not([class='order-saving discount order-detail']) span.value")
	List<WebElement> orderSummaryDetails;
	
	@FindBy(css = "#TTcommentCapture")
	WebElement purchaseSection;
	
	@FindBy(css = ".TTcommentCaptureBlock")
	WebElement purchaseblock;
	
	@FindBy(css = ".TTccCommentArea  textarea.TTccComment")
	WebElement purchasedCommentSection;
	
	@FindBy(css = ".TTbigSubmitBtn")
	WebElement btnSubmitPurchaseComment;
	
	@FindBy(css = ".TTccSavedCommentArea .TTccThanks")
	WebElement thanksMsgInPurchaseSection;
	
	@FindBy(css = ".product-list-item .name a")
	List<WebElement> lnkPrdName;
	
	@FindBy(css = ".line-item .item-image a img")
	List<WebElement> lnkPrdImage;
	
	@FindBy(css = ".quick-order-badge.hide-mobile .quick-order-badge-link")
	WebElement quickOrderCatalogBagde;
	
	@FindBy(css = ".quick-order-badge.hide-desktop.hide-tablet .quick-order-badge-link")
	WebElement quickOrderCatalogBagdeMobile;
	
	@FindBy(css = ".product-list-item .name a")
	WebElement 	productNameSection;
	
	@FindBy(css = ".product-list-item")
	WebElement productDetails;
	
	@FindBy(css = ".display-inline-block")
	List<WebElement> listPrdId;
	
	@FindBy(css = ".product-list-item div[data-attribute='color'] .value")
	List<WebElement> listPrdColor;
	
	@FindBy(css = ".product-list-item div[data-attribute='size'] .value")
	List<WebElement> listPrdSize; 
	
	@FindBy(css = ".col-2 .line-item-quantity .qty-value")
	List<WebElement> listPrdQty;
	
	@FindBy(css = ".price-sales")
	List<WebElement> listPrdPrize;    
	
	@FindBy(css = ".shipmentnumber >h2")
	WebElement shippingMethod;
	
	@FindBy(css = ".line-item")
	List<WebElement> divProductCount;
	
	@FindBy(css = ".order-shipments .line-item")
	List<WebElement> lstOrderItems;
	
	@FindBy(css = ".confirmation-recommendations.product-recommendations .name-link")
	List<WebElement> listRecommendationProductNameLink;
	
	@FindBy(css = ".mini-address-location address div")
	List<WebElement> billingAddress;
	
	// --------------- Header Section ---------------------- //
	/**
	 * Initiate the web driver
	 * @param driver -web driver
	 * 
	 */
	public OrderConfirmationPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver,  readyElement))) {
			Log.fail( "Order Confirmation Page didn't open", driver);
		}

		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	/**
	 * To get order number
	 * @return String - order number
	 * @throws Exception - Exception
	 */
	public String getOrderNumber()throws Exception{
		return lblOrderNumber.getText().trim();
	}
	
	/**
	 * To get order date
	 * @return String - Order Date
	 * @throws Exception - Exception
	 */
	public String getOrderDate()throws Exception{
		return lblOrderDate.getText().trim();
	}
	
	/**
	 * To get order total
	 * @return String - Order Total
	 * @throws Exception - Exception
	 */
	public String getOrderTotal() throws Exception{
		return lblOrderTotal.getText().trim();
	}
	
	/**
	 * To get subtotal in summary
	 * @return String
	 * @throws Exception - Exception
	 */
	public float getOrderTotalInSummary()throws Exception{
		String subTotalToReturn = orderSummaryTotal.getText().trim().replace("$", ""); 
		Log.event("Order Sub-Total :: " + subTotalToReturn);
		return Float.parseFloat(subTotalToReturn);
	}
	
	
	/**
	 * To get subtotal in summary
	 * @return String
	 * @throws Exception - Exception
	 */
	public MyAccountPage createUserInOrderSummary(String password)throws Exception{
		enterValueInPassword(password);
		enterValueInConfirmPassword(password);
		clickOnCreateAccount();
		return new MyAccountPage(driver).get();
	}
	
	/**
     * To click on Print link
     * @return void
     * @throws Exception - Exception
     */
    public void clickPrintLink()throws Exception {
        BrowserActions.scrollInToView(lnkPrint, driver);
        BrowserActions.clickOnElement(lnkPrint, driver, "Print link");
        Utils.waitForPageLoad(driver);
    }
    
	/**
	 * To enter value in password field
	 * @throws Exception - Exception
	 */
	public void enterValueInPassword(String password)throws Exception{
		BrowserActions.typeOnTextField(txtPasswordFld, password, driver, "Password Fld");
	}
	
	/**
	 * To enter value in confirm password field
	 * @throws Exception - Exception
	 */
	public void enterValueInConfirmPassword(String password)throws Exception{
		BrowserActions.typeOnTextField(txtConfirmPasswordFld, password, driver, "Password Fld");
	}
	
	/**
	 * To click create account button
	 * @throws Exception - Exception
	 */
	public void clickOnCreateAccount()throws Exception{
		BrowserActions.clickOnElementX(btnCreateAccount, driver, "Create account button");
	}
	
	/**
	 * To click the question and answer
	 * @throws Exception
	 */
	public void clickonQuestion()throws Exception{
		if(Utils.waitForElement(driver, qaQuestion))
		BrowserActions.clickOnElement(qaQuestion, driver, "QA question");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To get the Shipping address
	 * @return String - Shipping address details
	 * @throws Exception - Exception 
	 */
	public String getShippingAddressDetails()throws Exception {
		return BrowserActions.getText(driver, orderShipAddress, "Shipping Address").trim();
	}
	
	/**
	 * To get the billing address
	 * @return String - billing address details
	 * @throws Exception - Exception
	 */
	public String getBillingAddressDetails()throws Exception {
		return BrowserActions.getText(driver, orderBillAddress, "Shipping Address").trim();
	}
	
	/**
	 * To click the view details in mobile
	 * @throws Exception
	 */
	public void clickOnViewDetailsMobile()throws Exception{
		if(Utils.waitForElement(driver, viewDetailsMobile))
		BrowserActions.clickOnElementX(viewDetailsMobile, driver, "View Details");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To Compare entered address and the shipping address are same
	 * @param shippingInfo - address property
	 * @return boolean - boolean
	 * @throws Exception - Exception
	 */
	public boolean checkEnteredShippingAddressReflectedInOrderReceipt(String shippingInfo) throws Exception {
		String addressEntered = checkoutProperty.getProperty(shippingInfo).toLowerCase();
		String addressFromOrderDetails = BrowserActions.getText(driver, orderShipAddress, "Shipping Address").trim().toLowerCase();
		return elementLayer.verifyTwoAddressMatch(addressEntered, addressFromOrderDetails);
	}
	
	/**
	 * To check billing address given when placing order reflected in order receipt
	 * @param billingInfo - address property
	 * @return boolean - boolean
	 * @throws Exception - Exception
	 */
	public boolean checkEnteredBillingAddressReflectedInOrderReceipt(String billingInfo) throws Exception {
		String address = checkoutProperty.getProperty(billingInfo);
		String billAdd = BrowserActions.getText(driver, orderBillAddress, "Shipping Address").trim();
		return elementLayer.verifyTwoAddressMatch(address, billAdd);
	}
	
	/**
	 * To check shipping address given when placing order with international is reflected in order receipt
	 * @param shippingInfo - address property
	 * @return boolean - boolean
	 * @throws Exception - Exception
	 */
	public boolean checkEnteredShippingAddressReflectedInInternationalOrderReceipt(String shippingInfo) throws Exception {
		String addressEntered = checkoutProperty.getProperty(shippingInfo).toLowerCase();
		String addressFromOrderDetails = BrowserActions.getText(driver, orderShipAddress, "Shipping Address").trim().toLowerCase();
		return elementLayer.verifyTwoAddressMatch(addressEntered, addressFromOrderDetails);
	}
	
	/**
	 * To check billing address given when placing order with international is reflected in order receipt
	 * @param billingInfo - address property
	 * @return boolean - boolean
	 * @throws Exception - Exception
	 */
	public boolean checkEnteredBillingAddressReflectedInInternationalOrderReceipt(String billingInfo) throws Exception {
		String address = checkoutProperty.getProperty(billingInfo);
		String billAdd = BrowserActions.getText(driver, orderBillAddress, "Billing Address").trim();
		return elementLayer.verifyTwoAddressMatch(address, billAdd);
	}
	
	/**
	 * To get Card Number displayed in Order details section
	 * @return Card Number in Order Details
	 * @throws Exception
	 */
	public String getOrderDetailsCardNumber() throws Exception{
		String cardNo = BrowserActions.getText(driver, lblEnteredCardNo, "Card No").trim();
		Log.event("Card number in order confirmation page:: " + cardNo);
		return cardNo;
	}
	
	
	/**
	 * To compare payment method after ordering with entered payment method
	 * @param payment - payment property
	 * @return boolean - boolean
	 * @throws Exception - Exception
	 */
	public boolean comparePaymentAfterOrderWithEnteredPaymentMethod(String payment) throws Exception {
		String paymentInfos = checkoutProperty.getProperty(payment);
		
		String cardNo = BrowserActions.getText(driver, lblEnteredCardNo, "Card No").trim();
		String expiry = BrowserActions.getText(driver, lblEnteredExpiryDate, "Expiry date").trim();
		String cardOwnerName = BrowserActions.getText(driver, lblEnteredCardOwnerName, "Card Owner Name");
		
		String[] paymentInfo = paymentInfos.split("\\|");
		
		if(!(paymentInfo[1].toLowerCase().trim().contains(cardOwnerName.toLowerCase().trim()))) {
			return false;
		}
		
		if(!(cardNo.trim().contains(paymentInfo[2].trim().substring(13)))) {
			return false;
		}
		
		if(!(expiry.trim().contains(paymentInfo[4].trim()))) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * To get product ID in Checkout page
	 * @return HashSet<String> ProductId- Product IDs 
	 * @throws Exception - Exception
	 */
	public HashSet<String> getOrderedPrdListNumber() throws Exception{
		HashSet<String> ProductId = new HashSet<String> ();
		
		for(int i=0; i < lblPrdListNumbers.size(); i++) {
			WebElement prdId = lblPrdListNumbers.get(i);
			ProductId.add(BrowserActions.getText(driver, prdId, "Product ID"));
		}
		
		return ProductId;
	}
	
	/**
	 * To get product name in Checkout page
	 * @return List<String> Product Name=Product IDs 
	 * @throws Exception - Exception
	 */
	public List<String> getOrderedPrdListNames() throws Exception{
		List<String> ProductId = new ArrayList<String> ();		
		for(int i=0; i < lnkPrdName.size(); i++) {
			WebElement prdId = lnkPrdName.get(i);
			ProductId.add(BrowserActions.getText(driver, prdId, "Product name").toLowerCase());
		}		
		return ProductId;
	}
	
	
	/**
	 * To get total in order summary
	 * @return TotalToReturn - Order total
	 * @throws Exception - Exception
	 */
	public String getOrderSummaryTotal()throws Exception{
		String TotalToReturn = BrowserActions.getText(driver, lblOrderSummaryTableTotal, "Order Summary Table Total"); 
		Log.event("Order Summary Total :: " + TotalToReturn);
		return TotalToReturn.replace("$", "").trim();
	}
	
	
	/**
	 * To calculate the order summary total
	 * @return float
	 * @throws Exception - Exception
	 */
	public float calculateTotalInOrderSummaryList()throws Exception{
		float total = 0;
		float discount =0;
		for(int i = 0; i < orderSummaryDetails.size(); i++)
		{
			String tempString = orderSummaryDetails.get(i).getText();
			if(tempString.contains("-"))
				discount+=Float.parseFloat(tempString.replace("-", "").replace("$", "").trim());
			else
				total+=Float.parseFloat(tempString.replace("$", "").trim());
		}
		float amount = total-discount;
		return Float.parseFloat(String.format("%.2f", amount));
	}
	
	public void clickOnProductNameByIndex(int index)throws Exception{
		BrowserActions.clickOnElementX(lnkPrdName.get(index), driver, "Product Name");
		Utils.waitForPageLoad(driver);
	}
	
	
	/**
	 * To add message in pruchased item section
	 * @throws Exception
	 */
	public void addMsgInPurchasedItemSection(String message) throws Exception{
		if(Utils.waitForElement(driver, purchaseSection)){
			BrowserActions.scrollToView(purchasedCommentSection, driver);
			BrowserActions.typeOnTextField(purchasedCommentSection, message, driver, "message");
			Utils.waitForElement(driver, btnSubmitPurchaseComment);
			BrowserActions.clickOnElementX(btnSubmitPurchaseComment, driver, "submit button");			
			
		}else{
			Log.message("Purchase section is not displayed");
		}	
	}
	
	/**
	 * To get product type of the product
	 * @param index- product row or name index
	 * @return string - product type
	 * @throws Exception
	 */
	public String getProductType(int prdIndex_Size) throws Exception {
		String[] sizeType = {"size", "shoeWidth", "sizeFamily", "braBandSize"};
		WebElement orderRow = divProductCount.get(prdIndex_Size);
		for(String size : sizeType) {
			try{
				orderRow.findElement(By.cssSelector("div[data-attribute='"+size+"'] .value"));
				return size + "Product" ;
			} catch (NoSuchElementException ex) {
				Log.event("Entry does not have expected size type.");
			}
		}
		return null;
	}
	
	/**
	 * To get product name value by index
	 * @param index - Product name index
	 * @return String - Product name text
	 * @throws Exception - Exception
	 */
	public String getProductName(int prdIndex_Name)throws Exception {
		WebElement name = lnkPrdName.get(prdIndex_Name);
		String prdName = BrowserActions.getText(driver, name, "Product name");
		return prdName;
	}
	
	/**
	 * To get product Id value by index
	 * @param index - Product index for ID
	 * @return String - Product Id text
	 * @throws Exception - Exception
	 */
	public String getProductId(int prdIndex_Id)throws Exception {
		String prdId = BrowserActions.getText(driver, listPrdId.get(prdIndex_Id), "Product Id");
		return prdId;
	}
	
	/**
	 * To get product color value by index
	 * @param index - Product Index for Color
	 * @return String - Product color text
	 * @throws Exception - Exception
	 */
	public String getProductColorVariation(int prdIndex_Color)throws Exception {
		String prdColor = BrowserActions.getText(driver, listPrdColor.get(prdIndex_Color), "Product Color");
		return prdColor;
	}
	
	/**
	 * To get product size value by index
	 * @param index - Product index for size
	 * @return String - product size text
	 * @throws Exception - Exception
	 */
	public String getProductSizeVariation(int prdIndex_Size)throws Exception {
		WebElement size = null;
		WebElement orderRow = divProductCount.get(prdIndex_Size);		
		try{
			size = orderRow.findElement(By.cssSelector("div[data-attribute='size'] .value"));
			return BrowserActions.getText(driver, size, "Product Size");
		} catch (NoSuchElementException ex) {
			Log.event("size is not available for the product");
		}				
		return null;
	}
	
	/**
	 * To get the size family, shoe type and Bra type product sizes
	 * @param index - Product index for size
	 * @return hashmap string
	 * @throws Exception
	 */
	
	public LinkedHashMap<String, String> getProductSizeAttributes(int product_Size , String productType) throws Exception {
		LinkedHashMap<String, String > dataToReturn = new LinkedHashMap<String, String>();
		WebElement firsElement, secondElement ; 
		WebElement orderRow = divProductCount.get(product_Size);
		
		if(productType.contains("family")) {
			firsElement = orderRow.findElement(By.cssSelector("div[data-attribute='sizeFamily'] .value"));
			secondElement = orderRow.findElement(By.cssSelector("div[data-attribute='size'] .value"));
			
		} else if(productType.contains("shoe")) {
			firsElement = orderRow.findElement(By.cssSelector("div[data-attribute='shoeWidth'] .value"));
			secondElement = orderRow.findElement(By.cssSelector("div[data-attribute='shoeSize'] .value"));
		
		} else {
			firsElement = orderRow.findElement(By.cssSelector("div[data-attribute='braBandSize'] .value"));
			secondElement = orderRow.findElement(By.cssSelector("div[data-attribute='braCupSize'] .value"));
		}
		dataToReturn.put("firstsize", BrowserActions.getText(driver, firsElement, productType +"first attribute value"));
		dataToReturn.put("secondsize", BrowserActions.getText(driver, secondElement, productType + "Second attribute value"));
	
		return dataToReturn;
	}
	
	
	/**
	 * To get product Quantity value by index
	 * @param index - Product index for quantity
	 * @return String - Product quantity text
	 * @throws Exception - Exception
	 */
	public String getProductQuantity(int prdIndex_Quantity)throws Exception {
		String prdQuantity = BrowserActions.getText(driver, listPrdQty.get(prdIndex_Quantity), "Product Quantity");
		return prdQuantity;
	}
	
	/**
	 * To get product price value by index
	 * @param index - Product index for price
	 * @return String - Product price text
	 * @throws Exception - Exception
	 */
	public String getProductPrice(int prdIndex_Price)throws Exception {
		String prdPrice = BrowserActions.getText(driver, listPrdPrize.get(prdIndex_Price), "Product Price");
		return prdPrice;
	}
	
	/**
	 * To get product price value by index
	 * @return String - Shipping method of the order
	 * @throws Exception - Exception
	 */
	public String getShippingMethod()throws Exception {
		String shipMethod = BrowserActions.getText(driver, shippingMethod, "Shipping method");
		return shipMethod;
	}
	
	/**
	 * To get product details as list
	 * @return LinkedList - Product attributes 
	 * @throws Exception - Exception
	 */
	public LinkedList<LinkedHashMap<String, String>> getProductDetails()throws Exception {
		LinkedList<LinkedHashMap<String, String>> dataToReturn = new LinkedList<LinkedHashMap<String, String>>();
		
		String productType = null;
		for(int i = 0; i < divProductCount.size(); i++) {
			LinkedHashMap<String, String> product = new LinkedHashMap<String, String>();
			productType = getProductType(i);
			try { product.put("Name", getProductName(i)); }catch(NoSuchElementException e) {product.put("Name", "");}
			try { product.put("Id", getProductId(i)); }catch(NoSuchElementException e) {product.put("Id", "");}
			try { product.put("Color", getProductColorVariation(i)); }catch(NoSuchElementException e) {product.put("Color", "");}
			
			if(productType.contains("family")){
				try { product.put("SizeFamily", getProductSizeAttributes(i,productType).get("firstsize")); }catch(NoSuchElementException e) {product.put("Size", "");}
				try { product.put("Size", getProductSizeAttributes(i,productType).get("secondsize")); }catch(NoSuchElementException e) {product.put("Size", "");}
				
			} else if (productType.contains("shoe")) {
				try { product.put("ShoeWidth", getProductSizeAttributes(i,productType).get("firstsize")); }catch(NoSuchElementException e) {product.put("Size", "");}
				try { product.put("ShoeSize", getProductSizeAttributes(i,productType).get("secondsize")); }catch(NoSuchElementException e) {product.put("Size", "");}
				
			} else if (productType.contains("bra")) {
				try { product.put("BraBandSize", getProductSizeAttributes(i,productType).get("firstsize")); }catch(NoSuchElementException e) {product.put("Size", "");}
				try { product.put("BraCupSize", getProductSizeAttributes(i,productType).get("secondsize")); }catch(NoSuchElementException e) {product.put("Size", "");}
								
			} else {
				try { product.put("Size", getProductSizeVariation(i)); }catch(NoSuchElementException e) {product.put("Size", "");}
			}
			
			try { product.put("Quantity", getProductQuantity(i)); }catch(NoSuchElementException e) {product.put("Quantity", "");}
			try { product.put("Price", getProductPrice(i)); }catch(NoSuchElementException e) {product.put("Price", "");}
			dataToReturn.add(product);
		}
		return dataToReturn;
	}
	
	/**
	 * To get product Brand from cart line items based on given product ID
	 * @param prdId - Product ID
	 * @return String of Current Brand short name
	 * @throws Exception - Exception
	 */
	public Brand getProductBrand(String prdId) throws Exception {
		String productVariation = null;
		String productURL = null;
		for(int i=0 ; i < lstOrderItems.size() ; i++) {
			WebElement orderItemImage = lstOrderItems.get(i).findElement(By.cssSelector(".item-image>a"));
			productVariation = UrlUtils.getProductIDFromURL(orderItemImage.getAttribute("href"));
			if(productVariation.equalsIgnoreCase(prdId)) {
				productURL = lstOrderItems.get(i).findElement(By.cssSelector(".item-image>a")).getAttribute("href");
				break;
			}
		}
		productURL = UrlUtils.getSharedCartRedirectURL(productURL);
		return UrlUtils.getBrandFromUrl(productURL);
	}
	
	/**
	 * To get the Product ID and Brand Name from cart line items.
	 * @return HashMap<String, Brand> - HashMap<productId, brandName> of cart line items
	 * @throws Exception
	 */
	public HashMap<String, Brand> getProductVariationAndBrandName() throws Exception {
		HashMap<String, Brand> productDetails = new HashMap<String, Brand>();
		String productVariation = null;
		Brand brandName = null;
		for(int i=0 ; i < lstOrderItems.size() ; i++) {
			WebElement orderItemImage = lstOrderItems.get(i).findElement(By.cssSelector(".item-image>a"));
			productVariation = UrlUtils.getProductIDFromURL(orderItemImage.getAttribute("href"));
			brandName = getProductBrand(productVariation);
			Log.event(productVariation + ":" + brandName.toString());
			productDetails.put(productVariation, brandName);
		}
		return productDetails;
	}
	
	/**
     * To get Applied promo code
     * @return String - Applied promo code
     * @throws Exception - Exception
     */
    public String getAppliedPromoCode() throws Exception {
       if(Utils.waitForElement(driver, txtPromoCode)) {      
    	   return BrowserActions.getText(driver, txtPromoCode,"promo code").trim();
       }
       return null;
    }
    
    /**
     * To click on the product name based on given Index
     * @param index - integer
     * @return PdpPage
     * @throws Exception
     */
    public PdpPage clickOnproductNameByIndex(int index) throws Exception {
    	BrowserActions.clickOnElementX(lnkPrdName.get(index), driver, "order page product name");
    	Utils.waitForPageLoad(driver);
    	return new PdpPage(driver).get();
    }
    
    /**
     * To click on the product image based on given index
     * @param index - integer
     * @return PdpPage
     * @throws Exception
     */
    public PdpPage clickOnproductImageByIndex(int index) throws Exception {
    	BrowserActions.clickOnElementX(lnkPrdImage.get(index), driver, "order page product image");
    	Utils.waitForPageLoad(driver);
    	return new PdpPage(driver).get();
    }
    
    /**
     * To click on the recommendation product name link based on given index
     * @param index - integer
     * @return PdpPage
     * @throws Exception
     */
    public PdpPage clickOnRecommendationProductNameLinkByIndex(int index) throws Exception {
    	BrowserActions.clickOnElementX(listRecommendationProductNameLink.get(index), driver, "recommendation product name link");
    	Utils.waitForPageLoad(driver);
    	return new PdpPage(driver).get();
    }
    
    /**
     * To get the billing zip-code from the order confirmation page
     * @return zipcode - String
     * @throws Exception
     */
    public String getOrderZipCode() throws Exception {
    	String zipCode = BrowserActions.getText(driver, billingAddress.get(billingAddress.size() - 2), "Billing address zip code").trim();
    	zipCode = Utils.isMobile() ? zipCode.split("\\n")[1].trim() : zipCode.split(" ")[1].trim();
    	return zipCode;
    }
}