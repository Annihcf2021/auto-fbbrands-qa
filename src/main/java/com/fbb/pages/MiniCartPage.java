package com.fbb.pages;

import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;

public class MiniCartPage extends LoadableComponent<MiniCartPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	public static final String MINICART = "//*[@class='top-menu-utility']/div[@class='menu-utility']/div[@class='menu-utility-user']/div[@class='right-section']/div[@id='mini-cart']/div[@class='mini-cart-total']/div[@class='mini-cart-content']";
	
	public static final String MINICART_PRODUCT = MINICART +"/div[@class='mini-cart-products']/div[@class='mini-cart-product  ']";
	
	public static final String MINICART_PRODUCT_RIGHT = MINICART_PRODUCT +"/div[@class='mini-cart-products']/div[@class='mini-cart-product  ']/div[@class='right-content']/div[@class='mini-cart-product-info']";
	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(css = ".mini-cart-content")
	WebElement miniCartContent;
	
	@FindBy(css = ".top-banner #mini-cart .mini-cart-total .mini-cart-link .minicart-quantity")
	WebElement miniCartBagQuanity;

	@FindBy(css = "#mini-cart .mini-cart-content .mini-cart-totals .mini-cart-subtotals .value")
	WebElement lblTotalCostInMinicartOverlay;
	
	@FindBy(css = ".mini-cart-content")
	WebElement miniCartOverlay;
	
	@FindBy(css = ".mini-cart-total .mini-cart-image")
	WebElement miniCartImage;
	
	@FindBy(css = ".mini-cart-total  .mini-cart-product-info .mini-cart-name a")
	WebElement miniCartName;
	
	@FindBy(xpath = MINICART_PRODUCT_RIGHT + "/div[@class='mini-cart-attributes']/div[@data-attribute='size']/span[@class='value']")
	WebElement miniCartSizeVariation;
	
	@FindBy(css = ".mini-cart-total .mini-cart-content .mini-cart-product-info .mini-cart-attributes div[data-attribute = 'size']")
	WebElement miniCartSizeVariationByCSS;
	
	@FindBy(xpath = MINICART_PRODUCT_RIGHT + "/div[@class='mini-cart-attributes']/div[@data-attribute='color']/span[@class='value']")
	WebElement miniCartColorVariation;
	
	@FindBy(css = ".mini-cart-total .mini-cart-content .mini-cart-product-info .mini-cart-attributes div[data-attribute = 'color']")
	WebElement miniCartColorVariationByCSS;
	
	@FindBy(xpath = MINICART_PRODUCT_RIGHT + "/div[@class='mini-cart-pricing']/span[@class='value']")
	WebElement miniCartProductQuantity;
	
	@FindBy(css = ".mini-cart-total .mini-cart-content .mini-cart-product-info .product-availability-list")
	WebElement miniCartProductQuantityByCSS;
	
	@FindBy(xpath = MINICART_PRODUCT_RIGHT + "/div[@class='mini-cart-pricing']//div[@class='total-price']/span[@class='value']")
	WebElement miniCartProductTotalPrice;
	
	@FindBy(css = ".mini-cart-total .mini-cart-content .mini-cart-product-info .total-price")
	WebElement miniCartProductTotalPriceByCSS;
	
	@FindBy(xpath = MINICART_PRODUCT_RIGHT + "/div[@class='mini-cart-pricing']//div[@class='product-price']/span[@class='price-sales ']")
	WebElement miniCartProductPrice;
	
	@FindBy(css = ".mini-cart-total .mini-cart-content .mini-cart-product-info .product-price")
	WebElement miniCartProductPriceByCSS;
	
	@FindBy(xpath = MINICART_PRODUCT_RIGHT + "/div[@class='mini-cart-pricing']/span[@class='value']")
	List<WebElement> miniCartProducts;
	
	@FindBy(css = ".mini-cart-product .attribute[data-attribute='shoeSize']")
	List<WebElement> shoeSizeproduct;
	
	@FindBy(xpath = ".mini-cart-content .mini-cart-subtotals .value")
	WebElement miniCartProductSubTotalPrice;
	
	@FindBy(css = "#mini-cart .mini-cart-content .mini-cart-products .mini-cart-product .right-content")
	List<WebElement> miniCartProductinfo;
	
	@FindBy(css = "#mini-cart .mini-cart-content .mini-cart-products .mini-cart-product .mini-cart-product-info")
	WebElement miniCartProductinfoFirst;
	
	@FindBy(css = "#mini-cart .mini-cart-content .mini-cart-products .mini-cart-product .right-content .mini-cart-product-info .mini-cart-name>a")
	List<WebElement> miniCartProductName;
	
	@FindBy(css = ".mini-cart-name.is-product-brand")
	List<WebElement> lstCartedItems;
	
	@FindBy(css = "#mini-cart .mini-cart-content .mini-cart-products .mini-cart-product .right-content .mini-cart-product-info .mini-cart-name>a")
	WebElement miniCartfluoytProductName1st;
	
	@FindBy(css = "#mini-cart .mini-cart-content .mini-cart-products .mini-cart-product .left-content .mini-cart-image > a")
	List<WebElement> miniCartProductImage;
	
	@FindBy(css = "#mini-cart .mini-cart-products-wrapper")
	WebElement miniCartInnerOverLay;
	
	@FindBy(css = "#mini-cart .mini-cart-content")
	WebElement miniCartOuterOverLay;
	
	@FindBy(css = ".primary-logo svg")
	WebElement miniCartPrimayLogo;
	
	@FindBy(css = ".right-section .total-qty")
	WebElement qtyMiniCartBagIndicator;

	@FindBy(css = ".mini-cart-content .slimScrollDiv .slimScrollBar")
	WebElement divscrollerMinicart;
	
	@FindBy(xpath = "//div[@class='mini-cart-totals']/div[@class='mini-cart-subtotals']/span[contains(text(),'Sub Total:')]/following-sibling::*[1]")
	WebElement txtSubTotalmoretotal;
	
	@FindBy(css = ".button.mini-cart-link-cart")
	WebElement divCheckOutButton;
	
	@FindBy(css = ".mini-cart-upper-cta .button.mini-cart-link-cart")
	WebElement divTopCheckOutButton;
	
	@FindBy(css = "div:not([class='mini-cart-upper-cta']) > a.button.mini-cart-link-cart")
	WebElement divBottomCheckOutButton;
	
	@FindBy(xpath = "//div[@class='mini-cart-totals']/div[@class='mini-cart-totals-addtocartoverlay']/div[@class='mini-cart-subtotals']/span[contains(text(),'Order Subtotal:')]/following-sibling::*[1]")
	WebElement txtSubTotal;
	
	@FindBy(css = "#mini-cart .mini-cart-image a>img")
	WebElement imgProductImage;
	
	@FindBy(css = "#mini-cart .mini-cart-image .brand-logo img")
	WebElement imgBrandLogoUnderPrdImage;
	
	@FindBy(css = "#mini-cart .mini-cart-name a")
	WebElement lnkProductName;
	
	@FindBy(css = "#mini-cart .mini-cart-name a")
	List<WebElement> lnkProductNameList;
	
	@FindBy(css = "#mini-cart .mini-cart-attributes")
	WebElement divProductAttributes;
	
	@FindBy(css = "#mini-cart .mini-cart-product-info")
	WebElement divProductAttributesGC;
	
	@FindBy(css = ".shopping-bag-sm .minicart-quantity")
	WebElement lblQuantity;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing>span[class='value']")
	WebElement divQuantityValue;
	
	@FindBy(css = "#mini-cart .product-availability-list span.label")
	WebElement lblAvailability;
	
	@FindBy(xpath = "//div[@id='mini-cart']//div[@class='product-availability-list']//span[2]")
	WebElement divAvailabilityValue;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .lineitem-price")
	WebElement lblPrice;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing span.mini-cart-price.value")
	WebElement lblPriceGC;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .product-price")
	WebElement divPriceValue;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .product-price .price-sales:not(.price-standard-exist)")
	WebElement divPriceSingle;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .product-price .price-standard")
	WebElement divPriceStandard;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .product-price .price-sales.price-standard-exist")
	WebElement divPriceSales;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .mini-cart-price.value")
	WebElement lblGiftCertPrice;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .total-price span.label")
	WebElement lblTotalLabel;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .total-price span.value")
	WebElement lblTotal;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .total-price .value")
	WebElement lblGiftCertTotal;
	
	@FindBy(css = "#mini-cart .mini-cart-pricing .product-price")
	WebElement divTotalValue;
	
	@FindBy(css = "#mini-cart .mini-cart-product")
	List<WebElement> divProductSection;
	
	@FindBy(css = "#mini-cart .minicartslot")
	WebElement miniCartContentSlot;
	
	@FindBy(css = "#mini-cart .mini-cart-subtotals")
	WebElement divSubTotal;
	
	@FindBy(css = "#mini-cart .mini-cart-subtotals .value")
	WebElement lblSubTotalValue;
	
	@FindBy(css = "#mini-cart .giftcard.from")
	WebElement divGiftCardFrom;
	
	@FindBy(css = "#mini-cart .giftcard.to")
	WebElement divGiftCardTo;
	
	@FindBy(css = "#mini-cart .special-productset-child")
	List<WebElement> spsChildPrds;
	
	@FindBy(css = "#mini-cart .special-productset-child")
	WebElement spsFirstChildPrd;
	
	@FindBy(css = "#mini-cart .special-productset-child [data-attribute='color']")
	List<WebElement> spsChildPrdColor;
	
	@FindBy(css = "#mini-cart .special-productset-child [data-attribute='size']")
	List<WebElement> spsChildPrdSize;
	
	@FindBy(css = "#mini-cart .special-productset-child .product-availability-list")
	List<WebElement> spsChildPrdAvailability;
	
	@FindBy(css = "#mini-cart .slimScrollDiv")
	WebElement scrollSection;
	
	@FindBy(css = "#mini-cart .mini-cart-products")
	WebElement divProducts;
	
	@FindBy(css = "#mini-cart .slimScrollBar")
	WebElement scrollBar;
	
	//================================================================================
	//			WebElements Declaration End
	//================================================================================


	/**
	 * constructor of the class
	 * @param driver - Webdriver
	 */
	public MiniCartPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		if (isPageLoaded && !(Utils.waitForElement(driver, miniCartContent))) {
			Log.fail("Mini cart page didn't open up", driver);
		}
		
		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}
	
	public String getTotalCost()throws Exception{
		return lblTotalCostInMinicartOverlay.getAttribute("innerHTML").trim().replace("$", "");
	}
	
	/**
	 * To click on product image of 
	 * @param index -
	 * @return PdpPage 
	 * @throws Exception - Exception
	 */
	public PdpPage ClickOnProductImage(int index)throws Exception{
		Utils.waitForDisabledElement(driver, miniCartOverlay , 10);
		BrowserActions.mouseHover(driver, miniCartImage, "Mouse Hover the miniCart content");
		Utils.waitForDisabledElement(driver, miniCartProductImage.get(index) , 10);
		BrowserActions.clickOnElementX(miniCartProductImage.get(index), driver, "mini cart image");
		return new PdpPage(driver).get();
	}
	
	/**
	 * To click on First Product Image in Mini cart Overlay
	 * @return PdpPage - PdpPage page object
	 * @throws Exception - Exception
	 */
	public PdpPage clickOnProductImage()throws Exception{
		
		BrowserActions.clickOnElementX(imgProductImage, driver, "First Product Image");
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, imgProductImage))
		{
			BrowserActions.clickOnElementX(imgProductImage, driver, "First Product Image");
		}
		return new PdpPage(driver).get();
	}
	
	/**
	 * To click on the product name in Minicard Flyout
	 * @param index -
	 * @return PdpPage of Product
	 * @throws Exception - Exception
	 */
	public PdpPage ClickOnProductName(int index)throws Exception{
		Utils.waitForElement(driver, miniCartOverlay);
		Utils.waitForElement(driver, miniCartProductName.get(index));
		BrowserActions.clickOnElementX(miniCartProductName.get(index), driver, "Image icon");
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}
	/**
	 * To get the product name in Minicard Flyout
	 * @param index -
	 * @return PdpPage of Product
	 * @throws Exception - Exception
	 */
	public String getProductName(int index)throws Exception{
		Utils.waitForElement(driver, miniCartProductName.get(index));		
		return BrowserActions.getText(driver, miniCartProductName.get(index), "ProductName") ;
	}
	
	
	/**
	 * To verify minicart flyout is open / collapsed
	 * @return true - if flyout is visible
	 * @throws Exception - Exception
	 */
	public boolean verifyMiniCartFlyout()throws Exception{
		boolean flag=false;
		
		if(Utils.isDesktop()){
			BrowserActions.mouseHover(driver, miniCartContent, "Mouse Hover the miniCart content");
			
			if(Utils.waitForElement(driver, miniCartOverlay , 10)){
				flag=true;
			}
			
		}
		else if(Utils.isTablet()){
			Actions action = new Actions(driver);
			action.clickAndHold(miniCartContent).build().perform();
		}
		
		return flag;
	}
	
	/**
	 * To calculate total value of given variation 
	 * @param quantity -
	 * @param price -
	 * @param total -
	 * @return String - Total value 
	 * @throws Exception - Exception
	 */
	public String verifyMiniCartTotal(String quantity,String price,String total)throws Exception{

		double productQuantity = Double.parseDouble(quantity);
		double productPrice = Double.parseDouble(price.split("\\$")[1]);

		Log.message("Total is verified in minicart , Total =" +productQuantity*productPrice);
		return ("$"+String.valueOf(productQuantity*productPrice));
	}
	
	/**
	 * To click on checkout in mini cart flyout
	 * @return ShoppingBagPage - Cart page
	 * @throws Exception - Exception
	 */
	public ShoppingBagPage clickOnCheckOut()throws Exception{
		if(Utils.waitForElement(driver, divCheckOutButton)){
			BrowserActions.clickOnElementX(divCheckOutButton, driver, "mini cart image", false);
			Utils.waitForPageLoad(driver);
		}
		return new ShoppingBagPage(driver).get();	
	}
	
	/**
	 * To get Quantity of products in minicart flyout
	 * @return int  - number of products
	 * @throws Exception - Exception
	 */
	public int verifyMiniCartBagQuantity()throws Exception{

		return Integer.parseInt(miniCartBagQuanity.getText());
	}
	
	/**
	 * To verify minicart flyout is opened
	 * @return true - if minicart flyout is visible
	 * @throws Exception - Exception
	 */
	public boolean verifyMinicartCloseAction()throws Exception{
		BrowserActions.scrollToBottomOfPage(driver);
		if(miniCartOuterOverLay.isDisplayed())
			return true;
		else
			return false;
	}
	
	/**
	 * To get Mini cart count near bag icon in headers
	 * @return int - Total no.of products in bag
	 * @throws Exception - Exception
	 */
	public int getMiniCartCount()throws Exception{
		//boolean flag=false;
		int qty=0;
		BrowserActions.moveToElementJS(driver, qtyMiniCartBagIndicator);
		
		if(qtyMiniCartBagIndicator.getText().equals("")){
			qty=0;
		}else{
			qty=Integer.parseInt(qtyMiniCartBagIndicator.getText());
		}
		
		return qty;
		
	}
	
	/**
	 * To get color code of first product in list
	 * @return String - Color code
	 * @throws Exception - Exception
	 */
	public String getProductColorCode()throws Exception{
		String imgPath[] = imgProductImage.getAttribute("src").trim().split("\\/");
		Log.event("Image Path :: " + imgPath);
		String imgCode = imgPath[imgPath.length - 1].split("\\_")[3].split("\\?")[0].replace(".jpg", "");
		return imgCode;
	}
	
	/**
	 * To click on First product name link
	 * @return PdpPage - PdpPage page object
	 * @throws Exception - Exception
	 */
	public PdpPage clickOnProductName()throws Exception{
		BrowserActions.clickOnElementX(lnkProductName, driver, "First Product Name Link");
		return new PdpPage(driver).get();
	}
	
	/**
	 * To calculate sub total from each product total
	 * @return float - Sub total of all products
	 * @throws Exception - Exception
	 */
	public double calculateSubTotal(double ...discount)throws Exception{
		double price = 0;
		for(WebElement prdSection : divProductSection) {
			price += Double.parseDouble(prdSection.findElement(By.cssSelector(".mini-cart-pricing .lineitem-price span.price-sales")).getText().replace("$", "").trim());
		}
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		if(discount.length > 0) {
			price = Double.parseDouble(df.format(price - discount[0]));
		} else {
			price = Double.parseDouble(df.format(price));
		}
		Log.event("Calculated Sub Total :: " + price);
		return price;
	}
	
	/**
	 * To get float value from sub total label
	 * @return float - sub total
	 * @throws Exception - Exception
	 */
	public double getSubTotal()throws Exception{
		String price = null;
		price = lblSubTotalValue.getText().replace("$", "").trim();
		Log.event("Displayed Sub Total :: " + price);
		return StringUtils.getPriceFromString(price);
	}
	
	/**
	 * To get size value by product index
	 * @param index - index
	 * @return String - size
	 * @throws Exception - Exception
	 */
	public String getSizeByPrdIndex(int index)throws Exception{
			try{
			WebElement element= divProductSection.get(index).findElement(By.cssSelector(".attribute[data-attribute='size']"));
				if(Utils.waitForElement(driver, element))	
				return divProductSection.get(index).findElement(By.cssSelector(".attribute[data-attribute='size'] span.value")).getText().trim();
			}catch(NoSuchElementException e){
			Log.event("Product is not a normal size product/ product may be shoe size product");	
			}
		try{
			WebElement element= divProductSection.get(index).findElement(By.cssSelector(".attribute[data-attribute='shoeSize']"));
			if(Utils.waitForElement(driver, element))	
				return divProductSection.get(index).findElement(By.cssSelector(".attribute[data-attribute='shoeSize'] span.value")).getText().trim();
			}catch(NoSuchElementException e){
			Log.event("Product is not a Shoesize product");	
			}
		return "";
	}
	
	/**
	 * To get quantity value by product index
	 * @param index - index
	 * @return String - quantity
	 * @throws Exception - Exception
	 */
	public String getQtyByPrdIndex(int index)throws Exception{
		return divProductSection.get(index).findElement(By.cssSelector(".mini-cart-pricing>span[class='value']")).getText().trim();
	}
	
	/**
	 * To get total price by product index
	 * @param index - index
	 * @return float - total price
	 * @throws Exception - Exception
	 */
	public float getTotalByPrdIndex(int index)throws Exception{
		return Float.parseFloat(divProductSection.get(index).findElement(By.cssSelector(".total-price .value")).getText().replace("$", "").trim());
	}
	
	/**
	 * To get name list of products in mini-cart flyout.	
	 * @return HashSet<String> - Names of products
	 * @throws Exception - Exception
	 */
	public HashSet<String> getCartItemNameList() throws Exception {
		HashSet<String> listCartItemNames = new HashSet<String>();
		for(WebElement cartItem: lnkProductNameList) {
			listCartItemNames.add(cartItem.getText().trim().toLowerCase());
		}
		return listCartItemNames;
	}
	
	/**
	 * To verify the added products are displayed in Minicard flyout
	 * @param productNameToVerify - Product names to verify
	 * @return productDisplayed - true if all products displayed in mini cart flyout else false
	 * @throws Exception - Exception
	 */
	public boolean verifyProductDisplayedInOverlay(HashSet<String> productNameToVerify) throws Exception {
		boolean productDisplayStatus = false;
		HashSet<String>  productName = new HashSet<String>();
		for(WebElement elem : miniCartProductName) {
			productName.add(BrowserActions.getText(driver, elem , "ProductName").toLowerCase());
		}
		for(String prdName : productNameToVerify) {
			if(productName.contains(prdName)) {
				productDisplayStatus = true;
			} else {
				productDisplayStatus = false;
				Log.message(prdName + " is not displayed in mini cart ocerlay");
				break;
			}
		}
		return productDisplayStatus;
	}
}
