package com.fbb.pages;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.Brand;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.StopWatch;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;


public class SearchResultPage extends LoadableComponent<SearchResultPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	public Refinements refinements;
	String runBrowser;

	/**********************************************************************************************
	 ********************************* WebElements of Search ResultPage - start ****************************
	 **********************************************************************************************/
	private static final String LNK_VIEW_MORE_LESS = ".view-more.hide-desktop a";

	@FindBy(css = ".loader[style*='block']")
	WebElement slpSpinner;

	//---------WebElements Mobile------------//

	@FindBy(css = ".view-more.hide-desktop")
	List<WebElement> btnViewMore;
	
	@FindBy(css = ".view-more.hide-desktop > a")
	WebElement btnViewMoreButton;

	@FindBy(css = ".searchresult-hide-desktop")
	WebElement lblResultCountMobile;
	//---------------------------------------//

	//----------WebElements Desktop----------//

	@FindBy(css = ".searchresult-hide-mobile")
	WebElement lblResultCountDesktop;

	//---------------------------------------//


	@FindBy(css = ".category-search #q")
	WebElement secondarySearchSection;
	
	@FindBy(css = ".category-search button[type='submit']")
	WebElement secondarySearchButton;
	
	@FindBy(css = ".result-count .searchresult-hide-desktop")
	WebElement searchResultCountMob;
	
	@FindBy(css = ".result-count .searchresult-hide-mobile")
	WebElement searchResultCountDeskTab;

	@FindBy(id = "search-result-items")
	WebElement sectionSearchResult;

	@FindBy(css = ".category-search input[name='q']")
	WebElement txtSeacondarySearch;

	@FindBy(css = ".phrase-suggestions")
	WebElement searchSuggestion;

	@FindBy(css = ".grid-tile")
	List<WebElement> lstProducts;
	
	@FindBy(css = ".grid-tile .product-name")
	List<WebElement> lstProductsName;

	@FindBy(css = ".mobile-refinement")
	WebElement divRefinement;

	@FindBy(css = ".search-result-options.hide-tablet.hide-mobile .sort-by .selected-option.selected")
	WebElement drpSortBySelectedDesktop;
	
	@FindBy(css = ".search-brands")
	WebElement popularSearches;
	
	@FindBy(css = ".trending")
	WebElement lblTrendingNow;
	
	@FindBy(css = ".carousel-recommendations.slick-initialized.slick-slider")
	WebElement divTrendingNow;
	
	@FindBy(css = "#quickviewbutton")
	List<WebElement> lstQuickshopButtons;

	@FindBy(css = ".product-tile .name-link")
	List<WebElement> lstProductName;

	@FindBy(css = ".product-sales-price")
	List<WebElement> txtListProductPrice;

	@FindBy(css = ".refinements.ws-sticky")
	WebElement refinementSection;
	
	@FindBy(css = ".refinements .filters")
	WebElement divRefinementFilter;
	
	@FindBy(css = ".product-price")
	WebElement divPrdPrice;
	
	@FindBy(css = ".search-result-options .sort-by")
	WebElement divRefinementSortBy;
	
	@FindBy(css = ".refinements .sort-by")
	WebElement divRefinementSortByTabMob;
	
	@FindBy(css = ".category-search .simpleSearch input#q")
	WebElement inpSecondrySearch;

	@FindBy(css = "div#main.clearfix.ref_fixed .category-search")
	WebElement secondarySearchStickyBar;

	@FindBy(css = ".category-search>div>form>fieldset>button[type='submit']")
	WebElement secondarySearchIcon;
	
	@FindBy(css = ".category-search")
	WebElement lblResultCountContainer;

	@FindBy(css = ".category-search .result-count")
	WebElement lblresultCountAboveSecondarySearchBox;

	@FindBy(css = ".search-result-options .result-count")
	WebElement lblresultCountBelowRefinementBar;

	@FindBy(css = ".section-header>p")
	WebElement lblNotAbleToFindResults;

	@FindBy(css = ".section-header")
	WebElement secondarySearchSectionNoResultsFound;

	@FindBy(css = ".section-header input[name='q']")
	WebElement txtSeacondarySearchNoResultsFound;

	@FindBy(css = ".pagination>ul>li:not(.dots):not(.current-page):not(.page-next):not(.first-last)>a")
	List<WebElement> lstPaginationNumbers;
	
	@FindBy(css = ".pagination .current-page")
	WebElement selectedPage;
	
	@FindBy(css = "#secondary")
	WebElement leftNavigation;

	@FindBy(css = "a.page-previous")
	WebElement btnPaginationPrevious;

	@FindBy(css = "a.page-next")
	WebElement btnPaginationNext;
	
	@FindBy(css = ".product-name")
	List<WebElement> lblProductName;

	@FindBy(css = ".pagination li.current-page")
	WebElement btnPaginationCurrentPage;

	@FindBy(css = "[title$=': 1']")
	WebElement btnPaginationFirstPage;

	@FindBy(css = ".page-last")
	WebElement btnPaginationLastPage;

	@FindBy(css = ".pagination")
	WebElement paginationSection;

	@FindBy(css = ".search-result-options .sort-by.custom-select .selected-option")
	WebElement lblSortByOptionSelected;

	@FindBy(css = ".sort-by.custom-select.current_item .selection-list")
	WebElement flytSortByFlyOut;

	@FindBy(css = ".sort-by.custom-select.current_item .selection-list > li")
	List<WebElement> lstSortByOptions;

	@FindBy(css = "li:not(.two-space-tile):not(.one-space-tile) .product-tile:not([class*='set'])")
	List<WebElement> productTile;

	@FindBy(css = ".search-result-items>li")
	WebElement lstProductTiles;

	@FindBy(css = ".no-results-heading")
	WebElement lblNoResults;

	@FindBy(css = ".search-result-content")
	WebElement searchResultContent;

	@FindBy(css = "a.page-next")
	WebElement lnkNextPage;
	
	@FindBy(css = ".product-tile")
	WebElement lnkProdTile;
	
	@FindBy(css = ".hide-tablet.hide-mobile label[for='grid-sort-header']")
	WebElement selSortBy;
	
	@FindBy(css = ".product-tile .product-image")
	WebElement firstProductImage;

	@FindBy(css = ".product-name")
	WebElement firstProductName;
	
	@FindBy(css = ".product-pricing")
	WebElement firstProductPrice;

	@FindBy(css = ".product-standard-price")
	WebElement firstProductStandardPrice;

	@FindBy(css = ".product-swatches .swatch-list")
	WebElement firstProductSwatchList;
	
	@FindBy(css = ".product-swatches .swatch-list")
	List<WebElement> lstProductSwatchList;

	@FindBy(id = "category-level-1")
	WebElement leftHandNavigation;

	@FindBy(css = ".no-hits-search-term-suggest")
	WebElement lnkDidYouMeanSuggestion;

	@FindBy(css = ".button.shop-newarrivals")
	WebElement btnShopNewArrivals;
	
	@FindBy(css = ".product-name-image-container .product-image")
	WebElement imgTendingImage;

	@FindBy(css = "div[class='product-name']>a")
	List<WebElement> productlink;

	@FindBy(id = "search-result-items")
	WebElement sectionSearchResults;
	
	@FindBy(css = "#search-result-items>li.grid-tile ")
	List<WebElement> lstProductTile;

	@FindBy(id = "quickviewbutton")
	WebElement btnQuickView;

	@FindBy(css = ".viewmore-grid")
	WebElement lnkViewMore_mobileOrTablet;

	@FindBy(css = ".loader-bg")
	WebElement waitLoader;
	
	@FindBy(css = ".b_product_badge img")
	WebElement imgProductBadge;
	
	@FindBy(css = ".swatch-list img:not([src*='NO-SWATCH-TAB.svg'])")
	WebElement firstSwatchWithImage;

	/**********************************************************************************************
	 ********************************* WebElements of Search ResultPage - Mobile ****************************
	 **********************************************************************************************/

	@FindBy(css = ".hide-desktop .sort-by .selected-option.selected")
	WebElement drpSortBySelectedMobileTablet;

	/**********************************************************************************************
	 ********************************* WebElements of Search ResultPage - Ends ****************************
	 **********************************************************************************************/


	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public SearchResultPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
		refinements = new Refinements(driver).get();
		elementLayer = new ElementLayer(driver);
		runBrowser  = Utils.getRunBrowser(driver);
	}

	@Override
	protected void load() {
		Utils.waitForPageLoad(driver);
		isPageLoaded = true;
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		//Depending on the Search Result, the locator changes. hence checking if at least one locator is present
		if (isPageLoaded && !(Utils.waitForElement(driver, sectionSearchResult) || Utils.waitForElement(driver, secondarySearchSectionNoResultsFound))) {
			Log.fail("Search result Page did not open up. Search Results might not be found.", driver);
		}
		
		try {
			GlobalNavigation.closePopup(driver);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	/**
	 * To get the Load status of page
	 * @return boolean - 'true'(Page loaded) / 'false'(Page not loaded)
	 * @throws Exception - Exception
	 */
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}

	/**
	 * To search products with keyword
	 * @param textToSearch - 'Search Term'
	 * @return - SearchResultPage
	 * @throws Exception - Exception
	 */
	public SearchResultPage secondarySearchProductKeyword(String textToSearch)throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		final long startTime = StopWatch.startTime();
		if(Utils.isDesktop()) {
			BrowserActions.typeOnTextField(txtSeacondarySearch, textToSearch, driver,"Entering category in the search field");
			txtSeacondarySearch.sendKeys(Keys.ENTER);
		} else {
			BrowserActions.typeOnTextField(txtSeacondarySearch, textToSearch, driver,"Entering category in the search field");
			BrowserActions.clickOnElementX(secondarySearchIcon, driver, "Search icon");
		}
		Log.event("Searched the provided product!",StopWatch.elapsedTime(startTime));
		return new SearchResultPage(driver).get();
	}

	/**
	 * To verify the cursor is in secondary search box
	 * @return 'true' - cursor is in secondary search box
	 * 		   'false' - cursor not placed in secondary search box
	 * @throws Exception - Exception
	 */
	public boolean verifySecondarySearchCursor()throws Exception{
		boolean status = false;

		if(txtSeacondarySearch.equals(driver.switchTo().activeElement()))
			status = true;

		return status;
	}

	/**
	 * To click on the secondary search box
	 * @throws Exception - Exception
	 */
	public void clickOnSecondarySearch()throws Exception{
		BrowserActions.clickOnElementX(txtSeacondarySearch, driver, "Secondary Search Box");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get the text present in secondary search box
	 * @return String - Search term in secondary search box
	 * @throws Exception - Exception
	 */
	public String getTextFromSecondarySearchBox()throws Exception{
		return BrowserActions.getText(driver, txtSeacondarySearch, "Secondary Searchbox");
	}

	/**
	 * To get the text from Secondary Search box when there is no result found for given search term
	 * @return String - Search term in secondary search box
	 * @throws Exception - Exception
	 */
	public String getTextFromSecondarySearchBoxWhenNoResultsFound()throws Exception{
		return BrowserActions.getText(driver, txtSeacondarySearchNoResultsFound, "Secondary Searchbox When No Results Found");
	}

	/**
	 * To type given string into search input box
	 * @param txtToType - 
	 * @throws Exception - Exception
	 */
	public void typeTextInSearchField(String txtToType)throws Exception{
		BrowserActions.typeOnTextField(txtSeacondarySearch, txtToType, driver, "Secondary Searchbox");
	}

	/**
	 * to verify that the Secondary search is above the refinement bar
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySecondarySearchIsAboveRefinementBar()throws Exception{
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, secondarySearchSection, refinementSection);
	}

	/**
	 * to verify that the Secondary search icon is right to the secondary search box
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySecondarySearchIconIsRightToSecondarySearchBox()throws Exception{
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, secondarySearchIcon, secondarySearchSection);
	}

	/**
	 * to verify that the Secondary search icon is above the refinement bar
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySecondarySearchIconIsAboveRefinementBar()throws Exception{
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, secondarySearchIcon, refinementSection);
	}

	/**
	 * to verify that the 'We were not able to find results for' message is above the Secondary search box
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyNoResultsFoundMessageIsAboveSecondarySearchBox()throws Exception{
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, lblNotAbleToFindResults, txtSeacondarySearchNoResultsFound);
	}

	/**
	 * To verify the 'We were not able to find results for' message above the Secondary Search box
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyNoResultsFoundMessage()throws Exception{
		String actualText = BrowserActions.getText(driver, lblNotAbleToFindResults, "We were not able to find results for message");

		if(actualText.equalsIgnoreCase("We were not able to find results for"))
			return true;
		else
			return false;
	}

	/**
	 * to verify the pagination number of the current page is highlighted in black color
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfCurrentPageNumberIsHighlihted()throws Exception{
		Brand brand = Utils.getCurrentBrand();
		switch(brand) {
		case rm:
			if(runBrowser.equals("MicrosoftEdge"))
				return Utils.verifyCssPropertyForElement(btnPaginationCurrentPage, "background-color", "rgb(51, 30, 83)");
			else return(Utils.verifyCssPropertyForElement(btnPaginationCurrentPage, "background-color", "rgba(51, 30, 83, 1)") ||
						Utils.verifyCssPropertyForElement(btnPaginationCurrentPage, "background-color", "rgba(51, 30, 84, 1)"));
		case jl:
		case ww:
			if(runBrowser.equals("MicrosoftEdge"))
				return Utils.verifyCssPropertyForElement(btnPaginationCurrentPage, "background-color", "rgb(0, 0, 0)");
			else
				return Utils.verifyCssPropertyForElement(btnPaginationCurrentPage, "background-color", "rgba(0, 0, 0, 1)");
		case ks:
		case el:
			if(runBrowser.equals("MicrosoftEdge"))
				return Utils.verifyCssPropertyForElement(btnPaginationCurrentPage, "background-color", "rgb(255, 255, 255)");
			else
				return Utils.verifyCssPropertyForElement(btnPaginationCurrentPage, "background-color", "rgba(255, 255, 255, 1)");
		default:
			Log.failsoft("Current brand not configured.");
			return false;
		}
	}

	/**
	 * to verify the current page tab is not clickable
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfCurrentPageNumberTabIsClickable()throws Exception{
		//isEnabled() is not applicable here. 
		//Hence checking if the Current Page tab does not have <a> tag
		if(btnPaginationCurrentPage.findElements(By.cssSelector("a")).isEmpty())
			return true;
		else
			return false;
	}

	/**
	 * to get the pagination number of the current page
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getCurrentPageNumber()throws Exception{
		return btnPaginationCurrentPage.getText().trim();
	}

	/**
	 * to click on the pagination number 
	 * @param pageNumberToBeClicked - Page number
	 * @throws Exception - Exception
	 */
	public void clickOnPageNumberInPagination(int pageNumberToBeClicked)throws Exception{
		String pgNo = null;
		
		if(Utils.waitForElement(driver, selectedPage)) {
			 pgNo = BrowserActions.getText(driver, selectedPage, "Selected Page");
		}
		
		
		String pgNoInput = Integer.toString(pageNumberToBeClicked);
		
		if (pgNo.trim().equals(pgNoInput)) {
			Log.message("The page number '"+pageNumberToBeClicked+"' is already selected");
		} else {
			WebElement pageNumber = null;
			try {
				pageNumber = driver.findElement(By.xpath("//div[@class='pagination']//a[contains(text(),'"+pageNumberToBeClicked+"')]"));
				
				BrowserActions.scrollInToView(pageNumber, driver);
				BrowserActions.clickOnElementX(pageNumber, driver, "Click On The Page Number");
				Utils.waitUntilElementDisappear(driver, waitLoader);
				Utils.waitForElement(driver, paginationSection);
			} catch (NoSuchElementException ex) {
				Log.failsoft("Given Page number "+pageNumberToBeClicked+" is not exist");
			}
		}
	}

	/**
	 * to verify the default sort option is 'RECOMMENDED'
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyDefaultSortOption()throws Exception{
		if(lblSortByOptionSelected.getText().equals("RECOMMENDED"))
			return true;
		else
			return false;
	}

	/**
	 * to get the selected Sort By option
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getSelectedSortOption()throws Exception{
		return lblSortByOptionSelected.getText();
	}

	/**
	 * to select a sorting option
	 * @param option - 
	 * @return SearchResultPage
	 * @throws Exception - Exception
	 */
	public SearchResultPage selectSortingOption(String option)throws Exception{
		BrowserActions.clickOnElementX(lblSortByOptionSelected, driver, "Click on Sort By");
		Utils.waitForElement(driver, flytSortByFlyOut);

		for (WebElement ele : lstSortByOptions) {
			if(ele.getText().equalsIgnoreCase(option))
			{
				BrowserActions.clickOnElementX(ele, driver, "Sort By option");
			}
		}
		return new SearchResultPage(driver).get();
	}

	/**
	 * to verify When clicked outside the flyout, the flyout should close
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySortByFlyOutClosesIfClickedOutside()throws Exception{
		BrowserActions.clickOnElementX(lblSortByOptionSelected, driver, "Click on Sort By");
		Utils.waitForElement(driver, flytSortByFlyOut);

		BrowserActions.clickOnElementX(txtSeacondarySearch, driver, "Click on Secondary Search box");

		if(!(flytSortByFlyOut.isDisplayed()))
			return true;
		else
			return false;
	}
	
	/**
	 * To verify Product Badge image in Particular Product
	 * @param productID - 
	 * 
	 * @return boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyProdBadgeImgInParticularProd(String productID, String badgeType)throws Exception{
		Utils.waitForPageLoad(driver);
		WebElement prdLink = null;
		boolean flag = true;
		while (flag) {
			try {
				prdLink = driver.findElement(By.cssSelector("div[data-itemid='"+ productID.trim() +"']"));
				flag = false;
			} catch (Exception ee) {
				if(ee.toString().contains("NoSuchElementException")) {
					try {
						BrowserActions.clickOnElementX(lnkNextPage, driver, "Next page");
						Utils.waitForPageLoad(driver);
					} catch (Exception ex) {
						if(ex.toString().contains("NoSuchElementException")) {
							return false;
						}
					}
				}
			}
		}
		
		if(badgeType.toLowerCase().contains("any")) {
			return true;
		} else {
			WebElement productBadge = prdLink.findElement(By.cssSelector(".b_product_badge img"));
			
			String[] badgeTypeSrc = BrowserActions.getTextFromAttribute(driver, productBadge, "src", "Product badge").split("\\/");
			Log.event("-- -- -- --- -"+badgeType+"-- -- -- --- -- --"+badgeTypeSrc[badgeTypeSrc.length - 1]);
			
			if(badgeTypeSrc[badgeTypeSrc.length - 1].toLowerCase().contains(badgeType.toLowerCase()))
				return true;
			else
				return false;
		}
	}
	
	/**
	 * to verify When clicked outside the flyout, the flyout should close
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyColorSwatchDisplaysForAProduct(String prdID)throws Exception{
		WebElement colorSwatch = null;
		try {
			colorSwatch = driver.findElement(By.cssSelector("div[data-itemid='"+prdID+"'] .product-swatches img"));
		} catch (NoSuchElementException ex) {
			return false;
		}
		
		if(Utils.waitForElement(driver, colorSwatch)) {
			BrowserActions.scrollToViewElement(colorSwatch, driver);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * to verify if the secondary search section changes to a sticky bar while scrolling page 
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfSecondarySearchChangesToStickyBarWhileScrolling()throws Exception{
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0,document.body.scrollHeight)", "");
		if(Utils.waitForElement(driver, secondarySearchStickyBar))
		{
			return true;
		}else
			return false;
	}

	public void scrollToTopOfPage()throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
	}


	/**
	 * To verify the 'No Results' message in place of the Search Results grid
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyNoResultsMessage()throws Exception{
		String actualText = BrowserActions.getText(driver, lblNoResults, "No Results message");

		if(actualText.equalsIgnoreCase("No results"))
			return true;
		else
			return false;
	}


	/** to verify the number of product tiles displayed in the search result page
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyNumberOfProductTilesDisplayed(int expectedCount)throws Exception{
		return (lstProducts.size() == expectedCount);
	}

	/**
	 * to get the number of product tiles displayed per row in the search results  
	 * @return int - rowCount
	 * @throws Exception - Exception
	 */
	public int getNumberOfProductTilesPerRow()throws Exception{
		List<WebElement> productTiles = driver.findElements(By.cssSelector(".search-result-items>li"));

		int rowCount = 1;
		for (int i = 0; i < productTiles.size(); i++) {
			if(BrowserActions.verifyElementsAreInSameRow(driver, productTiles.get(i+1), productTiles.get(i)))
				rowCount++;
			else
				break;
		}
		
		Log.event("Row Count :: " + rowCount);
		return rowCount;
	}

	/**
	 * To get the product name of nth-Product in search result
	 * @param index - Product index in product tiles
	 * @return String - Product Name
	 * @throws Exception - Exception
	 */
	public String getProductName(int index)throws Exception{
		String dataToReturn = BrowserActions.getText(driver, lstProductsName.get(index-1), index + "th Product Name ");
		return dataToReturn;
	}

	/**
	 * To get the price for all products in product tile or search results
	 * @return List - List of price of all products
	 * @throws Exception - Exception
	 */
	public List<String> getPriceOfAllProducts()throws Exception{
		List<String> lstPrice = new ArrayList<String>();
		for(int i=0; i < txtListProductPrice.size(); i++)
			lstPrice.add(txtListProductPrice.get(i).getText().trim());

		return lstPrice;
	}

	/**
	 * to verify that the product tiles are displayed below the refinement bar
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyProductTilesDisplayedBelowRefinementBar()throws Exception{
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, refinementSection, sectionSearchResult);
	}

	/**
	 * To get the name for all products in product tile or search results
	 * @return List - List of names of all products
	 * @throws Exception - Exception
	 */
	public List<String> getProductNamesFromSearchResults()throws Exception{
		List<String> lstPrice = new ArrayList<String>();
		for(int i=0; i < lstProductName.size(); i++)
			lstPrice.add(lstProductName.get(i).getText().trim());

		return lstPrice;
	}

	/**
	 * to clear the text entered in the secondary search box
	 * @throws Exception - Exception
	 */
	public void clearSecondarySearchText()throws Exception{
		txtSeacondarySearch.clear();
	}

	/**
	 * to verify that the sort by is displayed to the right of result count
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifySortByDisplayedToTheRightOfResultCount()throws Exception{
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, lblSortByOptionSelected, lblresultCountBelowRefinementBar);
	}

	/**
	 * to get the pagination number of the last page
	 * @return String
	 * @throws Exception - Exception
	 */
	public String getLastPageNumber()throws Exception{
		if(Utils.waitForElement(driver, selectedPage)) {
			List<WebElement> lastPage = driver.findElements(By.cssSelector("a[class^='page-'][title*='Go to page number']"));
			int count = driver.findElements(By.cssSelector("a[class^='page-'][title*='Go to page number']")).size();
			return lastPage.get(count-1).getText().trim();
		} else {
			return "0";
		}
	}

	/**
	 * To click on last page number in pagination
	 * @throws Exception -
	 */
	public void clickLastPageNumber()throws Exception{
		List<WebElement> lastPage = driver.findElements(By.cssSelector("a[class^='page-'][title*='Go to page number']"));
		int count = driver.findElements(By.cssSelector("a[class^='page-'][title*='Go to page number']")).size();
		BrowserActions.clickOnElementX(lastPage.get(count-1), driver, "Last page");
		Utils.waitUntilElementDisappear(driver, waitLoader);
		Utils.waitForElement(driver, paginationSection);
	}

	/**
	 * to verify if the specified page number is displayed in the pagination
	 * @param pageNum - 
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean isPageNumberDisplayed(String pageNum)throws Exception{
		//Check if the specified page number is in the list of non-current page numbers
		BrowserActions.scrollInToView(paginationSection, driver);
		
		for (WebElement pageNumber : lstPaginationNumbers) {
			if(pageNumber.getText().contains(pageNum))
				return true;
		}

		//Else check if the specified page number is the current page
		if(btnPaginationCurrentPage.getText().trim().contains(pageNum))
			return true;
		else
			return false;
	}

	/**
	 * to verify that the pagination numbers are displayed below the product tiles
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPaginationDisplayedBelowProductTiles()throws Exception{
		BrowserActions.scrollInToView(paginationSection, driver);
		return BrowserActions.verifyVerticalAllignmentOfElements(driver, sectionSearchResult, paginationSection);
	}

	/**
	 * to verify the pagination number of the current page is displayed in white color
	 * @return Boolean - true/false if page number is in correct color
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfCurrentPageNumberIsDisplayedInWhiteText()throws Exception{
		switch(Utils.getCurrentBrand()) {
		case ks:
			if (runBrowser.equals("MicrosoftEdge")) {
				return Utils.verifyCssPropertyForElement(btnPaginationCurrentPage, "color", "rgb(0, 41, 83)");
			}else {
				return Utils.verifyCssPropertyForElement(btnPaginationCurrentPage, "color", "rgba(0, 41, 83, 1)");
			}
		case el:
			if (runBrowser.equals("MicrosoftEdge")) {
				return Utils.verifyCssPropertyForElement(btnPaginationCurrentPage, "color", "rgb(0, 0, 0)");
			}else {
				return Utils.verifyCssPropertyForElement(btnPaginationCurrentPage, "color", "rgba(0, 0, 0, 1)");
			}
		default:
			if (runBrowser.equals("MicrosoftEdge")){
				return Utils.verifyCssPropertyForElement(btnPaginationCurrentPage, "color", "rgb(255, 255, 255)");
			} else {
				return Utils.verifyCssPropertyForElement(btnPaginationCurrentPage, "color", "rgba(255, 255, 255, 1)");
			}
		}
	}

	/**
	 * to verify the pagination number of the non-current page has white background
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfNonCurrentPageNumberHasWhiteBackground()throws Exception{
		Boolean flag = false;
		for (WebElement pagination : lstPaginationNumbers) {
			if(runBrowser.equals("internet explorer") || runBrowser.equals("firefox") || runBrowser.equals("MicrosoftEdge")){
				if(Utils.verifyCssPropertyForElement(pagination, "background-color", "transparent"))
					flag = true;
				else
					return false;
			}
			else if (runBrowser.equals("chrome") || runBrowser.equals("safari")){
				if(Utils.verifyCssPropertyForElement(pagination, "background-color", "rgba(0, 0, 0, 0)"))
					flag = true;
				else
					return false;
			}
		}
		return flag;
	}

	/**
	 * to verify the pagination number of the current page is displayed in white color
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyIfNonCurrentPageNumberIsDisplayedInGrayText()throws Exception{
		//ToDo: Change to load color data from brand property file when all property files are available 
		for (WebElement pagination : lstPaginationNumbers) {
			switch(Utils.getCurrentBrand()) {
			case rm:
			case jl:
				if (runBrowser.equals("MicrosoftEdge")) {
					if(!Utils.verifyCssPropertyForElement(pagination, "color", "rgb(102, 102, 102)"))
						return false;
				} else {
					if(!Utils.verifyCssPropertyForElement(pagination, "color", "rgba(102, 102, 102, 1)"))
						return false;
				}
				break;
			case ww:
				if (runBrowser.equals("MicrosoftEdge")) {
					if(!Utils.verifyCssPropertyForElement(pagination, "color", "rgb(162, 162, 162)"))
						return false;
				} else {
					if(!Utils.verifyCssPropertyForElement(pagination, "color", "rgba(162, 162, 162, 1)"))
						return false;
				}
				break;
			case ks:
				if (runBrowser.equals("MicrosoftEdge")) {
					if(!Utils.verifyCssPropertyForElement(pagination, "color", "rgb(0, 41, 83)"))
						return false;
				} else {
					if(!Utils.verifyCssPropertyForElement(pagination, "color", "rgba(0, 41, 83, 1)"))
						return false;
				}
				break;
			case el:
				if (runBrowser.equals("MicrosoftEdge")) {
					if(!Utils.verifyCssPropertyForElement(pagination, "color", "rgb(75, 75, 75)"))
						return false;
				} else {
					if(!Utils.verifyCssPropertyForElement(pagination, "color", "rgba(75, 75, 75, 1)"))
						return false;
				}
				break;
			default:
				Log.failsoft("Current brand not configured.");
				return false;
			}
		}
		return true;
	}

	/**
	 * to click the NEXT link in the pagination section
	 * @throws Exception - 
	 */
	public void clickNextButtonInPagination()throws Exception{
		if (Utils.waitForElement(driver, btnPaginationNext)) {
			BrowserActions.scrollInToView(paginationSection, driver);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", btnPaginationNext);
			Utils.waitForElement(driver, paginationSection);
			Utils.waitUntilElementDisappear(driver, waitLoader);
		} else {
			Log.failsoft("Next button is not found in page");
		}
	}

	/**
	 * to verify that the previous link is displayed to the left of 1st page number
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPreviousLinkDisplayedToTheLeftOfFirstPageNumber()throws Exception{
		BrowserActions.scrollInToView(paginationSection, driver);
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, btnPaginationCurrentPage, btnPaginationPrevious);
	}

	/**
	 * to verify that the next link is displayed to the right of last page number
	 * @return Boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyNextLinkDisplayedToTheRightOfLastPageNumber()throws Exception{
		BrowserActions.scrollInToView(paginationSection, driver);
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, btnPaginationNext, btnPaginationLastPage);
	}

	/**
	 * To scroll down to view more button
	 * @throws Exception -
	 */
	public void scrollToViewMore()throws Exception{
		BrowserActions.scrollToViewElement(btnViewMore.get(btnViewMore.size()-1), driver);
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get product tile count in page
	 * @return Integer - 
	 * @throws Exception -
	 */
	public int getProductTileCount()throws Exception{
		return lstProducts.size();
	}

	/**
	 * To get search Result count
	 * @return Integer -
	 * @throws Exception -
	 */
	public int getSearchResultCount()throws Exception{
		if(Utils.isDesktop() || Utils.isTablet()){
			String resultCount = BrowserActions.getText(driver, searchResultCountDeskTab, "Result Count").replace(" ", "|").replace(",","").split("\\|")[0];
			return StringUtils.getNumberInString(resultCount);
		}else {
			String resultCount = BrowserActions.getText(driver, searchResultCountMob, "Result Count").replace(" ", "|").replace(",","").split("\\|")[0];
			return StringUtils.getNumberInString(resultCount);
		}
	}

	/**
	 * To click the view more/less link
	 * 
	 * @throws Exception - Exception
	 */
	public void clickViewMoreOrLessLnk() throws Exception {
		if(Utils.waitForElement(driver, btnViewMore.get(btnViewMore.size()-1))){
			Log.event("clicking on view more button");
			List<WebElement> viewMoreBtn = driver.findElements(By.cssSelector(LNK_VIEW_MORE_LESS));
			BrowserActions.javascriptClick(viewMoreBtn.get(viewMoreBtn.size()-1), driver, "click on the Tab");
			waitUntilPlpSpinnerDisappear();
			Utils.WaitForAjax(driver);
			Utils.waitForPageLoad(driver);
		}else{
			Log.message("No More Products to View!");
		}
	}

	/**
	 * To wait for Search results spinner
	 */
	private void waitUntilPlpSpinnerDisappear() {
		Utils.waitUntilElementDisappear(driver, slpSpinner);
	}

	/**
	 * To navigate to pdp from slp
	 * @param index -
	 * @return PdpPage -
	 * @throws Exception -
	 */
	public PdpPage navigateToPdp(int... index)throws Exception{
		if(index.length > 0){
			if(runBrowser.equalsIgnoreCase("chrome")&&Utils.isDesktop())
			{
				BrowserActions.javascriptClick(lstProductName.get(index[0]-1), driver, index[0] + "th Product ");
			}
			else
			{
				BrowserActions.clickOnElementX(lstProductName.get(index[0]-1), driver, index[0] + "th Product ");
			}
		}else{
			BrowserActions.clickOnElementX(lstProductName.get(Utils.getRandom(0, lstProductName.size())), driver, "Random Product ");
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}

	/**
	 * To click the Suggestion link next to 'Did you mean :'
	 * @return SearchResultPage
	 * @throws Exception - Exception
	 */
	public SearchResultPage clickDidYouMeanSuggestionLink() throws Exception {
		BrowserActions.clickOnElementX(lnkDidYouMeanSuggestion, driver,
				"click on the Suggestion ");
		return new SearchResultPage(driver).get();
	}

	/**
	 * To click the Shop New Arrivals button
	 * @return PlpPage
	 * @throws Exception - Exception
	 */
	public SearchResultPage clickShopNewArrivalsButton() throws Exception {
		BrowserActions.clickOnElementX(btnShopNewArrivals, driver,
				"click on the Shop New Arrivals ");
		return new SearchResultPage(driver).get();
	}

	/**
	 * To click on quickshop button
	 * @param productID -
	 * @return QuickShop -
	 * @throws Exception -
	 */
	public QuickShop clickQuickShop(String productID)throws Exception{
		Log.event("Given Item ID :: " + productID.trim());
		WebElement prdTile = driver.findElement(By.xpath("//div[@data-itemid='"+productID+"']//div[@class='product-image']"));
		BrowserActions.scrollInToView(prdTile, driver);
		BrowserActions.mouseHover(driver, prdTile);
		WebElement quickShopButton = driver.findElement(By.xpath("//div[@data-itemid='"+productID.trim()+"']//a[@id='quickviewbutton']"));

		if(quickShopButton.isDisplayed()) {
			BrowserActions.clickOnElementX(quickShopButton, driver, "Quick Shop Button");
		}else{
			BrowserActions.javascriptClick(quickShopButton, driver, "Quick Shop Button");
		}

		return new QuickShop(driver).get();
	}
	
	/**
	 * To click on quickshop button
	 * @param productID -
	 * @return QuickShop -
	 * @throws Exception -
	 */
	public QuickShop clickOnQuickShopByIndex(int index)throws Exception{
		List<WebElement> prdTile = driver.findElements(By.xpath("//div[@class='product-image']"));
		BrowserActions.scrollInToView(prdTile.get(index-1), driver);
		BrowserActions.mouseHover(driver, prdTile.get(index-1));
		WebElement quickShopButton = prdTile.get(index-1).findElement(By.xpath("//a[@id='quickviewbutton']"));

		if(quickShopButton.isDisplayed()) {
			BrowserActions.clickOnElementX(quickShopButton, driver, "Quick Shop Button");
		}else{
			BrowserActions.javascriptClick(quickShopButton, driver, "Quick Shop Button");
		}

		return new QuickShop(driver).get();
	}

	/**
	 * To click a product link from the search results
	 * @param productname - 
	 * @return PdpPage
	 * @throws Exception - Exception
	 */
	public PdpPage selectproductfromsearchresults(String productname) throws Exception {
		if(Utils.isMobile()) {
			if (Utils.waitForElement(driver, lnkViewMore_mobileOrTablet)) {
				BrowserActions.scrollInToView(lnkViewMore_mobileOrTablet, driver);
				lnkViewMore_mobileOrTablet.click();
			}
		}
		for(WebElement link:productlink)
		{
			if(link.getText().contains(productname))
			{
				BrowserActions.clickOnElementX(link, driver,"click on "+productname+" link");
				break;
			}
		}
		return new PdpPage(driver).get();
	}

	/**
	 * To get search results count
	 * @return Integer -
	 * @throws Exception -
	 */
	public int getSearchedProductCountInPage() throws Exception{
		return lstProductName.size();
	}

	/**
	 * To navigate to PDP by product ID
	 * @param prdID -
	 * @return PdpPage -
	 * @throws Exception -
	 */
	public PdpPage navigateToPdpByPrdID(String prdID)throws Exception{
		Log.event("Trying to Click product name link for product id :: " + prdID);

			try {
				WebElement prdLink = driver.findElement(By.cssSelector("div[data-itemid='"+ prdID.trim() +"'] .name-link .product-name"));
				BrowserActions.clickOnElementX(prdLink, driver, "Name Link for Product " + prdID + " ");
				Utils.waitForPageLoad(driver);
			}catch(NoSuchElementException e) {
				if(Utils.waitForElement(driver, btnPaginationNext)) {
					BrowserActions.clickOnElementX(btnPaginationNext, driver, "Next Page ");
					Utils.waitUntilElementDisappear(driver, waitLoader);
					return navigateToPdpByPrdID(prdID);
				}
				else
					Log.fail("Product ID("+prdID+") not found.");
			}

		return new PdpPage(driver).get();
	}
	
	/**
	 * To verify given product id is present in search result
	 * @param prdName - give product name exactly same in the application
	 * @return boolean - true if found else false
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPdpByPrdName(String prdName) throws Exception{
		int size = lblProductName.size();
		for (int i=0 ; i < size; i++) {
			String name = BrowserActions.getText(driver, lblProductName.get(i), "Product Name");
			if (name.equalsIgnoreCase(prdName)) {
				BrowserActions.clickOnElementX(lblProductName.get(i), driver, "Product Name");
				Utils.waitForPageLoad(driver);
				return new PdpPage(driver).get();
			}
		}
		return null;
	}

	public boolean verifyBadgeImageLocation()throws Exception{
		WebElement prdImg = driver.findElement(By.xpath("//div[@class='b_product_badge']//img//ancestor::div[@class='product-image']"));
		WebElement badge = driver.findElement(By.xpath("//div[@class='b_product_badge']//img"));
		
		int x = prdImg.getLocation().getX() + (prdImg.getSize().width/2);
		int x1 = badge.getLocation().getX();
		if(x1<x)
			return true;
		return false;
	}
	
		
	/**
	 * To get list of product id
	 * @param noOfIdNeeded -
	 * @return String[] -
	 * @throws Exception -
	 */
	public String[] getListOfProductId(int noOfIdNeeded)throws Exception{
		String[] prdId = new String[noOfIdNeeded];
		for (int i=0;i<noOfIdNeeded;i++) {
			WebElement prdLink = productTile.get(i);
			prdId[i] = BrowserActions.getTextFromAttribute(driver, prdLink, "data-itemid", "product");
		}
		return prdId;
	}

	/**
	 * To verify given product id is present in search result
	 * @param prdID -
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean isProductPresentInPage(String prdID) throws Exception{
		try {
			WebElement prdTile = driver.findElement(By.cssSelector("div[data-itemid='" + prdID + "']"));
			if(prdTile.isDisplayed())
				return true;
			else
				return false;
		}catch(NoSuchElementException e) {
			Log.event("Given product ID not available in page");
			return false;
		}
	}
	
	/**
	 * To verify given product id is present in search result
	 * @param prdName - give product name exactly same in the application
	 * @return boolean - true if found else false
	 * @throws Exception - Exception
	 */
	public boolean isProductPresentInPageBasedOnName(String prdName) throws Exception{
		try {
			int size = lblProductName.size();
			for (int i=0 ; i < size; i++) {
				String name = BrowserActions.getText(driver, lblProductName.get(i), "Product Name");
				if (name.equalsIgnoreCase(prdName)) {
					return true;
				}
			}
			return false;
		}catch(NoSuchElementException e) {
			Log.event("Given product Name not available in page");
			return false;
		}
	}
	
	/**
	 * To click on product tile
	 * @param prdID -
	 * @throws Exception -
	 */
	public void clickOnProduct(String prdID) throws Exception{
		WebElement prdTile = driver.findElement(By.cssSelector("div[data-itemid='"+prdID+"'] .product-image img"));
		BrowserActions.clickOnElementX(prdTile, driver, "Product Tile");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on product tile
	 * @param index - index of product to click on
	 * @throws Exception
	 */
	public PdpPage clickOnProductBasedOnIndex(int index) throws Exception{
		if(lstQuickshopButtons.size() > 0) {
			WebElement productTile = lstQuickshopButtons.get(index-1).findElement(By.xpath("../.."));
			BrowserActions.scrollInToView(productTile, driver);
			BrowserActions.clickOnElementX(productTile, driver, "Product Tile");
		} else if(lstProductSwatchList.size() > 0) {
			WebElement productTile = lstProductSwatchList.get(index-1).findElement(By.xpath("../..")).findElement(By.cssSelector("a.name-link"));
			BrowserActions.scrollInToView(productTile, driver);
			BrowserActions.clickOnElementX(productTile, driver, "Product Tile");
		} else {
			BrowserActions.clickOnElementX(lstProductName.get(index-1), driver, "Product Tile");
		}
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}
	
	/**
	 * To get the price for given product in search result page
	 * @param productID -
	 * @return String -
	 * @throws Exception -
	 */
	public String getPriceOf(String productID)throws Exception{
		String lstPrice = new String();
		lstPrice = driver.findElement(By.cssSelector("div[data-itemid='" + productID + "'] .product-sales-price")).getText().trim();
		return lstPrice;
	}
	
	/**
	 * To click the Shop New Arrivals button
	 * @return PlpPage
	 * @throws Exception - Exception
	 */
	public PdpPage clickTrendingImage() throws Exception {
		BrowserActions.clickOnElementX(imgTendingImage, driver,
				"click on the Trending Image ");
		return new PdpPage(driver).get();
	}
	
	/**
	 * To navigate to a product that has all swatches configured
	 * @return PdpPage - product page object
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPDPAllSwatch() throws Exception{
		for(WebElement colorSwatch : lstProductSwatchList) {
			BrowserActions.scrollInToView(colorSwatch, driver);
			boolean swatchConfigured = true;
			List<WebElement> swatchList = colorSwatch.findElements(By.cssSelector("a>img"));
			for(WebElement swatch : swatchList) {
				if(swatch.getAttribute("src").toUpperCase().contains("NO-SWATCH") || swatch.getAttribute("data-src").toUpperCase().contains("NO-SWATCH")) {
					swatchConfigured = false;
				}
				if(!swatchConfigured)
					break;
			}
			if(swatchConfigured) {
				WebElement prdTile = colorSwatch.findElement(By.xpath("../..")).findElement(By.cssSelector(".product-name-image-container a"));
				BrowserActions.clickOnElementX(prdTile, driver, "Product tile with all swatch configured");
				Utils.waitForPageLoad(driver);
				return new PdpPage(driver).get();
			}
		}
		Log.event("Product with fully configured swatch not found.");
		return navigateToPdpFirstSwatch();
	}
	
	/**
	 * To navigate to a product that has it's first swatch configured
	 * @return PdpPage - product page object
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPdpFirstSwatch() throws Exception{
		for(WebElement colorSwatch : lstProductSwatchList) {
			BrowserActions.scrollInToView(colorSwatch, driver);
			boolean swatchConfigured = false;
			List<WebElement> swatches = colorSwatch.findElements(By.cssSelector("a>img"));
			for(WebElement swatch : swatches) {
				if(!swatch.getAttribute("src").toUpperCase().contains("NO-SWATCH")) {
					swatchConfigured = true;
					break;
				}
			}
			
			if(!swatchConfigured)
				continue;
			else{
				WebElement prdTile = colorSwatch.findElement(By.xpath("../..")).findElement(By.cssSelector(".product-name-image-container a"));
				BrowserActions.clickOnElementX(prdTile, driver, "Product tile with all swatch configured");
				Utils.waitForPageLoad(driver);
				return new PdpPage(driver).get();
			}
		}
		Log.event("Product with first swatch configured not found.");
		return clickOnProductBasedOnIndex(0);
	}
	
	/**
	 * To navigate to PDP with configured color swatch
	 * @return Object with page object, product name, price
	 * @throws Exception
	 */
	public Object[] navigateToPDPforFirstSwatchWithImage()throws Exception{
		WebElement prdNameLink = firstSwatchWithImage.findElement(By.xpath("ancestor::li[contains(@class,'grid-tile')]//a[@class='name-link']//div[contains(@class,'product-name')]"));
		WebElement prdPrice = firstSwatchWithImage.findElement(By.xpath("ancestor::li[contains(@class,'grid-tile')]//div[contains(@class,'product-pricing')]"));
		String price = BrowserActions.getText(driver, prdPrice, "Product Price").trim();
		BrowserActions.scrollInToView(prdNameLink, driver);
		String prdName = prdNameLink.getAttribute("innerHTML").replace("amp;", "").trim();
		BrowserActions.clickOnElementX(prdNameLink, driver, prdNameLink.getAttribute("innerHTML"));
		Utils.waitForPageLoad(driver);
		PdpPage pdpPage = new PdpPage(driver).get();
		Object[] obj = new Object[3];
		obj[0] = pdpPage;
		obj[1] = prdName;
		obj[2] = price.replaceAll("(\n(\\s+)?)", " ");
		Utils.waitForPageLoad(driver);
		return obj;
	}
	
	public String getProductNameforFirstProductWithSwatchImage()throws Exception{
		WebElement elem = firstSwatchWithImage.findElement(By.xpath("ancestor::li[contains(@class,'grid-tile')]//a[@class='name-link']//div[contains(@class,'product-name')]"));
		BrowserActions.scrollInToView(elem, driver);
		return BrowserActions.getText(driver, elem, "Product Swatch With Image");
	}
	
	/**
	 * To get all product details as list of hashmap
	 * @return List -
	 * @throws Exception -
	 */
	public List<HashMap<String, String>> getAllProductDetails()throws Exception{
		List<HashMap<String, String>> prdDetails = new ArrayList<HashMap<String, String>>();

		String productName = new String();
		String price = new String();

		int x = lstProducts.size() > 10 ? 10 : lstProducts.size();
		for(int i = 0; i < x; i++){
			HashMap<String, String> product = new HashMap<String, String>();
			try{
				productName = lstProducts.get(i).findElement(By.cssSelector("a.name-link")).getText();
			}			
			catch(NoSuchElementException | StaleElementReferenceException e) {productName=null;}
			try {
				price = lstProducts.get(i).findElement(By.cssSelector("span.price-product-sales-price")).getText();
			}
			catch(NoSuchElementException | StaleElementReferenceException e) {price=null;}
			product.put("Name", productName);
			product.put("Price", price);
			prdDetails.add(product);
		}
		return prdDetails;
	}
	
	/**
	 * To click view more button in the SLP page
	 * @throws Exception - Exception
	 */
	public void clickOnViewMore()throws Exception{
		if (Utils.waitForElement(driver, btnViewMore.get(btnViewMore.size()-1))) {
			int size = getProductTileCount()+1;
			BrowserActions.scrollToViewElement(btnViewMore.get(btnViewMore.size()-1), driver);
			BrowserActions.clickOnElementX(btnViewMore.get(btnViewMore.size()-1), driver, "View more button");
			Utils.waitUntilElementDisappear(driver, waitLoader);
			Utils.waitForPageLoad(driver);
			List<WebElement> elem = driver.findElements(By.cssSelector(".product-tile"));
			Utils.waitForElement(driver, elem.get(size));
		} else {
			Log.fail("The view more button is not found in the page!");
		}
	}
		
}
