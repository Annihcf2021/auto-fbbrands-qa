package com.fbb.pages.ordering;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.reusablecomponents.Enumerations.Toggle;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;

public class QuickOrderPage  extends LoadableComponent <QuickOrderPage>{

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	private static final String SIZE_SELECTED = ".swatches.size .selected a";
	private static final String MINI_CART_OVERLAY = ".minicartpopup ";

	/*************** WebElements of Quick Order Page ***************/
	
	@FindBy(css = ".loader-bg")
	WebElement PDPspinner;
	
	@FindBy(css = "#wrapper")
	WebElement containerElement;
	
	@FindBy(css = "footer .footer-container.footer-top")
	WebElement footerTopSection;

	@FindBy(css = ".catalog-quick-order")
	WebElement divCatalogQuickOrder;

	//@FindBy(css = "input[id*='dwfrm_quickorder_search_itemnumber']")
	@FindBy(css = ".itemnumber input")
	WebElement txtItemSearch;
	
	@FindBy(css = ".hide-desktop.hide-tablet.back-arrow")
	WebElement breadcrumbArrowMobile;
	
	@FindBy(css = ".blueboxnumberfield input")
	WebElement txtBlueBoxNumberField;
	
	@FindBy(css = ".itemnumber input")
	List<WebElement> txtItemSearchList;
	
	@FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop")
    WebElement clickProductDetails;
	
	@FindBy(css = ".itemnumber input[value='']")
	List<WebElement> txtItemSearchListEmpty;
	
	@FindBy(css = ".itemnumber input:not([value=''])")
	List<WebElement> txtItemSearchListPopulated;
	
	@FindBy(css = ".hide-desktop.hide-tablet .catalog-item-number")
	List<WebElement> txtCQONumberOfItems_Mobile;
	
	@FindBy(css = ".hide-mobile .catalog-item-number")
	List<WebElement> txtCQONumberOfItems_Des_Tab;
	
	@FindBy(css = ".pdp-main form.pdpForm:not(.bottom-add-all)")
	List<WebElement> productsInQuickOrder;
	
	@FindBy(css = ".hide-mobile .catalog-item-number")
	List<WebElement> prodCQOId_Des_Tab;
	
	@FindBy(css = ".hide-desktop.hide-tablet .catalog-item-number")
	List<WebElement> prodCQOId_Mobile;
	
	@FindBy(css = ".itemnumber1 input")
	WebElement txtItemSearch1;
	
	@FindBy(css = ".itemnumber2 input")
	WebElement txtItemSearch2;
	
	@FindBy(css = ".itemnumber3 input")
	WebElement txtItemSearch3;
	
	@FindBy(css = ".itemnumber4 input")
	WebElement txtItemSearch4;
	
	@FindBy(xpath = "//button[contains(text(),'Update')]")
	WebElement updateBtn;
	
	@FindBy(css = ".catalog-quick-order__main.make-label-absolute")
	WebElement contentBox;
	
	@FindBy(css = ".pdpForm fieldset")
	WebElement frmOrderForm;

	@FindBy(css = ".cqo-btn-search")
	WebElement btnSearch;
	
	@FindBy(css = ".generic-error-required.error")
	WebElement genericErrorRequired;
	
	@FindBy(css = ".itemnumber1.error-handle")
	WebElement item1InvalidErrorMsg;
	
	@FindBy(css = ".itemnumber1 .error")
	WebElement item1ErrorMsg;
	
	@FindBy(css = ".itemnumber2 .error")
	WebElement item2ErrorMsg;
	
	@FindBy(css = ".itemnumber3 .error")
	WebElement item3ErrorMsg;
	
	@FindBy(css = ".itemnumber4 .error")
	WebElement item4ErrorMsg;
	
	@FindBy(css = ".generic-error.error")
	WebElement genericError;
	
	@FindBy(css = ".product-price")
	List<WebElement> productPriceList;

	@FindBy(css = "#pdpMain")
	WebElement divPdpMain;
	
	@FindBy(css = ".hemmingmonograming > a")
	List<WebElement> lstCQONumbers;
	
	@FindBy(css = "#pdpMain .pdpForm")
	List<WebElement> cqoPgProducts;
	
	@FindBy(css = ".itemnumber1 .invalid")
	WebElement invalidErrorMsg;
	
	@FindBy(css = ".hide-mobile .catalog-item-number")
	WebElement orderCatlogNumberDesktop;

	@FindBy(css = ".hide-desktop.hide-tablet .catalog-item-number")
	WebElement orderCatlogNumberMobile;
	
	@FindBy(css = ".itemnumber .error")
	WebElement txtInvalidErrorMessage;

	@FindBy(css = ".error-handle .label-text")
	WebElement txtrequiredErrorMessage;

	@FindBy(css = ".svg.svg-converted")
	WebElement divHeadBanner;

	@FindBy(css = ".footer-brand-selector .content-asset")
	WebElement divFooterBrandBanner;

	@FindBy(css = ".catalog-quick-order__leftNav")
	WebElement divCatalogQuickOrderleft;

	@FindBy(css = ".product-name.hide-mobile .name-text")
	WebElement txtProductName;

	@FindBy(css = ".product-name .name-text")
	WebElement txtProductNameMobileNew;

	@FindBy(css = ".product-col-2.product-quickorder-detail .product-price")
	WebElement txtPrice;

	@FindBy(css = ".product-variation-content")
	WebElement divApplicableVariation;

	@FindBy(css = ".size_icon")
	WebElement divSizeChart;

	@FindBy(css = ".hide-mobile.product-name .name-text")
	List<WebElement> lblPrdNameInDesktop;

	@FindBy(css = ".hide-desktop.hide-tablet .product-name .name-text")
	List<WebElement> lblPrdNameInMobile;

	@FindBy(css = SIZE_SELECTED)
	WebElement selectedSize;

	@FindBy(xpath = "//select[@id='Quantity']//following-sibling::div[@class='selected-option selected']")
	WebElement selectedQty;

	@FindBy(css = ".swatches.size li.selectable a")
	List<WebElement> lstSizeSwatchesSelectable;
	
	@FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop")
	List<WebElement> shopNowWindow_ProductSet;
	
	@FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop.open")
	List<WebElement> shopNowWindow_ProductSet_Opened;
	
	@FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop.open")
	WebElement shopNowWindow_ProductSet_Open;

	@FindBy(css = ".swatches.size li")
	List<WebElement> lstSizeSwatches;

	@FindBy(css = ".swatches.size li.unselectable a")
	List<WebElement> lstSizeSwatchesUnSelectable;
	
	@FindBy(css = "#current_size .selected-value")
	WebElement lableSizerSelected;

	@FindBy(css = ".swatches.color li.selected a")
	WebElement selectedColor;
	
	@FindBy(css = ".swatches.color > .selected img")
	WebElement imgSelectedColor;

	@FindBy(css = ".swatches.color li.selectable:not([class*='selected']) a")
	List<WebElement> lstColorSelectable;
	
	@FindBy(css = "#current_color .selected-value")
	WebElement lableColorSelected;

	@FindBy(css = "#add-to-cart")
	WebElement btnAddToCart;
	
	@FindBy(css = "#add-to-cart")
	List<WebElement> btnLstAddToCart;
	
	@FindBy(css = ".swatches.sizefamily .selected a")
	WebElement selectedSizeFamily;
	
	@FindBy(css = ".swatches.sizefamily .selectable:not(.selected) a")
	List<WebElement> sizeFamilyVariation;
	
	@FindBy(css = ".attribute.sizeFamily")
	WebElement divSizeFamily;
	
	@FindBy(css = ".add-all-to-cart-quick-order")
	WebElement btnAddAllToCartQuickOrder;

	@FindBy(css = ".primary-image-quick-order")
	WebElement imgPrimaryImage;
	
	@FindBy(css = ".product-primary-image .slick-current.slick-active img")
	WebElement imgPrimaryImage_Mobile;
	
	@FindBy(css = ".product-primary-image .slick-current.slick-active")
	WebElement divPrimaryImage_Mobile;
	
	@FindBy(css = ".product-name.hide-mobile span")
	WebElement productName;
	
	@FindBy(css = ".hide-desktop.hide-tablet .product-name span")
	WebElement productName_Mobile;
	
	@FindBy(css = ".product-add-to-cart .inventory")
	WebElement qtyDropDown;
	
	@FindBy(css = ".attribute.color")
	WebElement colorAttribute;
	
	@FindBy(css = ".attribute.size")
	WebElement sizeAttribute;
	
	@FindBy(css = ".swatches.size a")
	WebElement sizeAnchor;
	
	@FindBy(css = ".size-chart-link a")
	WebElement sizeChart;
	
	@FindBy(css = ".size-chart-link a .size_label")
	WebElement sizeChartLabel;
	
	@FindBy(css = ".size-guide-container")
	WebElement sizeChartDescription;
	
	@FindBy(css = ".ui-dialog-titlebar-close")
	WebElement sizeChartClose;
	
	@FindBy(css = ".primary-image")
	WebElement imgPrimaryImageNew;

	@FindBy(css = ".product-col-2.product-quickorder-detail")
	WebElement productInformationSection;

	@FindBy(css = ".price-sales.price-standard-exist")
	WebElement txtCatalogPrice;

	@FindBy(css = "select[id='Quantity']")
	WebElement drpQty;

	@FindBy(css = "div[class='quantity']")
	WebElement drpQuantity;

	@FindBy(css = ".btn-remove-item.hide-mobile")
	WebElement btnClose;
	
	@FindBy(css = ".hide-desktop.hide-tablet .btn-remove-item")
	WebElement btnClose_Mobile;
	
	@FindBy(css = ".btn-remove-item.hide-mobile")
	List<WebElement> lstBtnClose;
	
	@FindBy(css = ".hide-desktop.hide-tablet .btn-remove-item")
	List<WebElement> lstBtnClose_Mobile;

	@FindBy(css = ".product-monogramming .title")
	WebElement txtMonogrammingTitle;

	@FindBy(css = ".monogramming-enabled.form") 
	WebElement chkEnableMonogramming;

	@FindBy(css = ".monogramming-enabled.form>div>div>label>span")
	WebElement txtMonogramMessaging;

	@FindBy(css = ".color_gray")
	WebElement txtPrdAvailabilityMessaging;

	@FindBy(css = ".product-add-to-cart div[class='selected-option selected']")
	WebElement selectedQty1;
	
	@FindBy(css = ".hide-mobile.btn-remove-item")
	List<WebElement> removeItems_Des_Tab;
	
	@FindBy(css = ".hide-desktop.hide-tablet .btn-remove-item")
	List<WebElement> removeItems_Mobile;

	@FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop")
	WebElement btnShopNow;

	@FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop.open")
	WebElement btnShopNowClose;

	@FindBy(css = ".product-variations")
	WebElement divProductVariations;

	@FindBy(css = ".variation-add-to-cart-content")
	WebElement divProductVariationsDisplay;

	@FindBy(css = ".breadcrumb-element")
	WebElement lnkBreadcrumbHome;

	@FindBy(css = ".breadcrumb-element.current-element")
	WebElement lnkBreadcrumbCurrentPage;

	@FindBy(css = ".mainBanner")
	WebElement divGridHeader;

	@FindBy(css = ".breadcrumb")
	WebElement divBreadCrumb;

	@FindBy(css = "#product-content > div.product-hemmable")
	WebElement chkProductOptions;

	@FindBy(css = ".mainBanner .content-asset div.display-inline-block")
	WebElement divHeadAsset;
	
	@FindBy(css = ".mainBanner .content-asset img.hide-mobile.hide-tablet")
	WebElement divHeadAsset_WW_Desktop;
	
	@FindBy(css = ".mainBanner .content-asset img.hide-mobile.hide-desktop")
	WebElement divHeadAsset_WW_Tablet;
	
	@FindBy(css = ".mainBanner .content-asset img.hide-desktop.hide-tablet")
	WebElement divHeadAsset_WW_Mobile;

	@FindBy(css = ".catalog-quick-order__leftNav")
	WebElement divContentSlot;

	@FindBy(css = ".quantity .selected-option.selected")
	WebElement txtSelectedQuantity;

	@FindBy(css = ".quantity .selection-list li")
	List<WebElement> lstQty;

	@FindBy(css = ".swatches.size")
	List<WebElement> lstSizeGrid;

	@FindBy(css = ".availability-msg")
	List<WebElement> lstavailability;

	@FindBy(css = ".product-name.hide-mobile")
	List<WebElement> txtProductNameDesktopTablet;

	@FindBy(css = ".product-name.hide-mobile")
	List<WebElement> txtProductNameMobile;
	
	@FindBy(css = ".product-quickorder-detail")
	List<WebElement> prodQuickOrderList;

	@FindBy(css = "#pdpMain .product-price")
	WebElement divWebPricing;

	@FindBy(css = ".promotion .callout-message")
	WebElement divPromotionCallout;

	@FindBy(css = ".swatches.color")
	List<WebElement> lstColorGrid;
	
	@FindBy(css = ".swatches.color li")
	List<WebElement> lsrColorSwatches;
	
	@FindBy(css = ".btn-remove-item.button-text")
	List<WebElement> divCloseProduct;

	@FindBy(css = MINI_CART_OVERLAY + ".ui-dialog-titlebar-close")
	WebElement btnCloseMiniCartOverLay;

	@FindBy(css = ".minicartpopup")
	WebElement mdlMiniCartOverLay;

	@FindBy(css = ".quantity .selection-list li:not([class='selected'])")
	List<WebElement> lstQtySelectable;

	@FindBy(css = ".quantity-ats-message")
	WebElement errMsgQuantityATS;

	@FindBy(css = ".product-name .name-text")
	List<WebElement> productNameDesktop;

	@FindBy(css = ".menu-utility-user.hide-desktop.hide-tablet .mini-cart-link")
	WebElement iconMyBag;

	@FindBy(css = "#add-all-to-cart")
	WebElement btnAddAllToBag;

	@FindBy(css = "#add-to-cart")
	WebElement btnAddToBag;

	@FindBy(css = ".product-primary-image .catalog-item-number")
	WebElement quickOrderCatalogNumber;
	
	@FindBy(css = ".hide-desktop.hide-tablet .catalog-item-number")
	WebElement quickOrderCatalogNumberMobile;
	
	@FindBy(css = ".hide-mobile .catalog-item-number")
	WebElement quickOrderCatalogNumberDesktopTab;

	@FindBy(css = "#pdpMain")
	WebElement quickOrderMainContent;
	
	@FindBy(css = ".quick-order-banner")
	WebElement quickOrderHeading;

	@FindBy(css = ".mini-cart-product.slick-slide")
	List<WebElement> overlayProductCount;

	@FindBy(css = ".cart-mini-overlay")
	WebElement addToCartOverlay;
	
	@FindBy(css = ".slick-active .mini-cart-subtotals")
	WebElement overlayProductAdded;
	
	@FindBy(css = ".only-for-mobile .mini-cart-subtotals")
	WebElement overlayProductAdded_Mobile;

	@FindBy(css = ".expired-price")
	WebElement catalogPriceExpired;
	
	@FindBy(css = ".product-price .price-sales.price-standard-exist")
	WebElement catlogPriceDiscount;

	@FindBy(css = ".catalog-quick-order .mainBanner > div")
	List<WebElement> divHeadAssetWithBreadCrumb;

	@FindBy(css = "form .swatches.size")
	List<WebElement> divProductCountBasedOnSize;

	@FindBy(css = ".price-sales")
	WebElement productPrice;

	@FindBy(css = ".color_gray")
	WebElement availabilityMessaging;

	@FindBy(css = ".product-variations-toggle.hide-tablet.hide-desktop")
	WebElement shopNowMobile;
	
	@FindBy(xpath = "//span[contains(text(),'Hide Details')]")
	WebElement shopNowMobileOpened;
	
	@FindBy(xpath = "//span[contains(text(),'Product Details')]")
	WebElement shopNowMobileClosed;
	
	@FindBy(id = "thumbnails")
	WebElement divAlternateImages;
	
	@FindBy(css = "#thumbnails .thumb a img")
	List<WebElement> lstAlternateImages;
	
	@FindBy(css = ".prod-details p")
	WebElement lblProductDetails;
	
	@FindBy(css = ".prod-details.make-height-auto")
	WebElement hiddenDataInProductDetail;
	
	@FindBy(css = "span[class='read-more']")
	WebElement btnReadMore;
	
	@FindBy(css = ".read-more.opened")
	WebElement btnCloseProductDetails;
	
	@FindBy(id = "product-teasers")
	WebElement lblReview;
	
	@FindBy(css = ".slick-dots")
	WebElement icoSlickDots;
	
	@FindBy(css = "#thumbnails .thumb.selected img")
	WebElement selectedAltImage;
	
	@FindBy(css = "#thumbnails .thumb.selected a")
	WebElement lnkSelectedAltImage;
	
	@FindBy(css = ".slick-next")
	WebElement icoNextArrowInSlickTrack;
	
	@FindBy(css = "form[name='dwfrm_quickorder_items_i0']")
	WebElement firstProduct;
	
	@FindBy(css = "form[name='dwfrm_quickorder_items_i1']")
	WebElement secondProduct;
	
	@FindBy(css = "a.breadcrumb-element")
	WebElement bcCurrentMobile;
	
	@FindBy(css = ".quick-order-breadcrumb")
	WebElement qcBreadCrumb;
	
	@FindBy(css = "#header .top-banner")
	WebElement headerTopBanner;
	
	@FindBy(css = "#main")
	WebElement divMain;
	
	@FindBy(css = ".mainBanner .content-asset .heading-text")
	WebElement divHeadContentAsset;
	
	@FindBy(css = ".mainBanner .content-asset img.hide-mobile.hide-desktop")
	WebElement divHeadContentAssetImg_Tablet;
	
	@FindBy(css = ".mainBanner .content-asset img.hide-mobile.hide-tablet")
	WebElement divHeadContentAssetImg_Desktop;
	
	@FindBy(css = ".mainBanner .content-asset img.hide-desktop.hide-tablet")
	WebElement divHeadContentAssetImg_Mobile;
	
	@FindBy(css = ".mainBanner .content-asset img")
	WebElement imgHeadContentAsset_KS;
	
	@FindBy(css = ".mainBanner .content-asset img")
	List<WebElement> lstImgHeadContentAsset;
	
	@FindBy(css = ".select-size-text")
	WebElement btnAddAllToBagSizeError;

	@FindBy(css = ".pdp-main .pdpForm")
	List<WebElement> lstProducts;
	
	@FindBy(css = ".minicartpopup .mini-cart-product")
	List<WebElement> lstProductsInATBMdl;
	
	@FindBy(css = ".product-quickorder-detail .hide-desktop.hide-tablet .catalog-item-number")
	List<WebElement> lstProductItemNumber_Mobile;
	
	@FindBy(css = ".ui-button-icon.ui-icon.ui-icon-closethick")
	WebElement btnCloseAddToBagOverlay;
	
	@FindBy(css = ".zoomLens")
	WebElement divZoomLens;
	
	@FindBy(css = "div.zoomWindow")
	WebElement divZoomWindowContainer;
	
	@FindBy(css = "#thumbnails .slick-next.slick-arrow.slick-disabled")
	WebElement btnNextImageDisable;
	
	@FindBy(css = "#thumbnails .slick-prev.slick-arrow.slick-disabled")
	WebElement btnPrevImageDisable;

	@FindBy(css = "#thumbnails .slick-prev.slick-arrow")
	WebElement btnPrevImageEnable;

	@FindBy(css = "#thumbnails .slick-next.slick-arrow")
	WebElement btnNextImageEnable;
	
	@FindBy(css = "#thumbnails .slick-slide")
	List<WebElement> lstProductAltThumbnail;
	
	@FindBy(css = ".thumbnail-link")
	List<WebElement> lstProductThumbnaillink;
	
	/*************** WebElements of PDP Quick Order - Ends ***************/

	/**
	 * constructor of the class
	 * @param driver - Webdriver
	 */
	public QuickOrderPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, divCatalogQuickOrder))) {
			Log.fail("Quick Order Page did not open up. Site might be down.", driver);
		}
		elementLayer = new ElementLayer(driver);		
	}// isLoaded

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * TO Search for a product in Quick order page
	 * @param productID - Product ID
	 * @param enableError - True - if enabled error
	 * @throws Exception - Exception
	 */
	public void searchItemInQuickOrder(String productID, boolean enableError) throws Exception {
		searchCQOProduct(productID);
		if(enableError && Utils.waitForElement(driver, txtInvalidErrorMessage)){
			Log.fail("Given ID Not Available.", driver);
		}
	}

	/**
	 * To enter search term in CQO search box
	 * @param cqoNumber
	 * @param box(optional) - search box number to type in
	 * @throws Exception
	 */
	public void enterCQOSearchID(String cqoNumber, int... box) throws Exception {
		int boxNumber = box.length > 0 ? box[0] : 1;
		BrowserActions.typeOnTextField(txtItemSearchList.get(boxNumber - 1), cqoNumber, driver, "CQO search box #" + boxNumber);
	}
	
	/**
	 * To search catalog item by CQO number
	 * @param cqoNumber
	 * @param box(optional) - search box number to type in
	 * @throws Exception
	 */
	public void searchCQOProduct(String cqoNumber, int... box) throws Exception {
		int boxNumber = box.length > 0 ? box[0] : 1;
		enterCQOSearchID(cqoNumber, boxNumber);
		clickOnSearchButton();
	}
	
	/**
	 * Search multiple product Id in quick order
	 * @param productID - Array of product Ids need to search
	 * @throws Exception
	 */
	public void searchMultipleProductInQuickOrder(String[] productID) throws Exception {
		int searchLimit = productID.length > 4 ? productID.length : 4;
		for (int i = 1; i <= searchLimit; i++) {
			enterCQOSearchID(productID[i - 1], i);
		}
		
		BrowserActions.clickOnElementX(btnSearch, driver, "Search Button");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To verify catalog item is present
	 * @param cqoNumber
	 * @throws Exception
	 */
	public boolean verifyCatalogItemDisplayed(String cqoNumber) throws Exception {
		List<String> lstCQO = getCatalogNumberList();
		return (lstCQO.contains(cqoNumber) || lstCQO.contains(cqoNumber.replaceAll("\\-", "")));
	}
	
	/**
	 * To verify all the given items are searched in the quickorder
	 * @param cqoNumbers - Array of product Ids need to search
	 * @throws Exception
	 */
	public boolean verifySearchedCatalogItemsDisplayed(String[] cqoNumbers) throws Exception {
		List<String> lstVerifyCQO = Arrays.asList(cqoNumbers);
		List<String> lstMissingCQO = new ArrayList<String>();
		for (String verifyCQO : lstVerifyCQO) {
			if (!verifyCatalogItemDisplayed(verifyCQO)) {
				lstMissingCQO.add(verifyCQO);
			}
		}

		if (lstMissingCQO.size() > 0) {
			Log.event("Missing searched catalog IDs: " + lstMissingCQO.toString());
			return false;
		} else
			return true;
	}
	
	/**
	 * To search an Item in Quick order page with given product id
	 * @param productID - Product ID
	 * @throws Exception - Exception - Exception
	 */
	public void searchItemInQuickOrder(String productID) throws Exception {
		searchCQOProduct(productID);
		WebElement cqoTile = getCatalogItemByCQONumber(productID);
		if(cqoTile == null) {
			Log.event("CQO item not found for:: " + productID);
		} else {
			WebElement variationToggle = cqoTile.findElement(By.cssSelector(".product-variations-toggle"));
			String variationText = BrowserActions.getText(driver, variationToggle, "Variation Toggle");
			if(variationText.contains("Hide")) {
				Log.ticket("SC-1186");
			}
		}
	}
	
	/**
	 * To get list of products added in quick order page
	 * @return List of Product Names
	 * @throws Exception - Exception - Exception
	 */
	public List<String> getProductNames() throws Exception {
		List<String> productName = new ArrayList<String>();
		List<WebElement> prdName = null;
		if(Utils.isMobile())
			prdName = lblPrdNameInMobile;
		else
			prdName = lblPrdNameInDesktop;
		for(WebElement ele : prdName){
			productName.add(ele.getAttribute("innerHTML").trim().toLowerCase());
		}
		
		return productName;
	}

	/**
	 * To get recently added product into page
	 * @return Recently added product name
	 * @throws Exception - Exception - Exception
	 */
	public String getLastAddedProduct() throws Exception{
		if(Utils.isMobile())
			return lblPrdNameInMobile.get(0).getAttribute("innerHTML");
		else
			return lblPrdNameInDesktop.get(0).getAttribute("innerHTML");
	}

	/**
	 * To wait for spinner in the PDP
	 */
	public void waitForSpinner() {
		Utils.waitUntilElementDisappear(driver, PDPspinner);
	}

	/**
	 * To click on 'Update' button
	 * @throws Exception - Exception
	 */
	public void clickUpdateProduct()throws Exception{
		BrowserActions.scrollInToView(updateBtn, driver);
		BrowserActions.clickOnElementX(updateBtn, driver, "Add To Cart Button ");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on 'Home in breadcrumb' button
	 * @return HomePage - Home Page object
	 * @throws Exception - Exception
	 */
	public HomePage clickOnBreadCrumbHome()throws Exception{
		if(Utils.isMobile()) {
			BrowserActions.clickOnElementX(breadcrumbArrowMobile, driver, "BreadCrumb Home element");
		} else {
			BrowserActions.clickOnElementX(lnkBreadcrumbHome, driver, "BreadCrumb Home element");
		}
		Utils.waitForPageLoad(driver);
		return new HomePage(driver).get();
	}

	/**
	 * to select a value in Quantity drpdwn
	 * @param qty - Quantity to be set
	 * @return Quantity that has been set
	 * @throws Exception
	 */
	public String selectQuantity(int qty)throws Exception{
		BrowserActions.clickOnElementX(txtSelectedQuantity, driver, "Quantity dropdown");
		int qtyIndex;
		if (lstQty.size() >= qty) {
			qtyIndex = qty - 1;
		} else {
			qtyIndex = lstQty.size() - 1;
		}
		BrowserActions.clickOnElementX(lstQty.get(qtyIndex), driver, qtyIndex+1 + " on quanitity.");
		return lstQty.get(qtyIndex).getText().trim();
	}

	/**
	 * To click on search button
	 * @throws Exception
	 */
	public void clickOnSearchButton()throws Exception{
		BrowserActions.clickOnElementX(btnSearch, driver, "Search Button");
		Utils.waitForPageLoad(driver, 10); // CQO page may take longer than usual to finish loading after adding a product
	}
	
	/**
	 * To get on no. of products in quick order page
	 * @return int - no.of products
	 * @throws Exception - Exception
	 */
	public int getNoOfProductInQuickOrderPage()throws Exception {
		Log.event("CQO Search result count:: " + lstProducts.size());
		return lstProducts.size();
	}
	
	/**
	 * To verify CQO search result displayed for valid CQO number when both valid and invalid numbers are searched 
	 * @param cqValidPrds - Array of valid CQO numbers
	 * @param cqInvalidPrds - Array of invalid CQO numbers
	 * @return true/false if search result is displayed for all valid CQO numbers
	 * @throws Exception
	 */
	public boolean verifyValidCatalogSearchResults(String[] cqValidPrds, String[] cqInvalidPrds) throws Exception {
		removeAllProducts();
		searchMultipleProductInQuickOrder(ArrayUtils.addAll(cqValidPrds, cqInvalidPrds));
		Log.event("Searched with mix of valid and invalid CQO numbers");

		return verifySearchedCatalogItemsDisplayed(cqValidPrds);
	}
	
	/**
	 * Get no of product price
	 * @return int - no of product price
	 * @throws Exception
	 */
	public int getNoOfProductPrice()throws Exception {
		return productPriceList.size();
	}
	
	/**
	 * To verify catalog items do no have variations selected by default
	 * @return true/false is variation is selected
	 * @throws Exception
	 */
	public boolean verifyNoVariationSelectedByDefault() throws Exception {
		for (WebElement cqoItem : cqoPgProducts) {
			List<WebElement> cqoVariations = cqoItem.findElements(By.cssSelector(".product-variations .selected-value"));
			for (WebElement variation : cqoVariations) {
				String variationValue = BrowserActions.getText(driver, variation, "Variation value");
				if (!variationValue.equals(""))
					return false;
			}
		}
		return true;
	}

	/**
	 * To remove all the products in page
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public boolean removeAllProducts() throws Exception{
		BrowserActions.scrollToTopOfPage(driver);
		List<WebElement> elem = null;
		if (Utils.isMobile()) {
			elem = lstBtnClose_Mobile;
		} else {
			elem = lstBtnClose;
		}
		boolean allCQORemoved = false;
		int productRemovedCount = 0;
		int productsInCQOPage = elem.size();
		try{
			while(Utils.waitForElement(driver, quickOrderMainContent) && productRemovedCount < productsInCQOPage) {
				BrowserActions.clickOnElementX(elem.get(0), driver, "Click remove button.");
				Utils.waitForPageLoad(driver);
				productRemovedCount++;
			}
			allCQORemoved = true;
		}catch(ArrayIndexOutOfBoundsException e){
			Log.warning("Failed to remove all CQO item.");
		}

		return allCQORemoved;
	}
	
	/**
	 * To get catalog item name
	 * @param index
	 * @throws Exception
	 */
	public String getCatalogItemName(int index) throws Exception {
		return getCatalogItemName(getCatalogItemByIndex(index));
	}

	/**
	 * To get catalog item name
	 * @param cqoNumber
	 * @throws Exception
	 */
	public String getCatalogItemName(String cqoNumber) throws Exception {
		return getCatalogItemName(getCatalogItemByCQONumber(cqoNumber));
	}

	private String getCatalogItemName(WebElement cqoItem) throws Exception {
		String nameLocator = Utils.isMobile() ? ".product-name:not(.hide-mobile) .name-text" : ".product-name.hide-mobile .name-text";
		WebElement cqoNameElement = cqoItem.findElement(By.cssSelector(nameLocator));
		return BrowserActions.getText(driver, cqoNameElement, "catalog item name.");
	}
	
	/**
	 * To get product count
	 * @return int - product count
	 * @throws Exception - Exception
	 */
	public int getProductCount()throws Exception {
		return cqoPgProducts.size();
	}
	
	/**
	 * To verify products in Overlay
	 * @param count - 
	 * @return boolean value
	 * @throws Exception - Exception
	 */
	public boolean verifyProductCountInOverlay(int count) throws Exception {
		Utils.waitForElement(driver, addToCartOverlay);
		String countOverlayProduct = BrowserActions.getText(driver, overlayProductAdded, "product count in overlay");
		Log.event("Expected count in overlay:: " + count);
		Log.event("Actual count in overlay:: " + countOverlayProduct);
		return countOverlayProduct.toLowerCase().contains(count + " items");
	}

	/**
	 * To close add to bag overlay
	 * @throws Exception - Exception
	 */
	public void closeAddToBagOverlay() throws Exception {
		try{
			BrowserActions.clickOnElementX(btnCloseMiniCartOverLay, driver, "AddToBag overlay close button");
		}catch(NoSuchElementException e){
			if(Utils.waitForElement(driver, mdlMiniCartOverLay)){
				Log.failsoft("Close Button in minicart overlay not found", driver);
				mdlMiniCartOverLay.sendKeys(Keys.ESCAPE);
				Log.event("Sent Escape Keypress");
			}else{
				if(Utils.waitForElement(driver, errMsgQuantityATS))
					Log.fail("Inventory Problem occured :: " + errMsgQuantityATS.getText(), driver);
				else
					Log.fail("Minicart Overlay not opened", driver);
			}
		}
	}
	
	private String selectQty(WebElement cqoItem, int qty) throws Exception {
		toggleProductDetailsMobile(cqoItem, Toggle.Open);
		WebElement qtyDropdown = cqoItem.findElement(By.cssSelector("select#Quantity"));
		Select qtySelect = new Select(qtyDropdown);
		qtySelect.selectByIndex(qty - 1);
		WebElement selectedQty = cqoItem.findElement(By.cssSelector(".inventory .selected-option.selected"));
		return BrowserActions.getText(driver, selectedQty, "catalog item quantity");
	}

	/**
	 * To set quantity of a catalog item
	 * @param cqoNumber
	 * @param qty
	 * @return selected value
	 * @throws Exception
	 */
	public String selectQty(String cqoNumber, int qty) throws Exception {
		return selectQty(getCatalogItemByCQONumber(cqoNumber), qty);
	}

	/**
	 * To set quantity of a catalog item
	 * @param index
	 * @param qty
	 * @return selected value
	 * @throws Exception
	 */
	public String selectQty(int index, int qty) throws Exception {
		return selectQty(getCatalogItemByIndex(index), qty);
	}
	
	/**
	 * TO wait for error message to display
	 * @throws Exception - Exception
	 */
	public void waitForError() throws Exception{
		Utils.waitForElement(driver, txtInvalidErrorMessage, 120);
	}

	/**
	 * To get product price of given product id
	 * @param cqoNumber
	 * @return Product Price
	 * @throws Exception
	 */
	public String getProductPrice(String cqoNumber)throws Exception{
		WebElement cqoElement = getCatalogItemByCQONumber(cqoNumber);
		WebElement cqoPrice = cqoElement.findElement(By.cssSelector(".product-price>span"));		
		return BrowserActions.getText(driver, cqoPrice, "price for CQO item: " + cqoNumber);
	}
	
	private void clickOnSizeChart(WebElement cqoItem) throws Exception {
		WebElement btnSizeChart = cqoItem.findElement(By.cssSelector(".size-chart-link a"));
		BrowserActions.clickOnElementX(btnSizeChart, driver, "size chart link.");
	}
	
	/**
	 * To click on size chart on catalog item
	 * @param index
	 * @throws Exception
	 */
	public void clickOnSizeChart(int index) throws Exception {
		clickOnSizeChart(getCatalogItemByIndex(index));
	}
	
	/**
	 * To click on size chart on catalog item
	 * @param cqoNumber
	 * @throws Exception
	 */
	public void clickOnSizeChart(String cqoNumber) throws Exception {
		clickOnSizeChart(getCatalogItemByCQONumber(cqoNumber));
	}
	
	/**
	 * To close size chart
	 * @throws Exception
	 */
	public void closeSizeChart() throws Exception {
		BrowserActions.javascriptClick(sizeChartClose, driver, "Size Chart");
		Utils.waitUntilElementDisappear(driver, sizeChartClose);
	}
	
	/**
	 * To get product Id by index
	 * @param index - index of the product
	 * @return Catalog number
	 * @throws Exception
	 */
	public String getCatalogNumberByIndex(int index) throws Exception {
		List<String> lstCQOItemID = getCatalogNumberList();
		return lstCQOItemID.get(index);
	}

	/**
	 * To select index th alternate image
	 * @param index - Starts from 1 to N
	 * @throws Exception - Exception
	 */
	public void selectAlternateImageByIndex(int index)throws Exception{
		int sizeAI = lstAlternateImages.size();
		
		if(index >= sizeAI) {
			Log.failsoft("Given index is greater than no.of alternative images displaying");
		} else {
			String isHidden = lstProductAltThumbnail.get(index - 1).getAttribute("aria-hidden");
			while (isHidden.equalsIgnoreCase("true")) {
				BrowserActions.clickOnElementX(icoNextArrowInSlickTrack, driver, "Next Arrow");
				isHidden = lstProductAltThumbnail.get(index - 1).getAttribute("aria-hidden");
			}
			BrowserActions.javascriptClick(lstAlternateImages.get(index-1), driver, index + " th Alternate Image");
			Utils.waitForPageLoad(driver);
		}
	}
	
	/**
	 * To get number of alternate image
	 * @param productIndex - product index
	 * @throws Exception - Exception
	 */
	public int getAlternateImageCountForProduct(int productIndex)throws Exception{
		WebElement prodElement = driver.findElements(By.cssSelector(".product-image-container")).get(productIndex-1);
		List<WebElement> alternativeImages = prodElement.findElements(By.cssSelector("#thumbnails .thumb a img"));
		return alternativeImages.size();
	}
	
	/**
	 * To get alternate image code
	 * @return imageCode - image code
	 * @throws Exception - Exception
	 */
	public String getselectedAltImgCode()throws Exception{
		try {
			String codes[] = selectedAltImage.getAttribute("src").trim().split("\\?")[0].split("\\/");
			String code = codes[codes.length-1].replaceAll("[^0-9]", "");
			Log.event("Selcted alt-image code:: " + code);
			return code;
		}catch(ArrayIndexOutOfBoundsException|NullPointerException e) {
			Log.reference("Invalid image configuration. No image assigned to thumbnail.");
			return "no_image";
		}
	}

	/**
	 * To get primary image code
	 * @return imageCode - image code
	 * @throws Exception - Exception
	 */
	public String getPrimaryImgCode()throws Exception{
		try {
			String codes[] = imgPrimaryImage.getAttribute("src").trim().split("\\?")[0].split("\\/");
			String code = codes[codes.length-1].replaceAll("[^0-9]", "");
			Log.event("Product image code:: " + code);
			return code;
		}catch(ArrayIndexOutOfBoundsException|NullPointerException e) {
			Log.reference("Invalid image configuration. No image assigned to Primary Image.");
			return "no_image";
		}
	}

	/**
	 * To get selected color code
	 * @return imageCode - image code
	 * @throws Exception - Exception
	 */
	public String getSelectedColorCode()throws Exception{
		try {
			String codes[] = BrowserActions.getTextFromAttribute(driver, imgSelectedColor, "src", "color selected").trim().split("\\?")[0].split("\\/");
			String code = codes[codes.length-1].replaceAll("[^0-9]", "");
			Log.event("Selected color swatch code:: " + code);
			return code;
		}catch(ArrayIndexOutOfBoundsException|NullPointerException e) {
			Log.reference("Invalid image configuration. No image assigned to Color Swatch.");
			return "no_image";
		}
	}
	
	/**
	 * To check the alternate image is displayed fully/not when the image is clicked in slick slider.
	 * @param imgToVerify - image to verify
	 * @param statusToVerify - status to verify
	 * @return boolean - true if status of image is correct, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyAlternateImageDisplayStatus(int imgToVerify, String statusToVerify) throws Exception {
		String displayStatus = lstProductAltThumbnail.get(imgToVerify - 1).getAttribute("aria-hidden");
		if (displayStatus.equals(statusToVerify)) {
			return true;
		}
		return false;
	}
	
	/**
	 * To click on slick slide left/right arrows
	 * @param direction - 'left' / 'right'
	 * @throws Exception - Exception
	 */
	public void scrollAlternateImageInSpecifiedDirection(String direction) throws Exception {
		if (direction.equals("Next")) {
			while (!(Utils.waitForElement(driver, btnNextImageDisable))) {
				BrowserActions.clickOnElementX(btnNextImageEnable, driver, "Next Arrow in Alternate product image ");
			}
		} else if (direction.equals("Prev")) {
			while (!(Utils.waitForElement(driver, btnPrevImageDisable))) {
				BrowserActions.clickOnElementX(btnPrevImageEnable, driver, "Prev Arrow in Alternate product image ");
			}
		}
	}
	
	/**
	 * To zoom the product image in the Quick Shop overlay
	 * @throws Exception - Exception
	 */
	public void zoomProductImage()throws Exception{
		BrowserActions.scrollInToView(imgPrimaryImage, driver);
		BrowserActions.mouseHover(driver, imgPrimaryImage, "");
		Actions action = new Actions(driver);
		action.moveToElement(imgPrimaryImage).build().perform();
		Utils.waitForElement(driver, divZoomWindowContainer);
	}
	
	/**
	 * To verify zoomlens and zoom window are displayed
	 * @return boolean - true id zoomlens and zoom window displayed, else false
	 * @throws Exception - Exception
	 */
	public boolean verifyZoomLensAndZoomWindowAreDisplayed()throws Exception{
		Point lens = divZoomLens.getLocation();
		Point window = divZoomWindowContainer.getLocation();

		if((lens.x != 0 || lens.y != 0) && (window.x != 0 || window.y != 0))
			return true;
		else
			return false;
	}
	
	/**
	 * To verify that the top of zoomed image alligns with the top of product image
	 * @return Boolean - true if images are in same row, else false
	 * @throws Exception - Exception
	 */
	public Boolean verifyTopOfZoomedImageAllignsWithTopOfProductImage()throws Exception{
		return BrowserActions.verifyElementsAreInSameRow(driver, divZoomWindowContainer, imgPrimaryImage);
	}
	
	/**
	 * To get order catalog number
	 * @return valueFromCatlogNumber - catalog number
	 * @throws Exception - Exception
	 */
	public String getOrderCatlogNumber()throws Exception{
		String valueFromCatlogNumber = new String();
		if(Utils.isMobile()) {
			valueFromCatlogNumber = BrowserActions.getText(driver, orderCatlogNumberMobile, "Order Catlog Number");
		} else {
			valueFromCatlogNumber = BrowserActions.getText(driver, orderCatlogNumberDesktop, "Order Catlog Number");
		}
		
		return valueFromCatlogNumber.trim();
	}
	
	/**
	 * To add all product to bag
	 * @throws Exception - Exception
	 */
	public void addAllPrdToBag()throws Exception{
		for(WebElement btn: btnLstAddToCart) {
			BrowserActions.clickOnElementX(btn, driver, "Add To Cart");
			if(Utils.waitForElement(driver, mdlMiniCartOverLay)) {
				closeAddToBagOverlay();
			}else {
				Log.fail("Something wrong with adding products to cart.", driver);
			}
		}
	}
	
	/**
	 * To verify swipe to next image
	 * @return boolean - true if next image is displaying, else false
	 * @throws Exception - Exception
	 */
	public boolean verifySwipeNextImage()throws Exception {
		Point point = imgPrimaryImage_Mobile.getLocation();
		Dimension di = imgPrimaryImage_Mobile.getSize();
		
		int x = point.x;
		int y = point.y + (di.height/2);

		BrowserActions.scrollToViewElement(imgPrimaryImage_Mobile, driver);
		
		Actions actions = new Actions(driver);
		actions.dragAndDropBy(imgPrimaryImage_Mobile, x, y);

		try {
			if(driver.findElement(By.cssSelector("[data-slick-index='1']")).getAttribute("class").contains("slick-active"))
				return true;
			else
				return false;
		}catch(NoSuchElementException e) {
			Log.reference("No other alt images.");
			return false;
		}
	}
	
	/**
	 * To get product Id by CQO item number
	 * @param String prdIdCQO - item number of the CQO product
	 * @return Product ID of catalog item
	 * @throws Exception - Exceptions
	 */
	public String getProductIDByCatalogNumber(String cqoNumber) throws Exception {
		String productID = null;
		WebElement cqoPgProduct = getCatalogItemByCQONumber(cqoNumber);
		productID = cqoPgProduct.findElement(By.cssSelector(".productthumbnail")).getAttribute("data-productid");
		return productID;
	}
	
	/**
     * To click on product details section
     * @param index
     * @param toggle - desired state of product details
     * @throws Exception
     */
    public void toggleProductDetails(int index, Toggle toggle) throws Exception {
    	toggleProductDetailsMobile(getCatalogItemByIndex(index), toggle);
    }
    
    /**
     * To click on product details section
     * @param cqoNumber
     * @param toggle - desired state of product details
     * @throws Exception
     */
    public void toggleProductDetails(String cqoNumber, Toggle toggle) throws Exception {
    	toggleProductDetailsMobile(getCatalogItemByCQONumber(cqoNumber), toggle);
    }

	/**
	 * Get list of catalog numbers of existing catalog products
	 * @return list of CQO number
	 * @throws Exception
	 */
	public List<String> getCatalogNumberList() throws Exception {
		List<String> cqoList = new ArrayList<String>();
		for (WebElement cqoNumber : lstCQONumbers) {
			String cqoValue = cqoNumber.getAttribute("name");
			cqoList.add(cqoValue);
		}
		Log.event("CQO number list:: " + cqoList.toString());
		return cqoList;
	}

	/**
	 * Get WebElement of existing catalog product by CQO number
	 * @param cqoNumber
	 * @return WebElement of catalog search product
	 * @throws Exception
	 */
	private WebElement getCatalogItemByCQONumber(String cqoNumber) throws Exception {
		if (cqoPgProducts.size() == 0)
			return null;
		
		Log.event("Getting CQO ietm with ID:: " + cqoNumber);
		List<String> cqoList = getCatalogNumberList();
		for (String cqo : cqoList) {
			if (cqo.replaceAll("\\-", "").equals(cqoNumber.replaceAll("\\-", ""))) {
				int index = getCatalogNumberList().indexOf(cqo);
				return getCatalogItemByIndex(index);
			}
		}
		Log.event("Catalog ietm with #" + cqoNumber + " not found. Returnig null.");
		return null;
	}

	/**
	 * Get WebElement of existing catalog product by index
	 * @param index
	 * @return WebElement of catalog search product
	 * @throws Exception
	 */
	private WebElement getCatalogItemByIndex(int index) throws Exception {
		if (cqoPgProducts.size() == 0) {
			return null;
		} else
			return cqoPgProducts.get(index);
	}
	
	private void toggleProductDetailsMobile(WebElement cqoItem, Toggle toggle) throws Exception {
		BrowserActions.scrollInToView(cqoItem, driver);
		if (Utils.isMobile()) {
			WebElement btnShowDetails = cqoItem.findElement(By.cssSelector(".product-variations-toggle"));
			boolean isOpen = btnShowDetails.getAttribute("class").contains("open");
			if ((toggle.equals(Toggle.Open) && !isOpen) || (toggle.equals(Toggle.Close) && isOpen)) {
				BrowserActions.clickOnElementX(btnShowDetails, driver, "product details toggle button");
				Log.event("Product details is now " + toggle.toString());
			}
		}
	}
	
	private String selectColor(WebElement cqoItem, String... colorVariation) throws Exception {
		String color = null;
		toggleProductDetailsMobile(cqoItem, Toggle.Open);
		try {
			List<WebElement> lstColor = cqoItem.findElements(By.cssSelector(" #color-variants-container .selectable .swatchanchor.color"));
			WebElement colorToSelect = null;
			if(colorVariation.length > 0) {
				for(WebElement colorElement : lstColor) {
					if(colorElement.getAttribute("aria-label").trim().toLowerCase().contains(colorVariation[0].toLowerCase())) {
						colorToSelect = colorElement;
						break;
					}
				}
			} else {
				colorToSelect = lstColor.get(Utils.getRandom(0, lstColor.size() - 1));
			}
			BrowserActions.clickOnSwatchElement(colorToSelect, driver, "color swatch");
			Utils.waitForPageLoad(driver);
			WebElement colorSelected = cqoItem.findElement(By.cssSelector("#current_color .selected-value"));
			color = BrowserActions.getText(driver, colorSelected, "selected color");
		} catch (Exception e) {
			Log.event("Current catalog item does not have color to select.");
		}
		return color;
	}
	
	private String selectSizeFamily(WebElement cqoItem, String... sizeFamilyVariation) throws Exception {
		String sizeFamily = null;
		toggleProductDetailsMobile(cqoItem, Toggle.Open);
		try {
			List<WebElement> lstFamily = cqoItem.findElements(By.cssSelector("#sizefamily-variants-container .selectable a.swatchanchor"));
			WebElement familyToSelect = null;
			if(sizeFamilyVariation.length > 0) {
				for(WebElement sizeFamilyElement : lstFamily) {
					if(sizeFamilyElement.getText().equalsIgnoreCase(sizeFamilyVariation[0])) {
						familyToSelect = sizeFamilyElement;
						break;
					}
				}
			} else {
				familyToSelect = lstFamily.get(Utils.getRandom(0, lstFamily.size() - 1));
			}
			familyToSelect = lstFamily.get(Utils.getRandom(0, lstFamily.size() - 1));
			BrowserActions.clickOnSwatchElement(familyToSelect, driver, "size family swatch");
			Utils.waitForPageLoad(driver);
			WebElement familySelected = cqoItem.findElement(By.cssSelector("#current_sizeFamily .selected-value"));
			sizeFamily = BrowserActions.getText(driver, familySelected, "selected size family");
		} catch (Exception e) {
			Log.event("Current catalog item does not have size family to select.");
		}
		return sizeFamily;
	}

	private String selectSize(WebElement cqoItem, String... sizeVariation) throws Exception {
		String size = null;
		toggleProductDetailsMobile(cqoItem, Toggle.Open);
		try {
			List<WebElement> lstSize = cqoItem.findElements(By.cssSelector("#size-variants-container .selectable a.swatchanchor"));
			WebElement sizeToSelect = null;
			if(sizeVariation.length > 0) {
				for(WebElement sizeElement : lstSize) {
					if(sizeElement.getText().equalsIgnoreCase(sizeVariation[0])) {
						sizeToSelect = sizeElement;
						break;
					}
				}
			} else {
				sizeToSelect = lstSize.get(Utils.getRandom(0, lstSize.size() - 1));
			}
			BrowserActions.clickOnSwatchElement(sizeToSelect, driver, "size swatch");
			Utils.waitForPageLoad(driver);
			WebElement sizeSelected = cqoItem.findElement(By.cssSelector("#current_size .selected-value"));
			size = BrowserActions.getText(driver, sizeSelected, "selected size");
		} catch (Exception e) {
			Log.event("Current catalog item does not have size to select.");
		}
		return size;
	}
	
	private String selectShoeSize(WebElement cqoItem, String... shoeSizeVariation) throws Exception {
		String shoeSize = null;
		toggleProductDetailsMobile(cqoItem, Toggle.Open);
		try {
			List<WebElement> lstShoeSize = cqoItem.findElements(By.cssSelector("#shoesize-variants-container .selectable a.swatchanchor"));
			WebElement shoeSizeToSelect = null;
			if(shoeSizeVariation.length > 0) {
				for(WebElement shoeSizeElement : lstShoeSize) {
					if(shoeSizeElement.getText().equalsIgnoreCase(shoeSizeVariation[0])) {
						shoeSizeToSelect = shoeSizeElement;
						break;
					}
				}
			} else {
				shoeSizeToSelect = lstShoeSize.get(Utils.getRandom(0, lstShoeSize.size() - 1));
			}
			BrowserActions.clickOnSwatchElement(shoeSizeToSelect, driver, "shoe size swatch");
			Utils.waitForPageLoad(driver);
			WebElement shoeSizeSelected = cqoItem.findElement(By.cssSelector("#current_shoeSize .selected-value"));
			shoeSize = BrowserActions.getText(driver, shoeSizeSelected, "selected shoe size");
		} catch (Exception e) {
			Log.event("Current catalog item does not have shoe size to select.");
		}
		return shoeSize;
	}

	private String selectShoeWidth(WebElement cqoItem, String... shoeWidthVariation) throws Exception {
		String shoeWidth = null;
		toggleProductDetailsMobile(cqoItem, Toggle.Open);
		try {
			List<WebElement> lstShoeWidth = cqoItem.findElements(By.cssSelector("#shoewidth-variants-container .selectable a.swatchanchor"));
			WebElement widthToSelect = null;
			if(shoeWidthVariation.length > 0) {
				for(WebElement shoeWidthElement : lstShoeWidth) {
					if(shoeWidthElement.getText().equalsIgnoreCase(shoeWidthVariation[0])) {
						widthToSelect = shoeWidthElement;
						break;
					}
				}
			} else {
				widthToSelect = lstShoeWidth.get(Utils.getRandom(0, lstShoeWidth.size() - 1));
			}
			BrowserActions.clickOnSwatchElement(widthToSelect, driver, "shoe width swatch");
			Utils.waitForPageLoad(driver);
			WebElement widthSelected = cqoItem.findElement(By.cssSelector("#current_shoeWidth .selected-value"));
			shoeWidth = BrowserActions.getText(driver, widthSelected, "selected shoe width");
		} catch (Exception e) {
			Log.event("Current catalog item does not have shoe width to select.");
		}
		return shoeWidth;
	}
	
	private String selectBraBandSize(WebElement cqoItem, String... bandSizeVariation) throws Exception {
		String braBandSize = null;
		toggleProductDetailsMobile(cqoItem, Toggle.Open);
		try {
			List<WebElement> lstBraBandSize = cqoItem.findElements(By.cssSelector("#brabandsize-variants-container .selectable a.swatchanchor"));
			WebElement braBandSizeToSelect = null;
			if(bandSizeVariation.length > 0) {
				for(WebElement bandSizeElement : lstBraBandSize) {
					if(bandSizeElement.getText().equalsIgnoreCase(bandSizeVariation[0])) {
						braBandSizeToSelect = bandSizeElement;
						break;
					}
				}
			} else {
				braBandSizeToSelect = lstBraBandSize.get(Utils.getRandom(0, lstBraBandSize.size() - 1));
			}
			BrowserActions.clickOnSwatchElement(braBandSizeToSelect, driver, "bra band size swatch");
			Utils.waitForPageLoad(driver);
			WebElement braBandSizeSelected = cqoItem.findElement(By.cssSelector("#current_braBandSize .selected-value"));
			braBandSize = BrowserActions.getText(driver, braBandSizeSelected, "selected shoe size");
		} catch (Exception e) {
			Log.event("Current catalog item does not have bra band size to select.");
		}
		return braBandSize;
	}

	private String selectBraCupSize(WebElement cqoItem, String... cupSizeVariation) throws Exception {
		String braCupSize = null;
		toggleProductDetailsMobile(cqoItem, Toggle.Open);
		try {
			List<WebElement> lstBraCupSize = cqoItem.findElements(By.cssSelector("#bracupsize-variants-container .selectable a.swatchanchor"));
			WebElement braCupSizeToSelect = null;
			if(cupSizeVariation.length > 0) {
				for(WebElement cupSizeElement : lstBraCupSize) {
					if(cupSizeElement.getText().equalsIgnoreCase(cupSizeVariation[0])) {
						braCupSizeToSelect = cupSizeElement;
						break;
					}
				}
			} else {
				braCupSizeToSelect = lstBraCupSize.get(Utils.getRandom(0, lstBraCupSize.size() - 1));
			}
			BrowserActions.clickOnSwatchElement(braCupSizeToSelect, driver, "bra cup size swatch");
			Utils.waitForPageLoad(driver);
			WebElement braCupSizeSelected = cqoItem.findElement(By.cssSelector("#current_braCupSize .selected-value"));
			braCupSize = BrowserActions.getText(driver, braCupSizeSelected, "selected bra cup");
		} catch (Exception e) {
			Log.event("Current catalog item does not have bra cup size to select.");
		}
		return braCupSize;
	}
	
	private void clickAddProductToBag(WebElement cqoItem) throws Exception {
		toggleProductDetailsMobile(cqoItem, Toggle.Open);
		WebElement btnAddToBag = cqoItem.findElement(By.cssSelector("#add-to-cart"));
		BrowserActions.clickOnElementX(btnAddToBag, driver, "add to bag button.");
	}
	
	public String selectSizeFamily(String cqoNumber, String... sizeFamilyVariation) throws Exception {
		return selectSizeFamily(getCatalogItemByCQONumber(cqoNumber), sizeFamilyVariation);
	}

	public String selectColor(String cqoNumber, String... colorVariation) throws Exception {
		return selectColor(getCatalogItemByCQONumber(cqoNumber), colorVariation);
	}

	public String selectSize(String cqoNumber, String... sizeVariation) throws Exception {
		return selectSize(getCatalogItemByCQONumber(cqoNumber), sizeVariation);
	}

	public String selectShoeWidth(String cqoNumber, String... shoeWidthVariation) throws Exception {
		return selectShoeWidth(getCatalogItemByCQONumber(cqoNumber), shoeWidthVariation);
	}
	
	public String selectShoeSize(String cqoNumber, String... shoeSizeVariation) throws Exception {
		return selectShoeSize(getCatalogItemByCQONumber(cqoNumber), shoeSizeVariation);
	}
	
	public String selectBraBandSize(String cqoNumber, String... bandSizeVariation) throws Exception {
		return selectBraBandSize(getCatalogItemByCQONumber(cqoNumber), bandSizeVariation);
	}
	
	public String selectBraCupSize(String cqoNumber, String... cupSizeVariation) throws Exception {
		return selectBraCupSize(getCatalogItemByCQONumber(cqoNumber), cupSizeVariation);
	}

	/**
	 * To select all variation option of a catalog item
	 * @param cqoNumber
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void selectVariation(String cqoNumber, HashMap<String, String>... productCatalogVariatons) throws Exception {
		if(productCatalogVariatons.length > 0) {
			if(productCatalogVariatons[0].containsKey("sizefamily")) {selectSizeFamily(cqoNumber, productCatalogVariatons[0].get("sizefamily"));}
			if(productCatalogVariatons[0].containsKey("color")) {selectColor(cqoNumber, productCatalogVariatons[0].get("color"));}
			if(productCatalogVariatons[0].containsKey("size")) {selectSize(cqoNumber, productCatalogVariatons[0].get("size"));}
			if(productCatalogVariatons[0].containsKey("shoewidth")) {selectShoeWidth(cqoNumber, productCatalogVariatons[0].get("shoewidth"));}
			if(productCatalogVariatons[0].containsKey("shoesize")) {selectShoeSize(cqoNumber, productCatalogVariatons[0].get("shoesize"));}
			if(productCatalogVariatons[0].containsKey("bandsize")) {selectBraBandSize(cqoNumber, productCatalogVariatons[0].get("bandsize"));}
			if(productCatalogVariatons[0].containsKey("cupsize")) {selectBraCupSize(cqoNumber, productCatalogVariatons[0].get("cupsize"));}
		} else {
			selectSizeFamily(cqoNumber);
			selectColor(cqoNumber);
			selectSize(cqoNumber);
			selectShoeWidth(cqoNumber);
			selectShoeSize(cqoNumber);
			selectBraBandSize(cqoNumber);
			selectBraCupSize(cqoNumber);
		}
	}
	
	public void clickAddProductToBag(String cqoNumber) throws Exception {
		clickAddProductToBag(getCatalogItemByCQONumber(cqoNumber));
	}

	public String selectSizeFamily(int index, String... sizeFamilyVariation) throws Exception {
		return selectSizeFamily(getCatalogItemByIndex(index), sizeFamilyVariation);
	}

	public String selectColor(int index, String... colorVariation) throws Exception {
		return selectColor(getCatalogItemByIndex(index), colorVariation);
	}

	public String selectSize(int index, String... sizeVariation) throws Exception {
		return selectSize(getCatalogItemByIndex(index), sizeVariation);
	}

	public String selectShoeWidth(int index, String... shoeWidthVariation) throws Exception {
		return selectShoeWidth(getCatalogItemByIndex(index), shoeWidthVariation);
	}
	
	public String selectShoeSize(int index, String... shoeSizeVariation) throws Exception {
		return selectShoeSize(getCatalogItemByIndex(index), shoeSizeVariation);
	}
	
	public String selectBraBandSize(int index, String... bandSizeVariation) throws Exception {
		return selectBraBandSize(getCatalogItemByIndex(index), bandSizeVariation);
	}
	
	public String selectBraCupSize(int index, String... cupSizeVariation) throws Exception {
		return selectBraCupSize(getCatalogItemByIndex(index), cupSizeVariation);
	}

	/**
	 * To select all variation option of a catalog item
	 * @param cqoNumber
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void selectVariation(int index, HashMap<String, String>... productCatalogVariatons) throws Exception {
		if(productCatalogVariatons.length > 0) {
			if(productCatalogVariatons[0].containsKey("sizefamily")) {selectSizeFamily(index, productCatalogVariatons[0].get("sizefamily"));}
			if(productCatalogVariatons[0].containsKey("color")) {selectColor(index, productCatalogVariatons[0].get("color"));}
			if(productCatalogVariatons[0].containsKey("size")) {selectSize(index, productCatalogVariatons[0].get("size"));}
			if(productCatalogVariatons[0].containsKey("shoewidth")) {selectShoeWidth(index, productCatalogVariatons[0].get("shoewidth"));}
			if(productCatalogVariatons[0].containsKey("shoesize")) {selectShoeSize(index, productCatalogVariatons[0].get("shoesize"));}
			if(productCatalogVariatons[0].containsKey("bandsize")) {selectBraBandSize(index, productCatalogVariatons[0].get("bandsize"));}
			if(productCatalogVariatons[0].containsKey("cupsize")) {selectBraCupSize(index, productCatalogVariatons[0].get("cupsize"));}
		} else {
			selectSizeFamily(index);
			selectColor(index);
			selectSize(index);
			selectShoeWidth(index);
			selectShoeSize(index);
			selectBraBandSize(index);
			selectBraCupSize(index);
		}
	}
	
	public void clickAddProductToBag(int index) throws Exception {
		clickAddProductToBag(getCatalogItemByIndex(index));
	}
	
	/**
	 * To select variation options for all catalog items
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void selectVariationAllCatalogItem() throws Exception {
		int cqoCount = cqoPgProducts.size();
		for (int i = 0 ; i < cqoCount ; i++) {
			selectVariation(i);
		}
	}
	
	/**
	 * To get product variation ID from add to cart overlay
	 * <br>Variation ID is available only when a variation is added to cart.
	 * 
	 */
	public String getVariationFromCartOverlay() throws Exception {
		String variationURL = "", prdVariation = "";
		if (Utils.waitForElement(driver, mdlMiniCartOverLay)) {
			WebElement prdOverlayNameLink = mdlMiniCartOverLay.findElement(By.cssSelector(".mini-cart-name>a"));
			variationURL = prdOverlayNameLink.getAttribute("href");
			prdVariation = UrlUtils.getProductIDFromURL(variationURL);
		}
		
		return prdVariation;
	}
}
