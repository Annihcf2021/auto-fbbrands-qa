package com.fbb.pages.ordering;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.pages.HomePage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.SignIn;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class GuestOrderStatusLandingPage extends LoadableComponent <GuestOrderStatusLandingPage>{
	
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	/**********************************************************************************************
	 ********************************* WebElements of Guest Order Status Landing Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = ".pt_order")
	WebElement readyElement;
	
	@FindBy(css = ".pt_account.null")
	WebElement Lnk;
	
	@FindBy(css = ".pt_order")
	WebElement lnkGuestorderStatusPage;
	
	@FindBy(css = ".order-details-section .orderdetails [class='actions'] a")
	WebElement btnReturn;
	
	@FindBy(css = ".promo-banner")
	WebElement cntPromoBanner;
	
	@FindBy(css = "div.breadcrumb")
	WebElement cntBreadCrumb;
	
	@FindBy(css = ".breadcrumb .hide-mobile")
	List<WebElement> lblBreadCrumb;
	
	@FindBy(xpath = "//a[contains(@class,'breadcrumb-element')][contains(text(),'Check An Order')]")
	WebElement lnkBreadCrumbCheckAnOrder;
	
	@FindBy(css = "a[title='Go to Home']")
	WebElement lnkBreadCrumbHome;
	
	@FindBy(css = ".breadcrumb-desktop-label")
	WebElement lnkBreadCrumbHomeMobile;
	
	@FindBy(css = ".breadcrumb-element.current-element.hide-desktop.hide-tablet")
	WebElement lnkBreakCrumbMobile;
	
	@FindBy(css = ".return-line-items")
	WebElement sectionReturnItems;
	
	@FindBy(css = ".return-sub-header")
	WebElement lblReturnSubHeader;
	
	@FindBy(css = ".return-message")
	WebElement lblReturnMessage;
	
	@FindBy(css = ".return-sub-header")
	WebElement lblSpecialCalloutCopy;
	
	@FindBy(css = ".return-cancel")
	WebElement btnCancel;
	
	@FindBy(name = "dwfrm_ordertrack_returnorder")
	WebElement btnReturnSelectedItems;
	
	@FindBy(css = ".eligible-list .header-msg.heading")
	WebElement lblEligibleListHeader;
	
	@FindBy(css = ".returns-check")
	List<WebElement> chkItemListCheckBox;
	
	@FindBy(css = ".list-tems .cart-columns")
	List<WebElement> lstProductDetail;
	
	@FindBy(css = ".order-number .value")
	WebElement spanOrderNumber;
	
	@FindBy(css = ".order-status .receipt-message")
	WebElement spanReceiptMessage;
	
	@FindBy(css = ".line-item")
	List<WebElement> divProductCount;
	
	@FindBy(css = ".product-list-item .name a")
	List<WebElement> lnkPrdName;
	
	@FindBy(css = ".display-inline-block")
	List<WebElement> listPrdId;
	
	@FindBy(css = ".product-list-item div[data-attribute='color'] .value")
	List<WebElement> listPrdColor;
	
	@FindBy(css = ".col-2 .line-item-quantity .qty-value")
	List<WebElement> listPrdQty;
	
	@FindBy(css = ".price-sales")
	List<WebElement> listPrdPrize;
	
	@FindBy(css = ".coupon-codes .value")
	WebElement txtPromocode;
	
	@FindBy(css= ".cc-number .img")
	WebElement txtcardName;
	
	@FindBy(css= ".total .value")
	WebElement txtOrderTotal;
	
	@FindBy(css = ".address")
	WebElement orderShipAddress;
	
	@FindBy(css= ".mini-address-location")
	WebElement txtBillingAddress;
	
	@FindBy(css = ".method.hide-mobile .shipment-method .value")
	WebElement txtShippingMethodDeskTab;
	
	@FindBy(css = ".method.hide-desktop.hide-tablet .shipment-method .value")
	WebElement txtShippingMethodMob;
	
	@FindBy(css = ".view-more-link")
	WebElement lnkViewDetailsMob;
	
	@FindBy(css = ".line-items .buy-again a")
	List<WebElement> lnkBuyAgain;
	
	/**********************************************************************************************
	 ********************************* WebElements of EasyReturnsPage - Ends ****************************
	 **********************************************************************************************/

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public GuestOrderStatusLandingPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Page did not open up. Site might be down.", driver);
		}
		
		elementLayer = new ElementLayer(driver);

	}// isLoaded
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on Return 
	 * @throws Exception -
	 */
	public void clickOnReturn()throws Exception{
		BrowserActions.clickOnElementX(btnReturn, driver, "Return button");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To verify breadcrumb value
	 * @param checkString -
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyBreadCrumb(String checkString)throws Exception{
		String compareString = new String();
		for(WebElement ele : lblBreadCrumb) {
			compareString += ele.getAttribute("innerHTML").trim();
		}
		
		Log.event("String from Element :: " + compareString);
		Log.event("Text From User :: " + checkString);
		
		return (compareString.equals(checkString))? true : false;
	}

	/**
	 * To click on Check And Order link in Breadcrumb
	 * @return SignIn 
	 * @throws Exception - Exception
	 */
	public SignIn clickOnCheckAnOrderInBC()throws Exception{
		BrowserActions.clickOnElementX(lnkBreadCrumbCheckAnOrder, driver, "Check An Order in Breadcrumb");
		return new SignIn(driver).get();
	}
	
	/**
	 * To click on Home link in Breadcrumb
	 * @return SignIn 
	 * @throws Exception - Exception
	 */
	public HomePage clickOnHomeInBC()throws Exception{
		if(!Utils.isMobile()) {
			BrowserActions.clickOnElementX(lnkBreadCrumbHome, driver, "Home in Breadcrumb");
		}else {
			BrowserActions.clickOnElementX(lnkBreadCrumbHomeMobile, driver, "Home in Breadcrumb");
		}
		return new HomePage(driver).get();
	}
	
	/**
	 * To get order number
	 * @return String - Order number
	 * @throws Exception - Exception
	 */
	public String getOrderNumber() throws Exception {
		return BrowserActions.getText(driver, spanOrderNumber, "order number").trim();
	}
	
	/**
	 * To get product details as list
	 * @return LinkedList - Product attributes 
	 * @throws Exception - Exception
	 */
	public LinkedList<LinkedHashMap<String, String>> getProductDetails()throws Exception {
		LinkedList<LinkedHashMap<String, String>> dataToReturn = new LinkedList<LinkedHashMap<String, String>>();
		
		String productType = null;
		for(int i = 0; i < divProductCount.size(); i++) {
			LinkedHashMap<String, String> product = new LinkedHashMap<String, String>();
			productType = getProductType(i);
			try { product.put("Name", getProductName(i)); }catch(NoSuchElementException e) {product.put("Name", "");}
			try { product.put("Id", getProductId(i)); }catch(NoSuchElementException e) {product.put("Id", "");}
			try { product.put("Color", getProductColorVariation(i)); }catch(NoSuchElementException e) {product.put("Color", "");}
			
			if(productType.contains("family")){
				try { product.put("SizeFamily", getProductSizeAttributes(i,productType).get("firstsize")); }catch(NoSuchElementException e) {product.put("Size", "");}
				try { product.put("Size", getProductSizeAttributes(i,productType).get("secondsize")); }catch(NoSuchElementException e) {product.put("Size", "");}
				
			} else if (productType.contains("shoe")) {
				try { product.put("ShoeWidth", getProductSizeAttributes(i,productType).get("firstsize")); }catch(NoSuchElementException e) {product.put("Size", "");}
				try { product.put("ShoeSize", getProductSizeAttributes(i,productType).get("secondsize")); }catch(NoSuchElementException e) {product.put("Size", "");}
				
			} else if (productType.contains("bra")) {
				try { product.put("BraBandSize", getProductSizeAttributes(i,productType).get("firstsize")); }catch(NoSuchElementException e) {product.put("Size", "");}
				try { product.put("BraCupSize", getProductSizeAttributes(i,productType).get("secondsize")); }catch(NoSuchElementException e) {product.put("Size", "");}
								
			} else {
				try { product.put("Size", getProductSizeVariation(i)); }catch(NoSuchElementException e) {product.put("Size", "");}
			}
			
			try { product.put("Quantity", getProductQuantity(i)); }catch(NoSuchElementException e) {product.put("Quantity", "");}
			try { product.put("Price", getProductPrice(i)); }catch(NoSuchElementException e) {product.put("Price", "");}
			dataToReturn.add(product);
		}
		return dataToReturn;
	}
	
	/**
	 * To get product type of the product
	 * @param index- product row or name index
	 * @return string - product type
	 * @throws Exception
	 */
	public String getProductType(int prdIndex_Size) throws Exception {
		String[] sizeType = {"size", "shoeWidth", "sizeFamily", "braBandSize"};
		WebElement orderRow = divProductCount.get(prdIndex_Size);
		for(String size : sizeType) {
			try{
				orderRow.findElement(By.cssSelector("div[data-attribute='"+size+"'] .value"));
				return size + "Product" ;
			} catch (NoSuchElementException ex) {
				Log.event("Entry does not have expected size type.");
			}
		}
		return null;
	}
	
	/**
	 * To get product Quantity value by index
	 * @param index - Product index for quantity
	 * @return String - Product quantity text
	 * @throws Exception - Exception
	 */
	public String getProductQuantity(int prdIndex_Quantity)throws Exception {
		String prdQuantity = BrowserActions.getText(driver, listPrdQty.get(prdIndex_Quantity), "Product Quantity");
		return prdQuantity;
	}
	
	/**
	 * To get product price value by index
	 * @param index - Product index for price
	 * @return String - Product price text
	 * @throws Exception - Exception
	 */
	public String getProductPrice(int prdIndex_Price)throws Exception {
		String prdPrice = BrowserActions.getText(driver, listPrdPrize.get(prdIndex_Price), "Product Price");
		return prdPrice;
	}
	
	/**
	 * To get product color value by index
	 * @param index - Product Index for Color
	 * @return String - Product color text
	 * @throws Exception - Exception
	 */
	public String getProductColorVariation(int prdIndex_Color)throws Exception {
		String prdColor = BrowserActions.getText(driver, listPrdColor.get(prdIndex_Color), "Product Color");
		return prdColor;
	}
	
	/**
	 * To get product name value by index
	 * @param index - Product name index
	 * @return String - Product name text
	 * @throws Exception - Exception
	 */
	public String getProductName(int prdIndex_Name)throws Exception {
		WebElement name = lnkPrdName.get(prdIndex_Name).findElement(By.cssSelector("a"));
		String prdName = BrowserActions.getText(driver, name, "Product name");
		return prdName;
	}
	
	/**
	 * To get product Id value by index
	 * @param index - Product index for ID
	 * @return String - Product Id text
	 * @throws Exception - Exception
	 */
	public String getProductId(int prdIndex_Id)throws Exception {
		String prdId = BrowserActions.getText(driver, listPrdId.get(prdIndex_Id), "Product Id");
		return prdId;
	}
	
	/**
	 * To get the size family, shoe type and Bra type product sizes
	 * @param index - Product index for size
	 * @return hashmap string
	 * @throws Exception
	 */
	
	public LinkedHashMap<String, String> getProductSizeAttributes(int product_Size , String productType) throws Exception {
		LinkedHashMap<String, String > dataToReturn = new LinkedHashMap<String, String>();
		WebElement firsElement, secondElement ; 
		WebElement orderRow = divProductCount.get(product_Size);
		
		if(productType.contains("family")) {
			firsElement = orderRow.findElement(By.cssSelector("div[data-attribute='sizeFamily'] .value"));
			secondElement = orderRow.findElement(By.cssSelector("div[data-attribute='size'] .value"));
			
		} else if(productType.contains("shoe")) {
			firsElement = orderRow.findElement(By.cssSelector("div[data-attribute='shoeWidth'] .value"));
			secondElement = orderRow.findElement(By.cssSelector("div[data-attribute='shoeSize'] .value"));
		
		} else {
			firsElement = orderRow.findElement(By.cssSelector("div[data-attribute='braBandSize'] .value"));
			secondElement = orderRow.findElement(By.cssSelector("div[data-attribute='braCupSize'] .value"));
		}
		dataToReturn.put("firstsize", BrowserActions.getText(driver, firsElement, productType +"first attribute value"));
		dataToReturn.put("secondsize", BrowserActions.getText(driver, secondElement, productType + "Second attribute value"));
	
		return dataToReturn;
	}
	
	/**
	 * To get product size value by index
	 * @param index - Product index for size
	 * @return String - product size text
	 * @throws Exception - Exception
	 */
	public String getProductSizeVariation(int prdIndex_Size)throws Exception {
		WebElement size = null;
		WebElement orderRow = divProductCount.get(prdIndex_Size);		
		try{
			size = orderRow.findElement(By.cssSelector("div[data-attribute='size'] .value"));
			return BrowserActions.getText(driver, size, "Product Size");
		} catch (NoSuchElementException ex) {
			Log.event("product doesnt have size");
		}				
		return null;
	}
	
	/**
	 * To get promo code
	 * @return String-promo code
	 * @throws - Exception
	 */
	public String  getPromoCode() throws Exception {
		if(Utils.waitForElement(driver, txtPromocode)) {
			return BrowserActions.getText(driver, txtPromocode, "promo code").trim();
		}
		return null;
	}
	
	/**
	 * To get the card name
	 * @return- String Card name
	 * @throws Exception
	 */
	public String getPaymetMethodName() throws Exception {
		if(Utils.waitForElement(driver, txtcardName)) {
			return BrowserActions.getText(driver, txtcardName, "card name").trim();
		}
		return null;
	}
	
	/**
	 * To get Order total
	 * @return String = Order total
	 * @throws Exception- Exception
	 */
	public String getorderTotal() throws Exception {
		if(Utils.waitForElement(driver, txtOrderTotal)) {
			return BrowserActions.getText(driver, txtOrderTotal, "Order total").trim().replace("$", "");
		}
		return null;
	}

	/**
	 * To get the Shipping address
	 * @return String -  Shipping address details
	 * @throws Exception - Exception
	 */
	public String getShippingAddressDetails()throws Exception {
		return BrowserActions.getText(driver, orderShipAddress, "Shipping Address").trim();		
	}
	
	/**
	 * To get the billing address
	 * @return String - billing address details
	 * @throws Exception - Exception
	 */
	public String getBillingAddressDetails()throws Exception {
		return BrowserActions.getText(driver, txtBillingAddress, "Shipping Address").trim();		
	}

	/**
	 * To get Shipping method
	 * @return- String shipping method
	 * @throws Exception
	 */
	public String getShippingMethod() throws Exception {
		if(Utils.isMobile()) {
			BrowserActions.clickOnElementX(lnkViewDetailsMob, driver, "View Details");
			return BrowserActions.getText(driver, txtShippingMethodMob, "shipping method").trim();
		}
		return BrowserActions.getText(driver, txtShippingMethodDeskTab, "Shipping method").trim();
	}
	
	/**
	 * To click on the "buy again" link based on given index
	 * @param index - integer
	 * @return PdpPage
	 * @throws Exception
	 */
	public PdpPage clickOnBuyAgainLinkByIndex(int index) throws Exception {
		BrowserActions.clickOnElementX(lnkBuyAgain.get(index), driver, "buy again link");
		Utils.waitForPageLoad(driver);
		return new PdpPage(driver).get();
	}
}
