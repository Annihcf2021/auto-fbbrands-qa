package com.fbb.pages.ordering;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.ElementLayer;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;
	
public class OrderStatusPage extends LoadableComponent <OrderStatusPage>{
	
	
	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	
	/**********************************************************************************************
	 ********************************* WebElements of OrderStatusPage Page ***********************************
	 **********************************************************************************************/

	@FindBy(css = "#primary")
	WebElement readyElement;
	
	@FindBy(css = ".secondary-left-section")
	WebElement leftNavigationElements;
	
	@FindBy(css = ".button-text.view-detials")
	WebElement btnViewDetails;
	
	
	/**********************************************************************************************
	 ********************************* WebElements of OrderStatusPage - Ends ****************************
	 **********************************************************************************************/

	

	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : Webdriver
	 * 
	 */
	public OrderStatusPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		Utils.waitForPageLoad(driver);

		if (!isPageLoaded) {
			Assert.fail();
		}

		Utils.waitForPageLoad(driver);

		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Order Status Page did not open up. Site might be down.", driver);
		}
		
		elementLayer = new ElementLayer(driver);

	}// isLoaded
	
	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}
	
	
	/**
	 * To click on view details button
	 * @throws Exception - Exception - Exception
	 */
	public void clickViewDetailsBtn() throws Exception{
		
		BrowserActions.clickOnElementX(btnViewDetails, driver, "View Details");
		
	}




}
