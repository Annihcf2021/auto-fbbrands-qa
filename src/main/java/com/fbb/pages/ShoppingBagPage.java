package com.fbb.pages;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.fbb.pages.headers.Headers;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.Enumerations.Direction;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.CollectionUtils;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;


public class ShoppingBagPage extends LoadableComponent<ShoppingBagPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;
	private static EnvironmentPropertiesReader demandwareData = EnvironmentPropertiesReader.getInstance("demandware");

	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	private static final String Recently_Viewed_Section = ".product-listing.last-visited";
	private static final String Cart_Recommendation=".you-may-like";

	@FindBy(css = ".pt_cart")
	WebElement miniCartContent;

	@FindBy(css = ".name>a")
	WebElement txtProductNameDesktop;
	
	@FindBy(css = ".international-cart-message")
	WebElement internationalCartBanner;

	@FindBy(css = ".cart-top-banner h1")
	WebElement headerShoppingBag;

	@FindBy(css = ".order-subtotal .value")
	WebElement lblOrderSubtotal;

	@FindBy(css = ".cart-top-banner .button-fancy-large")
	WebElement btnCheckoutNow;
	
	@FindBy(css = ".bonusheading")
	WebElement btnBonusHeading;
	
	@FindBy(css = "#international-modal-submit")
	WebElement btnIntrnShippingContinue;
	
	@FindBy(css = ".bonus-product-list .primary-image")
	WebElement imgBonusPrd;
	
	@FindBy(css = ".product-availability-list.attribute .on-order")
	List<WebElement> lstLblProductAvailability;
	
	@FindBy(css = ".product-availability-list.attribute .on-order")
	WebElement lblProductAvailability;
	
	@FindBy(css = ".bonus-product-list .product-detail .product-name")
	WebElement txtBonusPrdName;
	
	@FindBy(css = ".bonus-product-items .product-name")
	WebElement txtBonusPrdNameMob;
	
	@FindBy(css = ".bonus-product-list .product-detail .product-desc")
	WebElement txtBonusPrdDesc;
	
	@FindBy(css = ".bonus-product-list .morelink")
	WebElement lnkBonusViewMore;
	
	@FindBy(css = ".bonus-product-list .morelink.less")
	WebElement lnkBonusViewMoreExpanded;
	
	@FindBy(css = ".bonus-product-list .price .value")
	WebElement txtBonusPrdPrice;
	
	@FindBy(css = ".bonus-product")
	WebElement txtBonusPrdAvailability;
	
	@FindBy(css = ".bonus-product .item-details")
	WebElement fixedBonusPrdDetailsSection;
	
	@FindBy(css = ".bonus-product .cart-columns>.columns-combine .custom-cart-update")
	WebElement fixedBonusPrdQty;
	
	@FindBy(css = ".bonus-product .cart-columns>.column.col-1 .custom-cart-update")
	WebElement fixedBonusPrdQtyMobile;
	
	@FindBy(css = ".bonus-product .cart-columns>.columns-combine .qty-increase.qty-arrows.disabled")
	WebElement bonusPrdQtyUpArrow;
	
	@FindBy(css = ".bonus-product .cart-columns>.column.col-1 .qty-increase.qty-arrows.disabled")
	WebElement bonusPrdQtyUpArrowMobile;
	
	@FindBy(css = ".bonus-product .cart-columns>.columns-combine .qty-decrease.qty-arrows.disabled")
	WebElement bonusPrdQtyDownArrow;
	
	@FindBy(css = ".bonus-product .cart-columns>.column.col-1 .qty-decrease.qty-arrows.disabled")
	WebElement bonusPrdQtyDownArrowMobile;
	
	@FindBy(css = ".bonus-product .item-total .bonus-item")
	WebElement bonusPrdSubTotal;
	
	@FindBy(css = ".bonus-product .cart-unit-price")
	WebElement txtCartBonusPrdPrice;
	
	@FindBy(css = ".bonus-product-calloutMsg")
	WebElement bonusCalloutMsg;
	
	@FindBy(css = ".bonus-product-item input.bonus-check")
	WebElement checkBonusRadio;
	
	@FindBy(css = ".promodetails ")
	WebElement lnkPromoDetails;
	
	@FindBy(css = ".promo-content")
	WebElement lnkPromoContent;
	
	@FindBy(css = ".intlshipping-exclusions")
	WebElement modalInternationalExclusion;
	
	@FindBy(css = ".intlshipping-exclusions .product-list-item .name a")
	List<WebElement> prdNameInInternationalExclusion;
	
	@FindBy(css = ".button-fancy-large.remove-items")
	WebElement btnInternationalExcRemoveItem;
	
	@FindBy(css = ".shipping-note")
	WebElement lblInternShipPromotionExclusion;
	
	@FindBy(css = ".intl-shipping-btn span")
	WebElement lnkInternationalShipping;
	
	@FindBy(css = ".international-shipping-modal")
	WebElement modalInternationalShipping;
	
	@FindBy(css = "#country_code")
	WebElement selectIntnShipCountry;
	
	@FindBy(css = ".tax-disclaimer")
	WebElement lblTaxDisclaimer;

	@FindBy(css = ".tooltip-click.coupon-tooltip-link")
	WebElement lnkToolTipCoupon;

	@FindBy(css = ".coupon-tooltip .ui-icon-closethick")
	WebElement lnkCloseToolTipCoupon;
	
	@FindBy(css = ".cart-footer .checkout-paypal-btn img")
	WebElement lnkCheckoutPaypal;

	@FindBy(css = ".cart-coupon-code .error-messages.hide-mobile")
	WebElement txtCouponErrorDeskTab;
	
	@FindBy(css = ".payment-cards img[alt*='Visa']")
	WebElement imgPaymentVisa;
	
	@FindBy(css = ".payment-cards img[alt*='Master']")
	WebElement imgPaymentMaster;
	
	@FindBy(css = ".payment-cards img[alt*='Amex']")
	WebElement imgPaymentAmex;
	
	@FindBy(css = ".payment-cards img[alt*='American Express Card']")
	WebElement imgPaymentAmex_BH;
	
	@FindBy(css = ".payment-cards img[alt*='Discover Card']")
	WebElement imgPaymentDiscover;
	
	@FindBy(css = ".payment-order .heading")
	WebElement paymentHeading;
	
	@FindBy(css = ".payment-cards img[alt*='PayPal']")
	WebElement imgPaymentPaypal;
	
	@FindBy(css = ".brand-cards img ")
	WebElement imgPaymentPlatinum;
	
	@FindBy(css = ".payment-order .brand-cards")
	WebElement plccInfo;
	
	@FindBy(css = ".hide-desktop.hide-tablet .rw-error")
	WebElement rwCouponErrorMobile;
	
	@FindBy(css = ".cartcoupon .removebutton")
	WebElement lnkCouponRemove;

	@FindBy(css = ".cart-coupon-code .error-messages.hide-desktop.hide-tablet")
	WebElement txtCouponErrorMob;

	@FindBy(css = ".cart-promo.cart-promo-approaching")
	WebElement divDiscountApproach;	

	@FindBy(css = ".order-totals-table .shippingoverlay-link")
	WebElement divShippingOverlayToolTip;

	@FindBy(css = ".shipping-overlay .ui-dialog-titlebar-close")
	WebElement divShippingOverlayToolTipClose;

	@FindBy(css = ".button-fancy-large.add-to-cart")
	WebElement btnUpdateInEditOverlay;

	@FindBy(css = ".item-edit-details.btm-space a")
	WebElement btnEditDetails;

	@FindBy(css = ".order-subtotal.order-detail")
	WebElement divOrderSubTotal;

	@FindBy(css = ".order-discount.discount.order-detail")
	WebElement orderSummaryDiscount;

	@FindBy (css = ".order-shipping.order-detail")
	WebElement orderSummaryShippingDetail;

	@FindBy (css = ".order-shipping-discount.discount.order-detail")
	WebElement orderSummaryShippingDiscountDetail;

	@FindBy (css = ".order-sales-tax.order-detail")
	WebElement orderSummarySalesTax;

	@FindBy (css = ".order-total.order-detail.order-total-remaining")
	WebElement orderSummaryOrderTotal;

	@FindBy (css = ".reward-certificate.discount.order-detail")
	WebElement orderSummaryRewardDiscount;

	@FindBy (css = ".gift-card.discount.order-detail")
	WebElement orderSummaryGiftCardDiscount;

	@FindBy (css = ".remaining-total.order-detail")
	WebElement orderSummaryRemainingTotal;

	@FindBy (css = ".order-saving.discount.order-detail")
	WebElement orderSummaryTotalSavings;

	@FindBy(css = ".product-list-item .display-inline-block")
	List<WebElement> lblPrdListNumbers;

	@FindBy(css = ".order-shipping.order-detail .label")
	WebElement divShippingMessage;
	
	@FindBy(css = ".order-shipping.order-detail .value")
	WebElement divShippingMessagePrice;
	
	@FindBy(css = ".order-shipping-discount.discount .value")
	WebElement divDiscountMessagePrice;

	@FindBy(css = ".item-details .price-standard")
	WebElement lblPriceStrikeDown;

	@FindBy(css = ".payment-order .see-details")
	WebElement lnkSeeDetails;

	@FindBy(css = "#dialog-container .surcharge-product")
	WebElement txtSurchargeProductRate;
	
	@FindBy(css = "#dialog-container .surcharge-product")
	List<WebElement> lstSurchargeProductRate;

	@FindBy(css = ".item-details .price-sales")
	WebElement lblSalesPrice;

	@FindBy(css = ".hide-desktop.btm-space a")
	WebElement btnEditDetails_Mobile_Tablet;

	@FindBy(css = ".button-text.btm-spac.remove-btn")
	List<WebElement> btnDeleteErrProd;
	
	@FindBy(css = ".cart-row")
	List<WebElement> sectionCartedProduct;
	
	@FindBy(css = ".item-links")
	WebElement txtWishlistItem;

	@FindBy(css = ".item-total")
	WebElement txtSubtotal;

	@FindBy(css = "#Quantity")
	WebElement btnEditOverlayQty;

	@FindBy(css = ".specialproductsetcomponents.hide-mobile .cart-row.specialproductsetchild")
	WebElement specialProductSetChild;

	@FindBy(css = ".specialproductsetcomponents.hide-mobile .cart-row.specialproductsetchild .sku .value")
	WebElement specialProductSetChildID;

	@FindBy(css = ".specialproductsetcomponents.hide-mobile .cart-row.specialproductsetchild div[data-attribute='size'] .value")
	WebElement specialProductSetChildSize;

	@FindBy(css = ".specialproductsetcomponents.hide-mobile .cart-row.specialproductsetchild div[data-attribute='color'] .value")
	WebElement specialProductSetChildColor;

	@FindBy(css = "button[name*='deleteProduct']")
	List<WebElement> lstBtnRemove;

	@FindBy(css = "button[name*='deleteProduct']")
	WebElement btnRemove;

	@FindBy(css = ".item-quantity")
	WebElement 	divItemQuantity;

	@FindBy(css = ".cart-top-banner")
	WebElement 	divCartTopBanner;

	@FindBy(css = ".gift-Card img:not(.cart-brand-logo)")
	WebElement 	imgGCBrandImage;

	@FindBy(css = ".cart-top-banner .cart-action-checkout .button-fancy-large")
	WebElement 	divCartTopBannerCheckOutButton;

	@FindBy(css = ".bonus-item-actions")
	WebElement 	bonusItemAction;

	@FindBy(css = ".specialmessaging")
	WebElement 	txtSplMessage;
	
	@FindBy(css = ".specialmessaging")
	List<WebElement> lstTxtSplMessage;

	@FindBy(css = ".cart-promo.bonus-product-calloutMsg")
	WebElement 	bonusCartPromo;

	@FindBy(css = ".custom-cart-update .input-text")
	List<WebElement> valueItemQuantity;

	@FindBy(name = "dwfrm_cart_shipments_i0_items_i0_quantity")
	WebElement 	txtItemQuantity;

	@FindBy(name = "dwfrm_cart_shipments_i0_items_i1_quantity")
	WebElement 	txtItemSecondQuantity;

	@FindBy(css = ".qty-error-msg")
	WebElement 	divQuantityMessage;

	@FindBy(css = ".cart-top-banner")
	WebElement 	divHeadSection;

	@FindBy(css = ".cart-promo.cart-promo-approaching")
	WebElement 	divAlertSection;

	@FindBy(css = ".add-to-wishlist")
	WebElement 	lnkAddToWishlist;

	@FindBy(css = ".in-wishlist")
	WebElement 	txtInWishList;

	@FindBy(css = ".button-text.btm-spac.remove-btn")
	WebElement removeProduct;
	
	@FindBy(css = ".cart-columns>.col-4 .button-text.btm-spac.remove-btn")
	List<WebElement> removeProductList;
	
	@FindBy(css = ".cart-columns>.col-1 .button-text.btm-spac.remove-btn")
	List<WebElement> removeProductListMobile;

	@FindBy(css = ".email input.email.required.error")
	WebElement receipentMailError;

	@FindBy(css = ".confirmemail input.email.required.error")
	WebElement receipentConfirmMailError;

	@FindBy(css = ".fields-address input.from.error")
	WebElement fromMailError;

	@FindBy(css = ".message.error textarea")
	WebElement msgAreaError;

	@FindBy(css = ".please-select.error")
	WebElement mainEGiftError;

	@FindBy(css = ".item-image img:not([alt='cart brand'])")
	WebElement 	imgProductSection;

	@FindBy(css = ".product-list-item div[data-attribute='color']")
	WebElement 	colorAttribute;

	@FindBy (css = ".specialproductsetcomponents.hide-mobile .cart-row.specialproductsetchild")
	WebElement prdSetChildProduct;

	@FindBy(css = ".specialproductsetcomponents.hide-desktop.hide-tablet .cart-row.specialproductsetchild")
	WebElement prdSetChildProductMobile;

	@FindBy(css = ".product-list-item .name a")
	WebElement 	productNameSection;
	
	@FindBy(css = ".product-list-item .name a")
	List<WebElement> ListproductName;

	@FindBy(css = ".product-list-item div[data-attribute='color'] .value")
	WebElement 	productColor;

	@FindBy(css = ".product-list-item div[data-attribute='shoeWidth'] .value")
	WebElement productShoeWidth;

	@FindBy(css = ".product-list-item div[data-attribute='shoeSize'] .value")
	WebElement productShoeSize;
	
	@FindBy(css = ".bonus-product-item .Width .selected-value")
	WebElement bonusProductShoeSize;
	
	@FindBy(css = ".Width .selecting.attr-size span")
	WebElement selectedShoeWidth;
	
	@FindBy(css = "div.order-totals-table")
	WebElement divCartSummary;
	
	@FindBy(css = "div.order-totals-table div div")
	List<WebElement> lstOrderElements;

	@FindBy(css = ".cart-footer .button-fancy-large")
	WebElement btnCheckoutNowFooter;

	@FindBy(css = ".product-list-item div[data-attribute='size'] .value")
	WebElement 	productSize;

	@FindBy(css = "#cart-items-form .product-availability-list span:not([class*='label'])")
	WebElement 	productAvailability;
	
	@FindBy(css= ".product-availability-list.attribute .not-available")
	WebElement productNotAvailable;
	
	@FindBy(css = ".availability-msg .not-available")
	WebElement inventoryMsg;

	@FindBy(css = ".cart-brand-logo")
	WebElement 	imgBrandLogo;

	@FindBy(css = ".cart-unit-price")
	WebElement 	productUnitPrice;

	@FindBy(css = ".cart-unit-price .price-standard")
	WebElement 	productMarkDownPrice;

	@FindBy(css = ".cart-unit-price .price-sales")
	List<WebElement> productUnitSalesPrice;
	
	@FindBy(css = ".cart-row .item-total")
	List<WebElement> productPriceTotal;

	@FindBy(css = ".item-details")
	WebElement productDetails;
	
	@FindBy(css = ".item-details")
	List<WebElement> lstProductDetails;
	
	@FindBy(css = ".item-total")
	WebElement productTotalPrice;

	@FindBy(css = ".custom-cart-update")
	WebElement productQuantityColumn;

	@FindBy(css = ".item-total .price-total-strikeoff")
	WebElement productItemTotalMarkDown;

	@FindBy(css = ".item-total .price-total")
	WebElement productItemTotalRegular;

	@FindBy(css = ".item-edit-details.btm-space a")
	WebElement lnkEdit;

	@FindBy(css = ".hide-desktop.btm-space")
	WebElement lnkEdit_Tablet_Mobile;

	@FindBy(css = ".sku span")
	WebElement 	txtproductId;
	
	@FindBy(css = ".sku .value")
	List<WebElement> lstProductSku;

	@FindBy(xpath = ".//*[@class='column col-1']/div[@class='item-details']/div[@class='product-list-item']/div[@data-attribute='size']/span[2]")
	WebElement 	spanProductSize;

	@FindBy(xpath = ".//*[@class='column col-1']/div[@class='item-details']/div[@class='product-list-item']/div[@data-attribute='color']/span[2]")
	WebElement 	spanProductColor;

	@FindBy(xpath = ".//*[@class='column col-1']/div[@class='item-details']/div[@class='product-availability-list attribute']/span[2]")
	WebElement 	spanProductAvailability;

	@FindBy(css = ".cart-unit-price.attribute .price-sales")
	WebElement 	spanProductPrice;

	@FindBy(css = ".cart-unit-price.attribute")
	WebElement productPriceAttribute;
	
	@FindBy(css = ".cart-columns>.columns-combine .custom-cart-update .qty-decrease.disabled")
	WebElement 	divDecreaseArrowDeskAndTab;

	@FindBy(css = ".cart-columns>.columns-combine .custom-cart-update .qty-increase.disabled")
	WebElement 	divIncreaseArrowDeskAndTab;

	@FindBy(css = ".cart-columns>.columns-combine .custom-cart-update .qty-decrease:not(.disabled)")
	WebElement 	divDecreaseArrowEnabledDeskAndTab;

	@FindBy(css = ".cart-columns>.columns-combine .custom-cart-update .qty-increase:not(.disabled)")
	WebElement 	divIncreaseArrowEnabledDeskAndTab;
	
	@FindBy(css = ".cart-columns .col-1 .custom-cart-update .qty-decrease.disabled")
	WebElement 	divDecreaseArrowMob;

	@FindBy(css = ".cart-columns .col-1 .custom-cart-update .qty-increase.disabled")
	WebElement 	divIncreaseArrowMob;

	@FindBy(css = ".cart-columns .col-1 .custom-cart-update .qty-decrease:not(.disabled)")
	WebElement 	divDecreaseArrowEnabledMob;

	@FindBy(css = ".cart-columns .col-1 .custom-cart-update .qty-increase:not(.disabled)")
	WebElement 	divIncreaseArrowEnabledMob;

	@FindBy(xpath = ".//*[@class='cart-columns']/div[@class='column col-1']/div[@class='item-details']/div[@class='cart-unit-price attribute']/span[2]")
	List<WebElement> productPrice;

	@FindBy(css = ".price-unadjusted")
	List<WebElement> totalProductPriceMarkdown;

	@FindBy(css = ".item-total .price-total")
	List<WebElement> totalProductPrice;

	@FindBy(css = ".item-quantity .input-text")
	List<WebElement> listProductQuantity;

	@FindBy(css = ".cart-columns>.col-4 .item-edit-details.btm-space a")
	List<WebElement> listEditlnkDesktop;
	
	@FindBy(css = ".cart-columns>.col-4 .hide-desktop.btm-space a")
	List<WebElement> listEditlnkTablet;

	@FindBy(css = ".cart-columns>.col-1 .hide-desktop.btm-space a")
	List<WebElement> listEditlnkMobile;
	
	@FindBy(css = ".btm-space.quickorder-item-edit a")
	List<WebElement> lstCQOEditlink;	
	
	@FindBy(css = ".btm-space.quickorder-item-edit a")
	WebElement CQOEditlink;	
	
	@FindBy(css = ".add-to-wishlist")
	List<WebElement> listWishlistLink;

	@FindBy(css = Recently_Viewed_Section)
	WebElement recentlyViewedSection;

	@FindBy(css = Recently_Viewed_Section + " .p-image")
	List<WebElement> recentlyViewedProdImage;

	@FindBy(css = Recently_Viewed_Section + " .product-name")
	List<WebElement> recentlyViewedProdName;

	@FindBy(css = ".search-result-items .product-name")
	WebElement txtProductNameRecentlyView;

	@FindBy(css = Recently_Viewed_Section + " .product-tile")
	WebElement recentlyViewedProdTile;
	
	@FindBy(css = Recently_Viewed_Section + " .product-tile")
	List<WebElement> lstRecentlyViewedProdTile;
	
	@FindBy(css = Recently_Viewed_Section + " .product-pricing")
	List<WebElement> recentlyViewedProdPricing;

	@FindBy(css = Recently_Viewed_Section + " .slick-prev.slick-arrow")
	WebElement recentlyViewSectionPrevArrow;

	@FindBy(css = Recently_Viewed_Section + " .slick-next.slick-arrow")
	WebElement recentlyViewSectionNextArrow;

	@FindBy(css = Recently_Viewed_Section + " .slick-prev.slick-arrow.slick-disabled")
	WebElement recentlyViewSectionPrevDisabledArrow;

	@FindBy(css = Recently_Viewed_Section + " .slick-next.slick-arrow.slick-disabled")
	WebElement recentlyViewSectionNextDisabledArrow;

	@FindBy(css = ".last-visited .slick-list.draggable .slick-slide.slick-active")
	List<WebElement> recentlyViewedCurrentProd;

	@FindBy(css = ".last-visited .slick-slide.slick-active .grid-tile .product-name")
	List<WebElement> recentlyViewedCurrentProdName;

	@FindBy(css = ".in-wishlist")
	List<WebElement> listWishlistLinkAdded;

	@FindBy(css = ".inner-block .login-box.login-account")
	WebElement divLoginPanel;

	@FindBy(css = ".cart-coupon-code button")
	WebElement btnPromoCouponApply;

	@FindBy(css = "#cart-table > .cart-columns")
	WebElement divCartColumnHeaders;
	
	@FindBy(css = "#cart-table >.cart-columns.hide-desktop.hide-tablet")
	WebElement divCartColumnHeadersMobile;

	@FindBy(css = ".cart-row")
	List<WebElement> divCartRow;

	@FindBy(css = ".cart-row")
	WebElement divSingleCartRow;

	@FindBy(css = ".cart-row")
	WebElement divCartEmpty;

	@FindBy(css = ".account-logout a")
	WebElement 	lnkSignOut; 

	@FindBy(css = ".account-links .Wishlist")
	WebElement 	lnkWishlist; 

	@FindBy(css = ".option-update button")
	WebElement 	removeWishlist; 

	@FindBy(css = ".price-unadjusted")
	WebElement totalProductPriceUnAdjusted;

	@FindBy(css = ".cart-unit-price.attribute")
	List<WebElement> divPriceAttribute;

	@FindBy(css = ".personalized-message")
	List<WebElement> divPersonalizedMessage;

	@FindBy(css = ".personalized-message")
	WebElement txtPersonalizedMessage;

	@FindBy(css = ".giftcard.from")
	List<WebElement> divGiftcartFrom;

	@FindBy(css = ".gift-Card")
	WebElement prdRowGiftCard;

	@FindBy(css = ".cart-empty")
	WebElement emptyCart;
	
	@FindBy(css = ".cart-empty .content-asset")
	WebElement emptyCartTile;
	
	@FindBy(css = ".cart-empty .content-asset h1")
	WebElement emptyShoppingHeader;
	
	@FindBy(css = ".cartempty-message")
	WebElement emptyCartMsg;

	@FindBy(css = ".gift-Card .giftcard.from")
	WebElement divProductGiftcartFrom;

	@FindBy(css = ".gift-Card .giftcard.from .value")
	WebElement divProductGiftcartFromValue;

	@FindBy(css = ".gift-Card .giftmessage")
	WebElement txtProductGiftcartMessage;

	@FindBy(css = ".giftcard.to")
	List<WebElement> divGiftcartTo;

	@FindBy(css = ".gift-Card .giftcard.to")
	WebElement txtGiftcartTo;

	@FindBy(css = ".gift-Card .giftcard.to .value")
	WebElement txtGiftcartToValue;

	@FindBy(css = "select[id='Quantity']")
	WebElement drpQty;

	@FindBy(css = ".shopping-bag-id .pid")
	WebElement lblCheckoutID;

	@FindBy(css = ".item-image img[alt='cart brand']")
	WebElement productBrandImg;
	
	@FindBy(css = ".item-image a")
	List<WebElement> productLink;

	@FindBy(css = ".name>a")
	List<WebElement> productNameDesktop;

	@FindBy(css = ".shippingoverlay-link")
	WebElement txtSeeDetails;
	
	@FindBy(css = ".shipping-overlay .shipping-method-cost .value")
	WebElement txtShippingPriceInTooltip;
	
	@FindBy(css = ".shipping-overlay .ui-dialog-content .discount .value")		
	WebElement discountPriceToolTip;
	
	@FindBy(css = ".cartcoupon .value")
	WebElement txtCouponApplied;
	
	@FindBy(css = ".qty-error-msg")
	WebElement txtQtyMergeMsg;

	@FindBy(css = ".you-may-like.loaded .product-tile")
	List<WebElement> productTile;

	@FindBy(css = ".you-may-like.loaded .product-tile a.name-link")
	List<WebElement> productTileLink;

	@FindBy(css = ".rowcoupons")
	WebElement divAppliedCoupon;
	
	@FindBy(css = ".discount-item-details .see-details:not(.opened)")
	WebElement txtCouponSeeDetails;

	@FindBy(css = ".rowcoupons .see-details.opened")
	WebElement lnkHideCpnDetails;

	@FindBy(css = ".price-adjusted-total")
	WebElement txtUpdatedSubTotal;

	@FindBy(css = ".cart-columns>.columns-combine .price-total-strikeoff")
	WebElement itemSubtotalStrikeoff;
	
	@FindBy(css = ".item-details-and-qty .price-total-strikeoff")
	WebElement itemSubtotalStrikeoffMobile;

	@FindBy(css = ".cart-columns>.columns-combine .price-total")
	WebElement itemSubtotalRegular;
	
	@FindBy(css = ".item-details-and-qty .price-total")
	WebElement itemSubtotalRegularMobile;

	@FindBy(css = ".item-total")
	WebElement txtUnitPrice;

	@FindBy(css = ".promo-adjustment")
	WebElement promotionCalloutMessage;

	@FindBy(css = ".button-fancy-large.all-set-button.add-all-to-cart")
	WebElement btnUpdateCartEditOverlay;

	@FindBy(css = ".discount-details.hide")
	WebElement txtDiscountDetails;

	@FindBy(css = ".qty-increase.qty-arrows.disabled")
	WebElement btnQtyArrowUpDisabled;

	@FindBy(css = ".qty-decrease.qty-arrows.disabled")
	WebElement btnQtyArrowDownDisabled;

	@FindBy(css = ".column.col-1 .product-list-item .sku .value")
	WebElement txtProductSetID;
	
	@FindBy(css = ".column.col-1 .product-list-item .sku .value")
	List<WebElement> txtProductIDs;

	@FindBy(css = ".cart-unit-price.attribute .price-sales")
	WebElement txtProductPrice;

	@FindBy(css = "button[value='Remove'] span")
	WebElement btnRemoveInCartPage;

	@FindBy(css = ".cart-columns>.columns-combine .custom-cart-update input")
	WebElement qtyDropDownInCart;
	
	@FindBy(css = ".cart-columns>.column.col-1 .custom-cart-update input")
	WebElement qtyDropDownInCartMobile;
	
	@FindBy(css = ".cart-columns>.columns-combine .custom-cart-update input")
	List<WebElement> qtyDropDownsInCart;
	
	@FindBy(css = ".cart-columns>.column.col-1 .custom-cart-update input")
	List<WebElement> qtyDropDownsInCartMobile;

	@FindBy(css = ".item-quantity")
	WebElement sectionQtyDropDown;
	
	@FindBy(css = ".item-quantity")
	List<WebElement> sectionQtyDropDownLst;

	@FindBy(css = "#primary .login-box.login-account ")
	WebElement readyElementSignInPage;

	@FindBy(css = ".cart-footer .checkout-paypal-btn")
	WebElement btnPaypal;
	
	@FindBy(css = ".cart-top-banner .checkout-paypal-btn")
	WebElement btnTopBannerPaypal;

	@FindBy(css = ".pt_checkout")
	WebElement readyElementCheckoutPage;
	
	@FindBy(css = ".pt_checkout .checkoutlogin")
	WebElement readyElementCheckoutSigninPage;

	@FindBy(css = ".cart-plcc-right>h1")
	WebElement headingPLCCcontentslot;	

	@FindBy(css = ".cart-plcc-section.active")
	WebElement divPLCCcontentslotExpanded;

	@FindBy(css = ".cart-plcc-section")
	WebElement divPLCCcontentslotMobileTablet;

	@FindBy(css = ".expand-close")
	WebElement expandclosePLCCcontentslot;

	@FindBy(css = ".cart-plcc-right .learn-more")
	WebElement learnmorelinkPlcc;	

	@FindBy(css = ".cart-plcc-right b")
	WebElement creditlimitPlcc;	

	@FindBy(css = ".cart-plcc-left.hide-mobile")
	WebElement logoPlccBrand;	

	@FindBy(css = ".cart-plcc-right>span.text")
	WebElement textPLCCcontentslot;

	@FindBy(css = ".cart-plcc-right .hide")
	WebElement textPLCCcontentslotExpanded;	

	@FindBy(css = ".bonus-item-actions .select-bonus")
	WebElement btnBonusProductAdd;	

	@FindBy(css = ".bonus-product-list")
	WebElement bonusProductOverlay;	

	@FindBy(css = ".cart-promo.bonus-product-calloutMsg .bonus-item-details")
	WebElement bonusCalloutMessageDisplay;	

	@FindBy(css = ".bonus-check")
	List<WebElement> chkBonusProductSelection;

	@FindBy(css = ".add-to-cart-bonus")
	WebElement btnBonusProductAddToBag;	

	@FindBy(css = ".cart-columns>.columns-combine .item-total .bonus-item")
	WebElement bonusItemPriceDeskAndTab;
	
	@FindBy(css = ".item-details-and-qty .item-total .bonus-item")
	WebElement bonusItemPriceMob;

	@FindBy(css = ".qty-increase.qty-arrows.disabled")
	WebElement qtyIncreaseArrowDisabled;

	@FindBy(css = ".qty-decrease.qty-arrows.disabled")
	WebElement qtyDecreaseArrowDisabled;

	@FindBy(css = ".bonus-item-actions.item-user-actions .btm-space .add-to-wishlist")
	WebElement btnBonusAddToWishlist;

	@FindBy(css = ".bonus-item-actions.item-user-actions .item-edit-details.btm-space a")
	WebElement btnBonusEdit;	

	@FindBy(css = ".item-user-actions")
	WebElement divUserActionItems;

	@FindBy(css = ".cart-message")
	WebElement txtPromoHeading;

	@FindBy(css = "input[id='dwfrm_cart_couponCode']")
	WebElement txtPromoCode;

	@FindBy(css = "button[id='add-coupon']")
	WebElement btnApplycoupon;

	@FindBy(css = ".discount-item-details .discount")
	WebElement txtpromotionCalloutMsg;

	@FindBy(css = ".cart-right-section .rw-error.error.hide")
	WebElement txtRewardCertificateError;

	@FindBy(css = ".cart-left-section .rw-error.error.hide")
	WebElement txtRewardCertificateErrorMobile;

	@FindBy(css = ".discount-details.hide")
	WebElement txtPromotionDescription;

	@FindBy(css = ".cart-coupon-code a")
	WebElement couponToolTip;

	@FindBy(css = ".tips")
	WebElement couponToolTipContent;

	@FindBy(css = ".cart-coupon-code input")
	WebElement promoInputfield;
	
	@FindBy(css = ".rowcoupons .not-applied")
	WebElement txtCpnNotApplied;
	
	@FindBy(css = "#coupon-submitted .error-messages")
	WebElement txtCpnError;
	
	@FindBy(css = ".rowcoupons .discount-details")
	WebElement divDiscountDetails;

	@FindBy(css = ".textbutton")
	WebElement btnRemoveButton;

	@FindBy(css = ".textbutton")
	List<WebElement> btnCouponRemoveButton;

	@FindBy(css = ".cartcoupon.clearfix .value")
	WebElement appliedCouponRow;

	@FindBy(css = ".cart-right-section .rowcoupons")
	List<WebElement> coupounAppliedCount;

	@FindBy(css = ".cart-right-section div[class='error']")
	WebElement txtcouponError;

	@FindBy(css = ".error-messages.hide-desktop.hide-tablet .error:not(.rw-error)")
	WebElement txtcouponErrorMobile;

	@FindBy(css = ".ui-button-icon.ui-icon.ui-icon-closethick")
	WebElement btnCouponToolTipClose;

	@FindBy(css = ".shippingoverlay-link")
	WebElement spanCostToolTip;

	@FindBy(css = ".order-shipping-discount.discount.order-detail")
	WebElement divShippingDiscount;

	@FindBy(css = ".order-sales-tax.order-detail .value")
	WebElement txtSalesTaxValue;

	@FindBy(css = ".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.shipping-overlay.ui-draggable")
	WebElement divShoppingCostToolTip;

	@FindBy(css = ".order-total.order-detail .label")
	WebElement divRemainingTotal;

	@FindBy(css = ".order-discount.discount.order-detail .value")
	WebElement divDicountTotal;

	@FindBy(css = ".order-shipping-discount.discount.order-detail .value")
	WebElement divShippingDicountTotal;

	@FindBy(css = ".order-value.value")
	WebElement divOrderTotal;

	@FindBy(css = ".order-saving.discount.order-detail .order-saving.value")
	WebElement divTotalSavings;

	@FindBy(css = ".quick-order-badge.hide-mobile .quick-order-badge-link")
	WebElement quickOrderCatalogBagde;

	@FindBy(css = ".quick-order-badge.hide-desktop.hide-tablet .quick-order-badge-link")
	WebElement quickOrderCatalogBagdeMobile;

	@FindBy(css = ".item-image img:nth-child(2)")
	List<WebElement> quickOrderbrandCount;

	@FindBy(css = ".cart-promo.bonus-product-calloutMsg")
	WebElement lblBonusProductCallout;

	@FindBy(css = ".select-bonus")
	WebElement lnkAddBonusProduct;

	@FindBy(css = "#QuickViewDialog")
	WebElement popUpBonusProduct;
	
	@FindBy(css = "#bonus-product-dialog")
	WebElement choiceOfBonusProduct;

	@FindBy(css = ".hide-desktop.hide-tablet .quick-order-badge-link .heading")
	WebElement txtCatalogQuickOrder_Mobile;

	@FindBy(css = ".cart-columns>.quick-order-item .quick-order-badge-link .heading")
	WebElement txtCatalogQuickOrder_Desk_Tab;

	@FindBy(css = ".quick-view .ui-dialog-titlebar-close .ui-icon")
	WebElement btnEditOverlayClose;
	
	@FindBy(css = ".bonus-product-item .attribute.color")
	WebElement sectionBonusColorSwatch;
	
	@FindBy(css = ".bonus-product-item .attribute.size")
	WebElement sectionBonusSizeSwatch;
	
	@FindBy(css = ".bonus-product-item .attribute.Size.Family")
	WebElement sectionBonusSizeFamilySwatch;
	
	@FindBy(css = ".bonus-product-item .attribute.Band.Size")
	WebElement sectionBonusBandSizeSwatch;
	
	@FindBy(css = ".cart-row")
	List<WebElement> lastCartProducts;
	
	@FindBy(css = ".cart-row:not(.bonus-product)")
	List<WebElement> lstCartRegularProducts;

	@FindBy(css = ".bonus-product-item")
	List<WebElement> bonusProducts;

	@FindBy(css = ".bonus-check")
	List<WebElement> checkbonusProducts;

	@FindBy(css = "div[data-attrinfo='size'] .selecting.attr-size")
	List<WebElement> sizeBonus;

	@FindBy(css = "div[data-attrinfo='sizeFamily'] .selecting.attr-size")
	List<WebElement> bonusSizeFamilySize;

	@FindBy(css = "div[data-attrinfo='shoeSize'] .selecting.attr-size")
	List<WebElement> bonusShoeSize;
	
	@FindBy(css = "div[data-attrinfo='shoeWidth'] .selecting.attr-size")
	List<WebElement> bonusShoeWidth;
	
	@FindBy(css = "div[data-attrinfo='braBandSize'] .selecting.attr-size")
	List<WebElement> bonusBandWidth;
	
	@FindBy(css = "div[data-attrinfo='braCupSize'] .selecting.attr-size")
	List<WebElement> bonusCupWidth;
	
	@FindBy(css = "div[data-attrinfo='shoeSize'] .selecting.attr-size")
	WebElement fstBonusShoeSize;

	@FindBy(css = ".attribute.Width.size_border .selected-value")
	WebElement selectedWidth;
	
	@FindBy(css = ".add-to-cart-bonus.button-fancy-medium")
	WebElement btnAddtoCartBonus;

	@FindBy(css = ".plcc-form-section")
	WebElement divplccLearnmorePage;

	@FindBy(css = ".selecting.attr-color")
	List<WebElement> selectcolortBonus;

	@FindBy(css = ".cart-top-banner")
	WebElement contentBaner;

	@FindBy(css = ".spc-guest-btn.filled-wineberry")
	WebElement btnContinueGuest;

	@FindBy(css = "#dwfrm_billing_billingAddress_email_emailAddress")
	WebElement emailInputField;

	@FindBy(css =  ".shipping-overlay .ui-dialog-content")
	WebElement shippingCostOverlay;

	@FindBy(css =  ".ui-button.ui-corner-all.ui-widget.ui-button-icon-only.ui-dialog-titlebar-close")
	WebElement shippingCostOverlayClose;
	
	@FindBy(css = ".shipping-overlay .shipping-method-cost.order-detail")
	WebElement deliveryMethodInTooltip;
	
	@FindBy(css = ".shipping-overlay .shipping-method-cost.order-detail .value")
	WebElement deliveryMethodPriceInTooltip;
	
	@FindBy(css = ".shipping-overlay .surcharge-product.order-detail")
	WebElement giftCardFeeInTooltip;
	
	@FindBy(css = ".shipping-overlay .shippingtotal.order-detail")
	WebElement totalFeeInTooltip;

	@FindBy(css =  "div[class='dialog-content ui-dialog-content ui-widget-content']  .promo.discount.order-detail")
	WebElement divShippingPromotionDetail;

	@FindBy(css =  "#primary > div.cart-actions.cart-actions-top > div.cart-content > div.cart-left-content > div.cart-footer > div.cart-order-totals > div > div > div.order-shipping-discount.discount.order-detail > span.label")
	WebElement shippingDiscountSection;

	@FindBy(css =  "div.cart-footer")
	WebElement orderSummarySection; 

	@FindBy(css = ".error-form")
	WebElement qtyError;

	@FindBy(css = ".name a")
	List<WebElement> lstCartProductNames;

	@FindBy(css = ".quick-view")
	WebElement viewEditModal;

	@FindBy(css = ".selected .color")
	WebElement modalSelectedColor;

	@FindBy(css = ".selected .size")
	WebElement modalSelectedSize;

	@FindBy(css = ".ui-dialog-titlebar-close")
	WebElement btnCloseEditModal;

	@FindBy (css = ".cart-empty .content-asset .button")
	WebElement btnEmptyWhatsNew;
	
	@FindBy (css = ".cart-empty .content-asset .button a")
	WebElement btnEmptyWhatsNewWWX;

	@FindBy(css = ".slick-track .product-name-image-container .name-link")
	WebElement trendNowImage;

	@FindBy(css = ".cart-empty .slick-list.draggable")
	WebElement trendNowSlickContainer;

	@FindBy(css = ".recommendation-heading h2")
	WebElement trendNowHeading;

	@FindBy(css = Cart_Recommendation)
	WebElement sectionRecommendation;

	@FindBy(css = Cart_Recommendation + " img")
	WebElement prodImageRecommendation;

	@FindBy(css = Cart_Recommendation + " .product-name")
	WebElement prodNameRecommendation;
	
	@FindBy(css = Cart_Recommendation + " .product-pricing")
    List<WebElement> lstProdPriceRecommendation;

	@FindBy(css = Cart_Recommendation + " .product-pricing")
	WebElement prodPriceRecommendation;
	
	@FindBy(css = ".product-name-image-container .product-image")
	WebElement prodImageRecommendationEmptyCart;
	
	@FindBy(css = ".product-name")
	WebElement prodNameRecommendationEmptyCart;
	
	@FindBy(css = ".product-pricing")
	WebElement prodPriceRecommendationEmptyCart;

	@FindBy(css = Cart_Recommendation + " .product-tile")
	List<WebElement> tileListRecommendationProd;

	@FindBy(css = Cart_Recommendation + " .product-tile")
	WebElement tileRecommendationProd;

	@FindBy(css = Cart_Recommendation + " .product-sales-price.product-actual-price")
	WebElement prodStdPriceRecommendation;

	@FindBy(css = Cart_Recommendation + " .product-sales-price:not(.product-actual-price)")
	WebElement prodSalePriceRecommendation;

	@FindBy(css = Cart_Recommendation + " .product-standard-price")
	WebElement prodSalePriceRecommendationStdPrice;

	@FindBy(css = Cart_Recommendation + " img")
	List<WebElement> prodImageListRecommendation;

	@FindBy(css = Cart_Recommendation + " .product-name")
	List<WebElement> prodNameListRecommendation;

	@FindBy(css = ".quick-view")
	WebElement divQuickShop;
	
	@FindBy(css= "a#quickviewbutton")
	WebElement btnQickShop;

	@FindBy(css = ".optioninfo .hemmable")
	WebElement divSpecialMessageHemming;

	@FindBy(css = ".optioninfo .hemmable .optionvalue")
	WebElement divSpecialMessageHemmingValue;
	
	@FindBy(xpath = "//p[contains(text(),'Hemmable')]")
	WebElement divSpecialMessageHemmingValue1;

	@FindBy(css = ".cart-show-basket-id")
	WebElement lnkShowBagID;
	
	@FindBy(css = ".hemmable .optionvalue")
	WebElement cartHemmingValue;
	
	@FindBy(css = ".shopping-bag-id .pid")
	WebElement spanBagID;
	
	@FindBy(css = ".order-value")
    WebElement lblOrderTotal;
	
	@FindBy(css = ".cart-row .column.col-1")
	WebElement cartProductAttribute;
	
	@FindBy(css = ".cart-row .column.col-4")
	WebElement cartProductOptions;

	@FindBy(css = "#crl8-homepage-carousel")
	WebElement curalateModule;

	//================================================================================
	//			WebElements Declaration End
	//================================================================================

	/**
	 * constructor of the class
	 * @param driver - WebDriver
	 */
	public ShoppingBagPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver, miniCartContent, 25);
		if (isPageLoaded && !(Utils.waitForElement(driver, miniCartContent))) {
			Log.fail("Shopping Bag Page didn't open", driver);
		}

		elementLayer = new ElementLayer(driver);

	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	public boolean getPageLoadStatus()throws Exception {
		return isPageLoaded;
	}


	/**
	 * To Get Product Name
	 * @return String of Product Name
	 * @throws Exception - Exception
	 */

	public String getProductName()throws Exception {
		String dataToReturn = new String();
		dataToReturn = BrowserActions.getText(driver, txtProductNameDesktop, "Product Name");
		return dataToReturn;
	}

	/**
	 * To Get Hemming value
	 *  @return String of hemming value
	 * @throws Exception - Exception
	 */
	
	public String getHemmingValue()throws Exception {
		return BrowserActions.getText(driver, cartHemmingValue, "Hemming value");
	}


	/**
	 * To Click on Checkout button in cart page and return checkout page
	 * @return CheckoutPage Page object
	 * @throws Exception - Exception
	 */
	public CheckoutPage clickOnCheckoutNowBtn() throws Exception {
		Utils.waitForElement(driver, btnCheckoutNow);
		BrowserActions.clickOnElementX(btnCheckoutNow, driver, "Checkout Now");
		Utils.waitForPageLoad(driver);
		return new CheckoutPage(driver).get();
	}


	/**
	 * To click on Special product main Image and return pdp page
	 * @param splPrdSetName -
	 * @return PdpPage object
	 * @throws Exception - Exception
	 * 
	 */
	public PdpPage clickOnSplProductMainImg(String splPrdSetName) throws Exception {
		WebElement splProductSetMainImage = 
				driver.findElement(By.cssSelector(".item-image "
						+ "a[title='Go to Product: "+ splPrdSetName +"'] img:not([alt='cart brand'])"));
		BrowserActions.clickOnElementX(splProductSetMainImage, driver, "Update Information");
		return new PdpPage(driver).get();
	}


	/**
	 * To click on Special product Name and return pdpPage
	 * @param splPrdSetName -
	 * @return PdpPage page object
	 * @throws Exception - Exception
	 */
	public PdpPage clickOnSplProductName(String splPrdSetName) throws Exception {
		WebElement splProductSetMainImage = 
				driver.findElement(By.cssSelector(".name a[title='Go to Product: "+ splPrdSetName +"']"));
		BrowserActions.clickOnElementX(splProductSetMainImage, driver, "Update Information");
		return new PdpPage(driver).get();
	}

	/**
	 * To get coupon error message
	 * @return cpnErrorMessage - Coupon error message
	 * @throws Exception - Exception
	 */
	public String getCouponErrorMessage() throws Exception {
		WebElement couponError;
		String cpnErrorMessage;
		if(Utils.isMobile()) {
			couponError = txtCouponErrorMob;
		} else {
			couponError = txtCouponErrorDeskTab;
		}
		Utils.waitForElement(driver, couponError);
		cpnErrorMessage = BrowserActions.getText(driver, couponError, "Coupon error message");
		return cpnErrorMessage;
	}
	
	/**
	 * To apply promo coupon code
	 * @param coupon - coupon to apply
	 * @throws Exception - Exception
	 */
	public boolean applyPromoCouponCode(String coupon) throws Exception {
		Utils.waitForElement(driver, promoInputfield);
		BrowserActions.typeOnTextField(promoInputfield, coupon, driver, "Promotion field");
		BrowserActions.clickOnElementX(btnPromoCouponApply, driver, "Apply");
		if (Utils.waitForElement(driver, txtCpnNotApplied)) {
			clickOnCouponSeeDetails();
			Log.warning("Coupon '"+coupon+"' not in effect:: " + BrowserActions.getText(driver, divDiscountDetails, "Discount details."));
			clickOnCouponHideDetails();
		} else if (Utils.waitForElement(driver, txtCpnNotApplied)) {
			Log.warning("Coupon '"+coupon+"' not applied:: " + BrowserActions.getText(driver, txtCpnNotApplied, "Discount details."));
			return false;
		}
		Utils.waitForPageLoad(driver);
		return true;
	}
	
	/**
	 * To apply promo coupon code
	 * @param coupon - coupon to apply
	 * @throws Exception - Exception
	 */
	public void enterCouponCode(String coupon) throws Exception {
		BrowserActions.typeOnTextField(promoInputfield, coupon, driver, "Promotion field");
	}

	/**
	 * To Remove specific product form cart page
	 * @param productIndex -
	 * @throws Exception - Exception
	 */

	public void removeCartProduct(int productIndex )throws Exception {
		BrowserActions.clickOnElementX(lstBtnRemove.get(productIndex), driver, productIndex + "th Item");
		Log.event(productIndex + "th Item removed from Cart");
		Utils.waitForPageLoad(driver);

	}
	
	
	/**
	 * To remove the products to match the specified quantity.
	 * @param qty - int
	 * @return true - if products quantity matches after removing.
	 * @throws Exception
	 */
	public boolean removeProductstoMatchqty(int qty) throws Exception {
		WebElement divDecreaseArrowEnabled = Utils.isMobile() ? divDecreaseArrowEnabledMob : divDecreaseArrowEnabledDeskAndTab;
		int initialCartQuantity = getTotalQtyInCart();
		if(initialCartQuantity == qty) {
			Log.message("Total number of products are"+qty);
			return true;
		} else if(initialCartQuantity < qty) {
			Log.message("available products are less than the given qty" +qty);			
		} else {
			while((getTotalQtyInCart() > qty)) {
				BrowserActions.clickOnElementX(divDecreaseArrowEnabled, driver, "quantity down arrow");
				Utils.waitForPageLoad(driver);
			}
			if(getTotalQtyInCart() == qty) {
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * Remove product to reach cart qyt as given
	 * @Parameter String
	 * @Return qty after removing
	 */
	public boolean updateProductstoMatchqty(int qty) throws Exception {
		int cartQty = getTotalQtyInCart();
		List<WebElement> dropDownCart = Utils.isMobile() ?  qtyDropDownsInCartMobile : qtyDropDownsInCart;
		
		if (cartQty==qty) {
			Log.message("Total number of products are same"+qty);
			return true;
		} else if(qty>cartQty) {
			Log.message("available products are lessthan the given qty" +qty);			
		} else {
			int product= getProductCountInCart();
			for(int p=0;p<product; p++) {
				String value = BrowserActions.getTextFromAttribute(driver, dropDownCart.get(p), "value", "Qty in Cart"); 
				int prdqty = Integer.parseInt(value);
				int cartqty=cartQty-prdqty;
				if(cartqty<qty && (qty-cartqty <11)) {
					String updateQty=Integer.toString(qty-cartqty);
					updateQuantityByPrdIndex(p,updateQty);
					return true;
				}
			}
			for(int p=0;p<product; p++) {			
				removeCartProduct(p);
				if(cartQty>qty)
					return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * To Remove all items from cart page
	 * @throws Exception - Exception
	 */
	public void removeAllItemsFromCart()throws Exception {
		for(int i = lstBtnRemove.size()-1; i >= 0 ; i--) {
			BrowserActions.clickOnElementX(lstBtnRemove.get(i), driver, (i+1) + "th Item's Remove Button");
			Log.event((i+1) + "th Item removed from Cart");
			Utils.waitForPageLoad(driver);
		}
	}

	/**
	 * To remove all item from cart page except some product
	 * @param except - 
	 * @return true - if all items are removed from cart
	 * @throws Exception -
	 */
	public boolean removeAllItemsFromCartExcept(String... except)throws Exception {
		List<String> productSku = getProductSkuList();
		boolean statusToReturn = false;
		int l = 0;
		if(!productSku.isEmpty()) {
			if(btnDeleteErrProd.size() > 0) {
				int size = btnDeleteErrProd.size();
				for(int i=size-1; i>=0; i--) {
					BrowserActions.clickOnElementX(btnDeleteErrProd.get(i), driver, "Error Item's Remove Button");
				}
			}
	
			for(int i = lstBtnRemove.size()-1, j = 0, k = 0; i >= j ; i--,k++) {
				WebElement deleteBtn = driver.findElement(By.xpath("//div[@class='sku']//span[contains(text(),'" + productSku.get(k) + "')]//ancestor::div[contains(@class,'cart-columns')]//button[contains(@name,'delete')]"));
				if(l < except.length) {
					if(!except[0].contains(productSku.get(k))) {
						BrowserActions.clickOnElementX(deleteBtn, driver, productSku.get(k) + " Item's Remove Button");
						Utils.waitForPageLoad(driver);
					}else
						l++;
				}else {
					BrowserActions.clickOnElementX(deleteBtn, driver, productSku.get(k) + " Item's Remove Button");
					Utils.waitForPageLoad(driver);
				}
			}
		} else {
			return statusToReturn;
		}

		if(l != except.length)
			statusToReturn = true;

		return statusToReturn;
	}

	/**
	 * To Verify product quantity
	 * @param proQuantity - quantity needed to verify
	 * @param index - product index
	 * @return true - if product quantity is equal
	 * @throws Exception - Exception
	 */
	public boolean verifyProductQuantity(int proQuantity, int index)throws Exception {
		boolean flag=false;
		Utils.waitForElement(driver, divItemQuantity);
		int productQuantity=Integer.parseInt(valueItemQuantity.get(index).getAttribute("value").trim());
		if(productQuantity==proQuantity) {
			flag=true;
		}else {
			flag=false;
		}
		return flag;
	}


	/**
	 * To click on Edit link for special product
	 * @return PdpPage page object
	 * @throws Exception - Exception
	 */
	public PdpPage clickOnSplProductEdit() throws Exception {
		Utils.waitForPageLoad(driver);
		if(!(Utils.isDesktop())) {
			BrowserActions.clickOnElementX(btnEditDetails_Mobile_Tablet, driver, "Edit qty");
			return new PdpPage(driver);
		} else {
			BrowserActions.clickOnElementX(btnEditDetails, driver, "Edit qty");
		}
		return null;
	}


	/**
	 * To click special product remove button
	 * @throws Exception - Exception
	 */
	public void clickOnSplProductRemove() throws Exception {
		BrowserActions.clickOnElementX(btnRemoveInCartPage, driver, "Update Information");
	}


	/**
	 * To click on Update cart special product edit overlay
	 * @throws Exception - Exception
	 */
	public void clickOnUpdateCartSplProdEditOverlay() throws Exception {
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, btnUpdateCartEditOverlay)) {
			Actions action = new Actions(driver);
			action.moveToElement(btnUpdateCartEditOverlay);
			BrowserActions.clickOnElementX(btnUpdateCartEditOverlay, driver, "Update Information");
		}
		Utils.waitForPageLoad(driver);
	}


	/**
	 * To get product quantity form cart
	 * @return int - value of quantity
	 * @throws Exception - Exception
	 */
	public int getQtyInCart() throws Exception {
		int qty = 0;
		WebElement dropDownCart = Utils.isMobile() ?  qtyDropDownInCartMobile : qtyDropDownInCart;
		Utils.waitForPageLoad(driver);
		try {
			String value = BrowserActions.getTextFromAttribute(driver, dropDownCart, "value", "Qty in Cart"); 
			qty = Integer.parseInt(value);
		} catch (Exception e) {
			Log.event("Cart is empty, returning 0.");
		}
		return qty;
	}
	
	/**
	 * To get Shipping Price form cart
	 * @return int - value of price
	 * @throws Exception - Exception
	 */
	public double getShippingPrice() throws Exception {
		String value = BrowserActions.getText(driver, divShippingMessagePrice, "Shipping price in Cart");
		return Double.valueOf(value.replace("$", "").trim());
	}
	
	/**
     * To get Sales tax form cart
     * @return double - value of price
     * @throws Exception - Exception
     */
    public double getSalesTax() throws Exception {
        String value = BrowserActions.getText(driver, txtSalesTaxValue, "Sales tax in Cart"); 
        double amount=0;
        if(value.contains("-")) {
        	return amount;
        } else {
        	amount = Double.valueOf(value.replace("$", "").trim());
        	return amount;
        }
    }
	
	/**
	 * To get the surcharge amount from cart Page
	 * @return double
	 * @throws Exception
	 */
	public double getShippingMethodPrice() throws Exception {
		double amount=0;
		clickOnShippingCostTool();
		if(Utils.waitForElement(driver, shippingCostOverlay)) { 
			String value = BrowserActions.getText(driver, deliveryMethodPriceInTooltip, "Surcharge value").replace("$","");
			amount = Double.parseDouble(value); 
		}
		closeShippingOverlayToolTip();
		return amount;
	}
	
	/**
	 * To get order shipping discount in cart
	 * @return double - shipping discount
	 * @throws Exception - Exception
	 */
	public double getOrderShippingDiscount() throws Exception {
		if (Utils.waitForElement(driver, divDiscountMessagePrice)) {
			String value = BrowserActions.getText(driver, divDiscountMessagePrice, "Shipping discount in Cart").replace("-", "");
			return Double.valueOf(value.replace("$", "").trim());
		} else {
			return 0d;
		}
	}

	/**
	 * To get total product quantity form cart
	 * @return int - value of quantity
	 * @throws Exception - Exception
	 */
	public int getTotalQtyInCart() throws Exception {
		int qty = 0;
		List<WebElement> dropDownCart = Utils.isMobile() ?  qtyDropDownsInCartMobile : qtyDropDownsInCart;
		Utils.waitForPageLoad(driver);
		try {
			for (int prd=0;prd<dropDownCart.size();prd++) {
				String value = BrowserActions.getTextFromAttribute(driver, dropDownCart.get(prd), "value", "Qty in Cart"); 
				qty =qty+ Integer.parseInt(value);
			}
		} catch(Exception e) {
			Log.event("Cart is empty, returning 0.");
		}
		return qty;
	}
	
	/**
	 * To select quantity in Edit overlay
	 * @param qty - Quantity
	 * @throws Exception - Exception
	 */

	public void selectQtyInEditOverlay(int qty) throws Exception {
		BrowserActions.scrollToViewElement(btnEditOverlayQty, driver);
		Select qtySelect = new Select(btnEditOverlayQty);
		if (qty > 0) {
			String value = Integer.toString(qty);
			qtySelect.selectByValue(value);
		} else {
			qtySelect.selectByIndex(0);
		}
	}

	/**
	 * To Verify cart for merged product
	 * @param qnt1 -
	 * @param qnt2 -
	 * @return true - if quantity is equl
	 * @throws Exception - Exception
	 */
	public boolean verifyMergedProduct(int qnt1,int qnt2)throws Exception {
		boolean flag=false;
		if(qnt1==Integer.parseInt(txtItemQuantity.getAttribute("value")) && qnt2==Integer.parseInt(txtItemSecondQuantity.getAttribute("value"))) {
			flag=true;
		}else {
			flag=false;
		}

		return flag;
	}
	
	/**
	 * To verify if if list of products is contained in another list
	 * @param listToContain - list to verify
	 * @param mergedCart - list to verify against
	 */
	public boolean verifyMergedCartContainsProductList(HashSet<String> mergedCart, HashSet<String> listToContain) throws Exception{
		if(listToContain.size() >  mergedCart.size()) {
			return false;
		}
		
		List<String> tempMissingElements = new LinkedList<String>();
		for(String elementInner : listToContain) {
			if(!mergedCart.contains(elementInner)) {
				tempMissingElements.add(elementInner);
			}
		}
		for(String elementInner : listToContain) {
			for(String elementOuter : mergedCart) {
				if (tempMissingElements.size() == 0) {
					return true;
				}
				if(elementInner.contains(elementOuter) || elementOuter.contains(elementInner)) {
					tempMissingElements.remove(elementInner);
				}
			}
		}
		
		return (tempMissingElements.size() == 0);
	}
	
	/**
	 * To verifyElementDisplayBelow
	 * @param firstElement -
	 * @param secondElement -
	 * @param obj -
	 * @return true - if element displayed below
	 * @throws Exception - Exception
	 */
	public boolean verifyElementDisplayedBelow(String firstElement,String secondElement, Object obj )throws Exception {

		boolean flag=false;
		Field f1 = obj.getClass().getDeclaredField(firstElement);
		Field f2 = obj.getClass().getDeclaredField(secondElement);
		f1.setAccessible(true);
		f2.setAccessible(true);
		WebElement firstEle = ((WebElement) f1.get(obj));
		WebElement secondEle = ((WebElement) f2.get(obj));

		if(firstEle.getLocation().getY() <secondEle.getLocation().getY())
			flag = true;

		return flag; 
	}

	/**
	 * To Verify Alert message
	 * @param message -
	 * @return true - if alert message present 
	 * @throws Exception - Exception
	 */
	public boolean verifyAlertMessage(String message)throws Exception {
		boolean flag=false;
		if(message==divAlertSection.getText())
			flag=true;

		return flag;
	}
	
	/**
	 * To verify product Added In cart page
	 * @param prdAddedInQuickOrder -
	 * @return true - if product added to cart
	 * @throws Exception - Exception
	 */
	public boolean verifyProductAddedInCart(String prdAddedInQuickOrder)throws Exception {
		String prdName;
		boolean status = false;

		Log.event("Prd name from User :: '" + prdAddedInQuickOrder + "'");
		for(int i=0; i< productNameDesktop.size(); i++) {
			prdName = BrowserActions.getText(driver, productNameDesktop.get(i), "Product Name").trim();

			Log.event("Prd name from Site :: '" + prdName + "'");
			if(prdAddedInQuickOrder.equalsIgnoreCase(prdName.trim())) {
				status = true;
				break;
			}				
		}
		return status;

	}

	/**
	 * To click on Shipping cost tool tip
	 * @throws Exception - Exception
	 */
	public void clickOnShippingCostTool() throws Exception {
		BrowserActions.clickOnElementX(txtSeeDetails, driver, "Shipping cost tool");
	}
	
	/**
	 * To click on International Shipping link
	 * @throws Exception - Exception
	 */
	public void clickOnInternationalShippingLink() throws Exception {
		BrowserActions.clickOnElementX(lnkInternationalShipping, driver, "Intenational shipping link");
		Utils.waitForElement(driver, modalInternationalShipping);
	}
	
	/**
	 * To get the product names in the International Exclusion modal
	 * @return list<String>
	 * @throws Exception 
	 */
	public List<String> getproductNamesInInternationalExclusion() throws Exception {
		List<String> prdNames=new ArrayList<String> ();
		for(WebElement prdName:prdNameInInternationalExclusion) {
			prdNames.add(BrowserActions.getText(driver, prdName, "prodct name").toLowerCase());			
		}		
		return prdNames;
	}
	
	/**
	 * To click on International Shipping link
	 * @throws Exception - Exception
	 */
	public void clickOnRemoveItemBtnInternationalExclusion() throws Exception {
		BrowserActions.clickOnElementX(btnInternationalExcRemoveItem, driver, "International Exclusion");
		Utils.waitForElement(driver, modalInternationalShipping);
	}
	
	
	/**
	 * To click on International Shipping Continue
	 * @throws Exception - Exception
	 */
	public iParcelCheckoutPage clickOnContinueInInternationalShipping() throws Exception {
		BrowserActions.scrollInToView(btnIntrnShippingContinue, driver);
		BrowserActions.clickOnElementX(btnIntrnShippingContinue, driver, "Shipping cost tool");
		return new iParcelCheckoutPage(driver).get();
	}
	
	/**
	 * To click on International Shipping link
	 * @throws Exception - Exception
	 */
	public void selectConutryOnInternationalShipping(int index) throws Exception {
		BrowserActions.selectDropdownByIndex(selectIntnShipCountry, index);
	}
	
	
	/**
	 * To get list of product id from recommendation
	 * @param noOfIdNeeded - No of product needed
	 * @return String[] - Product id array
	 * @throws Exception - Exception
	 */
	public String[] getListOfRecommendationProductId(int noOfIdNeeded)throws Exception {
		HashSet<String> ProductId = new HashSet<String> ();
		int i=0;
		while (ProductId.size() != noOfIdNeeded) {
			WebElement prdId = productTile.get(i);
			ProductId.add(BrowserActions.getTextFromAttribute(driver, prdId, "data-itemid", "product"));
			i++;
		}
		return ProductId.toArray(new String[0]);
	}

	/**
	 * To get product url
	 * @param prodId - No of product urls needed
	 * @return String[] - product url array
	 * @throws Exception - Exception
	 */
	public String[] getProductUrl(int noOfLinkNeeded)throws Exception {
		HashSet<String> ProductLink = new HashSet<String> ();
		int i=0;
		while (ProductLink.size() != noOfLinkNeeded) {
			WebElement prdLink = productTileLink.get(i);
			ProductLink.add(BrowserActions.getTextFromAttribute(driver, prdLink, "href", "product"));
			i++;
		}
		return ProductLink.toArray(new String[0]);
	}

	/**
	 * To Show Coupon Details 
	 * @throws Exception - Exception
	 */
	public void clickOnCouponSeeDetails() throws Exception {
		BrowserActions.clickOnElementX(txtCouponSeeDetails, driver, "See Coupon Details");
	}
	
	/**
	 * To hide Coupon Details 
	 * @throws Exception - Exception
	 */
	public void clickOnCouponHideDetails() throws Exception {
		BrowserActions.clickOnElementX(lnkHideCpnDetails, driver, "Hide coupon details");
	}

	/**
	 * To click on Updated Subtotal
	 * @throws Exception - Exception
	 */
	public void clickOnUpdatedSubtotal() throws Exception {
		BrowserActions.clickOnElementX(txtUpdatedSubTotal, driver, "Updated Subtotal");
	}


	/**
	 * To click on product Name
	 * @throws Exception - Exception
	 */
	public void clickOnProductName() throws Exception {
		BrowserActions.clickOnElementX(productNameDesktop.get(0), driver, "Product name");
	}
	
	/**
	 * To click on product Name by given index
	 * @throws Exception - Exception
	 */
	public void clickOnProductNameByID(String prdID) throws Exception {
		WebElement prdToClick = getCartProductByID(prdID);
		try {
			BrowserActions.clickOnElementX(prdToClick.findElement(By.cssSelector(".name>a")), driver, "Product name");
		} catch(NoSuchElementException | ArrayIndexOutOfBoundsException e) {
			Log.event("Product name element based on index not found");
		}
	}

	/**
	 * To click on get updted Sub Total
	 * @return double - price value
	 * @throws Exception - Exception
	 */
	public double getUpdatedSubtotalPrice() throws Exception {	
		String subtotal = null;
		if (Utils.isMobile()) {
			if (Utils.waitForElement(driver, itemSubtotalStrikeoffMobile)) {
				subtotal = BrowserActions.getText(driver, itemSubtotalStrikeoffMobile, "Subtotal");
			} else if (Utils.waitForElement(driver, itemSubtotalRegularMobile)) {
				subtotal = BrowserActions.getText(driver, itemSubtotalRegularMobile, "Subtotal");
			} else {
				subtotal = BrowserActions.getText(driver, txtUpdatedSubTotal, "Subtotal");
			}
		} else {
			if (Utils.waitForElement(driver, itemSubtotalStrikeoff)) {
				subtotal = BrowserActions.getText(driver, itemSubtotalStrikeoff, "Subtotal");
			} else if (Utils.waitForElement(driver, itemSubtotalRegular)) {
				subtotal = BrowserActions.getText(driver, itemSubtotalRegular, "Subtotal");
			} else {
				subtotal = BrowserActions.getText(driver, txtUpdatedSubTotal, "Subtotal");
			}
		}
		Log.event("Subtotal: " + subtotal);
		return StringUtils.getPriceFromString(subtotal);
	}

	/**
	 * To Get Order Sub Total
	 * @return double - Order Total
	 * @throws Exception - Exception
	 */
	public double getOrderSubTotal() throws Exception {	
		String orderTotal = null;
		if(Utils.waitForElement(driver, lblOrderSubtotal)) {
			orderTotal = BrowserActions.getText(driver, lblOrderSubtotal, "Subtotal");
		}
		Log.event("Order Subtotal: " + orderTotal);
		return StringUtils.getPriceFromString(orderTotal);
	}

	/**
	 * To Get Order Total
	 * @return double - Order Total
	 * @throws Exception - Exception
	 */
	public double getOrderTotal() throws Exception {	
		String orderTotal = null;
		if(Utils.waitForElement(driver, divOrderTotal)) {
			BrowserActions.scrollInToView(divOrderTotal, driver);
			orderTotal = BrowserActions.getText(driver, divOrderTotal, "Subtotal");
		}
		Log.event("Order total: " + orderTotal);
		return StringUtils.getPriceFromString(orderTotal);
	}

	/**
	 * To Get Unit price
	 * @return double
	 * @throws Exception - Exception
	 */
	public double getUnitPrice(int... index) throws Exception {
		int cartPrdIndex = index.length > 0 ? index[0] : 0;
		WebElement elemPrice = productUnitSalesPrice.get(cartPrdIndex);
		
		String unitPrice = BrowserActions.getText(driver, elemPrice, "Unit price");
		Log.event("Unit Price :: " + unitPrice);
		
		return StringUtils.getPriceFromString(unitPrice);
	}
	
	/**
	 * To Get Unit price
	 * @return double
	 * @throws Exception - Exception
	 */
	public double getProductSubtotal(int... index) throws Exception {
		int cartPrdIndex = index.length > 0 ? index[0] : 0;
		WebElement elemSubtotal = productPriceTotal.get(cartPrdIndex);
		String productSubtotal = "$0.00";
		try {
			WebElement productaAdjustedSubtotaL = elemSubtotal.findElement(By.cssSelector(".price-adjusted-total"));
			productSubtotal = BrowserActions.getText(driver, productaAdjustedSubtotaL, "Product adjusted total");
		} catch (NoSuchElementException e1) {
			try {
				WebElement productSubtotaL = elemSubtotal.findElement(By.cssSelector(".price-total-strikeoff"));
				productSubtotal = BrowserActions.getText(driver, productSubtotaL, "Product total strikeoff");
			} catch (NoSuchElementException e2) {
				WebElement productSubtotaL = elemSubtotal.findElement(By.cssSelector(".price-total"));
				productSubtotal = BrowserActions.getText(driver, productSubtotaL, "Product price total");
			}
		} // Alternatively, text from elemSubtotal can be conditionally split with "\n" to get productSubtotal
		Log.event("Product subtotal:: " + productSubtotal);
		return StringUtils.getPriceFromString(productSubtotal);
	}
	
	/**
	 * To get estimated product subtotal based on unit price and quantity
	 * @param prdUnitPrice - Product unit price 
	 * @param qty - Product quantity
	 * @return calculated subtotal
	 * @throws Exception
	 */
	public float getProductSubtotalEstimate(double prdUnitPrice, int qty) throws Exception {
		float productSubtotalEstimate = 0f;
		if(isBogoPromoApplied()) {
			productSubtotalEstimate = StringUtils.formatNumber(prdUnitPrice * ((int) (qty + 1) / 2), 2);
        } else if (isBogoHalfOffPromoApplied()) {
			int full = (int) ((qty + 1) / 2);
			float half = ((qty - (float) full) / 2);
			productSubtotalEstimate = StringUtils.formatNumber(prdUnitPrice * (full + half), 2);
        } else {
        	float calculatedUnitPrice = StringUtils.formatNumber(prdUnitPrice * (1 - getDiscountPercent()), 2);
        	productSubtotalEstimate = calculatedUnitPrice * qty;
        }
		Log.event("Estimated product total:: " + productSubtotalEstimate);
		return productSubtotalEstimate;
	}
	
	/**
	 * Check if a BOGO discount is applied to cart
	 */
	public boolean isBogoPromoApplied() throws Exception {
		if(Utils.waitForElement(driver, promotionCalloutMessage)) {
			String promoText = BrowserActions.getText(driver, promotionCalloutMessage, "Order promo message");
			return (promoText.toLowerCase().contains("1 free") || promoText.toLowerCase().contains("one free"));
		}
		return false;
	}
	
	/**
	 * Check if a BOGO 50% off discount is applied to cart
	 */
	public boolean isBogoHalfOffPromoApplied() throws Exception {
		if(Utils.waitForElement(driver, promotionCalloutMessage)) {
			String promoText = BrowserActions.getText(driver, promotionCalloutMessage, "Order promo message");
			return (promoText.toLowerCase().contains("buy one") || promoText.toLowerCase().contains("buy 1"))
					&& (promoText.toLowerCase().contains("half off") || promoText.toLowerCase().contains("50% off"));
		}
		return false;
	}
	
	/**
	 * To get any non BOGO discount found
	 * <br> Returns 0 if no discount is found
	 * @return discount percent in decimal format
	 * @throws Exception
	 */
	public double getDiscountPercent() throws Exception {
		double discountPercentage = 0d;
		if (Utils.waitForElement(driver, promotionCalloutMessage)
				&& !isBogoPromoApplied() && !isBogoHalfOffPromoApplied()) {
			String promoText = BrowserActions.getText(driver, promotionCalloutMessage, "Order promo message").toLowerCase();
			if(promoText.contains("%") && promoText.contains("off")) {
				discountPercentage = StringUtils.getNumberInString(promoText);
				Log.event("Discount detected to be:: " + discountPercentage + "%");
			}
			return discountPercentage / 100;
		} else {
			Log.event("No discount detected.");
			return discountPercentage;
		}
	}

	/**
	 * To Navigete to Checkout page
	 * @return Object ob Instance
	 * @throws Exception - Exception
	 */
	public Object navigateToCheckout()throws Exception {
		BrowserActions.clickOnElementX(btnCheckoutNow, driver, "Checkout Now Button");
		Utils.waitForPageLoad(driver);

		if(Utils.waitForElement(driver, readyElementSignInPage))
			return new SignIn(driver).get();
		else if(Utils.waitForElement(driver, readyElementCheckoutPage))
			return new CheckoutPage(driver).get();
		else {
			if(Utils.waitForElement(driver, miniCartContent)) {
				Log.fail("Still in cart page, checkout page not displayed",driver);
				return this;
			}
			Log.fail("Cannot navigate to desired page.");
		}
		return null;
	}

	/**
	 * To click on Image in cart
	 * @return PdpPage page object
	 * @throws Exception - Exception
	 */
	public PdpPage clickonImageInCart()throws Exception {
		BrowserActions.clickOnElementX(imgProductSection, driver, "Click the Product");
		return new PdpPage(driver).get();

	}


	/**
	 * To open on Shipping Overlay Link in cart
	 * @throws Exception - Exception
	 */
	public void openShippingOverlayToolTip()throws Exception {
		BrowserActions.clickOnElementX(divShippingOverlayToolTip, driver, "Shipping Overlay");
		Utils.waitForElement(driver, divShippingOverlayToolTipClose);
	}

	/**
	 * To open on Shipping Overlay Link in cart
	 * @throws Exception - Exception
	 */
	public void closeShippingOverlayToolTip()throws Exception {
		BrowserActions.clickOnElementX(divShippingOverlayToolTipClose, driver, "Click the Shipping Overlay Tool Tip");
		Utils.waitUntilElementDisappear(driver, divShippingOverlayToolTipClose);
	}


	/**
	 * To Remove all item from cart 
	 * @throws Exception
	 */
	public void removeAllCartProduct() throws Exception {
		List<WebElement> lstPrdRemoveLnk = Utils.isMobile() ? removeProductListMobile : removeProductList;
		Log.event("Number of product in cart:: " + lstPrdRemoveLnk.size());
		while(lstPrdRemoveLnk.size() != 0 && Utils.waitForElement(driver, lstPrdRemoveLnk.get(0))) {
			BrowserActions.clickOnElementX(lstPrdRemoveLnk.get(0), driver, "Remove the products");
			Utils.waitForPageLoad(driver);
			lstPrdRemoveLnk = Utils.isMobile() ? removeProductListMobile : removeProductList;
		}
		Log.event("All the Products in Cart page is Removed successfully.");
	}

	/**
	 * To Vefity product color
	 * @param color -
	 * @return true - if product color equal
	 * @throws Exception - Exception
	 */
	public boolean verifyProductColor(String color)throws Exception {

		boolean flag=false;
		String imgColor=BrowserActions.getText(driver, colorAttribute, "Color");
		if(imgColor.toLowerCase().contains(color.toLowerCase())) {
			flag=true;
		}
		return flag;
	}

	/**
	 * To click on Product Name In cart
	 * @return PdpPage page object
	 * @throws Exception - Exception
	 */
	public PdpPage clickonProductNameInCart()throws Exception {

		BrowserActions.clickOnElementX(productNameSection, driver, "Click the Product");
		return new PdpPage(driver).get();

	}

	/** 
	 * To get class names from order total based on index
	 * @param index - nth total
	 * @return String - class name
	 * @throws Exception - Exception
	 */
	public String getClassNamesFromOrderTotalsListByIndex(int index)throws Exception {
		return lstOrderElements.get(index-1).getAttribute("class").trim();
	}

	/**
	 * To Verify product price
	 * @param price -  
	 * @return true - if product price equals
	 * @throws Exception - Exception
	 */
	public boolean verifyProductPrice(String price) throws Exception {
		BrowserActions.scrollInToView(spanProductPrice, driver);
		String actualPrice = spanProductPrice.getAttribute("innerHTML").trim();
		Log.event(actualPrice + "Ac---Ex" + price);
		if(actualPrice.contains(price) || price.equals(actualPrice) || price.equalsIgnoreCase(actualPrice)) {
			return true;
		}else
			return false;
	}

	/**
	 * To Verify the Arrow diable in cart page
	 * @param direction -
	 * @return true -  if arrow disabled
	 * @throws Exception - Exception
	 */
	public boolean verifyArrowDisabled(String direction) throws Exception {
		boolean flag=false;
		WebElement divDecreaseArrow = Utils.isMobile() ? divDecreaseArrowMob : divDecreaseArrowDeskAndTab;
		WebElement divIncreaseArrow = Utils.isMobile() ? divIncreaseArrowMob : divIncreaseArrowDeskAndTab;
		if(direction.equals("down")) {
			flag=Utils.waitForElement(driver, divDecreaseArrow, 10);	
		}else if(direction.equals("up")) {
			flag=Utils.waitForElement(driver, divIncreaseArrow, 10);
		}

		return flag;
	}

	/**
	 * To click on Quantity Arrow on Cart page
	 * @param direction -
	 * @throws Exception - Exception
	 */
	public void clickOnQuantityArrow(String direction) throws Exception { 
		WebElement divDecreaseArrowEnabled = Utils.isMobile() ? divDecreaseArrowEnabledMob : divDecreaseArrowEnabledDeskAndTab;
		WebElement divIncreaseArrowEnabled = Utils.isMobile() ? divIncreaseArrowEnabledMob : divIncreaseArrowEnabledDeskAndTab;
		if(direction.equals("down") ){
			BrowserActions.scrollToView(divDecreaseArrowEnabled, driver);
			BrowserActions.clickOnElementX(divDecreaseArrowEnabled,driver,"Quantity Down arrow");	
		}else if(direction.equals("up")) {
			BrowserActions.scrollToView(divIncreaseArrowEnabled, driver);
			BrowserActions.clickOnElementX(divIncreaseArrowEnabled,driver,"Quantity Up arrow");

		}
		Utils.waitForPageLoad(driver);

	}


	/**
	 * To Verify Error message in cart page
	 * @param message -  
	 * @return true -  if error message is equal
	 * @throws Exception - Exception
	 */
	public boolean verifyErrorMessage(String message)throws Exception {
		boolean flag=false;
		if(message.equals(divQuantityMessage.getText()))
			flag=true;

		return flag;
	}


	/**
	 * To Varify total price in cart page 
	 * @param index -
	 * @return true - if total price equals
	 * @throws Exception - Exception
	 */
	public boolean verifyTotalPrice(int index) throws Exception {
		boolean flag=false;
		double totalProductText;
		String className = productPrice.get(index).getAttribute("class");
		if(className.equals("price-promotion")) {
			String css = ".price-sales";
			double productPriceText = StringUtils.getPriceFromString(productPrice.get(index).findElement(By.cssSelector(css)).getText());
			totalProductText = StringUtils.getPriceFromString(totalProductPriceMarkdown.get(index).getText());
			int productqty = StringUtils.getNumberInString(listProductQuantity.get(index).getAttribute("value"));
			
			flag = (productPriceText * productqty == totalProductText);
		}else {
			double  productPriceText = StringUtils.getPriceFromString(productPrice.get(index).getText());
			totalProductText = StringUtils.getPriceFromString(totalProductPrice.get(index).getText());
			int productqty = StringUtils.getNumberInString(listProductQuantity.get(index).getAttribute("Value"));
			
			flag = (Math.round(productPriceText * productqty) == Math.round(totalProductText));
		}

		return flag;
	}

	/**
	 * To edit a cart product by ID
	 * @param prdID - ID of cart product to edit
	 * @return instance of QuickShop or PdpPage based on platform spec
	 * @throws Exception
	 */
	public Object clickOnEditLink(String prdID)throws Exception {
		WebElement prdToEdit = getCartProductByID(prdID);
		if(prdToEdit == null) {
			return null;
		}
		WebElement prdEditLink = prdToEdit.findElement(By.cssSelector(".cart-columns>.col-4 .item-edit-details.btm-space a"));
		BrowserActions.clickOnElementX(prdEditLink, driver, "Click the Edit link");
		Utils.waitForPageLoad(driver);
		
		if(Utils.isDesktop()) {
			if(Utils.waitForElement(driver, popUpBonusProduct)) {
				return new QuickShop(driver).get();
			}
			else {
				BrowserActions.javascriptClick(prdEditLink, driver, "Click the Edit link");
				if(Utils.waitForElement(driver, popUpBonusProduct)) {
					return new QuickShop(driver).get();
				}
				else {
					Log.failsoft("Clicking on Edit link does not open Edit model.", driver);
				}
			}
		} else {
			if(driver.findElement(By.id("wrapper")).getAttribute("class").contains("pt_product")) {
				return new PdpPage(driver).get();
			}
			else {
				Log.failsoft("Clicking on Edit does not redirect to PDP page.", driver);
			}
		}
		return null;
	}
	
	
	/**
	 * To Click on lEdit link 
	 * @param index - 
	 * @return Object of Instance
	 * @throws Exception - Exception
	 */
	public Object clickOnEditLink(int index) throws Exception {
		if (Utils.waitForElement(driver, CQOEditlink)) {
			BrowserActions.clickOnElementX(lstCQOEditlink.get(index), driver, "Clicked edit link");
			Utils.waitForPageLoad(driver);
			return new QuickOrderPage(driver).get();
		}

		else if (Utils.isDesktop()) {
			BrowserActions.clickOnElementX(listEditlnkDesktop.get(index), driver, "Click the Edit link");
			Utils.waitForPageLoad(driver);
			if (Utils.waitForElement(driver, popUpBonusProduct)) {
				return new QuickShop(driver).get();
			} else {
				Log.failsoft("Clicking on Edit link does not open Edit model.", driver);
			}
		} else {
			List<WebElement> listEdit = Utils.isTablet() ? listEditlnkTablet : listEditlnkMobile;

			BrowserActions.clickOnElementX(listEdit.get(index), driver, "Click the Edit link");
			Utils.waitForPageLoad(driver);
			if (driver.findElement(By.id("wrapper")).getAttribute("class").contains("pt_product")) {
				return new PdpPage(driver).get();
			} else {
				Log.failsoft("Clicking on Edit does not redirect to PDP page.", driver);
			}
		}
		return null;
	}
	
	/**
	 * To click QO Edit link
	 * @param index-
	 * @returns object
	 * @throws Exception 
	 */
	public QuickOrderPage clickOnCOOProductEditLink(int index)throws Exception {		
		BrowserActions.clickOnElementX(lstCQOEditlink.get(index), driver, "Clicked edit link");
		Utils.waitForPageLoad(driver);
		return new QuickOrderPage(driver).get();		
	}
	

	/**
	 * To close edit overlay 
	 * @throws Exception - Exception
	 */
	public void closeEditOverlay() throws Exception {
		BrowserActions.clickOnElementX(btnEditOverlayClose, driver, "Click the Edit link");
		Utils.waitUntilElementDisappear(driver, btnEditOverlayClose);
	}

	/**
	 * To open coupon tool tip
	 * @throws Exception - Exception
	 */
	public void clickCouponToolTip() throws Exception {
		BrowserActions.clickOnElementX(lnkToolTipCoupon, driver, "Coupon Tool tip");
		Utils.waitForElement(driver, lnkCloseToolTipCoupon);
	}

	/**
	 * To close coupon tool tip
	 * @throws Exception - Exception
	 */
	public void closeCouponToolTip() throws Exception {
		BrowserActions.clickOnElementX(lnkCloseToolTipCoupon, driver, "Coupon Tool tip");
		Utils.waitUntilElementDisappear(driver, lnkCloseToolTipCoupon);
	}

	/**
	 * To click on checkout button in ATB overlay
	 * @return ShoppingBagPage -
	 * @throws Exception -
	 */
	public boolean verifyProductsAddedToCart(String[] productId) throws Exception {
		Log.event("Argument ID's :: " + Arrays.toString(productId));
		if (lstCartRegularProducts.size() < productId.length) {
			Log.reference("Number of products in cart is less than number of given product IDs.");
			return false;
		} else {
			String[] itemId = new String[lstCartRegularProducts.size()];
			for (int i=0 ; i<lstCartRegularProducts.size(); i++) {
				WebElement elem = lstCartRegularProducts.get(i);
				itemId[i] = BrowserActions.getTextFromAttribute(driver, elem, "data-itemid", "Product Item ID");
			}
			Log.event("Argument ID's :: " + Arrays.toString(productId));
			return CollectionUtils.compareTwoArraysAreEqualBySorting(itemId, productId);
		}
	}
	
	/**
	 * To verify if cart product has quantity filed
	 * @param productName - name of product to verify
	 * @return true/false if product has quantity field.
	 * <br>Returns false if product not found in cart.
	 * @throws Exception
	 */
	public boolean verifyProductQuantityField(String productName) throws Exception{
		WebElement gcElement = null;
		for(WebElement cartItem : divCartRow) {
			WebElement elementName = cartItem.findElement(By.cssSelector(".name"));
			String cartItemName = BrowserActions.getText(driver, elementName, "Cart row Item name");
			Log.event("cartItemName:: " + cartItemName);
			if(cartItemName.equalsIgnoreCase(productName)) {
				gcElement = cartItem;
				break;
			}
		}
		
		if(gcElement == null) {
			Log.failsoft(productName + " not found in cart.", driver);
			return false;
		} else {
			try {
				WebElement itemQty = gcElement.findElement(By.cssSelector(".item-quantity .custom-cart-update"));
				return Utils.waitForElement(driver, itemQty);
			} catch(Exception e) {
				return false;
			}
		}
	}
	
	/**
	 * To click on checkout button in ATB overlay
	 * @return ShoppingBagPage -
	 * @throws Exception -
	 */
	public boolean verifyProductsAddedToCartByProdName(String[] productName) throws Exception {
		Log.event("Argument ID's :: " + productName);
		if (ListproductName.size() != productName.length) {
			Log.reference("Given product ID's length does not match with number of available products in cart.");
			return false;
		} else {
			String[] itemName = new String[ListproductName.size()];
			for (int i=0 ; i<ListproductName.size(); i++) {
				WebElement elem = ListproductName.get(i);
				itemName[i] = BrowserActions.getText(driver, elem, "Product name");
			}
			Log.event("Argument ID's :: " + itemName);
			return Arrays.equals(itemName, productName);
		}
	}

	/**
	 * To check quickshop is displayed in recently viewed
	 * @return boolean - true if Quickshop is displayed, false if not
	 * @throws Exception - Exception
	 */
	public boolean checkQuickShopIsDisplayedInRecentlyViewed()throws Exception {
		BrowserActions.scrollInToView(recentlyViewedProdTile, driver);
		BrowserActions.mouseHover(driver, recentlyViewedProdTile, "Product Tile");
		WebElement qcButton = recentlyViewedProdTile.findElement(By.cssSelector(".quickview"));
		if (Utils.waitForElement(driver, qcButton)) {
			return true;
		}
		return false;
	}

	/**
	 * To click product image in recently viewed
	 * @return PdpPage - PdpPage object
	 * @throws Exception - Exception
	 */
	public PdpPage clickOnRecentlyViewedProductName(int index)throws Exception {
		WebElement elem = recentlyViewedProdName.get(index);
		BrowserActions.clickOnElementX(elem, driver, "Recently Viewed Product Name");
		return new PdpPage(driver).get();
	}

	/**
	 * To click update in edit overlay
	 * @throws Exception - Exception
	 */
	public void clickOnUpdateInEditOverlay()throws Exception {
		BrowserActions.clickOnElementX(btnUpdateInEditOverlay, driver, "Recently Viewed Product Name");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click product image in recently viewed
	 * @return PdpPage - PdpPage object
	 * @throws Exception - Exception
	 */
	public PdpPage clickOnRecentlyViewedProductImage(int index)throws Exception {
		WebElement elem = recentlyViewedProdImage.get(index);
		BrowserActions.clickOnElementX(elem, driver, "Recently Viewed Product Image");
		return new PdpPage(driver).get();
	}


	/**
	 * To click product image in recently viewed
	 * @return PdpPage - PdpPage object
	 * @throws Exception - Exception
	 */
	public List<String> getCurrentRecentViewProdName()throws Exception {
		List<String> names = new ArrayList<String>();
		int size = recentlyViewedCurrentProdName.size();
		for(int i=0; i<size; i++) {
			names.add(BrowserActions.getText(driver, recentlyViewedCurrentProdName.get(i), "Product Name"));
		}
		return names;
	}

	/**
	 * To click product image in recently viewed
	 * @return PdpPage - PdpPage object
	 * @throws Exception - Exception
	 */
	public boolean checkNextAndPrevArrowFunction()throws Exception {
		if(getNoOfProdDisplayInRecentView() > 4) {
			List<String> beforeNames = getCurrentRecentViewProdName();
			while (!Utils.waitForElement(driver, recentlyViewSectionNextDisabledArrow)) {
				BrowserActions.clickOnElementX(recentlyViewSectionNextArrow, driver, "Next arrow");
			}
			List<String> afterNames = getCurrentRecentViewProdName();

			boolean state1 =  CollectionUtils.compareTwoList(beforeNames, afterNames);

			beforeNames = getCurrentRecentViewProdName();
			while (!Utils.waitForElement(driver, recentlyViewSectionPrevDisabledArrow)) {
				BrowserActions.clickOnElementX(recentlyViewSectionPrevArrow, driver, "Prev arrow");
			}
			afterNames = getCurrentRecentViewProdName();

			boolean state2 =  CollectionUtils.compareTwoList(beforeNames, afterNames);

			if(!state1 && !state2) {
				return true;
			} else {
				return false;
			}

		} else {
			Log.event("Cannot be verified the next and Prev arrow");
			return false;
		}
	}


	/**
	 * To click on Wishlist on cart page
	 * @param productIndex -
	 * @return String of Page
	 * @throws Exception - Exception
	 */
	public String clickOnWishlist(int productIndex)throws Exception {

		String returnString="";
		BrowserActions.clickOnElementX(listWishlistLink.get(productIndex), driver, "Click the Wishlist link");
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, divCartTopBanner)){
			if(Utils.waitForElement(driver,listWishlistLinkAdded.get(productIndex))) {

				returnString=listWishlistLinkAdded.get(productIndex).getText();
				Log.message("The product Add to wishlist has been changed to " +returnString);
			}

		}else if(Utils.waitForElement(driver, divLoginPanel)) {

			returnString="SignUp_Page";
		}

		return returnString;

	}


	/**
	 * To get product count in cart page
	 * @return int -
	 * @throws Exception - Exception
	 */
	public int getProductCountInCart()throws Exception {
		int count=0;
		if(Utils.waitForElement(driver, divSingleCartRow)) {
			count=divCartRow.size();
		}
		return count;
	}


	/**
	 * To verify Empty cart 
	 * @return true - if cart it empty
	 * @throws Exception - Exception
	 */
	public boolean verifyEmptyCart()throws Exception {
		boolean flag=false;
		if(Utils.waitForElement(driver, emptyCart)) {
			flag=true;
		}
		return flag;
	}

	/**
	 * To verify Empty cart 
	 * @return true - if cart it empty
	 * @throws Exception - Exception
	 */
	public PlatinumCardLandingPage navigateToPlatinumCardLandingPage()throws Exception {
		BrowserActions.clickOnElementX(lnkSeeDetails, driver, "See Details in Payment Order");
		Utils.waitForPageLoad(driver);
		return new PlatinumCardLandingPage(driver).get();
	}

	/**
	 * To Sign out the User Accont
	 * @return SignIn Page object
	 * @throws Exception - Exception
	 */
	public SignIn signOutAccount() throws Exception {

		BrowserActions.scrollToTopOfPage(driver);
		new Headers(driver).get().mouseOverAccountMenu();
		BrowserActions.clickOnElementX(lnkSignOut, driver, "Serach icon");
		Utils.waitForPageLoad(driver);

		return new SignIn(driver).get();
	}


	/**
	 * To get Checkout Id in cart page
	 * @return String of Checkout ID
	 * @throws Exception - Exception
	 */
	public String getCheckoutID()throws Exception {
		return lblCheckoutID.getText().trim();
	}

	/**
	 * To click on paypal Button
	 * @return PaypagePage page object
	 * @throws Exception - Exception
	 */
	public PaypalPage clickOnPaypalButton()throws Exception {
		String baseWindowHandle = driver.getWindowHandle();
		Log.event("Base windows handle:: " + baseWindowHandle);
		BrowserActions.clickOnElementX(btnPaypal, driver, "Paypal Button");
		Set<String> openWindowsList = driver.getWindowHandles();
		Log.event("Number of open windows:: " + openWindowsList.size());
		if(openWindowsList.size() > 1) {
			Log.event("PayPal opened in separate window/tab.");
			String popUpWindowHandle = null;
			for(String windowHandle : openWindowsList) {
				Log.event("Current windows handle:: " + windowHandle);
				if (!windowHandle.equals(baseWindowHandle)) {
					popUpWindowHandle = windowHandle;
				}
			}
			driver.switchTo().window(popUpWindowHandle);
			Log.event("Popup window handle:: " + popUpWindowHandle);
		}else {
			Log.event("PayPal opened in same window/tab.");
		}
		Utils.waitForPageLoad(driver);
		return new PaypalPage(driver).get();
	}


	/**
	 * To click on Bonus product for Adding
	 * @throws Exception - Exception
	 */
	public void clickOnBonusProductAdd()throws Exception {
		BrowserActions.clickOnElementX(btnBonusProductAdd, driver, "Bonus Product Add");
		Utils.waitForElement(driver, choiceOfBonusProduct);
	}

	/**
	 * To click on Add To Wishlist
	 * @throws Exception - Exception
	 */
	public void clickOnAddToWishlist()throws Exception {
		if(Utils.waitForElement(driver, lnkAddToWishlist)) {
			BrowserActions.clickOnElementX(lnkAddToWishlist, driver, "Add To Wishlist");
			Utils.waitForPageLoad(driver);
		}
	}

	/**
	 * To click on Learn more link plcc
	 * @throws Exception - Exception
	 */
	public PlatinumCardApplication clickOnLearnmorelinkPlcc()throws Exception {
		Utils.waitForElement(driver, learnmorelinkPlcc);
		BrowserActions.clickOnElementX(learnmorelinkPlcc, driver, "Learn More Link");
		return new PlatinumCardApplication(driver).get();
	}

	/**
	 * To select Bonus product In modal
	 * @param index - 
	 * @return String - 
	 * @throws Exception - Exception
	 */
	public String selectBonusProductInModal(int index)throws Exception {	
		BrowserActions.scrollInToView(chkBonusProductSelection.get(index), driver);
		BrowserActions.clickOnElementX(chkBonusProductSelection.get(index), driver, "Bonus Product Selection");
		return BrowserActions.getText(driver, chkBonusProductSelection.get(index), "Bonus Product Selection");
	}

	/**
	 * To select a color of given bonus product
	 * @param index - index of which bonus product to select color of. 
	 * @throws Exception
	 */
	public void selectBonusProductColor(int index) throws Exception {
		if(Utils.waitForElement(driver, bonusProductOverlay)) {
			BrowserActions.clickOnElementX(selectcolortBonus.get(index), driver, "Click color");
			List<WebElement> elementColor= bonusProducts.get(index).findElements(By.cssSelector(".value.attributeinfo .swatches.color .selectable a"));
			BrowserActions.clickOnElementX(elementColor.get(0), driver, "clicked first color");
			Utils.waitForPageLoad(driver);
		}
	}

	/**
	 * To select Size of given bonus product
	 * @throws Exception - Exception
	 */
	public void selectBonusProductSize(int index) throws Exception {
		if(Utils.waitForElement(driver, bonusProductOverlay)) {
			if(productHaveSizeFamily(index)) {
				BrowserActions.clickOnElementX(bonusSizeFamilySize.get(index), driver, "Click size");	
				List<WebElement> elementFamilySize= bonusProducts.get(index).findElements(By.cssSelector(" [data-attrinfo='sizeFamily'] .swatches.size .selectable a"));
				BrowserActions.clickOnElementX(elementFamilySize.get(0), driver, "clicked first size");
				Utils.waitForPageLoad(driver);
				if(Utils.waitForElement(driver, sizeBonus.get(index))) {
					BrowserActions.clickOnElementX(sizeBonus.get(index), driver, "Click size");
					List<WebElement> elementSize= bonusProducts.get(index).findElements(By.cssSelector(" [data-attrinfo='size'] .swatches.size .selectable a"));
					BrowserActions.clickOnElementX(elementSize.get(0), driver, "clicked first size");
					Utils.waitForPageLoad(driver);
				}
			} else if(producthaveShoeWidth(index)) {
				BrowserActions.clickOnElementX(bonusShoeWidth.get(index), driver, "Click size");	
				List<WebElement> elementWidthSize= bonusProducts.get(index).findElements(By.cssSelector(" [data-attrinfo='shoeWidth'] .swatches.size .selectable a"));
				BrowserActions.clickOnElementX(elementWidthSize.get(0), driver, "clicked shoe size");
				Utils.waitForPageLoad(driver);
				if(Utils.waitForElement(driver, bonusShoeSize.get(index))) {
					BrowserActions.clickOnElementX(bonusShoeSize.get(index), driver, "Click size");	
					List<WebElement> elementShoeSize= bonusProducts.get(index).findElements(By.cssSelector(" [data-attrinfo='shoeSize'] .swatches.size .selectable a"));
					BrowserActions.clickOnElementX(elementShoeSize.get(0), driver, "clicked shoe size");
					Utils.waitForPageLoad(driver);
				}
			} else if(productHaveBandSize(index)) {
				BrowserActions.clickOnElementX(bonusBandWidth.get(index), driver, "Click size");	
				List<WebElement> elementBandSize= bonusProducts.get(index).findElements(By.cssSelector(" [data-attrinfo='braBandSize'] .swatches.size .selectable a"));
				BrowserActions.clickOnElementX(elementBandSize.get(0), driver, "clicked shoe size");
				Utils.waitForPageLoad(driver);
				if(Utils.waitForElement(driver, bonusCupWidth.get(index))) {
					BrowserActions.clickOnElementX(bonusCupWidth.get(index), driver, "Click size");	
					List<WebElement> elementCupSize= bonusProducts.get(index).findElements(By.cssSelector(" [data-attrinfo='braCupSize'] .swatches.size .selectable a"));
					BrowserActions.clickOnElementX(elementCupSize.get(0), driver, "clicked shoe size");
					Utils.waitForPageLoad(driver);
				}
			} else {
			 	BrowserActions.clickOnElementX(sizeBonus.get(index), driver, "Click size");
				List<WebElement> elementSize= bonusProducts.get(index).findElements(By.cssSelector(" [data-attrinfo='size'] .swatches.size .selectable a"));
				BrowserActions.clickOnElementX(elementSize.get(0), driver, "clicked first size");
				Utils.waitForPageLoad(driver);
			}
		}
	}

	/**
	 * To verify whether that product have size family or not
	 * index is product and i.e starts from 0,1,2,3...
	 * @throws Exception - Exception
	 */
	public boolean productHaveSizeFamily(int index) throws Exception {
		try {
			WebElement element = bonusProducts.get(index).findElement(By.cssSelector("div[data-attrinfo='sizeFamily'] .selecting.attr-size"));
			if(Utils.waitForElement(driver, element)) {
				return true;
			}
		} catch(NoSuchElementException e) {
			Log.event("Size family not found in page.");
		}
		return false;
	}
	
	/**
	 * To verify whether that product have Band size or not
	 * index is product and i.e starts from 0,1,2,3...
	 * @throws Exception - Exception
	 */
	public boolean productHaveBandSize(int index) throws Exception {
		try {
			WebElement element = bonusProducts.get(index).findElement(By.cssSelector("div[data-attrinfo='braBandSize'] .selecting.attr-size"));
			if(Utils.waitForElement(driver, element)) {
				return true;
			}
		} catch(NoSuchElementException e) {
			Log.event("Band size not found in page.");
		}
		return false;
	}
	
	/**
	 * To verify whether that product have size family or not
	 * index is product and i.e starts from 1,2,3...
	 * @throws Exception - Exception
	 */
	public int getNoOfBonusProducts() throws Exception {
		return bonusProducts.size();
	}
	
	
	public int getNoOfProductsInCart() throws Exception {
		return sectionCartedProduct.size();
	}


	/**
	 * To click on Size family and select based on index
	 * @throws Exception - Exception
	 */
	public HashMap<String, String> selectOptionsForProductIndex(int index) throws Exception {
		HashMap<String, String> selectionDetails = new HashMap<String, String>();
		if(Utils.waitForElement(driver, bonusProductOverlay)) {
			if(bonusProducts.size() < (index)) {
				Log.fail("There is not products more than " + bonusProducts.size());
			}else {
				//To select size family if present
				try {
					bonusProducts.get(index).findElement(By.cssSelector("div[data-attrinfo='sizeFamily'] .selecting.attr-size")).click();
					WebElement firstOption = bonusProducts.get(index).findElements(By.cssSelector("div[data-attrinfo='sizeFamily'] .selecting.attr-size + ul li")).get(0);
					BrowserActions.javascriptClick(firstOption, driver, "first Option");
					selectionDetails.put("SizeFamily", BrowserActions.getText(driver, firstOption, "first Option"));
				}catch(NoSuchElementException |IndexOutOfBoundsException e) {
					Log.event("Size family not available");
				}
				
				//To select Shoe width if present
				try{
					List<WebElement> elem1 = driver.findElements(By.cssSelector("div[data-attrinfo='shoeWidth'] .selecting.attr-size"));
					BrowserActions.clickOnElementX(elem1.get(index), driver, "shoe width dropdown");
					List<WebElement> elem = bonusProducts.get(index).findElements(By.cssSelector("div[data-attrinfo='shoeWidth'] .selecting.attr-size + ul li a"));
					BrowserActions.clickOnElementX(elem.get(0), driver, "width");
					selectionDetails.put("SizeFamily", BrowserActions.getText(driver, selectedShoeWidth, "Shoe Width"));
				}catch(NoSuchElementException  |IndexOutOfBoundsException e ) {
					Log.event("Shoe width not available");
				}
			}
		}
		return selectionDetails;
	}

	/**
	 * To verify whether that product have Shoe width or not
	 * index is product and i.e starts from 0,1,2,3...
	 * @throws Exception - Exception
	 */
	public boolean producthaveShoeWidth(int index) throws Exception {
		try{
			WebElement element=bonusProducts.get(index).findElement(By.cssSelector("div[data-attrinfo='shoeWidth']"));
			if(Utils.waitForElement(driver, element)) {
				return true;
			}
		} catch(NoSuchElementException e) {
			Log.event("Shoe width not found in page.");
		}
		return false;
	}
	
	/**
	 * To select options and add bonus product from selection popup to cart
	 * @param indexBonusProduct - index to bonus product to add
	 * @throws Exception
	 */
	public void addBonusProductToBagByIndex(int indexBonusProduct) throws Exception{
		if(Utils.waitForElement(driver, bonusProductOverlay)) {
			selectBonusProductInModal(indexBonusProduct);
			selectOptionsForProductIndex(indexBonusProduct);
			selectBonusProductColor(indexBonusProduct);
			selectBonusProductSize(indexBonusProduct);
			clickOnBonusProductAddToBag();
		}
	}

	/**
	 * To click on Bonus product to Add to Bag
	 * @throws Exception - Exception
	 */
	public void clickOnBonusProductAddToBag()throws Exception {
		BrowserActions.clickOnElementX(btnBonusProductAddToBag, driver, "Bonus Product Add to bag");	
		Utils.waitForPageLoad(driver);
	}


	/**
	 * To get Bonus item price
	 * @return String - 
	 * @throws Exception - Exception
	 */
	public String getBonusItemPrice()throws Exception {
		WebElement bonusItemPrice = Utils.isMobile() ? bonusItemPriceMob : bonusItemPriceDeskAndTab;
		return BrowserActions.getText(driver, bonusItemPrice, "Bonus Item Price");		
	}


	/**
	 * To click on bonus productEdit link
	 * @throws Exception - Exception
	 */
	public void clickOnBonusProductEdit()throws Exception {
		BrowserActions.scrollToViewElement(btnBonusEdit, driver);
		BrowserActions.clickOnElementX(btnBonusEdit, driver, "Bonus Product Edit");		
	}


	/**
	 * To Enter promo code in checkout page
	 * @param promoCode - 
	 * @throws Exception - Exception
	 */
	public void enterPromoCode(String promoCode)throws Exception {
		txtPromoCode.clear();
		txtPromoCode.sendKeys(promoCode);
	}

	/**
	 * To click on Apply coupon button
	 * @throws Exception - Exception
	 */
	public void clickOnApplycouponButton()throws Exception {
		BrowserActions.clickOnElementX(btnApplycoupon, driver, "Apply");		
	}


	/**
	 * To click on click on coupon Tooltip
	 * @throws Exception - Exception
	 */
	public void clickOnCouponToolTip()throws Exception {
		BrowserActions.clickOnElementX(couponToolTip, driver, "Coupon Tool Tip");	
	}


	/**
	 * To click on Shipping cost tool tip
	 * @throws Exception - Exception
	 */
	public void clickOnCouponRemoveButton()throws Exception {
		BrowserActions.clickOnElementX(btnRemoveButton, driver, "Coupon Tool Tip");	
	}

	/**
	 * To remove all applied coupon codes
	 * @throws Exception - Exception
	 */
	public void removeAllAppliedCoupons() throws Exception {
		int size = btnCouponRemoveButton.size();
		for(int index=size-1; index>=0; index--) {
			BrowserActions.clickOnElementX(btnCouponRemoveButton.get(index), driver, "Coupon Tool Tip");
		}
	}


	/**
	 * To click on coupon tool tip close
	 * @throws Exception - Exception
	 */
	public void clickOnCouponToolTipclose()throws Exception {
		Utils.waitForElement(driver, couponToolTipContent);
		Utils.waitForElement(driver, btnCouponToolTipClose);
		BrowserActions.clickOnElementX(btnCouponToolTipClose, driver, "Coupon Tool Tip Close");
	}


	/**
	 * To click on clickPlcc Expand
	 * @throws Exception - Exception
	 */
	public void clickplccExpand()throws Exception {
		BrowserActions.clickOnElementX(expandclosePLCCcontentslot, driver, "Expand PLCC content slot");

	}


	/**
	 * To click on Shopping Cart tool tip
	 * @return true - if Shopping Cart Tool Tip opened
	 * @throws Exception - Exception
	 */
	public boolean clickOnShoppingCartToolTip()throws Exception {
		BrowserActions.clickOnElementX(spanCostToolTip, driver, "Shopping cost Tool Tip");
		return Utils.waitForElement(driver, spanCostToolTip);
	}

	/**
	 * To Verify sales Tax
	 * @param salesTax -
	 * @return true -  if sales tax equals
	 * @throws Exception - Exception
	 */
	public boolean verifySalesTax(int salesTax )throws Exception {
		int ExactValue =0;
		String tax= txtSalesTaxValue.getText();
		
		if(tax.contains("-")) {
			return true;
		} else {
			String sales = tax.split("\\$")[1];
			ExactValue=Integer.parseInt(sales.split("\\.")[0]);
			return ExactValue==salesTax;
		}
		
	}

	/**
	 * To Verify To total savings
	 * @return true - if total savings equals
	 * @throws Exception - Exception
	 */
	public boolean verifyTotalSavings()throws Exception {
		double discount = StringUtils.getPriceFromString(divDicountTotal.getText());
		double discountShipping = StringUtils.getPriceFromString(divShippingDicountTotal.getText());
		double divTotalSavingsDouble = StringUtils.getPriceFromString(divShippingDicountTotal.getText());

		return (divTotalSavingsDouble == (discount + discountShipping));
	}
	/**
	 * To remove Wishlist All product
	 * @throws Exception - Exception
	 */
	public void removeWishlishAllProduct()throws Exception {

		BrowserActions.scrollToTopOfPage(driver);
		new Headers(driver).get().mouseOverAccountMenu();
		BrowserActions.clickOnElementX(lnkWishlist, driver, "WishList icon");
		Utils.waitForPageLoad(driver);

		BrowserActions.clickOnElementX(removeWishlist, driver, "WishList item");

	}

	/**
	 * To add choice bonus product
	 * @param index -
	 * @param productColor -
	 * @param productSize -
	 * @throws Exception - Exception
	 */
	public void addChoiceBonusProduct(int index, String productColor, String productSize) throws Exception {

		if(Utils.waitForElement(driver,lblBonusProductCallout)) {
			Utils.waitForElement(driver, lnkAddBonusProduct);
			BrowserActions.clickOnElementX(lnkAddBonusProduct, driver, "Add Bonus product button");
			if(Utils.waitForElement(driver, popUpBonusProduct)) {

				BrowserActions.clickOnElementX(checkbonusProducts.get(index), driver, "Bonus CheckBox");
				WebElement elementSize1= bonusProducts.get(index).findElement(By.cssSelector(".selecting.attr-size:nth-child(1)"));
				BrowserActions.clickOnElementX(elementSize1, driver, "size dropdown");
				List<WebElement> elementSize= bonusProducts.get(index).findElements(By.cssSelector(".value.attributeinfo .swatches.size .selectable a"));
				for(int in=0;in<elementSize.size();in++) {
					if(elementSize.get(in).getAttribute("title").contains(productSize)) {
						BrowserActions.clickOnElementX(elementSize.get(in), driver, "size dropdown");
						break;
					}else
						Log.message(productColor + " . Color is not displayed in the dropdown  ");
				}

				WebElement elementColor1= bonusProducts.get(index).findElement(By.cssSelector(".selecting.attr-color:nth-child(1)"));
				BrowserActions.clickOnElementX(elementColor1, driver, "size dropdown");
				List<WebElement> elementColor= bonusProducts.get(index).findElements(By.cssSelector(".value.attributeinfo .swatches.color .selectable a"));
				for(int in=0;in<elementColor.size();in++) {
					if(elementColor.get(in).getAttribute("title").contains(productColor)) {
						BrowserActions.clickOnElementX(elementColor.get(in), driver, "size dropdown");
						break;
					}
					else
						Log.message(productColor + " . Color is not displayed in the dropdown  ");
				}

			}
			else {
				Log.message("Bonus product Pop up is not open");
			}
			if(Utils.waitForElement(driver,btnAddtoCartBonus)) {
				BrowserActions.clickOnElementX(btnAddtoCartBonus, driver, "Add to cart button");	
				Utils.waitForPageLoad(driver);
			}else {
				Log.message("Add to Cart button is not displayed in the bonus product pop up layout");
			}


		}else {
			Log.message("Choose Bonus product layout is not displayed");
		}


	}
	
	/**
	 * To add choice bonus product
	 * @param index -
	 * @param productColor -
	 * @param productSize -
	 * @throws Exception - Exception
	 */
	public void addChoiceBonusProductByIndex(int index, int colorIndex, int sizeIndex) throws Exception {

		if(Utils.waitForElement(driver,lblBonusProductCallout)) {
			Utils.waitForElement(driver, lnkAddBonusProduct);
			BrowserActions.clickOnElementX(lnkAddBonusProduct, driver, "Add Bonus product button");
			if(Utils.waitForElement(driver, popUpBonusProduct)) {

				BrowserActions.clickOnElementX(checkbonusProducts.get(index), driver, "Bonus CheckBox");
				WebElement elementSize1= bonusProducts.get(index).findElement(By.cssSelector(".selecting.attr-size:nth-child(1)"));
				BrowserActions.clickOnElementX(elementSize1, driver, "size dropdown");
				List<WebElement> elementSize= bonusProducts.get(index).findElements(By.cssSelector(".value.attributeinfo .swatches.size .selectable a"));
				BrowserActions.clickOnElementX(elementSize.get(sizeIndex), driver, "size dropdown");
				
				WebElement elementColor1= bonusProducts.get(index).findElement(By.cssSelector(".selecting.attr-color:nth-child(1)"));
				BrowserActions.clickOnElementX(elementColor1, driver, "size dropdown");
				List<WebElement> elementColor= bonusProducts.get(index).findElements(By.cssSelector(".value.attributeinfo .swatches.color .selectable a"));
				BrowserActions.clickOnElementX(elementColor.get(colorIndex), driver, "size dropdown");
				
			}else {
				Log.message("Bonus product Pop up is not open");
			}
			if(Utils.waitForElement(driver,btnAddtoCartBonus)) {
				BrowserActions.clickOnElementX(btnAddtoCartBonus, driver, "Add to cart button");	
				Utils.waitForPageLoad(driver);
			}else {
				Log.message("Add to Cart button is not displayed in the bonus product pop up layout");
			}


		}else {
			Log.message("Choose Bonus product layout is not displayed");
		}
	}
	
	/**
	 * To add choice bonus product
	 * @param index -
	 * @param productColor -
	 * @param productSize -
	 * @throws Exception - Exception
	 */
	public void selectChoiceBonusProductWithSwatchesByIndex(int index, int colorIndex, int sizeIndex) throws Exception {
		if(Utils.waitForElement(driver,lblBonusProductCallout)) {
			Utils.waitForElement(driver, lnkAddBonusProduct);
			BrowserActions.clickOnElementX(lnkAddBonusProduct, driver, "Add Bonus product button");
			if(Utils.waitForElement(driver, choiceOfBonusProduct)) {
				BrowserActions.clickOnElementX(checkbonusProducts.get(index), driver, "Bonus CheckBox");
				WebElement elementSize1= bonusProducts.get(index).findElement(By.cssSelector(".selecting.attr-size:nth-child(1)"));
				BrowserActions.clickOnElementX(elementSize1, driver, "size dropdown");
				List<WebElement> elementSize= bonusProducts.get(index).findElements(By.cssSelector(".value.attributeinfo .swatches.size .selectable a"));
				BrowserActions.clickOnElementX(elementSize.get(sizeIndex), driver, "size dropdown");
				
				WebElement elementColor1= bonusProducts.get(index).findElement(By.cssSelector(".selecting.attr-color:nth-child(1)"));
				BrowserActions.clickOnElementX(elementColor1, driver, "size dropdown");
				List<WebElement> elementColor= bonusProducts.get(index).findElements(By.cssSelector(".value.attributeinfo .swatches.color .selectable a"));
				BrowserActions.clickOnElementX(elementColor.get(colorIndex), driver, "size dropdown");
			} else {
				Log.failsoft("Bonus product Pop up is not open");
			}
		}else {
			Log.failsoft("Choose Bonus product layout is not displayed");
		}
	}
	
	/**
	 * To get number of products displayed in recommendation section
	 * @return Integer - Number of products
	 * @throws Exception - Exception
	 */
	public int getNoOfProdDisplayInRecommendation() throws Exception {
		Log.event("Number of Products in the Recommendation section :: " + prodNameListRecommendation.size());
		return prodNameListRecommendation.size();
	}
	
	/**
	 * To number of products displayed in recently view section
	 * @return Integer - 
	 * @throws Exception -
	 */
	public int getNoOfProdDisplayInRecentView() throws Exception {
		Log.event("Number of Products Recently Viewed :: " + recentlyViewedProdName.size());
		return recentlyViewedProdName.size();
	}
	
	
	/**
	 * To click ViewMore in choice of bonus overlay
	 * @throws Exception -
	 */
	public void clickOnViewMoreInChoiceOfBonusOverlay() throws Exception {
		Utils.waitForElement(driver, lnkBonusViewMore);
		BrowserActions.scrollInToView(lnkBonusViewMore, driver);
		BrowserActions.javascriptClick(lnkBonusViewMore, driver, "View More");
		Utils.waitForElement(driver, lnkBonusViewMoreExpanded);
	}

	/**
	 * To Edit choice bonus product
	 * @param index -
	 * @param productColor -
	 * @param productSize -
	 * @throws Exception -
	 */
	public void EditChoiceBonusProduct(int index, String productColor, String productSize) throws Exception {

		if(Utils.waitForElement(driver, popUpBonusProduct)) {
			if(!checkbonusProducts.get(index).getAttribute("checked").equals("true")) {
				BrowserActions.clickOnElementX(checkbonusProducts.get(index), driver, "Bonus CheckBox");
			}

			WebElement elementSize1= bonusProducts.get(index).findElement(By.cssSelector(".selecting.attr-size:nth-child(1)"));
			BrowserActions.clickOnElementX(elementSize1, driver, "size dropdown");
			List<WebElement> elementSize= bonusProducts.get(index).findElements(By.cssSelector(".value.attributeinfo .swatches.size .selectable a"));
			for(int in=0;in<elementSize.size();in++) {
				if(elementSize.get(in).getAttribute("title").contains(productSize)) {
					BrowserActions.clickOnElementX(elementSize.get(in), driver, "size dropdown");
					break;
				}	
			}
			WebElement elementColor1= bonusProducts.get(index).findElement(By.cssSelector(".selecting.attr-color:nth-child(1)"));
			BrowserActions.clickOnElementX(elementColor1, driver, "size dropdown");

			List<WebElement> elementColor= bonusProducts.get(index).findElements(By.cssSelector(".value.attributeinfo .swatches.color .selectable a"));
			for(int in=0;in<elementColor.size();in++) {
				if(elementColor.get(in).getAttribute("title").contains(productColor)) {
					BrowserActions.clickOnElementX(elementColor.get(in), driver, "size dropdown");
					break;
				}

			}
		}else {
			Log.message("Bonus product Pop up is not open");
		}
		if(Utils.waitForElement(driver,btnAddtoCartBonus)) {
			BrowserActions.clickOnElementX(btnAddtoCartBonus, driver, "Add to cart button");	
		}else {
			Log.message("Add to Cart button is not displayed in the bonus product pop up layout");
		}
		Utils.waitForPageLoad(driver);
	}
	
	
	public CheckoutPage clickOnContinueButton(String guestEmailAddress) throws Exception {
		Utils.waitForPageLoad(driver);
		BrowserActions.typeOnTextField(emailInputField,guestEmailAddress, driver, "order Number Field");
		BrowserActions.clickOnElementX(btnContinueGuest, driver, "Checkout as Guest");
		Utils.waitForPageLoad(driver);
		return new CheckoutPage(driver).get();	
	}  


	/**
	 * To get product sku list
	 * @return List of String
	 * @throws Exception - Exception
	 */
	public List<String> getProductSkuList()throws Exception {
		List<String> productSku = new ArrayList<String>();
		for(WebElement sku : lstProductSku) {
			productSku.add(sku.getText().replace("#", ""));
		}
		return productSku;
	}
	
	/**
	* To verify Add to Wishlist link should not be displayed for E-gift card
	* @param String - giftCardName
	* @return boolean - True if Add to wishlist is not displayed for E-Gift else false
	* @throws Exception
	*/
	public boolean verifyAddToWishlistNotDisplayedForEGift(String giftCardName) throws Exception {
		WebElement giftEditelem = driver.findElement(By.cssSelector(".item-edit-details a[title*='"+ giftCardName.replace("E", "e") +"']"));
		WebElement parentelem = giftEditelem.findElement(By.xpath(".."));
		try {
			parentelem.findElement(By.cssSelector(".add-to-wishlist"));
			return false;
		} catch(NoSuchElementException e) {
			return true;
		}
	}
	
	/**
	 * To mouse hover on details in choice of bonus modal
	 * @return - void
	 * @throws Exception - Exception
	 */
	public void mouseHoverOnDetailsInChoiceOfBonusModal()throws Exception {
		BrowserActions.mouseHover(driver, lnkPromoDetails, "Promo Details");
	}
	
	/**
	 * To click on details in choice of bonus modal // Tablet & Mobile
	 * @return - void
	 * @throws Exception - Exception
	 */
	public void clickOnDetailsInChoiceOfBonusModal()throws Exception {
		BrowserActions.scrollInToView(lnkPromoDetails, driver);
		BrowserActions.safariClick(lnkPromoDetails,driver, "Promo Details");
	}
	

	/**
	 * To click on continuous arrow in cart page
	 * @param direction -
	 * @param count -
	 * @throws Exception -
	 */
	public void clickOnArrowContious(String direction,int count)throws Exception {
		WebElement divDecreaseArrowEnabled = Utils.isMobile() ? divDecreaseArrowEnabledMob : divDecreaseArrowEnabledDeskAndTab;
		WebElement divIncreaseArrowEnabled = Utils.isMobile() ? divIncreaseArrowEnabledMob : divIncreaseArrowEnabledDeskAndTab;
		if(direction.equals("down")) {
			for(int i=0;i<count;i++) {
				BrowserActions.clickOnElementX(divDecreaseArrowEnabled,driver,"Quantity Down arrow");	
				Utils.waitForPageLoad(driver);
			}
		}else if(direction.equals("up")) {
			for(int i=0;i<count;i++) {
				BrowserActions.clickOnElementX(divIncreaseArrowEnabled,driver,"Quantity Up arrow");	
				Utils.waitForPageLoad(driver);
			}
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on close button in shipping cost tool in shopping cart page
	 * @throws Exception -
	 */
	public void clickOnShippingCostToolClose() throws Exception {
		BrowserActions.clickOnElementX(shippingCostOverlayClose, driver, "Shipping cost tool close");
	}

	/**
	 * To get product color variation value by index
	 * @param index -
	 * @return String -
	 * @throws Exception -
	 */
	public String getProductColorVariation(int index)throws Exception {
		WebElement color = driver.findElements(By.cssSelector("#cart-items-form div.cart-row")).get(index).findElement(By.cssSelector("div[data-attribute='color'] .value"));
		return color.getText();
	}

	/**
	 * To get product width variation value by index
	 * @param index -
	 * @return String -
	 * @throws Exception -
	 */
	public String getProductWidthVariation(int index)throws Exception {
		WebElement width = driver.findElements(By.cssSelector("#cart-items-form div.cart-row")).get(index).findElement(By.cssSelector("div[data-attribute='shoeWidth'] .value"));
		return width.getText().trim();
	}

	/**
	 * To get product Size variation value by index
	 * @param index -
	 * @return String -
	 * @throws Exception -
	 */
	public String getProductSizeVariation(int index)throws Exception {
		WebElement size;
		WebElement cartRow = driver.findElements(By.cssSelector("#cart-items-form div.cart-row")).get(index);
		try{
			size = cartRow.findElement(By.cssSelector("div[data-attribute='size'] .value"));
		} catch(NoSuchElementException ex) {
			size = cartRow.findElement(By.cssSelector("div[data-attribute='shoeSize'] .value"));
		}				
		return size.getText().trim();
	}

	public String SelectedShoeWidth() throws Exception {
		if(Utils.waitForElement(driver, productShoeWidth)) {
			String productWidth= BrowserActions.getText(driver, productShoeWidth, "Shoe width");
			return productWidth;
		}else {
			return "";
		}
	}

	public boolean productIsShoeType()throws Exception {
		return Utils.waitForElement(driver, productShoeWidth);
	}
	
	/**
	 * To get product name value by index
	 * @param index -
	 * @return String -
	 * @throws Exception -
	 */
	public String getProductName(int index)throws Exception {
		WebElement name = driver.findElements(By.cssSelector("#cart-items-form div.cart-row")).get(index).findElement(By.cssSelector(".name a"));
		return name.getText();
	}

	/**
	 * To get product quantity value by index
	 * @param index -
	 * @return String -
	 * @throws Exception -
	 */
	public String getProductQuantity(int index)throws Exception {
		WebElement qty = driver.findElements(By.cssSelector("#cart-items-form div.cart-row")).get(index).findElement(By.cssSelector(".item-quantity input"));
		return qty.getAttribute("value").trim();
	}

	/**
	 * To get product price value by index
	 * @param index -
	 * @return String -
	 * @throws Exception -
	 */
	public String getProductPrice(int index)throws Exception {
		WebElement qty;
		String webElemet = Utils.isMobile() ? ".cart-columns .item-details-and-qty" : ".cart-columns>.columns-combine";
		try {
			qty = driver.findElements(By.cssSelector("#cart-items-form div.cart-row")).get(index).findElement(By.cssSelector(webElemet + " span[class*='price-total']"));
		}catch(Exception e) {
			qty = driver.findElements(By.cssSelector("#cart-items-form div.cart-row")).get(index).findElement(By.cssSelector(webElemet + " div[class*='price-adjusted-total']"));
		}
		return qty.getText().trim();
	}

	/**
	 * To get product details as list
	 * @return LinkedList -
	 * @throws Exception -
	 */
	public LinkedList<LinkedHashMap<String, String>> getProductDetails()throws Exception {
		LinkedList<LinkedHashMap<String, String>> dataToReturn = new LinkedList<LinkedHashMap<String, String>>();

		for(int i = 0; i < divCartRow.size(); i++) {
			LinkedHashMap<String, String> product = new LinkedHashMap<String, String>();
			try { product.put("Name", getProductName(i)); }catch(NoSuchElementException e) {product.put("Name", "");}
			try { product.put("Color", getProductColorVariation(i)); }catch(NoSuchElementException e) {product.put("Color", "");}
			try { product.put("Size", getProductSizeVariation(i)); }catch(NoSuchElementException e) {product.put("Size", "");}
			try { product.put("Quantity", getProductQuantity(i)); }catch(NoSuchElementException e) {product.put("Quantity", "");}
			try { product.put("Price", getProductPrice(i)); }catch(NoSuchElementException e) {product.put("Price", "");}
			dataToReturn.add(product);
		}

		return dataToReturn;
	}

	/**
	 * To update size of the product based on product index	
	 * @param index - index
	 * @return String - updated size
	 * @throws Exception - Exception
	 */
	public String updateSizeByPrdIndex(int index, int sizeIndex)throws Exception {
		Object obj = clickOnEditLink(index);
		String size;
		if(Utils.isDesktop()) {
			QuickShop qsModal = (QuickShop) obj;
			size = qsModal.selectSize("random").trim();
			BrowserActions.clickOnElement(qsModal.btnAddToBag, driver, "Update button");

		}else {
			PdpPage pdpPage = (PdpPage) obj;
			size = pdpPage.selectSizeBasedOnIndex(sizeIndex);
			pdpPage.clickOnUpdate();
		}

		Utils.waitForPageLoad(driver);
		return size;
	}
	
	/**
	 * To update quantity of the product based on product ID	
	 * @param prdID - ID of product to update
	 * @return qtyToUpdate - updated quantity
	 * @throws Exception
	 */
	public String updateQuantityByPrdID(String prdID, String qtyToUpdate)throws Exception {
		String qty;
		Object obj = clickOnEditLink(prdID);
		if(Utils.isDesktop()) {
			QuickShop qsModal = (QuickShop) obj;
			qty = qsModal.selectQty(qtyToUpdate).trim();
			BrowserActions.clickOnElement(qsModal.btnAddToBag, driver, "Update button");
		} else {
			PdpPage pdpPage = (PdpPage) obj;
			qty = pdpPage.selectQty(qtyToUpdate).trim();
			pdpPage.clickOnUpdate();
		}
		Utils.waitForPageLoad(driver);
		return qty;
	}

	/**
	 * To update quantity of the product based on product index	
	 * @param index - product index
	 * @return qtyToUpdate - updated quantity
	 * @throws Exception - Exception
	 */
	public String updateQuantityByPrdIndex(int index, String qtyToUpdate)throws Exception {
		String qty;
		try {
			WebElement cqoElement = null;
			
			if(Utils.isMobile()) {
				cqoElement = txtCatalogQuickOrder_Mobile;
			} else {
				cqoElement = txtCatalogQuickOrder_Desk_Tab;
			}
			boolean isQuickOrd = Utils.waitForElement(driver, cqoElement);
			
			Object obj = clickOnEditLink(index);
			if (isQuickOrd) {
				QuickOrderPage quickOrd = (QuickOrderPage) obj;
				int qtytoUpdate = Integer.parseInt(qtyToUpdate);
				qty = quickOrd.selectQuantity(qtytoUpdate).trim();
				quickOrd.clickUpdateProduct();
			} else { 
				if(Utils.isDesktop()) {
					QuickShop qsModal = (QuickShop) obj;
					qty = qsModal.selectQty(qtyToUpdate).trim();
					BrowserActions.clickOnElement(qsModal.btnAddToBag, driver, "Update button");
				} else {
					PdpPage pdpPage = (PdpPage) obj;
					qty = pdpPage.selectQty(qtyToUpdate).trim();
					pdpPage.clickOnUpdate();
				}
			}
		} catch(NullPointerException ex) {
			return "";
		}
		
		Utils.waitUntilElementDisappear(driver, popUpBonusProduct);
		return qty;
	}
	
	/**
	 * To verify qty update arrow of product is disabled 
	 * @param prdID - Product to verify
	 * @param direction - Arrow direction to verify
	 * @return if arrow is disabled
	 * @throws Exception
	 */
	public boolean verifyDisabledQtyUpdateArrow(String prdID, Direction direction) throws Exception{
		WebElement prdToVerify = getCartProductByID(prdID);
		if(prdToVerify == null)
			return false;
		
		if(direction.equals(Direction.Up)) {
			WebElement qtyUp = prdToVerify.findElement(By.cssSelector(".qty-increase"));
			return qtyUp.getAttribute("class").contains("disabled");
			
		} else if ((direction.equals(Direction.Down))) {
			WebElement qtyDown = prdToVerify.findElement(By.cssSelector(".qty-decrease"));
			return qtyDown.getAttribute("class").contains("disabled");
		} else {
			Log.event(direction.toString()  + " is a not a valid parameter for this method.");
		}
		return false;
	}

	/**
	 * To get name list of products in shopping bag.	
	 * @return HashSet<String> - Names of products
	 * @throws Exception - Exception
	 */
	public HashSet<String> getCartItemNameList() throws Exception {
		HashSet<String> listCartItemNames = new HashSet<String>();
		String itemName;
		for(WebElement cartItem: lstCartProductNames) {
			itemName = cartItem.getText().trim().toLowerCase();
			Log.event("Product name from Cart: " + itemName);
			listCartItemNames.add(itemName);
		}
		return listCartItemNames;
	}
	
	/**
	 * To get product availability message
	 * @param productID - ID of product
	 * @return availability message of product
	 * @throws Exception
	 */
	public String getAvailabilityMessageByID(String productID) throws Exception {
		if(productID.contains("|"))
			productID = productID.split("\\|")[0];
		String availabilityText = "";
		if (divCartRow.size() > 0) {
			WebElement cartRow = getCartProductByID(productID);
			WebElement availability = cartRow.findElement(By.cssSelector(".product-availability-list span:not(.label)"));
			availabilityText = BrowserActions.getText(driver, availability, "availability message for cart product:: " + productID);
		} else {
			Log.event("Cart is empty.");
		}
		return availabilityText;
	}

	/**
	 * To verify that availability is not shown for OOS item	
	 * @return boolean- 
	 * @throws Exception - Exception
	 */
	public boolean verifyAvailabilityMessageForOOS() throws Exception {
		for(WebElement cartItem: divCartRow) {
			WebElement quantity = cartItem.findElement(By.cssSelector(".item-quantity"));
			WebElement availability = cartItem.findElement(By.cssSelector(".product-availability-list"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			String quantityText = (String) executor.executeScript("return window.getComputedStyle(arguments[0], '::before').getPropertyValue('content')", quantity);
			if(quantityText.contains("OUT OF STOCK") && availability.isDisplayed()) {
				return false;
			}
		}
		return true;
	}

	public void clickCloseModal() throws Exception {
		BrowserActions.clickOnElementX(btnCloseEditModal, driver, "Close edit modal");
	}

	public PlpPage clickShopNewArrivalButton() throws Exception {
		BrowserActions.scrollToTopOfPage(driver);
		if(Utils.getCurrentBrand().equals(Brand.wwx)) {
			BrowserActions.clickOnElementX(btnEmptyWhatsNewWWX, driver, "Shop whats new button");
		} else {
			BrowserActions.clickOnElementX(btnEmptyWhatsNew, driver, "Shop new Arrivals");
		}
		return new PlpPage(driver).get();
	}

	public PdpPage clickTrendNowImage() throws Exception {
		BrowserActions.clickOnElement(trendNowImage, driver, "Trend now Image");
		return new PdpPage(driver).get();
	}

	/**
	 * To verify quantity modification arrow position
	 * @param index - index(optional)
	 * @return boolean - true/false if arrow are positioned correctly
	 * @throws Exception - Exception
	 */
	public boolean verifyQuanityEditArrowposition(int  ...index)throws Exception {
		WebElement itemQty = null;
		if(index.length > 0) {
			itemQty = listProductQuantity.get(index[0]);
		}else {
			itemQty = listProductQuantity.get(Utils.getRandom(0, getProductCountInCart()-1));
		}
		WebElement arrowIncrease = itemQty.findElement(By.xpath("..")).findElement(By.cssSelector(".qty-increase"));
		WebElement arrowDecrease = itemQty.findElement(By.xpath("..")).findElement(By.cssSelector(".qty-decrease"));
		if(Utils.isMobile()) {
			if(BrandUtils.isBrand(Brand.rm) || (BrandUtils.isBrand(Brand.jl))) {
			return (BrowserActions.verifyHorizontalAllignmentOfElements(driver, arrowIncrease, itemQty)
					&& BrowserActions.verifyHorizontalAllignmentOfElements(driver, itemQty, arrowDecrease));
			}else {
			Log.message("As per the issue *SM-2631* for WW, El, KS sites, arrows should be displayed right side of the qty");
			return (BrowserActions.verifyVerticalAllignmentOfElements(driver, arrowIncrease, arrowDecrease)
					&& BrowserActions.verifyHorizontalAllignmentOfElements(driver, arrowIncrease, itemQty)
					&& BrowserActions.verifyHorizontalAllignmentOfElements(driver, arrowDecrease, itemQty));
			}
		}else {
			return (BrowserActions.verifyVerticalAllignmentOfElements(driver, arrowIncrease, arrowDecrease)
					&& BrowserActions.verifyHorizontalAllignmentOfElements(driver, arrowIncrease, itemQty)
					&& BrowserActions.verifyHorizontalAllignmentOfElements(driver, arrowDecrease, itemQty));
		}
	}


	/**
	 * To check quickshop is displayed in recently viewed
	 * @return boolean - true if Quickshop is displayed, false if not
	 * @throws Exception - Exception
	 */
	public boolean checkQuickShopIsDisplayedInRecommendation()throws Exception {
		BrowserActions.scrollInToView(tileRecommendationProd, driver);
		BrowserActions.mouseHover(driver, tileRecommendationProd, "Product Tile");
		WebElement qcButton = tileRecommendationProd.findElement(By.cssSelector("#quickviewbutton"));
		if (Utils.waitForElement(driver, qcButton)) {
			return true;
		}
		return false;
	}

	/**
	 * To click on quickshop button
	 * @return QuickShop -
	 * @throws Exception -
	 */
	public QuickShop clickOnQuickShop()throws Exception {		
		WebElement prdTile = null;
		prdTile = driver.findElements(By.cssSelector(".you-may-like.loaded .product-tile")).get(0);
		BrowserActions.scrollInToView(prdTile, driver);
		BrowserActions.mouseHover(driver, prdTile, "Product Tile");
		WebElement qcButton = prdTile.findElement(By.cssSelector(".quickview"));
		if (Utils.waitForElement(driver, qcButton)) {
			BrowserActions.javascriptClick(qcButton, driver, "Quick Shop Button for First Product");
		}
		return new QuickShop(driver).get();
	}
	
	/**
	 * To click Quick shop button in recommendation product
	 * @return Quick shop
	 * @throws Exception
	 */
	public QuickShop clickOnQuickShopButton() throws Exception {
		Utils.waitForElement(driver, btnQickShop);
		BrowserActions.clickOnElementX(btnQickShop, driver, "Quick shop button", false);
		
		return new QuickShop(driver).get();
	}
	/**
	 * To verify the all products have prices
	 * @returns true
	 * @throws Exception - Exception
	 */
	public boolean verifyPricesInRecommendations()throws Exception {
		int stdproduct=0, SaleProduct=0, productSize=tileListRecommendationProd.size();
		for(int product=0;product<productSize;product++) {
			tileListRecommendationProd.get(product);
			if(Utils.waitForElement(driver, prodStdPriceRecommendation)) {
				stdproduct=stdproduct+1;
			}else if(Utils.waitForElement(driver, prodSalePriceRecommendation)) {
				SaleProduct=SaleProduct+1; 
			}
		}
		Log.message("stdproduct; "+stdproduct+" SaleProduct"+ SaleProduct+" no of products: "+productSize);
		if(stdproduct==productSize) {
			Log.message("Products have only standard prices ");
			return true;
		}
		else if(SaleProduct==productSize) {
			Log.message("Products have only sale prices ");
			return true;
		}else if((stdproduct+SaleProduct)==productSize) {
			Log.message("Products have both sale price and standard price");
			return true;
		}
		return false;
	}
	
	/**
	 * Mouse hover on Instock recommmended product
	 * @return int - product index in the recommendation section
	 * @throws Exception 
	 */
	
	public int mousehoverOnInstockRecommedationImage() throws Exception {
		int prdIndex=0; String prdPrice;
		for(int index = 0; index < getNoOfProdDisplayInRecommendation(); index++) {
			BrowserActions.scrollInToView(prodImageListRecommendation.get(index), driver);
			prdPrice = BrowserActions.getText(driver, lstProdPriceRecommendation.get(index), "Recommendation Prod Name");
			if(prdPrice.contains("N/A")) {
				Log.event("Recommendation product is out of stock");
			} else {
				BrowserActions.mouseHover(driver, prodImageListRecommendation.get(index), "Recommedation prduct image");
				return prdIndex;
			}
		}
		return prdIndex;
	}
	/**
	 * To click on the In-stock product image in the recommendation section
	 * @return String - Product name of the selected recommendation product
	 * @throws Exception - Exception
	 */
	public String clickInStockRecommendationPrdImage() throws Exception {
		String prdName = null, prdPrice;
		for(int index = 0; index < getNoOfProdDisplayInRecommendation(); index++) {
			BrowserActions.scrollInToView(prodImageListRecommendation.get(index), driver);
			prdPrice = BrowserActions.getText(driver, lstProdPriceRecommendation.get(index), "Recommendation Prod Name");
			if(prdPrice.contains("N/A")) {
				Log.event("Recommendation product is out of stock");
			} else {
				prdName = BrowserActions.getText(driver, prodNameListRecommendation.get(index), "Recommendation Prod Name");
				BrowserActions.clickOnElementX(prodImageListRecommendation.get(index), driver, "Recommedation prduct image");
				break;
				}
		}
		Utils.waitForPageLoad(driver);
		return prdName.trim().replace("amp;", "");
	}
	
	/**
	 * To click on the product image in the recommendation section 
	 * @param index - index of the product
	 * @throws Exception - Exception
	 */
	public String clickRecommendationPrdImageByIndex(int index) throws Exception {
		String name;
		name = BrowserActions.getText(driver, prodNameListRecommendation.get(index), "Recommendation Prod Name");
		BrowserActions.clickOnElementX(prodImageListRecommendation.get(index), driver, "Recommedation prduct image");

		Utils.waitForPageLoad(driver);
		return name.trim().replace("amp;", "");
	}

	/**
	 * To get Shopping bag ID
	 * @return String - bagID
	 * @throws Exception - Exception
	 */
	public String getShoppingBagID()throws Exception {
		if(Utils.waitForElement(driver, lnkShowBagID)) {
			BrowserActions.clickOnElementX(lnkShowBagID, driver, "Show bag ID");
			Utils.waitForPageLoad(driver);
		}
		String bagID = BrowserActions.getText(driver, spanBagID, "Bag ID");
		if (bagID.contains(" "))
			bagID = bagID.split(" ")[1];
		
		return bagID;
	}

	/**
	 * To get order discount amount if available
	 * @return Order discount amount. Returns 0 if there is no order discount.
	 * @throws Exception
	 */
	public double getOrderDiscount() throws Exception {
		if(Utils.waitForElement(driver, divDicountTotal)) {
			double orderDiscount = Double.parseDouble(divDicountTotal.getText().split("\\$")[1]);
			Log.event("Order discount amount:: " + orderDiscount);
			return orderDiscount;
		}else {
			return 0d;
		}
	}
	
	/**
	 * To get cart row of product by product ID
	 * @param prdID - Product ID
	 * @return WebElement of cart row
	 * @throws Exception
	 */
	private WebElement getCartProductByID(String prdID) throws Exception {
		for (WebElement cartRow : sectionCartedProduct) {
			String cartRowID = BrowserActions.getTextFromAttribute(driver, cartRow, "data-itemid", "Product Id");
			if(cartRowID.equalsIgnoreCase(prdID)) {
				Log.event("Cart item found: " + cartRow.toString());
				return cartRow;
			}
		}
		Log.event(prdID + " is not in cart.");
		return null;
	}
	
	/**
	 * To get product index based on the given product id
	 * @return Returns product index in the cart items list
	 * @throws Exception
	 */
	public int getProductIndexByPrdID(String prdId)throws Exception {
		Utils.waitForPageLoad(driver);
		try {
			for(int prd=0;prd<sectionCartedProduct.size();prd++) {
				String value = BrowserActions.getTextFromAttribute(driver, sectionCartedProduct.get(prd), "data-itemid", "Product Id");
				if(value.equalsIgnoreCase(prdId)) {
					return prd;
				}
			}
		}catch(Exception e) {
			Log.event("Expected product not available in the cart.");
		}
		return 0;
	}
	
	/**
	 * To verify inventory message displayed below the quantity drop-down based on given product index
	 * @param prdID
	 * @return true, if inventory message and quantity aligned properly.
	 */
	public boolean verifyInventoryMsgBelowQty(String prdID) throws Exception{
		Utils.waitForPageLoad(driver);
		WebElement prdToVerify = getCartProductByID(prdID);
		try {
			WebElement elementQty, elementAvailability;
			if(Utils.isMobile()) {
				elementQty = prdToVerify.findElement(By.cssSelector(".cart-columns .item-details-and-qty .item-quantity"));
				elementAvailability = prdToVerify.findElement(By.cssSelector(".cart-columns .item-details-and-qty .availability-msg"));
			} else {
				elementQty = prdToVerify.findElement(By.cssSelector(".cart-columns>.columns-combine .item-quantity"));
				elementAvailability = prdToVerify.findElement(By.cssSelector(".cart-columns>.columns-combine .availability-msg"));
			}
			if(elementQty.getLocation().getY() < elementAvailability.getLocation().getY()) {
				return true;
			}
		}catch(Exception e) {
			Log.event("Expected element not available in the cart.");
		}
		return false;
	}
	
	/**
	 * To get product Brand from cart line items based on given product ID
	 * @param prdId - Product ID
	 * @return String of Current Brand short name
	 * @throws Exception - Exception
	 */
	public Brand getProductBrand(String prdId) throws Exception {
		String productId = null;
		String productURL = null;
		for(int i=0 ; i < sectionCartedProduct.size() ; i++) {
			WebElement element = sectionCartedProduct.get(i);
			productId = BrowserActions.getTextFromAttribute(driver, element, "data-itemid", "product");
			if(productId.equalsIgnoreCase(prdId)) {
				productURL = element.findElement(By.cssSelector(".item-image>a")).getAttribute("href");
				break;
			}
		}
		productURL = UrlUtils.getSharedCartRedirectURL(productURL);
		return UrlUtils.getBrandFromUrl(productURL);
	}
	
	/**
	 * To get the Product ID and Brand Name from cart line items.
	 * @return HashMap<String, Brand> - HashMap<productId, brandName> of cart line items
	 * @throws Exception
	 */
	public HashMap<String, Brand> getProductIdAndBrandName() throws Exception {
		HashMap<String, Brand> productDetails = new HashMap<String, Brand>();
		String productId = null;
		Brand brandName = null;
		for(int i=0 ; i < sectionCartedProduct.size() ; i++) {
			WebElement prdId = sectionCartedProduct.get(i);
			productId = BrowserActions.getTextFromAttribute(driver, prdId, "data-itemid", "Product ID");
			brandName = getProductBrand(productId);
			productDetails.put(productId, brandName);
		}
		return productDetails;
	}
	
	/**
	 * To get the Product variation ID and Brand Name from cart items.
	 * @return HashMap<String, Brand> - HashMap<productId, brandName> of cart items
	 * @throws Exception
	 */
	public HashMap<String, Brand> getProductVariationAndBrandName() throws Exception {
		HashMap<String, Brand> productDetails = new HashMap<String, Brand>();
		String productVariation = null;
		Brand brandName = null;
		for(int i=0 ; i < sectionCartedProduct.size() ; i++) {
			WebElement cartItem = sectionCartedProduct.get(i).findElement(By.cssSelector(".item-image>a"));;
			String prdURL = cartItem.getAttribute("href");
			prdURL = UrlUtils.getSharedCartRedirectURL(prdURL);
			productVariation = UrlUtils.getProductIDFromURL(prdURL);
			brandName = UrlUtils.getBrandFromUrl(prdURL);
			Log.event(productVariation + ":" + brandName.toString());
			productDetails.put(productVariation, brandName);
		}
		return productDetails;
	}
	
	/**
	 * To get the list of product id from the cart page line items
	 * @return HashSet<String> - HashSet<productId>
	 * @throws Exception
	 */
	public HashSet<String> getProductIdList() throws Exception {
		HashSet<String> productId = new HashSet<String>();
		String prdId = null;
		for(int i=0 ; i < sectionCartedProduct.size() ; i++) {
			WebElement element = sectionCartedProduct.get(i);
			prdId = BrowserActions.getTextFromAttribute(driver, element, "data-itemid", "product");
			productId.add(prdId);
		}
		return productId;
	}
	
	/**
	 * To get list of product ID in recently viewed section
	 * @return prdId - Recently viewed product ID.
	 * @throws Exception - Exception
	 */
	public List<String> getRecentlyViewedProdID()throws Exception {
		List<String> prdId = new ArrayList<String>();
		int size = lstRecentlyViewedProdTile.size();
		for(int i=0; i<size; i++) {
			prdId.add(BrowserActions.getTextFromAttribute(driver, lstRecentlyViewedProdTile.get(i), "data-itemid", "Product ID"));
		}
		return prdId;
	}
	
	/**
	 * To verify the gift card fee from shipping tool-tip overlay in the cart page.
	 * @return true - if gift card fee is displayed as expected
	 * <br>false - if gift card fee is not displayed as expected
	 * @throws Exception
	 */
	public boolean verifyGiftCardFeeInToolTip() throws Exception {
		String giftCardFeeExpected = demandwareData.get("gift_Card_Fee");
		DecimalFormat df = new DecimalFormat("#.##");
		Double giftCardPrice, singleGiftCardPrice, giftCardQty, subTotal, shippingTotal, discountPrice;
		giftCardPrice = singleGiftCardPrice = giftCardQty = subTotal = shippingTotal = discountPrice = 0.00;
		for(int i=0 ; i < lstSurchargeProductRate.size() ; i++) {
			String surchargeProductName = lstSurchargeProductRate.get(i).findElement(By.cssSelector(".label")).getText();
			if(surchargeProductName.equalsIgnoreCase("Gift Card Fee:")) {
				singleGiftCardPrice = Double. parseDouble(lstSurchargeProductRate.get(i).findElement(By.cssSelector(".value")).getText().split("\\$")[1]);
				giftCardQty =  Double.parseDouble(lstSurchargeProductRate.get(i).findElement(By.cssSelector(".value")).getText().split("\\$")[0].trim().replaceAll("x", ""));
				giftCardPrice = Double.parseDouble(df.format(giftCardQty * singleGiftCardPrice));
			} else {
				subTotal = Double.parseDouble( df.format((subTotal + Double.parseDouble(lstSurchargeProductRate.get(i).findElement(By.cssSelector(".value")).getText().split("\\$")[1]))) );
			}
		}
		subTotal = Double.parseDouble( df.format((subTotal + Double.parseDouble(txtShippingPriceInTooltip.getText().split("\\$")[1]) + giftCardPrice)) );
		try {
			if(discountPriceToolTip.isDisplayed() && discountPriceToolTip.getText().split("\\$")[0].contains("-")) {
				discountPrice = Double.parseDouble(discountPriceToolTip.getText().split("\\$")[1]);
				subTotal = Double.parseDouble( df.format((subTotal - discountPrice)));
			}
		} catch(NoSuchElementException e) {
			Log.event("Shipping discount is not applied");
		}
		
		shippingTotal = Double.parseDouble(totalFeeInTooltip.findElement(By.cssSelector(".value")).getText().split("\\$")[1]);
		if((subTotal.compareTo(shippingTotal) == 0) && (singleGiftCardPrice == Double.parseDouble(giftCardFeeExpected))) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * To update order total that meet PLCC discount
	 * @param prdId - product ID to update quantity.
	 * @throws Exception - Exception
	 */
	public void updateOrderTotalMeetPlccDiscount(String prdId) throws Exception {
		double subTotal = StringUtils.getPriceFromString(Double.toString(getOrderSubTotal()));
		int qty = 2;
		while(subTotal < 25.00) {
			if(!verifyArrowDisabled("up")) {
				updateQuantityByPrdID(prdId, Integer.toString(qty));
				subTotal = StringUtils.getPriceFromString(Double.toString(getOrderSubTotal()));
				qty++;
			} else {
				Log.warning("Product have less than " + qty + " quantity.");
				break;
			}
		}
	}
}