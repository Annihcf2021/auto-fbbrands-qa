package com.fbb.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PasswordResetPage;
import com.fbb.pages.ordering.GuestOrderStatusLandingPage;
import com.fbb.reusablecomponents.AccountUtils;
import com.fbb.reusablecomponents.GlobalNavigation;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.StringUtils;
import com.fbb.support.Utils;

public class SignIn extends LoadableComponent<SignIn> {

	private final WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	private static EnvironmentPropertiesReader demandWareProperty = EnvironmentPropertiesReader.getInstance("demandware");
	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	
	public enum UserAccount {
		FirstName, LastName, Email, Password, BirthMonth, CustomerID, EmailSubscription;
	}

	//***********************************************************
	//  	WebElement description - common
	//***********************************************************
	private final static String MYACCOUTLOGIN = "#primary .login-box.login-account ";
	@FindBy(css = MYACCOUTLOGIN)
	public static WebElement divLoginBox;
	
	@FindBy(css = MYACCOUTLOGIN + "#dwfrm_login")
	WebElement divLoginBoxContent;
	
	@FindBy(css = "#primary>.create-login .inner-block>.inner-block")
	WebElement divReturningCustomer;

	@FindBy(css = ".login-create")
	WebElement divCreateAccountBox;
	
	@FindBy(css = ".login-box.login-create-account")
	WebElement divCreateAccountBoxMobile;

	@FindBy(css = ".pt_account")
	WebElement readyElement;
	
	@FindBy(css = ".promo-banner")
	WebElement txtPLCCPromoBanner;

	@FindBy(css = ".pt_checkout")
	WebElement readyElementCheckOut;
	
	@FindBy(css = ".menu-category")
	WebElement divGlobalNavigation;

	@FindBy(css = ".username .label-text")
	WebElement lblEmailPlaceHolder;

	@FindBy(css = ".password .label-text")
	WebElement lblPasswordPlaceHolder;

	@FindBy(css = MYACCOUTLOGIN + "input[name*='dwfrm_login_username']")
	WebElement fldEmail;

	@FindBy(css = MYACCOUTLOGIN + "input[name*='dwfrm_login_password']")
	WebElement fldPassword;

	@FindBy(css = MYACCOUTLOGIN + ".password.required span.error")
	WebElement txtPasswordError;

	@FindBy(css = MYACCOUTLOGIN + ".username.required span.error")
	WebElement txtEmailError;

	@FindBy(css = MYACCOUTLOGIN + "button[name='dwfrm_login_login']")
	WebElement btnSignIn;
	
	@FindBy(css = "button[name='dwfrm_login_login']")
	WebElement tempButton;
	
	@FindBy(css = "button[name='dwfrm_login_register']")
	WebElement btnSubmitCreateAccount;

	@FindBy(css = ".error-form")
	WebElement txtSignInError;

	@FindBy(css = "#dwfrm_billing_billingAddress_email_emailAddress")
	WebElement txtGuestEmail;
	
	@FindBy(css = ".add-to-emaillist-warningmsg")
	WebElement txtNewsletterPopup;

	@FindBy(name = "dwfrm_login_unregistered")
	WebElement btnGuestLoginContinue;

	//checkout login
	@FindBy(css = ".login>.checkout-tab-head>h2")
	WebElement accountHeading;
	
	@FindBy(css = ".header-mian-banner")
	WebElement stickyNavigationBar;

	@FindBy(css = ".checkout-login-email")
	WebElement accountSubHeading;

	@FindBy(css = ".checkout-login-brands")
	WebElement checkoutBrands;

	@FindBy(css = ".login-account .heading")
	WebElement registeredUser;

	@FindBy(css = ".form-row.username .label-text")
	WebElement txtemail;
	
	@FindBy(css = ".username .input-focus span")
	WebElement lblLoginEmailPlaceholder;

	@FindBy(css = ".form-row.password .label-text")
	WebElement txtpassword;

	@FindBy(css = "#dwfrm_login_password_d0jltfawfakj-error")
	WebElement txtinvalidpassword;

	@FindBy(css = "#primary .login-account .pwd-show")
	WebElement showpassword;
	
	@FindBy(css = "fieldset > .reset-error-caption.invalid-email")
	WebElement errLoginErrorMsg;
	
	@FindBy(css = ".wrapper-forgot-password p")
    WebElement newSubHeadPasswordResetConfirmation;
	
	@FindBy(css = ".login-form .username.error-handle")
	WebElement errUsernameTxtBox;
	
	@FindBy(css = ".login-form .password.no-pwd-validate")
	WebElement errPasswordTxtBox;
	
	@FindBy(css = "span[id*='dwfrm_login_username'][class='error']")
	WebElement errorEmailAddresslabel;
	
	@FindBy(css = ".login-form .password .label-text")
	WebElement errorpasswordlabel;

	@FindBy(css = MYACCOUTLOGIN + "#dwfrm_login_rememberme")
	WebElement cbRememberme;
	
	@FindBy(css = ".setnewpassword")
	WebElement divForgotPassword;
	
	@FindBy(css = ".ui-widget.forgot-password")
	WebElement divForgotPasswordModal;

	@FindBy(css = ".setnewpassword input.email")
	WebElement emailTbRequestPassword;
	
	@FindBy(xpath = ".wrapper-forgot-password span")
	WebElement emailRequestForPasswordReset;
	
	@FindBy(css = ".wrapper-forgot-password p:nth-of-type(2)")
	WebElement subHeadPasswordResetConfirmation;

	@FindBy(css = ".form-row.username.username.required.error-handle span[id*='dwfrm_login_username_']")
	WebElement emailTbErrorRequestPassword;

	@FindBy(css = ".login-box-content.returning-customers.clearfix.make-label-absolute .form-row.username.username.required input[id*='dwfrm_login_username_']")
	WebElement txtForgotEmail;
	
	@FindBy(css = ".email.required #dwfrm_requestpassword_email")
	WebElement enteredForgetEmailID;
	
	@FindBy(css = "#PasswordResetForm .reset-error-caption.invalid-email")
	WebElement txtUnRegisteredEmailModal;
	
	@FindBy(css = ".reset-error-caption.invalid-email a")
	WebElement lnkUnRegisteredEmailError;
	
	@FindBy(css = ".create-login .reset-error-caption.invalid-email")
	WebElement UnRegisteredEmailError;

	@FindBy(css = ".wrapper-forgot-password h1")
	WebElement headingRequestPassword;
	
	@FindBy(css = ".setnewpassword .heading")
	WebElement headingRequestPasswordMobile;

	@FindBy(css = "button[name='dwfrm_requestpassword_send']")
	WebElement btnSendRequestPassword;
	
	@FindBy(css = ".wrapper-forgot-password")
	WebElement passwordRequestReceivedContainer;

	@FindBy(css = "button.ui-dialog-titlebar-close")
	WebElement closeForgotPasswordDialog;

	@FindBy(css = "#Facebook")
	WebElement facebookLink;

	@FindBy(css = ".create-inner-block.hide-mobile .create-account")
	WebElement btnCreateAccount;
	
	@FindBy (css = ".form-horizontal .form-row.form-row-button")
	WebElement btnSignUp;

	@FindBy(css = ".inner-block.display-mobile .create-account")
	WebElement btnCreateAccountMobile;
	
	@FindBy(css = ".or-section")
	WebElement lblOrSection;

	@FindBy(css = ".form-horizontal .form-row.form-row-button button")
	WebElement btnRegisterCreateAccount;

	@FindBy(css = ".primary-content .login-box.login-account #dwfrm_login #password-reset")
	WebElement lnkForgotPwd;

	@FindBy(css = ".col-2 .login-box-content.clearfix h1")
	WebElement txtHeaderElement;

	@FindBy(css = ".login-box-content.returning-customers.clearfix.make-label-absolute a[id='password-reset']")
	WebElement btnPasswordResetSubmit;

	@FindBy(css = ".col-2 .login-box-content.clearfix h2")
	WebElement txtSubHeaderElement;

	@FindBy(css = ".wrapper-forgot-password .reset-error-caption")
	WebElement passwordFieldError;
	
	@FindBy(css = "#PasswordResetForm .reset-error-caption")
	WebElement passwordFieldErrorMobile;

	@FindBy(css = ".wrapper-forgot-password")
	WebElement forgotPasswordConfirmationPage;
	
	@FindBy(css = ".ui-dialog .dialog-content")
	WebElement subHeadRequestPasswordMobile;
	
	@FindBy(css = ".wrapper-forgot-password p")
	WebElement subHeadRequestPasswordModal;
	
	@FindBy(css = ".setnewpassword p")
	WebElement subHeadRequestPasswordModalMobile;
	
	@FindBy(id = "dwfrm_profile_customer_firstname-error")
	WebElement txtFirstNameMandatoryError;

	@FindBy(id = "dwfrm_profile_customer_lastname-error")
	WebElement txtLastNameMandatoryError;

	@FindBy(id = "dwfrm_profile_customer_email-error")
	WebElement txtEmailMandatoryError;

	@FindBy(id = "dwfrm_profile_customer_emailconfirm-error")
	WebElement txtConfirmEmailMandatoryError;

	@FindBy(xpath = "//span[contains(@id, 'dwfrm_profile_login_password_')]")
	WebElement txtPasswordMandatoryError;

	@FindBy(xpath = "//span[contains(@id, 'dwfrm_profile_login_passwordconfirm_')]")
	WebElement txtConfirmPasswordMandatoryError;

	@FindBy(css = "#dwfrm_profile_customer_emailconfirm-error")
	WebElement confirmEmailMissmatchError;

	@FindBy(css = "span[id*='dwfrm_profile_login_passwordconfirm']")
	WebElement confirmPasswordMissmatchError;

	@FindBy(id = "dwfrm_profile_customer_email")
	WebElement txtEmail;
	
	@FindBy(css = ".form-row.lastname.required.show-text .input-focus .label-text")
	WebElement txtLastNamePlaceHolder;

	@FindBy(css = ".form-row.firstname.required.show-text .input-focus .label-text")
	WebElement txtFirstNamePlaceHolder;

	@FindBy(css = ".form-row.password.required.show-text .input-focus .label-text")
	WebElement txtPasswordPlaceHolder;

	@FindBy(css = ".form-row.password.required.show-text .form-caption")
	WebElement txtPasswordMessage;

	@FindBy(css = ".checkout-order-summary")
	WebElement divCheckoutLoginOrderSummery;
	
	@FindBy(css = ".items-in-bag")
	WebElement divItemsInBagSection;
	
	@FindBy(css = ".align-left")
	WebElement lblSignedInAsHeading;
	
	@FindBy(css = ".welcome-check")
	WebElement lblWelcomeEmail;


	//***********************************************************
	//  	WebElement description - Mobile
	//***********************************************************

	@FindBy(css = "#primary #password-reset")
	WebElement lnkForgetPasswordMobile;

	@FindBy(css = "#dialog-container")
	WebElement forgotPwdSuccessMessageContainer;

	@FindBy(css = ".form-row.email.required .input-focus .label-text")
	WebElement emailPlaceHolder;

	@FindBy(css = ".form-row.emailconfirm.required .input-focus .label-text")
	WebElement confirmEmailPlaceHolder;

	@FindBy(css = ".form-row.passwordconfirm.required .input-focus .label-text")
	WebElement confirmPasswordPlaceHolder;
	
	@FindBy(css = "#secondary .checkout-order-summary .checkout-order-totals")
	WebElement checkoutOrderTotalTable;

	@FindBy(css = ".form-row.customerID .label-text")
	WebElement customerIDPlaceHolder;

	@FindBy(css = ".breadcrumb-element.current-element.hide-mobile")
	WebElement breadcrumbValue;

	@FindBy(css = "div.breadcrumb")
	WebElement breadCrumb;

	@FindBy(css = ".breadcrumb .breadcrumb-element")
	List<WebElement> lstTxtProductInBreadcrumb;

	@FindBy(css = ".breadcrumb-desktop-label")
	WebElement breadcrumbMobile;

	@FindBy(css = ".breadcrum-device a")
	WebElement txtProductInBreadcrumbMobile;
	
	@FindBy(css = ".breadcrumb .back-arrow")
	WebElement backArrowBreadcrumbMobile;

	@FindBy(css = ".login-box.login-account>h2")
	WebElement customerSubHeading;

	@FindBy(css = ".login-box.login-account h1")
	WebElement loginCustomerHeading;

	@FindBy(css = ".login-box.login-account h2")
	WebElement loginCustomerSubHeading;

	@FindBy(css = ".col-2 .login-box-content.clearfix .login-box-content.clearfix.make-label-absolute.view-more-hide")
	WebElement createAccountSection;
	
	@FindBy(css = ".login-create>h1")
	WebElement createAccountHeading;
	
	@FindBy(css = ".login-box-content h1")
	WebElement createAccountHeadingMobile;

	@FindBy(css = ".login-create>h2")
	WebElement createAccountSubHeading;
	
	@FindBy(css = ".login-box-content h2")
	WebElement createAccountSubHeadingMobile;

	@FindBy(css = ".legal")
	WebElement accountBenefitSection;
	
	@FindBy(css = ".benfits-content")
	WebElement createBenifitsContent;
	
	@FindBy(css = "div.display-mobile>div>div>.benfits-content")
	WebElement createBenifitsContentMobile;

	@FindBy(css = ".view-more.hide-desktop.hide-tablet")
	WebElement lnkViewMoreMobile;

	@FindBy(css = "div[class='login-box-content returning-customers clearfix make-label-absolute'] .pwd-show")
	WebElement passwordShowHide;

	@FindBy(id = "dwfrm_profile_customer_firstname")
	WebElement txtFirstName;

	@FindBy(css = "#RegistrationForm")
	WebElement formFieldSignUp;
	
	@FindBy(css = ".forgot-password.ui-dialog .forgot-password")
	WebElement mdlForgetPassword;

	@FindBy(id = "dwfrm_profile_customer_lastname")
	WebElement txtLastName;

	@FindBy(css = "#dwfrm_requestpassword_resetemail")
	WebElement txtEmailInForgotPassword;

	@FindBy(id = "dwfrm_profile_customer_email")
	WebElement txtEmailId;

	@FindBy(id = "dwfrm_profile_customer_emailconfirm")
	WebElement txtConfirmEmailId;

	@FindBy(xpath = "//input[contains(@id, 'dwfrm_profile_login_password_')]")
	WebElement txtpasswordField;

	@FindBy(xpath = "//input[contains(@id, 'dwfrm_profile_login_passwordconfirm_')]")
	WebElement txtConfirmPasswordField;

	@FindBy(css = ".col-2 .oAuthIcon")
	WebElement registerFacebookBtn;

	@FindBy(css = ".selection-list li")
	List<WebElement> monthDropDownValues;

	@FindBy(css = ".birthMonth .custom-select")
	WebElement selectMonthDrop;

	@FindBy(css = ".input-select.valid")
	WebElement dropDownSelectedValue;
	
	@FindBy(css = ".selected-option")
	WebElement dropMonth;

	@FindBy(css = ".birthMonth .form-caption")
	WebElement BirthMonthMessage;

	@FindBy(id = "dwfrm_profile_customer_customerID")
	WebElement txtCustomerID;

	@FindBy(css = ".form-row.customerID .form-caption")
	WebElement customerIdMessage;

	@FindBy(id = "dwfrm_profile_customer_addtoemaillist")
	WebElement checkNewsletters;

	@FindBy(xpath = "//input[contains(@id, 'dwfrm_profile_login_passwordconfirm_')]")
	WebElement txtOR;

	@FindBy(xpath = "//div[contains(@class, 'form-row  password required')]/div[@class='field-wrapper']/button")
	WebElement lnkPasswordShow;
	
	@FindBy(css = ".input-checkbox.deals-trends-mail")
	WebElement chkGuestNewsletterSignUp;

	@FindBy(css = ".legal")
	WebElement txtLegalCopyMessage;

	@FindBy(css = "#main .login-box.login-account")
	WebElement divLoginBoxDesktop;

	@FindBy(css = ".pt_account:not(.null)")
	WebElement readyElementMyAccountPage;

	@FindBy(css = "span[id='dwfrm_requestpassword_email-error']")
	WebElement lblErrorEmailInForgotPassword;

	@FindBy(css = "button[name='dwfrm_requestpassword_send']")
	WebElement btnSendInForgotPassword;
	
	@FindBy(css = ".close-button")
	WebElement btnCancelForgotPassword;
	
	@FindBy(css = ".back-to-login")
	WebElement lnkBackToLoginMobile;

	@FindBy(css = ".ui-dialog.forgot-password .ui-dialog-titlebar-close")
	WebElement btnCloseForgotPasswordMdl;

	//   Guest user order status
	@FindBy(css = ".login-box.login-order-track")
	WebElement divOrderStatusBox;
	
	@FindBy(css = ".login-box.login-order-track h2")
	WebElement lblCheckAnOrder;

	@FindBy(css = ".login-box-content.clearfix.make-label-absolute p")
	WebElement subHeadTxt;

	@FindBy(name = "dwfrm_ordertrack_findorder")
	WebElement btnCheckInOrder;
	
	@FindBy(css = "a.orderLookupToggle.lookUpButton")
	WebElement btnLookUpBeforeToggle;
	
	@FindBy(css = ".buttonCancel")
	WebElement btnCancelAfterToggle;
	
	@FindBy(css = ".trackingnumber.hide-mobile")
	WebElement lnkTrackMyPackage;
	
	@FindBy(xpath = "//a[contains(@class,'continue' )]")
	WebElement btnReturn;
	
	@FindBy(css = ".orderLookupNarvar .orderLookupToggle")
	WebElement btnNarvarOrderLookupToggle;
	
	@FindBy(css = ".orderLookupNarvar a[href*='returns']")
	WebElement btnNarvarReturn;

	@FindBy(id = "dwfrm_ordertrack_orderNumber")
	WebElement txtFldOrderNumber;

	@FindBy(id = "dwfrm_ordertrack_orderEmail")
	WebElement txtFldOrderEmail;

	@FindBy(id = "dwfrm_ordertrack_postalCode")
	WebElement txtFldBillingZipcode;

	@FindBy(css = "button[name='dwfrm_ordertrack_findorder']")
	WebElement checkorderstatusbtn;

	@FindBy(css = ".orderdetails")                  
	WebElement lnkguestOrderStatusLandingPage;
	
	@FindBy(css = ".order-number")
	WebElement divOrderNumberPrint;
	
	@FindBy(css = ".order-number .value")
	WebElement orderNumberValue;
	
	@FindBy(css = ".internation-shipment-method")
	WebElement internationalOrderPageBanner;
	
	@FindBy(css = ".shipping-method a")
	WebElement internationalBannerLink;

	@FindBy(css = ".pt_account .create-login .inner-block .login-box .error-form")                  
	WebElement nullOrderNoErrormsg;
	
	@FindBy(css = ".error-form")                  
	WebElement orderNoErrorMsg;

	@FindBy(id = "dwfrm_ordertrack_orderNumber-error")                  
	WebElement orderNoInvalidErrorMsg;

	@FindBy(id = "dwfrm_ordertrack_orderEmail-error")                  
	WebElement orderEmailErrorMsg;

	@FindBy(id = "dwfrm_ordertrack_postalCode-error")                  
	WebElement orderPostalZipCodeErrorMsg;
	
	@FindBy(css = ".form-row.postalCode.required.error-handle .input-focus .error")                  
	WebElement orderInvalidZipCodeErrorMsg;
	
	@FindBy(css = "label[for='dwfrm_ordertrack_orderNumber'] .label-text")
	WebElement placeHolderOrderNumber;

	@FindBy(css = "label[for='dwfrm_ordertrack_orderEmail'] .label-text")
	WebElement placeHolderOrderEmail;

	@FindBy(css = "label[for='dwfrm_ordertrack_postalCode'] .label-text")
	WebElement placeHolderZipCode;

	@FindBy(css = ".error-form.ordererror")
	WebElement orderErrorMsg;

	@FindBy(css = ".pt_order")
	WebElement lnkGuestorderStatusPage;
	
	@FindBy(css = ".mini-address-name")
	WebElement txtMiniAddressName;
	
	@FindBy(css = ".address .default")
	WebElement txtShippingAddressName;
	
	@FindBy(css = ".mail-box")
	WebElement icoEmailBox;
	
	@FindBy(css = ".receipt-message")
	WebElement txtReceiptMessage;
	
	@FindBy(css = ".order-detail-section")
	WebElement divOrderDetails;
	
	@FindBy(css = ".view-more-link")
	WebElement lnkViewDetails;
	
	@FindBy(css = ".view-more-link.opened")
	WebElement lnkViewLess;
	
	@FindBy(css = ".order-shipments")
	WebElement divOrderShipment;
	
	private static final String SHIPMENT = ".order-shipment-table ";
	
	@FindBy(css = SHIPMENT)
	WebElement divShipment;
	
	@FindBy(css = SHIPMENT + ".shipment-address .heading")
	WebElement divShipmentHeading;
	
	@FindBy(css = SHIPMENT + ".shipment-address .address")
	WebElement divShipmentAddress;
	
	@FindBy(css = SHIPMENT + ".order-shipping-status .value")
	WebElement divShipmentStatus;
	
	@FindBy(css = SHIPMENT + ".item-details")
	WebElement divShipmentDetails;
	
	@FindBy(css = SHIPMENT + ".line-item-quantity:not(.hide-desktop) .qty-value")
	WebElement divShipmentQuantity;
	
	@FindBy(css = SHIPMENT + ".line-item-quantity.attribute .qty-value")
	WebElement divShipmentQuantityMobile;
	
	@FindBy(css = SHIPMENT + ".hide-mobile .specialmessaging")
	WebElement divShipmentSpecialMessage;
	
	@FindBy(css = SHIPMENT + ".details-review a")
	WebElement lnkWriteReview;
	
	@FindBy(css = SHIPMENT + ".buy-again a")
	WebElement lnkBuyAgain;
	
	@FindBy(css = ".order-detail-summary")
	WebElement divOrderSummery;
	
	@FindBy(css = ".tax-disclaimer")
	WebElement divTaxDisclaimer;

	//***********************************************************
	//***********************************************************
	/**
	 * Constructor class for Login page Here we initializing the driver for page
	 * factory objects. For ajax element waiting time has added while
	 * initialization
	 * @param driver - WebDriver
	 */
	public SignIn(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver,
				Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {

		if (!isPageLoaded) {
			Assert.fail();
		}

		if (isPageLoaded && !(Utils.waitForElement(driver, divLoginBox))) {
			Log.fail("SignIn page didn't open up", driver);
		}
		try {
			GlobalNavigation.closePopup(driver);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get page load status
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}
	
	/**
	 * To click on the signIn button
	 * @return Object - If navigated to Checkout page it will return checkout page object
	 * @throws Exception - Exception
	 */
	public Object clickSignInButton() throws Exception {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].style.opacity=1", btnSignIn);

		BrowserActions.clickOnElementX(btnSignIn, driver, "SignIn Button In Sign in page ");
		Utils.waitForPageLoad(driver);
		if (Utils.waitForElement(driver, readyElementMyAccountPage))
			return new MyAccountPage(driver).get();
		else if (Utils.waitForElement(driver, readyElementCheckOut))
			return new CheckoutPage(driver).get();
		else
			return this;
	}


	/**
	 * To login into Account upon a successful login
	 * @param username - User Email ID
	 * @param password - User Password
	 * @return MyAccountPage - on a successful login 
	 * @throws Exception - Exception
	 */
	public MyAccountPage navigateToMyAccount(String username, String password)throws Exception{
		BrowserActions.refreshPage(driver);
		BrowserActions.typeOnTextField(fldEmail, username, driver, "Email Address ");
		BrowserActions.typeOnTextField(fldPassword, password, driver, "Password ");
		Utils.waitForElement(driver, btnSignIn);
		BrowserActions.clickOnElementX(btnSignIn, driver, "Login ");

		if(Utils.waitForElement(driver, tempButton))
			BrowserActions.clickOnElementX(tempButton, driver, "Sign In button");
		Utils.waitForPageLoad(driver);

		return new MyAccountPage(driver).get();
	}
	
	/**
	 * To login to account by given credentials
	 * @param email
	 * @param password
	 * @param rememberMe
	 */
	public Object loginAccount(String email, String password, boolean ...rememberMe) throws Exception {
		typeOnEmail(email);
		typeOnPassword(password);
		if (rememberMe.length > 0) {
			checkRememberMe(rememberMe[0]);
		}
		Object obj = clickSignInButton();
		GlobalNavigation.saveCredentialInDataSheet(driver, email, readyElementMyAccountPage);
		return obj;
	}
	
	/**
	 * To create an account with given account details
	 * @param userDetails - HashHashMap<UserAccount, String> storing account details.
	 * <br> For null value in a userDetails.get(key)
	 * <br> Required value will be filled with default value, and optional values will not be set
	 * @throws Exception
	 */
	public Object createAccount(HashMap<UserAccount, String> userDetails) throws Exception {
		String firstName = userDetails.get(UserAccount.FirstName) != null ? userDetails.get(UserAccount.FirstName) : "Hello";
		String lastName = userDetails.get(UserAccount.LastName) != null ? userDetails.get(UserAccount.LastName) : "Welcome";
		String email = userDetails.get(UserAccount.Email) != null ? userDetails.get(UserAccount.Email) : AccountUtils.generateEmail(driver);
		String password = userDetails.get(UserAccount.Password) != null ? userDetails.get(UserAccount.Password) : accountData.get("password_global");
		String birthMonth = userDetails.get(UserAccount.BirthMonth) != null ? userDetails.get(UserAccount.BirthMonth) : null;
		String custID = userDetails.get(UserAccount.CustomerID) != null ? userDetails.get(UserAccount.CustomerID) : null;
		String emailSub = userDetails.get(UserAccount.EmailSubscription) != null ? userDetails.get(UserAccount.EmailSubscription) : null;
		
		clickOnCreateAccount();
		BrowserActions.typeOnTextField(txtFirstName, firstName, driver, "first name");
		BrowserActions.typeOnTextField(txtLastName, lastName, driver, "last name");
		BrowserActions.typeOnTextField(txtEmail, email, driver, "email");
		BrowserActions.typeOnTextField(txtConfirmEmailId, email, driver, "confirm email");
		BrowserActions.typeOnTextField(txtpasswordField, password, driver, "passowrd");
		BrowserActions.typeOnTextField(txtConfirmPasswordField, password, driver, "confirm password");
		if (birthMonth != null) {
			selectMonth(birthMonth);
		}
		if (custID != null) {
			BrowserActions.typeOnTextField(txtCustomerID, custID, driver, "Customer ID");
		}
		if (emailSub != null) {
			clickOnNewsletterCheckBox(emailSub.equalsIgnoreCase("checked"));
		}
		return clickOnConfirmRegisterBtn();
	}

	/**
	 * To enter the given email id into email field
	 * @param email - User Email ID
	 * @throws Exception - Exception
	 */
	public void typeOnEmail(String email)throws Exception{
		fldEmail.clear();
		BrowserActions.typeOnTextField(fldEmail, email, driver, "Email field");
	}
	
	/**
	 * To get the value entered in the email field
	 * @return Value entered in the Email field
	 * @throws Exception -
	 */
	public String getValueEnteredInEmailAddress()throws Exception{
		return BrowserActions.getTextFromAttribute(driver, fldEmail, "value", "Email");
	}
	
	/**
	 * To enter the given password in Password field
	 * @param password - Password
	 * @throws Exception - Exception
	 */
	public void typeOnPassword(String password)throws Exception{
		fldPassword.clear();
		BrowserActions.typeOnTextField(fldPassword, password, driver, "Password field");
	}

	/**
	 * To clear both username and password fields
	 * @throws Exception - Exception
	 */
	public void clearUserNamePassword()throws Exception{
		fldEmail.clear();
		fldPassword.clear();
	}

	/**
	 * To click on forget password password link
	 * @return PasswordResetPage object
	 * @throws Exception - Exception
	 */
	public PasswordResetPage clickOnForgetPassword()throws Exception{
		BrowserActions.clickOnElementX(lnkForgetPasswordMobile, driver, "Forget Password Link ");
		return new PasswordResetPage(driver).get();
	}
	
	/**
	 * To click on Track My Package link
	 * @return vaid
	 * @throws Exception - Exception
	 */
	public void clickOnTrackMyPackage()throws Exception{
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		if (Utils.waitForElement(driver, lnkTrackMyPackage)) {
			BrowserActions.clickOnElementX(lnkTrackMyPackage, driver, "Track My Package link ");
			Utils.waitForPageLoad(driver);
			driver.switchTo().window(tabs.get(0));
		} else {
			Log.event("Order tracking not available.");
			if(BrowserActions.getText(driver, divShipmentStatus, "Shipment status").toLowerCase().contains("Not")) {
				Log.event("Order not shipped yet.");
			}
		}
	}
	
	/**
	 * To click on Return button
	 * @return ReturnOrderPage object
	 * @throws Exception - Exception
	 */
	public ReturnOrderPage clickOnReturn()throws Exception{
		if(Utils.waitForElement(driver, btnReturn)) {
			BrowserActions.clickOnElementX(btnReturn, driver, "Return button ");
			return new ReturnOrderPage(driver).get();
		} else {
			Log.event("Order return not available.");
			return null;
		}
	}

	/**
	 * To click on forget password password link
	 * @throws Exception - Exception
	 */
	public void clickOnForgetPassword1()throws Exception{
		BrowserActions.clickOnElementX(lnkForgetPasswordMobile, driver, "Forget Password Link ");
		Utils.waitForElement(driver, mdlForgetPassword);
	}

	/**
	 * To type guest email as per the parameter
	 * @param email -
	 * @throws Exception - Exception
	 */
	public void typeGuestEmail(String email)throws Exception{
		BrowserActions.typeOnTextField(txtGuestEmail, email, driver, "Guest Email");
	}
	
	/**
	 * To select or deselect the checkbox of newsletter in guest login section
	 * @return boolean - true if selected
	 * @throws Exception- exception
	 */
	public void selectNewsLetterSignUpCheckbox(boolean state) throws Exception {
		if(state) {
			BrowserActions.selectRadioOrCheckbox(chkGuestNewsletterSignUp, driver, "YES");	
		}else {
			BrowserActions.selectRadioOrCheckbox(chkGuestNewsletterSignUp, driver, "NO");			
		}
		Utils.waitForPageLoad(driver);				
	}
	
	/**
	 * To Click on the continue button in the guest log in page
	 * @return CheckoutPage object
	 * @throws Exception - Exception
	 */
	public CheckoutPage clickOnContinueInGuestLogin()throws Exception{
		BrowserActions.clickOnElementX(btnGuestLoginContinue, driver, "Continue");
		return new CheckoutPage(driver).get();
	}
	
	/**
	 * To checkout as guest in checkout log-in page
	 * @param guestEmail - Guest email address
	 * @return instance of CheckoutPage
	 * @throws Exception
	 */
	public CheckoutPage checkoutAsGuest(String guestEmail) throws Exception{
		typeGuestEmail(guestEmail);
		return clickOnContinueInGuestLogin();
	}

	/**
	 * To verify email and password fields are editable
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public  boolean verifyEmailAndPasswordFielsareEditable() throws Exception{
		if(fldEmail.isEnabled()&&fldPassword.isEnabled()){
			BrowserActions.typeOnTextField(fldEmail, "automation@yopmail.com", driver, "Typed mail id");
			BrowserActions.typeOnTextField(fldPassword, "test@123", driver, "Typed password");
		    if (BrowserActions.getTextFromAttribute(driver, fldEmail, "value", "Email field").equals("automation@yopmail.com") &&
		    BrowserActions.getTextFromAttribute(driver, fldPassword, "value", "Password field").equals("test@123")) {
		    	return true;
		    }
		}
		return false;
	}
	
	public Boolean verifyEmailandPasswordFieldsAreEditable()throws Exception {
		Boolean flag = true;
		Boolean temp1,temp2;
		temp1=temp2=true;
		List<String> fieldnames = new ArrayList<String>();		
		List<WebElement> ListEditablefields=driver.findElements(By.cssSelector("input[id^='dwfrm_login_']:not([readonly='readonly'])"));
		int temp=0;
		for (int i = 0; i < ListEditablefields.size(); i++) {
			String tempp=ListEditablefields.get(i).getAttribute("data-required-text");
			fieldnames.add(tempp);
			if(ListEditablefields.get(i).getAttribute("data-required-text").contains("Email Address"))
			{
				String oldvalue1=ListEditablefields.get(0).getAttribute("value");
				ListEditablefields.get(0).sendKeys("automation@yopmail.com");
				String newvalue1=ListEditablefields.get(0).getAttribute("value");
				ListEditablefields.get(0).clear();
				if(oldvalue1.equals(newvalue1))
				{
					temp1=false;
				}
				temp=temp+1;
			}
			if(ListEditablefields.get(i).getAttribute("data-required-text").contains("Password"))
			{
				String oldvalue2=ListEditablefields.get(1).getAttribute("value");
				ListEditablefields.get(1).sendKeys("password");
				String newvalue2=ListEditablefields.get(1).getAttribute("value");
				ListEditablefields.get(1).clear();
				if(oldvalue2.equals(newvalue2))
				{
					temp2=false;
				}
				temp=temp+1;
			}
			if(temp1&&temp2)
			{
				flag=true;
			}
			if(temp==2)break;

		}		
		return flag;
	}

	/**
	 * To verify email and password are mandatory
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailandPasswordareMandatory()throws Exception {
		Boolean flag = true;
		String[] fields= {"First Name","Last Name","Address Line1","Zip Code","State","City","Phone Number"};
		List<WebElement> ListEditableTextfields=driver.findElements(By.cssSelector("span[id^=dwfrm_billing].error")); 

		for (int i = 0; i < ListEditableTextfields.size(); i++) {
			if (ListEditableTextfields.get(i).getAttribute("innerHTML").equals(fields[i])) 
			{
				flag=Utils.verifyCssPropertyForElement(ListEditableTextfields.get(i), "color", "rgb(230, 0, 60)");
				if(flag)
				{
					Log.message(ListEditableTextfields.get(i)+" is mandatory field");
				}
				else
				{
					Log.message(ListEditableTextfields.get(i)+" is not mandatory field");
				}
			}

		}
		return flag;
	}

	/**
	 * To verify the email placeholder is moving to the top
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyEmailPlaceholderMovestotheTop()throws Exception {
		Boolean flag = true;

		float y1=txtemail.getLocation().getY();
		typeOnEmail("email@gmail.com");
		float y2=txtemail.getLocation().getY();
		if(y2>y1)
			flag=true;
		return flag;
	}

	/**
	 * To verify the password placeholder is moving to the top
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPasswordPlaceholderMovestotheTop()throws Exception {
		Boolean flag = true;

		float y1=txtpassword.getLocation().getY();
		typeOnEmail("Password");
		float y2=txtpassword.getLocation().getY();
		if(y2>y1)
			flag=true;
		return flag;
	}

	/**
	 * To verify the mask status of password 
	 * @param type -
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public Boolean verifyPasswordisMaskedorUnmasked(String type)throws Exception {
		Boolean flag = true;
		if(type.equals("mask"))
		{if(fldPassword.getAttribute("type").equals("password"))
			flag=true;}
		if(type.equals("unmask"))
		{
			if(fldPassword.getAttribute("type").equals("text"))
				flag=true;
		}
		return flag;
	}

	/**
	 * To click show password element
	 * @throws Exception - Exception
	 */
	public void clickShowPassword()throws Exception {
		Utils.waitForElement(driver, showpassword);
		BrowserActions.javascriptClick(showpassword, driver, "Show password");
	}

	/**
	 * To type email in request password field
	 * @param email -
	 * @throws Exception - Exception
	 */
	public void typeEmailaddrsinRequestPassword(String email)throws Exception {			
		BrowserActions.typeOnTextField(emailTbRequestPassword, email, driver, "Email Textbox");
	}

	/**
	 * To click on send button in password reset
	 * @throws Exception -
	 */
	public void clickSendRequestPassword()throws Exception {
		Utils.waitForElement(driver, btnSendRequestPassword);
		BrowserActions.javascriptClick(btnSendRequestPassword, driver, "Send Request");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on close button in forget password dialog
	 * @throws Exception -
	 */
	public void clickcloseForgotPasswordDialog()throws Exception {
		Utils.waitForElement(driver, closeForgotPasswordDialog);
		BrowserActions.javascriptClick(closeForgotPasswordDialog, driver, "Forgot Password");
	}

	/**
	 * To click on Forget password link
	 * @throws Exception -
	 */
	public void clickForgotPwdLink()throws Exception {
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver, lnkForgotPwd);
		if(!Utils.isMobile()) {
			BrowserActions.clickOnElementX(lnkForgotPwd, driver, "Forgot Pwd link", false);
		} else {
			BrowserActions.clickOnElementX(lnkForgotPwd, driver, "Forgot Pwd link");
		}
		Utils.waitForElement(driver, mdlForgetPassword);

	}
	
	public void enterForgotPasswordEmailID(String emailID) throws Exception{
		BrowserActions.clickOnElementX(txtEmailInForgotPassword, driver, "Clicked email field");
		BrowserActions.typeOnTextField(txtEmailInForgotPassword, emailID, driver, "Email id");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on facebook link
	 * @throws Exception -
	 */
	public void clickFacebooklink()throws Exception {
		Utils.waitForElement(driver, facebookLink);
		facebookLink.click();

	}

	/**
	 * To click on create account button
	 * <br> Opens new customer form
	 * @throws Exception
	 */
	public void clickOnCreateAccount()throws Exception{	
		if (!Utils.waitForElement(driver, btnSubmitCreateAccount)) {
			if (!Utils.isMobile()) {
				BrowserActions.clickOnElementX(btnCreateAccount, driver, "Create Account Button");
			} else {
				BrowserActions.scrollToView(btnCreateAccountMobile, driver);
				BrowserActions.clickOnElementX(btnCreateAccountMobile, driver, "Create Account Button");
			}			
		}
	}

	/**
	 * To click on submit button in forget password dialog
	 * @throws Exception -
	 */
	public void clickForgotPwdSubmit()throws Exception {
		if(!Utils.isMobile()) {
			BrowserActions.clickOnElementX(btnPasswordResetSubmit, driver, "Forgot Pwd Submit");
		} else {
			BrowserActions.clickOnElementX(btnPasswordResetSubmit, driver, "Forgot Pwd Submit");
		}
	}

	/**
	 * To enter email id in the forgot email field
	 * @param emailID -
	 * @throws Exception - Exception
	 */
	public void enterEmailID(String emailID) throws Exception{
		BrowserActions.clickOnElementX(txtForgotEmail, driver, "Clicked email field");
		txtForgotEmail.sendKeys(emailID);
		txtForgotEmail.sendKeys(Keys.TAB);
		Utils.waitForElement(driver, emailPlaceHolder);
	}
	
	/**
	 * To Check/UnCheck the 'Remember Me' Checkbox 
	 * @param needCheck - True to check the Remember me Checkbox else false
	 * @throws Exception -
	 */
	public void checkOnRememberMeMobile(boolean needCheck)throws Exception{
		if(needCheck) {
			if(!(cbRememberme.isSelected())) {
				BrowserActions.clickOnElementX(cbRememberme, driver, "Sign In Button");
			}
		} else {
			if(cbRememberme.isSelected()) {
				BrowserActions.clickOnElementX(cbRememberme, driver, "Sign In Button");
			}
		}
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To get email entered in forget password modal
	 * @return String -
	 * @throws Exception -
	 */
	public String getEnteredEmailID() throws Exception{
		return BrowserActions.getText(driver, txtEmailInForgotPassword, "Email ID");
	}

	/**
	 * To get error message displayed in forget password
	 * @return String -
	 * @throws Exception -
	 */
	public String getDisplayedErrorMessage() throws Exception{		
		return BrowserActions.getText(driver, emailTbErrorRequestPassword, "Error Message");
	}

	/**
	 * To close forget password dialog
	 * @throws Exception -
	 */
	public void closeForgotPasswordDialog()throws Exception {
		BrowserActions.clickOnElementX(btnCloseForgotPasswordMdl, driver, "Close Button for Forgot Password Modal");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click on breadcrumb category link by index
	 * @param index -
	 * @throws Exception -
	 */
	public void clickOnBreadCrumbValue(int index) throws Exception{
		BrowserActions.clickOnElementX(lstTxtProductInBreadcrumb.get(index), driver, "Breadcrumb");
	}

	/**
	 * To click on breadcrumb in mobile viewport
	 * @throws Exception -
	 */
	public void clickOnBreakCrumbMobile() throws Exception{
		BrowserActions.clickOnElementX(breadcrumbMobile, driver, "Breadcrumb");
	}

	/**
	 * To get the text in the breadcrumb
	 * @param runMode -
	 * @return list of text from breadcrumb
	 * @throws InterruptedException - InterruptedException
	 */
	public List<String> getTextInBreadcrumb(String... runMode)throws Exception {
		List<String> breadcrumbText = new ArrayList<>();
		BrowserActions.scrollToViewElement(lstTxtProductInBreadcrumb.get(0),
				driver);
		if (Utils.isDesktop() || Utils.isTablet() || (runMode.length > 0)) {
			for (WebElement element : lstTxtProductInBreadcrumb) {
				if (!element.getText().equals(""))
					breadcrumbText.add(element.getText());
			}
		} else {
			breadcrumbText.add(txtProductInBreadcrumbMobile.getText());
		}
		return breadcrumbText;
	}

	/**
	 * To get the text in breadcrumb (mobile)
	 * @return text -
	 * @throws Exception - Exception
	 */
	public String getTextInBreadcrumbMobile()
			throws Exception {

		BrowserActions.scrollToViewElement(breadcrumbMobile,
				driver);
		String breadcrumbText = BrowserActions.getText(driver, breadcrumbMobile, "Breadcrumb in mobile");
		return breadcrumbText;
	}

	/**
	 * To click on view more in mobile viewport
	 * @throws Exception -
	 */
	public void clickOnViewMoreInMobile() throws Exception{
		BrowserActions.clickOnElementX(lnkViewMoreMobile, driver, "View More in mobile");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To check the remember me checkbox
	 * @param check -
	 * @throws Exception -
	 */
	public void checkRememberMe(boolean check) throws Exception {
		if(check) {
			if(!(cbRememberme.isSelected())) {
				BrowserActions.clickOnElementX(cbRememberme, driver, "Remember me");
			}
		} else {
			if(cbRememberme.isSelected()) {
				BrowserActions.clickOnElementX(cbRememberme, driver, "Remember me");
			}
		}
	}

	/**
	 * To verify Show label displayed right side of password field
	 * @return boolean -
	 * @throws Exception -
	 */
	public Boolean verifyShowLabelDisplayedRightOfPassword()throws Exception{
		return BrowserActions.verifyHorizontalAllignmentOfElements(driver, passwordShowHide, txtpassword);
	}

	/**
	 * To click on Show password Label
	 * @throws Exception -
	 */
	public void clickOnShowPasswordLabel()throws Exception{
		BrowserActions.clickOnElementX(passwordShowHide, driver, "Show Password Label");
	}

	/**
	 * To click on register button
	 * @throws Exception
	 */
	public void clickOnRegisterBtn()throws Exception {
		if (Utils.waitForElement(driver, formFieldSignUp)) {
			BrowserActions.clickOnElementX(btnSubmitCreateAccount, driver, "Register Account");
		}
	}

	/**
	 * To type email address in forget password modal email field
	 * @param email -
	 * @throws Exception -
	 */
	public void enterEmailInForgotPasswordMdl(String email)throws Exception{
		BrowserActions.typeOnTextField(txtEmailInForgotPassword, email, driver, "Email Address Field in Forgot Password Modal");
	}

	/**
	 * To click on confirm register button
	 * @return Signin or MyAccountPage or null 
	 * @throws Exception - Exception
	 */
	public Object clickOnConfirmRegisterBtn()throws Exception{
		clickOnRegisterBtn();
		Utils.waitForPageLoad(driver);
		if (Utils.waitForElement(driver, readyElementMyAccountPage)) {
			return new MyAccountPage(driver).get();
		} else if (Utils.waitForElement(driver, divLoginBoxDesktop)) {
			return this;
		} else {
			Log.fail("Cannot Navigate to MyAccount Page", driver);
			return null;
		}
	}

	/**
	 * To enter text on field
	 * @param element to type -
	 * @param textToType -
	 * @param Description -
	 * @param obj Page object
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean enterTextOnField(String element,String textToType,String Description,Object obj)throws Exception{
		boolean flag=false;
		WebElement verifyElement=ElementLayer.getElement(element, obj);
		BrowserActions.scrollToView(verifyElement, driver);
		BrowserActions.typeOnTextField(verifyElement, textToType, driver, Description);
		BrowserActions.scrollToView(txtConfirmPasswordField, driver);
		BrowserActions.clickOnElementX(txtConfirmPasswordField, driver, "Empty space");
		String customerTextValue=verifyElement.getAttribute("value");
		if(customerTextValue.equals(textToType)){
			flag=true;
		}
		return flag;
	}

	/**
	 * To verify error message in email 
	 * @param textToType -
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyEmailErrorMessage(String textToType)throws Exception{
		boolean flag=false;
		BrowserActions.typeOnTextField(txtEmail, textToType, driver, "Email Text field");
		Utils.waitForElement(driver, txtEmailMandatoryError);
		if(txtEmailMandatoryError.getText().trim().equals("Please enter a valid email address.")){
			flag=true;
		}
		return flag;
	}

	/**
	 * To click on send button in forgot password
	 * @throws Exception - Exception
	 */
	public void clickonSendBtnInForgotPassword()throws Exception{
		BrowserActions.clickOnElementX(btnSendInForgotPassword, driver, "Send Button in Forget Password Modal");
		if(Utils.waitForElement(driver, lblErrorEmailInForgotPassword)) {
			Log.event("Error in Email.");
		}else {
			Utils.waitUntilElementDisappear(driver, txtEmailInForgotPassword,20);
		}
		
	}

	/**
	 * To wait for success message after submit button clicked in forgot password modal
	 * @throws Exception -
	 */
	public void waitForSuccessMessage()throws Exception{
		Utils.waitUntilElementDisappear(driver, txtEmailInForgotPassword,20);
	}

	/**
	 * Verify error messages from specified field
	 * @param textToType -
	 * @param errorMessage -
	 * @param fieldToVerify -
	 * @return true - if Error message displayed
	 * @throws Exception - Exception
	 */
	public boolean verifyFieldErrorMessages(String textToType, String errorMessage ,String fieldToVerify)throws Exception{
		boolean flag=false;

		switch(fieldToVerify){
		case "FirstName":
			BrowserActions.typeOnTextField(txtFirstName, textToType, driver, fieldToVerify+" Field");
			Utils.waitForElement(driver, txtOR);
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtFirstNameMandatoryError);

			if(txtFirstNameMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;
		case "LastName":
			BrowserActions.typeOnTextField(txtLastName, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtLastNameMandatoryError);

			if(txtLastNameMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;
		case "Email":
			BrowserActions.typeOnTextField(txtEmail, textToType, driver, fieldToVerify+" Field");

			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtEmailMandatoryError);
			if(txtEmailMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;
		case "Password":
			BrowserActions.typeOnTextField(txtpasswordField, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtEmailMandatoryError);
			if(txtPasswordMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;

		case "ConfirmPassword":
			BrowserActions.typeOnTextField(txtConfirmEmailId, textToType, driver, fieldToVerify+" Field");
			BrowserActions.clickOnElementX(txtOR, driver, "Empty space");
			Utils.waitForElement(driver, txtConfirmPasswordMandatoryError);
			if(txtConfirmPasswordMandatoryError.getText().trim().equalsIgnoreCase(errorMessage.trim())){
				flag=true;
			}

			break;

		}

		return flag;
	}

	/**
	 * To verify facebook button exists
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyFaceBookButton()throws Exception{
		boolean flag=false;
		if(Utils.waitForElement(driver, registerFacebookBtn)){
			BrowserActions.clickOnElementX(registerFacebookBtn, driver, "Register with Facebook button");
			Utils.waitForPageLoad(driver);
			String url=driver.getCurrentUrl();
			if(url.contains("Facebook")){
				flag=true;
			}

		}else
		{
			Log.failsoft("Register with Facebook button is not displayed");
		}
		return flag;
	}

	/**
	 * To click verify password text in the state
	 * @param txtToVerify -
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean ClickVerifyPasswordText(String txtToVerify)throws Exception{
		boolean flag=false;
		String spanName="";
		if(txtToVerify.equals("text")){
			spanName="Show";
		}else{
			spanName="Hide";
		}
		if(Utils.waitForElement(driver, lnkPasswordShow)){
			BrowserActions.clickOnElementX(lnkPasswordShow, driver, "password Hide/show link");
		}
		Utils.waitForElement(driver, txtpasswordField);
		String typeValue=txtpasswordField.getAttribute("type");
		if(typeValue.equals(txtToVerify)){
			Log.message("The Textbox text property is " + typeValue);
			flag=true;
		}else if(lnkPasswordShow.getText().equals(spanName)){
			flag=true;
		}
		return flag;
	}

	/**
	 * To select month option in create account
	 * @param month - Month to select
	 * @return status as boolean
	 * @throws Exception
	 */
	public boolean selectMonth(String month) throws Exception {
		boolean flag=false;
		Utils.waitForElement(driver, selectMonthDrop);
		BrowserActions.scrollToView(selectMonthDrop, driver);

		BrowserActions.clickOnElementX(selectMonthDrop, driver, "Quantity dropdown");
		Utils.waitForElement(driver, driver.findElement(By.xpath("//ul[@class='selection-list']/li[@label='"+month+"']")));
		WebElement listElement=driver.findElement(By.xpath("//ul[@class='selection-list']/li[@label='"+month+"']"));
		BrowserActions.clickOnElementX(listElement, driver, month );
		if(dropDownSelectedValue.getText().contains(month)){
			flag=true;
		}

		return flag;
	}

	/**
	 * To verify the dropdown sort order
	 * @param expectedElements -
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean VerifyDropDownSortOrder(List<String> expectedElements)throws Exception{

		boolean flag=false;
		for(int indexValue=0;indexValue<expectedElements.size();indexValue++){
			if(monthDropDownValues.get(indexValue).getAttribute("label").trim().equals(expectedElements.get(indexValue).trim())){
				flag=true;
			}else{
				flag=false;
				break;
			}	
		}
		return flag;
	}

	/**
	 * To verify the valid value in customer field
	 * @param inputText -
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyCheckboxValid(String inputText)throws Exception{

		boolean flag=false;
		if(Utils.waitForElement(driver, txtCustomerID)){
			BrowserActions.typeOnTextField(txtCustomerID, inputText, driver, "customerId");
		}
		Utils.waitForElement(driver, txtCustomerID);
		String customerTextValue=txtCustomerID.getAttribute("value");
		if(customerTextValue.equals(inputText)){
			flag=true;
		}
		return flag;
	}

	/**
	 * To verify status of News Letters field
	 * @return boolean - status as boolean
	 * @throws Exception - Exception
	 */
	public boolean getNewsletterStatus()throws Exception{
		String checkboxbackgroundPosition = checkNewsletters.getCssValue("background-position-y");
		int positionValue = StringUtils.getNumberInString(checkboxbackgroundPosition);
		return (positionValue > -50);
	}

	/**
	 * To click on news letter checkbox
	 * @param desiredState -
	 * @return boolean status of checked/unchecked
	 * @throws Exception - Exception
	 */
	public boolean clickOnNewsletterCheckBox(boolean desiredState) throws Exception {
		if(getNewsletterStatus() == desiredState) {
			return true;
		} else {
			BrowserActions.clickOnElementX(checkNewsletters, driver, "NewsLetters Checkbox");
			Utils.waitForPageLoad(driver);
		}
		return (getNewsletterStatus() == desiredState);
	}

	/**
	 * To verify password mismatch error  
	 * @param errorMsg -
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyPasswordMissmatch(String errorMsg)throws Exception{
		boolean flag=false;

		Utils.waitForElement(driver, confirmPasswordMissmatchError);
		if(confirmPasswordMissmatchError.getText().trim().equals(errorMsg.trim())){
			flag=true;
		}
		return flag;
	}

	/**
	 * To verify the email mismatch error
	 * @param errorMsg -
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyEmailMissmatch(String errorMsg)throws Exception{
		boolean flag=false;

		Utils.waitForElement(driver, confirmEmailMissmatchError);
		if(confirmEmailMissmatchError.getText().trim().equals(errorMsg.trim())){
			flag=true;
		}
		return flag;
	}

	// Guest user placed order status 
	/**
	 * To verify the demandware property of check or order label
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyDemandWarePropertyOfCheckAnOrderLabel() throws Exception {
		String propertyText;
		String textToVerify;
		textToVerify = lblCheckAnOrder.getText();  
		propertyText = demandWareProperty.getProperty("CheckAnOrderLabel");
		Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
		if (textToVerify.equalsIgnoreCase(propertyText)) {
			return true;
		}
		return false;
	}

	/**
	 * To verify demandware property of subhead text
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean verifyDemandWarePropertyOfSubHeadText() throws Exception {
		String propertyText;
		String textToVerify;
		textToVerify = subHeadTxt.getText();  
		propertyText = demandWareProperty.getProperty("CheckSubheader");
		Log.message("propertyText : " + propertyText + " : textToVerify : " + textToVerify);
		if (textToVerify.equals(propertyText)) {
			return true;
		}
		return false;		
	}

	/**
	 * To enter order track values
	 * @param orderNumber -
	 * @param orderEmail -
	 * @param billingZipCode -
	 * @throws Exception - Exception
	 */
	public void typeOnOrderTrackFormFields(String orderNumber,String orderEmail,String billingZipCode) throws Exception{
		
		if (Utils.waitForElement(driver, btnLookUpBeforeToggle)) {
			clickOnLookUpToggle();
		}
		
		BrowserActions.typeOnTextField(txtFldOrderNumber, orderNumber, driver, "Order Number");
		BrowserActions.typeOnTextField(txtFldOrderEmail, orderEmail, driver, "Order Email");
		BrowserActions.typeOnTextField(txtFldBillingZipcode, billingZipCode, driver, "Order Zipcode");
	}

	/**
	 * Click on submit button page 
	 * @return GuestOrderStatusLandingPage Page Object
	 * @throws Exception - Exception
	 */
	public GuestOrderStatusLandingPage clickOnSubmitButton()throws Exception{
		BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Clicked on submit");
		Utils.waitForPageLoad(driver);
		return new GuestOrderStatusLandingPage(driver).get();
	}

	/**
	 * To navigate to guest order status page
	 * @param orderNumber -
	 * @param orderEmail - 
	 * @param billingZipCode -
	 * @return GuestOrderStatusLandingPage Page object
	 * @throws Exception - Exception
	 */
	public GuestOrderStatusLandingPage navigateToGuestOrderStatusPage(String orderNumber,String orderEmail,String billingZipCode)throws Exception{
		typeOnOrderTrackFormFields(orderNumber, orderEmail, billingZipCode);
		BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Guest order status  page Link");
		Utils.waitForPageLoad(driver);
		if(Utils.waitForElement(driver, orderErrorMsg)) {
			Log.event("Error looking up order.");
			return null;
		}
		return new GuestOrderStatusLandingPage(driver).get();
	} 

	/**
	 * To enter Zipcode limit
	 * @param billingZipCodeFormat1 -
	 * @return true - if Zipcode Limit is Type1
	 * @throws Exception - Exception
	 */
	public boolean zipCodeLimitType1(String billingZipCodeFormat1) throws Exception{
		BrowserActions.typeOnTextField(txtFldBillingZipcode,billingZipCodeFormat1, driver, "order Billing zip code Field format1 ");
		if(txtFldBillingZipcode.getText().equals(billingZipCodeFormat1)){
			return true;
		}
		return false;
	}

	/**
	 * To enter Zipcode limit format 2
	 * @param billingZipCodeFormat2 -
	 * @return true - If Zipcode Limit type is 2
	 * @throws Exception - Exception
	 */
	public boolean zipCodeLimitType2(String billingZipCodeFormat2) throws Exception{
		BrowserActions.typeOnTextField(txtFldBillingZipcode,billingZipCodeFormat2, driver, "order Billing zip code Field format1 ");
		if(txtFldBillingZipcode.getText().equals(billingZipCodeFormat2)){
			return true;
		}
		return false;
	}

	/**
	 * To validate order number mismatch
	 * @param invalidOrderNumber -
	 * @param orderEmail -
	 * @param billingZipCode -
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean orderNumberMissmatch(String invalidOrderNumber,String orderEmail,String billingZipCode) throws Exception{

		BrowserActions.typeOnTextField(txtFldOrderNumber,invalidOrderNumber, driver, "order Number Field");
		BrowserActions.typeOnTextField(txtFldOrderEmail,orderEmail, driver, "order Email Field");
		BrowserActions.typeOnTextField(txtFldBillingZipcode,billingZipCode, driver, "order Billing zip code Field");
		BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Guest order status  page Link");
		String urlBeforeClick= driver.getCurrentUrl();
		Utils.waitForElement(driver, orderNoErrorMsg);
		String urlAfterClick= driver.getCurrentUrl();
		if(orderNoErrorMsg.getText().contains("We’re having trouble finding your order.")){
			return true;
		}
		if(urlAfterClick.equals(urlBeforeClick)) {
			return true;
		}
		return false;
	}

	/**
	 * To validate the order email mismatch
	 * @param orderNumber -
	 * @param invalidOrderEmail -
	 * @param billingZipCode -
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean orderEmailMissmatch(String orderNumber,String invalidOrderEmail,String billingZipCode) throws Exception{
		BrowserActions.typeOnTextField(txtFldOrderNumber,orderNumber, driver, "order Number Field");
		BrowserActions.typeOnTextField(txtFldOrderEmail,invalidOrderEmail, driver, "order Email Field");
		BrowserActions.typeOnTextField(txtFldBillingZipcode,billingZipCode, driver, "order Billing zip code Field");
		BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Guest order status  page Link");
		Utils.waitForElement(driver, orderEmailErrorMsg);
		if(orderEmailErrorMsg.getText().contains("Please enter a valid email.") || orderEmailErrorMsg.getAttribute("class").equals("error"))
		{
			return true;
		}
		return false;

	}

	/**
	 * To check the order billing mismatch
	 * @param orderNumber -
	 * @param orderEmail -
	 * @param invalidBillingZipCode -
	 * @return status as boolean
	 * @throws Exception - Exception
	 */
	public boolean orderBillingZipCodeMissmatch(String orderNumber,String orderEmail,String invalidBillingZipCode) throws Exception{
		txtFldOrderNumber.clear();
	    BrowserActions.typeOnTextField(txtFldOrderNumber,orderNumber, driver, "order Number Field");
		txtFldOrderEmail.clear();
		BrowserActions.typeOnTextField(txtFldOrderEmail,orderEmail, driver, "order Email Field");
		txtFldBillingZipcode.clear();
		BrowserActions.typeOnTextField(txtFldBillingZipcode,invalidBillingZipCode, driver, "order Billing zip code Field");
		BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Guest order status  page Link");
		Utils.waitForElement(driver, orderInvalidZipCodeErrorMsg);
		if(orderInvalidZipCodeErrorMsg.getText().equalsIgnoreCase("Invalid Zip"))
		{
			return true;
		}
		else
			return false;
	}
	
	/**
	 * To verify all 3 fields are mandatory in check an order section
	 * @param orderNumber -
	 * @param orderEmail -
	 * @return boolean - 
	 * @throws Exception -
	 */
	public boolean verifyOrderPostalZipcCodeMandatory(String orderNumber,String orderEmail) throws Exception
	{
		txtFldOrderNumber.clear();
		BrowserActions.typeOnTextField(txtFldOrderNumber,orderNumber, driver, "order Number Field");
		txtFldOrderEmail.clear();
		BrowserActions.typeOnTextField(txtFldOrderEmail,orderEmail, driver, "order Email Field");
		txtFldBillingZipcode.clear();
		txtFldBillingZipcode.sendKeys("");
		String urlBeforeClick= driver.getCurrentUrl();
		BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Clicked on submit");
		String urlAfterClick= driver.getCurrentUrl();
		if(urlAfterClick.equals(urlBeforeClick)) {
			return true;
		}
		if(orderPostalZipCodeErrorMsg.getText().equals("0 orders found or please confirm ZipCode")){
				return true;
		}
	return false;
	}
	
	/**
	 * To verify order number is mandatory in Check an order section
	 * @param orderEmail -
	 * @param billingZipCode -
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyOrderNoIsMandatory(String orderEmail,String billingZipCode) throws Exception{
		txtFldOrderNumber.clear();
		txtFldOrderNumber.sendKeys("");
		txtFldOrderEmail.clear();
		BrowserActions.typeOnTextField(txtFldOrderEmail,orderEmail, driver, "order Email Field");
		txtFldBillingZipcode.clear();
		BrowserActions.typeOnTextField(txtFldBillingZipcode,billingZipCode, driver, "order zip billing code Field");
		String urlBeforeClick= driver.getCurrentUrl();
		BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Clicked on submit");
		String urlAfterClick= driver.getCurrentUrl();
		
		if(urlAfterClick.equals(urlBeforeClick)) {
			return true;
		}
		if(orderNoErrorMsg.getText().equals("0 orders found or please confirm Order Number")) {
			return true;
		}
		return false;
	}
	
	/**
	 * To verify order email is mandatory
	 * @param orderNumber -
	 * @param billingZipCode -
	 * @return boolean -
	 * @throws Exception -
	 */
	public boolean verifyOrderEmailIsMandatory(String orderNumber,String billingZipCode) throws Exception{
		txtFldOrderNumber.clear();
		BrowserActions.typeOnTextField(txtFldOrderNumber,orderNumber, driver, "order Number Field");
		txtFldOrderEmail.clear();
		txtFldOrderEmail.sendKeys("");
		txtFldBillingZipcode.clear();
		BrowserActions.typeOnTextField(txtFldBillingZipcode,billingZipCode, driver, "order Billing zip code Field");
		String urlBeforeClick= driver.getCurrentUrl();
		BrowserActions.clickOnElementX(checkorderstatusbtn, driver, "Clicked on submit");
		String urlAfterClick= driver.getCurrentUrl();
		if(urlBeforeClick.equals(urlAfterClick)){
			return true;
		}
		else
			return false;
	}

	/**
	 * To get entered email address 
	 * @return String - 
	 * @throws Exception -
	 */
	public String getEnteredEmailAddress() throws Exception{
		return BrowserActions.getText(driver, fldEmail, "Email ID");
	}

	/**
	 * To clear all check order fields 
	 * @throws Exception - Exception
	 */
	public void clearCheckOrderFields()throws Exception{
		txtFldOrderNumber.clear();
		txtFldOrderNumber.sendKeys(Keys.TAB);
		txtFldOrderEmail.clear();
		txtFldOrderEmail.sendKeys(Keys.TAB);
		txtFldBillingZipcode.clear();
		txtFldBillingZipcode.sendKeys(Keys.TAB);
	}

	
	/**
	 * To click on LookUP button Before Toggle
	 * @throws Exception - Exception
	 */
	public void clickOnLookUpToggle()throws Exception{
		BrowserActions.clickOnElementX(btnLookUpBeforeToggle, driver, "Lookup button Before Toggle");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on LookUP button Before Toggle
	 * @throws Exception - Exception
	 */
	public void openOrCloseCheckAnOrderSection(boolean Open)throws Exception{
		if (Utils.waitForElement(driver, btnLookUpBeforeToggle) && Open) {
			clickOnLookUpToggle();
		} else if(!Utils.waitForElement(driver, btnLookUpBeforeToggle) && !Open) {
			clickOnCancelAfterToggle();
		}
	}
	
	/**
	 * To click on cancel button after toggle
	 * @throws Exception - Exception
	 */
	public void clickOnCancelAfterToggle()throws Exception{
		BrowserActions.clickOnElementX(btnCancelAfterToggle, driver, "Cancel button after toggle");
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To click on check in order button
	 * @throws Exception - Exception
	 */
	public void clickOnCheckInOrder()throws Exception{
		openOrCloseCheckAnOrderSection(true);
		Utils.waitForElement(driver, btnCheckInOrder);
		BrowserActions.clickOnElementX(btnCheckInOrder, driver, "Check button in Check Order");
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To fill order email in the field
	 * @param mail -
	 * @throws Exception - Exception
	 */
	public void fillOrderMailBox(String mail)throws Exception{
		BrowserActions.typeOnTextField(txtFldOrderEmail, mail, driver, "Order Mail Txt Field");
	}

	/**
	 * To fill order fields 
	 * @param orderNumber - String
	 * @param orderMail - String
	 * @param orderZip - String
	 * @throws Exception - Exception
	 */
	public void fillOrderFields(String orderNumber, String orderMail, String orderZip)throws Exception{
		if(Utils.waitForElement(driver, btnNarvarOrderLookupToggle)) {
			BrowserActions.clickOnElementX(btnNarvarOrderLookupToggle, driver, "Narvar order lookup toggle");
		}
		BrowserActions.typeOnTextField(txtFldOrderNumber, orderNumber, driver, "Order Number");
		BrowserActions.typeOnTextField(txtFldOrderEmail, orderMail, driver, "Order Mail");
		BrowserActions.typeOnTextField(txtFldBillingZipcode, orderZip, driver, "Order Zipcode");
	}

	/**
	 * To get password success message 
	 * @return String - Password reset success subhead message
	 * @throws Exception - Exception
	 */
	public String getForgetPasswordSuccessMsg()throws Exception{
		Log.event("Message displayed :: " + headingRequestPassword.getAttribute("innerHTML"));
		return headingRequestPassword.getText();
	}
	
	/**
	 * To open/close order details
	 * @param state - true/false open open/collapse details
	 * @throws Exception - Exception 
	 */
	public void clickOpenCollapseOrderDetailsMobile(boolean state)throws Exception{
		if(Utils.isMobile()) {
			if(state) {
				if(Utils.waitForElement(driver, lnkViewDetails))
					BrowserActions.clickOnElementX(lnkViewDetails, driver, "View Details");
				else
					Log.event("Order Details already opened.");
			}else {
				if(Utils.waitForElement(driver, lnkViewLess))
					BrowserActions.clickOnElementX(lnkViewLess, driver, "View Less");
				else
					Log.event("Order Details already collapsed.");
			}
		}
		else
			Log.event("Platform non applicable.");
	}

	/**
	 * To close forget password page in mobile view
	 * @throws Exception -
	 */
	public void closeForgotPasswordPageMobile()throws Exception {
		if(Utils.waitForElement(driver, lnkBackToLoginMobile)) {
			BrowserActions.clickOnElementX(lnkBackToLoginMobile, driver, "Back to Login link from Forgot Password Page");
			Utils.waitForPageLoad(driver);
		} else if(Utils.waitForElement(driver, backArrowBreadcrumbMobile)) {
			BrowserActions.clickOnElementX(backArrowBreadcrumbMobile, driver, "Back arrow to navigate to my account section");
			Utils.waitForPageLoad(driver);
		}
	}
}
