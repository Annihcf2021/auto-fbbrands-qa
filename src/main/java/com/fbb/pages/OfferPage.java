package com.fbb.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.fbb.reusablecomponents.Enumerations.ElementDimension;
import com.fbb.support.BrowserActions;
import com.fbb.support.Log;
import com.fbb.support.Utils;


public class OfferPage extends LoadableComponent<OfferPage> {

	private WebDriver driver;
	private boolean isPageLoaded;
	public ElementLayer elementLayer;

	//================================================================================
	//			WebElements Declaration Start
	//================================================================================

	@FindBy(css = ".pt_offerscoupons")
	WebElement readyElement;
	
	@FindBy(css = ".you-may-like")
	WebElement recommendationSection;
	
	@FindBy(css = ".you-may-like li:not(.two-space-tile):not(.one-space-tile) .product-tile:not([class*='set'])")
	List<WebElement> productTile;
	
	@FindBy(css = ".you-may-like li:not(.two-space-tile):not(.one-space-tile) .product-tile:not([class*='set'])")
	WebElement firstProductTile;
	
	@FindBy(css = ".you-may-like .product-tile .product-name")
	List<WebElement> recomSecPrdName;
	
	@FindBy(css = ".oc-group-content .oc-offer-content")
	List<WebElement> lstOffers;
	
	@FindBy(css = ".oc-offer-content img")
	List<WebElement> lstOffersWithImage;
	
	@FindBy(css = ".oc-group-content section")
	List<WebElement> lstOffersNoImage;
	
	@FindBy(css = ".oc-content-row")
	List<WebElement> lstPromoCode;
	

	//================================================================================
	//			WebElements Declaration End
	//================================================================================

	/**
	 * constructor of the class
	 * @param driver - WebDriver
	 */
	public OfferPage(WebDriver driver) {
		this.driver = driver;
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, Utils.maxElementWait);
		PageFactory.initElements(finder, this);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}
		Utils.waitForPageLoad(driver);
		Utils.waitForElement(driver, readyElement, 25);
		if (isPageLoaded && !(Utils.waitForElement(driver, readyElement))) {
			Log.fail("Offer Page didn't open", driver);
		}

		elementLayer = new ElementLayer(driver);
	}

	@Override
	protected void load() {
		isPageLoaded = true;
		Utils.waitForPageLoad(driver);
	}

	public boolean getPageLoadStatus()throws Exception{
		return isPageLoaded;
	}

	/**
	 * To click on quickshop button
	 * @param index - index
	 * @return QuickShop - quickShop
	 * @throws Exception - Exception
	 */
	public QuickShop clickOnQuickShopByIndex(int index)throws Exception{
		BrowserActions.scrollInToView(productTile.get(index-1), driver);
		BrowserActions.mouseHover(driver, productTile.get(index-1), "Product Tile");
		WebElement qcButton = productTile.get(index-1).findElement(By.cssSelector(" .quickview"));
		if (Utils.waitForElement(driver, qcButton)) {
			BrowserActions.javascriptClick(qcButton, driver, "Quick Shop Button for First Product");
		}
		return new QuickShop(driver).get();
	}
	
	/**
	 * To get product id by index
	 * @param noOfIdNeeded - No of product needed
	 * @return String[] - Product id array
	 * @throws Exception - Exception
	 */
	public String getProductIdByIndex(int index)throws Exception{
		Utils.waitForElement(driver, firstProductTile);
		WebElement prdId = productTile.get(index-1);
		return BrowserActions.getTextFromAttribute(driver, prdId, "data-itemid", "product");
	}
	
	/**
	 * To navigate to pdp page
	 * @param index - index
	 * @return PdpPage - PdpPage
	 * @throws Exception - Exception
	 */
	public PdpPage navigateToPdpByIndex(int index)throws Exception{
		BrowserActions.scrollInToView(recomSecPrdName.get(index-1), driver);
		BrowserActions.javascriptClick(recomSecPrdName.get(index-1), driver, "Product");
		return new PdpPage(driver).get();
	}
	
	/**
	 * To verify all offers are properly displayed
	 * @return boolean
	 * @throws Exception
	 */
	public boolean verifyOffersDisplayed() throws Exception{
		if(lstOffers.size() == lstPromoCode.size() || lstOffersWithImage.size() == 0) {
			return true;
		} else {
			int imageHeight = BrowserActions.getElementDimension(lstOffersWithImage.get(0)).get(ElementDimension.height);
			int imageWidth = BrowserActions.getElementDimension(lstOffersWithImage.get(0)).get(ElementDimension.width);
			int sectionHeight = BrowserActions.getElementDimension(lstOffersNoImage.get(0)).get(ElementDimension.height);
			int sectionWidth = BrowserActions.getElementDimension(lstOffersNoImage.get(0)).get(ElementDimension.width);
			return (imageHeight == sectionHeight && imageWidth == sectionWidth);
		}
	}
	
	/**
	 * To get the product name in recommendation section
	 * @param index - integer
	 * @return- String product Name
	 */
	public String getProductNameByIndex(int index) throws Exception {
		if(Utils.waitForElement(driver, firstProductTile)) {
			return BrowserActions.getText(driver, recomSecPrdName.get(index-1), "recommendation section porduct name").trim().replace("amp;", "");
		}
		return null;
	}
	
	public int getNoOfOffers() throws Exception{
		return lstOffers.size();
	}

}
