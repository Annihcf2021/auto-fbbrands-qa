package com.fbb.externalAPI;

import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.authentication.PreemptiveBasicAuthScheme;
import com.jayway.restassured.response.Response;

public class JiraAPI {
	public static HashMap<String, HashMap<String, String>> LocalTicketCache = new HashMap<String, HashMap<String,String>>();
	private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
	private static String jiraURL = configProperty.getProperty("jiraURL");
	private static String jiraUser = configProperty.getProperty("jiraUser");
	private static String jiraKey = configProperty.getProperty("jiraKey");
	
	public static Response getJiraResponse(String ticket) throws Exception {
		RestAssured.baseURI = jiraURL;
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(jiraUser);
		authScheme.setPassword(jiraKey);
		RestAssured.authentication = authScheme;
		
		String jiraCallURL = jiraURL + ticket;
		Response restResponse =  RestAssured.given().when().get(jiraCallURL);
		restResponse = RestAssured.get(jiraCallURL);
		return restResponse;
	}
	
	/**
	 * To format API response as JSON object
	 * @param response - Response from API call
	 * @return Response formatted as JSON object
	 * @throws Exception
	 */
	public static JSONObject getJson(Response response) throws Exception{
		return getJson(response.getBody().asString());
	}
	
	private static JSONObject getJson(String jsonString) throws Exception{
		JSONParser parseJson = new JSONParser();
		JSONObject stringAsJson = (JSONObject) parseJson.parse(jsonString);
		return stringAsJson;
	}
	
	/**
	 * To format address JSON object as Array with given key
	 * @param jsonObject - Address JSON object
	 * @param property - key to get array from JSON object
	 * @return JSON object as an array
	 * @throws Exception
	 */
	public static JSONArray getJsonArray(JSONObject jsonObject, String property) throws Exception{
		JSONArray jsonObjectAsArray = (JSONArray)jsonObject.get(property);
		Log.event("Number of Array elements:: " + jsonObjectAsArray.size());
		return jsonObjectAsArray;
	}
	
	/**
	 * To get Jira ticket select details as a HashMap
	 * @param ticketNumber - Jira issue number
	 * @return Jira ticket details
	 * @throws Exception
	 */
	public static HashMap<String, String> getJiraTicketSummery(String ticketNumber) throws Exception {
		HashMap<String, String> jiraTicket = new HashMap<String, String>();
		if(LocalTicketCache.containsKey(ticketNumber)) {
			Log.event(ticketNumber + " read form local cache.");
			jiraTicket = LocalTicketCache.get(ticketNumber);
		} else {
			jiraTicket.put("trackingNumber", ticketNumber);
			JSONObject ticketJSON = (JSONObject) getJson(getJiraResponse(ticketNumber)).get("fields");
			jiraTicket.put("description", ticketJSON.get("summary").toString());
			JSONObject statusJson = getJson(ticketJSON.get("status").toString());
			jiraTicket.put("status", statusJson.get("name").toString());
			JSONObject severityJson = getJson(ticketJSON.get("customfield_12301").toString());
			jiraTicket.put("severity", severityJson.get("value").toString());

			LocalTicketCache.put(ticketNumber, jiraTicket);
			Log.event(ticketNumber + " saved to local cache.");
		}
		return jiraTicket;
	}

}
