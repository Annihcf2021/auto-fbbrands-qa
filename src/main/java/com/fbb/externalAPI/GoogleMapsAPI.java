package com.fbb.externalAPI;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

public class GoogleMapsAPI {
	private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
	
	/**
	 * To get REST response to API call to Google Geolocation
	 * @param address - address to get response on 
	 * @return restResponse - REST response to provided address
	 * @throws Exception
	 */
	public static Response getAddressAsResponse(String address) throws Exception{
		String APIKey = configProperty.getProperty("googleMapsKey");
		String geocodeURL = "https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=" + APIKey;
		Response restResponse = RestAssured.given().when().get(geocodeURL);
		restResponse = RestAssured.get(geocodeURL);
		return restResponse;
	}
	
	/**
	 * To get REST response to API call to Google Geolocation
	 * @param address - address to get response on
	 * @param APIKey - custom API key 
	 * @return restResponse - REST response to provided address
	 * @throws Exception
	 */
	public static Response getAddressAsResponse(String address, String APIKey) throws Exception{
		String geocodeURL = "https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=" + APIKey;
		Response restResponse = RestAssured.given().when().get(geocodeURL);
		restResponse = RestAssured.get(geocodeURL);
		return restResponse;
	}
	
	/**
	 * To format address API response as JSON object
	 * @param addressResponse - Response from API call
	 * @return addressAsJson - Response formatted as JSON object
	 * @throws Exception
	 */
	public static JSONObject getResponseAsJson(Response addressResponse) throws Exception{
		JSONParser parseJson = new JSONParser();
		JSONObject addressAsJson = (JSONObject)parseJson.parse(addressResponse.getBody().asString());
		return addressAsJson;
	}
	
	/**
	 * To format address JSON object as Array with given key
	 * @param jsonObject - Address JSON object
	 * @param property - key to get array from JSON object
	 * @return jsonObjectAsArray - JSON object as an array
	 * @throws Exception
	 */
	public static JSONArray getJsonArray(JSONObject jsonObject, String property) throws Exception{
		JSONArray jsonObjectAsArray = (JSONArray)jsonObject.get(property);
		Log.event("Number of Array elements:: " + jsonObjectAsArray.size());
		return jsonObjectAsArray;
	}
	
	/**
	 * To get formatted adrress from Google
	 * @param address - address to send in API call 
	 * @return formattedAddress - Address formatted by Google
	 * @throws Exception
	 */
	public static String getFormattedAddress(String address) throws Exception{
		String formattedAddress = address;
		Log.event("Entered Address:: " + address);
		JSONArray addressJsonArray = getJsonArray(getResponseAsJson(getAddressAsResponse(address)), "results");
		JSONObject addressJson = (JSONObject) addressJsonArray.get(0);
		formattedAddress = (String) addressJson.get("formatted_address");
		Log.event("Formatted Address:: " + formattedAddress);
		return formattedAddress;
	}
	
	/**
	 * To get formatted adrress from Google
	 * @param address - address to send in API call
	 * @param APIKey - custom API key 
	 * @return formattedAddress - Address formatted by Google
	 * @throws Exception
	 */
	public static String getFormattedAddress(String address, String APIKey) throws Exception{
		String formattedAddress = address;
		Log.event("Entered Address:: " + address);
		JSONArray addressJsonArray = getJsonArray(getResponseAsJson(getAddressAsResponse(address, APIKey)), "results");
		JSONObject addressJson = (JSONObject) addressJsonArray.get(0);
		formattedAddress = (String) addressJson.get("formatted_address");
		Log.event("Formatted Address:: " + formattedAddress);
		return formattedAddress;
	}
	
	/**
	 * To get a property value of returned value of API call to Google
	 * @param address - address to get property of
	 * @param property - property of given address to get
	 * @return addressProperty - property found of given address
	 * @throws Exception
	 */
	public static String getAddressPropertyValue(String address, String property) throws Exception{
		String addressProperty = "";
		JSONArray addressJsonArray = getJsonArray(getResponseAsJson(getAddressAsResponse(address)), "results");
		JSONObject addressJson = (JSONObject) addressJsonArray.get(0);
		addressProperty = (String) addressJson.get(property);
		Log.event("Found " + property + " as:: " + addressProperty);
		if(addressProperty == null) {
			Log.event(property + " might be returning a JSON array.");
		}
		return addressProperty;
	}
	
	/**
	 * To get a property value of returned value of API call to Google
	 * @param address - address to get property of
	 * @param property - property of given address to get
	 * @param APIKey - custom API key 
	 * @return addressProperty - property found of given address
	 * @throws Exception
	 */
	public static String getAddressPropertyValue(String address, String property, String APIKey) throws Exception{
		String addressProperty = "";
		JSONArray addressJsonArray = getJsonArray(getResponseAsJson(getAddressAsResponse(address, APIKey)), "results");
		JSONObject addressJson = (JSONObject) addressJsonArray.get(0);
		addressProperty = (String) addressJson.get(property);
		Log.event("Found " + property + " as:: " + addressProperty);
		if(addressProperty == null) {
			Log.event(property + " might be returning a JSON array.");
		}
		return addressProperty;
	}
}
