package com.fbb.externalAPI;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.config.ConnectionConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.response.Response;
import com.fbb.support.Log;

public class RestAssuredAPI {

	/**setBaseURL method sets baseURL
	 * @param baseURL
	 */
	public static void setBaseURL(String baseURL)
	{
		try
		{
			if(!baseURL.isEmpty()||!baseURL.contains(null))
			{
				RestAssured.baseURI = baseURL;
			}}catch (NullPointerException e) {
				System.out.println("Base URL is set as null");
			}	
	}

	/**
	 * Returns response of GET API method execution
	 * 
	 * @param baseURL
	 * @param headers
	 * @param username
	 * @param password
	 * @param contentType
	 * @param url
	 * @return Response of GET command
	 */
	public static Response GET(String baseURL,Map<String, String> headers,String username,String password,String contentType, String url) 
	{
		setBaseURL(baseURL);
		Response resp = RestAssured.given()
				.auth().basic(username, password)
				.headers(headers)
				.contentType(contentType).get(url);

		System.out.println("running GET command");		
		System.out.println("URL: \n" + url+"\n");
		System.out.println("Header: \n" + resp.getHeaders().toString()+"\n");
		System.out.println("Response body: \n" + resp.asString()+"\n");
		System.out.println("Status Code: \n" + resp.getStatusCode()+"\n");		
		System.out.println("Time taken to get response is: \n" + resp.getTime()+" milli second\n");
		return resp;
	}
	
	/**
	 * Returns response of GET API method execution based on provided query parameters
	 * @param baseURL
	 * @param headers
	 * @param queryParam
	 * @param contentTypeJson
	 * @param body
	 * @param url
	 * @return
	 */
	public static Response GET(String baseURL,Map<String, String> headers,Map<String, String> queryParam, String url) 
	{
		setBaseURL(baseURL);
		Response resp = RestAssured.given()
				.parameters(queryParam)
				.headers(headers)
				.get(url);

		System.out.println("running GET command");		
		System.out.println("URL: \n" + url+"\n");
		System.out.println("Header: \n" + resp.getHeaders().toString()+"\n");
		System.out.println("Response body: \n" + resp.asString()+"\n");
		System.out.println("Status Code: \n" + resp.getStatusCode()+"\n");		
		System.out.println("Time taken to get response is: \n" + resp.getTime()+" milli second\n");
		return resp;
	}
	
	public static Response GET(String url, String params)
	{
		System.out.println(url +params);
		Response resp = RestAssured.given().when().log().all().get(url +params);
		Log.message(  RestAssured.given().when().log().all().toString());
		System.out.println("running GET command");		
		System.out.println("URL: \n" + url+"\n");
		System.out.println("Response body: \n" + resp.asString()+"\n");
		System.out.println("Status Code: \n" + resp.getStatusCode()+"\n");
		return resp;
	}
	
	public static Response GETBASEURL(String baseURL,String url)
	{
		setBaseURL(baseURL);
		RestAssured.useRelaxedHTTPSValidation();
		Response resp = RestAssured.given().when().log().all().get(url);
		Log.message(  RestAssured.given().when().log().all().toString());
		System.out.println("running GET command");		
		System.out.println("URL: \n" + url+"\n");
		System.out.println("Response body: \n" + resp.asString()+"\n");
		System.out.println("Status Code: \n" + resp.getStatusCode()+"\n");
		return resp;
	}


	public static Response GET(String baseURL,Map<String, String> headers,Map<String, String> queryParam, String url, Boolean LongResponse){

		if(LongResponse) return GET(baseURL,headers,queryParam,url);

		setBaseURL(baseURL);
		Response resp = RestAssured.given()
				.parameters(queryParam)
				.headers(headers)
				.get(url);
		System.out.println("GET : \n" + baseURL+url+"\n");
		return resp;
	}
	/**
	 * Returns response of POST API method execution
	 * 
	 * @param headerKey
	 * @param headerValue
	 * @param contentTypeJson
	 * @param body
	 * @param url
	 * @return Response of POST command
	 */
	public static Response POST(String baseURL,Map<String, String> headers,String username,String password,String contentType, String body, String url) {
		setBaseURL(baseURL);
		Response resp = RestAssured.given()
				.auth().basic(username, password)
				.headers(headers)
				.contentType(contentType)				
				.body(body)
				.post(url)
				.andReturn();
		System.out.println("running POST command");
		System.out.println("URL \n" + url);
		System.out.println("Header \n" + resp.getHeaders().toString());
		System.out.println("Response body \n" + resp.getBody().asString());
		System.out.println("Status Code \n" + resp.getStatusCode());
		System.out.println("Time taken to get response is \n" + resp.getTime()+" milli second");
		return resp;
	}
	
	/**
	 * Returns response of POST API method execution
	 * 
	 * @param baseURL
	 * @param headers
	 * @param body
	 * @return Response of POST command
	 */
	/**
	 * Returns response of POST API method execution
	 * 
	 * @param headerKey
	 * @param body
	 * @param url
	 * @return Response of POST command
	 */
	public static Response POST(String baseURL,Map<String, String> headers, String body) {
		setBaseURL(baseURL);
		RestAssured.useRelaxedHTTPSValidation();
		Response resp = RestAssured.given()
				.headers(headers)				
				.body(body).post()
				.andReturn();
		System.out.println("running POST command");
		System.out.println(baseURL);
		System.out.println("Header \n" + resp.getHeaders().toString());
		System.out.println("Response body \n" + resp.asString());
		System.out.println("Status Code \n" + resp.getStatusCode());
		return resp;
	}
	
	/**
	 * Returns response of POST API method execution
	 * 
	 * @param headerKey
	 * @param body
	 * @param url
	 * @return Response of POST command
	 */
	public static Response POST(String baseURL,Map<String, String> headers) {
		setBaseURL(baseURL);
		RestAssured.useRelaxedHTTPSValidation();
		Response resp = RestAssured.given()
				.headers(headers)				
				.post()
				.andReturn();
		System.out.println("running POST command");
		System.out.println(baseURL);
		System.out.println("Header \n" + resp.getHeaders().toString());
		System.out.println("Response body \n" + resp.asString());
		System.out.println("Status Code \n" + resp.getStatusCode());
		return resp;
	}
	
	/**
	 * Returns response of POST API method execution
	 * 
	 * @param baseURL
	 * @param headers
	 * @param inputData
	 * @param body
	 * @param url
	 * @return Response of POST command
	 */
	public static Response POST(String baseURL,Map<String, String> headers, String body, String url) {
		setBaseURL(baseURL);
		RestAssured.useRelaxedHTTPSValidation();
		RestAssured.requestSpecification = new RequestSpecBuilder().setBaseUri(baseURL).build().log().all();
		Response resp = RestAssured.given().headers(headers).body(body)
				.log().all().post(url).andReturn();
		System.out.println("running POST command");
		System.out.println("get requestSpecification"+ RestAssured.requestSpecification);
		System.out.println("URL \n" + url);
		System.out.println("Header \n" + resp.getHeaders().toString());
		System.out.println("Response body \n" + resp.asString());
		System.out.println("Time taken to get response is \n" + resp.getTime()+" milli second");

		return resp;

	}
	
	
	/**
	 * Returns response of POST API method execution
	 * 
	 * @param baseURL
	 * @param headers
	 * @param inputData
	 * @param body
	 * @param url
	 * @return Response of POST command
	 */
	public static Response POST(String baseURL,Map<String, String> headers,Map<String, String> inputData, String body, String url) {
		setBaseURL(baseURL);
		RestAssured.useRelaxedHTTPSValidation();
		RestAssured.requestSpecification = new RequestSpecBuilder().setBaseUri(baseURL).build().log().all();
		Response resp = RestAssured.given().headers(headers).body(body)
				.queryParameters(inputData).log().all().post(url).andReturn();
		System.out.println("running POST command");
		System.out.println("get requestSpecification"+ RestAssured.requestSpecification);
		System.out.println("URL \n" + url);
		System.out.println("Header \n" + resp.getHeaders().toString());
		System.out.println("Response body \n" + resp.asString());
		System.out.println("Time taken to get response is \n" + resp.getTime()+" milli second");

		return resp;

	}
	
	/**
	 * Returns response of POST API method execution
	 * 
	 * @param baseURL
	 * @param headers
	 * @param inputData
	 * @param body
	 * @param url
	 * @return Response of POST command
	 */
	public static Response POST(String baseURL,Map<String, String> headers,String username,String password, String body, String url) {
		setBaseURL(baseURL);
		RestAssured.useRelaxedHTTPSValidation();
		Response resp = RestAssured.given().headers(headers).auth().basic(username, password).body(body)
				.log().all().post(url).andReturn();
		System.out.println("running POST command");
		System.out.println("get requestSpecification"+ RestAssured.requestSpecification);
		System.out.println("URL \n" + url);
		System.out.println("Header \n" + resp.getHeaders().toString());
		System.out.println("Response body \n" + resp.asString());
		System.out.println("Time taken to get response is \n" + resp.getTime()+" milli second");

		return resp;

	}


	
	/**
	 * The Publisher APIs require File to be uploaded, so the given API is
	 * specific to POST Publisher APIs
	 * 
	 * @param headerKey
	 * @param headerValue
	 * @param contentTypeJson
	 * @param controlname
	 * @param path
	 * @param url
	 * @return Response of API execution
	 * @throws Exception 
	 */
	public static Response POSTPublisher(String baseURL,Map<String, String> headers,String username,String password,
			String contentTypeJson, String path, String url) throws Exception {
		setBaseURL(baseURL);
		RestAssured.useRelaxedHTTPSValidation();
		Response resp = null;
		try {
			File file = new File(path);
			resp = RestAssured.given().headers(headers)
					.auth().basic(username, password)
					.headers(headers)
					.multiPart("files", file).contentType(contentTypeJson)
					.when().post(url);
			System.out.println("running POST command");
			System.out.println("URL \n" + url);
			System.out.println("Header \n" + resp.getHeaders().toString());
			System.out.println("Response body \n" + resp.asString());
			System.out.println("Time taken to get response is \n" + resp.getTime()+" milli second");

		} catch (Exception e) {
			System.out.println(e);
			System.out.println("something went wrong while running POSTPublisher");
		}

		return resp;
	}
	
	
	/**
	 * Returns response of PUT API method execution based on query parameters
	 
	 * @param headers
	 * @param url
	 * @return
	 */
	public static Response PUT(String baseURL,Map<String, String> headers) {
		setBaseURL(baseURL);
		Response resp = RestAssured.given()
				.headers(headers).put()
				.andReturn();
		System.out.println("running PUT command");
		System.out.println("Header \n" + resp.getHeaders().toString());
		System.out.println("Response body \n" + resp.asString());
		System.out.println("Time taken to get response is \n" + resp.getTime()+" milli second");

		return resp;

	}

	/**
	 * Returns response of PUT API method execution
	 * 
	 * @param headerKey
	 * @param headerValue
	 * @param contentTypeJson
	 * @param body
	 * @param url
	 * @return Response of PUT command
	 */
	public static Response PUT(String baseURL,Map<String, String> headers,String username,String password
			,String body, String url){
		setBaseURL(baseURL);
		RestAssured.useRelaxedHTTPSValidation();
		Response resp = RestAssured.given()
				.headers(headers).auth().basic(username, password)
				.body(body).log().all()
				.put(url)
				.andReturn();
		System.out.println("running PUT command");
		System.out.println("URL \n" + url);
		System.out.println("Header \n" + resp.getHeaders().toString());
		System.out.println("Response body \n" + resp.asString());
		System.out.println("Time taken to get response is \n" + resp.getTime()+" milli second");

		return resp;

	}


	/**
	 * Returns response of PUT API method execution based on query parameters
	 * @param baseURL
	 * @param headers
	 * @param inputData
	 * @param contentTypeJson
	 * @param body
	 * @param url
	 * @return
	 */
	public static Response PUT(String baseURL,Map<String, String> headers,Map<String, String> inputData, String body, String url) {
		setBaseURL(baseURL);
		Response resp = RestAssured.given()
				.headers(headers).parameters(inputData)
				.body(body)
				.put(url)
				.andReturn();
		System.out.println("running PUT command");
		System.out.println("URL \n" + url);
		System.out.println("Header \n" + resp.getHeaders().toString());
		System.out.println("Response body \n" + resp.asString());
		System.out.println("Time taken to get response is \n" + resp.getTime()+" milli second");

		return resp;

	}


	/**
	 * Returns response of DELETE API method execution
	 * 
	 * @param headerKey
	 * @param headerValue
	 * @param contentTypeJson
	 * @param body
	 * @param url
	 * @return Response of DELETE command
	 */
	public static Response DELETE(String baseURL,Map<String, String> headers,String username,String password,
			String contentTypeJson, String url) {
		setBaseURL(baseURL);
		Response resp = RestAssured.given().headers(headers)
				.delete(url);

		System.out.println("running DELETE command");
		System.out.println("URL \n" + url);
		System.out.println("Header \n" + resp.getHeaders().toString());
		System.out.println("Response body \n" + resp.asString());
		System.out.println("Time taken to get response is \n" + resp.getTime()+" milli second");

		return resp;

	}

	public static Response POST(String baseURL,Map<String, String> headers,
			Map<String, String> inputData, String url) {
		setBaseURL(baseURL);
		Response resp = RestAssured.given().headers(headers)
				.formParams(inputData).post(url).andReturn();
		System.out.println("running POST command");
		System.out.println("URL \n" + url);
		System.out.println("Header \n" + resp.getHeaders().toString());
		System.out.println("Response body \n" + resp.asString());
		System.out.println("Time taken to get response is \n" + resp.getTime()+" milli second");

		return resp;

	}

	/**verifyStatus methods verify status of the request sent
	 * @param headers
	 * @param userName
	 * @param password
	 * @param contentTypeJson
	 * @param URL
	 * @param expectedValue
	 * @return responseFlag true or false
	 */
	public static boolean verifyStatus(Map<String, String> headers,String userName,String password,String contentTypeJson,String URL,int expectedValue)
	{
		boolean responseFlag = false;	
		Response resp = GET("",headers,userName,password,contentTypeJson,URL);	
		if(resp.getStatusCode()==expectedValue)
		{
			responseFlag=true;
		}			
		return responseFlag;

	}

	/**closeConnection method would be closing idle Connection
	 * 
	 */
	public static void closeConnection()
	{
		RestAssured.config = RestAssuredConfig.newConfig().connectionConfig(ConnectionConfig.connectionConfig().closeIdleConnectionsAfterEachResponse());
	}

	public static Response DELETE(String baseURL,Map<String, String> headers,Map<String, String> queryParam, String url) {
		setBaseURL(baseURL);
		Response resp = RestAssured.given()
				.parameters(queryParam)
				.headers(headers)
				.delete(url);

		System.out.println("running GET command");		
		System.out.println("URL: \n" + url+"\n");
		System.out.println("Header: \n" + resp.getHeaders().toString()+"\n");
		System.out.println("Response body: \n" + resp.asString()+"\n");
		System.out.println("Status Code: \n" + resp.getStatusCode()+"\n");		
		System.out.println("Time taken to get response is: \n" + resp.getTime()+" milli second\n");
		return resp;
	}
	
	
     public static Response DELETE(String baseURL,Map<String, String> headers,String inputpara,String contentType, String url) 
     {
            setBaseURL(baseURL);
            Response resp = RestAssured.given()
                         .headers(headers).queryParam(inputpara)
                         .contentType(contentType).delete(url);

            System.out.println("running Delete command");          
            System.out.println("URL: \n" + url+"\n");
            System.out.println("Header: \n" + resp.getHeaders().toString()+"\n");
            System.out.println("Response body: \n" + resp.asString()+"\n");
            System.out.println("Status Code: \n" + resp.getStatusCode()+"\n");         
            System.out.println("Time taken to get response is: \n" + resp.getTime()+" milli second\n");
            return resp;
     }
     
     public static String generateStringFromResource(String path) throws IOException  {

    	    return new String(Files.readAllBytes(Paths.get(path)));

    	}

}