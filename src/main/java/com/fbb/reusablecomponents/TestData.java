package com.fbb.reusablecomponents;

import java.util.HashMap;
import java.util.List;

import com.fbb.support.BaseTest;
import com.fbb.support.Brand;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.Utils;

public class TestData {

	public static HashMap<Integer, HashMap<String, String>> testData = new HashMap<Integer, HashMap<String, String>>();
	public static HashMap<String, List<String>> missingData = new HashMap<String, List<String>>();
	public static HashMap<String, List<String>> emailData = new HashMap<String, List<String>>();
	
	public static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
	public static EnvironmentPropertiesReader envProperty = EnvironmentPropertiesReader.getInstance("env");
	public static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	public static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");

	public static HashMap<String, String> others_rm = new HashMap<String, String>();
	public static HashMap<String, String> others_ww = new HashMap<String, String>();
	public static HashMap<String, String> others_ks = new HashMap<String, String>();
	public static HashMap<String, String> others_el = new HashMap<String, String>();
	public static HashMap<String, String> others_jl = new HashMap<String, String>();
	public static HashMap<String, String> others_bh = new HashMap<String, String>();
	public static HashMap<String, String> others_sa = new HashMap<String, String>();
	public static HashMap<String, String> others_fb = new HashMap<String, String>();

	public static HashMap<String, String> data_rm = new HashMap<String, String>();
	public static HashMap<String, String> data_ww = new HashMap<String, String>();
	public static HashMap<String, String> data_ks = new HashMap<String, String>();
	public static HashMap<String, String> data_el = new HashMap<String, String>();
	public static HashMap<String, String> data_jl = new HashMap<String, String>();
	public static HashMap<String, String> data_bh = new HashMap<String, String>();
	public static HashMap<String, String> data_sa = new HashMap<String, String>();
	public static HashMap<String, String> data_fb = new HashMap<String, String>();

	public static HashMap<String, String> giftcard_rm = new HashMap<String, String>();
	public static HashMap<String, String> giftcard_ww = new HashMap<String, String>();
	public static HashMap<String, String> giftcard_ks = new HashMap<String, String>();
	public static HashMap<String, String> giftcard_el = new HashMap<String, String>();
	public static HashMap<String, String> giftcard_jl = new HashMap<String, String>();
	public static HashMap<String, String> giftcard_bh = new HashMap<String, String>();
	public static HashMap<String, String> giftcard_sa = new HashMap<String, String>();
	public static HashMap<String, String> giftcard_fb = new HashMap<String, String>();

	public static HashMap<String, String> coupons_rm = new HashMap<String, String>();
	public static HashMap<String, String> coupons_ww = new HashMap<String, String>();
	public static HashMap<String, String> coupons_ks = new HashMap<String, String>();
	public static HashMap<String, String> coupons_el = new HashMap<String, String>();
	public static HashMap<String, String> coupons_jl = new HashMap<String, String>();
	public static HashMap<String, String> coupons_bh = new HashMap<String, String>();
	public static HashMap<String, String> coupons_sa = new HashMap<String, String>();
	public static HashMap<String, String> coupons_fb = new HashMap<String, String>();

	public static HashMap<String, String> reward_rm = new HashMap<String, String>();
	public static HashMap<String, String> reward_ww = new HashMap<String, String>();
	public static HashMap<String, String> reward_ks = new HashMap<String, String>();
	public static HashMap<String, String> reward_el = new HashMap<String, String>();
	public static HashMap<String, String> reward_jl = new HashMap<String, String>();
	public static HashMap<String, String> reward_bh = new HashMap<String, String>();
	public static HashMap<String, String> reward_sa = new HashMap<String, String>();
	public static HashMap<String, String> reward_fb = new HashMap<String, String>();

	public static HashMap<String, String> colorcode_rm = new HashMap<String, String>();
	public static HashMap<String, String> colorcode_ww = new HashMap<String, String>();
	public static HashMap<String, String> colorcode_ks = new HashMap<String, String>();
	public static HashMap<String, String> colorcode_el = new HashMap<String, String>();
	public static HashMap<String, String> colorcode_jl = new HashMap<String, String>();
	public static HashMap<String, String> colorcode_bh = new HashMap<String, String>();
	public static HashMap<String, String> colorcode_sa = new HashMap<String, String>();
	public static HashMap<String, String> colorcode_fb = new HashMap<String, String>();

	/**
	 * To get test data
	 * <br> Without Brand parameter, will return test data for current brand
	 * @param key
	 * @param brandName (Optional)
	 * @return Test Data
	 * @throws Exception
	 */
	public static String get(String key, Brand... brandName)throws Exception{
		Brand brand = (brandName.length == 0) ? Utils.getCurrentBrand() : brandName[0];
		String brandShort = brand.toString().replace("x", "");
		
		if(EnvironmentPropertiesReader.getInstance("config").get("dataSource").equals("TestRail")) {
			if(key.startsWith("_"))
				return testData.get(Utils.getCurrentTestCode()).get(key);
			else
				return testData.get(Utils.getCurrentTestCode()).get(key + "_" + brandShort);
		}else if(EnvironmentPropertiesReader.getInstance("config").get("dataSource").equals("g-sheet")){
			if(missingData.containsKey(brandShort) && missingData.get(brandShort).contains(key))
				Log.fail("Requested data("+ key + ") not available.");
			return getData(brand, key);
		}else
			return null;//BaseTest.prdData.get(key);
	}
	
	/**
	 * To get test data
	 * <br> Without Brand parameter, will return test data for current brand
	 * @param key
	 * @param brandName (Optional)
	 * @return Test Data
	 * @throws Exception
	 */
	public static boolean verifyEmailIDExist(String emailID, Brand... brandName)throws Exception{
		if(Boolean.getBoolean("emailIDCheck") || configProperty.get("emailIDCheck").equals("true")) {
			Brand brand = (brandName.length == 0) ? Utils.getCurrentBrand() : brandName[0];
			String brandShort = brand.toString().replace("x", "");
			String brandDevice = "";
			if (Utils.isDesktop()) {
				brandDevice = brandShort+"_des";
			} else if (Utils.isMobile()) {
				brandDevice = brandShort+"_mob";
			} else {
				brandDevice = brandShort+"_tab";
			}
			if(EnvironmentPropertiesReader.getInstance("config").get("dataSource").equals("g-sheet")){
				if(emailData.containsKey(brandDevice) && emailData.get(brandDevice).contains(emailID)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			Log.event("Email ID verification is disabled, Hence returning false");
			return false;
		}
	}
	
	public static HashMap<String, String> getAllProductData(Brand... brand) {
		Brand brandToGet = brand.length > 0 ? brand[0] : Utils.getCurrentBrand();
		Log.event("Getiing product data list for " + brandToGet.getConfiguration());
		
		switch (brandToGet) {
		case rmx:
		case rm:
			return data_rm;
		case wwx:
		case ww:
			return data_ww;
		case ksx:
		case ks:
			return data_ks;
		case jlx:
		case jl:
			return data_jl;
		case elx:
		case el:
			return data_el;
		case bhx:
		case bh:
			return data_bh;
		case fbx:
		case fb:
			return data_fb;
		case sax:
		case sa:
			return data_sa;
		}
		return null;
	}
	
	private static String getData(Brand brand, String key) {
		switch(brand) {
		case rmx:
		case rm: if(data_rm.containsKey(key))
			return data_rm.get(key);
		else if(giftcard_rm.containsKey(key))
			return giftcard_rm.get(key);
		else if(coupons_rm.containsKey(key))
			return coupons_rm.get(key);
		else if(reward_rm.containsKey(key))
			return reward_rm.get(key);
		else if(colorcode_rm.containsKey(key))
			return colorcode_rm.get(key);
		else
			return others_rm.get(key);
		
		case wwx:
		case ww: if(data_ww.containsKey(key))
			return data_ww.get(key);
		else if(giftcard_ww.containsKey(key))
			return giftcard_ww.get(key);
		else if(coupons_ww.containsKey(key))
			return coupons_ww.get(key);
		else if(reward_ww.containsKey(key))
			return reward_ww.get(key);
		else if(colorcode_ww.containsKey(key))
			return colorcode_ww.get(key);
		else
			return others_ww.get(key);
		
		case ksx:
		case ks: if(data_ks.containsKey(key))
			return data_ks.get(key);
		else if(giftcard_ks.containsKey(key))
			return giftcard_ks.get(key);
		else if(coupons_ks.containsKey(key))
			return coupons_ks.get(key);
		else if(reward_ks.containsKey(key))
			return reward_ks.get(key);
		else if(colorcode_ks.containsKey(key))
			return colorcode_ks.get(key);
		else
			return others_ks.get(key);
		
		case elx:
		case el: if(data_el.containsKey(key))
			return data_el.get(key);
		else if(giftcard_el.containsKey(key))
			return giftcard_el.get(key);
		else if(coupons_el.containsKey(key))
			return coupons_el.get(key);
		else if(reward_el.containsKey(key))
			return reward_el.get(key);
		else if(colorcode_el.containsKey(key))
			return colorcode_el.get(key);
		else
			return others_el.get(key);
		
		case jlx:
		case jl: if(data_jl.containsKey(key))
			return data_jl.get(key);
		else if(giftcard_jl.containsKey(key))
			return giftcard_jl.get(key);
		else if(coupons_jl.containsKey(key))
			return coupons_jl.get(key);
		else if(reward_jl.containsKey(key))
			return reward_jl.get(key);
		else if(colorcode_jl.containsKey(key))
			return colorcode_jl.get(key);
		else
			return others_jl.get(key);
		
		case bhx:
		case bh: if(data_bh.containsKey(key))
			return data_bh.get(key);
		else if(giftcard_bh.containsKey(key))
			return giftcard_bh.get(key);
		else if(coupons_bh.containsKey(key))
			return coupons_bh.get(key);
		else if(reward_bh.containsKey(key))
			return reward_bh.get(key);
		else if(colorcode_bh.containsKey(key))
			return colorcode_bh.get(key);
		else
			return others_bh.get(key);
		
		case sax:
		case sa: if(data_sa.containsKey(key))
			return data_sa.get(key);
		else if(giftcard_sa.containsKey(key))
			return giftcard_sa.get(key);
		else if(coupons_sa.containsKey(key))
			return coupons_sa.get(key);
		else if(reward_sa.containsKey(key))
			return reward_sa.get(key);
		else if(colorcode_sa.containsKey(key))
			return colorcode_sa.get(key);
		else
			return others_sa.get(key);
		
		case fbx:
		case fb: if(data_fb.containsKey(key))
			return data_fb.get(key);
		else if(giftcard_fb.containsKey(key))
			return giftcard_fb.get(key);
		else if(coupons_fb.containsKey(key))
			return coupons_fb.get(key);
		else if(reward_fb.containsKey(key))
			return reward_fb.get(key);
		else if(colorcode_fb.containsKey(key))
			return colorcode_fb.get(key);
		else
			return others_fb.get(key);
		default:
			return null;
		}
	}

	public static String product(String key)throws Exception{
		if(EnvironmentPropertiesReader.getInstance("config").get("dataSource").equals("TestRail")) {
			if(key.startsWith("_"))
				return testData.get(Utils.getCurrentTestCode()).get(key);
			else
				return testData.get(Utils.getCurrentTestCode()).get(key + "_" + Utils.getCurrentBrandShort());
		}else if(EnvironmentPropertiesReader.getInstance("config").get("dataSource").equals("g-sheet")){

			switch(Utils.getCurrentBrand()) {
				case rmx:
				case rm: return data_rm.get(key);
				
				case wwx:
				case ww: return data_ww.get(key);
				
				case ksx:
				case ks: return data_ks.get(key);
				
				case elx:
				case el: return data_el.get(key);
				
				case jlx:
				case jl: return data_jl.get(key);
				
				case bhx:
				case bh: return data_bh.get(key);
				
				case sax:
				case sa: return data_sa.get(key);
				
				case fbx:
				case fb: return data_fb.get(key);
				
				default:
					return null;
			}
		}else
			return BaseTest.prdData.get(key);
	}

	public static String giftcard(String key)throws Exception{
		if(EnvironmentPropertiesReader.getInstance("config").get("dataSource").equals("TestRail")) {
			if(key.startsWith("_"))
				return testData.get(Utils.getCurrentTestCode()).get(key);
			else
				return testData.get(Utils.getCurrentTestCode()).get(key + "_" + Utils.getCurrentBrandShort());
		}else if(EnvironmentPropertiesReader.getInstance("config").get("dataSource").equals("g-sheet")){

			switch(Utils.getCurrentBrand()) {
			case rm: return giftcard_rm.get(key);
			case ww: return giftcard_ww.get(key);
			case ks: return giftcard_ks.get(key);
			case el: return giftcard_el.get(key);
			case jl: return giftcard_jl.get(key);
			case bh: return giftcard_bh.get(key);
			case sa: return giftcard_sa.get(key);
			case fb: return giftcard_fb.get(key);
			default:
				return null;
			}
		}else
			return BaseTest.prdData.get(key);
	}

	public static String coupon(String key)throws Exception{
		if(EnvironmentPropertiesReader.getInstance("config").get("dataSource").equals("TestRail")) {
			if(key.startsWith("_"))
				return testData.get(Utils.getCurrentTestCode()).get(key);
			else
				return testData.get(Utils.getCurrentTestCode()).get(key + "_" + Utils.getCurrentBrandShort());
		}else if(EnvironmentPropertiesReader.getInstance("config").get("dataSource").equals("g-sheet")){

			switch(Utils.getCurrentBrand()) {
			case rm: return coupons_rm.get(key);
			case ww: return coupons_ww.get(key);
			case ks: return coupons_ks.get(key);
			case el: return coupons_el.get(key);
			case jl: return coupons_jl.get(key);
			case bh: return coupons_bh.get(key);
			case sa: return coupons_sa.get(key);
			case fb: return coupons_fb.get(key);
			default:
				return null;
			}
		}else
			return BaseTest.prdData.get(key);
	}

	public static String reward(String key)throws Exception{
		if(EnvironmentPropertiesReader.getInstance("config").get("dataSource").equals("TestRail")) {
			if(key.startsWith("_"))
				return testData.get(Utils.getCurrentTestCode()).get(key);
			else
				return testData.get(Utils.getCurrentTestCode()).get(key + "_" + Utils.getCurrentBrandShort());
		}else if(EnvironmentPropertiesReader.getInstance("config").get("dataSource").equals("g-sheet")){

			switch(Utils.getCurrentBrand()) {
			case rm: return reward_rm.get(key);
			case ww: return reward_ww.get(key);
			case ks: return reward_ks.get(key);
			case el: return reward_el.get(key);
			case jl: return reward_jl.get(key);
			case bh: return reward_bh.get(key);
			case sa: return reward_sa.get(key);
			case fb: return reward_fb.get(key);
			default:
				return null;
			}
		}else
			return BaseTest.prdData.get(key);
	}

	public static String colorcode(String key)throws Exception{
		if(EnvironmentPropertiesReader.getInstance("config").get("dataSource").equals("TestRail")) {
			if(key.startsWith("_"))
				return testData.get(Utils.getCurrentTestCode()).get(key);
			else
				return testData.get(Utils.getCurrentTestCode()).get(key + "_" + Utils.getCurrentBrandShort());
		}else if(EnvironmentPropertiesReader.getInstance("config").get("dataSource").equals("g-sheet")){

			switch(Utils.getCurrentBrand()) {
			case rm: return colorcode_rm.get(key);
			case ww: return colorcode_ww.get(key);
			case ks: return colorcode_ks.get(key);
			case el: return colorcode_el.get(key);
			case jl: return colorcode_jl.get(key);
			case bh: return colorcode_bh.get(key);
			case sa: return colorcode_sa.get(key);
			case fb: return colorcode_fb.get(key);
			default:
				return null;
			}
		}else
			return BaseTest.prdData.get(key);
	}
}
