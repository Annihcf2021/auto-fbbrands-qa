package com.fbb.reusablecomponents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.fbb.pages.CheckoutPage;
import com.fbb.pages.ElementLayer;
import com.fbb.pages.GiftCardPage;
import com.fbb.pages.HomePage;
import com.fbb.pages.OrderConfirmationPage;
import com.fbb.pages.PdpPage;
import com.fbb.pages.PlpPage;
import com.fbb.pages.ShoppingBagPage;
import com.fbb.pages.SignIn;
import com.fbb.pages.account.AddressesPage;
import com.fbb.pages.account.EmailPreferencePage;
import com.fbb.pages.account.MailDropPage;
import com.fbb.pages.account.MyAccountPage;
import com.fbb.pages.account.PaymentMethodsPage;
import com.fbb.pages.account.ProfilePage;
import com.fbb.pages.account.WishListPage;
import com.fbb.pages.footers.Footers;
import com.fbb.pages.headers.Headers;
import com.fbb.pages.headers.PopUp;
import com.fbb.pages.ordering.QuickOrderPage;
import com.fbb.reusablecomponents.Enumerations.ShippingMethod;
import com.fbb.support.Brand;
import com.fbb.support.BrandUtils;
import com.fbb.support.BrowserActions;
import com.fbb.support.EnvironmentPropertiesReader;
import com.fbb.support.Log;
import com.fbb.support.SheetsQuickstart;
import com.fbb.support.StopWatch;
import com.fbb.support.StringUtils;
import com.fbb.support.UrlUtils;
import com.fbb.support.Utils;

/**
 * Re-Usable methods of End-To-End Script Functionality for Retail Sites
 */
public class GlobalNavigation {
	public static ElementLayer elementLayer;

	private static EnvironmentPropertiesReader accountData = EnvironmentPropertiesReader.getInstance("accounts");
	private static EnvironmentPropertiesReader redirectData = EnvironmentPropertiesReader.getInstance("redirect");
	private static EnvironmentPropertiesReader checkoutData = EnvironmentPropertiesReader.getInstance("checkout");
	private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance("config");
	final static String spreadsheetId = configProperty.get("dataSourceID");
	
	/**
	 * Set up user account with given configuration
	 * @param driver - WebDriver instance
	 * @param accountDetails - HashMap holding account details to set
	 * <br> Format: Key should be classname.class.getName(), value should be a count or comma separated property key value
	 * @param credentials - (Optional) Credentials. Pipe separated.
	 * @return HashMap of Object holding details of set up account
	 * <br> Format: Key should be classname.class.getName(), value is HashMap is details of individual sections.
	 * @throws Exception
	 */
	public static LinkedHashMap<String, Object> setupTestUserAccount(WebDriver driver, HashMap<String, String> accountDetails, String... credentials) throws Exception {
		long startTime = StopWatch.startTime();
		LinkedHashMap<String, Object> objToReturn = new LinkedHashMap<String, Object>();
		
		HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
		String email = credentials.length > 0 ? credentials[0].split("\\|")[0] : AccountUtils.generateEmail(driver);
		String password = credentials.length > 0 ? credentials[0].split("\\|")[1] : accountData.get("password_global");
		HashMap<String, String> finalCredentials = new HashMap<>();
		finalCredentials.put("email", email);
		finalCredentials.put("password", password);
		objToReturn.put("credentials", finalCredentials);
		
		MyAccountPage accountPage = homePage.headers.navigateToMyAccountWithRememberChkBox(email, password, false, true);
		objToReturn.put(MyAccountPage.class.getName(), accountPage);
		objToReturn.put(HomePage.class.getName(), homePage);
		
		if(accountDetails.get(AddressesPage.class.getName()) != null) {
			objToReturn = setupSavedAddress(driver, objToReturn, accountDetails.get(AddressesPage.class.getName()));
			Log.event("Address set up:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
		}
		
		if (accountDetails.get(PaymentMethodsPage.class.getName()) != null) {
			objToReturn = setupSavedPayment(driver, objToReturn, accountDetails.get(PaymentMethodsPage.class.getName()));
			Log.event("Payment set up:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
		}

		if (accountDetails.get(WishListPage.class.getName()) != null) {
			objToReturn = setupWishlistProduct(driver, objToReturn, accountDetails.get(WishListPage.class.getName()));
			Log.event("Wishlist set up:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
		}

		if (accountDetails.get(CheckoutPage.class.getName()) != null) {
			objToReturn = setupCartPage(driver, objToReturn, accountDetails.get(CheckoutPage.class.getName()));
			Log.event("Shopping Bag set up:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
		}
		
		accountPage = (MyAccountPage) objToReturn.get(MyAccountPage.class.getName());
		homePage.headers.logoutUser();
				
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		Log.reference("Elapsed time on setupTestUserAccount for " + stackTrace[2].getMethodName() + " :: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)), false);
		return objToReturn;
	}
	
	private static LinkedHashMap<String, Object> setupSavedAddress(WebDriver driver, LinkedHashMap<String, Object> objToReturn, String address) throws Exception {
		HomePage homePage = (HomePage) objToReturn.get(HomePage.class.getName());
		MyAccountPage accountPage = (MyAccountPage) objToReturn.get(MyAccountPage.class.getName());
		AddressesPage addressPg = accountPage.navigateToAddressPage();
		HashMap<String, String> savedAddresses = new HashMap<>();
		if (address.length() == 1) {
			int addressCount = StringUtils.getNumberInString(address);
			if (addressCount <= 4) {
				int i = 1;
				if (addressPg.getSavedAddressesCount() > addressCount) {
					addressPg.deleteAllTheAddresses();
				} else {
					i = i + addressPg.getSavedAddressesCount();
				}

				while ((i <= 4) && (i++ <= addressCount)) {
					String addressKey = "address_" + i;
					addressPg.addNewAddress(addressKey, false);
					savedAddresses.put(addressKey, checkoutData.getProperty(addressKey));
					BrowserActions.refreshPage(driver);
				}
			}
		} else {
			List<String> listAddress = StringUtils.splitStringToList(address);
			addressPg.deleteAllTheAddresses();
			for (String addressKey : listAddress) {
				addressPg.addNewAddress(addressKey, false);
				savedAddresses.put(addressKey, checkoutData.getProperty(addressKey));
				BrowserActions.refreshPage(driver);
			}
		}
		objToReturn.put(AddressesPage.class.getName(), savedAddresses);
		accountPage = homePage.headers.navigateToMyAccount();
		objToReturn.put(MyAccountPage.class.getName(), accountPage);
		return objToReturn;
	}
	
	private static LinkedHashMap<String, Object> setupSavedPayment(WebDriver driver, LinkedHashMap<String, Object> objToReturn, String payment) throws Exception {
		HomePage homePage = (HomePage) objToReturn.get(HomePage.class.getName());
		MyAccountPage accountPage = (MyAccountPage) objToReturn.get(MyAccountPage.class.getName());
		PaymentMethodsPage paymentPg = accountPage.navigateToPaymentMethods();
		HashMap<String, String> savedPayments = new HashMap<>();
		if (payment.length() == 1) {
			int paymentCount = StringUtils.getNumberInString(payment);
			if (paymentCount >= 1 && paymentCount <= 4) {
				int i = paymentPg.getNumberOfSavedCards() + 1;
				while ((i <= 4) && (i++ < paymentCount)) {
					String cardKey = "card_" + i;
					paymentPg.addNewCard(cardKey);
					savedPayments.put(cardKey, checkoutData.getProperty(cardKey));
				}
			} else if (paymentCount == 0) {
				paymentPg.deleteAllSavedCards();
			}
		} else {
			List<String> listPayment = StringUtils.splitStringToList(payment);
			paymentPg.deleteAllSavedCards();
			for (String cardKey : listPayment) {
				paymentPg.addNewCard(cardKey);
				savedPayments.put(cardKey, checkoutData.getProperty(cardKey));
			}
		}
		objToReturn.put(PaymentMethodsPage.class.getName(), savedPayments);
		accountPage = homePage.headers.navigateToMyAccount();
		objToReturn.put(MyAccountPage.class.getName(), accountPage);
		return objToReturn;
	}
	
	private static LinkedHashMap<String, Object> setupWishlistProduct(WebDriver driver, LinkedHashMap<String, Object> objToReturn, String wishlistProduct) throws Exception {
		HomePage homePage = (HomePage) objToReturn.get(HomePage.class.getName());
		MyAccountPage accountPage = (MyAccountPage) objToReturn.get(MyAccountPage.class.getName());
		HashMap<String, String> wishlistProducts = new HashMap<>();
		if (wishlistProduct.length() == 1) {
			int productCount = StringUtils.getNumberInString(wishlistProduct);
			for(int i = 1 ; i <= productCount ; i++) {
				PlpPage plp = homePage.headers.navigateToRandomCategory();
				PdpPage pdp = plp.navigateToPdp();
				pdp.addToWishlist();
				wishlistProducts.put("wishlist_" + i, pdp.getVariationFromCartOverlay());
			}
		} else {
			List<String> listProduct = StringUtils.splitStringToList(wishlistProduct, ",");
			for(String prdKey : listProduct) {
				prdKey = TestData.get(prdKey) == null ? prdKey : TestData.get(prdKey);
				PdpPage pdp = homePage.headers.navigateToPDP(prdKey);
				pdp.addToWishlist();
				wishlistProducts.put(prdKey, pdp.getVariationFromCartOverlay());
			}
		}
		objToReturn.put(WishListPage.class.getName(), wishlistProducts);
		accountPage = homePage.headers.navigateToMyAccount();
		objToReturn.put(MyAccountPage.class.getName(), accountPage);
		return objToReturn;
	}
	
	private static LinkedHashMap<String, Object> setupCartPage(WebDriver driver, LinkedHashMap<String, Object> objToReturn, String cartProduct) throws Exception {
		HomePage homePage = (HomePage) objToReturn.get(HomePage.class.getName());
		MyAccountPage accountPage = (MyAccountPage) objToReturn.get(MyAccountPage.class.getName());
		HashMap<String, String> cartProducts = new HashMap<>();
		if (cartProduct.length() == 1) {
			int productCount = StringUtils.getNumberInString(cartProduct);
			for(int i = 1 ; i <= productCount ; i++) {
				PlpPage plp = homePage.headers.navigateToRandomCategory();
				PdpPage pdp = plp.navigateToPdp();
				pdp.addToBagCloseOverlay();
				cartProducts.put("cart_" + i, pdp.getVariationFromCartOverlay());
			}
		} else {
			List<String> listProduct = StringUtils.splitStringToList(cartProduct, ",");
			for(String prdKey : listProduct) {
				prdKey = TestData.get(prdKey) == null ? prdKey : TestData.get(prdKey);
				PdpPage pdp = homePage.headers.navigateToPDP(prdKey);
				pdp.addToBagCloseOverlay();
				cartProducts.put(prdKey, pdp.getVariationFromCartOverlay());
			}
		}
		objToReturn.put(ShoppingBagPage.class.getName(), cartProducts);
		accountPage = homePage.headers.navigateToMyAccount();
		objToReturn.put(MyAccountPage.class.getName(), accountPage);
		return objToReturn;
	}

	public static Object[] checkout(WebDriver driver, String searchKey, int i, String credentials)throws Exception{
		long startTime = StopWatch.startTime();
		String username = new String();
		String password = new String();
		String chkType = new String();
		boolean productAddRequired = false;
		if(credentials.contains("|")){
			username = credentials.split("\\|")[0];
			password = credentials.split("\\|")[1];
			chkType = "user";
		}else{
			username = credentials;
			chkType = "guest";
		}

		HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
		Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

		ShoppingBagPage cartPage = null;
		PdpPage pdpPage = null;
		CheckoutPage checkoutPage =  null;
		Headers headers = homePage.headers;
		if(chkType.equals("user")){
			Log.message(i++ + ". Trying to Login with :: " + username + " / " + password, driver);

			homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Signed In as :: " + headers.getUserName(), driver);
			if(!headers.getMiniCartCount().equals("0")){

				cartPage = headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);

				cartPage.removeAllItemsFromCart();
				productAddRequired = true;
				Log.message(i++ + ". Removed All other Items from the Cart except given product id's.", driver);

				headers = new Headers(driver).get();
			}else{
				productAddRequired = true;
			}

		}else{
			productAddRequired = true;
		}

		if(productAddRequired){
			List<String> searchKeys = Arrays.asList(searchKey.split("\\|"));

			for(String searchkey : searchKeys){
				pdpPage = homePage.headers.navigateToPDP(searchkey);
				Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);

				pdpPage.addToBagCloseOverlay();
				Log.message(i++ + ". Product Added to Cart.", driver);

				headers = new Headers(driver).get();
			}
			if(headers == null) headers = homePage.headers;
			cartPage = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
		}

		LinkedList<LinkedHashMap<String, String>> productInformation = cartPage.getProductDetails();

		if(chkType.equals("user")){
			checkoutPage = (CheckoutPage)cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
		}else{
			SignIn signIn = (SignIn) cartPage.navigateToCheckout();
			signIn.typeGuestEmail(username);
			Log.message(i++ + ". Guest Email Id("+ username +") entered.", driver);
			checkoutPage = signIn.clickOnContinueInGuestLogin();
			Log.message(i++ + ". Navigated to Checkout page", driver);
		}

		//Handling PLCC Modal
		while(checkoutPage.getPLCCModalStatus())
			checkoutPage.closePLCCOfferByNoThanks1();

		Object[] obj = new Object[3];
		obj[0] = checkoutPage;
		obj[1] = i;
		obj[2] = productInformation;

		Log.event("Elapsed time on checkout:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
		return obj;
	}
	
	/**
	 * Re-Usable methods of End-To-End Script Functionality for Gift/eGift Card
	 * @param WebDriver - driver
	 * @param String - type of card - gc/egc
	 * @param int - current index
	 * @param String - Login credentials(Optional)
	 */
	public static Object[] checkoutGiftEgiftCard(WebDriver driver, String type, int i, String... credentials)throws Exception{
		String username = new String();
		String password = new String();
		String chkType = new String();
		String authenticated_gc_url = UrlUtils.getProtectedWebSiteWithYotta(Utils.getWebSite() + UrlUtils.getPath(TestData.get("prd_gc_physical_url")));
		String authenticated_egc_url = UrlUtils.getProtectedWebSiteWithYotta(Utils.getWebSite() + UrlUtils.getPath(TestData.get("prd_egift_card_url")));
		boolean productAddRequired = false;
		
		if(credentials[0].contains("|")){
			username = credentials[0].split("\\|")[0];
			password = credentials[0].split("\\|")[1];
			chkType = "user";
		}else{
			username = credentials[0];
			chkType = "guest";
		}

		HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
		Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);
		ShoppingBagPage cartPage = null;
		PdpPage pdpPage = null;
		CheckoutPage checkoutPage =  null;
		Headers headers = homePage.headers;
		
		if(chkType.equals("user")){
			Log.message(i++ + ". Trying to Login with :: " + username + " / " + password, driver);

			homePage.headers.navigateToMyAccount(username, password, true);
			Log.message(i++ + ". Signed In as :: " + headers.getUserName(), driver);
			if(!headers.getMiniCartCount().equals("0")){
				cartPage = headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
				cartPage.removeAllItemsFromCart();
				productAddRequired = true;
				Log.message(i++ + ". Removed All other Items from the Cart except given product id's.", driver);
				headers = new Headers(driver).get();
			}else{
				productAddRequired = true;
			}

		}else{
			productAddRequired = true;
		}
		
		if(productAddRequired){
			GiftCardPage gcLanding = homePage.footers.navigateToGiftCardLandingPage();
			Log.message(i++ + ". Navigated to Gift Card Landing Page", driver);
			
			if(type.equals("gc")) {
				try {
					pdpPage = gcLanding.navigateToGiftCards();
				}catch(Exception e) {
					driver.get(authenticated_gc_url);
					pdpPage = new PdpPage(driver).get();
				}
				Log.message(i++ + ". Navigated to Gift Card", driver);
			}
			if(type.equals("egc")) {
				try {
					pdpPage = gcLanding.navigateToGiftCertificate();
				}catch(Exception e) {
					driver.get(authenticated_egc_url);
					pdpPage = new PdpPage(driver).get();
				}
				Log.message(i++ + ". Navigated to eGift Card", driver);
			}
			
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Card Added to Cart.", driver);

			headers = new Headers(driver).get();
			if(headers == null) headers = homePage.headers;
			cartPage = headers.navigateToShoppingBagPage();
			Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);
		}

		LinkedList<LinkedHashMap<String, String>> productInformation = cartPage.getProductDetails();
		if(chkType.equals("user")){
			checkoutPage = (CheckoutPage)cartPage.navigateToCheckout();
			Log.message(i++ + ". Navigated to Checkout Page.", driver);
		}else{
			SignIn signIn = (SignIn) cartPage.navigateToCheckout();
			signIn.typeGuestEmail(username);
			Log.message(i++ + ". Guest Email Id("+ username +") entered.", driver);
			checkoutPage = signIn.clickOnContinueInGuestLogin();
			Log.message(i++ + ". Navigated to Checkout page", driver);
		}

		//Handling PLCC Modal
		while(checkoutPage.getPLCCModalStatus())
			checkoutPage.closePLCCOfferByNoThanks1();

		Object[] obj = new Object[3];
		obj[0] = checkoutPage;
		obj[1] = i;
		obj[2] = productInformation;
		return obj;
	}

	public static Object[] shoppingCart(WebDriver driver, String searchKey, int i, String... credentials)throws Exception{
		String username = new String();
		String password = new String();
		String chkType = new String();
		if(credentials.length > 0) {
			if(credentials[0].contains("|")){
				username = credentials[0].split("\\|")[0];
				password = credentials[0].split("\\|")[1];
				chkType = "user";
			}else{
				username = credentials[0];
				chkType = "guest";
			}
		}else {
			username = AccountUtils.generateEmail(driver);
			chkType = "guest";
		}

		HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
		Log.message(i++ + ". Navigated to 'Full Beauty Brands' Home Page!", driver);

		ShoppingBagPage cartPage = null;
		PdpPage pdpPage = null;
		Headers headers = homePage.headers;
		if(chkType.equals("user")){
			homePage.headers.navigateToMyAccount(username, password);
			Log.message(i++ + ". Signed In as :: " + headers.getUserName(), driver);
			if(!headers.getMiniCartCount().equals("0")){

				cartPage = headers.navigateToShoppingBagPage();
				Log.message(i++ + ". Navigated to Shopping Bag Page.", driver);

				cartPage.removeAllItemsFromCart();
				Log.message(i++ + ". Removed All Items from the Cart.", driver);

				headers = new Headers(driver).get();
			}

		}

		List<String> searchKeys = Arrays.asList(searchKey.split("\\|"));

		for(String searchkey : searchKeys){
			BrowserActions.refreshPage(driver);
			pdpPage = homePage.headers.navigateToPDP(searchkey);
			Log.message(i++ + ". Navigated to PDP Page for Product :: " + pdpPage.getProductName(), driver);

			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Cart.", driver);

			headers = new Headers(driver).get();
		}
		if(headers == null) headers = homePage.headers;

		cartPage = headers.navigateToShoppingBagPage();
		Log.message(i++ + ". Navigated to Shopping Bag Page." + cartPage.getCheckoutID(), driver);

		Object[] obj = new Object[2];
		obj[0] = cartPage;
		obj[1] = i;

		return obj;
	}

	public static Object[] addProduct_Checkout(WebDriver driver, String searchKey, int i, String credentials, boolean... returnPrdNames)throws Exception{
		long startTime = StopWatch.startTime();
		HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
		Utils.waitForPageLoad(driver);

		LinkedHashMap<String, String> prdNames = new LinkedHashMap<String, String>();
		Headers headers = homePage.headers;
		PdpPage pdpPage = null;
		for(int x=0; x < searchKey.split("\\|").length; x++){
			pdpPage = headers.navigateToPDP(searchKey.split("\\|")[x]);
			if(returnPrdNames.length > 0 && (returnPrdNames[0])) {
				String prdName = pdpPage.getProductName();
				prdNames.put(searchKey.split("\\|")[x], prdName);
				Log.message(i++ + ". Navigated to PDP Page for Product :: " + prdName, driver);
			}else
				Log.message(i++ + ". Navigated to PDP Page", driver);

			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Bag!", driver);
		}

		ShoppingBagPage cartPage = headers.navigateToShoppingBagPage();//new ShoppingBagPage(driver).get();
		Log.message(i++ + ". Navigated to Cart Page.", driver);

		CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
		Log.message(i++ + ". Navigated to Checkout Page - SignIn Section", driver);

		checkoutPage.continueToShipping(credentials);
		Log.message(i++ + ". Checkout As :: " + credentials.split("\\|")[0], driver);

		Object[] obj = new Object[3];
		obj[0] = checkoutPage;
		obj[1] = i;
		obj[2] = prdNames;

		Log.event("Elapsed time on addProduct_Checkout:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
		return obj;
	}
	
	public static Object[] addProduct_PlaceOrder(WebDriver driver, String searchKey, int i, String credentials)throws Exception{

		Object[] obj= addProduct_Checkout(driver, searchKey, i, credentials);

		CheckoutPage checkoutPage = (CheckoutPage) obj[0];
		i = (Integer) obj[1];

		checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", ShippingMethod.Standard, "valid_address7");
		Log.message(i++ + ". Shipping Address entered successfully", driver);

		checkoutPage.continueToPayment();
		Log.message(i++ + ". Continued to Payment Page", driver);

		checkoutPage.fillingCardDetails1("card_Visa", false, false);
		Log.message(i++ + ". Card Details filling Successfully", driver);

		checkoutPage.clickOnPaymentDetailsContinueBtn();
		Log.message(i++ + ". Continued to Review & Place Order", driver);

		OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
		Log.message(i++ + ". Order Placed successfully", driver);

		LinkedHashMap<String, String> orderInfo = new LinkedHashMap<String, String>();
		orderInfo.put("OrderNumber", receipt.getOrderNumber());
		orderInfo.put("OrderDate", receipt.getOrderDate());
		orderInfo.put("OrderTotal", receipt.getOrderTotal());

		HomePage homePage = new Headers(driver).get().clickOnBrandFromHeader();
		Log.message(i++ + ". Returned To Home Page.", driver);

		obj = new Object[5];
		obj[0] = homePage;
		obj[1] = i;
		obj[2] = "valid_address1";
		obj[3] = "card_Visa";
		obj[4] = orderInfo;

		return obj;
	}
	
	/**
	 * Add Gift card and E-gift card to checkout
	 * @param driver
	 * @param searchKey
	 * @param i
	 * @param credentials
	 * @param returnPrdNames
	 * @return
	 * @throws Exception
	 */
	public static Object[] addGCProduct_Checkout(WebDriver driver, boolean gc,boolean egc, int i, String credentials, boolean... returnPrdNames)throws Exception{
		HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
		Utils.waitForPageLoad(driver);
		
		if((gc==false && egc==false)){
		 Log.fail("Products gc or E-gc should be mentioned in parameter section" );	
		}
		LinkedHashMap<String, String> prdNames = new LinkedHashMap<String, String>();
		Footers footer = homePage.footers ;
		PdpPage pdpPage = null;
		
		if(gc){
			pdpPage = footer.navigateToPhysicalGiftCardPDP();
			Log.message(i++ +". Navigated to gift  card PDP", driver);
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Bag!", driver);
		}
		if(egc){
			if(BrandUtils.isBrand(Brand.bh)) {
				driver.get(UrlUtils.getProtectedWebSite() +"/products/any-occasion-e-gift-card/5000055.html");
				pdpPage = new PdpPage(driver).get();
			} else {
				pdpPage = footer.navigateToEGiftCard();
			}
			Log.message(i++ + "Navigated to E-gift  card PDP", driver);
			pdpPage.addToBagCloseOverlay();
			Log.message(i++ + ". Product Added to Bag!", driver);
		}

		ShoppingBagPage cartPage = new Headers(driver).get().navigateToShoppingBagPage();//new ShoppingBagPage(driver).get();
		Log.message(i++ + ". Navigated to Cart Page.", driver);

		CheckoutPage checkoutPage = cartPage.clickOnCheckoutNowBtn();
		Log.message(i++ + ". Navigated to Checkout Page - SignIn Section", driver);

		checkoutPage.continueToShipping(credentials);
		Log.message(i++ + ". Checkout As :: " + credentials.split("\\|")[0], driver);

		Object[] obj = new Object[3];
		obj[0] = checkoutPage;
		obj[1] = i;
		obj[2] = prdNames;

		return obj;
	}
	public static Object[] addGCProducts_PlaceOrder(WebDriver driver,boolean gc,boolean egc , int i, String credentials)throws Exception{

		Object[] obj= addGCProduct_Checkout(driver,gc,egc, i, credentials);

		CheckoutPage checkoutPage = (CheckoutPage) obj[0];
		i = (Integer) obj[1];

		checkoutPage.fillingShippingDetailsAsSignedInUser("NO", "NO", "YES", ShippingMethod.Standard, "valid_address7");
		Log.message(i++ + ". Shipping Address entered successfully", driver);

		checkoutPage.continueToPayment();
		Log.message(i++ + ". Continued to Payment Page", driver);
		
		checkoutPage.clickAddNewCredit();
		checkoutPage.fillingCardDetails1("card_Visa", false, false);
		Log.message(i++ + ". Card Details filling Successfully", driver);

		checkoutPage.clickOnPaymentDetailsContinueBtn();
		Log.message(i++ + ". Continued to Review & Place Order", driver);

		OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
		Log.message(i++ + ". Order Placed successfully", driver);

		LinkedHashMap<String, String> orderInfo = new LinkedHashMap<String, String>();
		orderInfo.put("OrderNumber", receipt.getOrderNumber());
		orderInfo.put("OrderDate", receipt.getOrderDate());
		orderInfo.put("OrderTotal", receipt.getOrderTotal());

		HomePage homePage = new Headers(driver).get().clickOnBrandFromHeader();
		Log.message(i++ + ". Returned To Home Page.", driver);
		logoutUser(driver);
		obj = new Object[5];
		obj[0] = homePage;
		obj[1] = i;
		obj[2] = "valid_address1";
		obj[3] = "card_Visa";
		obj[4] = orderInfo;

		return obj;
	}

	/**
	 * To remove all items from quick order
	 * @throws Exception - Exception
	 */
	public static void removeAllItemsFromQuickOrder(WebDriver driver) throws Exception {
		try {
			String nav_URL = Utils.getWebSite() + redirectData.getProperty("quick-order");
			driver.get(nav_URL);

			Log.event("---------------------------------------------");
			Log.event("-------Removing Items from Quick Order-------");
			Log.event("---------------------------------------------");

			QuickOrderPage quickOrderPage = new QuickOrderPage(driver).get();

			quickOrderPage.removeAllProducts();
			Log.event("Quick Order products are removed!.");

		} catch (Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
            Log.reference("Exception caught :: " + e.getLocalizedMessage(), driver);
            Log.event(e.getMessage());
			//throw e;
		}
	}

	public static Object[] addProduct_PlaceOrder_G(WebDriver driver, String searchKey, int i, String credentials)throws Exception{
		long startTime = StopWatch.startTime();
		Object[] obj = addProduct_Checkout(driver, searchKey, i, credentials);

		CheckoutPage checkoutPage = (CheckoutPage) obj[0];
		i = (Integer) obj[1];

		checkoutPage.fillingShippingDetailsAsGuest("NO", "valid_address7", ShippingMethod.Standard);
		Log.message(i++ + ". Shipping Address entered successfully", driver);

		checkoutPage.continueToPayment();
		Log.message(i++ + ". Continued to Payment Page", driver);

		checkoutPage.fillingCardDetails1("card_Visa", false);
		Log.message(i++ + ". Card Details filling Successfully", driver);

		checkoutPage.clickOnPaymentDetailsContinueBtn();
		Log.message(i++ + ". Continued to Review & Place Order", driver);

		OrderConfirmationPage receipt= checkoutPage.clickOnPlaceOrderButton();
		Log.message(i++ + ". Order Placed successfully", driver);

		LinkedHashMap<String, String> orderInfo = new LinkedHashMap<String, String>();
		orderInfo.put("OrderNumber", receipt.getOrderNumber());
		orderInfo.put("OrderDate", receipt.getOrderDate());
		orderInfo.put("OrderTotal", receipt.getOrderTotal());

		obj = new Object[6];
		obj[0] = checkoutPage;
		obj[1] = i;
		obj[2] = "valid_address7";
		obj[3] = "card_Visa";
		obj[4] = orderInfo;
		obj[5] = receipt;

		Log.event("Elapsed time in addProduct_PlaceOrder_G:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
		return obj;
	}

	public static Object[] Direct_Checkout(WebDriver driver, int i, String credentials)throws Exception{

		String nav_URL = UrlUtils.getProtectedWebSite() + "/account";
		driver.get(nav_URL);

		SignIn signIn = new SignIn(driver).get();
		Log.message(i++ + ". Navigated To Sign In Page", driver);

		signIn.navigateToMyAccount(credentials.split("\\|")[0], credentials.split("\\|")[1]);
		Log.message(i++ + ". Signed In As :: " + credentials.toString());

		nav_URL = UrlUtils.getProtectedWebSite() + "/checkout";
		driver.get(nav_URL);

		CheckoutPage checkoutPage = new CheckoutPage(driver).get();
		Log.message(i++ + ". Navigated to Checkout Page - SignIn Section", driver);

		Object[] obj = new Object[2];
		obj[0] = checkoutPage;
		obj[1] = i;

		return obj;
	}

	public static WishListPage RemoveWishListProducts(WebDriver driver)throws Exception{
		try{
			String wishlist = redirectData.get("wishlist");
			String nav_URL = Utils.getWebSite() + wishlist;
			driver.get(nav_URL);

			Log.event("----------------------------------------------------");
			Log.event("------------Removing Items from Wishlist------------");
			Log.event("----------------------------------------------------");

			WishListPage wishList = new WishListPage(driver).get();
			Log.event("Navigated to Wishlist page.");

			wishList.removeAllProduct();
			Log.event("Removed All Items from Wishlist.");
			
			return wishList;
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	}
	
	public static void RemoveWishListProductsWithLogin(WebDriver driver, String... emailPassword)throws Exception{
		try{
			HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
			String mail = "";
			String password = "";
			
			if(emailPassword.length > 0) {
				mail = emailPassword[0].split("\\|")[0];
				password = emailPassword[0].split("\\|")[1];
			} else {
				mail = AccountUtils.generateEmail(driver);
				password = accountData.get("password_global");
			}
			
			homePage.headers.navigateToMyAccountWithRememberChkBox(mail, password, false, true);
			RemoveWishListProducts(driver);
			
			logoutUser(driver);
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
            Log.reference("Exception caught :: " + e.getLocalizedMessage(), driver);
            Log.event(e.getMessage());
			//throw e;
		}
	}
	
	/**
	 * To add product to account wishlist
	 * @param driver instance
	 * @param Product IDs
	 * <br> Pass multiple products separated by comma (prd1, prd1...)
	 * <br> Variations should be separated by pipe (prd1color|prd1size, prd2color|prd2size...)
	 * @param if wishlist is to be made public
	 * @param optional credential
	 * @throws Exception 
	 */
	public static void createAccountAddProductsWishList(WebDriver driver, String productIDs, boolean makeItPublic,  String... emailPassword)throws Exception {
		long startTime = StopWatch.startTime();
		try{
			HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
			PdpPage pdpPage = null;
			
			String mail = "", password = "";
			
			if(emailPassword.length > 0) {
				mail = emailPassword[0].split("\\|")[0];
				password = emailPassword[0].split("\\|")[1];
			} else {
				mail = AccountUtils.generateEmail(driver);
				password = accountData.get("password_global");
			}
			homePage.headers.navigateToMyAccountWithRememberChkBox(mail, password, false, true);
			WishListPage wishListPg = RemoveWishListProducts(driver);
			
			List<String> prdList = Arrays.asList(productIDs.split("\\,"));
			for(String productID : prdList) {
				pdpPage = homePage.headers.navigateToPDP(productID);
				pdpPage.addToWishlist();
			}
			
			driver.get(Utils.getWebSite() + redirectData.get("wishlist"));
			wishListPg = new WishListPage(driver).get();
			if(makeItPublic) {
				wishListPg.clickMakeWishlistPublicToggle("public");
			} else {
				wishListPg.clickMakeWishlistPublicToggle("private");
			}
			
			Log.event("Given product added to wishlist.");
			
			logoutUser(driver);
			Log.event("Elapsed time on createAccountAddProductsWishList:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	}

	public static void RemoveAllProducts(WebDriver driver)throws Exception{
		try{
			String nav_URL = Utils.getWebSite() + redirectData.get("cart");
			driver.get(nav_URL);

			Log.event("------------------------------------------------");
			Log.event("------------Removing Items from Cart------------");
			Log.event("------------------------------------------------");

			ShoppingBagPage cartPage = new ShoppingBagPage(driver).get();
			Log.event("Navigated to Cart page.");

			cartPage.removeAllCartProduct();
			Log.event("Removed All Items from Cart.");
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			Log.reference("Exception caught :: " + e.getLocalizedMessage(), driver);
			Log.event(e.getMessage());
			//throw e;
		}
	}
	
	public static void RemoveAllProducts(String username, String password, WebDriver driver)throws Exception{
		long startTime = StopWatch.startTime();
		try{
			HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();

			Log.event("------------------------------------------------");
			Log.event("------------Removing Items from Cart------------");
			Log.event("------------------------------------------------");

			SignIn signIn = homePage.headers.navigateToSignInPage();
			Log.event("Navigated to Sign in page. -> " + driver.getCurrentUrl());
			
			signIn.navigateToMyAccount(username, password);
			Log.event("Navigated to My Account page. -> " + driver.getCurrentUrl());
			
			ShoppingBagPage cartPage = homePage.headers.navigateToShoppingBagPage();
			Log.event("Navigated to Cart page. --> " + driver.getCurrentUrl());

			cartPage.removeAllCartProduct();
			Log.event("Removed All Items from Cart. --> " + driver.getCurrentUrl());
		}catch(Exception e) {
			e.printStackTrace();
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			//throw e;
		}
		logoutUser(driver);
		Log.event("Signed out from session. --> " + driver.getCurrentUrl());
		Log.event("Elapsed time on RemoveAllProducts:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));

	}

	@FindBy(css = ".pt_account:not(.null)")
	WebElement accountPage;

	@FindBy(css = ".server-error")
	WebElement serverError;

	@FindBy(css = ".login-create .create-account")
	WebElement createAccBtn;
	
	/**
	 * To register new user with (optional) given credentials
	 * @param driver - WebDriver instance
	 * @param credentails - (Optional) user credential to create account with in "email|password" format
	 * @return page object for navigated page after account creation
	 * @throws Exception 
	 */
	public static Object registerNewUser(WebDriver driver, String... credentails)throws Exception{
		long startTime = StopWatch.startTime();
		String userID = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		if(credentails.length > 0){
			userID = credentails[0].split("\\|")[0];
			password = credentails[0].split("\\|")[1];
		}
		HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
		SignIn signIn = homePage.headers.navigateToSignInPage();
		String signInUrl = UrlUtils.getProtectedWebSite() + redirectData.get("signin");
		if(TestData.verifyEmailIDExist(userID, Utils.getCurrentBrand())) {
			Log.event("The emailID is already registered");
			return null;
		} else {
			String firstName = "Hello";
			String lastName = "Welcome";
			Log.event("Creating new account with: " + userID + " || " + password);
			
			homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
			signIn = homePage.headers.navigateToSignInPage();
			closePopup(driver);
			
			try{
				WebElement btnCreateAccount;
				if(Utils.isMobile()) {
					btnCreateAccount = driver.findElement(By.cssSelector(".create-account.hide-desktop.hide-tablet")); 
				} else {
					btnCreateAccount = driver.findElement(By.cssSelector(".login-create .create-account")); 
				}
	
				if (Utils.waitForElement(driver, btnCreateAccount)) {
					BrowserActions.clickOnElementX(btnCreateAccount, driver, "Create Account Button");
					Utils.waitForElement(driver, driver.findElement(By.id("dwfrm_profile_customer_firstname")));
				}
				
				driver.findElement(By.id("dwfrm_profile_customer_firstname")).sendKeys(firstName);
				driver.findElement(By.id("dwfrm_profile_customer_lastname")).sendKeys(lastName);
				driver.findElement(By.id("dwfrm_profile_customer_email")).sendKeys(userID);
				driver.findElement(By.id("dwfrm_profile_customer_emailconfirm")).sendKeys(userID);
				driver.findElement(By.cssSelector("input[id*='dwfrm_profile_login_password_']")).sendKeys(password);
				driver.findElement(By.cssSelector("input[id*='dwfrm_profile_login_passwordconfirm_']")).sendKeys(password);
				WebElement registerBtn = null;
	
				if(driver.findElement(By.id("wrapper")).getAttribute("class").contains("wish-list"))
					registerBtn = driver.findElement(By.name("dwfrm_profile_confirm"));
				else
					registerBtn = driver.findElement(By.name("dwfrm_login_register"));
	
				BrowserActions.scrollToViewElement(registerBtn, driver);
				BrowserActions.javascriptClick(registerBtn, driver, "Register Button");
				Log.event("Clicked On Register Button");
			}catch(NoSuchElementException e){
				Log.exception(e, driver);
			}
			Utils.waitForPageLoad(driver);
			try{
				if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(".pt_account:not(.null)")))){
					Log.message("User Account Created for :: " + userID, driver);
					saveCredentialInDataSheet(driver, userID);
					return new MyAccountPage(driver).get();
				}else if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(".server-error")))){
					Log.message("User ID already taken.", driver);
					driver.get(signInUrl);
					signIn = new SignIn(driver).get();
					signIn.navigateToMyAccount(userID, password);
					Log.event("Elapsed time on registerNewUser:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
					saveCredentialInDataSheet(driver, userID);
					return new MyAccountPage(driver).get();
				}else{
					Log.message("Unhandled Error Occured.", driver);
					return null;
				}
			}catch(NoSuchElementException e){
				try{
					if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(".server-error")))){
						Log.message("User ID already taken.", driver);
						driver.get(signInUrl);
						signIn = new SignIn(driver).get();
						signIn.navigateToMyAccount(userID, password);
						Log.event("Elapsed time on registerNewUser:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
						saveCredentialInDataSheet(driver, userID);
						return new MyAccountPage(driver).get();
					}else{
						Log.message("Unhandled Error Occured.", driver);
						return null;
					}
				}catch(NoSuchElementException e1){
					Log.message("User ID already taken.", driver);
					driver.get(signInUrl);
					signIn = new SignIn(driver).get();
					signIn.navigateToMyAccount(userID, password);
					Log.event("Elapsed time on registerNewUser:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
					saveCredentialInDataSheet(driver, userID);
					return new MyAccountPage(driver).get();
				}
			}
		}
	}
	
	public static Object registerNewUserWithGivenUserDetail(WebDriver driver, String firstName, String lastName, String... credentails)throws Exception{
		String userID = AccountUtils.generateEmail(driver);
		String password = accountData.get("password_global");
		String signInUrl = UrlUtils.getProtectedWebSite() + redirectData.get("signin");
		if(credentails.length > 0){
			userID = credentails[0].split("\\|")[0];
			password = credentails[0].split("\\|")[1];
		}
		HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
		if(TestData.verifyEmailIDExist(userID, Utils.getCurrentBrand())) {
			Log.event("The emailID is already registered");
			return null;
		} else {
			try{
				driver.findElement(By.cssSelector(".pt_account.null"));
			}catch(Exception x){
				homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
				if(Utils.isMobile()){
					homePage.headers.openCloseHamburgerMenu("open");
					homePage.headers.clickOnSignInMobile();
				}else{
					homePage.headers.navigateToSignInPage();
				}
			}
			closePopup(driver);
			Log.event("Navigated to Login Page!!!");
			
			try{
				WebElement element;
				if(Utils.isMobile()) {
					element = driver.findElement(By.cssSelector(".create-account.hide-desktop.hide-tablet")); 
				} else {
					element = driver.findElement(By.cssSelector(".login-create .create-account")); 
				}
	
				BrowserActions.clickOnElementX(element, driver, "Create Account Button");
				Utils.waitForElement(driver, driver.findElement(By.id("dwfrm_profile_customer_firstname")));
				driver.findElement(By.id("dwfrm_profile_customer_firstname")).sendKeys(firstName);
				driver.findElement(By.id("dwfrm_profile_customer_lastname")).sendKeys(lastName);
				driver.findElement(By.id("dwfrm_profile_customer_email")).sendKeys(userID);
				driver.findElement(By.id("dwfrm_profile_customer_emailconfirm")).sendKeys(userID);
				driver.findElement(By.cssSelector("input[id*='dwfrm_profile_login_password_']")).sendKeys(password);
				driver.findElement(By.cssSelector("input[id*='dwfrm_profile_login_passwordconfirm_']")).sendKeys(password);
				WebElement registerBtn = null;//driver.findElement(By.name("dwfrm_login_register"));
	
				if(driver.findElement(By.id("wrapper")).getAttribute("class").contains("wish-list"))
					registerBtn = driver.findElement(By.name("dwfrm_profile_confirm"));
				else
					registerBtn = driver.findElement(By.name("dwfrm_login_register"));
	
				BrowserActions.scrollToViewElement(registerBtn, driver);
				BrowserActions.clickOnElement(registerBtn, driver, "Register Button");
				Log.event("Clicked On Register Button");
			}catch(NoSuchElementException e){
				Log.exception(e, driver);
			}
			Utils.waitForPageLoad(driver);
			try{
				if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(".pt_account:not(.null)")))){
					Log.message("User Account Created for :: " + userID, driver);
					saveCredentialInDataSheet(driver, userID);
					return new MyAccountPage(driver).get();
				}else if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(".server-error")))){
					Log.message("User ID already taken.", driver);
					driver.get(signInUrl);
					SignIn signIn = new SignIn(driver).get();
					signIn.navigateToMyAccount(userID, password);
					saveCredentialInDataSheet(driver, userID);
					return new MyAccountPage(driver).get();
				}else{
					Log.message("Unhandled Error Occured.", driver);
					return null;
				}
			}catch(NoSuchElementException e){
				try{
					if(Utils.waitForElement(driver, driver.findElement(By.cssSelector(".server-error")))){
						Log.message("User ID already taken.", driver);
						driver.get(signInUrl);
						SignIn signIn = new SignIn(driver).get();
						signIn.navigateToMyAccount(userID, password);
						saveCredentialInDataSheet(driver, userID);
						return new MyAccountPage(driver).get();
					}else{
						Log.message("Unhandled Error Occured.", driver);
						return null;
					}
				}catch(NoSuchElementException e1){
					Log.message("User ID already taken.", driver);
					driver.get(signInUrl);
					SignIn signIn = new SignIn(driver).get();
					signIn.navigateToMyAccount(userID, password);
					saveCredentialInDataSheet(driver, userID);
					return new MyAccountPage(driver).get();
				}
			}
		}
	}
	
	/**
	 * To update password to previous password  
	 * @param WebDriver - driver
	 * @throws Exception - Exception
	 */
	public static void updatePassword(WebDriver driver, String newPassword, String credentials)throws Exception{
		logoutUser(driver);
		SignIn signin = new SignIn(driver).get();
		String email = credentials.split("\\|")[0];
		String password = credentials.split("\\|")[1];
		signin.navigateToMyAccount(email, password);

		try{
			Log.event("------------Updating Password-------------");
			driver.get(Utils.getWebSite() + redirectData.get("profile"));
			ProfilePage profile = new ProfilePage(driver).get();
			profile.typeCurrentPassword(password);
			profile.typeNewPassword(newPassword);
			profile.typeConfirmPassword(newPassword);
			profile.clickOnUpdatePassword();
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	}
	
	/**
	 * If does not exist with given optional credential, create new account with provided number of saved address and saved payment method
	 * @param driver
	 * @param number of addresses to be saved
	 * @param number of payment methods to be saved
	 * @param credential (optional)
	 * @throws Exception
	 */
	public static LinkedHashMap<String, Object> registerNewUser(WebDriver driver, int noOfAddr, int noOfCards, String... credentials)throws Exception{
		long startTime = StopWatch.startTime();
		LinkedHashMap<String, Object> objToReturn = new LinkedHashMap<String, Object>();
		try {
			HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
			MyAccountPage myAcc = null;
			String email = credentials.length > 0 ? credentials[0].split("\\|")[0] : AccountUtils.generateEmail(driver);
			String password = credentials.length > 0 ? credentials[0].split("\\|")[1] : accountData.get("password_global");
			objToReturn.put("email", email);
			objToReturn.put("password", password);
			myAcc = homePage.headers.navigateToMyAccountWithRememberChkBox(email, password, false, true);

			if(noOfAddr >= 1 && noOfAddr <= 4){
				AddressesPage addPage = myAcc.navigateToAddressPage();
				Log.event("Navigated To Address Page!");
				
				int i=1;
				if(addPage.getSavedAddressesCount() > noOfAddr) {
					addPage.deleteAllTheAddresses();
				}else {
					i = i + addPage.getSavedAddressesCount();
				}
				
				while((i <= 4) && (i++ <= noOfAddr)){
					addPage.clickOnAddNewAddress();
					addPage.fillingAddressDetails("address_" + i, false);
					addPage.clickSaveAddress();
					objToReturn.put("address_" + i, EnvironmentPropertiesReader.getInstance("checkout").getProperty("address_" + i));
					BrowserActions.refreshPage(driver);
				}
			} else if (noOfAddr == 0) {
				removeAllSavedAddress(driver);
			}

			if(noOfCards >=1 && noOfCards <= 4){
				PaymentMethodsPage paymentPage = myAcc.navigateToPaymentMethods();
				int x = paymentPage.getNumberOfSavedCards();
				if(x < noOfCards)
					for(int i = 1; i <= (noOfCards-x); i++){
						paymentPage.addNewCard("card_" + i);
						objToReturn.put("card_" + i, EnvironmentPropertiesReader.getInstance("checkout").getProperty("card_" + i));
					}
			} else if (noOfCards == 0) {
				removeAllPaymentMethods(driver);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			//throw e;
		}
		logoutUser(driver);
		Log.event("Elapsed time on registerNewUser:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
		return objToReturn;
	}
	
	/**
	 * If does not exist with given optional credentials, create new account with given address and payment method
	 * @param driver
	 * @param address to save
	 * @param payment method to save
	 * @param credential(optional)
	 * @throws Exception
	 */
	public static LinkedHashMap<String, Object> registerNewUserAddressPayment(WebDriver driver, String address, String payment, String... credentials)throws Exception{
		long startTime = StopWatch.startTime();
		try {
			LinkedHashMap<String, Object> objToReturn = new LinkedHashMap<String, Object>();
			HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
			MyAccountPage myAcc = null;
			String email = credentials[0].split("\\|")[0];
			String password = credentials[0].split("\\|")[1];
			if(credentials.length == 0){
				email = AccountUtils.generateEmail(driver);
				password = accountData.get("password_global");
			}
			objToReturn.put("email", email);
			objToReturn.put("password", password);
			myAcc = homePage.headers.navigateToMyAccountWithRememberChkBox(email, password, false, true);
			
			//Address
			AddressesPage addPage = myAcc.navigateToAddressPage();
			String[] addressProp = address.split("\\|");
			int noOfAddr = addressProp.length;
			addPage.deleteAllTheAddresses();
			for (int i=0 ; i < noOfAddr; i++) {
				addPage.clickOnAddNewAddress();
				addPage.fillingAddressDetails(addressProp[i], false);
				addPage.clickSaveAddress();
				BrowserActions.refreshPage(driver);
			}
			
			//Payment
			PaymentMethodsPage paymentPage = myAcc.navigateToPaymentMethods();
			String[] paymentProp = payment.split("\\|");
			int noOfPayment = paymentProp.length;
			paymentPage.deleteAllSavedCards();
			for (int i=0 ; i < noOfPayment; i++) {
				paymentPage.addNewCard(paymentProp[i]);
				BrowserActions.refreshPage(driver);
			}

			logoutUser(driver);
			
			Log.event("Elapsed time on registerNewUserAddressPayment:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
			return objToReturn;
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	}
	
	/**
	 * If does not exist with given optional credentials, create new account with given address and payment method
	 * @param driver
	 * @param address to save
	 * @param credential(optional)
	 * @throws Exception
	 */
	public static LinkedHashMap<String, Object> registerNewUserAddress(WebDriver driver, String address, String... credentials)throws Exception{
		long startTime = StopWatch.startTime();
		try {
			LinkedHashMap<String, Object> objToReturn = new LinkedHashMap<String, Object>();
			HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
			MyAccountPage myAcc = null;
			String email = credentials[0].split("\\|")[0];
			String password = credentials[0].split("\\|")[1];
			if(credentials.length == 0){
				email = AccountUtils.generateEmail(driver);
				password = accountData.get("password_global");
			}
			objToReturn.put("email", email);
			objToReturn.put("password", password);
			myAcc = homePage.headers.navigateToMyAccountWithRememberChkBox(email, password, false, true);
			
			//Address
			AddressesPage addPage = myAcc.navigateToAddressPage();
			String[] addressProp = address.split("\\|");
			int noOfAddr = addressProp.length;
			addPage.deleteAllTheAddresses();
			for (int i=0 ; i < noOfAddr; i++) {
				addPage.clickOnAddNewAddress();
				addPage.fillingAddressDetails(addressProp[i], false);
				addPage.clickSaveAddress();
				BrowserActions.refreshPage(driver);
			}
			
			removeAllPaymentMethods(driver);
			
			logoutUser(driver);
			
			Log.event("Elapsed time on registerNewUserAddress:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
			return objToReturn;
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	}
		
	public static LinkedHashMap<String, Object> registerNewUserWithUserDetail(WebDriver driver, int noOfAddr, int noOfCards,String firstName, String lastName, String... credentials)throws Exception{
		long startTime = StopWatch.startTime();
		try {
			LinkedHashMap<String, Object> objToReturn = new LinkedHashMap<String, Object>(); 
			HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
			MyAccountPage myAcc = null;
			if(credentials.length > 0) {
				myAcc = homePage.headers.navigateToMyAccountWithSpecifiedUser(credentials[0].split("\\|")[0], credentials[0].split("\\|")[1],firstName,lastName, false, true);
				objToReturn.put("email", credentials[0].split("\\|")[0]);
				objToReturn.put("password", credentials[0].split("\\|")[1]);
			} else {
				String autoGeneratedEmail = AccountUtils.generateEmail(driver);
				myAcc = homePage.headers.navigateToMyAccountWithSpecifiedUser(autoGeneratedEmail, accountData.get("password_global"),firstName,lastName, false, true);
				objToReturn.put("email", autoGeneratedEmail);
				objToReturn.put("password", accountData.get("password_global"));
			}

			if(noOfAddr >= 1 && noOfAddr <= 4){
				AddressesPage addPage = myAcc.navigateToAddressPage();
				Log.event("Navigated To Address Page!");
				
				int i=1;
				if(addPage.getSavedAddressesCount() > noOfAddr) {
					addPage.deleteAllTheAddresses();
				}else {
					i = i + addPage.getSavedAddressesCount();
				}
				
				while((i <= 4) && (i++ <= noOfAddr)){
					addPage.clickOnAddNewAddress();
					addPage.fillingAddressDetails("address_" + i, false);
					addPage.clickSaveAddress();
					objToReturn.put("address_" + i, EnvironmentPropertiesReader.getInstance("checkout").getProperty("address_" + i));
					Log.message("Default address created!", driver);
					BrowserActions.refreshPage(driver);
				}
			} else if (noOfAddr == 0) {
				removeAllSavedAddress(driver);
			}

			if(noOfCards >=1 && noOfCards <= 4){
				PaymentMethodsPage paymentPage = myAcc.navigateToPaymentMethods();
				int x = paymentPage.getNumberOfSavedCards();
				if(x < noOfCards)
					for(int i = 1; i <= (noOfCards-x); i++){
						paymentPage.addNewCard("card_" + i);
						objToReturn.put("card_" + i, EnvironmentPropertiesReader.getInstance("checkout").getProperty("card_" + i));
					}
			} else if (noOfCards == 0) {
				removeAllPaymentMethods(driver);
			}

			logoutUser(driver);
			
			Log.event("Elapsed time on registerNewUserWithUserDetail:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
			return objToReturn;
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	}
	
	
	public static void removeCard(WebDriver driver, String cardNumber) throws Exception{
		try{
			driver.get(Utils.getWebSite() + "/wallet");
			PaymentMethodsPage paymentPage = new PaymentMethodsPage(driver).get();
			Log.message("Navigated To Payments Page", driver);
			paymentPage.deleteCard(cardNumber);
			if(paymentPage.verifyCardPresent(cardNumber)){
				Log.fail("Cannot Delete Card.");
			}
			Log.message("Card("+ cardNumber +") deleted from payment methods");
		}catch(Exception e){
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
            Log.reference("Exception caught :: " + e.getLocalizedMessage(), driver);
            Log.event(e.getMessage());
			//throw e;
		}
	}

	public static void emptyCartForUser(String username, String password, WebDriver driver)throws Exception{
		long startTime = StopWatch.startTime();
		
		Headers header = new Headers(driver).get();
		header.navigateToHome();
		header.navigateToMyAccount(username, password);
		Log.event("Navigated to MyAccount Page.");

		RemoveAllProducts(driver);

		logoutUser(driver);
		Log.event("Logged out successfully.");
		Log.event("Elapsed time on emptyCartForUser:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
	}

	public static void loginAddNewAddress(WebDriver driver, int noOfAddr, String... credentails)throws Exception{
		HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
		Log.event("Navigated to Home Page.");

		MyAccountPage myAcc = homePage.headers.navigateToMyAccount(credentails[0].split("\\|")[0], credentails[0].split("\\|")[1]);
		Log.event("Navigated to My Account page.");

		AddressesPage addPage = myAcc.navigateToAddressPage();
		Log.event("Navigated To Address Page!");
		
		if(addPage.getSavedAddressesCount() >= noOfAddr) {
			for(int i = 1; i <= noOfAddr; i++){
				addPage.clickOnAddNewAddress();
				addPage.fillingAddressDetails("address_" + i, false);
				addPage.clickSaveAddress();
				BrowserActions.refreshPage(driver);
			}
			Log.event("Added Addresses.");
		} else {
			Log.event("Already account have saved address!");
		}

		logoutUser(driver);
		Log.event("Signed Out of website.");
	}

	public static String formatToBillingAddress(String address)throws Exception{
		String nickName = address.split("\\|")[0].trim();
		String firstName = address.split("\\|")[1].trim();
		String lastName = address.split("\\|")[2].trim();
		String addressLine1 = address.split("\\|")[3].trim();
		String addressLine2 = address.split("\\|")[4].trim();
		String city = address.split("\\|")[5].trim();
		String state = address.split("\\|")[6].trim();
		String zipcode = address.split("\\|")[7].trim();

		String billingAddress = nickName + firstName + lastName + addressLine1 + addressLine2 + city + StateUtils.getStateCode(state) + zipcode + "USA";
		return billingAddress;
	}

	public static HashMap<String, String> formatAddressToMap(String address)throws Exception{
		HashMap<String, String> addressAsMap = new HashMap<String, String>();
		addressAsMap.put("addressLine1", address.split("\\|")[0].trim());
		addressAsMap.put("addressLine2", address.split("\\|")[1].trim());
		addressAsMap.put("city", address.split("\\|")[2].trim());
		addressAsMap.put("state", address.split("\\|")[3].trim());
		addressAsMap.put("zipcode", address.split("\\|")[4].trim());
		addressAsMap.put("phone", address.split("\\|")[5].trim());
		addressAsMap.put("country", address.split("\\|")[6].trim());
		addressAsMap.put("firstName", address.split("\\|")[7].trim());
		addressAsMap.put("lastName", address.split("\\|")[8].trim());
		return addressAsMap;
	}
	
	public static HashMap<String, String> formatPLCCAddressToMapWithSSN(String address,String SSNdetails, WebDriver driver, String...email)throws Exception{
		HashMap<String, String> addressAsMap = new HashMap<String, String>();
		addressAsMap.put("addressLine1", address.split("\\|")[0].trim());
		addressAsMap.put("addressLine2", address.split("\\|")[1].trim());
		addressAsMap.put("city", address.split("\\|")[2].trim());
		addressAsMap.put("state", address.split("\\|")[3].trim());
		addressAsMap.put("zipcode", address.split("\\|")[4].trim());
		addressAsMap.put("phone", address.split("\\|")[5].trim());
		addressAsMap.put("country", address.split("\\|")[6].trim());
		addressAsMap.put("firstName", address.split("\\|")[7].trim());
		addressAsMap.put("lastName", address.split("\\|")[8].trim());
		if(email.length > 0) {
			addressAsMap.put("email", email[0]);
		} else {
			addressAsMap.put("email", AccountUtils.generateEmail(driver));
		}
		addressAsMap.put("SSN", SSNdetails.split("\\|")[0].trim());
		addressAsMap.put("month", SSNdetails.split("\\|")[1].trim());
		addressAsMap.put("day", SSNdetails.split("\\|")[2].trim());
		addressAsMap.put("year", SSNdetails.split("\\|")[3].trim());
		return addressAsMap;
	}
	
	public static void addNewAddressToAccount(WebDriver driver, String address, boolean deleteExistingAdd, String... emailPassword)throws Exception{
		long startTime = StopWatch.startTime();
		HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
		MyAccountPage myAcc =  null;
		String mail = "";
		String password = "";
		
		if(emailPassword.length > 0) {
			mail = emailPassword[0].split("\\|")[0];
			password = emailPassword[0].split("\\|")[1];
		} else {
			mail = AccountUtils.generateEmail(driver);
			password = accountData.get("password_global");
		}
		
		myAcc = homePage.headers.navigateToMyAccountWithRememberChkBox(mail, password, false, true);
		AddressesPage addPage = myAcc.navigateToAddressPage();
		String[] addressProp = address.split("\\|");
		int noOfAddr = addressProp.length;
		
		if (deleteExistingAdd) {
			addPage.deleteAllTheAddresses();
		}
		
		for (int i=0 ; i < noOfAddr; i++) {
			addPage.clickOnAddNewAddress();
			addPage.fillingAddressDetails(addressProp[i], false);
			addPage.clickSaveAddress();
			BrowserActions.refreshPage(driver);
		}
		logoutUser(driver);
		Log.event("Elapsed time on addNewAddressToAccount:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
	}
	
	public static void addNewPaymentToAccount(WebDriver driver, String paymentAd, boolean deleteExistingPay, String... emailPassword)throws Exception{
		long startTime = StopWatch.startTime();
		HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
		MyAccountPage myAcc =  null;
		String mail = "";
		String password = "";
		
		if(emailPassword.length > 0) {
			mail = emailPassword[0].split("\\|")[0];
			password = emailPassword[0].split("\\|")[1];
		} else {
			mail = AccountUtils.generateEmail(driver);
			password = accountData.get("password_global");
		}
		
		myAcc = homePage.headers.navigateToMyAccountWithRememberChkBox(mail, password, false, true);
		PaymentMethodsPage payment = myAcc.navigateToPaymentMethods();
		String[] paymentProp = paymentAd.split("\\|");
		int noOfPay = paymentProp.length;
		
		if (deleteExistingPay) {
			removeAllPaymentMethods(driver);
		}
		
		for (int i=0 ; i < noOfPay; i++) {
			payment.addNewCard(paymentProp[i]);
			BrowserActions.refreshPage(driver);
		}
		logoutUser(driver);
		Log.event("Elapsed timeon addNewPaymentToAccount:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
	}
	
	/**
	 * To delete saved default address
	 * @param driver 
	 */
	public static void deleteDefaultAddress(WebDriver driver)throws Exception{
		HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
		MyAccountPage myAcc =  null;
		String autoGeneratedEmail = AccountUtils.generateEmail(driver);
		myAcc = homePage.headers.navigateToMyAccountWithRememberChkBox(autoGeneratedEmail, accountData.get("password_global"), false, false);
		AddressesPage addPage = myAcc.navigateToAddressPage();
		addPage.deleteDefaultAddress();

		logoutUser(driver);
	}
	
	
	/**
	 * To delete all saved payment methods from account 
	 * @param WebDriver - driver
	 * @throws Exception - Exception
	 */
	public static void removeAllPaymentMethods(WebDriver driver)throws Exception{
		try {
			Log.event("-------------Removing Saved Payment Methods-------------");
			driver.get(Utils.getWebSite() + "/wallet");			
			PaymentMethodsPage paymentPage = new PaymentMethodsPage(driver).get();
			Log.event("Navigated To Payments Page");
			paymentPage.deleteAllSavedCards();
			Log.event("Removed all saved payment methods.");

		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
            Log.reference("Exception caught :: " + e.getLocalizedMessage(), driver);
            Log.event(e.getMessage());
            //throw e;
		}
	}

	/**
	 * To delete all saved addresses from account 
	 * @param WebDriver - driver
	 * @throws Exception - Exception
	 */
	public static void removeAllSavedAddress(WebDriver driver)throws Exception{
		try {
			Log.event("-------------Removing Saved Addresses-------------");
			driver.get(Utils.getWebSite() + redirectData.get("addressBook"));
			AddressesPage addrPage = new AddressesPage(driver).get();
			Log.event("Navigated to Addresses.");
			addrPage.deleteAllTheAddresses();
			Log.event("Removed all saved addresses.");
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
            Log.reference("Exception caught :: " + e.getLocalizedMessage(), driver);
            Log.event(e.getMessage());
            //throw e;
		}
	}

	/**
	 * To reset email subscription state for user
	 * @param WebDriver - driver
	 * @throws Exception - Exception
	 */
	public static void resetSubscriptionStatus(WebDriver driver)throws Exception{
		try {
			Log.event("-------------Reseting authenticated user email subscription status-------------");
			driver.get(Utils.getWebSite() + redirectData.get("emailPreference"));	
			EmailPreferencePage emailPrefs = new EmailPreferencePage(driver).get();
			Log.event("Navigated to Email preferences page.");
			emailPrefs.resetSubscriptionState();
			Log.event("Reset current brand email subscription.");
		}catch(Exception e) {
			SheetsQuickstart.LogException(Utils.getCurrentTestName(), e);
			throw e;
		}
	}

	/**
	 * To add ADS address to account
	 * @param driver
	 * @param address
	 * @param deleteExistingAdd
	 * @param emailPassword
	 */
	public static void addNewAddressToAccountADS(WebDriver driver, HashMap<String, String> address, boolean deleteExistingAdd, String... emailPassword) throws Exception {
		long startTime = StopWatch.startTime();
		HomePage homePage = new HomePage(driver, UrlUtils.getProtectedWebSiteWithYotta()).get();
		MyAccountPage myAcc =  null;
		String mail = "";
		String password = "";
		
		if(emailPassword.length > 0) {
			mail = emailPassword[0].split("\\|")[0];
			password = emailPassword[0].split("\\|")[1];
		} else {
			mail = AccountUtils.generateEmail(driver);
			password = accountData.get("password_global");
		}
		
		myAcc = homePage.headers.navigateToMyAccountWithRememberChkBox(mail, password, false, true);
		AddressesPage addPage = myAcc.navigateToAddressPage();
		
		if (deleteExistingAdd) {
			addPage.deleteAllTheAddresses();
		}
		
		addPage.clickOnAddNewAddress();
		addPage.fillingAddressDetailsADS(address, false);
		addPage.clickSaveAddress();
		BrowserActions.refreshPage(driver);
		Log.message("Creating Normal Address!", driver);
		logoutUser(driver);
		Log.event("Elapsed time on addNewAddressToAccountADS:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
	}
	
	/**
	 * To close monetate/SF popup
	 * @param driver - WebDriver instance
	 */
	public static void closePopup(WebDriver driver) throws Exception{
		long startTime = StopWatch.startTime();
		PopUp popUp = new PopUp(driver);
		try {
			popUp.closeLightboxPopup();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.event("Elapsed time on closePopup:: " + StopWatch.elapsedTimeFormatted(StopWatch.elapsedTime(startTime)));
	}
	
	/**
	 * To log out user
	 * @param driver - WebDrier instance
	 */
	public static void logoutUser(WebDriver driver) throws Exception{
		Headers header = new Headers(driver).get();
		header.navigateToHome();
		header.logoutUser();
	}
	
	/**
	 * To save credential in data sheet
	 * @param username - User Name to store
	 * @param driver - WebDrier instance
	 * @throws Exception - Exception
	 */
	public static void saveCredentialInDataSheet(WebDriver driver, String username, WebElement... myAcc) throws Exception {
		try {
			if((Boolean.getBoolean("emailIDCheck") || configProperty.get("emailIDCheck").equals("true")) && (username.contains("aut"))) {
				boolean status = true;
				if (myAcc.length > 0) {
					status = Utils.waitForElement(driver, myAcc[0]);
				}
				if (status && !TestData.verifyEmailIDExist(username, Utils.getCurrentBrand())) {
					SheetsQuickstart.updateEmailID(username);
					Brand brand = Utils.getCurrentBrand();
					String brandShort = brand.toString().replace("x", "");
					String brandDevice = "";
					if (Utils.isDesktop()) {
						brandDevice = brandShort+"_des";
					} else if (Utils.isMobile()) {
						brandDevice = brandShort+"_mob";
					} else {
						brandDevice = brandShort+"_tab";
					}
					List<String> UserList = new ArrayList<String>();
					UserList.add(username);
					TestData.emailData.put(brandDevice, UserList);
				}
			} else {
				Log.event("Email ID verification is disabled/Unique Email ID generated. Hence data not saved");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} // catch
	}
	
	/** To get inbox count from email address
	 * @param emailAddress - email address to get inbox count
	 * @param driver - WebDrier instance
	 * @return int - Inbox count
	 */
	public static int getInboxCountFromMailDrop(String emailAddress, WebDriver driver) throws Exception {
		MailDropPage mail = new MailDropPage(driver).get();
		Log.event("MailDrop page loaded successfully");
		
		mail.navigateToDropMailBox(emailAddress);
		Log.event("Navigated to usermail inbox " + emailAddress);
		
		return mail.mailAvailabilityCount();
    }

}// GlobalNavigation