package com.fbb.reusablecomponents;

/**
 * Declare framework enumerations here
 */
public class Enumerations {
	
	/**
	 * To write predefined test results
	 */
	public enum SheetTestResult {
		Passed,
		Failed,
		Exception;
	}
	
	/**
	 * To select from predefined Test Gift Card values
	 */
	public enum TestGiftCardValue {
		$10,
		$20,
		$50,
		$100,
		$500,
		$1000,
		valid;
	}
	
	/**
	 * To use with element size
	 */
	public enum ElementDimension {
		height,
		width;
	}
	
	/**
	 * To select from predefined directions
	 */
	public enum Direction {
		Up,
		Down,
		Right,
		Left;
	}
	
	/**
	 * To select from predefined shipping method names
	 */
	public enum ShippingMethod {
		Standard,
		Express,
		SuperFast,
		NextDay,
		HeathersFixed;
	}
	
	/**
	 * To select from predefined open/close states
	 */
	public enum Toggle {
		Open,
		Close;
	}
}
