package com.fbb.support;

import java.util.List;

import org.testng.IAlterSuiteListener;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

/**
 * {@link IAlterSuiteListener} requires that you use TestNG 6.9.10 or higher.
 */
public class ThreadCountChanger implements IAlterSuiteListener {
	@Override
	public void alter(List<XmlSuite> suites) {
		System.err.println("**Alter is invoked**");
		int count = -1;
		try {
			count = Integer.parseInt(System.getProperty("threadCount", "-1"));
		} catch (Exception e) {
			System.err.println("Invalid input for thread count found.");
		}
		for (XmlSuite suite : suites) {
			if (!(count <= 0)) {
				suite.setThreadCount(count);
				suite.setDataProviderThreadCount(count);
				List<XmlTest> tests = suite.getTests();
				for (XmlTest test : tests) {
					test.setThreadCount(count);
					System.out.println("Thread count for " + test.getName() + " has been set to :: " + count);
				}
			} else {
				System.out.println("Thread count not set from system.");
			}
		}
	}
}
