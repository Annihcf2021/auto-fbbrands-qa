package com.fbb.support;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.Reporter;

public class SystemProperties {
	
	public enum Platform {
		desktop, mobile, tablet
	};
	
	enum Orientation {
		portrait, landscape
	};
	
	public static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
	
	// Before Execution
	private static boolean commandLineExecution = false;
	private static boolean runAWSFromLocal = false;
	private static boolean runUserAgentDeviceTest = false;
	private static boolean realDeviceTest = false;
	
	private static String driverHost;
	private static String driverPort;
	private static URL hubURL;
	private static String screenResolution;
	
	// Execution
	private static boolean headless = false;
	private static boolean enableTestrailPriority = true;
	private static boolean enableTestRailAPI = false;
	private static boolean enableSkipping = false;
	private static boolean isTakeScreenShot = true;
	private static boolean documentLoad = true;
	private static boolean imageLoad = false;
	private static boolean framesLoad = true;
	private static boolean enabledAdobeURL = false;
	
	// Suit
	private static String suiteName = "";
	private static String currentEnv = null;
	private static Brand suiteBrand = null;
	private static String suiteURL = null;
	private static String browserName = null;
	private static String deviceName = "desktop";
	private static Platform runPlatform = Platform.desktop;

	// Test
	private static HashMap<String, Brand> executionBrand = new HashMap<String, Brand>();
	private static HashMap<String, String> websiteURL = new HashMap<String, String>();
	private static HashMap<String, String> runIDMapping = new HashMap<String, String>();
	
	// Script
	private static HashMap<String, Brand> ucNavigatedBrand = new HashMap<String, Brand>();
	private static HashMap<String, Platform> currentTestPlatform = new HashMap<String, Platform>();
	private static HashMap<String, String> currentTestDeviceName = new HashMap<String, String>();
	private static HashMap<String, Orientation> currentDeviceOrientation = new HashMap<String, Orientation>();
	
	// After Suite
	private static boolean updateGSheet = false;
	private static boolean enableReportPortal = false;
	private static boolean writeResult = false;
	private static boolean archiveTestResult = false;
	private static boolean ignoreSkippedCount = false;

	public static void setupProperties() {
		setRunAWSFromLocal();
		setSuiteName(Reporter.getCurrentTestResult().getTestContext().getSuite().getName());
		setDriverHostAndPort();
		setHubURL();
		
		//setUserAgentDeviceTest();
		setDeviceNameAndPlatform();		
		setEnvironment();
		setScreenResolution();
		setBrowserName();
		if (commandLineExecution) {
			setSuiteBrand();
			setSuiteURL();
		}
		
		enableReportPortal();
		setTestrailPriority();
		setEnableTestRailAPI();
		setEnableSkipping();
		setUpdateGSheet();
		
		setTakeScreenShot();
		setWriteResult();
		setArchiveTestResult();
		setIgnoreSkippedCount();
		setDocumentLoad();
		setImageLoad();
		setFramesLoad();
	}
	
	public static boolean isCommandLineExecution() {
		return commandLineExecution;
	}

	public static void setCommandLineExecution(boolean commandLineExecution) {
		System.out.println("Setting [commandLineExecution] to:: " + (SystemProperties.commandLineExecution || commandLineExecution));
		SystemProperties.commandLineExecution = SystemProperties.commandLineExecution || commandLineExecution;
	}

	public static Brand getSuiteBrand() {
		return suiteBrand;
	}

	public static void setSuiteBrand() {
		SystemProperties.suiteBrand = Brand.fromConfiguration(System.getProperty("brand"));
	}

	public static String getSuiteURL() {
		return suiteURL;
	}

	public static void setSuiteURL() {
		SystemProperties.suiteURL = UrlUtils.getWebSite(getEnvironment(), getSuiteBrand());
	}

	public static String getBrowserName() {
		return browserName;
	}

	public static void setBrowserName() {
		browserName = System.getProperty("browserName") != null ? System.getProperty("browserName")
				: Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("browserName").toLowerCase();
	}

	public static String getRunID() {
		return runIDMapping.get(Utils.getXMLTestName());
	}

	public static void setRunID(ITestContext context) {
		String runID = context.getCurrentXmlTest().getParameter("runID").trim();
		if (isEnableTestRailAPI() && runID.equals("")) {
			String platform = StringUtils.toCamelCase(Utils.getRunPlatForm());
			String brand = Utils.getCurrentBrandShort().substring(0, 2).toUpperCase();
			runID = SheetsQuickstart.getRunID(platform + "-" + brand);
		}
		
		System.out.println("Setting [runID] for " + Utils.getXMLTestName() + " to: " + runID);
		runIDMapping.put(Utils.getXMLTestName(), runID);
	}

	public static void setTestDefaults(ITestContext context) {
		setCurrentBrand(context);
		setWebsiteURL(context);
		setRunID(context);
	}
	
	private static String getTestScriptKey() {
		try {
			return (Utils.getXMLTestName() + Utils.getCurrentTestName());
		} catch (Exception e) {
			return null;
		}
	}
	
	private static boolean getBooleanFlag(String flagKey) {
		boolean flag = false;
		try {
			flag = configProperty.get(flagKey).equals("true");
		} catch (NullPointerException npe) {
			System.err.println("[" + flagKey + "] is not defined in Config properties.");
		} finally {
			if (System.getProperty(flagKey) != null) {
				flag = Boolean.getBoolean(flagKey);
				System.clearProperty(flagKey);
			}
		}
		System.out.println("Setting [" + flagKey + "] to: " + flag);
		return flag;
	}
	
	public static void enableReportPortal() {
		enableReportPortal = getBooleanFlag("enableReportPortal");
	}
	
	public static boolean getReportPortalEnabled() {
		return enableReportPortal;
	}
	
	public static String getSuiteName() {
		return suiteName;
	}

	public static void setSuiteName(String suiteName) {
		System.out.println("Saving [suiteName] as: " + suiteName);
		SystemProperties.suiteName = suiteName;
	}

	public static String getEnvironment() {
		return currentEnv;
	}

	public static void setEnvironment() {
		currentEnv = System.getProperty("env") != null ? System.getProperty("env") 
				: Reporter.getCurrentTestResult().getTestContext().getSuite().getParameter("env");
	}

	/**
	 * To save execution brand name to variable
	 */
	public static void setCurrentBrand(ITestContext context) {
		if (executionBrand.get(Utils.getXMLTestName()) == null) {
			String brand = System.getProperty("brand") != null ? 
					System.getProperty("brand") : context.getCurrentXmlTest().getParameter("brand");
					
			System.out.println("Setting [executionBrand] for " + Utils.getXMLTestName() + " to: " + brand);
			executionBrand.put(Utils.getXMLTestName(), Brand.fromConfiguration(brand));
		}
	}
	
	/**
	 * To get execution brand name
	 */
	public static Brand getCurrentBrand() {
		if (commandLineExecution) {
			return suiteBrand;
		}
		return executionBrand.get(Utils.getXMLTestName());
	}
	
	public static void setWebsiteURL(ITestContext context) {
		if (websiteURL.get(Utils.getXMLTestName()) == null) {
			String website = null;
			try {
				website = UrlUtils.getWebSite(getEnvironment(), getCurrentBrand());
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Defaulting to Roaman's");
				website = "https://adobe.roamans.com";
			}
			System.out.println("Setting [websiteURL] for " + Utils.getXMLTestName() + " to: " + website);
			websiteURL.put(Utils.getXMLTestName(), website);
		}
		
	}

	public static String getWebsiteURL() {
		if (commandLineExecution) {
			return suiteURL;
		}
		return websiteURL.get(Utils.getXMLTestName());
	}
	
	public static void setRunAWSFromLocal() {
		if (System.getProperty("deviceType") != null) {
			runAWSFromLocal = true;
			if (System.getProperty("runAWSFromLocal") != null) {
				runAWSFromLocal = Boolean.getBoolean("runAWSFromLocal");
				System.clearProperty("runAWSFromLocal");
			}
		} else {
			runAWSFromLocal = getBooleanFlag("runAWSFromLocal");
		}
	}
	
	public static boolean getRunAWSFromLocal() {
		return runAWSFromLocal;
	}
	
	public static void setDeviceNameAndPlatform() {
		if (System.getProperty("deviceType") != null) {
			setCommandLineExecution(true);
			if (System.getProperty("deviceType").equalsIgnoreCase("desktop")) {
				System.out.println("[enabled] " + runPlatform + " test for [" + deviceName + "]");
			} else {
				System.out.println("[enabled] User-Agent Device-Test" );
				runUserAgentDeviceTest = true;
				if (System.getProperty("deviceName") != null ) {
					deviceName = System.getProperty("deviceName");
					System.clearProperty("deviceName");
				} else {
					deviceName = configProperty.getProperty(System.getProperty("deviceType").toLowerCase() + "Default");
				}
				runPlatform = Platform.valueOf(System.getProperty("deviceType"));
				System.out.println("[enabled] " + runPlatform + " test for [" + deviceName + "]");
			}
			System.clearProperty("deviceType");
		} else {
			runUserAgentDeviceTest = getBooleanFlag("runUserAgentDeviceTest");
			if (runUserAgentDeviceTest) {
				System.out.println("[enabled] User-Agent Device-Test" );
				if (System.getProperty("deviceName") != null ) {
					deviceName = System.getProperty("deviceName");
					System.clearProperty("deviceName");
				} else {
					deviceName = configProperty.get("deviceName");
				}
				runPlatform = Platform.valueOf(deviceName.split("\\_")[2]);
				System.out.println("[enabled] " + runPlatform + " test for [" + deviceName + "]");
			} else {
				System.out.println("[enabled] " + runPlatform + " test for [" + deviceName + "]");
			}
		}
	}

	public static void setUserAgentDeviceTest(boolean runUserAgentDeviceTest) {
		SystemProperties.runUserAgentDeviceTest = runUserAgentDeviceTest;
		System.out.println("Setting [runUserAgentDeviceTest] to: " + SystemProperties.runUserAgentDeviceTest);
	}
	
	public static void setUserAgentDeviceTest() {
		runUserAgentDeviceTest = getBooleanFlag("runUserAgentDeviceTest");
	}
	
	public static boolean getUserAgentDeviceTest() {
		return runUserAgentDeviceTest;
	}
	
	public static boolean isRealDeviceTest() {
		return realDeviceTest;
	}

	public static void setRealDeviceTest() {
		realDeviceTest = getBooleanFlag("realDeviceTest");
	}
	
	public static void setDriverHostAndPort() {
		if (getRunAWSFromLocal()) {
			driverHost = configProperty.get("hostAWS") != null ? configProperty.get("hostAWS") : "ec2-52-10-108-138.us-west-2.compute.amazonaws.com";
			driverPort = configProperty.get("portAWS") != null ? configProperty.get("portAWS") : "4444";
		} else {
			ISuite testSuite = Reporter.getCurrentTestResult().getTestContext().getSuite();
			driverHost = System.getProperty("hubHost") != null ? System.getProperty("hubHost") : testSuite.getParameter("deviceHost");
			driverPort = System.getProperty("hubPort") != null ? System.getProperty("hubPort") : testSuite.getParameter("devicePort");
		}
	}
	
	public static String getDriverHost() {
		return driverHost;
	}
	
	public static String getDriverPort() {
		return driverPort;
	}
	
	public static void setHubURL() {
		try {
			hubURL = new URL("http://" + driverHost + ":" + driverPort + "/wd/hub");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		System.out.println("Setting [hubURL] to: " + hubURL);
	}
	
	public static URL getHubURL() {
		return hubURL;
	}
	
	public static boolean getAdobeURLEnabled() {
		return SystemProperties.enabledAdobeURL;
	}

	public static void setAdobeURLEnabled() {
		enabledAdobeURL = getBooleanFlag("enabledAdobeURL");
	}
	
	public static void setUCNavigatedBrand(Brand ucNavBrand) throws Exception {
		ucNavigatedBrand.put(getTestScriptKey(), ucNavBrand);
		System.out.println("Setting [ucNavBrand] for " + Utils.getCurrentTestName() + " to: " + websiteURL);
	}
	
	public static void setUCNavigatedBrand(String ucNavBrand) throws Exception {
		setUCNavigatedBrand(Brand.fromConfiguration(ucNavBrand));
	}
	
	public static Brand getUCNavigatedBrand() {
		if (ucNavigatedBrand.get(getTestScriptKey()) == null)
			return getCurrentBrand();
		else
			return ucNavigatedBrand.get(getTestScriptKey());
	}

	public static void setCurrentTestPlatform(Platform currentPlatform) throws Exception {
		currentTestPlatform.put(getTestScriptKey(), currentPlatform);
		System.out.println("Setting [currentPlatform] for " + Utils.getCurrentTestName() + " to: " + currentPlatform);
	}

	public static void setCurrentTestPlatform(String currentPlatform) throws Exception {
		setCurrentTestPlatform(Platform.valueOf(currentPlatform));
	}

	public static Platform getCurrentTestPlatform() {
		if (currentTestPlatform.get(getTestScriptKey()) == null)
			return getRunPlatform();
		else
			return currentTestPlatform.get(getTestScriptKey());
	}

	public static void setCurrentDeviceOrientation(Orientation currentOrientation) throws Exception {
		currentDeviceOrientation.put(getTestScriptKey(), currentOrientation);
		System.out.println("Setting [currentOrientation] for " + Utils.getCurrentTestName() + " to: " + currentOrientation);
	}

	public static void setCurrentDeviceOrientation(String currentOrientation) throws Exception {
		setCurrentDeviceOrientation(Orientation.valueOf(currentOrientation));
	}
	
	public static Orientation getCurrentDeviceOrientation() {
		if (currentDeviceOrientation.get(getTestScriptKey()) == null) {
			if (getCurrentTestPlatform().equals(Platform.desktop)) {
				return Orientation.landscape;
			} else {
				return Orientation.portrait;
			}
		} else
			return currentDeviceOrientation.get(getTestScriptKey());
	}
	
	public static void setDeviceName(String deviceName) {
		currentTestDeviceName.put(getTestScriptKey(), deviceName);
		System.out.println("Setting [currentTestDeviceName] for " + Utils.getCurrentTestName() + " to: " + deviceName);
	}
	
	public static String getDeviceName() {
		if (currentTestPlatform.get(getTestScriptKey()) == null)
			return deviceName;
		else
			return currentTestDeviceName.get(getTestScriptKey());
	}
	
	public static void setRunPlatform(Platform runPlatform) {
		SystemProperties.runPlatform = runPlatform;
	}
	
	public static void setRunPlatform(String runPlatform) {
		setRunPlatform(Platform.valueOf(runPlatform));
	}
	
	public static Platform getRunPlatform() {
		return runPlatform;
	}

	public static boolean isHeadless() {
		return headless;
	}

	public static void setHeadless() {
		headless = getBooleanFlag("headless");
	}

	public static boolean getTestrailPriority() {
		return enableTestrailPriority;
	}

	public static void setTestrailPriority() {
		enableTestrailPriority = getBooleanFlag("enableTestrailPriority");
	}

	public static boolean getEnableSkipping() {
		return enableSkipping;
	}

	public static void setEnableSkipping() {
		enableSkipping = getBooleanFlag("enableSkipping");
	}

	public static boolean getUpdateGSheet() {
		return updateGSheet;
	}

	public static void setUpdateGSheet() {
		updateGSheet = getBooleanFlag("updateGSheet");
	}

	public static boolean getTakeScreenShot() {
		return isTakeScreenShot;
	}

	public static void setTakeScreenShot() {
		isTakeScreenShot = getBooleanFlag("isTakeScreenShot");
	}

	public static boolean getWriteResult() {
		return writeResult;
	}

	public static void setWriteResult() {
		writeResult = getBooleanFlag("writeResult");
	}

	public static boolean getArchiveTestResult() {
		return archiveTestResult;
	}

	public static void setArchiveTestResult() {
		archiveTestResult = getBooleanFlag("archiveTestResult");
	}

	public static boolean getIgnoreSkippedCount() {
		return ignoreSkippedCount;
	}

	public static void setIgnoreSkippedCount() {
		ignoreSkippedCount = getBooleanFlag("ignoreSkippedCount");
		
	}

	public static boolean getDocumentLoad() {
		return documentLoad;
	}

	public static void setDocumentLoad() {
		documentLoad = getBooleanFlag("documentLoad");
	}

	public static boolean getImageLoad() {
		return imageLoad;
	}

	public static void setImageLoad() {
		imageLoad = getBooleanFlag("imageLoad");
	}

	public static boolean getFramesLoad() {
		return framesLoad;
	}

	public static void setFramesLoad() {
		framesLoad = getBooleanFlag("framesLoad");
	}

	public static String getScreenResolution() {
		return screenResolution;
	}

	public static void setScreenResolution() {
		screenResolution = System.getProperty("screenResolution") == null
				? (configProperty.hasProperty("screenResolution") ? configProperty.getProperty("screenResolution") : null) 
						: System.getProperty("screenResolution");
	}
	
	public static boolean isEnableTestRailAPI() {
		return enableTestRailAPI;
	}

	public static void setEnableTestRailAPI() {
		boolean testrailEnabled = getBooleanFlag("enableTestRailAPI");
		enableTestRailAPI = testrailEnabled;
	}
}
