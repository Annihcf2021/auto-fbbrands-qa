package com.fbb.support;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BrandUtils {
	private static EnvironmentPropertiesReader envProperty = EnvironmentPropertiesReader.getInstance("env");

	/**
	 * To get the brand full name
	 * @param (Optional) brandName - which Brand to get full name
	 * <br> Will return full name for current brand if no parameter given 
	 * @return string
	 * @throws Exception
	 */
	public static String getBrandFullName(Brand... brandName)throws Exception{
		Brand brand;
		if(brandName.length == 0) {
			brand = Utils.getCurrentBrand();
		} else {
			brand = brandName[0];
		}
		
		switch(brand){
			case jl:
			case jlx:
				return "JessicaLondon";
				
			case ww:
			case wwx:
				return "WomanWithin";
				
			case rm:
			case rmx:
				return "Roamans";
				
			case ks:
			case ksx:
				return "KingSize";
				
			case el:
			case elx:
				return "Ellos";
				
			case bh:
			case bhx:
				return "Brylane Home";
				
			case fb:
			case fbx:
				return "Fullbeauty";
				
			case sa:
			case sax:
				return "SwimsuitsForAll";
				
			default:
				return null;
		}	
	}

	/**
	 * To match Brand value regardless of Universal Cart x suffix
	 * @param brand1
	 * @param brand2
	 * @return try if brands actually match
	 */
	public static boolean matchBrand(Brand brand1, Brand brand2) {
		return brand1.toString().contains(brand2.toString()) || brand2.toString().contains(brand1.toString());
	}

	/**
	 * To compare two full brand names
	 * @param brandNameToVerify
	 * @param brandNameToVerifyAgainst
	 * @return if sanitized brand names match
	 * @throws Exception
	 */
	public static boolean compareBrandName(String brandNameToVerify, String brandNameToVerifyAgainst) throws Exception{
		brandNameToVerify = brandNameToVerify.replaceAll("'|\\s+", "").toLowerCase();
		brandNameToVerifyAgainst = brandNameToVerifyAgainst.replaceAll("'|\\s+", "").toLowerCase();
		
		return brandNameToVerify.equals(brandNameToVerifyAgainst);
	}

	/**
	 * To compare partial brand names
	 * @param brandName1
	 * @param brandName2
	 * @return if sanitized partial brand names match
	 * @throws Exception
	 */
	public static boolean comparePartialBrandName(String brandName1, String brandName2) throws Exception{
		brandName1 = brandName1.replaceAll("'|\\s+", "").toLowerCase();
		brandName2 = brandName2.replaceAll("'|\\s+", "").toLowerCase();
		
		return brandName1.contains(brandName2.substring(0,  4)) || brandName2.contains(brandName1.substring(0, 4));
	}

	/**
	 * To compare partial brand names
	 * @param strVerify
	 * @param brandName
	 * @return if sanitized partial brand names match
	 * @throws Exception
	 */
	public static boolean containBrandName(String strVerify, String brandName) throws Exception{
		strVerify = strVerify.replaceAll("'|\\s+", "").toLowerCase();
		brandName = brandName.replaceAll("'|\\s+", "").toLowerCase();
		
		return strVerify.contains(brandName);
	}

	/**
	 * To verify Current brand name
	 * @param brand - Brand name to verify current brand against
	 */
	public static boolean isBrand(Brand brand){
		return matchBrand(Utils.getCurrentBrand(), brand);
	}

	/**
	 * To get the brandName except the current brand
	 * @param brand -
	 * @return brandToReturn -
	 * @throws Exception -
	 */
	public static Brand[] returnBrandExcept(Brand brand)throws Exception{
		List<Brand> lstBrand = Arrays.asList(Brand.values()); 
		Brand[] brandToReturn = new Brand[lstBrand.size()];
		lstBrand.remove(brand);
		brandToReturn = lstBrand.toArray(brandToReturn);
		return brandToReturn;
	}

	/**
	 * Get Brands specified in env properties by key
	 * @param envKey
	 * @return List<Brand> of brands
	 * @throws Exception
	 */
	public static List<Brand> getEnvPropertyBrands(String envKey) throws Exception {
		String envPropertyString = envProperty.get(envKey).replaceAll("\\s+", "").toLowerCase();
		String[] brandsArray = envPropertyString.split(",");
		if (envPropertyString.contains(Utils.getCurrentBrandShort())
				&& !envPropertyString.startsWith(Utils.getCurrentBrandShort())) {
			String firstValue = brandsArray[0];
			String brandShort = Utils.getCurrentBrandShort().contains("x") ? Utils.getCurrentBrandShort() : Utils.getCurrentBrandShort() + "x";
			envPropertyString = envPropertyString.replace(brandShort, firstValue)
											.replaceFirst(firstValue, brandShort);
			brandsArray = envPropertyString.split(",");
		}
		List<String> envPropertyList = Arrays.asList(brandsArray);
		List<Brand> brands = new ArrayList<Brand>();
		for(String brandString : envPropertyList) {
			brands.add(Brand.valueOf(brandString));
		}
		return brands;
	}

}
