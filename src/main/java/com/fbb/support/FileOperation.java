package com.fbb.support;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class FileOperation {
	
	String filePath;

    FileOperation(String file) {
           filePath = file;
    }

    /**
    * Append the string to the file
    * 
     * @param filePath
    *            path of the file to which the message is to be appended
    * @param message
    *            message to be appended
    * @return
    */
    void appendFile(String message) {
           BufferedWriter out;
           try {
                  out = new BufferedWriter(new FileWriter(filePath, true));
                  out.write(message);
                  out.close();
           } catch (IOException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
           }

    }

    void appendFileWithNewLine(String message) {
           BufferedWriter out;
           try {
                  out = new BufferedWriter(new FileWriter(filePath, true));
                  out.write(message + System.getProperty("line.separator"));
                  out.close();
           } catch (IOException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
           }

    }

    static void ReplaceAllMatchString(String fileName, String matchString, String replaceString) throws IOException {
           File file = new File(fileName);
           BufferedReader reader;
           try {
                  reader = new BufferedReader(new FileReader(file));
                  String line = "", oldtext = "";
                  while ((line = reader.readLine()) != null) {
                        oldtext += line + "\r\n";
                  }
                  reader.close();

                  // To replace a line in a file
                  String newtext = oldtext.replaceAll(matchString, replaceString);

                  FileWriter writer = new FileWriter(file);
                  writer.write(newtext);
                  writer.close();
           } catch (FileNotFoundException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
           }

    }


}
