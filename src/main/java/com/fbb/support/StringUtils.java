package com.fbb.support;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.WordUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class StringUtils {

	/**
	 * To return the Title case format of given word or sentence
	 * @param data string
	 * @return title case string
	 */
	public static String toCamelCase(String data) {
		String dataToReturn = new String();
		try {
			dataToReturn = WordUtils.capitalize(data);
		} catch (Exception e) {
			return data;
		}
		return dataToReturn;
	}

	/**
	 * To extract number from a string
	 * @param String - String to extract number
	 * @return int - Number extracted from given string 
	 */
	public static int getNumberInString(String stringWithNumer) throws IllegalArgumentException{
		try {
			return Integer.parseInt(stringWithNumer.replaceAll("[^0-9|\\-]", ""));
		} catch(IllegalArgumentException e) {
			Log.event("String does not contain any number.");
			return 0;
		}
		
	}

	/**
	 * To extract long number from a string
	 * @param String - String to extract number
	 * @return Long - Number extracted from given string 
	 */
	public static Long getLongNumberInString(String stringWithNumer) throws IllegalArgumentException{
		try {
			return Long.parseLong(stringWithNumer.replaceAll("[^0-9|\\-]", ""));
		} catch(IllegalArgumentException e) {
			Log.event("String does not contain any number.");
			return 0L;
		}
	}

	/**
	 * To extract price from a string
	 * @param String - String to extract price
	 * @return Double - Price extracted from given string 
	 */
	public static double getPriceFromString(String stringWithPrice) {
		try {
			return Double.parseDouble(stringWithPrice.replaceAll("[^0-9|\\-|.]", ""));
		} catch(IllegalArgumentException e) {
			Log.event("String does not contain any number.");
			return 0.0;
		}
	}

	/**
	 * To extract highest price from a price range string
	 * @param String - String to extract price
	 * @return Double - Price extracted from given string 
	 */
	public static double getRangePriceCeiling(String stringWithPrice) {
		try {
			if(stringWithPrice.contains("-")) {
				stringWithPrice = stringWithPrice.split("-")[1].trim();
			}
			return Double.parseDouble(stringWithPrice.replaceAll("[^0-9|\\-|.]", ""));
		} catch(IllegalArgumentException e) {
			Log.event("String does not contain any number.");
			return 0.0;
		}
	}

	/**
	 * To split a given string into parts
	 * @param strInput - String to split
	 * @return Given String split into list of its parts
	 * @throws Exception
	 */
	public static List<String> splitStringToList(String strInput, String... splitter) throws Exception{
		List<String> strAsList = null;
		strInput = strInput.replaceAll("(\n(\\s+)?)|(\\s+)", " ");
		if(splitter.length > 0)
			strAsList = Arrays.asList(strInput.split(splitter[0]));
		else {
			if(strInput.contains("|")) {
				strAsList = Arrays.asList(strInput.split("\\|"));
			} else if(strInput.contains(",")) {
				strAsList = Arrays.asList(strInput.split(","));
			} else if(strInput.contains(" ")) {
				strAsList = Arrays.asList(strInput.split(" "));
			} else {
				strAsList = Arrays.asList(strInput);
			}
		}
		return strAsList;
	}

	/**
	 * To format a number to a given decimal point.
	 * @param doubleValue - Value to format
	 * @param decimalPoints - number of decimal points to keep
	 * @return formatted value as floating point number
	 */
	public static float formatNumber(double doubleValue, int decimalPoints) {
		int decimalFactor = (int) Math.pow(10, decimalPoints);
		return (float) ((int) (doubleValue * decimalFactor)) / decimalFactor;
	}

	/**
	 * To get the value of an input field.
	 * @param element - the input field you need the value/text of
	 * @param driver - WebDriver instance
	 * @return text of the input's value
	 */
	public static String getValueOfInputField(WebElement element, WebDriver driver) {
		String sDataToBeReturned = null;
		if (Utils.waitForElement(driver, element)) {
			sDataToBeReturned = element.getAttribute("value");
		}
		return sDataToBeReturned;
	}

	/**
	 * Is run device platform is Table?
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public static String getDateAndTime()throws Exception{
		String pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	
		String date = simpleDateFormat.format(new Date());
		return date;
	}

	/**
	 * To get specific detail of current date
	 * @param String - The part of date to get
	 * @return String - specific detail of current date
	 * @throws Exception - Exception
	 */
	public static int getCurrentDateDetails(String partOfDate) throws Exception{
		Map<String, String> today = Utils.getCurrentDateDetails();
		try {
			return Integer.parseInt(today.get(partOfDate));
		}catch(Exception e) {
			Log.message("Invalid argument. Verify part of date parameter.");
			return 0;
		}
	}

}
