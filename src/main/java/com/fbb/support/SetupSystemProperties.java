package com.fbb.support;

public class SetupSystemProperties {

	public static void setupProperties() {
		if(System.getProperty("updateGSheet") == null)
			System.setProperty("updateGSheet", BaseTest.configProperty.get("updateGSheet"));

		if(System.getProperty("enableTestrailPriority") == null) 
			System.setProperty("enableTestrailPriority", BaseTest.configProperty.get("enableTestrailPriority"));

		if(System.getProperty("enableSkipping") == null)
			System.setProperty("enableSkipping", BaseTest.configProperty.get("enableSkipping"));
		
		if(System.getProperty("emailIDCheck") == null)
			System.setProperty("emailIDCheck", BaseTest.configProperty.get("emailIDCheck"));
		
		//To enable/disable User Agent mode
		if (System.getProperty("deviceType") != null) {
			if (System.getProperty("deviceType").equalsIgnoreCase("desktop")) {
				System.out.println("[enabled]Desktop Test" );
			} else {
				System.out.println("[enabled]User-Agent Device-Test" );
				System.setProperty("runUserAgentDeviceTest", "true");
				System.setProperty("deviceName", BaseTest.configProperty.getProperty(System.getProperty("deviceType").toLowerCase() + "Default"));
			}

			if(System.getProperty("runAWSFromLocal") == null) {
				System.out.println("[enabled]AWS Grid" );
				System.setProperty("runAWSFromLocal", "true");
			}
		} else {
			//To set test execution grid environment
			if(System.getProperty("runAWSFromLocal") == null) 
				System.setProperty("runAWSFromLocal", BaseTest.configProperty.getProperty("runAWSFromLocal"));
			
			//To set user agent test
			if(System.getProperty("runUserAgentDeviceTest") == null)
				System.setProperty("runUserAgentDeviceTest", BaseTest.configProperty.getProperty("runUserAgentDeviceTest"));

			if (System.getProperty("runUserAgentDeviceTest").equals("true")) {
				if (!System.getProperties().toString().contains("deviceName")) {
					System.setProperty("deviceName", BaseTest.configProperty.get("deviceName"));
				}
			}
		}

		//To enable/disable TestRail
		SystemProperties.setEnableTestRailAPI();

		//To enable Jira Integration
		//if(System.getProperty("enableJiraAPI")==null) System.setProperty("enableJiraAPI", configProperty.getProperty("enableJiraAPI"));
	}
}