package com.fbb.support;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.SkipException;

import com.epam.reportportal.service.ReportPortal;
import com.fbb.externalAPI.JiraAPI;
import com.fbb.reusablecomponents.Enumerations.SheetTestResult;
import com.fbb.reusablecomponents.TestData;
import com.utils.testrail.TestRailAPICalls;

/**
 * Log class consists capturing and printing screenshots,methods for writing log
 * status as pass/fail logs,methods to print test case info and messages
 */
public class Log {

	public static boolean printconsoleoutput;
	private static String screenShotFolderPath;

	private static AtomicInteger screenShotIndex = new AtomicInteger(0);
	private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
	
	static final String TEST_TITLE_HTML_BEGIN = "&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#000080\">";
	static final String TEST_TITLE_HTML_END = "</font> </strong> </div>&emsp;<div><strong>Steps:</strong></div>";

	static final String TEST_COND_HTML_BEGIN = "&emsp;<div class=\"test-title\"> <strong><font size = \"3\" color = \"#0000FF\">";
	static final String TEST_COND_HTML_END = "</font> </strong> </div>&emsp;";

	static final String MESSAGE_HTML_BEGIN = "<div class=\"test-message\">&emsp;";
	static final String MESSAGE_HTML_END = "</div>";

	static final String PASS_HTML_BEGIN = "<div class=\"test-result\"><br><font color=\"green\"><strong> ";
	static final String PASS_HTML_END1 = " </strong></font> ";
	static final String PASS_HTML_END2 = "</div>&emsp;";

	static final String FAIL_HTML_BEGIN = "<div class=\"test-result\"><br><font color=\"red\"><strong> ";
	static final String FAIL_HTML_END1 = " </strong></font> ";
	static final String FAIL_HTML_END2 = "</div>&emsp;";

	static final String SKIP_EXCEPTION_HTML_BEGIN = "<div class=\"test-result\"><br><font color=\"orange\"><strong> ";
	static final String SKIP_HTML_END1 = " </strong></font> ";
	static final String SKIP_HTML_END2 = " </strong></font> ";

	static final String EVENT_HTML_BEGIN = "<div class=\"test-event\"> <font color=\"maroon\"> <small> &emsp; &emsp;--- ";
	static final String EVENT_HTML_END = "</small> </font> </div>";

	static final String TRACE_HTML_BEGIN = "<div class=\"test-event\"> <font color=\"brown\"> <small> &emsp; &emsp;--- ";
	static final String TRACE_HTML_END = "</small> </font> </div>";

	/**
	 * Static block clears the screenshot folder if any in the output during
	 * every suite execution and also sets up the print console flag for the run
	 */
	static {

		try {
			Properties props = new Properties();
			InputStream cpr = Log.class.getResourceAsStream("/log4j.properties");
			props.load(cpr);
			PropertyConfigurator.configure(props);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// PropertyConfigurator.configure("./src/main/resources/log4j.properties");
		File screenShotFolder = new File(Reporter.getCurrentTestResult().getTestContext().getOutputDirectory());
		screenShotFolderPath = screenShotFolder.getParent() + File.separator + "ScreenShot" + File.separator;
		screenShotFolder = new File(screenShotFolderPath);

		if (!screenShotFolder.exists()) {
			screenShotFolder.mkdir();
		}

		File[] screenShots = screenShotFolder.listFiles();

		// delete files if the folder has any
		if (screenShots != null && screenShots.length > 0) {
			for (File screenShot : screenShots) {
				File[] screenShotSFolder = screenShot.listFiles();
				for (File screenShott : screenShotSFolder) {
					screenShott.delete();
				}
				screenShot.delete();
			}
		}

		final Map<String, String> params = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getAllParameters();
		if (params.containsKey("printconsoleoutput")) {
			Log.printconsoleoutput = Boolean.parseBoolean(params.get("printconsoleoutput"));
		}
	} // static block

	/**
	 * takeScreenShot will take the screenshot by sending driver as parameter in
	 * the log and puts in the screenshot folder
	 * <br>
	 * depends on system variable isTakeScreenShot status, if true it will take
	 * the screenshot, else return the empty string
	 * @param driver - to take screenshot
	 * @return static
	 */
	public static String takeScreenShot(WebDriver driver) {
		String inputFile = "";
		try {
			inputFile = Reporter.getCurrentTestResult().getName() + "_" + screenShotIndex.incrementAndGet() + ".jpg";
			File screenShotFolder = new File(Reporter.getCurrentTestResult().getTestContext().getOutputDirectory());
			String screenShotFolderPath = screenShotFolder.getParent() + File.separator + "ScreenShot" + File.separator + Utils.getCurrentTestName() + File.separator;
			ScreenshotManager.takeScreenshot(driver, screenShotFolderPath + inputFile);	
		} catch (Exception e) {
			Log.event("Could not save screenshot. Exception type:: " + e.getClass().getName());
		}
		return inputFile;
	}

	/**
	 * getScreenShotHyperLink will convert the log status to hyper link depends on
	 * system variable isTakeScreenShot status,
	 * <br>
	 * If true it will take the screenshot,
	 * else return the empty string
	 * @param inputFile - converts log status to hyper link
	 * @return static values
	 */
	public static String getScreenShotHyperLink(String inputFile) {
		String screenShotLink = "";
		screenShotLink = "<a href=\"." + File.separator + "ScreenShot" + File.separator + Utils.getCurrentTestName() + File.separator + inputFile
				+ "\" target=\"_blank\" >[" + inputFile + "]</a>";
		return screenShotLink;
	}

	/**
	 * addTestRunMachineInfo will get the information of Hub/Node , browser
	 * details if executing through Grid
	 * @param driver - WebDriver instance
	 */
	public static void addTestRunMachineInfo(WebDriver driver) {
		Object params[] = Reporter.getCurrentTestResult().getParameters();
		String testMachine = "";
		String hub = "localhost";

		try {
			hub = (Reporter.getCurrentTestResult().getHost() == null) ? Inet4Address.getLocalHost().getHostName()
					: Reporter.getCurrentTestResult().getHost();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		try {
			testMachine = "(Browser: " + ((RemoteWebDriver) driver).getCapabilities().getBrowserName() + ", Hub: " + hub;
					//+ ", Node: " + WebDriverFactory.getTestSessionNodeIP(driver).toUpperCase() + ")";
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (params == null || params.length == 0) {
			params = new Object[1];
		}
		params[0] = testMachine + ", " + ((RemoteWebDriver) driver).getCapabilities().getBrowserName() + "_"
				+ ((RemoteWebDriver) driver).getCapabilities().getPlatform();
		Reporter.getCurrentTestResult().setParameters(params);
		ExtentReporter.addAttribute(params[0].toString());
	}

	/**
	 * lsLog4j returns name of the logger from the current thread
	 * @return static 
	 */
	public static Logger lsLog4j() {
		return Logger.getLogger(Thread.currentThread().getName());
	}

	/**
	 * callerClass method used to retrieve the Class Name
	 * @return static
	 */
	public static String callerClass() {
		return Thread.currentThread().getStackTrace()[2].getClassName();
	}
	
	/**
	 * testCaseInfo method print the description of the test case in the log
	 * (level=info)
	 * @param description - test case
	 */
	public static synchronized void testCaseInfo(String... description){
		boolean applicableForCurrentBrand = true;
		boolean toBeSkippedConfig = false;
		boolean toBeSkippedTestrail = false;
		String testCaseId;
		HashMap<String, String> caseInfo = new HashMap<String, String>();
		BuildProperties.setInitialLoadMainBrand(true);

		try {
			Throwable t = new Throwable();
			String testClass = t.getStackTrace()[1].getMethodName();
			if(testClass.contains("_C")) {
				testCaseId = t.getStackTrace()[1].getMethodName().split("\\_C")[1];
				String testrailURL = "https://fullbeautybrands.testrail.net/index.php?/cases/view/" + testCaseId;
				Log.reference("<a href= " + testrailURL + " >TestRail URL: C" + testCaseId + " </a>", false);
			} else {
				testCaseId = testClass;
				Log.reference(testClass, false);
			}
			
			new TestRailAPICalls();
			caseInfo = TestRailAPICalls.getCaseInfo(testCaseId);
			
			if(description.length == 0) {
				description = new String[1];
				description[0] = caseInfo.get("title");
			}
			
			SystemProperties.setCurrentTestPlatform(SystemProperties.getCurrentTestPlatform());

			if(SystemProperties.getEnableSkipping())
				toBeSkippedConfig = configProperty.get("skipTestCases").contains(testCaseId);
			if(SystemProperties.getTestrailPriority())
				toBeSkippedTestrail = StringUtils.getNumberInString(caseInfo.get("priority_id")) < StringUtils.getNumberInString(configProperty.get("minimumTestrailPriority"));
			applicableForCurrentBrand = caseInfo.get("custom_applicablefor" + Utils.getCurrentBrandShort().substring(0, 2)).equals("true");

			if(configProperty.get("dataSource").equals("TestRail")) {
				String testData = caseInfo.get("custom_testdata");
				Object obj = (JSONObject) new JSONParser().parse(testData);
				TestData.testData.put(Utils.getCurrentTestCode(), TestRailAPICalls.convertJSON(obj));
			}
		} catch(Exception e) {
			Log.event(e.getMessage());
		}

		if (description.length == 0) {
			description = new String[1];
			description[0] = "Test Case Title";
		}
		
		String callerMethodName = Utils.getCurrentTestName() + ":: ";
		lsLog4j().setLevel(Level.ALL);
		lsLog4j().info("");
		lsLog4j().log(callerClass(), Level.INFO, callerMethodName + "****             " + description[0] + "             *****", null);
		lsLog4j().info("");
		
		if(SystemProperties.getReportPortalEnabled()) {
			ReportPortal.emitLog(callerMethodName + "****             " + description[0] + "             *****", "info", Calendar.getInstance().getTime());
		}

		if (Reporter.getOutput(Reporter.getCurrentTestResult()).toString().contains("<div class=\"test-title\">")) {
			Reporter.log(TEST_TITLE_HTML_BEGIN + description[0] + TEST_TITLE_HTML_END + "&emsp;");
		} else {
			Reporter.log(TEST_TITLE_HTML_BEGIN + description[0] + TEST_TITLE_HTML_END + "<!-- Report -->&emsp;");
		}
		ExtentReporter.testCaseInfo(description[0]);

		if(!applicableForCurrentBrand) {
			if(SystemProperties.getReportPortalEnabled()) {
				ReportPortal.emitLog(callerMethodName + "****          Test case not applicable for current brand           *****", "warn", Calendar.getInstance().getTime());
			}
			throw new SkipException("Test case not applicable for current brand.");
		}
		if(toBeSkippedConfig) {
			if(SystemProperties.getReportPortalEnabled()) {
				ReportPortal.emitLog(callerMethodName + "****          Test case skipped by configuration           *****", "warn", Calendar.getInstance().getTime());
			}
			throw new SkipException("Test case skipped by configuration.");
		}		
		if(toBeSkippedTestrail) {
			if(SystemProperties.getReportPortalEnabled()) {
				ReportPortal.emitLog(callerMethodName + "****          Test case skipped by Testrail, priority low           *****", "warn", Calendar.getInstance().getTime());
			}
			throw new SkipException("Test case skipped by Testrail, priority low.");
		}
	}

	/**
	 * testCaseInfo method print the description of the test case in the log
	 * (level=info)
	 * @param testdata - HashMap of test case test data 
	 */
	public static void testCaseInfo(HashMap<String, String> testdata) {
		String description = new String();
		try {
			new TestRailAPICalls();
			description = TestRailAPICalls.getCaseInfo(testdata.get("TestID").split("\\_C")[1]).get("title");//testdata.containsKey("Description") ? testdata.get("Description").trim() : null;
		}catch(Exception e) {}
		String bugId = testdata.containsKey("BugId") ? testdata.get("BugId") : null;
		String comments = testdata.containsKey("Comments") ? testdata.get("Comments") : null;

		lsLog4j().setLevel(Level.ALL);
		lsLog4j().info("");
		lsLog4j().log(callerClass(), Level.INFO, "****             " + description + "             *****", null);
		lsLog4j().info("");

		if (Reporter.getOutput(Reporter.getCurrentTestResult()).toString().contains("<div class=\"test-title\">")) {
			if (bugId != null && comments != null)
				Reporter.log(TEST_TITLE_HTML_BEGIN + description + " BUGID:" + bugId + " COMMENTS:" + comments
						+ TEST_TITLE_HTML_END + "&emsp;");
			else if (bugId != null && comments == null)
				Reporter.log(TEST_TITLE_HTML_BEGIN + description + " BUGID:" + bugId + TEST_TITLE_HTML_END + "&emsp;");
			else if (bugId == null && comments != null)
				Reporter.log(
						TEST_TITLE_HTML_BEGIN + description + " COMMENTS:" + comments + TEST_TITLE_HTML_END + "&emsp;");
			else
				Reporter.log(TEST_TITLE_HTML_BEGIN + description + TEST_TITLE_HTML_END + "&emsp;");
		} else {
			if (bugId != null && comments != null)
				Reporter.log(TEST_TITLE_HTML_BEGIN + description + " BUGID:" + bugId + " COMMENTS:" + comments
						+ TEST_TITLE_HTML_END + "<!-- Report -->&emsp;");
			else if (bugId != null && comments == null)
				Reporter.log(TEST_TITLE_HTML_BEGIN + description + " BUGID:" + bugId + TEST_TITLE_HTML_END
						+ "<!-- Report -->&emsp;");
			else if (bugId == null && comments != null)
				Reporter.log(TEST_TITLE_HTML_BEGIN + description + " COMMENTS:" + comments + TEST_TITLE_HTML_END
						+ "<!-- Report -->&emsp;");
			else
				Reporter.log(TEST_TITLE_HTML_BEGIN + description + TEST_TITLE_HTML_END + "<!-- Report -->&emsp;");
		}
		ExtentReporter.testCaseInfo(description);
	}

	/**
	 * testConditionInfo method print the info of the test case in the log
	 * @param description - test case
	 */
	public static void testConditionInfo(String description) {
		Reporter.log(TEST_COND_HTML_BEGIN + description + TEST_COND_HTML_END);
	}

	/**
	 * Outputs Pass/Fail message in the test log as per the test step outcome
	 * @throws Exception
	 */
	public static void testCaseResult() throws Exception {
		Log.event("Completing Test Script.");
		boolean flag = Reporter.getOutput(Reporter.getCurrentTestResult()).toString().contains("FAIL");

		if (flag) {
			fail("Test Failed. Check the steps above in red color.");
		} else {
			pass("Test Passed.");
		}

	}

	/**
	 * endTestCase to print log in the console as a part of ending the test case
	 * @param driver
	 */
	public static void endTestCase(WebDriver... driver) {
		String callerMethodName = Utils.getCurrentTestName() + ":: ";
		lsLog4j().info(callerMethodName + "****             " + "-E---N---D-" + "             *****");
		if(SystemProperties.getReportPortalEnabled())
			ReportPortal.emitLog(callerMethodName + "****             " + "-E---N---D-" + "             *****","info",Calendar.getInstance().getTime());

		ExtentReporter.endTest();
		
		TestRailSupportUtils.updateScriptResultMap(TestRailSupportUtils.getTestCaseResults());
		if (SystemProperties.isEnableTestRailAPI()) {
			if (Utils.getDeclaredMethods().length == 1) {
				TestRailSupportUtils.submitResult();
			} else if (TestRailSupportUtils.getTestCaseMap().size() == Utils.getDeclaredMethods().length) {
				TestRailSupportUtils.submitResult(TestRailSupportUtils.getCompiledTestCaseResult(TestRailSupportUtils.getTestCaseMap()));
			}
		}

		if(SystemProperties.getUpdateGSheet()) {
			if(Reporter.getCurrentTestResult().getStatus() == ITestResult.SUCCESS)
				SheetsQuickstart.updateTCStatus(Utils.getCurrentTestName(), "Pass");
			else if(Reporter.getCurrentTestResult().getStatus() == ITestResult.FAILURE)
				SheetsQuickstart.updateTCStatus(Utils.getCurrentTestName(), "Fail");
		}
		
		BuildProperties.setInitialLoadMainBrand(true);
		if (driver.length > 0) {
			BuildProperties.setBuildInfo(driver[0]);
			
			event("Quitting driver...");
			driver[0].quit();
		}
	}
    
	/**
	 * message print the test case custom message in the log (level=info)
	 * @param description - test case
	 * @param testrailUpload - (Optional) Flag to set if description will be uploaded to Testrail.
	 * <br> Will not be uploaded if nothing is set.
	 */
	public static void message(String description, boolean... testrailUpload) {
		String callerMethodName = Utils.getCurrentTestName() + ":: ";
		boolean linkTestrail = testrailUpload.length > 0 ? testrailUpload[0] : false;
		
		Reporter.log(MESSAGE_HTML_BEGIN + description + MESSAGE_HTML_END);
		lsLog4j().log(callerClass(), Level.INFO, callerMethodName + description, null);
		if(SystemProperties.getReportPortalEnabled())
			ReportPortal.emitLog(description,"info",Calendar.getInstance().getTime());
		ExtentReporter.info(description);
		
		//TestRail Integration
		if (linkTestrail) {
			TestRailSupportUtils.mapTestComment(description);
		}
	}
	
	/**
	 * message print the test case custom message in the log (level=info)
	 * @param description - text describing the reference
	 * @param testrailUpload - (Optional) Flag to set if description will be uploaded to Testrail.
	 * <br> Will be uploaded if nothing is set.
	 */
	public static void reference(String description, boolean... testrailUpload) {
		String callerMethodName = Utils.getCurrentTestName() + ":: ";
		boolean linkTestrail = testrailUpload.length > 0 ? testrailUpload[0] : true;
		
		Reporter.log(MESSAGE_HTML_BEGIN +"<font color='blue'>"+ description +"</font>"+ MESSAGE_HTML_END);
		lsLog4j().log(callerClass(), Level.INFO, callerMethodName + description, null);
		if(SystemProperties.getReportPortalEnabled())
			ReportPortal.emitLog(description,"warn",Calendar.getInstance().getTime());
		ExtentReporter.info(description);

		//TestRail Integration
		if (linkTestrail) {
			TestRailSupportUtils.mapTestComment(description);
		}
	}
	
	/**
	 * message print the test case custom message in the log (level=info)
	 * @param description - warning text
	 * @param testrailUpload - (Optional) Flag to set if description will be uploaded to Testrail.
	 * <br> Will be uploaded if nothing is set.
	 */
	public static void warning(String description, boolean... testrailUpload) {
		String callerMethodName = Utils.getCurrentTestName() + ":: ";
		boolean linkTestrail = testrailUpload.length > 0 ? testrailUpload[0] : true;
		
		Reporter.log(MESSAGE_HTML_BEGIN +"<font color='orange'>"+ description +"</font>"+ MESSAGE_HTML_END);
		lsLog4j().log(callerClass(), Level.WARN, callerMethodName + description, null);
		ExtentReporter.info(description);

		//TestRail Integration
		if (linkTestrail) {
			TestRailSupportUtils.mapTestComment(description);
		}
	}
    
	/**
	 * message print the test case custom message in the log (level=info)
	 * @param description - test case
	 * @param driver - to take screenshot
	 * @param testrailUpload - (Optional) Flag to set if description will be uploaded to Testrail.
	 * <br> Will be uploaded if nothing is set.
	 */
	public static void reference(String description, WebDriver driver, boolean... testrailUpload) {
		String callerMethodName = Utils.getCurrentTestName() + ":: ";
		boolean linkTestrail = testrailUpload.length > 0 ? testrailUpload[0] : true;
		
		if (SystemProperties.getTakeScreenShot()) {
			String inputFile = takeScreenShot(driver);
			Reporter.log(MESSAGE_HTML_BEGIN +"<font color='blue'>"+  description + "&emsp;" + getScreenShotHyperLink(inputFile) + MESSAGE_HTML_END);
			ExtentReporter.info(description + " " + getScreenShotHyperLink(inputFile));

		} else {
			Reporter.log(MESSAGE_HTML_BEGIN +"<font color='blue'>"+  description + MESSAGE_HTML_END);
			ExtentReporter.info(description);
		}
		lsLog4j().log(callerClass(), Level.INFO, callerMethodName + description, null);
		Reporter.log(MESSAGE_HTML_BEGIN +"<font color='black'>");

		//TestRail Integration
		if (linkTestrail) {
			TestRailSupportUtils.mapTestComment(description);
		}
	}

	public static void messageT(String description) {
		Reporter.log(MESSAGE_HTML_BEGIN + description + MESSAGE_HTML_END);
		lsLog4j().log(callerClass(), Level.INFO, description, null);
		if(SystemProperties.getReportPortalEnabled())
			ReportPortal.emitLog(description,"info",Calendar.getInstance().getTime());
		
		ExtentReporter.info(description);
	}

	/**
	 * message print the test case custom message in the log with screenshot
	 * (level=info)
	 * @param description  - test case
	 * @param driver - to take screenshot
	 * @param testrailUpload - (Optional) Flag to set if description will be uploaded to Testrail.
	 * <br> Will not be uploaded if nothing is set.
	 */
	public static void message(String description, WebDriver driver, boolean... testrailUpload) {
		String callerMethodName = Utils.getCurrentTestName() + ":: ";
		boolean linkTestrail = testrailUpload.length > 0 ? testrailUpload[0] : false;
		
		if (SystemProperties.getTakeScreenShot()) {
			String inputFile = takeScreenShot(driver);
			Reporter.log(
					MESSAGE_HTML_BEGIN + description + "&emsp;" + getScreenShotHyperLink(inputFile) + MESSAGE_HTML_END);
			ExtentReporter.info(description + " " + getScreenShotHyperLink(inputFile));
			inputFile = new File(Reporter.getCurrentTestResult().getTestContext().getOutputDirectory()).getParent() + File.separator + "ScreenShot" + File.separator + Utils.getCurrentTestName() + File.separator + inputFile;
			if(SystemProperties.getReportPortalEnabled())
				ReportPortal.emitLog(description,"info",Calendar.getInstance().getTime(), new File(inputFile));
			 
		} else {
			Reporter.log(MESSAGE_HTML_BEGIN + description + MESSAGE_HTML_END);
			ExtentReporter.info(description);
			if(SystemProperties.getReportPortalEnabled())
				ReportPortal.emitLog(description,"info",Calendar.getInstance().getTime());
		}
		
		lsLog4j().log(callerClass(), Level.INFO, callerMethodName + description, null);
		
		//TestRail Integration
		if (linkTestrail) {
			TestRailSupportUtils.mapTestComment(description);
		}
	}

	/**
	 * message print test case description along with hyper link provided
	 * @param driver -
	 * @param description -
	 * @param screenshotfolderpath -
	 * @param screenshotfileName -
	 * @throws IOException -
	 */
	public static void message(
			WebDriver driver, String description, String screenshotfolderpath, String screenshotfileName) throws IOException {

		String inputFile = screenshotfileName + ".png";
		WebDriver augmented = new Augmenter().augment(driver);
		File screenshot = ((TakesScreenshot) augmented).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshot, new File(screenshotfolderpath + File.separator + inputFile));
		FileUtils.copyFile(screenshot, new File(screenShotFolderPath + File.separator + inputFile));
		Reporter.log(MESSAGE_HTML_BEGIN + String.format("%-75s", description).replace(" ", "&nbsp;") + "---&emsp;"
				+ getScreenShotHyperLink(inputFile) + MESSAGE_HTML_END);
		ExtentReporter.info(String.format("%-75s", description).replace(" ", "&nbsp;") + "---&emsp;"
				+ getScreenShotHyperLink(inputFile));
		lsLog4j().log(callerClass(), Level.INFO, description, null);
		if(SystemProperties.getReportPortalEnabled())
			ReportPortal.emitLog(description,"info",Calendar.getInstance().getTime());

	}

	/**
	 * buildLogMessage print test case description along with hyper link
	 * provided to test case status linked to respective screenshot
	 * @param driver -
	 * @param description -
	 * @param screenshotfolderpath -
	 * @param screenshotfileName -
	 * @throws IOException -
	 * @return static
	 */
	public static String buildLogMessage(
			WebDriver driver, String description, String screenshotfolderpath, String screenshotfileName) throws IOException {

		String inputFile = screenshotfileName + ".png";
		WebDriver augmented = new Augmenter().augment(driver);
		File screenshot = ((TakesScreenshot) augmented).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshot, new File(screenshotfolderpath + File.separator + inputFile));
		FileUtils.copyFile(screenshot, new File(screenShotFolderPath + inputFile));
		return MESSAGE_HTML_BEGIN + String.format("%-75s", description).replace(" ", "&nbsp;") + "---&emsp;"
		+ getScreenShotHyperLink(inputFile) + MESSAGE_HTML_END;
	}

	/**
	 * message print custom test case status message with screenshot
	 * (level=info)
	 * @param description - custom message in the test case
	 * @param screenshot - to take screenshot
	 * @throws IOException
	 */
	public static void message(String description, String screenshot) throws IOException {

		try {
			String inputFile = Reporter.getCurrentTestResult().getName() + "_" + screenShotIndex.incrementAndGet()
			+ ".png";
			FileUtils.copyFile(new File(screenshot), new File(screenShotFolderPath + inputFile));
			Reporter.log(MESSAGE_HTML_BEGIN + description + getScreenShotHyperLink(inputFile) + MESSAGE_HTML_END);
			ExtentReporter.info(description + " " + getScreenShotHyperLink(inputFile));
			lsLog4j().log(callerClass(), Level.INFO, description, null);
			if(SystemProperties.getReportPortalEnabled())
				ReportPortal.emitLog(description,"info",Calendar.getInstance().getTime());

		} catch (FileNotFoundException e) {
			Reporter.log(MESSAGE_HTML_BEGIN + description + MESSAGE_HTML_END);
			ExtentReporter.info(description);
		}
	}

	/**
	 * event print the page object custom message in the log which can be seen
	 * through short cut keys used during debugging (level=info)
	 * @param description - test case
	 */
	public static void event(String description) {
		String callerMethodName = Utils.getCurrentTestName() + ":: ";
		String currDate = new SimpleDateFormat("dd MMM HH:mm:ss SSS").format(Calendar.getInstance().getTime());
		Reporter.log(EVENT_HTML_BEGIN + currDate + " - " + description + EVENT_HTML_END);
		lsLog4j().log(callerClass(), Level.DEBUG, callerMethodName + description, null);
		if(SystemProperties.getReportPortalEnabled())
			ReportPortal.emitLog(description,"debug",Calendar.getInstance().getTime());
		ExtentReporter.debug(description);
	}

	/**
	 * event print the page object custom message in the log which can be seen
	 * through short cut keys used during debugging along with duration of the
	 * particular action (level=debug)
	 * @param description - test case
	 * @param duration - to print the time taken
	 */
	public static void event(String description, long duration) {
		String callerMethodName = Utils.getCurrentTestName() + ":: ";
		String currDate = new SimpleDateFormat("dd MMM HH:mm:ss SSS").format(Calendar.getInstance().getTime());
		Reporter.log(EVENT_HTML_BEGIN + currDate + " - <b>" + duration + "</b> - " + description + " - "
				+ Thread.currentThread().getStackTrace()[2].toString() + EVENT_HTML_END);
		lsLog4j().log(callerClass(), Level.DEBUG, callerMethodName + description, null);
		if(SystemProperties.getReportPortalEnabled())
			ReportPortal.emitLog(description,"debug",Calendar.getInstance().getTime());
		
		ExtentReporter.debug(currDate + " - <b>" + duration + "</b> - " + description + " - "
				+ Thread.currentThread().getStackTrace()[2].toString());
	}

	/**
	 * event print the page object custom message in the log which can be seen
	 * through short cut keys used during debugging (level=info)
	 * @param description - test case
	 */
	public static void trace(String description) {
		String currDate = new SimpleDateFormat("dd MMM HH:mm:ss SSS").format(Calendar.getInstance().getTime());
		Reporter.log(TRACE_HTML_BEGIN + currDate + " - " + description + TRACE_HTML_END);
		lsLog4j().log(callerClass(), Level.TRACE, description, null);
		if(SystemProperties.getReportPortalEnabled())
			ReportPortal.emitLog(description,"trace",Calendar.getInstance().getTime());
	}

	/**
	 * event print the page object custom message in the log which can be seen
	 * through short cut keys used during debugging along with duration of the
	 * particular action (level=debug)
	 * @param description - test case
	 * @param duration - to print the time taken
	 */
	public static void trace(String description, long duration) {
		String currDate = new SimpleDateFormat("dd MMM HH:mm:ss SSS").format(Calendar.getInstance().getTime());
		Reporter.log(TRACE_HTML_BEGIN + currDate + " - <b>" + duration + "</b> - " + description + " - "
				+ Thread.currentThread().getStackTrace()[2].toString() + TRACE_HTML_END);
		lsLog4j().log(callerClass(), Level.TRACE, description, null);
		if(SystemProperties.getReportPortalEnabled())
			ReportPortal.emitLog(description,"trace",Calendar.getInstance().getTime());
	}

	/**
	 * pass print test case status as Pass with custom message (level=info)
	 * @param description - custom message in the test case
	 */
	public static void pass(String description) {
		String callerMethodName = Utils.getCurrentTestName() + ":: ";
		Reporter.log(PASS_HTML_BEGIN + description + PASS_HTML_END1 + PASS_HTML_END2);
		lsLog4j().log(callerClass(), Level.INFO, callerMethodName + description, null);
		if(SystemProperties.getReportPortalEnabled())
			ReportPortal.emitLog(description,"info",Calendar.getInstance().getTime());
		ExtentReporter.pass(description);
		Reporter.getCurrentTestResult().setStatus(ITestResult.SUCCESS);
		
		//TestRail Integration
		TestRailSupportUtils.mapTestComment(description);
		TestRailSupportUtils.STATUS_MAPPING.put(Integer.toString(Utils.getCurrentTestCode()), 1);

		writeResult(SheetTestResult.Passed);
	}

	/**
	 * pass print test case status as Pass with custom message and take
	 * screenshot (level=info)
	 * @param description - custom message in the test case
	 * @param driver - to take screenshot
	 */
	public static void pass(String description, WebDriver driver) {
		Reporter.getCurrentTestResult().setStatus(ITestResult.SUCCESS);
		writeResult(SheetTestResult.Passed);

		if(SystemProperties.getTakeScreenShot()){
			String inputFile = takeScreenShot(driver);
			Reporter.log(PASS_HTML_BEGIN + description + PASS_HTML_END1 + getScreenShotHyperLink(inputFile)
			+ PASS_HTML_END2);
			ExtentReporter.pass(description + " " + getScreenShotHyperLink(inputFile));
			inputFile = new File(Reporter.getCurrentTestResult().getTestContext().getOutputDirectory()).getParent() + File.separator + "ScreenShot" + File.separator + Utils.getCurrentTestName() + File.separator + inputFile;
			if(SystemProperties.getReportPortalEnabled())
				ReportPortal.emitLog(description,"info",Calendar.getInstance().getTime(), new File(inputFile));
		} else {
			Reporter.log(PASS_HTML_BEGIN + description + PASS_HTML_END1 + PASS_HTML_END2);
			ExtentReporter.pass(description);
			if(SystemProperties.getReportPortalEnabled())
				ReportPortal.emitLog(description,"info",Calendar.getInstance().getTime());
		}
		lsLog4j().log(callerClass(), Level.INFO, description, null);

		//TestRail Integration
		TestRailSupportUtils.mapTestComment(description);
		TestRailSupportUtils.STATUS_MAPPING.put(Integer.toString(Utils.getCurrentTestCode()), 1);
	}

	/**
	 * fail print test case status as Fail with custom message and take
	 * screenshot (level=error)
	 * @param description - custom message in the test case
	 * @param driver - to take screenshot
	 */
	public static void fail(String description, WebDriver driver) {
		if(SystemProperties.getTakeScreenShot()){
			description = "***" + description + "***";
			String inputFile = takeScreenShot(driver);
			Reporter.log("<!--FAIL-->");
			Reporter.log(
					FAIL_HTML_BEGIN + description + FAIL_HTML_END1 + getScreenShotHyperLink(inputFile) + FAIL_HTML_END2);
			ExtentReporter.fail(description + " " + getScreenShotHyperLink(inputFile));
			ExtentReporter.logStackTrace(new AssertionError(description));
			inputFile = new File(Reporter.getCurrentTestResult().getTestContext().getOutputDirectory()).getParent() + File.separator + "ScreenShot" + File.separator + Utils.getCurrentTestName() + File.separator + inputFile;
			if(SystemProperties.getReportPortalEnabled())
				ReportPortal.emitLog(description,"error",Calendar.getInstance().getTime(), new File(inputFile));
			 
			//TestRail Integration
			TestRailSupportUtils.mapTestComment(description);
			TestRailSupportUtils.STATUS_MAPPING.put(Integer.toString(Utils.getCurrentTestCode()), 5);

			writeResult(SheetTestResult.Failed);
			Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);
			Assert.fail(description);
		}else
			fail(description);
	}

	/**
	 * fail print test case status as Fail with custom message (level=error)
	 * @param description - custom message in the test case
	 */
	public static void fail(String description) {
		String callerMethodName = Utils.getCurrentTestName() + ":: ";
		description = "***" + description + "***";
		lsLog4j().log(callerClass(), Level.ERROR, callerMethodName + description, null);
		if(SystemProperties.getReportPortalEnabled())
			ReportPortal.emitLog(description,"error",Calendar.getInstance().getTime());
		Reporter.log("<!--FAIL-->");
		Reporter.log(FAIL_HTML_BEGIN + description + FAIL_HTML_END1 + FAIL_HTML_END2);
		ExtentReporter.fail(description);
		ExtentReporter.logStackTrace(new AssertionError(description));
		Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);

		//TestRail Integration
		TestRailSupportUtils.mapTestComment(description);
		TestRailSupportUtils.STATUS_MAPPING.put(Integer.toString(Utils.getCurrentTestCode()), 5);
		
		writeResult(SheetTestResult.Failed);
		Assert.fail(description);
	}

	/**
	 * hasFailSofts returns true if the test steps contains any fail
	 * @return boolean
	 */
	public static boolean hasFailSofts() {
		return Reporter.getOutput(Reporter.getCurrentTestResult()).toString().contains("FAILSOFT");
	}

	/**
	 * failsoft print test case step failure as fail with screenshot and let
	 * execution continue (level=error)
	 * 
	 * @param description - custom message in the test case
	 * @param driver - to take screenshot
	 */
	public static void failsoft(String description, WebDriver driver) {
		if(SystemProperties.getTakeScreenShot()){
			String callerMethodName = Utils.getCurrentTestName() + ":: ";
			description = "***" + description + "***" + System.lineSeparator();
			String inputFile = takeScreenShot(driver);
			Reporter.log("<div class=\"test-result\"><font color=\"red\">&emsp;" + description + " Please refer this Screenshot--></font>"
					+ getScreenShotHyperLink(inputFile) + "</div>");
			Reporter.log("<!--FAILSOFT-->");
			ExtentReporter.fail(description + " " + getScreenShotHyperLink(inputFile));
			lsLog4j().log(callerClass(), Level.ERROR, callerMethodName + description, null);
			
			inputFile = new File(Reporter.getCurrentTestResult().getTestContext().getOutputDirectory()).getParent() + File.separator + "ScreenShot" + File.separator + Utils.getCurrentTestName() + File.separator + inputFile;
			if(SystemProperties.getReportPortalEnabled())
				ReportPortal.emitLog(description,"error",Calendar.getInstance().getTime(), new File(inputFile));
			
			//TestRail Integration
			TestRailSupportUtils.mapTestComment(description);
			TestRailSupportUtils.STATUS_MAPPING.put(Integer.toString(Utils.getCurrentTestCode()), 5);
		} else {
			failsoft(description);
		}
	}

	/**
	 * failsoft print test case step failure as fail and let execution continue
	 * (level=error)
	 * 
	 * @param description - custom message in the test case
	 */
	public static void failsoft(String description) {
		description = "***" + description + "***" + System.lineSeparator();
		String callerMethodName = Utils.getCurrentTestName() + ":: ";
		Reporter.log("<!--FAILSOFT-->");
		Reporter.log("<div class=\"test-result\"><font color=\"red\">&emsp;" + description + "</font></div>");
		ExtentReporter.fail(description);
		lsLog4j().log(callerClass(), Level.ERROR, callerMethodName + description, null);
		if(SystemProperties.getReportPortalEnabled())
			ReportPortal.emitLog(description,"error",Calendar.getInstance().getTime());
		
		//TestRail Integration
		TestRailSupportUtils.mapTestComment(description);
		TestRailSupportUtils.STATUS_MAPPING.put(Integer.toString(Utils.getCurrentTestCode()), 5);
	}
	
	/**
	 * To track automation Jira tickets
	 * <br> Call within condition block that checks for known issue with existing ticket.
	 * <br> References ticket by calling failsoft if closed issue is found during automation.
	 * References ticket as reference if issue is open in Jira.
 	 * @param ticketNumber - Ticket number stored in data sheet 
	 */
	public static void ticket(String ticketNumber, boolean ...testrailUpload) {
		HashMap<String, String> ticketData = null;
		boolean linkTestrail = testrailUpload.length > 0 ? testrailUpload[0] : true;
		
		try {
			ticketData = JiraAPI.getJiraTicketSummery(ticketNumber);
		} catch(Exception e) {
			Log.event("Error getting ticket infor from JIRA. Reading ticket status from Test Data sheet, status may be outdated.\n" + e.getLocalizedMessage());
			ticketData = SheetsQuickstart.getIssueData(ticketNumber);
		}
		
		if (ticketData != null) {
			String ticketMessage = ticketData.get("trackingNumber") + " :: " + ticketData.get("severity") + " || " + ticketData.get("description");
			if(ticketData.get("status").equalsIgnoreCase("closed") || ticketData.get("status").equalsIgnoreCase("done")) {
				failsoft(ticketMessage);
			} else {
				warning(ticketMessage);
			}
		}
		
		// TestRail Integration
		if (linkTestrail) {
			TestRailSupportUtils.mapTestDefect(ticketNumber);
		}
	}

	/**
	 * failsoft print test case step failure as fail in red color with
	 * screenshot (level=error)
	 * 
	 * @param description - custom message in the test case
	 * @param driver - to take screenshot
	 * @return static         
	 */
	public static String buildfailsoftMessage(String description, WebDriver driver) {
		String inputFile = takeScreenShot(driver);
		return "<div class=\"test-result\">&emsp; <font color=\"red\"><strong>" + description + " </strong> </font>"
		+ getScreenShotHyperLink(inputFile) + "</div>&emsp;";
	}
	
	/**
	 * To print test environment info
	 * @param driver - WebDriver instance
	 */
	public static void printEnviromentInfo(WebDriver driver) {
		Log.messageT("----------------------------------------------");
		Log.messageT("Platform        :: " + StringUtils.toCamelCase(Utils.getRunPlatForm()));
		Log.messageT("Browser Name    :: " + StringUtils.toCamelCase(Utils.getRunBrowser(driver)));
		Log.messageT("Browser Version :: " + Utils.getRunBrowserVersion(driver));
		Log.messageT("Testing Brand   :: " + Utils.getCurrentBrand().getConfiguration());
		Log.messageT("Testing Environ.:: " + Utils.getCurrentEnv());
		Log.messageT("----------------------------------------------");
	}

	/**
	 * exception prints the exception message as fail/skip in the log with
	 * screenshot (level=fatal)
	 * 
	 * @param e - exception message
	 * @param driver - to take screenshot
	 * @throws Exception
	 */
	public static void exception(Exception e, WebDriver driver) throws Exception {
		if(SystemProperties.getUpdateGSheet()) {
			SheetsQuickstart.updateTCStatus(Utils.getCurrentTestName(), e.getClass().getSimpleName());
		}
		writeResult(SheetTestResult.Exception);

		if(SystemProperties.getTakeScreenShot()){
			String screenShotLink = "";
			try {
				String inputFile = takeScreenShot(driver);
				screenShotLink = getScreenShotHyperLink(inputFile);
				inputFile = new File(Reporter.getCurrentTestResult().getTestContext().getOutputDirectory()).getParent() + File.separator + "ScreenShot" + File.separator + Utils.getCurrentTestName() + File.separator + inputFile;
				if(SystemProperties.getReportPortalEnabled())
					ReportPortal.emitLog(e.getMessage(),"error",Calendar.getInstance().getTime(), new File(inputFile)); 			
			} catch (Exception ex) {

			}
			String eMessage = e.getMessage();
			String exceptionName = e.getClass().getSimpleName();
			if (eMessage != null && eMessage.contains("\n")) {
				eMessage = eMessage.substring(0, eMessage.indexOf("\n"));
			}

			//TestRail Integration
			TestRailSupportUtils.mapTestComment("Exception Occured. Need to be Retested!" + System.lineSeparator() + exceptionName);
			TestRailSupportUtils.STATUS_MAPPING.put(Integer.toString(Utils.getCurrentTestCode()), 4);

			lsLog4j().log(callerClass(), Level.FATAL, eMessage, e);
			if (e instanceof SkipException) {
				Reporter.log(SKIP_EXCEPTION_HTML_BEGIN + eMessage + SKIP_HTML_END1 + screenShotLink + SKIP_HTML_END2);
				ExtentReporter.skip(eMessage + " " + screenShotLink);
				ExtentReporter.logStackTrace(e);
			} else {
				Reporter.log("<!--FAIL-->");
				Reporter.log(FAIL_HTML_BEGIN + eMessage + FAIL_HTML_END1 + screenShotLink + FAIL_HTML_END2);
				ExtentReporter.fail(eMessage + " " + screenShotLink);
				ExtentReporter.logStackTrace(e);
			}
			throw e;
		}else
			exception(e);
	}

	/**
	 * exception prints the exception message as fail/skip in the log
	 * (level=fatal)
	 * 
	 * @param e - exception message
	 * @throws Exception
	 */
	public static void exception(Exception e) throws Exception {
		if(SystemProperties.getUpdateGSheet()) {
			SheetsQuickstart.updateTCStatus(Utils.getCurrentTestName(), e.getClass().getSimpleName());
		}
		writeResult(SheetTestResult.Exception);

		if(SystemProperties.getReportPortalEnabled())
			ReportPortal.emitLog(e.getMessage(),"error",Calendar.getInstance().getTime());
		String eMessage = e.getMessage();

		if (eMessage != null && eMessage.contains("\n")) {
			eMessage = eMessage.substring(0, eMessage.indexOf("\n"));
		}

		//TestRail Integration
		TestRailSupportUtils.mapTestComment("Exception Occured. Need to be Retested!" + System.lineSeparator() + eMessage);
		TestRailSupportUtils.STATUS_MAPPING.put(Integer.toString(Utils.getCurrentTestCode()), 4);

		lsLog4j().log(callerClass(), Level.FATAL, eMessage, e);
		if (e instanceof SkipException) {
			Reporter.log(SKIP_EXCEPTION_HTML_BEGIN + eMessage + SKIP_HTML_END1 + SKIP_HTML_END2);
			ExtentReporter.skip(eMessage);
			ExtentReporter.logStackTrace(e);
		} else {
			Reporter.log("<!--FAIL-->");
			Reporter.log(FAIL_HTML_BEGIN + eMessage + FAIL_HTML_END1 + FAIL_HTML_END2);
			ExtentReporter.fail(eMessage);
			ExtentReporter.logStackTrace(e);
		}
		throw e;
	}

	/**
	 * Asserts that a condition is true or false, depends upon the status. Then
	 * it will print the verified message if status is true, else stop the
	 * script and print the failed message
	 * 
	 * @param status - boolean or expression returning boolean
	 * @param passmsg - message to be logged when assert status is true
	 * @param failmsg - message to be logged when assert status is false
	 * @throws Exception
	 */
	public static void assertThat(boolean status, String passmsg, String failmsg) throws Exception {
		if (!status) {
			Log.fail(failmsg);
		} else {
			Log.message(passmsg);
		}
	}

	/**
	 * Asserts that a condition is true or false, depends upon the status. Then
	 * it will print the verified message with screen shot if status is true,
	 * else stop the script and print the failed message with screen shot
	 * 
	 * @param status - boolean or expression returning boolean
	 * @param passmsg - message to be logged when assert status is true
	 * @param failmsg - message to be logged when assert status is false
	 * @param driver - WebDriver, using this driver will taking the screen shot and mapping to log report
	 */
	public static void assertThat(boolean status, String passmsg, String failmsg, WebDriver driver) {
		if (!status) {
			Log.fail(failmsg, driver);
		} else {
			Log.message(passmsg, driver);
		}
	}

	/**
	 * Asserts that a condition is true or false, depends upon the status. Then
	 * it will print the verified message with screen shot if status is true,
	 * else stop the script and print the failed message with screen shot
	 * 
	 * @param status - boolean or expression returning boolean
	 * @param expected - expected state
	 * @param passmsg - message to be logged when assert status is true
	 * @param failmsg - message to be logged when assert status is false
	 * @param driver - WebDriver, using this driver will taking the screen shot andmapping to log report
	 * @throws Exception
	 */
	public static void assertThat(boolean status, String expected, String passmsg, String failmsg, WebDriver driver)throws Exception {
		message("<br>");
		if (!status) {
			message("<b>*Expected Result: </b>" + expected + "*", true);
			Log.fail("<b>Actual Result(Failed) : </b>" + failmsg, driver);
		} else {
			message("<b>*Expected Result: </b>" + expected + "*");
			message("<b>*Actual Result : </b>" + passmsg + "*", driver);
		}
	}

	/**
	 * Asserts that a condition is true or false, depends upon the status. Then
	 * it will print the verified message if status is true, else print the
	 * failed message in red color and continue the next step(not
	 * stopping/breaking the test script)
	 * 
	 * @param status - boolean or expression returning boolean
	 * @param passmsg - message to be logged when assert status is true
	 * @param failmsg - message to be logged when assert status is false
	 */
	public static void softAssertThat(boolean status, String passmsg, String failmsg) {
		if (!status) {
			Log.failsoft(failmsg);
		} else {
			message("<b>*Actual Result : </b>" + passmsg + "*");
			//Log.message(passmsg);
		}
	}

	public static void softAssertThat(boolean status, String expected, String passmsg, String failmsg) {
		message("<br>");
		if (!status) {
			message("<b>*Expected Result: </b>" + expected + "*", true);
			failsoft("<b>Actual Result(Failed) : </b>" + failmsg);
		} else {
			message("<b>*Expected Result: </b>" + expected + "*");
			message("<b>*Actual Result : </b>" + passmsg + "*");
		}
	}

	
	/**
	 * Asserts that a condition is true or false, depends upon the status. Then
	 * it will print the verified message with screen shot if status is true,
	 * else print the failed message in red color with screen shot and continue
	 * the next step(not stopping/breaking the test script)
	 * 
	 * @param status - boolean or expression returning boolean
	 * @param passmsg - message to be logged when assert status is true
	 * @param failmsg - message to be logged when assert status is false
	 * @param driver - WebDriver, using this driver will taking the screen shot andmapping to log report
	 */
	public static void softAssertThat(boolean status, String passmsg, String failmsg, WebDriver driver) {
		if (!status) {
			Log.failsoft(failmsg, driver);
		} else {
			Log.message(passmsg, driver);
		}
	}
	
	/**
	 * Asserts that a condition is true or false, depends upon the status. Then it
	 * will print the verified message with screen shot if status is true, else
	 * print the failed message in red color with screen shot and continue the next
	 * step(not stopping/breaking the test script)
	 * 
	 * @param status   - boolean or expression returning boolean
	 * @param expected - expected behavior message
	 * @param passmsg  - message to be logged when assert status is true
	 * @param failmsg  - message to be logged when assert status is false
	 * @param driver   - WebDriver, using this driver will taking the screen shot and mapping to log report
	 */
	public static void softAssertThat(boolean status, String expected, String passmsg, String failmsg, WebDriver driver) {
		message("<br>");
		if (!status) {
			message("<b>*Expected Result: </b>" + expected + "*", true);
			failsoft("<b>Actual Result(Failed) : </b>" + failmsg, driver);
		} else {
			message("<b>*Expected Result: </b>" + expected + "*");
			message("<b>*Actual Result : </b>" + passmsg + "*", driver);
		}
	}

	/**
	 * Asserts that a condition is true or false, depends upon the status. Then it
	 * will print the verified message with screen shot if status is true, else
	 * print the failed message in red color with screen shot and continue the next
	 * step (not stopping/breaking the test script)
	 * 
	 * @param status   - boolean or expression returning boolean
	 * @param expected - expected behavior message
	 * @param driver   - WebDriver, using this driver will taking the screen shot and mapping to log report
	 */
	public static void softAssertThat(boolean status, String expected, WebDriver driver) {
		message("<br>");
		if (!status) {
			message("<b>Actual Result(Failed) : </b>" + expected, true);
			failsoft("<b>Actual Result(Failed) : </b>" + expected, driver);
		} else {
			message("<b>*Expected Result: </b>" + expected + "*");
			message("<b>*Actual Result : </b>" + expected + "*", driver);
		}
	}
	
	/**
	 * To validate utag for email campaign
	 * @param typeOfUTAG - utag name
	 * @param expectedUTAG - Expected value from data file
	 * @param actualUTAG - Actual value from page
	 */
	public static void softAssertUTAG(String typeOfUTAG, String expectedUTAG, String actualUTAG) {
		message("<br>");
		message("<b>*Checking UTAG: </b>" + typeOfUTAG + "*");
		if (actualUTAG == null) {
			failsoft("<b>*Result (Failed) : null or Undefined value:: </b>*" + expectedUTAG + " :: " + actualUTAG);
		} else {
			expectedUTAG = expectedUTAG.replace(" - ", " ");
			actualUTAG = actualUTAG.replace(" - ", " ");
			if(actualUTAG.equalsIgnoreCase(expectedUTAG) 
					|| actualUTAG.toLowerCase().contains(expectedUTAG.toLowerCase())
					|| actualUTAG.toLowerCase().contains(expectedUTAG.toLowerCase())) {
				message("<b>*Result (Passed): UTAG matches</b>*");
			} else {
				warning("<b>*Result (Mismatch): UTAG mismatches</b>* :: " + expectedUTAG + " :: " + actualUTAG);
			}
		}
	}

	/**
	 * Empty message for alignment
	 * @param description - Description to fill
	 */
	public static void message1(String description) {
		Reporter.log(description);
		lsLog4j().log(callerClass(), Level.INFO, description, null);
		ExtentReporter.info(description);
	}
	
	/**
	 * To Write Test Result to Excel Sheet Based on Configuration
	 * @throws Exception
	 */
	protected static void writeResult(SheetTestResult statusToWrite){
		try {
			if(SystemProperties.getWriteResult()) {
				HashMap<String, String> writeXL = new HashMap<String, String>();
				writeXL.put("Status", statusToWrite.toString());
				writeXL.put("LastRun", DateTimeUtility.getCurrentDateAndTime());
				writeXL.put("LastPass", DateTimeUtility.getCurrentDateAndTime());
				TestDataWritter.initWrite("testresult\\result.xls", "Sheet1", "TestID", Utils.getCurrentTestName(), writeXL);
			}
		} catch(Exception e) {
			Log.event("Writing test result to Excel sheet failed.");
			e.printStackTrace();
		}
	}
	
	/**
	 * To print content of a map to log
	 * @param mapToPrint
	 * @throws Exception
	 */
	public static <K, V> void printMapContent(Map<K, V> mapToPrint) throws Exception {
		try {
			for (Entry<K, V> entry : mapToPrint.entrySet()) {
			    Log.event(entry.getKey().toString() + " :: " + entry.getValue().toString());
			}
		} catch (ClassCastException cce) {
			Log.event(cce.getMessage());
		}
	}
	
	/**
	 * To print passed parameter as a HTML table row content
	 * @param rowContent - List of data ot be printed as cell content in row
	 * @param driver - (Optional) WebDriver instance to attach a screenshot with last cell
	 * @throws Exception
	 */
	public static void printTableRow(List<String> rowContent, WebDriver... driver) throws Exception {
		int rowSize = rowContent.size();
		String rowStart = "<td bgcolor='#efefef' style='color:black; min-width: 128px;' >", rowEnd = "</td>";

		//row till last cell
		Log.message1("<br><table id='printTableRow' style='border-collapse: collapse;'><tr>");
		for (int i = 0; i < (rowSize - 1); i++) {
			Log.message1(rowStart + rowContent.get(i) + rowEnd);
		}
		
		//last cell
		if (driver.length > 0) {
			String inputFile = takeScreenShot(driver[0]);
			Log.message1(rowStart + rowContent.get(rowSize-1) + "&emsp;" + getScreenShotHyperLink(inputFile) + rowEnd);
			inputFile = new File(Reporter.getCurrentTestResult().getTestContext().getOutputDirectory()).getParent() + File.separator + "ScreenShot" + File.separator + Utils.getCurrentTestName() + File.separator + inputFile;
		} else {
			Log.message1(rowStart + rowContent.get(rowSize-1) + rowEnd);
		}
		Log.message1("</tr></table>");
	}
}
