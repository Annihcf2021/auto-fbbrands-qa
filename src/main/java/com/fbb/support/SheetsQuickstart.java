package com.fbb.support;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.fbb.reusablecomponents.TestData;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetResponse;
import com.google.api.services.sheets.v4.model.ClearValuesRequest;
import com.google.api.services.sheets.v4.model.ClearValuesResponse;
import com.google.api.services.sheets.v4.model.DuplicateSheetRequest;
import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;

enum Columns{A, B, C, D, E, F, G, H, I, J, K, L};

public class SheetsQuickstart {

	private static final String APPLICATION_NAME = "GSheetAPITest";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final String CREDENTIALS_FOLDER = "credentials"; // Directory to store user credentials.

	private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance("config");
	final static String spreadsheetId = configProperty.get("dataSourceID");

	private static final List<String> SCOPES = Collections.singletonList(SheetsScopes.DRIVE);
	private static final String CLIENT_SECRET_DIR = "../../../client_secret.json";

	private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
		// Load client secrets.
		InputStream in = SheetsQuickstart.class.getResourceAsStream(CLIENT_SECRET_DIR);
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
				.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(CREDENTIALS_FOLDER)))
				.setAccessType("offline")
				.build();

		in.close();
		return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}

	synchronized public static void updateTCStatus(String tcID, String status) {
		try {
			Log.event("[GAPI]Initiating result upload to Google Sheets...");
			// Build a new authorized API client service.
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			final String spreadsheetId;
			if(Utils.getCurrentTestName().toLowerCase().contains("e2e")) {
				spreadsheetId = configProperty.get("e2e-sheetID");
			}else {
				spreadsheetId = configProperty.get("g-sheetID");
			}
			//String today = DateTimeUtility.getCurrentDateAs("dd-MM-yyyy");
			String today = BaseTest.startDate;
			final String range = "report_"+today+"!B1:B";
			String range1 = "report_"+today+"!";
			String unavailablerange = "report_"+today+"!";
			switch (Utils.getCurrentBrand()) {
			case rm:
				if (Utils.isDesktop()) {
					range1 += "C";
				} else if (Utils.isMobile()) {
					range1 += "D";
				} else {
					range1 += "E";
				}
				break;
			case ww:
				if (Utils.isDesktop()) {
					range1 += "F";
				} else if (Utils.isMobile()) {
					range1 += "G";
				} else {
					range1 += "H";
				}
				break;
			case ks:
				if (Utils.isDesktop()) {
					range1 += "I";
				} else if (Utils.isMobile()) {
					range1 += "J";
				} else {
					range1 += "K";
				}
				break;
			case el:
				if (Utils.isDesktop()) {
					range1 += "L";
				} else if (Utils.isMobile()) {
					range1 += "M";
				} else {
					range1 += "N";
				}
				break;
			case jl:
				if (Utils.isDesktop()) {
					range1 += "O";
				} else if (Utils.isMobile()) {
					range1 += "P";
				} else {
					range1 += "Q";
				}
				break;

			case bh:
				if (Utils.isDesktop()) {
					range1 += "R";
				} else if (Utils.isMobile()) {
					range1 += "S";
				} else {
					range1 += "T";
				}
				break;

			case sa:
				if (Utils.isDesktop()) {
					range1 += "U";
				} else if (Utils.isMobile()) {
					range1 += "V";
				} else {
					range1 += "W";
				}
				break;

			case fb:
				if (Utils.isDesktop()) {
					range1 += "X";
				} else if (Utils.isMobile()) {
					range1 += "Y";
				} else {
					range1 += "Z";
				}
				break;

			default:
				Log.event("[GAPI]Brand configuration Required.");
				break;
			}
			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();
			ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
			List<List<Object>> values = response.getValues();
			if (values == null || values.isEmpty()) {
				System.out.println("[GAPI]No data found.");
			} else {
				boolean flag = false;
				for (int i = 0; i < values.size(); i++) {
					List<Object> row = values.get(i);
					if (row.size() > 0 && row.get(0).equals(tcID)) {
						range1 += Integer.toString(i + 1);
						System.out.println("[GAPI]Row ID :: " + range1);
						flag = true;
						break;
					}
				}
				if (!flag) {
					Log.event("[GAPI]Current method name == " + tcID + " == not available in current sheet.");
					String rowNumber = Integer.toString(values.size() + 1);
					unavailablerange += "A" + rowNumber;
					String tcId = "TC" + new Throwable().getStackTrace()[2].getClassName().split("TC")[1];
					List<List<Object>> values1 = Arrays.asList(Arrays.asList(tcId));
					ValueRange body = new ValueRange().setValues(values1);
					UpdateValuesResponse result = service.spreadsheets().values().update(spreadsheetId, unavailablerange, body).setValueInputOption("RAW").execute();
					System.out.printf("[GAPI]%d cells appended.", result.getUpdatedCells());

					unavailablerange = "";
					unavailablerange += "B" + rowNumber;
					tcId = "TC" + new Throwable().getStackTrace()[2].getClassName().split("TC")[1];
					//values1.clear();
					values1 = Arrays.asList(Arrays.asList(tcID));
					body = new ValueRange().setValues(values1);
					result = service.spreadsheets().values().update(spreadsheetId, unavailablerange, body).setValueInputOption("RAW").execute();
					Log.event("[GAPI]New Row created. " + result.getUpdatedCells() + " cells appended.");

					range1 += rowNumber;
				}
			}

			//Writing value
			List<List<Object>> values1 = Arrays.asList(Arrays.asList(status));
			ValueRange body = new ValueRange().setValues(values1);
			UpdateValuesResponse result = service.spreadsheets().values().update(spreadsheetId, range1, body).setValueInputOption("RAW").execute();
			Log.event("[GAPI]Result Updated. " + (int) result.getUpdatedCells() + "cells appended.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void updateData(String spreadsheetId, String sheetName, String startingCellID, List<String> values){
		try {
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();

			String endingCellID = startingCellID.replaceAll("[^A-Za-z]", "") + (TestData.getAllProductData().size());
			//To clear values present in column
			ClearValuesRequest requestBody = new ClearValuesRequest();
			System.out.println("Clear request:: " + spreadsheetId + ", " + sheetName + "!" + startingCellID + ":" + endingCellID);
			Sheets.Spreadsheets.Values.Clear request = service.spreadsheets().values().clear(spreadsheetId, sheetName + "!" + startingCellID + ":" + endingCellID, requestBody);
			ClearValuesResponse response = request.execute();
			System.out.println("Clear response :: " + response);

			if (values != null && values.size() > 0) {
				List<List<Object>> values1 = new ArrayList<List<Object>>();
				for (int i = 0; i < values.size(); i++) {
					List<Object> val = new ArrayList<Object>();
					val.add(values.get(i));
					values1.add(val);
				}

				ValueRange body = new ValueRange().setValues(values1);
				UpdateValuesResponse result = service.spreadsheets().values().update(spreadsheetId, sheetName + "!" + startingCellID, body).setValueInputOption("RAW").execute();
				Log.event("[GAPI]Result Updated. " + (int) result.getUpdatedCells() +  "cells appended.");
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static boolean validateReportDate()throws Exception{
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		final String spreadsheetId;
		if(Utils.getCurrentTestName().toLowerCase().contains("e2e")) {
			spreadsheetId = configProperty.get("e2e-sheetID");
		}else {
			spreadsheetId = configProperty.get("g-sheetID");
		}
		final String range = "SheetsList!A1:A";
		String range1 = "SheetsList!B";
		Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();
		ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
		List<List<Object>> values = response.getValues();

		if (values == null || values.isEmpty()) {
			Log.event("[GAPI]No data found.");
			return false;
		} else {
			boolean flag = false;

			if(values.get(values.size()-1).get(0).toString().equals("current")){
				range1 += Integer.toString(values.size());
				flag=true;
			}else{
				for (int i = 0; i < values.size(); i++) {
					List<Object> row = values.get(i);
					if (row.size() > 0 && row.get(0).equals("current")) {
						range1 += Integer.toString(i + 1);
						Log.event("[GAPI]Row ID :: " + range1);
						flag = true;
						break;
					}
				}
			}

			if(flag){
				response = service.spreadsheets().values().get(spreadsheetId, range1).execute();
				values = response.getValues();
				String lastDated = values.get(0).get(0).toString().split("\\_")[1];
				String today = BaseTest.startDate;
				return (today.equals(lastDated)) ? true : false;
			}else{
				return false;
			}
		}
	}

	public static void duplicateSheetFromTemplate()throws Exception{
		System.out.println("[GAPI]Creating New Sheet for today report...");

		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		final String spreadsheetId;
		if(Utils.getCurrentTestName().toLowerCase().contains("e2e")) {
			spreadsheetId = configProperty.get("e2e-sheetID");
		}else {
			spreadsheetId = configProperty.get("g-sheetID");
		}
		Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();

		String today = BaseTest.startDate;//DateTimeUtility.getCurrentDateAs("dd-MM-yyyy");
		BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();

		List<Request> requests = new ArrayList<>();

		DuplicateSheetRequest requestBody = new DuplicateSheetRequest();
		String reportName = "report_" + today;
		requestBody.setNewSheetName(reportName);
		if(Utils.getCurrentTestName().toLowerCase().contains("e2e"))
			requestBody.setSourceSheetId(Integer.parseInt(configProperty.get("e2e-sheetTemplate")));
		else
			requestBody.setSourceSheetId(Integer.parseInt(configProperty.get("g-sheetTemplate")));

		requests.add(new Request().setDuplicateSheet(requestBody));


		batchUpdateSpreadsheetRequest.setRequests(requests);
		Sheets.Spreadsheets.BatchUpdate request =
				service.spreadsheets().batchUpdate(spreadsheetId, batchUpdateSpreadsheetRequest);

		BatchUpdateSpreadsheetResponse updateResponse = request.execute();
		Log.event("New Sheet Properties :: " + updateResponse);

		final String range = "SheetsList!A1:B";
		String range1 = "SheetsList!A";
		String range2 = "SheetsList!";

		int rowID = 0;

		ValueRange response = service.spreadsheets().values().get(spreadsheetId, range).execute();
		List<List<Object>> values = response.getValues();

		for (int i = 0; i < values.size(); i++) {
			List<Object> row = values.get(i);
			if (row.size() > 0 && row.get(0).equals("current")) {
				range1 += Integer.toString(i + 1);
				rowID = i;
				range2 += "A" + Integer.toString(i + 2) + ":B" + Integer.toString(i + 2);
				break;
			}
		}

		String val = values.get(rowID).get(1).toString().split("_")[1];
		List<List<Object>> values1 = Arrays.asList(Arrays.asList(val));
		ValueRange body = new ValueRange().setValues(values1);
		UpdateValuesResponse result = service.spreadsheets().values().update(spreadsheetId, range1, body).setValueInputOption("RAW").execute();
		Log.event("[GAPI]Result Updated. " + result.getUpdatedCells() + " cells appended.");

		values1 = Arrays.asList(Arrays.asList("current",reportName));
		body = new ValueRange().setValues(values1);
		result = service.spreadsheets().values().update(spreadsheetId, range2, body).setValueInputOption("RAW").execute();
		Log.event("[GAPI]Result Updated. " + result.getUpdatedCells() + " cells appended.");
	}

	public static void LogException(String tc_name, Exception e)throws Exception{
		try {
			if(SystemProperties.getUpdateGSheet()) {
				SheetsQuickstart.updateTCStatus(tc_name, e.getClass().getSimpleName());
			}
		}catch(Exception e1) {
			e.printStackTrace();
		}
	}

	public static void readData(){
		try {
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			final String spreadsheetId = configProperty.get("dataSourceID");
			String product = "product_" + Utils.getCurrentEnv() + "!A2:J";
			String gfitcard = "giftcard!A2:J";
			String coupon = "coupons!A2:J";
			String reward = "rewards!A2:J";
			String colorcode = "colorcodes!A2:J";
			String others = "brand_" + Utils.getCurrentEnv() + "!A2:J";

			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();


			ValueRange response = service.spreadsheets().values().get(spreadsheetId, product).execute();
			List<List<Object>> values = response.getValues();

			for (int i = 0; i < values.size(); i++) {
				List<Object> row = values.get(i);
				if(!(row.isEmpty() || row.get(0).toString().isEmpty() || row.get(0).toString().trim().equals("") || row.get(0).toString().equals(null))) { 
					TestData.data_rm.put(row.get(0).toString().trim(), row.get(1).toString().trim());
					TestData.data_ww.put(row.get(0).toString().trim(), row.get(2).toString().trim());
					TestData.data_ks.put(row.get(0).toString().trim(), row.get(3).toString().trim());
					TestData.data_el.put(row.get(0).toString().trim(), row.get(4).toString().trim());
					TestData.data_jl.put(row.get(0).toString().trim(), row.get(5).toString().trim());
					TestData.data_bh.put(row.get(0).toString().trim(), row.get(6).toString().trim());
					TestData.data_sa.put(row.get(0).toString().trim(), row.get(7).toString().trim());
					TestData.data_fb.put(row.get(0).toString().trim(), row.get(8).toString().trim());
				}
			}

			response.clear();
			values.clear();
			response = service.spreadsheets().values().get(spreadsheetId, gfitcard).execute();
			values = response.getValues();

			for (int i = 0; i < values.size(); i++) {
				List<Object> row = values.get(i);
				if(!(row.isEmpty() || row.get(0).toString().isEmpty() || row.get(0).toString().trim().equals("") || row.get(0).toString().equals(null))) { 
					TestData.giftcard_rm.put(row.get(0).toString().trim(), row.get(1).toString().trim());
					TestData.giftcard_ww.put(row.get(0).toString().trim(), row.get(2).toString().trim());
					TestData.giftcard_ks.put(row.get(0).toString().trim(), row.get(3).toString().trim());
					TestData.giftcard_el.put(row.get(0).toString().trim(), row.get(4).toString().trim());
					TestData.giftcard_jl.put(row.get(0).toString().trim(), row.get(5).toString().trim());
					TestData.giftcard_bh.put(row.get(0).toString().trim(), row.get(6).toString().trim());
					TestData.giftcard_sa.put(row.get(0).toString().trim(), row.get(7).toString().trim());
					TestData.giftcard_fb.put(row.get(0).toString().trim(), row.get(8).toString().trim());
				}
			}

			response.clear();
			values.clear();
			response = service.spreadsheets().values().get(spreadsheetId, coupon).execute();
			values = response.getValues();

			for (int i = 0; i < values.size(); i++) {
				List<Object> row = values.get(i);
				if(!(row.isEmpty() || row.get(0).toString().isEmpty() || row.get(0).toString().trim().equals("") || row.get(0).toString().equals(null))) { 
					TestData.coupons_rm.put(row.get(0).toString().trim(), row.get(1).toString().trim());
					TestData.coupons_ww.put(row.get(0).toString().trim(), row.get(2).toString().trim());
					TestData.coupons_ks.put(row.get(0).toString().trim(), row.get(3).toString().trim());
					TestData.coupons_el.put(row.get(0).toString().trim(), row.get(4).toString().trim());
					TestData.coupons_jl.put(row.get(0).toString().trim(), row.get(5).toString().trim());
					TestData.coupons_bh.put(row.get(0).toString().trim(), row.get(6).toString().trim());
					TestData.coupons_sa.put(row.get(0).toString().trim(), row.get(7).toString().trim());
					TestData.coupons_fb.put(row.get(0).toString().trim(), row.get(8).toString().trim());
				}
			}

			response.clear();
			values.clear();
			response = service.spreadsheets().values().get(spreadsheetId, reward).execute();
			values = response.getValues();

			for (int i = 0; i < values.size(); i++) {
				List<Object> row = values.get(i);
				if(!(row.isEmpty() || row.get(0).toString().isEmpty() || row.get(0).toString().trim().equals("") || row.get(0).toString().equals(null))) { 
					TestData.reward_rm.put(row.get(0).toString().trim(), row.get(1).toString().trim());
					TestData.reward_ww.put(row.get(0).toString().trim(), row.get(2).toString().trim());
					TestData.reward_ks.put(row.get(0).toString().trim(), row.get(3).toString().trim());
					TestData.reward_el.put(row.get(0).toString().trim(), row.get(4).toString().trim());
					TestData.reward_jl.put(row.get(0).toString().trim(), row.get(5).toString().trim());
					TestData.reward_bh.put(row.get(0).toString().trim(), row.get(6).toString().trim());
					TestData.reward_sa.put(row.get(0).toString().trim(), row.get(7).toString().trim());
					TestData.reward_fb.put(row.get(0).toString().trim(), row.get(8).toString().trim());
				}
			}

			response.clear();
			values.clear();
			response = service.spreadsheets().values().get(spreadsheetId, colorcode).execute();
			values = response.getValues();

			for (int i = 0; i < values.size(); i++) {
				List<Object> row = values.get(i);
				if(!(row.isEmpty() || row.get(0).toString().isEmpty() || row.get(0).toString().trim().equals("") || row.get(0).toString().equals(null))) { 
					TestData.colorcode_rm.put(row.get(0).toString().trim(), row.get(1).toString().trim());
					TestData.colorcode_ww.put(row.get(0).toString().trim(), row.get(2).toString().trim());
					TestData.colorcode_ks.put(row.get(0).toString().trim(), row.get(3).toString().trim());
					TestData.colorcode_el.put(row.get(0).toString().trim(), row.get(4).toString().trim());
					TestData.colorcode_jl.put(row.get(0).toString().trim(), row.get(5).toString().trim());
					TestData.colorcode_bh.put(row.get(0).toString().trim(), row.get(6).toString().trim());
					TestData.colorcode_sa.put(row.get(0).toString().trim(), row.get(7).toString().trim());
					TestData.colorcode_fb.put(row.get(0).toString().trim(), row.get(8).toString().trim());
				}
			}

			response.clear();
			values.clear();
			response = service.spreadsheets().values().get(spreadsheetId, others).execute();
			values = response.getValues();

			for (int i = 0; i < values.size(); i++) {
				List<Object> row = values.get(i);
				if(!(row.isEmpty() || row.get(0).toString().isEmpty() || row.get(0).toString().trim().equals("") || row.get(0).toString().equals(null))) { 
					TestData.others_rm.put(row.get(0).toString().trim(), row.get(1).toString().trim());
					TestData.others_ww.put(row.get(0).toString().trim(), row.get(2).toString().trim());
					TestData.others_ks.put(row.get(0).toString().trim(), row.get(3).toString().trim());
					TestData.others_el.put(row.get(0).toString().trim(), row.get(4).toString().trim());
					TestData.others_jl.put(row.get(0).toString().trim(), row.get(5).toString().trim());
					TestData.others_bh.put(row.get(0).toString().trim(), row.get(6).toString().trim());
					TestData.others_sa.put(row.get(0).toString().trim(), row.get(7).toString().trim());
					TestData.others_fb.put(row.get(0).toString().trim(), row.get(8).toString().trim());
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void readMissingData() {
		try {
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			final String spreadsheetId = configProperty.get("dataSourceID");
			String[] column = {"A","B","C","D","E","F","G","H"};
			String[] columnHead = {"rm","ww","ks","el","jl","bh","sa","fb"};
			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();

			for(int x=0; x<column.length; x++) {
				ValueRange response = service.spreadsheets().values().get(spreadsheetId, "missingData!" + column[x] + "1:" + column[x]).execute();
				List<List<Object>> values = response.getValues();
				List<String> data = new ArrayList<String>();
				for (int i = 1; i < values.size(); i++) {
					List<Object> row = values.get(i);
					if(!(row.size() == 0 || row.get(0).toString().isEmpty() || row.get(0).toString().trim().equals("") || row.get(0).toString().equals(null))) { 
						data.add(row.get(0).toString());
					}
				}
				TestData.missingData.put(columnHead[x], data);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void readEmailData() {
		try {
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			final String spreadsheetId = configProperty.get("dataSourceID");
			String[] column = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X"};
			String[] columnHead = {"rm_des","rm_mob","rm_tab","ww_des","ww_mob","ww_tab","ks_des","ks_mob","ks_tab","el_des","el_mob","el_tab","jl_des","jl_mob","jl_tab","bh_des","bh_mob","bh_tab","sa_des","sa_mob","sa_tab","fb_des","fb_mob","fb_tab"};
			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();
			
			for(int x=0; x<column.length; x++) {
				ValueRange response = service.spreadsheets().values().get(spreadsheetId, "email_"+Utils.getCurrentEnv()+"!" + column[x] + "1:" + column[x]).execute();
				List<List<Object>> values = response.getValues();
				List<String> data = new ArrayList<String>();
				for (int i = 1; i < values.size(); i++) {
					List<Object> row = values.get(i);
					if(!(row.size() == 0 || row.get(0).toString().isEmpty() || row.get(0).toString().trim().equals("") || row.get(0).toString().equals(null))) { 
						data.add(row.get(0).toString());
					}
				}
				TestData.emailData.put(columnHead[x], data);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	/*public static void markProductUnAvailable(String key, Brand brand) {

    	try {
    		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            final String spreadsheetId = "1Ny2AQ-VbTxOtYKt387I1tGsv2Me3hlXFLTJ8Xa73MoY";//configProperty.get("");
            String range1 = "product!A1:H1";

            Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();
            ValueRange response = service.spreadsheets().values().get(spreadsheetId, range1).execute();

            List<List<Object>> values = response.getValues();

            String columnNumber = "A";
            List<Object> row = values.get(0);
            for (int i = 0; i < row.size(); i++) {
            	if(brand.name().toLowerCase().equals(row.get(i).toString().toLowerCase())) {
            		columnNumber = Columns.values()[i].name().toString(); 
            	}
            }

            range1 = "product!A1:A";
            response.clear();
            values.clear();
            response = service.spreadsheets().values().get(spreadsheetId, range1).execute();
            values = response.getValues();

            int rowNumber = 0;
            for (int i = 0; i < values.size(); i++) {
                row = values.get(i);
                if(key.toLowerCase().equals(row.get(0).toString().toLowerCase()))
                	rowNumber = i+1; 
            }

            System.out.println("Location [column, row] :: [" + columnNumber + "," + rowNumber + "]");


    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    }*/

	public static List<List<Object>> getData(String sheetID, String range) {
		List<List<Object>> values = null;
		try {
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();
			ValueRange response = service.spreadsheets().values().get(sheetID, range).execute();
			values = response.getValues();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return values;
	}

	public static HashMap<String, String> getDataAsHashMap(String sheetID, String range) {
		List<List<Object>> values = null;
		HashMap<String, String> hashMap = new HashMap<String, String>();
		try {
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();
			ValueRange response = service.spreadsheets().values().get(sheetID, range).execute();
			values = response.getValues();
			for(List<Object> obj : values) {
				hashMap.put(obj.get(0).toString(), obj.get(1).toString());
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return hashMap;
	}

	public static List<HashMap<String, String>> getAPIData() {
		List<HashMap<String, String>> returnData = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> api_calls = getDataAsHashMap(configProperty.get("dataSourceID"), "api_calls!A1:B");
		try {
			//To use local property file to get api_call data
			//EnvironmentPropertiesReader api_calls = EnvironmentPropertiesReader.getInstance("api_calls");
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();
			ValueRange response = service.spreadsheets().values().get(configProperty.get("dataSourceID"), "api_data!A2:N").execute();
			List<List<Object>> values = response.getValues();

			Iterator<List<Object>> rows = values.iterator();

			while(rows.hasNext()) {
				HashMap<String, String> data = new HashMap<String, String>();
				List<Object> row = rows.next();
				if(row.get(0).toString().equals("api_id")) {
					continue;
				}

				if(row.get(5).toString().equals("no"))
					continue;

				data.put("api_id", row.get(0).toString());
				data.put("api_state", row.get(1).toString());
				data.put("api_description", row.get(2).toString());
				data.put("api_base", api_calls.get(row.get(3).toString()));
				data.put("api_type", row.get(4).toString());
				data.put("enabled", row.get(5).toString());
				data.put("api_url", api_calls.get(row.get(0).toString() + "_" + row.get(4).toString()));
				
				if(row.get(1).toString().equals("positive")) {
					data.put("api_path_parameters", row.get(6).equals(null)? "": row.get(6).toString());
					data.put("api_parameters", row.get(8).equals(null)? "": row.get(8).toString());
					data.put("api_input_data", row.get(10).equals(null)? "": row.get(10).toString());
					data.put("expected_status_code", row.get(12).toString());
				} else {
					data.put("api_path_parameters", row.get(7).equals(null)? "": row.get(7).toString());
					data.put("api_parameters", row.get(9).equals(null)? "": row.get(9).toString());
					data.put("api_input_data", row.get(11).equals(null)? "": row.get(11).toString());
					data.put("expected_status_code", row.get(13).toString());
				}
				returnData.add(data);
			}

		}catch(Exception e) {
			e.printStackTrace();
		}
		return returnData;
	}

	public static synchronized HashMap<String, String> getPlccData(String... type){
		HashMap<String, String> returnData = new HashMap<String, String>();

		try{
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();
			ValueRange response = service.spreadsheets().values().get(configProperty.get("dataSourceID"), configProperty.get("plcc-sheetName") + "!A1:M").execute();
			List<List<Object>> values = response.getValues();

			for(int i=1; i < values.size(); i++){
				List<Object> row = values.get(i);
				/*if(row.get(0).toString().equalsIgnoreCase("status")) {
					continue;
				}*/

				if(row.get(0).toString().equalsIgnoreCase("used"))
					continue;

				else if(row.get(0).toString().equalsIgnoreCase("unused")
						|| row.get(0).toString().equals("")){
					returnData.put("firstname", row.get(1).toString());
					returnData.put("middlename", row.get(2).toString());
					returnData.put("lastname", row.get(3).toString());
					returnData.put("suffix", row.get(4).toString());
					returnData.put("ssn", row.get(5).toString());
					returnData.put("dob", row.get(6).toString());
					returnData.put("streetnumber", row.get(7).toString());
					returnData.put("streetname", row.get(8).toString());
					returnData.put("streettype", row.get(9).toString());
					returnData.put("city", row.get(10).toString());
					returnData.put("state", row.get(11).toString());
					returnData.put("zipcode", row.get(12).toString());
					returnData.put("cellID", "A"+Integer.toString(i+1));
					returnData.put("status", "active");

					updateData(configProperty.get("dataSourceID"), configProperty.get("plcc-sheetName"), "A"+Integer.toString(i+1), Arrays.asList("active"));
					break;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		return returnData;
	}

	public static void updatePLCCDataStatus(HashMap<String, String> data) {
		try {
			if(data.get("status").equals("active")) {
				updateData(configProperty.get("dataSourceID"), configProperty.get("plcc-sheetName"), data.get("cellID"), Arrays.asList("unused"));
			}else if(data.get("status").equals("approved"))
				updateData(configProperty.get("dataSourceID"), configProperty.get("plcc-sheetName"), data.get("cellID"), Arrays.asList("approved"));
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized HashMap<String, String> getIssueData(String ticketNumber) {
		HashMap<String, String> ticketData = new HashMap<String, String>();
		try {
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();
			ValueRange response = service.spreadsheets().values().get(configProperty.get("dataSourceID"), "jira_tickets" + "!A1:D").execute();
			List<List<Object>> values = response.getValues();
			
			for(int i=1; i < values.size(); i++){
				List<Object> row = values.get(i);
				if(row.get(0).toString().equalsIgnoreCase(ticketNumber)){
					Log.event(values.get(i).toString());
					ticketData.put("trackingNumber", ticketNumber);
					ticketData.put("status", row.get(1).toString());
					ticketData.put("description", row.get(2).toString());
					ticketData.put("severity", row.get(3).toString());
					break;
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch(GeneralSecurityException gse) {
			gse.printStackTrace();
		}

		return ticketData;
	}
	
	public static synchronized String getRunID(String runCombination) {
		String comboRunID = "";
		try {
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();
			ValueRange response = service.spreadsheets().values().get(configProperty.get("dataSourceID"), "RunID" + "!A3:B").execute();
			List<List<Object>> sheetCells = response.getValues();
			for (int i = 0; i < sheetCells.size(); i++) {
				List<Object> row = sheetCells.get(i);
				if (row.get(0).toString().equalsIgnoreCase(runCombination)) {
					Log.event("Run combo and ID:: " + sheetCells.get(i).toString());
					comboRunID = row.get(1).toString();
					break;
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch(GeneralSecurityException gse) {
			gse.printStackTrace();
		}
		return comboRunID;
	}
	
	public static void updateEmailID(String username) {
		try {
			Log.event("[GAPI]Initiating result upload to Google Sheets...");
			// Build a new authorized API client service.
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			String range1 = "";
			String currentBrand = Utils.getCurrentBrandShort().replace("x", "");
			switch (currentBrand) {
			case "rm":
				if (Utils.isDesktop()) {
					range1 += "A";
				} else if (Utils.isMobile()) {
					range1 += "B";
				} else {
					range1 += "C";
				}
				break;
			case "ww":
				if (Utils.isDesktop()) {
					range1 += "D";
				} else if (Utils.isMobile()) {
					range1 += "E";
				} else {
					range1 += "F";
				}
				break;
			case "ks":
				if (Utils.isDesktop()) {
					range1 += "G";
				} else if (Utils.isMobile()) {
					range1 += "H";
				} else {
					range1 += "I";
				}
				break;
			case "el":
				if (Utils.isDesktop()) {
					range1 += "J";
				} else if (Utils.isMobile()) {
					range1 += "K";
				} else {
					range1 += "L";
				}
				break;
			case "jl":
				if (Utils.isDesktop()) {
					range1 += "M";
				} else if (Utils.isMobile()) {
					range1 += "N";
				} else {
					range1 += "O";
				}
				break;

			case "bh":
				if (Utils.isDesktop()) {
					range1 += "P";
				} else if (Utils.isMobile()) {
					range1 += "Q";
				} else {
					range1 += "R";
				}
				break;

			case "sa":
				if (Utils.isDesktop()) {
					range1 += "S";
				} else if (Utils.isMobile()) {
					range1 += "T";
				} else {
					range1 += "U";
				}
				break;

			case "fb":
				if (Utils.isDesktop()) {
					range1 += "V";
				} else if (Utils.isMobile()) {
					range1 += "W";
				} else {
					range1 += "X";
				}
				break;

			default:
				Log.event("[GAPI]Brand configuration Required.");
				break;
			}
			String thisRange = "email_qa!"+range1+"1:"+range1;
			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT)).setApplicationName(APPLICATION_NAME).build();
			ValueRange response = service.spreadsheets().values().get(spreadsheetId, thisRange).execute();
			List<List<Object>> ResponseValues = response.getValues();
			int size = ResponseValues.size()+1;
			
			List<String> UserList = new ArrayList<String>();
			UserList.add(username);
			List<List<Object>> values = new ArrayList<List<Object>>();
			for(int i=0; i<UserList.size();i++) {
				List<Object> val = new ArrayList<Object>();
				val.add(UserList.get(i));
				values.add(val);
			}

			ValueRange body = new ValueRange().setValues(values);
			UpdateValuesResponse result = service.spreadsheets().values().update(spreadsheetId, "email_"+Utils.getCurrentEnv()+"!"+range1+size, body).setValueInputOption("RAW").execute();
			Log.event("[GAPI]Result Updated. " + (int) result.getUpdatedCells() + "cells appended.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getColumnKey(Brand... brand) {
		Brand brandToGet = brand.length > 0 ? brand[0] : Utils.getCurrentBrand();
		switch(brandToGet) {
		case rmx:
		case rm:
			return Columns.B.toString();
		case wwx:
		case ww:
			return Columns.C.toString();
		case ksx:
		case ks:
			return Columns.D.toString();
		case elx:
		case el:
			return Columns.E.toString();
		case jlx:
		case jl:
			return Columns.F.toString();
		case bhx:
		case bh:
			return Columns.G.toString();
		case sax:
		case sa:
			return Columns.H.toString();
		case fbx:
		case fb:
			return Columns.I.toString();
		}
		return null;
	}

}