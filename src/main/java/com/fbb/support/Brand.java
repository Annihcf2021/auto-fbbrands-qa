package com.fbb.support;

/**
 * BrowserType is to set the configuration for browsers in the xml file during
 * suite execution
 */
public enum Brand {
	jl("JessicaLondon"), 
	ww("WomanWithin"), 
	rm("Roamans"), 
	ks("KingSize"), 
	el("Ellos"), 
	bh("BrylaneHome"),
	fb("Fullbeauty"),
	sa("SwimsuitsForAll"),
	jlx("JessicaLondon"), 
	wwx("WomanWithin"), 
	rmx("Roamans"), 
	ksx("KingSize"), 
	elx("Ellos"), 
	bhx("BrylaneHome"),
	fbx("Fullbeauty"),
	sax("SwimsuitsForAll");

	private String _brandConfigName;

	/**
	 * Constructor
	 * @param configuration -
	 */
	private Brand(String configuration) {
		this._brandConfigName = configuration;
	}

	/**
	 * To get current configuration
	 * @return String -
	 */
	public String getConfiguration() {
		return this._brandConfigName;
	}

	/**
	 * To get brand from given value
	 * @param value -
	 * @return Brand -
	 */
	public static Brand fromValue(String value){
		for(Brand b : values()){
			if(b.getConfiguration().contains(value) || b.getConfiguration().equalsIgnoreCase(value))
				return b;
		}

		return null;
	}

	/**
	 * To get brand from configuration name
	 * @param configurationName -
	 * @return Brand -
	 */
	public static Brand fromConfiguration(String configurationName) {
		for (Brand b : values()) {
			if (b.name().equals(configurationName)) {
				return b;
			}
		}
		throw new IllegalArgumentException(
				"There is no value '" + configurationName + "' in Enum '" + Brand.class.getName() + "'");
	}
}