package com.fbb.support;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;

/**
 * To store and access site build properties
 */
public class BuildProperties {
	
	private static boolean isUniversalCart = false;
	private static boolean isUCValueSet = false;
	private static String buildInfo;
	
	private static HashMap<String, HashMap<String, Boolean>> initialLoadMainBrand = new HashMap<String, HashMap<String, Boolean>>();
	private static HashMap<String, HashMap<String, Boolean>> resetBrowserSuiteMap = new HashMap<String, HashMap<String, Boolean>>();
	
	public static boolean isUCValueSet() {
		return isUCValueSet;
	}

	/**
	 * To access if Universal Cart is enabled for current site build on execution environment
	 * @return true/false is current site is UC enabled
	 */
	public static boolean isUniversalCart() {
		return isUniversalCart;
	}
	
	/**
	 * To store if Universal Cart is enabled for current site build on execution environment
	 * @param isUniversalCart
	 */
	public static void setUniversalCart(boolean isUniversalCart) {
		BuildProperties.isUniversalCart = isUniversalCart;
		isUCValueSet = true;
	}
	
	/**
	 * To save build information of current environment to static variable
	 * @param WebDriver instance 
	 */
	public static void setBuildInfo(WebDriver driver) {
		String pageTitle = driver.getTitle();
		Log.event("Page Title:: " + pageTitle);
		if (pageTitle.contains("|") && pageTitle.split("\\|").length == 3) {
			buildInfo = pageTitle.split("\\|")[0].trim();
		} else {
			buildInfo = "Production";
		}
		Log.event("Build information:: " + buildInfo);
	}
	
	/**
	 * To get build information of current environment saved in static variable 
	 */
	public static String getBuildInfo() {
		if(buildInfo != null) {
			return buildInfo;
		} else {
			return "";
		}
	}
	
	/**
	 * To get initial main brand load status
	 */
	public static boolean getInitialLoadMainBrand() {
		String xmlTest = Utils.getXMLTestName();
		String testName = Utils.getCurrentTestName();
		HashMap<String, Boolean> initLoadMap = initialLoadMainBrand.get(xmlTest);
		boolean initLoad = true;
		if (initLoadMap == null || initLoadMap.get(testName) == null)
			initLoad = true;
		else
			initLoad = initLoadMap.get(testName);
		
		Log.event("Getting initLoad status for "  + testName + " in " + xmlTest + ":: "+ initLoad);
		return initLoad;
	}
	
	/**
	 * To set initial main brand load status
	 * @param initLoad
	 */
	public static void setInitialLoadMainBrand(Boolean initLoad) {
		String xmlTest = Utils.getXMLTestName();
		String testName = Utils.getCurrentTestName();
		HashMap<String, Boolean> initLoadMap = initialLoadMainBrand.get(xmlTest);
		if (initLoadMap == null) {
			initLoadMap = new HashMap<String, Boolean>();
		}
		Log.event("Setting initLoad status for "  + testName + " in " + xmlTest + ":: "+ initLoad);
		initLoadMap.put(testName, initLoad);
		initialLoadMainBrand.put(xmlTest, initLoadMap);
	}
	
	/**
	 * To set test browser reset status
	 * @param initLoad
	 */
	public static void setTestBrowserReset(Boolean resetBrowser) {
		String xmlTest = Utils.getXMLTestName();
		String testName = Utils.getCurrentTestName();
		HashMap<String, Boolean> resetBrowserTestMap = resetBrowserSuiteMap.get(xmlTest);
		if (resetBrowserTestMap == null) {
			resetBrowserTestMap = new HashMap<String, Boolean>();
		}
		Log.event("Setting reset browser for "  + testName + " in " + xmlTest + ":: "+ resetBrowser);
		resetBrowserTestMap.put(testName, resetBrowser);
		resetBrowserSuiteMap.put(xmlTest, resetBrowserTestMap);
	}
	
	/**
	 * To get initial main brand load status
	 */
	public static boolean getTestBrowserReset() {
		String xmlTest = Utils.getXMLTestName();
		String testName = Utils.getCurrentTestName();
		HashMap<String, Boolean> resetBrowserTestMap = resetBrowserSuiteMap.get(xmlTest);
		boolean resetBrowser = false;
		if (resetBrowserTestMap == null || resetBrowserTestMap.get(testName) == null)
			resetBrowser = false;
		else
			resetBrowser = resetBrowserTestMap.get(testName);
		
		Log.event("Getting reset browser for "  + testName + " in " + xmlTest + ":: "+ resetBrowser);
		return resetBrowser;
	}

}
