package com.fbb.support;

import java.util.HashMap;

/**
 * Class to set the Mobile emulation configuration on chrome browser
 * 
 * <a href=
 *      "http://www.webapps-online.com/online-tools/user-agent-strings/dv">User
 *      Agent String</a> for more info on the available user agent string.
 * <a href=
 *      "https://sites.google.com/a/chromium.org/chromedriver/mobile-emulation">
 *      Mobile emulation on chrome</a> for more info on chrome mobile emulation.
 * 
 */
public class MobileEmulationUserAgentConfiguration {

	public static final String CHROME_IPHONE_IOS = "iphone11_chrome_mobile";
	public static final String CHROME_IPAD_IOS = "ipad_chrome_tablet";
	public static final String CHROME_TABLET_ANDROID = "galaxytabs5e_chrome_tablet";
	public static final String CHROME_MOBILE_ANDROID = "galaxys8_chrome_mobile";
	public static final String SAFARI_IPHONE_IOS = "iphone11_safari_mobile";
	public static final String SAFARI_IPAD_IOS = "ipad_safari_tablet";
	public static final String CHROME_MACOS_SAFARI = "macbookPro_safari_desktop";
	public static final String SAMSUNG_MOBILE_ANDROID = "galaxys8plus_samsung_mobile";
	public static final String SILK_KINDLE_FIREOS = "amazonKindle_silk_tablet";
	
	public static final String ANDROID_GALAXY_TAB_A5_1_LANDSCAPE = "androidgalaxy_a5_1_tablet_landscape";

	@SuppressWarnings("serial")
	private final HashMap<String, String> chrome_iphone11_ios13 = new HashMap<String, String>() {
		{
			put("userAgent", "Mozilla/5.0 (iPhone; CPU iPhone OS 13 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/78.0.3904.84 Mobile/15E148 Safari/605.1");
			put("width", "414");
			put("height", "896");
			put("pixelRatio", "2");			
		}
	};
	
	@SuppressWarnings("serial")
	private final HashMap<String, String> chrome_ipad_ios13 = new HashMap<String, String>() {
		{
			put("userAgent", "Mozilla/5.0 (iPad; CPU iPad OS 13 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/78.0.3904.84 Mobile/15E148 Safari/605.1");
			put("width", "810");
			put("height", "1080");
			put("pixelRatio", "2");
		}
	};
	
	@SuppressWarnings("serial")
	private final HashMap<String, String> safari_iphone11_ios13 = new HashMap<String, String>() {
		{
			put("userAgent", "Mozilla/5.0 (iPhone; CPU iPhone OS 13 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.1 Mobile/15E148 Safari/604.1");
			put("width", "414");
			put("height", "896");
			put("pixelRatio", "2");			
		}
	};
	
	@SuppressWarnings("serial")
	private final HashMap<String, String> safari_ipad_ios13 = new HashMap<String, String>() {
		{
			put("userAgent", "Mozilla/5.0 (iPad; CPU iPad OS 13 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.1 Mobile/15E148 Safari/604.1");
			put("width", "810");
			put("height", "1080");
			put("pixelRatio", "2");
		}
	};
	
	@SuppressWarnings("serial")
	private final HashMap<String, String> chrome_galaxytabs5e_android8 = new HashMap<String, String>() {
		{
			put("userAgent", "Mozilla/5.0 (Linux; Android 8; SM-T720) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Mobile Safari/537.36");
			put("width", "800");
			put("height", "1280");
			put("pixelRatio", "2");
		}
	}; 
	
	@SuppressWarnings("serial")
	private final HashMap<String, String> chrome_galaxys8_android8 = new HashMap<String, String>() {
		{
			put("userAgent", "Mozilla/5.0 (Linux; Android 8; SM-G950) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Mobile Safari/537.36");
			put("width", "720");
			put("height", "1480");
			put("pixelRatio", "2");		
		}
	}; 
	
	@SuppressWarnings("serial")
	private final HashMap<String, String> safari_mackbookpro_macos = new HashMap<String, String>() {
		{
			put("userAgent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit/604.3.5 (KHTML, like Gecko) Version/12.0.1 Safari/604.3.5");
			put("width", "1440");
			put("height", "900");
			put("pixelRatio", "2");			
		}
	};
	
	@SuppressWarnings("serial")
	private final HashMap<String, String> samsung11_galaxys8plus_android9 = new HashMap<String, String>() {
		{
			put("userAgent", "Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G955U) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/11.1 Chrome/75.0.3770.143 Mobile Safari/537.36");
			put("width", "720");
			put("height", "1480");
			put("pixelRatio", "2");			
		}
	};
	
	@SuppressWarnings("serial")
	private final HashMap<String, String> silk73_amazonKindleFireHD10_fireOS = new HashMap<String, String>() {
		{
			put("userAgent", "Mozilla/5.0 (Linux; Android 5.1.1; KFSUWI) AppleWebKit/537.36 (KHTML, like Gecko) Silk/73.2.3 like Chrome/73.0.3683.90 Safari/537.36");
			put("width", "800");
			put("height", "1280");
			put("pixelRatio", "2");			
		}
	};

	/**
	 * To storing the all the devices configurations
	 *
	 * @return userAgentData - have mobile emulation configuration data(user
	 *         agent, width, height and pixelRatio)
	 */
	public HashMap<String, HashMap<String, String>> setUserAgentConfigurationValue() {

		HashMap<String, HashMap<String, String>> userAgentData = new HashMap<String, HashMap<String, String>>();

		userAgentData.put(CHROME_IPHONE_IOS, chrome_iphone11_ios13);	
		userAgentData.put(CHROME_IPAD_IOS, chrome_ipad_ios13);
		userAgentData.put(SAFARI_IPHONE_IOS, safari_iphone11_ios13);
		userAgentData.put(SAFARI_IPAD_IOS, safari_ipad_ios13);
		userAgentData.put(CHROME_MOBILE_ANDROID, chrome_galaxys8_android8);
		userAgentData.put(CHROME_TABLET_ANDROID, chrome_galaxytabs5e_android8);
		userAgentData.put(CHROME_MACOS_SAFARI, safari_mackbookpro_macos);
		userAgentData.put(SAMSUNG_MOBILE_ANDROID, samsung11_galaxys8plus_android9);
		userAgentData.put(SILK_KINDLE_FIREOS, silk73_amazonKindleFireHD10_fireOS);
		
		return userAgentData;
	}

	/**
	 * To get the user agent string from device name
	 * 
	 * @param deviceName
	 *            - device name which going to perform a mobile emulation on
	 *            chrome
	 * @return dataToBeReturned- device width
	 */
	public String getUserAgent(String deviceName) {
		String dataToBeReturned = null;
		HashMap<String, HashMap<String, String>> getUserAgent = setUserAgentConfigurationValue();
		dataToBeReturned = hasDeviceName(deviceName) ? (String) getUserAgent.get(deviceName).get("userAgent") : null;
		return dataToBeReturned;
	}

	/**
	 * To get the device width string from device name
	 * 
	 * @param deviceName
	 *            - device name which going to perform a mobile emulation on
	 *            chrome
	 * @return String -
	 */
	public String getDeviceWidth(String deviceName) {
		String dataToBeReturned = null;
		HashMap<String, HashMap<String, String>> getDeviceWidth = setUserAgentConfigurationValue();
		dataToBeReturned = hasDeviceName(deviceName) ? (String) getDeviceWidth.get(deviceName).get("width") : null;
		return dataToBeReturned;
	}

	/**
	 * To get the device height string from device name
	 * 
	 * @param deviceName
	 *            - device name which going to perform a mobile emulation on
	 *            chrome
	 * @return dataToBeReturned- device height
	 */
	public String getDeviceHeight(String deviceName) {
		String dataToBeReturned = null;
		HashMap<String, HashMap<String, String>> getDeviceHeight = setUserAgentConfigurationValue();
		dataToBeReturned = hasDeviceName(deviceName) ? (String) getDeviceHeight.get(deviceName).get("height") : null;
		return dataToBeReturned;
	}

	/**
	 * To get the device pixel ratio string from device name
	 * 
	 * @param deviceName
	 *            - device name which going to perform a mobile emulation on
	 *            chrome
	 * @return dataToBeReturned - device pixel ratio
	 */
	public String getDevicePixelRatio(String deviceName) {
		String dataToBeReturned = null;
		HashMap<String, HashMap<String, String>> getDevicePixelRatio = setUserAgentConfigurationValue();
		dataToBeReturned = hasDeviceName(deviceName) ? (String) getDevicePixelRatio.get(deviceName).get("pixelRatio")
				: null;
		return dataToBeReturned;
	}

	/**
	 * To check the device name present in the set up key hash map
	 * 
	 * @param deviceName
	 *            - device name which going to perform a mobile emulation on
	 *            chrome
	 * @return boolean value - if device name in the set up key will return
	 *         true, otherwise false
	 */
	private boolean hasDeviceName(String deviceName) {
		HashMap<String, HashMap<String, String>> hasDeviceName = setUserAgentConfigurationValue();
		return hasDeviceName.containsKey(deviceName);
	}

	/**
	 * To get the device name from mobile emulation attributes
	 * 
	 * @param userAgent
	 *            - mapped user agent string with device name
	 * @param pixelRatio
	 *            - mapped pixel ratio with device name
	 * @param width
	 *            - mapped width with device name
	 * @param height
	 *            - mapped height with device name
	 * @return dataToBeReturned - device name mapped with user agent, pixel
	 *         ratio, width and height
	 */
	public String getDeviceNameFromMobileEmulation(String userAgent, String pixelRatio, String width, String height) {
		String dataToBeReturned = null;
		boolean found = false;
		HashMap<String, HashMap<String, String>> getDeviceData = setUserAgentConfigurationValue();
		for (Object usKey : getDeviceData.keySet()) {
			if (getDeviceData.get(usKey).get("userAgent").equals(userAgent)
					&& getDeviceData.get(usKey).get("pixelRatio").equals(pixelRatio)
					&& getDeviceData.get(usKey).get("width").equals(width)
					&& getDeviceData.get(usKey).get("height").equals(height)) {
				dataToBeReturned = (String) usKey;
				found = true;
			}
		}
		if (!found) {
			dataToBeReturned = null;
		}
		return dataToBeReturned;
	}

	
}
