package com.fbb.support;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryAnalyzer implements IRetryAnalyzer {

	
	/** 
	 *  
	 * Need to set  'MaxRetryCount','IncludeExceptions' and 'ExcludeExceptions' properties
	 * in config file for this implementation
	 * 
	 * Eg: MaxRetryCount =2 
	 * IncludeExceptions =TimeoutException
	 * ExcludeExceptions = NoSuchWindowException
	 */
	private EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
	private static Logger logger = LoggerFactory.getLogger(RetryAnalyzer.class);
	
	private int retriedCount = 0;
	private int maxRetryCount = configProperty.hasProperty("MaxRetryCount") ? Integer.parseInt(configProperty.getProperty("MaxRetryCount")) : 0;
	
	private List<String> excludeExceptions = configProperty.hasProperty("ExcludeExceptions") ? Arrays.asList(configProperty.getProperty("ExcludeExceptions").split("\\|")) : Arrays.asList();
	
	@Override
	public boolean retry(ITestResult result) {
		boolean isRetry = false;
		if(System.getProperty("MaxRetryCount") != null) {
			try {
				maxRetryCount = Integer.parseInt(System.getProperty("MaxRetryCount"));
				System.out.println("MaxRetryCount set from command line:: " + maxRetryCount);
			} catch(Exception e) {
				System.err.println("Error reading MaxRetryCount from command line.\n" + e.getMessage());
			}
		}
		
		String exception = result.getThrowable().getClass().getSimpleName();
		System.out.println(result.getName() + " failed with:: " + result.getThrowable().getClass().getName());
		
		if (retriedCount < maxRetryCount && !excludeExceptions.contains(exception)) {
			logger.debug(result.getName() + " failed with " + result.getThrowable().getClass().getName());
			logger.debug("Retrying " + result.getName() + " for '" + ++retriedCount + "' time");
			System.out.println("Retrying " + result.getName() + " for '" + retriedCount + "' time");
			ExtentReporter.setTestStatusAsSkip(result);
			isRetry = true;
		} else if (retriedCount < maxRetryCount) {
			System.out.println(result.getName() + " has reached maximum number of retry.");
		} else if (excludeExceptions.contains(exception)) {
			System.out.println(exception + " is inlcuded in ExcludeExceptions list.");
		}
		return isRetry;
	}

}
