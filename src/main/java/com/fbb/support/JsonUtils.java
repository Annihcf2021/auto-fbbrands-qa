package com.fbb.support;

import java.io.FileReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JsonUtils {
	private static JSONParser jsonParser = new JSONParser();
	
	public static JSONObject fileToJson(String filePath) {
		JSONObject fileAsJson = null;
		try {
			FileReader file = new FileReader(filePath);
			Object obj = jsonParser.parse(file);
			fileAsJson = (JSONObject) jsonParser.parse(obj.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileAsJson;
	}
}
