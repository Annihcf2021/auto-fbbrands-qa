package com.fbb.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.testng.Reporter;
import com.utils.testrail.TestRailAPICalls;

public class TestRailSupportUtils {

	public static HashMap<String, String> TCSUMMARY_MAPPING = new HashMap<String, String>();
	public static HashMap<String, String> TRCASEID_MAPPING = new HashMap<String, String>();
	public static HashMap<String, Integer> STATUS_MAPPING = new HashMap<String, Integer>();
	private static HashMap<String, String> COMMENT_MAPPING = new HashMap<String, String>();
	private static HashMap<String, String> DEFECT_MAPPING = new HashMap<String, String>();
	public static HashMap<String, String> DEFECT_COMMENT_MAPPING = new HashMap<String, String>();
	public static HashMap<String, Integer> HASHCODE_MAPPING = new HashMap<String, Integer>();

	@SuppressWarnings("rawtypes")
	public static HashMap<String, HashMap<Integer, HashMap>> STEP_RESULT_MAPPING = new HashMap<String, HashMap<Integer, HashMap>>();
	private static HashMap<String, HashMap<String, HashMap<String, String>>> SCRIPT_METHOD_MAP = new HashMap<String, HashMap<String, HashMap<String, String>>>();

	/**
	 * To Add Step wise result in TestRail
	 * @param stepNo - TestRail Case - Step Number
	 * @param actualResult - Result To Update
	 * @param status - TestRail Status code 1- Pass, 2-Block, 3-Untest, 4- Retest, 5- Failed
	 * @throws Exception - Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void addStepResult(String stepNo, String actualResult, String status)throws Exception{
		HashMap<Integer, HashMap> result = new HashMap<Integer, HashMap>();
		if(STEP_RESULT_MAPPING.containsKey(Reporter.getCurrentTestResult().getName()))
			result = STEP_RESULT_MAPPING.get(Reporter.getCurrentTestResult().getName());

		HashMap stepResult = new HashMap();
		int status_id = 4;
		if(status.equalsIgnoreCase("pass"))
			status_id = 1;
		else if(status.equalsIgnoreCase("hold"))
			status_id = 3;
		else if(status.equalsIgnoreCase("fail"))
			status_id = 5;

		//stepResult.put("content","Step " + stepNo);
		stepResult.put("actual", actualResult);
		stepResult.put("status_id", status_id);
		result.put(Integer.parseInt(stepNo), stepResult);
		STEP_RESULT_MAPPING.put(Reporter.getCurrentTestResult().getName(), result);
	}

	/**
	 * To submit current test result to TestRail
	 */
	public static void submitResult(){
		try{
			String tc_id = getTestCaseKey();
			int tc_status = STATUS_MAPPING.get(Integer.toString(Utils.getCurrentTestCode()));
			String tc_comment = COMMENT_MAPPING.get(Integer.toString(Utils.getCurrentTestCode())) == null ? ""
								: COMMENT_MAPPING.get(Integer.toString(Utils.getCurrentTestCode()));
			String tc_bug = DEFECT_MAPPING.get(Integer.toString(Utils.getCurrentTestCode())) == null ? ""
								: DEFECT_MAPPING.get(Integer.toString(Utils.getCurrentTestCode()));
			
			tc_status = finalTestStatusMapping(tc_status);
			String run_id = SystemProperties.getRunID();
			Log.message("Submitting Results to Testrail for Testcase :: " + tc_id + "  & RunID :: " + run_id);
			new TestRailAPICalls().addResultForCase(run_id, tc_id, tc_status, tc_comment, tc_bug);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * To submit given test result to TestRail
	 * @param testResult
	 */
	public static void submitResult(HashMap<String, String> testResult){
		try {
			String tc_id = testResult.get("tc_id");
			int tc_status = StringUtils.getNumberInString(testResult.get("tc_status"));
			String tc_comment = testResult.get("tc_comment") == null ? "" : testResult.get("tc_comment");
			String tc_bug = testResult.get("tc_bug") == null ? "" : testResult.get("tc_bug");
			
			tc_status = finalTestStatusMapping(tc_status);
			String run_id = SystemProperties.getRunID();
			Log.message("Submitting Results to Testrail for Testcase :: " + tc_id + "  & RunID :: " + run_id);
			new TestRailAPICalls().addResultForCase(run_id, tc_id, tc_status, tc_comment, tc_bug);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * To convert tc_status values as desired for internal use
	 */
	private static int finalTestStatusMapping(int tc_status) {
		int finalStatus = tc_status;
		if(tc_status == 4){
			Log.message("Test Case failed due to Script Exception which might be caused by Application Issue (or) Some technical Issue.");
			Log.message("So Testrail Result will be updated as Re-Test.");
		}
		if(tc_status == 5) {
			Log.message("Test case is getting failed, hence marking as Retest");
			finalStatus = 4;
		}
		return finalStatus;
	}
	
	/**
	 * To get the Result for the case
	 * @return- Result of the case
	 * @throws - Exception 
	 */
	public static HashMap<String, String> getTestCaseResults() {		
		HashMap<String, String> dataTobeReturn = new HashMap<String, String>();
		String tc_id = getTestCaseKey();
		int tc_status = STATUS_MAPPING.get(Integer.toString(Utils.getCurrentTestCode()));
		
		String tc_comment = "";
		if(tc_status != 1) 
			tc_comment = COMMENT_MAPPING.get(Integer.toString(Utils.getCurrentTestCode()));
		
		String tc_bug = DEFECT_MAPPING.get(Integer.toString(Utils.getCurrentTestCode())) != null ? DEFECT_MAPPING.get(Integer.toString(Utils.getCurrentTestCode())) : "";
		
		dataTobeReturn.put("tc_id", tc_id);
		dataTobeReturn.put("tc_status", String.valueOf(tc_status));
		dataTobeReturn.put("tc_comment", tc_comment);
		dataTobeReturn.put("tc_bug", tc_bug);
				
		return dataTobeReturn;
	}
	
	/**
	 * To save result of test script method to data structure
	 * <br> Result will be save in <test case, <script name, <script result map>>> format
	 * @param scriptResult
	 */
	public static void updateScriptResultMap(HashMap<String, String> scriptResult) {
		String nameScriptMethod = Utils.getCurrentTestName();
		String nameTestCase = getTestCaseKey();
		HashMap<String, HashMap<String, String>> mapTestCase;
		
		if (SCRIPT_METHOD_MAP.get(nameTestCase) != null) {
			mapTestCase = SCRIPT_METHOD_MAP.get(nameTestCase);
		} else {
			mapTestCase = new HashMap<String, HashMap<String,String>>();
		}
		mapTestCase.put(nameScriptMethod, scriptResult);
		SCRIPT_METHOD_MAP.put(nameTestCase, mapTestCase);
	}
	
	/**
	 * To get saved result data structure for a TestCase
	 * <br> Result is retrieved as <script name, <script result map>>
	 */
	public static HashMap<String, HashMap<String, String>> getTestCaseMap() {
		return SCRIPT_METHOD_MAP.get(getTestCaseKey());
	}
	
	private static String getTestCaseKey() {
		String nameScriptMethod = Utils.getCurrentTestName();
		if (nameScriptMethod.contains("_C")) {
			return nameScriptMethod.split("_C")[1];
		} else
			return nameScriptMethod;
	}
	
	/**
	 * To get compiled TestCase result from multiple scripts under same TestCase
	 * @param mapTestCase
	 * @return Compiled result as HashMap
	 */
	public static HashMap<String, String> getCompiledTestCaseResult(HashMap<String, HashMap<String, String>> mapTestCase) {
		List<HashMap<String, String>> scriptReults = new ArrayList<HashMap<String, String>>(mapTestCase.values());
		HashMap<String, String> compiledResultMap = scriptReults.get(0);
		String tc_comment= "", tc_bug= "";
		compiledResultMap.putIfAbsent("tc_comment", "");
		compiledResultMap.putIfAbsent("tc_bug", "");

		for (HashMap<String, String> result : scriptReults) {
			int currentStatus = StringUtils.getNumberInString(result.get("tc_status"));
			if (currentStatus > StringUtils.getNumberInString(compiledResultMap.get("tc_status"))) {
				compiledResultMap.put("tc_status", Integer.toString(currentStatus));
			}

			if (result.get("tc_comment") != null && result.get("tc_comment") != "") {
				tc_comment = tc_comment + System.lineSeparator() + result.get("tc_comment");
				compiledResultMap.put("tc_comment", tc_comment);
			}

			if (result.get("tc_bug") != null && result.get("tc_bug") != "") {
				tc_bug = tc_bug + result.get("tc_bug");
				compiledResultMap.put("tc_bug", tc_bug);
			}
		}
		
		if(compiledResultMap.get("tc_bug").equals("")) {
			compiledResultMap.remove("tc_bug");
		}
		
		return compiledResultMap;
	}

	/**
	 * To map comments for TestRail upload
	 * @param testComment
	 */
	public static void mapTestComment(String testComment) {
		if (SystemProperties.isEnableTestRailAPI()) {
      		COMMENT_MAPPING.putIfAbsent(Integer.toString(Utils.getCurrentTestCode()), "Test Initialization Success.");
      		String existingComments = COMMENT_MAPPING.get(Integer.toString(Utils.getCurrentTestCode()));
      		if (!existingComments.contains(testComment)) {
				COMMENT_MAPPING.put(Integer.toString(Utils.getCurrentTestCode()), existingComments + System.lineSeparator() + testComment);
      		}
      	}
	}
	
	/**
	 * To map defects for TestRail upload
	 * @param testComment
	 */
	public static void mapTestDefect(String testDefect) {
		if (SystemProperties.isEnableTestRailAPI()) {
			DEFECT_MAPPING.putIfAbsent(Integer.toString(Utils.getCurrentTestCode()), "");
			String existingDefects = DEFECT_MAPPING.get(Integer.toString(Utils.getCurrentTestCode()));
			if (!existingDefects.contains(testDefect)) {
				DEFECT_MAPPING.put(Integer.toString(Utils.getCurrentTestCode()), existingDefects + ", " + System.lineSeparator() + testDefect);
			}
		}
	}
}