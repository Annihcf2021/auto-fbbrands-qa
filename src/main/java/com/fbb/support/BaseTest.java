package com.fbb.support;

import java.io.File;

import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.utils.testrail.TestRailAPICalls;


public class BaseTest{

	public static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
	public static EnvironmentPropertiesReader prdData;
	public static EnvironmentPropertiesReader envProperties = EnvironmentPropertiesReader.getInstance("env");
	public static String startDate = DateTimeUtility.getCurrentDateAs("dd-MM-yyyy");
	{
		File outputDirectory = new File(new File("").getAbsolutePath() + File.separator + "test-output");
		if(outputDirectory.exists())
			FileUtils.deleteAllFilesFromFolder(outputDirectory);
	}

	/**
	 * To load pre-required parameters for suite level
	 */
	@BeforeSuite(alwaysRun = true)
	public static void setDefaultsSuite(){
		System.out.println("[BeforeSuite] Initializing System Parameters for Execution...");
		SystemProperties.setupProperties();
		SheetsQuickstart.readData();
		SheetsQuickstart.readMissingData();
		SheetsQuickstart.readEmailData();
		System.out.println("[BeforeSuite] Initializing System Parameters for Execution...");

		if (SystemProperties.getUpdateGSheet()) {
			try {
				if (!SheetsQuickstart.validateReportDate()) {
					SheetsQuickstart.duplicateSheetFromTemplate();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		//To Initiate TestRail Services
		new TestRailAPICalls();
		new TestDataWritter();
	}

	/**
	 * To Load pre-required parameters for test level
	 * @param context -
	 * @return String - WebSite URL
	 * @throws Exception 
	 */
	@BeforeTest(alwaysRun = true)
	public static String setDefaults(ITestContext context) throws Exception{
		System.out.println("[BeforeTest-Start] Initializing test parameters for :: " + context.getName());
		SystemProperties.setTestDefaults(context);
		System.out.println("Number of Test(s) in " + context.getName() + ":: " + context.getAllTestMethods().length);

		String brand = SystemProperties.getCurrentBrand().toString();
		prdData = EnvironmentPropertiesReader.getInstance("data_" + brand.replace("x", ""));
		
		String webSite = SystemProperties.getWebsiteURL();
		System.out.println("[BeforeTest-End] Initializing test parameters...");
		return webSite;
	}

	/**
	 * To load site brand if running multiple test in a single test suite
	 * @param context -
	 * @throws Exception 
	 */
	//To Set defaults for WebSite & Brand when running brands in parallel from a single testng suite
	public static void setSiteBrand(ITestContext context) throws Exception{
		if(Reporter.getCurrentTestResult().getTestContext().getSuite().getAllMethods().size() > 1){
			//To Initiate webSite URL with brand & environment
			String webSite = Utils.getWebSite();

			String brand = (System.getProperty("brand") != null ? 
					System.getProperty("brand") : context.getCurrentXmlTest().getParameter("brand"));

			String env = (System.getProperty("env") != null ? 
					System.getProperty("env") : context.getCurrentXmlTest().getParameter("env"));

			//To make system properties in lower case
			brand = Brand.fromConfiguration(brand.toLowerCase()).name().toLowerCase();
			env = env.toLowerCase();
			webSite = webSite.toLowerCase();

			//To construct webSite URL
			webSite = webSite.replace("!brand!", brand).replace("!env!", env);

			boolean isParallel = context.getSuite().getAllMethods().size() > 1 || brand.contains("|");

			if(isParallel){
				System.setProperty(Utils.getCurrentTestCode() + "_brand", brand);
				System.setProperty(Utils.getCurrentTestCode() + "_env", env);
				System.setProperty(Utils.getCurrentTestCode() + "_webSite", webSite);
			}
		}
	}

}
