package com.fbb.support;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
public class LogResult {

	// Variable declaration
	String logTCDesc = "";
	String logTCStep = "";
	static boolean pathFlag;
	String testResultLogPath;
	String responseLogPath;

	Date timeStart, timeStop;
	long calendarIns;
	FileOperation objTestResultFile, objResponseLogFile;
	// KO
	LinkedHashMap<String, String> lhmAllOutputResponse = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> lhmAllInputResponse = new LinkedHashMap<String, String>();

	/* Logging Results New Template */
	public FileWriter fWriter = null, fWriterOverall = null;
	public FileReader freader = null;
	public BufferedWriter writer = null, writerOverall = null;
	public BufferedWriter orverAllWriter = null;
	public BufferedWriter failwriter = null;
	public BufferedReader reader = null;
	public String sFilePath;
	public String ResultFile = null, ResultFileOverall = null;

	// Testlink - XML results import
	public FileWriter xmlfWriter = null;
	public BufferedWriter xmlwriter = null;
	public String xmlResultFile = null;

	public int intStep = 1, intStepOverall = 1;
	public int[] intStepNo = new int[5000];

	static boolean initStatus; // added for p2
	public LinkedHashMap<String, Boolean> lhmTCStatusP2 = new LinkedHashMap<String, Boolean>(); // added for p2
	public ArrayList<ArrayList<String>> strTotalReport = new ArrayList<ArrayList<String>>();

	public LinkedHashMap<String, String> lhmTotalReport = new LinkedHashMap<String, String>();
	public String keyResult = "", valueResult = "";

	public String failSeprate = "";
	public String reportfileHeader = "";
	public LinkedHashMap<String, String> lhmFailureReport = new LinkedHashMap<String, String>();

	public LinkedHashMap<String, String> lhmTCHeaderDetails = new LinkedHashMap<String, String>();
	public LinkedHashMap<String, Boolean> lhmTCStatus = new LinkedHashMap<String, Boolean>();
	public int PassCount = 0, FailCount = 0;

	/*******************
	 * Variable Declaration ************************
	 ******************************************************************/

	public LinkedHashMap<String, String> lhmTCDesc = new LinkedHashMap<String, String>();
	public LinkedHashMap<String, String> lhmTCGenericException = new LinkedHashMap<String, String>();

	public FileWriter fWriterCSV = null;
	public BufferedWriter writerCSV = null;
	public String ResultFileCSV = null;

	/**
	 * Constructor for the class
	 */

	public LogResult(String logFilePath) {
		try {
			String tempPath = "";
			String[] logFileArr = logFilePath.split("\\\\");
			for (int i = 0; i < logFileArr.length; i++) {
				tempPath = tempPath + logFileArr[i] + "\\";
			}

			// Create the log directory under soap ui installed directory
			File responseFile = new File(tempPath);
			if (responseFile.exists() == false) {
				responseFile.mkdir();
			}

			testResultLogPath = tempPath + "TestResultLog_.txt";
			responseLogPath = tempPath + "RequestResponseLog_.txt";

			objTestResultFile = new FileOperation(testResultLogPath);
			objResponseLogFile = new FileOperation(responseLogPath);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public String createResFile(String Script_Name) {
		try {
			ResultFile = new File(".").getCanonicalPath() + File.separator + "TestReport" + File.separator + Script_Name + ".html";
			fWriter = new FileWriter(ResultFile);
			writer = new BufferedWriter(fWriter);

			// Testlink XML import
			xmlResultFile = sFilePath + "Testlink_Results_Import.xml";
			xmlfWriter = new FileWriter(xmlResultFile);
			xmlwriter = new BufferedWriter(xmlfWriter);

			xmlwriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			xmlwriter.newLine();
			xmlwriter.write("<results>");

			reportfileHeader = "<Html> " + "<head>  "
					+ "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>"
					+ "<script type=\"text/javascript\">    google.load('visualization', '1.0', {'packages':['corechart']});  google.setOnLoadCallback(drawChart);"
					+ "function drawChart() {"
					+ "var data = new google.visualization.DataTable(); data.addColumn('string', 'Result'); data.addColumn('number', 'Count');"
					+

					"data.addRows([" + "['PASS', PASS_COUNT_REPLACEMENT]," + "['FAIL', FAIL_COUNT_REPLACEMENT]," + "]);"
					+ " var options = {'title':'::: Execution Trend :::'," + " 'width':500," + " 'height':300,"
					+ " 'backgroundColor':'DAF1FE'," + " 'colors': ['#009933', '#F00000']," + " 'fontSize': 15};"
					+ "var chart = new google.visualization.PieChart(document.getElementById('chart_div'));"
					+ "chart.draw(data, options);	}  </script>" +

					"<title> Result - " + Script_Name + " </title>  </head> " + "<script> function showDiv(objectID) { "
					+ "var theElementStyle = document.getElementById(objectID); " +

					" if(theElementStyle.style.display == \"none\") "
					+ " { theElementStyle.style.display = \"block\";  }"
					+ " else { theElementStyle.style.display = \"none\"; } " + " } " +

					" </script> " + "<body bgcolor=\"DAF1FE\">";

			writer.newLine();
			intStep = 1;
			Arrays.fill(intStepNo, 1);
			// Arrays.fill(logFinalStatement, "");
			// writer.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return ResultFile;
	}

	/*
	 * public String createFailResFile(String Script_Name) { String failFile; try {
	 * failFile = sFilePath + Script_Name + ".html"; fWriter = new
	 * FileWriter(failFile); failwriter = new BufferedWriter(fWriter);
	 * 
	 * failwriter.write("<Html> " + "<head>  <title> Result - " + Script_Name +
	 * " </title>  </head> " + "<script> function showDiv(objectID) { " +
	 * "var theElementStyle = document.getElementById(objectID); " +
	 * 
	 * " if(theElementStyle.style.display == \"none\") " +
	 * " { theElementStyle.style.display = \"block\";  }" +
	 * " else { theElementStyle.style.display = \"none\"; } " + " } " +
	 * 
	 * " </script> " + "<body bgcolor=\"DAF1FE\">");
	 * 
	 * failwriter.newLine(); intStep = 1; Arrays.fill(intStepNo, 1); //
	 * Arrays.fill(logFinalStatement, "");
	 * 
	 * } catch (Exception e) { e.printStackTrace(); return null; } return failFile;
	 * }
	 */

	public void logHeader(String ScriptName) {

		//String strReport = null;
		String strHeaderTableWidth1 = "35%";
		String strHeaderTableWidth = "100%";
		String strHeaderFirstColumnWidth = "45%";
		String strHeaderSecondColumnWidth = "70%";
		try {
			reportfileHeader = reportfileHeader + "<table width=\"100%\" border=\"0\" margin=\"10,5,5,50\"  >"
					+ "<tr width=\"100%\">" + "<th > " + ScriptName + " Execution Report</th>" + "</tr>	" +
					/*
					 * "<tr width=\"100%\">" +
					 * "<th align=\"right\"> <a href=\"#\" Target=\"_bottom\"> Go to Bottom </a></th>"
					 * + "</tr>	" +
					 */
					"</table>" + "<br /> <div id=\"chart_div\" style=\"float:right\"></div>";

			// writer.write(strReport);
			// writer.newLine();

			reportfileHeader = reportfileHeader + "<table width=\"" + strHeaderTableWidth1
					+ "\" border=\"1\" margin=\"10,5,5,50\"    >" + "<tr width=\"" + strHeaderTableWidth
					+ "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\"" + strHeaderFirstColumnWidth
					+ "\" >Test Suite</th>" + "<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> "
					+ ScriptName + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">Environment</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "QU" + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">Sprint Name</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">Start Time</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "hStartTime" + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">End Time</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "hEndTime" + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">Total Execution Time</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "hTotalTime" + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">Total Count</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "hTestCaseCount" + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">Pass Count</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "hPassCount" + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">Fail Count</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "hFailCount" + "</th>" + "</tr>	" +


					"</table>" + "<table width=\"100%\" border=\"0\" margin=\"10,5,5,50\"  >" + "<tr width=\"100%\">"
					+ "<th width=\"100%\"> <font color=\"red\"><a  href=\"Failure_Report.html\" > Filter Failures <a/></font></th>"
					+ "</tr> </table>" + "<br /> ";

			// writer.write(strReport);
			// writer.newLine();

			reportfileHeader = reportfileHeader + "<table width=\"100%\" class=\"" + ScriptName
					+ "\"  bgcolor=\"#82B6EA\" border=\"1\" margin=\"10,5,5,50\">" + "<tr>"
					+ "<th width=\"8%\">Environment</th>"
					+ "<th width=\"60%\">Test Case Number & Description</th>"
					+ "<th width=\"10%\">Execution Duration (Seconds)</th>" + "<th width=\"7%\">Test Status</th>"
					+ "<th width=\"10%\">Bug_ID</th>" + "</tr>" + "</table>";
			writer.write(reportfileHeader);
			writer.newLine();
		} catch (Exception ex) {
		}
	}

	public void logTCDesc(String strTCNumber, String strTCDesc) {
		String strReport = "";
		int stepTCID = Integer.parseInt(strTCNumber);
		try {

			strReport = "<table width=\"100%\" class=\"" + strTCNumber
					+ "\"  bgcolor=\"#DEE6EE\" border=\"2\" margin=\"10,5,5,50\">" + " <tr>" + "<td width=\"8%\" >"
					+ "QU" + "</td>" +
					// "<td width=\"64%\"><a href=\"#\" ID=\"One\" style=\"visibility:visible\">" +
					// strTCNumber + ". " + strTCDesc + "</a></td>" +
					// "<td width=\"5%\" align=\"center\"> " + "tcLinkID" + stepTCID + " </td>"
					"<td width=\"60%\"><a href=\"#\" onClick=\"showDiv('targetDiv" + strTCNumber
					+ "');return false;\">" + strTCNumber + ". " + strTCDesc + "</a></td>"
					+ "<td width=\"10%\" align=\"center\"> " + "tcExecDuration" + stepTCID + " </td>"
					+ "<td width=\"7%\" align=\"center\"> " + "tcStatus" + stepTCID + " </td>"
					+ "<td width=\"10%\" align=\"center\"> " + "tcException" + stepTCID + " </td>" + "</tr>"
					+ "</table>" + "<div id=\"targetDiv" + strTCNumber + "\" style=\"display:none\">";

			keyResult = strTCNumber.toString() + "_0"; // + (stepTCID-1)
			// keyResult = keyResult + stepTCID+ "_" + (stepTCID-1)
			// keyResult = keyResult
			valueResult = strReport.toString();
			writer.write(strReport);
			writer.newLine();
			// logTCDesc = strReport;
			lhmTotalReport.put(keyResult, valueResult);
			lhmTCStatus.put(strTCNumber, true);
			lhmTCDesc.put(strTCNumber, strTCDesc);
			lhmTCGenericException.put(strTCNumber, "");
		} catch (Exception ex) {
		}
	}

	public void logFailTCDesc(String strTCNumber, String strTCDesc) {
		String strReport = "";
		int stepTCID = Integer.parseInt(strTCNumber);
		try {

			strReport = strReport.toString() + "<table width=\"100%\" class=\"" + strTCNumber
					+ "\"  bgcolor=\"#DEE6EE\" border=\"2\" margin=\"10,5,5,50\">" + " <tr>" + "<td width=\"8%\" >"
					+ "QU" + "</td>" +
					// "<td width=\"64%\"><a href=\"#\" ID=\"One\" style=\"visibility:visible\">" +
					// strTCNumber + ". " + strTCDesc + "</a></td>" +
					"<td width=\"5%\" align=\"center\"> " + "tcLinkID" + stepTCID + " </td>"
					+ "<td width=\"60%\"><a href=\"#\" onClick=\"showDiv('targetDiv" + strTCNumber
					+ "');return false;\">" + strTCNumber + ". " + strTCDesc + "</a></td>"
					+ "<td width=\"10%\" align=\"center\"> " + "tcExecDuration" + stepTCID + " </td>"
					+ "<td width=\"7%\" align=\"center\"> " + "tcStatus" + stepTCID + " </td>"
					+ "<td width=\"10%\" align=\"center\"> " + "tcException" + stepTCID + " </td>" + "</tr>"
					+ "</table>" + "<div id=\"targetDiv" + strTCNumber + "\" style=\"display:none\">";

			keyResult = strTCNumber.toString() + "_0"; // + (stepTCID-1)
			// keyResult = keyResult + stepTCID+ "_" + (stepTCID-1)
			// keyResult = keyResult
			valueResult = strReport.toString();
			lhmFailureReport.put(keyResult, valueResult);
			lhmTCStatus.put(strTCNumber, true);
			lhmTCDesc.put(strTCNumber, strTCDesc);
			lhmTCGenericException.put(strTCNumber, "");
		} catch (Exception ex) {
		}
	}

	/**
	 * This is used to log statements with Pass in black and Fail in red color
	 *
	 * @param enumResultType
	 *            - To define what kind of step it is. Like SCRIPT , DB_VALUE ,
	 *            DB_COMPARISON, APPLICATION_ERROR , DB_ERROR, SCRIPT_ERROR ,
	 *            UNKNOWN_ERROR
	 * @param strReport
	 *            - Logger statement
	 * @param status
	 *            - To define whether its pass or fail. True - Pass, False - Fail
	 */
	public void logPassorFail(String strTCNumber, String strReport, boolean status, String req, String response) {

		int stepTCID = Integer.parseInt(strTCNumber) - 1;
		String failmessage = "";
		try {
			if (status) {

				{
					String showReqResText = req;
					String shwResdivIndex = "TargetShowRes" + strTCNumber;

					showReqResText = showReqResText.replace("<", "&lt;");
					showReqResText = showReqResText.replace(">", "&gt;");
					showReqResText = showReqResText.replace("&gt;&lt;", "&gt;<br>&lt;");
					showReqResText = "<pre><table bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White;\"><tr><td><font color=\"blue\" size=\"4\"><b>REQUEST</b></font><br><br>"
							+ showReqResText + "<br><br></td>"
							+ "<td><font color=\"blue\" size=\"4\"><b>RESPONSE</b></font><br><br>" + response
							+ "</td></tr></table>";
					showReqResText = showReqResText.replace("|| REPLACE WITH RESP HEADER ||",
							"<br><br><hr><br>" + "<font color=\"blue\" size=\"4\"><b>RESPONSE</b></font><br><br>");
					showReqResText = showReqResText + "</pre></div>";

					strReport = logTCStep + "<TABLE ID=\"One\" class= \"" + strTCNumber
							+ "\" bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White;\">"
							+ " <TR> <TD width = \"8%\">" + "StepNo:  " + intStepNo[stepTCID] + // intStep +
							"    </TD>       <TD width = \"92%\"  bgcolor=\"PowderBlue\" > <b>" + " :: " + strReport
							+ "</b> <a href=\"#\" onClick=\"showDiv('" + shwResdivIndex
							+ "');return false;\"> Show Request / Response </a>  </TD>        </TR></TABLE></div> "
							+ "<div id=\"" + shwResdivIndex + "\" style=\"display:none\">" + showReqResText ;
					// strReport = strReport.replace("Actual Value -", "<br><b>Actual Value -</b>")
					// strReport = strReport.replace("Expected Value -", "<br><b>Expected Value
					// -</b>")
					strReport = strReport.replace("Actual Data -", "<b>Actual Data -</b>");
					strReport = strReport.replace("Expected Data -", "<br><b>Expected Data -</b>");
					strReport = strReport.replace("Actual Result -", "<br><hr size='1'><b>Actual Result -</b>");
					strReport = strReport.replace("Expected Result -", "<br><b>Expected Result -</b>");
					lhmTCStatus.put(strTCNumber, true);
					/*
					 * strReport = "<TABLE ID=\"One\" class= \"" + strTCNumber +
					 * "\" bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White;\">"
					 * + " <TR> <TD width = \"8%\" >" + "StepNo:  " + intStepNo[stepTCID] + //
					 * intStep + "    </TD>       <TD width = \"92%\">" + strReport +
					 * " </TD>        </TR></TABLE>";
					 */
				}
			} else {
				// strReport = strReport.replace("Actual Value -", "<br><b>Actual Value -</b>")
				// strReport = strReport.replace("Expected Value -", "<br><b>Expected Value
				// -</b>")
				strReport = strReport.replace("Actual Data -", "<b>Actual Data -</b>");
				strReport = strReport.replace("Expected Data -", "<br><b>Expected Data -</b>");
				strReport = strReport.replace("Actual Result -", "<br><hr size='1'><b>Actual Result -</b>");
				strReport = strReport.replace("Expected Result -", "<br><b>Expected Result -</b>");
				strReport = "<TABLE ID=\"One\" class= \"" + strTCNumber
						+ "\" bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White; color:red\">"
						+ " <TR> <TD width = \"8%\" >" + "StepNo:  " + intStepNo[stepTCID] + // intStep +
						"    </TD>       <TD width = \"92%\">" + strReport + " </TD>        </TR></TABLE>";
				lhmTCStatus.put(strTCNumber, false);
				failmessage = strReport;

				// Calculate step id and number of failure count here
			}

			keyResult = strTCNumber.toString() + "_" + intStepNo[stepTCID];
			valueResult = strReport;
			writer.write(strReport);
			writer.newLine();
			lhmTotalReport.put(keyResult, valueResult);
			if (failmessage != "")
				lhmFailureReport.put(keyResult, failmessage);

			// writer.write(strReport);
			// writer.newLine();
			intStep++;
			// intStepNo[stepTCID] = intStepNo[stepTCID] + 1
			// incrementStepNo(stepTCID);
		} catch (Exception e) {
		}
		// return (intStepNo[stepTCID])
	}

	/**
	 * This function updates result file with specified content - Generic
	 * consolidated statement in green color and warning messages in blue color
	 *
	 * @param strReport
	 *            - Display message to be printed in output html report
	 * @param bWarning
	 *            - To specify whether the message to be printed is "Warning" - true
	 *            (will be printed in blue color) Consolidated statement - false
	 *            (will be printed in green color)
	 */
	public void logGenericorWarning(String strTCNumber, String strReport, boolean bWarning, boolean bReplace) {
		try {
			int stepTCID = Integer.parseInt(strTCNumber) - 1;

			if (!bWarning) {
				strReport = "<TABLE width = '100%' cellspacing = 0 cellpadding = 2  style = 'border-left:solid 3px White;color:green'>"
						+ "   <TR>       <TD width = '8%' >" + "       </TD>       <TD width = '84%'>" + strReport
						+ "       </TD>       <TD width = '8%'>" + "<SPAN></SPAN>       </TD>   </TR></TABLE>";
			} else {
				strReport = "<TABLE ID=\"One\"   class= \"" + strTCNumber
						+ "\" bgcolor=\"#FBFBEF\" border width ='100%'  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White; color:blue; visibility:visible\"\">"
						+ " <TR> <TD width = \"8%\" >" + "StepNo:  " + intStepNo[stepTCID] + // intStep +
						"    </TD>       <TD width = \"92%\">" + " :: " + strReport + " </TD>        </TR></TABLE>";
			}

			// strTotalReport.get(stepTCID-1).add(strReport);
			keyResult = strTCNumber.toString() + "_" + intStepNo[stepTCID];
			valueResult = strReport;
			lhmTotalReport.put(keyResult, valueResult);

			intStep++;
			// intStepNo[stepTCID-1]++;
			// incrementStepNo(stepTCID);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void logTCEnd(String strOverallReportPath) {
		try {
			totalPassorFailTC();

			// printInFile(tcIDs, strScriptName);
			String strReport = "</div> <br /> <table width=\"100%\" border=\"0\" margin=\"10,5,5,50\"  >"
					+ "<tr width=\"100%\">" + "<th width=\"90%\"> <a  href=\"file:///" + strOverallReportPath
					+ "#\" > Back to Overall Report <a/></th>"
					+ "<th width=\"10%\"> <a  href=\"#\" Target=\"_top\"> Go to Top <a/></th>"
					+ "</tr>	</table> <br /> ";

			writer.write(strReport);
			writer.newLine();

			writer.write(" </body> </html>");
			writer.newLine();
			writer.close();

			

		} catch (Exception e) {
		}
	}

	public void logHeader_Overall(String ScriptName) {
		String strReport = null;
		String strHeaderTableWidth1 = "35%";
		String strHeaderTableWidth = "100%";
		String strHeaderFirstColumnWidth = "30%";
		String strHeaderSecondColumnWidth = "70%";
		try {
			strReport = "<table width=\"100%\" border=\"0\" margin=\"10,5,5,50\"  >" + "<tr width=\"100%\">" + "<th > "
					+ ScriptName + " </th>" + "</tr>	" + "</table>" + "<br /> ";

			writerOverall.write(strReport);
			writerOverall.newLine();

			strReport = "<table width=\"" + strHeaderTableWidth1 + "\" border=\"1\" margin=\"10,5,5,50\"    >"
					+ "<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\" >Test Type</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + ScriptName + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">Environment</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "QU" + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">Start Time</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "hStartTime" + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">End Time</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "hEndTime" + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">Total Count</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "hTestCaseCount" + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">Pass Count</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "hPassCount" + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">Fail Count</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "hFailCount" + "</th>" + "</tr>	" +

					"<tr width=\"" + strHeaderTableWidth + "\" align=\"left\">" + "<th bgcolor=\"#82B6EA\" width=\""
					+ strHeaderFirstColumnWidth + "\">Skipped Count</th>" + "<th bgcolor=\"#FBFBEF\" width=\""
					+ strHeaderSecondColumnWidth + "\"> " + "hSkippedCount" + "</th>" + "</tr>	" +

					// "<tr width=\""+ strHeaderTableWidth + "\" align=\"left\">" +
					// "<th bgcolor=\"#82B6EA\" width=\""+ strHeaderFirstColumnWidth + "\">Avg API
					// Response Time</th>" +
					// "<th bgcolor=\"#FBFBEF\" width=\"" + strHeaderSecondColumnWidth + "\"> " +
					// "hAPIResponseTime" + "</th>" +
					// "</tr> " +

					"</table>" + "<br /> ";

			writerOverall.write(strReport);
			writerOverall.newLine();

			strReport = "<table width=\"100%\" class=\"" + ScriptName
					+ "\"  bgcolor=\"#82B6EA\" border=\"1\" margin=\"10,5,5,50\">" + "<tr>"
					+ "<th width=\"8%\">Number</th>" + "<th width=\"30%\">Module Name</th>" +
					/*
					 * "<th width=\"8%\">Execution Start Time</th>" +
					 * "<th width=\"8%\">Execution End Time</th>" +
					 */
					"<th width=\"8%\">TC Executed</th>" + "<th width=\"8%\">TC Passed</th>"
					+ "<th width=\"8%\">TC Failed</th>" + "<th width=\"8%\">TC Skipped</th>" +
					// "<th width=\"10%\">Exec Time (in min)</th>" +
					"<th width=\"20%\">Detailed Report</th>" + "</tr>" + "</table>";
			writerOverall.write(strReport);
			writerOverall.newLine();
		} catch (Exception ex) {
		}
	}

	public void logTCEnd_Overall() {
		try {
			writerOverall.write(" </body> </html>");
			writerOverall.newLine();
			writerOverall.close();
		} catch (Exception e) {
		}
	}

	public void logTCEnd_Overall(String path) {
		try {
			fWriterOverall = new FileWriter(path);
			writerOverall = new BufferedWriter(fWriterOverall);
			writerOverall.write(" </body> </html>");
			writerOverall.newLine();
			writerOverall.close();
		} catch (Exception e) {
		}
	}

	/**
	 * TODO Put here a description of what this method does.
	 *
	 */
	public void logTCDesc_Overall(String strModuleName, String strTotalExecuted, String strPass, String strFail,
			String strDetailedRptPath, String strSkipped, String strExecTime) {
		// scenarioStart = System.DateTime.Now;
		String strReport = null;
		try {
			strReport = "<table width=\"100%\" class=\""
					+ "\"  bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White;\">"
					+ " <tr>" + "<td width=\"8%\" >" + intStepOverall + "</td>" + "<td width=\"30%\">" + strModuleName
					+ "</td>" +
					/*
					 * "<td width=\"8%\" align=\"center\"> " + "Current Date Time" + "</td>" +
					 * "<td width=\"8%\" align=\"center\"> " + "SEndTime" + " </td>" +
					 */
					"<td width=\"8%\" align=\"center\"> " + strTotalExecuted + " </td>"
					+ "<td width=\"8%\" align=\"center\"> " + strPass + " </td>" + "<td width=\"8%\" align=\"center\"> "
					+ strFail + " </td>" + "<td width=\"8%\" align=\"center\"> " + strSkipped + " </td>" +
					// "<td width=\"10%\" align=\"center\"> " + strExecTime + " </td>" +
					"<td width=\"20%\" align=\"center\"> <a href=\"file:///" + strDetailedRptPath
					+ "#\" ID=\"One\" style=\"visibility:visible\">" + "Click here for detailed report" + "</a> </td>"
					+ "</tr>" + "</table>";

			writerOverall.write(strReport);
			writerOverall.newLine();

			intStepOverall++;
		} catch (Exception ex) {
		}
	}

	public void logTCDesc_Overall(String path, String strModuleName, String strTotalExecuted, String strPass,
			String strFail, String strDetailedRptPath, String strSkipped, String strExecTime, String countModule) {
		// scenarioStart = System.DateTime.Now;
		String strReport = null;

		try {

			fWriterOverall = new FileWriter(path, true);
			writerOverall = new BufferedWriter(fWriterOverall);

			strReport = "<table width=\"100%\" class=\""
					+ "\"  bgcolor=\"#FBFBEF\" border width =\"100%\"  cellspacing = 0 cellpadding = 2  style = \"border-left:solid 2px White;\">"
					+ " <tr>" + "<td width=\"8%\" >" + countModule + "</td>" + "<td width=\"30%\">" + strModuleName
					+ "</td>" +
					/*
					 * "<td width=\"8%\" align=\"center\"> " + "Current Date Time" + "</td>" +
					 * "<td width=\"8%\" align=\"center\"> " + "SEndTime" + " </td>" +
					 */
					"<td width=\"8%\" align=\"center\"> " + strTotalExecuted + " </td>"
					+ "<td width=\"8%\" align=\"center\"> " + strPass + " </td>" + "<td width=\"8%\" align=\"center\"> "
					+ strFail + " </td>" + "<td width=\"8%\" align=\"center\"> " + strSkipped + " </td>" +
					// "<td width=\"10%\" align=\"center\"> " + strExecTime + " </td>" +
					"<td width=\"20%\" align=\"center\"> <a href=\"file:///" + strDetailedRptPath
					+ "#\" ID=\"One\" style=\"visibility:visible\">" + "Click here for detailed report" + "</a> </td>"
					+ "</tr>" + "</table>";

			fWriterOverall.write(strReport);
			// fWriterOverall.newLine();
			fWriterOverall.close();
			intStepOverall++;
		} catch (Exception ex) {
		}
	}

	public void log_overAll_Close() {
		try {
			writerOverall.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void totalPassorFailTC() {
		// Getting Collection of values from HashMap

		Collection<Boolean> values = lhmTCStatus.values();

		// Creating an ArrayList of values

		ArrayList<Boolean> valueStatus = new ArrayList<Boolean>(values);
		// Collection<Boolean> valueStatus = lhmTCStatus.values();
		for (Boolean boolean1 : valueStatus) {
			if (boolean1)
				PassCount++;
			else
				FailCount++;
		}

	}

	public void reportBuilder() {
		String strReport = null;
		try {
			writer = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(sFilePath + "filename.html"), "utf-8"));
			writer.write(reportfileHeader);
			writer.write(logTCDesc);
			// writer.write(valueResult);
			strReport = "</div> <br /> <table width=\"100%\" border=\"0\" margin=\"10,5,5,50\"  >"
					+ "<tr width=\"100%\">" + "<th width=\"90%\"> <a  href=\"file:///"

					+ "#\" > Back to Overall Report <a/></th>"
					+ "<th width=\"10%\"> <a  href=\"#\" Target=\"_top\"> Go to Top <a/></th>"
					+ "</tr>	</table> <br /> ";

			writer.write(strReport);
			writer.newLine();

			writer.write(" </body> </html>");
			writer.newLine();
		} catch (IOException ex) {
			// Report
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
				/* ignore */}
		}
	}
	
	public void updateResults(String Script_Name) {
		//String strReport = null;
		try {
			File file = new File(new File(".").getCanonicalPath() + File.separator + "TestReport" + File.separator + Script_Name + ".html");
	        String fileContext = FileUtils.readFileToString(file);
	        Iterator<Entry<String, Boolean>> it = lhmTCStatus.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Boolean> pair = (Map.Entry<String, Boolean>) it.next();
				System.out.println(pair.getKey() + " = " + pair.getValue());
				if(pair.getValue())
					fileContext = fileContext.replaceAll("<td width=\"7%\" align=\"center\"> " + "tcStatus" + pair.getKey() + " </td>", "<td width=\"7%\" align=\"center\" bgcolor=\"green\"> " + "Passed"+" </td>");
				else
					fileContext = fileContext.replaceAll("<td width=\"7%\" align=\"center\"> " + "tcStatus" + pair.getKey() + " </td>", "<td width=\"7%\" align=\"center\" bgcolor=\"#FF0000\"> " + "Failed"+" </td>");
			}
	        
	        FileUtils.write(file, fileContext);
		} catch (IOException ex) {
			// Report
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
				/* ignore */}
		}
	}
}
