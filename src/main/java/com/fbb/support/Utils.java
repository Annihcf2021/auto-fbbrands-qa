package com.fbb.support;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import com.fbb.support.SystemProperties.Platform;

/**
 * Util class consists wait for page load,page load with user defined max time
 * and is used globally in all classes and methods
 */
public class Utils {
	private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();

	public static int maxElementWait = Integer.parseInt(configProperty.getProperty("maxElementWait"));

	/**
	 * waitForPageLoad waits for the page load with default page load wait time
	 * @param driver - WebDriver instance
	 */
	public static void waitForPageLoad(final WebDriver driver) {
		waitForPageLoad(driver, WebDriverFactory.maxPageLoadWait);
	}

	/**
	 * waitForPageLoad waits for the page load with custom page load wait time
	 * @param driver - WebDriver instance
	 * @param maxWait - Max wait duration
	 */
	public static void waitForPageLoad(final WebDriver driver, int maxWait) {
		long startTime = StopWatch.startTime();
		FluentWait<WebDriver> wait = new WebDriverWait(driver, maxWait).pollingEvery(Duration.ofMillis(500))
				.ignoring(StaleElementReferenceException.class).withMessage("Page Load Timed Out");
		try {
			if (configProperty.getProperty("documentLoad").equalsIgnoreCase("true"))
				wait.until(WebDriverFactory.documentLoad);

			if (configProperty.getProperty("imageLoad").equalsIgnoreCase("true"))
				wait.until(WebDriverFactory.imagesLoad);

			if (configProperty.getProperty("framesLoad").equalsIgnoreCase("true"))
				wait.until(WebDriverFactory.framesLoad);

			String title = driver.getTitle().toLowerCase();
			String url = driver.getCurrentUrl().toLowerCase();
			Log.event("Page URL:: " + url);

			if ("the page cannot be found".equalsIgnoreCase(title)
					|| title.contains("is not available")
					|| url.contains("/error/")
					|| url.toLowerCase().contains("/errorpage/")) {
				Assert.fail("Site is down. [Title: " + title + ", URL:" + url + "]");
			}
		} catch (TimeoutException te) {
			Log.event("Page load timed out.");
		} catch (Exception e) {
			Log.event("Unknown exception: " + e.getClass().getName());
		}
		
		Log.event("Page Load Completed.", StopWatch.elapsedTime(startTime));
	} // waitForPageLoad

	/**
	 * To wait for the specific element on the page
	 * @param driver - WebDriver instance
	 * @param element - WebElement to wait for
	 * @return boolean - return true if element is present else return false
	 */
	public static boolean waitForElement(WebDriver driver, WebElement element) {
		return waitForElement(driver, element, maxElementWait);
	}
	
	/**
	 * To wait for the specific element on the page
	 * @param driver - WebDriver instance
	 * @param elementToWaitFor - WebElement to wait for
	 * @param obj - Page object
	 * @return boolean - return true if element is present else return false
	 */
	public static boolean waitForElement(WebDriver driver, String elementToWaitFor, Object obj) {
		return waitForElement(driver, elementToWaitFor, obj, maxElementWait);
	}

	/**
	 * To wait for the specific element on the page
	 * @param driver - WebDriver instance
	 * @param element - WebElement to wait for
	 * @param maxWait - Max wait duration
	 * @return boolean - return true if element is present else return false
	 */
	public static boolean waitForElement(WebDriver driver, WebElement element, int maxWait) {
		long startTime = StopWatch.startTime();
		boolean statusOfElementToBeReturned = false;
		WebDriverWait wait = new WebDriverWait(driver, maxWait);
		try {
			WebElement waitElement = wait.until(ExpectedConditions.visibilityOf(element));
			if (waitElement.isDisplayed() && waitElement.isEnabled()) {
				statusOfElementToBeReturned = true;
				Log.event("Element is displayed:: " + element.toString());
			}
		} catch (Exception e) {
			statusOfElementToBeReturned = false;
			Log.event("Unable to find a element after " + StopWatch.elapsedTime(startTime) + " sec ==> " + element.toString());
		}
		return statusOfElementToBeReturned;
	}
	
	/**
	 * To check if an element is instantly available
	 * @param element - Element to check availability of
	 * @return true/false if element is instantly available
	 */
	public static boolean isElementAvailable(WebElement element) {
		boolean elementAvailable = false;
		try {
			elementAvailable = element.isDisplayed() && element.isEnabled();
			Log.event("Element is displayed:: " + element.toString());
		} catch (Exception e) {
			Log.event("Element not displayed:: " + element.toString());
		}
		return elementAvailable;
	}
	
	/**
	 * To wait for the specific element on the page
	 * @param driver - WebDriver instance
	 * @param elementToWaitFor - WebElement to wait for
	 * @param obj - Page object
	 * @param maxWait - Time to wait for elementToWaitFor to appear
	 * @return boolean - return true if element is present else return false
	 */
	public static boolean waitForElement(WebDriver driver, String elementToWaitFor, Object obj, int maxWait) {
		WebElement element = null;
		try {
			Field f = obj.getClass().getDeclaredField(elementToWaitFor);
			f.setAccessible(true);
			element = ((WebElement) f.get(obj));
		} catch (Exception e) {
			e.printStackTrace();
			Log.failsoft("No such a field present on this page, Please check the value:: " + elementToWaitFor);
			return false;
		}
		
		boolean statusOfElementToBeReturned = false;
		WebDriverWait wait = new WebDriverWait(driver, maxWait);
		long startTime = StopWatch.startTime();
		try {
			WebElement waitElement = wait.until(ExpectedConditions.visibilityOf(element));
			if (waitElement.isDisplayed() && waitElement.isEnabled()) {
				statusOfElementToBeReturned = true;
				Log.event("Element is displayed:: " + element.toString());
			}
		} catch (Exception e) {
			statusOfElementToBeReturned = false;
			Log.event("Unable to find a element after " + StopWatch.elapsedTime(startTime) + " sec ==> " + element.toString());
		}
		return statusOfElementToBeReturned;
	}

	public static boolean waitForElementFromDOM(WebDriver driver, WebElement element) {
		return waitForElementFromDOM(driver, element, maxElementWait);
	}
	
	public static boolean waitForElementFromDOM(WebDriver driver, WebElement element, int maxWait) {
		boolean statusOfElementToBeReturned = false;
		WebDriverWait wait = new WebDriverWait(driver, maxWait);
		long startTime = StopWatch.startTime();
		try {
			wait.until(ExpectedConditions.visibilityOf(element));
			statusOfElementToBeReturned = true;
			Log.event("Element Available:: " + element.toString());
		} catch (Exception e) {
			statusOfElementToBeReturned = false;
			Log.event("Unable to find a element after " + StopWatch.elapsedTime(startTime) + " sec ==> " + element.toString());
		}
		return statusOfElementToBeReturned;
	}

	/**
	 * To switch between windows
	 * @param driver - WebDriver instance
	 * @param windowToSwitch -
	 * @param opt - title/url
	 * @param closeCurrentDriver -
	 * @return WebDriver of current window
	 * @throws Exception - Exception
	 */
	public static WebDriver switchWindows(
			WebDriver driver, String windowToSwitch, String opt, String closeCurrentDriver) throws Exception {

		WebDriver currentWebDriver = driver;
		WebDriver assingedWebDriver = driver;
		boolean windowFound = false;
		ArrayList<String> multipleWindows = new ArrayList<String>(assingedWebDriver.getWindowHandles());

		for (int i = 0; i < multipleWindows.size(); i++) {
			assingedWebDriver.switchTo().window(multipleWindows.get(i));
			Utils.waitForPageLoad(assingedWebDriver);
			Log.event("Switched to:: " + multipleWindows.get(i));
			if (opt.equals("title")) {
				if (assingedWebDriver.getTitle().trim().equals(windowToSwitch)) {
					windowFound = true;
					break;
				}
			} else if (opt.equals("url")) {
				try {
					windowToSwitch = windowToSwitch.split("//")[1];
					if (assingedWebDriver.getCurrentUrl().contains(windowToSwitch)) {
						windowFound = true;
						break;
					}
				} catch(ArrayIndexOutOfBoundsException e) {
					if (assingedWebDriver.getCurrentUrl().contains(windowToSwitch)) {
						windowFound = true;
						break;
					}
				}
			}
		}

		if (!windowFound)
			throw new Exception("Window: " + windowToSwitch + ", not found!!");
		else {
			if (closeCurrentDriver.equals("true"))
				currentWebDriver.close();
		}
		return assingedWebDriver;
	}// switchWindows
	
	public static WebDriver switchToFirstWindow(WebDriver driver) throws Exception {
		ArrayList<String> multipleWindows = new ArrayList<String>(driver.getWindowHandles());
		driver = driver.switchTo().window(multipleWindows.get(0));

		return driver;
	}// switchWindows

	/**
	 * Switching between tabs or windows in a browser
	 * @param driver - WebDriver instance
	 */
	public static WebDriver switchToNewWindow(WebDriver driver) {
		String winHandle = driver.getWindowHandle();
		for (String index : driver.getWindowHandles()) {
			if (!index.equals(winHandle)) {
				driver.switchTo().window(index);
				break;
			}
		}
		if (!((RemoteWebDriver) driver).getCapabilities().getBrowserName().matches(".*safari.*")) {
			((JavascriptExecutor) driver).executeScript(
					"if(window.screen){window.moveTo(0, 0); window.resizeTo(window.screen.availWidth, window.screen.availHeight);};");
		}
		
		return driver;
	}
	
	public static void openNewTab(WebDriver driver) {
		((JavascriptExecutor) driver).executeScript("window.open('about:blank', '_blank');");
		switchToNewWindow(driver);
	}
	
	public static WebDriver openNewTab(WebDriver driver, String url) {
		((JavascriptExecutor) driver).executeScript("window.open('about:blank', '_blank');");
		switchToNewWindow(driver);
		driver.get(url);
		return driver;
	}
	
	/**
	 * Verify the css property for an element
	 * @param element - WebElement for which to verify the css property
	 * @param cssProperty - the css property name to verify
	 * @param actualValue - the actual css value of the element
	 * @return boolean
	 */
	public static boolean verifyCssPropertyForElement(WebElement element, String cssProperty, String actualValue) {
		boolean result = false;

		String actualClassProperty = element.getCssValue(cssProperty);
		Log.event("Value from DOM :: " + actualClassProperty);
		Log.event("Value from USer :: " + actualValue);
		if (actualClassProperty.contains(actualValue)) {
			result = true;
		}
		return result;
	}

	/**
	 * To wait for the specific element which is in disabled state on the page
	 * @param driver - current driver object
	 * @param element - disabled webelement
	 * @param maxWait - duration of wait in seconds
	 * @return boolean - return true if disabled element is present else return false
	 */
	public static boolean waitForDisabledElement(WebDriver driver, WebElement element, int maxWait) {
		boolean statusOfElementToBeReturned = false;
		long startTime = StopWatch.startTime();
		WebDriverWait wait = new WebDriverWait(driver, maxWait);
		try {
			WebElement waitElement = wait.until(ExpectedConditions.visibilityOf(element));
			if ((!waitElement.isEnabled()) || waitElement.getAttribute("disabled").equals("true")) {
				statusOfElementToBeReturned = true;
				Log.event("Element is displayed and disabled:: " + element.toString());
			}
		} catch (Exception ex) {
			statusOfElementToBeReturned = false;
			Log.event("Unable to find disabled element after " + StopWatch.elapsedTime(startTime) + " sec ==> "
					+ element.toString());
		}
		return statusOfElementToBeReturned;
	}

	/**
	 * Wait until element disappears in the page
	 * @param driver - driver instance
	 * @param element - webelement to wait to have disaapear
	 * @return true if element is not appearing in the page
	 */
	public static boolean waitUntilElementDisappear(WebDriver driver, final WebElement element) {
		final boolean isNotDisplayed;

		WebDriverWait wait = new WebDriverWait(driver, 60);
		isNotDisplayed = wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				boolean isPresent = false;
				try {
					if (Utils.waitForElement(driver, element)) {
						isPresent = false;
						Log.event("Element " + element.toString() + ", is still visible in page");
					}else{
						isPresent = true;
						Log.event("Element " + element.toString() + ", is not displayed in page ");
					}
				} catch (Exception ex) {
					isPresent = true;
					Log.event("Element " + element.toString() + ", is not displayed in page ");
					return isPresent;
				}
				return isPresent;
			}
		});
		return isNotDisplayed;
	}

	/**
	 * Wait until element disappears in the page
	 * @param driver -
	 * @param element -
	 * @param maxWait -
	 * @return boolean -
	 */
	public static boolean waitUntilElementDisappear(WebDriver driver, final WebElement element, int maxWait) {
		final boolean isNotDisplayed;
		if(!waitForElement(driver, element, 0)) {
			return true;
		}

		WebDriverWait wait = new WebDriverWait(driver, maxWait);
		isNotDisplayed = wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver webDriver) {
				boolean isPresent = false;
				try {
					if (element.isDisplayed()) {
						isPresent = false;
						Log.event("Element " + element.toString() + ", is still visible in page");
					}
				} catch (Exception ex) {
					isPresent = true;
					Log.event("Element " + element.toString() + ", is not displayed in page ");
					return isPresent;
				}
				return isPresent;
			}
		});
		return isNotDisplayed;
	}

	/**
	 * To get screen resolution from the config.Property files
	 * @return screen resolution value stored in config file
	 */
	public static String getScreenResolution() {
		String dataToBeReturned = null;

		if (configProperty.hasProperty("screenResolution")) {
			dataToBeReturned = configProperty.getProperty("screenResolution");
		}

		return dataToBeReturned;
	}// getScreenResolution

	public static void setRunPlatForm(String platform)throws Exception {
		SystemProperties.setRunPlatform(platform);
	}
	
	/**
	 * To get run platform from the config.Property files
	 * @return String - return mobile/desktop value
	 */
	public static String getRunPlatForm() {
		return SystemProperties.getCurrentTestPlatform().toString();
	}
	
	/**
	 * To get platform set from System argument
	 * <br> Intended to be used with Jenkins
	 * @return platform - Parsed platform name read from System argument. 
	 * <br> Returns empty String for unconfigured argument
	 */
	public static String getRunPlatformFromSystem() {
		return SystemProperties.getRunPlatform().toString();
	}
	
	/**
	 * To set execution properties for target platform
	 * @param targetDeviceType - "desktop", "table", "mobile"
	 * @param browser - Script method browser argument
	 * @throws Exception
	 */
	public static String setDeviceTypeExecutionProperties(Platform targetDeviceType, String browser) throws Exception {
		if (targetDeviceType.equals(Platform.desktop)) {
			browser = browser.split("\\&")[0];
			System.out.println("[enabled] Desktop Test");
			SystemProperties.setCurrentTestPlatform(targetDeviceType);
			SystemProperties.setUserAgentDeviceTest(false);
			SystemProperties.setDeviceName("desktop");
			BuildProperties.setTestBrowserReset(true);
		} else {
			browser = browser.split("\\&")[0];
			browser += (targetDeviceType.equals(Platform.mobile)) ? "&iphone11_safari_mobile" : "&ipad_safari_tablet";
			System.out.println("[enabled] User-Agent Device-Test");
			SystemProperties.setUserAgentDeviceTest(true);
			SystemProperties.setCurrentTestPlatform(targetDeviceType);
			SystemProperties.setDeviceName(configProperty.get(targetDeviceType.toString() + "Default"));
		}
		BuildProperties.setInitialLoadMainBrand(true);
		return browser;
	}
	
	/**
	 * To convert WebDriver viewport
	 * @param targetDeviceType - "desktop", "table", "mobile"
	 * @param browser - Script method browser argument
	 * @param driver - WebDriver instance
	 * @return WebDriver
	 * @throws Exception
	 */
	public static WebDriver changeUADeviceTypeTo(Platform targetDeviceType, String browser, WebDriver driver) throws Exception{
		browser = setDeviceTypeExecutionProperties(targetDeviceType, browser);
		driver.close();
		driver.quit();
		return WebDriverFactory.get(browser);
	}

	/**
	 * To get current device Platform
	 * @param driver -
	 * @return String -
	 */
	public static String getRunDevicePlatform() {

		String OS_Platform;
		String Device_Platform = Utils.getRunPlatForm();

		if (isTablet() || isMobile()) {
			if (getRunningDeviceName() != null) {
				OS_Platform = getRunningDeviceName().contains("android") ? "android" : "mac";
			} else {
				OS_Platform = System.getProperty(Device_Platform + Utils.getCurrentTestCode());
			}
			Log.event("OS_Platform :: " + OS_Platform);
		} else {
			if (System.getProperty("SELENIUM_DRIVER") != null)
				OS_Platform = System.getProperty("SELENIUM_DRIVER").split("os=")[1].split("&")[0].trim();
			else if (System.getenv("SELENIUM_DRIVER") != null)
				OS_Platform = System.getenv("SELENIUM_DRIVER").split("os=")[1].split("&")[0].trim();
			else
				OS_Platform = System.getProperty("os.name").trim();
		}

		return OS_Platform;
	}

	/**
	 * To get browser name
	 * @param driver - Web Driver 
	 * @return String - Browser Name
	 */
	public static String getRunBrowser(WebDriver driver) {
		String runBrowser = null;
		try {
			if(SystemProperties.getUserAgentDeviceTest() || (!Utils.isDesktop())) {
				return getRunMobileBrowser();
			}
		} catch(NullPointerException ne) {
			Log.warning("runUserAgentDeviceTest not set.", false);
		} catch (Exception e) {
			Log.warning("Error reading platform.", false);
		}

		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		runBrowser = cap.getBrowserName();
		Log.event("Run Browser:: " + runBrowser);

		return runBrowser.toLowerCase();
	}

	/**
	 * To get device name
	 * @return String - device name
	 */
	public static String getRunningDeviceName(){
		return SystemProperties.getDeviceName();
	}
	
	/**
	 * To get mobile browser name
	 * @return String - Browser Name
	 */
	private static String getRunMobileBrowser(){
		String deviceString = SystemProperties.getDeviceName();
		Log.event("getRunMobileBrowser deviceString:: " + deviceString);
		return deviceString.split("\\_")[1];
	}
	
	/**
	 * To get browser version 
	 * @param driver - Web Driver
	 * @return String - Browser version
	 */
	public static String getRunBrowserVersion(WebDriver driver) {
		String dataToBeReturned = null;
		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		dataToBeReturned = cap.getVersion();
		return dataToBeReturned;
	}

	/**
	 * To generate random number from '0 to Maximum Value' or 'Minimum Value - Maximum Value'
	 * @param min - origin bound
	 * @param max - maximum bound
	 * @return Random number between 'min to max' or '0 to max'
	 * @throws Exception - Exception
	 */
	public static int getRandom(int min, int max) throws Exception {
		if(min == max) {
			return min;
		} else {
			if(min > max) {
				int temp;
				temp = min;
				min = max;
				max = temp;
			}
			Random random = new Random();
			int rand;
			if (min == 0)
				rand = random.nextInt(max);
			else
				rand = ThreadLocalRandom.current().nextInt(min, max);
			return rand;
		}
	}

	/**
	 * To get the current brand.
	 * @return text of the brand's name
	 */
	public static Brand getCurrentBrand() {
		return SystemProperties.getCurrentBrand();
	}
	
	/**
	 * To save current brand name to system property 
	 * @throws Exception
	 */
	public static void setUCNavigatedBrand(WebDriver driver) throws Exception {
		SystemProperties.setUCNavigatedBrand(UrlUtils.getBrandFromUrl(driver.getCurrentUrl()));
	}
	
	/**
	 * To get the brand after UC navigation
	 * @return text of the brand's name
	 */
	public static Brand getUCNavigatedBrand() {
		return SystemProperties.getUCNavigatedBrand();
	}
	
	/**
	 * To get current environment where the test cases being executed
	 * @return String environment short form
	 */
	public static String getCurrentEnv() {
		return SystemProperties.getEnvironment();
	}
	
	/**
	 * To get the current brand Short.
	 * 
	 * @return text of the brand's name
	 */
	public static String getCurrentBrandShort() {
		return SystemProperties.getCurrentBrand().toString();
	}
	
	/**
	 * To get XML parameter
	 * @param key
	 * @return Returns parameter value from System if set
	 * <br> Returns parameter value from XML if system property is not set
	 */
	public static String getTestXMLParameter(String key) {
		String parameter;
		if (System.getProperty(key) != null) {
			System.out.println("System property is set for " + key);
			parameter = System.getProperty(key);
		} else {
			System.out.println("System property is not set for " + key + ". Extracting parameter value from test XML.");
			parameter = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter(key);
		}
		System.out.println("Execution test parameter '" + key + "' is:: " + parameter);
		return parameter;
	}
	
	/**
	 * To get config property
	 * @param key
	 * @return Returns property value from System if set
	 * <br> Returns parameter value from config file if system property is not set
	 */
	public static String getExecutionConfiguration(String key) {
		String config;
		if (System.getProperty(key) != null) {
			System.out.println("System property is set for " + key);
			config = System.getProperty(key);
		} else {
			System.out.println("System property is not set for " + key + ". Extracting congig from property.");
			config = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter(key);
		}
		System.out.println("Execution config value '" + key + "' is:: " + config);
		return config;
	}
		
	/**
	 * To get the Name of the Test that is running currently
	 * @return String - Test Name
	 */
	public static String getCurrentTestName() {
		return Reporter.getCurrentTestResult().getName();
	}
	
	/** To get test case ID
	 * @return String- Test case ID
	 */ 
	public static String getTestCaseID() {		
		return getCurrentTestName().split("_")[3];
	}
	
	/**
	 * To get the reward certificate of particular test case
	 * @param number-  number of reward certificate
	 * @return- String
	 */
	public static String getRewardCertificate(int... number) {
		String rewardCertificate = null;
		rewardCertificate = "reward_"+getTestCaseID()+"_"+Utils.getRunPlatForm();
		
		if(number.length >0) {
			rewardCertificate =rewardCertificate +"_"+number[0];
		}		
		return rewardCertificate;
	}
	
	/**
	 * To get the Name of the Test that is running currently
	 * @return int
	 */
	public static int getCurrentTestCode() {
		return Reporter.getCurrentTestResult().hashCode();
	}
    
    /**
     * To get the declared methods in the Test case
     * 
     * @return String of Array
     */
    public static Method[] getDeclaredMethods() {
        Method[] methods = Reporter.getCurrentTestResult().getTestClass().getRealClass().getDeclaredMethods();
        Log.event("Declared Methods:: " + methods.toString());
        return methods ;
    }
    
	/**
	 * To get name of executing XML test
	 * @return XML Test Name
	 */
	public static String getXMLTestName() {
		return Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getName();
	}

	/**
	 * To get window size
	 * @return actual WebDriver window size
	 */
	public static Dimension getWindowSize(WebDriver driver) {
		return driver.manage().window().getSize();
	}

	/**
	 * Get expected page WebElement's location
	 * @param ele - expected WebElement value
	 * @param obj - the page object the element is on
	 * @return String element x and y coordinate in the form "x|y"
	 * @throws Exception - Exception
	 */
	public static String getElementPosition(String ele, Object obj) throws Exception {
		Field f = null;
		try {
			f = obj.getClass().getDeclaredField(ele);
			f.setAccessible(true);
		} catch (NoSuchFieldException | SecurityException e1) {
			throw new Exception("No such a field present on this page, Please check the value:: " + ele);
		}
		WebElement element = null;
		try {
			element = ((WebElement) f.get(obj));
		} catch (IllegalArgumentException | IllegalAccessException e1) {
			Log.exception(e1);
		}
		return element.getLocation().getX() + "|" + element.getLocation().getY();
	}

	/**
	 * To wait until element to be clickable
	 * @param driver -
	 * @param element -
	 * @param maxWait -
	 * @return true - if element is clickable
	 */
	public static boolean waitUntilElementClickable(WebDriver driver, WebElement element, int maxWait) {
		boolean statusOfElementToBeReturned = false;
		long startTime = StopWatch.startTime();
		try {
			WebElement waitElement=(new WebDriverWait(driver, 3).pollingEvery(Duration.ofMillis(500)).ignoring(NoSuchElementException.class)).until(ExpectedConditions.elementToBeClickable(element));
			//WebElement waitElement = wait.until(ExpectedConditions.elementToBeClickable(element));
			if (waitElement!=null) {
				statusOfElementToBeReturned = true;
				Log.event("Element is displayed and disabled:: " + element.toString());
			}

		} catch (Exception ex) {
			statusOfElementToBeReturned = false;
			Log.event("Unable to find disabled element after " + StopWatch.elapsedTime(startTime) + " sec ==> "
					+ element.toString());
		}
		return statusOfElementToBeReturned;
	}
	
	/**
	 * To get mapped webSite URL for current test method name
	 * @return String - webSite URL
	 * @throws Exception - Exception
	 */
	public static String getWebSite()throws Exception {
		return UrlUtils.getWebSite(Utils.getCurrentEnv(), Utils.getCurrentBrand());
	}
	
	/**
	 * Is run device platform is Desktop?
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public static boolean isDesktop()throws Exception{
		return (Utils.getRunPlatForm().equals("desktop"));
	}
	
	/**
	 * Is run device platform is Mobile?
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public static boolean isMobile() {
		return (Utils.getRunPlatForm().equals("mobile"));
	}
	
	/**
	 * Is run device platform is Table?
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public static boolean isTablet() {
		return (Utils.getRunPlatForm().equals("tablet"));
	}
	
	/**
	 * To wait for Ajax to load 
	 * @param WebDriver - driver
	 * @throws Exception - Exception
	 */
	public static void WaitForAjax(WebDriver driver) {
		try {
			boolean ajaxIsComplete = false;
			while (!ajaxIsComplete) // Handle timeout somewhere 
			{
				ajaxIsComplete = (boolean)((JavascriptExecutor) driver).executeScript("return jQuery.active == 0");
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * To get all details of current date as a map
	 * @return Map<String, String> - details of date as map
	 * @throws Exception - Exception
	 */
	public static Map<String, String> getCurrentDateDetails() throws Exception{
		Map<String, String> today = new HashMap<String,String>();
		Calendar now = Calendar.getInstance();
		String dateCurrent = ""+now.get(Calendar.DATE);
		String dayCurrent = ""+now.get(Calendar.DAY_OF_WEEK);
		String weekNoCurrent = ""+now.get(Calendar.WEEK_OF_YEAR);
		String monthCurrent = ""+now.get(Calendar.MONTH);
		String yearCurrent = ""+now.get(Calendar.YEAR);
		today.put("date", dateCurrent);
		today.put("day", dayCurrent);
		today.put("week", weekNoCurrent);
		today.put("month", monthCurrent);
		today.put("year", yearCurrent);
		return today;
	}
	
	/**
	 * To wait until an element is visible
	 * @param driver
	 * @param element - Name of element
	 */
	public static void waitUntilVisible(WebDriver driver, WebElement element) {
		long startTime = StopWatch.startTime();
		WebDriverWait driverWait = new WebDriverWait(driver, Integer.parseInt(configProperty.getProperty("maxImageLoadWait")));
		
		try {
			driverWait.until(new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver input) {
					return (boolean) ((JavascriptExecutor) driver).executeScript("return arguments[0].complete", element);
				};
			});
		} catch(TimeoutException e) {
			Log.reference("Image Load Timout.", driver);
		}
		Log.event("Image Load waited for :: ", StopWatch.elapsedTime(startTime));
	}
	
	/**
	 * To get catalog variations from Test data
	 * @param catalogItem - Test data
	 * @return getCatalogVariationFromTestData - To return the variation as HashMap<String, String>
	 */
	public static HashMap<String, String> getCatalogVariationFromTestData(String catalogItem) {
		String[] cqoItem = catalogItem.split("\\|");
		HashMap<String, String> catalogVariations = new HashMap<String, String>();
		for(int i = 1; i < cqoItem.length; i++) {
			String[] prdVariation = cqoItem[i].split("\\-");
			if(prdVariation[0].equalsIgnoreCase("color")) {
				catalogVariations.put("color", prdVariation[1]);
				continue;
			}
			if(prdVariation[0].equalsIgnoreCase("size")) {
				catalogVariations.put("size", prdVariation[1]);
				continue;
			}
			if(prdVariation[0].equalsIgnoreCase("sizefamily")) {
				catalogVariations.put("sizefamily", prdVariation[1]);
				continue;
			}
			if(prdVariation[0].equalsIgnoreCase("shoewidth")) {
				catalogVariations.put("shoewidth", prdVariation[1]);
				continue;
			}
			if(prdVariation[0].equalsIgnoreCase("shoesize")) {
				catalogVariations.put("shoesize", prdVariation[1]);
				continue;
			}
			if(prdVariation[0].equalsIgnoreCase("bandsize")) {
				catalogVariations.put("bandsize", prdVariation[1]);
				continue;
			}
			if(prdVariation[0].equalsIgnoreCase("cupsize")) {
				catalogVariations.put("cupsize", prdVariation[1]);
				continue;
			}
		}
		return catalogVariations;
	}
}
