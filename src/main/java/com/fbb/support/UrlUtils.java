package com.fbb.support;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.fbb.reusablecomponents.TestData;

public class UrlUtils {
	private static EnvironmentPropertiesReader envProperty = EnvironmentPropertiesReader.getInstance("env");
	private static JSONObject jsonURLs = JsonUtils.fileToJson("./src/main/resources/executionData/sfccURL.json");

	/**
	 * To get mapped webSite URL for current test method name
	 * @param env
	 * @param brand
	 * @return String - webSite URL
	 * @throws Exception - Exception
	 */
	public static String getWebSite(String env, Brand brand){
		String brandShort = brand.toString();
		brandShort = brandShort.length() == 3 ? brandShort : brandShort + 'x';
		JSONObject envURLs = (JSONObject) jsonURLs.get(env);
		String urlFromJson = (String) envURLs.get(brandShort);
		System.out.println(env.toUpperCase() + " - " + brand.toString().toUpperCase() + " URL:: " + urlFromJson);
		Log.event(env.toUpperCase() + " - " + brand.toString().toUpperCase() + " URL:: " + urlFromJson);
		return urlFromJson;
	}

	/**
	 * To get mapped webSite URL for current test method name
	 * @return String - webSite URL
	 * @throws Exception - Exception
	 */
	public static String getProtectedWebSite(String... noAuthURL)throws Exception{
		String url;
		if(noAuthURL.length == 0) {
			url = UrlUtils.getBaseURL(Utils.getWebSite());
		} else {
			url = noAuthURL[0];
		}
		String auth = TestData.envProperty.get("appUserName") + ":" + TestData.envProperty.get("appPassword");
		if (envProperty.getProperty("siteProtection").equals("true") && !Utils.getCurrentEnv().equals("prod")
				&& !url.contains(auth)) {
			url = url.replace("://", "://" + auth + "@");
			if(url.startsWith("http:")) {
				url = url.replace("http:", "https:");
			}
			Log.event("Basic Auth attached URL :: " + url);
		}
		return url;
	}

	/**
	 * To get mapped webSite URL for current test method name
	 * @return String - webSite URL
	 * @throws Exception - Exception
	 */
	public static String getProtectedWebSiteWithYotta(String... regularURL)throws Exception{
		String url;
		if(regularURL.length == 0) {
			url = getProtectedWebSite();
		} else {
			url = getProtectedWebSite(regularURL[0]);
		}
		url = UrlUtils.applyYottaSkip(url);
	    return url;
	}

	/**
	 * To apply skip yotta based on configuration
	 * @param driver - WebDriver instance
	 * @param originalURL - (Optional) URL to modify with Yotta skip.
	 * <br> if no URL given, current URL will be modified.
	 * @return URL modified with yotta skip
	 */
	public static String applyYottaSkip(String originalURL) throws Exception {
		String url = originalURL;
		String skipYotta = envProperty.get("yottaaUrl").trim();
		if (envProperty.get("yottaaEnabled").contains(Utils.getCurrentBrandShort().substring(0, 2))
				&& !Utils.getCurrentEnv().equals("prod") && !originalURL.contains(skipYotta)) {
			url = url + skipYotta;
			Log.event("Yotta skip applied URL:: " + url);
		}
		return url;
	}

	/**
	 * To get brand from url
	 * @param url - current Website URL
	 * @return brand 
	 * @throws Exception
	 */
	public static Brand getBrandFromUrl(String url) throws Exception {
		Brand brand = Utils.getUCNavigatedBrand();
		Log.event("Brand parse URL:: " + url);
		if (url.contains("sfcc")) {
			String brandFromUrl = url.split(Utils.getCurrentEnv() + ".")[1].split(".pl")[0];
			brand = Brand.fromConfiguration(brandFromUrl);
		} else if (url.contains("adobe")) {
			String brandFromUrl = url.split("adobe.")[1].split("\\.")[0];
			brand = Brand.fromValue(brandFromUrl);
			brand = Brand.fromConfiguration(brand.toString() + "x");
		} else {
			String brandFromUrl = url.split("\\.")[1].split("\\.")[0];
			brand = Brand.fromValue(brandFromUrl);
		}
		Log.event("Brand from URL:: " + brand.toString());
		return brand;
	}

	/**
	 * To get product (master/variation) ID from irl
	 * @param url
	 * @return Product ID
	 * @throws Exception
	 */
	public static String getProductIDFromURL(String url) throws Exception {
		if (url.contains("products")) {
			Log.event("Variation product URL detected.");
			url = url.split(".html")[0];
			String[] urlParts = null;
			if (url.contains("%2F")) {
				urlParts = url.split("%2F");
			} else {
				urlParts = url.split("\\/");
			}
			Log.event("Product variation or ID:: " + urlParts[urlParts.length-1]);
			return urlParts[urlParts.length-1];
		} else if(url.contains("cqopid")) {
			Log.event("CQO porduct URL detected.");
			String cqoVariation = UrlUtils.getUrlParatemeterPairs(url).get("cqopid");
			return (cqoVariation == null) ? "" : cqoVariation;
		}
		return "";
	}

	/**
	 * To get domain from a given URL
	 * @param url - String URL to get domain of
	 * @return domain of given url
	 * @throws MalformedURLException
	 */
	public static String getBaseURL(String rawURL) throws MalformedURLException{
		if(rawURL.contains("@")) {
			rawURL = rawURL.split("@")[1];
	    }
		if(!rawURL.startsWith("http") && !rawURL.startsWith("https")){
			rawURL = "https://" + rawURL;
		}
		URL url = new URL(rawURL);
		String protocol = url.getProtocol();
		String host = url.getHost();
		int port = url.getPort(); // if the port is not explicitly specified in the input, it will be -1.
		
		String baseURL = (port == -1) ? String.format("%s://%s", protocol, host)
									: String.format("%s://%s:%d", protocol, host, port);
		Log.event("baseURL:: " + baseURL);
	    return baseURL;
	}

	/**
	 * To get path of a given URL
	 * @param url - String URL to get path of
	 * @return path of given url
	 * @throws MalformedURLException
	 */
	public static String getPath(String rawURL) throws MalformedURLException{
		if(!rawURL.startsWith("http") && !rawURL.startsWith("https")){
			rawURL = "https://" + rawURL;
		}
		URL url = new URL(rawURL);
		String path = url.getPath();
		Log.event("URL path:: " + path);
	    return path;
	}

	/**
	 * To get destination from a redirect URL
	 * @param rawURL
	 * @return Redirect URL parsed from input
	 * @throws Exception
	 */
	public static String getSharedCartRedirectURL(String rawURL) throws Exception {
		String redirectURL = rawURL;
		if(rawURL.contains("shared_session_redirect")) {
			redirectURL = rawURL.split("shared_session_redirect")[1];
			
			if(redirectURL.contains("https") && !redirectURL.startsWith("https")) {
				redirectURL	= "https" + redirectURL.split("https")[1];
			} else if(redirectURL.contains("http") && !redirectURL.startsWith("http")) {
				redirectURL	= "http" + redirectURL.split("http")[1];
			}
		}
		
		Log.event("RAW Redirect URL:: " + redirectURL);
		return UrlUtils.getSanitizedURL(redirectURL);
	}

	/**
	 * To decode URL to more readable format
	 * <br>Changes %3A, %2F to regular characters
	 * @param rawURL
	 * @return decoded URL
	 * @throws Exception
	 */
	public static String getSanitizedURL(String rawURL) throws Exception {
		String sanitizedURL = URLDecoder.decode( rawURL, "UTF-8" );
		Log.event("Sanitized URL:: " + sanitizedURL);
		return sanitizedURL;
	}

	/**
	 * Getting url parameter keys and values as Map
	 * @param url
	 * @throws Exception
	 */
	public static Map<String, String> getUrlParatemeterPairs(String url)  throws Exception{
		Map<String, String> urlQueryPairs = new LinkedHashMap<String, String>();
		if (url.contains("?"))
			url = url.split("\\?")[1];
		List<String> lstKeyValue = Arrays.asList(url.split("&"));
		for (String pair : lstKeyValue) {
			urlQueryPairs.put(pair.split("=")[0], pair.split("=")[1]);
		}
		Log.printMapContent(urlQueryPairs);
		return urlQueryPairs;
	}

	/**
	 * To verify the page url contains the given word
	 * @param driver -
	 * @param hostURL -
	 * @param stringContains -
	 * @return boolean -
	 */
	public static boolean verifyPageURLContains(final WebDriver driver, String hostURL, String stringContains) {
		boolean status = false;
		String url = null;
		try {
			url = driver.getCurrentUrl();
			if (url == null) {
				url = ((JavascriptExecutor) driver).executeScript("return document.URL;").toString();
			}
		} catch (Exception e) {
			url = ((JavascriptExecutor) driver).executeScript("return document.URL;").toString();
		}
		if (url.contains(hostURL.split("https://")[1]) && url.contains(stringContains)) {
			status = true;
		}
	
		return status;
	}

}

