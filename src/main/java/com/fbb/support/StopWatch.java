package com.fbb.support;

/**
 * Stopwatch the elapsed/start time of the stop watch
 */
public class StopWatch {

	/**
	 * Returns the start time of the stop watch
	 * @return time in seconds
	 */
	public static long startTime() {
		return System.currentTimeMillis();
	}

	/**
	 * Returns the elapsed time of the stop watch.
	 * @param startTime - start time
	 * @return elapsed time in seconds
	 */
	public static long elapsedTime(long startTime) {
		return (long) (System.currentTimeMillis() - startTime) / 1000;
	}
	
	/**
	 * Returns elapsed time in easily readable format
	 * @param elapsedTime - Elapsed time in second
	 * @return Elapsed time formatted in hour, minute, and second
	 */
	public static String elapsedTimeFormatted(long elapsedTime) {
		if (elapsedTime < 60) {
			return elapsedTime + " second";
		} else if (elapsedTime < 3600) {
			long minutes = elapsedTime / 60;
			return minutes + " minute, " + elapsedTimeFormatted(elapsedTime % 60);
		} else {
			long hours = elapsedTime / 3600;
			return hours + " hour, " + elapsedTimeFormatted(elapsedTime % 3600);
		}
	}
}
