package com.fbb.support;

import java.awt.Color;

public class ColorUtils {

	static class ColorLab {
		float L, a, b;

		ColorLab(float L, float a, float b) {
			this.L = L;
			this.a = a;
			this.b = b;
		}
	}
	
	/**
	 * To get standard Color object from CSS color code
	 * @param cssColorCode - Color code string from CSS
	 * @return cssColorCode converted to Color
	 */
	public static Color getColorRGB(String cssColorCode) {
		Color colorRGB;
		if (cssColorCode.contains("rgb")) {
			String[] colorValue = cssColorCode.substring(cssColorCode.indexOf('('), cssColorCode.indexOf(')')).split(",");
			int red = StringUtils.getNumberInString(colorValue[0]);
			int green = StringUtils.getNumberInString(colorValue[1]);
			int blue = StringUtils.getNumberInString(colorValue[2]);
			colorRGB = new Color(red, green, blue);
		} else if (cssColorCode.contains("#") || cssColorCode.length() <= 6) {
			cssColorCode = cssColorCode.startsWith("#") ? cssColorCode : "#" + cssColorCode;
			colorRGB = Color.decode(cssColorCode);
		} else if (cssColorCode.replaceAll(" ", "").length() <= 11) {
			return getColorRGB("rgb(" + cssColorCode + ")");
		} else {
			Log.event(cssColorCode + " did not match with configured CSS color code formats.");
			return null;
		}
		Log.event("Color in RGB space:: " + colorRGB.toString());
		return colorRGB;
	}
	
	public static ColorLab getColorLAB(String cssColorCode) {
		return convertRGBtoLAB(getColorRGB(cssColorCode));
	}

	/**
	 * To get DeltaE-1976 value between two colors
	 * @param rgb1
	 * @param rgb2
	 * @return Delta E value
	 */
	@SuppressWarnings("unused")
	private static Double getColorDeltaE76(Color rgb1, Color rgb2) {
		if (rgb1 == null || rgb2 == null) {
			Log.event("Passed value is not a color.");
			return 0d;
		} else {
			int dRed = (rgb1.getRed() - rgb2.getRed());
			int dGreen = (rgb1.getGreen() - rgb2.getGreen());
			int dBlue = (rgb1.getBlue() - rgb2.getBlue());
			return Math.sqrt(Math.pow(dRed, 2) + Math.pow(dGreen, 2) + Math.pow(dBlue, 2));
		}
	}
	
	/**
	 * To get DeltaE-1994 value between two colors
	 * @param lab1
	 * @param lab2
	 * @return Delta E value
	 */
	private static double getColorDeltaE94(ColorLab lab1, ColorLab lab2) {
		if (lab1 == null || lab2 == null) {
			Log.event("Passed value is not a color.");
			return 0d;
		} else {
			//Graphic Art
			var Kl = 1.0;
			var K1 = .045;
			var K2 = .015;

			var deltaL = lab1.L - lab2.L;
			var deltaA = lab1.a - lab2.a;
			var deltaB = lab1.b - lab2.b;

			var c1 = Math.sqrt(Math.pow(lab1.a, 2) + Math.pow(lab1.b, 2));
			var c2 = Math.sqrt(Math.pow(lab2.a, 2) + Math.pow(lab2.b, 2));
			var deltaC = c1 - c2;

			var deltaH = Math.pow(deltaA, 2) + Math.pow(deltaB, 2) - Math.pow(deltaC, 2);
			deltaH = deltaH < 0 ? 0 : Math.sqrt(deltaH);

			var sl = 1.0;
			var kc = 1.0;
			var kh = 1.0;

			var sc = 1.0 + K1 * c1;
			var sh = 1.0 + K2 * c1;

			var i = Math.pow(deltaL / (Kl * sl), 2) + Math.pow(deltaC / (kc * sc), 2) + Math.pow(deltaH / (kh * sh), 2);
			var deltaE94 = i < 0 ? 0 : Math.sqrt(i);
			Log.event("CIE dE94:: " + deltaE94);
			return (double) deltaE94;
		}
	}
	
	/**
	 * To get DeltaE value between two colors
	 * @param color1 in RGB or Hex
	 * @param color2 in RGB or Hex
	 * @return Delta E value
	 */
	public static Double getColorDeltaE(String color1, String color2) {
		return getColorDeltaE94(getColorLAB(color1), getColorLAB(color2));
	}
	
	private static ColorLab convertRGBtoLAB(Color colorRGB) {
		if (colorRGB == null)
			return null;
		
		float fx, fy, fz;
		float eps = 216.f / 24389.f;
		float k = 24389.f / 27.f;

		float Xr = 0.964221f; // reference white D50
		float Yr = 1.0f;
		float Zr = 0.825211f;

		// RGB to XYZ
		float r = colorRGB.getRed() / 255.f; // R 0..1
		float g = colorRGB.getGreen() / 255.f; // G 0..1
		float b = colorRGB.getBlue() / 255.f; // B 0..1

		// assuming sRGB (D65)
		if (r <= 0.04045)
			r = r / 12;
		else
			r = (float) Math.pow((r + 0.055) / 1.055, 2.4);

		if (g <= 0.04045)
			g = g / 12;
		else
			g = (float) Math.pow((g + 0.055) / 1.055, 2.4);

		if (b <= 0.04045)
			b = b / 12;
		else
			b = (float) Math.pow((b + 0.055) / 1.055, 2.4);

		float X = 0.436052025f * r + 0.385081593f * g + 0.143087414f * b;
		float Y = 0.222491598f * r + 0.71688606f * g + 0.060621486f * b;
		float Z = 0.013929122f * r + 0.097097002f * g + 0.71418547f * b;

		// XYZ to Lab
		float xr = X / Xr;
		float yr = Y / Yr;
		float zr = Z / Zr;

		if (xr > eps)
			fx = (float) Math.pow(xr, 1 / 3.);
		else
			fx = (float) ((k * xr + 16.) / 116.);

		if (yr > eps)
			fy = (float) Math.pow(yr, 1 / 3.);
		else
			fy = (float) ((k * yr + 16.) / 116.);

		if (zr > eps)
			fz = (float) Math.pow(zr, 1 / 3.);
		else
			fz = (float) ((k * zr + 16.) / 116);

		float Ls = (116 * fy) - 16;
		float as = 500 * (fx - fy);
		float bs = 200 * (fy - fz);

		ColorLab lab = new ColorLab((int) (2.55 * Ls + .5), (int) (as + .5), (int) (bs + .5));
		Log.event("Color in Lab space:: L:" + lab.L + ", a:" + lab.a + ", b:" + lab.b);
		return lab;
	}
}
