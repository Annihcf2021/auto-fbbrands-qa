package com.fbb.support;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CollectionUtils {

	public static boolean compareTwoArraysAreEqualBySorting(String[] Array1, String[] Array2) throws Exception {
		Arrays.sort(Array1);
		Arrays.sort(Array2);
		for (int intProd = 0; intProd < Array1.length; intProd++) {
			if (!(Array1[intProd].equals(Array2[intProd]))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * To compare two maps are equal/same
	 * @param map1
	 * @param map2
	 * @return If given maps satisfy requirement
	 */
	public static <K, V> boolean verifyMapsEqual(Map<K, V> map1, Map<K, V> map2) throws Exception {
		if(map1.entrySet().equals(map2.entrySet())) {
			return true;
		} else {
			Log.printMapContent(map1);
			Log.printMapContent(map2);
			if(map2.size() > map1.size()) {
				CollectionUtils.verifyMapContainsMap(map2, map1);
			} else {
				CollectionUtils.verifyMapContainsMap(map1, map2);
			} 
			return false;
		}
	}

	/**
	 * To verify that all key,value pair of smaller map is present in bigger map
	 * @param mapBigger
	 * @param mapSmaller
	 * @return true if bigger map contains smaller map
	 * @throws Exception 
	 */
	public static <K, V> boolean verifyMapContainsMap(Map<K, V> mapBigger, Map<K, V> mapSmaller) throws Exception {
		Map<K, V> missedORmissmatched = new HashMap<K, V>();
		try {
			for (Entry<K, V> entry : mapSmaller.entrySet()) {
			    if(mapBigger.get(entry.getKey()) == null 
	    		|| (mapBigger.get(entry.getKey()) != null && !mapBigger.get(entry.getKey()).equals(entry.getValue()))) {
			    	missedORmissmatched.put(entry.getKey(), entry.getValue());
			    }
			}
		} catch (ClassCastException cce) {
			Log.event(cce.getMessage());
		}
		
		if(missedORmissmatched.size() != 0) {
			Log.printMapContent(missedORmissmatched);
		}
		return missedORmissmatched.size() == 0;
	}

	/**
	 * To compare two HashMap values,then print unique list value and print missed list value
	 * @param expectedList - expected element list
	 * @param actualList - actual element list
	 * @return statusToBeReturned - returns true if both the lists are equal, else returns false
	 */
	public static boolean compareTwoHashMap(Map<String, String> expectedList, Map<String, String> actualList) {
		List<String> missedkey = new ArrayList<String>();
		HashMap<String, String> missedvalue = new HashMap<String, String>();
		try {
			for (String k : expectedList.keySet()) {
				if (!(actualList.get(k).equalsIgnoreCase(expectedList.get(k)))) {
					missedvalue.put(k, actualList.get(k));
					Log.failsoft("Missed Values:: Expected List:: " + missedvalue);
					return false;
				}
			}
			for (String y : actualList.keySet()) {
				if (!expectedList.containsKey(y)) {
					missedkey.add(y);
					Log.failsoft("Missed keys:: Actual List:: " + missedkey);
					return false;
				}
			}
		} catch (NullPointerException np) {
			return false;
		}
		return true;
	}

	/**
	 * To compare two HashMap values,then print unique list value and print missed list value
	 * @param expectedList - expected element list
	 * @param actualList - actual element list
	 * @return statusToBeReturned - returns true if both the lists are equal, else returns false
	 */
	public static boolean compareTwoHashMapNotEqual(Map<String, String> expectedList, Map<String, String> actualList) {
		List<String> expectedKey = new ArrayList<>(expectedList.keySet());
		List<String> actualKey = new ArrayList<>(actualList.keySet());
		boolean state = false;
		try {
			if(!expectedKey.equals(actualKey))
				return true;
	
			for(String key : expectedList.keySet()){
				if(!expectedList.get(key).equals(actualList.get(key)))
					return true;
			}
	
		} catch (NullPointerException np) {
			state = false;
		}
		return state;
	}

	/**
	 * To compare two hashmaps
	 * @param expectedList -
	 * @param actualList -
	 * @param onlyThis -
	 * @return true - if both hashmaps are equal
	 */
	@SafeVarargs
	public static boolean compareTwoHashMap(Map<String, String> expectedList, Map<String, String> actualList,
			List<String>... onlyThis) {
		List<String> missedkey = new ArrayList<String>();
		HashMap<String, String> missedvalue = new HashMap<String, String>();
		try {
			for (String k : expectedList.keySet()) {
				if (onlyThis[0].contains(k)) {
					if (!(actualList.get(k).equalsIgnoreCase(expectedList.get(k)))) {
						missedvalue.put(k, actualList.get(k));
						Log.failsoft("Missed Values:: Expected List:: " + missedvalue);
						return false;
					}
				}
			}
			for (String y : actualList.keySet()) {
				if (onlyThis[0].contains(y)) {
					if (!expectedList.containsKey(y)) {
						missedkey.add(y);
						Log.failsoft("Missed keys:: Actual List:: " + missedkey);
						return false;
					}
				}
			}
		} catch (NullPointerException np) {
			return false;
		}
		return true;
	}

	/**
	 * To compare two array list values,then print unique list value and print
	 * @param List<String> - List of element to verify
	 * @param List<String> - List of actual elements
	 * @param disableMissedLog - if missing elements are logged
	 * @return boolean - true if list completely conatains list
	 */
	public static boolean compareTwoList(List<String> expectedElements, List<String> actualElements, boolean... disableMissedLog) {
		boolean statusToBeReturned = false;
		List<String> uniqueList = new ArrayList<String>();
		List<String> missedList = new ArrayList<String>();
		for (String item : expectedElements) {
			if (actualElements.contains(item)) {
				uniqueList.add(item);
			} else {
				missedList.add(item);
			}
		}
		
		Collections.sort(expectedElements); Collections.sort(actualElements);
		Log.event("Expected elements: " + expectedElements.toString());
		Log.event("Actual elements: " + actualElements.toString());
		
		if (expectedElements.equals(actualElements)) {
			Log.event("All elements checked on this page:: " + uniqueList);
			statusToBeReturned = true;
		} else {
			if(disableMissedLog.length == 0 || (disableMissedLog.length > 0 ? disableMissedLog[0] == false : false))
				Log.event("Missing element on this page:: " + missedList);
			statusToBeReturned = false;
		}
		return statusToBeReturned;
	}

	/**
	 * To compare two list of strings
	 * @param List<String> - List of element to verify
	 * @param List<String> - List of actual elements
	 * @return boolean - if elements are missing
	 */
	public static boolean compareTwoList1(List<String> expectedElements, List<String> actualElements) {
		boolean statusToBeReturned = true;
		List<String> missedList = new ArrayList<String>();
		for (int i = 0; i < expectedElements.size(); i++) {
			if (!(expectedElements.get(i).equals(actualElements.get(i)))) {
				statusToBeReturned = false;
				missedList.add(expectedElements.get(i));
			}
		}
		if(statusToBeReturned == false)
			Log.failsoft("Missing element on this page:: " + missedList);
		return statusToBeReturned;
	}

	/**
	 * To verify actual list contains expected list values
	 * @param expectedElements - List of actual elements
	 * @param actualElements - List of element to verify
	 * @return true/false
	 */
	public static boolean listContainsList(List<String> expectedElements, List<String> actualElements) {
		boolean allElementsFound = true;
		List<String> missedList = new ArrayList<String>();
		expectedElements.replaceAll(String::toUpperCase);
		actualElements.replaceAll(String::toUpperCase);
		
		for(String element : expectedElements) {
			if(!actualElements.contains(element)){
				allElementsFound = false;
				missedList.add(element);
			}
		}
		
		Log.event("Expected elements: " + expectedElements.toString());
		Log.event("Actual elements: " + actualElements.toString());
		if(!allElementsFound) {
			Log.event(missedList.size() + " elements not found.\nMissing list elements:: " + Arrays.toString(missedList.toArray()));
			return false;
		} else
			return true;
	}

	/**
	 * copyHashMap
	 * @param actual -
	 * @param ignore -
	 * @return LinkedList -
	 * @throws Exception - Exception
	 */
	public static LinkedHashMap<String, String> copyHashMap(LinkedHashMap<String, String> actual, String ignore) throws Exception {
		List<String> indexes = new ArrayList<String>(actual.keySet());
		LinkedHashMap<String, String> expected = new LinkedHashMap<String, String>();
	
		for (int i = 0; i < indexes.size(); i++) {
			if (!indexes.get(i).equals(ignore))
				expected.put(indexes.get(i), actual.get(indexes.get(i)));
		}
	
		return expected;
	}

	/**
	 * copyLinkedListHashMap
	 * @param actual -
	 * @param ignore  -
	 * @return LinkedList - 
	 * @throws Exception - Exception
	 */
	public static LinkedList<LinkedHashMap<String, String>> copyLinkedListHashMap(
			LinkedList<LinkedHashMap<String, String>> actual, String ignore) throws Exception {
		int size = actual.size();
		LinkedList<LinkedHashMap<String, String>> expected = new LinkedList<LinkedHashMap<String, String>>();
		for (int j = 0; j < size; j++) {
			List<String> indexes = new ArrayList<String>(actual.get(j).keySet());
			LinkedHashMap<String, String> hashMap = new LinkedHashMap<String, String>();
			for (int i = 0; i < indexes.size(); i++) {
				if (!indexes.get(i).equals(ignore))
					hashMap.put(indexes.get(i), actual.get(j).get(indexes.get(i)));
			}
			expected.add(hashMap);
		}
		return expected;
	}

	/**
	 * To compare print hash map as Table
	 * @param title - Table title
	 * @param col1Head - column-1 header
	 * @param col2Head - column-2 header
	 * @param hashMap1 - hashMap-1
	 * @param hashMap2 - hashMap-2
	 * @param noNeed - Ignore list
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public static boolean compareAndPrintTableHashMap(String title, String col1Head, String col2Head,
			LinkedHashMap<String, String> hashMap1, LinkedHashMap<String, String> hashMap2, String[]... noNeed) throws Exception {
	
		if (noNeed.length > 0) {
			for (int i = 0; i < noNeed[0].length; i++) {
				System.out.println("Removed Key from <b>Expected:: " + noNeed[0][i]);
				hashMap1.remove(noNeed[0][i]);
				System.out.println("Removed Key from Actual  :: " + noNeed[0][i]);
				hashMap2.remove(noNeed[0][i]);
			}
		}
	
		Log.message("<br>");
		Log.message1("<table><tr bgcolor='#BBBBBB'><td colspan=3><b><font color='black'>"
				+ title + "</font></b></td></tr>");
		Log.message1("<tr align='center' bgcolor='#BBBBBB' style='color:black;'><td>Contents</td><td>"
				+ col1Head + "</td><td>" + col2Head + "</td></tr>");
		List<String> indexes1 = new ArrayList<String>(hashMap1.keySet());
		List<String> indexes2 = new ArrayList<String>(hashMap2.keySet());
		List<String> maxIndex = indexes1.size() > indexes2.size() ? indexes1 : indexes2;
	
		for (int i = 0; i < maxIndex.size(); i++) {
			String value1 = hashMap1.containsKey(maxIndex.get(i)) ? hashMap1.get(maxIndex.get(i)) : "No Value";
			String value2 = hashMap2.containsKey(maxIndex.get(i)) ? hashMap2.get(maxIndex.get(i)) : "No Value";
					if (value1.equalsIgnoreCase(value2))
						Log.message1("<tr><td bgcolor='#BBBBBB' style='color:black;'>"
								+ maxIndex.get(i) + "</td><td>" + value1 + "</td><td>"
								+ value2 + "</td></tr>");
					else
						Log.message1("<tr><td bgcolor='#BBBBBB' style='color:black;'>"
								+ maxIndex.get(i) + "</td><td bgcolor='red'>" + value1
								+ "</td><td>" + value2 + "</td></tr>");
		}
		Log.message1("</table>");
	
		return compareTwoHashMap(hashMap1, hashMap2);
	}

	/**
	 * To compare and print linked list of HashMap in table format
	 * <br> No color matched, yellow for mismatched case, red for unmatched value
	 * @param title - Table title
	 * @param col1Head - column-1 header
	 * @param col2Head - column-2 header
	 * @param hashMap1 - hashMap-1
	 * @param hashMap2 - hashMap-2
	 * @param noNeed - Ignore list
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public static boolean compareAndPrintTableLinkedListHashMap(String title, String col1Head, String col2Head,
			LinkedList<LinkedHashMap<String, String>> hashMap1, LinkedList<LinkedHashMap<String, String>> hashMap2, String[]... noNeed) throws Exception {
		
		// int iteration1 = hashMap1.size();
		int iteration2 = hashMap2.size();
		boolean flag = true;
		for (int j = 0; j < iteration2; j++) {
			if (noNeed.length > 0) {
				for (int i = 0; i < noNeed[0].length; i++) {
					System.out.println("Removed Key from Expected:: " + noNeed[0][i]);
					hashMap1.get(j).remove(noNeed[0][i]);
					System.out.println("Removed Key from Actual  :: " + noNeed[0][i]);
					hashMap2.get(j).remove(noNeed[0][i]);
				}
			}
	
			Log.message("<br>");
			Log.message1("<table><tr bgcolor='#BBBBBB' align='center'><td colspan=3><b><font color='black'>"
					+ title + "(" + (j + 1) + ")</font></b></td></tr>");
			Log.message1("<tr align='center' bgcolor='#BBBBBB' style='color:black;'><td>Contents</td><td>"
					+ col1Head + "</td><td>" + col2Head + "</td></tr>");
			List<String> indexes1 = new ArrayList<String>(hashMap1.get(j).keySet());
			List<String> indexes2 = new ArrayList<String>(hashMap2.get(j).keySet());
			List<String> maxIndex = indexes1.size() > indexes2.size() ? indexes1 : indexes2;
			for (int i = 0; i < maxIndex.size(); i++) {
				String value1 = hashMap1.get(j).containsKey(maxIndex.get(i)) ? hashMap1.get(j).get(maxIndex.get(i)) : "<font color='red'>No Value</font>";
				String value2 = hashMap2.get(j).containsKey(maxIndex.get(i)) ? hashMap2.get(j).get(maxIndex.get(i)) : "<font color='red'>No Value</font>";
				if (value1.equals(value2)) {
					Log.message1("<tr>"
							+ "<td bgcolor='#BBBBBB' style='color:black;'>" + maxIndex.get(i)+ "</td>"
							+ "<td>" + value1 + "</td>"
							+ "<td>" + value2 + "</td></tr>");
				} else if (value1.equalsIgnoreCase(value2)) {
					Log.message1("<tr>"
							+ "<td bgcolor='#BBBBBB' style='color:black;'>" + maxIndex.get(i) + "</td>" 
							+ "<td bgcolor='yellow'>" + value1 + "</td>"
							+ "<td>" + value2 + "</td></tr>");
				} else {
					Log.message1("<tr>"
							+ "<td bgcolor='#BBBBBB' style='color:black;'>" + maxIndex.get(i) + "</td>" 
							+ "<td bgcolor='red'>" + value1  + "</td>"
							+ "<td>" + value2 + "</td></tr>");
				}
			}
			Log.message1("</table>");
	
			flag = compareTwoHashMap(hashMap1.get(j), hashMap2.get(j));
		}
	
		return flag;
	}

	/**
	 To compare and print hashMap of hashMap in table format
	 * @param title - Table title
	 * @param col1Head - column-1 header
	 * @param col2Head - column-2 header
	 * @param hashMap1 - hashMap-1
	 * @param hashMap2 - hashMap-2
	 * @return boolean - true/false
	 * @throws Exception - Exception
	 */
	public static boolean compareLinkedHashMapOfHashMap(String title, String col1Head, String col2Head,
			LinkedHashMap<String, LinkedHashMap<String, String>> hashMap1,
			LinkedHashMap<String, LinkedHashMap<String, String>> hashMap2) throws Exception {
		boolean flag = true;
		
		List<String> expPrdNames = new ArrayList<String>(hashMap1.keySet());
		List<String> actPrdNames = new ArrayList<String>(hashMap2.keySet());
		
		Collections.sort(expPrdNames, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Collator.getInstance().compare(o1, o2);
			}
		});
		
		Collections.sort(actPrdNames, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Collator.getInstance().compare(o1, o2);
			}
		});
		
		if (expPrdNames.equals(actPrdNames)) {
			for(String key : expPrdNames){
				if(!compareAndPrintTableHashMap(title, col1Head, col2Head, hashMap1.get(key), hashMap2.get(key)))
					flag=false;
			}
		} else
			return false;
		
		return flag;
	}

	/**
	 * To alternate merge two lists
	 * @param List<E> - First list
	 * @param List<E> - Second list
	 * @return List<E> - Merged list
	 * @throws Exception - Exception
	 */
	public static <E>List<E> listAlternateMerge(List<E> list1, List<E> list2) throws Exception{
		Iterator<E> itLst1 = list1.iterator();
		Iterator<E> itLst2 = list2.iterator();
		List<E> mergedList = new ArrayList<E>();
		
		while(itLst1.hasNext() || itLst2.hasNext()) {
			if(itLst1.hasNext()) mergedList.add(itLst1.next());
			if(itLst2.hasNext()) mergedList.add(itLst2.next());
		}
		return mergedList;
	}

}
