package com.fbb.support;

import java.lang.reflect.Field;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.fbb.pages.HomePage;
import com.fbb.reusablecomponents.Enumerations.Direction;
import com.fbb.reusablecomponents.Enumerations.ElementDimension;

/**
 * Wrapper for Selenium WebDriver actions which will be performed on browser
 * 
 * Wrappers are provided with exception handling which throws Skip Exception on
 * occurrence of NoSuchElementException
 * 
 */

public class BrowserActions {
	/**
	 * Wrapper to type a text in browser text field
	 * 
	 * @param txt : WebElement of the Text Field
	 * @param txtToType : Text to type [String]
	 * @param driver : WebDriver Instances
	 * @param elementDescription : Description about the WebElement
	 * @return String -
	 * @throws Exception - Exception
	 */
	public static String typeOnTextField(
			WebElement txtField, String txtToType, WebDriver driver, String elementDescription) throws Exception {
		try {
			clearTextField(txtField, elementDescription);
			Log.event("Cleared text field.");
			txtField.sendKeys(txtToType);
			txtField.sendKeys(Keys.TAB);
			Log.event("Typed text '" + txtToType + "'" + " into " + elementDescription);
		}catch(NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}
		return txtToType;
	}// typeOnTextField

	/**
	 * Wrapper to type a text in browser text field
	 * @param txt : WebElement of the Text Field
	 * @param txtToType : Text to type [String]
	 * @param driver : WebDriver Instances
	 * @param elementDescription : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static void updateOnTextField(
			WebElement txt, String txtToType, WebDriver driver, String elementDescription) throws Exception {
		try {
			txt.click();
			txt.sendKeys(txtToType);
		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}

	}// typeOnTextField


	/**
	 * To clear text from text field
	 * @param field - Web Element for text field
	 * @param elementDescription - Web Element description
	 * @throws Exception - Exception
	 */
	public static void clearTextField(WebElement field, String elementDescription) throws Exception{
		try{
			field.clear();
		} catch(NoSuchElementException e){
			Log.fail(elementDescription + " not found in page / Locator change detected.");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Wrapper to type a text in browser text field
	 * 
	 * @param txt : String Input (CSS Locator)
	 * @param txtToType : Text to type [String]
	 * @param driver : WebDriver Instances
	 * @param elementDescription : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static void typeOnTextField(
			String txt, String txtToType, WebDriver driver, String elementDescription) throws Exception {

		WebElement element = checkLocator(driver, txt);
		try {
			clearTextField(element, elementDescription);
			element.click();
			element.sendKeys(txtToType);
			element.sendKeys(Keys.TAB);
			Log.event("Typed(" + txtToType + ") in " + elementDescription);
		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		} catch (WebDriverException e){
			element.sendKeys(txtToType);
		}

	}// typeOnTextField
	
	/**
	 * To scroll into particular element
	 * 
	 * @param driver -
	 * @param element - the element to scroll to
	 * @throws InterruptedException -
	 */
	public static void scrollToView(WebElement element, WebDriver driver) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
	}

	/**
	 * To scroll to given element in page
	 * @param element - Web Element
	 * @param driver - Web Driver
	 * @throws InterruptedException - Exception
	 */
	public static void scrollInToView(WebElement element, WebDriver driver) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
	}

	/**
	 * To scroll to top of the page
	 * @param driver - Web Driver
	 * @throws InterruptedException - Exception
	 */
	public static void scrollToTopOfPage(WebDriver driver) throws InterruptedException {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(document.body.scrollHeight,0)");
	}

	/**
	 * To scroll to top of the page
	 * @param driver - Web Driver
	 * @throws InterruptedException - Exception
	 */
	public static void scrollUp(WebDriver driver) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0, -100)", "");
	}

	/**
	 * To scroll to bottom of the page
	 * @param driver - Web Driver
	 * @throws InterruptedException - Exception
	 */
	public static void scrollToBottomOfPage(WebDriver driver) throws InterruptedException {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	/**
	 * To scroll into particular element
	 * 
	 * @param driver -
	 * @param element - the element to scroll to
	 */
	public static void scrollToViewElement(WebElement element, WebDriver driver) {
		try {
			String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
					+ "var elementTop = arguments[0].getBoundingClientRect().top;"
					+ "window.scrollBy(0, elementTop-(viewPortHeight/2));";
			((JavascriptExecutor) driver).executeScript(scrollElementIntoMiddle, element);
			Log.event("Scrolled element to view.");
		} catch (NoSuchElementException ex) {
			Log.event("Element not found in page / Locator change detected.");
		}
	}
	
	/**
	 * To click on PDP Swatch elements
	 * @param Family, color or size swatch element
	 * @param driver
	 * @param element description
	 * @throws Exception
	 */
	public static void clickOnSwatchElement(WebElement btn, WebDriver driver, String elementDescription) throws Exception{
		if(Utils.isDesktop()) {
			clickOnElementX(btn, driver, elementDescription);
		}else {
			clickByPressEnter(btn, driver, elementDescription);
		}
	}

	/**
	 * To click on element
	 * @param btn - button
	 * @param driver - driver
	 * @param elementDescription - element description
	 * @param scroll - whether framework will attempt to scroll bring element into viewport
	 * @throws Exception - Exception
	 */
	public static void clickOnElementX(WebElement btn, WebDriver driver, String elementDescription, boolean... scroll) throws Exception {
		try {
			if (scroll.length == 0 || scroll[0]) {
				BrowserActions.scrollToViewElement(btn, driver);
			}
			String browser = Utils.getRunBrowser(driver);
			if((browser.equals("chrome")) && (Utils.isDesktop() || Utils.isTablet()))
				chromeClick(btn, driver, elementDescription);
			else if(browser.equals("firefox")){
				chromeClick(btn, driver, elementDescription);
			}else if(browser.equals("safari")){
				safariClick(btn, driver, elementDescription);
			}else 
				javascriptClick(btn, driver, elementDescription);
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException(elementDescription + " button/link not found in page!!");
		} catch(ElementNotVisibleException e2){
			javascriptClick(btn, driver, elementDescription);
		} catch (WebDriverException e1){
			if(e1.getMessage().contains("Other element would receive the click"))
				javascriptClick(btn, driver, elementDescription);
			else
				throw e1;
		} 
		Log.event("Clicked on " + elementDescription);
	}// clickOnButton

	/**
	 * To click on an element from script
	 * @param driver - driver
	 * @param element - element
	 * @param obj - page object where element instance is created in
	 * @throws Exception - Exception 
	 */
	public static void clickOnElementX(WebDriver driver, String element, Object obj, String elementDescription)throws Exception{
		try {
			Field f = obj.getClass().getDeclaredField(element);
			f.setAccessible(true);
			WebElement webElement = ((WebElement) f.get(obj));
			clickOnElementX(webElement, driver, elementDescription);
			Utils.waitForPageLoad(driver);
		}catch(NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}catch(NoSuchFieldException e2) {
			Log.fail(elementDescription + " not defined in page object.", driver);
		}
	}
	
	/**
	 * Dedicated method for web element click
	 * @param element - 
	 * @param elementDescription - 
	 * @throws Exception - 
	 */
	public static void chromeClick(WebElement element, WebDriver driver, String elementDescription)throws Exception{
		try {
			element.click();
		}catch(NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}
	}

	/**
	 * Dedicated method for Safari click
	 * @param element - 
	 * @param driver -
	 * @param elementDescription -
	 * @throws Exception -
	 */
	public static void safariClick(WebElement element, WebDriver driver, String elementDescription)throws Exception{
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	/**
	 * Wrapper to click on button/text/radio/checkbox in browser
	 * 
	 * @param btn : String Input (CSS Locator) [of the Button Field]
	 * @param driver : WebDriver Instances
	 * @param elementDescription : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static void clickOnElement(String btn, WebDriver driver, String elementDescription) throws Exception {
		WebElement element = checkLocator(driver, btn);
		try {
			element.click();
			Log.event("Clicked on " + elementDescription);
		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}
	}// clickOnButton

	/**
	 * Wrapper to click on button/text/radio/checkbox in browser
	 * @param btn : String Input (CSS Locator) [of the Button Field]
	 * @param driver : WebDriver Instances
	 * @param elementDescription : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static void clickOnElement(WebElement btn, WebDriver driver, String elementDescription) throws Exception {
		try {
			scrollToViewElement(btn, driver);
			btn.click();
			Log.event("Clicked on " + elementDescription);
		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}

	}// clickOnButton

	/**
	 * To click on given element suing actions
	 * @param element -
	 * @param driver -
	 * @param elementDescription -
	 * @throws Exception - Exception
	 */
	public static void actionClick(WebElement element, WebDriver driver, String elementDescription) throws Exception {
		try {
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click(element).build().perform();
		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}
	}
	
	/**
	 * To click on given element using Enter key
	 * @param ele - element
	 * @param driver - driver
	 * @param description - Description
	 * @throws Exception - Exception
	 */
	public static void clickByPressEnter(WebElement ele, WebDriver driver, String description) throws Exception{
		BrowserActions.scrollToViewElement(ele, driver);
		ele.sendKeys(Keys.ENTER);
		Log.event("Clicked on " + description);
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To click and hold the click on given element
	 * @param WebElement - element
	 * @param WebDriver - driver
	 * @param String - description of element
	 * @throws Exception - Exception
	 */
	public static void clickAndHoldOnElement(WebElement element, WebDriver driver, String elementDescription) throws Exception {
		try {
			Actions actions = new Actions(driver);
			actions.clickAndHold(element).build().perform();
		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}
	}
	
	/**
	 * To drag and element up or down
	 * @param WebElement - element
	 * @param WebDriver - driver
	 * @String elementDescription - description of element to be dragged
	 * @String direction - direction of dragging
	 */
	public static void dragElement(WebElement element, WebDriver driver, String elementDescription, Direction direction) throws Exception{
		try {
			Actions actions = new Actions(driver);
			switch(direction) {
			case Up:
				actions.dragAndDropBy(element, 0, -500).build().perform();
				Log.event("Dragged element up");
				break;
			case Down:
				actions.dragAndDropBy(element, 0, 200).build().perform();
				Log.event("Dragged element down");
				break;
			case Left:
				actions.dragAndDropBy(element, -200, 0).build().perform();
				Log.event("Dragged element left");
				break;
			case Right:
				actions.dragAndDropBy(element, 500, 0).build().perform();
				Log.event("Dragged element right");
				break;
			default:
				Log.event("Direction not defined. Please check script.");
				break;
			}
		}catch(NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}
	}

	/**
	 * To click on an element using Javascript
	 * @param element - WebElement which has to be click by using Javascript
	 * @param driver
	 * @param elementDescription - Description of the element
	 * @throws Exception
	 */
	public static void javascriptClick(WebElement element, WebDriver driver, String elementDescription) throws Exception {

		try {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);

			String browser = Utils.getRunBrowser(driver);
			Log.event("Clicked on "+elementDescription);
			if(Utils.isDesktop() && !(browser.equals("safari") || browser.equals("firefox"))){
				Actions action = new Actions(driver);
				action.moveByOffset(10, 10).build().perform();
			}
		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}
	}
	
	/**
	 * To click on an element using Javascript without any Actions
	 * @param element - WebElement which has to be click by using Javascript
	 * @param driver
	 * @param elementDescription - Description of the element
	 * @throws Exception
	 */
	public static void javascriptClickWithoutAction(WebElement element, WebDriver driver, String elementDescription) throws Exception {
		try {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);

			Log.event("Clicked on " + elementDescription);
		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}
	}

	/**
	 * Wrapper to get a text from the provided WebElement
	 * @param driver : WebDriver Instance
	 * @param fromWhichTxtShldExtract : WebElement from which text to be extract in String format
	 * @return: String - text from web element
	 * @param elementDescription : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static String getText(WebDriver driver, WebElement fromWhichTxtShldExtract, String elementDescription) throws Exception {
		String textFromHTMLAttribute = null;
		try {
			Utils.waitForElement(driver, fromWhichTxtShldExtract);
			textFromHTMLAttribute = fromWhichTxtShldExtract.getText().trim();

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = fromWhichTxtShldExtract.getAttribute("textContent");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = fromWhichTxtShldExtract.getAttribute("value");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = fromWhichTxtShldExtract.getAttribute("innerHTML");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = fromWhichTxtShldExtract.getText();

		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}

		Log.event("Value Extracted for " + elementDescription + " :: " + textFromHTMLAttribute);
		return textFromHTMLAttribute.isEmpty() ? "" : textFromHTMLAttribute.trim();
	}
	
	/**
	 * Wrapper to get a text from the provided WebElement
	 * <br>
	 * To be used where framework needs to avoid early termination of test
	 * @param driver : WebDriver Instance
	 * @param fromWhichTxtShldExtract : WebElement from which text to be extract in String format
	 * @param elementDescription : Description about the WebElement
	 * @return Text from WebElement
	 * @throws Exception - Exception
	 */
	public static String getTextNoFail(WebDriver driver, WebElement fromWhichTxtShldExtract, String elementDescription) throws Exception {
		String textFromHTMLAttribute = null;
		try {
			Utils.waitForElement(driver, fromWhichTxtShldExtract);
			textFromHTMLAttribute = fromWhichTxtShldExtract.getText().trim();

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = fromWhichTxtShldExtract.getAttribute("textContent");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = fromWhichTxtShldExtract.getAttribute("value");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = fromWhichTxtShldExtract.getAttribute("innerHTML");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = fromWhichTxtShldExtract.getText();
		} catch (NoSuchElementException e) {
			Log.event(elementDescription + " not found in page / Locator change detected.");
		}

		Log.event("Value Extracted from Element :: " + textFromHTMLAttribute);
		return textFromHTMLAttribute.isEmpty() ? "" : textFromHTMLAttribute.trim();
	}
	

	/**
	 * Wrapper to get a text from the provided WebElement
	 * @param driver : WebDriver Instance
	 * @param fromWhichTxtShldExtract : WebElement from which text to be extract in String format
	 * @return: String - text from web element
	 * @param elementDescription : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static String getText(WebDriver driver, List<WebElement> fromWhichTxtShldExtract, String elementDescription) throws Exception {
		String textFromHTMLAttribute = " ";
		try {
			for(int i = 0; i < fromWhichTxtShldExtract.size(); i++){
				String textFromHTML = fromWhichTxtShldExtract.get(i).getText().trim() + System.lineSeparator();

				if (textFromHTML.isEmpty())
					textFromHTML += fromWhichTxtShldExtract.get(i).getAttribute("textContent") + System.lineSeparator();

				if (textFromHTML.isEmpty())
					textFromHTML += fromWhichTxtShldExtract.get(i).getAttribute("innerHTML") + System.lineSeparator();
				textFromHTMLAttribute += textFromHTML;
			}

		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}
		Log.event(elementDescription + " :: " + textFromHTMLAttribute);
		return textFromHTMLAttribute;

	}// getText

	/**
	 * Wrapper to get a text from the provided WebElement
	 * @param driver : WebDriver Instance
	 * @param fromWhichTxtShldExtract : WebElement from which text to be extract in String format
	 * @return: String - text from web element
	 * @param elementDescription : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static List<String> getText(
			List<WebElement> fromWhichTxtShldExtract, String elementDescription, WebDriver driver) throws Exception {

		List<String> textFromHTMLAttribute = new ArrayList<String>();
		try {
			for(int i = 0; i < fromWhichTxtShldExtract.size(); i++){
				String textFromHTML = fromWhichTxtShldExtract.get(i).getText().trim() + System.lineSeparator();

				if (textFromHTML.isEmpty())
					textFromHTML += fromWhichTxtShldExtract.get(i).getAttribute("textContent") + System.lineSeparator();

				if (textFromHTML.isEmpty())
					textFromHTML += fromWhichTxtShldExtract.get(i).getAttribute("innerHTML") + System.lineSeparator();
				textFromHTMLAttribute.add(textFromHTML);
			}
			Log.event(elementDescription + " :: " + textFromHTMLAttribute.toString());
		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}
		return textFromHTMLAttribute;
	}// getText

	/**
	 * Wrapper to get a text from the provided WebElement
	 * @param driver : WebDriver Instance
	 * @param fromWhichTxtShldExtract : String Input (CSS Locator) [from which text to be extract in String format]
	 * @return: String - text from web element
	 * @param elementDescription : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static String getText(WebDriver driver, String fromWhichTxtShldExtract, String elementDescription) throws Exception {
		String textFromHTMLAttribute = "";
		WebElement element = checkLocator(driver, fromWhichTxtShldExtract);
		try {
			textFromHTMLAttribute = element.getText().trim();

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = element.getAttribute("textContent");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = element.getAttribute("value");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = element.getAttribute("innerHTML");

			if (textFromHTMLAttribute.isEmpty())
				textFromHTMLAttribute = element.getText();

		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}
		Log.event(elementDescription + " :: " + textFromHTMLAttribute);
		return textFromHTMLAttribute;
	}// getText

	/**
	 * Wrapper to get a text from the provided WebElement's Attribute
	 * @param driver : WebDriver Instance
	 * @param fromWhichTxtShldExtract : WebElement from which text to be extract in String format
	 * @param attributeName : Attribute Name from which text should be extracted like "style, class, value,..."
	 * @return: String - text from web element
	 * @param elementDescription : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static String getTextFromAttribute(WebDriver driver, WebElement fromWhichTxtShldExtract, String attributeName, String elementDescription) throws Exception {
		String textFromHTMLAttribute = "";
		try {
			textFromHTMLAttribute = fromWhichTxtShldExtract.getAttribute(attributeName).trim();
		} catch (NoSuchElementException e) {
			Log.failsoft(elementDescription + " not found in page / Locator change detected.", driver);
		}
		Log.event(elementDescription + " :: " + textFromHTMLAttribute);
		return textFromHTMLAttribute;
	}// getTextFromAttribute

	/**
	 * Wrapper to get a text from the provided WebElement's Attribute
	 * @param driver : WebDriver Instance
	 * @param fromWhichTxtShldExtract : String Input (CSS Locator) [from which text to be extract in String format]
	 * @param attributeName : Attribute Name from which text should be extracted like "style, class, value,..."
	 * @return: String - text from web element
	 * @param elementDescription : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static String getTextFromAttribute(
			WebDriver driver, String fromWhichTxtShldExtract, String attributeName, String elementDescription) throws Exception {

		String textFromHTMLAttribute = "";
		WebElement element = checkLocator(driver, fromWhichTxtShldExtract);

		try {
			textFromHTMLAttribute = element.getAttribute(attributeName).trim();
		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}

		return textFromHTMLAttribute;

	}// getTextFromAttribute

	/**
	 * Wrapper to select option from combobox in browser and doesn't wait for spinner to disappear
	 * @param btn : String Input (CSS Locator) [of the ComboBox Field]
	 * @param optToSelect : option to select from combobox
	 * @param driver : WebDriver Instances
	 * @param elementDescription : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static void selectFromComboBox(
			String btn, String optToSelect, WebDriver driver, String elementDescription) throws Exception {

		WebElement element = checkLocator(driver, btn);
		try {
			Select selectBox = new Select(element);
			selectBox.selectByValue(optToSelect);
		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}

	}// selectFromComboBox

	/**
	 * Wrapper to select option from combobox in browser
	 * @param btn : WebElement of the combobox Field
	 * @param optToSelect : option to select from combobox
	 * @param driver : WebDriver Instances
	 * @param elementDescription : Description about the WebElement
	 */
	public static void selectFromComboBox(WebElement btn, String optToSelect,
			WebDriver driver, String elementDescription) {

		try {
			Select selectBox = new Select(btn);
			selectBox.selectByValue(optToSelect);
		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}

	}// selectFromComboBox

	/**
	 * To select the option which matches by visible text in dropdown
	 * @param element -
	 * @param value -
	 * @throws Exception - Exception
	 */
	public static void selectDropdownByValue(WebElement element, String value)throws Exception{
		try {
			Select select = new Select(element);
			select.selectByVisibleText(value);
		}catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.");
		}
	}

	/**
	 * To mouse hover on the given element
	 * @param driver -
	 * @param element -
	 * @param description -
	 * @throws Exception - Exception
	 */
	public static void mouseHover(WebDriver driver, WebElement element, String description)throws Exception {
		try {
			if(!Utils.waitForElement(driver, element)) {
				Log.fail("Element " + description + " not found in page. Cannot do mouse over.", driver);
			}
			scrollToViewElement(element, driver);
			if ((Utils.isDesktop() && Utils.getRunBrowser(driver).equals("safari"))
					|| Utils.getRunBrowser(driver).equals("internet explorer")
					|| Utils.getRunDevicePlatform().toLowerCase().contains("mac")) {
				moveToElementJS(driver, element);
			} else {
				Actions action = new Actions(driver);
				action.moveToElement(element).build().perform();
			}
			Log.event("Tried to Mouse hover on " + description);
		}catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.", driver);
		}
	}

	/**
	 * To perform mouse hover on an element using javascript
	 * @param driver - WebDriver instance
	 * @param element - WebElement to move to
	 */
	public static void moveToElementJS(WebDriver driver, WebElement element) {
		String jsExecCommand;
		if (Utils.getRunBrowser(driver).contains("ie")) {
			jsExecCommand = "var fireOnThis = arguments[0];" + "var evObj = document.createEvent('MouseEvents');"
					+ "evObj.initEvent( 'mouseover', true, true );" + "fireOnThis.dispatchEvent(evObj);";
		} else {
			jsExecCommand = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');"
					+ "evObj.initEvent('mouseover',true, false); arguments[0].dispatchEvent(evObj);}"
					+ " else if(document.createEventObject){ arguments[0].fireEvent('onmouseover');}";
		}
		((JavascriptExecutor) driver).executeScript(jsExecCommand, element);
	}
	
	/**
	 * To choose/check a radio/check box
	 * @param element - Web Element of radio/check box
	 * @param enableOrDisable - To check - YES
	 * 							To Uncheck - NO
	 */
	public static void selectRadioOrCheckbox(WebElement element, String enableOrDisable) {
		try {
			if ("YES".equalsIgnoreCase(enableOrDisable)) {
				if (!(isRadioOrCheckBoxSelected(element))) {
					element.click();
				}
			}
			if ("NO".equalsIgnoreCase(enableOrDisable)) {
				if (isRadioOrCheckBoxSelected(element)) {
					element.click();
				}
			}
		}catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.");
		}
	}

	/**
	 * To select the radio / checkbox
	 * @param element -
	 * @param driver -
	 * @param enableOrDisable -
	 * @throws Exception - Exception
	 */
	public static void selectRadioOrCheckbox(WebElement element,WebDriver driver, String enableOrDisable) throws Exception{
		try {
			if ("YES".equalsIgnoreCase(enableOrDisable)) {
				if (!(isRadioOrCheckBoxSelected(element))) {
					clickOnElementX(element, driver, "Checkbox/Radio Button");
				}
			}
			if ("NO".equalsIgnoreCase(enableOrDisable)) {
				if (isRadioOrCheckBoxSelected(element)) {
					clickOnElementX(element, driver, "Checkbox/Radio Button");
				}
			}
		}catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.");
		}
	}

	/**
	 * To select the radio / checkbox
	 * @param element -
	 * @param enableOrDisable -
	 * @param driver -
	 */
	public static void selectRadioOrCheckbox(WebElement element, String enableOrDisable, WebDriver driver)throws Exception {
		try {
			if(Utils.getRunBrowser(driver).toLowerCase().contains("edge")){
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].style.opacity=1", element); 
			}else{
				BrowserActions.moveToElementJS(driver, element);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].style.opacity=1", element); 

				if ("YES".equalsIgnoreCase(enableOrDisable)) {
					if (!(isRadioOrCheckBoxSelected(element))) {
						javascriptClick(element, driver, "element");
					}
				}
				if ("NO".equalsIgnoreCase(enableOrDisable)) {
					if (isRadioOrCheckBoxSelected(element)) {
						javascriptClick(element, driver, "element");
					}
				}
			}
		}catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.", driver);
		}
	}

	/**
	 * To verify the radio / checkbox is selected or not
	 * @param element -
	 * @return true - if selected
	 */
	public static boolean isRadioOrCheckBoxSelected(WebElement element) {
		try {
			if (element.getCssValue("background").contains("checkmark")) {
				return true;
			}

			if (element.getCssValue("background").contains("checkbox")) {
				return false;
			}

			if (element.getAttribute("class").contains("active")) {
				return true;
			}

			if (null != element.getAttribute("checked")) {
				return true;
			}

			if(null != element.getAttribute("value"))
				if(element.getAttribute("value").equals("true"))
					return true;

			for (WebElement childElement : element.findElements(By.xpath(".//*"))) {
				if (childElement.getAttribute("class").contains("active")) {
					return true;
				}
			}
		} catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.");
		}
		return false;
	}

	/**
	 * To check whether locator string is XPath or CSS
	 * @param driver -
	 * @param locator - 
	 * @return elements -
	 */
	public static List<WebElement> checkLocators(WebDriver driver, String locator) {
		List<WebElement> elements = null;
		try {
			if (locator.startsWith("//")) {
				elements = (new WebDriverWait(driver, 10).pollingEvery(Duration.ofMillis(500))
						.ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
						.withMessage("Couldn't find " + locator)).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(locator)));
			} else {
				elements = (new WebDriverWait(driver, 10).pollingEvery(Duration.ofMillis(500))
						.ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
						.withMessage("Couldn't find " + locator)).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(locator)));
			}
		} catch (NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.");
		}
		return elements;
	}

	/**
	 * To check whether locator string is XPath or CSS
	 * @param driver -
	 * @param locator -
	 * @return element -
	 */
	public static WebElement checkLocator(WebDriver driver, String locator) {
		WebElement element = null;
		try {
			if (locator.startsWith("//")) {
				element = (new WebDriverWait(driver, 10).pollingEvery(Duration.ofMillis(500))
						.ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
						.withMessage("Couldn't find " + locator)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
			} else {
				element = (new WebDriverWait(driver, 10).pollingEvery(Duration.ofMillis(500))
						.ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
						.withMessage("Couldn't find " + locator)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
			}
		}catch(Exception e) {
			if(e.getMessage().toLowerCase().contains("expected condition failed"))
				Log.fail(locator + " not found in page / Locator change detected.", driver);
			else
				Log.fail("Un-expected cause: " + e.getMessage().toString());
		}
		return element;
	}
	
	/**
	 * To get WebElement from page object by name
	 * @param element - name of element as declared in page object class
	 * @param obj - page object where element is declared in
	 * @return WebElement
	 * @throws Exception
	 */
	public static WebElement checkLocator(String element, Object obj) throws Exception {
		WebElement webElement = null;
		try {
			Field f = obj.getClass().getDeclaredField(element);
			f.setAccessible(true);
			webElement = ((WebElement) f.get(obj));
		} catch (Exception e) {
			Log.event("Locator may not be delared in object.");
		}
		return webElement;
	}

	/**
	 * To get matching text element from List of web elements
	 * @param elements -
	 * @param contenttext - text to match
	 * @param condition -
	 * @return elementToBeReturned as WebElement
	 * @throws Exception - Exception
	 */
	public static WebElement getMatchingTextElementFromList(
			List<WebElement> elements, String contenttext, String condition) throws Exception {
		WebElement elementToBeReturned = null;
		boolean found = false;
		try {
			if (elements.size() > 0) {
				for (WebElement element : elements) {
					if (condition.toLowerCase().equals("equals")
							&& element.getText().trim().replaceAll("\\s+", " ").equalsIgnoreCase(contenttext)) {
						elementToBeReturned = element;
						found = true;
						break;
					}

					if (condition.toLowerCase().equals("contains")
							&& element.getText().trim().replaceAll("\\s+", " ").contains(contenttext)) {
						elementToBeReturned = element;
						found = true;
						break;
					}
				}
				if (!found) {
					throw new Exception("Didn't find the correct text(" + contenttext + ")..! on the page");
				}
			} else {
				throw new Exception("Unable to find list element...!");
			}
		}catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.");
		}
		return elementToBeReturned;
	}

	/**
	 * Open a new tab on the browser
	 * @param driver -
	 */
	public static void openNewTab(WebDriver driver) {
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
	}

	/**
	 * To navigate to back to previous page
	 * @param driver -
	 */
	public static void navigateToBack(WebDriver driver) {
		driver.navigate().back();
		Utils.waitForPageLoad(driver);
	}

	/**
	 * To select dropdown by index
	 * @param element -
	 * @param index -
	 */
	public static void selectDropdownByIndex(WebElement element, int index) {
		try {
			Select selectByIndex = new Select(element);
			selectByIndex.selectByIndex(index);
		}catch(NoSuchElementException e) {
			e.printStackTrace();
			Log.fail("Element not found in page / Locator change detected.");
		}
	}

	/**
	 * To get select option from dropdown
	 * @param element -
	 * @return String -
	 */
	public static String getSelectedOption(WebElement element) {
		try {
			Select selectByIndex = new Select(element);
			WebElement ele = selectByIndex.getFirstSelectedOption();
			return ele.getText().trim();
		}catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.");
		}
		return null;
	}

	/**
	 * To verify given elements are same
	 * @param driver -
	 * @param element1 -
	 * @param element2 -
	 * @return Boolean -
	 * @throws Exception - Exception
	 */
	public static Boolean verifyElementsAreInSameRow(
			WebDriver driver, WebElement element1, WebElement element2) throws Exception {
		try {
			Point ele1 = element1.getLocation();
			Point ele2 = element2.getLocation();

			Log.event("Element1 Position :: " + ele1);
			Log.event("Element2 Position :: " + ele2);

			if(ele1.y == ele2.y || ((ele1.y - ele2.y) <= 5))
				return true;
			else
				return false;
		}catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.");
		}
		return false;
	}

	/**
	 * Wrapper to verify if the first element is above the second element in the UI
	 * @param driver : WebDriver Instance
	 * @param elementAbove : WebElement which is present above in the UI
	 * @return: Boolean
	 * @param elementBelow : WebElement which is present below in the UI
	 * @throws Exception - Exception
	 */
	public static Boolean verifyVerticalAllignmentOfElements(
			WebDriver driver, WebElement elementAbove, WebElement elementBelow) throws Exception {
		
		try {
			scrollToTopOfPage(driver);
			Point eleAbove = elementAbove.getLocation();
			Point eleBelow = elementBelow.getLocation();
			if(eleAbove.y < eleBelow.y)
				return true;
			else
				return false;
		}catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.");
		}
		return null;
	}

	/**
	 * Wrapper to verify if the first element is right to the second element in the UI
	 * @param driver : WebDriver Instance
	 * @param elementRight : WebElement which is present right side in the UI
	 * @return: Boolean
	 * @param elementLeft : WebElement which is present left side in the UI
	 * @throws Exception - Exception
	 */
	public static Boolean verifyHorizontalAllignmentOfElements(
			WebDriver driver, WebElement elementRight, WebElement elementLeft) throws Exception {
		
			Point eleRight = elementRight.getLocation();
			Point eleLeft = elementLeft.getLocation();

			Log.event(eleLeft.x + "-->" + eleRight.x);
			if(eleLeft.x < eleRight.x)
				return true;
			else
				return false;
	}

	/**
	 * To refresh page
	 * @param driver -
	 * @throws Exception - Exception
	 */
	public static void refreshPage(WebDriver driver)throws Exception{
		driver.navigate().refresh();
		BrowserActions.scrollToTopOfPage(driver);
		Utils.waitForPageLoad(driver);
	}
	
	/**
	 * To get dimension of element
	 * @param element - element to get dimension of
	 * @return dimension - 
	 * @throws Exception - Exception
	 */
	public static HashMap<ElementDimension, Integer> getElementDimension(WebElement element)throws Exception{
		HashMap<ElementDimension, Integer> dimension = new HashMap<ElementDimension, Integer>();
		dimension.put(ElementDimension.height, 0);
		dimension.put(ElementDimension.width, 0);
		try {
			int height = element.getSize().getHeight();
			int width = element.getSize().getWidth();
			dimension.put(ElementDimension.height, height);
			dimension.put(ElementDimension.width, width);
			Log.event("Element height and width :: " + height + ", " + width);
		}catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.");
		}
		return dimension;
	}

	/**
	 * To vertically click on outside of a modal/popup
	 * @param driver - driver
	 * @param element - element
	 * @param obj - obj
	 * @throws Exception - Exception
	 */
	public static void clickOnEmptySpaceVertical(WebDriver driver, String element, Object obj)throws Exception{
		try {
			Field f = obj.getClass().getDeclaredField(element);
			f.setAccessible(true);
			WebElement webElement = ((WebElement) f.get(obj));

			if(Utils.getRunBrowser(driver).equalsIgnoreCase("Safari")) {
				try {
					int clickLocationX = webElement.getLocation().x + webElement.getSize().getWidth()/2;
					int clickLocationY = webElement.getLocation().y + webElement.getSize().getHeight() + 20;
					JavascriptExecutor executor = (JavascriptExecutor) driver;
					executor.executeScript("document.elementFromPoint(arguments[0], arguments[1]).click();", clickLocationX, clickLocationY);
				} catch(Exception e) {
					int x = webElement.getLocation().getX();
					int y = webElement.getLocation().getY() + 10;
					String code = "document.elementFromPoint(" + x + "," + y + ").click();";
					((JavascriptExecutor) driver).executeScript(code);
				}
			} else {
				int clickLocationX = webElement.getLocation().x + webElement.getSize().getWidth()/2;
				int clickLocationY = webElement.getLocation().y + webElement.getSize().getHeight() + 20;
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("document.elementFromPoint(arguments[0], arguments[1]).click();", clickLocationX, clickLocationY);
			}
		}catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.");
		}catch(NoSuchFieldException e1) {
			Log.fail("Given element not declared in page object");
		}
	}
	
	/**
	 * To vertically click on outside of a modal/popup
	 * @param driver - driver
	 * @param element - element
	 * @throws Exception - Exception
	 */
	public static void clickOnEmptySpaceVertical(WebDriver driver, WebElement element)throws Exception{
		try {
			WebElement webElement = element;
			if(Utils.getRunBrowser(driver).equalsIgnoreCase("Safari")) {
				try {
					int clickLocationX = webElement.getLocation().x + webElement.getSize().getWidth()/2;
					int clickLocationY = webElement.getLocation().y + webElement.getSize().getHeight() + 20;
					JavascriptExecutor executor = (JavascriptExecutor) driver;
					executor.executeScript("document.elementFromPoint(arguments[0], arguments[1]).click();", clickLocationX, clickLocationY);
				}catch(Exception e) {
					int x = webElement.getLocation().getX();
					int y = webElement.getLocation().getY() + 10;
					String code = "document.elementFromPoint(" + x + "," + y + ").click();";
					((JavascriptExecutor) driver).executeScript(code);
				}
			}
			else {
				int clickLocationX = webElement.getLocation().x + webElement.getSize().getWidth()/2;
				int clickLocationY = webElement.getLocation().y + webElement.getSize().getHeight() + 20;
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("document.elementFromPoint(arguments[0], arguments[1]).click();", clickLocationX, clickLocationY);
			}
		}catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.");
		}
	}


	/**
	 * To horizontally click on outside of a modal/popup
	 * @param driver - driver
	 * @param element - element
	 * @param obj - obj
	 * @throws Exception - Exception
	 */
	public static void clickOnEmptySpaceHorizontal(WebDriver driver, String element, Object obj)throws Exception{
		try {
			Field f = obj.getClass().getDeclaredField(element);
			f.setAccessible(true);
			WebElement webElement = ((WebElement) f.get(obj));

			if(Utils.getRunBrowser(driver).equalsIgnoreCase("Safari")) {
				try {
					int clickLocationX = webElement.getLocation().x + webElement.getSize().getWidth() + 20;
					int clickLocationY = webElement.getLocation().y + webElement.getSize().getHeight()/2;
					JavascriptExecutor executor = (JavascriptExecutor) driver;
					executor.executeScript("document.elementFromPoint(arguments[0], arguments[1]).click();", clickLocationX, clickLocationY);
				}catch(Exception e) {
					int x = webElement.getLocation().getX() - 10;
					int y = webElement.getLocation().getY();
					String code = "document.elementFromPoint(" + x + "," + y + ").click();";
					((JavascriptExecutor) driver).executeScript(code);
				}
			}
			else{
				int clickLocationX = webElement.getLocation().x + webElement.getSize().getWidth() + 20;
				int clickLocationY = webElement.getLocation().y + webElement.getSize().getHeight()/2;
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("document.elementFromPoint(arguments[0], arguments[1]).click();", clickLocationX, clickLocationY);
			}
		}catch(NoSuchElementException e) {
			Log.fail("Element not found in page / Locator change detected.");
		}catch(NoSuchFieldException e1) {
			Log.fail("Given element not declared in page object");
		}
	}

	/**
	 * To delete cookies
	 * @param driver - WebDriver instance
	 * @return HomePage instance
	 * @throws Exception
	 */
	public static HomePage clearCookies(WebDriver driver) throws Exception{
		driver.manage().deleteAllCookies();
		return new HomePage(driver, Utils.getWebSite()).get();
	}

	/**
	 * To mouse hover on element using actions
	 * @param driver -
	 * @param element -
	 */
	public static void mouseHover(WebDriver driver, WebElement element) {
		Actions actions = new Actions(driver);
		actions.moveToElement(element).build().perform();
	}

	/**
	 * To mouse hover on element using java script
	 * @param driver -
	 * @param element -
	 */
	public static void javaScriptMouseHover(WebDriver driver, WebElement element) {
		String javaScript =
				"var evObj = document.createEvent('MouseEvents');" +
						"evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);"
						+ "arguments[0].dispatchEvent(evObj);"; 
		((JavascriptExecutor) driver).executeScript(javaScript, element);
	}

	/**
	 * To verify if element(s) are displayed in page
	 * @param driver - driver
	 * @param expectedElements - expectedElements
	 * @return boolean - true/false if all elements are displayed
	 */
	public static boolean VerifyElementDisplayed(WebDriver driver, List<WebElement> expectedElements) {
		List<WebElement> missingElements = new ArrayList<WebElement>();
		for (WebElement expEle : expectedElements) {
			try {
				if (expEle.isDisplayed()) {
				} else {
					missingElements.add(expEle);
				}
			} catch (NoSuchElementException e){
				missingElements.add(expEle);
			}
		}
		
		if(missingElements.size() != 0) {
			Log.event("Elements missing in page:: " + missingElements.size());
			return false;
		} else {
			Log.event("All elements found in page.");
			return true;
		}
			
	}
	
	/**
	 * Set browser window size to target resolution
	 * @param driver - WebDriver instance
	 * @param targetResolution - resolution to set browser window to in "resX * resY" format
	 * @throws Exception
	 */
	public static void setBrowserWindowSize(WebDriver driver, String targetResolution) throws Exception{
		targetResolution = targetResolution.replaceAll("[^0-9]", " ");
		int resX = StringUtils.getNumberInString(targetResolution.split(" ")[0]);
		int resY = StringUtils.getNumberInString(targetResolution.split(" ")[1]);
		Dimension customWindow = new Dimension(resX , resY);
		driver.manage().window().setSize(customWindow);
	}
	
	/**
	 * To upload image
	 * @param btnUpload : WebElement of the upload button
	 * @param imagePath : Image path to upload
	 * @param driver : WebDriver Instances
	 * @param elementDescription : Description about the WebElement
	 * @throws Exception - Exception
	 */
	public static void uploadImage(WebElement btnUpload, String imagePath, WebDriver driver, String elementDescription) throws Exception {
		try {
			btnUpload.sendKeys(imagePath);
		} catch (NoSuchElementException e) {
			Log.fail(elementDescription + " not found in page / Locator change detected.", driver);
		}
	}
}// BrowserActions